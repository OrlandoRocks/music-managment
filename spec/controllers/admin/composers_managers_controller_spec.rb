require "rails_helper"

describe Admin::ComposersManagersController do
  before :each do
    @admin    = create(:person, :admin)
    @person   = create(:person)
    @account  = create(:account, person: @person)

    login_as(@admin)
  end

  describe "#show" do
    it "responds successfully" do
      get :show, params: { id: @account.id }
      expect(response.status).to eq 200
    end
  end

  describe "#update" do
    before :each do
      @tc_user  = create(:person)
      @composer = create(:composer, account: @account, person_id: nil)
    end

    context "person_id passes validation" do
      it "updates the composer's person_id to the one provided" do
        post :update, params: { id: @account.id, composer: { id: @composer.id, person_id: @tc_user.id } }
        expect(@composer.reload.person_id).to eq @tc_user.id
      end

      it "updates the composer's person_id to nil if nothing is provided" do
        post :update, params: { id: @account.id, composer: { id: @composer.id, person_id: "" } }
        expect(@composer.reload.person_id).to eq nil
      end

      it "updates the composer's person_id to nil if the one provided is 0" do
        post :update, params: { id: @account.id, composer: { id: @composer.id, person_id: 0 } }
        expect(@composer.reload.person_id).to eq nil
      end
    end

    context "person_id does not pass validation" do

      it "does not update the composer's person_id if the provided person_id does not exist" do
        expect do
          post :update, params: { id: @account.id, composer: { id: @composer.id, person_id: "dana" } }
        end.to_not change { @composer.reload.person_id }
      end

      it "does not update if the provided person_id is already associated to a composer managed by somebody else" do
        other_account   = create(:account)
        other_composer  = create(:composer, account: other_account, person: other_account.person)
        other_person    = other_composer.person

        expect do
          post :update, params: { id: @account.id, composer: { id: @composer.id, person_id: other_person.id} }
        end.to_not change { @composer.reload.person_id }
      end
    end
  end

  describe "#destroy" do
    let(:account) { @account }
    let(:composer) { create(:composer, account: account, person_id: nil) }

    context "when the params are valid" do
      let!(:extra_composer) { create(:composer, account: account, person_id: nil) }
      let!(:terminated_composer) { create(:terminated_composer, :fully, composer: composer) }

      it "deletes the composer" do
        expect(Composer.find_by(id: composer.id)).to be_truthy

        delete :destroy, params: { id: account.id, composer: { id: composer.id } }

        expect(Composer.find_by(id: composer.id)).to be_falsy
      end

      it "deletes the composer" do
        delete :destroy, params: { id: account.id, composer: { id: composer.id } }

        expect(subject.request.flash[:notice]).to include("Composer with ID of #{composer.id} has been successfully deleted!")
      end
    end

    context "when the params are invalid" do
      context "when a composer is not passed in" do
        it "flashes an error" do
          delete :destroy, params: { id: account.id, composer: { id: nil } }

          expect(subject.request.flash[:error]).to include("Composer can't be blank")
        end
      end

      context "when a composer is passed in" do
        context "when that composer does not have a terminated_composer" do
          let!(:extra_composer) { create(:composer, account: account, person_id: nil) }

          it "it does not delete the composer and flashes an error" do
            delete :destroy, params: { id: account.id, composer: { id: composer.id } }

            expect(Composer.find_by(id: composer.id)).to be_truthy
            expect(subject.request.flash[:error]).to include("Composer does not have a terminated_composer record with a 'fully' termination_type")
          end
        end

        context "when that composer does have a terminated_composer, but it is only partially terminated" do
          let!(:extra_composer) { create(:composer, account: account, person_id: nil) }
          let!(:terminated_composer) { create(:terminated_composer, :partially, composer: composer) }

          it "it does not delete the composer and flashes an error" do
            delete :destroy, params: { id: account.id, composer: { id: composer.id } }

            expect(Composer.find_by(id: composer.id)).to be_truthy
            expect(subject.request.flash[:error]).to include("Composer does not have a terminated_composer record with a 'fully' termination_type")
          end
        end

        context "when that composer is the only composer on the account" do
          let!(:terminated_composer) { create(:terminated_composer, :fully, composer: composer) }

          it "it does not delete the composer and flashes an error" do
            delete :destroy, params: { id: account.id, composer: { id: composer.id } }

            expect(Composer.find_by(id: composer.id)).to be_truthy
            expect(subject.request.flash[:error]).to include("Composer cannot be deleted if it is the only one on the account")
          end
        end

        context "when that composer is the primary account composer" do
          let(:composer) { create(:composer, account: account, person_id: account.person.id) }
          let!(:terminated_composer) { create(:terminated_composer, :fully, composer: composer) }
          let!(:extra_composer) { create(:composer, account: account, person_id: nil) }

          it "it does not delete the composer and flashes an error" do
            delete :destroy, params: { id: account.id, composer: { id: composer.id } }

            expect(Composer.find_by(id: composer.id)).to be_truthy
            expect(subject.request.flash[:error]).to include("Composer cannot be deleted if it is the primary account composer")
          end
        end
      end
    end
  end
end
