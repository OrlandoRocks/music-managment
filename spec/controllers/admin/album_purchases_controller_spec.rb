require "rails_helper"

describe Admin::AlbumPurchasesController, type: :controller do
 # These specs are broken. Routes not found.
 xcontext  "when being accessed by non-administrators" do
  before(:each) do
    person = TCFactory.build_person(:force_is_administrator => false)
    login_as(person)
  end

  it "GET 'show' should not be successful" do
    get 'show'
    expect(response).not_to be_successful
  end

  it "GET 'index' should not be successful" do
    get 'index'
    expect(response).not_to be_successful
  end
 end

context "when setting up prior to the action" do

  before(:each) do
    person = TCFactory.build_person(:force_is_administrator => true)
    login_as(person)

    @person = mock_model(Person)
    allow(Person).to receive(:find).and_return(@person)

    album_proxy = double('album_proxy')
    @album = Album.new
    allow(@album).to receive(:related_purchases).and_return([])
    expect(album_proxy).to receive(:find).with("808").and_return(@album)

    allow(@person).to receive(:albums).and_return(album_proxy)
    allow(@person).to receive(:is_administrator?).and_return(true)
  end

  it "should load the person and album on show" do
    allow(@controller).to receive(:load_purchase)
    expect(Person).to receive(:find).with("909").and_return(@person)
    get 'show', params: { person_id: "909", album_id: "808", id: "1" }
  end

  it "should load the person and album on index" do
    expect(Person).to receive(:find).with("909").and_return(@person)
    get 'index', params: { person_id: "909", album_id: "808" }
  end

  it "should assign @person on show" do
    allow(@controller).to receive(:load_purchase)
    get 'show', params: { person_id: "909", album_id: "808", id: "1" }
    expect(assigns(:person)).to eql(@person)
  end

  it "should assign @album on show" do
    allow(@controller).to receive(:load_purchase)
    get 'show', params: { person_id: "909", album_id: "808", id: "1" }
    expect(assigns(:album)).to eql(@album)
  end

  it "should assign @person on index" do
    get 'index', params: { person_id: "909", album_id: "808" }
    expect(assigns(:person)).to eql(@person)
  end

  it "should assign @album on index" do
    get 'index', params: { person_id: "909", album_id: "808" }
    expect(assigns(:album)).to eql(@album)
  end
end

context "when an administrator is adding a cert" do
  before(:each) do
    TCFactory.generate_album_product
    @admin_person = TCFactory.build_person(:force_is_administrator => true)
    login_as(@admin_person)

    @person = TCFactory.build_person(:name => 'some_user', :email => 'someone@somewhere.net')
    @album = TCFactory.build_album(:name => 'my album', :person => @person)
    @purchase = Product.add_to_cart(@person, @album)
  end

  def controller_params(params = {})
    {:person_id => @person.to_param, :album_id => @album.to_param, :id => @purchase.to_param}.merge(params)
  end

  it "should respond to GET of new_cert" do
    get 'new_cert', params: controller_params
    expect(response).to be_successful
  end

  it "should render the correct template" do
    get 'new_cert', params: controller_params
    expect(response).to render_template('admin/album_purchases/new_cert')
  end

  it "should not redirect if Cert will not verify" do
    allow(Cert).to receive(:verify).and_return("FAIL")
    post 'create_cert', params: controller_params(:cert_code => 'DOES NOT MATTER')
    expect(response).not_to be_redirect
    expect(response).to render_template('admin/album_purchases/new_cert')
  end

  it "should render the new_cert form if Cert will not verify" do
    allow(Cert).to receive(:verify).and_return("FAIL")
    post 'create_cert', params: controller_params(:cert_code => 'DOES NOT MATTER')
    expect(response).to render_template('admin/album_purchases/new_cert')
  end

  it "should stick any refusal message into flash" do
    allow(Cert).to receive(:verify).and_return("FAIL")
    post 'create_cert', params: controller_params(:cert_code => 'DOES NOT MATTER')
    expect(flash[:error]).to match(/FAIL/)
  end
end
end
