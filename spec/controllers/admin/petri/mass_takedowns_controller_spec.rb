require 'rails_helper'

describe Admin::Petri::MassTakedownsController, type: :controller do

  before(:each) do
    @person = TCFactory.build_person(force_is_administrator: true)
    @album = TCFactory.create(:album)
    Store.all.each do |store|
      TCFactory.create(
        :salepoint,
        store: store,
        album: @album,
        variable_price: store.variable_prices.try(:first),
      )
    end

    login_as(@person)
  end

  context 'takedown distribution for all' do
    before(:each) do
      @album.petri_bundles << FactoryBot.create(:petri_bundle)
      @album.save
    end

    it 'marks takedown_at when album.id is argument' do
      params = { mass_takedown_form: { album_level_stores: :all, album_ids: @album.id, send_email: false } }
      post :create, params: params
      expect(@album.reload.takedown_at).to be_truthy
    end

    it 'marks takedown_at when album upc is argument ' do
      params = { mass_takedown_form: { album_level_stores: :all, album_ids: @album.upc.number, send_email: false } }
      post :create, params: params
      expect(@album.reload.takedown_at).to be_truthy
    end
  end

  it 'should takedown an album from all stores' do
    params = {
      mass_takedown_form: {
        album_level_stores: [16, 17, 18, 19],
        album_ids: @album.id,
        send_email: false,
      },
    }
    expect_any_instance_of(Album).not_to receive(:takedown!)

    post :create, params: params

    notifications = assigns(:notify_list)

    # One person
    expect(notifications.length).to eq 1
    # One album
    expect(notifications.first[:albums].length).to eq 1
    # Four salepoints
    expect(notifications.first[:albums].first[:stores].length).to eq 4
  end

  it 'calls the believe API when all selected' do
    allow_any_instance_of(Album).to receive(:takedown!).and_return(true)
    params = { mass_takedown_form: { album_level_stores: :all, album_ids: @album.id, send_email: false } }
    post :create, params: params
  end

  describe ".encumbrance_info" do
    let(:album1) {create(:album)}
    let(:album2) {create(:album)}

    it 'should fetch album ids if album person has encumbrance info' do
      create(:encumbrance_summary, person: album1.person)
      post :encumbrance_info, params: {album_ids: "#{album1.id}, #{album2.id}"}
      response_body = JSON.parse(response.body)
      expect(response_body['album_ids']).to eq(["#{album1.id}"])
    end

    it "should return empty array if person doens't have encumbrance" do
      post :encumbrance_info, params: {album_ids: "#{album1.id}, #{album2.id}"}
      response_body = JSON.parse(response.body)
      expect(response_body['album_ids']).to eq([])
    end

    it "should return empty array if person doens't have encumbrance" do
      create(:encumbrance_summary, person: album2.person)
      post :encumbrance_info, params: {album_ids: "#{album1.id}, #{album2.upc.number}"}
      response_body = JSON.parse(response.body)
      expect(response_body['album_ids']).to eq(["#{album2.upc.number}"])
    end

    it "should return empty array params is empty" do
      post :encumbrance_info, params: {album_ids: ""}
      response_body = JSON.parse(response.body)
      expect(response_body['album_ids']).to eq([])
    end

  end
end
