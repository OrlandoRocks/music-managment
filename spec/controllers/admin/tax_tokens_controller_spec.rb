require "rails_helper"

describe Admin::TaxTokensController do
  let(:admin) { create(:person, :with_role) }

  before :each do
    login_as(admin)
    @tax_tokens_cohort = create(:tax_tokens_cohort)
  end

  describe "#index" do
    it "returns a successful response" do
      get :index, params: { tax_tokens_cohort_id: @tax_tokens_cohort.id }
      expect(response.status).to eq(200)
    end
  end

  describe "#update" do
    before :each do
      @tax_tokens_cohort = create(:tax_tokens_cohort)
      tax_tokens = create_list(:tax_token, 5, tax_tokens_cohort: @tax_tokens_cohort)
    end

    it "updates is_visible to false for all the tax tokens the person belongs has" do
      tax_token_id = @tax_tokens_cohort.tax_tokens.last.id

      allow_any_instance_of(controller.request.class).to receive(:referrer)
        .and_return('http://development.tunecore.local:3000/admin/people/2')

      put :update, params: { tax_tokens_cohort_id: @tax_tokens_cohort.id, tax_token_id: tax_token_id }
      expect(TaxToken.where(person_id: @person_id).all? {|tax_token| tax_token.is_visible?}).to eq(true)
    end
  end
end
