require "rails_helper"

describe Admin::BulkAlbumApprovalsController, type: :controller do
  before :each do
    login_as(Person.find_by_name!("Tunecore Admin"))
  end

  describe "#new" do
    it "loads low risk albums" do
      albums = [double]
      expect(LowRiskArtistsQueryBuilder).to receive(:build).and_return(albums)

      get :new

      expect(response.status).to eq 200
      expect(assigns(:low_risk_albums)).to eq albums
    end
  end

  describe "#create" do
    it "redirects to new page if save is succussful" do
      bulk_album_approval_params = {}
      fake_form = double(save: true)
      expect(BulkAlbumApprovalForm).to receive(:new).with(bulk_album_approval_params).and_return(fake_form)
      post :create, params: bulk_album_approval_params
      expect(response).to redirect_to new_admin_bulk_album_approval_path
    end

    it "renders new page if save is unsuccussful" do
      bulk_album_approval_params = {}
      fake_form = double(save: false)
      expect(BulkAlbumApprovalForm).to receive(:new).with(bulk_album_approval_params).and_return(fake_form)
      post :create, params: bulk_album_approval_params
      expect(response).to render_template :new
    end
  end
end
