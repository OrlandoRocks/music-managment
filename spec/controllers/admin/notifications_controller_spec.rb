require "rails_helper"

describe Admin::NotificationsController do
  before(:each) do
    @login_as = Person.find_by_name!("Tunecore Admin")
    @role =  Role.create(:name => "Notifications")
    @login_as.roles << @role
    @login_as.save

    login_as(@login_as)

    @notification = create(:targeted_notification)
  end

  describe "#index" do
    it "shows targeted notifications" do
      get :index

      assert_response :success, @response.body
      expect(assigns(:notifications)).to include(@notification)
    end

    it "requires notification role" do
      @login_as.roles.clear
      @login_as.save

      get :index

      expect(response).to be_redirect
    end
  end

  describe "#show" do
    it "shows a targeted notification" do
      get :show, params: { id: @notification.id }

      assert_response :success, @response.body
      assert assigns(:notification) == @notification
    end
  end

  describe "#new" do
    it "builds a notification" do
      get :new

      assert_response :success, @response.body
      assert assigns(:notification).new_record?
    end
  end

  describe "#create" do
    before(:each) do
      @params = {
        title: "Title",
        text: "Text",
        url: "www.tunecore.com",
        link_text: "Click Here MAN",
        global: true
      }
    end

    it "creates a new notification and sets the creator" do
      expect {
        post :create, params: { targeted_notification: @params }
        expect(response).to be_redirect
      }.to change(TargetedNotification, :count).by(1)

      expect(assigns(:notification)).not_to be_blank

      @params.each do |k,v|
        expect(assigns(:notification).send(k)).to eq(v)
      end

      expect(assigns(:notification).person).to eq(@login_as)
    end

    it "only creates valid notifications" do
      expect {
        post :create, params: { targeted_notification: { text: 'this is missing a bunch of required params' } }
        assert_response :success, @response.body
      }.not_to change(TargetedNotification, :count)
    end

    it "allows icon upload" do
      expect {
        post :create, params: {
             targeted_notification: @params,
             file: Rack::Test::UploadedFile.new("#{Rails.root}/spec/files/albumcover.jpg", "image/jpeg")
        }
      }.to change(NotificationIcon, :count).by(1)
    end

    it "allows selection of an existing icon" do
      notification_icon = create(:notification_icon)

      expect {
        post :create, params: { targeted_notification: @params.merge(notification_icon_id: notification_icon.id) }
      }.to change(TargetedNotification, :count).by(1)

      expect(assigns[:notification].notification_icon_id).to eq(notification_icon.id)
    end
  end

  describe "#edit" do
    it "retrieves a notification" do
      get :edit, params: { id: @notification.id }

      assert_response :success, @response.body
      expect(assigns(:notification)).to eq(@notification)
    end
  end

  describe "#update" do
    it "updates a targeeted notification" do
      put :update, params: { id: @notification.id, targeted_notification: {text: "Updated Text"} }

      expect(response).to be_redirect
      expect(assigns(:notification).text).to eq("Updated Text")
    end

    it "only updates to a valid notification" do
      put :update, params: { id: @notification.id, targeted_notification: {text: ""} }

      assert_response :success, @response.body
      expect(assigns(:notification).reload.text).not_to be_blank
    end
  end

  describe "#destroy" do
    it "deletes the notification" do

      expect {
        delete :destroy, params: { id: @notification.id }
        assert_redirected_to admin_notifications_path
      }.to change(TargetedNotification, :count).by(-1)
    end

    it "renders show if it cannot destroy" do
      expect_any_instance_of(TargetedNotification).to receive(:destroy).and_return(false)

      expect {
        delete :destroy, params: { id: @notification.id }
        assert_response :success, @response.body
      }.not_to change(TargetedNotification, :count)
    end
  end
end
