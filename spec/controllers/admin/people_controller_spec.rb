require "rails_helper"

describe Admin::PeopleController, type: :controller do
  describe "POST #update_profile", versioning: true do
    context "when updating the user's email" do
      it "updates the email and creates a note for the change" do
        person = create(:person)
        new_email = "update_profile_check@example.com"

        login_as(Person.find_by(email: "super_admin@tunecore.com"))

        post :update_profile, params: {
          id: person.id,
          person: {
            email: new_email,
            postal_code: person.us_zip_code.code
          }
        }

        person.reload

        expect(person.email).to eq(new_email)
        expect(person.notes.first.note).to match(/Changed field "email"/)
      end
    end

    context "when updating the user's zipcode" do
      it "updates the zipcode and creates a note for the change" do
        person = create(:person)
        new_postal_code = "07057"

        login_as(Person.find_by(email: "super_admin@tunecore.com"))

        post :update_profile, params: {
          id: person.id,
          person: {
            email: person.email,
            postal_code: new_postal_code
          }
        }

        person.reload

        expect(person.us_zip_code.code).to eq(new_postal_code)
        expect(person.notes.first.note).to match(/Changed field.*code/)
      end
    end
  end

  describe "#migrate_to_bigbox" do

    before(:each) do
      login_as(Person.where("email = ?","super_admin@tunecore.com").first)
      @person = TCFactory.create(:person)
      allow(Person).to receive(:find).with(@person.id.to_s).and_return(@person)
    end

    it "migrates the person to bigbox" do
      expect(@person).to receive(:migrate_to_bigbox).and_return(1)
      get :migrate_to_bigbox, params: { id: @person.id }
    end

    it "redirects with a status message" do
      expect(@person).to receive(:migrate_to_bigbox).and_return(4)
      get :migrate_to_bigbox, params: { id: @person.id }
      assert_redirected_to admin_person_path(@person)
      assert flash[:success].include?("Sent #{4}")
    end
  end

  describe "#remove_dormancy" do
    before(:each) do
      login_as(Person.find_by(email: "super_admin@tunecore.com"))
      @person = create(:person, dormant: true, recent_login: Time.current - 3.year)
    end

    it "redirects to admin_person_path" do
      patch :remove_dormancy, params: { :id=>@person.id }
      assert_redirected_to admin_person_path(@person)
    end
    context "note fails" do
      it "flashes error" do
        allow(Note).to receive(:create!).and_raise(StandardError)
        patch :remove_dormancy, params: { :id=>@person.id }
        assert flash[:error].include?("Unable to save dormancy removal")
      end
      it "doesn't update dormancy" do
        allow(Note).to receive(:create!).and_raise(StandardError)
        patch :remove_dormancy, params: { :id=>@person.id }
        @person.reload
        expect(@person.dormant).to be(true)
      end
    end
    context "update fails" do
      it "flashes error" do
        allow_any_instance_of(Person).to receive(:update!).and_raise(StandardError)
        patch :remove_dormancy, params: { :id=>@person.id }
        assert flash[:error].include?("Unable to save dormancy removal")
      end
      it "doesn't update dormancy" do
        allow_any_instance_of(Person).to receive(:update!).and_raise(StandardError)
        patch :remove_dormancy, params: { :id=>@person.id }
        @person.reload
        expect(@person.dormant).to be(true)
      end
    end
    context "success" do
      it "removes dormancy" do
        patch :remove_dormancy, params: { :id=>@person.id }
        @person.reload
        expect(@person.dormant).to be(false)
      end
      it "sets recent_login" do
        old_login = @person.recent_login
        patch :remove_dormancy, params: { :id=>@person.id }
        @person.reload
        expect(@person.recent_login).to be > old_login
      end
      it "creates note" do
        old_num_of_notes = Note.count
        patch :remove_dormancy, params: { :id=>@person.id }
        expect(Note.count).to eq(old_num_of_notes + 1)
        expect(Note.last.note).to eq("Removed Dormant Status for Person_id: #{@person.id}. Recent Login Updated.")
      end
    end
  end

  describe ".create_recover_assets" do
    let(:person1) { create(:person) }
    let(:person2) { create(:person) }
    let(:download_assets_role) { create(:role, name: "Mass Download Assets") }
    let(:album) { create(:album, person: person2) }

    it "should authorize and redirect with flash message" do
      person1.roles << Role.find_by(name: "Admin")
      login_as(person1)

      post :create_recover_assets, params: { id: person2.id, admin_recover_user_assets_form: {album_ids_and_isrcs: album.id }}
      expect(response).to redirect_to('/')
      expect(flash[:alert]).to match(/You are not authorized to do this./)
    end

    it "should save and redirect with flash message" do
      person1.roles << [Role.find_by(name: "Admin"), download_assets_role]
      login_as(person1)

      allow_any_instance_of(Admin::RecoverUserAssetsForm).to receive(:save).and_return(true)
      post :create_recover_assets, params: { id: person2.id, admin_recover_user_assets_form: {album_ids_and_isrcs: album.id }}
      expect(response).to redirect_to(download_assets_admin_person_path)
      expect(flash[:success]).to match(/Request has been submitted successfuly. Please find the status in the items listed below./)
    end

    it "should save and redirect with flash message" do
      person1.roles << [Role.find_by(name: "Admin"), download_assets_role]
      login_as(person1)

      allow_any_instance_of(Admin::RecoverUserAssetsForm).to receive(:save).and_return(false)
      post :create_recover_assets, params: { id: person2.id, admin_recover_user_assets_form: {album_ids_and_isrcs: album.id }}
      expect(response.status).to eq(200)
      expect(flash[:error]).to match(/Something went wrong, please check the errors and submit again./)
      expect(response).to render_template('download_assets')
    end
  end

  describe ".block_withdrawals" do
    let!(:person1) { create(:person) }
    let!(:person2) { create(:person) }

    it "should block and redirect with flash message" do
      person1.roles << Role.find_by(name: "Admin")
      login_as(person1)

      get :block_withdrawals, params: { id: person2.id}
      expect(response).to redirect_to(admin_person_path)
      expect(flash[:success]).to match(/Successfuly blocked withdrawals for #{person2.name}/)
    end

    it "should not block if already blocked and redirect with flash message" do
      person1.roles << Role.find_by(name: "Admin")
      login_as(person1)

      person2.flags << Flag.find_by(Flag::WITHDRAWAL)
      get :block_withdrawals, params: { id: person2.id}
      expect(response).to redirect_to(admin_person_path)
      expect(flash[:error]).to match(/Not able to block the withdrawals, contact support team./)
    end
  end

  describe ".unblock_withdrawals" do
    let!(:person1) { create(:person) }
    let!(:person2) { create(:person) }

    it "should unblock and redirect with flash message" do
      person1.roles << Role.find_by(name: "Admin")
      login_as(person1)
      person2.flags << Flag.find_by(Flag::WITHDRAWAL)
      get :unblock_withdrawals, params: { id: person2.id}
      expect(response).to redirect_to(admin_person_path)
      expect(flash[:success]).to match(/Successfuly unblocked withdrawals for #{person2.name}/)
    end

    it "should not unblock if already unblocked and redirect with flash message" do
      person1.roles << Role.find_by(name: "Admin")
      login_as(person1)

      allow(person2).to receive(:unblock_withdrawal!).and_return(false)
      get :unblock_withdrawals, params: { id: person2.id}
      expect(response).to redirect_to(admin_person_path)
      expect(flash[:error]).to match(/Not able to unblock the withdrawals, contact support team./)
    end
  end
end

describe Admin::PeopleController, type: :controller do
  describe "#transactions_for" do
    let(:person) { create(:person, :with_balance, amount: 50_000) }

    before :each do
      login_as(Person.find_by(email: "super_admin@tunecore.com"))
    end

    context "when show_rollover_transactions is set" do
      it "includes transactions with target-type 'TaxableEarningsRollover'" do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
        txn = create(:person_transaction, :with_rollover_target, person: person)
        get :transactions_for, params: { id: person.id }
        expect(@controller.instance_variable_get(:@transactions)).to include(txn)
      end
    end

    context "when show_rollover_transactions is not set" do
      it "excludes transactions with target-type 'TaxableEarningsRollover'" do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(false)
        txn = create(:person_transaction, :with_rollover_target, person: person)
        get :transactions_for, params: { id: person.id }
        expect(@controller.instance_variable_get(:@transactions)).not_to include(txn)
      end
    end
  end
end
