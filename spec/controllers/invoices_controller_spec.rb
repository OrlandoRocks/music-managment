require "rails_helper"

describe InvoicesController do
  COUNTRY_URLS.each do |country_code, country_url|
    before(:each) do
      request.host = country_url
      country_website = CountryWebsite.find_by(country: country_code)
      @person = create(:person)
      @person.country_website = country_website
      login_as(@person)
    end

    describe "show" do
      it "should set up invoice variables" do
        p1 = double(Purchase)
        p2 = double(Purchase)
        p3 = double(Purchase)

        @invoice = mock_model(Invoice)
        allow(@invoice).to receive(:purchases).and_return([p1, p2, p3])
        allow(@person).to receive_message_chain(:invoices, :where, :first).and_return(@invoice)

        @store = double(Store)

        expect(Cart::Purchases::Manager).to receive(:new).with(@person, [p1, p2, p3])

        expect(Store).to receive_message_chain(:order, :first).and_return(@store)

        get :show, params: { id: @invoice.id }

        expect(assigns[:page_title]).to eq I18n.t("controllers.invoices.invoice")
        expect(assigns[:newest_store]).to eq(@store)
        expect(assigns[:purchases].size).to eq(3)
      end
    end
  end

  describe "#thanks" do
    let(:account)               { create(:account) }
    let(:publishing_composer)    { create(:publishing_composer, account: account, person: account.person) }
    let(:person)                { publishing_composer.person }
    let(:invoice_person_state)  { double(:invoice_person_state, has_distro_purchase: nil, active_composer_with_rights_app?: true) }

    before(:each) do
      request.host = person.country_website.url
      allow(controller).to receive(:current_user) { person }
      allow(InvoicePersonState).to receive(:new)  { invoice_person_state }
    end

    context "when rights app is enabled" do
      let(:invoice) { create(:invoice, :with_purchase, related: publishing_composer, person: publishing_composer.account.person) }

      before(:each) do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
      end

      it "redirects to the compositions path" do
        person.invoices << invoice

        get :thanks, params: { id: invoice.id }

        expect(assigns[:publishing_link]).to eq publishing_administration_composition_path(publishing_composer.id)
      end
    end
  end

  describe "download" do
    before(:each) do
      person = create(:person)
      login_as(person)

      request.host = person.country_website.url
      allow(controller).to receive(:current_user) { person }
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

      album = TCFactory.build_album(person: person)
      purchase = Product.add_to_cart(person, album)
      @invoice = Invoice.factory(person, purchase)
      @invoice.update(settled_at: Time.zone.now, invoice_sequence: 123)
    end

    it 'should be able to download invoice as pdf' do
      get :download, params: { id: @invoice.id, format: :pdf }

      expect(response).to be_successful
      expect(response.headers['Content-Type']).to eq('application/pdf')
      expect(response.headers['Content-Disposition']).to include("#{@invoice.invoice_number}.pdf")
    end
  end
end
