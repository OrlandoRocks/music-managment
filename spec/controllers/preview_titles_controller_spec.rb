require "rails_helper"

describe PreviewTitlesController do
  describe "#show" do
    let(:person) { create(:person) }
    let(:artist) { create(:artist, name: "Chance the Rapper") }
    let(:other_creatives) {
      [
        { role: "featuring", name: "Lil Wayne"},
        { role: "featuring", name: "2 Chainz"},
      ]
    }

    before do
      login_as(person)
    end

    it "renders a preview title for a song" do
      album = create(:album, person: person, creatives: [
        { role: "primary_artist", name: artist.name }.with_indifferent_access
      ])
      get :show, params: { song: { name: "no problem", creatives: other_creatives }, id: album.id }

      expect(response.body).to eq("No Problem (feat. Lil Wayne & 2 Chainz)")
    end

    it "renders a preview title for a new ringtone" do
      get :show, params: { ringtone: { name: "no problem", creatives: other_creatives } }

      expect(response.body).to eq("No Problem (feat. Lil Wayne & 2 Chainz)")
    end

    it "renders a preview title for a saved ringtone" do
      ringtone = create(:ringtone, person: person, creatives: [
        { role: "primary_artist", name: artist.name }.with_indifferent_access
      ])
      get :show, params: { ringtone: { name: "no problem", creatives: other_creatives }, id: ringtone.id }

      expect(response.body).to eq("No Problem (feat. Lil Wayne & 2 Chainz)")
    end

    it "renders a preview title for a new single" do
      get :show, params: { ringtone: { name: "no problem", creatives: other_creatives } }

      expect(response.body).to eq("No Problem (feat. Lil Wayne & 2 Chainz)")
    end

    it "renders a preview title for a saved single" do
      single = create(:single, person: person, creatives: [
          { role: "primary_artist", name: artist.name }.with_indifferent_access
      ])
      get :show, params: { ringtone: { name: "no problem", creatives: other_creatives }, id: single.id }

      expect(response.body).to eq("No Problem (feat. Lil Wayne & 2 Chainz)")
    end
  end
end
