# frozen_string_literal: true

require "rails_helper"

describe TaxFormRevenueStreamsController, type: :controller do
  let(:person) { create(:person) }
  let(:tax_forms) { create_list(:tax_form, 4, person: person) }
  let(:mapping_params) { { tax_form_mappings: { distribution: "primary", publishing: "secondary" } } }
  let(:mapping_form_on_success) { double(save: true, person: person) }
  let(:mapping_form_on_failure) do
    double(save: false, person: person, errors: double(full_messages: ["invalid mapping"]))
  end

  before(:each) do
    login_as(person)
  end

  describe "POST #create" do
    it "initializes and saves the taxform mapping form" do
      allow(PayoutProvider::TaxFormRevenueStreamMapperForm)
        .to receive(:new)
        .with(
          person: person,
          mappings: ActionController::Parameters.new(mapping_params[:tax_form_mappings])
        )
        .and_return(mapping_form_on_success)
      post :create, params: mapping_params
      expect(flash[:success]).to eq(custom_t("people.edit.tax_form_mapping.success_message"))
      expect(response).to redirect_to account_settings_path(tab: "payoneer_payout")
    end

    it "renders error on failure" do
      allow(PayoutProvider::TaxFormRevenueStreamMapperForm).to receive(:new).and_return(mapping_form_on_failure)
      expect(Airbrake)
        .to receive(:notify)
        .with(
          "Failed mapping Tax Forms to Revenue Sources: #{mapping_form_on_failure.errors.full_messages.join(', ')}",
          ActionController::Parameters.new(mapping_params[:tax_form_mappings].merge(person_id: person.id))
        )

      post :create, params: mapping_params
      expect(flash[:error]).to eq(custom_t("people.edit.tax_form_mapping.error_message"))
      expect(response).to redirect_to account_settings_path(tab: "payoneer_payout")
    end
  end
end
