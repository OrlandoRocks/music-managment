require "rails_helper"

describe BandPhotosController, type: :controller, skip: true do
  context "When trying to destroy someone's else band photo" do
    it "should return a 404 not found error" do
      login_as(person_with_no_band_photo)

      expect { delete :destroy, params: { id: 1 } }
        .to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  context "When trying to update someone's else band photo" do
    it "should return a 404 not found error" do
      login_as(person_with_no_band_photo)

      expect { put :update, params: { id: 1 } }
        .to raise_error(ActiveRecord::RecordNotFound)
    end
  end
end

private

def person_with_no_band_photo
  TCFactory.build_person
end
