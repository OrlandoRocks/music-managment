require "rails_helper"

describe TcSocialController, type: :controller do
  include TcSocialHelper
  describe "#index" do
    it "redirects to dashboard url" do
      person = FactoryBot.create(:person, :with_tc_social)
      album = FactoryBot.create(:album, :finalized, person: person)
      plan_status = ::Social::PlanStatus.for(person)
      login_as(person)
      get :index
      expect(response).to redirect_to(tc_social_dashboard_url)
    end
  end

  describe "#create" do
    it "creates tc_social_message cookie" do
      person = FactoryBot.create(:person, :with_tc_social)
      album  = FactoryBot.create(:album, :finalized, person: person)
      login_as(person)
      post :create, params: { message: "test message" }
      expect(cookies).to have_key(:tc_social_message)
    end

    it "properly encodes url" do
      person = FactoryBot.create(:person, :with_tc_social)
      album  = FactoryBot.create(:album, :finalized, person: person)
      url    = "http://itunes.apple.com/album/id1000000000"
      login_as(person)
      post :create, params: { message: url }
      expect(cookies[:tc_social_message]).to eq CGI.escape(url)
    end
  end
end
