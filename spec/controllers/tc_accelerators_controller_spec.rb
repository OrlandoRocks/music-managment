require 'rails_helper'

describe TcAcceleratorsController do
  let(:person) {create(:person)}

  context "#do_not_notify" do

    before :each do
      login_as person
    end

    context "given a valid person_id" do
      it "should respond with the appropriate JSON" do
        post :do_not_notify, params: { tc_accelerator: { person_id: person.id }}
        parsed_response = Oj.load(response.body)
        expect(parsed_response.fetch("person_id")).to eq(person.id)
        expect(parsed_response.fetch("do_not_notify")).to be true
        expect(parsed_response.fetch("success")).to be true
      end
    end

    context "given an invalid person_id" do
      it "should raise an error" do
        expect do
          post :do_not_notify, params: { tc_accelerator: { person_id: Person.last.id + 1 }}
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
