require "rails_helper"

describe AccountController, type: :controller do
  describe "#release_account" do
    context "when the album parameter is an emptry string" do
      it "redirects to the admin person page" do
        admin = create(:person, :admin)

        login_as(admin)
        get :release_account

        expect(subject).to redirect_to admin_person_path(admin)
      end
    end

    context "when an existing album is passed in" do
      it "redirects to the admin album page" do
        admin = create(:person, :admin)
        album = create(:album)

        login_as(admin)
        get :release_account, params: { album: album.id }

        expect(subject).to redirect_to admin_album_path(album)
      end
    end
  end
end
