require "rails_helper"

describe SpecializedRelease::AlbumsController, type: :controller do
  describe "GET #edit" do
    before(:each) do
      @person = create(:person)
      login_as(@person)
      @free_album = create(:album, :freemium_release, person: @person)
      @reg_album = create(:album, person: @person)

      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
    end

    context "user visits freemium album edit page with a non freemium release" do
      it "should redirect to the regular album edit page" do
        get :edit, params: { id: @reg_album.id }

        expect(controller).to redirect_to(edit_album_path(@reg_album))
      end
    end

    context "user visits facebook album edit page with a facbook only release" do
      context "when discovery_platforms is enabled" do 
        it "redirects to discovery_platform edit path" do
          get :edit, params: { id: @free_album.id }
         
          expect(controller).to redirect_to(edit_discovery_platforms_album_path(@free_album)) 
        end 
      end 
      
      context "when Discovery Platforms is not yet enabled" do  
        before do 
          allow(FeatureFlipper).to receive(:show_feature?).with(:discovery_platforms, an_instance_of(Person)).and_return(false)
        end 
        
        it "should give a success status" do
          allow_any_instance_of(ActionController::TestRequest).to receive(:path).and_return(edit_facebook_album_path(@free_album.id))
          get :edit, params: { id: @free_album.id }
          
          expect(response.status).to eq(200)
        end
      end
    end

    context "when user visits discovery platform edit page" do 
      context "when discovery platform enabled" do 
        it "should give a success status" do 
          allow_any_instance_of(ActionController::TestRequest).to receive(:path).and_return(edit_discovery_platforms_album_path(@free_album))
          get :edit, params: { id: @free_album.id }
            
          expect(response.status).to eq(200)
        end
      end 
    end 
  end
end