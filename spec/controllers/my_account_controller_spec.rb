require "rails_helper"

describe MyAccountController do
  let(:person) { create(:person, :with_balance, amount: 50_000) }
  let(:payout_provider) { create(:payout_provider, provider_status: "approved", person: person) }

  before :each do
    # TODO: Refactor once V4 payoneer API in place 
    allow(FeatureFlipper).to receive(:show_feature?) do |arg1, arg2|
      if arg1 == :use_payoneer_v4_api
        false
      else
        true
      end 
    end
    allow(FeatureFlipper).to receive(:show_feature?).with(:use_payoneer_v4_api, person).and_return(false)
    allow(FeatureFlipper).to receive(:show_feature?).with(:use_payoneer_v4_api).and_return(false)

    login_as person
  end

  describe "#withdraw" do
    context "user has no balance" do
      it "redirects to dashboard" do
        person_with_no_balance = create(:person)
        login_as person_with_no_balance
        get :withdraw
        expect(response.status).to eq(302)
      end
    end

    context "payoneer payout provider" do
      context "payoneer not mandated" do
        before do
          allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_mandatory, person).and_return(false)
          allow_any_instance_of(Payoneer::V2::PayoutApiClient).to receive(:send_request).and_return(OpenStruct.new(body: '{}'))
        end

        it "does not redirect to the new payout transfer path" do
          create(:payout_provider, person: person, tunecore_status: PayoutProvider::ACTIVE)
          get :withdraw
          expect(response.status).to eq(200)
        end
      end

      context "payoneer mandated" do
        before do
          allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_mandatory, person).and_return(true)
          allow(FeatureFlipper).to receive(:show_feature?).with(:use_payoneer_v4_api, person).and_return(false)
          allow(FeatureFlipper).to receive(:show_feature?).with(:use_payoneer_v4_api).and_return(false)

          allow_any_instance_of(Payoneer::V2::PayoutApiClient).to receive(:send_request).and_return(OpenStruct.new(body: '{}'))
        end

        it "redirects to payout_transfers#new for US account" do
          allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_opt_out, person).and_return(false)
          allow(FeatureFlipper).to receive(:show_feature?).with(:auto_approved_withdrawals, person.id).and_return(false)

          person.payout_providers << payout_provider
          create(:approved_payout_transfer, payout_provider: payout_provider)

          get :withdraw

          expect(response).to redirect_to(new_payout_transfer_path)
        end

        it "does not redirect when the payoneer opt out flag is enabled for a user" do
          allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_opt_out, person).and_return(true)
          person.payout_providers << payout_provider

          get :withdraw
          expect(response.status).to eq(200)
        end

        it "does not redirect for non-US account" do
          allow(FeatureFlipper).to receive(:show_feature?).with(:use_payoneer_v4_api, person).and_return(false)
          allow(FeatureFlipper).to receive(:show_feature?).with(:use_payoneer_v4_api).and_return(false)

          canadian_person = create(:person, :with_balance, amount: 50_000, country_website_id: 2, country: "CA")
          ca_payout_provider_config = PayoutProviderConfig.find_by_program_id(PayoutProviderConfig::TC_CA_PROGRAM_ID)
          canadian_payout_provider = create(:payout_provider,
                                            person: person,
                                            tunecore_status: PayoutProvider::ACTIVE,
                                            currency: "CAD",
                                            payout_provider_config: ca_payout_provider_config)
          canadian_person.payout_providers << canadian_payout_provider
          login_as canadian_person

          get :withdraw
          expect(response).not_to redirect_to(new_payout_transfer_path)
        end

        it "redirects to redesigned onboarding path when feature is enabled" do
          allow(controller).to receive(:show_payoneer_redesigned_onboarding?).and_return(true)
          get :withdraw
          expect(response).to redirect_to(payoneer_fees_path)
        end
      end
    end
  end

  describe "#withdraw_check" do
    context "user has no balance" do
      it "redirects to dashboard" do
        person_with_no_balance = create(:person)
        login_as person_with_no_balance
        post :withdraw_check, params: {
          withdrawal: {
            payment_amount_main_currency: 1000,
            payment_amount_fractional_currency: 00
          }
        }
        expect(response.status).to eq(302)
      end
    end

    context "with two factor auth" do
      let(:two_factor_auth) { create(:two_factor_auth, person: person) }
      let(:new_record_double) { double(new_record?: true) }

      before :each do
        allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_mandatory, person).and_return(false)
      end

      it "works with a valid two factor auth" do
        two_factor_auth.update(last_verified_at: 1.minute.ago)

        expect(ManualCheckTransfer).to receive(:generate).and_return(new_record_double)
        post :withdraw_check, params: {
          withdrawal: {
            payment_amount_main_currency: 1000,
            payment_amount_fractional_currency: 00
          }
        }
      end

      it "does not work with an invalid two factor auth" do
        two_factor_auth.update(last_verified_at: 100.minutes.ago)
        allow(ENV).to receive(:[]).with("TWO_FACTOR_AUTHENTICATION_ENABLED").and_return("true")
        expect(ManualCheckTransfer).not_to receive(:generate)
        post :withdraw_check, params: {
          withdrawal: {
            payment_amount_main_currency: 1000,
            payment_amount_fractional_currency: 00
          }
        }
      end
    end
  end

  describe "#list_transactions" do
    context "with no payoneer feature flag enabled" do
      before do
        allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_mandatory, person).and_return(false)
      end

      it "does not redirect to balance history page" do
        get :list_transactions
        expect(response.status).to eq(200)
      end
    end

    context "when show_rollover_transactions is set" do
      it "includes transactions with target-type 'TaxableEarningsRollover'" do
        allow(FeatureFlipper).to receive(:show_feature?).with(:show_rollover_transactions, person).and_return(true)
        txn = create(:person_transaction, :with_rollover_target, person: person)
        get :list_transactions
        expect(@controller.instance_variable_get(:@transactions)).to include(txn)
      end
    end

    context "when show_rollover_transactions is not set" do
      it "excludes transactions with target-type 'TaxableEarningsRollover'" do
        allow(FeatureFlipper).to receive(:show_feature?).with(:show_rollover_transactions, person).and_return(false)
        txn = create(:person_transaction, :with_rollover_target, person: person)
        get :list_transactions
        expect(@controller.instance_variable_get(:@transactions)).not_to include(txn)
      end
    end
  end
end

describe MyAccountController, type: :controller do
  let(:person) { create(:person, :with_balance, amount: 50_000) }
  let(:payout_provider) { create(:payout_provider, provider_status: "approved", person: person) }

  before :each do
    login_as person
    allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
    allow(FeatureFlipper).to receive(:show_feature?).with(:bi_transfer, person).and_return(false)
    payout_transfer_double = instance_double("PaypalTransfer", new_record?: false, errors: [])
    allow(PaypalTransfer).to receive(:generate).and_return(payout_transfer_double)
  end

  [:withdraw_check, :withdraw_paypal].each do |withdraw_action|
    describe withdraw_action.to_s do
      context "when tax blocking is enabled for a US user" do
        before do
          allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking, person).and_return(true)
        end

        it "summarizes earnings" do
          service_double = instance_double("TaxBlocking::PersonEarningsSummarizationService")
          expect(TaxBlocking::PersonEarningsSummarizationService)
            .to receive(:new).with(person.id, Date.current.year).and_return(service_double)
          expect(service_double).to receive(:summarize)
          get withdraw_action
        end

        it "updates tax-forms metadata" do
          service_double = instance_double("TaxBlocking::TaxformInfoUpdateService")
          expect(TaxBlocking::TaxformInfoUpdateService)
            .to receive(:new).with(person.id, Date.current.year).and_return(service_double)
          expect(service_double).to receive(:summarize)
          get withdraw_action
        end

        it "updates tax-blocked status" do
          expect(TaxBlocking::UpdatePersonTaxblockStatusService).to receive(:call).with(person)
          get withdraw_action
        end

        it "redirects to taxform submission page if tax-blocked user attempts withdrawal" do
          allow(person).to receive(:tax_blocked?).and_return(true)
          post withdraw_action, params: {
            withdrawal: {
              payment_amount_main_currency: 1000,
              payment_amount_fractional_currency: 00
            }
          }
          expect(response).to redirect_to(account_settings_path(tab: "taxpayer_id"))
        end

        it "doesn't redirect to taxform submission page if tax-unblocked user attempts withdrawal" do
          allow(person).to receive(:tax_blocked?).and_return(false)
          post withdraw_action, params: {
            withdrawal: {
              payment_amount_main_currency: 1000,
              payment_amount_fractional_currency: 00
            }
          }
          expect(response).not_to redirect_to(account_settings_path(tab: "taxpayer_id"))
        end
      end

      context "when tax blocking is disabled for a US user" do
        before do
          allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking, person).and_return(false)
        end

        it "doesn't summarize earnings" do
          expect(TaxBlocking::PersonEarningsSummarizationService).not_to receive(:new).with(person.id, Date.current.year)
          get withdraw_action
        end

        it "doesn't update tax-forms metadata" do
          expect(TaxBlocking::TaxformInfoUpdateService).not_to receive(:new).with(person.id, Date.current.year)
          get withdraw_action
        end

        it "doesn't redirect to taxform submission page if tax-blocked user attempts withdrawal" do
          allow(person).to receive(:tax_blocked?).and_return(true)
          post withdraw_action, params: {
            withdrawal: {
              payment_amount_main_currency: 1000,
              payment_amount_fractional_currency: 00
            }
          }
          expect(response).not_to redirect_to(account_settings_path(tab: "taxpayer_id"))
        end

        it "doesn't update tax-blocked status" do
          expect(TaxBlocking::UpdatePersonTaxblockStatusService).not_to receive(:call).with(person)
          get withdraw_action
        end
      end

      context "for a non-US user" do
        before do
          person.update(country: "LU")
          allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking, person).and_return(true)
        end

        it "doesn't summarize earnings" do
          expect(TaxBlocking::PersonEarningsSummarizationService).not_to receive(:new).with(person.id, Date.current.year)
          get withdraw_action
        end

        it "doesn't update tax-forms metadata" do
          expect(TaxBlocking::TaxformInfoUpdateService).not_to receive(:new).with(person.id, Date.current.year)
          get withdraw_action
        end

        it "updates tax-blocked status" do
          expect(TaxBlocking::UpdatePersonTaxblockStatusService).to receive(:call).with(person)
          get withdraw_action
        end
      end
    end
  end
end
