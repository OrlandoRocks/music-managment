require "rails_helper"

describe MissingSalepointPurchasesController do
  describe "#show" do
    it "redirects to cart" do
      person = create(:person)
      create(:album, :approved, person: person)
      login_as(person)
      get :show, params: { store: "iTunesWW" }
      expect(response).to redirect_to(cart_path)
    end
  end
end
