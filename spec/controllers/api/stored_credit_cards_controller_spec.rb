require 'rails_helper'

RSpec.describe Api::StoredCreditCardsController, type: :controller do

  context "with a valid create StoredCreditCard method" do
    it "creates a creditcard for a person" do
      person = FactoryBot.create(:person, :with_tc_social)
      request.headers["Authorization"] = "Token token=#{SessionEncryptionEngine.encrypt64(person.tc_social_token)}"
      credit_card_count = person.stored_credit_cards.count
      params = { firstname: person.name, lastname: person.name,
                 address1: "address1",
                 braintree_payment_nonce: "fake-valid-visa-nonce",
                 api_key: API_CONFIG["API_KEY"] }
      post :create, params: params
      expect(JSON.parse(response.body)["status"]).to eq("success")
      expect(JSON.parse(response.body)["stored_credit_card"]).to be_a(Hash)
      expect(JSON.parse(response.body)["stored_credit_card"]["address1"]).to eq("address1")
      expect { post :create, params: params }
      .to change { person.stored_credit_cards.count }.by(1)
    end
  end

  context "with a invalid create StoredCreditCard method" do
    it " error on create of a creditcard for a person" do
      person = people(:tunecore_customer_brand_new)
      credit_card_count = person.stored_credit_cards.count
      params = { firstname: person.name, lastname: person.name,
                 braintree_payment_nonce: "fake-valid-visa-nonce",
                 api_key: API_CONFIG["API_KEY"] }
      service = post :create, params: params
      expect(JSON.parse(response.body)["status"]).to eq("failure")
    end
  end

  context " List all StoredCreditCards of a person" do
    it "list all stored creditcards for a person" do
      person = FactoryBot.create(:person, :with_tc_social)
      request.headers["Authorization"] = "Token token=#{SessionEncryptionEngine.encrypt64(person.tc_social_token)}"
      credit_card_count = person.stored_credit_cards.count
      params = { api_key: API_CONFIG["API_KEY"] }
      service = get :index, params: params
      expect(JSON.parse(response.body)["status"]).to eq("success")
    end
  end

  context "Invalid entry for  List all StoredCreditCards of a person" do
    it "Invalid entry for list all stored creditcards for a person" do
      person = people(:tunecore_customer_brand_new)
      credit_card_count = person.stored_credit_cards.count
      params = { api_key: API_CONFIG["API_KEY"] }
      service = get :index, params: params
      expect(JSON.parse(response.body)["status"]).to eq("failure")
    end
  end

  context "With valid edit fields for stored credit cards" do
    it "Updates the stored creditcards for a person" do
      person = FactoryBot.create(:person, :with_tc_social)
      request.headers["Authorization"] = "Token token=#{SessionEncryptionEngine.encrypt64(person.tc_social_token)}"
      credit_card = create(:stored_credit_card)
      credit_card.update(person: person)
      params = { firstname: "New_First_Name", lastname: "New_Last_Name",
                 api_key: API_CONFIG["API_KEY"], id: credit_card.id,
                 cvv: 256, }
      expect_any_instance_of(Braintree::PaymentMethodGateway).to receive(:update).and_return(true)
      btvt = BraintreeVaultTransaction.create(person: person, related: credit_card, response_code: "success", ip_address: "0.0.0.0")
      expect(StoredCreditCard).to receive(:handle_braintree_result).and_return([true, btvt])
      put :update, params: params
      expect(JSON.parse(response.body)["status"]).to eq("success")
    end
  end

  context "With invalid edit fields for stored credit cards" do
    it "throws failure" do
      person = FactoryBot.create(:person, :with_tc_social)
      request.headers["Authorization"] = "Token token=#{SessionEncryptionEngine.encrypt64(person.tc_social_token)}"
      credit_card = create(:stored_credit_card)
      params = { firstname: "New_First_Name", lastname: "New_Last_Name",
                 api_key: API_CONFIG["API_KEY"], id: credit_card.id,
                 cvv: 256, }
      put :update, params: params
      expect(JSON.parse(response.body)["status"]).to eq("failure")
      expect(JSON.parse(response.body)["message"]).to eq("Card not found")
    end
  end
  describe "#braintree_token" do
    let!(:person) { FactoryBot.create(:person, :with_tc_social) }
    before(:each) do
      request.headers["Authorization"] = "Token token=#{SessionEncryptionEngine.encrypt64(person.tc_social_token)}"
      params = { api_key: API_CONFIG["API_KEY"] }

      get :braintree_token, params: params
    end
    it "returns a successful response" do
      expect(JSON.parse(response.body)["status"]).to eq("success")
    end
    it "returns a token" do
      expect(JSON.parse(response.body)["data"].present?).to be(true)
    end
  end
end
