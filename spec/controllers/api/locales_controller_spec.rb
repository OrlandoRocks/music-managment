require "rails_helper"

describe Api::LocalesController, type: :controller do
  it "Get list of all countries" do
    get :available_locales, params: { api_key: API_CONFIG["API_KEY"] }
    expect(JSON.parse(response.body)["status"]).to eq("success")
    expect(JSON.parse(response.body)).to be_a(Hash)
  end

  it "Failure list of all countries" do
    get :available_locales
    expect(response.status).to eq(401)
  end
end
