require "rails_helper"

describe Api::V2::ContentReview::TrackMonetizationsController, type: :controller do
  let(:person) { create(:person) }
  let(:store_id) { Store::FBTRACKS_STORE_ID }

  before(:each) do
    allow(subject).to receive(:authenticate_user!).and_return(true)
    allow(subject).to receive(:authorize_content_review_users!).and_return(true)
    allow(subject).to receive(:current_user) { person }
  end

  describe "#block" do
    context "when blocking a track successfully" do
      it "should block the track monetization" do
        track = create(:track_monetization)

        params = {
          album_id: track.song.album_id,
          song_ids: [track.song.id],
          store_id: store_id
        }

        expect(TrackMonetizationBlocker)
          .to receive(:create)
          .with(song_id: track.song.id, store_id: store_id)
          .and_call_original

        put :block, params: params

        expect(track.reload.state).to eq "blocked"
      end

      it "should return the track monetizations state and store name" do
        song = create(:song, :with_s3_asset)
        create(:track_monetization, song: song)

        params = {
          album_id: song.album_id,
          song_ids: [song.id],
          store_id: store_id
        }

        put :block, params: params

        res = response_json.first["track_monetizations"].first
        expect(res["state"]).to eq "blocked"
        expect(res["store"]).to eq "Facebook Track Monetizations"
      end

      it "should create note after blocking monetization" do
        song = create(:song, :with_s3_asset)
        create(:track_monetization, song: song)

        params = {
          album_id: song.album_id,
          song_ids: [song.id],
          store_id: store_id
        }

        expect {put :block, params: params}.to change(Note, :count).by(1)
      end
    end

    context "when unable to block a track" do
      it "returns an error message" do
        track = create(:track_monetization)

        params = {
          album_id: track.song.album_id,
          song_ids: [track.song.id],
          store_id: nil
        }

        put :block, params: params

        expect(response_json["base"]).to include "An error occured when trying to block tracks"
      end

      it "should not create note when monetization is not blocked" do
        track = create(:track_monetization)

        params = {
          album_id: track.song.album_id,
          song_ids: [track.song.id],
          store_id: nil
        }

        expect {put :block, params: params}.to change(Note, :count).by(0)
      end
    end

    it "should create a track monetization blocker" do
      song = create(:song, :with_s3_asset)
      create(:track_monetization, song: song)

      params = {
        album_id: song.album_id,
        song_ids: [song.id],
        store_id: store_id
      }

      put :block, params: params

      res = response_json.first["track_monetization_blockers"].first
      expect(res["store"]).to eq "Facebook Track Monetizations"
    end
  end
end
