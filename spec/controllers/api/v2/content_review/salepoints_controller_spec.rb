require "rails_helper"

describe Api::V2::ContentReview::SalepointsController, type: :controller do
  let(:person) { create(:person) }
  before(:each) do
    allow(subject).to receive(:authenticate_user!).and_return(true)
    allow(subject).to receive(:authorize_content_review_users!).and_return(true)
    allow(subject).to receive(:current_user) { person }
  end

  describe "#block_salepoint" do
    context "when the salepoint is successfully blocked" do
      it "should block the salepoint and create a note" do
        distribution = create(:distribution, :with_salepoint, state: "new")
        salepoint = distribution.salepoints.first
        album = salepoint.salepointable

        params = {
          album_id: album.id,
          salepoint_id: salepoint.id
        }
        expect {
          put :block_salepoint, params: params
        }.to change {
          distribution.reload.state
        }.from("new").to("blocked")

        note = album.notes.last
        expect(note.subject).to eq "Blocked Salepoint"
      end
    end

    context "when the salepoint is not blocked" do
      it "should return an error" do
        salepoint = create(:salepoint)
        album = salepoint.salepointable

        params = {
          album_id: album.id,
          salepoint_id: salepoint.id
        }
        expect {
          put :block_salepoint, params: params
        }.not_to change {
          album.notes.count
        }
        expect(response_json["base"]).to eq "Unable to block Spotify"
      end
    end
  end

  describe "#unblock_salepoint" do
    context "when the salepoint is successfully blocked" do
      it "should unblock the salepoint and create a note" do
        distribution = create(:distribution, :with_salepoint)
        distribution.update(state: "delivered")
        blocked_distribution = create(:distribution)
        blocked_distribution.update(state: "blocked")
        salepoint = distribution.salepoints.first
        salepoint.distributions << blocked_distribution
        album = salepoint.salepointable

        params = {
          album_id: album.id,
          salepoint_id: salepoint.id
        }
        expect {
          put :unblock_salepoint, params: params
        }.to change {
          blocked_distribution.reload.state
        }.from("blocked").to("new")

        note = album.notes.last
        expect(note.subject).to eq "Unblocked Salepoint"
      end
    end

    context "when the salepoint is not unblocked" do
      it "should return an error" do
        salepoint = create(:salepoint)
        album = salepoint.salepointable

        params = {
          album_id: album.id,
          salepoint_id: salepoint.id
        }
        expect {
          put :unblock_salepoint, params: params
        }.not_to change {
          album.notes.count
        }
        expect(response_json["base"]).to eq "Unable to unblock Spotify"
      end
    end
  end
end
