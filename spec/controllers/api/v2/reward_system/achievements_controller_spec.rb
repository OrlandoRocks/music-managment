require "rails_helper"

describe Api::V2::RewardSystem::AchievementsController, type: :controller do
  let(:person) { create(:person) }
  before(:each) do
    expect(subject).to receive(:authenticate_user!).and_return(true)
    expect(subject).to receive(:authorize_user!).and_return(true)
    allow(subject).to receive(:current_user) { person }
  end

  describe "#index" do
    let!(:achievements) { create_list(:achievement, 2) }

    it "should return serialized all achievements" do
      get :index
      expect(response.status).to eq(200)

      expected_achievements = Achievement.all.order(:id).map do |achievement|
        Api::V2::RewardSystem::AchievementSerializer.new(achievement)
      end
      expected_achievements = JSON.parse(expected_achievements.to_json)
      returned_achievements = JSON.parse(response.body)['achievements']

      expect(returned_achievements).to eq(expected_achievements)
    end
  end

  describe "#show" do
    let!(:achievement) { create(:achievement) }

    it "should return one achievement" do
      get :show, params: { id: achievement.id }
      expect(response.status).to eq(200)

      expected_achievement = JSON.parse(Api::V2::RewardSystem::AchievementSerializer.new(achievement).to_json)
      returned_achievement = JSON.parse(response.body)

      expect(returned_achievement).to eq(expected_achievement)
    end
  end

  describe "#create" do
    let!(:achievement) { build(:achievement) }

    it "should create an achievement" do
      expect do
        post :create, params: { achievement: JSON.parse(Api::V2::RewardSystem::AchievementSerializer.new(achievement).to_json) }
      end.to change { Achievement.count }.by(1)
      expect(response.status).to eq(200)

      keys = [:name, :description, :link, :points, :category, :is_active].map(&:to_s)

      returned_achievement = JSON.parse(response.body)
      keys.each { |key| expect(returned_achievement[key]).to eq(achievement.attributes[key]) }

      retrieved_achievement = Achievement.find(returned_achievement['id'])
      keys.each { |key| expect(retrieved_achievement.attributes[key]).to eq(achievement.attributes[key]) }

      expect(returned_achievement['tiers']).to be_nil
    end

    it "should throw an error if required fields are missing" do
      achievement_data = JSON.parse(Api::V2::RewardSystem::AchievementSerializer.new(achievement).to_json)
      achievement_data['name'] = nil
      post :create, params: { achievement: achievement_data }
      expect(response.status).to be(422)
    end
  end

  describe "#update" do
    let!(:achievement) { create(:achievement) }

    it "should update an achievement" do
      achievement_data = JSON.parse(Api::V2::RewardSystem::AchievementSerializer.new(achievement).to_json)
      achievement_data['name'] = 'v2'
      put :update, params: { id: achievement.id, achievement: achievement_data }
      expect(response.status).to be(200)

      achievement.reload
      keys = [:name, :description, :link, :points, :category, :is_active].map(&:to_s)
      keys.each { |key| expect(achievement.attributes[key]).to eq(achievement_data[key]) }

      returned_achievement = JSON.parse(response.body)
      keys.each { |key| expect(returned_achievement[key]).to eq(achievement_data[key]) }

      expect(returned_achievement['tiers']).to be_nil
    end

    it "should throw an error if required fields are missing" do
      achievement_data = JSON.parse(Api::V2::RewardSystem::AchievementSerializer.new(achievement).to_json)
      achievement_data['name'] = nil
      put :update, params: { id: achievement.id, achievement: achievement_data }
      expect(response.status).to eq(422)
    end
  end
end
