require "rails_helper"

describe Api::V2::RewardSystem::BaseController, type: :controller do
  let(:person) { create(:person) }
  before(:each) do
    expect(subject).to receive(:authenticate_user!).and_return(true)
    allow(subject).to receive(:current_user) { person }
  end

  describe "#authorize_user!" do
    controller(Api::V2::RewardSystem::BaseController) do
      before_action :authenticate_user!
      before_action :authorize_user!

      def index
        render json: {test: 'OK'}
      end
    end

    context "user is not reward admin" do
      it "should be unauthorized" do
        allow(person).to receive(:is_reward_admin?).and_return(false)
        get :index

        response_body = JSON.parse(response.body)
        expect(response.status).to eq(401)
        expect(response_body).to eq({'status' => 'unauthorized'})
      end
    end

    context "user is a reward admin" do
      it "should return success" do
        allow(person).to receive(:is_reward_admin?).and_return(true)
        get :index

        response_body = JSON.parse(response.body)

        expect(response.status).to eq(200)
        expect(response_body).to eq({'test' => 'OK'})
      end
    end
  end
end
