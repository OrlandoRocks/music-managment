require "rails_helper"

describe Api::V2::RewardSystem::CertificationsController, type: :controller do
  let(:person) { create(:person) }
  before(:each) do
    expect(subject).to receive(:authenticate_user!).and_return(true)
    expect(subject).to receive(:authorize_user!).and_return(true)
    allow(subject).to receive(:current_user) { person }
  end

  describe "#index" do
    let!(:certifications) { create_list(:certification, 2) }

    it "should return serialized all rewards" do
      get :index
      expect(response.status).to eq(200)

      expected_certifications = Certification.all.order(:id).map do |certification|
        Api::V2::RewardSystem::CertificationSerializer.new(certification)
      end
      expected_certifications = JSON.parse(expected_certifications.to_json)
      returned_certifications = JSON.parse(response.body)['certifications']

      expect(returned_certifications).to eq(expected_certifications)
    end
  end

  describe "#show" do
    let!(:certification) { create(:certification)}

    it "should return a single certification" do
      get :show, params: {id: certification.id}
      expect(response.status).to eq(200)

      expected_certification = JSON.parse(Api::V2::RewardSystem::CertificationSerializer.new(certification).to_json)
      returned_certification = JSON.parse(response.body)

      expect(returned_certification).to eq(expected_certification)
    end
  end

  describe "#create" do
    let!(:certification) { build(:certification)}
    it "should create a certification" do
      expect {
        post :create, params: {certification: JSON.parse(Api::V2::RewardSystem::CertificationSerializer.new(certification).to_json)}
      }.to change{Certification.count}.by(1)
      expect(response.status).to eq(200)

      returned_certification = JSON.parse(response.body)
      expect(returned_certification['name']).to eq(certification.name)
      expect(returned_certification['points']).to eq(certification.points)
      expect(returned_certification['tiers']).to be_nil
    end

    it "should throw error when input is invalid" do
      certification_data = JSON.parse(Api::V2::RewardSystem::CertificationSerializer.new(certification).to_json)
      certification_data['name'] = nil
      post :create, params: {certification: certification_data}
      expect(response.status).to eq(422)
    end
  end

  describe "#update" do
    let!(:certification) { create(:certification) }

    it "should update a certification" do
      certification_data = JSON.parse(Api::V2::RewardSystem::CertificationSerializer.new(certification, except: [:tiers]).to_json)
      certification_data['name'] = 'v2'
      put :update, params: { id: certification.id, certification: certification_data }
      expect(response.status).to eq(200)

      certification.reload
      expect(certification.name).to eq('v2')

      returned_certification = JSON.parse(response.body)
      expect(returned_certification).to eq(certification_data)
      expect(returned_certification['tiers']).to be_nil
    end

    it "should throw an error when input is invalid" do
      certification_data = JSON.parse(Api::V2::RewardSystem::CertificationSerializer.new(certification).to_json)
      certification_data['name'] = nil
      put :update, params: { id: certification.id, certification: certification_data }
      expect(response.status).to eq(422)
    end
  end
end
