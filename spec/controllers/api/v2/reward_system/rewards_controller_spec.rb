require "rails_helper"

describe Api::V2::RewardSystem::RewardsController, type: :controller do
  let(:person) { create(:person) }
  before(:each) do
    expect(subject).to receive(:authenticate_user!).and_return(true)
    allow(subject).to receive(:current_user) { person }
  end

  before(:each, authorized: true) do
    expect(subject).to receive(:authorize_user!).and_return(true)
  end

  describe "#person_info" do
    context "when a valid user" do
      it "should return serialized response" do
        get :person_info
        expect(response.status).to eq(200)

        serialized_response = Api::V2::RewardSystem::RewardPersonSerializer.new(person)
        expect(response.body).to eq(serialized_response.to_json)
      end
    end
  end

  describe "#index", authorized: true do
    let!(:rewards) { create_list(:reward, 2) }

    it "should return serialized all rewards" do
      get :index
      expect(response.status).to eq(200)

      expected_rewards = ActiveModel::ArraySerializer.new(
        Reward.all,
        each_serializer: Api::V2::RewardSystem::RewardSerializer,
        scope: {include_associations: true}
      )

      expected_rewards = JSON.parse(expected_rewards.to_json)
      returned_rewards = JSON.parse(response.body)['rewards']

      expect(returned_rewards).to eq(expected_rewards)
    end
  end

  describe "#create", authorized: true do
    context "missing params" do
      let(:params) do
        {
          description: "abc"
        }
      end

      it "should return missing params error" do
        post :create, params: { reward: params }

        response_body = JSON.parse(response.body)
        expect(response.status).to eq(400)
        expect(response_body["error_key"]).to eq("params_missing")
        expect(response_body["error"]).to include("param is missing or the value is empty")
      end
    end

    it "should create reward" do
      reward_params = {
        name: "reward1",
        content_type: "video",
        category: "freebie",
        points: 25,
        is_active: true
      }.stringify_keys

      post :create, params: { reward: reward_params }
      response_body = JSON.parse(response.body)
      expected_reward_attrs = response_body.slice(*reward_params.keys)

      expect(response.status).to eq(200)
      expect(response_body).to include(expected_reward_attrs)
    end
  end

  describe "#show", authorized: true do
    it "should return error when invalid reward" do
      get :show, params: { id: 0 }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(404)
      expect(response_body).to eq({"error" => "Invalid Reward"})
    end

    it "should return individual reward" do
      reward = create(:reward)
      get :show, params: { id: reward.id }

      serialized_response = Api::V2::RewardSystem::RewardSerializer.new(reward, scope: {include_associations: true})
      expect(response.body).to eq(serialized_response.to_json)
    end
  end

  describe "#update", authorized: true do
    let(:reward) { create(:reward) }

    it "should return error when invalid reward" do
      put :update, params: { id: 0 }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(404)
      expect(response_body).to eq({"error" => "Invalid Reward"})
    end

    context "invalid reward params" do
      it "should return the validation error" do
        params = {
          name: ""
        }

        put :update, params: { id: reward.id, reward: params }

        error_json = {
          "name"=> [
            {
              "error"=>"blank"
            },
          ]
        }

        response_body = JSON.parse(response.body)
        expect(response.status).to eq(422)
        expect(response_body).to eq(error_json)
      end
    end

    it "should update the reward" do
      params = {
        description: "abc"
      }

      put :update, params: { id: reward.id, reward: params }
      expect(reward.reload.description).to eq(params[:description])
    end
  end

  describe "#redeem" do
    let(:reward) { create(:reward) }
    let(:person) { create(:person) }

    before do
      allow(person).to receive(:is_reward_admin?).and_return(true)
    end

    it "should return error when invalid reward" do
      post :redeem, params: { id: 0 }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(404)
      expect(response_body).to eq({"error" => "Invalid Reward"})
    end

    it "should return error when no person params" do
      post :redeem, params: { id: reward.id }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(400)
      expect(response_body).to include({"error_key" => "params_missing"})
    end

    it "should return error when invalid person" do
      post :redeem, params: { id: reward.id, person_id: 0 }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(404)
      expect(response_body).to eq({"error" => "Invalid Person"})
    end

    context "user is admin" do
      let(:other_person) { create(:person) }

      it "should accept any valid person id" do
        allow_any_instance_of(Reward).to receive(:redeem!)

        post :redeem, params: { id: reward.id, person_id: other_person.id }

        response_body = JSON.parse(response.body)
        expect(response.status).to eq(200)
      end
    end

    context "user is non admin" do
      let(:other_person) { create(:person) }

      before do
        allow(person).to receive(:is_reward_admin?).and_return(false)
      end

      it "should return error when other person id is passed" do
        post :redeem, params: { id: reward.id, person_id: other_person.id }

        response_body = JSON.parse(response.body)
        expect(response.status).to eq(401)
        expect(response_body).to eq({"error" => "unauthorized"})
      end

      it "should accept their own id in person" do
        allow_any_instance_of(Reward).to receive(:redeem!)

        post :redeem, params: { id: reward.id, person_id: person.id }

        response_body = JSON.parse(response.body)
        expect(response.status).to eq(200)
      end
    end

    it "should return error from redeem! call" do
      allow_any_instance_of(Reward).to receive(:redeem!).and_raise("Boom")

      post :redeem, params: { id: reward.id, person_id: person.id }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(422)
      expect(response_body).to eq({"error" => "Boom"})
    end

    it "should call redeem on reward" do
      expect_any_instance_of(Reward).to receive(:redeem!)

      post :redeem, params: { id: reward.id, person_id: person.id }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(200)
    end
  end
end
