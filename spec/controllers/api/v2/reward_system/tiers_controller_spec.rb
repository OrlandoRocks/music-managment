require "rails_helper"

describe Api::V2::RewardSystem::TiersController, type: :controller do
  let(:person) { create(:person) }
  before(:each) do
    expect(subject).to receive(:authenticate_user!).and_return(true)
    expect(subject).to receive(:authorize_user!).and_return(true)
    allow(subject).to receive(:current_user) { person }
  end

  describe "#index" do
    let!(:tiers) { create_list(:tier, 2) }

    it "should return serialized all tiers" do
      get :index
      expect(response.status).to eq(200)

      expected_tiers = Tier.all.order(:hierarchy).map do |tier|
        Api::V2::RewardSystem::TierSerializer.new(tier, scope: {tier_associations: true})
      end
      expected_tiers = JSON.parse(expected_tiers.to_json)
      returned_tiers = JSON.parse(response.body)['tiers']

      expect(returned_tiers).to eq(expected_tiers)
    end

    it "should sort tiers by hierarchy" do
      tiers
      get :index

      returned_tiers = JSON.parse(response.body)['tiers']

      expect(returned_tiers.first['hierarchy']).to be < returned_tiers.last['hierarchy']
    end
  end

  describe "#get" do
    it "should return error when invalid tier" do
      get :show, params: { id: 0 }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(404)
      expect(response_body).to eq({"error" => "Invalid Tier"})
    end

    it "should return individual tier" do
      tier = create(:tier)
      get :show, params: { id: tier.id }

      expected_response = JSON.parse(tier.to_json)["tier"].slice(
        "id", "name", "description", "badge_url", "hierarchy", "is_active", "is_sub_tier", "parent_id", "slug"
      )

      response_body = JSON.parse(response.body)
      expect(response_body).to include(expected_response)
      expect(response_body.keys).to include("achievements", "certifications", "rewards")
    end
  end

  describe "#create" do
    context "missing params" do
      let(:params) do
        {
          description: "abc"
        }
      end

      it "should return missing params error" do
        post :create, params: { tier: params }

        response_body = JSON.parse(response.body)
        expect(response.status).to eq(400)
        expect(response_body["error_key"]).to eq("params_missing")
        expect(response_body["error"]).to include("param is missing or the value is empty")
      end
    end

    context "invalid tier params" do
      let(:params) do
        {
          name: "tier1",
          is_active: true,
          is_sub_tier: true
        }
      end

      it "should return the validation error" do
        post :create, params: { tier: params }

        error_json = {
          "parent"=> [
            {"error"=>"blank"},
            {"error"=>"Invalid parent tier id. Parent not assigned."}
          ],
          "hierarchy"=>[
            {"error"=>"Invalid parent tier id. Hierarchy not assigned."}
          ]
        }

        response_body = JSON.parse(response.body)
        expect(response.status).to eq(422)
        expect(response_body).to eq(error_json)
      end
    end
  end

  describe "#update" do
    let(:tier) { create(:tier) }

    it "should return error when invalid tier" do
      put :update, params: { id: 0 }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(404)
      expect(response_body).to eq({"error" => "Invalid Tier"})
    end

    context "missing params" do
      it "should return missing params error" do
        put :update, params: { id: tier.id, tier: {} }

        response_body = JSON.parse(response.body)
        expect(response.status).to eq(400)
        expect(response_body["error_key"]).to eq("params_missing")
        expect(response_body["error"]).to include("param is missing or the value is empty")
      end
    end

    context "invalid tier params" do
      it "should return the validation error" do
        existing_tier = create(:tier)

        params = {
          name: existing_tier.name
        }

        put :update, params: { id: tier.id, tier: params }

        error_json = {
          "name"=> [
            {
              "error"=>"taken",
              "value"=>existing_tier.name
            },
          ]
        }

        response_body = JSON.parse(response.body)
        expect(response.status).to eq(422)
        expect(response_body).to include(error_json)
      end
    end

    it "should update the tier" do
      params = {
        description: "abc"
      }

      put :update, params: { id: tier.id, tier: params }
      returned_tier = JSON.parse(response.body)

      expect(tier.reload.description).to eq(params[:description])
      expect(returned_tier['rewards']).to be_nil
      expect(returned_tier['certifications']).to be_nil
      expect(returned_tier['achievements']).to be_nil
    end
  end

  describe "#add_achievement" do
    let(:tier) { create(:tier) }
    let(:achievement) { create(:achievement) }

    it "should return error when invalid tier" do
      post :add_achievement, params: { id: 0 }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(404)
      expect(response_body).to eq({"error" => "Invalid Tier"})
    end

    it "should return error when no achievement params" do
      post :add_achievement, params: { id: tier.id }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(400)
      expect(response_body).to include({"error_key" => "params_missing"})
    end

    it "should return error when invalid achievement" do
      post :add_achievement, params: { id: tier.id, achievement_id: 0 }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(404)
      expect(response_body).to eq({"error" => "Invalid Achievement"})
    end

    context "achievement already associated with tier" do
      it "should respond with success" do
        create(:tier_achievement, tier: tier, achievement: achievement)
        expect(tier.achievement_ids).to include(achievement.id)

        post :add_achievement, params: { id: tier.id, achievement_id: achievement.id }

        response_body = JSON.parse(response.body)
        expect(response.status).to eq(200)
        expect(response_body["id"]).to eq(tier.id)
      end
    end

    it "should add the achievement to tier" do
      expect(tier.achievement_ids).to_not include(achievement.id)
      post :add_achievement, params: { id: tier.id, achievement_id: achievement.id }

      expect(tier.reload.achievement_ids).to include(achievement.id)

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(200)
      expect(response_body["id"]).to eq(tier.id)
    end
  end

  describe "#remove_achievement" do
    let(:tier) { create(:tier) }
    let(:achievement) { create(:achievement) }

    it "should return error when invalid tier" do
      post :remove_achievement, params: { id: 0 }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(404)
      expect(response_body).to eq({"error" => "Invalid Tier"})
    end

    it "should return error when no achievement params" do
      post :remove_achievement, params: { id: tier.id }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(400)
      expect(response_body).to include({"error_key" => "params_missing"})
    end

    it "should return error when invalid achievement" do
      post :remove_achievement, params: { id: tier.id, achievement_id: 0 }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(404)
      expect(response_body).to eq({"error" => "Invalid Achievement"})
    end

    context "achievement not associated with tier" do
      it "should respond with success" do
        post :remove_achievement, params: { id: tier.id, achievement_id: achievement.id }

        response_body = JSON.parse(response.body)
        expect(response.status).to eq(200)
        expect(response_body["id"]).to eq(tier.id)
      end
    end

    it "should remove the achievement from tier" do
      create(:tier_achievement, tier: tier, achievement: achievement)

      expect(tier.achievement_ids).to include(achievement.id)
      post :remove_achievement, params: { id: tier.id, achievement_id: achievement.id }

      expect(tier.reload.achievement_ids).to_not include(achievement.id)

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(200)
      expect(response_body["id"]).to eq(tier.id)
    end
  end

  describe "#add_certification" do
    let(:tier) { create(:tier) }
    let(:certification) { create(:certification) }

    it "should return error when invalid tier" do
      post :add_certification, params: { id: 0 }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(404)
      expect(response_body).to eq({"error" => "Invalid Tier"})
    end

    it "should return error when no certification params" do
      post :add_certification, params: { id: tier.id }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(400)
      expect(response_body).to include({"error_key" => "params_missing"})
    end

    it "should return error when invalid certification" do
      post :add_certification, params: { id: tier.id, certification_id: 0 }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(404)
      expect(response_body).to eq({"error" => "Invalid Certification"})
    end

    context "Certification already associated with tier" do
      it "should respond with success" do
        create(:tier_certification, tier: tier, certification: certification)
        expect(tier.certification_ids).to include(certification.id)

        post :add_certification, params: { id: tier.id, certification_id: certification.id }

        response_body = JSON.parse(response.body)
        expect(response.status).to eq(200)
        expect(response_body["id"]).to eq(tier.id)
      end
    end

    it "should add the certification to tier" do
      expect(tier.certification_ids).to_not include(certification.id)
      post :add_certification, params: { id: tier.id, certification_id: certification.id }

      expect(tier.reload.certification_ids).to include(certification.id)

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(200)
      expect(response_body["id"]).to eq(tier.id)
    end
  end

  describe "#remove_certification" do
    let(:tier) { create(:tier) }
    let(:certification) { create(:certification) }

    it "should return error when invalid tier" do
      post :remove_certification, params: { id: 0 }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(404)
      expect(response_body).to eq({"error" => "Invalid Tier"})
    end

    it "should return error when no certification params" do
      post :remove_certification, params: { id: tier.id }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(400)
      expect(response_body).to include({"error_key" => "params_missing"})
    end

    it "should return error when invalid certification" do
      post :remove_certification, params: { id: tier.id, certification_id: 0 }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(404)
      expect(response_body).to eq({"error" => "Invalid Certification"})
    end

    context "certification not associated with tier" do
      it "should respond with success" do
        post :remove_certification, params: { id: tier.id, certification_id: certification.id }

        response_body = JSON.parse(response.body)
        expect(response.status).to eq(200)
        expect(response_body["id"]).to eq(tier.id)
      end
    end

    it "should remove the certification from tier" do
      create(:tier_certification, tier: tier, certification: certification)

      expect(tier.certification_ids).to include(certification.id)
      post :remove_certification, params: { id: tier.id, certification_id: certification.id }

      expect(tier.reload.certification_ids).to_not include(certification.id)

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(200)
      expect(response_body["id"]).to eq(tier.id)
    end
  end

  describe "#remove_reward" do
    let(:tier) { create(:tier) }
    let(:reward) { create(:reward) }

    it "should return error when invalid tier" do
      post :remove_reward, params: { id: 0 }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(404)
      expect(response_body).to eq({"error" => "Invalid Tier"})
    end

    it "should return error when invalid reward" do
      post :remove_reward, params: { id: tier.id, reward_id: 0 }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(404)
      expect(response_body).to eq({"error" => "Invalid Reward"})
    end

    context "reward not associated with tier" do
      it "should respond with success" do
        post :remove_reward, params: { id: tier.id, reward_id: reward.id }

        response_body = JSON.parse(response.body)
        expect(response.status).to eq(200)
        expect(response_body["id"]).to eq(tier.id)
      end
    end

    it "should remove the reward from tier" do
      create(:tier_reward, tier: tier, reward: reward)

      expect(tier.reward_ids).to include(reward.id)
      post :remove_reward, params: { id: tier.id, reward_id: reward.id }

      expect(tier.reload.reward_ids).to_not include(reward.id)

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(200)
      expect(response_body["id"]).to eq(tier.id)
    end
  end

  describe "#add_reward" do
    let(:tier) { create(:tier) }
    let(:reward) { create(:reward) }

    it "should return error when invalid tier" do
      post :add_reward, params: { id: 0 }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(404)
      expect(response_body).to eq({"error" => "Invalid Tier"})
    end

    it "should return error when invalid reward" do
      post :add_reward, params: { id: tier.id, reward_id: 0 }

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(404)
      expect(response_body).to eq({"error" => "Invalid Reward"})
    end

    context "reward already associated with tier" do
      it "should respond with success" do
        create(:tier_reward, tier: tier, reward: reward)
        expect(tier.reward_ids).to include(reward.id)

        post :add_reward, params: { id: tier.id, reward_id: reward.id }

        response_body = JSON.parse(response.body)
        expect(response.status).to eq(200)
        expect(response_body["id"]).to eq(tier.id)
      end
    end

    it "should add the reward to tier" do
      expect(tier.reward_ids).to_not include(reward.id)
      post :add_reward, params: { id: tier.id, reward_id: reward.id }

      expect(tier.reload.reward_ids).to include(reward.id)

      response_body = JSON.parse(response.body)
      expect(response.status).to eq(200)
      expect(response_body["id"]).to eq(tier.id)
    end
  end
end
