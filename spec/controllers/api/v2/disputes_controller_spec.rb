require "rails_helper"

describe Api::V2::DisputesController, type: :controller do
  let(:admin) { create(:person, :with_refund_role) }

  before(:each) do
    allow(subject).to receive(:authenticate_user!).and_return(true)
    allow(subject).to receive(:authorize_refund_users!).and_return(true)
    allow(subject).to receive(:current_user) { admin }
    allow(FeatureFlipper).to receive(:show_feature?).and_call_original
    allow(FeatureFlipper).to receive(:show_feature?).with(:chargebacks, admin).and_return(true)
  end

  describe "GET #index" do
    let(:braintree_dispute) { create(:dispute_status, :braintree) }
    let(:paypal_dispute) { create(:dispute_status, :paypal) }
    context "without_params" do
      it "respond with all disputes" do
        all_disputes =
          Disputes::TransactionListingService
          .new.disputes

        get :index
        subject = JSON.parse(response.body)

        expect(subject).to eq(
          {
            status: "success",
            data: {
              disputes: all_disputes.as_json(root: false),
              total_pages: all_disputes.total_pages,
              total_entries: all_disputes.total_entries
            }
          }.with_indifferent_access
        )
      end
    end

    context "with_params" do
      it "respond with filtered disputes" do
        params = { source_type: "braintree" }

        braintree_disputes =
          Disputes::TransactionListingService
          .new(params)
          .disputes

        get :index, params: params
        subject = JSON.parse(response.body)

        expect(subject).to eq(
          {
            status: "success",
            data: {
              disputes: braintree_disputes.as_json(root: false),
              total_pages: braintree_disputes.total_pages,
              total_entries: braintree_disputes.total_entries
            }
          }.with_indifferent_access
        )
      end
    end
  end

  describe "GET #transaction_types" do
    it "respond with transaction types and with possible statuses" do
      braintree_statuses = DisputeStatus.where(source_type: "braintree")
      paypal_statuses = DisputeStatus.where(source_type: "paypal")
      payu_statuses = DisputeStatus.where(source_type: "payu")

      get :transaction_types
      subject = JSON.parse(response.body)

      expect(subject).to eq(
        {
          status: "success",
          data:
          [
            {
              source_type: DisputeStatus::SOURCE_TYPE_NAMES[DisputeStatus::BRAINTREE],
              statuses: braintree_statuses.map do |obj|
                {
                  id: obj.id,
                  status: obj.status.humanize
                }
              end
            },
            {
              source_type: DisputeStatus::SOURCE_TYPE_NAMES[DisputeStatus::PAYPAL],
              statuses: paypal_statuses.map do |obj|
                {
                  id: obj.id,
                  status: obj.status.humanize
                }
              end
            },
            {
              source_type: DisputeStatus::SOURCE_TYPE_NAMES[DisputeStatus::PAYU],
              statuses: payu_statuses.map do |obj|
                {
                  id: obj.id,
                  status: obj.status.humanize
                }
              end
            }
          ]
        }.with_indifferent_access
      )
    end
  end

  describe "Accept dispute" do
    let(:dispute) { create(:dispute) }
    it "Accept dispute and return data" do
      allow_any_instance_of(Braintree::DisputeService).to receive(:accept).and_return(
        {success: true, errors: []}
      )
      post :accept_dispute, params: {id: dispute.id}
      dispute.reload
      subject = JSON.parse(response.body)
      expect(subject).to eq(
        {
          status: "success",
          data: {
            success: true,
            errors: [],
            dispute: {
              id: dispute.id,
              last_action: dispute.action_type,
              updated_at: dispute.updated_at.to_s
            }
          }
        }.with_indifferent_access
      )
    end
  end

  describe "Finalise dispute" do
    let(:dispute) { create(:dispute) }
    it "Finalise dispute and return data" do
      allow_any_instance_of(Braintree::DisputeService).to receive(:finalize).and_return(
        {success: true, errors: []}
      )
      post :finalise_dispute, params: {id: dispute.id}
      dispute.reload
      subject = JSON.parse(response.body)
      expect(subject).to eq(
        {
          status: "success",
          data: {
            success: true,
            errors: [],
            dispute: {
              id: dispute.id,
              last_action: 'dispute_completed',
              updated_at: dispute.updated_at.to_s
            }
          }
        }.with_indifferent_access
      )
    end
  end

  describe "Submit Evidence" do
    let(:dispute) { create(:dispute) }
    it "Submit text evidence" do
      allow_any_instance_of(Braintree::DisputeService).to receive(:attach_text_evidence).and_return(
        {success: true, errors: []}
      )
      post :submit_evidence, params: {id: dispute.id, text_evidence: 'sample text'}
      dispute.reload
      subject = JSON.parse(response.body)
      expect(subject).to eq(
        {
          status: "success",
          data: {
            success: true,
            text_evidence:
            {
              success: true,
              errors: []
            },
            file_evidence: {},
            dispute: {
              id: dispute.id,
              last_action: 'dispute_initiated',
              updated_at: dispute.updated_at.to_s
            }
          }
        }.with_indifferent_access
      )
    end
  end
end
