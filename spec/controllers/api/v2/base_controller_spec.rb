require "rails_helper"

describe Api::V2::BaseController, type: :controller do
  let(:password) { 'Password123$' }
  let(:person) { FactoryBot.create(:person, password: password, password_confirmation: password) }
  let(:crt_specialist) { FactoryBot.create(:person, :with_crt_specialist_role, password: password, password_confirmation: password) }

  describe "#authenticate_user!" do
    controller(Api::V2::BaseController) do
      before_action :authenticate_user!

      def index
        render json: {test: 'OK'}
      end
    end

    before do
      controller.request.env["HTTP_AUTHORIZATION"] = "Bearer #{token}"
      get :index
    end

    shared_examples_for "authentication_unauthorized" do
      it "should return 401" do
        expect(response.status).to eq(401)
        expect(JSON.parse(response.body)).to eq({'status' => 'unauthorized'})
      end
    end

    describe "When Auth headers are supplied with a JWT generated for a user" do
      let(:token) { Api::V2::BaseController.new.send(:generate_jwt, person) }

      it "should return 200" do
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)).to eq({'test' => 'OK'})
      end
    end

    describe "When Auth headers are supplied with invalid JWT" do
      let(:token) { "invalid token" }
      it_behaves_like "authentication_unauthorized"
    end

    describe "When Auth headers are supplied with expired JWT" do
      let(:expired_time) { Time.now.to_i - 1000 }
      let(:token) { JWT.encode({id: person.id, exp: expired_time}, ENV["API_V1_SECRET_KEY"], 'HS256') }
      it_behaves_like "authentication_unauthorized"
    end
  end

  describe "#authorize_content_review_users!" do
    controller(Api::V2::BaseController) do
      before_action :authenticate_user!
      before_action :authorize_content_review_users!

      def index
        render json: {test: 'OK'}
      end
    end

    before do
      controller.request.env["HTTP_AUTHORIZATION"] = "Bearer #{token}"
      get :index
    end

    context "As a user with no content review role" do
      let(:token) { Api::V2::BaseController.new.send(:generate_jwt, person) }
      it "should be unauthorized" do
        expect(response.status).to eq(401)
      end
    end

    context "As a user with in-correct credentials" do
      let(:token) { Api::V2::BaseController.new.send(:generate_jwt, crt_specialist) }
      it "should return an unauthorized status" do
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)).to eq({'test' => 'OK'})
      end
    end
  end

  describe "#reformat_errors" do
    let(:errored_update_attributes) { Api::V2::ContentReview::AlbumsController::ERRORED_UPDATE_ATTRIBUTES }

    context "when the error is an eligible album error attribute" do
      it "returns the album error validation" do
        album = create(:album, :with_petri_bundle, :with_uploaded_songs, number_of_songs: 2, legal_review_state: 'APPROVED')
        album.update(name: '')

        formatted_errors = Api::V2::BaseController
          .new
          .send(:reformat_errors, album.errors.messages, errored_update_attributes)

        expect(formatted_errors[:name]).to eq "can't be blank"
      end
    end

    context "when the error is not an eligible album error attribute" do
      it "adds the validation to the base key" do
        album = create(:album, :with_petri_bundle, :with_uploaded_songs, number_of_songs: 2, legal_review_state: 'APPROVED')
        album.songs << create(:song)
        album.save

        formatted_errors = Api::V2::BaseController
          .new
          .send(:reformat_errors, album.errors.messages, errored_update_attributes)

        expect(formatted_errors[:base]).to eq "Album has a missing track."
      end
    end

    context "when the error is on the album base object" do
      it "adds the validation to the base key" do
        album = create(:album, :with_petri_bundle, :with_uploaded_songs, number_of_songs: 2, legal_review_state: 'APPROVED')
        album.errors.add(:base, "Album error")
        allow(Album).to receive(:find_by) { album }

        formatted_errors = Api::V2::BaseController
          .new
          .send(:reformat_errors, album.errors.messages, errored_update_attributes)

        expect(formatted_errors[:base]).to eq "Album error"
      end
    end
  end
end
