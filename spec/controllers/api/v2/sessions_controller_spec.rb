require "rails_helper"

describe Api::V2::SessionsController, type: :controller do
  describe "#create" do
    let(:password) { 'Password123$' }
    let(:person) { FactoryBot.create(:person, password: password, password_confirmation: password) }

    context "As a user with the correct credentials" do
      it "should return a JWT token in the json response" do
        post :create, params: { email: person.email, password: password }

        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)).to have_key("token")
      end
    end

    context "As a user with in-correct credentials" do
      it "should return an unauthorized status" do
        post :create, params: { email: person.email, password: "wrongpass" }

        expect(response.status).to eq(401)
      end
    end
  end
end
