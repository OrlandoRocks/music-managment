require "rails_helper"

describe Api::V2::FraudPrevention::CopyrightClaimsController, type: :controller do
  let(:person) { create(:person) }
  before(:each) do
    allow(subject).to receive(:authenticate_user!).and_return(true)
    allow(subject).to receive(:authorize_content_review_users!).and_return(true)
    allow(subject).to receive(:current_user) { person }
  end
  let(:album)          { create(:album, :with_uploaded_song_distribution_option) }
  let(:store)          { album.stores.last }
  let(:admin)          { create(:person, :admin) }

  describe "#index" do
    let!(:copyright_claims) { create_list(:copyright_claim, 5, asset: album, admin_id: admin.id) }
    it "should list all active copyright claims" do
      get :index
      expect(response.status).to eq(200)
    end
  end

  describe "#create" do
    it "should create an copyright_claim" do
      params = {
        copyright_claim: {
          asset: album.upc.number,
          asset_type: "UPC",
          internal_claim_id: "test897",
          store_id: 1,
          fraud_type: "Streaming Abuse",
          compute_hold_amount: true,
          note: "test note"
        }
      }
      post :create, params: params
      expect(response.status).to eq(201)
    end

    it "should throw an error if asset is not preesnt" do
      params = {
        copyright_claim: {
          internal_claim_id: "test897",
          store_id: 1,
          fraud_type: "Streaming Abuse",
          hold_amount: 0.9,
          note: "test note"
        }
      }
      post :create, params: params
      expect(response.status).to eq(404)
    end
  end
  describe "#update" do
    let!(:copyright_claim) { create(:copyright_claim, asset: album, admin_id: admin.id) }

    it "should create an copyright_claim" do
      params = {
        id: copyright_claim.id,
        copyright_claim: {
          asset: album.upc.number, asset_type: "UPC", internal_claim_id: "test897", store_id: 1, fraud_type: "Streaming Abuse", compute_hold_amount: true, note: "test note"
        }
      }
      put :update, params: params
      expect(response.status).to eq(200)
    end

    it "should throw an error if asset is not present" do
      params = { id: copyright_claim.id, copyright_claim: { hold_amount: 10.0, note: "test note" } }
      put :update, params: params
      expect(response.status).to eq(404)
    end
  end
end
