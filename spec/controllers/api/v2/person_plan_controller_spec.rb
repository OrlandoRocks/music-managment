require "rails_helper"

describe Api::V2::PersonPlanController, type: :controller do
  let(:person) { create(:person) }

  before(:each) do
    allow(subject).to receive(:authenticate_user!).and_return(true)
  end

  describe "#person_plan" do
    context "person with plan" do
      let!(:person_plan) { create(:person_plan, person: person) }
      it "returns person_plan" do
        params = {
          id: person.id
        }

        get :show, params: params
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)).to include(
                                               {
                                                 plan_id: person_plan.plan_id,
                                                 name: person_plan.name,
                                                 created_at: person_plan.created_at.as_json,
                                                 person: {
                                                   id: person.id,
                                                   name: person.name,
                                                   email: person.email,
                                                   status: person.status
                                                 }
                                               }.with_indifferent_access
                                             )
      end
    end

    context "person without any plan" do
      it "returns plan not found" do
        params = {
          id: person.id
        }

        get :show, params: params
        expect(response.status).to eq(404)
        p JSON.parse(response.body)
        expect(JSON.parse(response.body)).to include(
                                               {
                                                 error: 'No plan found'
                                               }.with_indifferent_access
                                             )
      end
    end
  end
end
