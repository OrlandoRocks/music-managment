require "rails_helper"
describe Api::SubscriptionsController do
  let(:person) { create(:person, :with_social_subscription_status_and_event_and_purchase) }

  describe "#index" do
    context "with valid oauth token" do
      it "renders the serialized subscription with a top-level key of 'success'" do
        controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(person.tc_social_token))
        get :index, params: { subscription_type: "Social" }
        expect(response.status).to eq(200)
        response_body = JSON.parse(response.body)
        expect(response_body["status"]).to eq("success")
        expect(response_body["data"]["subscription"]).to_not eq nil
        expect(response_body["data"]["user"]).to_not eq nil
      end
    end

    context "with an invalid oauth token" do
      it "renders an error response with a 200 status and a code of 2000" do
        get :index, params: { subscription_type: "Social" }
        expect(response.status).to eq(200)
        expect(response.body).to eq({status: "failure", code: 2000}.to_json)
      end
    end
  end

  describe "#destroy" do
    it "renders the serialized canceled subscription" do
      controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(person.tc_social_token))
      delete :destroy, params: { subscription_type: "Social" }
      expect(response.status).to eq(200)
      response_body = JSON.parse(response.body)
      expect(response_body["status"]).to eq("success")
      expect(response_body["data"]["subscription"]).to_not eq nil
      expect(response_body["data"]["user"]).to_not eq nil
    end

    it "records a user_note if one is provided" do
      controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(person.tc_social_token))
      delete :destroy, params: { subscription_type: "Social", user_note: "12345" }
      response_body = JSON.parse(response.body)
      expect(response.status).to eq(200)
    end
  end
end
