require "rails_helper"

describe Api::Frontstage::ArtistsController do
  it "return 401 Unauthorized if no oauth token" do
    get :show
    expect(response.status).to eq 401
  end
end
