require "rails_helper"

describe Api::Frontstage::TwoFactorAuth::ResendsController do

  let(:person) { create(:person, :with_two_factor_auth) }

  before(:each) do
    allow(ENV).to receive(:[]).and_return(:default)
    allow(ENV).to receive(:[]).with("JWT_ENCRYPTION_KEY").and_return("123357890")
    allow(ENV).to receive(:[]).with("JWT_ALGORITHM").and_return("HS256")
    allow(ENV).to receive(:[]).with("TFA_LOGIN_TIMEOUT_IN_SEC").and_return("120")
    allow(FeatureFlipper).to receive(:show_feature?).with(:verify_2fa, person).and_return(true)
    allow_any_instance_of(TwoFactorAuth::ApiClient).to receive(:log).and_return(true)
  end

  describe "#create" do
    it "redirects to login if token missing" do
      post :create, xhr: true
      expect(response).to redirect_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
    end

    it "redirects to login if token invalid" do
      post :create, params: { token: "asdfasdf" }, xhr: true
      expect(response).to redirect_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
    end

    it "redirects to login if token expired" do
      Timecop.freeze
      token = TwoFactorAuth::TokenService.issue({ email: person.email })
      Timecop.travel(Time.now + 3.minutes)

      post :create, params: { token: token }, xhr: true

      expect(response).to redirect_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
      Timecop.return
    end

    it "calls 2fa client to resend token" do
      fake_api_client = double(request_authorization: true)
      expect(TwoFactorAuth::ApiClient).to receive(:new).with(person).and_return(fake_api_client)
      expect(fake_api_client).to receive(:request_authorization)
      token = TwoFactorAuth::TokenService.issue({ email: person.email })

      post :create, params: { token: token }, xhr: true

      expect(response.status).to eq 200
    end

    context "when the ajax feature flag is enabled" do
      context "when an auth event has happened less than a minute ago" do
        it "does not send a auth code" do
          Timecop.freeze
          resend_event = create(:two_factor_auth_event, two_factor_auth: person.two_factor_auth, action: "frontstage_code_resend", created_at: 20.seconds.ago)

          allow(FeatureFlipper).to receive(:show_feature?).with(:two_factor_auth_ajax, person).and_return(true)
          count = TwoFactorAuthEvent.count

          token = TwoFactorAuth::TokenService.issue({ email: person.email })

          post :create, params: { token: token }

          expect(response.status).to eq 200
          expect(TwoFactorAuthEvent.count).to eq(count)
          Timecop.return
        end
      end

      context "when an auth event has happened over a minute ago" do
        it "does send an auth code" do
          Timecop.freeze
          resend_event = create(:two_factor_auth_event, two_factor_auth: person.two_factor_auth, action: "frontstage_code_resend", created_at: 2.days.ago)

          allow(FeatureFlipper).to receive(:show_feature?).with(:two_factor_auth_ajax, person).and_return(true)
          count = TwoFactorAuthEvent.count

          fake_api_client = double(request_authorization: true)
          expect(TwoFactorAuth::ApiClient).to receive(:new).with(person).and_return(fake_api_client)
          expect(fake_api_client).to receive(:request_authorization)
          token = TwoFactorAuth::TokenService.issue({ email: person.email })

          post :create, params: { token: token }, xhr: true

          expect(response.status).to eq 200
          expect(TwoFactorAuthEvent.count).to eq(count + 1)
          Timecop.return
        end
      end
    end
  end
end
