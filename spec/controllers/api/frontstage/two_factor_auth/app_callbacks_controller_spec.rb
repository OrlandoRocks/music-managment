require "rails_helper"

describe Api::Frontstage::TwoFactorAuth::AppCallbacksController do
  let(:person) { create(:person, :with_two_factor_auth) }

  before(:each) do
    allow(ENV).to receive(:[]).and_return(ENV)
    allow(ENV).to receive(:[]).with("JWT_ENCRYPTION_KEY").and_return("123357890")
    allow(ENV).to receive(:[]).with("JWT_ALGORITHM").and_return("HS256")
  end

  describe "#show" do
    it "redirects to login if token missing" do
      get :show, xhr: true
      expect(response).to redirect_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
    end

    it "redirects to login if token invalid" do
      get :show, params: { token: "asdfasdf" }, xhr: true
      expect(response).to redirect_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
    end

    it "redirects to login if token expired" do
      Timecop.freeze
      token = TwoFactorAuth::TokenService.issue({ email: person.email })
      Timecop.travel(Time.now + 3.minutes)
      get :show, params: { token: token }, xhr: true
      expect(response).to redirect_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
      Timecop.return
    end

    it "returns successful status if authenticated" do
      person.two_factor_auth.update(last_verified_at: 1.minute.ago)
      token = TwoFactorAuth::TokenService.issue({ email: person.email })

      get :show, params: { token: token }, xhr: true

      expect(response.status).to eq 200
      expect(JSON.parse(response.body)["status"]).to eq "successful"
    end

    it "returns unsuccessful status if outsite auth window" do
      person.two_factor_auth.update(last_verified_at: 10.minute.ago)
      token = TwoFactorAuth::TokenService.issue({ email: person.email })

      get :show, params: { token: token }, xhr: true

      expect(response.status).to eq 200
      expect(JSON.parse(response.body)["status"]).to eq "unsuccessful"
    end
  end
end

