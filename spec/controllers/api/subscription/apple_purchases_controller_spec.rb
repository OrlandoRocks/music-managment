require "rails_helper"

describe Api::Subscription::ApplePurchasesController do
  let(:params) {
    {
      "purchase" => {"receipt_data" => "abcdefg12345"}
    }
  }

  let(:purchase_date) {Date.today.to_s}
  let(:expires_date)  {1.year.from_now.to_date.to_s}
  let(:person)        { FactoryBot.create(:person, :with_tc_social) }

  let(:purchase_params) {
    {
      :purchase_date=>purchase_date,
      :expires_date=>expires_date,
      :receipt_data=> 'abcdefg12345',
      :product_name=> "Social",
      :product_type=> "annually",
      :person_id=>person.id
    }
  }
  describe "#create" do

    context "unauthenticated" do
      it "renders failure if not authorized" do
        post :create, params: { purchase: [] }
        expect(JSON.parse(response.body)["status"]).to eq "failure"
      end
    end

    context "authenticated" do
      before(:each) do
        controller.request.env["HTTP_AUTHORIZATION"] =
          ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(person.tc_social_token))
      end

      it "renders failure if error returned from Apple" do
        fake_apple_api(
          body: {
            "status" => "2100",
          },
          method_call: :make_post
        )

        post :create, params: params
        expect(JSON.parse(response.body)["status"]). to eq "failure"
        expect(JSON.parse(response.body)["messages"]). to eq "2100: error status from Apple"
      end

      it "renders failure if finalize is false" do
         handler = double(finalize: false, errors: "Unable to finalize purchase")
        expect(::Subscription::Apple::PurchaseRequest).to receive(:new).with(purchase_params).and_return(handler)
        fake_apple_api(
          body: {
            "status" => 0,
            "receipt" => {
              "in_app" => [
                {
                  "purchase_date" => purchase_date,
                  "expires_date" => expires_date
                }
              ]
            },
            "latest_receipt" => "abcdefg12345"
          },
          method_call: :make_post
        )

        post :create, params: params
        expect(JSON.parse(response.body)["status"]). to eq "failure"
        expect(JSON.parse(response.body)["messages"]). to eq "Unable to finalize purchase"
      end

      it "renders success if finalize is true" do
        handler = double(finalize: true, plan: "pro", plan_expires_at: expires_date, gtm_invoice_data: nil)
        expect(::Subscription::Apple::PurchaseRequest).to receive(:new).with(purchase_params).and_return(handler)

        fake_apple_api(
          body: {
            "status" => 0,
            "receipt" => {
              "in_app" => [
                {
                  "purchase_date" => purchase_date,
                  "expires_date" => expires_date
                }
              ]
            },
            "latest_receipt" => "abcdefg12345"
          },
          method_call: :make_post
        )

        post :create, params: params

        expect(response.body).to eq Api::Subscription::PurchaseHandlerSerializer.new(handler, root: false).to_json
      end
    end
  end
end
