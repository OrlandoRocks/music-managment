require "rails_helper"

describe Api::Subscription::InAppPurchasesController do

  let(:purchase_date) {Date.today.to_s}
  let(:expires_date)  {1.year.from_now.to_date.to_s}
  let(:person)        { FactoryBot.create(:person, :with_tc_social) }
  
  describe "#create" do
    describe "ApplePurchase" do
      let(:purchase_params) {
        {
          :purchase_date=>purchase_date,
          :expires_date=>expires_date,
          :receipt_data=> 'abcdefg12345',
          :product_name=> "Social",
          :product_type=> "annually",
          :person_id=>person.id
        }
      }
      let(:params) {
        {
          platform: 'ios',
          purchase: {"receipt_data" => "abcdefg12345"}
        }
      }

      context "unauthenticated" do
        it "renders failure if not authorized" do
          post :create, params: { purchase: [] }
          expect(JSON.parse(response.body)["status"]).to eq "failure"
        end
      end

      context "authenticated" do
        before(:each) do
          controller.request.env["HTTP_AUTHORIZATION"] =
            ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(person.tc_social_token))
        end

        it "renders failure if error returned from Apple" do
          fake_apple_api(
            body: {
              "status" => "2100",
            },
            method_call: :make_post
          )

          post :create, params: params
          expect(JSON.parse(response.body)["status"]). to eq "failure"
          expect(JSON.parse(response.body)["messages"]). to eq ["2100: error status from Apple"]
        end

        it "renders failure if finalize is false" do
           handler = double(finalize: false, errors: "Unable to finalize purchase")
          expect(::Subscription::Apple::PurchaseRequest).to receive(:new).with(purchase_params).and_return(handler)
          fake_apple_api(
            body: {
              "status" => 0,
              "receipt" => {
                "in_app" => [
                  {
                    "purchase_date" => purchase_date,
                    "expires_date" => expires_date
                  }
                ]
              },
              "latest_receipt" => "abcdefg12345"
            },
            method_call: :make_post
          )

          post :create, params: params
          expect(JSON.parse(response.body)["status"]). to eq "failure"
          expect(JSON.parse(response.body)["messages"]). to eq "Unable to finalize purchase"
        end

        it "renders success if finalize is true" do
          handler = double(finalize: true, plan: "pro", plan_expires_at: expires_date, gtm_invoice_data: nil, purchase_name: 'Social')
          expect(::Subscription::Apple::PurchaseRequest).to receive(:new).with(purchase_params).and_return(handler)

          fake_apple_api(
            body: {
              "status" => 0,
              "receipt" => {
                "in_app" => [
                  {
                    "purchase_date" => purchase_date,
                    "expires_date" => expires_date
                  }
                ]
              },
              "latest_receipt" => "abcdefg12345"
            },
            method_call: :make_post
          )

          post :create, params: params

          expect(response.body).to eq Api::Subscription::PurchaseHandlerSerializer.new(handler, root: false).to_json
        end
      end
    end
  end

  describe "#create" do
    describe "AndroidPurchase" do
      let(:purchase_params) {
        {
          :purchase_date=>purchase_date,
          :expires_date=>expires_date,
          :product_name=> "Social",
          :product_type=> "annually",
          :person_id=>person.id
        }
      }
      let(:params) {
        {
          platform: 'android',
          purchase_token: 'njjpnooplbfcnhddjngmcfhe.AO-J1OwzPBAcnF8dG9efGa1lJ3UMpGpFZ05EWlfPlrfkzJFf1C1kDcuDu84CLoTu47C4CdgQy34tJ46JKC5fO-gVXkAEPp2YTU1YUTz1EiCvAM6LdstpH4YtQN6AzYymrezxnn4-3DuTHhx_D9KYNd4TcGo9J8UQkw',
          product_id: 'com.tunecore.social.pro.monthly'
        }
      }

      context "unauthenticated" do
        it "renders failure if not authorized" do
          post :create, params: { purchase: [] }
          expect(JSON.parse(response.body)["status"]).to eq "failure"
        end
      end

      context "authenticated" do
        before(:each) do
          controller.request.env["HTTP_AUTHORIZATION"] =
            ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(person.tc_social_token))
        end
        it "renders success if finalize is true" do
          purchase_response = {
            purchase_date: Date.today,
            expires_date:  Date.today + 30.days,
            purchase_token: params[:purchase_token],
            product_id: params[:product_id]
          }
          allow_any_instance_of(Subscription::Android::PurchaseVerifier).to receive(:purchase_verifier).and_return(purchase_response)
          post :create, params: params
          expect(JSON.parse(response.body)["status"]).to eq "success"
        end
      end
    end
  end

  describe "#purchase token update" do

    let(:params) { {purchase_token: "assdASDsdSD", product_id: "com.tunecore.social.pro.monthly"} }
    let(:person)        { FactoryBot.create(:person, :with_tc_social) }

    before do

      @subscription_product = SubscriptionProduct.active_by_product_type_for(person, "Social", "monthly")

      @subscription = SubscriptionPurchase.create!({
        subscription_product_id: @subscription_product.id,
        payment_channel: "Android",
        person: person,
        effective_date: Date.today.prev_month ,
        termination_date: Date.today.to_s
      })

      @payment_channel_receipt = PaymentChannelReceipt.create!({
        person_id: person.id,
        subscription_purchase_id: @subscription.id,
        receipt_data: nil
      })
    end

    context "update receipt data to latest subscription purchase's payment_channel_receipt" do
      it 'updates receipt_data' do
        payment_channel_receipt = SubscriptionPurchase.where(person_id: person.id, payment_channel: "Android")
                                                      .last.payment_channel_receipt
        receipt_data = "#{params[:purchase_token]} #{params[:product_id]}"
        payment_channel_receipt.update(receipt_data: receipt_data)
        expect(payment_channel_receipt.receipt_data).to eq("assdASDsdSD com.tunecore.social.pro.monthly")
      end
    end
  end

  describe "#update_subscription_status" do

    let(:params) { 
      {
        message: 
        { data: "ewogICJ2ZXJzaW9uIjoiMS4wIiwKICAicGFja2FnZU5hbWUiOiJjb20uc29tZS50aGluZyIsCiAgImV2ZW50VGltZU1pbGxpcyI6IjE1MDMzNDk1NjYxNjgiLAogICJzdWJzY3JpcHRpb25Ob3RpZmljYXRpb24iOgogIHsKICAgICJ2ZXJzaW9uIjoiMS4wIiwKICAgICJub3RpZmljYXRpb25UeXBlIjo0LAogICAgInB1cmNoYXNlVG9rZW4iOiJQVVJDSEFTRV9UT0tFTiIsCiAgICAic3Vic2NyaXB0aW9uSWQiOiJjb20udHVuZWNvcmUuc29jaWFsLnByby5tb250aGx5IgogIH0KfQo="} 
      } 
    }
    let(:invalid_params)  {
      {
        message: { data: "ivalid_data"}
      }
    }
    let(:person) { FactoryBot.create(:person, :with_tc_social) }

    before do

      @subscription_product = SubscriptionProduct.active_by_product_type_for(person, "Social", "monthly")

      @subscription = SubscriptionPurchase.create!({
        subscription_product_id: @subscription_product.id,
        payment_channel: "Android",
        person: person,
        effective_date: Date.today.prev_month ,
        termination_date: Date.today.to_s
      })

      PersonSubscriptionStatus.create!({
        person: person,
        effective_date: Date.today.prev_month,
        termination_date: Date.today.to_s,
        subscription_type: @subscription_product.product_name,
        grace_period_length: 0
      })

      @payment_channel_receipt = PaymentChannelReceipt.create!({
        person_id: person.id,
        subscription_purchase_id: @subscription.id,
        receipt_data: "PURCHASE_TOKEN com.tunecore.social.pro.monthly"
      })
    end

    context "with valid params" do
      it 'update subscription status of android purchase' do
        post :update_subscription_status, params: params
        expect(JSON.parse(response.body)["status"]). to eq "success"
     end
    end
    context "with invalid params" do
      it 'it returns failure response' do
        post :update_subscription_status, params: invalid_params
        expect(JSON.parse(response.body)["status"]). to eq "failure"
      end
    end
  end
end
