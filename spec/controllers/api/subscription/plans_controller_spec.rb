require "rails_helper"
describe Api::Subscription::PlansController do
  describe "#index" do
    context "with valid oauth token" do
      it "renders the serialized plans with a top-level key of 'success'" do
        person = FactoryBot.create(:person, :with_tc_social)
        controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(person.tc_social_token))
        get :index
        expect(response.status).to eq(200)
        response_body = JSON.parse(response.body)
        expect(response_body["status"]).to eq("success")
        expect(response_body["data"]["plans"]).to_not eq nil
      end
    end

    context "with an invalid oauth token" do
      it "renders an error response with a 200 status and a code of 2000" do
        get :index
        expect(response.status).to eq(200)
        expect(response.body).to eq({status: "failure", code: 2000}.to_json)
      end
    end
  end
end
