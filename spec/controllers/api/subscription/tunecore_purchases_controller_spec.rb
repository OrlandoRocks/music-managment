require "rails_helper"

describe Api::Subscription::TunecorePurchasesController do
  describe "#create" do

    context "unauthenticated" do
      it "renders failure if not authorized" do
        post :create, params: { purchase: [] }
        expect(JSON.parse(response.body)["status"]).to eq "failure"
      end
    end

    context "authenticated" do
      let(:person) { FactoryBot.create(:person, :with_tc_social) }

      before(:each) do
        controller.request.env["HTTP_AUTHORIZATION"] =
          ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(person.tc_social_token))
      end

      it "renders failure if finalize is false" do
        params = { "payment_method" => "credit_card", "request_ip"=>"0.0.0.0", "person_id" => person.id }
        handler = double(finalize: false, errors: "Unable to finalize purchase")
        expect(::Subscription::Tunecore::PurchaseRequest).to receive(:new).with(params).and_return(handler)

        post :create, params: { purchase: params }

        expect(JSON.parse(response.body)["status"]). to eq "failure"
        expect(JSON.parse(response.body)["messages"]). to eq "Unable to finalize purchase"
      end

      it "renders success if finalize is true" do
        params = { "payment_method" => "credit_card", "request_ip"=>"0.0.0.0", "person_id" => person.id }
        handler = double(finalize: true, payment_method: "credit_card", plan: "pro", plan_expires_at: Date.today, gtm_invoice_data: 'an instance of Gtm::Page')
        expect(::Subscription::Tunecore::PurchaseRequest).to receive(:new).with(params).and_return(handler)

        post :create, params: { purchase: params }

        expect(response.body).to eq Api::Subscription::PurchaseHandlerSerializer.new(handler, root: false).to_json
      end
    end
  end
end
