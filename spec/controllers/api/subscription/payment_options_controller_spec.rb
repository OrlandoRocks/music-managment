require "rails_helper"

describe Api::Subscription::PaymentOptionsController do
  describe "#index" do
    context "with valid oauth token" do
      context "with both paypal and credit cards" do
        it "renders the correct response" do
          person = FactoryBot.create(:person, :with_tc_social, :with_stored_paypal_account, :with_stored_credit_card)
          card = person.stored_credit_cards.first
          controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(person.tc_social_token))
          get :index
          expect(response.status).to eq(200)
          response_body = JSON.parse(response.body)
          expect(response_body["status"]).to eq("success")
          expect(response_body["data"]).to eq({"payment_options" => [
            {"paypal" => {"email" => person.email}},
            {"credit_cards" => [
              {
                "type" => card.cc_type,
                "expiration_month" => card.expiration_month,
                "expiration_year" => card.expiration_year,
                "last_four_digits" => card.last_four
              }
            ]}
          ]})
        end
      end

      context "without paypal" do
        it "renders the correct response" do
          person = FactoryBot.create(:person, :with_tc_social, :with_stored_credit_card)
            card = person.stored_credit_cards.first
            controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(person.tc_social_token))
            get :index
            expect(response.status).to eq(200)
            response_body = JSON.parse(response.body)
            expect(response_body["status"]).to eq("success")
            expect(response_body["data"]).to eq({"payment_options" => [
              {"paypal"=>{"email"=>nil}},
              {"credit_cards" => [
                {
                  "type" => card.cc_type,
                  "expiration_month" => card.expiration_month,
                  "expiration_year" => card.expiration_year,
                  "last_four_digits" => card.last_four
                }
              ]}
            ]})
        end
      end

      context "without credit cards" do
        it "renders the correct response" do
          person = FactoryBot.create(:person, :with_tc_social, :with_stored_paypal_account)
          controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(person.tc_social_token))
          get :index
          expect(response.status).to eq(200)
          response_body = JSON.parse(response.body)
          expect(response_body["status"]).to eq("success")
          expect(response_body["data"]).to eq({"payment_options" => [
            {"paypal" => {"email" => person.email}},
            {"credit_cards"=>[]}
          ]})
        end
      end
    end

    context "with an invalid oauth token" do
      it "renders an error response with a 200 status and a code of 2000" do
        get :index
        expect(response.status).to eq(200)
        expect(response.body).to eq({status: "failure", code: 2000}.to_json)
      end
    end
  end
end
