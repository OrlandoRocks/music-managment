require "rails_helper"

describe Api::SocialLoginController, type: :controller do  

  context "with invalid create_person using apple signup" do
    it "responds with a failure message" do
      post :create, params: {
          accepted_terms_and_conditions: "true",
          email: " ",
          name: "Test User",
          social_profile: "apple",
          access_token: "access_token",
          api_key: API_CONFIG["API_KEY"]
        }
      expect(JSON.parse(response.body)["status"]).to eq("failure")
    end
  end

  context "with invalid create_person using facebook login" do
    it "responds with a failure message" do
      post :create, params: {
          accepted_terms_and_conditions: "true",
          email: "test@tunecore.com",
          name: "Test User",
          social_profile: "facebook",
          access_token: "invalid_access_token",
          api_key: API_CONFIG["API_KEY"]
        }
      expect(JSON.parse(response.body)["message"]).to eq(["api.tc.message.invalid_access_token"])
      expect(JSON.parse(response.body)["status"]).to eq("failure")
    end
  end

  context "when missing country parameter" do
    it "fails with missing country message" do
      post :create, params: {
          accepted_terms_and_conditions: "true",
          email: "test@tunecore.com",
          name: "Test User",
          social_profile: "apple",
          access_token: "access_token",
          api_key: API_CONFIG["API_KEY"]
      }
      person = Person.find_by(email:'test@tunecore.com')
      external_service_id = ExternalService.find_by(name: 'apple').id
      external_service_person = person&.external_services_people&.where(external_service_id: external_service_id)&.last
      expect(external_service_person).to be_nil
      expect(person).to be_nil

      parsed_response = JSON.parse(response.body)

      expect(parsed_response["status"]).to eq("failure")
      expect(parsed_response["message"].count).to eq(1)
      expect(parsed_response["message"].first).to eq("Country can't be blank")
    end
  end

  context "with valid create_person using apple signup" do
    it "responds with a success message" do
      post :create, params: {
          accepted_terms_and_conditions: "true",
          email: "test@tunecore.com",
          name: "Test User",
          social_profile: "apple",
          access_token: "access_token",
          country: 1,
          api_key: API_CONFIG["API_KEY"]
        }
      person = Person.find_by(email:'test@tunecore.com')
      external_service_id = ExternalService.find_by(name: 'apple').id
      external_service_person = person.external_services_people.where(external_service_id: external_service_id).last
      expect(external_service_person).not_to be_nil
      expect(person.password).not_to be_nil
      expect(JSON.parse(response.body)["status"]).to eq("success")
    end
  end

  context "without phone_number create_person for INDIA user" do
    it "responds with a failure message" do
      post :create, params: {
        accepted_terms_and_conditions: "true",
        email: "test@tunecore.com",
        name: "Test User",
        social_profile: "apple",
        access_token: "access_token",
        country: "101",
        api_key: API_CONFIG["API_KEY"]
      }
      expect(JSON.parse(response.body)["status"]).to eq("failure")
    end
  end

  context "with phone_number create_person for INDIA user" do
    it "responds with a success message" do
      post :create, params: {
        accepted_terms_and_conditions: "true",
        email: "test@tunecore.com",
        name: "Test User",
        social_profile: "apple",
        access_token: "access_token",
        country: "101",
        phone_number: "+912345678909",
        api_key: API_CONFIG["API_KEY"]
      }
      expect(JSON.parse(response.body)["status"]).to eq("success")
    end
  end

  context "social login updating user data" do
    let(:person) {
      create(
        :person,
        name: "old name",
        country: "101",
        phone_number: "+912345678909"
      )
    }
    it "does not update country" do
      post :create,
           params: {
             accepted_terms_and_conditions: "true",
             email: person.email,
             name: "new name",
             social_profile: "apple",
             access_token: "access_token",
             country: "1",
             phone_number: "+915555555555",
             api_key: API_CONFIG["API_KEY"]
           }
      person.reload
      expect(JSON.parse(response.body)["status"]).to eq("success")
      expect(JSON.parse(response.body)["message"]).to eq(["api.tc.message.person_updated"])
      expect(person.name).to eq("new name")
      expect(person.country).to eq("101")
      expect(person.phone_number).to eq("+915555555555")
    end
  end
end
