require "rails_helper"

RSpec.describe Api::TranslationServiceController, type: :controller do
  it "returns 200 OK" do
    allow_any_instance_of(Faraday::Connection).to receive(:post).and_return(:ok)
    post :phrase_webhook
    expect(response.status).to eq(200)
  end

  it "notifies airbrake in case of failures" do
    allow_any_instance_of(Faraday::Connection).to receive(:post) { raise(Faraday::Error) }
    expect(Airbrake).to receive(:notify)
    post :phrase_webhook
  end
end
