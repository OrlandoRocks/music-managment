require "rails_helper"

describe Api::SipController, type: :controller do

  let(:api_key) { "CGUAE01HGL0IOVHNCKAX12J8QG1H4GAC" }

  describe "mapped" do
    context "when all store_ids are mapped" do
      it "should respond true" do
        post :mapped, params: { api_key: api_key, store_ids: "1,2,3,4,5,6,7" }
        assert_response :success, @response.body

        json = JSON.parse(@response.body)
        assert json["mapped"]
      end
    end

    context "when there is a store that is not mapped" do
      it "should respond false" do
        post :mapped, params: { api_key: api_key, store_ids: "1,2,3,4,5,6,7,8" }
        assert_response :success, @response.body

        json = JSON.parse(@response.body)
        expect(json["mapped"]).to eq true
      end
    end
  end

  describe "POST #posting_complete" do
    before do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
    end

    xit "should pass posting ids to the background workers" do
      expect(RewardSystem::PostingToBalancesWorker).to receive(:perform_async).with(["1"])
      expect(TaxBlocking::BatchPersonEarningsSummarizationWorker).to receive(:perform_async).with({ posting_ids: ["1"] })

      post :posting_complete, params: { api_key: api_key, posting_ids: [1] }
    end

    it "should respond with ok" do
      post :posting_complete, params: { api_key: api_key, posting_ids: [1] }
        expect(response.status).to eq(200)

        expected_response = { status: :ok }.to_json
        expect(response.body).to eq(expected_response)
    end

    it "calls TaxWithholdings::SipBatchWorker" do
      expect(TaxWithholdings::SipBatchWorker).to receive(:perform_async)
      post :posting_complete, params: { api_key: api_key }
    end
  end

  describe "vat_posting" do
    context "sip vat posting" do
      it "return vat is not applicable for the person" do
        person = create(:person)
        result = sip_posting_result(person)
        expect(VatTaxAdjustment.exists?).to eq false
        expect(result["message"]).to eq "VAT is not applicable for the person"
      end

      it "return vat is applicable for the person" do
        person = create(:person)
        person.update(country: "Luxembourg")
        result = sip_posting_result(person)
        expect(VatTaxAdjustment.exists?).to eq true
        expect(result["message"]).to eq "Successfully completed VAT posting"
      end
    end
  end

  def sip_posting_result(person)
    intake_records = create_intake_records(person)
    expect(VatTaxAdjustment.exists?).to eq false
    mock_vat_applicable_countries_api
    sales_record_master_id = intake_records[:sales_record_master_id]
    you_tube_royalty_record_ids = intake_records[:you_tube_royalty_record_ids]
    allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
    post :vat_posting, params: { api_key: ENV.fetch('SIP_VAT_API_KEY'), person_id: person.id,
                                 sales_record_master_ids: sales_record_master_id,
                                 you_tube_royalty_record_ids: you_tube_royalty_record_ids
                               }
    assert_response :success
    JSON.parse(@response.body)
  end

  def create_intake_records(person)
    you_tube_royalty_intake = create(:you_tube_royalty_intake, person: person)
    you_tube_royalty_record_ids = you_tube_royalty_intake.you_tube_royalty_records.pluck(:id)
    sip_store = create(:sip_store)
    sales_record_master = create(:sales_record_master, sip_store: sip_store)
    person_intake = create(:person_intake, person: person)
    create(:person_intake_sales_record_master, person_intake: person_intake, sales_record_master: sales_record_master)
    { sales_record_master_id: [sales_record_master.id], you_tube_royalty_record_ids: you_tube_royalty_record_ids }
  end

  def mock_vat_applicable_countries_api
    allow_any_instance_of(TcVat::Base)
      .to receive(:vat_applicable_countries)
      .and_return(
        'business' => [{
          'country' => 'Luxembourg',
          'tax_rate' => 10
        }],
        'individual' => []
      )
  end
end
