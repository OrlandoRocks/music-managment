require "rails_helper"

describe Api::TcSocialController do
  describe "index" do

    context "hitting index with a non-existent token" do
      it "should respond with status 401" do
        get :index, params: { token: "FOOBAR" }
        expect(response.status).to eq(401)
      end
    end

    context "hitting index with a valid token" do
      let(:person) {FactoryBot.create(:person, :with_live_album)}

      it "should respond with json user" do
        allow(Person).to receive(:find).and_return(person)
        plan_status = ::Social::PlanStatus.for(person)
        client      = ClientApplication.create(name: "FooClient", url: "http://bar.com")
        oa          = Oauth2Token.create(user: person, client_application: client)
        get :index, params: { token: oa.token }

        data = { "tc_user_id"           => oa.user_id,
              "tc_first_name"           => person.first_name,
              "tc_last_name"            => person.last_name,
              "tc_email"                => person.email,
              "tc_plan"                 => plan_status.plan,
              "tc_plan_expires_at"      => plan_status.plan_expires_at.to_s,
              "tc_grace_period_ends_on" => plan_status.grace_period_ends_on.to_s,
              "tc_payment_channel"      => nil,
              "tc_country_website"      => person.country_domain }

        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)).to eq(data)
      end
    end
  end
end
