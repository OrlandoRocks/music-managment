require "rails_helper"

describe Api::ArtworkController, type: :controller do
  before(:each) do
    @person = FactoryBot.create(:person)
    @album = FactoryBot.create(:album, person: @person)
    login_as @person
    allow_any_instance_of(Artwork).to receive(:copy_image_file_from_s3).and_return(true)
  end

  describe "Artwork#create" do
    it "creates an album" do
      headers = { 'Accept' => Mime[:json], 'Content-Type' => Mime[:json].to_s }
      request.headers.merge!(headers)
      post :create, params: {
        api_key: API_CONFIG["artwork"]["API_KEY"],
        album_id: @album.id,
        s3_artwork_keys: {
          bucket: "tunecore.com.dev",
          key: "artv1/38528.tiff"
        }
      }
      expect(response.status).to eq(201)
    end

    it "does not create albums" do
      headers = { 'Accept' => Mime[:json], 'Content-Type' => Mime[:json].to_s }
      request.headers.merge!(headers)
      post :create, params: {
        api_key: API_CONFIG["artwork"]["API_KEY"],
        album_id: nil,
        s3_artwork_keys: {
          bucket: "tunecore.com.dev",
          key: "artv1/38528.tiff"
        }
      }
      expect(response.status).to eq(422)
    end
  end
end
