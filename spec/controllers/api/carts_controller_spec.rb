require 'rails_helper'

RSpec.describe Api::CartsController, type: :controller do

  context "With a valid finalize CartsController" do
    let(:person) { create(:person, :with_unpaid_purchases) }
    let(:invoice)  { create(:invoice, person: person) }

    let(:card_params)do
      {
        firstname: person.name,
        lastname: person.name,
        braintree_payment_nonce: "fake-valid-visa-nonce",
        ip_address: "0.0.0.0",
        merchant_defined_field_5: 1,
        merchant_defined_field_10: 1
      }
    end

    let(:card) { StoredCreditCard.create_with_braintree(person, card_params) }

    let(:success_params) do
      {
        cart_finalize: {
          use_balance: "false",
          payment_id: card.last.related_id
        },
        api_key: API_CONFIG["API_KEY"]
      }
    end

    before do
      client_application = ClientApplication.where(name: "tc_social").first
      Oauth2Token.create!(user: person, client_application: client_application, scope: nil)

      request.headers["Authorization"] = "Token token=#{SessionEncryptionEngine.encrypt64(person.tc_social_token)}"

      cart_form = double(save: true, invoice: invoice, purchased_before?: false)
      allow(CartFinalize).to receive(:new).and_return(cart_form)
    end

    it "Creates a success for finalize API" do
      expect(SuccessfulPurchaseWorker).to receive(:perform_async).with(invoice.id)
      post :finalize, params: success_params
      person.reload
      expect(JSON.parse(response.body)["status"]).to eq("success")
    end
  end

  context "With a invalid finalize CartsController" do
    it "Creates a failure for finalize API" do
      person = people(:tunecore_customer_with_a_finalized_and_ready_to_go_album)
      client_application = ClientApplication.where(name: "tc_social").first
      Oauth2Token.create!(user: person, client_application: client_application, scope: nil)
      request.headers["Authorization"] = "Token token=#{SessionEncryptionEngine.encrypt64(person.tc_social_token)}"
      params = {
                cart_finalize: {
                  use_balance: "false",
                  payment_id: ''
                },
                api_key: API_CONFIG["API_KEY"]
               }
      post :finalize, params: params
      person.reload
      expect(JSON.parse(response.body)["status"]).to eq("failure")
      expect(JSON.parse(response.body)["message"]).to eq(["Purchases can't be blank"])
    end
  end

  context "With a invalid finalize CartsController" do
    it "Creates a failure for finalize API" do
      person = people(:tunecore_customer_with_a_finalized_and_ready_to_go_album)
      params = {
                 cart_finalize: {
                  use_balance: "false",
                  payment_id: ''
                },
                api_key: API_CONFIG["API_KEY"]
               }
      post :finalize, params: params
      person.reload
      expect(JSON.parse(response.body)["status"]).to eq("failure")
      expect(JSON.parse(response.body)["code"]).to eq(2000)
    end
  end

end
