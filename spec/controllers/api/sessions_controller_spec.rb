require 'rails_helper'

RSpec.describe Api::SessionsController, type: :controller do
  context "with a valid create Session method" do
    it "creates a session for a person" do
      person = FactoryBot.create(:person, :with_tc_social)
      expect(session[:person]).to be_blank
      params = { api_key: API_CONFIG["API_KEY"] }
      request.headers["Authorization"] = ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(person.tc_social_token))
      post :create, params: params
      expect(JSON.parse(response.body)["status"]).to eq("success")
      expect(session[:person]).not_to be_blank
    end
  end

  context "with a invalid create Session method" do
    it "failure to create a session for a person" do
      expect(session[:person]).to be_blank
      params = { api_key: API_CONFIG["API_KEY"] }
      request.headers["Authorization"] = " "
      service = post :create, params: params
      expect(JSON.parse(response.body)["status"]).to eq("failure")
      expect(session[:person]).to be_blank
    end
  end

  context "with valid fetch user Session method" do
    it "gives a success response" do
      person = FactoryBot.create(:person, :with_tc_social)
      session[:person] = person.id
      session_key = "session:#{session.id}"
      allow(Marshal).to receive(:load)
                    .with($redis.get(session_key))
                    .and_return({"person" => "#{person.id}"})
      params = { api_key: API_CONFIG["API_KEY"], tunecore_id: session.id }
      get :fetch_user, params: params
      expect(JSON.parse(response.body)["status"]).to eq("success")
      expect(JSON.parse(response.body)).to be_a(Hash)
    end
  end

  context "with an invalid fetch user Session method" do
    it "throws failure for a person" do
      person = FactoryBot.create(:person, :with_tc_social)
      session[:person] = person.id
      session_key = "abcdefgh"
      params = { api_key: API_CONFIG["API_KEY"], tunecore_id: session_key }
      get :fetch_user, params: params
      expect(JSON.parse(response.body)["status"]).to eq("failure")
    end
  end

  context "with a valid logout Session method" do
    it "destroys a session for a person" do
      person = FactoryBot.create(:person, :with_tc_social)
      session[:person] = person.id
      params = { api_key: API_CONFIG["API_KEY"] }
      request.headers["Authorization"] = ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(person.tc_social_token))
      delete :clear_session, params: params
      expect(JSON.parse(response.body)["status"]).to eq("success")
      expect(session[:person]).to be_blank
    end
  end

  context "with a invalid logout Session method" do
    it "failure to destroy a session for a person" do
      session[:person] = nil
      params = {api_key: API_CONFIG["API_KEY"] }
      request.headers["Authorization"] = " "
      service = delete :clear_session, params: params
      expect(JSON.parse(response.body)["status"]).to eq("failure")
    end
  end
end
