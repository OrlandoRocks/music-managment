require "rails_helper"

describe Api::External::ApiClientController, type: :controller do
  let(:album) { create(:album, :with_petri_bundle, :with_distribution_api_album) }
  let(:admin) { create(:person, :admin, :with_role, role_name: "Content Review Manager")}
  let(:review_audit) { create(:review_audit, :approved, person: admin, album: album) }
  describe ".get_album_status" do
    it "should respond with status 'ok'" do
      puts review_audit
      expect(Bytedance::ApiClientWorker).to receive(:perform_async).and_return(true)
      get :get_album_status, params: {tc_album_id: album.id, source_album_id: 'test1345', api_key: API_CONFIG["API_KEY"]}
      json_response = JSON.parse(response.body)
      expect(json_response['status']).to eq('ok')
    end
    it "should respond with status 'not found'" do
      puts review_audit
      expect(Bytedance::ApiClientWorker).not_to receive(:perform_async)
      get :get_album_status, params: {tc_album_id: 'test12345', source_album_id: 'test1345', api_key: API_CONFIG["API_KEY"]}
      json_response = JSON.parse(response.body)
      expect(json_response['status']).to eq('not_found')
    end
  end
end
