require "rails_helper"

describe Api::External::SongsController, type: :controller do
  let(:api_key) { API_CONFIG["songs"]["API_KEY"] }

  before(:each) do
    @person = FactoryBot.create(:person)
    @album = FactoryBot.create(:album, :finalized, :approved, :with_uploaded_songs_and_artwork, :with_apple_music, person: @person)

    login_as @person
  end

  describe "Songs#ad_songs" do

    context "authenticated" do
      it "gets songs for the ad campaign" do
        allow_any_instance_of(Api::V2::BaseController).to receive(:authenticate_user!).and_return(true)
        headers = { 'Accept' => 'application/json', 'Content-Type' => Mime[:json].to_s }
        request.headers.merge!(headers)

        get :ad_songs, params: {
          api_key: api_key,
          person_id: @person.id,
        }

        expect(response.status).to eq(200)
      end
    end

    it "sends a Not Found Status if person does not exist" do
      allow_any_instance_of(Api::V2::BaseController).to receive(:authenticate_user!).and_return(true)
      headers = { 'Accept' => 'application/json', 'Content-Type' => Mime[:json].to_s }
      request.headers.merge!(headers)

      get :ad_songs, params: {
        api_key: api_key,
        person_id: 9000000,
      }

      expect(response.status).to eq(404)
    end

    context "unauthenticated" do
      it "should return a 401 status code" do
        headers = { 'Accept' => 'application/json', 'Content-Type' => Mime[:json].to_s }
        request.headers.merge!(headers)

        get :ad_songs, params: {
          api_key: api_key,
          person_id: 9000000,
        }

        expect(response.status).to eq(401)
      end
    end
  end
end
