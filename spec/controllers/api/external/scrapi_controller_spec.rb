require "rails_helper"

describe Api::External::ScrapiController, type: :controller do
  describe ".callback" do
    it "should respond with status 'ok'" do
      expect(Scrapi::GetJobWorker).to receive(:perform_async).and_return(true)
      get :callback, params: {jobId: 'test', api_key: API_CONFIG["API_KEY"] }
      json_response = JSON.parse(response.body)
      expect(json_response['status']).to eq('ok')
    end
  end
end
