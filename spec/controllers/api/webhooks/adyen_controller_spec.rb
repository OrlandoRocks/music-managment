require "rails_helper"

RSpec.describe Api::Webhooks::AdyenController, type: :controller do
  describe "authorize_payment" do
    let(:person) { create(:person, country: "PH") }
    let(:adyen_transaction) { create(:adyen_transaction, person: person, country: Country.find_by(iso_code: "PH")) }
    it "should render accepted if the transaction details are present" do
      allow(AdyenTransaction)
        .to receive(:find_by)
        .and_return(adyen_transaction)
      allow_any_instance_of(Adyen::UpdateAdyenTransactionInfoService)
        .to receive(:update_transaction)
        .and_return("authorised")
      allow_any_instance_of(Adyen::AuthorizationService)
        .to receive(:authorize!)
        .and_return(true)
        
      get "authorize_payment",
          params: {
            "live" => "false",
            "notificationItems" => [
              {
                "NotificationRequestItem" => {
                  "additionalData" => {
                    "expiryDate" => "10/2030",
                    "authCode" => "036938",
                    "recurring.recurringDetailReference" => "VZCQPCV6B6KXWD82",
                    "cardSummary" => "0000",
                    "threeds2.cardEnrolled" => "false",
                    "checkoutSessionId" => "CSA77D6D249E170BD1",
                    "recurringProcessingModel" => "CardOnFile",
                    "recurring.shopperReference" => "ADYEN-ID-12"
                  },
                  "amount" => { "currency" => "PHP", "value" => 987_294 },
                  "eventCode" => "AUTHORISATION",
                  "eventDate" => "2022-04-19T19:13:17+02:00",
                  "merchantAccountCode" => "TuneCoreECOM",
                  "merchantReference" => "39",
                  "operations" => ["CANCEL", "CAPTURE", "REFUND"],
                  "paymentMethod" => "cup",
                  "pspReference" => "DG9RDMJTLGXXGN82",
                  "reason" => "036938:0000:10/2030",
                  "success" => "true"
                }
              }
            ]
          }
      expect(JSON.parse(response.body)).to eq({ "notificationResponse" => "[accepted]", "authorisation" => "authorised" })
    end
  end
end
