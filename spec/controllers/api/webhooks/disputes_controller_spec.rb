require "rails_helper"

RSpec.describe Api::Webhooks::DisputesController, type: :controller do
  describe "POST #braintree" do
    let(:payin_provider_config) do
      PayinProviderConfig.find_by(name: PayinProviderConfig::BRAINTREE_NAME)
    end

    let(:gateway) do
      Braintree::ConfigGatewayService.new(payin_provider_config).gateway
    end

    context "success" do
      it "responds with http status 200" do
        webhook_notification = gateway.webhook_testing.sample_notification(
          Braintree::WebhookNotification::Kind::DisputeOpened,
          "my_id"
        )

        parsed_response =
          Braintree::WebhookParser
          .new
          .parse!(webhook_notification[:bt_signature], webhook_notification[:bt_payload])
          .dispute

        create(
          :invoice,
          :with_braintree_settlement,
          transaction_id: parsed_response.transaction.id
        )

        post :braintree, params: {
          bt_signature: webhook_notification[:bt_signature],
          bt_payload: webhook_notification[:bt_payload]
        }

        expect(response.status).to eq(200)
      end
    end

    context "failure" do
      it "responds with http status 400" do
        post :braintree, params: {
          bt_signature: "wrong",
          bt_payload: "wrong"
        }

        expect(response.status).to eq(400)
      end
    end
  end
end
