require "rails_helper"

describe Api::Social::PasswordResetController do
  context "with a valid user" do
    it "responds with a successful reset message" do
      allow(TransactionalEmail::Base).to receive(:deliver).and_return(true)
      person = create(:person)
      post :create, params: {email: person.email}
      expect(JSON.parse(response.body)["status"]).to eq("success")
    end
  end

  context "with invalid user" do
    it "responds with a failure message" do
      expect(TransactionalEmail::ForgotPassword).not_to receive(:deliver)
      post :create, params: {email: "james@gmail.com"}
      expect(JSON.parse(response.body)["status"]).to eq("failure")
    end
  end
end
