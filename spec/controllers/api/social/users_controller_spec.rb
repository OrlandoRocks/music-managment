require "rails_helper"

describe Api::Social::UsersController, type: :controller do

  describe "#show" do
    context "unauthorized access" do
      it "renders a failure response" do
        get :show, params: {}
        expect(response.status).to eq(200)
        expect(response.body).to eq({status: "failure", code: 2000}.to_json)
      end
    end

    context "authorized access" do
      it "renders a 200 json response" do
        @person = FactoryBot.create(:person, :with_tc_social)
        controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(@person.tc_social_token))
        get :show
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)).to be_a(Hash)
      end
    end
  end
end

