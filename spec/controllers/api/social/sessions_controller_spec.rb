require "rails_helper"

describe Api::Social::SessionsController, type: :controller do

  describe "#create" do
    context "unauthorized access" do
      it "renders a failure response when no params" do
        post :create, params: {}
        expect(response.status).to eq(200)
        expect(response.body).to eq({status: "failure", code: 2000}.to_json)
      end

      it "renders failure on bad user/password" do
        @person = FactoryBot.create(:person, :with_active_social_subscription_status)
        controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(@person.tc_social_token))
        post :create, params: {email: @person.email, password: 'wrong pass'}
        expect(response.status).to eq(200)
        expect(response.body).to eq({status: "failure", code: 2000}.to_json)
      end
    end

    context "when given cookies prefixed with wp_tunecore_" do
      it "builds referral_data" do
        cookie_data_1 = {
          ref: "TestData",
          jt: 123,
          utm_source: "pubadmin",
          utm_medium: "web",
          utm_term: "music",
          utm_content: "testing",
          utm_campaign: "footer-links",
          tc_timestamp: 1458752702,
        }
        cookie_data_2 = {
          ref: "TestData",
          jt: 123,
          utm_source: "pubadmin",
          utm_medium: "web",
          utm_term: "music",
          utm_content: "testing",
          utm_campaign: "footer-links",
          tc_timestamp: 1458752702,
        }
        cookies[:tunecore_wp] = cookie_data_1.to_json
        cookies[:tunecore_wp_1] = cookie_data_2.to_json

        @person = FactoryBot.create(:person, :with_active_social_subscription_status)
        controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(@person.tc_social_token))
        post :create, params: {email: @person.email, password: 'Password123!'}

        expect(assigns[:person].referral_data.size).to eq 14
      end
    end

    context "when given cookies NOT prefixed with wp_tunecore_" do
      it "doesnt build referral_data" do
        cookies["other_cookie"] = { ref: "TestData" }.to_json

        @person = FactoryBot.create(:person, :with_active_social_subscription_status)
        controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(@person.tc_social_token))
        post :create, params: {email: @person.email, password: 'Password123!'}

        expect(assigns[:person].referral_data.size).to eq 0
      end
    end

    context "authorized access" do
      it "renders a 200 json response" do
        @person = FactoryBot.create(:person, :with_active_social_subscription_status)
        controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(@person.tc_social_token))
        post :create, params: {email: @person.email, password: 'Password123!'}
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)).to be_a(Hash)
      end
    end
  end
end

def encrypt_token(token)
  SessionEncryptionEngine.encrypt64(token)
end
