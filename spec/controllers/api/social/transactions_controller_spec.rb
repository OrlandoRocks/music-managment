require "rails_helper"

describe Api::Social::TransactionsController do
  let(:person) {FactoryBot.create(:person, :with_tc_social)}
  describe "index" do
    context "success" do

      before do
        controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(person.tc_social_token))
      end

      it "returns the correct response" do
        params = {start_date: "2017-01-01", end_date: "2017-03-01"}
        get :index, params: params

        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)["status"]).to eq("success")
      end
    end

    context "failure" do
      it "returns the correct response" do
        get :index
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)).to eq({"status" => "failure", "code" => 2000})
      end
    end
  end
end
