require "rails_helper"

describe Api::Linkshare::PreviewLinkController, type: :controller do
  fixtures :songs

  context "With a valid song_id" do
    let(:person) { create(:person, :with_tc_social) }
    let(:album)  { create(:album, :with_spotify_album_esi, person: person) }
    let(:song)  { create(:song, :with_spotify_song_esi, album: album) }

    it "Creates a successful stores list" do
      request.headers["Authorization"] = "Token token=#{SessionEncryptionEngine.encrypt64(person.tc_social_token)}"
      store_ids = ExternalServiceId.where(linkable: song).ids
      params = { song_id: song.id, store_ids: "#{store_ids}" }
      get :index, params: params
      expect(JSON.parse(response.body)["status"]).to eq("success")
    end

    it "throws failure if no stores found" do
      request.headers["Authorization"] = "Token token=#{SessionEncryptionEngine.encrypt64(person.tc_social_token)}"
      params = { song_id: song.id, store_ids: "[]" }
      get :index, params: params
      expect(JSON.parse(response.body)["status"]).to eq("failure")
      expect(JSON.parse(response.body)["message"]).to eq("No Stores found")
    end
  end

  context "With an invalid song_id" do
    let(:person) { create(:person, :with_tc_social) }

    it "throws failure if no songs found" do
      request.headers["Authorization"] = "Token token=#{SessionEncryptionEngine.encrypt64(person.tc_social_token)}"
      params = { song_id: nil, store_ids: "[]" }
      get :index, params: params
      expect(JSON.parse(response.body)["status"]).to eq("failure")
      expect(JSON.parse(response.body)["message"]).to eq("No Song found")
    end
  end
end
