require "rails_helper"

describe Api::Linkshare::StoresController, type: :controller do
  fixtures :songs

  context "With a valid song_id" do
    let(:person) { create(:person, :with_tc_social) }
    let(:album)  { create(:album, :with_spotify_album_esi, person: person) }
    let(:song)  { create(:song, :with_spotify_song_esi, album: album) }

    it "Creates a successful stores list" do
      song.external_service_ids.last.update(identifier: '53A2MsBqPdDzZzP3Dm8rfR')
      request.headers["Authorization"] = "Token token=#{SessionEncryptionEngine.encrypt64(person.tc_social_token)}"
      params = { song_id: song.id }
      get :index, params: params
      expect(JSON.parse(response.body)["status"]).to eq("success")
    end

    it "throws failure if no stores found" do
      album = create(:album, person:person)
      song = create(:song, album: album)
      request.headers["Authorization"] = "Token token=#{SessionEncryptionEngine.encrypt64(person.tc_social_token)}"
      params = { song_id: song.id }
      get :index, params: params
      expect(JSON.parse(response.body)["status"]).to eq("failure")
      expect(JSON.parse(response.body)["message"]).to eq("api.tc.message.no_stores")
    end

    it "throws failure if song does not belong to user" do
      person_2 = create(:person, :with_tc_social)
      album = create(:album, person: person_2)
      song = create(:song, album: album)
      request.headers["Authorization"] = "Token token=#{SessionEncryptionEngine.encrypt64(person.tc_social_token)}"
      params = { song_id: song.id }
      get :index, params: params
      expect(JSON.parse(response.body)["status"]).to eq("failure")
      expect(JSON.parse(response.body)["message"]).to eq("api.tc.message.not_user_song")
    end
  end
end
