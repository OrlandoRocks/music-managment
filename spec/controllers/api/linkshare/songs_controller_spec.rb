require 'rails_helper'

describe Api::Linkshare::SongsController, type: :controller do
  describe "#index" do
    context "unauthorized access" do
      it "renders a failure response" do
        get :index
        expect(response.status).to eq(200)
        expect(response.body).to eq({status: "failure", code: 2000}.to_json)
      end
    end

   context "authorized access with releases" do
      it "renders a 200 json response" do
        @person = create(:person, :with_tc_social)
        controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(@person.tc_social_token))
        album = create(:album, :with_spotify_album_esi, person: @person)
        song_1 = create(:song, :with_spotify_song_esi, album: album)
        song_2 = create(:song, album: album)
        params = { page: 1, per_page: 10 }
        get :index, params: params
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)).to be_a(Hash)
      end
    end

    context "authorized access with no releases" do
      it "renders a 200 json response" do
        @person = create(:person, :with_tc_social)
        controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(@person.tc_social_token))
        params = { page: 1, per_page: 10 }
        get :index, params: params
        expect(response.status).to eq(200)
        expect(response.body).to eq({ status: "failure", message: ["api.tc.message.user_no_albums"] }.to_json)
      end
    end
  end
end
