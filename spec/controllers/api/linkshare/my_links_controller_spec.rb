require 'rails_helper'

describe Api::Linkshare::MyLinksController, type: :controller do
  context "With a valid song_id" do
    let(:person) { create(:person, :with_tc_social) }
    let(:album)  { create(:album, :with_spotify_album_esi, person: person) }
    let(:song_1)  { create(:song, :with_spotify_song_esi, album: album) }
    let(:song_2)  { create(:song, :with_spotify_song_esi, album: album) }

    it "Creates a successful stores list" do
      request.headers["Authorization"] = "Token token=#{SessionEncryptionEngine.encrypt64(person.tc_social_token)}"
      params = { songs_ids: [song_1.id, song_2.id] }
      post :create, params: params
      expect(JSON.parse(response.body)["status"]).to eq("success")
    end

    it "Throws failure if user has no albums" do
      user = create(:person, :with_tc_social)
      request.headers["Authorization"] = "Token token=#{SessionEncryptionEngine.encrypt64(user.tc_social_token)}"
      params = { songs_ids: [song_1.id, song_2.id] }
      post :create, params: params
      expect(JSON.parse(response.body)["status"]).to eq("failure")
      expect(JSON.parse(response.body)["message"]).to eq("User has no Albums")
    end
  end

  context "With an invalid song_id" do
    let(:person) { create(:person, :with_tc_social) }
    let(:album)  { create(:album, :with_spotify_album_esi, person: person) }
    let(:song_1)  { create(:song, :with_spotify_song_esi, album: album) }
    let(:song_2)  { create(:song, :with_spotify_song_esi, album: album) }

    it "Throws failure if user has no albums" do
      user = create(:person, :with_tc_social)
      user_album = create(:album, :with_spotify_album_esi, person: user)
      request.headers["Authorization"] = "Token token=#{SessionEncryptionEngine.encrypt64(user.tc_social_token)}"
      params = { songs_ids: [song_1.id, song_2.id] }
      post :create, params: params
      expect(JSON.parse(response.body)["status"]).to eq("failure")
      expect(JSON.parse(response.body)["message"]).to eq("No Valid Song ids given for the user")
    end
  end
end
