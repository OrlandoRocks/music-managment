require 'rails_helper'

RSpec.describe Api::CertController, type: :controller do

  context "With a valid Cert create" do
    let(:person) { create(:person, :with_unpaid_purchases) }
    let(:invoice)  { create(:invoice, person: person) }
    it "Creates a success for cert API" do
      client_application = ClientApplication.where(name: "tc_social").first
      Oauth2Token.create!(user: person, client_application: client_application, scope: nil)
      request.headers["Authorization"] = "Token token=#{SessionEncryptionEngine.encrypt64(person.tc_social_token)}"
      purchase = person.purchases.last
      product = purchase.product
      product.product_type = 'Renewal'
      product.created_by = person
      product.save
      cert = Cert.last
      cert.cert_engine = 'DefaultRenewalPercent'
      cert.save
      params = { purchase_id: purchase.id,
                cert: {cert: cert.cert}, api_key: API_CONFIG["API_KEY"] }   
      post :create, params: params
      expect(JSON.parse(response.body)["status"]).to eq("success")
    end
  end

  context "With an invalid Cert create" do
    it "Creates a failure for cert create API" do
      person = people(:tunecore_customer_with_a_finalized_and_ready_to_go_album)
      client_application = ClientApplication.where(name: "tc_social").first
      Oauth2Token.create!(user: person, client_application: client_application, scope: nil)
      request.headers["Authorization"] = "Token token=#{SessionEncryptionEngine.encrypt64(person.tc_social_token)}"
      params = {
                purchase_id: person.purchases.last.id,
                "cert": {"cert": Cert.last.cert},
                api_key: API_CONFIG["API_KEY"]
               }
      post :create, params: params
      person.reload
      expect(JSON.parse(response.body)["status"]).to eq("failure")
      expect(JSON.parse(response.body)["message"]).to eq(["Cert not Applied"])
    end
  end

  context "With an invalid user for Cert create" do
    it "Creates a failure for invalid user cert create API" do
      person = people(:tunecore_customer_with_a_finalized_and_ready_to_go_album)
      client_application = ClientApplication.where(name: "tc_social").first
      Oauth2Token.create!(user: person, client_application: client_application, scope: nil)
      params = {
                purchase_id: person.purchases.last.id,
                "cert": {"cert": Cert.last.cert},
                api_key: API_CONFIG["API_KEY"]
               }
      post :create, params: params
      person.reload
      expect(JSON.parse(response.body)["status"]).to eq("failure")
      expect(JSON.parse(response.body)["code"]).to eq(2000)
    end
  end

  describe "POST remove_cert" do
    let(:person) { create(:person, :with_unpaid_purchases) }
    let(:purchase) { person.purchases.last }
    context "With valid params" do
      it "Returns success" do
        cert = Cert.last
        cert.cert_engine = "DefaultRenewalPercent"
        cert.expiry_date = 30.days.from_now
        cert.save
        verified = Cert.verify verify_opts(entered_code: cert.cert)
        expect(verified).to be_a(Cert)
        client_application = ClientApplication.where(name: "tc_social").first
        Oauth2Token.create!(user: person, client_application: client_application, scope: nil)
        request.headers["Authorization"] = "Token token=#{SessionEncryptionEngine.encrypt64(person.tc_social_token)}"
        params = { purchase_id: purchase.id, api_key: API_CONFIG["API_KEY"] }
        post :remove, params: params
        expect(JSON.parse(response.body)["status"]).to eq("success")
      end
    end
    context "With purchase not having cert applied" do
      it "Returns failure" do
        client_application = ClientApplication.where(name: "tc_social").first
        Oauth2Token.create!(user: person, client_application: client_application, scope: nil)
        request.headers["Authorization"] = "Token token=#{SessionEncryptionEngine.encrypt64(person.tc_social_token)}"
        params = { purchase_id: purchase.id, api_key: API_CONFIG["API_KEY"] }
        post :remove, params: params
        person.reload
        expect(JSON.parse(response.body)["status"]).to eq("failure")
        expect(JSON.parse(response.body)["message"]).to eq(["Cert not available"])
      end
    end

    def verify_opts(options = {})
      {
        entered_code: "0001",
        purchase: purchase,
        current_user: person
      }.merge(options)
    end
  end
end
