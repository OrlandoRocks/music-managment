require "rails_helper"

describe Api::SongsController, type: :controller do
  before(:each) do
    @person = create(:person)
  end

  context "with valid parameters" do
    before(:each) do
      queue = double
      allow(queue).to receive(:send_message)
      sqs = double(queues: double(named: queue))
      stub_const("SQS_CLIENT", sqs)

      @album = create(:album, person: @person)

      @track = [
        {
          "name" => "Natham Loves Family, Rock, and iPod",
          "creatives" => [{ "role" => "primary_artist", "name" => "Beltzer" }],
          "optional_isrc" => "",
          "parental_advisory" => "0",
          "clean_version" => "0",
          "track_num" => "1",
          "asset" => { "key" => "test/884088560058/01 NathamSmith_Beltzer.mp3", "bucket" => "tunecore.mass-ingestion" }
        }
      ]

      json_mime = Mime::Type.lookup("application/json")

      request.headers["Accept"] = json_mime
      request.headers["Content-Type"] = json_mime.to_s

      post :create, params: {
        album_id: @album.id,
        tracks: @track,
        song: { "album_id" => @album.id },
        api_key: API_CONFIG["songs"]["API_KEY"]
      }
    end

    it "creates the song" do
      expect(response.status).to eq(201)
    end
  end
end
