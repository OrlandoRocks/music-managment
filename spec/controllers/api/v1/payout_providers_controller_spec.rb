require "rails_helper"

describe Api::V1::PayoutProvidersController do
  let(:key) { "ABC123" }

  before(:each) do
    allow(SECRETS).to receive(:[]).and_return(:default)
    allow(SECRETS).to receive(:[]).with("api_v1_secret_key").and_return("secretkey")
    create(:api_key, key: Digest::SHA2.hexdigest(SECRETS["api_v1_secret_key"] + key))
  end

  describe "#approved" do
    let(:person) { create(:person) }
    let(:active_provider) { create(:payout_provider) }
    let(:declined_provider) { create(:payout_provider, :declined, active_provider: false) }

    it "returns 422 if no api key" do
      params = { client_payee_id: 1 }

      get :approved, params: params

      expect(response.status).to eq 422
    end

    it "returns 200 with not found message if payout provider not found" do
      params = { client_payee_id: 1, key: key }

      get :approved, params: params

      expect(response.status).to eq 200
      expect(response.body).to eq Api::V1::PayoutProvidersController::RESPONSES[:not_found].to_json
    end

    it "returns 200 with approved message and updates provider" do
      params = { client_payee_id: active_provider.client_payee_id, key: key, provider_identifier: "11234" }

      get :approved, params: params

      expect(response.status).to eq 200
      expect(response.body).to eq Api::V1::PayoutProvidersController::RESPONSES[:approved].to_json
      expect(active_provider.reload.provider_status).to eq PayoutProvider::APPROVED
      expect(active_provider.reload.provider_identifier).to eq "11234"
    end

    it "activates an inactive provider" do
      params = { client_payee_id: declined_provider.client_payee_id, key: key, provider_identifier: "11234" }
      get :approved, params: params

      expect(response.status).to eq 200
      expect(response.body).to eq Api::V1::PayoutProvidersController::RESPONSES[:approved].to_json
      expect(declined_provider.reload.provider_status).to eq PayoutProvider::APPROVED
      expect(declined_provider.reload.active_provider).to eq true
    end

    it "returns failure response when there is already an active provider" do
      person.payout_providers << declined_provider
      person.payout_providers << active_provider

      params = { client_payee_id: declined_provider.client_payee_id, key: key, provider_identifier: "11234" }

      get :approved, params: params

      expect(response.status).to eq 200
      expect(response.body).to eq Api::V1::PayoutProvidersController::RESPONSES[:not_approved].to_json
      expect(declined_provider.reload.provider_status).to eq PayoutProvider::DECLINED
    end

    it "updates the person's address and country with data from Payoneer" do
      person.payout_providers << active_provider
      params = { client_payee_id: active_provider.client_payee_id, key: key, provider_identifier: "11234" }
      payee_extended_detail_address = {
        "address_line_1" => Faker::Address.street_address,
        "address_line_2" => Faker::Address.secondary_address,
        "city" => Faker::Address.city,
        "state" => Faker::Address.state,
        "zip_code" => Faker::Address.zip_code,
        "country" => "CA"
      }
      contact = {
        "first_name" => Faker::Name.first_name,
        "last_name" => Faker::Name.last_name,
        "email" => Faker::Internet.unique.email,
        "mobile" => Faker::PhoneNumber.unique.phone_number,
        "phone" => Faker::PhoneNumber.unique.phone_number
      }
      payee_extended_detail_response = Payoneer::Responses::PayeeExtendedDetail.new(
        double(
          "response",
          body: JSON.dump(
            address: payee_extended_detail_address,
            contact: contact
          )
        )
      )

      api_service_double = double
      allow(api_service_double).to receive(:payee_extended_details).and_return(payee_extended_detail_response)
      allow(PayoutProvider::ApiService).to receive(:new).and_return(api_service_double)
      allow(FeatureFlipper).to receive(:show_feature?) { true }

      get :approved, params: params

      expect(person.reload.attributes).to include(
        "country" => payee_extended_detail_address["country"]
      )
    end
  end

  describe "#declined" do
    it "returns 422 if no api key" do
      params = { client_payee_id: 1 }

      get :declined, params: params

      expect(response.status).to eq 422
    end

    it "returns 200 with not found message if payout provider not found" do
      params = { client_payee_id: 1, key: key }

      get :declined, params: params

      expect(response.status).to eq 200
      expect(response.body).to eq Api::V1::PayoutProvidersController::RESPONSES[:not_found].to_json
    end

    it "returns 200 with declined message and updates provider" do
      payout_provider = create(:payout_provider)
      params = { client_payee_id: payout_provider.client_payee_id, key: key, provider_identifier: "11234" }

      get :declined, params: params

      expect(response.status).to eq 200
      expect(response.body).to eq Api::V1::PayoutProvidersController::RESPONSES[:declined].to_json
      expect(payout_provider.reload.provider_status).to eq PayoutProvider::DECLINED
      expect(payout_provider.reload.provider_identifier).to eq "11234"
    end
  end

  describe "#released" do
    context "no api key" do
      before do
        get :released, params: { client_payee_id: 1 }
      end

      it "should respond with error" do
        expect(response).to be_unprocessable
      end
    end

    context "invalid client payee id" do
      before do
        get :released, params: { client_payee_id: 1, key: key }
      end

      it "should responds with not found message" do
        # legacy response.
        expect(response).to be_ok
        expect(response.body)
          .to eq Api::V1::PayoutProvidersController::RESPONSES[:not_found].to_json
      end
    end

    context "valid payee id" do
      let(:payout_provider) { create(:payout_provider) }

      before do
        get :released, params: {
          client_payee_id: payout_provider.client_payee_id,
          key: key
        }
      end

      it "should update payout provider status to 'pending'" do
        expect(payout_provider.reload.provider_status).to eq(PayoutProvider::PENDING)
      end

      it "should respond with ok" do
        expect(response).to be_ok
        expect(response_json["message"]).to eq("ok")
      end
    end
  end
end
