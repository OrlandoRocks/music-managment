require "rails_helper"

describe Api::V1::UsersController do
  let(:key) { "AGSRGS" }

  before(:each) do
    allow(SECRETS).to receive(:[]).and_return(:default)
    allow(SECRETS).to receive(:[]).with("api_v1_secret_key").and_return("secretkey")
    create(:api_key, key: Digest::SHA2.hexdigest(SECRETS["api_v1_secret_key"] + key))
  end

  describe "index" do
    context "with valid API key" do
      let(:person)  { FactoryBot.create(:person) }
      context "with valid email" do
        it "returns the correct response" do
          get :index, params: {email: person.email, key: key}
          expect(response.status).to eq(200)
          expect(JSON.parse(response.body)).to include("email" => person.email, "person_id" => person.id)
        end
      end

      context "with invalid email" do
        it "returns the correct response" do
          get :index, params: {email: "notreal@dontexist.com", key: key}
          expect(response.status).to eq(404)
        end
      end
    end

    context "with invalid API key" do
      it "returns the correct response" do
        get :index, params: {key: "wrongkey"}
        expect(response.status).to eq(422)
      end
    end
  end
end
