require "rails_helper"

describe Api::V1::DocusignController do
  it "should update the correct document signed at" do
    provider_uuid = SecureRandom.uuid
    document = create(:legal_document, provider_identifier: provider_uuid)
    signed_date = Time.now.strftime("%Y-%m-%d")
    payload = {
        "DocuSignEnvelopeInformation" => {
            "EnvelopeStatus" => {
                "Status" => "Completed",
                "RecipientStatuses" => {
                    "RecipientStatus" => {
                        "Signed" => signed_date
                    }
                },
            "EnvelopeID" => provider_uuid
            }
        }
    }

    allow(Hash).to receive(:from_xml).and_return(payload)

    post :update_document_status

    expect(document.reload.signed_at.strftime("%Y-%m-%d")).to eq(signed_date)
    expect(document.reload.completed?).to be_truthy
  end
end
