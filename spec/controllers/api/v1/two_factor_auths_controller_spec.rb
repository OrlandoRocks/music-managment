require "rails_helper"

describe Api::V1::TwoFactorAuthsController do
  before(:each) do
    allow(SECRETS).to receive(:[]).and_return(:default)
    allow(SECRETS).to receive(:[]).with("api_v1_secret_key").and_return("secretkey")
  end

  describe "POST #create" do

    context "with invalid API key" do
      it "returns 422" do
        post :create, params: { key: "bad_key" }
        expect(response.status).to eq(422)
      end
    end

    context "with valid API key" do
      let(:key) { "ABC123" }
      before(:each) do
        create(:api_key, key: Digest::SHA2.hexdigest(SECRETS["api_v1_secret_key"] + key))
        @person  = create(:person)
        @tfa     = create(:two_factor_auth, person: @person)
      end

      context "when it locates a two_factor_auth record" do
        context "and the status code = approved" do
          it "calls auth_success! on tfa" do
            expect_any_instance_of(TwoFactorAuth).to receive(:auth_success!)
            post :create, params: { key: key, status: "approved", authy_id: @tfa.authy_id }
            expect(response.status).to eq(200)
          end
        end

        context "and the status code = denied" do
          it "calls auth_failure! on tfa" do
            expect_any_instance_of(TwoFactorAuth).to receive(:auth_failure!)
            post :create, params: { key: key, status: "denied", authy_id: @tfa.authy_id  }
            expect(response.status).to eq(200)
          end
        end
      end

      context "when NO two_factor_auth record is found" do
        it "does NOT call auth_sucess! or auth_failure! on tfa" do
          expect_any_instance_of(TwoFactorAuth).to_not receive(:auth_success!)
          expect_any_instance_of(TwoFactorAuth).to_not receive(:auth_failure!)
          post :create, params: { key: key, status: "whatever", authy_id: "bad_id_67868" }
          expect(response.status).to eq(200)
        end
      end
    end
  end
end
