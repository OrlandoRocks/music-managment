require "rails_helper"

describe Api::V1::BaseController do
  controller(Api::V1::BaseController) do
    def index
      render json: { status: :successful }
    end
  end

  let(:key) { "ABC123" }

  before(:each) do
    allow(SECRETS).to receive(:[]).and_return(:default)
    allow(SECRETS).to receive(:[]).with("api_v1_secret_key").and_return("secretkey")
    create(:api_key, key: Digest::SHA2.hexdigest(SECRETS["api_v1_secret_key"] + key))
  end

  describe "#authenticate" do
    it "has http status 422 if not key present" do
      get :index
      expect(response.status).to eq 422
    end

    it "has http status 422 if key is incorrect" do
      get :index, params: { key: "someotherkey" }
      expect(response.status).to eq 422
    end

    it "has http status 200 if key is correct" do
      get :index, params: { key: key }
      expect(response.status).to eq 200
      expect(response.body).to eq({ status: :successful }.to_json)
    end
  end
end
