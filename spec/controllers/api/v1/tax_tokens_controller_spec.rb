require "rails_helper"

describe Api::V1::TaxTokensController do
  let(:person) { create(:person).tap { |person| person.tax_tokens.create(token: SecureRandom.hex, is_visible: true) } }
  let(:key)  { "ABC123" }

  before(:each) do
    allow(SECRETS).to receive(:[]).and_return(:default)
    allow(SECRETS).to receive(:[]).with("api_v1_secret_key").and_return("secretkey")
    create(:api_key, key: Digest::SHA2.hexdigest(SECRETS["api_v1_secret_key"] + key))
  end

  it "updates the tax form type of the person's tax token" do
    get :update, params: { key: key, TaxFormType: 1, apuid: person.id }
    expect(person.tax_tokens.last.tax_form).to eq("W8BEN")
  end

  it "updates the tax form of a person that has is_visible set to false" do
    person.tax_tokens.create(token: SecureRandom.hex, is_visible: false)
    get :update, params: { key: key, TaxFormType: 1, apuid: person.id }
    expect(person.tax_tokens.last.tax_form).to eq("W8BEN")
  end
end
