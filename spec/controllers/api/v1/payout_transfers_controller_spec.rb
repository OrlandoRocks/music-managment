require "rails_helper"

describe Api::V1::PayoutTransfersController do
  let(:key) { "ABC123" }

  before(:each) do
    allow(SECRETS).to receive(:[]).and_return(:default)
    allow(SECRETS).to receive(:[]).with("api_v1_secret_key").and_return("secretkey")
    create(:api_key, key: Digest::SHA2.hexdigest(SECRETS["api_v1_secret_key"] + key))
  end

  describe "#pending" do
    it "returns failure if payout transfer not found" do
      get :pending, params: { payout_id: 1, key: key }
      expect(response.status).to eq 422
      err_msg = "Payoneer IPCN call failed: Not able to find the payout transfer by identifier 1"
      expect(response.body).to eq({ message: err_msg}.to_json)
    end

    it "updates the provider status to pending if payout_id record is found" do
      payout_transfer = create(:approved_payout_transfer)
      get :pending, params: { payout_id: payout_transfer.client_reference_id, key: key }

      expect(payout_transfer.reload.provider_status).to eq PayoutTransfer::PENDING
      expect(response.status).to eq 200
      expect(response.body).to eq({message: "payout transfer marked pending"}.to_json)
    end
  end

  describe "#completed" do
    it "returns failure if payout transfer not found" do
      get :completed, params: { payout_id: 1, key: key }
      expect(response.status).to eq 422
      err_msg = "Payoneer IPCN call failed: Not able to find the payout transfer by identifier 1"
      expect(response.body).to eq({ message: err_msg}.to_json)
    end

    context "when payout transfer for given payout_id is found" do
      let!(:payout_transfer) { create(:approved_payout_transfer, :pending) }

      shared_examples "a found payout transfer" do
        it "returns ok" do
          get :completed, params: { payout_id: payout_transfer.client_reference_id, key: key }

          expect(response.status).to eq 200
          expect(response.body).to eq({ message: "payout transfer marked completed" }.to_json)
        end

        it "marks the payout transfer as 'completed'" do
          get :completed, params: { payout_id: payout_transfer.client_reference_id, key: key }

          expect(payout_transfer.reload.provider_status).to eq PayoutTransfer::COMPLETED
        end
      end

      context "when 'amount' is not sent to the webhook as an argument" do
        it_behaves_like "a found payout transfer"
      end

      context "when 'amount' is sent to the webhook as an argument (V4 only)" do
        before { allow(FeatureFlipper).to receive(:show_feature?).with(:use_payoneer_v4_api, any_args).and_return(true) }

        context "when given amount doesn't match amount in the payout transfer" do

          it "posts an Airbrake" do
            error_msg = "Payout Transfer Amount Mismatch"
            expect(Airbrake).to receive(:notify).with(Exception.new(error_msg), any_args)

            get :completed, params: {
              payout_id: payout_transfer.client_reference_id,
              key: key,
              amount: payout_transfer.amount_to_transfer.to_d + 5
            }
          end

          it_behaves_like "a found payout transfer"
        end

        context "when the given amount matches the amount in the payout transfer" do
          it_behaves_like "a found payout transfer"
        end
      end
    end
  end

  describe "#cancelled" do
    context "if payout transfer is not found" do
      it "returns failure if payout transfer not found" do
        get :completed, params: { payout_id: 1, key: key }
        expect(response.status).to eq 422
        err_msg = "Payoneer IPCN call failed: Not able to find the payout transfer by identifier 1"
        expect(response.body).to eq({ message: err_msg}.to_json)
      end
    end

    context "if the payout transfer is already marked cancelled" do
      it "does not update the status or issue a refund" do
        payout_transfer = create(:payout_transfer, description: nil, provider_status: PayoutTransfer::CANCELLED, amount_cents: 50000)
        old_balance = payout_transfer.person_balance.balance.to_i

        expect {
          get :cancelled, params: { payout_id: payout_transfer.client_reference_id, key: key, ReasonCode: 500, ReasonDescription: "something went wrong" }
        }.to change { PayoutTransaction.count }.by(0)
        current_balance = payout_transfer.person_balance.reload.balance.to_i
        expect(current_balance).to eq(old_balance)
        expect(response.status).to eq 400
        expect(response.body).to eq({ message: "Payoneer IPCN call failed: payment #{payout_transfer.client_reference_id} already marked as cancelled/declined" }.to_json)
      end
    end

    context "if the payout transfer is already marked declined" do
      it "does not update the status or issue a refund" do
        payout_transfer = create(:payout_transfer, description: nil, provider_status: PayoutTransfer::DECLINED, amount_cents: 50000)
        old_balance = payout_transfer.person_balance.balance.to_i

        expect {
          get :cancelled, params: { payout_id: payout_transfer.client_reference_id, key: key, ReasonCode: 500, ReasonDescription: "something went wrong" }
        }.to change { PayoutTransaction.count }.by(0)
        current_balance = payout_transfer.person_balance.reload.balance.to_i
        expect(current_balance).to eq(old_balance)
        expect(response.status).to eq 400
        expect(response.body).to eq({ message: "Payoneer IPCN call failed: payment #{payout_transfer.client_reference_id} already marked as cancelled/declined" }.to_json)
      end
    end

    context "if payout_id record is found" do
      it "updates the payout_transfer to cancelled and updates balances" do
        payout_transfer = create(:approved_payout_transfer, description: nil)
        expect(payout_transfer.code.blank?).to be_truthy
        expect(payout_transfer.description.blank?).to be_truthy
        old_balance = payout_transfer.person_balance.balance.to_i

        expect {
          get :cancelled, params: { payout_id: payout_transfer.client_reference_id, key: key, ReasonCode: 500, ReasonDescription: "something went wrong" }
        }.to change { PayoutTransaction.count }.by(1)
        expect(payout_transfer.reload.provider_status).to eq PayoutTransfer::CANCELLED
        current_balance = payout_transfer.person_balance.balance.to_i
        expect(current_balance).to eq(old_balance + payout_transfer.amount_to_transfer.to_f)
        expect(response.status).to eq 200
        expect(response.body).to eq({ message: "payout transfer marked cancelled" }.to_json)
        expect(payout_transfer.reload.code).to eq(500)
        expect(payout_transfer.reload.description).to eq("something went wrong")
      end
    end

    context "if payout_id record is not found" do
      it "does not update the payout transfer" do
        error_msg = "Payoneer IPCN call failed: Not able to find the payout transfer by identifier 9567"
        expect(Airbrake).to receive(:notify).with(Exception.new(error_msg), {})

        get :cancelled, params: { payout_id: 9567, key: key, ReasonCode: 500, ReasonDescription: "something went wrong" }

        expect(response.status).to eq(422)
        expect(response.body).to eq({ message: error_msg }.to_json)
      end
    end
  end

end
