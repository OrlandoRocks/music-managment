require "rails_helper"

describe Api::V1::AffiliateReportsController do
  let(:key) { "super_secret_key" }

  before(:each) do
    allow(SECRETS)
      .to receive(:[])
      .and_return(:default)

    allow(SECRETS)
      .to receive(:[])
      .with("api_v1_secret_key")
      .and_return("secretkey")

    create(
      :api_key,
      key: Digest::SHA2.hexdigest(SECRETS["api_v1_secret_key"] + key)
    )
  end

  describe "#create" do
    let(:user) { create(:person) }

    it "creates an affiliate report" do
      @request.env["CONTENT_TYPE"] = "application/json"

      post :create, params: { key: key, affiliate_report: {
        person_id: user.id,
        person_email: user.email,
        product_name: "Product",
        units_sold: 100,
        sale_amount: 999,
        sale_currency: "USD",
        date_paid: Date.today.strftime("%Y-%m-%d"),
        data: {
          foo: "bar",
          baz: {
            quux: "corge"
          }
        }
      }}

      affiliate_report = AffiliateReport.where(person_id: user.id).first

      expect(response.status).to eq(201)
      expect(affiliate_report).to be_present
      expect(affiliate_report.data).to eq(
        "foo" => "bar",
        "baz" => {
          "quux" => "corge"
        }
      )
    end

    it "does not create an affiliate report with required params not given" do
      @request.env["CONTENT_TYPE"] = "application/json"

      post :create, params: { key: key, affiliate_report: {
        person_id: user.id,
        product_name: "Product",
        # person_email: "intentionally left out",
        units_sold: 100,
        sale_amount: 999,
        sale_currency: "USD",
        date_paid: Date.today.strftime("%Y-%m-%d"),
        data: {
          foo: "bar",
          baz: {
            quux: "corge"
          }
        }
      }}

      affiliate_report = AffiliateReport.where(person_id: user.id).first

      expect(response.status).to eq(422)
      response_errors = JSON.parse(response.body)["errors"]
      expect(response_errors["person_email"].first).to eq("can't be blank")
      expect(affiliate_report).not_to be_present
    end

    it "returns a 422 when the request is not of type application/json" do
      @request.env["CONTENT_TYPE"] = "text/html"

      post :create, params: { key: key, affiliate_report: {
        person_id: user.id,
        product_name: "Product",
        units_sold: 100,
        sale_amount: 999,
        sale_currency: "USD",
        date_paid: Date.today.strftime("%Y-%m-%d"),
        data: {
          foo: "bar",
          baz: {
            quux: "corge"
          }
        }
      }}

      expect(response.status).to eq(422)
    end
  end
end
