require "rails_helper"

describe Api::V1::BraintreeController do
  describe "#account_updater_report" do

    it "should raise error if the update kind is not account updater daily report" do
      webhook_notification_mock = double(:webhook_notification, kind: 'somekind', report_url: 'https://sampleurl')
      allow_any_instance_of(Braintree::Gateway)
          .to receive_message_chain(:webhook_notification, :parse)
                  .and_return(webhook_notification_mock)

      expect(Airbrake).to receive(:notify)
      post :account_updater_report
    end

    it "should download and process the csv for account updater daily report" do
      report_mock = double(:account_updater_daily_report, report_url: 'https://sampleurl')
      webhook_notification_mock = double(:webhook_notification,
                                         kind: Braintree::WebhookNotification::Kind::AccountUpdaterDailyReport,
                                         account_updater_daily_report: report_mock)
      allow_any_instance_of(Braintree::Gateway)
          .to receive_message_chain(:webhook_notification, :parse)
                  .and_return(webhook_notification_mock)
      expect(Braintree::AccountUpdaterWorker).to receive(:perform_async).with(report_mock.report_url)

      post :account_updater_report
    end
  end
end