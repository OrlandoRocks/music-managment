require "rails_helper"

describe Api::V1::TaxFormsController do
  let(:person) { create(:person) }
  let(:key) { "ABC123" }

  before(:each) do
    allow(SECRETS).to receive(:[]).and_return(:default)
    allow(SECRETS).to receive(:[]).with("api_v1_secret_key").and_return("secretkey")
    create(:api_key, key: Digest::SHA2.hexdigest(SECRETS["api_v1_secret_key"] + key))
  end

  describe "#create" do
    it "returns 422 if no api key" do
      params = {}

      post :create, params: params

      expect(response).to have_http_status 422
    end

    it "returns not_found status if payee is not found" do
      params = { payeeid: "non-existent-payee-id", key: key, provider: PayoutProvider::PAYONEER }
      post :create, params: params
      expect(JSON.parse(response.body)).to eq({ "status" => "not_found", "message" => "not found" })
    end

    it "returns unprocessable_entity status for unknown TaxFormType" do
      payout_provider = create(:payout_provider, person: person)
      params = { payeeid: payout_provider.client_payee_id, TaxFormType: "8", key: key, provider: PayoutProvider::PAYONEER }

      expect(Airbrake).to receive(:notify).with(
        "Unknown TaxFormType(8) received from TaxformSubmitted webhook",
        { person_id: person.id, TaxFormType: "8" }
      )

      post :create, params: params

      expect(JSON.parse(response.body)).to eq(
        {
          "status" => "unprocessable_entity",
          "message" => "Unknown TaxFormType(8) received from TaxformSubmitted webhook"
        }
      )
    end

    it "returns 200 with success message if the tax-form already exists" do
      payout_provider = create(:payout_provider, person: person)
      tax_form = create(:tax_form, person: person)
      expect(person.tax_forms.size).to eq(1)

      params = { payeeid: payout_provider.client_payee_id, TaxFormType: "2", key: key, provider: PayoutProvider::PAYONEER }
      post :create, params: params

      expect(response).to have_http_status 200
      expect(person.tax_forms.reload.size).to eq(1)
      expect(person.tax_forms[0].id).to eq(tax_form.id)
      expect(response.body).to eq({ status: :created, message: "Tax form created" }.to_json)
    end

    context "when secondary_taxform_submission feature flag is set" do
      before do
        allow(Payoneer::FeatureService).to receive(:secondary_taxform_submission?).with(person).and_return(true)
      end

      it "allows creation of multiple tax forms of same type for the same user" do
        payout_provider = create(:payout_provider, person: person)

        expect(FetchTaxFormsWorker).to receive(:perform_async).with(person.id)
        params = { payeeid: payout_provider.client_payee_id, TaxFormType: "2", key: key, provider: PayoutProvider::PAYONEER }
        post :create, params: params

        expect(FetchTaxFormsWorker).to receive(:perform_async).with(person.id)
        params = { payeeid: payout_provider.client_payee_id, TaxFormType: "2", key: key, provider: PayoutProvider::PAYONEER }
        post :create, params: params

        expect(response).to have_http_status 200
        expect(response.body).to eq({ status: :created, message: "Tax form created" }.to_json)
      end
    end

    it "returns 200 and creates a new tax form" do
      expect(person.tax_forms.size).to eq(0)
      payout_provider = create(:payout_provider, person: person)
      
      expect(FetchTaxFormsWorker).to receive(:perform_async).with(person.id)

      params = { payeeid: payout_provider.client_payee_id, TaxFormType: "2", key: key }
      post :create, params: params

      expect(response).to have_http_status 200
      expect(response.body).to eq({ status: :created, message: "Tax form created" }.to_json)
    end
  end
end
