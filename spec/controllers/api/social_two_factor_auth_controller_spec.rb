require "rails_helper"

describe Api::SocialTwoFactorAuthController do
  before(:each) do
    allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
    allow_any_instance_of(TwoFactorAuth::ApiClient).to receive(:log).and_return(true)
  end

  describe "#generate_auth_code" do
    let(:person) { create(:person, :with_two_factor_auth) }
    let(:generate_auth_code_params) do
      {
        person_id: person.id,
        api_key: API_CONFIG["API_KEY"],
        alternate_method: false,
        source: "tc_social_web",
        resend: false
      }
    end
    context "when an auth event has happened less than a minute ago" do
      # FIXME: expected `TwoFactorAuthEvent.count` not to have changed, but did change from 1 to 2
      xit "does not send a auth code" do
        create(
          :two_factor_auth_event, two_factor_auth: person.two_factor_auth,
                                  action: "sign_in",
                                  created_at: 20.seconds.ago
        )
        expect {
          post :generate_auth_code, params: generate_auth_code_params
        }.not_to change(TwoFactorAuthEvent.count)
      end
    end
    context "when an auth event has happened over a minute ago" do
      it "does send an auth code" do
        create(
          :two_factor_auth_event, two_factor_auth: person.two_factor_auth,
                                  action: "sign_in",
                                  created_at: 2.days.ago
        )
        count = TwoFactorAuthEvent.count
        post :generate_auth_code, params: generate_auth_code_params
        expect(response.status).to eq 200
        expect(TwoFactorAuthEvent.count).to eq(count + 1)
      end
    end
  end

  describe "#validate_auth_code" do
    context "when auth code is denied" do
      it "does not update tfa auth event action" do
        person = create(:person)
        tfa = create(:two_factor_auth, person: person)
        tfa_event = create(
          :two_factor_auth_event, two_factor_auth: tfa,
                                  action: "sign_in",
                                  created_at: 20.seconds.ago
        )
        action = tfa_event.action
        allow_any_instance_of(TwoFactorAuth::ApiClient).to receive(:submit_auth_code).and_return(double(successful?: false, error_message: :invalid_verification_code))
        post :validate_auth_code, params: {
          person_id: person.id,
          api_key: API_CONFIG["API_KEY"],
          tfa_event_id: tfa_event.id,
          auth_code: "12345"
        }
        expect(response.status).to eq 200
        expect(tfa_event.action).to eq action
      end
    end

    context "when auth code is successfully validated" do
      let(:tfa_client_double) { double }
      let(:person) { create(:person, country_website_id: CountryWebsite::UNITED_STATES, country: "US") }
      let(:tfa) { create(:two_factor_auth, person: person) }
      let(:tfa_event) do
        create(
          :two_factor_auth_event,
          two_factor_auth: tfa,
          action: "sign_in",
          created_at: 20.seconds.ago
        )
      end

      before do
        allow(tfa_client_double).to receive(:submit_auth_code) {
          dummy_auth =
            Struct.new(nil) do
              def successful?
                true
              end
            end
          dummy_auth.new
        }
        allow(TwoFactorAuth::ApiClient).to receive(:new).and_return(tfa_client_double)
      end

      subject do
        post :validate_auth_code, params: {
          person_id: person.id,
          api_key: API_CONFIG["API_KEY"],
          tfa_event_id: tfa_event.id,
          auth_code: "12345"
        }
      end

      it "update tfa auth event action to 'complete'" do
        subject
        expect(response.status).to eq 200
        expect(tfa_event.reload.action).to eq("complete")
      end

      it "returns serialized Person info in response" do
        subject
        expected_response = {
          status: "success",
          data: {
            person_id: person.id,
            email: person.email,
            name: person.name,
            plan: nil,
            balance: "0.0",
            country: 233,
            country_website: COUNTRY_URLS["US"],
            plan_expires_at: "",
            grace_period_ends_on: "",
            payment_channel: nil
          },
          message: "api.tc.message.auth_code.valid_code"
        }
        expect(response.status).to eq 200
        expect(response.body).to eq(expected_response.to_json)
      end
    end
  end
end
