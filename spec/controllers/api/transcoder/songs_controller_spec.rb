require "rails_helper"

describe Api::Transcoder::SongsController do
  let!(:song) { create(:song) }

  ENV['ASSETS_BACKFILL_DESTINATION_BUCKET'] = 'destination_bucket'

  describe '#set_asset_data' do
    it 'should create s3 asset and update song duration and s3_flac_asset if state is completed' do
      post :set_asset_data, params: {
        state: "COMPLETED",
        songId: song.id,
        outputs: [
          {
            duration: 431,
            key: "destination_key"
          }
        ]
      }
      expect(JSON.parse(response.body)["status"]).to eq("successful")
      song.reload
      expect(song.duration).to eq(431)
      expect(song.s3_flac_asset.key).to eq('destination_key')
      expect(song.s3_flac_asset.bucket).to eq('destination_bucket')
    end

    it 'should not update duration and s3_flac_asset if state is not completed' do
      post :set_asset_data, params: {
        state: "ERROR",
        songId: song.id,
        outputs: [
          {
            duration: 431,
            key: "destination_key"
          }
        ]
      }
      expect(JSON.parse(response.body)["status"]).to eq("successful")
      song.reload
      expect(song.duration).to eq(nil)
      expect(song.s3_flac_asset).to eq(nil)
    end
  end

  describe "#set_streaming_asset" do
    it "should create s3_streaming_asset" do
      post :set_streaming_asset, params: {
        state: "COMPLETED",
        songId: song.id,
        outputs: [
          {
            duration: 431,
            key: "streaming_destination_key"
          }
        ]
      }
      expect(JSON.parse(response.body)["status"]).to eq("successful")
      song.reload
      expect(song.s3_streaming_asset.key).to eq('streaming_destination_key')
      expect(song.s3_streaming_asset.bucket).to eq('destination_bucket')
    end

    it "throw error and not save s3_streaming asset if status is not completed" do
      post :set_streaming_asset, params: {
        state: "ERROR",
        songId: song.id,
        outputs: [
          {
            duration: 431,
            key: "streaming_destination_key"
          }
        ]
      }
      expect(JSON.parse(response.body)["status"]).to eq("unsuccessful")
      song.reload
      expect(song.s3_streaming_asset).to be nil
    end
  end
end
