require "rails_helper"

describe Api::Transcoder::SoundoutReportsController do
  let!(:soundout_report) { create(:soundout_report, :with_track) }

  describe '#complete_order' do
    it 'should call the Soundout OrderWorker if state is completed' do
      expect(Soundout::OrderWorker).to receive(:perform_async).with(soundout_report.id.to_s)

      post :complete_order,
           params: {
             state: "COMPLETED",
             id: soundout_report.id
           }

      expect(JSON.parse(response.body)["status"]).to eq("successful")
      expect(response).to have_http_status(:ok)
    end

    it 'should not update duration and s3_flac_asset if state is not completed' do
      expect(Soundout::OrderWorker).to_not receive(:perform_async).with(soundout_report.id)

      post :complete_order,
           params: {
             state: "ERROR",
             id: soundout_report.id
           }

      expect(JSON.parse(response.body)["status"]).to eq("unsuccessful")
      expect(response).to have_http_status(422)
    end
  end
end
