require 'rails_helper'

describe Api::ArtistDiscovery::SongsController, type: :controller do
  describe "#fetch" do
    context "invalid user" do
      it "renders a failure response" do
        post :fetch, params: {api_key: ENV['SONGS_API_KEY'] }
        expect(response.status).to eq(200)
        expect(response.body).to eq({ status: "failure", message: ["api.tc.message.invalid_email"] }.to_json)
      end
    end

    context "valid user with valid songs" do
      it "renders a 200 json response" do
        person = create(:person, :with_tc_social)
        album = create(:album, :with_uploaded_songs_and_artwork_and_esi, person: person)
        distribution = create(:distribution, :delivered_to_spotify)
        distribution.petri_bundle.update_attribute(:album, album)
        params = { page: 1, per_page: 10,
                   email: person.email,
                   api_key: ENV['SONGS_API_KEY']}
        post :fetch, params: params
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)).to be_a(Hash)
        expect((JSON.parse(response.body)['data']).count).to eq 2
      end
    end

    context "valid user with no releases" do
      it "renders a 200 json response" do
        person = create(:person, :with_tc_social)
        params = { page: 1, per_page: 10,
                   email: person.email,
                   api_key: ENV['SONGS_API_KEY'] }
        post :fetch, params: params
        expect(response.status).to eq(200)
        expect(response.body).to eq({ status: "failure", message: ["api.tc.message.user_no_albums"] }.to_json)
      end
    end
  end
end
