require "rails_helper"

describe Api::SystemstatusController, type: :controller do
  it "transaction_status is unauthorized without an api key" do
    get :transaction_status
    expect(response.status).to eq (401)
  end

  it "transaction_status returns a 'score'" do
    get :transaction_status, params: { api_key: API_CONFIG["API_KEY"] }
    expect(response.status).to eq (200)
    expect(response_body).to have_key("score")
  end

  def response_body
    JSON.parse response.body
  end
end
