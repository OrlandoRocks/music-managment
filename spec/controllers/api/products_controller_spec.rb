require 'rails_helper'

RSpec.describe Api::ProductsController, type: :controller do
  context "With a valid Product add to cart method" do
    it "Creates a subscription product purchase for a person" do
      person = people(:tunecore_customer_with_a_finalized_and_ready_to_go_album)
      client_application = ClientApplication.where(name: "tc_social").first
      Oauth2Token.create!(user: person, client_application: client_application, scope: nil)
      request.headers["Authorization"] = "Token token=#{SessionEncryptionEngine.encrypt64(person.tc_social_token)}"
      count = person.purchases.count
      params = {
                product_type: "social", 
                name: "tc_social_monthly",
                api_key: API_CONFIG["API_KEY"]
               }
      post :social_add_to_cart, params: params
      expect(JSON.parse(response.body)["status"]).to eq("success")
      person.reload
      expect(person.purchases.count).to eq(count+1)
    end
  end

  context "With RF video download Product add to cart method" do
    it "Creates a product purchase for a person" do
      product = FactoryBot.create(:product, :rf_video_download)
      person = people(:tunecore_customer_with_a_finalized_and_ready_to_go_album)
      client_application = ClientApplication.where(name: "tc_social").first
      Oauth2Token.create!(user: person, client_application: client_application, scope: nil)
      request.headers["Authorization"] = "Token token=#{SessionEncryptionEngine.encrypt64(person.tc_social_token)}"
      params = {
                product_type: "rf_video_download",
                name: "rf_video_download",
                api_key: API_CONFIG["API_KEY"]
               }
      expect { post :social_add_to_cart, params: params }.to change { person.purchases.count }.by(1)
    end
  end

  context "With an invalid Person add to cart method" do
    it "Throws an Invalid person for a product purchase for a person" do
      person = people(:tunecore_customer_brand_new)
      params = {
                product_type: "social", 
                name: "tc_social_monthly",
                api_key: API_CONFIG["API_KEY"]
               }
      post :social_add_to_cart, params: params
      expect(JSON.parse(response.body)["status"]).to eq("failure")
    end
  end

  context "With an invalid Product Type for add to cart method" do
    it "Throws an Invalid product name for a product purchase for a person" do
      person = FactoryBot.create(:person, :with_tc_social)
      request.headers["Authorization"] = "Token token=#{SessionEncryptionEngine.encrypt64(person.tc_social_token)}"
      params = {
                product_type: "test", 
                name: "test",
                api_key: API_CONFIG["API_KEY"]
               }
      post :social_add_to_cart, params: params
      expect(JSON.parse(response.body)["status"]).to eq("failure")
      expect(JSON.parse(response.body)["message"]).to eq(["Invalid product name"])
    end
  end

end
