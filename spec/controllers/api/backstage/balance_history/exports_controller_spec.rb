require "rails_helper"

describe Api::Backstage::BalanceHistory::ExportsController, type: :controller do
  describe '#create' do

    let(:person) { create(:person) }
    let(:params) { { export_form: { num_pages: 100 } } }
    let(:id_array) { [] }
    let(:csv_header_string) { PersonTransactionExporter::CSV_HEADERS.join(",") }

    before(:each) do
      5.times do
        txn = create(:person_transaction, person: person)
        id_array << txn.id
      end
      params[:export_form][:txn_ids] = id_array
      login_as(person)
    end

    context "input" do
      it "loads a specific set of txn_ids into a PersonTransaction::ExportForm" do
        id_array.shift
        params[:export_form][:txn_ids] = id_array

        args = {
          transactions: BalanceHistory::TransactionsQueryBuilder.build(params[:export_form].merge(person: person)),
          person: person
        }

        form = PersonTransaction::ExportForm.new(args)

        expect(PersonTransaction::ExportForm).to receive(:new).with(args).and_return(form)

        expect(form).to receive(:save).and_return(true)
        post :create, params: params
      end
    end

    context "response" do
      before(:each) do
        Timecop.freeze
        post :create, params: params
      end

      after(:each) do
        Timecop.return
      end

      it "renders a string-encoded CSV" do
        expect(response.body).to be_a String
        expect(response.body).to include(*person.person_transactions.map(&:comment))
      end

      it "returns data for all matching transactions" do
        csv_array = response.body.split(PersonTransactionExporter::ROW_SEPARATOR)
        expect(csv_array.length).to eq(person.person_transactions.length + 1)
      end

      it "returns the correct set of headers in the CSV" do
        data_headers = response.body.split(PersonTransactionExporter::ROW_SEPARATOR).shift
        expect(data_headers).to eq csv_header_string
      end

      context "response headers" do
        let (:time) { I18n.localize(Time.now, format: :underscore) }
        let(:res_header) { response.header }

        it "has a timestamped filename attachment" do
          file_header = "attachment; filename=tunecore-transactions_#{time}.csv"
          expect(res_header["Content-Disposition"]).to eq file_header
        end

        it "is encoded as binary for transit" do
          expect(res_header["Content-Transfer-Encoding"]).to eq 'binary'
        end

        it "indicates to the browser that it should be interpreted as a CSV" do
          expect(res_header["Content-Type"]).to eq 'text/csv'
        end

        it "uses private Cache-Control " do
          expect(res_header["Cache-Control"]).to eq "private"
        end
      end
    end
  end
end
