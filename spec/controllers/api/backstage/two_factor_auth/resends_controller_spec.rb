require "rails_helper"

describe Api::Backstage::TwoFactorAuth::ResendsController do
  let(:person) { create(:person, :with_two_factor_auth) }

  before(:each) do
    allow_any_instance_of(TwoFactorAuth::ApiClient).to receive(:log).and_return(true)
  end

  describe "#create" do
    it "redirects to login if person is not logged in" do
      post :create, xhr: true
      expect(response).to redirect_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
    end

    it "calls the two factor auth client" do
      login_as(person)
      fake_api_client = double(request_authorization: true)
      allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
      allow(FeatureFlipper).to receive(:show_feature?).with(:verify_2fa, current_user).and_return(true)
      expect(TwoFactorAuth::ApiClient).to receive(:new).and_return(fake_api_client)
      expect(fake_api_client).to receive(:request_authorization)

      post :create, xhr: true

      expect(response.status).to eq 200
    end

    context "when the ajax feature flag is enabled" do
      context "when an auth event has happened less than a minute ago" do
        it "does not send an auth code" do
          login_as(person)
          Timecop.freeze
          resend_event = create(:two_factor_auth_event, two_factor_auth: person.two_factor_auth, action: "backstage_code_resend", created_at: 20.seconds.ago)

          allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
          allow(FeatureFlipper).to receive(:show_feature?).with(:two_factor_auth_ajax, person).and_return(true)
          allow(FeatureFlipper).to receive(:show_feature?).with(:verify_2fa, current_user).and_return(true)
          count = TwoFactorAuthEvent.count

          post :create, xhr: true

          expect(response.status).to eq 200
          expect(TwoFactorAuthEvent.count).to eq(count)
          Timecop.return
        end
      end

      context "when an auth event has happened over a minute ago" do
        it "does send an auth code" do
          login_as(person)
          Timecop.freeze
          resend_event = create(:two_factor_auth_event, two_factor_auth: person.two_factor_auth, action: "backstage_code_resend", created_at: 2.days.ago)

          allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
          allow(FeatureFlipper).to receive(:show_feature?).with(:two_factor_auth_ajax, person).and_return(true)
          allow(FeatureFlipper).to receive(:show_feature?).with(:verify_2fa, current_user).and_return(true)
          count = TwoFactorAuthEvent.count

          fake_api_client = double(request_authorization: true)
          expect(TwoFactorAuth::ApiClient).to receive(:new).and_return(fake_api_client)
          expect(fake_api_client).to receive(:request_authorization)

          post :create, xhr: true

          expect(response.status).to eq 200
          expect(TwoFactorAuthEvent.count).to eq(count + 1)
          Timecop.return
        end
      end

      context "when the user has exceeded their daily 2FA resends limit" do
        it "does not send an auth code" do
          login_as(person)
          Timecop.freeze
          3.times do
            create(:two_factor_auth_event, two_factor_auth: person.two_factor_auth, action: "back_stage_code_resend", type: "authentication", page: "account_settings", created_at: 1.hour.ago)
          end

          allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
          allow(FeatureFlipper).to receive(:show_feature?).with(:verify_2fa, current_user).and_return(true)
          count = TwoFactorAuthEvent.count

          params = {page: "authentication"}
          post :create, xhr: true, params: params

          expect(response.status).to eq 200
          expect(TwoFactorAuthEvent.count).to eq(count)
          expect(JSON.parse(response.body).fetch("exceeds_daily_resends")).to be true
          Timecop.return
        end
      end
    end
  end
end
