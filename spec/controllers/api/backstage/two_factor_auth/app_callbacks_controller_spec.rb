require "rails_helper"

describe Api::Backstage::TwoFactorAuth::AppCallbacksController do
  let(:person) { create(:person, :with_two_factor_auth) }

  before(:each) do
    login_as(person)
  end

  describe "#show" do
    it "returns successful status if authenticated" do
      person.two_factor_auth.update(last_verified_at: 1.minute.ago)

      get :show, xhr: true

      expect(response.status).to eq 200
      expect(JSON.parse(response.body)["status"]).to eq "successful"
    end

    it "returns unsuccessful status if outsite auth window" do
      person.two_factor_auth.update(last_verified_at: 10.minute.ago)

      get :show, xhr: true

      expect(response.status).to eq 200
      expect(JSON.parse(response.body)["status"]).to eq "unsuccessful"
    end
  end
end
