require "rails_helper"
include ActiveSupport::Testing::TimeHelpers

describe Api::Backstage::TwoFactorAuth::PromptsController do
  let(:person) { create(:person) }

  describe "#update" do
    before do
      allow(subject).to receive(:current_user) { person }
      @tfa_prompt = create(:two_factor_auth_prompt, person: person)
      travel_to Time.local(2020)
    end

    after do
      travel_back
    end

    it "dismisses the prompt" do
      put :update, params: { id: @tfa_prompt.id }
      expect(@tfa_prompt.reload.dismissed_count).to eq(2)
      expect(@tfa_prompt.prompt_at.to_date).to eq(30.days.from_now.to_date)
    end
  end
end
