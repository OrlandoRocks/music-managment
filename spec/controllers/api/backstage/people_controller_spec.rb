require "rails_helper"

describe Api::Backstage::PeopleController do
  describe "#update_preference" do
    let(:person) { create(:person) }
    let(:language_record)  { create(:country_website_language) }
    let(:params) do
      {
        language: {
          locale: language_record.yml_languages
        }
      }
    end

    it "updates language preference" do
      login_as(person)

      put :update_preference, params: params, format: :json
      expect(person.country_website_language_id).to eq(language_record.id)
    end
  end
end
