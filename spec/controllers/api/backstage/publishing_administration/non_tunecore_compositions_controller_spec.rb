require "rails_helper"

describe Api::Backstage::PublishingAdministration::NonTunecoreCompositionsController do
  describe "#create" do
    let(:person) { create(:person) }

    before :each do
      login_as(person)
      allow(MailerWorker).to receive(:perform_async).and_return true
    end

    it "invokes the NonTunecorePublishingCompositionForm" do
      publishing_composer = create(:publishing_composer, person: person)
      publishing_composer.person.update(account: publishing_composer.account)

      params = {
        non_tunecore_composition: {
          title: "Song Title ",
          artist_name: "Performing Artist ",
          album_name: "Album Name ",
          isrc_number: "123456789",
          release_date: "01/01/2018",
          percent: "75"
        },
        composer_id: publishing_composer.id
      }.with_indifferent_access

      ntc_composition_params = params[:non_tunecore_composition].merge(
        person_id: person.id.to_s,
        composer_id: publishing_composer.id.to_s,
        title: "Song Title",
        artist_name: "Performing Artist",
        album_name: "Album Name",
      )

      expect(PublishingAdministration::NonTunecorePublishingCompositionForm).to receive(:new)
        .with(ntc_composition_params).and_call_original

      post :create, params: params
    end

    it "returns a JSON response" do
      publishing_composer = create(:publishing_composer, person: person)
      publishing_composer.person.update_attributes(account: publishing_composer.account)

      params = {
        non_tunecore_composition: {
          title: "Song Title",
          artist_name: "Performing Artist",
          album_name: "Album Name ",
          isrc_number: "123456789",
          release_date: "01/01/2018",
          percent: "75"
        },
        composer_id: publishing_composer.id
      }.with_indifferent_access

      ntc_composition_params = params[:non_tunecore_composition].merge!(
        person_id:   person.id.to_s,
        composer_id: publishing_composer.id.to_s
      )

      ntc_composition_form = PublishingAdministration::NonTunecorePublishingCompositionForm.new(ntc_composition_params)
      ntc_composition_form.save

      allow(PublishingAdministration::NonTunecorePublishingCompositionForm).to receive(:new) { ntc_composition_form }
      allow(ntc_composition_form).to receive(:save) { true }

      post :create, params: params

      expect(response.body).to eq(ntc_composition_form.response_body.to_json)
    end
  end
end
