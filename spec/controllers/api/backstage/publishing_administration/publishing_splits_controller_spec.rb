require "rails_helper"

describe Api::Backstage::PublishingAdministration::PublishingSplitsController do
  let(:person)  { create(:person) }
  let(:account) { create(:account, person: person) }

  before :each do
    login_as(person)
  end

  describe "#create" do
    context "data is invalid" do
      it "does not save the publishing_composition_splits" do
        publishing_composer    = create(:publishing_composer, first_name: "Mary", last_name: "Berry", account: account, person: account.person)
        publishing_composition = create(:publishing_composition, account: account)
        params      = {
          composer_id:      publishing_composer.id,
          composition_id:   publishing_composition.id,
          composer_share:   100.to_d,
          cowriter_params:  [{ first_name: "Paul", last_name: "Hollywood", cowriter_share: 50.to_d }]
        }

        post :create, params: params
        expect(publishing_composition.publishing_composition_splits.present?).to be false
      end

      it "returns a JSON response" do
        publishing_composer    = create(:publishing_composer, first_name: "Mary", last_name: "Berry", account: account, person: account.person)
        publishing_composition = create(:publishing_composition, account: account)
        params      = {
          composer_id:      publishing_composer.id,
          composition_id:   publishing_composition.id,
          composer_share:   100.to_d,
          cowriter_params:  [{ first_name: "Paul", last_name: "Hollywood", cowriter_share: 50.to_d }]
        }

        post :create, params: params
        expect(JSON.parse(response.body)["errors"].present?).to be true
      end
    end

    context "when a cowriting publishing_composer is created" do
      it "removes trailing whitespaces characters from the cowriters first and last names"  do
        publishing_composer    = create(:publishing_composer, first_name: "Mary", last_name: "Berry", account: account, person: account.person)
        publishing_composition = create(:publishing_composition, account: account)
        params      = {
          composer_id:      publishing_composer.id,
          composition_id:   publishing_composition.id,
          cowriter_params:  [{ first_name: "Paul  ", last_name: "Hollywood   ", cowriter_share: 50 }]
        }

        expected_params = {
          person:          person,
          composer:        publishing_composer,
          composition:     publishing_composition,
          composer_share:  0.0,
          cowriter_params: [{ first_name: "Paul", last_name: "Hollywood", cowriter_share: "50" }],
          public_domain: nil,
          translated_name: nil
        }

        expect(PublishingAdministration::PublishingCompositionSplitsForm).to receive(:new)
          .with(expected_params).and_call_original
        post :create, params: params
      end
    end

    context "when the publishing_composition does not have cowriting publishing_composers" do
      it "sets the cowriter params to an empty array" do
        publishing_composer    = create(:publishing_composer, first_name: "Mary", last_name: "Berry", account: account, person: account.person)
        publishing_composition = create(:publishing_composition, account: account)
        params      = {
          composer_id:     publishing_composer.id,
          composition_id:  publishing_composition.id,
          cowriter_params: nil
        }

        expected_params = {
          person:          person,
          composer:        publishing_composer,
          composition:     publishing_composition,
          composer_share:  0.0,
          cowriter_params: [],
          public_domain: nil,
          translated_name: nil
        }

        expect(PublishingAdministration::PublishingCompositionSplitsForm).to receive(:new)
          .with(expected_params).and_call_original
        post :create, params: params
      end
    end

    it "saves the publishing_composition_splits" do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
      composer    = create(:publishing_composer, first_name: "Mary", last_name: "Berry", account: account, person: account.person)
      composition = create(:publishing_composition, account: account)
      params      = {
        composer_id:      composer.id,
        composition_id:   composition.id,
        composer_share:   50.to_d,
        cowriter_params:  [{ first_name: "Paul", last_name: "Hollywood", cowriter_share: 50.to_d }]
      }

      post :create, params: params
      expect(composition.publishing_composition_splits.present?).to be true
    end

    it "returns a JSON response" do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
      composer    = create(:publishing_composer, first_name: "Mary", last_name: "Berry", account: account, person: account.person)
      composition = create(:publishing_composition, account: account)
      cowriter    = create(:publishing_composer, first_name: "Paul", last_name: "Hollywood", is_primary_composer: false, is_unknown: false, account: account, person: account.person)
      params      = {
        composer_id:      composer.id,
        composition_id:   composition.id,
        composer_share:   50.to_d,
        cowriter_params:  [{ first_name: "Paul", last_name: "Hollywood", cowriter_share: 50.to_d }],
        translated_name:  ""
      }

      split_created_time = Time.current

      Timecop.freeze(split_created_time) do
        post :create, params: params

        cowriter_split = cowriter.reload.publishing_composition_splits.first

        json_response = JSON.parse({
          composition_id: composition.id.to_s,
          composer_percent: 50.to_d,
          cowriter_percent: 50.to_d,
          cowriters: [
            {
              id: cowriter_split.id,
              first_name: cowriter.first_name,
              full_name: cowriter.full_name,
              last_name: cowriter.last_name,
              uuid: cowriter_split.id,
              is_unknown: 0,
              cowriter_share: cowriter_split.percent,
              errors: {}
            }
          ],
          public_domain: nil,
          translated_name: "",
          submitted_at: split_created_time
        }.to_json)

        parsed_response = JSON.parse(response.body)
        returned_submitted_date = Time.parse(parsed_response["submitted_at"]).utc.to_s(:db)

        expect(parsed_response.except("submitted_at")).to eq(json_response.except("submitted_at"))

        expect(split_created_time.to_s(:db)).to eq(returned_submitted_date)
      end
    end
  end

  describe "#update" do
    it "invokes the UpdatePublishingCompositionSplitsForm with the correct params" do
      params = {
        "id" => 64,
        "composition_params"=> {
          "id"=>64,
          "composition_title"=>"Composition Title",
          "translated_name"=>nil,
          "appears_on"=>"Appears On",
          "cowriters"=>[1],
          "cowriter_share"=>"50.0",
          "status"=>"resubmitted",
          "composer_share"=>"50.0",
          "errors"=>{},
          "isrc"=>"",
          "submitted_at"=>"2020-02-06T12:23:34.000-05:00",
          "performing_artist"=>"Performing Artist",
          "release_date"=>"",
          "is_ntc_song"=>true,
        },
        "cowriter_params"=> [
          {
            "id"=>1,
            "first_name"=>"First",
            "full_name"=>"First Last",
            "last_name"=>"Last",
            "uuid"=>1,
            "is_unknown"=>0,
            "cowriter_share"=>"50.0",
            "errors"=>{}
          },
        ],
        "composition_id"=>"64",
      }

      update_splits_params = {
        "composition_title"=>"Composition Title",
        "translated_name"=>"",
        "appears_on"=>"Appears On",
        "composer_share"=>"50.0",
        "cowriter_share"=>"50.0",
        "status"=>"resubmitted",
        "isrc"=>"",
        "performing_artist"=>"Performing Artist",
        "release_date"=>"",
        "is_ntc_song"=>true,
        "cowriter_params"=> [
          {"id"=>"1", "first_name"=>"First", "last_name"=>"Last", "cowriter_share"=>"50.0", "is_unknown"=>"0", "uuid"=>"1"},
        ],
        "composition_id"=>"64"
      }

      expect(PublishingAdministration::UpdatePublishingCompositionSplitsForm)
        .to receive(:new)
        .with(as_params(update_splits_params))
        .and_return(double(update: false, errors: {}))

      post :update, params: params
    end
  end
end
