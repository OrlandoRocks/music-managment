require "rails_helper"

describe Api::Backstage::PublishingAdministration::MultiTenantPublishingSplitsController do
  let(:person)  { create(:person) }
  let(:account) { create(:account, person: person) }

  before :each do
    login_as(person)
  end

  describe "#create" do
    context "data is valid" do
      it "saves the publishing splits" do
        publishing_composer    = create(:publishing_composer, first_name: "Mary", last_name: "Berry", account: account, person: account.person)
        publishing_composition = create(:publishing_composition, account: account)
        params      = {
          composition_id:   publishing_composition.id,
          composer_params:  [{ id: publishing_composer.id, composer_share: 50.to_d }],
          cowriter_params:  [{ first_name: "Paul", last_name: "Hollywood", cowriter_share: 50.to_d }]
        }

        post :create, params: params
        expect(publishing_composition.publishing_composition_splits.present?).to be true
      end

      it "returns a JSON response" do
        publishing_composer    = create(:publishing_composer, first_name: "Mary", last_name: "Berry", account: account, person: account.person)
        publishing_composition = create(:publishing_composition, account: account)
        cowriter    = create(:publishing_composer, :cowriter, first_name: "Paul", last_name: "Hollywood", account: account, person: account.person)
        params      = {
          composition_id:   publishing_composition.id,
          composer_params:  [{ id: publishing_composer.id, composer_share: 50.to_d }],
          cowriter_params:  [{ first_name: "Paul", last_name: "Hollywood", cowriter_share: 50.to_d }],
          translated_name:  ""
        }

        split_created_time = Time.current

        Timecop.freeze(split_created_time) do
          post :create, params: params

          cowriter_split = cowriter.reload.publishing_composition_splits.where(right_to_collect: false).last
          publishing_composer_split = publishing_composer.reload.publishing_composition_splits.where(right_to_collect: true).last

          json_response = JSON.parse({
            composition_id: publishing_composition.id.to_s,
            composer_percent: 50.to_d,
            cowriter_percent: 50.to_d,
            cowriters: [
              {
                id: cowriter_split.id,
                first_name: cowriter.first_name,
                full_name: cowriter.full_name,
                last_name: cowriter.last_name,
                uuid: cowriter_split.id,
                is_unknown: 0,
                cowriter_share: cowriter_split.percent,
                errors: {}
              }
            ],
            composers: [
              {
                id: publishing_composer_split.id,
                composer_share: publishing_composer_split.percent,
                errors: {},
                uuid: publishing_composer_split.id,
              }
            ],
            translated_name: "",
            submitted_at: split_created_time,
            unknown_split_percent: 100.to_d
          }.to_json)

          parsed_response = JSON.parse(response.body)
          returned_submitted_date = Time.parse(parsed_response["submitted_at"]).utc.to_s(:db)

          expect(parsed_response.except("submitted_at")).to eq(json_response.except("submitted_at"))

          expect(split_created_time.to_s(:db)).to eq(returned_submitted_date)
        end
      end
    end

    context "data is invalid" do
      it "does not save the publishing splits" do
        publishing_composer    = create(:publishing_composer, first_name: "Mary", last_name: "Berry", account: account, person: account.person)
        publishing_composition = create(:publishing_composition, account: account)
        params      = {
          composition_id:   publishing_composition.id,
          composer_params:  [{ id: publishing_composer.id, composer_share: 100.to_d }],
          cowriter_params:  [{ first_name: "Paul", last_name: "Hollywood", cowriter_share: 50.to_d }]
        }

        post :create, params: params
        expect(publishing_composition.publishing_composition_splits.present?).to be false
      end

      it "returns a JSON response" do
        publishing_composer    = create(:publishing_composer, first_name: "Mary", last_name: "Berry", account: account, person: account.person)
        publishing_composition = create(:publishing_composition, account: account)
        params      = {
          composition_id:   publishing_composition.id,
          composer_params:  [{ id: publishing_composer.id, composer_share: 100.to_d }],
          cowriter_params:  [{ first_name: "Paul", last_name: "Hollywood", cowriter_share: 50.to_d }]
        }

        post :create, params: params
        expect(JSON.parse(response.body)["errors"].present?).to be true
      end
    end

    context "when a cowriter is created" do
      it "removes trailing whitespaces characters from the cowriters first and last names"  do
        publishing_composer    = create(:publishing_composer, first_name: "Mary", last_name: "Berry", account: account, person: account.person)
        publishing_composition = create(:publishing_composition, account: account)
        params      = {
          composition_id:   publishing_composition.id,
          composer_params:  [{ id: publishing_composer.id, composer_share: 0 }],
          cowriter_params:  [{ first_name: "Paul  ", last_name: "Hollywood   ", cowriter_share: 50 }]
        }

        expected_params = {
          person:          person,
          composition:     publishing_composition,
          composer_params: [{ "id" => publishing_composer.id.to_s, "composer_share" => "0" }],
          cowriter_params: [{ "first_name" => "Paul", "last_name" => "Hollywood", "cowriter_share" => "50" }],
          translated_name: nil
        }

        expect(PublishingAdministration::MultiTenantPublishingCompositionSplitsForm).to receive(:new)
          .with(expected_params).and_call_original
        post :create, params: params
      end
    end

    context "when the publishing_composition does not have cowriters" do
      it "sets the cowriter params to an empty array" do
        publishing_composer    = create(:publishing_composer, first_name: "Mary", last_name: "Berry", account: account, person: account.person)
        publishing_composition = create(:publishing_composition, account: account)
        params      = {
          composition_id:  publishing_composition.id,
          composer_params:  [{ id: publishing_composer.id, composer_share: 0 }],
          cowriter_params: nil
        }

        expected_params = {
          person:          person,
          composition:     publishing_composition,
          composer_params: [{ "id" => publishing_composer.id.to_s, "composer_share" => "0" }],
          cowriter_params: [],
          translated_name: nil
        }

        expect(PublishingAdministration::MultiTenantPublishingCompositionSplitsForm).to receive(:new)
          .with(expected_params).and_call_original
        post :create, params: params
      end
    end
  end
end
