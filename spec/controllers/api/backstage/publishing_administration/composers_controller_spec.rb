require "rails_helper"

describe Api::Backstage::PublishingAdministration::ComposersController do

  describe "#update" do
    let(:person)  { create(:person) }
    let(:account) { create(:account, person: person) }
    let(:composer) do
      create(
        :composer,
        account: account,
        person: person,
        publishing_role_id: 5,
        email: Faker::Internet.email
      )
    end
    let(:params) do
      {
        name_prefix:      composer.name_prefix,
        first_name:       "Bell   ",
        middle_name:      composer.middle_name,
        last_name:        composer.last_name,
        name_suffix:      composer.name_suffix,
        composer_pro_id:  "1",
        composer_cae:     "123456789"
      }
    end

    before :each do
      login_as(person)
    end

    xit "saves a composer with new info" do
      put :update, params: { id: composer.id, composer: params }
      expect(composer.reload.first_name).to eq "Bell"
      expect(composer.reload.cae).to eq(params[:composer_cae])
    end

    xit "returns a JSON response" do
      put :update, params: { id: composer.id, composer: params }
      serialized_composer = Api::Backstage::ComposerSerializer.new(composer.reload).to_json(root: false)
      expect(response.body).to eq(serialized_composer)
    end
  end

end
