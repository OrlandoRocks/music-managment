require "rails_helper"

describe Api::Backstage::TrackMonetization::DashboardController do
  before(:each) do
    login_as(person)
  end

  describe "#create" do
    let(:person) { create(:person) }
    let(:track)  { create(:track_monetization, person: person, eligibility_status: "eligible") }
    let(:params) {{ track_monetization: { track_monetization_ids: [track.id], takedown: false } }}
    let(:expected_response_data) do
      { data: [{ track_id: track.id, status: "processing" }] }.as_json
    end

    it "returns response data" do
      post :create, params: params, format: :json
      expect(JSON.parse(response.body)).to eq expected_response_data
    end
  end
end
