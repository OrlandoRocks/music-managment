require "rails_helper"

describe Api::Backstage::AlbumApp::DistributionProgressesController, type: :controller do
  let(:person) { create(:person) }
  let(:album) { create(:album, person: person) }

  let(:params) do
    {
      album_id: album.id,
    }
  end

  before do
    login_as person
  end

  describe "show" do
    context "when an album_id is passed in" do
      it "returns a success" do
        get :show, params: params, as: :json

        assert_response :success, @response.body
      end
    end

    context "when an album_id is not passed in" do
      it "returns a failure" do
        params = {
          album_id: nil,
        }

        get :show, params: params, as: :json

        assert_response :unprocessable_entity, @response.body
      end
    end
  end
end
