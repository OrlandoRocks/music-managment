require "rails_helper"

describe Api::Backstage::AlbumApp::ExternalServiceIdsController, type: :controller do
  let(:person) { create(:person) }

  before do
    login_as person
  end

  describe "spotify_search_artists, " do
    before do
      allow(Spotify::ApiClient).to receive_message_chain(:new, :find_by_artist_name).and_return(double(body: {}))
    end

    it "succeeds with name param" do
      get :spotify_search_artists, params: { name: 'ppcocaine' }
      assert_response :success, @response.body
    end

    it "errors without name param" do
      get :spotify_search_artists, params: {}
      assert_response :unprocessable_entity, @response.body
    end
  end

  describe "spotify_recent_albums, " do
    before do
      allow(Spotify::ApiClient).to receive_message_chain(:new, :spotify_recent_albums).and_return(double(body: {}))
    end

    it "succeeds with artist_id param" do
      get :spotify_recent_albums, params: { artist_id: 123 }
      assert_response :success, @response.body
    end

    it "errors without artist_id param" do
      get :spotify_recent_albums, params: {}
      assert_response :unprocessable_entity, @response.body
    end
  end

  describe "spotify_get_artist, " do
    before do
      allow(Spotify::ApiClient).to receive_message_chain(:new, :find_by_artist_name).and_return(double(body: {}))
      allow(Spotify::ApiClient).to receive_message_chain(:new, :get_valid_artist_by_id).and_return(double(success?: true, body: {}))
    end

    it "succeeds with name and id params" do
      get :spotify_get_artist, params: { id: 123, name: 'ppcocaine' }
      assert_response :success, @response.body
    end

    it "errors without name and id params" do
      get :spotify_get_artist, params: {}
      assert_response :unprocessable_entity, @response.body
    end
  end

  describe "apple_search_artists, " do
    before do
      allow(Apple::ApiClient).to receive_message_chain(:new, :find_by_artist_name).and_return(double(body: {}))
    end

    it "succeeds with name param" do
      get :apple_search_artists, params: { name: 'ppcocaine' }
      assert_response :success, @response.body
    end

    it "errors without name param" do
      get :apple_search_artists, params: {}
      assert_response :unprocessable_entity, @response.body
    end
  end

  describe "apple_get_artist, " do
    before do
      allow(Apple::ApiClient).to receive_message_chain(:new, :get_artist_by_id).and_return(double(body: {}))
    end

    it "succeeds with name and id params" do
      get :apple_get_artist, params: { name: 'ppcocaine', id: 123 }
      assert_response :success, @response.body
    end

    it "errors without name and id params" do
      get :apple_get_artist, params: {}
      assert_response :unprocessable_entity, @response.body
    end
  end
end
