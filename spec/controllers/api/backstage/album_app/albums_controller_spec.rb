require "rails_helper"

describe Api::Backstage::AlbumApp::AlbumsController, type: :controller do
  let(:person) { create(:person) }
  let(:album) { create(:album) }
  let(:creatives) do
    [
      {
        name: "New Test Artist",
        role: "primary_artist",
        apple: {
          create_page: false
        },
        spotify: {
          create_page: false
        }
      }
    ]
  end
  let(:params) do
    {
      album: {
        name: album.name,
        label_name: album.label_name,
        language_code: album.language_code,
        primary_genre_id: album.primary_genre_id,
        secondary_genre_id: album.secondary_genre_id,
        golive_date: {hour: "01", min: '01', month: '12', day: '12', year: (DateTime.now + 1.year).strftime('%Y'), meridian: 'am'},
        timed_release_timing_scenario: album.timed_release_timing_scenario,
        creatives: creatives,
        sale_date: album.sale_date,
      }
    }
  end

  before do
    login_as person
  end

  describe "selected_countries" do
    it "creates with selected territories" do
      post :create, params: params.deep_merge({ album: { selected_countries: 'KH,PK' } })

      saved_album_countries = Country.where(id: AlbumCountry.last(2).map(&:country_id))
      expect(saved_album_countries.map(&:iso_code)).to eq(%w[KH PK])
    end

    it "updates the territories" do
      post :create, params: params.deep_merge({ album: { selected_countries: 'AF,MX' } })

      saved_album_countries = Country.where(id: AlbumCountry.last(2).map(&:country_id))
      expect(saved_album_countries.map(&:iso_code)).to eq(%w[AF MX])
    end
  end

  describe "#create_esids" do
    it "calls ESID creation and matching services" do
      allow(ExternalServiceIds::CreationService).to receive(:create_update_match)
      expect(ExternalServiceIds::CreationService).to receive(:create_update_match)
      post :create, params: params.deep_merge({ album: { selected_countries: 'KH,PK' } })
    end
  end

  describe "invalid form submission" do
    let (:name) { "Nick Cage's Favorite Tune" }
    let (:label_name) { "Update label name" }
    let (:language_code) { "fr" }
    let (:primary_genre_id) { 1 }
    let (:secondary_genre_id) { Genre.find_by(name: "Pop").id }
    let (:timed_release_timing_scenario) { "relative_time" }
    let (:new_creative_name) { "Jonny Mo" }
    let (:creatives) { [{name: new_creative_name, role: "primary_artist"}] }

    let(:album)  { create(:album,
                          :with_petri_bundle,
                          :with_uploaded_songs,
                          :with_one_year_renewal,
                          name: "Album 1",
                          label_name: "Label 1",
                          language_code: "de",
                          primary_genre: Genre.find_by(name: "Folk"),
                          golive_date: DateTime.now + 1.days,
                          timed_release_timing_scenario: "absolute_time",
                          number_of_songs: 2,
                          creatives: [{"name" => "Test Artist", "role" => "primary_artist"}],
                          legal_review_state: 'NEEDS REVIEW') }

    context "update" do
      context "name is missing" do
        before(:each) do
          post :update, params: {
            id: album.id,
            album: {
              name: '',
              label_name: label_name,
              language_code: language_code,
              primary_genre_id: primary_genre_id,
              secondary_genre_id: secondary_genre_id,
              golive_date: album.golive_date,
              timed_release_timing_scenario: timed_release_timing_scenario,
              creatives: creatives,
              sale_date: 'April 24, 2000',
              selected_countries: 'AF,MX'
            }
          }
        end

        it "should give back a 400-level status code" do
          expect(response.status).to eq(422)
        end

        it "should have an error message for missing album name" do
          expect(JSON.parse(response.body).dig("errors", "name")).to include("can't be blank")
        end
      end

      context "primary genre is missing" do
        before(:each) do
          post :update, params: {
            id: album.id,
            album: {
              name: name,
              label_name: label_name,
              language_code: language_code,
              primary_genre_id: nil,
              secondary_genre_id: secondary_genre_id,
              golive_date: album.golive_date,
              timed_release_timing_scenario: timed_release_timing_scenario,
              creatives: creatives,
              sale_date: 'April 24, 2000',
              selected_countries: 'AF,MX'
            }
          }
        end

        it "should give back a 400-level status" do
          expect(response.status).to eq(422)
        end

        it "should give a primary genre missing error" do
          expect(JSON.parse(response.body).dig("errors", "primary_genre_id")).to include("Please select at least one Genre")
        end
      end

      context "language is invalid" do
        context "it's missing" do
          before(:each) do
            post :update, params: {
              id: album.id,
              album: {
                name: name,
                label_name: label_name,
                language_code: nil,
                primary_genre_id: primary_genre_id,
                secondary_genre_id: secondary_genre_id,
                golive_date: album.golive_date,
                timed_release_timing_scenario: timed_release_timing_scenario,
                creatives: creatives,
                sale_date: 'April 24, 2000',
                selected_countries: 'AF,MX'
              }
            }
          end

          it "should give back a 400-level status" do
            expect(response.status).to eq(422)
          end

          it "should give a language is missing error" do
            expect(JSON.parse(response.body).dig("errors", "language_code")).to include("Please select at least one Language")
          end
        end

        context "it's not a valid code" do
          before(:each) do
            post :update, params: {
              id: album.id,
              album: {
                name: name,
                label_name: label_name,
                language_code: 'ZZ',
                primary_genre_id: primary_genre_id,
                secondary_genre_id: secondary_genre_id,
                golive_date: album.golive_date,
                timed_release_timing_scenario: timed_release_timing_scenario,
                creatives: creatives,
                sale_date: 'April 24, 2000',
                selected_countries: 'AF,MX'
              }
            }
          end

          it "should give back a 400-level status" do
            expect(response.status).to eq(422)
          end

          it "should give a language is invalid error" do
            expect(JSON.parse(response.body).dig("errors", "language_code")).to include("Invalid language code")
          end
        end
      end

      context "golive_date is invalid" do
        context "it's missing" do
          before(:each) do
            post :update, params: {
              id: album.id,
              album: {
                name: name,
                label_name: label_name,
                language_code: nil,
                primary_genre_id: primary_genre_id,
                secondary_genre_id: secondary_genre_id,
                golive_date: nil,
                timed_release_timing_scenario: timed_release_timing_scenario,
                creatives: creatives,
                sale_date: 'April 24, 2000',
                selected_countries: 'AF,MX'
              }
            }
          end

          it "should give back a 400-level status" do
            expect(response.status).to eq(422)
          end

          it "should give a golive_date is missing error" do
            expect(JSON.parse(response.body).dig("errors", "golive_date")).to include("Golive date is required")
          end
        end

      end

      context "creatives are invalid" do
        context "they're missing" do
          before(:each) do
            post :update, params: {
              id: album.id,
              album: {
                name: name,
                label_name: label_name,
                language_code: nil,
                primary_genre_id: primary_genre_id,
                secondary_genre_id: secondary_genre_id,
                golive_date: album.golive_date,
                timed_release_timing_scenario: timed_release_timing_scenario,
                creatives: [],
                sale_date: 'April 24, 2000',
                selected_countries: 'AF,MX'
              }
            }
          end

          it "should give back a 400-level status" do
            expect(response.status).to eq(422)
          end

          it "should give a creatives are missing error" do
            expect(JSON.parse(response.body).dig("errors", "creatives")).to include("Creatives required")
          end
        end

        context "names are the same" do
          before(:each) do
            post :update, params: {
              id: album.id,
              album: {
                name: name,
                label_name: label_name,
                language_code: nil,
                primary_genre_id: primary_genre_id,
                secondary_genre_id: secondary_genre_id,
                golive_date: album.golive_date,
                timed_release_timing_scenario: timed_release_timing_scenario,
                creatives: creatives.push({name: new_creative_name, role: "primary_artist"}),
                sale_date: 'April 24, 2000',
                selected_countries: 'AF,MX'
              }
            }
          end

          it "should give back a 400-level status" do
            expect(response.status).to eq(422)
          end

          it "should give a creatives are duplicates error" do
            expect(JSON.parse(response.body).dig("errors", "creatives")).to include("Creatives names can't be duplicated")
          end
        end
      end
    end

    context "create" do
      context "name is missing" do
        before(:each) do
          post :create, params: {
            album: {
              name: '',
              label_name: label_name,
              language_code: language_code,
              primary_genre_id: primary_genre_id,
              secondary_genre_id: secondary_genre_id,
              golive_date: album.golive_date,
              timed_release_timing_scenario: timed_release_timing_scenario,
              creatives: creatives,
              sale_date: 'April 24, 2000',
              selected_countries: 'AF,MX'
            }
          }
        end

        it "should give back a 400-level status code" do
          expect(response.status).to eq(422)
        end

        it "should have an error message for missing album name" do
          expect(JSON.parse(response.body).dig("errors", "name")).to include("can't be blank")
        end
      end

      context "primary genre is missing" do
        before(:each) do
          post :create, params: {
            album: {
              name: name,
              label_name: label_name,
              language_code: language_code,
              primary_genre_id: nil,
              secondary_genre_id: secondary_genre_id,
              golive_date: album.golive_date,
              timed_release_timing_scenario: timed_release_timing_scenario,
              creatives: creatives,
              sale_date: 'April 24, 2000',
              selected_countries: 'AF,MX'
            }
          }
        end

        it "should give back a 400-level status" do
          expect(response.status).to eq(422)
        end

        it "should give a primary genre missing error" do
          expect(JSON.parse(response.body).dig("errors", "primary_genre_id")).to include("Please select at least one Genre")
        end
      end

      context "language is invalid" do
        context "it's missing" do
          before(:each) do
            post :create, params: {
              album: {
                name: name,
                label_name: label_name,
                language_code: nil,
                primary_genre_id: primary_genre_id,
                secondary_genre_id: secondary_genre_id,
                golive_date: album.golive_date,
                timed_release_timing_scenario: timed_release_timing_scenario,
                creatives: creatives,
                sale_date: 'April 24, 2000',
                selected_countries: 'AF,MX'
              }
            }
          end

          it "should give back a 400-level status" do
            expect(response.status).to eq(422)
          end

          it "should give a language is missing error" do
            expect(JSON.parse(response.body).dig("errors", "language_code")).to include("Please select at least one Language")
          end
        end

        context "it's not a valid code" do
          before(:each) do
            post :create, params: {
              album: {
                name: name,
                label_name: label_name,
                language_code: 'ZZ',
                primary_genre_id: primary_genre_id,
                secondary_genre_id: secondary_genre_id,
                golive_date: album.golive_date,
                timed_release_timing_scenario: timed_release_timing_scenario,
                creatives: creatives,
                sale_date: 'April 24, 2000',
                selected_countries: 'AF,MX'
              }
            }
          end

          it "should give back a 400-level status" do
            expect(response.status).to eq(422)
          end

          it "should give a language is invalid error" do
            expect(JSON.parse(response.body).dig("errors", "language_code")).to include("Invalid language code")
          end
        end
      end

      context "golive_date is invalid" do
        context "it's missing" do
          before(:each) do
            post :create, params: {
              album: {
                name: name,
                label_name: label_name,
                language_code: nil,
                primary_genre_id: primary_genre_id,
                secondary_genre_id: secondary_genre_id,
                golive_date: nil,
                timed_release_timing_scenario: timed_release_timing_scenario,
                creatives: creatives,
                sale_date: 'April 24, 2000',
                selected_countries: 'AF,MX'
              }
            }
          end

          it "should give back a 400-level status" do
            expect(response.status).to eq(422)
          end

          it "should give a golive_date is missing error" do
            expect(JSON.parse(response.body).dig("errors", "golive_date")).to include("Golive date is required")
          end
        end

      end

      context "creatives are invalid" do
        context "they're missing" do
          before(:each) do
            post :create, params: {
              album: {
                name: name,
                label_name: label_name,
                language_code: nil,
                primary_genre_id: primary_genre_id,
                secondary_genre_id: secondary_genre_id,
                golive_date: album.golive_date,
                timed_release_timing_scenario: timed_release_timing_scenario,
                creatives: [],
                sale_date: 'April 24, 2000',
                selected_countries: 'AF,MX'
              }
            }
          end

          it "should give back a 400-level status" do
            expect(response.status).to eq(422)
          end

          it "should give a creatives are missing error" do
            expect(JSON.parse(response.body).dig("errors", "creatives")).to include("Creatives required")
          end
        end

        context "names are the same" do
          before(:each) do
            post :create, params: {
              album: {
                name: name,
                label_name: label_name,
                language_code: nil,
                primary_genre_id: primary_genre_id,
                secondary_genre_id: secondary_genre_id,
                golive_date: album.golive_date,
                timed_release_timing_scenario: timed_release_timing_scenario,
                creatives: creatives.push({name: new_creative_name, role: "primary_artist"}),
                sale_date: 'April 24, 2000',
                selected_countries: 'AF,MX'
              }
            }
          end

          it "should give back a 400-level status" do
            expect(response.status).to eq(422)
          end

          it "should give a creatives are duplicates error" do
            expect(JSON.parse(response.body).dig("errors", "creatives")).to include("Creatives names can't be duplicated")
          end
        end
      end
    end
  end

  describe "specialized release submission" do
    let (:name) { "Nick Cage's Free Tune" }
    let (:label_name) { "Update label name" }
    let (:language_code) { "fr" }
    let (:primary_genre_id) { 1 }
    let (:secondary_genre_id) { Genre.find_by(name: "Pop").id }
    let (:timed_release_timing_scenario) { "relative_time" }
    let (:new_creative_name) { "Jonny Mo" }
    let (:creatives) {
      [{
         name: new_creative_name,
         role: "primary_artist",
         apple: {
           create_page: false
         },
         spotify: {
           create_page: false
         }
       }] }

    context "valid submission" do
      before(:each) do
        post :create, params: {
          album: {
            name: 'My name',
            label_name: label_name,
            language_code: language_code,
            primary_genre_id: primary_genre_id,
            specialized_release_type: 'facebook',
            sale_date: 'April 24, 2000',
            creatives: creatives
          }
        }
      end

      it "should return a success response" do
        expect(response.status).to eq(200)
      end
    end
  end

  describe "user update" do
    let (:creatives) { [{name: "Terry Chamato", role: "primary_artist", apple: {
      create_page: false
    },
    spotify: {
      create_page: false
    }}] }
    let (:timed_release_timing_scenario) { "relative_time" }
    let (:album)  { create(:album,
      :with_petri_bundle,
      :with_uploaded_songs,
      :with_one_year_renewal,
      creatives: [{"name" => "Test Artist", "role" => "primary_artist"}],
      primary_genre: Genre.find_by(name: "Folk"),
      golive_date: DateTime.now + 1.days,
      timed_release_timing_scenario: "absolute_time",
      number_of_songs: 2,
      legal_review_state: 'NEEDS REVIEW',
      language_code: "EN"
    )}

    context "golive_date is changed" do

      it "should run handle_golive_date to sync dates" do
        post :update, params: {
          id: album.id,
          album: {
            name: album.name,
            primary_genre_id: album.primary_genre_id,
            creatives: creatives,
            language_code: album.language_code,
            golive_date: {hour: "01", min: '01', month: '12', day: '12', year: (DateTime.now + 1.year).strftime('%Y'), meridian: 'am'},
            timed_release_timing_scenario: timed_release_timing_scenario
          }
        }
        
        changed_album = controller.view_assigns["album"]

        expect(response.status).to eq(200)
        expect(Date.parse(changed_album.sale_date.to_s)).to eq(Date.parse(changed_album.golive_date.to_s))

          
      end

    end
  end

end