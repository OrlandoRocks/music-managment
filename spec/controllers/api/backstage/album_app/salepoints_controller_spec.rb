require "rails_helper"

describe Api::Backstage::AlbumApp::SalepointsController, type: :controller do
  let(:person) { create(:person) }
  let(:album) { create(:album, person: person) }

  let(:params) do
    {
      album_id: album.id,
      deliver_automator: false,
      apple_music: false,
      store_ids: [],
    }
  end

  let(:store_ids) {
    [
      Store::ITUNES_WW_ID,
      Store::IHEART_RADIO_STORE_ID,
      Store::NAPSTER_STORE_ID,
    ]
  }

  before do
    login_as person
  end

  describe "create" do
    context "when album_id and store_ids params are passed in" do
      context "when store_ids contains values" do
        it "returns a success" do
          post :create, params: {**params, store_ids: store_ids}, as: :json

          assert_response :success, @response.body
        end
      end

      context "when store_ids does not contain values" do
        it "returns a success" do
          post :create, params: {**params, store_ids: []}, as: :json

          assert_response :success, @response.body
        end
      end
    end

    context "when an album_id is not passed in" do
      it "returns a failure" do
        post :create, params: {**params, album_id: nil}, as: :json

        assert_response :unprocessable_entity, @response.body
      end
    end

    context "when a store_ids array is not passed in" do
      it "returns a failure" do
        post :create, params: {**params, store_ids: nil}, as: :json

        assert_response :unprocessable_entity, @response.body
      end
    end
  end
end
