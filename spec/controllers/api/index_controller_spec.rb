require 'rails_helper'

RSpec.describe Api::IndexController, type: :controller do
  context "With a valid finalize CartsController" do
    it "Gets success" do
      get :index
      expect(JSON.parse(response.body)["status"]).to eq("success")
    end

    it "doesn't create a tunecore cookie" do
      get :index, params: { garbage: "gobbldygook", junk: "gunk" }
      expect(cookies).not_to have_key("tunecore_wp_1")
    end

    it "creates a tunecore cookie" do
      get :index, params: { ref: "applemusicartist", jt: "JoinNow" }
      expect(cookies).to have_key("tunecore_wp_1")
      expect(JSON.parse(cookies["tunecore_wp_1"])).to have_key("ref")
      expect(JSON.parse(cookies["tunecore_wp_1"])).to have_key("jt")
    end
  end
end
