require "rails_helper"

describe Api::PeopleController do

  context "with a valid user register" do
    it "responds with a successful success message" do
     post :registration, params: { accepted_terms_and_conditions: "true",
      country: "1",
      email: "test@tunecore.com",
      name: "Test Man",
      password: "Testpass123!",
      password_confirmation: "Testpass123!",
      api_key: API_CONFIG["API_KEY"]}
      expect(JSON.parse(response.body)["status"]).to eq("success")
    end
  end

  context "referral_data from cookies" do
    it "builds from prefixed with wp_tunecore_" do
      cookie_data_1 = {
        ref: "TestData",
        jt: 123,
        utm_source: "pubadmin",
        utm_medium: "web",
        utm_term: "social",
        utm_content: "testing",
        utm_campaign: "footer-links",
        tc_timestamp: 1_458_752_702
      }
      cookie_data_2 = {
        ref: "TestData",
        jt: 123,
        utm_source: "pubadmin",
        utm_medium: "web",
        utm_term: "social",
        utm_content: "testing",
        utm_campaign: "footer-links",
        tc_timestamp: 1_458_752_702
      }
      cookies[:tunecore_wp] = cookie_data_1.to_json
      cookies[:tunecore_wp_1] = cookie_data_2.to_json
      post :registration, params: { accepted_terms_and_conditions: "true",
      country: "1",
      email: "test@tunecore.com",
      name: "Test Man",
      password: "Testpass123!",
      password_confirmation: "Testpass123!",
      api_key: API_CONFIG["API_KEY"]}
      expect(assigns[:person].referral_data.size).to eq 14
    end

    it "doesnt build from non wp_tunecore_ cookies" do
      cookies["other_cookie"] = { ref: "TestData" }.to_json
      post :registration, params: { accepted_terms_and_conditions: "true",
      country: "1",
      email: "test@tunecore.com",
      name: "Test Man",
      password: "Testpass123!",
      password_confirmation: "Testpass123!",
      api_key: API_CONFIG["API_KEY"]}
      expect(assigns[:person].referral_data.size).to eq 0
    end
  end

  context "with invalid user" do
    it "responds with a failure message" do
      post :registration, params: { accepted_terms_and_conditions: "true",
        country: "1",
        email: "test@tunecore.com",
        name: "Test Man",
        password: "",
        password_confirmation: "",
        api_key: API_CONFIG["API_KEY"]
      }
      expect(JSON.parse(response.body)["status"]).to eq("failure")
    end
  end

  context "with a valid user password update" do
    it "responds with a successful success message" do
     person = people(:tunecore_customer_verified_new)
     remote_ip = "0.0.0.0"
     user_agent = "Rails Testing"
     request.headers["remote_ip"] = remote_ip
     request.headers["user_agent"] = user_agent
     get :update_password, params: {id: person.id, invite_code: person.invite_code, api_key: API_CONFIG["API_KEY"],
       password: "Testpass123!", password_confirmation: "Testpass123!" }
     expect(JSON.parse(response.body)["status"]).to eq("success")
     person.reload
     expect(person.account_locked_until).to eq(nil)
     expect(person.login_attempts).to eq(0)
     expect(person.invite_code).to eq(nil)
     expect(person.invite_expiry).to eq(nil)
     expect { person.post_update_password(person.password,remote_ip,user_agent) }
      .to change(LoginEvent, :count).by(1)
     expect { person.post_update_password(person.password,remote_ip,user_agent) }
          .to change { ActionMailer::Base.deliveries.count }.by(1)
     end
   end

   context "with invalid user" do
    it "responds with a failure message" do
      person = people(:tunecore_customer_verified_new)
      get :update_password, params: {id: person.id, invite_code: nil, api_key: API_CONFIG["API_KEY"],
       password: "Testpass123!", password_confirmation: "Testpass123!" }
       expect(JSON.parse(response.body)["status"]).to eq("failure")
     end
   end

  context "with a valid resend_confirmation_email method" do
    it "resend confirmation email success message" do
     person = people(:tunecore_customer_verified_new)
     resend_params = {id: person.id, api_key: API_CONFIG["API_KEY"] }
     expect { get :resend_confirmation_email, params: resend_params }.to change { ActionMailer::Base.deliveries.count }.by(1)
     get :resend_confirmation_email, params: resend_params
     expect(JSON.parse(response.body)["status"]).to eq("success")
    end
  end

  context "with a invalid resend_confirmation_email method" do
    it "resend confirmation email failure message" do
     person = people(:tunecore_customer_verified_new)
     resend_params = {id: '', api_key: API_CONFIG["API_KEY"] }
     get :resend_confirmation_email, params: resend_params
     expect(JSON.parse(response.body)["status"]).to eq("failure")
    end
  end

  context "with a valid complete_verify method" do
    it "redirects to TCS2.0 with a already_verified message and person_id" do
     person = people(:tunecore_customer_verified_new)
     service = get :complete_verify, params: { key: person.invite_code, api_key: API_CONFIG["API_KEY"], id: person.id.to_s }
     message = "already_verified"
     expect(response).to redirect_to(ENV['TC_SOCIAL_REGISTRATION_URL']+"?message=#{message}&id=#{person.id}")
   end
  end


  context "with a valid complete_verify method" do
    it "will trigger RewardSystem::Events.new_user" do
     person = people(:tunecore_customer_verified_new)
     service = get :complete_verify, params: { key: person.invite_code, api_key: API_CONFIG.fetch("API_KEY") { raise "API_KEY ENV not set" }, id: person.id.to_s }
     allow(RewardSystem::Events).to receive(:new_user)
    end
  end

  context "with a valid complete_verify method" do
    it "redirects to TCS2.0  with a verified message and person_id" do
     person = people(:tunecore_customer_brand_new)
     person.generate_invite_code
     person.reload
     service = get :complete_verify, params: { key: person.invite_code, api_key: API_CONFIG["API_KEY"], id: person.id.to_s  }
     person.reload
     expect(person.is_verified).to eq(1)
     expect(person.verified_at).not_to eq(nil)
     message = "verified"
     expect(response).to redirect_to(ENV['TC_SOCIAL_REGISTRATION_URL']+"?message=#{message}&id=#{person.id}")
    end
  end

  context "with a not_recognized complete_verify method" do
    it "redirects to TCS2.0 with a not_recognized message and person_id" do
     person = people(:tunecore_customer_brand_new)
     person.generate_invite_code
     person.reload
     service = get :complete_verify, params: { key: '', api_key: API_CONFIG["API_KEY"], id: person.id.to_s  }
     message = "not_recognized"
     expect(response).to redirect_to(ENV['TC_SOCIAL_REGISTRATION_URL']+"?message=#{message}&id=#{person.id}")
    end
  end

  context "with a expired complete_verify method" do
    it "redirects to TCS2.0" do
     person = people(:tunecore_customer_brand_new)
     person.invite_expiry = Time.now - 1.day
     person.save
     person.reload
     service = get :complete_verify, params: { key: person.invite_code, api_key: API_CONFIG["API_KEY"], id: person.id.to_s  }
     message = "expired"
     expect(response).to redirect_to(ENV['TC_SOCIAL_REGISTRATION_URL']+"?message=#{message}&id=#{person.id}")
    end
  end

  context "#send_auth_code" do
    before(:each) do
      @email = "test@tunecore.com"
      @country_code = "001"
      @phone_number = "123456789"
      @params = {
        email:          @email,
        phone_code:     @country_code,
        phone_number:   @phone_number,
        api_key:        API_CONFIG["API_KEY"]
      }
    end
    it "responds with a success message" do
      mock_response = double(ok?: true, id: "mock_authy_id")
      allow(Authy::API).to receive(:register_user).with(
        email:          @email,
        country_code:   @country_code,
        cellphone:      @phone_number
      ).and_return(mock_response)
      expected_args = { id: "mock_authy_id", force: true }
      expect(Authy::API).to receive(:request_sms).with(expected_args).and_return(mock_response)
      post :send_auth_code, params: @params
      expect(JSON.parse(response.body)["status"]).to eq("success")
    end

    it "responds with a failure message" do
      mock_response = double(ok?: false, errors: { "message": "error" })
      expect(Authy::API).to receive(:register_user).and_return(mock_response)
      post :send_auth_code, params: @params
      expect(JSON.parse(response.body)["status"]).to eq("failure")
    end
  end

  context "without phone_number register for INDIA user" do
    it "responds with a failure message" do
      post :registration, params: { accepted_terms_and_conditions: "true",
                                    country: "101",
                                    email: "test@tunecore.com",
                                    name: "Test Man",
                                    password: "Testpass123!",
                                    password_confirmation: "Testpass123!",
                                    api_key: API_CONFIG["API_KEY"] }
      expect(JSON.parse(response.body)["status"]).to eq("failure")
    end
  end

  context "with phone_number register for INDIA user" do
    it "responds with a success message" do
      authy_id = "11111"
      auth_code = "12345"
      expected_args = { token: auth_code, id: authy_id }
      mock_response = double(ok?: true)
      expect(Authy::API).to receive(:verify).with(expected_args).and_return(mock_response)
      params = {
        accepted_terms_and_conditions: "true",
        country: "101",
        email: "test@tunecore.com",
        name: "Test Man",
        password: "Testpass123!",
        password_confirmation: "Testpass123!",
        phone_code: "91",
        phone_number: "123456789",
        authy_id: authy_id,
        auth_code: auth_code,
        api_key: API_CONFIG["API_KEY"] }
      post :registration, params: params
      expect(JSON.parse(response.body)["status"]).to eq("success")
    end
  end

  context "create vat informations" do
    it "returns failure when vat registration number format is wrong" do
      @person = create(:person, :with_tc_social, country: "Austria")
      params = { vat_information_attributes: { id: nil, company_name: "abc", vat_registration_number: "1210909" },
                 customer_type: "individual",
                 api_key: API_CONFIG["API_KEY"] }
      controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(@person.tc_social_token))
      post :update_vat_information, params: params
      expect(JSON.parse(response.body)["status"]).to eq("failure")
    end

    it "returns failure when vat informations are wrong" do
      @person = create(:person, :with_tc_social, country: "Austria")
      params = { vat_information_attributes: { id: nil, company_name: "abc", vat_registration_number: "U1210909" },
                 customer_type: "individual",
                 api_key: API_CONFIG["API_KEY"] }
      controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(@person.tc_social_token))
      post :update_vat_information, params: params
      expect(JSON.parse(response.body)["status"]).to eq("failure")
    end

    it "saves address and compliance info while adding vat information" do
      @person = create(:person, :with_tc_social, country: "US")
      params = { vat_information_attributes: { company_name: "Test", vat_registration_number: "U57477929" },
                 compliance_info_fields_attributes: [ { field_name: "first_name", field_value: "name1"}],
                 customer_type: "individual",
                 country: "Austria",
                 address1: "address line 1",
                 address2: "address line 2",
                 api_key: API_CONFIG["API_KEY"] }
      controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(@person.tc_social_token))
      post :update_vat_information, params: params
      @person.reload

      expect(JSON.parse(response.body)["status"]).to eq("success")
      expect(@person.country).to eq("Austria")
      expect(@person.address1).to eq("address line 1")
      expect(@person.address2).to eq("address line 2")

      compliance_info_field = @person.compliance_info_fields.find_by_field_name('first_name')
      expect(compliance_info_field.field_value).to eq("name1")
    end
  end

  describe "GET #personal_info" do
    context "with successfull authentcation" do
      it "returns personal info for the current logged in user" do
        person = create(:person, :with_tc_social, country: "US", address1: "address line 1", address2: "address line 2")
        controller.request.env["HTTP_AUTHORIZATION"] =
          ActionController::HttpAuthentication::Token
          .encode_credentials(encrypt_token(person.tc_social_token))
        get :personal_info, params: { id: person.id, api_key: API_CONFIG["API_KEY"] }

        data = JSON.parse(response.body)["data"]

        expect(data["address"]["country_code"]).to eq("US")
        expect(data["address"]["address1"]).to eq("address line 1")
        expect(data["address"]["address2"]).to eq("address line 2")
      end
    end
  end

  describe 'GET #non_self_identified_countries' do
    context "with successfull authentication" do
      it "returns list of non_self_identified_countries for the user" do
        person = create(:person, :with_tc_social)
        create(:stored_credit_card, person: person, country: "AU")

        controller.request.env["HTTP_AUTHORIZATION"] =
          ActionController::HttpAuthentication::Token
          .encode_credentials(encrypt_token(person.tc_social_token))
        get :non_self_identified_countries, params: {
          id: person.id, api_key:
          API_CONFIG["API_KEY"]
        }

        data = JSON.parse(response.body)["data"]
        expect(data.count).to eq(1)
        expect(data.first["name"]).to eq("Australia")
      end
    end
  end
end
