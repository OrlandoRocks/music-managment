require "rails_helper"

describe Api::RoyaltySolutionsController, type: :controller do
  before(:each) do
    @person = FactoryBot.create(:person)
  end
  describe "#verify_params" do
    it "should respond with missing params email on validate_account and should send an airbrake notification" do
      expect(Airbrake).to receive(:notify)
      get :validate_account, params: { api_key: API_CONFIG["royalty_solutions"]["API_KEY"] }
      json = JSON.parse(@response.body)
      expect(json["message"]).to eq("Required Params:email")
      expect(json["status"]).to eq("failure")
      expect(json["timestamp"]).not_to be_empty
      expect(@response).to have_http_status(:unprocessable_entity)
    end

    it "should respond with missing params on non validate_account calls and should send an airbrake notification" do
      expect(Airbrake).to receive(:notify)
      get :register, params: { api_key: API_CONFIG["royalty_solutions"]["API_KEY"] }
      json = JSON.parse(@response.body)
      expect(json["message"]).to eq("Required Params:email upcs isrcs")
      expect(json["status"]).to eq("failure")
      expect(json["timestamp"]).not_to be_empty
      expect(@response).to have_http_status(:unprocessable_entity)
    end
  end

  describe "#validate_account" do
    it "should respond with success message on valid email" do
      get :validate_account, params: { api_key: API_CONFIG["royalty_solutions"]["API_KEY"], email: @person.email }
      json = JSON.parse(@response.body)
      expect(json["message"]).to eq("OK")
      expect(json["status"]).to eq("success")
      expect(json["timestamp"]).not_to be_empty
      expect(@response).to have_http_status(:ok)
    end

    it "should respond with failure message for an invalid email and should send an airbrake notification" do
      expect(Airbrake).to receive(:notify)
      get :validate_account, params: { api_key: API_CONFIG["royalty_solutions"]["API_KEY"], email: @person.email + ".foo.bar" }
      json = JSON.parse(@response.body)
      expect(json["message"]).to eq("Not Found")
      expect(json["status"]).to eq("failure")
      expect(json["timestamp"]).not_to be_empty
      expect(@response).to have_http_status(:unprocessable_entity)
    end

    it "should respond with success message on suspicious account" do
      @suspicious_person = FactoryBot.create(
        :person,
        email: "suspicious_customer@example.com",
        status: "Suspicious"
      )
      get :validate_account, params: { api_key: API_CONFIG["royalty_solutions"]["API_KEY"], email: @suspicious_person.email }
      json = JSON.parse(@response.body)
      expect(json["message"]).to eq("OK")
      expect(json["status"]).to eq("success")
      expect(json["timestamp"]).not_to be_empty
      expect(@response).to have_http_status(:ok)
    end
  end

  describe "#validate_assets" do
    it "should call model method" do
      expect(RoyaltySolutions).to receive(:validate_assets).and_return({ message: "OK", status: "success", timestamp: Time.now })
      get :validate_assets, params: { api_key: API_CONFIG["royalty_solutions"]["API_KEY"], email: @person.email, upcs: "", isrcs: "" }
      json = JSON.parse(@response.body)
      expect(json["message"]).to eq("OK")
      expect(json["status"]).to eq("success")
      expect(json["timestamp"]).not_to be_empty
    end

    it "should return a success status if the registration was successful" do
      expect(RoyaltySolutions).to receive(:validate_assets).and_return({ message: "OK", status: "success", timestamp: Time.now })
      get :validate_assets, params: { api_key: API_CONFIG["royalty_solutions"]["API_KEY"], email: @person.email, upcs: "", isrcs: "" }
      expect(@response).to have_http_status(:ok)
    end

    it "should return a failure status if the registration was unsuccessful and should send an airbrake notification" do
      expect(Airbrake).to receive(:notify)
      expect(RoyaltySolutions).to receive(:validate_assets).and_return({ message: "not found", status: "failure", timestamp: Time.now })
      get :validate_assets, params: { api_key: API_CONFIG["royalty_solutions"]["API_KEY"], email: @person.email, upcs: "", isrcs: "" }
      expect(@response).to have_http_status(:unprocessable_entity)
    end
  end

  describe "#register" do
    it "should call model method" do
      expect(RoyaltySolutions).to receive(:register).and_return({ message: "OK", status: "success", timestamp: Time.now })
      get :register, params: { api_key: API_CONFIG["royalty_solutions"]["API_KEY"], email: @person.email, upcs: "", isrcs: "" }
      json = JSON.parse(@response.body)
      expect(json["message"]).to eq("OK")
      expect(json["status"]).to eq("success")
      expect(json["timestamp"]).not_to be_empty
    end

    it "should return a success status if the registration was successful" do
      expect(RoyaltySolutions).to receive(:register).and_return({ message: "OK", status: "success", timestamp: Time.now })
      get :register, params: { api_key: API_CONFIG["royalty_solutions"]["API_KEY"], email: @person.email, upcs: "", isrcs: "" }
      expect(@response).to have_http_status(:ok)
    end

    it "should return a failure status if the registration was unsuccessful and should send an airbrake notification" do
      expect(Airbrake).to receive(:notify)
      expect(RoyaltySolutions).to receive(:register).and_return({ message: "not found", status: "failure", timestamp: Time.now })
      get :register, params: { api_key: API_CONFIG["royalty_solutions"]["API_KEY"], email: @person.email, upcs: "", isrcs: "" }
      expect(@response).to have_http_status(:unprocessable_entity)
    end
  end
end
