require "rails_helper"

describe Api::V3::PeopleFlagsController, type: :controller do
  let(:person) { create(:person) }
  let(:flag_reason) { build(:flag_reason) }

  before(:each) do
    allow(subject).to receive(:authenticate_user!).and_return(true)
    allow(subject).to receive(:authorize_content_review_users!).and_return(true)
    allow(subject).to receive(:current_user) { person }
    Timecop.freeze(Time.parse('2017-01-03').in_time_zone)
  end

  after do
    Timecop.return
  end

  describe "#create" do
    context "when valid person_ids are passed in" do
      it "should return valid people" do
        params = {
          person_ids: person.id.to_s,
          flag_name: "suspicious",
          flag_reason: flag_reason.reason
        }

        post :create, params: params

        people_flag = person.reload.people_flags.first
        flag_transition = people_flag.flag_transitions.first

        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)).to include(
          {
            people_flags: [{
              id: person.id,
              name: person.name,
              email: person.email,
              country: person.country,
              roles: [],
              balance: "0.0",
              available_balance: "0.0",
              lock_reason: person.lock_reason,
              status_updated_at: person.status_updated_at,
              people_flags: [{
                id: people_flag.id,
                flag: { id: people_flag.flag_id, name: "suspicious" },
                created_at: people_flag.created_at,
                flag_transitions: [
                  {
                    id: flag_transition.id,
                    add_remove: flag_transition.add_remove,
                    flag_reason: nil
                  }
                ]
              }]
            }]
          }.with_indifferent_access
        )
      end
    end

    context "when invalid person_ids are passed in" do
      it "should return valid people" do
        invalid_email = "invalid@email.com"
        params = {
          person_ids: invalid_email,
          flag_name: "suspicious",
          flag_reason: flag_reason.reason
        }

        post :create, params: params

        expect(response.status).to eq(422)
        expect(JSON.parse(response.body)).to include(
          {
            "invalid_people" => [invalid_email]
          }.with_indifferent_access
        )
      end
    end

    context "when invalid person_ids are passed in" do
      it "should return valid people" do
        invalid_email = "invalid@email.com"
        params = {
          person_ids: invalid_email,
          flag_name: "suspicious",
          flag_reason: flag_reason.reason
        }

        post :create, params: params

        expect(response.status).to eq(422)
        expect(JSON.parse(response.body)).to include(
          {
            "invalid_people" => [invalid_email]
          }.with_indifferent_access
        )
      end
    end
  end

  describe "#destroy" do
    context "when valid person_ids are passed in" do
      it "should return valid people" do
        params = {
          person_ids: person.id.to_s,
          flag_name: "suspicious",
          flag_reason: flag_reason.reason
        }

        post :destroy, params: params

        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)).to eq(
          {
            people_flags: [{
              id: person.id,
              name: person.name,
              email: person.email,
              country: person.country,
              roles: [],
              people_flags: [],
              balance: "0.0",
              available_balance: "0.0",
              lock_reason: nil,
              status_updated_at: person.created_on.as_json
            }]
          }.with_indifferent_access
        )
      end
    end

    context "when invalid person_ids are passed in" do
      it "should return valid people" do
        invalid_email = "invalid@email.com"
        params = {
          person_ids: invalid_email,
          flag_name: "suspicious",
          flag_reason: flag_reason.reason
        }

        post :create, params: params

        expect(response.status).to eq(422)
        expect(JSON.parse(response.body)).to include(
          {
            "invalid_people" => [invalid_email]
          }.with_indifferent_access
        )
      end
    end
  end
end
