require "rails_helper"

describe Api::V3::RefundsController, type: :controller do
  let(:person) { create(:person) }
  let(:admin) { create(:person, :with_refund_role) }

  before(:each) do
    allow(subject).to receive(:authenticate_user!).and_return(true)
    allow(subject).to receive(:authorize_refund_users!).and_return(true)
    allow(subject).to receive(:current_user) { admin }
    allow(FeatureFlipper).to receive(:show_feature?).and_call_original
    allow(FeatureFlipper).to receive(:show_feature?).with(:new_refunds, admin).and_return(true)
  end

  describe "#list_transactions" do
    let(:invoice) { create(:invoice, :settled, person: person) }

    it "should respond with transaction list" do
      get :list_transactions, params: { id: invoice.id }

      transactions = JSON.parse(response.body)["transactions"]
      expect(transactions.count).to eq(1)
      expect(transactions.first["id"]).to eq(invoice.id)
      expect(transactions.first["name"]).to eq(person.name)
    end
  end

  describe "#create" do
    let(:invoice) { create(
      :invoice,
      :with_two_purchases,
      :settled,
      :with_balance_settlement) }

    let!(:exchange_rate) { create(
        :foreign_exchange_rate,
        source_currency: CurrencyCodeType::USD,
        target_currency: CurrencyCodeType::EUR,
        exchange_rate: 0.85
      ) }

    let(:refund_reason) { create(:refund_reason) }

    it "should create refund" do
      post :create, params: {
        invoice_id: invoice.id,
        refund_level: Refund::INVOICE,
        refund_type: Refund::FULL_REFUND,
        tax_inclusive: true,
        refund_reason_id: refund_reason.id
       }, as: :json

      response_body = JSON.parse(response.body)
      expect(response_body["message"]).to eq("refund created")
      expect(response_body["refund_id"]).to eq(invoice.refunds.last.id)
    end

    it "should not create refund with refund amount greater than invoice amount" do
      post :create, params: {
        invoice_id: invoice.id,
        refund_level: Refund::INVOICE,
        refund_type: Refund::FIXED_AMOUNT,
        request_amount: invoice.final_settlement_amount.to_f + 1,
        tax_inclusive: true,
        refund_reason_id: refund_reason.id
       }, as: :json

      response_body = JSON.parse(response.body)
      expect(response_body["status"]).to eq("failure")
      expect(response_body["refund_id"]).to eq(nil)
    end
  end
end
