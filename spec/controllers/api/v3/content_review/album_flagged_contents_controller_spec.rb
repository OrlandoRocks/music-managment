require "rails_helper"

describe Api::V3::ContentReview::AlbumFlaggedContentsController, type: :controller do
  let(:person) { create(:person) }
  let(:album)     { create(:album, :with_uploaded_songs_and_content_fingerprint, :finalized) }
  let(:song)      { album.songs.last }
  let(:dup_album) {create(:album, :with_songs)}
  let(:dup_song) {create(:song, album: dup_album )}
  let(:scrapi_job) { FactoryBot.create(:scrapi_job, song: song) }

  before(:each) do
    allow(subject).to receive(:authenticate_user!).and_return(true)
    allow(subject).to receive(:authorize_content_review_users!).and_return(true)
    allow(subject).to receive(:current_user) { person }
  end

  describe "#expand" do
    context "when both album_flagged_content and audio_fingerprint are requested" do
      it "should return both the objects" do
        dup_song.update(content_fingerprint: song.content_fingerprint)
        album.flag_duplicate_content
        scrapi_job_detail = FactoryBot.create(:scrapi_job_detail, scrapi_job: scrapi_job)
        flagged_data_params = {album_flagged_content_id:[album.reload.album_flagged_content.id], scrapi_job_ids:[scrapi_job.id]}

        expanded_flagged_data = {
          "expanded_scrapi_jobs_details"=>
          [{"flagged_song_id"=>song.id,
            "artists"=>[{"name"=>scrapi_job_detail.artists.first}],
            "flagged_song_name"=>song.name,
            "matched_song_name"=>scrapi_job_detail.track_title,
            "matched_song_album_name"=>scrapi_job_detail.album_title,
            "matched_album_label"=>scrapi_job_detail.label,
            "id"=>scrapi_job_detail.id}],
         "expanded_flagged_content"=>
          [{"matched_song_id"=>dup_song.id,
            "matched_song_name"=>dup_song.name,
            "matched_song_album_name"=>dup_album.name,
            "matched_album_label"=>dup_album.label_name,
            "matched_song_album_id"=>dup_album.id,
            "flagged_song_name"=>song.name,
            "flagged_song_id"=>song.id,
            "artists"=>[{"name"=>dup_album.artists.map(&:name).first}]}]
          }
        get :expand, params: flagged_data_params
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body).count).to eq(2)
        expect(JSON.parse(response.body)).to eq expanded_flagged_data
      end
    end

    context "when only audio_fingerprint are requested" do
      it "it should return audio_fingerprint object" do
        flagged_data_params = {scrapi_job_ids:[scrapi_job.id]}
        FactoryBot.create(:scrapi_job_detail, scrapi_job: scrapi_job)
        get :expand, params: flagged_data_params
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)["expanded_scrapi_jobs_details"][0]["matched_song_name"]).to eq("Bravo")

      end
    end
  end
end
