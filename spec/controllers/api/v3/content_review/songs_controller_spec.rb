require "rails_helper"

describe Api::V3::ContentReview::SongsController, type: :controller do
  let(:person) { create(:person) }
  before(:each) do
    allow(subject).to receive(:authenticate_user!).and_return(true)
    allow(subject).to receive(:authorize_content_review_users!).and_return(true)
    allow(subject).to receive(:current_user) { person }
  end

  describe "#update" do
    it "should update the song attributes" do
      album = create(:album, :with_petri_bundle, :with_uploaded_songs, number_of_songs: 2, legal_review_state: 'NEEDS REVIEW', finalized_at: Time.now)
      song = album.songs.first

      expect(song.name).to eq("This Is a Song")
      expect(song.parental_advisory).to be nil
      expect(song.instrumental).to be false
      expect(song.creatives).to be_empty

      post :update, params: {
        id: song.id,
        song_attributes: {
          id: song.id,
          name: "Updated Name",
          explicit: true,
          instrumental: true,
          artists: [
            {artist_name: "Primary Artist 1", associated_to: "Song", credit: "primary_artist", role_ids: []},
            {artist_name: "Featuring Artist 1", associated_to: "Song", credit: "featuring", role_ids: []},
            {artist_name: "Contributor 1", associated_to: "Song", credit: "contributor", role_ids: []},
            {artist_name: "Songwriter 1", associated_to: "Song", credit: "songwriter", role_ids: [3]},
          ]
        }
      }, as: :json

      song.reload

      expect(song.name).to eq("Updated Name")
      expect(song.parental_advisory).to be true
      expect(song.instrumental).to be true
      expect(song.primary_artists.first.name).to eq("Primary Artist 1")
      expect(song.featurings.first.name).to include("Featuring Artist 1")
      expect(song.contributors.first.name).to include("Contributor 1")
    end

    context "when the release is a single" do
      it "should save the album" do
        single = create(:single)
        song = create(:song, :with_s3_asset, :with_upload, :with_language_code, album: single)
        song_data_form_dbl = double(SongDataForm, save: true)

        allow(SongDataForm).to receive(:new).and_return(song_data_form_dbl)
        allow(Song).to receive(:find).and_return(song)
        allow(song).to receive(:reload).and_return(true)

        expect(single).to receive(:save)

        post :update, params: {
          id: song.id,
          song_attributes: {
            id: song.id
          }
        }, as: :json
      end
    end

    context "when the release is a ringtone" do
      it "should save the album" do
        ringtone = create(:ringtone)
        song = create(:song, :with_s3_asset, :with_upload, :with_language_code, album: ringtone)
        song_data_form_dbl = double(SongDataForm, save: true)

        allow(SongDataForm).to receive(:new).and_return(song_data_form_dbl)
        allow(Song).to receive(:find).and_return(song)
        allow(song).to receive(:reload).and_return(true)

        expect(ringtone).to receive(:save)

        post :update, params: {
          id: song.id,
          song_attributes: {
            id: song.id
          }
        }, as: :json
      end
    end

    context "when retrying audio fingerprint" do
      it "should create scrapi job" do
        album = create(:album, :with_petri_bundle, :with_uploaded_songs, number_of_songs: 2, legal_review_state: 'NEEDS REVIEW', finalized_at: Time.now)
        song = album.songs.first
        expect(Scrapi::CreateJobWorker).to receive(:perform_async).with(song.id)
        put :retry_audio_fingerprint, params: {song_id: song.id}
      end
    end
  end
end
