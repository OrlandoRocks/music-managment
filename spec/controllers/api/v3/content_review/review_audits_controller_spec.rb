require "rails_helper"

describe Api::V3::ContentReview::ReviewAuditsController, type: :controller do
  let(:person) { create(:person, :admin) }

  before(:each) do
    allow(subject).to receive(:authenticate_user!).and_return(true)
    allow(subject).to receive(:authorize_content_review_users!).and_return(true)
    allow(subject).to receive(:current_user) { person }
  end

  describe "#create" do
    context "when approving a release" do
      let(:album) do
        create(
          :album,
          :with_petri_bundle,
          :with_uploaded_songs,
          number_of_songs: 2,
          legal_review_state: 'NEEDS REVIEW',
          finalized_at: Time.now
        )
      end

      context "when all appropriate params are passed in" do
        it "should return a successful response" do
          params = { album_id: album.id, event: "APPROVED" }

          post :create, params: params

          expect(response).to have_http_status(201)
        end

        it "should create an approved review_audit" do
          params = { album_id: album.id, event: "APPROVED" }

          expect {
            post :create, params: params
          }.to change(ReviewAudit.where(album_id: album.id, event: "APPROVED"), :count).by(1)
        end
      end

      context "when all appropriate params are NOT passed in" do
        it "should not create an approved review_audit" do
          params = { album_id: album.id }

          expect {
            post :create, params: params
          }.to change(ReviewAudit.where(album_id: album.id, event: "APPROVED"), :count).by(0)
        end
      end
    end

    context "when rejecting a release" do
      it "should create a rejected Review Audit" do
        album = create(:album)
        role = Role.find_by(name: "Content Review Manager")
        person.roles << role
        review_reason = create(:review_reason, roles: [role])
        note = "I am the note"

        params = {
          album_id: album.id,
          event: "REJECTED",
          review_reason_ids: [review_reason.id],
          person_id: person.id,
          note: note,
        }

        expect {
          post :create, params: params
        }.to change {
          album.reload.review_audits.count
        }.by(1)

        review_audit = album.reload.review_audits.last

        expect(review_audit.note).to eq note
        expect(review_audit.album).to eq album
        expect(review_audit.event).to eq "REJECTED"
        expect(review_audit.review_reasons.first).to eq review_reason
      end

      it "correctly returns serialized data" do
        album = create(:album)
        role = Role.find_by(name: "Content Review Manager")
        person.roles << role
        review_reason = create(:review_reason, roles: [role])
        note = "I am the note"

        params = {
          album_id: album.id,
          event: "REJECTED",
          review_reason_ids: [review_reason.id],
          person_id: person.id,
          note: note,
        }

        post :create, params: params

        expect(response_json["note"]).to eq note
        expect(response_json["event"]).to eq "REJECTED"
        expect(response_json.dig("review_reasons", 0, "reason")).to eq review_reason.reason
        expect(response_json.dig("person", "name")).to eq person.name
      end

      it "returns the errors if the Review Audit is invalid" do
        album = create(:album)
        role = Role.find_by(name: "Content Review Manager")
        person.roles << role
        review_reason = create(:review_reason)

        params = {
          album_id: album.id,
          event: "REJECTED",
          review_reason_ids: [review_reason.id],
          person_id: person.id,
        }

        post :create, params: params

        expect(response_json["base"]).to be_present
      end
    end
  end
end
