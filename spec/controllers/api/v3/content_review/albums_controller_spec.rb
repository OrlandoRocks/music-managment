require "rails_helper"

describe Api::V3::ContentReview::AlbumsController, type: :controller do
  let(:person) { create(:person) }
  before(:each) do
    allow(subject).to receive(:authenticate_user!).and_return(true)
    allow(subject).to receive(:authorize_content_review_users!).and_return(true)
    allow(subject).to receive(:current_user) { person }
  end

  describe "#index" do
    context "when no params are passed in" do
      it "should invoke the crt_list_view_query with no params" do
        expect(Album).to receive(:send_chain).with([[:crt_list_view_query, "albums.finalized_at", "asc", nil]]).and_call_original
        get :index
      end
    end

    context "when a search_term and search_by field are passed in as params" do
      it "should invoke the crt_list_view_query with the proper search_by query" do
        crt_list_view_params = {
          search_params: {
            search_term: "Search Term",
            search_by: "album_name"
          }
        }.stringify_keys

        expect(Album).to receive(:send_chain)
          .with([[:crt_list_view_query, "albums.finalized_at", "asc", nil], [:search_by_album_name, "Search Term"]])
          .and_call_original

        get :index, params: crt_list_view_params
      end
    end

    context "when searching for albums by a date range field" do
      it "should not invoke the date range field search unless the start and end date are present" do
        crt_list_view_params = {
          search_params: {
            date_range_field: "album_id"
          }
        }.stringify_keys

        expect(Album).to receive(:send_chain)
          .with([[:crt_list_view_query, "albums.finalized_at", "asc", nil]])
          .and_call_original

        get :index, params: crt_list_view_params
      end

      it "should not invoke the date range field search unless date range field is present" do
        crt_list_view_params = {
          search_params: {
            range_date_start: "2020-01-01",
            range_date_end: "2020-01-10"
          }
        }.stringify_keys

        expect(Album).to receive(:send_chain)
          .with([[:crt_list_view_query, "albums.finalized_at", "asc", nil]])
          .and_call_original

        get :index, params: crt_list_view_params
      end
    end

    context "when all the applicable parameters are passed in" do
      it "should invoke the crt_list_view_query with the appropriate queries" do
        range_start_date = 1.year.ago.localtime.to_s
        range_end_date = Time.current.to_s
        date_range_field = "album_name"
        legal_review_state = "APPROVED"
        language_code = "en"
        country_website_id = "1"
        search_by = "artist_name"
        search_term = "Yeezus"
        sort_by = "albums.finalized_at"
        sort_order = "asc"

        crt_list_view_params = {
          search_params: {
            range_start_date: range_start_date,
            range_end_date: range_end_date,
            date_range_field: date_range_field,
            legal_review_state: legal_review_state,
            country_website_id: country_website_id,
            language_code: language_code,
            search_by: search_by,
            search_term: search_term,
            sort_by: sort_by,
            sort_order: sort_order
          }
        }.stringify_keys

        search_by_date_range_args = { start_date: range_start_date, end_date: range_end_date }
        crt_list_view_queries = [
          [:crt_list_view_query, sort_by, sort_order, "APPROVED"],
          [:search_by_legal_review_state, legal_review_state],
          [:search_by_language_code, language_code],
          [:search_by_country_website_id, country_website_id],
          ["search_by_#{search_by}".to_sym, search_term],
          ["search_by_#{date_range_field}".to_sym, search_by_date_range_args]
        ]

        expect(Album).to receive(:send_chain)
          .with(crt_list_view_queries)
          .and_call_original

        get :index, params: crt_list_view_params
      end
    end
  end

  describe "#album_for_review" do
    context "when a user has albums to review" do
      it "should return an album to review in the JSON response" do
        album = create(:album, :with_petri_bundle, :with_uploaded_songs, :with_one_year_renewal, number_of_songs: 2, legal_review_state: 'NEEDS REVIEW', finalized_at: Time.now)
        get :album_for_review
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)['name']).to eq(album.name)
      end

      it "should return without account_sift_score" do
        album = create(:album, :with_petri_bundle, :with_uploaded_songs, :with_one_year_renewal, number_of_songs: 2, legal_review_state: 'NEEDS REVIEW', finalized_at: Time.now)
        get :album_for_review
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)['account_sift_score']).to eq(nil)
      end

      it "should return with account_sift_score round down" do
        album = create(:album, :with_petri_bundle, :with_uploaded_songs, :with_one_year_renewal, number_of_songs: 2, legal_review_state: 'NEEDS REVIEW', finalized_at: Time.now, person: person)
        person_sift_score = FactoryBot.create(:person_sift_score, person: person, score: 0.94499999)
        get :album_for_review
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)['account_sift_score']).to eq(94)
      end

      it "should return with account_sift_score round up" do
        album = create(:album, :with_petri_bundle, :with_uploaded_songs, :with_one_year_renewal, number_of_songs: 2, legal_review_state: 'NEEDS REVIEW', finalized_at: Time.now, person: person)
        person_sift_score = FactoryBot.create(:person_sift_score, person: person, score: 0.94599999)
        get :album_for_review
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)['account_sift_score']).to eq(95)
      end

      it "should return with total_track_length_in_seconds and scrapi_jobs_stats" do
        album = create(:album, :with_petri_bundle, :with_uploaded_songs, :with_one_year_renewal, number_of_songs: 2, legal_review_state: 'NEEDS REVIEW', finalized_at: Time.now, person: person)
        get :album_for_review
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)['total_track_length_in_seconds']).to eq(album.total_track_length_in_seconds)
        expect(JSON.parse(response.body)['scrapi_jobs_stats']).to eq({"completed"=>0, "total"=>2})
      end
    end

    context "when a user has no albums to review" do
      it "should return with no album" do
        album = create(:album, :with_petri_bundle, :with_uploaded_songs, number_of_songs: 2, legal_review_state: 'NEEDS REVIEW')
        get :album_for_review
        expect(response.status).to eq(404)
      end
    end

    context "when the album of release_id in the url is not finalized" do
      it "should return album not found" do
        album = create(:album, :with_uploaded_songs)
        get :album_for_review, params: { id: album.id }
        expect(response.status).to eq(404)
      end
    end
  end

  describe "#update" do
    let(:album) {
      create(
        :album,
        :with_petri_bundle,
        :with_uploaded_songs,
        :with_one_year_renewal,
        name: "Album 1",
        label_name: "Label 1",
        language_code: "de",
        primary_genre: Genre.find_by(name: "Folk"),
        golive_date: Date.today,
        timed_release_timing_scenario: "absolute_time",
        number_of_songs: 2,
        creatives: [{ "name" => "Test Artist", "role" => "primary_artist" }],
        legal_review_state: 'NEEDS REVIEW'
      )
    }
    context "When a user tries to update an album" do
      it "should update the album attributes" do
        name = "Update Album Name"
        label_name = "Update label name"
        metadata_language_code_id = "1"
        primary_genre_id = Genre.find_by(name: "Rock").id
        secondary_genre_id = Genre.find_by(name: "Pop").id
        golive_date = (Date.today + 10.days)
        timed_release_timing_scenario = "relative_time"
        new_creative_name = "New Test Artist"
        creatives = [{ name: new_creative_name, role: "primary_artist" }]

        post :update,
             params: {
               id: album.id,
               album: {
                 name: name,
                 label_name: label_name,
                 metadata_language_code_id: metadata_language_code_id,
                 primary_genre_id: primary_genre_id,
                 secondary_genre_id: secondary_genre_id,
                 golive_date: golive_date.strftime("%F %H:%M"),
                 timed_release_timing_scenario: timed_release_timing_scenario,
                 creatives: creatives
               }
             }

        album.reload

        expect(album.name).to eql(name)
        expect(album.label_name).to eql(label_name)
        expect(album.metadata_language_code_id).to eql(metadata_language_code_id.to_i)
        expect(album.primary_genre_id).to eql(primary_genre_id)
        expect(album.secondary_genre_id).to eql(secondary_genre_id)
        expect(album.golive_date).to eql(golive_date.to_time)
        expect(album.timed_release_timing_scenario).to eql(timed_release_timing_scenario)
        expect(album.creatives.first.name).to eql(new_creative_name)
        expect(album.language_code).to eql('en')
      end
    end

    context "when updating an album to is_various" do
      it "should set is_various to true" do
        expect(album.is_various).to eq false

        post :update,
             params: {
               id: album.id,
               album: {
                 is_various: true,
               }
             }

        expect(album.reload.is_various).to eq true
      end
    end

    context "when updating an album" do
      it "should create a note describing album changes" do

        name = "Update Album Name"
        metadata_language_code_id = "1"
        primary_genre_id = Genre.find_by(name: "Rock").id
        secondary_genre_id = Genre.find_by(name: "Pop").id
        golive_date = (Date.today + 10.days)
        timed_release_timing_scenario = "relative_time"

        post :update,
             params: {
               id: album.id,
               album: {
                 name: name,
                 metadata_language_code_id: metadata_language_code_id,
                 primary_genre_id: primary_genre_id,
                 secondary_genre_id: secondary_genre_id,
                 golive_date: golive_date.strftime("%F %H:%M"),
                 timed_release_timing_scenario: timed_release_timing_scenario,
               }
             }

        album.reload
        note = album.notes.last 

        expect(note.note_created_by_id).to eq person.id
        expect(note.note).to include("name", "metadata_language_code_id", "primary_genre_id","secondary_genre_id", "golive_date", "timed_release_timing_scenario")
      end
    end
  end
end
