require "rails_helper"

describe Api::V3::ContentReview::PeopleController, type: :controller do
  let(:person) { create(:person) }
  let!(:person_plan) { create(:person_plan, person: person)}
  before(:each) do
    allow(subject).to receive(:authenticate_user!).and_return(true)
    allow(subject).to receive(:authorize_admin_user!).and_return(true)
    allow(subject).to receive(:current_user) { person }
  end


  describe "#downgrade_eligibility" do 
    context "with a eligible person and plan" do
      it "should return an array of lower plans" do
        response = get :plan_downgrade_eligibility, params: {
          person_id: person.id
        }
        parsed_response = JSON.parse(response.body)["people"]
        expect(parsed_response).to be_an_instance_of(Array)
        expect(parsed_response.length).to eq(person.plan.lower_plan_ids.length)
      end
    end

    context "with invalid person id" do
      it "should returrn an error" do
        response = get :plan_downgrade_eligibility, params: {
          person_id: 0
        }
        expect(response.status).to eq(404)
        response_body = JSON.parse(response.body)["error"]
        expect(response_body).to eq("Person 0 not found")
      end
    end

    context "with invalid plan" do
      it "should returrn an error" do
        person.plan = nil
        response = get :plan_downgrade_eligibility, params: {
          person_id: person.id
        }
        expect(response.status).to eq(404)
        response_body = JSON.parse(response.body)["error"]
        expect(response_body).to eq("PersonPlan for user_id #{person.id} not found")
      end
    end
  end
end
