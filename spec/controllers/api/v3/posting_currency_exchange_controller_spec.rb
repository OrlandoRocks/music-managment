require "rails_helper"

describe Api::V3::PostingCurrencyExchangeController, type: :controller do
  let(:person) { create(:person, :admin) }
  posting_id = "posting_id"
  before(:each) do
    allow(subject).to receive(:authenticate_user!).and_return(true)
    allow(subject).to receive(:current_user) { person }
    Royalties::Posting.add(posting_id)
    Royalties::Posting.update_state(Royalties::Posting::FINANCE_APPROVAL_REQUESTED)
  end
  hash =
    [
      {"base_currency" => 'USD', "currency" => 'AUD', "fx_rate" => 1.2},
      {"base_currency" => 'USD', "currency" => 'CAD', "fx_rate" => 1.25},
      {"base_currency" => 'USD', "currency" => 'EUR', "fx_rate" => 0.90},
      {"base_currency" => 'USD', "currency" => 'GBP', "fx_rate" => 0.85, "coverage_rate" => 3},
      {"base_currency" => 'USD', "currency" => 'USD', "fx_rate" => 1}
    ]

  after(:each) do
    Royalties::Posting.remove
  end

  describe "#currency exchange" do

    context "update and save currency exchange value" do

      it "should fetch posting royalties if it's in finance approved state" do
        posting_royalties = create_list(
          :posting_royalty,
          3,
          posting_id: "posting_id",
          total_amount_in_usd: 100,
          encumbrance_amount_in_usd: 2,
          split_adjustments_in_usd: 0
          )
        get :get_current_eligible_posting_royalties
        expect(response.status).to eq(200)

      end

      it "should not fetch posting royalties if it's in finance approved state" do
        posting_royalties = create_list(
          :posting_royalty,
          3,
          posting_id: "posting_id",
          total_amount_in_usd: 100,
          encumbrance_amount_in_usd: 2,
          split_adjustments_in_usd: 0
          )
        Royalties::Posting.update_state(Royalties::Posting::STARTED)

        get :get_current_eligible_posting_royalties
        expect(response.status).to eq(404)

      end

      it "should calculate the exchange rates based on the given hash" do
        posting_royalties = create_list(
          :posting_royalty,
          3,
          posting_id: "posting_id",
          total_amount_in_usd: 100,
          encumbrance_amount_in_usd: 2,
          split_adjustments_in_usd: 0
          )
        params = {exchange_rates: hash}
        put :apply_currency_exchange, :params => params
        expect(response.status).to eq(200)
        PostingRoyalty.all.each do |royalty|
          expect(royalty.converted_amount.to_i).to eq 102
          expect(royalty.state).to eq 'submitted'
          expect(royalty.fx_rate_submitted_by).to eq person.name
          expect(royalty.approved_by).to eq nil
        end
      end

      it "should update the postings' state to pending and set the fx_submitted_by to nil" do
        posting_royalties = create_list(
          :posting_royalty,
          3,
          posting_id: "posting_id",
          total_amount_in_usd: 100,
          encumbrance_amount_in_usd: 2,
          split_adjustments_in_usd: 0,
          state: 'submitted',
          fx_rate_submitted_by: person.name,
          )

        put :make_currency_exchange_editable
        expect(response.status).to eq(200)
        PostingRoyalty.all.each do |royalty|
          expect(royalty.state).to eq 'pending'
          expect(royalty.fx_rate_submitted_by).to eq nil
          expect(royalty.approved_by).to eq nil
        end
      end

      it "should send the report" do
        posting_royalties = create_list(
          :posting_royalty,
          3,
          posting_id: "posting_id",
          total_amount_in_usd: 100,
          encumbrance_amount_in_usd: 2,
          split_adjustments_in_usd: 0,
          state: 'submitted',
          fx_rate_submitted_by: person.name,
          )

        get :export_posting_data
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)["error"]).to eq nil
      end

      it "should update the postings to approved state and post on approval" do
        posting_royalties = create_list(
          :posting_royalty,
          3,
          posting_id: "posting_id",
          total_amount_in_usd: 100,
          encumbrance_amount_in_usd: 2,
          split_adjustments_in_usd: 0,
          state: 'submitted',
          fx_rate_submitted_by: person.name,
          )

        put :approve_currency_exchange
        expect(response.status).to eq(200)
        PostingRoyalty.all.each do |royalty|
          expect(royalty.state).to eq 'approved'
          expect(royalty.approved_by).to eq person.name
        end
      end

    end
  end
end
