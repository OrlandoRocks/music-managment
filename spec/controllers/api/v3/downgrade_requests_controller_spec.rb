require "rails_helper"

describe Api::V3::DowngradeRequestsController, type: :controller do
  let(:person) { create(:person) }

  before(:each) do
    allow(subject).to receive(:authenticate_user!).and_return(true)
    allow(subject).to receive(:authorize_admin_user!).and_return(true)
  end

  describe "#plan_downgrade_requests" do
    context "person with requested plan downgrades" do
      let!(:person_plan) { create(:person_plan, person: person) }
      let!(:downgrade_category) { create(:downgrade_category) }
      let!(:downgrade_category_reason) { create(:downgrade_category_reason, downgrade_category: downgrade_category) }
      let!(:plan_downgrade_request) { create(:plan_downgrade_request, person: person) }
      let!(:person_plan_history) { create(:person_plan_history, person: person) }

      it "returns all requested plan downgrades" do
        params = {
          id: person.id
        }

        get :show, params: params
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)).to include(
          {
            id: person.id,
            name: person.name,
            email: person.email,
            status: person.status,
            current_plan: {
              name: person_plan.name,
              created_at: person_plan_history.plan_start_date,
              expires_at: person_plan_history.plan_end_date
            }.with_indifferent_access,
            plan_downgrade_requests: [
              {
                id: plan_downgrade_request.id,
                status: plan_downgrade_request.status,
                date: plan_downgrade_request.created_at.as_json,
                name: plan_downgrade_request.requested_plan.name,
                reason_type: plan_downgrade_request.reason.downgrade_category.name,
                reason: plan_downgrade_request.reason.name,
                reason_detail: plan_downgrade_request&.downgrade_other_reason&.description,
              }
            ]
          }.with_indifferent_access
        )
      end
    end

    context "person with current and without requested plan downgrades" do
      let!(:person_plan) { create(:person_plan, person: person) }
      let!(:person_plan_history) { create(:person_plan_history, person: person) }
      it "returns the current plan and an empty list for requested plan downgrades" do
        params = {
          id: person.id
        }

        get :show, params: params
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)).to include(
          {
            id: person.id,
            name: person.name,
            email: person.email,
            status: person.status,
            current_plan: {
              name: person_plan.name,
              created_at: person_plan_history.plan_start_date.as_json,
              expires_at: person_plan_history.plan_end_date.as_json
            }.with_indifferent_access,
            plan_downgrade_requests: []
          }.with_indifferent_access
        )
      end
    end

    context "person without current and without requested plan downgrades" do
      it "returns the current plan and an empty list for requested plan downgrades" do
        params = {
          id: person.id
        }

        get :show, params: params
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)).to include(
          {
            id: person.id,
            name: person.name,
            email: person.email,
            status: person.status,
            current_plan: nil,
            plan_downgrade_requests: []
          }.with_indifferent_access
        )
      end
    end

    context "create a plan downgrade request" do
      let!(:plan) { create(:plan) }
      let!(:reason) { create(:downgrade_category_reason) }
      it "returns unprocessable entity status if the person does not have any plan" do
        params = {
          person_id: person.id,
          requested_plan_id: plan.id,
          reason_id: reason.id
        }

        post :create, params: params
        expect(response.status).to eq(422)
        expect(JSON.parse(response.body)).to include(
          {
            base: ["Person does not have any plan"]
          }.with_indifferent_access
        )
      end

      it "returns unprocessable entity status if plan id is not supplied" do
        params = {
          person_id: person.id,
          reason_id: reason.id
        }

        post :create, params: params
        expect(response.status).to eq(422)
        expect(JSON.parse(response.body)).to include(
          {
            requested_plan: ["can't be blank"]
          }.with_indifferent_access
        )
      end

      it "returns unprocessable entity status if reason id is not supplied" do
        params = {
          person_id: person.id,
          requested_plan_id: plan.id,
        }

        post :create, params: params
        expect(response.status).to eq(422)
        expect(JSON.parse(response.body)).to include(
          {
            reason: ["can't be blank"]
          }.with_indifferent_access
        )
      end

      it "returns unprocessable entity status if person id is not supplied" do
        params = {
          requested_plan_id: plan.id,
          reason_id: reason.id
        }

        post :create, params: params
        expect(response.status).to eq(422)
        expect(JSON.parse(response.body)).to include(
          {
            person: ["can't be blank"]
          }.with_indifferent_access
        )
      end
    end

    context "cancel a plan downgrade request" do
      let!(:person_plan) { create(:person_plan, person: person) }
      let!(:downgrade_category) { create(:downgrade_category) }
      let!(:downgrade_category_reason) { create(:downgrade_category_reason, downgrade_category: downgrade_category) }
      let!(:plan_downgrade_request) { create(:plan_downgrade_request, person: person) }

      context "no downgrade request with specified id" do
        it "responds with 404 and error message" do
          put :cancel, params: { id: "0" }
          expect(response.status).to eq(404)
          expect(JSON.parse(response.body)).to include(
            {
              message: "Downgrade Request not found"
            }.with_indifferent_access
          )
        end
      end

      context "downgrade request doesn't match person" do
        it "responds with 400 and error message" do
          put :cancel, params: {
            id: plan_downgrade_request.id,
            person_id: "0"
          }
          expect(response.status).to eq(400)
          expect(JSON.parse(response.body)).to include(
            {
              message: "Downgrade Request does not match person"
            }.with_indifferent_access
          )
        end
      end

      context "downgrade request can't be canceled" do
        let!(:plan_downgrade_request) { create(:plan_downgrade_request, :processed, person: person) }
        it "responds with 422 and error messages" do
          put :cancel, params: {
            id: plan_downgrade_request.id,
            person_id: person.id
          }, as: :json
          expect(response.status).to eq(422)
          expect(JSON.parse(response.body)).to include(
            {
              base: ["Not an active request"]
            }.with_indifferent_access
          )
        end
      end

      context "downgrade request is successfully canceled" do
        let!(:plan_downgrade_request) { create(:plan_downgrade_request, :active, person: person) }
        it "responds with 200 and success message" do
          put :cancel, params: {
            id: plan_downgrade_request.id,
            person_id: person.id
          }, as: :json
          expect(response.status).to eq(200)
          expect(JSON.parse(response.body)).to include(
            {
              status: "success"
            }.with_indifferent_access
          )
        end
      end
    end
  end
end
