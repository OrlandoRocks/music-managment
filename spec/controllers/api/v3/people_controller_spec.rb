require "rails_helper"

describe Api::V3::PeopleController, type: :controller do
  describe "#api_attributes" do
    let(:person) { create(:person, :with_crt_specialist_role) }

    context "when a valid user" do
      it "should return current_user attributes" do
        allow(subject).to receive(:authenticate_user!).and_return(true)
        allow(subject).to receive(:current_user) { person }
        get :current_user_from_token
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)).to eq(
          {
            id: person.id,
            name: person.name,
            email: person.email,
            country: person.country,
            roles: [{
              name: "Content Review Specialist",
              long_name: "Content Review Specialist",
              description: "Content Review Specialist",
              is_administrative: 1,
            }],
            people_flags: [],
            balance: "0.0",
            available_balance: "0.0",
            lock_reason: person.lock_reason,
            status_updated_at: person.status_updated_at,
          }.with_indifferent_access
        )
      end
    end

    context "when an invalid user" do
      it "should return current_user attributes" do
        allow(subject).to receive(:current_user) { person }
        get :current_user_from_token
        expect(response.status).to eq(401)
      end
    end
  end

  describe "#fetch_feature_flags_status" do
    let(:person) { create(:person) }

    it "should return feature flags status" do
      allow(subject).to receive(:authenticate_user!).and_return(true)
      allow(subject).to receive(:current_user) { person }
      allow(FeatureFlipper).to receive(:show_feature?).and_call_original
      allow(FeatureFlipper).to receive(:show_feature?).with("new_refunds", person).and_return(true)

      get :fetch_feature_flags_status, params: { feature_flags: "new_refunds,vat_tax" }

      feature_flags = JSON.parse(response.body)["feature_flags"]
      expect(feature_flags.first["flag"]).to eq("new_refunds")
      expect(feature_flags.first["enabled"]).to eq(true)
      expect(feature_flags.last["flag"]).to eq("vat_tax")
      expect(feature_flags.last["enabled"]).to eq(false)
    end
  end
end
