require 'rails_helper'

describe Api::Renderforest::ReleaseUrlsController, type: :controller do
  describe "#index" do
    context "With valid song_id" do
      it "renders a success json response with valid s3_key" do
        person = create(:person, :with_tc_social)
        controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(
          encrypt_token(person.tc_social_token)
        )
        album = create(:album, person: person)
        song = create(:song, :with_s3_asset, album: album)
        params = { song_id: song.id, s3_key: song.s3_asset.key }
        get :index, params: params
        expect(JSON.parse(response.body)["status"]).to eq("success")
        expect(JSON.parse(response.body)["data"]).to eq(song.s3_asset.original_url(1.day))
      end

      it "renders a failure json response with an invalid s3_key" do
        person = create(:person, :with_tc_social)
        controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(
          encrypt_token(person.tc_social_token)
        )
        person2 = create(:person, :with_tc_social)
        album = create(:album, person: person)
        album2 = create(:album, person: person2)
        song = create(:song, :with_s3_asset, album: album)
        song2 = create(:song, :with_s3_asset, album: album2)
        song2.s3_asset.update(key: "new_key")
        params = { song_id: song.id, s3_key: song2.s3_asset.key }
        get :index, params: params
        expect(JSON.parse(response.body)["status"]).to eq("failure")
        expect(JSON.parse(response.body)["message"]).to eq(['api.tc.message.invalid_key'])
      end
    end

    context "With an invalid song_id" do
      it "renders a failure json response" do
        person = create(:person, :with_tc_social)
        controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(
          encrypt_token(person.tc_social_token)
        )
        person2 = create(:person, :with_tc_social)
        album = create(:album, person: person2)
        song = create(:song, :with_s3_asset, album: album)
        params = { song_id: song.id, s3_key: song.s3_asset.key }
        get :index, params: params
        expect(JSON.parse(response.body)["status"]).to eq("failure")
        expect(JSON.parse(response.body)["message"]).to eq(["api.tc.message.not_users_song"])
      end
    end
  end
end
