require "rails_helper"

describe Api::Renderforest::ProjectsController do
  context "Authorized access" do
    let(:params) {
      {
        project_id: 1,
        queue_id: 1,
        status: "ok",
        timestamp: "xxx",
        message: "",
        quality: 320,
        thumbnailUrl: "https://s3.amazonaws.test/rf-test.tunecore.com/projects/1/output_360.png",
        videoUrl: "https://s3.amazonaws.test/rf-test.tunecore.com/projects/1/output_360.mp4"
      }
    }

    before do
      params_headers = "#{params[:project_id]}:#{params[:queue_id]}:#{params[:status]}:#{params[:timestamp]}"
      header = "#{params_headers}:#{ENV['SOCIAL_RENDERFOREST_API_KEY']}"
      request.headers["Authorization"] = md5_hashed_string(header)
    end

    it "responds with a success message" do
      post :project_status, params: params
      expect(JSON.parse(response.body)["status"]).to eq("success")
    end

    it "adds a Social::RenderforestRenditionWorker job" do
      expected_payload = {
        message: "",
        quality: params[:quality].to_s,
        status: params[:status],
        id: params[:project_id].to_s,
        thumbnail: params[:thumbnailUrl],
        video_url: params[:videoUrl]
      }
      expect(Social::RenderforestRenditionWorker).to receive(:perform_async).with(expected_payload.to_json)
      post :project_status, params: params
    end
  end

  context "Unauthorized access" do
    it "responds with a failure message for invalid Authorization header" do
      post :project_status
      expect(JSON.parse(response.body)["status"]).to eq("failure")
    end
  end
end

def md5_hashed_string(input)
  Digest::MD5.hexdigest(input)
end
