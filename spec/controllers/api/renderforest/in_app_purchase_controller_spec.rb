require "rails_helper"

describe Api::Renderforest::InAppPurchaseController do

  let(:purchase_date) {Date.today.to_s}
  let(:person)        { FactoryBot.create(:person, :with_tc_social) }
  let(:product)       { FactoryBot.create(:product, :rf_video_download)}
  
  describe "#create" do
    describe "ApplePurchase" do
      let(:purchase_params) {
        {
          :purchase_date=>purchase_date,
          :receipt_data=> 'abcdefg12345',
          :product=> product,
          :person=> person
        }
      }
      let(:params) {
        {
          platform: 'ios',
          name: product.display_name,
          purchase: {"receipt_data" => "abcdefg12345"}
        }
      }

      context "unauthenticated" do
        it "renders failure if not authorized" do
          post :create, params: { purchase: [] }
          expect(JSON.parse(response.body)["status"]).to eq "failure"
        end
      end

      context "authenticated" do
        before(:each) do
          controller.request.env["HTTP_AUTHORIZATION"] =
            ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(person.tc_social_token))
          Product.where(display_name: 'rf_video_download').where.not(id: product.id).destroy_all
        end

        it "renders failure if error returned from Apple" do
          fake_apple_api(
            body: {
              "status" => "2100",
            },
            method_call: :make_post
          )

          post :create, params: params
          expect(JSON.parse(response.body)["status"]). to eq "failure"
          expect(JSON.parse(response.body)["messages"]). to eq ["2100: error status from Apple"]
        end

        it "renders failure if finalize is false" do
          handler = double(finalize: false, errors: "Unable to finalize purchase")
          expect(Renderforest::Apple::PurchaseRequest).to receive(:new).with(purchase_params).and_return(handler)
          fake_apple_api(
            body: {
              "status" => 0,
              "receipt" => {
                "in_app" => [
                  {
                    "purchase_date" => purchase_date,
                  }
                ]
              },
              "latest_receipt" => "abcdefg12345"
            },
            method_call: :make_post
          )

          post :create, params: params
          expect(JSON.parse(response.body)["status"]). to eq "failure"
          expect(JSON.parse(response.body)["messages"]). to eq "Unable to finalize purchase"
        end

        it "renders success if finalize is true" do
          handler = double(finalize: true, purchase_id: 1, paid_at: Date.today)
          expect(Renderforest::Apple::PurchaseRequest).to receive(:new).with(purchase_params).and_return(handler)

          fake_apple_api(
            body: {
              "status" => 0,
              "receipt" => {
                "in_app" => [
                  {
                    "purchase_date" => purchase_date,
                  }
                ]
              },
              "latest_receipt" => "abcdefg12345"
            },
            method_call: :make_post
          )

          post :create, params: params

          expect(response.body).to eq Api::Renderforest::PurchaseHandlerSerializer.new(handler, root: false).to_json
        end
      end
    end
  end

  describe "#create" do
    describe "AndroidPurchase" do
      let(:params) {
        {
          platform: 'android',
          name: 'rf_video_download',
          purchase_token: 'njjpnooplbfcnhddjngmcfhe.AO-J1OwzPBAcnF8dG9efGa1lJ3UMpGpFZ05EWlfPlrfkzJFf1C1kDcuDu84CLoTu47C4CdgQy34tJ46JKC5fO-gVXkAEPp2YTU1YUTz1EiCvAM6LdstpH4YtQN6AzYymrezxnn4-3DuTHhx_D9KYNd4TcGo9J8UQkw',
          product_id: 'com.tunecore.social.rf2.video_download'
        }
      }

      context "unauthenticated" do
        it "renders failure if not authorized" do
          post :create, params: { purchase: [] }
          expect(JSON.parse(response.body)["status"]).to eq "failure"
        end
      end

      context "authenticated" do
        before(:each) do
          controller.request.env["HTTP_AUTHORIZATION"] =
            ActionController::HttpAuthentication::Token.encode_credentials(encrypt_token(person.tc_social_token))
          Product.where(display_name: 'rf_video_download').where.not(id: product.id).destroy_all
        end
        it "renders success if finalize is true" do
          
          purchase_response = {
            purchase_date: Date.today,
            purchase_token: params[:purchase_token],
            product_id: params[:product_id]
          }
          purchase_params = purchase_response.merge!(
            product: product,
            person: person
          )
          allow_any_instance_of(Renderforest::Android::PurchaseVerifier).to receive(:purchase_verifier).and_return(purchase_response)
          handler = double(finalize: true, purchase_id: 1, paid_at: Date.today)
          expect(Renderforest::Android::PurchaseRequest).to receive(:new).with(purchase_params).and_return(handler)
          post :create, params: params
          expect(JSON.parse(response.body)["status"]).to eq "success"
        end
      end
    end
  end
end
