require 'rails_helper'

describe Api::Renderforest::SongsController, type: :controller do
  describe "#index" do
    context "unauthorized access" do
      it "renders a failure response" do
        get :index
        expect(response.status).to eq(200)
        expect(response.body).to eq({ status: "failure", code: 2000 }.to_json)
      end
    end

    context "authorized access with TC Distribution customer" do
      it "renders a 200 json response" do
        person = create(:person, :with_tc_social)
        controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(
          encrypt_token(person.tc_social_token)
        )
        album = create(:album, :with_uploaded_songs_and_artwork_and_esi, person: person)
        distribution = create(:distribution, :delivered_to_spotify)
        distribution.petri_bundle.update_attribute(:album, album)
        params = { page: 1, per_page: 10, sort: 'asc' }
        get :index, params: params
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)).to be_a(Hash)
        expect((JSON.parse(response.body)['data']).count).to eq 2
      end
    end

    context "authorized access without a TC Distribution customer" do
      it "renders a 200 json response" do
        person = create(:person, :with_tc_social)
        controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(
          encrypt_token(person.tc_social_token)
        )
        create(:album, person: person)
        params = { page: 1, per_page: 10 }
        get :index, params: params
        expect(response.status).to eq(200)
        expect(response.body).to eq({ status: "failure", message: ["api.tc.message.user_no_albums"] }.to_json)
      end
    end

    context "authorized access with no releases" do
      it "renders a 200 json response" do
        person = create(:person, :with_tc_social)
        controller.request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Token.encode_credentials(
          encrypt_token(person.tc_social_token)
        )
        params = { page: 1, per_page: 10 }
        get :index, params: params
        expect(response.status).to eq(200)
        expect(response.body).to eq({ status: "failure", message: ["api.tc.message.user_no_albums"] }.to_json)
      end
    end
  end
end
