require "rails_helper"

describe Api::ReleasesController, type: :controller do
  describe "#show" do
    let(:album) { FactoryBot.create(:album, :finalized, :with_uploaded_songs_and_artwork, :with_salepoints, :with_booklet) }

    it "responds with 401 if missing api key" do
      get :show, params: { id: album.id }
      expect(response.code).to eq "401"
    end

    it "responds with 401 if bad api key" do
      get :show, params: { id: album.id, api_key: "SUPERBADKEY" }
      expect(response.code).to eq "401"
    end

    it "responds with error message if album not found" do
      get :show, params: { id: 12355, api_key: API_CONFIG["API_KEY"] }
      error_message = { "status" => "error", "message" => "Release with id 12355 not found" }
      expect(JSON.parse(response.body)).to eq error_message
    end

    it "responds with album json object" do
      serialized_album = ReleaseSerializer.new(album)
      get :show, params: { id: album.id, api_key: API_CONFIG["API_KEY"] }
      serialized_album.object.songs.map(&:reload)
      expect(response.body).to eq serialized_album.to_json
    end
  end
end
