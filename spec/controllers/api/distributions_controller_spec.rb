require "rails_helper"

describe Api::DistributionsController, type: :controller do
  let(:distribution) { FactoryBot.create(:distribution) }

  describe "#show_state" do
    context "invalid api_key" do
      it "responds with status 401" do
        get :show_state, params: { api_key: "GWELL" }
        expect(response.status).to eq(401)
      end
    end

    context "valid api_key" do
      it "responds with status 200" do
        get :show_state, params: { id: distribution.id, api_key: API_CONFIG["API_KEY"] }
        expect(response.status).to eq(200)
      end

      it "responds with error if distribution not found" do
        post :show_state, params: { id: 1000, api_key: API_CONFIG["API_KEY"] }
        json = { status: "error", reason: "Please provide a valid distribution id or store and album ids or upc and store name"}.to_json

        expect(response.body).to eq json
      end

      it "responds with proper json structure" do
        json = {
          status: "success",
          id: distribution.id,
          state: distribution.state,
        }.to_json

        get :show_state, params: { id: distribution.id, api_key: API_CONFIG["API_KEY"] }
        expect(response.body).to eq json
      end

      it "response with json error message if no id is given" do
        json = {
          status: "error",
          reason: "Please provide a valid distribution id or store and album ids or upc and store name",
        }.to_json
        get :show_state, params: { api_key: API_CONFIG["API_KEY"] }
        expect(response.body).to eq json
      end
    end
  end

  describe "#update_state" do
    let(:distribution) { FactoryBot.create(:distribution, state: "new") }

    context "invalid api key" do
      it "responds with status 401" do
        get :update_state, params: { api_key: "GWELL" }
        expect(response.status).to eq(401)
      end
    end

    context "valid api key" do
      it "responds with error if no distribution id given" do
        post :update_state, params: { api_key: API_CONFIG["API_KEY"] }
        json = { status: "error", reason: "Please provide a valid distribution id or store and album ids or upc and store name"}.to_json

        expect(response.body).to eq json
      end

      it "responds with error if no state given" do
        post :update_state, params: { id: distribution.id, api_key: API_CONFIG["API_KEY"] }
        json = { status: "error", reason: "Please provide a valid distribution state"}.to_json

        expect(response.body).to eq json
      end

      it "responds with error if distribution not found" do
        post :update_state, params: { id: 1000, state: "enqueued", api_key: API_CONFIG["API_KEY"] }
        json = { status: "error", reason: "Please provide a valid distribution id or store and album ids or upc and store name"}.to_json

        expect(response.body).to eq json
      end

      it "resonds with error if not a valid distribution state" do
        post :update_state, params: { id: distribution.id, state: "invalid_state", api_key: API_CONFIG["API_KEY"] }
        json = { status: "error", reason: "Please provide a valid distribution state"}.to_json

        expect(response.body).to eq json
      end

      it "responds with success if distribution state updated" do
        post :update_state, params: { id: distribution.id, state: "enqueued", api_key: API_CONFIG["API_KEY"] }
        json = { status: "success", id: distribution.id, state: "enqueued" }.to_json

        expect(response.body).to eq json
      end

      it "updates state by store/album if provided store_id and album_id" do
        allow(Distribution).to receive(:find_by_store_and_album).and_return(distribution)
        post :update_state, params: { store_id: 36, album_id: 1, state: "enqueued", api_key: API_CONFIG["API_KEY"] }
        json = { status: "success", id: distribution.id, state: "enqueued" }.to_json

        expect(response.body).to eq json
      end

      it "responds with error if album_id and no store_id" do
        allow(Distribution).to receive(:find_by_store_and_album).and_return(distribution)
        post :update_state, params: { album_id: 1, state: "enqueued", api_key: API_CONFIG["API_KEY"] }
        json = { status: "error", reason: "Please provide a valid distribution id or store and album ids or upc and store name"}.to_json

        expect(response.body).to eq json
      end

      context "YoutubeSR distributions updates based on isrc" do
        let(:distribution) { create(:distribution, :with_distribution_songs) }
        let(:album)        { distribution.salepoints.last.salepointable }
        let(:song_isrc_1)  { distribution.distribution_songs.first.salepoint_song.song.isrc }
        let(:song_isrc_2)  { distribution.distribution_songs.last.salepoint_song.song.isrc }

        it "updates a distribution song state if track isrc is included in the params" do
          post :update_state, params: { upc: album.upc.number, store_name: "YoutubeSR", state: "enqueued", isrcs: [song_isrc_1], api_key: API_CONFIG["API_KEY"] }
          expect(distribution.reload.state).to eq "enqueued"
          expect(distribution.distribution_songs.first.reload.state).to eq "enqueued"
          expect(distribution.distribution_songs.first.transitions.last.new_state).to eq "enqueued"
          expect(distribution.distribution_songs.last.reload.state).to eq "new"
          expect(distribution.distribution_songs.last.transitions).to be_empty
        end

        it "only updates ytsr distribution to delivered if all ytsr tracks are delivered" do
          post :update_state, params: { upc: album.upc.number, store_name: "YoutubeSR", state: "delivered", isrcs: [song_isrc_1], api_key: API_CONFIG["API_KEY"] }
          expect(distribution.reload.state).to eq "new"
          expect(distribution.distribution_songs.first.reload.state).to eq "delivered"
          expect(distribution.distribution_songs.first.transitions.last.new_state).to eq "delivered"
          expect(distribution.distribution_songs.last.reload.state).to eq "new"
          expect(distribution.distribution_songs.last.transitions).to be_empty

          post :update_state, params: { upc: album.upc.number, store_name: "YoutubeSR", state: "delivered", isrcs: [song_isrc_2], api_key: API_CONFIG["API_KEY"] }
          expect(distribution.reload.state).to eq "delivered"
          expect(distribution.distribution_songs.last.reload.state).to eq "delivered"
          expect(distribution.distribution_songs.last.transitions.last.new_state).to eq "delivered"
        end
      end
    end
  end
end
