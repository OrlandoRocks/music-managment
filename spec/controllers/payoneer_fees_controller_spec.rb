require "rails_helper"

describe PayoneerFeesController do
  let(:person) { create(:person) }

  describe "#index" do
    context "#payoneer_payout returns true" do
      it "should redirect to withdraw path" do
        login_as(person)
        allow(Payoneer::FeatureService).to receive(:payoneer_payout_enabled?).with(person).and_return(true)
        get :index

        expect(response.status).to eq(200)
      end
    end

    context "user has active payout provider" do
      it "should redirect to the payoneer status page" do
        login_as(person)
        allow(Payoneer::FeatureService).to receive(:payoneer_payout_enabled?).with(person).and_return(true)
        payout_provider = create(:payout_provider, person: person, provider_status: PayoutProvider::APPROVED)

        get :index
        expect(response.status).to eq(302)
        expect(response).to redirect_to(payoneer_status_index_path)
      end
    end

    context "user has no active payout provider" do
      it "should render the page" do
        login_as(person)
        allow(Payoneer::FeatureService).to receive(:payoneer_payout_enabled?).with(person).and_return(true)

        get :index
        expect(response.status).to eq(200)
      end
    end
  end
end
