require "rails_helper"

describe ArtworkSuggestionsController do
  before(:each) do
    @person = create(:person, :admin)
    @album = create(:album, :finalized, person: @person)
    @cover = create(:cover, album: @album)

    allow(Cover).to receive(:new).and_return(@cover)
    allow(@cover).to receive(:render).and_return(true)
    allow(@cover).to receive(:rendered_cover_filename).and_return("rendered_file.png")

    login_as(@person)

    allow(@album).to receive(:permit_metadata_changes?).and_return(true)
  end

  describe "#index" do
  end
end
