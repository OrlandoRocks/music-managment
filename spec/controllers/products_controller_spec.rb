require "rails_helper"

describe ProductsController, type: :controller do
  let!(:person) { create(:person) }
  let!(:album) { create(:album, :with_songs, person: person) }

  let!(:credit_product) { Product.find_by(display_name: "credit", country_website_id: 1) }
  let!(:plan_person) { create(:person) }
  let!(:plan_product) { Product.find_by(display_name: "rising_artist", country_website_id: 1) }

  before(:each) do
    login_as person
  end

  describe "#add_to_cart_with_credit" do
    it "should cart credit usage when credits available" do
      TCFactory.build_ad_hoc_credit_usage_product
      product = TCFactory.build_album_credit_product(5)
      purchase = TCFactory.create(:purchase, person_id: person.id, product_id: product.id, related: product, paid_at: Time.now)
      inventory = TCFactory.create(:inventory,
                                   person: person,
                                   inventory_type: 'Album',
                                   product_item: product.product_items.first,
                                   purchase: purchase,
                                   quantity: 5)

      expect(inventory.valid?).to eq(true)

      expect do
        expect do
          post :add_to_cart_with_credit, params: { album_id: album.id }
        end.to change(Purchase, :count).by(1)
      end.to change(CreditUsage, :count).by(1)
    end

    it "should redirect to dashboard with message when no credits" do
      TCFactory.build_ad_hoc_credit_usage_product
      expect do
        expect do
          post :add_to_cart_with_credit, params: { album_id: album.id }
        end.to_not change(Purchase, :count)
      end.to_not change(CreditUsage, :count)

      assert_redirected_to dashboard_path
      expect(flash[:error]).to eq("Credit can not be applied.")
    end

    it "should create store automator if requested" do
      expect_any_instance_of(Album).to receive(:add_automator)
      post :add_to_cart_with_credit, params: { album_id: album.id, add_automator: "1" }
    end

    it "should not create store_automator if not requested" do
      expect_any_instance_of(Album).not_to receive(:add_automator)
      post :add_to_cart_with_credit, params: { album_id: album.id }
    end
  end

  describe "#add_to_cart" do
    let(:product) { create(:product) }

    it "should add automator if requested" do
      expect_any_instance_of(Album).to receive(:add_automator)
      post :add_to_cart, params: { album_id: album.id, product_id: product.id, add_automator: "1" }
    end

    it "should not add automator if not requested" do
      expect_any_instance_of(Album).not_to receive(:add_automator)
      post :add_to_cart, params: { product_id: product.id, album_id: album.id }
    end

    it "should create a Social purchase if it is added to cart" do
      social_purchases = SubscriptionPurchase.by_product_name("Social").count
      post :add_to_cart, params: { product_id: SubscriptionProduct.find_by(product_name: "Social").product_id, product_type: "social" }
      expect(SubscriptionPurchase.by_product_name("Social").count).to eq(social_purchases + 1)
    end

    it "should NOT create a Social purchase if a different product is added to cart" do
      social_purchases = SubscriptionPurchase.by_product_name("Social").count
      post :add_to_cart, params: { product_id: product.id, album_id: album.id }
      expect(SubscriptionPurchase.by_product_name("Social").count).to eq(social_purchases)
    end

    it "should not create a purchase when wrong product is added to the cart" do
      purchases = Purchase.count
      @single = create(:single, person: person)
      post :add_to_cart, params: { product_id: product.id, album_id: @single.id }
      expect(Purchase.count).to eq(purchases)
    end

    it "should redirect to dashboard if cart has Plan" do
      post :add_to_cart, params: { product_id: plan_product.id }

      post :add_to_cart, params: { product_id: credit_product.id }
      expect(response).to redirect_to dashboard_path
      expect(flash[:error]).to be_present
    end

    it "should redirect to dashboard if user has Plan" do
      login_as plan_person
      create(:person_plan, person: plan_person, plan: Plan.first)

      post :add_to_cart, params: { product_id: credit_product.id }
      expect(response).to redirect_to dashboard_path
      expect(flash[:error]).to be_present
    end
  end

  describe "#add_stores_to_cart" do
    it "should add automator if requested" do
      expect_any_instance_of(Album).to receive(:add_automator)
      post :add_stores_to_cart, params: { album_id: album.id, add_automator: "1" }
    end

    it "should not request automator if not requested" do
      expect_any_instance_of(Album).not_to receive(:add_automator)
      post :add_stores_to_cart, params: { album_id: album.id }
    end

    it "should add automator to cart if present" do
      album = create(:album, :with_uploaded_songs_and_artwork, :with_songwriter, person: person)
      create(:salepoint, salepointable: album)
      TCFactory.build_ad_hoc_salepoint_subscription_product
      album.update(finalized_at: Time.now)
      album.create_salepoint_subscription(is_active: true)

      expect do
        post :add_stores_to_cart, params: { album_id: album.id }
      end.to change(Purchase, :count).by(2) # one for the salepoint, one for the store automator

      expect(album.salepoint_subscription.purchase.blank?).to eq(false)
      expect(album.salepoint_subscription.purchase.paid?).to eq(false)
    end
  end
end

describe ProductsController do
  describe "#add_renewals_to_cart" do
    before(:each) do
      @person = TCFactory.create(:person)
      login_as @person
      @renewal = double(Renewal)
      @renewals = [@renewal]
      allow(@person).to receive_message_chain(:renewals, :with_renewal_information) { @renewals }
      allow(@renewals).to receive(:find).and_return(@renewal)
      allow(@renewal).to receive(:renewal_product_id) { 1 }
      allow(Product).to receive(:find) { double(Product) }
      allow(Product).to receive(:add_to_cart) { double(Purchase) }
    end

    it "should redirect to the cart" do
      allow(@renewals).to receive(:find).and_return(@renewals)
      get :add_renewals_to_cart, params: { ids: [1, 2, 3] }
      expect(response).to redirect_to cart_path
    end

    it "should receive the correct params of ids and call find with the correct ids" do
      expect(@renewals).to receive(:find).with(%w(1 2 3)).and_return(@renewals)
      get :add_renewals_to_cart, params: { ids: [1, 2, 3] }
    end
  end

  describe "#remove_unsupported_salepoints" do
    let(:person) { create(:person) }
    let(:album) { create(:album, person: person) }

    before(:each) do
      login_as person
    end

    it "is called on add_to_cart" do
      expect(controller).to receive(:remove_unsupported_salepoints)
      post :add_to_cart, params: { id: 1, album_id: album.id }
    end

    it "is called on add_to_cart_with_credit" do
      expect(controller).to receive(:remove_unsupported_salepoints)
      post :add_to_cart_with_credit, params: { id: 1, album_id: album.id }
    end
  end

  describe "#distribution_credits" do
    it "should redirect to dashboard for users with membership plan" do
      person = create(:person)
      create(:person_plan, person: person, plan: Plan.first)
      login_as(person)
      get :distribution_credits
      expect(response).to redirect_to dashboard_path
    end
  end

  describe "#add_splits_collaborator_to_cart" do
    let!(:person) { create(:person) }
    before(:each) do
      login_as(person)
    end
    context "invalid request" do
      it "should redirect to dashboard if user has active splits collaborator PlanAddon" do
        create(:plan_addon, :splits_collaborator, :paid, person: person)
        get :add_splits_collaborator_to_cart
        expect(flash[:error]).to eq(custom_t("controllers.products.user_has_active_collaborator"))
        expect(response).to redirect_to dashboard_path
      end
      it "should redirect to dashboard if user doesn't have new artist plan paid or in cart and surface error" do
        get :add_splits_collaborator_to_cart
        expect(flash[:error]).to eq(custom_t("controllers.products.not_new_artist_plan"))
        expect(response).to redirect_to dashboard_path
      end
    end
    context "valid request" do
      before(:each) do
        allow_any_instance_of(Person).to receive(:user_or_cart_plan?).with(Plan::NEW_ARTIST).and_return(true)
      end
      it "finds or creates unpaid SplitsCollaborator for user" do
        get :add_splits_collaborator_to_cart
        expect(SplitsCollaboratorAddon.active.unpaid.exists?(person: person)).to be(true)
      end
      it "adds SplitsCollaborator to cart" do
        get :add_splits_collaborator_to_cart
        expect(person.purchases.unpaid.not_in_invoice.any?(&:splits_collaborator?)).to be(true)
      end
      it "redirects user to cart" do
        get :add_splits_collaborator_to_cart
        expect(response).to redirect_to cart_path
      end
    end
  end
end
