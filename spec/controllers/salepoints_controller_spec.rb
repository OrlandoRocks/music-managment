require "rails_helper"

describe SalepointsController, type: :controller do
  let(:person) { create(:person) }
  let(:album) { create(:album, person: person) }
  let(:salepoint_hash_with_no_checked_store_ids) do
    # NOTE: this is here so that rails 5 doesn't munge the album/salepoints
    #  key and params.
    {
      salepoints: {
        "9999": {
          salepoint: {
            variable_price_id: "1"
          }
        }
      }
    }
  end

  before do
    login_as(person)
  end

  describe "index" do
    it "should respond with success" do
      get :index, params: { album_id: album.id }

      assert_response :success, @response.body
    end
  end

  def salepoint_hash(salepoint, store)
    {
      salepoint: {
        id: salepoint.id.to_s,
        store_id: salepoint.store_id.to_s,
        variable_price_id: "42"
      }
    }
  end

  describe "create" do
    before do
      @store     = Store.find_by(name: "Amazon Music")
      @salepoint = album.salepoints.create!(
                                      store: @store,
                                      variable_price: @store.variable_prices.first,
                                      has_rights_assignment: true
                                    )
      @params_hash = {
        album_id: album.id.to_s,
        album: {
          salepoints: {
            @salepoint.store_id.to_s => salepoint_hash(@salepoint, @store)
          }
        },
        apple_music_opt_in: "on"
      }

      @params = ActionController::Parameters.new(@params_hash)
    end

    it "calls SalepointService to create/update salepoints" do
      expected_instance = SalepointService.new(
                            person,
                            album,
                            @params_hash[:album][:salepoints],
                            @params[:apple_music_opt_in]
                          )

      expect(SalepointService)
        .to receive(:new)
        .with(
           person,
           album,
           @params_hash[:album][:salepoints],
           @params[:apple_music_opt_in]
        )
        .and_return(expected_instance)

      expect_any_instance_of(SalepointService)
        .to receive(:call)

      post :create, params: @params_hash
    end

    context "#block_paid_salepoint_changes?," do
      context "plans disabled," do
        it "does NOT remove paid stores" do
          album.update(finalized_at: DateTime.now)

          post :create, params: @params_hash

          result = controller.instance_eval { eligible_salepoint_params }

          expect(result.empty?).to be false
        end
      end

      context "plans enabled," do
        before { allow(FeatureFlipper).to receive(:show_feature?).and_return true }

        context "no plan," do
          context "release finalized, some hacking going on," do
            before { album.update(finalized_at: DateTime.now) }

            it "removes paid stores" do
              post :create, params: @params_hash

              result = controller.instance_eval { eligible_salepoint_params }

              expect(result.empty?).to be true
            end
          end

          context "release finalized, free stores submitted," do
            let!(:free_store) { Store.find_by(abbrev: "yttm") }
            let!(:free_salepoint) do
              album.salepoints.create!(
                store: free_store,
                variable_price: free_store.variable_prices.first,
                has_rights_assignment: true
              )
            end

            before do
              album.update(finalized_at: DateTime.now)
              @params_hash[:album][:salepoints]
                .merge!({ free_store.id.to_s => salepoint_hash(free_salepoint, free_store) })
            end

            it "allows the free stores" do
              post :create, params: @params_hash

              result = controller.instance_eval { eligible_salepoint_params }

              expect(result[free_store.id.to_s]).to be_truthy
              expect(result.size).to                eq 1
            end
          end

          context "release NOT finalized" do
            it "does not remove paid stores" do
              post :create, params: @params_hash

              result = controller.instance_eval { eligible_salepoint_params }

              expect(result.empty?).to be false
            end
          end
        end

        context "has plan," do
          before { create(:person_plan, person: person) }

          context "release finalized," do
            before { album.update(finalized_at: DateTime.now) }

            it "does NOT remove paid stores" do
              post :create, params: @params_hash

              result = controller.instance_eval { eligible_salepoint_params }

              expect(result.empty?).to be false
            end
          end

          context "release NOT finalized," do
            it "does NOT remove paid stores" do
              post :create, params: @params_hash

              result = controller.instance_eval { eligible_salepoint_params }

              expect(result.empty?).to be false
            end
          end
        end
      end
    end

    it "removes BeliveLiv salepoint when params doesn't have store ids" do
      params = {
        album_id: album.id.to_s,
        album: salepoint_hash_with_no_checked_store_ids,
        apple_music_opt_in: "on"
      }.with_indifferent_access

      store = Store.find_by(short_name: "BelieveLiv")
      album.salepoints.create!(
        store: store,
        variable_price: store.variable_prices.first,
        has_rights_assignment: true
      )

      expect(album.stores.exists?(short_name: "BelieveLiv")).to be true

      post :create, params: params

      expect(album.stores.exists?(short_name: "BelieveLiv")).to be false
    end

    context "with no album params" do
      before :each do
        @params = {
          album_id: album.id.to_s,
          apple_music_opt_in: "on"
        }.with_indifferent_access
      end

      it "does not raise an error" do
        expect { post :create, params: @params }.not_to raise_error
      end

      it "redirects to the index view" do
        post :create, params: @params

        expect(response).to render_template(:index)
      end
    end

    context "with no album salepoint store_id params" do
      let(:params) do
        {
          album_id: album.id.to_s,
          album: {
            salepoints: {}
          },
          apple_music_opt_in: "on"
        }
      end

      it "does not raise an error" do
        expect { post :create, params: params }.not_to raise_error
      end

      it "redirects to the index view" do
        post :create, params: params

        expect(response).to render_template(:index)
      end
    end

    context "with salepoint_subscriptions" do
      it "creates and carts a salepoint subscription for unfinalized, carted album" do
        allow(Purchase).to receive(:albums_in_cart).and_return([album])

        expect(Product).to receive(:add_to_cart).with(
          album.person,
          an_instance_of(SalepointSubscription)
        )
        expect do
          post :create, params: {
            album_id: album.id,
            deliver_automator: "1",
            album: salepoint_hash_with_no_checked_store_ids
          }
        end.to change(SalepointSubscription, :count).by(1)

        assert_redirected_to album_path(album)
        expect(album.salepoint_subscription.blank?).to eq(false)
        expect(album.salepoint_subscription.is_active).to eq(true)
        expect(album.salepoint_subscription.effective.blank?).to eq(true)
      end

      it "skips creation of a salepoint subscription for unfinalized, uncarted album" do
        post :create, params: {
          album_id: album.id,
          deliver_automator: "1",
          album: salepoint_hash_with_no_checked_store_ids
        }

        assert_redirected_to album_path(album)
        expect(album.salepoint_subscription.blank?).to eq(true)
        expect(album.salepoint_subscription.nil?).to eq(true)
      end

      it "removes and uncarts salepoint subscription from unfinalized, carted album" do
        album.create_salepoint_subscription(:is_active => true)
        Product.add_to_cart(person, album)
        Product.add_to_cart(person, album.salepoint_subscription)

        expect(Purchase.cart_items_count(person)).to eq(2)
        expect do
          post :create, params: {
            album_id: album.id,
            deliver_automator: "0",
            album: salepoint_hash_with_no_checked_store_ids
          }
        end.to change(SalepointSubscription, :count).by(-1)
        expect(Purchase.cart_items_count(person)).to eq(1)

        album.reload

        assert_redirected_to album_path(album)
        expect(album.salepoint_subscription.blank?).to eq(true)
      end
    end

    context "when there is a pre-existing freemium salepoint" do
      before { allow(FeatureFlipper).to receive(:show_feature?).and_return(true) }
      let(:freemium_store) { Store.find(Store::FB_REELS_STORE_ID)}
      let!(:salepoint) { create(:salepoint, store: freemium_store, salepointable: album) }
      let!(:new_store) { Store.find_by(name: "Amazon Music") }

      let(:expected_salepoint_params) {
        {
          new_store.id.to_s => {
            salepoint: {
              store_id: new_store.id.to_s,
              variable_price_id: new_store.variable_prices.last&.id.to_s
            }
          },
          freemium_store.id.to_s => {
            salepoint: {
              has_rights_assignment: salepoint.has_rights_assignment.to_s,
              id: salepoint.id.to_s,
              store_id: freemium_store.id.to_s,
              variable_price_id: freemium_store.variable_prices.last&.id.to_s
            }
          }
        }
      }

      let(:params_hash) {
        {
          album_id: album.id.to_s,
          album: {
            salepoints: {
              new_store.id.to_s => {
                salepoint: {
                  store_id: new_store.id.to_s,
                  variable_price_id: new_store.variable_prices.last.id.to_s
                }
              }
            }
          }
        }
      }

      it "includes the existing freemium salepoint in params passed to Salepoint Service" do
        expect(SalepointService)
        .to receive(:new)
        .with(
           person,
           album,
           expected_salepoint_params,
           nil
        )
        .and_call_original

        expect_any_instance_of(SalepointService)
          .to receive(:call)
          post :create, params: params_hash
      end
    end

    context "with freemium salepoint params" do
      context "when there are pre-existing non-freemium salepoints" do
        before { allow(FeatureFlipper).to receive(:show_feature?).and_return(true) }
        let!(:store) { Store.find_by(name: "Spotify") }
        let(:existing_salepoint) { @salepoint }
        let(:new_freemium_store) { Store.find(Store::FB_REELS_STORE_ID)}
        let!(:salepoint) { create(:salepoint, store: store, salepointable: album) }


        let(:expected_salepoint_params) {
          {
            new_freemium_store.id.to_s => {
              salepoint: {
                store_id: new_freemium_store.id.to_s,
                variable_price_id: new_freemium_store.variable_prices.last&.id.to_s
              }
            },
            @salepoint.store_id.to_s => {
              salepoint: {
                has_rights_assignment: @salepoint.has_rights_assignment.to_s,
                id: @salepoint.id.to_s,
                store_id: @salepoint.store_id.to_s,
                variable_price_id: @salepoint.variable_price_id.to_s
              }
            },
            store.id.to_s => {
              salepoint: {
                has_rights_assignment: salepoint.has_rights_assignment.to_s,
                id: salepoint.id.to_s,
                store_id: store.id.to_s,
                variable_price_id: salepoint.variable_price_id.to_s
              }
            }
          }
        }

        let(:params_hash) {
          {
            album_id: album.id.to_s,
            album: {
              salepoints: {
                new_freemium_store.id.to_s => {
                  salepoint: {
                    store_id: new_freemium_store.id.to_s,
                    variable_price_id: new_freemium_store.variable_prices.last&.id.to_s
                  }
                }
              }
            }
          }
        }

        it "includes the pre-existing salepoint data in params passed to Salepoint Service" do
          expect(SalepointService)
          .to receive(:new)
          .with(
            person,
            album,
            expected_salepoint_params,
            nil
          )
          .and_call_original

          expect_any_instance_of(SalepointService)
            .to receive(:call)

          post :create, params: params_hash
        end
      end
    end
  end

  describe 'activate_itunes_ww' do

    before do
      store = Store.find_by(short_name: 'iTunesCA')
      create(:petri_bundle, album: album)
      create(:salepoint, store: store , salepointable: album, variable_price: store.default_variable_price)
    end

    context 'when user has legacy itunes salepoint and not itunes ww' do
      it 'creates a itunes ww salepoint' do
        post :activate_itunes_ww, params: { album_id: album.id}, format: 'js'
        expect(album.salepoints.pluck(:store_id)).to include(Store::ITUNES_WW_ID)
      end

      it 'creates a distribution for itunes' do
        expect {
          post :activate_itunes_ww,
          params: { album_id: album.id },
          format: 'js'
        }.to change { Distribution.count }.by(1)
      end
    end
  end

  describe '#add_active_stores_to_album' do
    let!(:freemium_salepoint) { create(:salepoint, store_id: Store::FB_REELS_STORE_ID, salepointable: album) }
    let(:params_hash) { { album_id: album.id } }
    let(:stores) { Store.freemium_upsell_stores }
    subject { post :add_active_stores_to_album, params: params_hash  }

    context "when salepoints are created succesfully" do
      it "creates salepoints for all active stores" do
        expect { subject }.to change { Salepoint.count }.by(stores.count)
      end
      
      it "re-adds the product to the cart" do 
        expect(Product).to receive(:add_to_cart).with(person, album)
        subject
      end 
    end

    context "when salepoints are not create succesfully" do
      before { allow_any_instance_of(SalepointService).to receive(:call).and_return(false) }
      it "it redirects to carts path" do
        expect { subject }.to change { Salepoint.count }.by(0)

        expect(subject).to redirect_to(cart_path)
      end
    end
  end
end
