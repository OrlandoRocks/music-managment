require "rails_helper"

describe TourDatesController do
  context "When logged in" do
    before(:each) do
      login_as(person_with_a_tour_date)
    end

    it "should create and save a new instance of TourDate with valid params" do
      tour_date_params = { tour_date: { artist_id: @tour_date.artist.id, country_id: person_with_a_tour_date.country_website.id, us_state_id: 1, city: "Queens", venue: "TuneCore", event_date_at: Date.today } }

      expect { post :create, params: tour_date_params }.to change{ TourDate.count }.by(1)
      expect(response).to redirect_to(tour_dates_path)
    end

    it "should not create and save a new instance of TourDate with invalid params" do
      tour_date_params = { tour_date: { country_id: person_with_a_tour_date.country_website.id, us_state_id: 1, city: "Queens", venue: "TuneCore", event_date_at: Date.today } }

      expect { post :create, params: tour_date_params }.to change{ TourDate.count }.by(0)
      expect(response).to render_template('tour_dates/new')
    end

    it "should update an existing TourDate with valid params" do
      allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person_with_a_tour_date)
      put :update, params: { id: @tour_date.id, tour_date: @tour_date.attributes = { city: "Brooklyn" } }

      expect(response).to redirect_to(tour_dates_path)
      expect(@tour_date.reload.city).to eq("Brooklyn")
    end

    it "should redirect to the tour_dates path after successfully updating, when specified in params" do
      allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person_with_a_tour_date)
      put :update, params: { id: @tour_date.id, tour_date: @tour_date.attributes = { city: "Brooklyn" } }

      expect(response).to redirect_to(tour_dates_path)
      expect(@tour_date.reload.city).to eq("Brooklyn")
    end

    it "should not update an existing TourDate with invalid params" do
      country_id = @tour_date.country_id
      allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person_with_a_tour_date)
      put :update, params: { id: @tour_date.id, tour_date: @tour_date.attributes = { country_id: "" } }

      expect(response).to render_template('tour_dates/edit')
      expect(@tour_date.reload.country_id).to eq(country_id)
    end

    it "should load countries and us states on new" do
      get :new

      expect(assigns(:countries)).not_to be_nil
      expect(assigns(:us_states)).not_to be_nil
    end

    it "should load countries and us states on edit" do
      get :edit, params: { id: @tour_date.id }

      expect(assigns(:countries)).not_to be_nil
      expect(assigns(:us_states)).not_to be_nil
    end
  end

  context "When trying to destroy someone's else tour date" do
    it "should return a 404 not found error" do
      person_with_a_tour_date
      login_as(person_with_no_tour_date)

      expect {delete :destroy, params: { id: @tour_date.id } }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  context "When trying to update someone's else tour date" do
    it "should return a 404 not found error" do
      person_with_a_tour_date
      login_as(person_with_no_tour_date)

      expect {put :update, params: { id: @tour_date.id } }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end
end


private
def person_with_a_tour_date
  @tour_date = create(:tour_date)
  @tour_date.person
end

def person_with_no_tour_date
  create(:person)
end
