require 'rails_helper'

RSpec.describe PlansController, type: :controller do
  let!(:person) { create(:person) }
  let!(:new_artist_plan) { Plan.find(Plan::NEW_ARTIST) }
  let!(:rising_artist_plan) { Plan.find(Plan::RISING_ARTIST) }
  let!(:breakout_artist_plan) { Plan.find(Plan::BREAKOUT_ARTIST) }
  let!(:new_artist_product) { new_artist_plan.products.find_by!(country_website: person.country_website) }
  let!(:breakout_artist_product) { breakout_artist_plan.products.find_by!(country_website: person.country_website) }

  before(:each) do
    login_as(person)
    session[:previous_path] = Faker::Internet.url
  end
  describe "#dont_want_a_plan" do
    it "updates session[:dont_want_a_plan] cookie" do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
      get :dont_want_a_plan
      expect(session[:dont_want_a_plan]).not_to be_blank
    end
    it "redirects to session[:previous_path]" do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
      get :dont_want_a_plan
      assert_redirected_to(session[:previous_path])
    end
  end

  describe "#verify_cart_already_has_a_plan" do
    render_views
    context "When there is no plan in cart" do
      it "should return false and not render popup template" do
        get :verify_cart_already_has_a_plan, params: { clicked_plan_id: new_artist_plan.id }
        parsed_body = JSON.parse(response.body)
        expect(parsed_body["pricier_plan_in_cart"]).to be false
        expect(parsed_body["popup_template"]).to be nil
      end
    end

    context "When a plan is already in cart" do
      before do
        product = rising_artist_plan.products.find_by!(country_website: person.country_website)
        Product.add_to_cart(person, product, product)
      end

      it "should return true and render the modal when carted plan is more expensive" do
        get :verify_cart_already_has_a_plan, params: { clicked_plan_id: rising_artist_plan.id, clicked_plan_price: Integer(new_artist_product.price) }
        parsed_body = JSON.parse(response.body)
        expect(parsed_body["pricier_plan_in_cart"]).to be true
        expect(parsed_body["popup_template"]).to match(/plan_id=#{rising_artist_plan.id}/)
      end

      it "should return false when cheaper carted plan" do
        get :verify_cart_already_has_a_plan, params: { clicked_plan_id: rising_artist_plan.id, clicked_plan_price: Integer(breakout_artist_product.price) }
        parsed_body = JSON.parse(response.body)
        expect(parsed_body["pricier_plan_in_cart"]).to be false
        expect(parsed_body["popup_template"]).to be nil
      end
    end
  end

  describe "#replace_plan_in_cart" do
    it "should receive a new plan_id and redirect to cart_path" do
      get :replace_plan_in_cart, params: { plan_id: rising_artist_plan.id }
      expect(response).to redirect_to(cart_path)
    end

    it "should redirect to dashboard_path on error" do
      get :replace_plan_in_cart, params: { plan_id: 0 }
      expect(response).to redirect_to(dashboard_path)
      expect(flash[:error]).to eq("Something went wrong")
    end
  end
end
