require "rails_helper"

describe OutboundRoyaltyInvoicesController, type: :controller do
  let(:invoice) {
    create(
      :invoice,
      :with_two_purchases,
      :settled,
      :with_balance_settlement,
      :with_outbound_vat
    )
  }

  let(:outbound_refund) { create(:outbound_refund, outbound_invoice: invoice.outbound_invoice) }

  before :each do
    login_as outbound_refund.person
  end

  describe "#preview" do
    it "should render page with invoice" do
      get :show, params: { id: outbound_refund.id }

      expect(response.status).to eq 200
      expect(assigns(:refund_presenter)).to be_an_instance_of CreditNoteInvoices::OutboundPresenter
    end

    it "should have the correct data" do
      outbound_refund.update(invoice_sequence: 1)
      get :show, params: { id: outbound_refund.id }
      details = assigns(:refund_presenter).refund_details

      expect(details[:credit_note_invoice_number]).to eq outbound_refund.credit_note_invoice_number
      expect(details[:currency]).to eq "USD"

      expect(details[:base_amount]).to eq "9.99"
      expect(details[:tax_amount]).to eq "0.00"
      expect(details[:total_amount]).to eq "9.99"
      expect(details[:tax_rate]).to eq "0.00%"

      expect(details[:refunded_invoice_number]).to eq "BI-INV#00001"
    end

    it "should be able to download the pdf" do
      outbound_refund.update(invoice_sequence: 1)
      get :show, params: { id: outbound_refund.id }, format: :pdf

      expect(response.status).to eq 200
      expect(response.content_type).to eq "application/pdf"
      expect(response.headers["Content-Disposition"]).to include(outbound_refund.credit_note_invoice_number)
    end
  end

  describe "#undefined_invoice" do
    it "should raise error if invoice not found" do
      get :show, params: { id: 1234 }

      expect(response.status).to eq 302
      expect(flash[:error]).to match("Invalid Request. Try Again.")
    end
  end
end
