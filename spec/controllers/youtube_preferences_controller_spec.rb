require "rails_helper"

describe YoutubePreferencesController, type: :controller do

  describe "show" do
    before(:each) do
      @person = TCFactory.create(:person)
      allow(@person).to receive(:paid_for_composer?).and_return(true)
      ytm = double(YoutubeMonetization)
      allow(@person).to receive(:youtube_monetization).and_return(ytm)
      allow(ytm).to receive(:effective_date).and_return(Time.now)
      allow(ytm).to receive(:termination_date).and_return(nil)
      login_as(@person)
    end

    it "should set page title" do
      get :show
      expect(assigns[:page_title]).to eq("Connect To YouTube")
    end

    it "should create new YoutubePreference object it not set yet" do
      expect(@person).to receive(:youtube_preference).and_return(nil)

      yt = mock_model(YoutubePreference)
      expect(YoutubePreference).to receive(:new).and_return(yt)

      get :show
      expect(assigns[:preferences]).to eq(yt)
    end

    it "should return current preferences if already submitted" do
      yt = mock_model(YoutubePreference)

      expect(@person).to receive(:youtube_preference).and_return(yt)

      get :show
      expect(assigns[:preferences]).to eq(yt)
    end
  end


  describe "create" do
    before(:each) do
      @person = TCFactory.create(:person)
      allow(@person).to receive(:paid_for_composer?).and_return(true)
      ytm = double(YoutubeMonetization)
      allow(@person).to receive(:youtube_monetization).and_return(ytm)
      allow(ytm).to receive(:effective_date).and_return(Time.now)
      allow(ytm).to receive(:termination_date).and_return(nil)
      login_as(@person)
    end

    it "should send an email to user if successful create" do
      expect(controller).to receive(:send_email_confirmation)

      expect {
        post :create, params: { youtube: { mcn: true, whitelist: false, channel_id: 'testing' } }
      }.to change(YoutubePreference, :count).by(1)
    end
  end

  describe "update" do
    before(:each) do
      @person = TCFactory.create(:person)
      allow(@person).to receive(:paid_for_composer?).and_return(true)
      ytm = double(YoutubeMonetization)
      allow(@person).to receive(:youtube_monetization).and_return(ytm)
      allow(ytm).to receive(:termination_date).and_return(nil)
      allow(ytm).to receive(:effective_date).and_return(Time.now)

      @yt = TCFactory.create(:youtube_preference, :person => @person)
      login_as(@person)
    end

    it "should send an email to user if successful update" do
      expect(controller).to receive(:send_email_confirmation)

      expect {
        post :update, params: { youtube: { mcn: true, whitelist: false, channel_id: 'testing' } }
      }.to_not change(YoutubePreference, :count)
    end
  end

  describe "send_email_confirmation" do
    it "should send youtube publishing confirmation email" do
      person = TCFactory.build(:person)

      mailer = double(Object)
      expect(PersonNotifier).to receive(:youtube_publishing_confirmation).with(person).and_return(mailer)
      expect(mailer).to receive(:deliver)

      controller.instance_eval { @person = person }
      controller.instance_eval { send_email_confirmation }
    end
  end

  describe "require_publishing_purchase" do
    before(:each) do
      @person = TCFactory.create(:person)
      login_as(@person)
    end

    it "should redirect to artist services if the user does not have the youtube_monetization product" do
      allow(@person).to receive(:paid_for_composer?).and_return(false)
      allow(@person).to receive(:youtube_monetization).and_return(nil)
      get :show

      assert_redirected_to "//support.tunecore.com/hc/en-us/articles/115006507207"
    end

    it "should not redirect if the youtube_monetization product is paid for and not terminated" do
      allow(@person).to receive(:paid_for_composer?).and_return(true)
      ytm = double(YoutubeMonetization)
      allow(@person).to receive(:youtube_monetization).and_return(ytm)
      allow(ytm).to receive(:effective_date).and_return(Time.now)
      allow(ytm).to receive(:termination_date).and_return(nil)

      get :show

      assert_response :success
    end

    it "should redirect to artist services if the user has a youtube_monetization product but does not have effective date" do
      allow(@person).to receive(:paid_for_composer?).and_return(false)
      ytm = double(YoutubeMonetization)
      allow(@person).to receive(:youtube_monetization).and_return(ytm)
      allow(ytm).to receive(:effective_date).and_return(Time.now)
      allow(ytm).to receive(:termination_date).and_return(Time.now)
      get :show

      assert_redirected_to "//support.tunecore.com/hc/en-us/articles/115006507207"
    end
  end

end
