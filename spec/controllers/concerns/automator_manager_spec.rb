require "rails_helper"

describe AutomatorManager do
  class AutomatorManagerWrapper
    include AutomatorManager

    attr_reader :params, :current_user
    attr_accessor :session

    def initialize
      @session = {}
    end

    def controller_name
      'dummy_controller'
    end

    def dummy_controller_params
      params
    end
  end

  describe "#manage_automator_state" do
    it "sets session var to true if deliver_automator" do
      params = ActionController::Parameters.new(
        deliver_automator: "1"
      )
      controller = AutomatorManagerWrapper.new
      allow(controller).to receive(:params).and_return(params)

      controller.manage_automator_state(params)
      expect(controller.session).to eq({ automator_checked: true })
    end

    it "sets session var to true if add_automator" do
      params = ActionController::Parameters.new(
        add_automator: "1"
      )
      controller = AutomatorManagerWrapper.new
      allow(controller).to receive(:params).and_return(params)

      controller.manage_automator_state(params)
      expect(controller.session).to eq({ automator_checked: true })
    end

    it "sets session var to false otherwise" do
      params = ActionController::Parameters.new(
        add_automator: "0"
      )
      controller = AutomatorManagerWrapper.new
      allow(controller).to receive(:params).and_return(params)

      controller.manage_automator_state(params)
      expect(controller.session).to eq({ automator_checked: false })
    end
  end
end
