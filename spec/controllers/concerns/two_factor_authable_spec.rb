require "rails_helper"

describe TwoFactorAuthable do
  class DummyAuthController
    include TwoFactorAuthable

    attr_reader :current_user, :params, :step

    def initialize(current_user, step = nil)
      @current_user = current_user
      @params = {
        person: {
          email: current_user.email,
          password: "Password123!"
        }
      }
      @step = step
    end

    def under_admin_control?
      false
    end
  end

  describe "#person_params" do
    let(:person) { create(:person) }

    it "returns a hash with an expected shape" do
      controller = DummyAuthController.new(person)

      expect(controller.person_params)
        .to eq(
          {
            email: person.email,
            password: "Password123!"
          }
        )
    end
  end

  describe "#two_factor_auth_required?" do
    let(:person) { create(:person) }

    it "is false if user does NOT have two_factor_auth" do
      controller = DummyAuthController.new(person)

      expect(controller.two_factor_auth_required?).to eq false
    end

    it "is false if under admin control" do
      allow_any_instance_of(DummyAuthController)
        .to receive(:under_admin_control?)
        .and_return(true)

      controller = DummyAuthController.new(person)

      expect(controller.two_factor_auth_required?).to eq false
    end

    context "a user has a two_factor_auth" do
      let(:person) { create(:person, :with_two_factor_auth) }

      context "but it is INACTIVE" do
        it "returns false" do
          allow(person.two_factor_auth)
            .to receive(:active?)
            .and_return(false)

          controller = DummyAuthController.new(person)

          expect(controller.two_factor_auth_required?).to eq false
        end
      end

      context "and it is active" do
        context "and they are on the authenticated action" do
          context "inside of the authentication window" do
            it "returns false" do
              allow(person.two_factor_auth)
                .to receive(:active?)
                .and_return(true)
              allow(person.two_factor_auth)
                .to receive(:last_verified_at)
                .and_return(1.minute.ago)

              controller = DummyAuthController.new(person)

              expect(controller.two_factor_auth_required?).to eq false
            end
          end

          context "outside of the authentication window" do
            it "returns true" do
              allow(FeatureFlipper)
                .to receive(:show_feature?)
                .and_return(true)
              allow(person.two_factor_auth)
                .to receive(:active?)
                .and_return(true)
              allow(person.two_factor_auth)
                .to receive(:last_verified_at)
                .and_return(3.minutes.ago)

              controller = DummyAuthController.new(person)

              expect(controller.two_factor_auth_required?).to eq true
            end
          end
        end
      end
    end
  end

  describe "#exceeds_max_daily_attempts?" do
    let(:person) { create(:person, :with_two_factor_auth) }

    context "if person has too many two_factor_auth_events" do
      it "returns true for enrollments" do
        4.times do
          create(:two_factor_auth_event, two_factor_auth: person.two_factor_auth, action: "authentication", type: "authentication", page: "enrollments", created_at: 1.hour.ago)
        end

        controller = DummyAuthController.new(person)

        expect(controller.exceeds_max_daily_attempts?(person, :signup)).to be true
      end

      it "returns true for authentication" do
        4.times do
          create(:two_factor_auth_event, two_factor_auth: person.two_factor_auth, action: "back_stage_code_resend", type: "authentication", page: "account_settings", created_at: 1.hour.ago)
        end

        controller = DummyAuthController.new(person)

        expect(controller.exceeds_max_daily_attempts?(person, :resend)).to be true
      end
    end

    context "if person has less than the max two_factor_auth_events" do
      it "returns false for enrollments" do
        controller = DummyAuthController.new(person)

        expect(controller.exceeds_max_daily_attempts?(person, :signup)).to be false
      end

      it "returns false for authentication" do
        create(:two_factor_auth_event, two_factor_auth: person.two_factor_auth, action: "back_stage_code_resend", type: "authentication", page: "account_settings", created_at: 1.hour.ago)
        controller = DummyAuthController.new(person)

        expect(controller.exceeds_max_daily_attempts?(person, :resend)).to be false
      end
    end
  end

  describe "#exceeds_signups_or_resends?" do
    let(:person) { create(:person, :with_two_factor_auth) }

    context "if person has too many two_factor_auth_events" do
      it "returns true on the preferences step" do
        4.times do
          create(:two_factor_auth_event, two_factor_auth: person.two_factor_auth, action: "authentication", type: "authentication", page: "enrollments", created_at: 1.hour.ago)
        end

        controller = DummyAuthController.new(person, "preferences")

        expect(controller.exceeds_signups_or_resends?).to be true
      end

      it "returns true on the authentication step" do
        4.times do
          create(:two_factor_auth_event, two_factor_auth: person.two_factor_auth, action: "back_stage_code_resend", type: "authentication", page: "account_settings", created_at: 1.hour.ago)
        end

        controller = DummyAuthController.new(person, "authentication")

        expect(controller.exceeds_signups_or_resends?).to be true
      end
    end

    context "if person has exceeded two_factor_auth_events from a DIFFERENT step" do
      it "returns false on the preferences step" do
        4.times do
          create(:two_factor_auth_event, two_factor_auth: person.two_factor_auth, action: "back_stage_code_resend", type: "authentication", page: "account_settings", created_at: 1.hour.ago)
        end

        controller = DummyAuthController.new(person, "preferences")

        expect(controller.exceeds_signups_or_resends?).to be false
      end

      it "returns false on the authentication step" do
        @step = "authentication"
        4.times do
          create(:two_factor_auth_event, two_factor_auth: person.two_factor_auth, action: "authentication", type: "authentication", page: "enrollments", created_at: 1.hour.ago)
        end

        controller = DummyAuthController.new(person, "authentication")

        expect(controller.exceeds_signups_or_resends?).to be false
      end
    end

    context "if person has less than the max two_factor_auth_events" do
      it "returns false on the preferences step" do
        controller = DummyAuthController.new(person, "preferences")

        expect(controller.exceeds_signups_or_resends?).to be false
      end

      it "returns false on the authentication step" do
        @step = "authentication"
        create(:two_factor_auth_event, two_factor_auth: person.two_factor_auth, action: "back_stage_code_resend", type: "authentication", page: "account_settings", created_at: 1.hour.ago)
        controller = DummyAuthController.new(person, "authentication")

        expect(controller.exceeds_signups_or_resends?).to be false
      end
    end
  end
end
