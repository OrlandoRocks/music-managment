require 'rails_helper'

describe ExtendAccessToken do
  let(:test_class) { Struct.new(:access_token) { include ExtendAccessToken } }
  let(:token) { test_class.new() }

  it "has a invalid_access_token" do
    expect(token.extend_fb_token("invalid_access_token")).to eq(nil)
  end
end