require "rails_helper"

describe AlbumApp do
  class MockAlbumAppController
    def self.before_action(*args)
    end

    include AlbumApp
    include AlbumHelper
    include ApplicationHelper
    include CountryDomainHelper
    include RegionHelper

    attr_reader :params, :current_user

    def initialize(user)
      @current_user = user
    end

    def controller_name
      'dummy_controller'
    end

    def dummy_controller_params
      params
    end
  end

  describe "#init_album_app_vars" do
    let!(:album) { create(:album) }
    let!(:user) { create(:person, :with_person_plan) }

    required_attributes = [
      :id,
      :"data-album",
      :"data-copyrights-form-enabled",
      :"data-countries",
      :"data-country-website",
      :"data-dayjs-locale",
      :"data-features",
      :"data-genres",
      :"data-language-codes",
      :"data-locale",
      :"data-person",
      :"data-possible-artists",
      :"data-specialized-release-type",
      :"data-release-type-vars"
    ]

    it "defines attributes" do
      allow(I18n).to receive(:locale).and_return 'en'

      allow(FeatureFlipper).to                      receive(:show_feature?).and_return true
      allow_any_instance_of(CountryDomainHelper).to receive(:country_website).and_return('US')
      allow_any_instance_of(RegionHelper).to        receive(:available_in_region?).and_return(true)
      allow_any_instance_of(AlbumHelper).to         receive(:display_name).and_return('foo')
      allow_any_instance_of(ApplicationHelper).to   receive(:language_codes).and_return([])

      params     = ActionController::Parameters.new
      controller = MockAlbumAppController.new(user)

      allow(controller).to receive(:params).and_return(params)

      controller.init_album_app_vars(album, nil)
      result = controller.instance_variable_get(:@album_app_vars)

      expect(required_attributes.all? { |attr| result.key?(attr) }).to eq true

      expect(JSON.parse(result[:"data-release-type-vars"])).to       be_a(Hash)
      expect(JSON.parse(result[:"data-person"])["person_plan"]).to   be_a(Numeric)
      expect(JSON.parse(result[:"data-person"])["plan_features"]).to be_a(Hash)
    end
  end
end
