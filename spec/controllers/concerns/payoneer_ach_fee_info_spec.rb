require 'rails_helper'
include CountryDomainHelper

describe PayoneerAchFeeInfo do
  let(:expected_countries) {
    PayoneerAchFee.where(source_currency: "USD")
                  .order(:target_country_code)
                  .pluck(:target_country_code)
                  .uniq
  }
  let(:fee_info_class) { Struct.new(:payoneer_ach_fee) { include PayoneerAchFeeInfo } }

  before(:each) do
    allow_any_instance_of(CountryDomainHelper).to receive(:country_website).and_return('US')
  end

  describe "#load_payoneer_ach_fee_info" do
    it "should load fees and countries map for the loaded fee info" do
      fee_info = fee_info_class.new
      fee_info.load_payoneer_ach_fee_info

      countries_map = fee_info.instance_variable_get(:@countries_map)
      ach_fees = fee_info.instance_variable_get(:@ach_fees)

      expect(ach_fees.keys).to eq(expected_countries)
      expect(countries_map["India"]).to eq("IN")
      expect(countries_map["Mainland China"]).to eq("CN")
    end

    it "should load only for source currency of current domain fee info" do
      fee_info = fee_info_class.new
      fee_info.load_payoneer_ach_fee_info

      countries_map = fee_info.instance_variable_get(:@countries_map)
      ach_fees = fee_info.instance_variable_get(:@ach_fees)

      expect(ach_fees.keys).to eq(expected_countries)
      expect(countries_map["India"]).to eq("IN")

      allow_any_instance_of(CountryDomainHelper).to receive(:country_website).and_return('UK')

      fee_info = fee_info_class.new
      fee_info.load_payoneer_ach_fee_info

      countries_map = fee_info.instance_variable_get(:@countries_map)
      ach_fees = fee_info.instance_variable_get(:@ach_fees)

      expect(ach_fees.keys).to eq([])
      expect(countries_map).to eq({})
    end

    context 'countries with multiple target currencies' do
      it "should load multiple fees for countries with multiple target currencies" do
        fee_info = fee_info_class.new
        fee_info.load_payoneer_ach_fee_info

        ach_fees = fee_info.instance_variable_get(:@ach_fees)
        expect(ach_fees['AE'].length).to eq(3)
        expect(ach_fees['GU'].length).to eq(2)
      end
      it 'has all of the correct target_currencies' do
        fee_info = fee_info_class.new
        fee_info.load_payoneer_ach_fee_info

        ach_fees = fee_info.instance_variable_get(:@ach_fees)
        india_currencies  = ach_fees['IN'].map(&:target_currency)
        italia_currencies = ach_fees['IT'].map(&:target_currency)
        expect(india_currencies).to eq(['INR'])
        expect(italia_currencies).to eq(['EUR'])
      end
    end

  end

  let(:expected_fees) { PayoneerAchFee.find_by(target_country_code: "US") }
  let(:united_states) { Country.find_by(iso_code: "US") }

  describe '#load_default_ach_fee_info' do
    it "should load fees for the country website" do
      fee_info = fee_info_class.new
      fee_info.load_default_ach_fee_info

      default_fees = fee_info.instance_variable_get(:@default_fees)

      expect(default_fees.country).to eq(united_states)
      expect(default_fees.transfer_fees).to eq(expected_fees.transfer_fees)
      expect(default_fees.minimum_threshold).to eq(expected_fees.minimum_threshold)
    end
  end

end
