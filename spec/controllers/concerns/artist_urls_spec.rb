require "rails_helper"

describe ArtistUrls do
  class ArtistUrlWrapper
    def self.before_action(*args)
    end

    include ArtistUrls

    attr_reader :params, :current_user

    def controller_name
      'dummy_controller'
    end

    def dummy_controller_params
      params
    end
  end

  describe "#map_artist_name_to_artist_url" do
    it "only returns primary_artists" do
      params = ActionController::Parameters.new(
        creatives: [
          { "id" => "45", "role" => "primary_artist", "name" => "artist1" },
          { "id" => "46", "role" => "primary_artist", "name" => "artist2" },
          { "id" => "47", "role" => "djembe", "name" => "artist3" },
        ],
        artist_urls: [
          { "apple" => "http://example.com" },
          { "apple" => "http://example2.com" },
          { "apple" => "http://example3.com" }
        ]
      )
      controller = ArtistUrlWrapper.new
      allow(controller).to receive(:params).and_return(params)

      result = controller.map_artist_name_to_artist_url
      expected_result = [
        { "apple" => "http://example.com", "artist_name" => "artist1" },
        { "apple" => "http://example2.com", "artist_name" => "artist2" }
      ]
      expect(result).to eq(expected_result)
    end
  end
end
