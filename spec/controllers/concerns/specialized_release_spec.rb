require "rails_helper"

describe SpecializedRelease do
  class SpecializedReleaseWrapper
    include SpecializedRelease

    attr_reader :params, :current_user
    attr_accessor :session

    def initialize(album)
      @album = album
      @session = {}
    end

    def controller_name
      'dummy_controller'
    end

    def dummy_controller_params
      params
    end
  end

  describe "popups" do
    before do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
    end

    describe "#set_fb_popup" do
      it "sets popup to fb_promo when it becomes strictly freemium" do
        previous_store_ids = []
        album = create(:album)
        create(:salepoint, salepointable: album, store: Store.find(Store::FB_REELS_STORE_ID))
        
        controller = SpecializedReleaseWrapper.new(album)
        expected = SpecializedRelease::FB_PROMO

        expect(controller.set_discovery_popup(previous_store_ids)).to eq(expected)
      end

      it "sets popup to fb_end when previously freemium" do
        previous_store_ids = [Store::FB_REELS_STORE_ID]
        album = create(:album)
        create(:salepoint, salepointable: album)

        controller = SpecializedReleaseWrapper.new(album)
        expected = SpecializedRelease::FB_END

        expect(controller.set_discovery_popup(previous_store_ids)).to eq(expected)
      end

      it "does NOT set popup to fb_end when not previously freemium" do
        previous_store_ids = [2]
        album = create(:album)
        create(:salepoint, salepointable: album)

        controller = SpecializedReleaseWrapper.new(album)

        expect(controller.set_discovery_popup(previous_store_ids)).to be_nil
      end

      it "sets popup to fb_start when it becomes strictly freemium" do
        previous_store_ids = [2, 3]
        album = create(:album)
        create(:salepoint, salepointable: album, store: Store.find(Store::FB_REELS_STORE_ID))
        
        controller = SpecializedReleaseWrapper.new(album)
        controller.set_discovery_popup(previous_store_ids)
        expected = SpecializedRelease::FB_START

        # Second call in same session
        expect(controller.set_discovery_popup(previous_store_ids)).to eq(expected)
      end
    end

    describe "#fb_upsell" do
      it "sets promo popup when session is clean" do
        album = create(:album)
        create(:salepoint, salepointable: album, store: Store.find(Store::FB_REELS_STORE_ID))

        controller = SpecializedReleaseWrapper.new(album)
        expected = SpecializedRelease::FB_PROMO

        expect(controller.fb_upsell).to eq(expected)
      end

      it "skips promo popup and shows fb_start when session is dirty" do
        album = create(:album)
        create(:salepoint, salepointable: album, store: Store.find(Store::FB_REELS_STORE_ID))

        controller = SpecializedReleaseWrapper.new(album)
        controller.fb_upsell
        expected = SpecializedRelease::FB_START

        # Second call in same session
        expect(controller.fb_upsell).to eq(expected)
      end
    end

    describe "#remove_automator_from_freemium" do
      it "removes automator from freemium release" do
        album = create(:album)
        create(:salepoint, salepointable: album, store: Store.find(Store::FB_REELS_STORE_ID))
        album.add_automator
        expect(album.salepoint_subscription).not_to be_nil

        controller = SpecializedReleaseWrapper.new(album)
        controller.remove_automator_from_freemium
        album.reload

        expect(album.salepoint_subscription).to be_nil
      end

      it "does NOT remove automator from non-freemium release" do
        album = create(:album)
        create(:salepoint, salepointable: album, store: Store.where(discovery_platform: false).is_active.last)
        album.add_automator
        expect(album.salepoint_subscription).not_to be_nil

        controller = SpecializedReleaseWrapper.new(album)
        controller.remove_automator_from_freemium
        album.reload

        expect(album.salepoint_subscription).not_to be_nil
      end
    end
  end
end
