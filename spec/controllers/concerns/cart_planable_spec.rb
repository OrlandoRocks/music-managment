require "rails_helper"

describe CartPlanable, type: :helper do
  let!(:person) { create(:person) }
  let!(:plan_person) { create(:person) }
  let!(:plan_product) { Product.find_by(display_name: "rising_artist", country_website_id: 1) }

  before(:each) do
    helper.extend(MockApplicationHelper)
    allow(helper).to receive(:current_user).and_return(person)
  end

  describe "#hellper.plan_in_cart?" do
    it "is true when a user has a plan in the cart" do
      Product.add_to_cart(person, plan_product)
      expect(helper.plan_in_cart?).to be true
    end

    it "is fase when a user has a plan in the cart" do
      expect(helper.plan_in_cart?).to be false
    end
  end

  describe "#owns_or_carted_plan?" do
    it "is true when a user has a plan" do
      allow(helper).to receive(:current_user).and_return(plan_person)
      create(:person_plan, person: plan_person, plan: Plan.first)

      expect(helper.owns_or_carted_plan?).to be true
    end

    it "is false when a user does not have a plan" do
      expect(helper.owns_or_carted_plan?).to be false
    end
  end

  describe "#reapply_credit_usage_to_releases" do
    let!(:album) { create(:album, person: person) }
    let!(:unpaid_album_purchase) { create(:purchase, :unpaid, person: person, related: album) }
    let!(:product) { TCFactory.build_album_credit_product(1) }
    let!(:purchase) {
      create(:purchase, :unpaid, person: person, product_id: product.id, related: product, paid_at: Time.now)
    }
    let!(:inventory) do
      create(
        :inventory,
        person: person,
        inventory_type: "Album",
        product_item: product.product_items.first,
        purchase: purchase,
        quantity: 1
      )
    end

    it "re-applies credit usages to applicable unpaid purchases" do
      expect { helper.reapply_credit_usage_to_releases }.to change { CreditUsage.count }.by(1)
      expect(CreditUsage.last).to have_attributes({ related_id: album.id, person_id: person.id })
    end
  end
end
