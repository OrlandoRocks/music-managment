require "rails_helper"

describe CountryPhoneCodeable do
  class DummyController
    include CountryPhoneCodeable

    attr_reader :current_user, :params

    def initialize(phone_number, mobile_number, prefix)
      @params = {
          person: {
              phone_number: phone_number,
              mobile_number: mobile_number,
              prefix: prefix
          }
      }
    end

    def country_website
      "IN"
    end
  end
  let!(:phone_number_without_prefix) {"1234567"}
  let!(:mobile_number_without_prefix) {"3456789"}

  let!(:phone_number_with_prefix) {"+921234567"}
  let!(:mobile_number_with_prefix) {"+923456789"}

  let!(:prefix) {"+92"}
  context "given phone_number and mobile_number" do
    context "without prefixes" do
      it "adds +91 to the numbers" do
        controller = DummyController.new(phone_number_without_prefix, mobile_number_without_prefix, nil)
        result = controller.sanitize_phone_number_params
        expected_result = {phone_number: "+911234567", mobile_number: "+913456789"}
        expect(result).to eq(expected_result)
      end
    end
    context "with prefixes" do
      it "returns the numbers themselves" do
        controller = DummyController.new(phone_number_with_prefix, mobile_number_with_prefix, nil)
        result = controller.sanitize_phone_number_params
        expected_result = {phone_number: "+921234567", mobile_number: "+923456789"}
        expect(result).to eq(expected_result)
      end
    end
  end
  context "given mobile_number and prefix" do
    it "returns mobile_number and prefix" do
      controller = DummyController.new(nil, mobile_number_without_prefix, prefix)
      result = controller.sanitize_phone_number_params
      expected_result = {phone_number: nil, mobile_number: "+923456789" }
      expect(result).to eq(expected_result)
    end
  end
end
