require "rails_helper"

describe ReleaseOfferUpdater do
  class ReleaseOfferUpdaterWrapper
    include ReleaseOfferUpdater

    attr_reader :params, :current_user
    attr_accessor :session

    def initialize(user)
      @current_user = user
    end

    def controller_name
      'dummy_controller'
    end

    def dummy_controller_params
      params
    end
  end

  describe "#update_release_offers" do
    it "adds/removes targeted_product_store_offer discount when salepoints are added/removed" do
      person           = create(:person)
      allow(person).to receive(:created_on).and_return(Date.today)

      album_product    = Product.find(Product::US_ONE_YEAR_SINGLE_PRODUCT_ID)
      store_targeted_offer   = create(:targeted_offer, start_date: Time.now - 40.days, expiration_date: Time.now + 10.days, offer_type: "existing", status: "New")
      targeted_product = create(:targeted_product, product: album_product, targeted_offer: store_targeted_offer, price_adjustment: 5.00, price_adjustment_type: "dollars_off")
      store_targeted_offer.update_attribute(:status, "Active")

      album     = create(:single, person: person)
      salepoint = create(:salepoint, salepointable: album, finalized_at: DateTime.now)
      create(:targeted_product_store, targeted_product: targeted_product, store: salepoint.store)
      purchase = Product.add_to_cart(person, album, Product.find(Product::US_ONE_YEAR_SINGLE_PRODUCT_ID))

      # Meets offer criteria
      expect(purchase.discount_cents).to eq 500
      expect(purchase.non_plan_discount?).to be true

      album.salepoints.destroy_all
      controller = ReleaseOfferUpdaterWrapper.new(person)
      controller.update_release_offers
      purchase.reload

      # No longer meets offer criteria
      expect(purchase.discount_cents.zero?).to eq true
      expect(purchase.non_plan_discount?).to be false
      expect(purchase.no_discount?).to be true

      salepoint = create(:salepoint, salepointable: album, finalized_at: DateTime.now)
      controller.update_release_offers
      purchase.reload

      # Meets offer criteria again
      expect(purchase.discount_cents).to eq 500
      expect(purchase.non_plan_discount?).to be true
    end

    it "adds regular targeted_offer discount when there is no targeted_product_store_offer" do
      person           = create(:person)
      allow(person).to receive(:created_on).and_return(Date.today)

      album_product    = Product.find(Product::US_ONE_YEAR_SINGLE_PRODUCT_ID)
      non_targeted_store = Store.find_by(short_name: 'Limewire')
      targeted_offer   = create(:targeted_offer, start_date: Time.now - 40.days, expiration_date: Time.now + 10.days, offer_type: "existing", status: "New")
      targeted_product = create(:targeted_product, product: album_product, targeted_offer: targeted_offer, price_adjustment: 3.00, price_adjustment_type: "dollars_off")
      targeted_offer.update_attribute(:status, "Active")
      create(:targeted_person, {:person => person, :targeted_offer => targeted_offer})

      album     = create(:single, person: person)
      salepoint = create(:salepoint, store: non_targeted_store, salepointable: album, finalized_at: DateTime.now)

      purchase = Product.add_to_cart(person, album, Product.find(Product::US_ONE_YEAR_SINGLE_PRODUCT_ID))

      # Falls back to regular offer
      expect(purchase.discount_cents).to eq 300
      expect(purchase.non_plan_discount?).to be true
    end

    it "adds regular targeted_offer discount when the targeted_product_store_offer criteria is NOT met" do
      person           = create(:person)
      allow(person).to receive(:created_on).and_return(Date.today)

      album_product    = Product.find(Product::US_ONE_YEAR_SINGLE_PRODUCT_ID)
      store_targeted_offer   = create(:targeted_offer, start_date: Time.now - 40.days, expiration_date: Time.now + 10.days, offer_type: "existing", status: "New")
      store_targeted_product = create(:targeted_product, product: album_product, targeted_offer: store_targeted_offer, price_adjustment: 5.00, price_adjustment_type: "dollars_off")
      store_targeted_offer.update_attribute(:status, "Active")
      targeted_store = Store.find_by(short_name: 'Spotify')
      create(:targeted_product_store, targeted_product: store_targeted_product, store: targeted_store)

      targeted_offer   = create(:targeted_offer, start_date: Time.now - 40.days, expiration_date: Time.now + 10.days, offer_type: "existing", status: "New")
      targeted_product = create(:targeted_product, product: album_product, targeted_offer: targeted_offer, price_adjustment: 3.00, price_adjustment_type: "dollars_off")
      targeted_offer.update_attribute(:status, "Active")
      create(:targeted_person, {:person => person, :targeted_offer => targeted_offer})

      album     = create(:single, person: person)
      non_targeted_store = Store.find_by(short_name: 'Limewire')
      salepoint = create(:salepoint, store: non_targeted_store, salepointable: album, finalized_at: DateTime.now)

      purchase = Product.add_to_cart(person, album, Product.find(Product::US_ONE_YEAR_SINGLE_PRODUCT_ID))

      # Falls back to regular offer
      expect(purchase.discount_cents).to eq 300

      salepoint2 = create(:salepoint, store: targeted_store, salepointable: album, finalized_at: DateTime.now)
      controller = ReleaseOfferUpdaterWrapper.new(person)
      controller.update_release_offers
      purchase.reload

      # Has the targeted store, but also a non-targeted store
      expect(purchase.discount_cents).to eq 300
      expect(purchase.non_plan_discount?).to be true

      album.salepoints.where(store_id: non_targeted_store.id).destroy_all
      controller = ReleaseOfferUpdaterWrapper.new(person)
      controller.update_release_offers
      purchase.reload

      # Has only the targeted store
      expect(purchase.discount_cents).to eq 500
      expect(purchase.non_plan_discount?).to be true
    end
  end
end
