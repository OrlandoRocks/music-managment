require 'rails_helper'

describe PaymentFinalizable do

  class MockController < ApplicationController
    include PaymentFinalizable
  end

  subject {MockController.new}

  before do
    allow(SuccessfulPurchaseWorker).to receive(:perform_async)
    allow(PersonPlansPurchaseService).to receive(:process)
    allow(subject).to receive(:optimizely_events)
  end

  describe "#successful_purchase_after_effects" do
    let(:invoice) {create(:invoice)}
    let(:purchase) {create(:purchase)}

    before {invoice.purchases << purchase}
    
    context "after hosted checkout" do
        it "marks purchases as 'processed'" do
            expect(purchase).to receive(:processed!)
            subject.successful_purchase_after_effects(invoice, true)
        end       
    end

    context "after api checkout" do
        it "does not change status of purchases directly" do
            expect_any_instance_of(Purchase).to_not receive(:processed!)
            subject.successful_purchase_after_effects(invoice)
        end
    end
    
  end

  describe "#failed_purchase_after_effects_for_hosted_checkout" do
    let(:invoice) {create(:invoice)}
    let(:purchase) {create(:purchase)}

    before {invoice.purchases << purchase}
    
    it "marks purchases as 'failed'" do
        expect(purchase).to receive(:failed!)
        subject.failed_purchase_after_effects_for_hosted_checkout(invoice)
    end
    
  end
end
