require "rails_helper"

describe ResetPasswordsController do
  let(:person) {TCFactory.create(:person, password: "Testpass123!", password_confirmation: "Testpass123!", is_verified: true, accepted_terms_and_conditions: true )}
  describe "#create" do
    context "with valid recaptcha" do
      before(:each) do
        allow(RecaptchaService).to receive(:verify).and_return(true)
      end
      it "allows the user to submit their email" do
        expect(PasswordResetService).to receive(:reset)
        post :create, params: { person: { email: person.email, password: "Testpass123!" }, "g-recaptcha-response" => "yup" }
      end
    end

    context "with invalid recaptcha" do
      it "does not allow the user to submit their email" do
        allow_any_instance_of(RecaptchaHelper).to receive(:use_recaptcha?).and_return(true)

        expect(PasswordResetService).not_to receive(:reset)
        post :create, params: { person: { email: person.email, password: "Testpass123!" } }
      end
    end

    context "when a user is logged_in and clicks on the 'Request Password Reset' button" do
      it "should send the user an email to reset their password" do
        allow(subject).to receive(:current_user).and_return(person)
        expect(PasswordResetService).to receive(:reset).with(person.email, :user)
        post :create, params: { person: { email: person.email, password: "Testpass123!" } }
      end
    end
  end
end
