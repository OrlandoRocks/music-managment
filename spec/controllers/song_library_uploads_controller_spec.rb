require "rails_helper"

describe SongLibraryUploadsController, type: :controller do
  before(:each) do
    login_as(create(:person))
    @mp3_file = Rack::Test::UploadedFile.new("spec/assets/track_90_seconds.mp3", "audio/mpeg")
  end

  describe "user creates a song_library_upload" do
    it "should create a new song_library_upload when valid" do
      original_count = SongLibraryUpload.count

      post :create, params: {
        genre_id: "1",
        name: "Foo Bar",
        artist_name: "Artist Name",
        song_library_asset: @mp3_file
      }

      expect(flash[:error]).to_not be_present
      expect(response).to redirect_to(purchase_soundout_reports_url)
      expect(SongLibraryUpload.count).to be(original_count + 1)
    end

    it "should not create a new song_library_upload when missing params" do
      original_count = SongLibraryUpload.count

      post :create, params: {
        genre_id: "1",
        # name: "Foo Bar",
        artist_name: "Artist Name",
        song_library_asset: @mp3_file
      }

      expect(flash[:error]).to be_present
      expect(response).to redirect_to(purchase_soundout_reports_url)
      expect(SongLibraryUpload.count).to be(original_count)
    end
  end
end
