require "rails_helper"

describe PersonSubscriptionsController do
  let(:person) { create(:person) }

  before(:each) do
    allow(subject).to receive(:current_user).and_return(person)
  end

  describe "#update" do
    it "cancels the person subscription" do
      expect(Subscription::SubscriptionRequest)
        .to receive(:cancel)
        .with(person, "Social")

      put :update, params: { subscription_type: "Social" }, format: :js
    end
  end
end
