require "rails_helper"

describe PayoutProviderLoginLandingsController do
  describe "#show" do
    it "redirects to dashboard if already approved" do
      person = create(:person)
      payout_provider = create(:payout_provider, person: person, provider_status: PayoutProvider::APPROVED)
      login_as(person)

      get :show

      expect(payout_provider.provider_status).to eq PayoutProvider::APPROVED
      expect(request).to redirect_to dashboard_path
    end

    it "updates to approved and redirects to withdraw page" do
      person = create(:person)
      payout_provider = create(:payout_provider, person: person, provider_status: PayoutProvider::ONBOARDING)
      login_as(person)

      get :show

      expect(payout_provider.reload.provider_status).to eq PayoutProvider::PENDING
      expect(request).to redirect_to withdraw_path
    end
  end
end
