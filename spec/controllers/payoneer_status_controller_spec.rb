require "rails_helper"

describe PayoneerStatusController do
  let(:person) { create(:person) }

  describe "#index" do
    context "#payoneer_payout returns true" do
      it "should redirect to withdraw path" do
        login_as(person)
        allow(Payoneer::FeatureService).to receive(:payoneer_payout_enabled?).with(person).and_return(true)
        get :index

        expect(response.status).to eq(200)
      end
    end
  end
end
