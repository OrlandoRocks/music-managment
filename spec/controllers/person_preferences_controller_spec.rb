require "rails_helper"

describe PersonPreferencesController, type: :controller do

  before(:each) do
    @person = TCFactory.create(:person)

    pp = PersonPreference.new
    pp.do_not_autorenew = 1
    pp.person=@person
    expect(pp.save(:validate=>false)).to eq(true)

    @person.is_verified=1
    @person.save

    login_as(@person)
    allow(Braintree::PaymentMethod).to receive(:delete).and_return(true)
  end

  #This posts to the person preferences create to update auto renewal
  it "should enable auto renew" do
    expect {
      post :create, params: { enable_auto_renewal: 1 }
      assert_redirected_to person_preferences_path
    }.to change(Note,:count).by(1)

    @person.reload
    expect(@person.person_preference.do_not_autorenew).to eq(0)
  end

  context "legacy customer who autorenew disabled before we archived stored payment methods" do

    it "should enable auto renew and disable stored payment methods" do

      mock_braintree_query()

      @person.person_preference.preferred_credit_card_id    = TCFactory.create(:stored_credit_card, :person_id=>@person.id).id
      @person.person_preference.preferred_paypal_account_id = TCFactory.create(:stored_paypal_account, :person=>@person).id

      expect(@person.stored_credit_cards.active.count).to be > 0
      expect(@person.stored_paypal_accounts.active.count).to be > 0

      expect {
        post :create, params: { enable_auto_renewal: 1 }
        assert_redirected_to person_preferences_path
      }.to change(Note, :count).by(2)

      @person.reload
      expect(@person.person_preference.do_not_autorenew).to eq(0)

      expect(@person.stored_credit_cards.active.count).to eq(0)
      expect(@person.stored_paypal_accounts.active.count).to eq(0)
    end
  end

  context "#update" do
    it "should create note on person_preference update" do
      old_credit_card   = create(:stored_credit_card, person: @person)
      new_credit_card   = create(:stored_credit_card, person: @person)
      person_preference = create(:person_preference, person: @person, preferred_credit_card: old_credit_card)

      expect {
        put :update, params: { id: person_preference.id, person_preference: { preferred_credit_card_id: new_credit_card.id }, pay_with_balance: true }
      }.to change(Note, :count).by(2)
    end

    it "updates switching from Paypal account to a credit card" do
      paypal_account = create(:stored_paypal_account, person: @person)
      credit_card   = create(:stored_credit_card, person: @person)
      person_preference = create(:person_preference, person: @person, preferred_credit_card: credit_card, preferred_paypal_account: paypal_account, preferred_payment_type: "PayPal")

      expect {
        put :update, params: { id: person_preference.id, person_preference: { preferred_payment_type: "CreditCard", preferred_paypal_account_id: paypal_account.id }, pay_with_balance: true }
      }.to change(Note, :count).by(2)
      expect(person_preference.reload.preferred_payment_type).to eq("CreditCard")
    end
  end
end
