require 'rails_helper'

describe StoredBankAccountsController do
  let(:person) {create(:person)}
  let(:payout_provider) { create(:payout_provider, person: person, provider_status: "approved") }
  let(:person_with_bank_account) { FactoryBot.create(:person, :with_stored_bank_account) }

  context "#new" do

    before :each do
      login_as person
    end

    it "should render the new page" do
      get :new
      expect(response.status).to eq(200)
    end
  end

  xcontext "#edit" do
    before :each do
      login_as person_with_bank_account
    end

    # `edit` action requires an ID
    xit "should render the edit page" do
      get :edit
      expect(response.status).to eq(200)
    end
  end

  context "#process_response" do
    xit "has test coverage"
  end
end
