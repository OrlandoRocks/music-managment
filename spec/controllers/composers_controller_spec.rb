require "rails_helper"

describe ComposersController, type: :controller do
  before(:each) do
    allow($believe_api_client).to receive(:post)
  end

  context "When a composer has already been created" do
    before(:each) do
      allow_any_instance_of(Believe::ApiClient).to receive(:post).and_return(true)
      login_as(person_with_publishing_access)
      setup_composer
      @person.composers = [@composer]
      allow_any_instance_of(ComposersController).to receive(:rights_app_enabled?).and_return(false)
    end

    it "should be able to update a composers registration info", run_sidekiq_worker: true do
      expect(@composer.publisher).to be_nil
      expect(@composer.first_name).not_to eq("Foo")
      expect(@composer.last_name).not_to eq("Bar")

      post :update, params: {
        "composer" => {
          "dob" => {
            "month" => "01",
            "day" => "01",
            "year" => "1900"
          },
          "alternate_email" => "",
          "agreed_to_terms" => "1",
          "country" => "US",
          "cae" => "123456789",
          "name_suffix" => "",
          "last_name" => "Bar",
          "name_prefix" => "",
          "alternate_phone" => "",
          "agreed_to_terms_at" => Time.current,
          "middle_name" => "",
          "first_name" => "Foo"
        },
        "publisher" => {
          "name" => "DKPUB",
          "cae" => "123456789",
          "performing_rights_organization_id" => "56"
        },
        id: @composer.id
      }

      expect(response).to redirect_to("/products/add_composer_registration_to_cart?composer_id=#{@composer.id}")

      @composer.reload
      expect(@composer.publisher.name).to eq("DKPUB")
      expect(@composer.publisher.cae).to eq("123456789")
      expect(@composer.publisher.performing_rights_organization_id).to eq(56)
      expect(@composer.first_name).to eq("Foo")
      expect(@composer.last_name).to eq("Bar")
    end

    it "should be able to go to the edit page" do
      get :edit, params: { id: @composer.id }

      expect(response).to render_template('composers/registration')
    end

  end

  context "When not logged in and trying to access composers index page" do
    it "should redirect user to login page" do
      get :index
      expect(response).to redirect_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
    end
  end

  context "When logged in" do
    before(:each) do
      login_as(person)
      allow_any_instance_of(ComposersController).to receive(:rights_app_enabled?).and_return(false)
    end

    it "should redirect me to home page if I dont have access to publishing feature" do
      get :index
      expect(response).to redirect_to('/')
    end
  end

  describe ComposersController, "When logged in with publishing access", run_sidekiq_worker: true do
    before(:each) do
      allow_any_instance_of(Believe::ApiClient).to receive(:post).and_return(true)
      login_as(person_with_publishing_access)
      setup_composer
      allow_any_instance_of(ComposersController).to receive(:rights_app_enabled?).and_return(false)
    end

    it "should allow me to access the composer form" do
      get :new
      expect(response).to render_template('composers/registration')
    end

    it "should allow me to create a composer record" do
      setup_publishing_product
      created_by = Person.find_by(email: "admin@tunecore.com")
      p = Product.find_by(name: SONGWRITER_PRODUCT_NAME)
      p = Product.create(created_by_id: created_by.id, name: SONGWRITER_PRODUCT_NAME ) if p.nil?

      post :create, params: {"composer"=>
                     {"dob"=>{"month"=>"01","day"=>"01","year"=>"1900"},
                      "alternate_email"=>"",
                      "agreed_to_terms"=>"1", "country"=>"US",
                      "cae"=>"", "name_suffix"=>"", "last_name"=>"Cheung",
                      "name_prefix"=>"", "alternate_phone"=>"", "agreed_to_terms_at"=> Time.now,
                      "middle_name"=>"", "first_name"=>"Ed"},
                      "publisher"=>{"name"=>"", "cae"=>""}}

      c = Composer.last
      expect(response).to redirect_to("/products/add_composer_registration_to_cart?composer_id=#{c.id}")

    end

    it "should redirect me to index page when trying to hit show action" do
      c = Composer.first
      get :show, params: { id: c.id }
      expect(response).to redirect_to('/composers')
    end

    it "should redirect me to index page when trying to hit destroy action" do
      c = Composer.first
      delete :destroy, params: { id: c.id }
      expect(response).to redirect_to('/composers')
    end
  end

  context "when rights app is enabled" do
    before(:each) do
      allow_any_instance_of(ComposersController).to receive(:rights_app_enabled?).and_return(true)
      login_as(person_with_publishing_access)
    end

    context "when the user does not have a publishing_composer record" do
      describe "#index" do
        it "redirects to the publishing roles controller" do
          get :index
          expect(response).to redirect_to new_publishing_administration_enrollment_path
        end

        it "redirects to the publishing roles controller" do
          get :new
          expect(response).to redirect_to new_publishing_administration_enrollment_path
        end
      end
    end

    context "when the user has a publishing_composer record" do
      describe "#new" do
        it "redirects to the publishing dashboard" do
          publishing_composer = create(:publishing_composer)
          @person.publishing_composers << publishing_composer
          @person.account = publishing_composer.account
          get :index
          expect(response).to redirect_to publishing_administration_composers_path
        end

        it "redirects to the publishing roles controller" do
          publishing_composer = create(:publishing_composer)
          @person.publishing_composers << publishing_composer
          @person.account = publishing_composer.account
          get :new
          expect(response).to redirect_to publishing_administration_composers_path
        end
      end
    end
  end
end

private

def person_with_publishing_manager_role
  @publishing_manager = TCFactory.build_role(
    { id: 1,
      name: "Publishing Manager",
  }
  )
  @publishing = Feature.find_or_create_by(name: "publishing")
  @publishing.restrict_by_user = true
  @publishing.save
  @person = TCFactory.build_person(
    {
      features: [@publishing],
      roles: [@publishing_manager]
    }
  )
end

def person_with_publishing_access
  @publishing = Feature.find_or_create_by(name: "publishing")
  @publishing.restrict_by_user = true
  @publishing.save

  @person = TCFactory.build_person(
    {
      features: [@publishing]
    }
  )
end

def person
  @publishing = Feature.find_or_create_by(name: "publishing")
  @publishing.restrict_by_user = true
  @publishing.save
  @person = TCFactory.build_person
end

def setup_composer
  @composer = TCFactory.build_composer(
    agreed_to_terms: 1
  )
end

def setup_publishing_product
  #TODO: extract this to TCFactory::Base
  @product_name = SONGWRITER_PRODUCT_NAME
  @display_name = "Songwriter Service"
  @description =  "Collecting, licensing and policing your songwriter copyrights"
  unless Product.find_by(name: @product_name)
    product = Product.new(name: @product_name,
                          display_name: @display_name,
                          description: @description,
                          status: 'Active',
                          product_type: 'Ad Hoc',
                          is_default: true,
                          sort_order: 1,
                          renewal_level: 'None',
                          price: 75.00,
                          currency: 'USD',
                          created_by_id: 15536,
                          applies_to_product: 'Composer',
                          country_website_id: 1,
                          original_product_id: 0)
    product.id = Product::SONGWRITER_SERVICE_ID
    product.save!

    product_item = ProductItem.create(product: product,
                                      name: @product_name,
                                      description: @description,
                                      price: 75.00,
                                      currency: 'USD')
    ProductItemRule.create(product_item: product_item,
                           inventory_type: 'Composer',
                           rule_type: 'inventory',
                           rule: 'price_for_each',
                           quantity: 1,
                           price: 75.00,
                           currency: 'USD')
  end
end
