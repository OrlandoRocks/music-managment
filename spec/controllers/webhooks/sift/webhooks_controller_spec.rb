require "rails_helper"

describe Webhooks::Sift::WebhooksController, type: :controller do
  describe "#create", :type => :request do
    let(:person) { create(:person) }

    context 'with a valid payload' do
      let(:body) do
        {
          entity: {
          id: person.id,
            type: "user"
          },
          decision: {
            id: "payment_abuse"
          },
          time: 1655147136352
        }
      end

      it "returns a 200 status" do
        digest  = OpenSSL::Digest.new('sha256')
        calculated_hmac = OpenSSL::HMAC.hexdigest(digest, '123456', body.to_json)
        signature = "sha256=#{calculated_hmac}"

        allow(ENV).to receive(:fetch).with(any_args).and_call_original
        allow(ENV).to receive(:fetch).with("X_SIFT_SCIENCE_SIGNATURE_KEY").and_return('123456')
        allow(subject).to receive(:authenticate_signature).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
        expect(Webhooks::WebhookWorker).to receive(:perform_async)

        post "/webhooks/sift", params: body.to_json, headers: { 'HTTP_X_SIFT_SCIENCE_SIGNATURE': signature }
        expect(response.status).to eq(200)
      end
    end

    context 'with an invalid payload' do
      let(:body) do
        {}
      end

      it "returns a 400 status" do
        digest  = OpenSSL::Digest.new('sha256')
        calculated_hmac = OpenSSL::HMAC.hexdigest(digest, '123456', body.to_json)
        signature = "sha256=#{calculated_hmac}"

        allow(ENV).to receive(:fetch).with("X_SIFT_SCIENCE_SIGNATURE_KEY").and_return('123456')
        allow(subject).to receive(:authenticate_signature).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

        post "/webhooks/sift", params: body.to_json, headers: { 'HTTP_X_SIFT_SCIENCE_SIGNATURE': signature }
        expect(response.status).to eq(400)
      end
    end

    context 'with an invalid signature' do
      let(:body) do
        {}
      end

      it "returns a 400 status" do
        digest  = OpenSSL::Digest.new('sha256')
        calculated_hmac = OpenSSL::HMAC.hexdigest(digest, '123456', body.to_json)
        signature = "sha256=#{calculated_hmac}"

        allow(ENV).to receive(:fetch).with("X_SIFT_SCIENCE_SIGNATURE_KEY").and_return('123458')
        allow(subject).to receive(:authenticate_signature).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

        post "/webhooks/sift", params: body.to_json, headers: { 'HTTP_X_SIFT_SCIENCE_SIGNATURE': signature }
        expect(response.status).to eq(403)
      end
    end
  end
end
