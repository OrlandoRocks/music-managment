require "rails_helper"

xdescribe Ytm::SalepointSongsController, type: :controller do

  describe "For non-admin user" do

    before(:each) do
      @person = TCFactory.build_person
      login_as(@person)
    end

    it "should properly redirect user who does not have permissions" do
      get :index
      assert_redirected_to dashboard_path
    end

    it "should properly allow access to user who does have permissions" do
      @person.roles << Role.find_by(name: "YTM Approver")
      get :index
      expect(response).to be_successful
    end
  end

  describe "For admin user" do

    before(:each) do
      @person = TCFactory.build_person(force_is_administrator: true)
      login_as(@person)
    end

    it "should properly redirect user who does not have permissions" do
      get :index
      assert_redirected_to admin_home_path
    end

    it "should properly allow access to user who does have permissions" do
      @person.roles << Role.find_by(name: "YTM Approver")
      get :index
      expect(response).to be_successful
    end
  end
end
