require "rails_helper"

describe YoutubeMonetizationsController do
  let(:person) { create(:person) }

  before(:each) do
    login_as(person)
  end

  describe "#setup_ytm" do
    it "redirects if user has a terminated YTM record" do
      create(:youtube_monetization, person_id: person.id, termination_date: DateTime.now - 1.days)

      params = {
        agree_to_terms: true,
        product_type: 'youtube-money',
        ytm_product_id: 145
      }

      get :setup_ytm, params: params

      expect(response.status).to eq 302
      expect(response).to redirect_to(add_to_cart_product_url(id: params[:ytm_product_id], product_type: params[:product_type]))
    end
  end
end
