require "rails_helper"

describe YoutubeTrackMonetization::DashboardsController do

  describe "#show" do
    let(:person) { create(:person) }

    before(:each) do
      allow(FeatureFlipper).to receive(:show_feature?) { true }
      allow_any_instance_of(TrackMonetization::DashboardSerializer).to receive(:discovery).and_return(false)
    end

    context "when the user does not subscribe to YTTracks" do
      it "redirects to the dashboard if the user does not subscribe to YTTracks" do
        login_as(person)
        get :show
        expect(request).to redirect_to dashboard_path
      end

      context ", but has a YTM release" do
        let(:album) { create(:album, :finalized, person: person) }

        before do
          create(:salepoint, salepointable: album, store_id: Store::YTSR_PROXY_ID)
        end

        it ", does NOT redirect to the dashboard" do
          login_as(person)
          get :show
          expect(request.url).to eq "http://test.host/youtube_tracks/dashboard"
        end
      end
    end

    context "when the user subscribes to YTTracks" do
      ytsr = Store.find_by(abbrev: "ytsr")
      let!(:track) { create(:track_monetization, :eligible, person: person, store: ytsr) }
      let(:song)   { track.song }
      let(:album)  { song.album }
      let(:expected_track_data) do
        [{ id:        track.id,
          song:       song.name,
          isrc:       song.tunecore_isrc,
          artist:     song.artist.name,
          discovery:  false,
          appears_on: album.name,
          upc:        album.upc.number,
          status:     "new",
          tc_distributor_state: nil
        }.with_indifferent_access]
      end

      before(:each) do
        create(:person_subscription_status,
              :with_subscription_event_and_purchase,
              subscription_type: "YTTracks",
              person: person)
        login_as(person.reload)
      end

      it "returns track data" do
        get :show, params: { format: :json }
        expect(JSON.parse(response.body)).to eq expected_track_data
      end
    end

    context "when the user has an active youtube_monetization" do
      it "returns track data" do
        ytsr = Store.find_by_abbrev("ytsr")
        track = create(:track_monetization, :eligible, person: person, store: ytsr)
        song = track.song
        album = song.album
        expected_track_data = [
          {
            id: track.id,
            song: song.name,
            isrc: song.tunecore_isrc,
            artist: song.artist.name,
            discovery:  false,
            appears_on: album.name,
            upc: album.upc.number,
            status: "new",
            tc_distributor_state: nil
          }.with_indifferent_access
        ]

        Timecop.freeze

        create(:youtube_monetization, effective_date: Time.now, person: person)

        Timecop.travel(Time.now + 3.years)

        login_as(person.reload)

        get :show, params: { format: :json }
        expect(JSON.parse(response.body)).to eq expected_track_data

        Timecop.return
      end
    end
  end
end
