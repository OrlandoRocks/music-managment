require 'rails_helper'

describe SongsController do
  let(:person) { create(:person) }
  let(:album) { create(:album, person: person) }

  before :each do
    login_as(person)
  end

  describe '#index' do
    it 'should get a list of songs for the album' do
      song = create(:song, album_id: album.id)

      get :index, params: { album_id: album.id }
      expect(response.status).to eq(200)
    end
  end

  describe '#new' do
    it 'should render the songs form' do
      get :new, params: { album_id: album.id }
      expect(response).to render_template(:new)
      expect(response.status).to eq(200)
    end
  end

  describe '#create' do
    it 'should create a new song for the album successfully' do
      expect {
        post :create, params: {
          "album_id": album.id,
          "song": {
            "clean_version": "0",
            "creatives": [
              {
                "name": "Les Brown & The Band of Renown",
                "role": "primary_artist"
              },
              {
                "name": "The Four Preps",
                "role": "primary_artist"
              },
              {
                "name": "Doris Day",
                "role": "primary_artist"
              },
              {
                "name": "Taj Mahal",
                "role": "primary_artist"
              },
              {
                "name": "Omar Torrez & Orpheus",
                "role": "primary_artist"
              },
              {
                "name": "Adrianne Leon",
                "role": "primary_artist"
              },
              {
                "name": "Karen Hammack",
                "role": "primary_artist"
              },
              {
                "name": "Roger Connelly & The Blues Merchants",
                "role": "primary_artist"
              },
              {
                "name": "Billy Fox",
                "role": "primary_artist"
              },
              {
                "name": "Dave Koz",
                "role": "primary_artist"
              },
              {
                "name": "Les Brown & The Bank of Reknown",
                "role": "featuring"
              }
            ],
            "name": "Sing, Sing, Sing",
            "parental_advisory": "0",
            "track_num": "1"
          },
          "utf8": "✓"
        }
      }.to change {
        Song.count
      }.by(1)
    end
  end

  describe '#update' do
    let(:song) { create(:song, album: album) }

    it 'should update an existing song successfully' do
      put :update, params: {
        "id": song.id,
        "album_id": album.id,
        "song": {
          "clean_version": "0",
          "creatives": [
            {
              "name": "Les Brown & The Band of Renown",
              "role": "primary_artist"
            },
            {
              "name": "The Four Preps",
              "role": "primary_artist"
            },
            {
              "name": "Doris Day",
              "role": "primary_artist"
            },
            {
              "name": "Taj Mahal",
              "role": "primary_artist"
            },
            {
              "name": "Omar Torrez & Orpheus",
              "role": "primary_artist"
            },
            {
              "name": "Adrianne Leon",
              "role": "primary_artist"
            },
            {
              "name": "Karen Hammack",
              "role": "primary_artist"
            },
            {
              "name": "Roger Connelly & The Blues Merchants",
              "role": "primary_artist"
            },
            {
              "name": "Billy Fox",
              "role": "primary_artist"
            },
            {
              "name": "Dave Koz",
              "role": "primary_artist"
            },
            {
              "name": "Les Brown & The Bank of Reknown",
              "role": "featuring"
            }
          ],
          "name": "Sing, Sing, Sing, Sing",
          "parental_advisory": "0",
          "track_num": "1"
        },
        "utf8": "✓"
      }, xhr: true
      song.reload
      expect(song.name).to eq('Sing, Sing, Sing, Sing')
    end
  end
end
