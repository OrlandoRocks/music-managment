require "rails_helper"

describe IndexController do
  render_views

  describe "#index" do
    let(:person) { create(:person) }

    before(:each) do
      allow(subject).to receive(:current_user).and_return(person)
    end

    it "redirects a logged in user to their dashboard" do
      login_as(people(:tunecore_admin))
      get :index, params: { pb: "fl" }
      assert_redirected_to dashboard_path(pb: "fl")
    end

    context "bizdev form" do
      it "creates a tunecore cookie" do
        get :bizdev, params: { ref: "applemusicartist", jt: "JoinNow" }
        expect(cookies).to have_key("tunecore_wp_1")
        expect(JSON.parse(cookies["tunecore_wp_1"])).to have_key("ref")
        expect(JSON.parse(cookies["tunecore_wp_1"])).to have_key("jt")
      end

      it "doesn't create a tunecore cookie" do
        get :bizdev, params: { garbage: "gobbldygook", junk: "gunk" }
        expect(cookies).not_to have_key("tunecore_wp_1")
      end
    end
  end
end
