require "rails_helper"

describe StoredCreditCardsController, type: :controller do
  let(:person) { create(:person) }
  let(:payos_customer) { create(:payments_os_customer, person: person) }
  describe '#create' do
    it "should create a stored credit card successfully" do
      card_token = "99a1864d-b7ac-4c50-9d17-b87818a55269"

      allow(FeatureFlipper).to receive(:show_feature?) { true }
      allow(FeatureFlipper).to receive(:show_feature?).with(:sift, an_instance_of(Person)).and_return(false)
      allow(StoredCreditCard).to receive(:create_with_payments_os).and_return([:ok, OpenStruct.new({
        "token": card_token,
        "token_type": "credit_card",
        "state": "created",
        "holder_name": "John Mark",
        "expiration_date": "10/2029",
        "last_4_digits": "1111",
        "pass_luhn_validation": true,
        "vendor": "VISA",
        "card_type": "CREDIT",
        "country_code": "USA",
        "billing_address": {
          "country": "USA",
          "state": "NY",
          "city": "NYC",
          "line1": "fifth avenue 10th"
        },
      })])

      login_as(person)
      country_state = create(:country_state)
      post :create, params: {
        payments_os_token: card_token, payment_vendor: "payments_os", first_name: person.first_name,
        last_name: person.last_name, phone_number: person.phone_number,
        company: "Tunecore", address1: person.address1, address2: person.address2, state_city_name: "New City",
        state_id: country_state.id, country: person.country_domain, zip: person.zip
      }
      expect(response.flash.notice).to eq("Your credit card was added.")
      person.reload
      expect(person.state).to eq(country_state.name)
      expect(person.city).to eq("New City")
    end
  end
end
