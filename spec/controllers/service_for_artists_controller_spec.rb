require "rails_helper"

describe ServiceForArtistsController, type: :controller do
  let(:person) { create(:person) }

  let(:artist_obj) do
    [
      OpenStruct.new({
        artist_name: 'xoxo',
        identifier: '123'
      })
    ]
  end

  before do
    login_as(person)

    allow_any_instance_of(ServiceForArtists::QueryBuilder)
      .to receive(:artists_with_album_ids)
      .and_return(artist_obj)
  end

  describe "#apple" do
    context "user does not have amfa enabled" do
      before { allow(controller).to receive(:amfa_enabled?).and_return(false) }

      it "should redirect to dashboard" do
        get :apple

        expect(response).to redirect_to '/dashboard'
      end
    end

    context "user has amfa enabled" do
      before { allow(controller).to receive(:amfa_enabled?).and_return(true) }

      it "loads artists sets selected artist" do
        get :apple

        expect(controller.instance_variable_get(:@selected_artist)).to eq('123')
      end
    end
  end

  describe "#spotify" do
    context "user does not have amfa enabled" do
      before { allow(controller).to receive(:spotify_enabled?).and_return(false) }

      it "should redirect to dashboard" do
        get :spotify

        expect(response).to redirect_to '/dashboard'
      end
    end

    context "user has amfa enabled" do
      before { allow(controller).to receive(:spotify_enabled?).and_return(true) }

      it "does not preload a selected artist" do
        get :spotify

        expect(controller.instance_variable_get(:@selected_artist)).to eq(nil)
      end
    end
  end

  describe '#spotify_access' do
    context "with logged in user" do

      it 'caches the current user id and external artist uri' do
        get :spotify_access, params: { artists: "someexternaluri" }

        expect($redis.get("spotifyartist:user_id:#{person.id}")).to eq("someexternaluri")
      end


      it 'redirects to Spotify for artists authentication page' do
        get :spotify_access, params: { artists: "someexternaluri" }

        expect(response.redirect_url).to start_with("https://accounts.spotify.com/authorize?client_id=")
        expect(response.code).to eq("302")
      end
    end
  end

  describe '#spotify_redirect_access' do

    context 'when user grants access to TuneCore' do
      context 'When onboarding to Spotify for artists is successful' do
        let(:onboard_url) { "https://artists.spotify.com/c/distributor/invite/artist_uri_to_redirect_to" }

        before do
          allow($redis).to receive(:get).and_return("someexternaluri")
          allow(Spotify::SpotifyForArtistsService).to receive(:request_spotify_for_artists_access)
          .and_return(onboard_url)
          allow_any_instance_of(Spotify::SpotifyForArtistsService).to receive(:redirect_path).and_return("Https://a_redirect_url.com")
        end

        it 'redirects the user to url provided by spotify callback' do
          get :spotify_redirect_access, params: { code: "an_auth_token" }

          expect(response.code).to eq("302")
          expect(response.redirect_url).to eq(onboard_url)
        end
      end

      context 'When onboarding to Spotify for artists fails' do
        let(:error_url) { "https://artists.spotify.com/c/distributor/invite/invalid" }

        before do
          allow($redis).to receive(:get).and_return("someexternaluri")
          allow(Spotify::SpotifyForArtistsService).to receive(:request_spotify_for_artists_access)
          .and_return(error_url)
        end

        it 'redirects the user to error page provided by Spotify' do
          get :spotify_redirect_access, params: { code: "an_auth_token" }

          expect(response).to redirect_to(error_url)
          expect(response.code).to eq("302")
        end
      end

      context 'When there is no artist chached in redis' do
        it 'redirects the user to error page provided by Spotify' do
          get :spotify_redirect_access, params: { code: "an_auth_token" }

          expect(response).to redirect_to(spotify_for_artists_path)
          expect(response.code).to eq("302")
        end
      end
    end

    context 'when user denies access to TuneCore' do
      let(:spotify_error_url) {  "https://artists.spotify.com/c/distributor/invite/invalid" }

      it 'redirects the user to generic Spotify for Artists Page' do
        get :spotify_redirect_access, params: { error: "access_denied" }

        expect(response).to redirect_to(spotify_error_url)
        expect(response.code).to eq("302")
      end
    end
  end

  describe "#authenticate_youtube_user" do

    it 'caches the current user id and their selected artist name' do
        get :authenticate_youtube_user, params: { artists: "Martha $tweart" }

        expect($redis.get("youtubeartist:user_id:#{person.id}")).to eq("Martha $tweart")
    end

    it "redirects user to youtube authentication page" do

      get :authenticate_youtube_user, params: { artists: "Martha $tweart" }

      expect(response.redirect_url).to start_with(YoutubeMusic::OfficialArtistChannelService::AUTH_URL)
      expect(response.code).to eq("302")
    end
  end

  describe "#youtube_auth_callback" do
    let(:artist_name) { "Martha $tweart" }
    let(:params) { {
      code: "authorize_martha!",
      scope: YoutubeMusic::OfficialArtistChannelService::SCOPE_URLS
      }
    }
    before do
     $redis.set("youtubeartist:user_id:#{person.id}", artist_name)
     allow_any_instance_of(YoutubeMusic::OfficialArtistChannelService).to receive(:fetch_and_store_channel).and_return(true)
     allow_any_instance_of(YoutubeMusic::OfficialArtistChannelService).to receive(:enqueue_distribution!).and_return(true)
    end

    it "extracts artist name from redis and deletes cached key" do
      get :youtube_auth_callback, params: params
      expect($redis.get("youtubeartist:user_id:#{person.id}")).to be(nil)
      expect(@controller.instance_variable_get(:@artist_name)).to eq(artist_name)
    end

    it "creates an instance of YoutubeMusic::OfficialArtistChannelService with youtube_response params" do
      expected_params = ActionController::Parameters.new({
        code: "authorize_martha!",
        scope: YoutubeMusic::OfficialArtistChannelService::SCOPE_URLS,
        person: person,
        artist_name: artist_name
      })
      expected_params.permit!
      expect(YoutubeMusic::OfficialArtistChannelService).to receive(:new).with(expected_params).and_call_original
      get :youtube_auth_callback, params: params
    end

    it "calls create_esi_entries on YoutubeMusic::OfficialArtistChannelService with 'youtube_authorization" do
      expect_any_instance_of(YoutubeMusic::OfficialArtistChannelService).to receive(:fetch_and_store_channel)
      get :youtube_auth_callback, params: params
    end

    it "calls create_esi_entries on YoutubeMusic::OfficialArtistChannelService with 'youtube_authorization" do
      expect_any_instance_of(YoutubeMusic::OfficialArtistChannelService).to receive(:enqueue_distribution!)
      get :youtube_auth_callback, params: params
    end
  end

  describe "#youtube_oac" do
    before do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
      allow(FeatureBlacklist).to receive(:exists?).and_return(false)
    end

    context "when the user has 3 songs from 3 albums for YT Music" do
      it "there are 3 eligible artists" do
        3.times do
          album        = create(:album, :approved, person: person, style_review_state: "APPROVED")
          petri_bundle = create(:petri_bundle, album: album)
          distribution = create(:distribution, converter_class: "MusicStores::Google::Converter", petri_bundle: petri_bundle)
          distribution.update(state: 'delivered')
          salepoint    = create(:salepoint, salepointable: album, store: Store.find_by(short_name: "Google"))
          distribution.salepoints << salepoint
          create(:song, album: album)
        end

        expected = controller.instance_eval { oac_eligible_artists }
        expect(expected.size).to eq(3)

        show = controller.instance_eval { show_yt_oac? }
        expect(show).to eq true
      end
    end

    context "when the user has 3 songs on 1 album for YT Music" do
      it "there are 3 eligible artists" do
        album        = create(:album, :approved, person: person, style_review_state: "APPROVED")
        petri_bundle = create(:petri_bundle, album: album)
        distribution = create(:distribution, converter_class: "MusicStores::Google::Converter", petri_bundle: petri_bundle)
        distribution.update(state: 'delivered')
        salepoint    = create(:salepoint, salepointable: album, store: Store.find_by(short_name: "Google"))
        distribution.salepoints << salepoint
        3.times do
          create(:song, album: album)
        end

        expected = controller.instance_eval { oac_eligible_artists }
        expect(expected.size).to eq(1)

        show = controller.instance_eval { show_yt_oac? }
        expect(show).to eq true
      end
    end

    context "when the user has 1 song for YT Music" do
      it "there are 0 eligible artists" do
        album        = create(:single, :approved, person: person, style_review_state: "APPROVED")
        petri_bundle = create(:petri_bundle, album: album)
        distribution = create(:distribution, converter_class: "MusicStores::Google::Converter", petri_bundle: petri_bundle)
        distribution.update(state: 'delivered')
        salepoint    = create(:salepoint, salepointable: album, store: Store.find_by(short_name: "Google"))
        distribution.salepoints << salepoint
        create(:song, album: album)

        expected = controller.instance_eval { show_yt_oac? }
        expect(expected).to eq false
      end
    end
  end
end
