require "rails_helper"

describe ArtworkTemplateController, type: :controller do

  before(:each) do
    @person = TCFactory.build_person #Person.new
    login_as(@person)
    @album = Album.new#mock_model(Album)

    @salepoint = mock_model(Salepoint)
    allow(@album).to receive_message_chain(:salepoints,:find).and_return(@salepoint)
    allow(@person).to receive_message_chain(:albums,:find).and_return(@album)
    allow(controller).to receive(:alert_rejections).and_return(nil)
    ENV['TWO_FACTOR_AUTHENTICATION_ENABLED'] = 'false'
  end

  # This is broken. `@album.id` is nil.
  xit 'responds to edit' do
    get 'edit', params: { album_id: @album.id, salepoint_id: @salepoint }

    expect(response).to be_successful
  end

  # This is broken. `@album.id` is nil.
  xit 'renders "edit" if there is an error on update' do
    expect(@salepoint).to receive(:save).once.and_return(false)
    put 'update', params: { album_id: @album.id, salepoint_id: @salepoint.id }
    expect(response).to be_successful
    expect(response).to render_template('edit')
  end

  it 'should redirect if the update was successful' do
    expect(@salepoint).to receive(:save).once.and_return(true)
    expect(@album).to receive(:id).at_least(:once).and_return(666)
    allow(@person).to receive(:has_never_distributed?).and_return(true)

    put 'update', params: { album_id: @album.id, salepoint_id: @salepoint.id }

    expect(response).to be_redirect
  end
end
