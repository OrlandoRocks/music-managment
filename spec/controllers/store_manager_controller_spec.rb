require "rails_helper"

describe StoreManagerController, type: :controller do

  describe "show" do

    before(:each) do
      @person = TCFactory.build(:person)
      @person.features << features(:store_manager)
      @person.features << features(:store_automator)
      @person.save!
      login_as(@person)

      TCFactory.build_ad_hoc_salepoint_product(1.99)
    end

    it "should only show finalized releases" do
      # Create 2 albums both with no salepoints
      album1 = TCFactory.create(:album, :person => @person, :finalized_at => Date.today)
      album2 = TCFactory.create(:album, :person => @person)

      SalepointableStore.create!(:store=>Store.find(36), :salepointable_type=>"Album")

      get :show
      assert_response :success, @response.body
      expect(assigns[:stores].blank?).to eq(false)
      expect(assigns[:stores].first[:missing_releases].size).to eq(1)
      expect(assigns[:stores].first[:missing_releases].first).to eq(album1.id)
      expect(assigns[:album_hash].size).to eq(1)
      expect(assigns[:album_hash].has_key?(album1.id)).to eq(true)
    end

    it "should create array of stores and missing releases" do
      album = TCFactory.create(:album, :person => @person, :finalized_at => Date.today)
      Store.is_active.each do |store|
        SalepointableStore.create!(:store=>store, :salepointable_type=>"Album")
        salepoint = album.salepoints.build(:store=>store, :has_rights_assignment=>true, payment_applied: true)
        salepoint.variable_price_id = store.default_variable_price.id if store.variable_pricing_required?
      end

      # save all new salepoints
      album.save!

      get :show
      assert_response :success, @response.body
      expect(assigns[:stores].blank?).to eq(true)

      # Remove Spotify from list
      album.salepoints.where("store_id = ?", Store.find(26)).first.delete

      get :show
      assert_response :success, @response.body
      expect(assigns[:stores].blank?).to eq(false)
      expect(assigns[:stores].size).to eq(1)
      expect(assigns[:stores].first[:name]).to eq("Spotify")
      expect(assigns[:stores].first[:missing_releases].size).to eq(1)
      expect(assigns[:stores].first[:missing_releases].first).to eq(album.id)
      expect(assigns[:album_hash].size).to eq(1)
      expect(assigns[:album_hash].has_key?(album.id)).to eq(true)
    end

    it "should not include ringtones in future stores" do
      album1 = FactoryBot.create(:album, :with_uploaded_songs, person: @person, finalized_at: Date.today, album_type: "Album")
      album2 = FactoryBot.create(:album, :with_uploaded_songs, person: @person, finalized_at: Date.today, album_type: "Single")
      album3 = FactoryBot.create(:album, :with_uploaded_songs, person: @person, finalized_at: Date.today, album_type: "Ringtone")

      get :show
      assert_response :success, @response.body
      expect(assigns[:salepoint_subscriptions][:missing_releases].size).to eq(2)
      expect(assigns[:salepoint_subscriptions][:missing_releases].include?(album1.id)).to eq(true)
      expect(assigns[:salepoint_subscriptions][:missing_releases].include?(album2.id)).to eq(true)
      expect(assigns[:salepoint_subscriptions][:missing_releases].include?(album3.id)).to eq(false)
    end

    it "should not include salepoint subscriptions info without store_automator feature" do
      album1 = FactoryBot.create(:album, :with_uploaded_songs, person: @person, finalized_at: Date.today, album_type: "Album")
      album2 = FactoryBot.create(:album, :with_uploaded_songs, person: @person, finalized_at: Date.today, album_type: "Single")
      album3 = FactoryBot.create(:album, :with_uploaded_songs, person: @person, finalized_at: Date.today, album_type: "Ringtone")

      feat = @person.features.find_by(name: "store_automator")
      FeaturePerson.where("feature_id = ? AND person_id = ?", feat.id, @person.id).first.destroy

      get :show
      assert_response :success, @response.body
      expect(assigns[:salepoint_subscriptions].blank?).to eq(true)
    end

    it "defines ivars" do
      create(:album, :person => @person, :finalized_at => Date.today)

      get :show

      expect(controller.instance_variable_get(:@disable_expander)).to be_in([true, false])
      expect(controller.instance_variable_get(:@uncheck_all)).to      be_in([true, false])
      expect(controller.instance_variable_get(:@uncheck_stores)).to   be_in([true, false])
    end

    let(:base_price) { 
      Product.where.not(price: 0)
        .find_by(
        display_name: 'automator',
        country_website_id: @person.country_website_id
        ).price 
    }
   
      it "adds salepoints to initial total when expander is enabled" do
      create(:album, :person => @person, :finalized_at => Date.today)

      get :show

      expect(controller.instance_variable_get(:@initial_total)).to be > base_price
    end

    it "omits salepoints from initial total when expander is disabled" do
      allow(@person).to receive(:can_use_store_expander?).and_return false
      album1 = create(:album, :person => @person, :finalized_at => Date.today)

      get :show
      expect(controller.instance_variable_get(:@initial_total)).to eq base_price
    end
  end # show

  describe "create" do

    before(:each) do
      @person = TCFactory.create(:person)
      @person.features << features(:store_manager)
      @person.features << features(:store_automator)
      login_as(@person)

      TCFactory.build_ad_hoc_salepoint_subscription_product(5.00)
      TCFactory.build_ad_hoc_salepoint_product(1.99)

      @album = TCFactory.create(:album, :person => @person, :finalized_at => Date.today)
      @store = Store.find(36) # iTunesWW
      SalepointableStore.create!(:store=>@store, :salepointable_type=>"Album")
    end

    it "should not create duplicate salepoints" do
      # Create salepoint for store and album combo
      sp_attr = {:store => @store, :has_rights_assignment => true}
      sp_attr[:variable_price_id] = @store.default_variable_price.id if @store.variable_pricing_required?
      sp = @album.salepoints.create!(sp_attr)

      # attempt to duplicate the salepoint. Should not be created
      expect {
        post :create, params: { data: {"stores" => {"#{@store.id}" => [@album.id]}}.to_json }
      }.not_to change(Salepoint,:count)
    end

    it "should create salepoints" do
      spotify = Store.find(26) # from fixtures
      SalepointableStore.create!(:store => spotify, :salepointable_type=>"Album")

      data = {"stores" => {"#{@store.id}" => [@album.id], "#{spotify.id}" => [@album.id]}}

      expect {
        post :create, params: { data: data.to_json }
      }.to change(Salepoint, :count).by(2)

      # Records are created using raw sql. Loop over ensuring they are valid
      salepoints = Salepoint.order("id desc").limit(2)
      salepoints.each do |sp|
        expect(sp.valid?).to eq(true)
        expect(sp.purchase.valid?).to eq(true)
      end
    end

    it "should create salepoint subscriptions" do
      album2 = TCFactory.create(:album, :person => @person, :finalized_at => Date.today)
      album3 = TCFactory.create(:album, :person => @person, :finalized_at => Date.today)

      expect {
        post :create, params: { data: {"future" => [@album.id, album2.id, album3.id]}.to_json }
      }.to change(SalepointSubscription, :count).by(3)

      # Records are created using raw sql. Loop over ensuring they are valid
      subs = SalepointSubscription.order("id desc").limit(3)
      subs.each do |sub|
        expect(sub.valid?).to eq(true)
        expect(sub.purchase.valid?).to eq(true)
      end
    end

    it "should not create duplicate salepoint subscriptions" do
      expect {
        post :create, params: { data: {"future" => [@album.id, @album.id]}.to_json }
      }.to change(SalepointSubscription,:count).by(1)
    end

    it "should not create salepoint subscriptions for ringtones" do
      album2 = FactoryBot.create(:album, :with_uploaded_songs, person: @person, finalized_at: Date.today, album_type: "Single")
      album3 = FactoryBot.create(:album, :with_uploaded_songs, person: @person, finalized_at: Date.today, album_type: "Ringtone")

      expect {
        post :create, params: { data: {"future" => [@album.id, album2.id, album3.id]}.to_json }
      }.to change(SalepointSubscription,:count).by(2)

      sp_subs = SalepointSubscription.select("album_id").where("album_id IN (?)", [@album.id, album2.id, album3.id])

      sp_subs = sp_subs.map {|sp| sp.album_id}
      expect(sp_subs.include?(@album.id)).to eq(true)
      expect(sp_subs.include?(album2.id)).to eq(true)
      expect(sp_subs.include?(album3.id)).to eq(false)
    end

    it "should not create salepoint subscriptions without store automator feature" do
      feat = @person.features.find_by(name: "store_automator")
      FeaturePerson.where("feature_id = ? AND person_id = ?", feat.id, @person.id).first.destroy

      expect {
        post :create, params: { data: {"future" => [@album.id]}.to_json }
      }.not_to change(SalepointSubscription, :count)
      assert_redirected_to cart_path(:sm => 1)
    end

    context "feature gating," do
      it "should not create salepoints when expander is disabled" do
        allow(@person).to receive(:can_use_store_expander?).and_return false

        spotify = Store.find(26) # from fixtures
        create(:salepointable_store, store: spotify, salepointable_type: "Album")

        data = {"stores" => {"#{@store.id}" => [@album.id], "#{spotify.id}" => [@album.id]}}

        expect(Tunecore::StoreManager).not_to receive(:create_store_purchases)
        post :create, params: { data: data.to_json }

        assert_redirected_to cart_path(sm: 1)
      end

      it "should create salepoints when expander is enabled" do
        allow(@person).to receive(:can_use_store_expander?).and_return true

        spotify = Store.find(26) # from fixtures
        create(:salepointable_store, store: spotify, salepointable_type: "Album")

        data = {"stores" => {"#{@store.id}" => [@album.id], "#{spotify.id}" => [@album.id]}}

        expect(Tunecore::StoreManager).to receive(:create_store_purchases)
        post :create, params: { data: data.to_json }

        assert_redirected_to cart_path(sm: 1)
      end

      it "should not create automator subs when automator is disabled" do
        allow(@person).to receive(:can_do?).with(:store_automator).and_return false

        data = { "future" => ["foo"] }

        expect(Tunecore::StoreManager).not_to receive(:create_automator_purchases)
        post :create, params: { data: data.to_json }

        assert_redirected_to cart_path(sm: 1)
      end

      it "should create automator subs when automator is enabled" do
        allow(@person).to receive(:can_do?).with(:store_automator).and_return true

        data = { "future" => ["foo"] }

        expect(Tunecore::StoreManager).to receive(:create_automator_purchases)
        post :create, params: { data: data.to_json }

        assert_redirected_to cart_path(sm: 1)
      end

      it "redirects to dash when manager disabled" do
        allow_any_instance_of(described_class).to receive(:disable_manager?).and_return true

        data = { "future" => ["foo"] }

        post :create, params: { data: data.to_json }

        assert_redirected_to dashboard_path
      end

      it "redirects to dash when params empty" do
        post :create, params: {}

        assert_redirected_to dashboard_path
      end
    end
  end # create
end
