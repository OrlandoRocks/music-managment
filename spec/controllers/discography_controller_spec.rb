require "rails_helper"

describe DiscographyController, " when there is not a user logged in" do
  it "should redirect the user to the log-in screen" do
    get :index
    expect(response).to redirect_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
  end
end

describe DiscographyController, " when there is a user logged in" do
  before(:each) do
    @person = TCFactory.build_person
    login_as(@person)
  end

  it "should display the discography page if the user has albums" do
    allow_message_expectations_on_nil
    allow(@current_user).to receive_message_chain(:albums, :not_deleted, :reorder, :page, :per_page).and_return(
              [mock_model(Album, :finalized? => true, :has_unpaid_renewal? => false, :can_stream? => true),
               mock_model(Album, :finalized? => false, :has_unpaid_renewal? => false, :can_stream? => false)])
    get :index
    expect(response).to be_successful
  end

  it "should display redirect to the no_albums_yet page when there are no albums" do
    allow_message_expectations_on_nil
    allow(@current_user).to receive_message_chain(:albums, :not_deleted, :reorder, :page, :per_page).and_return([])
    get :index
    expect(response).to be_successful
  end

  it "uses view_type cookie when available" do
    cookies[:view_type] = 'grid'
    get :index
    expected = controller.instance_eval { set_view_type }

    expect(expected).to eq 'grid'
  end

  it "sets view_type cookie" do
    get :index, params: { view_type: 'table' }

    expect(cookies[:view_type]).to eq 'table'
  end
end
