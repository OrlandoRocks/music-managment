require "rails_helper"

describe SessionsController do
  let(:password) { "Password123!" }
  let(:person) do
    create(
      :person,
      password: password,
      password_confirmation: password,
      is_verified: true,
      accepted_terms_and_conditions: true
    )
  end

  describe "#new" do
    context "when given cookies prefixed with wp_tunecore_" do
      it "builds referral_data" do
        cookie_data_1 = {
          ref: "TestData",
          jt: 123,
          utm_source: "pubadmin",
          utm_medium: "web",
          utm_term: "music",
          utm_content: "testing",
          utm_campaign: "footer-links",
          tc_timestamp: 1458752702,
        }
        cookie_data_2 = {
          ref: "TestData",
          jt: 123,
          utm_source: "pubadmin",
          utm_medium: "web",
          utm_term: "music",
          utm_content: "testing",
          utm_campaign: "footer-links",
          tc_timestamp: 1458752702,
        }
        cookies[:tunecore_wp] = cookie_data_1.to_json
        cookies[:tunecore_wp_1] = cookie_data_2.to_json

        get :new

        expect(assigns[:person].referral_data.size).to eq 14
      end
    end

    context "when given cookies NOT prefixed with wp_tunecore_" do
      it "doesnt build referral_data" do
        cookies["other_cookie"] = { ref: "TestData" }.to_json

        get :new

        expect(assigns[:person].referral_data.size).to eq 0
      end
    end

    context "when passed referral_data from url parameters" do
      it "builds whitelisted" do
        get :new, params: { utm_source: "TestData", ref: "TestData2" }

        expect(assigns[:person].referral_data.size).to eq 2
      end

      it "doesnt build non-whitelisted" do
        get :new, params: { other_param: "TestData" }

        expect(assigns[:person].referral_data.size).to eq 0
      end
    end

    context "when passed redirect_url in url parameters" do
      it "stores the url in session" do
        url = Faker::Internet.url
        get :new, params: { redirect_url: url }

        expect(session[:redirect_url]).to eq(url)
        session.delete(:redirect_url)
      end
    end
  end

  describe "#create" do
    it "calls the sign in callback" do
      allow(V2::Authentication::SharedSignInSuccessCallback)
        .to receive(:call)
        .and_call_original

      post :create, params: {
        person: {
          email: person.email,
          password: password
        }
      }

      expect(V2::Authentication::SharedSignInSuccessCallback).to have_received(:call)
    end

    context "when logging in for the first time" do
      before do
        cookies[:tunecore_id] = "123"

        post :create, params: {
          person: {
            email: person.email,
            password: password
          },
          "g-recaptcha-response" => "yup"
        }
      end

      it { is_expected.to redirect_to dashboard_create_account_path }
    end

    context "with preset redirect url in session" do
      it "should redirect to the redirect url and delete it from session" do
        url = Faker::Internet.url
        session[:redirect_url] = url

        post :create, params: {
          person: {
            email: person.email,
            password: password
          },
          "g-recaptcha-response" => "yup"
        }

        expect(subject).to redirect_to(url)
        expect(session[:redirect_url]).to be_nil
      end
    end

    context "with referral data attributes" do
      it 'handle strong parameters properly' do
        person_params = {
          email: person.email,
          password: password,
          referral_data_attributes: {
            "0"=> {
              key: "jt",
              value: "offer",
              page: "login",
              source: "cookie",
              timestamp: "2020-04-08 10:33:22 +0530"
            }
          }
        }
        expect { post :create, params: { person: person_params } }.to change { person.reload.referral_data.count }.by(1)
      end
    end

    describe "SigninCallback Concern" do
      describe "#retrieve_publishing_data_if_account_not_blocked" do
        context "when the user has rights_app feature and publishing administration" do
          it "enqueues the RA ingester blocker job" do
            allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
            account = create(:account, person: person)
            create(:composer, :with_pub_admin_purchase, account: account, person: person)

            allow(PublishingAdministration::PublishingIngestionBlockerWorker)
              .to receive(:perform_async)

            post :create, params: {
              person: {
                email: person.email,
                password: password
              }
            }

            expect(PublishingAdministration::PublishingIngestionBlockerWorker)
              .to have_received(:perform_async)
              .with(account.id)
          end
        end
      end

      context "when the user doesn't have a plan, cookie, and feature flag enabled" do
        it "redirects" do
          allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
          allow(FeatureFlipper).to receive(:show_feature?)
            .with(:payoneer_login_redirect, instance_of(Person))
            .and_return(false)
          account = create(:account, person: person)
          person.update(recent_login: Time.now)

          post :create, params: { person: { email: person.email, password: password } }
          assert_redirected_to plans_path
        end
      end

      describe "#sign_in_failure" do
        context "when invoked" do
          it "creates a login attempt" do
            account = create(:account, person: person)
            expect {post :create, params: { person: { email: person.email, password: "wrongpassword" } }}.to change {LoginAttempt.count}.by(1)
          end
        end
      end
    end

    describe "reroutes logged in user" do
      it "redirects a payoneer customer to plans if first login" do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(false)
        allow(FeatureFlipper).to receive(:show_feature?)
                                 .with(:payoneer_login_redirect, instance_of(Person))
                                 .and_return(true)

        person.tax_tokens.create(token: SecureRandom.hex, is_visible: true)

        post :create, params: { person: { email: person.email, password: password }, "g-recaptcha-response" => "yup" }

        assert_redirected_to controller: "dashboard", action: "create_account"
      end

      it "redirects a payoneer customer to the tax info tab" do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(false)
        allow(FeatureFlipper).to receive(:show_feature?)
                                 .with(:payoneer_login_redirect, instance_of(Person))
                                 .and_return(true)

        allow_any_instance_of(Person).to receive(:first_login).and_return(false)

        person.tax_tokens.create(token: SecureRandom.hex, is_visible: true)

        post :create, params: { person: { email: person.email, password: password }, "g-recaptcha-response" => "yup" }

        assert_redirected_to edit_person_path(person, tab: "tax_info")
      end

      it "should redirect to survey on first login where album is already purchased" do
        cookies["profile_survey"] = "true"
        allow_any_instance_of(PersonProfileSurveyInfo).to receive(:show_interstitial?).and_return(true)

        post :create, params: { person: { email: person.email, password: password }, "g-recaptcha-response" => "yup" }

        assert_redirected_to account_profile_survey_announcement_path(first_login: true)
      end

      it "should redirect to create account on first login where album is not purchased" do
        cookies["profile_survey"] = "false"
        allow_any_instance_of(PersonProfileSurveyInfo).to receive(:show_interstitial?).and_return(false)

        post :create, params: { person: { email: person.email, password: password }, "g-recaptcha-response" => "yup" }

        assert_redirected_to :controller => "dashboard", :action => "create_account"
      end

      it "should redirect to survey on any login after first where album is already purchased" do
        person.update(recent_login: Time.now)
        cookies["profile_survey"] = "true"
        allow_any_instance_of(PersonProfileSurveyInfo).to receive(:show_interstitial?).and_return(true)

        post :create, params: { person: { email: person.email, password: password }, "g-recaptcha-response" => "yup" }

        assert_redirected_to account_profile_survey_announcement_path
      end

      COUNTRY_URLS.each do |country_code, country_url|
        it "should redirect to dashboard on any login where album is not purchased" do
          person.country_website = CountryWebsite.find_by(country: country_code)
          request.host = country_url
          person.update(recent_login: Time.now)
          cookies["profile_survey"] = "false"
          allow_any_instance_of(PersonProfileSurveyInfo).to receive(:show_interstitial?).and_return(false)

          post :create, params: { person: { email: person.email, password: password }, "g-recaptcha-response" => "yup" }

          expect(subject).to redirect_to(dashboard_path({pb:"n"}))
        end
      end

      context "when a users password_reset_tmsp is nil and the force_password_reset feature is on" do
        it "should redirect to the new_reset_password page" do
          allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
          person.update(password_reset_tmsp: nil)

          post :create, params: { person: { email: person.email, password: password } }

          assert_redirected_to new_reset_password_path(force: true)
        end
      end
    end

    describe "recaptcha handling" do
      before(:each) do
        allow_any_instance_of(RecaptchaHelper).to receive(:use_recaptcha?).and_return(true)
      end

      context "with the recaptcha param" do
        it "verifies the user's recaptcha with Google before logging in" do
          allow(RecaptchaService).to receive(:verify).and_return(true)
          expect(RecaptchaService).to receive(:verify).exactly(1).times

          post :create, params: {
            person: {
              email: person.email,
              password: password
            },
            "g-recaptcha-response" => "yup"
          }
        end

        context "valid recaptcha response" do
          before(:each) do
            allow(RecaptchaService).to receive(:verify).and_return(true)
          end

          it "reroutes the logged in user" do
            expect(subject).to receive(:reroute_logged_in_user).and_call_original

            post :create, params: {
              person: {
                email: person.email,
                password: password
              },
              "g-recaptcha-response" => "yup"
            }
          end
        end

        context "invalid recaptcha response" do
          before(:each) do
            allow(RecaptchaService).to receive(:verify).and_return(false)
            allow(SessionsMailer).to receive(:suspicious_login_mailer).and_return(double("mailer", deliver_now: true))
          end

          it "send a suspicious login mailer" do
            expect(SessionsMailer).to receive(:suspicious_login_mailer)

            post :create, params: {
              person: { email: person.email, password: password }, "g-recaptcha-response" => "yup"
            }
          end

          it "marks the sus login mail as sent for the user" do
            expect_any_instance_of(Person).to receive(:suspicious_email_sent!)

            post :create, params: {
              person: { email: person.email, password: password }, "g-recaptcha-response" => "yup"
            }
          end
        end
      end
    end

    describe "#handle_successful auth" do
      context "dormant user" do
        it "sets dormant to false" do
          person.update(dormant: true)
          post :create, params: { person: { email: person.email, password: password } }
          person.reload
          expect(person.dormant).to be(false)
        end
      end

      describe "#reset_plans_redirect" do
        context "legacy user" do
          context "missing redirect cookie" do
            it "sets redirect cookie" do
              post :create, params: { person: { email: person.email, password: password } }

              expect(cookies[:plans_redirect]).to be true
            end
          end

          context "plans user" do
            before { create(:person_plan, person: person) }

            it "does NOT set redirect cookie" do
              post :create, params: { person: { email: person.email, password: password } }

              expect(cookies[:plans_redirect]).to be_nil
            end
          end
        end
      end
    end
  end

  describe "#destroy" do
    before :each do
      login_as(person)
    end

    it "should redirect to supplied redirect url" do
      post :destroy, params: { redirect: logout_url }

      assert_redirected_to logout_url
    end

    it "should redirect to homepage without redirect url" do
      post :destroy

      assert_redirected_to login_url(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
    end

    it "should logout" do
      expect(session[:person]).not_to be_blank

      post :destroy

      expect(session[:person]).to be_blank
    end

    it "destroys the active session cookie" do
      post :destroy

      expect(cookies[:tc_active_session]).to be_nil
    end
  end

  context "the request has an invalid CSRF token" do
    before(:each) do
      request.env["HTTP_REFERER"] = login_url(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
    end

    it 'should redirect the user back to the login page' do
      ActionController::Base.allow_forgery_protection = true

      post :create, params: {
        person: {
          email: person.email,
          password: password
        },
        "g-recaptcha-response" => "yup",
        "authenticity_token"   => "nope"
      }

      assert_redirected_to login_url(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
    end

    context "and request type is json" do
      it "should not redirect back to login page"  do
        ActionController::Base.allow_forgery_protection = true

        post :create, params: {
          person: {
            email: person.email,
            password: password
          },
          "g-recaptcha-response" => "yup",
          "authenticity_token"   => "nope",
          format: :json
        }

        expect(subject).to_not redirect_to(login_url(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1))
      end
    end
  end
end
