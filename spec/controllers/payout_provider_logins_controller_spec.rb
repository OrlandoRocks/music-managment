require "rails_helper"

describe PayoutProviderLoginsController do
  let(:person) { create(:person) }

  before(:example) do
    login_as(person)
  end

  describe "#create" do
    it "returns 302 if feature not enabled" do
      allow(FeatureFlipper)
        .to receive(:show_feature?)
        .and_return(:default)
      allow(FeatureFlipper)
        .to receive(:show_feature?)
        .with(:payoneer_opt_out, person)
        .and_return(true)

      post :create, params: { payout_provider_name: "Payoneer" }

      expect(response.status).to eq 302
    end

    it "redirects to login form link if successful" do
      allow(FeatureFlipper)
        .to receive(:show_feature?)
        .and_return(:default)
      allow(FeatureFlipper)
        .to receive(:show_feature?)
        .with(:payoneer_opt_out, person)
        .and_return(false)

      form = double(save: true, link: dashboard_path)

      allow(PayoutProvider::LoginForm)
        .to receive(:new)
        .with(person: person, provider_name: "payoneer")
        .and_return(form)

      post :create, params: { payout_provider_name: "Payoneer" }

      expect(response).to redirect_to(dashboard_path)
    end

    it "redirects back if not successful" do
      allow(FeatureFlipper)
        .to receive(:show_feature?)
        .and_return(:default)
      allow(FeatureFlipper)
        .to receive(:show_feature?)
        .with(:payoneer_opt_out, person)
        .and_return(false)

      form = double(save: false)

      request.env["HTTP_REFERER"] = dashboard_path

      allow(PayoutProvider::LoginForm).to receive(:new).with(person: person, provider_name: "payoneer").and_return(form)

      post :create, params: { payout_provider_name: "Payoneer" }

      expect(response).to redirect_to(dashboard_path)
    end
  end
end
