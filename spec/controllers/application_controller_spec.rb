require "rails_helper"

describe ApplicationController, type: :controller do
  describe "#is_mobile_agent?" do
    it "a non-mobile browser user agents return false" do
      controller.request.env['HTTP_USER_AGENT'] = 'Mozilla/5.0'
      expect(controller.send(:is_mobile_agent?)).to eq(false)
    end
    it "a mobile browser user agents return true" do
      controller.request.env['HTTP_USER_AGENT'] = 'blackberry'
      expect(controller.send(:is_mobile_agent?)).to eq(true)
    end
  end

  describe 'redirect_stem_users' do
    let(:person) { create(:person) }

    controller(ApplicationController) do
      before_action :redirect_stem_users

      def index
        render nothing: true
      end
    end

    context "user is imported from stem and no payment" do
      it "should redirect to dashboard" do
        allow_any_instance_of(Person)
          .to receive(:blocked_for_stem?)
          .and_return(true)

        login_as(person)
        get :index

        expect(response).to redirect_to '/dashboard'
      end
    end
  end

  describe 'gon setup' do
    let(:person) { create(:person) }
    let(:gon) { RequestStore.store[:gon].gon }

    controller(ApplicationController) do
      before_action :set_gon_current_user

      def index
        head :ok
      end
    end

    it "should scrub sensitive data" do
      login_as(person)

      get :index

      expect(gon.as_json.dig('current_user', 'person', 'name')).to eq person.name
      expect(gon.as_json.dig('current_user', 'person', 'id')).to eq person.id

      expect(gon.as_json.dig('current_user', 'person', 'password')).to be_nil
      expect(gon.as_json.dig('current_user', 'person', 'salt')).to be_nil
      expect(gon.as_json.dig('current_user', 'person', 'password_reset_tmsp')).to be_nil
    end
  end


  describe '#handle_in_context_editor' do
    let(:person) { create(:person) }

    controller(ApplicationController) do
      around_action :handle_in_context_editor

      def index
        render nothing: true
      end
    end

    context "when the Rails env is not staging" do
      it "should disable the phrase app context editor" do
        allow(Rails).to receive(:env) { "production".inquiry  }
        login_as(person)

        expect(PhraseApp::InContextEditor)
          .to receive(:with_config)
          .with(enabled: false)

        get :index
      end
    end

    context "when the Rails env is staging" do
      context "when the phrase app feature flag is disabled" do
        it "should disable the phrase app context editor" do
          allow(Rails).to receive(:env) { "staging".inquiry  }
          allow(FeatureFlipper).to receive(:show_feature?) { false }

          login_as(person)

          expect(PhraseApp::InContextEditor)
            .to receive(:with_config)
            .with(enabled: false)

          get :index
        end
      end

      context "when the phrase app feature flag is enabled" do
        it "should enable the phrase app context editor" do
          allow(Rails).to receive(:env) { "staging".inquiry  }
          allow(FeatureFlipper).to receive(:show_feature?) { true }

          login_as(person)

          expect(PhraseApp::InContextEditor)
            .to receive(:with_config)
            .with(enabled: true)

          get :index
        end
      end
    end
  end

  describe "#handle_v2_login" do
    controller(described_class) do
      def index
        head :ok
      end

      def login
        head :ok
      end
    end


    context "relevant path" do
      it "calls the service" do
        service_double = double("V2::Authentication::SharedLoginService",
          redirect_to_v2?: true,
          v2_url: "/",
          person_id: nil
        )

        allow(V2::Authentication::SharedLoginService).to receive(:new).and_return(service_double)

        get :index

        expect(V2::Authentication::SharedLoginService).to have_received(:new)
      end
    end

    context "irrelevant path" do
      it "does NOT call the service" do
        routes.draw { get "login" => "anonymous#login" }

        expect(V2::Authentication::SharedLoginService).not_to receive(:new)

        get :login
      end
    end
  end

  describe "should_show_tfa_prompt?" do
    let(:person) { create(:person) }

    controller(described_class) do
      before_action :set_gon_two_factor_auth_prompt

      def index
        head :ok
      end
    end

    it "creates a two_factor_auth_prompt record" do
      login_as(person)
      expect { get :index }.to change(TwoFactorAuthPrompt, :count).by 1
    end
  end
end
