require "rails_helper"

describe SalepointSubscriptionsController, type: :controller do

  before(:each) do
    @person = TCFactory.create(:person)
    @person.features << features(:store_automator)
    @person.save!

    login_as @person

    @album = TCFactory.create(:album, :person => @person)
  end

  describe "#create" do

    it "should create a new salepoint subscription" do
      expect {
        post :create, params: { album_id: @album.id }
      }.to change(SalepointSubscription, :count).by(1)

      assert_redirected_to album_path(@album)
      expect(@album.salepoint_subscription.blank?).to eq(false)
      expect(@album.salepoint_subscription.is_active).to eq(true)
      expect(@album.salepoint_subscription.effective.blank?).to eq(true)
    end

    it "should not create duplicate salepoint subscriptions for an album" do
      sp_sub = TCFactory.create(:salepoint_subscription, :album => @album, :is_active => true)

      expect {
        post :create, params: { album_id: @album.id }
      }.to_not change(SalepointSubscription, :count)

      assert_redirected_to album_path(@album)
    end

  end

  describe "#destroy" do

    before(:each) do
      @sp_sub = TCFactory.create(:salepoint_subscription, :album => @album)
    end

    it "should remove an unfinalized salepoint subscription" do
      expect {
        delete :destroy, params: { :album_id => @album.id }
      }.to change(SalepointSubscription, :count).by(-1)

      assert_redirected_to album_path(@album)
    end

    it "should not remove a finalized salepoint subscription" do
      @sp_sub.finalize!

      expect {
        delete :destroy, params: { :album_id => @album.id }
      }.to_not change(SalepointSubscription, :count)

      @album.reload
      assert_redirected_to album_path(@album)
      expect(@album.salepoint_subscription.blank?).to eq(false)
    end

  end

  describe "#toggle_active" do
    let!(:salepoint_subscription) { create(:salepoint_subscription, album: @album, finalized_at: DateTime.current, effective: DateTime.current) }

    it "should toggle is_active for valid Plan users who have finalized salepoint_subscriptions" do
      person_plan = create(:person_plan, person: @person, plan: Plan.last)
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

      get :toggle_active, params: { album_id: @album.id, id: salepoint_subscription.id }
      salepoint_subscription.reload
      expect(salepoint_subscription.is_active).to eq(false)

      get :toggle_active, params: { album_id: @album.id, id: salepoint_subscription.id }
      salepoint_subscription.reload
      expect(salepoint_subscription.is_active).to eq(true)
    end

    it "should not toggle is_active for invalid Plan users" do
      person_plan = create(:person_plan, person: @person, plan: Plan.first)
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

      get :toggle_active, params: { album_id: @album.id, id: salepoint_subscription.id }
      salepoint_subscription.reload
      expect(salepoint_subscription.is_active).to eq(true)
    end

    it "should not toggle is_active for non-Plan users" do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

      get :toggle_active, params: { album_id: @album.id, id: salepoint_subscription.id }
      salepoint_subscription.reload
      expect(salepoint_subscription.is_active).to eq(true)
    end
  end

  describe "#require_store_automator_feature" do

    it "should create subscription if feature enabled" do
      expect(@person.feature_enabled?(:store_automator)).to eq(true)
      expect {
        post :create, params: { album_id: @album.id }
      }.to change(SalepointSubscription, :count).by(1)

      assert_redirected_to album_path(@album)
    end

    it "should not create subscription if feature disabled" do
      feat = @person.features.find_by(name: "store_automator")
      FeaturePerson.where("feature_id = ? AND person_id = ?", feat.id, @person.id).first.destroy

      expect(@person.feature_enabled?(:store_automator)).to eq(false)

      expect {
        post :create, params: { album_id: @album.id }
      }.to_not change(SalepointSubscription, :count)

      assert_redirected_to album_path(@album)
    end
  end

end
