require "rails_helper"

describe FacebookTrackMonetization::SubscriptionsController do
  context "FBTracks unavailable" do
    it "redirects to login if user is logged out" do
      get(:new)
      expect(response).to redirect_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
    end
  end

  context "FBTracks available" do
    before :each do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
    end

    it "renders the info page if the user has no live releases" do
      login_as(create(:person))

      get(:new)

      expect(response).to render_template("new")
    end

    context "new subscribers" do
      before :each do
        @person       = create(:person, :with_live_album)
        @sub_purchase = (double(finalize: true))
        login_as(@person)
      end

      it "creates a subscription purchase request" do
        allow(::Subscription::Tunecore::PurchaseRequest).to receive(:new).and_return(@sub_purchase)
        expect(Subscription::Tunecore::PurchaseRequest).to receive(:new)

        post(:create)
      end

      it "attempts to finalize the subscription purchase" do
        allow(::Subscription::Tunecore::PurchaseRequest).to receive(:new).and_return(@sub_purchase)
        expect(@sub_purchase).to receive(:finalize)

        post(:create)
      end

      it "redirects successful subscribers to the FBTracks dashboard" do
        allow(::Subscription::Tunecore::PurchaseRequest).to receive(:new).and_return(@sub_purchase)

        post(:create)

        expect(response).to redirect_to facebook_track_monetization_dashboard_path
      end

      it "re-renders the page with an error message if the subscription request cannot finalize" do
        allow(::Subscription::Tunecore::PurchaseRequest).to receive(:new).and_return(double(finalize: false))
        post(:create)
        expect(response).to render_template(:new)
      end
    end
  end

  describe "#create" do
    before do
      @person = create(:person, :with_live_album)
      login_as @person
    end

    it "enqueues a email for signing up for facebook track monetization without sending all tracks" do
      expect(MailerWorker).to receive(:perform_async).with(
          "FacebookTrackMonetizationMailer",
          :mail_for_sign_up_and_select,
          @person.id
      )

      post :create
    end

    it "enqueues an email for signing up for facebook track monetization while sending all tracks" do
      expect(MailerWorker).to receive(:perform_async).with(
          "FacebookTrackMonetizationMailer",
          :mail_for_sign_up_and_select_all,
          @person.id
      )

      post :create, params: { send_all: true }
    end
  end
end
