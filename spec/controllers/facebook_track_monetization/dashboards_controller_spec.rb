require "rails_helper"

describe FacebookTrackMonetization::DashboardsController do

  describe "#show" do
    let(:person) { create(:person) }

    before(:each) do
      allow(FeatureFlipper).to receive(:show_feature?) { true }
    end

    context "when the user does not subscribe to FBTracks" do
      it "redirects to the dashboard if the user does not subscribe to FBTracks" do
        login_as(person)
        get :show
        expect(request).to redirect_to dashboard_path
      end
    end

    context "when the user subscribes to FBTracks" do
      let!(:track) { create(:track_monetization, :eligible, person: person, created_at: "2021-06-14") }
      let(:song)   { track.song }
      let(:album)  { song.album }
      let(:expected_track_data) do
        [{ id:         track.id,
          song:       song.name,
          isrc:       song.tunecore_isrc,
          artist:     song.artist.name,
          discovery:  true,
          appears_on: album.name,
          upc:        album.upc.number,
          status:     "new",
          tc_distributor_state: nil }.with_indifferent_access]
      end

      before(:each) do
        create(:person_subscription_status, :with_subscription_event_and_purchase,
              subscription_type: "FBTracks", person: person)
        login_as(person.reload)
      end

      it "returns track data" do
        get :show, params: { format: :json }
        expect(JSON.parse(response.body)).to eq expected_track_data
      end
    end
  end

end
