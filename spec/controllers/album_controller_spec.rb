require "rails_helper"

describe AlbumController, type: :controller do
  describe "GET #show" do
    before(:each) do
      @person = create(:person)
      login_as(@person)
      @album = create(:album, person: @person)
      @salepoint = create(:salepoint, :itunes, salepointable: @album)
      @preorder = create(:salepoint_preorder_data, salepoint: @salepoint)
      allow(controller).to receive(:translate_release_date_time)
    end

    context "User has album with unpurchased salepoint_preorder_data" do
      it "should call #check_for_expired_preorder in the album_controller show action" do
        @preorder.update_attribute(:paid_at, nil)
        @album.update_attribute(:payment_applied, false)

        expect(controller).to receive(:check_for_expired_preorder)

        get :show, params: { id: @album.id }
      end
    end

    context "User has album with purchased album" do
      it "should not call #check_for_expired_preorder in the album_controller show action" do
        @preorder.update_attribute(:paid_at, Time.now)
        @album.update_attribute(:payment_applied, true)

        expect(controller).not_to receive(:check_for_expired_preorder)

        get :show, params: { id: @album.id }
      end
    end

    it "backfills asset metadata when unfinalized" do
      expect(controller).to receive(:backfill_album_metadata)

      get :show, params: { id: @album.id }
    end

    it "does not backfill asset metadata when finalized" do
      @album.update_attribute(:finalized_at, Date.today.to_s)

      expect(controller).not_to receive(:backfill_album_metadata)

      get :show, params: { id: @album.id }
    end
  end

  describe "POST #create" do
    it "should save an album's countries" do
      person = create(:person)
      login_as(person)

      params = {
        album: {
          name: "First Album",
          creatives: [{"id": "", "role": "primary_artist", "name": "martin fowler"}],
          primary_genre_id: "1",
          sale_date: "April 24, 2019",
          iso_codes: "US,CA"
        }
      }
      expect {
        post :create, params: params
      }.to change{Album.count}.by(1)
      new_album = Album.last
      expect(new_album.countries).to match_array([Country.find_by(iso_code: "US"), Country.find_by(iso_code: "CA")])
    end

    it "should save an album's metadata_language_code_id" do
      person = create(:person)
      login_as(person)

      params = {
        album: {
          name: "First Album",
          creatives: [{"id": "", "role": "primary_artist", "name": "martin fowler"}],
          primary_genre_id: "1",
          sale_date: "April 24, 2019",
          iso_codes: "US,CA",
          language_code_legacy_support: "en"
        }
      }
      expect {
        post :create, params: params
      }.to change{Album.count}.by(1)
      language_code = LanguageCode.find_by(code: "en")
      new_album = Album.last
      expect(new_album.metadata_language_code_id).to eq(language_code.id)
    end
  end

  describe "PUTS #update" do
    before(:each) do
      request.env['CONTENT_TYPE'] = 'application/json'
    end

    it "calls #check_for_expired_preorder if an album was not paid for" do
      album = create(:album)
      login_as(album.person)
      params = {
        id: album.id,
        album: {
          name: "New Album Name",
          creatives: [{"id": "", "role": "primary_artist", "name": "Creative"}],
          language_code_legacy_support: "en",
          primary_genre_id: "1",
          sale_date: "April 24, 2019",
          orig_release_year: ""
        }
      }

      expect(controller).to receive(:check_for_expired_preorder)

      put :update, params: params
    end

    it "updates an album's distribution countries" do
      album = create(:album)
      login_as(album.person)
      params = {
        id: album.id,
        album: {
          iso_codes: "AL",
        }
      }

      expect(controller).to receive(:check_for_expired_preorder)

      put :update, params: params

      expect(album.countries.count).to eq(1)
      expect(album.countries.first&.name).to eq('Albania')
    end
  end

  describe "#map_artist_name_to_artist_url" do
    before(:each) do
      request.env['CONTENT_TYPE'] = 'application/json'
    end

    it "returns an empty array when artist_urls is empty" do
      album = create(:album)
      params = {
        id: album.id,
        album: {
          name: album.name,
          creatives: [],
          language_code_legacy_support: "en",
        },
        artist_urls: [],
      }

      put :update, params: params

      expect(controller.map_artist_name_to_artist_url).to be_empty
    end

    it "maps the artist name to the artist urls" do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

      album = create(:album)
      creatives = [
        { role: "primary_artist", name: "Dj Ango" },
        { role: "primary_artist", name: "Ron Nope" },
        { role: "primary_artist", name: "Billie Baelish" },
      ]
      artist_urls = [
        { apple:   "https://itunes.apple.com/us/artist/ryan-moe/id12345" },
        { apple:   "" },
        { spotify: "https://open.spotify.com/artist/6qqNVTkY8uBg9cP3Jd7DAH?si=ccgCimHBR_WZhYdktaefGg" }
      ]
      params = {
        id: album.id,
        album: {
          name: album.name,
          creatives: creatives,
          language_code_legacy_support: "en"
        },
        artist_urls: artist_urls,
      }

      put :update, params: params

      expected_results = [{ "apple"=>"https://itunes.apple.com/us/artist/ryan-moe/id12345",
                            "artist_name"=>"Dj Ango",
                          }, {
                            "spotify" => "https://open.spotify.com/artist/6qqNVTkY8uBg9cP3Jd7DAH?si=ccgCimHBR_WZhYdktaefGg",
                            "artist_name" => "Billie Baelish"
                          }]
      expect(controller.map_artist_name_to_artist_url).to eq expected_results
    end
  end

  describe "#release_creatives" do
    before(:each) do
      request.env['CONTENT_TYPE'] = 'application/json'
    end

    context "when the request comes from the Album Controller" do
      it "returns creatives from the album params key" do
        album = create(:album)
        creatives = [{ "id": "", "name": "Album Creative", "role": "primary_artist" }.with_indifferent_access]
        params = {
          id: album.id,
          album: {
            name: album.name,
            creatives: creatives,
            language_code_legacy_support: "en"
          },
          url: [],
        }

        put :update, params: params

        expect(controller.release_creatives).to eq creatives
      end
    end
  end
end
