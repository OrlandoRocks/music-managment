require "rails_helper"

describe TrendReportsController, type: :controller do
  before(:each) do
    Timecop.freeze
  end

  after(:each) do
    Timecop.return
  end

  before(:each) do
    @album = Album
      .select("albums.*, count(songs.id) as count")
      .joins("inner join songs on songs.album_id = albums.id")
      .group("albums.id HAVING count >= 2")
      .first

    album2 = Album
      .select("albums.*, count(songs.id) as count")
      .joins("inner join songs on songs.album_id = albums.id")
      .where("albums.id != #{@album.id}")
      .group("albums.id HAVING count >= 2")
      .first

    song1 = @album.songs.first
    song2 = album2.songs.first

    ringtone = @album.person.ringtones[0] || create(:ringtone, person: @album.person)

    login_as(@album.person)

    zip = Zipcode.where(state: "NY").first

    create(
      :trend_data_detail,
      :with_summary_and_geo,
      zipcode: zip,
      zip_code: zip.zipcode,
      country_code: "US",
      sale_date: 1.week.ago,
      album: @album,
      artist: @album.artists.first
    )

    create(
      :trend_data_detail,
      :with_summary_and_geo,
      zipcode: zip,
      zip_code: zip.zipcode,
      sale_date: Date.today - 2.weeks + 1.day,
      album: @album
    )

    create(
      :trend_data_detail,
      :with_summary_and_geo,
      zipcode: zip,
      zip_code: zip.zipcode,
      sale_date: Date.today - 2.weeks + 1.day,
      album: @album,
      song: song1,
      trans_type_id: TransType.song_download.id
    )

    create(
      :trend_data_detail,
      :with_summary_and_geo,
      zipcode: zip,
      zip_code: zip.zipcode,
      sale_date: Date.today - 2.weeks + 1.day,
      album: album2,
      song: song2,
      trans_type_id: TransType.song_download.id
    )

    create(
      :trend_data_detail,
      :with_summary_and_geo,
      zipcode: zip,
      zip_code: zip.zipcode,
      sale_date: Date.today - 2.weeks + 1.day,
      album: album2,
      song: song2,
      trans_type_id: TransType.stream.id
    )

    create(
      :trend_data_detail,
      :with_summary_and_geo,
      zipcode: zip,
      zip_code: zip.zipcode,
      sale_date: Date.today - 2.weeks + 1.day,
      album: ringtone,
      song: ringtone.song,
      trans_type_id: TransType.ringtone.id,
      qty: 2
    )
  end

  describe "index" do
    it "should set all download and stream data" do
      get :index
      assert_response :success, @response.body
      expect(assigns[:download_provider_ids].blank?).to eq(false)
      expect(assigns[:provider_ids].size).to eq(3)
      expect(assigns[:top_download_releases].length).to eq(2)
      expect(assigns[:top_stream_releases].length).to eq(1)
      expect(assigns[:top_download_songs].length).to eq(2)
      expect(assigns[:top_stream_songs].length).to eq(1)
      expect(assigns[:top_ringtones].length).to eq(1)
      expect(assigns[:album_downloads].length).to eq(2)
      expect(assigns[:song_downloads].length).to eq(1)
      expect(assigns[:ringtone_downloads].length).to eq(1)
      expect(assigns[:top_download_countries].length).to eq(1)
      expect(assigns[:top_stream_countries].length).to eq(1)
      expect(assigns[:top_us_markets].length).to eq(1)

      get :index, params: { format: "csv" }
      assert_response :success, @response.body
    end
  end

  describe "release_view" do
    it "should set all download and stream data for csv" do
      get :release_view, params: { album_id: @album.id }
      assert_response :success, @response.body
      expect(assigns[:download_provider_ids].blank?).to eq(false)
      expect(assigns[:provider_ids].size).to eq(3)
      expect(assigns[:top_download_songs].length).to eq(1)
      expect(assigns[:top_stream_songs].length).to eq(0)
      expect(assigns[:album_downloads].length).to eq(2)
      expect(assigns[:song_downloads].length).to eq(1)
      expect(assigns[:top_download_countries].length).to eq(1)
      expect(assigns[:top_stream_countries].length).to eq(0)
      expect(assigns[:top_us_markets].length).to eq(1)

      get :release_view, params: { album_id: @album.id, format: "csv" }
      assert_response :success, @response.body
    end

    it "should set all download and stream data for html render" do
      get :release_view, params: { album_id: @album.id }
      assert_response :success, @response.body
      expect(assigns[:download_provider_ids].blank?).to eq(false)
      expect(assigns[:provider_ids].size).to eq(3)
      expect(assigns[:top_download_songs].length).to eq(1)
      expect(assigns[:top_stream_songs].length).to eq(0)
      expect(assigns[:album_downloads].length).to eq(2)
      expect(assigns[:song_downloads].length).to eq(1)
      expect(assigns[:top_download_countries].length).to eq(1)
      expect(assigns[:top_stream_countries].length).to eq(0)
      expect(assigns[:top_us_markets].length).to eq(1)

      get :release_view, params: { album_id: @album.id, format: "csv" }
      assert_response :success, @response.body
    end
  end

  describe "song_view" do
    it "should set all download and stream data for csv" do
      get :song_view, params: { album_id: @album.id, song_id: @album.songs.first.id }
      assert_response :success, @response.body
      expect(assigns[:download_provider_ids].blank?).to eq(false)
      expect(assigns[:provider_ids].size).to eq(3)
      expect(assigns[:song_downloads].length).to eq(1)
      expect(assigns[:top_download_countries].length).to eq(1)
      expect(assigns[:top_stream_countries].length).to eq(0)
      expect(assigns[:top_us_markets].length).to eq(1)

      get :song_view, params: { album_id: @album.id, song_id: @album.songs.first.id }
      assert_response :success, @response.body
    end

    it "should set all download and stream data for html render" do
      get :song_view, params: { album_id: @album.id, song_id: @album.songs.first.id }
      assert_response :success, @response.body
      expect(assigns[:download_provider_ids].blank?).to eq(false)
      expect(assigns[:provider_ids].size).to eq(3)
      expect(assigns[:song_downloads].length).to eq(1)
      expect(assigns[:top_download_countries].length).to eq(1)
      expect(assigns[:top_stream_countries].length).to eq(0)
      expect(assigns[:top_us_markets].length).to eq(1)

      get :song_view, params: { album_id: @album.id, song_id: @album.songs.first.id }
      assert_response :success, @response.body
    end
  end
end
