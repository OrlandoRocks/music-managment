require "rails_helper"

describe GraphqlController, type: :controller do
  include V2::JwtHelper

  describe "#current_user," do
    let!(:payload) do
      {
        id: 123,
        email: "foo",
        name: "bar",
        roles: ["baz"]
      }
    end
    let!(:valid_token) { generate_jwt(payload: payload) }

    before do
      save_token_digest(valid_token, V2::JwtHelper::EXPIRY_DURATION, payload[:id])
    end

    after { redis.del(digest_key(digest(valid_token))) }

    context "without header token," do
      it "is an empty hash" do
        post :execute

        expect(controller.instance_eval { current_user }).to eq({})
      end
    end

    context "with unknown header token" do
      before do
        controller.request.env["HTTP_AUTHORIZATION"] = "foo"
      end

      it "is an empty hash" do
        post :execute

        expect(controller.instance_eval { current_user }).to eq({})
      end
    end

    context "with known header token" do
      before do
        controller.request.env["HTTP_AUTHORIZATION"] = valid_token
      end

      it "matches the payload" do
        post :execute

        expect(controller.instance_eval { current_user }).to eq(payload.with_indifferent_access)
      end
    end
  end
end
