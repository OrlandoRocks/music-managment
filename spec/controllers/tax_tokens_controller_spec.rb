require "rails_helper"

describe TaxTokensController do
  let(:person)    { FactoryBot.create(:person) }
  let(:tax_token) { create(:tax_token, person: person, is_visible: true) }

  describe "#update" do
    before(:each) do
      allow(subject).to receive(:current_user).and_return(person)
      allow(TaxToken).to receive(:find).and_return(tax_token)
      allow(Payoneer::TaxTokenService).to receive(:update).with(person.id).and_return("12345")
    end

    it "gets the fresh token from the API" do
      expect(TaxTokenApiService).to receive(:update).with(person.id, tax_token).and_return(tax_token)

      put :update, params: { id: tax_token.id }
    end

    it "updates the tax tokens token and tax_form_type to the new token and nil" do
      put :update, params: { id: tax_token.id }
      tax_token.reload

      expect(tax_token.tax_form_type).to eq(nil)
      expect(tax_token.token).to eq("12345")
    end
  end
end
