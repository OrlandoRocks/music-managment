require 'rails_helper'

describe EftBatchTransactionsController do
  let(:person) { create(:person) }
  let(:payout_provider) { create(:payout_provider, person: person, provider_status: "approved") }

  before :each do
    allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
    login_as person
  end

  describe "#new" do
    context "eft_batch_transaction" do
      context "with no feature flag enabled" do
        before do
          allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_mandatory, person).and_return(false)
        end

        it "does not redirect to eft_batch_transactions_controller#new" do
          get :new
          expect(response.status).to eq(200)
        end

        it "does redirect to the payout_transfer#new page if the user has an approved payoneer account linked with the payoneer_payout feature flag enabled" do
          allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_opt_out, person).and_return(false)
          person.payout_providers << payout_provider
          get :new
          expect(response.status).to eq(302)
        end
      end

      context "with feature flag enabled" do
        before do
          allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_mandatory, person).and_return(true)
          allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_opt_out, person).and_return(false)
        end

        it "redirects to my_account#withdraw" do
          get :new
          expect(response).to redirect_to(withdraw_path)
        end
      end
    end
  end
end
