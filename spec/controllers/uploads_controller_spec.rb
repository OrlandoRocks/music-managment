require "rails_helper"

describe UploadsController do
  let(:person) { create(:person) }
  let(:song) { create(:song, person: person) }

  let(:asset_uploader_transcoder) { double(:transcoder) }

  before(:each) do
    login_as(person)
  end

  describe "#register" do
    it "responds with success if register form saves" do
      params = {
        upload: {
          song_id: song.id,
          orig_filename: "file_name.mp3",
          bit_rate: 128000
        },
        s3_asset: {
          key: "s3-key-file_name.mp3",
          uuid: "good-uuid",
          bucket: "valid.bucket.com.test"
        }
      }

      fake_form = double(save: true)
      allow(UploadsRegisterForm).to receive(:new).and_return(fake_form)

      post :register, params: params

      expect(response.status).to eq 200
      expect(response.body).to eq({ status: 0 }.to_json)
    end

    it "responds with failure if register form doesn't save" do
      params = {
        upload: {
          song_id: song.id,
          orig_filename: "corrupted_file.mp3",
          bit_rate: 0
        },
        s3_asset: {
          key: "bad-key.mp3",
          uuid: "what-in-the-HECK-is-a-uuid??",
          bucket: "no-bucket..."
        }
      }

      fake_form = double(save: false)
      error_response = { some_attribute: 'some error' }
      allow(UploadsRegisterForm).to receive(:new).and_return(fake_form)
      allow(fake_form).to receive(:errors).and_return(error_response)

      post :register, params: params

      expect(response.status).to eq 422
      expect(response.body).to eq({ errors: error_response }.to_json)
    end

    it "raises an error if certain params are missing" do
      params = {upload: {}, s3_asset: {}}

      fake_form = double(save: true)
      allow(UploadsRegisterForm).to receive(:new).and_return(fake_form)

      expect {
        post :register, params: params
      }.to raise_error
    end
  end

  describe "#get_put_presigned_url" do
    it "responds with put presigned url" do
      stub_const("BIGBOX_BUCKET_NAME", "BIGBOX_BUCKET_NAME")
      params = {
        filename: "filename"
      }

      res = {
        key1: "value1",
        key2: "value2"
      }

      expect(S3Asset).to receive(:get_put_presigned_url).with(
        "BIGBOX_BUCKET_NAME",
        "filename",
        person.id
      ).and_return(res)

      post :get_put_presigned_url, params: params

      expect(response.body).to eq(res.to_json)
    end
  end

  describe "#save_streaming_asset" do
    it "saves the streaming asset" do
      params = {
        key_name: "dir/key_name.mp3",
        bucket_name: "bucket_name",
        song_id: song.id
      }

      expect(AssetsUploader::Transcoder).to receive(:new).with(
        song: song,
        orig_filename: "key_name.mp3",
        source_key: "dir/key_name.mp3",
        source_bucket: "bucket_name"
      ).and_return(asset_uploader_transcoder)

      expect(asset_uploader_transcoder).to receive(:save_streaming_asset)

      post :save_streaming_asset, params: params
      expect(response.body).to eq({ status: "success" }.to_json)
    end
  end

  describe "#validate_asset" do
    before do
      @params = {
        key_name: "dir/key_name.mp3",
        bucket_name: "bucket_name",
        song_id: song.id
      }

      expect(AssetsUploader::Transcoder).to receive(:new).with(
        song: song,
        orig_filename: "key_name.mp3",
        source_key: "dir/key_name.mp3",
        source_bucket: "bucket_name",
        file_type_validation_enabled: false
      ).and_return(asset_uploader_transcoder)
    end

    it "renders respone of validate asset" do
      res = {
        key1: "value1",
        key2: "value2"
      }

      expect(asset_uploader_transcoder).to receive(:validate_asset).and_return(res)

      post :validate_asset, params: @params
      expect(response.status).to be 200
      expect(response.body).to eq(res.to_json)
    end

    it "returns error in case of error" do
      errors = {}
      errors[:duration] = "error in duration"

      expect(asset_uploader_transcoder).to receive(:validate_asset).and_return({
        errors: errors
      })

      post :validate_asset, params: @params
      expect(response.status).to eq(422)
      expect(response.body).to eq({
        errors: errors
      }.to_json)
    end
  end
end
