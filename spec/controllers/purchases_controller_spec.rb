require "rails_helper"

describe PurchasesController, type: :controller do

  describe "remove_all_for_store" do

    before(:each) do
      @person = TCFactory.create(:person)
      login_as(@person)

      @album1 = create(:album, :finalized_at => Date.today, :person => @person)
      @album2 = create(:album, :finalized_at => Date.today, :person => @person)
      @album3 = create(:album, :finalized_at => Date.today, :person => @person)

      @store = Store.find(26) # Spotify

      product = TCFactory.build_ad_hoc_salepoint_product(1.99)
    end

    it "should remove purchases for the store js" do
      create_salepoints(@store, @person, [@album1, @album2, @album3])

      expect {
        expect {
          post :remove_all_for_store, xhr: true, params: {store_id: 26}
        }.to change(Salepoint, :count).by(-3)
      }.to change(Purchase, :count).by(-3)

      assert_response :success, @response.body
    end

    it "should remove purchases for the store html" do
      create_salepoints(@store, @person, [@album1, @album2, @album3])

      expect {
        expect {
          post :remove_all_for_store, params: { store_id: 26 }
        }.to change(Salepoint, :count).by(-3)
      }.to change(Purchase, :count).by(-3)

      assert_redirected_to cart_path
    end
  end

  describe "remove_all_salepoint_subscriptions" do

    before(:each) do
      @person = TCFactory.create(:person)
      login_as(@person)

      @album1  = TCFactory.create(:album, :person => @person, :finalized_at => Date.today)
      @album2  = TCFactory.create(:album, :person => @person, :finalized_at => Date.today)

      product = TCFactory.build_ad_hoc_salepoint_subscription_product(5.00)
    end

    it "should remove all salepoint subscriptions from the cart js" do
      create_salepoint_subscriptions(@person, [@album1, @album2])

      expect {
        expect {
          post :remove_all_salepoint_subscriptions, xhr: true
        }.to change(SalepointSubscription, :count).by(-2)
      }.to change(Purchase,:count).by(-2)

      assert_response :success, @response.body
    end

    it "should remove all salepoint subscriptions from the cart html" do
      create_salepoint_subscriptions(@person, [@album1, @album2])

      expect {
        expect {
          post :remove_all_salepoint_subscriptions
        }.to change(SalepointSubscription, :count).by(-2)
      }.to change(Purchase, :count).by(-2)

      assert_redirected_to cart_path
    end
  end

  describe "destroy" do
    let!(:person) { create(:person) }
    let!(:plan_product) { Product.find_by(display_name: "rising_artist", country_website_id: 1) }
    let!(:album) { create(:album, finalized_at: Date.today, person: person) }
    let!(:album2) { create(:album, finalized_at: Date.today, person: person) }

    before(:each) do
      login_as(person)
    end

    it "should empty the cart when deleting a plan from a non-plan user" do
      purchase = Product.add_to_cart(person, plan_product, plan_product)
      album.create_purchase_with_credit
      album2.create_purchase_with_credit
      expect {
        delete :destroy, params: { id: purchase.id }
      }.to change(Purchase, :count).by(-3)
    end

    it "should not call the ImmersiveAudio RemovalService when the purchase is not Immersive Audio" do
      create(:song, :with_s3_asset, album: album, name: "Not Immersive")
      create(:song, :with_s3_asset, album: album, name: "RSPEC Find Out What It Means To Me")
      purchase = Product.add_to_cart(person, album)

      expect(ImmersiveAudio::RemovalService).to_not receive(:new)
      expect {
        delete :destroy, params: { id: purchase.id }
      }.to change(Purchase, :count).by(-1)
    end

    it "should call the ImmersiveAudio RemovalService when the purchase is Immersive Audio" do
      song = create(:song, :with_s3_asset, album: album, name: "Not Immersive (Yet)")
      create(:spatial_audio, song: song)
      purchase = Product.add_to_cart(person, song.spatial_audio, person.dolby_atmos_product)

      immersive_removal_dbl = double(:immersive_audio_removal_service, call: true)
      expect(ImmersiveAudio::RemovalService).to receive(:new).with({immersive_audio_id: purchase.related_id}).and_return(immersive_removal_dbl)

      expect {
        delete :destroy, params: { id: purchase.id }
      }.to change(Purchase, :count).by(-1)
    end
  end

  def create_salepoints(store, person, albums)
    albums.each do |album|
      salepoint = create(:salepoint, :salepointable => album, :store => store, payment_applied: false)
      Product.add_to_cart(person, salepoint)
    end
  end

  def create_salepoint_subscriptions(person, albums)
    albums.each do |album|
      sp_sub = TCFactory.create(:salepoint_subscription, :album => album)
      Product.add_to_cart(person, sp_sub)
    end
  end

end
