require "rails_helper"

describe PayoutTransfersController do
  let(:person) { create(:person, :with_balance, amount: 50_000) }
  let(:payout_provider) { create(:payout_provider, person: person) }
  let(:form_params) {
    { payout_transfer_create_form: {
        amount_cents_raw: 1000,
        amount: "$10.00"
      }
    }
  }
  let(:create_form_stub) { double(:create_form, save: true, payout_transfer: create(:payout_transfer)) }
  before(:each) do
    login_as(person)
    allow(PayoutTransfer::CreateForm).to receive(:new).and_return(create_form_stub)
    allow_any_instance_of(Payoneer::V2::PayoutApiClient).to receive(:send_request).and_return(OpenStruct.new(body: '{}'))
    allow(FeatureFlipper).to receive(:show_feature?).and_return(false)
    allow(FeatureFlipper).to receive(:show_feature?).with(:use_payoneer_v4_api, person).and_return(false)
    allow(FeatureFlipper).to receive(:show_feature?).with(:use_payoneer_v4_api).and_return(false)
  end

  describe "#show" do
    it "redirects to my_account#withdraw page if user not approved" do
      payout_provider.provider_status = PayoutProvider::ONBOARDING
      get :show, params: { id: payout_provider.id }
      expect(response).to redirect_to(withdraw_path)
    end
  end

  describe "#confirm" do
    before do
      payout_provider.update(provider_status: "approved")
      allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking, person).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_opt_out, person).and_return(false)
    end

    it "redirects to my_account#withdraw page if user not approved" do
      payout_provider.update(provider_status: PayoutProvider::ONBOARDING)
      get :confirm, params: form_params
      expect(response).to redirect_to(withdraw_path)
    end

    it "redirects to taxform submission page if tax-blocked user attempts withdrawal" do
      allow(person).to receive(:tax_blocked?).and_return(true)
      post :confirm, params: form_params
      expect(response).to redirect_to(account_settings_path(tab: "taxpayer_id"))
    end

    it "doesn't redirect to taxform submission page if tax-unblocked user attempts withdrawal" do
      allow(person).to receive(:tax_blocked?).and_return(false)
      post :confirm, params: form_params
      expect(response).not_to redirect_to(account_settings_path(tab: "taxpayer_id"))
    end

    it "doesn't redirect to taxform submission page when tax blocking is disabled" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking, person).and_return(false)
      post :confirm, params: form_params
      expect(response).not_to redirect_to(account_settings_path(tab: "taxpayer_id"))
    end
  end

  describe "#create" do
    it "redirects to my_account#withdraw page if user not approved" do
      payout_provider.provider_status = PayoutProvider::ONBOARDING
      post :create, params: form_params
      expect(response).to redirect_to(withdraw_path)
    end

    it "renders confirm if the form errors" do
      person = create(:person)
      payout_provider = create(:payout_provider,
                               person: person,
                               provider_status: PayoutProvider::APPROVED)
      failed_create_form = double(:create_form, save: false)
      allow(PayoutTransfer::CreateForm).to receive(:new).and_return(failed_create_form)
      login_as(person)

      post :create, params: form_params

      expect(response).to render_template(:confirm)
    end

    it "redirects to show page if form saves" do
      payout_provider.update(provider_status: PayoutProvider::APPROVED)

      post :create, params: form_params

      expect(response).to redirect_to(create_form_stub.payout_transfer)
    end
  end
end

describe PayoutTransfersController, type: :controller do
  let(:person) { create(:person, :with_balance, amount: 50_000) }
  let(:payout_provider) { create(:payout_provider, person: person) }
  let(:form_params) {
    {
      payout_transfer_create_form: {
        amount_cents_raw: 1000,
        amount: "$10.00"
      }
    }
  }
  let(:create_form_stub) { double(:create_form, save: true, payout_transfer: create(:payout_transfer)) }
  before(:each) do
    login_as(person)
    allow(PayoutTransfer::CreateForm).to receive(:new).and_return(create_form_stub)
    allow_any_instance_of(Payoneer::V2::PayoutApiClient).to receive(:send_request).and_return(OpenStruct.new(body: '{}'))
    allow(FeatureFlipper).to receive(:show_feature?).and_return(false)
    allow(FeatureFlipper).to receive(:show_feature?).with(:use_payoneer_v4_api, person).and_return(false)
    allow(FeatureFlipper).to receive(:show_feature?).with(:use_payoneer_v4_api).and_return(false)
  end

  describe "#new" do
    it "renders the page if the user is signed up and approved for payoneer" do
      payout_provider.update_attribute(:provider_status, "approved")
      get :new
      expect(response.status).to eq(200)
    end

    it "redirects to the my_account#withdraw page if the user is opted out of payoneer" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_opt_out, person).and_return(true)
      payout_provider.update_attribute(:provider_status, "approved")
      get :new
      expect(response.status).to eq(302)
      expect(response).to redirect_to(withdraw_path)
    end

    context "when tax blocking is enabled" do
      before do
        payout_provider.update(provider_status: "approved")
        allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking, person).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_opt_out, person).and_return(false)
      end

      it "summarizes earnings" do
        service_double = instance_double("TaxBlocking::PersonEarningsSummarizationService")
        expect(TaxBlocking::PersonEarningsSummarizationService)
          .to receive(:new).with(person.id, Date.current.year).and_return(service_double)
        expect(service_double).to receive(:summarize)
        get :new
      end

      it "updates tax-forms metadata" do
        service_double = instance_double("TaxBlocking::TaxformInfoUpdateService")
        expect(TaxBlocking::TaxformInfoUpdateService)
          .to receive(:new).with(person.id, Date.current.year).and_return(service_double)
        expect(service_double).to receive(:summarize)
        get :new
      end

      it "updates tax-blocked status" do
        expect(TaxBlocking::UpdatePersonTaxblockStatusService).to receive(:call).with(person)
        get :new
      end
    end

    context "when tax blocking is disabled" do
      before do
        payout_provider.update(provider_status: "approved")
        allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_opt_out, person).and_return(false)
        allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking, person).and_return(false)
      end

      it "doesn't summarize earnings" do
        expect(TaxBlocking::PersonEarningsSummarizationService).not_to receive(:new).with(person.id, Date.current.year)
        get :new
      end

      it "doesn't update tax-forms metadata" do
        expect(TaxBlocking::TaxformInfoUpdateService).not_to receive(:new).with(person.id, Date.current.year)
        get :new
      end

      it "doesn't update tax-blocked status" do
        expect(TaxBlocking::UpdatePersonTaxblockStatusService).not_to receive(:call).with(person)
        get :new
      end
    end

    context "for a non-US user" do
      before do
        person.update(country: "LU")
        payout_provider.update(provider_status: "approved")
        allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_opt_out, person).and_return(false)
        allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking, person).and_return(true)
      end

      it "doesn't summarize earnings" do
        expect(TaxBlocking::PersonEarningsSummarizationService).not_to receive(:new).with(person.id, Date.current.year)
        get :new
      end

      it "doesn't update tax-forms metadata" do
        expect(TaxBlocking::TaxformInfoUpdateService).not_to receive(:new).with(person.id, Date.current.year)
        get :new
      end

      it "updates tax-blocked status" do
        expect(TaxBlocking::UpdatePersonTaxblockStatusService).to receive(:call).with(person)
        get :new
      end
    end
  end
end
