require "rails_helper"

describe SoundoutReportsController, type: :controller do

  describe "show" do

    it "should redirect if the report is not received" do
      person = Person.where("email=?", "customer@tunecore.com").first
      login_as(person)
      soundout_report = create(:soundout_report, :with_track, person: person, status: "new")

      get :show, params: { format: "pdf", id: soundout_report.id }
      assert_redirected_to soundout_reports_path
    end

  end

  describe "overall_ratings" do

    it "should require a non cancelled report" do
      person = Person.where("email=?", "customer@tunecore.com").first
      login_as(person)

      soundout_report = create(:soundout_report, :with_track, person: person, status: 'received')
      get :overall_ratings, params: { id: soundout_report.id }
      assert_response :success, @response.body

      soundout_report.toggle_cancel!

      get :overall_ratings, params: { id: soundout_report.id }
      assert_redirected_to soundout_reports_path
    end

    it "should allow an admin to view any report" do
      person = Person.where("email=?", "customer@tunecore.com").first
      soundout_report = create(:soundout_report, :with_track, person: person, status: 'received')

      login_as(person)
      get :overall_ratings, params: { id: soundout_report.id }
      assert_response :success, @response.body

      admin = Person.where("email=?", "admin@tunecore.com").first
      login_as(admin)
      get :overall_ratings, params: { id: soundout_report.id }
      assert_response :success, @response.body
    end

  end

  describe "purchase" do
    before(:each) do
      @album = create(
        :album,
        :with_salepoints,
        :with_uploaded_songs,
        :with_artwork,
        :with_songwriter,
        :paid,
        :live
      )
      petri_bundle = create(:petri_bundle, album: @album)
      create(:distribution, :delivered_to_spotify_find_or_create_salepoint, petri_bundle: petri_bundle)
      @song = @album.songs.first
      allow_any_instance_of(Song).to receive(:duration).and_return(95000)
    end

    it "should set soundout_songs with all song_library_uploads" do
      person = TCFactory.create(:person, country: "US")
      login_as(person)
      upload = FactoryBot.create(:song_library_upload, person: person)
      get :purchase
      assert_response :success, @response.body
      expect(assigns(:soundout_songs)).to eq([upload])
    end

    it "should find an US soundout product to purchase" do
      @us_person = TCFactory.create(:person, :country=>"US")
      login_as(@us_person)

      #One use report
      report = TCFactory.create(:soundout_product)

      purchase = mock_model(Purchase)

      expect(Product).to receive(:find).with(report.product_id).and_return(report.product)
      expect(Product).to receive(:add_to_cart).and_return(purchase)
      post :purchase_reports, params: { assets: "{}", purchase: { "#{@song.id}" => report.report_type }.to_json }
    end

    it "should find a CA soundout product to purchase" do
      @ca_person = TCFactory.create(:person, :country=>"CA")
      login_as(@ca_person)

      #Report is for CA country website
      report   = TCFactory.create(:soundout_product, :product=>Product.find(Product::CA_ONE_YEAR_ALBUM_PRODUCT_ID))
      purchase = mock_model(Purchase)

      expect(Product).to receive(:find).with(report.product_id).and_return(report.product)
      expect(Product).to receive(:add_to_cart).and_return(purchase)
      post :purchase_reports, params: { assets: "{}", purchase: { "#{@song.id}" => report.report_type }.to_json }
    end

    it "should find a specific report type to purchase" do
      @us_person = TCFactory.create(:person, :country=>"US")
      login_as(@us_person)

      #Two reports, one with a unique report type
      report  = TCFactory.create(:soundout_product, :report_type=>"Find Me")
      report2 = TCFactory.create(:soundout_product)

      purchase = mock_model(Purchase)
      expect(Product).to receive(:find).with(report.product_id).and_return(report.product)
      expect(Product).to receive(:add_to_cart).and_return(purchase)
      post :purchase_reports, params: { assets: "{}", purchase: { "#{@song.id}" => report.report_type }.to_json }
    end

    it "should create an soundout_report" do
      @us_person = TCFactory.create(:person, :country=>"US")
      login_as(@us_person)

      report   = mock_model(SoundoutProduct)
      product  = mock_model(Product)
      purchase = mock_model(Purchase)

      allow(report).to receive(:id).and_return(1)

      expect(SoundoutProduct).to receive(:find_for_purchase).and_return(report)
      expect(Product).to receive(:find).and_return(product)
      expect(Product).to receive(:add_to_cart).and_return(purchase)

      expect {
        post :purchase_reports, params: { assets: "{}", purchase: { "#{@song.id}" => report.report_type }.to_json }
      }.to change( SoundoutReport, :count).by(1)

      expect(SoundoutReport.last.soundout_product_id).to eq(report.id)
    end

    it "should add an soundout_report, soundout_product, and product to cart" do
      @us_person = TCFactory.create(:person, :country=>"US")
      login_as(@us_person)

      report          = mock_model(SoundoutProduct)
      product         = mock_model(Product)
      purchase        = mock_model(Purchase)
      report_purchase = mock_model(SoundoutReport)
      purchase        = mock_model(Purchase)

      expect(SoundoutProduct).to receive(:find_for_purchase).and_return(report)
      expect(Product).to receive(:find).and_return(product)
      expect(SoundoutReport).to receive(:create).and_return(report_purchase)

      expect(Product).to receive(:add_to_cart).with(@us_person, report_purchase, product).and_return(purchase)
      
      post :purchase_reports, params: { assets: "{}", purchase: { "#{@song.id}" => report.report_type }.to_json }
    end

  end

  describe "#available" do

    it "should require authentication" do
      get :available
      assert_response :unauthorized, @response.body

      credentials = ActionController::HttpAuthentication::Basic.encode_credentials "bad_username", SOUNDOUT_CONFIG["TC_API_PASSWORD"]
      request.env['HTTP_AUTHORIZATION'] = credentials
      assert_response :unauthorized, @response.body

      credentials = ActionController::HttpAuthentication::Basic.encode_credentials SOUNDOUT_CONFIG['TC_API_AUTH_ID'], SOUNDOUT_CONFIG["TC_API_PASSWORD"]
      request.env['HTTP_AUTHORIZATION'] = credentials
      get :available
      assert_response :unprocessable_entity, @response.body
    end

    it "should find a report purchase by soundout identifier" do
      expect(Soundout::FetchWorker).to receive(:perform_async)

      report = create(:soundout_product)
      report_purchase = create(:soundout_report, :with_track, soundout_product: report, soundout_id: "123")

      credentials = ActionController::HttpAuthentication::Basic.encode_credentials SOUNDOUT_CONFIG['TC_API_AUTH_ID'], SOUNDOUT_CONFIG["TC_API_PASSWORD"]
      request.env['HTTP_AUTHORIZATION'] = credentials

      get :available, params: { id: "123" }
      assert_response :ok, @response.body
    end

    it "should mark a report as available" do
      product = TCFactory.create(:soundout_product)
      report = create(:soundout_report, :with_track, soundout_product: product, soundout_id: "123")

      credentials = ActionController::HttpAuthentication::Basic.encode_credentials SOUNDOUT_CONFIG['TC_API_AUTH_ID'], SOUNDOUT_CONFIG["TC_API_PASSWORD"]
      request.env['HTTP_AUTHORIZATION'] = credentials
      get :available, params: { id: "123" }
      assert_response :ok, @response.body

      expect(report.reload.available?).to be_truthy
    end

  end

end
