require "rails_helper"

feature "dashboard" do
  context "upon first logging in" do
    it "displays basic product messaging" do
      person = create(:person)

      login_user(person, "Testpass123!")
      visit dashboard_path

      expect(page).to have_content "two or more songs ready?"
      expect(page).to have_content "new single?"
      expect(page).to have_content "ringtone ready to go?"
    end
  end

  context "with unfinalized content" do
    scenario "displays albums, singles and ringtones" do
      person = create(:person)
      album = create(:album, :purchaseable, name: "New Album", person: person)
      single = create(:single, :with_artwork, name: "New Single", person: person)
      ringtone = create(:ringtone, person: person, name: "New Ringtone")

      login_user(person, "Testpass123!")
      visit dashboard_path

      expect(page).to have_content "Your Releases"
      expect(page).to have_content "Incomplete"
      expect(page).to have_content album.name
      expect(page).to have_content single.name
      expect(page).to have_content ringtone.name
    end
  end

  context "with finalized content" do
    scenario "displays albums, singles and ringtones" do
      person = create(:person)
      album = create(:album, :finalized, :purchaseable, name: "New Album", person: person)
      single = create(:finalized_single, name: "New Single", person: person)
      ringtone = create(:ringtone, name: "New Ringtone", person: person)

      login_user(person, "Testpass123!")
      visit dashboard_path

      expect(page).to have_content "Your Releases"
      expect(page).to have_content album.name
      expect(page).to have_content single.name
      expect(page).to have_content ringtone.name
    end
  end

  context "when actively selling" do
    scenario "displays store manager addon" do
      person = create(:person)
      create(:album, :finalized, :purchaseable, name: "New Album", person: person)

      login_user(person, "Testpass123!")
      visit dashboard_path

      expect(page).to have_content "Tap into unexplored markets"
      expect(page).to have_content "1 of your releases"
    end
  end

  context "distribution credits" do
    context "with no distribution credit" do
      xscenario "displays empty distribution credit info" do
        person = create(:person)

        login_user(person, "Testpass123!")
        visit dashboard_path

        expect(page).to have_content "You have no distribution credits."
        expect(page).to have_link "Distribution Credit Packs"
      end
    end

    context "with distribution credits" do
      xscenario "displays empty distribution credit info" do
        person = create(:person)
        product = create(:product, :with_product_item, product_type: "Package")
        product_item = product.product_items.first
        create(:album_credit_inventory, person: person, quantity: 1, product_item: product_item)

        login_user(person, "Testpass123!")
        visit dashboard_path

        expect(page).to have_content "1 Album Credit"
        expect(page).to have_link "Distribution Credit Packs"
      end
    end
  end

  context "tunecore social interactive publisher" do
    before(:each) do
      FeatureFlipper.update_feature :tc_social, 100, ""
    end

    after do
      FeatureFlipper.update_feature :tc_social, 0, ""
    end
  end

  describe "stem user" do
    let(:person) { create(:person) }

    before do
      allow_any_instance_of(Person).to receive(:blocked_for_stem?).and_return(true)
      login_user(person, "Testpass123!")
    end

    scenario "should display blocking popup modal" do
      visit dashboard_path

      expect(page).to have_selector("div", id: "stem-welcome-modal")
    end

    context "not blocked for stem" do
      before do
        allow_any_instance_of(Person).to receive(:blocked_for_stem?).and_return(false)
        visit dashboard_path
      end

      scenario "should not display blocking popup modal" do
        expect(page).to_not have_selector("div", id: "stem-welcome-modal")
      end
    end
  end
end
