require "rails_helper"

feature "Monetize/Mark Tracks from Album Page" do
  xscenario "should redirect to YTSR dashboard (temporary)" do
    person = create(:person)
    album = create(:album, :purchaseable, :approved, person: person)
    song = album.songs.first
    yt_store = Store.find_by_abbrev("ytsr")
    create(:salepoint, salepointable: album, store: yt_store)
    create(:youtube_monetization, person: person, effective_date: Time.now)

    login_user(person, "Testpass123!")
    visit discography_path
    click_on album.name
    click_on "Monetize on YouTube"

    expect(page).to have_content "Send Your Music To YouTube"
  end

  xscenario "successfully adds a track to youtube monetization" do
    person = create(:person)
    album = create(:album, :purchaseable, :approved, person: person)
    song = album.songs.first
    yt_store = Store.find_by(abbrev: "ytsr")
    create(:salepoint, salepointable: album, store: yt_store)
    create(:youtube_monetization, person: person, effective_date: Time.now)

    login_user(person, "Testpass123!")
    visit discography_path
    click_on album.name
    click_on "Monetize on YouTube"
    choose("monetize_#{song.id}")
    click_on "Save & Review"
    click_on "Confirm"

    expect(page).to have_content "Monetize Tracks on YouTube"
    expect(page).to have_content "Your selections have been saved."
  end

  xscenario "flashes an error if button is clicked with no songs selected" do
    person = create(:person)
    album = create(:album, :purchaseable, :approved, person: person)
    yt_store = Store.find_by(abbrev: "ytsr")
    create(:salepoint, salepointable: album, store: yt_store)
    create(:youtube_monetization, person: person, effective_date: Time.now)

    login_user(person, "Testpass123!")
    visit discography_path
    click_on album.name
    click_on "Monetize on YouTube"
    click_on "Save & Review"

    expect(page).to have_content "No songs selected."
  end

  xscenario "displays songs marked ineligible and monetized in the table" do
    person = create(:person)
    album = create(:album, :purchaseable, :approved, person: person)
    song = album.songs.first
    another_song = album.songs.last
    yt_store = Store.find_by(abbrev: "ytsr")
    create(:salepoint, salepointable: album, store: yt_store)
    create(:youtube_monetization, person: person, effective_date: Time.now)

    login_user(person, "Testpass123!")
    visit discography_path
    click_on album.name
    click_on "Monetize on YouTube"
    choose("monetize_#{song.id}")
    choose("ineligible_#{another_song.id}")
    click_on "Save & Review"
    click_on "Confirm"

    expect(page).to have_content "Do Not Monetize"
  end
end
