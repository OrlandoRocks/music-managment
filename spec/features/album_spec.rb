require "rails_helper"

feature "album", type: :feature do
  let(:person) { create(:person) }

  let(:album) do
    create(
      :album,
      :with_uploaded_songs_and_artwork,
      number_of_songs: 2,
      person: person
    )
  end

  before(:each) do
    login_user(person, "Testpass123!")
  end

  describe "user can visit album pages" do
    it "/albums/:id/edit" do
      visit edit_album_path(album)
      expect(page).to have_content album.name
    end

    it "/albums/show" do
      visit album_path(album)
      expect(page).to have_text album.name
    end

    it "/albums/new" do
      visit new_album_path
      expect(page).to have_text custom_t("album.new.create_an_album")
    end

    it "/albums/:id/artwork" do
      visit album_artwork_path(album)
      expect(page).to have_text custom_t("artwork.artwork_requirements.add_cover_art")
    end
  end

  describe "user creates a new album release" do
    it "sets default language code on new album form for all country websites" do
      CountryWebsite.all.each do |cw|
        visit new_album_path(country_locale: cw.country)
        language_selected = find('#album_language_code_legacy_support').value
        expect(language_selected).to eq(I18n.locale.to_s[0..1])
      end
    end
  end
end
