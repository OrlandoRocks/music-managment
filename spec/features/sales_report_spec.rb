require "rails_helper"
require_relative "../support/features/signup_helpers"
include Features::SignupHelpers

describe "Sales reporting", type: :feature do

  let(:person) { FactoryBot.create(:person) }
  let(:password) { "Testpass123!" }
  let(:album) { FactoryBot.create(:album, :paid, :with_uploaded_songs, person: person, name: "SIP Album", finalized_at: '2010-09-06') }

  before(:each) do
    login_user(person, password)
  end

  it "visits a sales report page with no sales" do
    visit date_report_path
    expect(page).to have_content "No music sales have been reported for your TuneCore account"
  end

  context "person has 1 sale" do
    before(:each) do
      @sales_period = Date.today.beginning_of_month - 4.months
      srm = FactoryBot.create(:sales_record_master, period_sort: @sales_period)
      sales_record = FactoryBot.create(:sales_record, sales_record_master: srm, related: album, related_type: 'Album', person: person, quantity: 1)
      TCFactory.summarize_sales_record_master(srm)
    end

    it "visits the release report page with 1 sales release" do
      visit "/sales/release_report"

      expect(page.find("div#albums-filter")).to have_content "Sip Album"
      expect(page.find("div#sales-releases")).to have_content "$0.70"
      expect(page.find("div#sales-totals")).to have_content "$0.70"
      expect(page.find("table#sales-table")).to have_content "Sip Album"
    end

    it "visits the store report page with 1 sales release" do
      visit "/sales/store_report"

      expect(page.find("div#sales-releases")).to have_content "$0.70"
      expect(page.find("div#sales-totals")).to have_content "$0.70"
      expect(page.find("table#sales-table")).to have_content "iTunes"
    end

    it "visits the date report page with 1 sales release" do
      visit "/sales/date_report"

      expect(page.find("div#albums-filter")).to have_content "Sip Album"
      expect(page.find("div#sales-releases")).to have_content "$0.70"
      expect(page.find("div#sales-totals")).to have_content "$0.70"
      expect(page.find("table#sales-table")).to have_content @sales_period.strftime("%b %Y")
    end
  end

  context "person has 2 download sales and 1 streaming sale" do
    before(:each) do
      @sales_period = Date.today.beginning_of_month - 4.months
      @srm = FactoryBot.create(:sales_record_master, period_sort: @sales_period)
      @sales_record = FactoryBot.create(:sales_record, related: album.songs[0], related_type: 'Song', person: person, quantity: 2, revenue_total: 1.40, amount: 1.40, sales_record_master: @srm)
      @streaming_sip_store = FactoryBot.create(:sip_store, name: "Spotify", store_id: 26, display_group_id: 5)
      @streaming_sales_record_master = FactoryBot.create(:sales_record_master, sip_store: @streaming_sip_store, period_sort: @sales_period)
      @streaming_sales_record = FactoryBot.create(:streaming_sales_record, sales_record_master: @streaming_sales_record_master, related: album, related_type: 'Album', person: person)
      TCFactory.summarize_sales_record_master(@sales_record.sales_record_master)
      TCFactory.summarize_sales_record_master(@streaming_sales_record_master)
    end

    it "visits the release report page" do
      visit "/sales/release_report"

      expect(page.find("div#albums-filter")).to have_content "Sip Album"
      expect(page.find("div#sales-songs")).to have_content "$1.40"
      expect(page.find("div#sales-songs")).to have_content "From 2 Sold"
      expect(page.find("div#sales-streams")).to have_content "$0.03"
      expect(page.find("div#sales-streams")).to have_content "From 3 Plays"
      expect(page.find("div#sales-totals")).to have_content "$1.43"
      expect(page.find("table#sales-table")).to have_content "Sip Album"
      expect(page.find("td.sales-songs-sold")).to have_content "2"
      expect(page.find("td.sales-streams-sold")).to have_content "3"
      expect(page.find("td.sales-total-earned")).to have_content "$1.43"
    end

    it "visits the release report page" do
      visit "/sales/store_report"

      expect(page.find("div#albums-filter")).to have_content "Sip Album"
      expect(page.find("div#sales-songs")).to have_content "$1.40"
      expect(page.find("div#sales-songs")).to have_content "From 2 Sold"
      expect(page.find("div#sales-streams")).to have_content "$0.03"
      expect(page.find("div#sales-streams")).to have_content "From 3 Plays"
      expect(page.find("div#sales-totals")).to have_content "$1.43"
      expect(page.find("table#sales-table")).to have_content "iTunes"
      expect(page.find("table#sales-table")).to have_content "Spotify"
      expect(page.find("tr.odd/td.sales-songs-sold")).to have_content "2"
      expect(page.find("tr.even/td.sales-streams-sold")).to have_content "3"
      expect(page.find("tr.odd/td.sales-total-earned")).to have_content "$1.40"
      expect(page.find("tr.even/td.sales-total-earned")).to have_content "$0.03"
    end

    it "visits the date report page" do
      visit "/sales/date_report"

      expect(page.find("div#albums-filter")).to have_content "Sip Album"
      expect(page.find("div#sales-songs")).to have_content "$1.40"
      expect(page.find("div#sales-songs")).to have_content "From 2 Sold"
      expect(page.find("div#sales-streams")).to have_content "$0.03"
      expect(page.find("div#sales-streams")).to have_content "From 3 Plays"
      expect(page.find("div#sales-totals")).to have_content "$1.43"
      expect(page.find("table#sales-table")).to have_content @sales_period.strftime("%b %Y")
      expect(page.find("td.sales-songs-sold")).to have_content "2"
      expect(page.find("td.sales-streams-sold")).to have_content "3"
      expect(page.find("td.sales-total-earned")).to have_content "$1.43"
    end

  end
end
