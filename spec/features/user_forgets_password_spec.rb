require "rails_helper"

feature "forgot password reskin" do
  before do
    allow(FeatureFlipper).to receive(:show_feature?).with(any_args).and_call_original
    allow(FeatureFlipper).to receive(:show_feature?).with(:reskin, any_args).and_return(true)
  end

  scenario "user requests reset link" do
    visit new_reset_password_url
    fill_in("Email Address", with: "email@example.com")
    click_on("Send Reset Link")

    expect(current_path).to include(login_path)
    find("[data-testid=flash]", text: "Instructions on how to reset your password have been sent")
  end

  scenario "sending the reset email fails" do
    expect(PasswordResetService).to receive(:reset).with(any_args).and_raise("oh no")
    person = create(:person)
    visit new_reset_password_url
    fill_in("Email Address", with: person.email)
    click_on("Send Reset Link")

    find("[data-testid=flash]", text: "Sorry, we were not able to send password reset instructions")
  end
end
