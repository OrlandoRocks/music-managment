require "rails_helper"

feature "balance report" do
  scenario "correctly filters by date" do
    password = "Password123!"
    person = create(:person, :with_balance, amount: 50_000, password: password, password_confirmation: password)
    old_person_transaction = create(
      :person_transaction,
      comment: "old transaction",
      person: person,
      created_at: 2.months.ago
    )
    new_person_transaction = create(
      :person_transaction,
      comment: "new transaction",
      person: person
    )

    login_user(person, password)
    visit list_transactions_path

    expect(page).to have_content new_person_transaction.comment
    expect(page).to have_content old_person_transaction.comment

    first('#date_end_date option').select_option
    click_on "Filter"

    expect(page).to have_content old_person_transaction.comment
    expect(page).to_not have_content new_person_transaction.comment
  end

  scenario "correctly filters by transaction type" do
    password = "Testpass123!"
    person = create(:person, :with_stored_bank_account, :with_balance, amount: 50_000)
    check_transfer = create(:check_transfer, person: person, password_entered: password)
    check_transaction = create(
      :person_transaction,
      target: check_transfer,
      person: person,
      comment: "Check paid to: #{person.name}"
    )
    paypal_transfer = create(:paypal_transfer, person: person)
    paypal_transaction = create(
      :person_transaction,
      target: paypal_transfer,
      person: person,
      comment: "Paypal funds transfer to: #{person.email}"
    )
    eft_transfer = create(
      :eft_batch_transaction,
      stored_bank_account: person.stored_bank_accounts.first
    )
    eft_transaction = create(
      :person_transaction,
      target: eft_transfer,
      person: person,
      comment: "Electronic Funds Transfer"
    )
    invoice_transaction = create(:person_transaction, person: person)

    login_user(person, password)
    visit list_transactions_path

    expect(page).to have_content paypal_transaction.comment
    expect(page).to have_content eft_transaction.comment
    expect(page).to have_content invoice_transaction.comment
    expect(page).to have_content check_transaction.comment

    uncheck "Check Withdrawal"
    click_on "Filter"

    expect(page).to have_content paypal_transaction.comment
    expect(page).to have_content eft_transaction.comment
    expect(page).to have_content invoice_transaction.comment
    expect(page).to_not have_content check_transaction.comment

    uncheck "Purchases"
    click_on "Filter"

    expect(page).to have_content paypal_transaction.comment
    expect(page).to have_content eft_transaction.comment
    expect(page).to_not have_content invoice_transaction.comment
    expect(page).to_not have_content check_transaction.comment

    uncheck "Paypal Withdrawal"
    click_on "Filter"

    expect(page).to have_content eft_transaction.comment
    expect(page).to_not have_content paypal_transaction.comment
    expect(page).to_not have_content invoice_transaction.comment
    expect(page).to_not have_content check_transaction.comment

    uncheck "Electronic Funds Transfer"
    click_on "Filter"

    within "#sales-table" do
      expect(page).to_not have_content eft_transaction.comment
      expect(page).to_not have_content paypal_transaction.comment
      expect(page).to_not have_content invoice_transaction.comment
      expect(page).to_not have_content check_transaction.comment
    end
  end
end
