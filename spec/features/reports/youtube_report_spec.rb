require "rails_helper"

feature "youtube report" do
  scenario "correctly filters by album and date range" do
    password = "Password1!"
    person = create(:person, :with_ytsr_purchase, password: password, password_confirmation: password)
    person.youtube_monetization.update(effective_date: Time.now)
    first_album = person.albums.where(legal_review_state: "APPROVED").first
    second_album = person.albums.where(legal_review_state: "APPROVED").last
    third_album = person.albums.where(legal_review_state: "APPROVED").last
    youtube_royalty_record = create(
      :you_tube_royalty_record,
      person: person,
      net_revenue: 40.00,
      sales_period_start: Time.now.beginning_of_month,
      song: first_album.songs.first
    )
    another_youtube_royalty_record = create(
      :you_tube_royalty_record,
      person: person,
      net_revenue: 20.00,
      sales_period_start: Time.now.beginning_of_month,
      song: second_album.songs.first
    )
    out_of_range_youtube_royalty_record = create(
      :you_tube_royalty_record,
      person: person,
      net_revenue: 80.00,
      sales_period_start: 1.month.ago.beginning_of_month,
      song: third_album.songs.first
    )

    login_user(person, password)
    visit youtube_royalties_release_report_path

    expect(page).to have_content "revenue from your sound recordings on YouTube videos."
    expect(page).to have_content youtube_royalty_record.net_revenue
    expect(page).to have_content another_youtube_royalty_record.net_revenue
    expect(page).not_to have_content out_of_range_youtube_royalty_record.net_revenue

    uncheck "release_albums_#{first_album.id}"
    click_on "Filter"

    expect(page).to have_content another_youtube_royalty_record.net_revenue
    expect(page).not_to have_content youtube_royalty_record.net_revenue
    expect(page).not_to have_content out_of_range_youtube_royalty_record.net_revenue

    check "release_albums_#{first_album.id}"
    first('#date_start_date option').select_option
    first('#date_end_date option').select_option
    click_on "Filter"

    expect(page).not_to have_content youtube_royalty_record.net_revenue
    expect(page).not_to have_content another_youtube_royalty_record.net_revenue
    expect(page).to have_content out_of_range_youtube_royalty_record.net_revenue
  end
end
