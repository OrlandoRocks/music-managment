require "rails_helper"

feature "song library uploads for soundout purchases", js: true do
  before do
    allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
    allow(FeatureFlipper).to receive(:show_feature?).with(:recaptcha).and_return(false)
  end

  # This spec is failing due to a Selenium issue after updating
  # the test docker image in CircleCi.
  # Commenting this spec out for now and will address it in a
  # separate ticket.
  xscenario "a user uploads a song" do
    expect(SongLibraryUpload.count).to eq(0)

    song_title = "Buckets and Bolts"
    artist_name = "Ralph Raphliyson and the Bangers"
    song_file = Rails.root.join("spec", "assets", "track_90_seconds.mp3")

    password = "Password1!"
    person = create(
      :person,
      password: password,
      password_confirmation: password,
      is_verified: true,
      verified_at: Time.current
    )
    create(
      :album,
      :live,
      :with_songs,
      :with_streaming_distribution,
      person: person
    )

    login_user(person, password)

    visit(purchase_soundout_reports_path)

    fill_in "Song Title*", with: song_title
    fill_in "Artist Name*", with: artist_name
    select "Alternative", from: "Genre"
    attach_file "song_library_asset", song_file, visible: false

    song_library_upload = SongLibraryUpload.first

    has_checkbox_for_song_library_upload = find("input[song_id='#{song_library_upload.id}']")
    expect(has_checkbox_for_song_library_upload).to be_truthy
  end
end
