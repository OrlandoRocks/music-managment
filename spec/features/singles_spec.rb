require "rails_helper"

feature "single", type: :feature do
  let(:person) { create(:person) }
  let(:single) { create(:finalized_single, person: person) }

  before(:each) do
    login_user(person, "Testpass123!")
  end

  describe "user can visit single pages" do
    it "/singles/:id/edit" do
      visit edit_single_path(single)
    end

    it "/single/show" do
      visit single_path(single)
      expect(page).to have_text single.name
    end

    it "/single/new" do
      visit new_single_path
    end

    it "/singles/:id/monetize_single" do
      visit monetize_single_path(single)
    end
  end

  describe "user creates a new singles release" do
    it "sets default language code on new album form for all country websites" do
      CountryWebsite.all.each do |cw|
        visit new_single_path(country_locale: cw.country)
        language_selected = find('#single_language_code_legacy_support').value
        expect(language_selected).to eq(I18n.locale.to_s[0..1])
      end
    end
  end
end
