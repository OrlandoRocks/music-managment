require "rails_helper"

# FIXME: 'The asset "apps/songs.js" is not present in the asset pipeline.
xdescribe "Updating a single with preorder", type: :feature do
  context "album has preorder with invalid start_date" do
    before(:each) do
      @password = "Testpass123!"
      @person = FactoryBot.create(:person)
      login_user(@person, @password)
      @single = FactoryBot.create(:single, person: @person)
      @song = FactoryBot.create(:song, album: @single)
      @preorder_purchase = FactoryBot.create(:preorder_purchase, album: @single)
      store = Store.find_by(short_name: "iTunesWW")
      FactoryBot.create(:salepointable_store, store: store, salepointable_type: "Single")
      @salepoint = FactoryBot.create(:salepoint, salepointable: @single, store: store, variable_price_id: store.variable_prices.first.id)
      @spd = FactoryBot.create(:salepoint_preorder_data, preorder_purchase: @preorder_purchase, salepoint: @salepoint)
    end

    context "the album has not been paid for" do
      it "should set the preorder start date to be nil" do
        expect(@spd.reload.start_date.nil?).to eq(false)
        visit single_path(@single)
        expect(@spd.reload.start_date.nil?).to eq(true)
      end
    end

    context "the album has been paid for" do
      it "should not change the preorder start date" do
        @single.update_attribute(:payment_applied, true)
        start_date = @spd.start_date
        visit single_path(@single)
        expect(@spd.reload.start_date == start_date).to eq(true)
      end
    end
  end
end
