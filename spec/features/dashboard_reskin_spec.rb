require "rails_helper"

feature "dashboard reskin" do
  before do
    allow(FeatureFlipper).to receive(:show_feature?).with(any_args).and_call_original
    allow(FeatureFlipper).to receive(:show_feature?).with(:reskin, any_args).and_return(true)
  end

  scenario "clicking View All Releases navigates to discography" do
    password = "Testpass123!"
    person = create(:person, password: password)
    create(:album, :purchaseable, name: "New Album", person: person)
    login_user(person, password)

    visit(dashboard_path)
    click_on "VIEW ALL RELEASES"

    expect(current_path).to eq(discography_path)
  end
end
