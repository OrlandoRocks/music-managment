require "rails_helper"

def validate_tags(page, metatags)
  metatags.each do |metatag|
    meta = "meta[name=\"#{metatag.name}\"][content=\"#{metatag.content}\"]"
    title_meta = "meta[name=\"title\"]"
    expect(page).to have_css meta, :visible => false
    expect(page).not_to have_css title_meta, :visible => false
  end
end

feature "MetaTags", type: :feature do
  it "/people/new displays meta tags" do
    metatags = MetaTag.where(page_name: "people#new", name: ["keywords", "description"], country_website_id: 1)
    visit "/people/new"
    validate_tags(page, metatags)
  end

  it "/people/new displays meta tags for german user" do
    metatags = MetaTag.where(page_name: "people#new", name: ["keywords", "description"], country_website_id: 5)
    visit "/signup?country_locale=DE"
    validate_tags(page, metatags)
  end

  it "/session/new displays meta tags" do
    metatags = MetaTag.where(page_name: "session#new", name: ["keywords", "description"], country_website_id: 1)
    visit "/session/new"
    validate_tags(page, metatags)
  end
end
