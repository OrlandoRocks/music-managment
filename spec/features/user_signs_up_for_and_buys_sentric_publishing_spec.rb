require "rails_helper"

feature "publishing" do
  xscenario "user signs up for and buys publishing", js: true do
    # TODO: Add in the fake Docusign step once we have a more modern headless
    # browser

    FeatureFlipper.add_feature "rights_app"
    FeatureFlipper.update_feature "rights_app", 100, ""

    allow(DocumentService).to receive(:get_docusign_view) do |args|
      composer = args[:composer]
      person = args[:user]

      person.person_balance.update(balance: 1000)

      legal_document = person.legal_documents.create(
        name: "Legal document",
        subject: composer,
        provider: "Docusign"
      )

      double(
        DocuSign_eSign::ViewUrl,
        url: "http://localhost:4567/iframe?composer_id=#{composer.id}&document_id=#{legal_document.id}"
      )
    end

    person = build(:person)

    create_new_account(person)

    # This route doesn't exist
    # visit new_publishing_administration_publishing_roles_path

    expect(page).to have_content "Publishing Administration"

    click_on "Next"

    expect(page).to have_content "Songwriter Details"

    fill_in "First Name", with: person.first_name
    fill_in "Last Name", with: person.last_name
    click_on "Next"

    expect(page).to have_content "Review & Verify"

    click_on "Next"

    expect(page).to have_content "Terms & Conditions"

    params = get_params_from_url(page.first("iframe")[:src])

    visit "#{publishing_administration_terms_and_conditions_path}" \
          "?event=signing_complete" \
          "&id=#{params[:composer_id]}" \
          "&legal_document_id=#{params[:document_id]}"

    expect(page).to have_content "Congratulations!"

    click_on "Continue to Shopping Cart"

    expect(page).to have_content "You have 1 item in your cart"

    click_on("Proceed To Checkout", match: :first)
    page.first("button", text: "PAY NOW").click

    expect(page).to have_content "You have successfully completed your purchase"

    visit publishing_administration_composers_path

    expect(page).not_to have_content "Pay Now"
    expect(page).to have_content "active"
  end

  def get_params_from_url(url)
    query = URI::Parser.new.parse(url).query
    params = CGI.parse(query)
    {
      composer_id: params["composer_id"].first,
      document_id: params["document_id"].first
    }
  end
end
