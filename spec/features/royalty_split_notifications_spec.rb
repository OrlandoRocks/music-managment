require "rails_helper"

feature "royalty split notifications", type: :feature do
  include ActionView::RecordIdentifier

  let(:current_user_password) { "MyPassword123!" }
  let(:current_user) { create :person, password: current_user_password }
  before(:each) do
    login_user(current_user, current_user_password)
    allow(FeatureFlipper).to receive(:show_feature?)
    allow(FeatureFlipper).to receive(:show_feature?).with(:royalty_splits, current_user).and_return(true)
  end

  describe "user notifications after being in a new split, even if split was set with email" do
    let!(:mah) { create :person }
    subject { page.find(".notification_items") }
    before do
      Notification.where(person: current_user).destroy_all
      @royalty_split = royalty_split_with_recipients(2, title: "MyNewSplit") do |split|
        recipient = split.not_owner_recipients.first
        recipient.person = nil
        recipient.email = current_user.email
      end
      visit notifications_path
    end
    it { expect(page).to have_css(".notification_items") }
    it { should have_text "You’ve Been Invited to Receive Royalties via Splits!" }
    it { should have_text "50.0% of \"MyNewSplit\"" }
    it { should have_link "View Invited Split", href: invited_royalty_splits_path(anchor: dom_id(@royalty_split)) }
  end

  describe "user notifications after split changed" do
    let!(:mah) { create :person }
    let!(:royalty_split) do 
      royalty_split_with_recipients(2, title: "MySplitUpdate") do |split|
        recipient = split.not_owner_recipients.first
        recipient.person = nil
        recipient.email = current_user.email
      end
    end
    subject { page.find(".notification_items") }
    before do
      Notification.where(person: current_user).destroy_all
      royalty_split.owner_recipient.percent = 10
      royalty_split.not_owner_recipients.first.percent = 90
      royalty_split.save!
      visit notifications_path
    end
    it { expect(page).to have_css(".notification_items") }
    it { should have_text "Your Split Percentage Has Been Updated" }
    it { should have_text "Your percentage has been updated from 50.0% to 90.0%." }
    it { should have_link "View Invited Split", href: invited_royalty_splits_path(anchor: dom_id(royalty_split)) }
  end

  describe "user notifications after removed from split" do
    let!(:mah) { create :person }
    let!(:royalty_split) do 
      royalty_split_with_recipients(2, title: "MyDeletedSplit") do |split|
        recipient = split.not_owner_recipients.first
        recipient.person = nil
        recipient.email = current_user.email
      end
    end
    let(:owner_recipient) { royalty_split.owner_recipient }
    let(:collaborator_recipient) { royalty_split.not_owner_recipients.first }
    subject { page.find(".notification_items") }
    before do
      Notification.where(person: current_user).destroy_all
      owner_recipient.percent = 100
      collaborator_recipient.destroy!
      royalty_split.recipients = [owner_recipient]
      royalty_split.save!
      visit notifications_path
    end
    it { expect(page).to have_css(".notification_items") }
    it { should have_text "You Have Been Removed from a Split" }
    it { should have_text "You are no longer receiving royalties from \"MyDeletedSplit\"." }
  end

  describe "user notifications after split destroyed" do
    let!(:mah) { create :person }
    let!(:royalty_split) do 
      create(:royalty_split, title: "MyDeletedSplit2") do |split|
        split.owner_recipient.percent = 0
        split.recipients << build(:royalty_split_recipient, royalty_split: split, percent: 30)
        split.recipients << build(:royalty_split_recipient, royalty_split: split, percent: 30)
        split.recipients << build(:royalty_split_recipient, royalty_split: split, percent: 40, person: current_user)
      end.tap(&:save!)
    end
    subject { page.find(".notification_items") }
    before do
      Notification.where(person: current_user).destroy_all
      royalty_split.destroy!
      visit notifications_path
    end
    it { expect(page).to have_css(".notification_items") }
    it { should have_text "You Have Been Removed from a Split" }
    it { should have_text "You are no longer receiving royalties from \"MyDeletedSplit2\"." }
  end
end

