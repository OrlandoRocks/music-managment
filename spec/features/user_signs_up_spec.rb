require "rails_helper"

feature "User signs up" do
  before(:each) do
    allow_any_instance_of(RecaptchaVerifiable).to receive(:recaptcha).and_return(true)
  end

  context "successfully" do
    scenario "no validation or other errors" do
      person = build(:person)

      expect do
        create_new_account(person)
      end.to change { LoginEvent.count }.by(1)

      expect(page).to have_content person.name
    end

    scenario "in the United States" do
      person = build(:person, country: "United States")

      create_new_account(person)
      load_edit_page(person)

      expect(page).to have_content "United States"
    end

    scenario "outside of the United States" do
      person = build(:person, country: "Canada")

      create_new_account(person)
      load_edit_page(person)

      expect(page).to have_content "Canada"
    end

    scenario "with uppercase email" do
      person = build(:person, email: "JSMITH@example.com")

      create_new_account(person)
      load_edit_page(person)

      expect(page).to have_content "jsmith@example.com"
    end
  end

  context "fails to sign up" do
    scenario "with an empty form" do
      visit signup_path
      click_on "Create My Free Account"

      expect(page).to have_content "Name can't be blank"
      expect(page).to have_content "Email can't be blank"
      expect(page).to have_content "Password can't be blank"
      expect(page).to have_content "Terms & Conditions must be accepted"
    end

    scenario "email already in use" do
      person = create(:person)

      create_new_account(person)

      expect(page).to have_content "There were errors creating your account"
      expect(page).to have_content "Email already in use"
    end

    scenario "with mismatching passwords" do
      password = "Password123!"
      wrong_password = "WrongPassword123!"
      person = build(:person, password: password, password_confirmation: password)

      create_new_account(person, password_confirmation: wrong_password)

      expect(page).to have_content "There were errors creating your account"
      expect(page).to have_content "Passwords do not match"
    end

    scenario "with incorrectly formatted password" do
      password = "password"
      person = build(:person, password: password, password_confirmation: password)

      create_new_account(person)

      expect(page).to have_content custom_t("activerecord.errors.models.person.attributes.password.password_requirements")
    end

    scenario "without accepting terms & conditions" do
      person = build(:person)

      create_new_account(person, check_terms: false)

      expect(page).to have_content "There were errors creating your account"
      expect(page).to have_content "Terms & Conditions must be accepted"
    end
  end

  context "with reskin" do
    before do
      allow(FeatureFlipper).to receive(:show_feature?).with(any_args).and_call_original
      allow(FeatureFlipper).to receive(:show_feature?).with(:reskin, any_args).and_return(true)
    end

    scenario "successfully" do
      email = "me@t.co"
      visit(signup_path)

      fill_in("Name", with: "It's me :)")
      fill_in("Email Address", with: email)
      fill_in("person_password", with: "Passw3rd!")
      fill_in("Confirm Password", with: "Passw3rd!")
      select("Ireland", from: "Country/Territory")
      check("I Agree to the TuneCore")
      click_button("Sign Up")

      expect(current_path).to eql(dashboard_create_account_path)
      expect(Person.find_by(email: email)).to be_present
    end

    scenario "successfully for India user" do
      email = "me@t.co"
      visit(signup_url(host: CountryWebsite::IN_DOMAIN))

      fill_in("Name", with: "It's me :)")
      fill_in("Email Address", with: email)
      fill_in("person_password", with: "Passw3rd!")
      fill_in("Confirm Password", with: "Passw3rd!")
      select("India", from: "Country/Territory")
      fill_in("person_mobile_number_prefix", with: "+91")
      fill_in("person_mobile_number_mobile_number", with: "1234567890")
      check("I Agree to the TuneCore")
      click_button("Sign Up")

      expect(current_path).to eql(dashboard_create_account_path)
      expect(Person.find_by(email: email)).to be_present
    end

    scenario "unsuccessfully with blank form" do
      visit(signup_path)
      click_button("Sign Up")
      expect(page).to have_content("There were errors creating your account.")
    end
  end
end
