require "rails_helper"

feature "account settings" do
  xscenario "user updates their address" do
    person = create(:person, :with_address)
    login_user(person, "Testpass123!")

    visit edit_person_path(person)

    expect(page).to have_text "Address"

    within "#account-settings-address-info" do
      click_on "edit"
      expect(page).to have_text "Update your address"

      expected_address = [
        person.address1,
        person.address2,
        person.city,
        person.state,
        person.postal_code,
        person.country,
      ].join(", ")

      expect(page).to have_text expected_address

      fill_in "Address", with: "123 Main St"
      fill_in "Address 2", with: "Apt B"
      fill_in "City", with: "Toronto"
      fill_in "State", with: "ON"
      fill_in "ZIP Code*", with: "10101"
      select "Canada", from: "Country*"
      fill_in "Current Password", with: "Testpass123!"

      click_on "Save changes"

      expect(page).to have_text "123 Main St, Apt B, Toronto, ON, 10101, Canada"
    end
  end
end
