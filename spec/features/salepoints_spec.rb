require "rails_helper"

feature "album", type: :feature do

  let(:person) { FactoryBot.create(:person) }
  let(:album) { FactoryBot.create(:album, person: person) }

  before(:each) do
    login_user(person, "Testpass123!")
    allow(FeatureFlipper).to receive(:show_feature?)
    allow(FeatureFlipper).to receive(:show_feature?).with(:discovery_platforms, person).and_return(true)
  end

  scenario "/albums/:album_id/salepoints" do
    visit album_salepoints_path(album)
  end
end
