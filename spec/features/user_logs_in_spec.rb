require "rails_helper"

feature "user logs in" do
  context "successfully" do
    scenario "with the correct email and password" do
      password = "Testpass123!"
      person = create(:person, password: password, password_confirmation: password)

      expect do
        login_user(person, password)
      end.to change { LoginEvent.count }.by(1)

      expect(page).to have_content person.name
    end
  end

  context "user fails to login" do
    scenario "with the wrong email" do
      email = "testuser@tunecore.com"
      password = "Testpass123!"
      person = create(:person, email: email, password: password, password_confirmation: password)
      wrong_email = "wronguser@tunecore.com"

      visit login_path
      fill_in "Email", with: wrong_email
      fill_in "Password", with: person.password
      click_button "Log In"

      expect(page).to have_content "Email or password entered incorrectly."
    end

    scenario "with the wrong password" do
      password = "Testpass123!"
      person = create(:person, password: password, password_confirmation: password)
      wrong_password = "Testpass1234!"

      visit login_path
      fill_in "Email", with: person.email
      fill_in "Password", with: wrong_password
      click_button "Log In"

      expect(page).to have_content "Email or password entered incorrectly."
    end
  end

  context "with the reskin enabled" do
    before do
      allow(FeatureFlipper).to receive(:show_feature?).with(any_args).and_call_original
      allow(FeatureFlipper).to receive(:show_feature?).with(:reskin, any_args).and_return(true)
    end

    context "successfully" do
      scenario "with the correct email and password" do
        password = "Testpass123!"
        person = create(:person, password: password, password_confirmation: password)

        expect do
          login_user(person, password)
        end.to change { LoginEvent.count }.by(1)

        expect(current_path).to include(dashboard_path)
      end

      scenario "with TFA enabled" do
        password = "Testpass123!"
        person = create(:person, :with_two_factor_auth, password: password)
        allow(FeatureFlipper).to receive(:show_feature?).with(:verify_2fa, person).and_return(true)
        allow_any_instance_of(TwoFactorAuth::ApiClient).to receive(:log).and_return(true)
        allow_any_instance_of(TwoFactorAuth::ApiClient)
          .to receive(:submit_auth_code)
          .and_return(double(successful?: true))

        login_user(person, password)
        fill_in("Enter Code", with: "123456")
        click_button("Verify")

        expect(current_path).to include(dashboard_path)
      end
    end

    context "user fails to login" do
      scenario "with the wrong email" do
        email = "testuser@tunecore.com"
        password = "Testpass123!"
        person = create(:person, email: email, password: password, password_confirmation: password)
        wrong_email = "wronguser@tunecore.com"

        visit login_path
        fill_in "Email", with: wrong_email
        fill_in "Password", with: person.password
        click_button "Log In"

        expect(page).to have_content "Email or password entered incorrectly."
      end

      scenario "with the wrong password" do
        password = "Testpass123!"
        person = create(:person, password: password, password_confirmation: password)
        wrong_password = "Testpass1234!"

        visit login_path
        fill_in "Email", with: person.email
        fill_in "Password", with: wrong_password
        click_button "Log In"

        expect(page).to have_content "Email or password entered incorrectly."
      end

      scenario "with the right email/password but wrong TFA code" do
        password = "Testpass123!"
        person = create(:person, :with_two_factor_auth, password: password)
        allow(FeatureFlipper).to receive(:show_feature?).with(:verify_2fa, person).and_return(true)
        allow_any_instance_of(TwoFactorAuth::ApiClient).to receive(:log).and_return(true)
        allow_any_instance_of(TwoFactorAuth::ApiClient)
          .to receive(:submit_auth_code)
          .and_return(double(successful?: false, error_message: "nope"))

        login_user(person, password)
        fill_in("Enter Code", with: "123456")
        click_button("Verify")

        expect(current_path).to include(new_two_factor_auth_sessions_path)
        find("[data-testid=flash]", text: "Wrong Code. Try Again.")
      end
    end
  end
end
