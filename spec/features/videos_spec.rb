require "rails_helper"
feature "videos", type: :feature do
  let(:person) do
    FactoryBot.create(
      :person,
      email: "testuser@tunecore.com",
    )
  end

  let(:video) { FactoryBot.create(:video, person: person) }

  scenario "User visits /videography (videos#index)" do
    login_user(person, "Testpass123!")
    @virtual_path = "videos.index"
    visit videography_path
    expect(page).to have_text custom_t('.my_videos')
    logout_user
  end

  scenario "User visits /videos/:id (videos#show)" do
    login_user(person, "Testpass123!")
    @virtual_path = "videos.index"
    visit video_path(video)
    logout_user
  end
end
