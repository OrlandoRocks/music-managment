require "rails_helper"

feature "person signup" do
  context "user views the sign up page" do
    context "includes referral_data from cookies" do
      ReferralDatum.allowed_parameters.each do |param_key|
        scenario param_key.to_s do
          set_browser_cookie("tunecore_wp", param_key.to_s => "TestData", tc_timestamp: 1_458_752_702)
          visit signup_path

          key = page.find_by_id("person_referral_data_attributes_0_key", visible: false).value
          value = page.find_by_id("person_referral_data_attributes_0_value", visible: false).value
          page_value = page.find_by_id("person_referral_data_attributes_0_page", visible: false).value
          source_value = page.find_by_id("person_referral_data_attributes_0_source", visible: false).value

          expect(key).to eq param_key.to_s
          expect(value).to eq "TestData"
          expect(page_value).to eq "signup"
          expect(source_value).to eq "cookie"
        end
      end
    end

    scenario "doesnt includes referral_data from cookie params that aren't whitlisted" do
      set_browser_cookie("tunecore_wp", "not_whitelisted" => "TestData", tc_timestamp: 1_458_752_702)
      visit signup_path

      page.assert_no_selector("#person_referral_data_attributes_0_key")
      page.assert_no_selector("#person_referral_data_attributes_0_value")
      page.assert_no_selector("#person_referral_data_attributes_0_page")
      page.assert_no_selector("#person_referral_data_attributes_0_source")
    end

    context "includes referral_data from url params" do
      ReferralDatum.allowed_parameters.each do |param_key|
        scenario param_key.to_s do
          visit "/signup?#{param_key}=TestSource"

          key = page.find_by_id("person_referral_data_attributes_0_key", visible: false).value
          value = page.find_by_id("person_referral_data_attributes_0_value", visible: false).value
          page_value = page.find_by_id("person_referral_data_attributes_0_page", visible: false).value
          source_value = page.find_by_id("person_referral_data_attributes_0_source", visible: false).value

          expect(key).to eq param_key.to_s
          expect(value).to eq "TestSource"
          expect(page_value).to eq "signup"
          expect(source_value).to eq "url"
        end
      end
    end

    scenario "doesnt includes referral_data from url params that aren't whitlisted" do
      visit "/signup?garbage=TestSource"

      page.assert_no_selector("#person_referral_data_attributes_0_key")
      page.assert_no_selector("#person_referral_data_attributes_0_value")
      page.assert_no_selector("#person_referral_data_attributes_0_page")
      page.assert_no_selector("#person_referral_data_attributes_0_source")
    end
  end
end
