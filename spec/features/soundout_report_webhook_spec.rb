require "rails_helper"

feature "soundout report webhook", js: true do
  scenario "soundout sends a POST response to complete order" do
    with_remote_model_cleanup(SoundoutReport) do
      allow(Soundout::FetchWorker).to receive(:perform_async)
      soundout_id = "SOUNDOUT_ID"
      report = remote_create(
        :soundout_report,
        :with_track,
        status: "requested",
        soundout_id: soundout_id
      )

      credentials = ActionController::HttpAuthentication::Basic.encode_credentials(
        SOUNDOUT_CONFIG['TC_API_AUTH_ID'],
        SOUNDOUT_CONFIG["TC_API_PASSWORD"]
      )

      hostname = "#{Capybara.current_session.server.host}:#{Capybara.current_session.server.port}"

      response = HTTParty.post(
        "http://#{hostname}/fanreviews/available",
        body: { "id" => soundout_id },
        headers: { "Authorization" => credentials }
      )

      expect(response.code).to eq(200)
      expect(Soundout::FetchWorker).to have_received(:perform_async).with(report.id)
    end
  end

  private

  def with_remote_model_cleanup(model, &block)
    remote_destroy_all model

    block.call

    remote_destroy_all model
  end
end
