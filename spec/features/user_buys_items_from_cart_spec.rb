require "rails_helper"

feature "checkout from cart" do
  scenario "removes purchases for albums that have been deleted from the cart" do
    password = "Password123!"
    person = create(:person, password: password, password_confirmation: password)
    album = create(:album, :purchaseable, number_of_songs: 2, person: person)
    deleted_album = create(:album, :purchaseable, :is_deleted, number_of_songs: 2, person: person)
    _purchase = create(:purchase, related: album, person: person, paid_at: nil)
    _deleted_purchase = create(:purchase, related: deleted_album, person: person, paid_at: nil)

    login_user(person, password)
    visit cart_path

    expect(page).to have_content "You have 1 item in your cart"
    expect(page).to have_content "We removed the album #{deleted_album.name} from your cart because it was deleted."
  end

  scenario "pays for many items with balance" do
    password = "Password123!"
    person = create(:person, password: password, password_confirmation: password)
    product = create(:product)
    albums = create_list(
      :album,
      2,
      :purchaseable,
      number_of_songs: 2,
      person: person
    )
    albums.each do |album|
      Product.add_to_cart(person, album, product)
    end
    balance = 800
    person.person_balance.update_attribute :balance, balance

    login_user(person, password)
    visit cart_path
    expect(page).to have_content "You have 2 items in your cart."
    expect(page).to have_content albums.first.name
    expect(page).to have_content albums.second.name

    click_on("Proceed To Checkout", match: :first)

    expect(page).to have_content "You have $#{balance}.00 in your account."

    click_on("Pay Now", match: :first)

    expect(page).to have_content custom_t("invoices.thanks.congrats_successfully_submitted")
  end

  scenario "pays for many items with credit card" do
    password = "Password123!"
    person = create(:person, password: password, password_confirmation: password)
    create(:stored_credit_card, bt_token: "1yhsgd", person: person, last_four: "1234")
    last_four = person.stored_credit_cards.first.last_four
    product = create(:product)
    albums = create_list(
      :album,
      2,
      :purchaseable,
      number_of_songs: 2,
      person: person
    )
    albums.each do |album|
      Product.add_to_cart(person, album, product)
    end
    mock_braintree_blue_purchase

    login_user(person, password)
    visit cart_path

    expect(page).to have_content "You have 2 items in your cart."

    click_on("Proceed To Checkout", match: :first)

    first(".cc_select").click
    billed_to_div = first(".billed_to")
    expect(billed_to_div)
      .to have_content "To be billed to credit card:\n************#{last_four}"

    click_on("Pay Now", match: :first)

    expect(page)
      .to have_content custom_t("invoices.thanks.congrats_successfully_submitted")
  end

  scenario "pays for many items with paypal" do
    password = "Password123!"
    person = create(:person, :with_stored_paypal_account, password: password, password_confirmation: password)
    product = create(:product)
    albums = create_list(
      :album,
      2,
      :purchaseable,
      number_of_songs: 2,
      person: person
    )
    albums.each do |album|
      Product.add_to_cart(person, album, product)
    end
    mock_paypal_transaction("Completed", "AMT" => "20.00")

    login_user(person, password)
    visit cart_path

    Capybara.using_wait_time(60) do
      expect(page).to have_content "You have 2 items in your cart."
    end

    click_on("Proceed To Checkout", match: :first)

    Capybara.using_wait_time(60) do
      expect(page).to have_content "To be billed to PayPal Account"
    end

    click_on("Pay Now", match: :first)
    Capybara.using_wait_time(60) do
      expect(page).to have_content custom_t("invoices.thanks.congrats_successfully_submitted")
    end
  end
end
