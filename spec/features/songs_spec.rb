require "rails_helper"

feature "album", type: :feature do
  let(:person) { FactoryBot.create(:person) }

  let(:album) {
    FactoryBot.create(
      :album,
      :with_uploaded_songs_and_artwork,
       number_of_songs: 10,
       person: person
    )
  }

  before(:each) do
    login_user(person, "Testpass123!")
  end

  scenario "/albums/:album_id/songs" do
    visit album_songs_path(album)
  end

  scenario "/albums/:album_id/songs/new" do
    visit new_album_song_path(album)
  end

  scenario "/albums/:album_id/songs/:id/edit" do
    visit edit_album_song_path(album, album.songs.first)
  end
end
