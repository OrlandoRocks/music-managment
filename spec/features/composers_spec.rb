require "rails_helper"

feature "composer", type: :feature do
  let(:composer) { FactoryBot.create(:composer) }

  it "/composer/index" do
    visit composer_path(composer)
  end

  it "/composer/new" do
    visit new_composer_path(composer)
  end

  it "/composer/edit" do
    visit edit_composer_path(composer)
  end

  it "/song_registrations/index" do
    visit compositions_path
  end
end
