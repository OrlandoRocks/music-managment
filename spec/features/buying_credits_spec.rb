# encoding: UTF-8
require "rails_helper"

describe "Purchase credits", type: :feature do
  let(:password) { "Testpass123!" }

  it "purchases all credit products for USD with balance" do
    person = create(:person, :with_balance, amount: 50_000, email: "high_roller_USD@tunecore.com")
    login_user(person, password)
    products = Product.where(country_website_id: person.country_website_id, status: "Active", product_type: "Package")
    products.each do |p|
      buy_a_credit_with_balance_and_validate_purchase(person, p)
    end
    logout_user
  end

  it "purchases a credit product for each country" do
    countries = CountryWebsite.all

    countries.each do |country|
      if !country.country = 'DE'
        country.country = "GB" if country.country == "UK"
        person = FactoryBot.create(:person, :with_balance, amount: 50_000, email: "high_roller_#{country.id}@tunecore.com", country: Country.find_by(iso_code: country.country).name)
        product = Product.where(name: "Album Distribution Credit", status: "Active", currency: country.currency).first
        login_user(person, password)
        buy_a_credit_with_balance_and_validate_purchase(person, product)
        logout_user
      end
    end
  end


  def buy_a_credit_with_balance_and_validate_purchase(person, product)
    visit "/credits"
    balance_cents = (person.person_balance.balance * 100).to_i
    expect(page).to have_content product.display_name
    find("//a[@href='/products/#{product.id}/add_to_cart']").click
    expect(page).to have_content product.name
    expect(page).to have_content product.description_safely_translated
    purchase = person.purchases.last
    expect(purchase.product_id).to eq(product.id)
    expect(purchase.invoice_id).to be_nil
    first("//a[@href='/cart/confirm_and_pay']").click
    expect(page).to have_content "Clicking the “Pay Now” button will complete your order and charge your TuneCore balance"
    page.driver.post("/cart/finalize", use_balance: "1") # lands on a redirect page due to MockResponse need separate test for invoice/thanks. Verify purchase complete through object
    #page.driver.post("/cart/finalize", payment_id: person.stored_credit_cards.first.id) # having trouble executing cc purchase, keep trying
    expect(purchase.reload.invoice_id?).to be true
    expected_new_balance = balance_cents - purchase.cost_cents
    expect((person.reload.person_balance.balance * 100).to_i).to eq expected_new_balance.to_i
  end

end
