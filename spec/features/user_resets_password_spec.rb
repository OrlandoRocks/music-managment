require "rails_helper"

feature "password reset" do
  scenario "user successfully resets password" do
    person = create(:person)
    password = "Password123!"
    url = PasswordResetService.new(person.email).fallback_url

    visit url
    fill_in("New Password", with: password)
    fill_in("Confirm", with: password)
    click_on("Change Password")

    expect(page).to have_content(I18n.t("controllers.reset_passwords.your_password_has_been_updated"))
  end

  scenario "passwords do not meet requirements" do
    person = create(:person)
    password = "password123"
    url = PasswordResetService.new(person.email).fallback_url

    visit url
    fill_in("New Password", with: password)
    fill_in("Confirm", with: password)
    click_on("Change Password")

    expect(page).to have_content(I18n.t("activerecord.errors.models.person.attributes.password.password_requirements"), count: 1)
  end

  scenario "user passwords don't match" do
    person = create(:person)
    password = "Password123!"
    password_confirmation = "DifferentPassword123!"
    url = PasswordResetService.new(person.email).fallback_url

    visit url
    fill_in("New Password", with: password)
    fill_in("Confirm", with: password_confirmation)
    click_on("Change Password")

    expect(page).to have_content(I18n.t("models.person.passwords_do_not_match"), count: 1)
  end

  scenario "user doesn't enter a password confirmation" do
    person = create(:person)
    password = "Password123!"
    url = PasswordResetService.new(person.email).fallback_url

    visit url
    fill_in("New Password", with: password)
    click_on("Change Password")

    expect(page).to have_content(I18n.t("errors.messages.blank"), count: 1)
  end

  context "with reskin" do
    before do
      allow(FeatureFlipper).to receive(:show_feature?).with(any_args).and_call_original
      allow(FeatureFlipper).to receive(:show_feature?).with(:reskin, any_args).and_return(true)
    end

    scenario "user successfully resets password" do
      person = create(:person)
      password = "Password123!"
      url = PasswordResetService.new(person.email).fallback_url

      visit url
      fill_in("New Password", with: password)
      fill_in("Confirm Password", with: password)
      click_on("Reset Password")

      expect(current_path).to eql(dashboard_path)
      expect(page).to have_content(I18n.t("controllers.reset_passwords.your_password_has_been_updated"))
    end

    scenario "passwords do not meet requirements" do
      person = create(:person)
      password = "password123"
      url = PasswordResetService.new(person.email).fallback_url

      visit url
      fill_in("New Password", with: password)
      fill_in("Confirm Password", with: password)
      click_on("Reset Password")

      expect(page).to have_content(I18n.t("activerecord.errors.models.person.attributes.password.password_requirements"), count: 1)
    end

    scenario "user passwords don't match" do
      person = create(:person)
      password = "Password123!"
      password_confirmation = "DifferentPassword123!"
      url = PasswordResetService.new(person.email).fallback_url

      visit url
      fill_in("New Password", with: password)
      fill_in("Confirm Password", with: password_confirmation)
      click_on("Reset Password")

      expect(page).to have_content(I18n.t("models.person.passwords_do_not_match"), count: 1)
    end

    scenario "user doesn't enter a password confirmation" do
      person = create(:person)
      password = "Password123!"
      url = PasswordResetService.new(person.email).fallback_url

      visit url
      fill_in("New Password", with: password)
      click_on("Reset Password")

      expect(page).to have_content(I18n.t("errors.messages.blank"), count: 1)
    end
  end
end
