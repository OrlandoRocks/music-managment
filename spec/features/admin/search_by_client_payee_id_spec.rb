require 'rails_helper'

describe "Search by Client Payee ID" do

  it "shows a page with the person that has that client payee id" do
    admin = create(:person, :admin)
    login_user(admin, "Testpass123!")
    customer = create(:person)
    payout_provider = create(:payout_provider, :approved, person: customer)

    visit admin_dashboard_index_path
    select "Client Payee ID", from: 'query_type'

    fill_in "query_term", with: payout_provider.client_payee_id

    click_on "search_everything"

    expect(page).to have_text(customer.name)
  end
end
