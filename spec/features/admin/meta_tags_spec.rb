require "rails_helper"

describe "Meta Tag Admin Interface", type: :feature do
  let(:admin)    { Person.find_by_name!("Super Admin") }
  let(:test_host){ COUNTRY_URLS["US"] }

  before(:each) do
    login_user(admin, "Testpass123!")
    visit "/admin"
    click_link "Meta Tags"
  end

  context "Adding New Meta Tag" do
    it "submits successfully" do
      click_link "New Meta Tag"
      select "description"
      fill_in "Content", with: "My Awesome new description text"
      select "TuneCore"
      click_button "Create Meta tag"

      expect(page.current_url).to eq(admin_meta_tags_url(host: test_host))
    end

    it "errors if form fields are blank" do
      click_link "New Meta Tag"
      click_button "Create Meta tag"
      expect(page).to have_content("Please review the problems below")
    end
  end

  context "Editing New Meta Tag" do
    it "submits successfully" do
      first(:link, "Edit").click
      fill_in "Content", with: "My Awesome new text"
      click_button "Update Meta tag"

      expect(page.current_url).to eq(admin_meta_tags_url(host: test_host))
    end
  end
end
