require "rails_helper"

describe "Mass Takedown Un-Do Takedowns Admin Interface", type: :feature do
  let(:admin)     { Person.find_by_name!("Super Admin") }
  let(:store)     { Store.mass_takedown_stores.first }
  let(:person)    { FactoryBot.create(:person) }
  let(:album)     { FactoryBot.create(
                      :album,
                      :finalized,
                      :with_salepoints,
                      :with_uploaded_songs_and_artwork,
                       number_of_songs: 2,
                       person: person)
                  }
  let(:salepoints){ album.salepoints }

  let(:album_upc) { album.upcs.first.number}

  let(:song_isrc) { album.songs.first.tunecore_isrc }

  before(:each) do
    login_user(admin, "Testpass123!")
    visit "/admin"
    click_link "Mass un-takedown tool"

    bulk_adder = BulkAddStoresByAlbums.new({store_id: store.id, album_ids_or_upcs: "#{album.id}"})
    bulk_adder.save
    salepoints.each do |sp| 
      sp.update(takedown_at: Time.now)
      distribution = create(
        :distribution, 
        delivery_type: "metadata_only", 
        last_delivery_type: "metadata_only"
      )
      distribution.salepoints << sp
    end
  end

  describe "Un-doing takedowns for given albums for selected stores" do
    context "success" do
      before(:each) do
        allow_any_instance_of(MassTakedown).to receive(:salepoints_to_un_takedown).and_return(album.salepoints)
        allow_any_instance_of(Salepoint).to receive(:redeliver_untakedown).and_return(true)
      end

      context "when given album ID" do
        it "removes the takedown" do
          check store.name
          fill_in "album_ids", with: "#{album.id}"
          click_button "mass_untakedown_submit"
          expect(page.current_url).to match(/\/admin\/petri\/mass_untakedown/)
          expect(page).to have_content("Succesfully undid takedowns for albums: #{album_upc}")
        end
      end

      context "when given album UPC" do
        it "removes the take down" do
          check store.name
          fill_in "album_ids", with: "#{album_upc}"
          click_button "mass_untakedown_submit"
          expect(page.current_url).to match(/\/admin\/petri\/mass_untakedown/)
          expect(page).to have_content("Succesfully undid takedowns for albums: #{album_upc}")
        end
      end

      context "when given a song isrc" do
        it "removes the take down" do
          check store.name
          fill_in "album_ids", with: "#{song_isrc}"
          click_button "mass_untakedown_submit"
          expect(page.current_url).to match(/\/admin\/petri\/mass_untakedown/)
          expect(page).to have_content("Succesfully undid takedowns for albums: #{album_upc}")
        end
      end
    end

    context "failure" do
      before do 
        allow_any_instance_of(Salepoint).to receive(:redeliver_untakedown).and_return(false)
      end 
      
      context "when the album ID/UPC does not exist" do
        it "displays error message" do
          check store.name
          fill_in "album_ids", with: "12blah"
          click_button "mass_untakedown_submit"
          expect(page.current_url).to match(/\/admin\/petri\/mass_untakedown/)
          expect(page).to have_content("Album with ID of 12blah or UPC of 12blah or song ISRC of 12blah not found.")
        end
      end

      context "when takedown can not be removed" do
        before do
          bulk_adder = BulkAddStoresByAlbums.new({store_id: store.id, album_ids_or_upcs: "#{album.id}"})
          bulk_adder.save
        end

        it "displays error message" do
          check store.name
          fill_in "album_ids", with: "#{album.id}"
          click_button "mass_untakedown_submit"
          expect(page.current_url).to match(/\/admin\/petri\/mass_untakedown/)
          expect(page).to have_content("Unable to undo takedown for album ID: #{album.id}")
        end
      end
    end
  end

  describe "#mass_untakedown" do
    context "when undoing takedowns for a release" do 
      it "removes the takedown from the salepoint" do
        expect_any_instance_of(Salepoint).to receive(:remove_takedown).once.and_call_original
        MassTakedown.mass_untakedown([album.id], [store.id], person)
        expect(album.salepoints.find_by(store_id: store.id).taken_down?).to eq(false)
      end

      it "retries the distribution'" do        
        expect_any_instance_of(Distribution).to receive(:retry)
        .once
        .with(
          {
            actor: "Salepoint#redeliver_untakedown",
            message: "issuing a reactivation distribution"   
          }
        )
        MassTakedown.mass_untakedown([album.id], [store.id], person)
      end

      it "sets the distribution as a 'full_delivery'" do        
        MassTakedown.mass_untakedown([album.id], [store.id], person)
        redelivered_distribution = album.salepoints.find_by(store_id: store.id).distributions.first
        expect(redelivered_distribution.delivery_type).to eq("full_delivery")
      end
    end 
  end 
end
