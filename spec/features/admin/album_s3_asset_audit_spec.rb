require "rails_helper"

describe "Auditing an album's S3 assets", type: :feature do
  scenario "with good album art dimensions" do
    album = create(:album)
    create(:artwork, album: album, width: 1600, height: 1601)

    login_as_admin
    visit audit_s3_assets_admin_album_path(album)

    expect(page).to have_notice("Dimension 1600x1601")
  end

  scenario "with album art dimensions that are too small" do
    album = create(:album)
    create(:artwork, album: album, width: 1598, height: 1599)

    login_as_admin
    visit audit_s3_assets_admin_album_path(album)

    expect(page).to have_error("Dimension 1598x1599")
  end

  scenario "with no album art" do
    album = create(:album)

    login_as_admin
    visit audit_s3_assets_admin_album_path(album)

    expect(page).not_to have_content("Dimension ")
  end

  scenario "with missing album art dimensions" do
    album = create(:album)
    create(:artwork, album: album, width: nil, height: nil)

    login_as_admin
    visit audit_s3_assets_admin_album_path(album)

    expect(page).to have_error("Dimension unknown")
  end

  def login_as_admin
    password = "Password123!"
    admin = create(:person, :admin, password: password)
    login_user(admin, password)
  end

  def have_notice(text)
    have_selector(".notice", text: text)
  end

  def have_error(text)
    have_selector(".error", text: text)
  end
end
