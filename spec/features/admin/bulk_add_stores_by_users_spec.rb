require "rails_helper"
require_relative "../../support/shared_examples/bulk_album_adder_by_users"

describe "Bulk Add Store by Users Admin Interface", type: :feature do
  let(:admin)    { Person.find_by_name!("Super Admin") }
  let(:store)    { Store.find_by(short_name: "Pandora") }
  let(:person1) { FactoryBot.create(:person, :with_live_album) }
  let(:person2) { FactoryBot.create(:person, :with_live_album) }
  let(:album1) { person1.albums.first }
  let(:album2) { person2.albums.first }

  before(:each) do
    login_user(admin, "Testpass123!")
    visit "/admin"
    click_link "Bulk add stores by users"
  end

  context "Adding New Store For Users" do
    context "by user IDs" do
      context "with comma separated User ID values" do
        it_behaves_like "bulk_album_adder_by_users", {fill_in_form_with: "comma_sep_users"}
      end

      context "with space separated user ID values" do
        it_behaves_like "bulk_album_adder_by_users", {fill_in_form_with: "space_sep_users"}
      end

      context "when one of a user's albums already has selected store" do
        it "does not create a new purchase and does show an error message" do
          create(:salepoint, salepointable: album1, store: store)
          select store.name
          fill_in "User ids", with: "#{person1.id}"
          click_button "add store"
          expect(page.current_url).to match(/\/admin\/bulk_add_stores_by_users\/new/)
          expect(page).to have_content("Album #{album1.id}-#{album1.name} already has store #{store.id}")
        end
      end
    end
  end
end
