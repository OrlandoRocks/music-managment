require "rails_helper"

describe 'Updating refunded_at for mastered_tracks', type: :feature do
  let(:admin){ Person.find_by_name!("Super Admin") }

  before(:each){
    login_user(admin, "Testpass123!")
    @customer = FactoryBot.create(:person)
  }

  context "on the mastered_tracks page for a user with a mastered_track" do
    before(:each) do
      @mastered_track = FactoryBot.create(:mastered_track, person: @customer)
    end

    it "does not display tracks that are not paid for" do
      visit admin_person_mastered_tracks_path(@customer)
      expect(page).to have_no_content(@mastered_track.name)
    end

    context "when the customer has a paid_mastered_track" do
      before(:each) do
        @mastered_track.update_attribute(:paid_at, Time.now)
      end

      it "displays the track" do
        visit admin_person_mastered_tracks_path(@customer)
        expect(page).to have_content(@mastered_track.name)
      end

      it "has a link to mark the track as refunded" do
        visit admin_person_mastered_tracks_path(@customer)
        expect(page).to have_link("Mark as Refunded")
      end

      it "when clicking the link the track is marked as refunded" do
        visit admin_person_mastered_tracks_path(@customer)
        expect(@mastered_track.reload.refunded_at.blank?).to be true
        click_link("Mark as Refunded")
        expect(@mastered_track.reload.refunded_at.present?).to be true
      end

      it "has a link to unmark the track as refunded if the track is refunded" do
        @mastered_track.update_attribute(:refunded_at, Time.now)
        visit admin_person_mastered_tracks_path(@customer)
        expect(page).to have_link("Unmark Refund")
      end

      it "when clicking the link to Unmark Refund the redunded_at date should be blanked out" do
        @mastered_track.update_attribute(:refunded_at, Time.now)
        visit admin_person_mastered_tracks_path(@customer)
        expect(@mastered_track.reload.refunded_at.present?).to be true
        click_link("Unmark Refund")
        expect(@mastered_track.reload.refunded_at.blank?).to be true
      end
    end
  end
end
