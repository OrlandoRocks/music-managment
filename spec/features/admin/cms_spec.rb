require "rails_helper"

describe "Adding Promo CMS", type: :feature do
  let(:admin)    { Person.find_by_name!("Super Admin") }
  let(:customer) { FactoryBot.create(:person) }
  let(:test_host){ COUNTRY_URLS["US"] }

  before(:each) do
    login_user(admin, "Testpass123!")
  end

  context "Dashboard Promo for a specific country" do
    it "submits successfully" do
      visit new_admin_cm_path(type: "dashboard")

      fill_in "Callout Name", with: "Test Callout"
      check "country_languages_United_States"
      check "country_languages_United_Kingdom"
      check "country_languages_Australia"
      attach_file "Dashboard Banner", "#{Rails.root}/public/images/fan_reviews/featureimg.png"
      click_button "Submit"

      expect(page.current_url).to eq(admin_cms_url host: test_host)
    end
  end

  context "Interstitial Promo for a specific country" do
    it "submits successfully" do
      visit new_admin_cm_path(type: "interstitial")

      fill_in "Callout Name", with: "Test Interstitial Callout"
      check "country_languages_United_States"
      check "country_languages_United_Kingdom"
      check "country_languages_Australia"
      find("#asset_logo_color_black").set(true)
      attach_file "Banner", "#{Rails.root}/public/images/fan_reviews/featureimg.png"
      click_button "Submit"

      expect(page.current_url).to eq(admin_cms_url host: test_host)
    end
  end

  context "Login Promo for a specific country" do
    it "submits successfully" do
      visit new_admin_cm_path(type: "login")

      fill_in "Callout Name", with: "Test Interstitial Callout"
      check "country_languages_United_States"
      check "country_languages_United_Kingdom"
      check "country_languages_Australia"
      attach_file "Login Sticker", "#{Rails.root}/public/images/fan_reviews/featureimg.png"
      click_button "Submit"

      expect(page.current_url).to eq(admin_cms_url host: test_host)
    end
  end
end
