require "rails_helper"

feature 'Admin YouTube Monetization' do
  scenario "termination" do
    admin = create(:person, :admin, :with_role, role_name: "YTM Approver")
    login_user(admin, 'Testpass123!')
    customer = create(:person, :with_ytsr, name: "O'Malley")

    visit admin_person_path(customer)
    click_on "Disable YouTube Sound Recording Revenue"
    fill_in "Note*", with: "Terminated"
    click_on "Save or Confirm"

    expect(page).to have_text("YTM Disabled for #{customer.name}")

    visit admin_person_path(customer)

    expect(page).to have_text("YouTube Sound Recording Revenue Terminated")
    expect(page).to have_text("Terminated")
  end
end
