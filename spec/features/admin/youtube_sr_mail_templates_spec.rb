require "rails_helper"

describe "Youtube SR Mail Templates" do
  let(:password) { "Password123!" }
  let(:developer) {
    create(:person,
           password: password,
           password_confirmation: password).tap do |person|
    person.roles << Role.all
  end
  }

  let(:admin) { Person.find_by(name: "Super Admin") }

  context "as Developer" do
    before do
      admin.roles << Role.find_by(name: "YTM Approver")
      login_user(developer, "Password123!")
    end

    it "creates a new Youtube SR Mail Template" do
      visit "/admin"
      click_link "YouTube Content ID Monetization Email Templates"
      click_button "Create Youtube Song Recording Email Template"
      fill_in "youtube_sr_mail_template_select_label", with: "Removed for being too cool"
      fill_in "Body:", with: '@message_data.each { |message| "<p>#{message[:custom_field]["1"]["third_party"]} thinks your song is too cool.</p>"'
      fill_in "Custom Fields:", with: "third_party"
      click_button "Create"

      expect(page).to have_text("Removed for being too cool")
      expect(page).not_to have_text("Needs Tech Team Support")
    end
  end

  context "as non-Developer Admin" do
    before do
      admin.roles << Role.find_by(name: "YTM Approver")
      login_user(admin, "Testpass123!")
    end

    it "displays a list of all the Youtube SR Mail Templates" do
      visit "/admin"
      click_link "YouTube Content ID Monetization Email Templates"
      expect(page).to have_content(YoutubeSrMailTemplate.first.select_label)
    end

    it "creates a new Youtube SR Mail Template in an unfinshed state without the body" do
      visit "/admin"
      click_link "YouTube Content ID Monetization Email Templates"
      click_button "Create Youtube Song Recording Email Template"
      fill_in "youtube_sr_mail_template_select_label", with: "Removed for being too cool"
      fill_in "youtube_sr_mail_template_subject", with: "Removed for being way too cool"
      expect(page).not_to have_css("youtube_sr_mail_template_body")
      click_button "Create"

      expect(page).to have_text("Removed for being too cool")
      expect(page).to have_text("Needs Tech Team Support")
    end

    it "updates an existing Youtube SR Mail Template" do
      youtube_sr_mail_template = YoutubeSrMailTemplate.return_template("removed_monetization")

      visit "/admin"
      click_link "YouTube Content ID Monetization Email Templates"

      within "#youtube_sr_mail_template_#{youtube_sr_mail_template.id}" do
        click_link "Edit"
      end

      header = "Unfortunately <%= @person_name %>, someone has complained that your Youtube Song is too cool so we have to remove it"
      fill_in "Header:", with: header
      click_button "Update"

      expect(youtube_sr_mail_template.reload.header).to eq(header)
    end

    it "deletes an existing Youtube SR Mail Template" do
      youtube_sr_mail_template = create(:youtube_sr_mail_template)

      visit "/admin"
      click_link "YouTube Content ID Monetization Email Templates"

      within "#youtube_sr_mail_template_#{youtube_sr_mail_template.id}" do
        click_link "Delete"
      end

      expect(page).not_to have_text(youtube_sr_mail_template.select_label)
    end
  end
end
