require "rails_helper"

feature "user login" do
  context "user views the login page" do
    context "includes referral_data from cookies" do
      ReferralDatum.allowed_parameters.each do |param_key|
        scenario param_key.to_s do
          set_browser_cookie("tunecore_wp", param_key.to_s => "TestData", tc_timestamp: 1_458_752_702)

          visit login_path

          key = page.find_by_id("person_referral_data_attributes_0_key", visible: false).value
          value = page.find_by_id("person_referral_data_attributes_0_value", visible: false).value
          page_value = page.find_by_id("person_referral_data_attributes_0_page", visible: false).value
          source_value = page.find_by_id("person_referral_data_attributes_0_source", visible: false).value

          expect(key).to eq param_key.to_s
          expect(value).to eq "TestData"
          expect(page_value).to eq "login"
          expect(source_value).to eq "cookie"
        end
      end
    end

    scenario "doesnt includes referral_data from cookie params that aren't whitlisted" do
      set_browser_cookie("tunecore_wp", "not_whitelisted" => "TestData", tc_timestamp: 1_458_752_702)

      visit login_path

      page.assert_no_selector("#person_referral_data_attributes_0_key")
      page.assert_no_selector("#person_referral_data_attributes_0_value")
      page.assert_no_selector("#person_referral_data_attributes_0_page")
      page.assert_no_selector("#person_referral_data_attributes_0_source")
    end

    context "includes referral_data from url params" do
      ReferralDatum.allowed_parameters.each do |param_key|
        scenario param_key.to_s do
          visit "/login?#{param_key}=TestSource"

          key = page.find_by_id("person_referral_data_attributes_0_key", visible: false).value
          value = page.find_by_id("person_referral_data_attributes_0_value", visible: false).value
          page_value = page.find_by_id("person_referral_data_attributes_0_page", visible: false).value
          source_value = page.find_by_id("person_referral_data_attributes_0_source", visible: false).value

          expect(key).to eq param_key.to_s
          expect(value).to eq "TestSource"
          expect(page_value).to eq "login"
          expect(source_value).to eq "url"
        end
      end
    end

    scenario "doesnt includes referral_data from url params that aren't whitlisted" do
      visit "/login?garbage=TestSource"

      page.assert_no_selector("#person_referral_data_attributes_0_key")
      page.assert_no_selector("#person_referral_data_attributes_0_value")
      page.assert_no_selector("#person_referral_data_attributes_0_page")
      page.assert_no_selector("#person_referral_data_attributes_0_source")
    end
  end

  describe "captcha" do
    context "ip added to whitelist" do
      before do
        whitelist = create(:captcha_whitelist)
        allow_any_instance_of(ActionDispatch::Request)
          .to receive(:remote_ip)
          .and_return(whitelist.ip)
      end

      scenario "should not display captcha" do
        visit login_path

        expect(page).to_not have_selector('div', id: 'captcha')
      end
    end

    context "ip not in whitelist" do
      scenario "should display a captcha" do
        visit login_path

        expect(page).to have_selector('div', id: 'captcha')
      end
    end
  end

  context "with reskin enabled" do
    before do
      allow(FeatureFlipper).to receive(:show_feature?).with(any_args).and_call_original
      allow(FeatureFlipper).to receive(:show_feature?).with(:reskin).and_return(true)
    end

    context "user views the login page" do
      context "includes referral_data from cookies" do
        ReferralDatum.allowed_parameters.each do |param_key|
          scenario param_key.to_s do
            set_browser_cookie("tunecore_wp", param_key.to_s => "TestData", tc_timestamp: 1_458_752_702)

            visit login_path

            key = page.find_by_id("person_referral_data_attributes_0_key", visible: false).value
            value = page.find_by_id("person_referral_data_attributes_0_value", visible: false).value
            page_value = page.find_by_id("person_referral_data_attributes_0_page", visible: false).value
            source_value = page.find_by_id("person_referral_data_attributes_0_source", visible: false).value

            expect(key).to eq param_key.to_s
            expect(value).to eq "TestData"
            expect(page_value).to eq "login"
            expect(source_value).to eq "cookie"
          end
        end
      end

      scenario "doesnt includes referral_data from cookie params that aren't whitlisted" do
        set_browser_cookie("tunecore_wp", "not_whitelisted" => "TestData", tc_timestamp: 1_458_752_702)

        visit login_path

        page.assert_no_selector("#person_referral_data_attributes_0_key")
        page.assert_no_selector("#person_referral_data_attributes_0_value")
        page.assert_no_selector("#person_referral_data_attributes_0_page")
        page.assert_no_selector("#person_referral_data_attributes_0_source")
      end

      context "includes referral_data from url params" do
        ReferralDatum.allowed_parameters.each do |param_key|
          scenario param_key.to_s do
            visit "/login?#{param_key}=TestSource"

            key = page.find_by_id("person_referral_data_attributes_0_key", visible: false).value
            value = page.find_by_id("person_referral_data_attributes_0_value", visible: false).value
            page_value = page.find_by_id("person_referral_data_attributes_0_page", visible: false).value
            source_value = page.find_by_id("person_referral_data_attributes_0_source", visible: false).value

            expect(key).to eq param_key.to_s
            expect(value).to eq "TestSource"
            expect(page_value).to eq "login"
            expect(source_value).to eq "url"
          end
        end
      end

      scenario "doesnt includes referral_data from url params that aren't whitlisted" do
        visit "/login?garbage=TestSource"

        page.assert_no_selector("#person_referral_data_attributes_0_key")
        page.assert_no_selector("#person_referral_data_attributes_0_value")
        page.assert_no_selector("#person_referral_data_attributes_0_page")
        page.assert_no_selector("#person_referral_data_attributes_0_source")
      end
    end

    describe "captcha" do
      context "ip added to whitelist" do
        before do
          whitelist = create(:captcha_whitelist)
          allow_any_instance_of(ActionDispatch::Request)
            .to receive(:remote_ip)
            .and_return(whitelist.ip)
        end

        scenario "should not display captcha" do
          visit login_path

          expect(page).to_not have_selector('div', id: 'captcha')
        end
      end

      context "ip not in whitelist" do
        scenario "should display a captcha" do
          visit login_path

          expect(page).to have_selector('div', id: 'captcha')
        end
      end
    end
  end
end
