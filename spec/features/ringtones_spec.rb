require "rails_helper"

feature "ringtone", type: :feature do
  let(:person)    { create(:person) }
  let(:ringtone)  { create(:ringtone, person: person) }

  before(:each) do
    login_user(person, "Testpass123!")
  end

  describe "user can visit ringtone pages" do
    it "/ringtones/:id/edit" do
      visit edit_ringtone_path(ringtone)
    end

    it "/ringtone/show" do
      visit ringtone_path(ringtone)
      expect(page).to have_text ringtone.name
    end

    it "/ringtone/new" do
      visit new_ringtone_path
    end
  end

  describe "user creates a new ringtone release" do
    it "sets default language code on new ringtone form for all country websites" do
      CountryWebsite.all.each do |cw|
        visit new_ringtone_path(country_locale: cw.country)
        language_selected = find('#ringtone_language_code_legacy_support').value
        expect(language_selected).to eq(I18n.locale.to_s[0..1])
      end
    end
  end
end
