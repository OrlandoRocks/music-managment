require "rails_helper"

feature "checkout", type: :feature do
  scenario "user can add a credit card" do
    # RAILS_6_UPGRADE: Refactor this spec once we have a better Javascript driver
    # (headless Firefox or chromedriver)

    password = "Password1!"
    person = create(:person, password: password, password_confirmation: password)
    login_user(person, password)

    mock_braintree_create_payment_method_credit_card

    visit person_preferences_path
    click_on "Add a Card"

    expect(page).to have_content("Add Card")

    fill_in "Card Number", with: "4111111111111111"
    fill_in "Expiration Date", with: 4.years.from_now.strftime("%m/%Y")
    fill_in "cvv", with: "123"
    fill_in "First Name", with: person.first_name
    fill_in "Last Name", with: person.last_name
    fill_in "Address Line 1", with: Faker::Address.street_address
    fill_in "City", with: Faker::Address.city
    select Faker::Address.state.upcase, from: "State"
    fill_in "Postal Code", with: Faker::Address.postcode
    click_on "Add Card"

    expect(page).to have_content("Your credit card was added")
  end

  scenario "user adds a paypal account" do
    password = "Password123!"
    person = create(:person, password: password, password_confirmation: password)

    login_user(person, password)
    visit person_preferences_path
    mock_paypal_account_added
    click_on "Add a PayPal Account"

    expect(page).to have_content custom_t('controllers.stored_paypal_accounts.added_account')
  end

  scenario "user cannot submit payment preferences with no stored payment method" do
    password = "Password123!"
    person = create(:person, password: password, password_confirmation: password)

    login_user(person, password)
    visit person_preferences_path

    expect(page).to have_content "Payment settings"

    click_on "Save My Preferences"

    expect(page).to have_content "You must select a valid credit card"
  end

  scenario "user cannot select expired credit card and first valid card should be auto selected" do
    password = "Password123!"
    person = create(:person, password: password, password_confirmation: password)
    expired_credit_card = create(:stored_credit_card, :braintree_card, :expired, person: person, last_four: "1234")
    valid_credit_card = create(:stored_credit_card, :braintree_card, person: person, last_four: "2345")

    login_user(person, password)
    visit person_preferences_path
    expect(page.has_checked_field?("***#{valid_credit_card.last_four}")).to eq(true)
    field = "person_preference_preferred_credit_card_id_#{expired_credit_card.id}"
    expect(page).to have_css("input[id=#{field}][disabled=disabled]")
  end

  scenario "user can delete an expired credit card" do
    password = "Password123!"
    person = create(:person, password: password, password_confirmation: password)
    allow(Braintree::PaymentMethod).to receive(:delete).and_return(double(Object))
    create(:stored_credit_card, :expired, :braintree_card, person: person, last_four: "1234")

    login_user(person, password)
    visit person_preferences_path
    click_link "Delete"

    expect(page).to have_content "Your credit card was removed from our records."
  end

  describe "stem free trial message" do
    context "user is imported from stem" do
      let(:person) { create(:person) }

      before do
        allow_any_instance_of(Person).to receive(:imported_from_stem?).and_return(true)
      end

      scenario "user should see welcome trial message" do
        allow_any_instance_of(Person).to receive(:stem_free_trial_days_left).and_return(10)

        login_user(person, "Testpass123!")
        visit person_preferences_path

        expect(page).to have_selector('div', id: 'stem-trial-message')
      end

      scenario "user should not see welcome message when trial is over" do
        allow_any_instance_of(Person).to receive(:stem_free_trial_days_left).and_return(-10)

        login_user(person, "Testpass123!")
        visit person_preferences_path
        expect(page).to_not have_selector('div', id: 'stem-trial-message')
      end
    end
  end
end
