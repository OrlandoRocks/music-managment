require "rails_helper"

describe "Rejected album alerts and UI changes", type: :feature do
  before :each do
    @person = create(:person)
    @good_album = FactoryBot.create(:album, person: @person)
    @rejected_album = FactoryBot.create(:album, :paid, :with_uploaded_songs_and_artwork, number_of_songs: 3, person: @person, name: "Six Songs I Wrote by the Beatles")
    store = Store.find_by(abbrev: "ytsr")
    FactoryBot.create(:salepointable_store, store: store)
    @salepoint = FactoryBot.create(:salepoint, salepointable: @rejected_album, store: store)
    @rejected_album.update_attribute(:legal_review_state, "REJECTED")
    @rejected_album.review_audits << build(:review_audit, :rejected, album: @rejected_album)
    login_user(@person, "Testpass123!")
    visit dashboard_path
  end

  it "shows a persistent alert if album(s) are marked rejected" do
    expect(page).to have_content("Release Not Sent: Action RequiredWe found an issue with your release \u201c#{@rejected_album.name}\u201d. Please check your email for more detail on the problems and how to fix them in order to get your music live.")
  end

  it "doesn't show an alert if nothing's rejected" do
    @rejected_album.update_attribute(:legal_review_state, "NEEDS REVIEW")
    @rejected_album.review_audits = []
    # clear flash
    2.times { visit dashboard_path }
    expect(page).to_not have_content("Release Not Sent: Action Required")
  end

  context "on album detail pages" do
    it "shows specific alerts for a rejected album" do

      visit album_path(@rejected_album)
      expect(page).to have_content("In order to distribute cover songs")
    end

    it "doesn't show specific alerts otherwise" do
      visit album_path(@good_album)
      expect(page).to_not have_content("Release Not Sent: Action RequiredWe found an issue with this release and as a result it was not sent to stores. Please check your email for more detail on the problems and how to fix them in order to get your music live.")
    end
  end

  context "on single detail pages" do
    before :each do
      @good_single = FactoryBot.create(:single, person: @person)
      @rejected_single = FactoryBot.create(:paid_single, person: @person)
      @rejected_single.update_attribute(:legal_review_state, "REJECTED")
      @rejected_single.review_audits << build(:review_audit, :rejected, album: @rejected_single)
    end

    it "shows specific alerts for a rejected single" do

      visit album_path(@rejected_single)
      expect(page).to have_content("In order to distribute cover songs")
    end

    it "doesn't show specific alerts otherwise" do
      visit album_path(@good_single)
      expect(page).to_not have_content("Release Not Sent: Action Required We found an issue with this release and as a result it was not sent to stores. Please check your email for more detail on the problems and how to fix them in order to get your music live.")
    end
  end
end
