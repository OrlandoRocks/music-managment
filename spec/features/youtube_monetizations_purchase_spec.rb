require "rails_helper"

describe 'Purchase YouTube Monetizations', type: :feature do
  let(:password){ "Testpass123!" }
  let(:customer){ FactoryBot.create(:person, name: "John Smith", email: "jsmith@example.com", password: password, password_confirmation: password) }

  before(:each){
    login_user(customer, password)
    FactoryBot.create(:person)
    FactoryBot.create(:album, :finalized, person: customer)
    customer.person_balance.update_attribute(:balance, 1000)
  }

  it 'should add the product to the cart if terms is accepted' do
    visit artist_services_path
    within('.ytm') do
      click_on "Add to Cart"
    end
    expect(page).to have_content "You have 1 item in your cart."
    expect(page).to have_content "YouTube Sound Recording"
  end

  it "should succeed when trying to purchase the YouTube Money Product" do
    visit artist_services_path
    within('.ytm') do
      click_on "Add to Cart"
    end
    expect(page).to have_content "You have 1 item in your cart."
    expect(page).to have_content "YouTube Sound Recording"
    first(:link, "Proceed To Checkout").click
    first(:button, "Pay Now").click
    expect(page).to have_content "Thank you"
  end
end
