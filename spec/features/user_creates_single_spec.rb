require "rails_helper"

feature "single creation" do
  xscenario "user creates a single for distribution", js: true do
    allow_any_instance_of(Song).to receive(:big_box_meta_data)
      .and_return("duration" => [100])

    FakeBigBoxServer.while_running do
      password = "Password1!"
      person = build(:person, password: password, password_confirmation: password)
      create_new_account(person)
      single_title = "Ralphs Just Wanna Have Fun"

      visit(new_single_path)
      fill_in("Song Title", with: single_title)
      fill_in("single_creatives__name", with: "Ralph")
      select("Rock", from: "Primary Genre")
      click_on("Create My Single")

      expect(page).to have_css(".status_complete", text: "Details")
      expect(page).not_to have_css(".status_complete", text: "Stores")
      expect(page).not_to have_css(".status_complete", text: "Artwork")
      expect(page).not_to have_css(".status_complete", text: "Upload")
      expect(page).to have_content("Single Checklist")
      expect(page).to have_content(single_title)
      expect(page).to have_content("Ralph")

      click_on("Add Stores")
      check("deliver_all")
      click_on("Save", match: :first)

      expect(page).to have_css(".status_complete", text: "Stores")

      click_on("Upload or Create Artwork")
      find("form input[type='file']", visible: false)
        .set(File.join(Rails.root, "spec", "assets", "cover.jpg"))
      page.execute_script("$('form.edit_artwork').submit()")
      wait_for_ajax
      click_on("Save & Return")

      expect(page).to have_css(".status_complete", text: "Artwork")

      find(".songwriter-text-field").set("Ralph")
      click_on("Save")
      wait_for_ajax

      first(".song-file-upload-input", visible: false)
        .set(File.join(Rails.root, "spec", "assets", "track.wav"))
      wait_for_ajax

      expect(page).to have_css(".status_complete", text: "Upload")

      click_on("Add to Cart")
      click_on("Go to checkout")

      expect(page).to have_content("You have 1 item in your cart")
      expect(page).to have_content(single_title)
    end
  end
end
