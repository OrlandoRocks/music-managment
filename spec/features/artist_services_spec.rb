require "rails_helper"
feature "artist_services", type: :feature do
  let(:person) {
    FactoryBot.create(
      :person,
      email: "testuser@tunecore.com",
    )
  }

  scenario "User visits /artist_services" do
    login_user(person, "Testpass123!")
    visit artist_services_path
    expect(page).to have_text custom_t("services.index.artist_services")
    logout_user
  end
end
