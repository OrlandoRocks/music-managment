require "rails_helper"

feature "album creation" do
  xscenario "user creates an album for distribution", js: true do
    allow_any_instance_of(Song).to receive(:big_box_meta_data).and_return({})

    FakeBigBoxServer.while_running do
      password = "Password1!"
      person = build(:person, password: password, password_confirmation: password)
      create_new_account(person)

      visit(new_album_path)
      fill_in("Album Title", with: "My Amazing Album")
      fill_in("album_creatives__name", with: "Ralph")
      select("Rock", from: "Primary Genre")
      click_on("Create An Album")

      expect(page).to have_css(".status_complete", text: "Details")
      expect(page).not_to have_css(".status_complete", text: "Stores")
      expect(page).not_to have_css(".status_complete", text: "Artwork")
      expect(page).to have_content("Album Checklist")
      expect(page).to have_content("My Amazing Album")
      expect(page).to have_content("Ralph")

      click_on("Add Stores")
      check("deliver_all")
      click_on("Save", match: :first)

      expect(page).to have_css(".status_complete", text: "Stores")

      click_on("Upload or Create Artwork")
      find("form input[type='file']", visible: false)
        .set(File.join(Rails.root, "spec", "assets", "cover.jpg"))
      page.execute_script("$('form.edit_artwork').submit()")
      wait_for_ajax
      click_on("Save & Return")

      expect(page).to have_css(".status_complete", text: "Artwork")

      fill_in("Song Title (Required)", with: "The Ballad of Ralph")
      find(".songwriter-text-field").set("Ralph")
      click_on("Save and Add Another")
      wait_for_ajax
      fill_in("Song Title (Required)", with: "Ralphs Just Wanna Have Fun")
      find(".songwriter-text-field").set("Ralph")
      click_on("Save and Add Another")
      wait_for_ajax

      first(".song-file-upload-input", visible: false)
        .set(File.join(Rails.root, "spec", "assets", "track.wav"))
      all(".song-file-upload-input", visible: false).last
        .set(File.join(Rails.root, "spec", "assets", "track.wav"))
      wait_for_ajax

      expect(page).to have_css(".status_complete", text: "Songs")

      click_on("Add to Cart")
      click_on("Go to checkout")

      expect(page).to have_content("You have 1 item in your cart")
      expect(page).to have_content("My Amazing Album")
    end
  end
end
