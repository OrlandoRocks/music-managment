require "rails_helper"

feature "royalty split invitation", type: :feature do
  include ActionView::RecordIdentifier

  let(:current_user_password) { "MyPassword123!" }
  let(:current_user) { create :person, password: current_user_password, plan: Plan.find(4) }
  let!(:current_user_w9) { create :tax_form, :current_active_individual_w9, person: current_user }

  before(:each) do
    login_user(current_user, current_user_password)
    allow(FeatureFlipper).to receive(:show_feature?)
    allow(FeatureFlipper).to receive(:show_feature?).with(:royalty_splits, current_user).and_return(true)
  end

  describe "show pending split" do
    let!(:recipient) { create :royalty_split_recipient, person: current_user, percent: 60 }
    let(:split) { recipient.royalty_split }
    before do
      visit invited_royalty_splits_path
    end
    subject { find id: dom_id(split) }
    it { should have_text "60" }
    it { should have_text "Status: Pending" }
    it { should have_text "Accept Split" }
  end

  describe "accept pending split" do
    let!(:recipient) { create :royalty_split_recipient, person: current_user, percent: 60 }
    let(:split) { recipient.royalty_split }
    before do
      visit invited_royalty_splits_path
      within(:id, dom_id(split)) do
        click_on "Accept Split"
      end
    end
    subject { find(:id, dom_id(split)) }
    it { should have_text "60" }
    it { should have_text "Status: Accepted" }
    it { should_not have_text "Accept Split" }
  end

  describe "accept all pending splits" do
    let!(:recipients) { create_list :royalty_split_recipient, 3, :not_owner, person: current_user }
    before do
      visit invited_royalty_splits_path
      click_on "Accept All Pending Splits"
    end
    subject { page }
    it { should have_text("Status: Accepted", count: 3) }
    it { should have_text("Status: Pending", count: 0) }
    it { should_not have_text "Accept Split" }
  end
end
