require "rails_helper"

describe PersonPlansPurchaseService do
  describe "#process" do
    subject(:service) { described_class.process(invoice) }
    let(:person) { create(:person) }
    let(:invoice) {  create(:invoice, person: person) }
    let(:person) { create(:person) }
    let(:plan) { Plan.second }
    let(:product) { plan.product_by_person(person) }
    let!(:purchase) { create(:purchase, person_id: person.id, related: product, product: product, invoice: invoice) }

    let(:expected_args) {
      {
        plan_id: plan.id,
        person_id: person.id,
        purchase_id: purchase.id
      }
    }

    context "without a PersonPlan" do
      it "should call a PersonPlanCreationService with correct change_type" do
        expect(PersonPlanCreationService).to receive(:call).with(expected_args)
        subject
      end

      it "creates a new person_plan, renewal and person_plan_histories record" do
        expect { subject }.to change { PersonPlanHistory.count }.by(1)
          .and change { PersonPlan.count }.by(1)
          .and change { Renewal.count }.by(1)
      end

      it "should not create a PlanCreditUsagePurchase entry" do
        expect(PlanCreditUsagePurchase).not_to receive(:log_purchase)
        subject
      end
    end

    context "with a PersonPlan" do
      before do
        create(:person_plan, person: person, plan: plan)
      end

      it "should call a PersonPlanCreationService with correct change_type" do
        expect(PersonPlanCreationService).to receive(:call).with(expected_args)
        subject
      end

      it "creates a new person_plan_histories and renewal record" do
        expect { subject }.to change { PersonPlanHistory.count }.by(1)
          .and change { PersonPlan.count }.by(0)
          .and change { Renewal.count }.by(1)
      end

      it "should log to PlanCreditUsagePurchase when a user purchases a release" do
        plan_credit_usage = create(:plan_credit_usage, person: person)
        plan_credit_usage_purchase = create(:purchase, :unpaid, :credit_usage, {
          person_id: person.id,
          related: plan_credit_usage,
          invoice: invoice
        })

        expect { subject }.to change { PlanCreditUsagePurchase.count }.by(1)
      end
    end

    context "when PersonPlanCreationService raises an error" do
      let(:error_msg) { "You done messed up A.A.ron!" }

      before do
        allow(PersonPlanCreationService).to receive(:call).and_raise(ArgumentError.new(error_msg))
      end

      it "logs the error to airbrake" do
        expect(Airbrake).to receive(:notify).with(
          PersonPlansPurchaseService::PersonPlansPurchaseServiceError,
          {
            invoice: invoice,
            message: error_msg,
            purchase_id: purchase.id
          })
        subject
      end
    end
  end
end
