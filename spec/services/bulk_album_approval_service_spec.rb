require "rails_helper"

describe BulkAlbumApprovalService do
  before { allow_any_instance_of(ReviewAudit).to receive(:send_sns_notification) }

  describe ".approve" do
    it "is true if admin doesn't have the rights to approve" do
      admin = create(:person, :admin)
      person = create(:person)
      3.times { create(:album, person: person) }
      expect(BulkAlbumApprovalService.approve(
        person_id: admin.id, album_ids: person.albums.pluck(:id)
      )).to eq false
    end

    it "creates 2 review audits, approves, and returns true" do
      admin = create(:person, :admin)
      admin.roles << Role.new(name: 'Content Review Manager')
      person = create(:person)
      3.times { create(:album, :with_petri_bundle, :paid, person: person) }

      person.albums.each do |album|
        album.petri_bundle.distributions << FactoryBot.create(:distribution, petri_bundle_id: album.petri_bundle.id)
      end

      allow_any_instance_of(Distribution).to receive(:start)

      expect(BulkAlbumApprovalService.approve(
        person_id: admin.id, album_ids: person.albums.pluck(:id)
      )).to eq true
      person.albums.each do |album|
        expect(album.review_audits.count).to eq 2
        expect(album.reload.approved?).to eq true
      end
    end
  end
end
