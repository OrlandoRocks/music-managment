require "rails_helper"

describe ClearFailedLoginAttemptsService do
  it "clears the failed login attempts" do
    person = create(:person, recent_login: 10.days.ago, login_attempts: 2)
    ClearFailedLoginAttemptsService.clear(person.id)
    person.reload
    expect(person.recent_login).to be_within(1).of(Time.now)
    expect(person.login_attempts).to eq(0)
  end
end
