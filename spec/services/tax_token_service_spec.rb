require "rails_helper"

describe TaxTokenService do
  before :each do
    @tax_tokens_cohort = create(:tax_tokens_cohort)
    @tax_tokens = create_list(:tax_token, 5, tax_tokens_cohort: @tax_tokens_cohort)
  end

  describe ".remove_user_from_all_cohorts" do
    it "sets is_visible to false for the user in all cohorts" do
      person_id = @tax_tokens.first.person_id
      TaxTokenService.new(person_id).remove_user_from_all_cohorts
      persons_tax_tokens = TaxToken.where(person_id: person_id).all? { |token| token.is_visible? == false }
      expect(persons_tax_tokens).to eq(true)
    end
  end
end
