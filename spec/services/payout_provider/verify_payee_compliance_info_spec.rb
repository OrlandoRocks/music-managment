require 'rails_helper'

describe PayoutProvider::VerifyPayeeComplianceInfo do
  let(:payoneer_kyc_country_audit_source) do
    CountryAuditSource.find_by(name: CountryAuditSource::PAYONEER_KYC_NAME)
  end
  let(:us_person) { create(:person, :with_address, country: "US") }
  let(:ca_person) { create(:person, :with_address, country: "CA") }
  let!(:us_payout_provider) { create(:payout_provider, person: us_person) }
  let!(:ca_payout_provider) { create(:payout_provider, person: ca_person) }

  let(:payout_provider_address) do
    {
      "country" => "US",
      "address_line_1" => Faker::Address.street_address,
      "address_line_2" => Faker::Address.secondary_address,
      "city" => Faker::Address.city,
      "state" => Faker::Address.state,
      "zip_code" => Faker::Address.zip_code
    }
  end

  let(:payout_provider_address_invalid_state) do
    {
      "country" => "CA",
      "address_line_1" => Faker::Address.street_address,
      "address_line_2" => Faker::Address.secondary_address,
      "city" => Faker::Address.city,
      "state" => "00",
      "zip_code" => Faker::Address.zip_code
    }
  end

  let(:payout_provider_contact_individual) do
    {
      "first_name" => Faker::Name.first_name,
      "last_name" => Faker::Name.last_name,
      "email" => Faker::Internet.unique.email,
      "mobile" => Faker::PhoneNumber.unique.phone_number,
      "phone" => Faker::PhoneNumber.unique.phone_number
    }
  end

  let(:payout_provider_contact_business) do
    {
      "email" => Faker::Internet.unique.email,
      "mobile" => Faker::PhoneNumber.unique.phone_number,
      "phone" => Faker::PhoneNumber.unique.phone_number
    }
  end

  let!(:payee_extended_detail_response_individual) do
    Payoneer::Responses::PayeeExtendedDetail.new(
      double(
        "response",
        body: JSON.dump(
          address: payout_provider_address,
          contact: payout_provider_contact_individual,
          type: "INDIVIDUAL"
        )
      )
    )
  end

  let(:payee_extended_detail_response_business) do
    Payoneer::Responses::PayeeExtendedDetail.new(
      double(
        "response",
        body: JSON.dump(
          address: payout_provider_address,
          company: { name: Faker::Company.name },
          contact: payout_provider_contact_business,
          type: "COMPANY"
        )
      )
    )
  end

  let(:payee_extended_detail_response) do
    Payoneer::Responses::PayeeExtendedDetail.new(
      double(
        "response",
        body: JSON.dump(
          address: payout_provider_address,
          contact: payout_provider_contact_individual
        )
      )
    )
  end

  let(:payee_extended_detail_response_invalid_state) do
    Payoneer::Responses::PayeeExtendedDetail.new(
      double(
        "response",
        body: JSON.dump(
          address: payout_provider_address_invalid_state,
          contact: payout_provider_contact_individual
        )
      )
    )
  end

  def call_verify_payee_compliance_info_as_individual(person)
    described_class.new(
      person,
      provider_payee_details: payee_extended_detail_response_individual
    ).call
  end

  def call_verify_payee_compliance_info_as_individual_with_null_state(person)
    described_class.new(
      person,
      provider_payee_details: payee_extended_detail_response_invalid_state
    ).call
  end

  def call_verify_payee_compliance_info_as_business(person)
    described_class.new(
      person,
      provider_payee_details: payee_extended_detail_response_business
    ).call
  end

  def country_audit_count(person)
    person.country_audits.where(
      country: Country.search_by_iso_code(person[:country]),
      country_audit_source: payoneer_kyc_country_audit_source
    ).count
  end

  describe ".call" do
    context "when address and compliance_fields are updated for KYC COMPANY" do
      before do
        call_verify_payee_compliance_info_as_business(us_person)
      end

      it "updates a person's address line 1 to match payout provider address line 1" do
        expect(us_person.address1).to eq(payout_provider_address["address_line_1"])
      end

      it "updates a person's address line 2 to match payout provider address line 2" do
        expect(us_person.address2).to eq(payout_provider_address["address_line_2"])
      end

      it "updates a person's city to match payout provider city" do
        expect(us_person.city).to eq(payout_provider_address["city"])
      end

      it "updates a person's state to match payout provider state" do
        expect(us_person.state).to eq(payout_provider_address["state"])
      end

      it "person's first name should be nil" do
        expect(us_person.compliance_first_name).to eq(nil)
      end

      it "person's last name should be nil" do
        expect(us_person.compliance_last_name).to eq(nil)
      end

      it "update's a person's company name to match payout provider company name" do
        expect(us_person.compliance_company_name).to eq(payee_extended_detail_response_business.company_name)
      end

      it "locks a person's address" do
        expect(us_person.address_locked?).to eq true
      end

      it "does not lock a person's first_name" do
        expect(us_person.first_name_locked?).to eq false
      end

      it "does not lock a person's last_name" do
        expect(us_person.last_name_locked?).to eq false
      end

      it "locks a person's company_name" do
        expect(us_person.company_name_locked?).to eq true
      end

      it "locks a person's customer_type" do
        expect(Person::FlagService::person_flag_exists?(
          {
            person: us_person,
            flag_attributes: Flag::CUSTOMER_TYPE_LOCKED
          }
        )).to eq true
      end

      it "switches a person's customer_type if TC type does not match Payoneer's" do
        expect(us_person.business?).to eq true
        expect(us_person.individual?).to eq false
      end
    end

    context "when address and compliance_fields are updated for KYC INDIVIDUAL" do
      before do
        call_verify_payee_compliance_info_as_individual(us_person)
      end

      it "updates a person's address line 1 to match payout provider address line 1" do
        expect(us_person.address1).to eq(payout_provider_address["address_line_1"])
      end

      it "updates a person's address line 2 to match payout provider address line 2" do
        expect(us_person.address2).to eq(payout_provider_address["address_line_2"])
      end

      it "updates a person's city to match payout provider city" do
        expect(us_person.city).to eq(payout_provider_address["city"])
      end

      it "updates a person's state to match payout provider state" do
        expect(us_person.state).to eq(payout_provider_address["state"])
      end

      it "updates a person's first name to match payout provider first name" do
        expect(us_person.compliance_first_name).to eq(payout_provider_contact_individual['first_name'])
      end

      it "updates a person's last name to match payout provider last name" do
        expect(us_person.compliance_last_name).to eq(payout_provider_contact_individual['last_name'])
      end

      it "person's company name should be nil" do
        expect(us_person.compliance_company_name).to eq(nil)
      end

      it "locks a person's address" do
        expect(us_person.address_locked?).to eq true
      end

      it "locks a person's first_name" do
        expect(us_person.first_name_locked?).to eq true
      end

      it "locks a person's last_name" do
        expect(us_person.last_name_locked?).to eq true
      end

      it "does not lock a person's company_name" do
        expect(us_person.company_name_locked?).to eq false
      end

      it "locks a person's customer_type" do
        expect(Person::FlagService::person_flag_exists?(
          {
            person: us_person,
            flag_attributes: Flag::CUSTOMER_TYPE_LOCKED
          }
        )).to eq true
      end

      it "does not switch TC customer type when KYC customer type is matching" do
        expect(us_person.business?).to eq false
        expect(us_person.individual?).to eq true
      end
    end

    context "when country is updated" do
      context "when payout provider country doesn't match person's country" do
        before do
          call_verify_payee_compliance_info_as_individual(ca_person)
        end

        it "updates a person's country to match payout provider country" do
          expect(ca_person[:country]).to eq("US")
        end

        it "creates a CountryAudit entry" do
          expect(country_audit_count(ca_person)).to eq(1)
        end

        it "locks a person's address" do
          expect(ca_person.address_locked?).to eq true
        end
      end

      context "when payout provider sends invalid state" do
        it "sets the address state to null when payoneer sends 00 for US users" do
          call_verify_payee_compliance_info_as_individual_with_null_state(ca_person)
          expect(ca_person[:state]).to be_nil
        end
      end

      context "when payout provider country matches person's country" do
        before do
          call_verify_payee_compliance_info_as_individual(us_person)
        end

        it "does not update a person's country" do
          expect(us_person[:country]).to eq("US")
        end

        it "creates a CountryAudit entry" do
          expect(country_audit_count(us_person)).to eq(1)
        end

        it "locks the person's address" do
          expect(us_person.address_locked?).to eq true
        end
      end
    end

    # If payoneer is the source of truth, we should always change to what payoneer has regardless if its locked or not.
    #
    # context "when person's address is already locked" do
    #   before do
    #     ca_person.lock_address
    #     call_verify_payee_compliance_info(ca_person)
    #   end

    #   it "does not update a person's address line 1" do
    #     expect(ca_person.address1).not_to eq(payout_provider_address["address_line_1"])
    #   end

    #   it "does not update a person's address line 2" do
    #     expect(ca_person.address2).not_to eq(payout_provider_address["address_line_2"])
    #   end

    #   it "does not update a person's city" do
    #     expect(ca_person.city).not_to eq(payout_provider_address["city"])
    #   end

    #   it "does not update a person's state" do
    #     expect(ca_person.state).not_to eq(payout_provider_address["state"])
    #   end

    #   it "does not update a person's zip/postal code" do
    #     expect(ca_person.postal_code).not_to eq(payout_provider_address["zip_code"])
    #   end

    #   it "does not update a person's country" do
    #     expect(ca_person[:country]).to eq("CA")
    #   end

    #   it "does not create a CountryAudit entry" do
    #     expect(country_audit_count(ca_person)).to eq(0)
    #   end
    # end
  end
end
