require 'rails_helper'

describe PayoutProvider::ApiService do
  let(:api_client) { double(:api_client) }
  let(:api_class) { double(:api_class, new: api_client) }
  let(:person) { create(:person) }
  let!(:payout_provider) { create(:payout_provider, person: person) }
  let(:transfer) { create(:payout_transfer, payout_provider: payout_provider)}

  let(:payout_client) { described_class.new(person: person, api_client: api_class) }

  it "creates a login link" do
    expect(api_client).to receive(:send_request).with(:register_existing_users, person: person)
    payout_client.register_existing_user(person: person)
  end

  it "creates a registration link" do
    response_mock = double(:response, body: JSON.dump({}))
    allow(api_client).to receive(:send_request)
                     .with(:register_new_users, {person: person, redirect_url: "www.google.com"})
                     .and_return(response_mock)
    response = payout_client.register_new_user(person: person, redirect_url: "www.google.com")
    expect(response.class).to eq(Payoneer::Responses::RegisterNewUser)
  end

  it "gets the status of a person" do
    expect(api_client).to receive(:send_request).with(:person_statuses, person: person)
    payout_client.get_status(person: person)
  end

  it "submits a payout" do
    response = double(:response, body: JSON.dump({}))
    expect(api_client).to receive(:send_request).with(:submit_payouts, person: person).and_return(response)
    payout_client.submit_payout(person: person)
  end

  it "cancels a payout" do
    expect(api_client).to receive(:send_request).with(:cancel_payouts, transfer: transfer)
    payout_client.cancel_payout(transfer: transfer)
  end

  it "gets the details of a payout" do
    expect(api_client).to receive(:send_request).with(:payout_details, person: person)
    payout_client.get_payout_details(person: person)
  end

  it "gets a report for a person" do
    expect(api_client).to receive(:send_request).with(:payee_reports, person: person)
    payout_client.payee_report(person: person)
  end

  it "gets payout provider payee details" do
    response_mock = double("response", body: JSON.dump({}))
    allow(api_client).to receive(:send_request).with(:payee_details, person: person).and_return(response_mock)
    details = payout_client.payee_details(person: person)
    expect(details.class).to eq(Payoneer::Responses::PayeeDetail)
  end

  it "gets payout provider extended payee details" do
    response_mock = double("response", body: JSON.dump({}))
    allow(response_mock).to receive(:success?).and_return(true)
    expect(api_client).to receive(:send_request).with(:payee_extended_details, person: person).and_return(response_mock)
    extended_details = payout_client.payee_extended_details(person: person)
    expect(extended_details.class).to eq(Payoneer::Responses::PayeeExtendedDetail)
  end

  it "gets payout provider payee status" do
    response_mock = double("response", body: JSON.dump({}))
    expect(api_client).to receive(:send_request).with(:payee_status, person: person).and_return(response_mock)
    extended_details = payout_client.payee_status(person: person)
    expect(extended_details.class).to eq(Payoneer::Responses::PayeeStatus)
  end

  describe ".move_program" do
    it "raises error in production" do
      allow(Rails.env).to receive(:production?).and_return(true)
      expect{payout_client.move_program}.to raise_error(PayoutProvider::ApiService::WrongEnvError)
    end
  end

  describe ".adopt_payee" do
    it "copies a payee to another program" do
      response_mock = double("response", body: JSON.dump({}))
      allow(response_mock).to receive(:success?).and_return(true)
      expect(api_client).to receive(:send_request).with(:adopt_payee, person: person).and_return(response_mock)
      extended_details = payout_client.adopt_payee(person: person)
      expect(extended_details.class).to eq(Payoneer::Responses::AdoptPayee)
    end
  end
end
