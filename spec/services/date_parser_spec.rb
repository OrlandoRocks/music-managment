require "rails_helper"

describe DateParser do
  describe "#parse" do
    context "when a valid input is passed to the parser" do
      it "successfully parses a date given a US formatted date" do
        unparsed_date = "12/30/2019"
        expect(DateParser.parse(unparsed_date)).to eq(Date.new(2019, 12, 30))
      end

      it "successfully parses a date given a EU formatted date" do
        unparsed_date = "30.12.2019"
        expect(DateParser.parse(unparsed_date)).to eq(Date.new(2019, 12, 30))
      end

      it "successfully parses a date given a Javascript formatted date" do
        unparsed_date = "2019-12-30"
        expect(DateParser.parse(unparsed_date)).to eq(Date.new(2019, 12, 30))
      end
    end

    context "when an invalid input is passed to the parser" do
      it "returns nil when an invalid date format is passed to the parser as a string" do
        unparsed_date = "abcd"
        expect(DateParser.parse(unparsed_date)).to eq(nil)
      end

      it "throws an exception when the parser is given an object that is not a string to parse" do
        expect{ DateParser.parse(100) }.to raise_exception("Type Error: unparsed_date must be a string")
      end
    end
  end
end