require "rails_helper"
# testing pretty heavily because the nature of this service is that we don't know what's causing issues with
# invoices/renewals. Therefore it will be hard to qa. We want to do your best to ensure that in every conceivable
# scenario we're logging the info so that we can find more root issues
describe InvoiceLogService do
  describe "log" do
    before(:each) do
      @person  = create(:person)

      # instantiating the @invoice variable in before all because while we reassign it in most of the specs, there are
      # dependent variables we're creating that we don't want to reassign in some specs (those values will obviously be
      # tied to unrelated invoices in specs where the @invoice is reassigned, but that's not an issue because they will
      # only need the id from those variables anyway).
      album  = create(:album, person: @person)
      @invoice = create(:invoice, :with_every_attribute_purchase, person: @person, related: album, vat_amount_cents: 500,
                        settled_at: Date.current - 1.day, foreign_exchange_pegged_rate_id: 1)
      @payment_batch = create(:payment_batch, country_website: CountryWebsite.find(1))
      @batch_transaction = create(:batch_transaction, invoice: @invoice, payment_batch: @payment_batch)

      @braintree_transaction = create(:braintree_transaction, invoice: @invoice)
      @paypal_transaction = create(:paypal_transaction, invoice: @invoice)
      @stored_paypal_account = create(:stored_paypal_account, person: @person)

      product = create(:product)
      item = create(
          :product_item,
          product: product,
          renewal_duration: 1,
          renewal_interval: nil
      )
      @renewal = create(:renewal, person: @person, purchase: @invoice.purchases.first, item_to_renew: item)
      @options = {
          person: nil,
          invoice: nil,
          braintree_transaction_id: nil,
          paypal_transaction_id: nil,
          batch_transaction_id: nil,
          payment_batch_id: nil,
          stored_paypal_account_id: nil,
          renewal_id: nil,
          message: nil,
          current_method_name: nil,
          caller_method_path: nil
      }
      @initial_invoice_log_count = InvoiceLog.count
    end
    context "given all possible options" do
      it "creates one InvoiceLog with the given options, and fills out every field" do
        @options = {
            person: @person,
            invoice: @invoice,
            braintree_transaction_id: @braintree_transaction.id,
            payments_os_transaction_id: 34,
            paypal_transaction_id: @paypal_transaction.id,
            batch_transaction_id: @batch_transaction.id,
            payment_batch_id: @payment_batch.id,
            stored_paypal_account_id: @stored_paypal_account.id,
            renewal_id: @renewal.id,
            message: "this is an error",
            current_method_name: "method",
            caller_method_path: "path/to/method"
        }
        InvoiceLogService.log(@options)
        log                = InvoiceLog.all[-1]
        any_nil_attributes = log.attributes.values.include?(nil)

        expect(@initial_invoice_log_count + 1).to be(InvoiceLog.count)
        expect(any_nil_attributes).to be(false)
      end
    end
    context "given only invoice and person" do
      context "invoice with two purchases" do
        it "creates two InvoiceLogs with the given options and fills out appropriate fields" do
          @invoice = create(:invoice, :with_two_purchases, person: @person, vat_amount_cents: 500,
                            settled_at: Date.current - 1.day, foreign_exchange_pegged_rate_id: 1)
          @payment_batch = create(:payment_batch, country_website: CountryWebsite.find(1))
          @batch_transaction = create(:batch_transaction, invoice: @invoice, payment_batch: @payment_batch)

          @options = {
              person: @person,
              invoice: @invoice,
              braintree_transaction_id: @braintree_transaction.id,
              paypal_transaction_id: @paypal_transaction.id,
              batch_transaction_id: @batch_transaction.id,
              payment_batch_id: @payment_batch.id,
              stored_paypal_account_id: @stored_paypal_account.id,
              renewal_id: @renewal.id,
              message: "this is an error",
              current_method_name: "method",
              caller_method_path: "path/to/method"
          }
          InvoiceLogService.log(@options)

          log_purchases     = InvoiceLog.all.map(&:purchase).sort
          invoice_purchases = @invoice.purchases.sort

          expect(@initial_invoice_log_count + 2).to be(InvoiceLog.count)
          expect(log_purchases).to eq(invoice_purchases)
        end
      end
      #this shouldn't happen in the wild but testing anyway because we want this service to be as error proof as
      # possible
      context "invoice without purchases" do
        it "creates one invoice log with no purchases" do
          @invoice = create(:invoice, person: @person)
          @options  = {
              person: @person,
              invoice: @invoice,
              braintree_transaction_id: @braintree_transaction.id,
              paypal_transaction_id: @paypal_transaction.id,
              stored_paypal_account_id: @stored_paypal_account.id,
              renewal_id: @renewal.id,
              message: "this is an error",
              current_method_name: "method",
              caller_method_path: "path/to/method"
          }
          InvoiceLogService.log(@options)

          log_purchases = InvoiceLog.all.map(&:purchase)

          expect(@initial_invoice_log_count + 1).to be(InvoiceLog.count)
          expect(log_purchases).to eq([nil])
        end
      end
    end
    context "given invoice, person, and braintree transaction id" do
      before(:each) do
        @invoice = create(:invoice, person: @person)
        @options[:invoice] = @invoice
        @options[:person] = @person
      end
      context "invoice with braintree transactions" do
        it "creates one InvoiceLog with the id provided, not the tx of the invoice" do
          invoice_braintree_transaction = create(:braintree_transaction, invoice: @invoice)
          @invoice.braintree_transactions << invoice_braintree_transaction
          @options[:braintree_transaction_id] = @braintree_transaction.id

          InvoiceLogService.log(@options)

          invoice_log_bt_tx_ids = InvoiceLog.all.map(&:braintree_transaction_id)
          invoice_bt_tx_ids     = @invoice.braintree_transactions.map(&:id)

          expect(@initial_invoice_log_count + 1).to eq(InvoiceLog.count)
          expect(invoice_log_bt_tx_ids).to_not eq(invoice_bt_tx_ids)
          expect(invoice_log_bt_tx_ids).to eq([@braintree_transaction.id])
        end
      end
      context "invoice without braintree transactions" do
        it "creates one InvoiceLog with the id provided" do
          @options[:braintree_transaction_id] = @braintree_transaction.id

          InvoiceLogService.log(@options)

          invoice_log_bt_tx_ids = InvoiceLog.all.map(&:braintree_transaction_id)

          expect(@initial_invoice_log_count + 1).to eq(InvoiceLog.count)
          expect(invoice_log_bt_tx_ids).to eq([@braintree_transaction.id])
        end
      end
    end
    context "payments os transaction" do
      it "creates invoice log with payments os transaction id if passed" do
        invoice = create(:invoice, person: @person)
        InvoiceLogService.log(
            {
                person: @person,
                invoice: invoice,
                payments_os_transaction_id: 3
            }
        )
        log = InvoiceLog.last
        expect(log.person_id).to eq(@person.id)
        expect(log.invoice_id).to eq(invoice.id)
        expect(log.payments_os_transaction_id).to eq(3)
      end
    end
    context "given invoice, person, and paypal transaction id" do
      context "invoice with paypal transactions" do
        it "creates one InvoiceLog with the id provided" do
          invoice_paypal_transaction = create(:paypal_transaction, invoice: @invoice)
          @invoice.paypal_transactions << invoice_paypal_transaction
          @options[:paypal_transaction_id] = @paypal_transaction.id

          InvoiceLogService.log(@options)

          invoice_log_paypal_tx_ids = InvoiceLog.all.map(&:paypal_transaction_id)
          invoice_paypal_tx_ids     = @invoice.paypal_transactions.map(&:id)


          expect(@initial_invoice_log_count + 1).to eq(InvoiceLog.count)
          expect(invoice_log_paypal_tx_ids).to_not eq(invoice_paypal_tx_ids)
          expect(invoice_log_paypal_tx_ids).to eq([@paypal_transaction.id])
        end
      end
      context "invoice without paypal transactions" do
        it "creates one InvoiceLog with the id provided" do
          @options[:paypal_transaction_id] = @paypal_transaction.id

          InvoiceLogService.log(@options)

          invoice_log_bt_tx_ids = InvoiceLog.all.map(&:paypal_transaction_id)


          expect(@initial_invoice_log_count + 1).to eq(InvoiceLog.count)
          expect(invoice_log_bt_tx_ids).to eq([@paypal_transaction.id])
        end
      end
    end
    context "given invoice, person, and batch transaction id" do
      context "invoice with batch transactions" do
        it "creates one InvoiceLog with the id provided" do
          invoice_batch_transaction = create(:batch_transaction, invoice: @invoice)
          @invoice.batch_transactions << invoice_batch_transaction
          @options[:batch_transaction_id] = @batch_transaction.id

          InvoiceLogService.log(@options)

          invoice_log_batch_tx_ids = InvoiceLog.all.map(&:batch_transaction_id)
          invoice_batch_tx_ids     = @invoice.batch_transactions.map(&:id)

          expect(@initial_invoice_log_count + 1).to eq(InvoiceLog.count)
          expect(invoice_log_batch_tx_ids).to_not eq(invoice_batch_tx_ids)
          expect(invoice_log_batch_tx_ids).to eq([@batch_transaction.id])
        end
      end
      context "invoice without batch transactions" do
        it "creates one InvoiceLog with the id provided" do
          @options[:batch_transaction_id] = @batch_transaction.id

          InvoiceLogService.log(@options)

          invoice_log_batch_tx_ids = InvoiceLog.all.map(&:batch_transaction_id)


          expect(@initial_invoice_log_count + 1).to eq(InvoiceLog.count)
          expect(invoice_log_batch_tx_ids).to eq([@batch_transaction.id])
        end
      end
    end
    context "given invoice, person, and payment batch id" do
      it "creates one InvoiceLog with the id provided" do
        @invoice = create(:invoice, person: @person)
        @options[:payment_batch_id] = @payment_batch.id

        InvoiceLogService.log(@options)

        invoice_log_payment_batch_ids = InvoiceLog.all.map(&:payment_batch_id)


        expect(@initial_invoice_log_count + 1).to eq(InvoiceLog.count)
        expect(invoice_log_payment_batch_ids).to eq([@payment_batch.id])
      end
    end
    context "given invoice, person, and stored paypal account id" do
      it "creates one InvoiceLog with the id provided" do
        @invoice = create(:invoice, person: @person)
        @options[:stored_paypal_account_id] = @stored_paypal_account.id

        InvoiceLogService.log(@options)

        log_stored_paypal_account_ids = InvoiceLog.all.map(&:stored_paypal_account_id)


        expect(@initial_invoice_log_count + 1).to eq(InvoiceLog.count)
        expect(log_stored_paypal_account_ids).to eq([@stored_paypal_account.id])
      end
    end
    context "given invoice, person, and renewal id" do
      it "creates one InvoiceLog with the id provided" do
        @invoice = create(:invoice, person: @person)
        @options[:renewal_id] = @renewal.id

        InvoiceLogService.log(@options)

        invoice_log_renewal_ids = InvoiceLog.all.map(&:renewal_id)


        expect(@initial_invoice_log_count + 1).to eq(InvoiceLog.count)
        expect(invoice_log_renewal_ids).to eq([@renewal.id])
      end
    end
    context "given only invoice and person" do
      context "invoice has two purchases" do
        before(:each) do
          @invoice = create(:invoice, :with_two_purchases, person: @person)
        end
        context "and no transactions" do
          it "creates two logs, one for each purchase" do
            @options = {
                invoice: @invoice,
                person: @person
            }
            InvoiceLogService.log(@options)
            expect(@initial_invoice_log_count + 2).to eq(InvoiceLog.count)
          end
        end
        context "and a braintree_transaction" do
          it "creates three logs, one for each purchase and one for the braintree transaction" do
            @braintree_transaction = create(:braintree_transaction, invoice: @invoice)
            @invoice.braintree_transactions << @braintree_transaction
            @options = {
                invoice: @invoice,
                person: @person
            }

            InvoiceLogService.log(@options)

            purchases = InvoiceLog.all.map(&:purchase).compact.sort_by(&:id)
            braintree_transactions = InvoiceLog.all.map(&:braintree_transaction).compact.sort_by(&:id)

            expect(@initial_invoice_log_count + 3).to eq(InvoiceLog.count)
            expect(purchases).to eq(@invoice.purchases.sort_by(&:id))
            expect(braintree_transactions).to eq(@invoice.braintree_transactions.sort_by(&:id))
          end
        end
        context "and two braintree_transactions" do
          it "creates four logs, one for each purchase and one for each braintree transaction" do
            braintree_transaction_1 = create(:braintree_transaction, invoice: @invoice)
            braintree_transaction_2 = create(:braintree_transaction, invoice: @invoice)
            @invoice.braintree_transactions << braintree_transaction_1
            @invoice.braintree_transactions << braintree_transaction_2
            @options = {
                invoice: @invoice,
                person: @person
            }

            InvoiceLogService.log(@options)

            purchases = InvoiceLog.all.map(&:purchase).compact.sort_by(&:id)
            braintree_transactions = InvoiceLog.all.map(&:braintree_transaction).compact.sort_by(&:id)

            expect(@initial_invoice_log_count + 4).to eq(InvoiceLog.count)
            expect(purchases).to eq(@invoice.purchases.sort_by(&:id))
            expect(braintree_transactions).to eq(@invoice.braintree_transactions.sort_by(&:id))
          end
        end
        context "and a paypal_transaction" do
          it "creates three logs, one for each purchase and one for the paypal transaction" do
            @paypal_transaction = create(:paypal_transaction, invoice: @invoice)
            @invoice.paypal_transactions << @paypal_transaction
            @options = {
                invoice: @invoice,
                person: @person
            }

            InvoiceLogService.log(@options)

            purchases = InvoiceLog.all.map(&:purchase).compact.sort_by(&:id)
            paypal_transactions = InvoiceLog.all.map(&:paypal_transaction).compact.sort_by(&:id)

            expect(@initial_invoice_log_count + 3).to eq(InvoiceLog.count)
            expect(purchases).to eq(@invoice.purchases.sort_by(&:id))
            expect(paypal_transactions).to eq(@invoice.paypal_transactions.sort_by(&:id))
          end
        end
        context "and two paypal_transactions" do
          it "creates four logs, one for each purchase and one for each paypal transaction" do
            paypal_transaction_1 = create(:paypal_transaction, invoice: @invoice)
            paypal_transaction_2 = create(:paypal_transaction, invoice: @invoice)
            @invoice.paypal_transactions << paypal_transaction_1
            @invoice.paypal_transactions << paypal_transaction_2
            @options = {
                invoice: @invoice,
                person: @person
            }

            InvoiceLogService.log(@options)

            purchases = InvoiceLog.all.map(&:purchase).compact.sort_by(&:id)
            paypal_transactions = InvoiceLog.all.map(&:paypal_transaction).compact.sort_by(&:id)

            expect(@initial_invoice_log_count + 4).to eq(InvoiceLog.count)
            expect(purchases).to eq(@invoice.purchases.sort_by(&:id))
            expect(paypal_transactions).to eq(@invoice.paypal_transactions.sort_by(&:id))
          end
        end
        context "and one braintree_transaction and one paypal_transaction" do
          it "creates four logs, one for each purchase, one for the paypal transaction, and one for the braintree" do

            @paypal_transaction = create(:paypal_transaction, invoice: @invoice)
            @invoice.paypal_transactions << @paypal_transaction
            @braintree_transaction = create(:braintree_transaction, invoice: @invoice)
            @invoice.braintree_transactions << @braintree_transaction

            @options = {
                invoice: @invoice,
                person: @person
            }

            InvoiceLogService.log(@options)

            purchases = InvoiceLog.all.map(&:purchase).compact.sort_by(&:id)
            paypal_transactions = InvoiceLog.all.map(&:paypal_transaction).compact.sort_by(&:id)
            braintree_transactions = InvoiceLog.all.map(&:braintree_transaction).compact.sort_by(&:id)

            expect(@initial_invoice_log_count + 4).to eq(InvoiceLog.count)
            expect(purchases).to eq(@invoice.purchases.sort_by(&:id))
            expect(paypal_transactions).to eq(@invoice.paypal_transactions.sort_by(&:id))
            expect(braintree_transactions).to eq(@invoice.braintree_transactions.sort_by(&:id))
          end
        end
        context "and a batch_transaction" do
          it "creates three logs, one for each purchase and one for the paypal transaction" do
            @batch_transaction = create(:batch_transaction, invoice: @invoice)
            @invoice.batch_transactions << @batch_transaction
            @options = {
                invoice: @invoice,
                person: @person
            }

            InvoiceLogService.log(@options)

            purchases = InvoiceLog.all.map(&:purchase).compact.sort_by(&:id)
            batch_transactions = InvoiceLog.all.map(&:batch_transaction).compact.sort_by(&:id)

            expect(@initial_invoice_log_count + 3).to eq(InvoiceLog.count)
            expect(purchases).to eq(@invoice.purchases.sort_by(&:id))
            expect(batch_transactions).to eq(@invoice.batch_transactions.sort_by(&:id))
          end
        end
        context "and two batch_transactions" do
          # not sure this is possible in real life, but the code is written to allow it so we'll test it
          it "creates four logs, one for each purchase and one for each batch transaction" do
            batch_transaction_1 = create(:batch_transaction, invoice: @invoice, payment_batch: @payment_batch)
            batch_transaction_2 = create(:batch_transaction, invoice: @invoice, payment_batch: @payment_batch)
            @invoice.batch_transactions << batch_transaction_1
            @invoice.batch_transactions << batch_transaction_2
            @options = {
                invoice: @invoice,
                person: @person
            }

            InvoiceLogService.log(@options)

            purchases = InvoiceLog.all.map(&:purchase).compact.sort_by(&:id)
            batch_transactions = InvoiceLog.all.map(&:batch_transaction).compact.sort_by(&:id)

            expect(@initial_invoice_log_count + 4).to eq(InvoiceLog.count)
            expect(purchases).to eq(@invoice.purchases.sort_by(&:id))
            expect(batch_transactions).to eq(@invoice.batch_transactions.sort_by(&:id))
          end
        end
      end
      context "invoice has a braintree_transaction" do
        it "creates one log, one for the braintree transaction" do
          @invoice = create(:invoice, person: @person)
          @braintree_transaction = create(:braintree_transaction, invoice: @invoice)
          @invoice.braintree_transactions << @braintree_transaction
          @options = {
              invoice: @invoice,
              person: @person
          }

          InvoiceLogService.log(@options)

          braintree_transactions = InvoiceLog.all.map(&:braintree_transaction).compact.sort_by(&:id)

          expect(@initial_invoice_log_count + 1).to eq(InvoiceLog.count)
          expect(braintree_transactions).to eq(@invoice.braintree_transactions.sort_by(&:id))
        end
      end
      context "invoice has a paypal_transaction" do
        it "creates one log, one for the paypal transaction" do
          @invoice = create(:invoice, person: @person)
          @paypal_transaction = create(:paypal_transaction, invoice: @invoice)
          @invoice.paypal_transactions << @paypal_transaction
          @options = {
              invoice: @invoice,
              person: @person
          }

          InvoiceLogService.log(@options)

          paypal_transactions = InvoiceLog.all.map(&:paypal_transaction).compact.sort_by(&:id)

          expect(@initial_invoice_log_count + 1).to eq(InvoiceLog.count)
          expect(paypal_transactions).to eq(@invoice.paypal_transactions.sort_by(&:id))
        end
      end
      context "invoice has a batch_transaction" do
        it "creates one log, one for the paypal transaction" do
          @invoice = create(:invoice, person: @person)
          @batch_transaction = create(:batch_transaction, invoice: @invoice)
          @invoice.batch_transactions << @batch_transaction
          @options = {
              invoice: @invoice,
              person: @person
          }

          InvoiceLogService.log(@options)

          batch_transactions = InvoiceLog.all.map(&:batch_transaction).compact.sort_by(&:id)

          expect(@initial_invoice_log_count + 1).to eq(InvoiceLog.count)
          expect(batch_transactions).to eq(@invoice.batch_transactions.sort_by(&:id))
        end
      end
    end
    # including this just to check that we don't error out if everything is nil
    context "given nothing" do
      context "given empty hash" do
        it "does not create anything" do
          @options = {}
          InvoiceLogService.log(@options)

          expect(@initial_invoice_log_count).to eq(InvoiceLog.count)
          expect(InvoiceLog).not_to receive(:create)
        end
      end
      context "given all incorrect keys" do
        it "does not create anything" do
          @options = {
              greg: "greggy",
              book: "football",
              sport: :the_picture_of_dorian_gray
          }
          InvoiceLogService.log(@options)

          expect(@initial_invoice_log_count).to eq(InvoiceLog.count)
          expect(InvoiceLog).not_to receive(:create)
        end
      end
      context "given all correct keys with nil values" do
        it "it does not create anything" do
          @options = {
              person: nil,
              invoice: nil,
              braintree_transaction_id: nil,
              paypal_transaction_id: nil,
              batch_transaction_id: nil,
              payment_batch_id: nil,
              stored_paypal_account_id: nil,
              renewal_id: nil,
              message: nil,
              current_method_name: nil,
              caller_method_path: nil
          }
          InvoiceLogService.log(@options)

          expect(@initial_invoice_log_count).to eq(InvoiceLog.count)
          expect(InvoiceLog).not_to receive(:create)
        end
      end
      context "given a mixture" do
        it "it does not create anything" do
          @options = {
              greg: "greggy",
              book: "football",
              sport: :the_picture_of_dorian_gray,
              person: nil,
              invoice: nil,
              braintree_transaction_id: nil,
              paypal_transaction_id: nil,
              batch_transaction_id: nil,
              payment_batch_id: nil,
              stored_paypal_account_id: nil,
              renewal_id: nil,
              message: nil,
              current_method_name: nil,
              caller_method_path: nil
          }
          InvoiceLogService.log(@options)

          expect(@initial_invoice_log_count).to eq(InvoiceLog.count)
          expect(InvoiceLog).not_to receive(:create)
        end
      end
    end
  end
end
