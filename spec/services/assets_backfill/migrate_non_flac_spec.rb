require "rails_helper"

describe AssetsBackfill::MigrateNonFlac do

  let(:s3_asset) {create(:s3_asset)}
  let(:song) { create(:song) }

  subject {
    described_class.new({
      song: song,
      asset: s3_asset,
      destination_asset_key: 'destination_asset_key'
    })
  }

  before do
    ENV['ASSETS_BACKFILL_DESTINATION_BUCKET'] = 'destination_bucket'
  end

  describe '#run' do
    it 'returns nil on executing transcode_request' do
      response = {
        etsJobId: "1592549005598-hr0hii",
        status: "in-progress"
      }.to_json

      allow(subject).to receive(:transcode_request).and_return(response)

      expect(subject.run).to be(nil)
    end
  end

  describe '#post_body' do
    it 'returns post body' do
      expected = {
        callbackURL: "https://#{CALLBACK_API_URLS['US']}/api/transcoder/songs/set_asset_data?songId=#{song.id}",
        source: {
          key: "s3-asset-key",
          bucket: "s3.asset.bucket"
        },
        destination: {
          key: "destination_asset_key",
          bucket: "destination_bucket"
        },
        format: {
          extension: "flac"
        }
      }.to_json

      post_body = subject.post_body
      expect(post_body).to eq(expected)
    end

    context "when song is a ringtone and duration should be specified" do
      before do
        allow_any_instance_of(Album).to receive(:ringtone?).and_return(true)
      end

      it 'returns post body with ringtone specified' do
        expected = {
          callbackURL: "https://#{CALLBACK_API_URLS['US']}/api/transcoder/songs/set_asset_data?songId=#{song.id}",
          source: {
            key: "s3-asset-key",
            bucket: "s3.asset.bucket"
          },
          destination: {
            key: "destination_asset_key",
            bucket: "destination_bucket"
          },
          format: {
            extension: "flac"
          },
          duration: 30
        }.to_json
        post_body = subject.post_body
        expect(post_body).to eq(expected)
      end
    end
  end

  describe '#post_headers' do
    it 'return post headers' do
      expect(subject.post_headers["Content-Type"]).to eq("application/json")
    end
  end
end
