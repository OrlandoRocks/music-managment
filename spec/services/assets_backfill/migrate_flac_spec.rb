require "rails_helper"

describe AssetsBackfill::MigrateFlac do

  let(:s3_asset) {create(:s3_asset)}
  let(:song) { create(:song) }

  subject(:non_big_box_asset_class) {
    described_class.new({
      song: song,
      asset: s3_asset,
      destination_asset_key: "destination_asset_key",
    })
  }

  let(:big_box_s3_asset) { FactoryBot.create(:s3_asset, bucket: 'destination_bucket')}

  let(:ffprobe_uri) { double(:ffprobe_uri) }

  subject(:big_box_asset_class) {
    described_class.new({
      song: song,
      asset: big_box_s3_asset,
      destination_asset_key: "destination_asset_key",
    })
  }

  before do
    ENV['ASSETS_BACKFILL_DESTINATION_BUCKET'] = 'destination_bucket'
    client = Aws::S3::Client.new(stub_responses: true)
    allow_any_instance_of(described_class).to receive(:s3_client).and_return(client)
    allow(SecureRandom).to receive(:uuid).and_return('77da9c00-8b75-4741-94eb-1d9b1312e3d8')
    allow(ENV).to receive(:fetch).with('FFPROBE_URI').and_return(ffprobe_uri)
  end

  context 'when track is as per allowed encoding standards' do
    describe '#run' do
      context 'when asset is not on destination_bucket' do
        before do
          expect(HTTParty).to receive(:post).with(
            ffprobe_uri,
            headers: {
              "Content-Type": "application/json"
            },
            body: {
              source: {
                bucket: s3_asset.bucket,
                key: s3_asset.key
              }
            }.to_json
          ).and_return(OpenStruct.new({
            body: "{
              \"streams\": [{
                \"sample_rate\": \"44100\",
                \"sample_fmt\": \"s16\",
                \"duration\": \"142\",
                \"channels\": 2
              }]
            }",
            code: 200
          }))
        end

        it 'copy file on s3 bucket and return response when asset is not on destination_bucket' do

          expect(non_big_box_asset_class.s3_client).to receive(:copy_object).with(
            {
              copy_source: "#{s3_asset.bucket}/#{s3_asset.key}",
              key: "destination_asset_key",
              bucket: "destination_bucket",
            }
          )

          response = non_big_box_asset_class.run
          expect(response).to eq({
            create_s3_flac_asset: true,
            duration: 142,
            key: 'destination_asset_key'
          })
        end

        it 'returns true for valid_track?' do
          expect(non_big_box_asset_class.valid_track?).to eq(true)
        end

        it 'returns track duration' do
          expect(non_big_box_asset_class.duration).to eq(142)
        end

        it 'returns track bit_depth' do
          expect(non_big_box_asset_class.bit_depth).to eq("16")
        end

        it 'returns track sampling_rate' do
          expect(non_big_box_asset_class.sampling_rate).to eq("44100")
        end
      end

      context 'when asset is on destination_bucket' do
        it 'return response when asset is on destination_bucket' do
          expect(HTTParty).to receive(:post).with(
            ffprobe_uri,
            headers: {
              "Content-Type": "application/json"
            },
            body: {
              source: {
                bucket: big_box_s3_asset.bucket,
                key: big_box_s3_asset.key
              }
            }.to_json
          ).and_return(OpenStruct.new({
            body: "{
              \"streams\": [{
                \"sample_rate\": \"44100\",
                \"sample_fmt\": \"s16\",
                \"duration\": \"142\",
                \"channels\": 2
              }]
            }",
            code: 200
          }))

          expect(big_box_asset_class.s3_client).to_not receive(:copy_object)

          response = big_box_asset_class.run
          expect(response).to eq({
            create_s3_flac_asset: false,
            duration: 142,
            key: 'destination_asset_key'
          })
        end

        it 'return response when asset is on destination bucket and as per tunecore standard sampling rate and bit depth' do
          expect(HTTParty).to receive(:post).with(
            ffprobe_uri,
            headers: {
              "Content-Type": "application/json"
            },
            body: {
              source: {
                bucket: big_box_s3_asset.bucket,
                key: big_box_s3_asset.key
              }
            }.to_json
          ).and_return(OpenStruct.new({
            body: "{
              \"streams\": [{
                \"sample_rate\": \"44100\",
                \"sample_fmt\": \"s16\",
                \"duration\": \"142\",
                \"channels\": 2
              }]
            }",
            code: 200
          }))

          expect(big_box_asset_class.s3_client).to_not receive(:copy_object)

          response = described_class.new({
            song: song,
            asset: big_box_s3_asset,
            destination_asset_key: "destination_asset_key",
          }).run

          expect(response).to eq({
            create_s3_flac_asset: false,
            duration: 142,
            key: 'destination_asset_key'
          })
        end

        it 'calls migrate flac if track is not as per tunecore standard sampling rate and bit depth' do
          expect(HTTParty).to receive(:post).with(
            ffprobe_uri,
            headers: {
              "Content-Type": "application/json"
            },
            body: {
              source: {
                bucket: big_box_s3_asset.bucket,
                key: big_box_s3_asset.key
              }
            }.to_json
          ).and_return(OpenStruct.new({
            body: "{
              \"streams\": [{
                \"sample_rate\": \"44100\",
                \"sample_fmt\": \"s24\",
                \"duration\": \"142\"
              }]
            }",
            code: 200
          }))
          migrate_flac = double(:migrate_flac)
          allow_any_instance_of(AssetsBackfill::MigrateNonFlac).to receive(:run).and_return(migrate_flac)

          response = described_class.new({
            song: song,
            asset: big_box_s3_asset,
            destination_asset_key: "destination_asset_key",
          }).run

          expect(response).to eq(migrate_flac)
        end
      end
    end
  end

  context 'track is not as per believe standard' do
    before do
      expect(HTTParty).to receive(:post).with(
        ffprobe_uri,
        headers: {
          "Content-Type": "application/json"
        },
        body: {
          source: {
            bucket: s3_asset.bucket,
            key: s3_asset.key
          }
        }.to_json
      ).and_return(OpenStruct.new({
        body: "{
          \"streams\": [{
            \"sample_rate\": \"48000\",
            \"sample_fmt\": \"s16\",
            \"duration\": \"142\"
          }]
        }",
        code: 200
      }))
    end

    describe '#run' do
      it 'executes flac as non flac asset if not as per believe standard' do
        migrate_flac = double(:migrate_flac)
        allow_any_instance_of(AssetsBackfill::MigrateNonFlac).to receive(:run).and_return(migrate_flac)
        response = non_big_box_asset_class.run
        expect(response).to eq(migrate_flac)
      end
    end

    it 'returns false for valid_track?' do
      expect(non_big_box_asset_class.valid_track?).to eq(false)
    end
  end

  context 'track is a ringtone with wrong duration' do
    before do
      expect(HTTParty).to receive(:post).with(
        ffprobe_uri,
        headers: {
          "Content-Type": "application/json"
        },
        body: {
          source: {
            bucket: s3_asset.bucket,
            key: s3_asset.key
          }
        }.to_json
      ).and_return(OpenStruct.new({
        body: "{
          \"streams\": [{
            \"sample_rate\": \"44100\",
            \"sample_fmt\": \"s16\",
            \"duration\": \"200\"
          }]
        }",
        code: 200
      }))

      allow_any_instance_of(Album).to receive(:ringtone?).and_return(true)
    end

    describe '#run' do
      it 'executes flac as non flac asset if a ringtone but not valid duration' do
        migrate_flac = double(:migrate_flac)
        allow_any_instance_of(AssetsBackfill::MigrateNonFlac).to receive(:run).and_return(migrate_flac)
        response = non_big_box_asset_class.run
        expect(response).to eq(migrate_flac)
      end
    end

    it 'returns false for valid_track?' do
      expect(non_big_box_asset_class.valid_track?).to eq(false)
    end
  end
end
