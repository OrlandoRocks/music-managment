require "rails_helper"

describe AssetsBackfill::Base do
  subject { described_class.new }

  let(:song) { FactoryBot.create(:song, name: 'ThisIsASong') }
  let(:song_with_unsanitized_name) { FactoryBot.create(:song, name: "(Ночное, /'-(),\/\/ Такси)") }

  let(:song_with_large_unsanitized_name) {
    FactoryBot.create(:song, name: '(Ночное, Такси) (Ночное, Такси) (Ночное, Такси) (Ночное, Такси) (Ночное, Такси) (Ночное, Такси) (Ночное, Такси) (Ночное, Такси) (Ночное, Такси)')
  }

  let(:flac_asset) { FactoryBot.create(:s3_asset, key: 'asset.flac') }
  let(:non_flac_asset) { FactoryBot.create(:s3_asset, key: 'asset.wav') }

  let(:song_with_flac_s3_asset) { FactoryBot.create(:song, s3_asset: flac_asset) }
  let(:song_with_non_flac_s3_asset) { FactoryBot.create(:song, s3_asset: non_flac_asset) }

  let(:song_with_flac_s3_flac_asset) { FactoryBot.create(:song, s3_flac_asset: flac_asset) }
  let(:song_with_non_flac_s3_flac_asset) { FactoryBot.create(:song, s3_flac_asset: non_flac_asset) }

  let(:song_with_flac_s3_orig_asset) { FactoryBot.create(:song, s3_orig_asset: flac_asset) }
  let(:song_with_non_flac_orig_s3_asset) { FactoryBot.create(:song, bucket: non_flac_asset) }

  let(:migrate_response) { double(:migrate_response) }
  let(:migrate_flac_response) { double(:migrate_flac_response) }
  let(:migrate_non_flac_response) { double(:migrate_non_flac_response) }

  let(:finalized_album1) { create(:album, :finalized) }
  let!(:song_with_duration1) { create(:song, album: finalized_album1) }
  let!(:song_with_duration2) { create(:song, album: finalized_album1) }
  let!(:song_with_duration3) { create(:song, album: finalized_album1, s3_flac_asset: flac_asset) }

  let(:finalized_album2) { create(:album, :finalized) }
  let!(:song_with_duration4) { create(:song, album: finalized_album2) }
  let!(:song_with_duration5) { create(:song, album: finalized_album2) }
  let!(:song_with_duration6) { create(:song, album: finalized_album2, s3_flac_asset: flac_asset) }

  let(:finalized_album3) { create(:album, :finalized) }
  let!(:song_with_duration7) { create(:song, album: finalized_album3) }
  let!(:song_with_duration8) { create(:song, album: finalized_album3) }
  let!(:song_with_duration9) { create(:song, album: finalized_album3) }

  before do
    ENV['ASSETS_BACKFILL_DESTINATION_BUCKET'] = 'destination_bucket'
    allow(Time).to receive(:now).and_return(Time.new(2020, 6, 11, 0, 0, 0, '+00:00'))
  end

  describe '#run' do
    before do
      allow_any_instance_of(described_class).to receive(:migrate).and_return(migrate_response)
    end

    it 'calls migrate song for songs having s3_flac_asset_id and finalized album' do
      count = Song.includes(:album).where.not('albums.finalized_at': nil).where(s3_flac_asset_id: nil).count
      expect(subject).to receive(:migrate).exactly(count).times
      subject.run
    end

    it 'calls migrate for songs with s3_flac_asset_id nil of a particular album if album id is passed' do
      expect_any_instance_of(described_class).to receive(:migrate).exactly(4).times
      described_class.new(album_ids: [finalized_album1.id, finalized_album2.id]).run
    end

    it 'calls migrate for all songs of a particular album if album id is passed' do
      expect_any_instance_of(described_class).to receive(:migrate).exactly(6).times
      described_class.new(album_ids: [finalized_album1.id, finalized_album2.id], force: true).run
    end
  end

  describe '#migrate' do
    it 'return nil if no asset is associated with a song' do
      expect(subject.migrate(song)).to be(nil)
    end

    it 'return nil if response for process is null' do
      expect(subject).to receive(:process).with(song_with_flac_s3_asset, flac_asset).and_return(nil)
      expect(subject.migrate(song_with_flac_s3_asset)).to eq(nil)
    end

    it 'return response of parse_response if both process and parse_response return non nil' do
      process = double(:process)
      process_response = double(:process_response)
      expect(subject).to receive(:process).with(song_with_flac_s3_asset, flac_asset).and_return(process)
      expect(subject).to receive(:process_response).with(song: song_with_flac_s3_asset, asset: flac_asset, response: process).and_return(process_response)
      expect(subject.migrate(song_with_flac_s3_asset)).to eq(process_response)
    end
  end

  describe '#process' do
    before do
      allow_any_instance_of(AssetsBackfill::MigrateFlac).to receive(:run).and_return(migrate_flac_response)
      allow_any_instance_of(AssetsBackfill::MigrateNonFlac).to receive(:run).and_return(migrate_non_flac_response)
    end

    it 'process flac s3_asset' do
      expect(AssetsBackfill::MigrateFlac).to receive(:new).with({
        song: song_with_flac_s3_asset,
        asset: flac_asset,
        destination_asset_key: "#{song_with_flac_s3_asset.person.id}/1591833600000-1-#{song_with_flac_s3_asset.id}-This_Is_a_Song.flac"
      }).and_call_original

      response = subject.process(song_with_flac_s3_asset, flac_asset)
      expect(response).to eq(migrate_flac_response)
    end

    it 'process non flac s3_asset' do
      expect(AssetsBackfill::MigrateNonFlac).to receive(:new).with({
        song: song_with_non_flac_s3_asset,
        asset: non_flac_asset,
        destination_asset_key: "#{song_with_non_flac_s3_asset.person.id}/1591833600000-#{song_with_non_flac_s3_asset.track_num}-#{song_with_non_flac_s3_asset.id}-This_Is_a_Song.flac"
      }).and_call_original

      response = subject.process(song_with_non_flac_s3_asset, non_flac_asset)

      expect(response).to eq(migrate_non_flac_response)
    end
  end

  describe '#process_response' do
    it 'if create_s3_flac_asset is true, create flac asset and update duration and s3_flac_asset' do
      response = {
        create_s3_flac_asset: true,
        key: 'destination_asset_key',
        duration: 431
      }

      subject.process_response(song: song, asset: flac_asset, response: response)

      expect(song.duration).to eq(431)

      expect(song.s3_flac_asset.key).to eq('destination_asset_key')
      expect(song.s3_flac_asset.bucket).to eq('destination_bucket')
    end

    describe '#parse_transcoder_response' do
      it 'creates S3 asset and update song duration and s3_flac_asset if response state is complete' do
        completed_response = {
          state: "COMPLETED",
          songId: song.id,
          outputs: [
            {
              id: "1",
              status: "Complete",
              duration: 431,
              key: "destination_key"
            }
          ]
        }

        expect(subject).to receive(:process_response).with(
          song: song,
          response: {
            create_s3_flac_asset: true,
            duration: 431,
            key: "destination_key"
          }
        )
        subject.parse_transcoder_response(completed_response)
      end

      it 'returns nil if respnse state is not completed' do
        error_response = {
          state: "ERROR"
        }

        expect(subject.parse_transcoder_response(error_response)).to be nil
      end
    end

    it 'if create_s3_flac_asset is false, update duration and s3_flac_asset with existing asset' do
      response = {
        create_s3_flac_asset: false,
        key: 'destination_asset_key',
        duration: 429
      }

      subject.process_response(song: song, asset: flac_asset, response: response)

      expect(song.duration).to eq(429)
      expect(song.s3_flac_asset).to eq(flac_asset)
    end
  end

  describe '#get_assets' do
    it 'return nil if no asset is present' do
      expect(subject.get_s3_asset(song)).to be(nil)
    end

    it 'return s3_flac_asset' do
      expect(subject.get_s3_asset(song_with_non_flac_s3_flac_asset)).to eq(non_flac_asset)
    end

    it 'return s3_orig_asset if s3_flac_asset is nil' do
      expect(subject.get_s3_asset(song_with_flac_s3_orig_asset)).to eq(flac_asset)
    end

    it 'return s3_asset if both s3_orig_asset and s3_asset are nil' do
      expect(subject.get_s3_asset(song_with_flac_s3_asset)).to eq(flac_asset)
    end
  end

  it 'create new S3Asset on destination_bucket' do
    s3_asset = subject.create_s3_flac_asset('key')
    expect(s3_asset.key).to eq('key')
    expect(s3_asset.bucket).to eq('destination_bucket')
  end

  describe '#sanitized_name' do
    it 'return sanitized name for song with unsanitized name' do
      sanitized_song_name = subject.sanitized_name(song_with_unsanitized_name)
      expect(sanitized_song_name).to eq('_ночное___________такси_')
    end


    it 'return name for song with sanitized name' do
      song_name = subject.sanitized_name(song)
      expect(song_name).to eq('ThisIsASong')
    end

    it 'return name for song with 100 characters sanitized name if name is more than 100 characters' do
      song_name = subject.sanitized_name(song_with_large_unsanitized_name)
      expect(song_name).to eq('_ночное__такси___ночное__такси___ночное__такси___ночное__такси___ночное__такси___ночное__такси___ночн')
    end


  end
end
