require "rails_helper"

describe AssetsBackfill::S3OrigAsset do
  subject { described_class.new }
  backfill_path = "/home/deploy/tmp/backfill_s3_orig_asset_id.csv"
  fix_path = "/home/deploy/tmp/fix_s3_orig_asset_id.csv"

  let!(:person) { FactoryBot.create(:person) }

  before do
    FileUtils.mkdir_p '/home/deploy/tmp'
  end

  after do
    File.delete(fix_path) if File.exist?(fix_path)
    File.delete(backfill_path) if File.exist?(backfill_path)
  end

  describe '#backfill_missing' do
    describe 'overwrite is false' do
      let!(:flac_asset1) { FactoryBot.create(:s3_asset, bucket: 'bigbox.tunecore.com', key: "#{person.id}/ThisIsASong1.flac") }
      let!(:song1) { FactoryBot.create(:song, name: 'ThisIsASong1', person: person, s3_flac_asset_id: flac_asset1.id) }
      let!(:upload1) { FactoryBot.create(:upload, song: song1, uploaded_filename: 'ThisIsASong1.flac') }

      it 'does not update song' do
        subject.backfill_missing(false)
        song1.reload
        expect(song1.s3_orig_asset_id).to be_nil
        expect(CSV.read(backfill_path).last.last).to eq("flac_asset available")
      end
    end

    describe 'song asset id points to missing s3_asset record' do
      let!(:flac_asset1) { FactoryBot.create(:s3_asset, bucket: 'bigbox.tunecore.com', key: "#{person.id}/ThisIsASong1.flac") }
      let!(:song1) { FactoryBot.create(:song, name: 'ThisIsASong1', person: person, s3_flac_asset_id: 123456789) }
      let!(:upload1) { FactoryBot.create(:upload, song: song1, uploaded_filename: 'ThisIsASong1.flac') }

      it 'logs the error' do
        subject.backfill_missing(false)
        song1.reload
        expect(song1.s3_orig_asset_id).to be_nil
        expect(CSV.read(backfill_path).last.last).to eq("bad s3_flac_asset_id")
      end
    end

    describe 'with only matching s3_flac_asset_id' do
      let!(:flac_asset1) { FactoryBot.create(:s3_asset, bucket: 'bigbox.tunecore.com', key: "#{person.id}/ThisIsASong1.flac") }
      let!(:song1) { FactoryBot.create(:song, name: 'ThisIsASong1', person: person, s3_flac_asset_id: flac_asset1.id) }
      let!(:upload1) { FactoryBot.create(:upload, song: song1, uploaded_filename: 'ThisIsASong1.flac') }

      it 'copies s3_flac_asset_id to s3_orig_asset_id' do
        subject.backfill_missing(true)
        song1.reload
        expect(song1.s3_orig_asset_id).to eq(song1.s3_flac_asset_id)
        expect(CSV.read(backfill_path)[-2].last).to eq("flac_asset available")
        expect(CSV.read(backfill_path).last.last).to eq("updated song record")
      end
    end

    describe 'with only s3_flac_asset from wrong bucket' do
      let!(:flac_asset1) { FactoryBot.create(:s3_asset, bucket: 'wrong', key: "#{person.id}/ThisIsASong1.flac") }
      let!(:song1) { FactoryBot.create(:song, name: 'ThisIsASong1', person: person, s3_flac_asset_id: flac_asset1.id) }
      let!(:upload1) { FactoryBot.create(:upload, song: song1, uploaded_filename: 'ThisIsASong1.flac') }

      it 'does not copy the s3_flac_asset_id to s3_orig_asset_id' do
        subject.backfill_missing(true)
        song1.reload
        expect(song1.s3_orig_asset_id).to be_nil
        expect(CSV.read(backfill_path).last.last).to eq("flac_asset bad bucket")
      end
    end

    describe 'with both s3_asset_id and s3_flac_asset_id' do
      let!(:flac_asset2) { FactoryBot.create(:s3_asset, bucket: 'bigbox.tunecore.com', key: "#{person.id}/ThisIsASong2.flac") }
      let!(:s3_asset2) { FactoryBot.create(:s3_asset, bucket: 'bigbox.tunecore.com', key: "#{person.id}/ThisIsASong2.flac") }
      let!(:song2) { FactoryBot.create(:song, name: 'ThisIsASong2', person: person, s3_asset_id: s3_asset2.id, s3_flac_asset_id: flac_asset2.id) }
      let!(:upload2) { FactoryBot.create(:upload, song: song2, uploaded_filename: 'ThisIsASong2.flac') }

      it 'copies s3_asset_id to s3_orig_asset_id' do
        subject.backfill_missing(true)
        song2.reload
        expect(song2.s3_orig_asset_id).to eq(song2.s3_asset_id)
        expect(CSV.read(backfill_path).last.last).to eq("updated song record")
      end
    end

    describe 'with bad s3_asset_id and s3_flac_asset_id' do
      let!(:flac_asset3) { FactoryBot.create(:s3_asset, bucket: 'bigbox.tunecore.com', key: "#{person.id}/mismatch-filename.flac") }
      let!(:s3_asset3) { FactoryBot.create(:s3_asset, bucket: 'bigbox.tunecore.com', key: "#{person.id}/mismatch-filename.flac") }
      let!(:song3) { FactoryBot.create(:song, name: 'ThisIsASong3', person: person, s3_asset_id: s3_asset3.id, s3_flac_asset_id: flac_asset3.id) }
      let!(:upload3) { FactoryBot.create(:upload, song: song3, uploaded_filename: 'ThisIsASong3.flac') }

      it 'creates a new s3_asset' do
        allow(subject).to receive(:asset_lookup_by_key_successful?).and_return(true)
        subject.backfill_missing(true)
        song3.reload
        expect(song3.s3_orig_asset_id).should_not eq(song3.s3_asset_id)
        expect(song3.s3_orig_asset_id).should_not eq(song3.s3_flac_asset_id)
        expect(song3.s3_orig_asset_id).should_not be_nil
        expect(CSV.read(backfill_path)[-3].last).to eq("s3_asset_id does not match upload")
        expect(CSV.read(backfill_path)[-2].last).to eq("created new asset record from matching upload")
        expect(CSV.read(backfill_path).last.last).to eq("updated song record")
      end
    end

    describe 'with no assets to try' do
      let!(:song2) { FactoryBot.create(:song, name: 'ThisIsASong2', person: person) }
      let!(:upload2) { FactoryBot.create(:upload, song: song2, uploaded_filename: 'ThisIsASong2.flac') }

      it 'logs to file' do
        subject.backfill_missing(true)
        song2.reload
        expect(song2.s3_orig_asset_id).to be_nil
        expect(CSV.read(backfill_path).last.last).to eq('song does not have any associated asset')
      end
    end
  end

  describe '#fix_originals' do
    describe 'with good s3_orig_asset_id and bad s3_asset_id' do
      let!(:person) { FactoryBot.create(:person) }
      let!(:s3_asset1) { FactoryBot.create(:s3_asset, bucket: 'bigbox.tunecore.com', key: "#{person.id}/mismatch-filename.flac") }
      let!(:s3_orig_asset1) { FactoryBot.create(:s3_asset, bucket: 'bigbox.tunecore.com', key: "#{person.id}/ThisIsASong1.flac") }
      let!(:song1) { FactoryBot.create(:song, name: 'ThisIsASong1', person: person, s3_asset_id: s3_asset1.id, s3_orig_asset_id: s3_orig_asset1.id) }
      let!(:upload1) { FactoryBot.create(:upload, song: song1, uploaded_filename: 'ThisIsASong1.flac') }

      it 'does not overwrite s3_orig_asset_id with s3_asset_id' do
        subject.fix_originals(true)
        song1.reload
        expect(song1.s3_orig_asset_id).to eq(s3_orig_asset1.id)
      end
    end

    describe 'with bad s3_orig_asset_id and good s3_asset_id' do
      let!(:s3_asset2) { FactoryBot.create(:s3_asset, bucket: 'bigbox.tunecore.com', key: "#{person.id}/ThisIsASong2.flac") }
      let!(:s3_orig_asset2) { FactoryBot.create(:s3_asset, bucket: 'bigbox.tunecore.com', key: "#{person.id}/mismatch-filename.flac") }
      let!(:song2) { FactoryBot.create(:song, name: 'ThisIsASong2', person: person, s3_asset_id: s3_asset2.id, s3_orig_asset_id: s3_orig_asset2.id) }
      let!(:upload2) { FactoryBot.create(:upload, song: song2, uploaded_filename: 'ThisIsASong2.flac') }

      it 'overwrites s3_orig_asset_id with s3_asset_id' do
        subject.fix_originals(true)
        song2.reload
        expect(song2.s3_orig_asset_id).to eq(song2.s3_asset_id)
        expect(CSV.read(fix_path)[-3].last).to eq("s3_orig_asset does not match upload")
        expect(CSV.read(fix_path)[-2].last).to eq("s3_asset_id should overwrite s3_orig_asset_id")
        expect(CSV.read(fix_path).last.last).to eq("updated song record")
      end
    end

    describe 'with bad s3_orig_asset_id and bad s3_asset_id' do
      let!(:s3_asset2) { FactoryBot.create(:s3_asset, bucket: 'bigbox.tunecore.com', key: "#{person.id}/mismatch-filename.flac") }
      let!(:s3_orig_asset2) { FactoryBot.create(:s3_asset, bucket: 'bigbox.tunecore.com', key: "#{person.id}/mismatch-filename.flac") }
      let!(:song2) { FactoryBot.create(:song, name: 'ThisIsASong2', person: person, s3_asset_id: s3_asset2.id, s3_orig_asset_id: s3_orig_asset2.id) }
      let!(:upload2) { FactoryBot.create(:upload, song: song2, uploaded_filename: 'ThisIsASong2.flac') }

      it 'logs to file' do
        subject.fix_originals(true)
        song2.reload
        expect(song2.s3_orig_asset_id).to eq(s3_orig_asset2.id)
        expect(CSV.read(fix_path)[-2].last).to eq("s3_orig_asset does not match upload")
        expect(CSV.read(fix_path).last.last).to eq("s3_orig_asset and s3_asset don't match upload")
      end
    end
  end
end
