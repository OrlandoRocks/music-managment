require "rails_helper"

describe Soundout::Transcoder do
  let(:soundout_report) { create(:soundout_report, :with_track) }
  let(:song) { soundout_report.track }

  let(:s3_asset) { create(:s3_asset) }

  subject {
    described_class.new(
      {
        soundout_report: soundout_report
      }
    )
  }

  before(:each) do
    allow(song).to receive(:s3_asset).and_return(s3_asset)
  end

  describe '#run' do
    it 'returns nil on executing transcode_request' do
      response = {
        etsJobId: "1592549005598-hr0hii",
        status: "in-progress"
      }.to_json

      allow(subject).to receive(:transcode_request).and_return(response)

      expect(subject.run).to be(nil)
    end
  end

  describe '#post_body' do
    it 'returns post body' do
      cb_url = "https://#{CALLBACK_API_URLS['US']}/api/transcoder/soundout_reports/#{soundout_report.id}/complete_order"

      expected = {
        callbackURL: cb_url,
        source: {
          key: s3_asset.key,
          bucket: s3_asset.bucket
        },
        destination: {
          key: song.s3_streaming_key,
          bucket: song.s3_streaming_bucket
        },
        format: { extension: "mp3" },
      }.to_json

      post_body = subject.post_body
      expect(post_body).to eq(expected)
    end
  end
end
