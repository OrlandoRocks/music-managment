require 'rails_helper'

describe TranslationFetcherService do
  let(:person) { create(:person) }
  let(:translation_fetcher) { TranslationFetcherService.translations_for("javascripts/my_artists", COUNTRY_LOCALE_MAP[person.country_website.country]) }

  describe "#with_support_links" do
    it "adds a key to the translation fetcher with the knowlegebase link value" do
      translation_fetcher.with_support_links(contact_customer_care_link: :new_distribution_request)
      expect(translation_fetcher[:contact_customer_care_link]).to eq("//support.tunecore.com/hc/en-us/requests/new?ticket_form_id=661208")
    end
  end

  describe "#[]" do
    it "just works" do
      expect(translation_fetcher[:releases]).to eq("Releases")
    end

    it "falls back to english if translation is missing in that language" do
      french_translation_fetcher = TranslationFetcherService.translations_for("javascripts/my_artists", "fr")
      expect(french_translation_fetcher[:wizard][:remove_image]).to eq("Remove Image")
    end

    it "falls back to english if translation yml file is not present on system" do
      gibberish_translation_fetcher = TranslationFetcherService.translations_for("javascripts/my_artists", "gb")
      expect(gibberish_translation_fetcher[:wizard][:remove_image]).to eq("Remove Image")
    end
  end
end
