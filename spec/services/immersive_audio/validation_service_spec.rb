require "rails_helper"

describe ImmersiveAudio::ValidationService do
  let(:immersive_audio_key) { "s3_key" }
  let(:immersive_audio_bucket) { "s3_bucket" }
  let(:song) { create(:song, :with_s3_asset) }
  let(:s3_asset) { song.s3_asset }
  let(:audio_type) { SpatialAudio::AUDIO_TYPE }
  let(:duration) { "3000.00" }
  let(:metadata_json) {
    { "format" => { "format_name" => "flac", "size" => "42" }, "streams" => [{ "duration" => duration }] }
  }
  let(:media_metadata_json) {
    {
      "media"=>{
        "@ref"=> "foobar.com",
        "track"=>[
          {
            "@type"=>"General",
          },
          {
            "@type"=>"Audio",
            "extra"=>
            {
              "AdmProfile_Format"=>"Dolby Atmos Master"
            }
          }
        ]
      }
    }
  }

  subject(:validation_service) {
    ImmersiveAudio::ValidationService.new(s3_asset, immersive_audio_key, immersive_audio_bucket, audio_type)
  }

  before do
    # mock FFProbe call
    resp = instance_double(HTTParty::Response, body: metadata_json.to_json, code: 200)
    allow(HTTParty).to receive(:post).and_return(resp)

    # mock mediainfo call
    allow_any_instance_of(ImmersiveAudio::ValidationService).to receive(:media_metadata).and_return(media_metadata_json)
  end

  describe "#initialize" do
    context "when s3_detail not available for stereo files" do
      it "populates the s3_detail record" do
        expect_any_instance_of(S3Asset).to receive(:set_asset_metadata!).once.and_call_original
        validation_service
        expect(song.s3_detail.metadata_json).to eq(metadata_json)
      end
    end

    context "when s3_detail has not been updated after file replaced" do
      let!(:s3_detail) {
        create(:s3_detail, s3_asset: song.s3_asset, metadata_json: { "streams" => [{ "duration" => 100 }] })
      } 

      before do
        allow(s3_detail).to receive(:created_at).and_return(1.year.ago.to_time)
      end

      it "populates the s3_detail record" do
        expect_any_instance_of(S3Asset).to receive(:set_asset_metadata!).once.and_call_original
        validation_service
        expect(song.s3_detail.metadata_json).to eq(metadata_json)
      end
    end

    let(:expected_duration) { (duration.to_f * 1000) }

    it "fetches duration for stereo_files in milliseconds" do
      expect(validation_service.stereo_duration).to eq(expected_duration)
    end

    it "fetches duration for stero_files in milliseconds" do
      expect(validation_service.immersive_duration).to eq(expected_duration)
    end
  end

  describe "#call" do
    context "with s3_detail" do

      context "when validating duration" do
        let!(:s3_detail) {
          create(:s3_detail, s3_asset: song.s3_asset, metadata_json: { "streams" => [{ "duration" => stereo_duration }] })
        }

        context "when duration is within 50ms" do
          let(:stereo_duration) { duration }

          it "returns a hash with a key 'valid' with value of true and a key 'errors' with value of {}" do
            expect(validation_service.call).to eq({ valid: true, errors: {} })
          end
        end

        context "when duration is not with in 50ms" do
          let(:stereo_duration) { "2550.00" }

          it "populates a hash with key 'errors' with values of error types and a key 'valid' equal to false" do
            expect(validation_service.call).to eq(
              {
                valid: false,
                errors: { validate_duration: :immersive_duration_error }
              }
            )
          end
        end
      end

      context "when validating file format" do
        context "when file has the appropriate atmos file format" do
          let(:media_metadata_json) {
            {
              "media"=>{
                "@ref"=> "foobar.com",
                "track"=>[
                  {
                    "@type"=>"General",
                  },
                  {
                    "@type"=>"Audio",
                    "extra"=>
                    {
                      "AdmProfile_Format"=>"Dolby Atmos Master"
                    }
                  }
                ]
              }
            }
          }

          it "returns a hash with a key 'valid' with value of true and a key 'errors' with value of []" do
            expect(validation_service.call).to eq({ valid: true, errors: {} })
          end
        end

        context "when file does not have the appropriate atmos file format" do
          let(:media_metadata_json) {
            {
              "media"=>{
                "@ref"=> "foobar.com",
                "track"=>[
                  {
                    "@type"=>"General",
                  },
                  {
                    "@type"=>"Audio",
                  }
                ]
              }
            }
          }

          it "populates a hash with key 'errors' with values of error types and a key 'valid' equal to false" do
            expect(validation_service.call).to eq(
              {
                valid: false,
                errors: { validate_file_type: :atmos_file_type_error }
              }
            )
          end
        end
      end
    end

    context "when s3_asset not persisted" do
      let(:metadata_json) { { "streams" => [{ "duration" => stereo_duration }] } }
      let!(:s3_asset) { S3Asset.new(key: "new_key", bucket: "new_bucket") }

      before do
        allow(s3_asset).to receive(:fetch_asset_metadata).and_return(metadata_json)
      end

      context "when duration is within 50ms" do
        let(:stereo_duration) { duration }

        it "returns a hash with a key 'valid' with value of true and a key 'errors' with value of []" do
          expect(validation_service.call).to eq({ valid: true, errors: {} })
        end
      end

      context "when duration is not with in 50ms" do
        let(:stereo_duration) { duration }
        it "populates a hash with key 'errors' with values of error types and a key 'valid' equal to false" do
          allow_any_instance_of(ImmersiveAudio::ValidationService).to receive(:fetch_asset_metadata).and_return({ "streams" => [{ "duration" => "50" }] })

          expect(validation_service.call).to eq(
            {
              valid: false,
              errors: { validate_duration: :immersive_duration_error }
            }
          )
        end
      end
    end
  end
end
