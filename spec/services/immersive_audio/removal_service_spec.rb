require "rails_helper"
describe ImmersiveAudio::RemovalService do
  let!(:immersive_audio) { create(:immersive_audio) }
  let(:s3_client) { AWS::S3::Client.new }
  let(:s3) { AWS::S3.new }

  before do
    stub_const("S3_CLIENT", s3)
    allow(s3).to receive(:client).and_return(s3_client)
  end

  subject {
    described_class.new(options).call
  }

  context "with existing immersive_audio record" do
    let(:options) { { immersive_audio_id: immersive_audio.id } }
    it "deletes the immersive audio file from S3" do
      expect(s3_client).to receive(:delete_object).with(
        {
          key: immersive_audio.song_file.key,
          bucket_name: immersive_audio.song_file.bucket
        }
      )
      subject
    end

    it "destroys imemersive audio record" do
      expect { subject }.to change { ImmersiveAudio.count }.by(-1)
    end

    it "destroys the associated s3_asset" do
      expect { subject }.to change { S3Asset.count }.by(-1)
    end
  end

  context "without existing immersive_audio record" do
    let(:options) { { key: "immersive_asset_key", bucket: "bigbox.tunecore.com" } }

    it "deletes the immersive audio file from S3" do
      expect(s3_client).to receive(:delete_object).with(
        {
          key: "immersive_asset_key",
          bucket_name: "bigbox.tunecore.com"
        }
      )
      subject
    end
  end
end
