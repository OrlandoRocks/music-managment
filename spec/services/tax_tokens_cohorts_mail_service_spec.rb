require "rails_helper"

describe TaxTokensCohortsMailService do
  it "enqueues mail for a tax token cohort" do
    tax_tokens_cohort = create(:tax_tokens_cohort)
    4.times do
      person = create(:person)
      create(:tax_token, person: person, tax_tokens_cohort: tax_tokens_cohort, is_visible: true)
    end

    person = create(:person)
    create(:tax_token, person: person, tax_tokens_cohort: tax_tokens_cohort, is_visible: true, tax_form_type: 1)

    expect(MailerWorker).to receive(:perform_async).exactly(4).times
    TaxTokensCohortsMailService.send_mail_for_cohort(tax_tokens_cohort.id)
  end
end
