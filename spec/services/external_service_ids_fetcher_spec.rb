require "rails_helper"

describe ExternalServiceIds::ExternalServiceIdsFetcher do

  let(:album) { create(:album) }

  describe ".fetch" do

    it "instantiates a new instance of the service" do
      expect(ExternalServiceIds::ExternalServiceIdsFetcher).to receive(:new).with(album, "apple_album_id").and_call_original
      ExternalServiceIds::ExternalServiceIdsFetcher.fetch(album, "apple_album_id")
    end

    it "calls fetch on the new instance of the service" do
      expect_any_instance_of(ExternalServiceIds::ExternalServiceIdsFetcher).to receive(:fetch)
      ExternalServiceIds::ExternalServiceIdsFetcher.fetch(album, "apple_album_id")
    end
  end

  describe "#fetch" do

    it "calls fetch_spotify_uris when given 'spotify_uris' " do
      fake_spotify_service = double(search_by_upc: nil, get_tracks_for_album: nil)
      allow(SpotifyService).to receive(:new).and_return(fake_spotify_service)
      allow(album).to receive(:external_id_for).and_return(1234)
      service = ExternalServiceIds::ExternalServiceIdsFetcher.new(album, "spotify_uris")

      expect(service).to receive(:fetch_spotify_uris)
      service.fetch
    end

    it "calls fetch_apple_album_id when given 'apple_album_id' " do
      service = ExternalServiceIds::ExternalServiceIdsFetcher.new(album, "apple_album_id")

      expect(service).to receive(:fetch_apple_album_id)
      service.fetch
    end
  end
end
