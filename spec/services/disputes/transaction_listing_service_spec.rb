require "rails_helper"

RSpec.describe Disputes::TransactionListingService, type: :service do
  describe "#disputes" do
    context "filters" do
      it "filters disputes by source_type" do
        create(:dispute, :braintree)
        create(:dispute, :paypal)

        disputes = Disputes::TransactionListingService.new(
          source_type: DisputeStatus::BRAINTREE
        ).formatted_disputes

        expect(disputes.map(&:attributes).pluck("source_type").uniq)
          .to contain_exactly(DisputeStatus::SOURCE_TYPE_NAMES[DisputeStatus::BRAINTREE])
      end

      it "filters transactions by status" do
        status = "Accepted"
        dispute1 = create(:dispute, status: status)
        create(:dispute, status: "Rejected")

        disputes = Disputes::TransactionListingService
                   .new(status: dispute1.dispute_status.id)
                   .disputes

        expect(disputes.map(&:attributes).pluck("status").uniq)
          .to contain_exactly(status)
      end

      it "filters transactions by transaction_identifier" do
        transaction_identifier = "test123"

        create(:dispute, transaction_identifier: transaction_identifier)
        create(:dispute)

        disputes = Disputes::TransactionListingService
                   .new(transaction_identifier: transaction_identifier)
                   .disputes

        expect(disputes.pluck(:transaction_identifier).uniq)
          .to contain_exactly(transaction_identifier)
      end

      it "filters transactions by dispute_identifier" do
        dispute_identifier = "test123"

        create(:dispute, dispute_identifier: dispute_identifier)
        create(:dispute)

        disputes = Disputes::TransactionListingService
                   .new(dispute_identifier: dispute_identifier)
                   .disputes

        expect(disputes.pluck(:dispute_identifier).uniq)
          .to contain_exactly(dispute_identifier)
      end

      it "filters transactions by start_date" do
        start_date = "10/12/2021".to_date

        create(:dispute, initiated_at: start_date)
        create(:dispute, initiated_at: start_date - 1)

        disputes = Disputes::TransactionListingService
                   .new(start_date: start_date)
                   .disputes

        expect(disputes.pluck(:created_at))
          .to all(be >= start_date.beginning_of_day)
      end

      it "filters transactions by end_date" do
        end_date = "10/12/2021".to_date

        create(:dispute).update(created_at: end_date)
        create(:dispute).update(created_at: end_date + 1)

        disputes = Disputes::TransactionListingService
                   .new(end_date: end_date)
                   .disputes

        expect(disputes.pluck(:created_at))
          .to all(be <= end_date.to_date.end_of_day)
      end

      it "filters transactions by maximum_amount" do
        maximum_amount_cents = 1000
        create(:dispute, amount_cents: maximum_amount_cents)
        create(:dispute, amount_cents: maximum_amount_cents + 1)

        disputes = Disputes::TransactionListingService
                   .new(maximum_amount: maximum_amount_cents.fdiv(100))
                   .disputes

        expect(disputes.pluck(:amount_cents))
          .to all(be <= maximum_amount_cents)
      end

      it "filters transactions by minimum_amount" do
        minimum_amount_cents = 1000
        create(:dispute, amount_cents: minimum_amount_cents)
        create(:dispute, amount_cents: minimum_amount_cents - 1)

        disputes = Disputes::TransactionListingService
                   .new(minimum_amount: minimum_amount_cents.fdiv(100))
                   .disputes

        expect(disputes.pluck(:amount_cents))
          .to all(be >= minimum_amount_cents)
      end
    end
  end

  describe ".transaction_types" do
    it "returns transaction types with corresponding statuses" do
      braintree_statuses = DisputeStatus.where(source_type: "braintree")
      paypal_statuses = DisputeStatus.where(source_type: "paypal")
      payu_statuses = DisputeStatus.where(source_type: "payu")

      subject = Disputes::TransactionListingService.transaction_types

      expect(subject).to eq(
        [
          {
            source_type: DisputeStatus::SOURCE_TYPE_NAMES[DisputeStatus::BRAINTREE],
            statuses: braintree_statuses.map do |obj|
              {
                id: obj.id,
                status: obj.status.humanize
              }
            end
          },
          {
            source_type: DisputeStatus::SOURCE_TYPE_NAMES[DisputeStatus::PAYPAL],
            statuses: paypal_statuses.map do |obj|
              {
                id: obj.id,
                status: obj.status.humanize
              }
            end
          },
          {
            source_type: DisputeStatus::SOURCE_TYPE_NAMES[DisputeStatus::PAYU],
            statuses: payu_statuses.map do |obj|
              {
                id: obj.id,
                status: obj.status.humanize
              }
            end
          }
        ]
      )
    end
  end
end
