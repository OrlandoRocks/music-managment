require "rails_helper"

RSpec.describe Disputes::CurrencyConverter, type: :service do
  context "same currency" do
    let(:args) do
      {
        original_currency: "USD",
        payload_currency:  "USD",
        payload_amount:  99.9,
        dispute_date:  Time.zone.yesterday
      }
    end
    subject { described_class.new(args) }

    describe ".amount_in_original_currency" do
      it "returns payload amount" do
        expect(subject.amount_in_original_currency).to eq(args[:payload_amount])
      end
    end

    describe ".fx_rate" do
      it "returns nil" do
        expect(subject.fx_rate).to be_nil
      end
    end
  end

  context "different currency" do
    let(:args) do
      {
        original_currency: "USD",
        payload_currency:  "GBP",
        payload_amount:  99.9,
        dispute_date:  Time.zone.yesterday
      }
    end
    let!(:fx_rate) do
      create(
        :foreign_exchange_rate,
        source_currency: "GBP",
        target_currency: "USD",
        exchange_rate: 1.31,
        valid_from: Time.zone.yesterday.beginning_of_day
      )
    end
    subject { described_class.new(args) }

    describe ".amount_in_original_currency" do
      it "returns amount in original currency" do
        expect(subject.amount_in_original_currency)
          .to eq(args[:payload_amount] * fx_rate.exchange_rate)
      end
    end

    describe ".fx_rate" do
      it "returns nil" do
        expect(subject.fx_rate).to eq(fx_rate)
      end
    end
  end
end
