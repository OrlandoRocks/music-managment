require "rails_helper"

RSpec.describe Disputes::AmountUpdateService, type: :service do
  describe ".call!" do
    it "updates dispute with amount cents and foreign_exchange_rate" do
      fx_rate = create(:foreign_exchange_rate)
      dispute = create(:dispute, payload_amount: 100, payload_currency: "GBP", currency: "USD")
      currency_converter =
        instance_double(
          Disputes::CurrencyConverter,
          amount_in_original_currency: 100,
          fx_rate: fx_rate
        )

      expect(Disputes::CurrencyConverter)
        .to receive(:new) { currency_converter }

      described_class.call!(dispute.id)
      dispute.reload

      expect(dispute.amount_cents).to eq(currency_converter.amount_in_original_currency * 100)
      expect(dispute.foreign_exchange_rate).to eq(fx_rate)
    end
  end
end
