require "rails_helper"

RSpec.describe Disputes::BraintreeDispute, type: :service do
  let(:payin_provider_config) do
    PayinProviderConfig.find_by(name: PayinProviderConfig::BRAINTREE_NAME)
  end

  let(:gateway) do
    Braintree::ConfigGatewayService.new(payin_provider_config).gateway
  end

  describe "update_or_create_dispute!" do
    context "success" do
      it "creates dispute" do
        webhook_notification = gateway.webhook_testing.sample_notification(
          Braintree::WebhookNotification::Kind::DisputeOpened,
          "my_id"
        )
        parsed_response =
          Braintree::WebhookParser
          .new
          .parse!(webhook_notification[:bt_signature], webhook_notification[:bt_payload])
          .dispute

        invoice = create(
          :invoice,
          :with_braintree_settlement,
          transaction_id: parsed_response.transaction.id
        )

        Disputes::BraintreeDispute
          .new(webhook_notification[:bt_signature], webhook_notification[:bt_payload])
          .update_or_create_dispute!

        subject = Dispute.find_by(dispute_identifier: parsed_response.id)
        expect(subject).not_to be_nil
        expect(subject.invoice).to eq(invoice)
      end
    end

    context "failure" do
      it "raises InvalidDisputeError" do
        subject = Disputes::BraintreeDispute
                  .new("wrong", "wrong")
        expect { subject.update_or_create_dispute! }
          .to raise_error(Disputes::BraintreeDispute::InvalidDisputeError)
      end
    end
  end
end
