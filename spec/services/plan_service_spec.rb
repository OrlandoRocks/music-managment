require "rails_helper"

describe PlanService do
  let(:person) { create(:person) }
  subject { PlanService.new(person) }

  describe "#person_plan" do
    context "user with plan" do
      let!(:person_plan) { create(:person_plan, person: person) }

      it "returns person_plan" do
        expect(subject.person_plan).to eq person_plan
      end
    end

    context "user without plan" do
      it "returns nil" do
        expect(subject.person_plan).to be_nil
      end
    end
  end

  describe "#adjust_cart_plan!" do
    let!(:carted_plan_person) do
      person = create(:person)
      product = plan.products.find_by!(country_website: person.country_website)
      Product.add_to_cart(person, product, product)
      person
    end
    let(:plan) { Plan.last }
    subject { PlanService.new(carted_plan_person) }

    it "should replace the plan in cart" do
      new_plan = Plan.first
      expect(carted_plan_person.cart_plan.id).to eq(plan.id)
      expect(new_plan.id).to_not eq(plan.id)

      expect(subject.adjust_cart_plan!(new_plan.id)).to be(:success)
      expect(carted_plan_person.cart_plan.id).to eq(new_plan.id)
    end

    it "should raise exception when an invalid plan id is passed for replacement" do
      expect(subject.adjust_cart_plan!(0)).to be(:failure)
    end

    context "splits recipient" do
      let!(:split) { create(:royalty_split) }
      context "adding New Artist to cart" do
        let!(:new_artist_plan) { Plan.first }
        context "with pending splits invite" do
          before(:each) do
            create(:royalty_split_recipient, person: carted_plan_person, royalty_split: split)
          end
          it "should add splits collaborator to the cart" do
            expect_any_instance_of(Person).to receive(:add_splits_collaborator_to_cart!)
            subject.adjust_cart_plan!(new_artist_plan.id)
          end
        end
        context "with no pending splits invite" do
          it "should not add splits collaborator to the cart" do
            expect_any_instance_of(Person).not_to receive(:add_splits_collaborator_to_cart!)
            subject.adjust_cart_plan!(new_artist_plan.id)
          end
        end
        context "with only accepted splits invite" do
          before(:each) do
            create(:royalty_split_recipient, :accepted, person: carted_plan_person, royalty_split: split)
          end
          it "should not add splits collaborator to the cart" do
            expect_any_instance_of(Person).not_to receive(:add_splits_collaborator_to_cart!)
            subject.adjust_cart_plan!(new_artist_plan.id)
          end
        end
      end
      context "adding any plan besides New Artist to cart" do
        let!(:paid_plan) { Plan.last }
        it "should not add splits collaborator to the cart" do
          expect_any_instance_of(Person).not_to receive(:add_splits_collaborator_to_cart!)
          subject.adjust_cart_plan!(paid_plan.id)
        end
      end
    end
  end
end
