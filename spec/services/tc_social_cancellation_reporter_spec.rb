require "rails_helper"

describe TcSocialCancellationReporter do
  let(:subscription_events) { FactoryBot.create_list(:subscription_event, 4, subscription_type: "Social", event_type: "Cancellation") }
  let(:report_params) {
    {
      person_id: "1",
      subscription_created: "2017-01-01"
    }
  }

  let(:empty_params) {
    {
      person_id: "",
      subscription_created: ""
    }
  }

  let(:report_form) {TcSocialCancellationsReportForm.new(report_params)}

  before(:each) do
    stub_const("TcSocialCancellationReporter::PER_PAGE", 2)
    allow(TcSocialCancellationEventQueryBuilder).to receive(:build_query).and_return(subscription_events)
  end
  describe ".build_paginated_report" do
    it "returns the queried results" do
      expect(TcSocialCancellationEventQueryBuilder).to receive(:build_query).with(report_form)
      reporter = TcSocialCancellationReporter.build_paginated_report(report_form: report_form)
      expect(reporter.report.length).to eq(2)
      expect(reporter.report.total_pages).to eq(2)
    end
    context "with page num" do
      it "returns the correct page" do
        expect(TcSocialCancellationEventQueryBuilder).to receive(:build_query).with(report_form)
        reporter = TcSocialCancellationReporter.build_paginated_report(page: 2, report_form: report_form)
        expect(reporter.report.current_page).to eq(2)
        expect(reporter.report.length).to eq(2)
        expect(reporter.report.total_pages).to eq(2)
      end
    end

    context "without page num" do
      it "defaults to page 1" do
        expect(TcSocialCancellationEventQueryBuilder).to receive(:build_query).with(report_form)
        reporter = TcSocialCancellationReporter.build_paginated_report(report_form: report_form)
        expect(reporter.report.current_page).to eq(1)
      end
    end
  end

  describe ".build_csv_report" do
    it "returns queried results in CSV format" do
      expect(TcSocialCancellationEventQueryBuilder).to receive(:build_query).with(report_form)
      reporter = TcSocialCancellationReporter.build_csv_report(report_form: report_form)
      expect(reporter.report.class).to eq(String)
      expect(reporter.report.split("\n").length).to eq(5)
    end
  end
end
