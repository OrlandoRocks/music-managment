require "rails_helper"

describe RecaptchaService do
  describe ".verify" do
    context "without captcha params" do
      it "returns false" do
        expect(RecaptchaService.verify(nil, "127.0.0.1")).to eq false
      end
    end

    context "with captcha params" do
      context "success from API" do
        before do
          allow_any_instance_of(GoogleRecaptcha).to receive(:verify_recaptcha).and_return(true)
        end
        it "returns true" do
          expect(RecaptchaService.verify("captcha code", "127.0.0.1")).to eq true
        end
      end

      context "failure from API" do
        before do
          allow_any_instance_of(GoogleRecaptcha).to receive(:verify_recaptcha).and_return(false)
        end
        it "returns false" do
          expect(RecaptchaService.verify("bad captcha code", "127.0.0.1")).to eq false
        end
      end
    end
  end
end
