require "rails_helper"

describe PublishingClientImportReportService do
  let(:mailer) { double.as_null_object }

  it "should receive params, process and sends a mail to an admin" do
    person = create(:person)
    allow(PublishingReport).to receive(:client_import).and_return('report')
    expect(AdminNotifier).to receive(:client_import_report).and_return(mailer)
    client_import_service = PublishingClientImportReportService.new({person_id: person.id, start_date: '01/01/2018', end_date: '02/02/2019'})
    expect { client_import_service.process }.to change(Note, :count).by(1)
  end
end
