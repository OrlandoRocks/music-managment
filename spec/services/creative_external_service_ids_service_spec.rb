require "rails_helper"

describe CreativeExternalServiceIdsService do
  describe "::update" do
    context "when a creative is passed in" do
      context "when a present apple_artist_id is passed in" do
        context "when that creative has an apple external_service_id" do
          it "updates that external_service_id" do
            service_name = "apple"

            album = create(:album)
            creative = album.creatives.first
            external_service_id = create(:external_service_id,
              linkable: creative,
              service_name: service_name,
              identifier: "abc"
            )

            expect(ExternalServiceId.count).to eq(1)

            apple_artist_id = "def"

            params = { creative: creative, apple_artist_id: apple_artist_id }

            CreativeExternalServiceIdsService.update(params)

            expect(ExternalServiceId.count).to eq(1)

            external_service_id.reload

            expect(external_service_id.identifier).to eq(apple_artist_id)
          end
        end

        context "when that creative has no apple external_service_id" do
          it "updates that external_service_id" do
            service_name = "apple"

            album = create(:album)
            creative = album.creatives.first

            expect(ExternalServiceId.count).to eq(0)

            apple_artist_id = "def"

            params = { creative: creative, apple_artist_id: apple_artist_id }

            CreativeExternalServiceIdsService.update(params)

            expect(ExternalServiceId.count).to eq(1)

            external_service_id = creative.external_service_ids.first

            expect(external_service_id.service_name).to eq(service_name)
            expect(external_service_id.identifier).to eq(apple_artist_id)
          end
        end
      end

      context "when a blank apple_artist_id is passed in" do
        context "when that creative has an apple external_service_id" do
          it "deletes that external_service_id" do
            service_name = "apple"

            album = create(:album)
            creative = album.creatives.first
            external_service_id = create(:external_service_id,
              linkable: creative,
              service_name: service_name,
              identifier: "abc"
            )

            expect(ExternalServiceId.count).to eq(1)

            apple_artist_id = ""

            params = { creative: creative, apple_artist_id: apple_artist_id }

            CreativeExternalServiceIdsService.update(params)

            expect(ExternalServiceId.count).to eq(0)

            creative.reload

            expect(creative.external_service_ids.count).to eq(0)
          end

          it "does not delete an unrelated external_service_id" do
            service_name = "apple"

            album = create(:album)
            creative = album.creatives.first
            external_service_id1 = create(:external_service_id,
              linkable: creative,
              service_name: service_name,
              identifier: "abc"
            )
            external_service_id2 = create(:external_service_id,
              linkable: creative,
              service_name: "DoNotDeleteMe",
              identifier: "abc"
            )

            expect(ExternalServiceId.count).to eq(2)

            apple_artist_id = ""

            params = { creative: creative, apple_artist_id: apple_artist_id }

            CreativeExternalServiceIdsService.update(params)

            expect(ExternalServiceId.count).to eq(1)

            creative.reload

            expect(creative.external_service_ids.count).to eq(1)

            expect(creative.external_service_ids.first.id).to eq(external_service_id2.id)
          end
        end

        context "when that creative has no apple external_service_id" do
          it "does nothing" do
            service_name = "apple"

            album = create(:album)
            creative = album.creatives.first

            expect(ExternalServiceId.count).to eq(0)

            apple_artist_id = ""

            params = { creative: creative, apple_artist_id: apple_artist_id }

            CreativeExternalServiceIdsService.update(params)

            expect(ExternalServiceId.count).to eq(0)

            creative.reload

            expect(creative.external_service_ids.count).to eq(0)
          end
        end
      end

      context "when a present spotify_artist_id is passed in" do
        context "when that creative has a spotify external_service_id" do
          it "updates that external_service_id" do
            service_name = "spotify"

            album = create(:album)
            creative = album.creatives.first
            external_service_id = create(:external_service_id,
              linkable: creative,
              service_name: service_name,
              identifier: "abc"
            )

            expect(ExternalServiceId.count).to eq(1)

            spotify_artist_id = "def"

            params = { creative: creative, spotify_artist_id: spotify_artist_id }

            CreativeExternalServiceIdsService.update(params)

            expect(ExternalServiceId.count).to eq(1)

            external_service_id.reload

            expect(external_service_id.identifier).to eq(spotify_artist_id)
          end
        end

        context "when that creative has no spotify external_service_id" do
          it "updates that external_service_id" do
            service_name = "spotify"

            album = create(:album)
            creative = album.creatives.first

            expect(ExternalServiceId.count).to eq(0)

            spotify_artist_id = "def"

            params = { creative: creative, spotify_artist_id: spotify_artist_id }

            CreativeExternalServiceIdsService.update(params)

            expect(ExternalServiceId.count).to eq(1)

            external_service_id = creative.external_service_ids.first

            expect(external_service_id.service_name).to eq(service_name)
            expect(external_service_id.identifier).to eq(spotify_artist_id)
          end
        end
      end

      context "when a blank spotify_artist_id is passed in" do
        context "when that creative has an spotify external_service_id" do
          it "deletes that external_service_id" do
            service_name = "spotify"

            album = create(:album)
            creative = album.creatives.first
            external_service_id = create(:external_service_id,
              linkable: creative,
              service_name: service_name,
              identifier: "abc"
            )

            expect(ExternalServiceId.count).to eq(1)

            spotify_artist_id = ""

            params = { creative: creative, spotify_artist_id: spotify_artist_id }

            CreativeExternalServiceIdsService.update(params)

            expect(ExternalServiceId.count).to eq(0)

            creative.reload

            expect(creative.external_service_ids.count).to eq(0)
          end

          it "does not delete an unrelated external_service_id" do
            service_name = "spotify"

            album = create(:album)
            creative = album.creatives.first
            external_service_id1 = create(:external_service_id,
              linkable: creative,
              service_name: service_name,
              identifier: "abc"
            )
            external_service_id2 = create(:external_service_id,
              linkable: creative,
              service_name: "DoNotDeleteMe",
              identifier: "abc"
            )

            expect(ExternalServiceId.count).to eq(2)

            spotify_artist_id = ""

            params = { creative: creative, spotify_artist_id: spotify_artist_id }

            CreativeExternalServiceIdsService.update(params)

            expect(ExternalServiceId.count).to eq(1)

            creative.reload

            expect(creative.external_service_ids.count).to eq(1)

            expect(creative.external_service_ids.first.id).to eq(external_service_id2.id)
          end
        end

        context "when that creative has no spotify external_service_id" do
          it "does nothing" do
            service_name = "spotify"

            album = create(:album)
            creative = album.creatives.first

            expect(ExternalServiceId.count).to eq(0)

            spotify_artist_id = ""

            params = { creative: creative, spotify_artist_id: spotify_artist_id }

            CreativeExternalServiceIdsService.update(params)

            expect(ExternalServiceId.count).to eq(0)

            creative.reload

            expect(creative.external_service_ids.count).to eq(0)
          end
        end
      end

      context "when that creative has sibling creatives" do
        context "when present _artist_ids are passed in" do
          it "updates or creates the external_service_ids of that particular creative" do
            album = create(:album)
            artist = album.artists.first

            creative1 = album.creatives.first
            creative2 = create(:creative, creativeable: album, artist: artist)
            creative3 = create(:creative, creativeable: album, artist: artist)

            apple_external_service_id1 = create(:external_service_id,
              linkable: creative1,
              service_name: "apple",
              identifier: "abc"
            )

            apple_external_service_id2 = create(:external_service_id,
              linkable: creative2,
              service_name: "apple",
              identifier: "abc"
            )

            spotify_external_service_id1 = create(:external_service_id,
              linkable: creative1,
              service_name: "spotify",
              identifier: "123"
            )

            spotify_external_service_id3 = create(:external_service_id,
              linkable: creative3,
              service_name: "spotify",
              identifier: "123"
            )

            expect(ExternalServiceId.count).to eq(4)

            apple_artist_id = "def"
            spotify_artist_id = "456"

            params = { creative: creative1, apple_artist_id: apple_artist_id, spotify_artist_id: spotify_artist_id }

            CreativeExternalServiceIdsService.update(params)

            expect(ExternalServiceId.count).to eq(4)

            apple_external_service_id3 = creative3.external_service_ids.find_by(service_name: "apple")
            spotify_external_service_id2 = creative2.external_service_ids.find_by(service_name: "spotify")

            apple_external_service_id1.reload
            apple_external_service_id2.reload
            spotify_external_service_id1.reload
            spotify_external_service_id3.reload

            expect(apple_external_service_id1.identifier).to eq(apple_artist_id)

            expect(spotify_external_service_id1.identifier).to eq(spotify_artist_id)
          end
        end

        context "when blank _artist_ids are passed in" do
          it "deletes the present respective external_service_ids of the creative" do
            album = create(:album)
            artist = album.artists.first

            creative1 = album.creatives.first
            creative2 = create(:creative, creativeable: album, artist: artist)
            creative3 = create(:creative, creativeable: album, artist: artist)

            apple_external_service_id1 = create(:external_service_id,
              linkable: creative1,
              service_name: "apple",
              identifier: "abc"
            )

            apple_external_service_id2 = create(:external_service_id,
              linkable: creative2,
              service_name: "apple",
              identifier: "abc"
            )

            spotify_external_service_id1 = create(:external_service_id,
              linkable: creative1,
              service_name: "spotify",
              identifier: "123"
            )

            spotify_external_service_id3 = create(:external_service_id,
              linkable: creative3,
              service_name: "spotify",
              identifier: "123"
            )

            expect(ExternalServiceId.count).to eq(4)

            apple_artist_id = ""
            spotify_artist_id = ""

            params = { creative: creative1, apple_artist_id: apple_artist_id, spotify_artist_id: spotify_artist_id }

            CreativeExternalServiceIdsService.update(params)

            creative1.reload
            creative2.reload
            creative3.reload

            expect(creative1.external_service_ids.count).to eq(0)
            expect(creative2.external_service_ids.count).to eq(1)
            expect(creative3.external_service_ids.count).to eq(1)
          end
        end
      end
    end

    context "when a creative is not passed in" do
      it "does nothing" do
        album = create(:album)
        creative = album.creatives.first
        external_service_id1 = create(:external_service_id,
          linkable: creative,
          service_name: "apple",
          identifier: "abc"
        )
        external_service_id2 = create(:external_service_id,
          linkable: creative,
          service_name: "spotify",
          identifier: "123"
        )

        expect(ExternalServiceId.count).to eq(2)

        apple_artist_id = "def"
        spotify_artist_id = "456"

        params = { apple_artist_id: apple_artist_id, spotify_artist_id: spotify_artist_id }

        CreativeExternalServiceIdsService.update(params)

        expect(ExternalServiceId.count).to eq(2)

        creative.reload

        expect(creative.external_service_ids.count).to eq(2)
      end
    end
  end
end
