require "rails_helper"

describe PersonPlanCreationService do
  let(:args) {
    {
      person_id: person.id,
      plan_id: plan.id,
      purchase_id: purchase&.id,
      change_type: change_type,
      expires_at: expires_at,
      starts_at: starts_at
    }
  }
  let!(:expires_at) {nil}
  let!(:starts_at) {nil}
  let!(:invoice) {  create(:invoice, person: person) }
  let(:person) { create(:person) }
  let(:product) { plan.product_by_person(person) }
  let(:purchase) {
    create(:purchase,
      person_id: person.id,
      related: product,
      product: product,
      invoice: invoice)
    }

  describe "#change_type" do
    subject(:service) { described_class.new(args).change_type }
    context "when a user does not have a plan" do
      let(:plan) { Plan.first }
      let(:expected_change_type) { PersonPlanHistory::INITIAL_PURCHASE }

      context "when no change_type is passed to the service" do
        let(:change_type) { nil }

        it "sets the change_type as '#{PersonPlanHistory::INITIAL_PURCHASE}'" do
          expect(subject).to eq(expected_change_type)
        end
      end

      context "when incorrect change_type is provided" do
        let(:change_type) { "im_tryna_break_the_service" }

        it "sets the change_type as '#{PersonPlanHistory::INITIAL_PURCHASE}'" do
          expect(subject).to eq(expected_change_type)
        end
      end

      context "when change_type if 'initial_purchase' is provided" do
        let(:change_type) { PersonPlanHistory::INITIAL_PURCHASE }

        it "sets the change_type as '#{PersonPlanHistory::INITIAL_PURCHASE}'" do
          expect(subject).to eq(expected_change_type)
        end
      end
    end

    context "when a user has an existing plan" do
      let!(:existing_person_plan) {
        create(
        :person_plan,
        plan_id: Plan.first.id,
        person: person
        )
      }
      let!(:plan_renewal) { create(:renewal, :plan_renewal, person: person, expires_at: Date.today + 6.months ) }

      context "when new plan is an upgrade" do
        let(:plan) { Plan.third }
        let(:expected_change_type) { PersonPlanHistory::USER_UPGRADE }

        context "when no change_type is passed to the service" do
          let(:change_type) { nil }

          it "sets the change_type as '#{PersonPlanHistory::USER_UPGRADE}'" do
            expect(subject).to eq(expected_change_type)
          end
        end

        context "when incorrect change_type is provided" do
          let(:change_type) { "im_tryna_break_it_again" }

          it "sets the change_type as '#{PersonPlanHistory::USER_UPGRADE}'" do
            expect(subject).to eq(expected_change_type)
          end
        end

        context "when change_type of '#{PersonPlanHistory::USER_UPGRADE}' is provided" do
          let(:change_type) { PersonPlanHistory::USER_UPGRADE }

          it "sets the change_type as '#{PersonPlanHistory::USER_UPGRADE}'" do
            expect(subject).to eq(expected_change_type)
          end
        end
      end

      context "when new plan is a renewal" do
        let(:plan) { Plan.first }
        let(:expected_change_type) { PersonPlanHistory::RENEWAL }

        context "when no change_type is passed to the service" do
          let(:change_type) { nil }

          it "sets the change_type as '#{PersonPlanHistory::RENEWAL}'" do
            expect(subject).to eq(expected_change_type)
          end
        end

        context "when incorrect change_type is provided" do
          let(:change_type) { "im_still_tryna_break_it" }

          it "sets the change_type as '#{PersonPlanHistory::RENEWAL}'" do
            expect(subject).to eq(expected_change_type)
          end
        end

        context "when change_type of '#{PersonPlanHistory::RENEWAL}' is provided" do
          let(:change_type) { PersonPlanHistory::RENEWAL }

          it "sets the change_type as '#{PersonPlanHistory::RENEWAL}'" do
            expect(subject).to eq(expected_change_type)
          end
        end
      end

      context "when new plan is a downgrade" do
        let!(:existing_person_plan) {
          create(
          :person_plan,
          plan_id: Plan.third.id,
          person: person
          )
        }
        let!(:plan_renewal) { create(:renewal, :plan_renewal, person: person, expires_at: Date.today + 6.months ) }
        let(:plan) { Plan.first }


        context "when no change_type is passed to the service" do
          let(:change_type) { nil }
          let!(:expected_change_type) { PersonPlanHistory::DEFAULT_DOWNGRADE }

          it "sets the change_type as #{PersonPlanHistory::DEFAULT_DOWNGRADE}" do
            subject
            expect(Airbrake).to_not receive(:notify)
            expect(subject).to eq(expected_change_type)
            expect { subject }.to_not raise_error
          end
        end

        context "when incorrect change_type is provided" do
          let(:change_type) { "its_going_to_break_this_time" }

          it "raises an error" do
            expect { subject }.to raise_error(
              PersonPlanCreationService::PurchasePlanCreationServiceError,
              "Change Type Is Invalid for person_id: #{person.id } plan_id: #{plan.id}"
            )
          end
        end

        context "when change_type of '#{PersonPlanHistory::FORCED_DOWNGRADE}' is provided" do
          let(:change_type) { PersonPlanHistory::FORCED_DOWNGRADE }

          it "sets the change_type to '#{PersonPlanHistory::FORCED_DOWNGRADE}'" do
            expect(subject).to eq(PersonPlanHistory::FORCED_DOWNGRADE)
          end
        end

        context "when change_type of '#{PersonPlanHistory::REQUESTED_DOWNGRADE}' is provided" do
          let(:change_type) { PersonPlanHistory::REQUESTED_DOWNGRADE}

          it "sets the change_type to '#{PersonPlanHistory::REQUESTED_DOWNGRADE}'" do
            expect(subject).to eq(PersonPlanHistory::REQUESTED_DOWNGRADE)
          end
        end
      end
    end
  end

  describe "#start_date" do
    subject(:service) { described_class.new(args).start_date}

    let(:change_type) { PersonPlanHistory::RENEWAL }
    let(:plan) { Plan.first }

    context "when service is invoked with an explicit start date" do
      let(:fixed_time) { Time.new(2007, 10, 31) }
      let(:starts_at) { fixed_time }

      it "the start date is set to the passed value" do
        expect(subject).to eq(fixed_time)
      end
    end

    context "when service is called without passing an explicit start date" do

      it "the start date is set to today's date" do
        expect(subject).to eq(Date.today)
      end
    end
  end

  describe "#expiration_date" do
    subject(:service) { described_class.new(args).expiration_date}

    let(:change_type) { PersonPlanHistory::RENEWAL }
    let(:plan) { Plan.first }

    context "when service is invoked with an explicit expiration date" do
      let(:fixed_time) { Time.new(2007, 10, 31) }
      let(:expires_at) { fixed_time }

      it "the expiration date is set to the passed value" do
        expect(subject).to eq(fixed_time)
      end
    end

    context "when service is called without passing an explicit expiration date" do

      it "the expiration date is set to 1 year from today" do
        expect(subject).to eq(Date.today + 1.year)
      end
    end
  end


  describe "#call" do
    subject(:service) { described_class.new(args).call }

    before do
      Timecop.freeze(Time.local(2020))
    end

    after do
      Timecop.return
    end

    context "when user does not have a plan" do
      let(:change_type) { PersonPlanHistory::INITIAL_PURCHASE }
      let(:plan) { Plan.first }

      it "creates a person plan, renewal and plan history" do
        expect { subject }.to change { PersonPlanHistory.count }.by(1)
          .and change { PersonPlan.count }.by(1)
          .and change { Renewal.count }.by(1)
      end

      it "creates a person_plan with correct plan and person" do
        subject
        expect(PersonPlan.find_by(person: person)).to have_attributes(plan_id: plan.id, person_id: person.id)
      end

      it "creates a person_plan_histories entry with 'initial_purchase'" do
        subject
        person_plan_history = PersonPlanHistory.find_by(person: person, plan: plan)
        expect(person_plan_history).to have_attributes(
          change_type: change_type,
          discount_reason: Purchase::DEFAULT_DISCOUNT_REASON.gsub(" ","_"),
          plan_start_date: Date.today.to_time,
          plan_end_date: (Date.today + 1.year).to_time
        )
      end

      context "no purchase provided" do
        let!(:purchase) { nil }

        it "raises error and does not commit changes" do
          expect(Airbrake).to receive(:notify)
          expect { subject }.to raise_error(NoMethodError)
          expect(PersonPlanHistory.where(person: person).count).to eq(0)
        end
      end
    end

    context "when person already has a plan" do
      let(:change_type) { PersonPlanHistory::USER_UPGRADE }
      let(:plan) { Plan.fourth }
      let!(:existing_person_plan) { create(:person_plan, plan_id: 1, person: person) }

      it "creates a new renewal and plan history record" do
        expect { subject }.to change { PersonPlanHistory.count }.by(1)
          .and change { PersonPlan.count }.by(0)
          .and change { Renewal.count }.by(1)
      end

      it "updates person plan with correct plan and person" do
        subject
        expect(PersonPlan.find_by(person: person)).to have_attributes(plan_id: plan.id, person_id: person.id)
      end

      it "creates a person_plan_histories entry with 'user_upgrade'" do
        subject
        person_plan_history = PersonPlanHistory.find_by(person: person, plan: plan)
        expect(person_plan_history).to have_attributes(
          change_type: change_type,
          discount_reason: Purchase::DEFAULT_DISCOUNT_REASON.gsub(" ","_"),
          plan_start_date: Date.today.to_time,
          plan_end_date: (Date.today + 1.year).to_time
        )
      end

      context "no purchase provided" do
        let!(:purchase) { nil }

        it "raises error and does not commit changes" do
          expect(Airbrake).to receive(:notify)
          expect { subject }.to raise_error(NoMethodError)
          expect(PersonPlan.where(person: person, plan: plan).count).to eq(0)
        end
      end
    end

    context "When there is an invalid change_reason passed to the service" do
      let(:change_type) { :spare_some_change }
      let(:plan) { Plan.third }

      context "when user does not have a plan" do
        it "creates the plan, renewal and plan history" do
          expect { subject }.to change { PersonPlanHistory.count }.by(1)
            .and change { PersonPlan.count }.by(1)
            .and change { Renewal.count }.by(1)
        end

        it "creates the plan history with 'initial_purchase' change type" do
          subject
          expect(PersonPlanHistory.find_by(person: person, plan: plan)).to have_attributes(
            change_type: PersonPlanHistory::INITIAL_PURCHASE,
            discount_reason: Purchase::DEFAULT_DISCOUNT_REASON.gsub(" ","_")
          )
        end
      end

      context "when user has a plan and is downgrading" do
        let!(:existing_person_plan) {
          create(:person_plan,
          plan_id: Plan.fourth.id,
          person: person)
          }
        let!(:plan_renewal) { create(:renewal, :plan_renewal, person: person, expires_at: Date.today + 6.months ) }
        
        it "notifies Airbrake and raises the error" do
          expect(Airbrake).to receive(:notify).once
          expect { subject }.to raise_error(PersonPlanCreationService::PurchasePlanCreationServiceError)
        end

      end
    end

    context "When there is no change_type passed to the service" do
      let(:change_type) { nil }
      let(:plan) { Plan.fourth }

      context "when user does not have a plan" do
        it "creates the plan, renewal and plan history" do
          expect { subject }.to change { PersonPlanHistory.count }.by(1)
            .and change { PersonPlan.count }.by(1)
            .and change { Renewal.count }.by(1)
        end

        it "creates the plan history with 'initial_purchase' change type" do
          subject
          expect(PersonPlanHistory.find_by(person: person, plan: plan)).to have_attributes(
            change_type: PersonPlanHistory::INITIAL_PURCHASE,
            discount_reason: Purchase::DEFAULT_DISCOUNT_REASON.gsub(" ","_")
          )
        end
      end

      context "when user has a plan" do
        let!(:existing_person_plan) {
          create(:person_plan,
            plan_id: Plan.first.id,
            person: person)
          }

        it "creates renewal and plan history" do
          expect { subject }.to change { PersonPlanHistory.count }.by(1)
            .and change { PersonPlan.count }.by(0)
            .and change { Renewal.count }.by(1)
        end

        it "creates the plan history with 'user_upgrade' change type" do
          subject
          expect(PersonPlanHistory.find_by(person: person, plan: plan)).to have_attributes(
            change_type: PersonPlanHistory::USER_UPGRADE,
            discount_reason: Purchase::DEFAULT_DISCOUNT_REASON.gsub(" ","_")
          )
        end
      end
    end

    context "when the plan is downgraded" do
      let(:plan) { Plan.first }
      let!(:existing_person_plan) {
        create(:person_plan,
        plan_id: Plan.fourth.id,
        person: person)
      }
      let!(:plan_renewal) { create(:renewal, :plan_renewal, person: person, expires_at: Date.today + 6.months ) }

      context "when no change_type is provided" do
        let(:change_type) { nil }

        it "creates the plan history with the default downgrade change type" do
          subject
          expect(Airbrake).to_not receive(:notify)
          expect(PersonPlanHistory.find_by(person: person, plan: plan)).to have_attributes(
            change_type: PersonPlanHistory::DEFAULT_DOWNGRADE,
            discount_reason: Purchase::DEFAULT_DISCOUNT_REASON.gsub(" ","_")
          )
          expect { subject }.to_not raise_error
        end
      end

      context "no purchase provided" do
        let!(:change_type) { nil }
        let!(:purchase) { nil }

        context "default/forced downgrade" do
          it "raises error and does not commit changes" do
            expect(Airbrake).to receive(:notify)
            expect { subject }.to raise_error(NoMethodError)
            expect(PersonPlan.where(person: person, plan: plan).count).to eq(0)
          end
        end

        context "requested downgrade" do
          let!(:change_type) { PersonPlanHistory::REQUESTED_DOWNGRADE }
          it "does not raise error" do
            expect(Airbrake).to_not receive(:notify)
            expect { subject }.to_not raise_error
            expect(PersonPlan.where(person: person, plan: plan).count).to eq(1)
          end
        end

      end

    end

  end
end
