require "rails_helper"

RSpec.describe AutoRefundService, type: :service do
  describe "#process" do
    context "admin without new_refunds feature flag" do
      it "returns nil" do
        admin = create(:person)

        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .with(:new_refunds, admin)
          .and_return(false)

        subject = described_class.new(admin, {})
        subject.process

        expect(subject.refund).to be_nil
      end
    end

    context "admin with new_refunds feature flag" do
      let(:admin) { create(:person, :with_role, role_name: Role::REFUNDS) }
      let(:invoice) { create(:invoice, :with_two_purchases, :settled) }
      let(:refund_reason) { create(:refund_reason) }

      before(:each) do
        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .and_call_original

        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .with(:new_refunds, admin)
          .and_return(true)
      end

      it "returns error without correct arguments" do
        args = {
          invoice_id: invoice.id,
          refund_level: Refund::INVOICE,
          refund_type: Refund::FIXED_AMOUNT,
          request_amount: (invoice.final_settlement_amount_cents + 2).fdiv(100),
          tax_inclusive: true,
          refund_reason_id: refund_reason.id,
          refunded_by_id: admin.id
        }

        subject = described_class.new(admin, args)
        subject.process

        expect(subject.success?).to be_falsy
        expect(subject.error).to_not be_nil
      end

      it "process refund" do
        args = {
          invoice_id: invoice.id,
          refund_level: Refund::INVOICE,
          refund_type: Refund::FULL_REFUND,
          request_amount: 100,
          tax_inclusive: true,
          refund_reason_id: refund_reason.id,
          refunded_by_id: admin.id
        }

        subject = described_class.new(admin, args)
        subject.process

        expect(subject.success?).to be_truthy
        expect(subject.error).to be_nil
      end
    end
  end
  describe "#process_dispute_refund" do
    context "admin without new_refunds feature flag" do
      it "returns nil" do
        admin = create(:person)

        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .with(:new_refunds, admin)
          .and_return(false)

        subject = described_class.new(admin, {})
        subject.process_dispute_refund

        expect(subject.refund).to be_nil
      end
    end

    context "admin with new_refunds feature flag" do
      let(:admin) { create(:person, :with_role, role_name: Role::REFUNDS) }
      let(:invoice) { create(:invoice, :with_two_purchases, :settled) }
      let(:refund_reason) { create(:refund_reason, reason: "Chargebacks") }
      let(:dispute) { create(:dispute, amount_cents: 10, processed_by: admin) }

      before(:each) do
        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .and_call_original

        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .with(:new_refunds, admin)
          .and_return(true)
      end

      it "process dispute refund" do
        args = {
          invoice_id: invoice.id,
          refund_level: Refund::INVOICE,
          refund_type: Refund::FIXED_AMOUNT,
          request_amount: dispute.amount_cents,
          tax_inclusive: true,
          refund_reason_id: refund_reason.id,
          refunded_by_id: dispute.processed_by.id
        }

        subject = described_class.new(admin, args)
        subject.process_dispute_refund

        expect(subject.success?).to be_truthy
        expect(subject.error).to be_nil
      end
    end
  end
end
