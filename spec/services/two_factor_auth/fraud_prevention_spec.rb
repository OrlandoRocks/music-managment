require "rails_helper"

describe TwoFactorAuth::FraudPrevention do
  describe ".request_banned?" do
    it "should return nil for non cll request method" do
      expect(TwoFactorAuth::FraudPrevention.request_banned?(1, 'sms')).to be_falsey
    end

    it "should return value from tfa_call_banned?" do
      allow_any_instance_of(TwoFactorAuth::FraudPrevention)
        .to receive(:tfa_call_banned?)
        .and_return(true)

      expect(TwoFactorAuth::FraudPrevention.request_banned?(1, 'call')).to be_truthy
    end
  end

  describe "#register_request" do
    let(:fp_service) { TwoFactorAuth::FraudPrevention.new(1) }

    it "should return nil for non cll request method" do
      expect(fp_service.register_request('sms')).to eq(nil)
    end

    it "should return nil if tfa call banned" do
      allow(fp_service)
        .to receive(:tfa_call_banned?)
        .and_return(true)

      expect(fp_service.register_request('call')).to eq(nil)
    end

    it "should institue a ban after 3 times registering within a minute" do
      expect(fp_service.tfa_call_banned?).to be_falsey

      3.times do
        fp_service.register_request('call')
      end

      expect(fp_service.tfa_call_banned?).to be_truthy
    end
  end
end
