require "rails_helper"

describe TwoFactorAuth::Steps::Preference do

  before(:each) do
    allow(FeatureFlipper).to receive(:show_feature?).with(:verify_2fa, person).and_return(true)
    allow_any_instance_of(TwoFactorAuth::ApiClient).to receive(:log).and_return(true)
  end

  describe "#check" do
    let(:person) { create(:person, :with_two_factor_auth) }
    let(:params) { {page: "enrollments", country_code: "1", phone_number: "8608172397", notification_method: "sms"} }

    context "when a user is on the enrollments form" do
      context "and has under the max 2FA signup events" do
        it "allows the creation of a new 2FA event" do
          tfa_preference = TwoFactorAuth::Steps::Preference.new
          count = person.two_factor_auth_events.count
          allow(tfa_preference).to receive(:make_update).and_return(true)
          allow_any_instance_of(TwoFactorAuth::ApiClient).to receive(:validate_phone_number).and_return('+18608172397')

          expect(count).to eq(0)
          TwoFactorAuth::Steps::Preference.check(person, params)
          count = person.two_factor_auth_events.count
          expect(count).to eq(1)
        end
      end

      context "and has more than the max 2FA signup events" do
        it "does not create a new 2FA event" do
          3.times do
            create(:two_factor_auth_event, two_factor_auth: person.two_factor_auth, action: "authentication", type: "authentication", page: "enrollments", created_at: 1.hour.ago)
          end

          tfa_preference = TwoFactorAuth::Steps::Preference.new
          allow(tfa_preference).to receive(:make_update).and_return(true)
          allow_any_instance_of(TwoFactorAuth::ApiClient).to receive(:validate_phone_number).and_return('+18608172397')

          TwoFactorAuth::Steps::Preference.check(person, params)
          count = person.two_factor_auth_events.count
          expect(count).to eq(3)
        end
      end
    end
  end
end
