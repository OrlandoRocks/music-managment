require "rails_helper"

describe TwoFactorAuth::TokenService do
  before(:each) do
    allow(ENV).to receive(:[]).and_return(ENV)
    allow(ENV).to receive(:[]).with("JWT_ENCRYPTION_KEY").and_return("123357890")
    allow(ENV).to receive(:[]).with("JWT_ALGORITHM").and_return("HS256")
  end

  describe ".issue" do
    it "makes a new token" do
      person = create(:person)
      token = TwoFactorAuth::TokenService.issue({ email: person.email })
      expect(token).not_to eq nil
    end
  end

  describe ".decode" do
    it "decodes the message" do
      Timecop.freeze
      person = create(:person)
      token = TwoFactorAuth::TokenService.issue({ email: person.email })
      expect(TwoFactorAuth::TokenService.decode(token).first).to eq(
        { "email" => person.email,
          "exp"   => (Time.now + TwoFactorAuth::AUTHENTICATION_WINDOW).to_i })
      Timecop.return
    end

    it "returns empty hash if token expired" do
      Timecop.freeze
      person = create(:person)
      token = TwoFactorAuth::TokenService.issue({ email: person.email })
      Timecop.travel(DateTime.now + 5.minutes)
      expect(TwoFactorAuth::TokenService.decode(token)).to eq([{}])
      Timecop.return
    end
  end
end
