require "rails_helper"

describe TwoFactorAuth::ApiClient do
  before(:each) do
    @person = create(:person)
    @tfa    = create(:two_factor_auth, person: @person)
    allow_any_instance_of(TwoFactorAuth::ApiClient).to receive(:log).and_return(true)
  end

  describe "#request_authorization" do
    it "should register the request with fraud prevention service" do
      allow(@tfa).to receive(:notification_method).and_return("sms")

      expect_any_instance_of(TwoFactorAuth::FraudPrevention)
        .to receive(:register_request)
        .with('sms')

      TwoFactorAuth::ApiClient.new(@person, @tfa).request_authorization
    end

    context "when notification_method is sms/text" do
      it "requests verification via sms" do
        allow(@tfa).to receive(:notification_method).and_return("sms")
        expect(@tfa.notification_method).to eq "sms"

        client = TwoFactorAuth::ApiClient.new(@person, @tfa, {})

        expect(client).to receive(:request_via_sms)
        client.request_authorization
      end
    end

    context "when notification_method is phone call" do
      before { allow(@tfa).to receive(:notification_method).and_return("call") }

      it "requests verification via call" do
        expect(@tfa.notification_method).to eq "call"

        client = TwoFactorAuth::ApiClient.new(@person, @tfa, {})

        expect(client).to receive(:request_via_call)
        client.request_authorization
      end

      it "should not request verification when ban in place" do
        allow_any_instance_of(TwoFactorAuth::FraudPrevention)
          .to receive(:tfa_call_banned?)
          .and_return(true)

        client = TwoFactorAuth::ApiClient.new(@person, @tfa, {})
        api = client.client

        expect(api.verify.v2.services(ENV["VERIFY_SID"]).verifications).to_not receive(:create)
        client.request_authorization
      end
    end

    context "when the notification_method is specified" do
      it "sends the code via the specified method" do
        allow(@tfa).to receive(:notification_method).and_return("call")
        expect(@tfa.notification_method).to eq "call"

        expected_args = { to: @tfa.e164, channel: 'sms' }
        client = TwoFactorAuth::ApiClient.new(@person, @tfa, {})
        api = client.client

        expect(api.verify.v2.services(ENV["VERIFY_SID"]).verifications).to receive(:create).with(expected_args)
        client.request_authorization("sms")
      end
    end
  end

  describe "#submit_auth_code" do
    context "when auth code is accepted" do
      it "returns successful submission" do
        auth_code = "55555"
        mock_response = double(status: 'approved')
        client = TwoFactorAuth::ApiClient.new(@person, nil, {})
        api = client.client

        expect(api.verify.v2.services(ENV["VERIFY_SID"]).verification_checks).to receive(:create).and_return(mock_response)
        response = client.submit_auth_code(auth_code)
        expect(response.successful?).to eq true
      end
    end

    context "when auth code is denied" do
      it "returns failed submission" do
        auth_code = "55555"
        mock_response = double(status: 'pending')
        client = TwoFactorAuth::ApiClient.new(@person, nil, {})
        api = client.client

        expect(api.verify.v2.services(ENV["VERIFY_SID"]).verification_checks).to receive(:create).and_return(mock_response)
        response = client.submit_auth_code(auth_code)
        expect(response.successful?).to eq false
      end
    end
  end

  describe ".get_e164" do
    it "Verifies phone number with Twilio and receives the e164 formatted number" do
      params = {
        country_code:   "1",
        phone_number:      "1234567890"
      }
      mock_response = double(phone_number: "+11234567890")
      client = TwoFactorAuth::ApiClient.new(@person, nil, params)
      api = client.client

      expect(api.lookups.v1.phone_numbers("11234567890")).to receive(:fetch).and_return(mock_response)
      response = client.get_e164
      expect(response).to eq("+11234567890")
    end
  end
end
