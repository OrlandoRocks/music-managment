require 'rails_helper'

describe PasswordRestrictionService do
  before(:each) do
    TcWww::Application.config.enable_password_restrictions = true
    ensure_restricted_passwords_set_exists
  end

  after(:all) do
    TcWww::Application.config.enable_password_restrictions = false
  end

  def ensure_restricted_passwords_set_exists
    if $redis.scard("restricted_passwords") == 0
      %w(123456 password monkey).map { |pwd| $redis.sadd("restricted_passwords", pwd) }
    end
  end

  describe ".restricted?" do
    context "when a password is in the restricted list" do
      it "returns true" do
        expect(PasswordRestrictionService.restricted?("password")).to be_truthy
      end
    end

    context "when a password is NOT in the restriced list" do
      it "returns false" do
        expect(PasswordRestrictionService.restricted?("h6anks863mlj!!")).to be_falsey
      end
    end
  end
end
