require "rails_helper"

describe BackfillSalepoint do
  let!(:album) { create(:album, :with_uploaded_songs_and_artwork, :with_songwriter) }

  let!(:store_akazoo) { Store.find_by(short_name: "Akazoo") }

  let!(:store_kkbox) { Store.find_by(short_name: "KKBox") }

  let!(:salepoint) {
    create(:salepointable_store, store: store_akazoo)
    create(
      :salepoint,
      salepointable: album,
      store_id: store_akazoo.id,
      finalized_at: Time.now
    )
  }

  context "with run" do
    it "create only salepoint if copy_distributions is false" do
      backfill_salepoint_object = BackfillSalepoint.new(store_akazoo.id, store_kkbox.id, false)
      expect {
        backfill_salepoint_object.run
      }.to change { Salepoint.count }
        .by(1)
        .and change { Distribution.count }
        .by(0)
    end

    it "create salepoint and distribution if copy_distributions is true" do
      petri_bundle = create(:petri_bundle, album: album)
      distro = create(
        :distribution,
        :delivered_distro,
        petri_bundle: petri_bundle,
        converter_class: store_akazoo.converter_class.to_s
      )
      distro.salepoints << salepoint
      backfill_salepoint_object = BackfillSalepoint.new(store_akazoo.id, store_kkbox.id, true)

      expect {
        backfill_salepoint_object.run
      }.to change { Salepoint.count }
        .by(1)
        .and change { Distribution.count }
        .by(1)
    end

    it "do not create salepoint if salepoint already exist for the store" do
      create(:salepointable_store, store: store_kkbox)
      create(
        :salepoint,
        salepointable: album,
        store_id: store_kkbox.id,
        finalized_at: Time.now
      )

      backfill_salepoint_object = BackfillSalepoint.new(store_akazoo.id, store_kkbox.id, false)

      expect {
        backfill_salepoint_object.run
      }.to change { Salepoint.count }
        .by(0)
        .and change { Distribution.count }
        .by(0)
    end

    it "do not create distribution if for salepoint distribution is not present and copy_distributions is true" do
      backfill_salepoint_object = BackfillSalepoint.new(store_akazoo.id, store_kkbox.id, true)
      expect {
        backfill_salepoint_object.run
      }.to change { Salepoint.count }
        .by(1)
        .and change { Distribution.count }
        .by(0)
    end
  end

  context "validate wrong input" do
    it "source_store_id is blank" do
      expect {
        BackfillSalepoint.new(nil, store_kkbox.id, true).run
      }.to raise_error(RuntimeError, "Please provide source_store_id")
    end

    it "destination_source_id is blank" do
      expect {
        BackfillSalepoint.new(store_akazoo.id, nil, true).run
      }.to raise_error(RuntimeError, "Please provide destination_source_id")
    end

    it "source_store_id and destination_source_id are same" do
      expect {
        BackfillSalepoint.new(store_kkbox.id, store_kkbox.id, true).run
      }.to raise_error(RuntimeError, "Source_store_id and destination_source_id should be different")
    end

    it "source_store_id store doesn't exist" do
      not_exist_store_id = Store.last.id + 1
      expect {
        BackfillSalepoint.new(not_exist_store_id, store_kkbox.id, true).run
      }.to raise_error(RuntimeError, "Source store does not exist")
    end

    it "destination_source_id store doesn't exist" do
      not_exist_store_id = Store.last.id + 1
      expect {
        BackfillSalepoint.new(store_akazoo.id, not_exist_store_id, true).run
      }.to raise_error(RuntimeError, "Destination store does not exist")
    end
  end
end
