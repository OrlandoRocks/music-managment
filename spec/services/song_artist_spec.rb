require "rails_helper"

describe SongArtist do
  let(:artist)         { create(:artist) }
  let(:person)         { create(:person) }
  let(:album)          { create(:album, person_id: person.id) }
  let(:song)           { create(:song, album_id: album.id) }

  before(:each) do
    @song_role1 = SongRole.first
    @song_role2 = SongRole.last
    @album_creative = album.creatives.first
  end

  describe ".for_song" do
    context "song does not have any creatives" do
      let(:song_artists) {SongArtist.for_song(song)}

      it "returns the creative from the song's album" do
        expect(song_artists.length).to eq(1)
        expect(song_artists.first.associated_to).to eq("Album")
      end
    end

    context "creative has song roles" do
      let(:song_artists) { SongArtist.for_song(song) }

      before do
        CreativeSongRole.create(creative_id: @album_creative.id, song_role_id: @song_role1.id, song_id: song.id)
        CreativeSongRole.create(creative_id: @album_creative.id, song_role_id: @song_role2.id, song_id: song.id)
      end

      it "returns the creative with the correct role IDs" do
        expect(song_artists.first.role_ids.split(",").length).to eq(2)
        expect(song_artists.first.role_ids).to include(@song_role1.id.to_s)
        expect(song_artists.first.role_ids).to include(@song_role2.id.to_s)
      end
    end
  end

  describe ".for_crt" do
    let(:song_artists) {SongArtist.for_crt(song)}

    before :each do
      FactoryBot.create(:creative,
        creativeable_id: song.id,
        creativeable_type: "Song",
        role: "with")
      FactoryBot.create(:creative,
        creativeable_id: song.id,
        creativeable_type: "Song",
        role: "featuring")
      FactoryBot.create(:creative,
        creativeable_id: song.id,
        creativeable_type: "Song",
        role: "primary_artist")
      FactoryBot.create(:creative,
        creativeable_id: song.id,
        creativeable_type: "Song",
        role: "contributor")
      creative = FactoryBot.create(:creative,
        creativeable_id: song.id,
        creativeable_type: "Song",
        role: "contributor")
      CreativeSongRole.create(creative_id: creative.id, song_role_id: SongRole.find_by(role_type: "songwriter").id, song_id: song.id)
    end

    it "returns all the song's creatives who are primary, featured and/or songwriters" do
      expect(song_artists.length).to eq(4)
      expect(song_artists.map(&:credit)).to eq(["with", "featuring", "primary_artist", "contributor"])
      songwriter_contributor = song_artists.find {|a| a.credit == "contributor"}
      expect(songwriter_contributor.role_types).to include("songwriter")
    end
  end
end
