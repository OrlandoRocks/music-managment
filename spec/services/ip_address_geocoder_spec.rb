require "rails_helper"

describe IpAddressGeocoder do
  describe ".geocode" do
    before do
      allow(IpAddressGeocoder).to receive(:geocode).and_call_original
    end

    context "successful request" do
      before do
        @response_body = {
          city:        "Brooklyn",
          subdivision: "New York",
          country:     "United States",
          latitude:    40.6501,
          longitude:   -73.9496
        }
      end

      it "makes a successful request to the geocoding service" do
        response = double("response", body: @response_body.to_json, success?: true)
        allow(Faraday).to receive(:new).and_return(double("faraday", get: response))
        expect(IpAddressGeocoder.geocode("100.37.214.12")["city"]).to eq("Brooklyn")
      end

      it "delegates options to the http library" do
        response = double("response", body: @response_body.to_json, success?: true)
        expect(Faraday).to receive(:new).with(url: "#{ENV["GEOLOCATION_ENDPOINT"]}/100.37.214.12", request: { timeout: 3 }).and_return(double("faraday", get: response))
        IpAddressGeocoder.geocode("100.37.214.12", request: { timeout: 3 })
      end

      it "uses the country name to extract the iso_code" do
        response = double("response", body: @response_body.to_json, success?: true)
        allow(Faraday).to receive(:new).with(url: "#{ENV["GEOLOCATION_ENDPOINT"]}/100.37.214.12", request: { timeout: 3 }).and_return(double("faraday", get: response))

        response = IpAddressGeocoder.geocode("100.37.214.12", request: { timeout: 3 })

        expect(response["country_code"]).to eq("US")
      end
    end

    context "unsuccessful request" do
      it "makes an unsuccessful request to the geocoding service" do
        response = double("response", body: "", success?: false)
        allow(Faraday).to receive(:new).and_return(double("faraday", get: response))

        expect(IpAddressGeocoder.geocode("")).to eq({})
      end
    end
  end
end
