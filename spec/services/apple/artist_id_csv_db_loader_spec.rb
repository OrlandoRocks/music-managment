require "rails_helper"

describe Apple::ArtistIdCsvDbLoader do

  let(:various_album)           { create(:album, :with_artwork, is_various: true) }
  let(:album) do create(:album, :with_artwork, artist: artist, creatives: [
      { name: artist.name, role: "primary_artist" }
  ])
  end

  let!(:various_upc)       { create(:tunecore_upc, number: "859711297342", upcable: various_album) }
  let!(:upc)               { create(:tunecore_upc, number: "859708954029", upcable: album) }

  let(:various_artist1)         { create(:artist, name: "Tangerine Dream") }
  let(:various_artist2)         { create(:artist, name: "Artist 2") }
  let(:artist)                  { create(:artist, name: "Al") }

  let!(:various_song1) do create(:song, name: "various artists track 1", album: various_album, creatives: [
    { name: various_artist1.name, role: "primary_artist" }.with_indifferent_access,
    { name: various_artist2.name, role: "primary_artist" }.with_indifferent_access])
  end

  let!(:song1) do  create(:song, name: "track 1", album: album, creatives: [
    { name: artist.name, role: "primary_artist" }])
  end

  let(:file)                    { fixture_file_upload("/apple_artist_ids.csv", 'text/csv') }

  context "Parses through CSV values" do
    it "saves external service IDs for both various artists album and an album with 1 primary artist" do
      Apple::ArtistIdCsvDbLoader.upload(file.path)

      various_album_external_service_ids = various_album.reload.songs.first.creatives.joins(:external_service_ids).count
      album_external_service_ids = album.creatives.joins(:external_service_ids).count

      expect(ExternalServiceId.all.size).to eq(3)
      expect(various_album_external_service_ids).to eq(2)
      expect(album_external_service_ids).to eq(1)
    end

    it "saves IDs after a given index when passed through a starting CSV index" do
      Apple::ArtistIdCsvDbLoader.upload(file.path, 3)
      various_album_external_service_ids = various_album.reload.songs.first.creatives.joins(:external_service_ids).count
      album_external_service_ids = album.creatives.joins(:external_service_ids).count

      expect(ExternalServiceId.all.size).to eq(1)
      expect(various_album_external_service_ids).to eq(0)
      expect(album_external_service_ids).to eq(1)
    end

    it "saves IDs before a given ending index when passed through a ending CSV index" do
      Apple::ArtistIdCsvDbLoader.upload(file.path, nil, 2)

      various_album_external_service_ids = various_album.reload.songs.first.creatives.joins(:external_service_ids).count
      album_external_service_ids = album.creatives.joins(:external_service_ids).count


      expect(ExternalServiceId.all.size).to eq(2)
      expect(various_album_external_service_ids).to eq(2)
      expect(album_external_service_ids).to eq(0)
    end

    it "saves IDs in a given range of indices when passed a range of indices" do
      Apple::ArtistIdCsvDbLoader.upload(file.path, 2, 2)

      various_album_external_service_ids = various_album.reload.songs.first.creatives.joins(:external_service_ids).count
      album_external_service_ids = album.creatives.joins(:external_service_ids).count


      expect(ExternalServiceId.all.size).to eq(1)
      expect(various_album_external_service_ids).to eq(1)
      expect(album_external_service_ids).to eq(0)
    end


    it "does not save any duplicate external service IDs" do
      ExternalServiceId.create(
          linkable: album.creatives.first,
          identifier: "1234",
          service_name: "apple",
          state: "matched"
      )
      Apple::ArtistIdCsvDbLoader.upload(file.path, 3, 3)

      album_external_service_ids = album.creatives.joins(:external_service_ids).count

      expect(album_external_service_ids).to eq(1)
      expect(ExternalServiceId.all.size).to be(1)
      expect(ExternalServiceId.first.identifier).to eq("1234")
    end
  end

end
