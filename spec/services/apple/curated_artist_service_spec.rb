require 'rails_helper'
describe Apple::CuratedArtistService do

    let!(:album)  { TCFactory.create_purchaseable_album(:creatives => [{"name"=>"Skateboard P", "role"=>"primary_artist"}]) }
    let(:salepoint) { create(:salepoint, store: Store.ITUNES_US, salepointable: album) }
    let(:non_itunes_salepoint) { create(:salepoint, store: Store.discovery_platforms.take,  salepointable: a)}
    let(:curated_creative) { album.artist.creatives.first }
    subject(:curated_artists) { described_class.verify_curated_artist(album.id) }


    context "when artist is curated" do
      before do
        allow_any_instance_of(Song).to receive(:duration).and_return(95000)
        allow_any_instance_of(Apple::Transporter).to receive(:curated_artists).and_return(["Skateboard P"])
      end

      it "it generates a metadata to validate for curated artist" do
        expect_any_instance_of(DistributionSystem::Itunes::Bundle).to receive(:create_bundle).once
        expect_any_instance_of(DistributionSystem::Itunes::Bundle).to receive(:write_metadata).once
        subject
      end

      it "calls #curated_artist on the correct remote server class" do 
        expect_any_instance_of(Apple::Transporter).to receive(:curated_artists).once
        subject
      end 


      it "it updates the curated artist flag to true" do
        subject
        expect(curated_creative.curated_artist_flag).to be(true)
      end

      it "it expects remove_curated_creatives_flags to remove curated_artist_flag" do
        subject.remove_curated_creatives_flags
        curated_creative.reload
        expect(curated_creative.curated_artist_flag).to be_falsey
      end
    end

    context "when artist is not curated" do
      before do
        allow_any_instance_of(Song).to receive(:duration).and_return(95000)
        allow_any_instance_of(Apple::Transporter).to receive(:curated_artists).and_return([])
      end

      it "it generates a metadata to validate for curated artist" do
        expect_any_instance_of(DistributionSystem::Itunes::Bundle).to receive(:create_bundle).once
        expect_any_instance_of(DistributionSystem::Itunes::Bundle).to receive(:write_metadata).once
        subject
      end

      it "it calls distributor curated artist method and does not update curated artist flag" do
        subject
        expect(curated_creative.curated_artist_flag).to be_falsey
      end
    end
end
