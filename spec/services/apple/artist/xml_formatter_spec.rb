require "rails_helper"

describe Apple::Artist::XmlFormatter do
  before(:each) do
    @person            = FactoryBot.create(:person)
    @artist            = FactoryBot.create(:artist, name: "Jerry Smith")
    album              = FactoryBot.create(:album)
    FactoryBot.create(:creative, artist_id: @artist.id, creativeable_id: album.id, creativeable_type: 'Album').update(person_id: @person.id)
    @external_service_id = ExternalServiceId.create(linkable_type: "Creative", linkable_id: Creative.last.id, service_name: "apple", state: "processing")
  end
  describe ".format" do
    let(:duplicate_name_xml) {Nokogiri::XML(File.open(Rails.root.join("spec", "fixtures", "apple_artist_package_duplicate_name.xml")))}

    let(:different_name_xml) {Nokogiri::XML(File.open(Rails.root.join("spec", "fixtures", "apple_artist_package_different_name.xml")))}

    let(:image_xml) {Nokogiri::XML(File.open(Rails.root.join("spec", "fixtures", "apple_artist_package_with_image.xml")))}

    context "with duplicate name" do
      it "writes valid XML" do
        xml = Apple::Artist::XmlFormatter.format(@external_service_id, {is_duplicate: true})
        expect(
          EquivalentXml.equivalent?(
            Nokogiri::XML(xml, nil, 'utf-8'),
            duplicate_name_xml,
            :element_order => false,
            :normalize_whitespace => true
          )
         ).to eq true
      end
    end

    context  "with different name" do
      it "writes duplicate XML" do
        xml = Apple::Artist::XmlFormatter.format(@external_service_id, {is_duplicate: false})
        expect(
          EquivalentXml.equivalent?(
            Nokogiri::XML(xml, nil, 'utf-8'),
            different_name_xml,
            :element_order => false,
            :normalize_whitespace => true
          )
         ).to eq true
      end
    end

    context "with image" do
      before(:each) do
        @artwork = ExternalServiceIdArtwork.create
        allow(@external_service_id).to receive(:artwork).and_return(@artwork)
        allow(@artwork).to receive(:asset).and_return(double({path: "/path/to/image", fingerprint: "12345", size: "5002"}))
        allow(@artwork).to receive(:asset_file_name).and_return("image.jpg")
        allow(Apple::Artist::ArtworkDownloader).to receive(:download_artwork).and_return(double({:[] => "image bytes"}))
      end

      it "write valid XML" do
        xml = Apple::Artist::XmlFormatter.format(@external_service_id, {is_duplicate: false})
        expect(
          EquivalentXml.equivalent?(
            Nokogiri::XML(xml, nil, 'utf-8'),
            image_xml,
            :element_order => false,
            :normalize_whitespace => true
          )
         ).to eq true
      end
    end
  end
end
