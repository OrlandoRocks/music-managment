require "rails_helper"

describe Apple::Artist::Package do
  describe ".build_package" do
    before(:each) do
      @person = create(:person)
      @artist = create(:artist, name: "Liz Lemon")
      album   = create(:album)
      create(:creative, artist_id: @artist.id, creativeable_id: album.id, creativeable_type: 'Album').update(person_id: @person.id)
      @external_service_id = ExternalServiceId.create(linkable_type: "Creative", linkable_id: Creative.last.id, service_name: "apple", state: "processing")
    end
    after(:each) do
      @package.cleanup
    end
    it "creates the directory" do
      @package = Apple::Artist::Package.build_package(@artist.id, @person.id, true)
      expect(File.directory?(Rails.root.join("./work/#{@artist.name.parameterize.underscore}.itmsp"))).to eq true
    end

    it "writes the xml to the directory" do
      @package = Apple::Artist::Package.build_package(@artist.id, @person.id, true)
      expect(File.exist?(Rails.root.join("./work/#{@artist.name.parameterize.underscore}.itmsp"))).to eq true
    end

    context "with image" do
      before(:each) do
        @artwork = ExternalServiceIdArtwork.create
        allow(ExternalServiceId).to receive(:artist_ids_for).with(@person.id, @artist.id, "apple").and_return([@external_service_id])
        allow(@external_service_id).to receive(:artwork).and_return(@artwork)
        allow(@artwork).to receive(:asset).and_return(double({path: "/path/to/image", fingerprint: "12345", size: "5002"}))
        allow(@artwork).to receive(:asset_file_name).and_return("image.jpg")
        allow(Apple::Artist::ArtworkDownloader).to receive(:download_artwork).and_return(double({:[] => "image bytes"}))
      end

      it "downloads the image from s3 to the directory" do
        expect(Apple::Artist::ArtworkDownloader).to receive(:download_artwork).with(
          object_key: @artwork.asset.path
        )
        @package = Apple::Artist::Package.build_package(@artist.id, @person.id, true)
      end
    end
  end
end
