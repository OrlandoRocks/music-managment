require "rails_helper"

describe Apple::ArtistIdUpdater do
  describe ".update_artist_id" do
    before(:each) do
      @person = create(:person)
      @artist = create(:artist, name: "Moebi")
      album   = create(:album)
      create(:creative, artist_id: @artist.id, creativeable_id: album.id, creativeable_type: 'Album').update(person_id: @person.id)
    end

    let(:with_identifier) {
      {
        "person_id" => @person.id,
        "artist_id" => @artist.id,
        "service_name" => "apple",
        "identifier" => "12345"
      }
    }

    context "when artist ID is a valid match to iTunes" do
      before(:each) do
        allow(Apple::ArtistIdValidator).to receive(:valid?).with(with_identifier).and_return(true)
        allow(ArtistMapping::AlbumRedistributor).to receive(:redistribute).with(with_identifier)
      end

      it "updates External Service IDs status to 'matched'" do
        Apple::ArtistIdUpdater.update_artist_id(with_identifier)
        external_service_ids = ExternalServiceId.artist_ids_for(@person.id, @artist.id, "apple")
        external_service_ids.each do |es|
          expect(es.state).to eq("matched")
        end
      end

      it "calls on the ArtistMapping::AlbumRedistributor" do
        expect(ArtistMapping::AlbumRedistributor).to receive(:redistribute).with(with_identifier)
        Apple::ArtistIdUpdater.update_artist_id(with_identifier)
      end
    end

    context "when the artist ID is not a valid match to iTunes" do
      before(:each) do
        allow(Apple::ArtistIdValidator).to receive(:valid?).with(with_identifier).and_return(false)
        allow(ArtistMappingMailer).to receive(:failed_apple_artist_match_email).with(with_identifier).and_return(double({deliver: true}))
      end

      it "updates the External Service Ids status to 'did_not_match'" do
        Apple::ArtistIdUpdater.update_artist_id(with_identifier)
        external_service_ids = ExternalServiceId.artist_ids_for(@person.id, @artist.id, "apple")
        external_service_ids.each do |es|
          expect(es.state).to eq("did_not_match")
        end
      end

      it "calls on the artist match failure email" do
        expect(ArtistMappingMailer).to receive(:failed_apple_artist_match_email)
        Apple::ArtistIdUpdater.update_artist_id(with_identifier)
      end
    end
  end
end
