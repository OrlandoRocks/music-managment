require 'rails_helper'

describe Apple::TicketCreator do
  let!(:person) { create(:person) }
  let!(:artist) { create(:artist, name: "DJ Cactus") }

  it "should trigger mail when the method is called" do
    expect {
        Apple::TicketCreator.create_ticket({person_id: person.id, artist_id: artist.id})
      }.to change(ActionMailer::Base.deliveries,:size).by(1)
  end
end
