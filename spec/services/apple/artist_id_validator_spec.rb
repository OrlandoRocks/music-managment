require "rails_helper"

describe Apple::ArtistIdValidator do
  before(:each) do
    @artist   = create(:artist, name: "Tracy Jordan")
    album     = create(:album)
    create(:creative, artist_id: @artist.id, creativeable_id: album.id, creativeable_type: 'Album')
  end

  let(:with_identifier) {
      {
        "artist_id" => @artist.id,
        "service_name" => "apple",
        "identifier" => "12345"
      }
    }

  let(:missing_artist_id) {
    {
      "artist_id" => nil,
      "service_name" => "apple",
      "identifier" => "12345",
      "name" => "Tracy Jordan"
    }
  }

  let(:name_mismatch) {
    {
      "artist_id" => nil,
      "service_name" => "apple",
      "identifier" => "12345",
      "name" => "Tray"
    }
  }

  let(:no_check) {
    {
      "artist_id" => nil,
      "service_name" => "apple",
      "identifier" => "",
      "name" => "Tray",
      "apple_name" => "tray",
      "check_id" => false
    }
  }

  describe ".valid?" do
    before(:each) do
      allow($apple_transporter_client).to receive(:transporter_present).and_return(true)
    end

    context "it is a valid name match to iTunes lookup result" do
      it "returns true" do
        allow($apple_transporter_client).to receive(:lookup_item).and_return({apple_id: "12345", name: "#{@artist.name}"})
        expect($apple_transporter_client).to receive(:lookup_item).with("artist", "12345")
        expect(Apple::ArtistIdValidator.valid?(with_identifier)).to eq(true)
      end
    end

    context "the artist is not yet persisted, and name is a match" do
      it "return true" do
        allow($apple_transporter_client).to receive(:lookup_item).and_return({apple_id: "12345", name: "#{@artist.name}"})
        expect($apple_transporter_client).to receive(:lookup_item).with("artist", "12345")
        expect(Apple::ArtistIdValidator.valid?(missing_artist_id)).to eq(true)
      end
    end

    context "the artist is not yet persisted, and the name is NOT a match" do
      it "returns false" do
        allow($apple_transporter_client).to receive(:lookup_item).and_return({apple_id: "12345", name: "#{@artist.name}"})
        expect($apple_transporter_client).to receive(:lookup_item).with("artist", "12345")
        expect(Apple::ArtistIdValidator.valid?(name_mismatch)).to eq(false)
      end
    end

    context "it is not a valid name match to iTunes lookup result" do
      it "returns false" do
        allow($apple_transporter_client).to receive(:lookup_item).and_return({apple_id: "12345", name: "Rick Sanchez"})
        expect($apple_transporter_client).to receive(:lookup_item).with("artist", "12345")
        expect(Apple::ArtistIdValidator.valid?(with_identifier)).to eq(false)
      end
    end

    context "matching apple_name provided and no_check" do
      it "returns true" do
        expect($apple_transporter_client).not_to receive(:lookup_item)
        expect(Apple::ArtistIdValidator.valid?(no_check)).to eq(true)
      end
    end
  end

  describe ".normalize_characters" do
    it "returns false if the transport doesn't return a result" do
      artist = create(:artist, name: "Mỹ Tâmü")
      result  = { apple_id: nil, name: nil }

      options = {
        "identifier" => "12345",
        "artist_id" => artist.id,
      }

      allow($apple_transporter_client).to receive(:transport).and_return(result)
      validator = Apple::ArtistIdValidator.new(options)
      expect(validator.valid_artist_name?).to eq(false)
    end

    it "returns true if artist names match" do
      artist = create(:artist, name: "Normal Name")
      result  = { apple_id: "123456789", name: artist.name }

      options = {
        "identifier" => "12345",
        "artist_id" => artist.id,
      }

      allow($apple_transporter_client).to receive(:transport).and_return(result)
      validator = Apple::ArtistIdValidator.new(options)
      expect(validator.valid_artist_name?).to eq(true)
    end
  end
end
