require 'rails_helper'
describe Apple::CreateAppleArtistsService do
  subject(:for_album) { described_class.for_album(album.id) }

  let!(:album) do
    TCFactory.create_purchaseable_album(creatives: [
                                          { "name" => "Skateboard P", "role" => "primary_artist" },
                                          { "name" => "Skateboard P2", "role" => "primary_artist" }
                                        ])
  end

  describe "#for_album" do
    before do
      create(:creative, creativeable: album, role: 'choir')
      create(:external_service_id, linkable: album.creatives.first, service_name: 'apple',
                                   identifier: 'NEW')
      create(:external_service_id, linkable: album.creatives.second, service_name: 'apple',
                                   identifier: 'NEW')
    end

    context "when creating a new artist succeeds" do
      before do
        allow($apple_transporter_client).to receive(:create_artist)
          .and_return(success: true, apple_id: '1234')
      end

      it "creates a new artist on Apple only for primary_artist role creatives" do
        expect(album.creatives.count).to eq(3)
        expect($apple_transporter_client).to receive(:create_artist).twice
        for_album
      end

      it "adds the new apple ID to the DB" do
        for_album
        expect(ExternalServiceId.last(2).pluck(:identifier)).to eq(%w[1234 1234])
      end
    end

    context "when creating new Artist fails" do
      before do
        allow($apple_transporter_client).to receive(:create_artist)
          .and_return(output: "Error: Apple id for artist already exists")
      end

      it "sets apple_artist_id to nil and external_service_id state to error" do
        for_album
        expect(ExternalServiceId.last(2).pluck(:identifier, :state)).to eq([%w[NEW error],
                                                                            %w[NEW error]])
      end

      it "notifies airbrake" do
        allow($apple_transporter_client).to receive(:create_artist)
          .and_return(output: "Error: Apple id for artist already exists")
        expect(Airbrake).to receive(:notify).with(Apple::Transporter::TransporterCreateArtistError)
                                            .twice
        expect(Rails.logger).to receive(:error).with(/^Failed to create artist on iTunes/).twice
        for_album
      end

      context 'when transport throws an unexpected response' do
        before do
            allow($apple_transporter_client).to receive(:create_artist)
          .and_raise("AN ERROR")
        end

        it "notifies Airbrake" do
          expect(Airbrake).to receive(:notify).twice
          for_album
        end

        it "continues to handle_result" do
          for_album
          expect(ExternalServiceId.last(1).pluck(:state)).to eq(["error"])
        end
      end
    end
  end
end
