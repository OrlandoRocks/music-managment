require "rails_helper"

describe Apple::Album do
  let(:album)         { FactoryBot.create(:album) }
  let(:document)      { double }
  let(:result)        { {apple_id: 6789, document: document, artist: {name: "#{album.artist.name}", apple_id: "1380948214"}} }
  let(:live_on)       { Time.now + 2.days }

  describe ".get_itunes_info" do
    context "album does not have an apple ID yet in iTunes" do
      let(:no_apple_id_result) { {apple_id: nil, document: document }}

      before(:each) do
        allow($apple_transporter_client).to receive(:transporter_present).and_return(true)
        allow($apple_transporter_client).to receive(:release_status).with(album.upc.number).and_return(no_apple_id_result)
        @itunes_status = FactoryBot.create(:album_itunes_status, album: album)
        allow(Album).to receive(:find).with(album.id).and_return(album)
        allow(album).to receive(:album_itunes_status).and_return(@itunes_status)
      end
      xit "updates the itunes status with failure info" do
        Apple::Album.get_itunes_info(album.id)
        expect(@itunes_status).to receive(:update_failure)
      end
    end

    context "can get info from apple transporter client" do
      before(:each) do
        allow($apple_transporter_client).to receive(:transporter_present).and_return(true)
        allow($apple_transporter_client).to receive(:release_status).with(album.upc.number).and_return(result)
        @itunes_status = FactoryBot.create(:album_itunes_status, album: album)
        allow(Album).to receive(:find).with(album.id).and_return(album)
        allow(album).to receive(:album_itunes_status).and_return(@itunes_status)

        allow_any_instance_of(Apple::Album).to receive(:get_valid_apple_artist_id).with(album.artist, result).and_return(result[:artist][:apple_id])
        allow(@itunes_status).to receive(:update_success_xml).with(document)
        allow(document).to receive(:xpath).with("//artist").and_return(result)
        allow(document).to receive_message_chain(:first, :attributes, :[], :value).and_return(result[:artist][:apple_id])
      end

      context "album does not have an apple ID or known_live_on" do
        it "sets those attributes" do
          Apple::Album.get_itunes_info(album.id)
          expect(album.apple_identifier).to eq("6789")
        end
      end

      context "album has an iTunes status" do
        it "updates the iTunes status" do
          expect(@itunes_status).to receive(:update_success_xml).with(document)
          Apple::Album.get_itunes_info(album.id)
        end
      end

      context "album does not have iTunes status" do
        it "creates an iTunes status" do
          allow(album).to receive(:album_itunes_status).and_return(nil)
          expect(AlbumItunesStatus).to receive(:new).with({album: album}).and_return(@itunes_status)
          Apple::Album.get_itunes_info(album.id)
        end
      end

      context ".create_or_update_apple_artist_ids" do
        it "creates an external service id if an apple ID is present" do
          creatives = album.artist.creatives
          expect_any_instance_of(Linkable).to receive(:update_or_create_external_service_id).with("apple", result[:artist][:apple_id], creatives)
          Apple::Album.get_itunes_info(album.id)
        end

        it "logs a failure when there is no apple ID" do
          allow_any_instance_of(Apple::Album).to receive(:get_valid_apple_artist_id).with(album.artist, result).and_return(nil)
          expect(Rails.logger).to receive(:error).with(/NO APPLE ID FOR ALBUM ID: #{album.id}/)
          Apple::Album.get_itunes_info(album.id)
        end
      end

      context ".get_valid_apple_artist_id" do
        it "returns a valid apple artist ID" do
          results = Apple::Album.get_itunes_info(album.id).get_valid_apple_artist_id(album.artist, result)
          expect(results).to eq(result[:artist][:apple_id])
        end
      end

      context "album is a various artists album" do
        it "does not call .create_or_update_apple_artist_ids" do
          album.update(is_various: true)
          expect_any_instance_of(Apple::Album).to_not receive(:create_or_update_apple_artist_ids)
          Apple::Album.get_itunes_info(album.id)
        end
      end
    end
  end

  describe ".create_or_update_itunes_status_failure" do
    before(:each) do
      allow(Album).to receive(:find).with(album.id).and_return(album)
      @itunes_status = FactoryBot.create(:album_itunes_status, album: album)
    end

    context "album has an iTunes status" do
      it "updates the iTunes status" do
        allow(album).to receive(:album_itunes_status).and_return(@itunes_status)
        expect(@itunes_status).to receive(:update_failure)
        Apple::Album.create_or_update_itunes_status_failure(album.id)
      end
    end

    context "album does not have iTunes status" do
      it "creates an iTunes status" do
        allow(album).to receive(:album_itunes_status).and_return(nil)
        expect(AlbumItunesStatus).to receive(:new).with({album: album}).and_return(@itunes_status)
        Apple::Album.create_or_update_itunes_status_failure(album.id)
      end
    end
  end
end
