require "rails_helper"

describe Apple::BulkAlbumIdFetchingService do
  describe ".fetch_for_albums" do
    before do
      allow(DistributorAPI::Album).to receive(:delivered_albums).and_return([double(http_body: { 'albums' => albums })])
    end

    context "when given a number of days" do
      before(:each) { @album1 = create(:album, finalized_at: 30.days.ago) }

      let(:albums) { [@album1] }

      it "enqueues the AlbumItunesInfoWorker with the correct album IDs" do
        allow_any_instance_of(Apple::BulkAlbumIdFetchingService).to receive(:albums_to_update).and_return(Album.where(id: @album1.id))
        expect(Apple::AlbumItunesInfoWorker).to receive(:perform_in).with(0, @album1.id)
        Apple::BulkAlbumIdFetchingService.fetch_for_albums("10")
      end
    end

    context "when NOT given a number of days" do
      before(:each) { @album2 = create(:album, finalized_at: 30.days.ago) }

      let(:albums) { [@album1] }

      it "enqueues the AlbumItunesInfoWorker with the correct album IDs" do
        allow_any_instance_of(Apple::BulkAlbumIdFetchingService).to receive(:albums_to_update).and_return(Album.where(id: @album2.id))
        expect(Apple::AlbumItunesInfoWorker).to receive(:perform_in).with(0, @album2.id)
        Apple::BulkAlbumIdFetchingService.fetch_for_albums
      end
    end

    context 'with delivered_albums' do
      subject { described_class.fetch_for_albums }

      before { allow(Apple::AlbumItunesInfoWorker).to receive(:perform_in) }
      after { subject }

      context 'when album is delivered' do
        let(:album) { create(:album, :with_apple_distribution, :finalized) }
        let(:albums) { [album] }

        it { expect(Apple::AlbumItunesInfoWorker).to receive(:perform_in).with(anything, album.id) }
      end
    end
  end
end
