require "rails_helper"

describe Apple::ArtistIdExtractionService do
  let(:url_with_id) {"https://itunes.apple.com/us/artist/ryan-moe/id12345"}
  let(:url)         {"https://music.apple.com/us/artist/ryan-moe/12345"}
  let(:url_with_query_params) { "https://itunes.apple.com/us/artist/ryan-moe/12345?ign-mpt=uo%3D4" }
  let(:url_with_id_and_query_params) { "https://itunes.apple.com/us/artist/ryan-moe/id12345?ign-mpt=uo%3D4" }
  let(:invalid_url) { "asdfasfasdf" }

  describe ".extract_artist_id" do
    context "when URL has 'id' in it" do
      it "returns the apple artist ID" do
        expect(Apple::ArtistIdExtractionService.extract_artist_id(url_with_id)).to eq("12345")
      end

      context "with some extra query params" do
        it "returns the apple artist ID" do
          expect(Apple::ArtistIdExtractionService.extract_artist_id(url_with_id_and_query_params)).to eq("12345")
        end
      end
    end

    context "when the URL does not have 'id' in it" do
      it "returns the apple artist ID" do
        expect(Apple::ArtistIdExtractionService.extract_artist_id(url)).to eq("12345")
      end

      context "with some extra query params" do
        it "returns the apple artist ID" do
          expect(Apple::ArtistIdExtractionService.extract_artist_id(url_with_query_params)).to eq("12345")
        end
      end
    end

    context "when given an invalid url" do
      it "returns nil" do
        expect(Apple::ArtistIdExtractionService.extract_artist_id(invalid_url)).to eq(nil)
      end
    end
  end
end
