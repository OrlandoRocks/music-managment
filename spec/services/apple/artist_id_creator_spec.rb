require "rails_helper"

describe Apple::ArtistIdCreator do
  let(:package) {double({dir: "path/to/package", cleanup: true})}

  describe ".create_artist_id" do
    before(:each) do
      @person = create(:person)
      @artist = create(:artist, name: "Random Name")
      album   = create(:album)
      create(:creative, artist_id: @artist.id, creativeable_id: album.id, creativeable_type: 'Album').update(person_id: @person.id)
    end

    before(:each) do
      allow(Apple::Artist::Package).to receive(:build_package).and_return(package)
      allow($apple_transporter_client).to receive(:transporter_present).and_return(true)
    end

    it "builds the package" do
      allow_any_instance_of(Apple::ArtistIdCreator).to receive(:submit_to_store)
      expect(Apple::Artist::Package).to receive(:build_package).with(@artist.id, @person.id, nil)
      Apple::ArtistIdCreator.create_artist_id(@person.id, @artist.id, nil)
    end

    context "when package is valid" do
      before(:each) do
        allow($apple_transporter_client).to receive(:validate_package).and_return({valid: true})
      end
      it "invokes the send package transporter cmd" do
        allow($apple_transporter_client).to receive(:send_package).and_return({success: true})
        expect($apple_transporter_client).to receive(:send_package)
        Apple::ArtistIdCreator.create_artist_id(@person.id, @artist.id, nil)
      end


      context "does not successfully send to iTunes" do
        it "raises an error" do
          allow($apple_transporter_client).to receive(:send_package).and_return({success: false})
          expect($apple_transporter_client).to receive(:send_package)
          expect{Apple::ArtistIdCreator.create_artist_id(@person.id, @artist.id, nil)}.to raise_error
        end
      end
    end

    context "when package is not valid" do
      it "raises an error" do
        allow($apple_transporter_client).to receive(:validate_package).and_return({valid: false})
        expect{Apple::ArtistIdCreator.create_artist_id(@person.id, @artist.id, nil)}.to raise_error
      end
    end

    it "removes the package dir after sending to iTunes" do
      expect(package).to receive(:cleanup)
      allow($apple_transporter_client).to receive(:send_package).and_return({success: true})
      allow($apple_transporter_client).to receive(:validate_package).and_return({valid: true})
      Apple::ArtistIdCreator.create_artist_id(@person.id, @artist.id, nil)
    end
  end
end
