require 'rails_helper'

describe BulkStoreLaunchEmail do
  let(:store)   { FactoryBot.create(:store, launched_at: Date.today+1.week) }
  let(:person1) { FactoryBot.create(:person) }
  let(:album1)  { FactoryBot.create(:album, person: person1) }
  let(:person2) { FactoryBot.create(:person) }
  let(:album2)  { FactoryBot.create(:album, person: person2) }

  before(:each) do
    FactoryBot.create(:salepoint_subscription, album: album1, finalized_at: Time.now, effective: Time.now)
    FactoryBot.create(:salepoint_subscription, album: album2, finalized_at: Time.now, effective: Time.now)

  end
  describe ".send_store_launch_emails" do
    after(:each) do
      $redis.del("StoreEmails:#{store.short_name}")
    end

    context "store has not been launched" do
      it "raises an error" do
        store.update_attribute(:launched_at, nil)
        expect{BulkStoreLaunchEmail.send_store_launch_emails(store.id)}.to raise_error
      end
    end
    context "email list is present in Redis" do
      before(:each) do
        releases_by_person = {person1.id=>[album1.id], person2.id=>[album2.id]}
        $redis.set("StoreEmails:#{store.short_name}", releases_by_person.to_json)
      end

      it "enqueue a StoreLaunchEmailWorker for each person" do
        expect(StoreLaunchEmailWorker).to receive(:perform_async).exactly(2).times
        BulkStoreLaunchEmail.send_store_launch_emails(store.id)
      end
    end

    context "email list is not present in Redis" do
      it "enqueue a StoreLaunchEmailWorker for each person" do
        expect(StoreLaunchEmailWorker).to receive(:perform_async).exactly(2).times
        BulkStoreLaunchEmail.send_store_launch_emails(store.id)
      end
    end

    context "with album that has been taken down" do
      it "does not enqueue a StoreLaunchEmailWorker" do
        album2.update_attribute(:takedown_at, Time.now)
        expect(StoreLaunchEmailWorker).to receive(:perform_async).exactly(1).times
        BulkStoreLaunchEmail.send_store_launch_emails(store.id)
      end
    end
  end
end
