require "rails_helper"
describe AdminDormancyService do
  before(:each) do
    @request = double(ActionController::TestRequest, remote_ip: '1.0.1')
    @admin   = Person.find_by(email: "super_admin@tunecore.com")
    @person  = create(:person, dormant: true, recent_login: Time.current - 3.year)
  end

  describe "remove_person_dormancy" do
    context "note fails" do
      it "flashes error" do
        allow(Note).to receive(:create!).and_raise(StandardError)
        instance = AdminDormancyService.remove_person_dormancy(@admin, @person, @request)
        expect(instance.error).to eq("Unable to save dormancy removal")
      end
      it "doesn't update dormancy" do
        allow(Note).to receive(:create!).and_raise(StandardError)
        AdminDormancyService.remove_person_dormancy(@admin, @person, @request)
        @person.reload
        expect(@person.dormant).to be(true)
      end
    end
    context "update fails" do
      it "flashes error" do
        allow_any_instance_of(Person).to receive(:update!).and_raise(StandardError)
        instance = AdminDormancyService.remove_person_dormancy(@admin, @person, @request)
        expect(instance.error).to eq("Unable to save dormancy removal")
      end
      it "doesn't update dormancy" do
        allow_any_instance_of(Person).to receive(:update!).and_raise(StandardError)
        AdminDormancyService.remove_person_dormancy(@admin, @person, @request)
        @person.reload
        expect(@person.dormant).to be(true)
      end
    end
    context "success" do
      it "removes dormancy" do
        AdminDormancyService.remove_person_dormancy(@admin, @person, @request)
        @person.reload
        expect(@person.dormant).to be(false)
      end
      it "sets recent_login" do
        old_login = @person.recent_login
        AdminDormancyService.remove_person_dormancy(@admin, @person, @request)
        @person.reload
        expect(@person.recent_login).to be > old_login
      end
      it "creates note" do
        old_num_of_notes = Note.count
        AdminDormancyService.remove_person_dormancy(@admin, @person, @request)
        expect(Note.count).to eq(old_num_of_notes + 1)
        expect(Note.last.note).to eq("Removed Dormant Status for Person_id: #{@person.id}. Recent Login Updated.")
      end
    end
  end
end
