require "rails_helper"

describe SalepointRemovalService do
  describe "#remove_unsupported_salepoints" do
    let(:album) { create(:album) }

    context "Qobuz" do
      it "removes Qobuz when invalid for Qobuz and another salepoint exists" do
        create(:salepoint, store: Store.find(Store::QOBUZ_STORE_ID), salepointable: album)
        create(:salepoint, store: Store.find(Store::GOOGLE_STORE_ID), salepointable: album)

        allow_any_instance_of(SalepointRemovalService).to receive(:valid_qobuz?).and_return false
        SalepointRemovalService.remove_unsupported_salepoints(album)
        album.reload

        expect(album.salepoints.map(&:store_id).include?(Store::QOBUZ_STORE_ID)).to be false
      end

      it "keeps Qobuz when valid for Qobuz" do
        create(:salepoint, store: Store.find(Store::QOBUZ_STORE_ID), salepointable: album)
        create(:salepoint, store: Store.find(Store::GOOGLE_STORE_ID), salepointable: album)

        allow_any_instance_of(SalepointRemovalService).to receive(:valid_qobuz?).and_return true
        SalepointRemovalService.remove_unsupported_salepoints(album)
        album.reload

        expect(album.salepoints.map(&:store_id).include?(Store::QOBUZ_STORE_ID)).to be true
      end

      it "keeps Qobuz when invalid for Qobuz but it's the only salepoint" do
        create(:salepoint, store: Store.find(Store::QOBUZ_STORE_ID), salepointable: album)

        allow_any_instance_of(SalepointRemovalService).to receive(:valid_qobuz?).and_return false
        SalepointRemovalService.remove_unsupported_salepoints(album)
        album.reload

        expect(album.salepoints.map(&:store_id).include?(Store::QOBUZ_STORE_ID)).to be true
      end
    end
  end
end
