require "rails_helper"

describe MassTakedown do
  let(:admin)         { Person.find_by_name!("Super Admin") }

  let(:ytsr_store_id) { Store.find_by(abbrev: "ytsr").id }
  let(:fbt_store_id)  { Store.find_by(abbrev: "fbt").id }
  let(:ig_store_id)   { Store.find_by(abbrev: "ig").id }

  let(:store)         { Store.mass_takedown_stores.first }
  let(:person)        { create(:person) }
  let!(:album)   { create(
      :album,
      :finalized,
      :with_uploaded_songs_and_artwork,
      number_of_songs: 2,
      person: person)
  }

  let(:album_upc) { album.upcs.first.number}

  describe "Un-doing takedowns for given albums for selected stores" do
    context "removing takedowns for a normal store" do
      it "removes takedown for selected stores on a given album" do
        spotify_salepoint = create(:salepoint, salepointable: album, store_id: 26, takedown_at: Time.now)
        tidal_salepoint = create(:salepoint, salepointable: album, store_id: 39, takedown_at: Time.now)
        params = { album_ids: album.id.to_s, selected_stores: [26, 39] }

        salepoints = [spotify_salepoint, tidal_salepoint]
        salepoints.each { |sp| album.salepoints << salepoints }

        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
        expect(Sns::Notifier).to receive(:perform).with(
          topic_arn: ENV.fetch('SNS_RELEASE_UNTAKEDOWN_TOPIC'),
          album_ids_or_upcs: [album.id],
          store_ids: [26, 39],
          delivery_type: 'untakedown',
          person_id: admin.id
        )
        MassTakedown.mass_untakedown(params[:album_ids], params[:selected_stores], admin)
        expect(spotify_salepoint.reload.takedown_at).to be(nil)
        expect(tidal_salepoint.reload.takedown_at).to be(nil)
      end
    end

    context "removing takedown from album" do
      it "removes takedown for selected stores and on a given album" do
        spotify_salepoint = create(:salepoint, salepointable: album, store_id: 26, takedown_at: Time.now)
        tidal_salepoint = create(:salepoint, salepointable: album, store_id: 39, takedown_at: Time.now)
        params = { album_ids: album.id.to_s, selected_stores: [26, 39] }

        salepoints = [spotify_salepoint, tidal_salepoint]
        salepoints.each { |sp| album.salepoints << salepoints }

        MassTakedown.mass_untakedown(params[:album_ids], params[:selected_stores], admin)
        expect(spotify_salepoint.reload.takedown_at).to be(nil)
        expect(tidal_salepoint.reload.takedown_at).to be(nil)
        expect(album.reload.takedown_at).to be(nil)
      end
    end

    context "removing takedown for a monetized store" do
      let(:ytsr_salepoint)  { create(:salepoint, salepointable: album, store_id: ytsr_store_id, takedown_at: Time.now) }
      let(:ig_salepoint)    { create(:salepoint, salepointable: album, store_id: ig_store_id, takedown_at: Time.now) }

      before(:each) do
        salepoints = [ytsr_salepoint, ig_salepoint]
        salepoints.each { |sp| album.salepoints << sp }
      end

      context "when passed in store_ids for track monetizable stores" do
        it "removes takedowns for each relevant track_monetization" do
          track_monetization_store_ids = Store::TRACK_MONETIZATION_STORES

          track_monetizations = track_monetization_store_ids.map do |store_id|
            create(
              :track_monetization,
              store_id: store_id,
              song_id: album.songs.first.id,
              person_id: album.person.id,
              takedown_at: Time.now
            )
          end

          params = {
            album_ids: album.id.to_s,
            selected_stores: track_monetization_store_ids
          }

          MassTakedown.mass_untakedown(params[:album_ids], params[:selected_stores], admin)

          takedown_ats = track_monetizations.map{ |track_monetization| track_monetization.reload.takedown_at }

          expect(takedown_ats.all?(&:nil?)).to be_truthy
        end

        context "when one of those store_ids is for youtube" do
          it "removes a takedown from youtube track_monetizations" do
            track_monetization = create(
              :track_monetization,
              store_id: ytsr_store_id,
              song_id: album.songs.first.id,
              person_id: album.person.id,
              takedown_at: Time.now
            )

            params = {
              album_ids: album.id.to_s,
              selected_stores: [ytsr_store_id]
            }

            MassTakedown.mass_untakedown(params[:album_ids], params[:selected_stores], admin)

            expect(track_monetization.reload.takedown_at).to be(nil)
          end

          it "does not remove a takedown for a ytsr salepoint" do
            params = {
              album_ids: album.id.to_s,
              selected_stores: [ytsr_store_id]
            }

            expect_any_instance_of(Salepoint).to_not receive(:remove_takedown)

            MassTakedown.mass_untakedown(params[:album_ids], params[:selected_stores], admin)
          end

          it "returns no errors" do
            track_monetization = create(
              :track_monetization,
              store_id: ytsr_store_id,
              song_id: album.songs.first.id,
              person_id: album.person.id,
              takedown_at: Time.now
            )

            params = {
              album_ids: album.id.to_s,
              selected_stores: [ytsr_store_id]
            }

            mass_untakedown = MassTakedown.mass_untakedown(params[:album_ids], params[:selected_stores], admin)

            expect(mass_untakedown.errors.present?).to be_falsy
          end
        end

        context "when one of those store_ids is for facebook" do
          let!(:track_monetization) { create(:track_monetization, song_id: album.songs.first.id, person_id: album.person.id, takedown_at: Time.now) }

          it "removes a takedown from fb tracks" do
            params = { album_ids: album.id.to_s, selected_stores: [fbt_store_id.to_s] }
            MassTakedown.mass_untakedown(params[:album_ids], params[:selected_stores], admin)
            expect(track_monetization.reload.takedown_at).to be(nil)
          end
        end
      end
    end

    context "and the release has automator" do
      it "should add salepoints it missed when the store launched and it was taken down using Album.process_missing_automator_salepoints" do
        effective_date = Store.find_by(short_name: "KKBox").launched_at - 1.days
        create(:salepoint, salepointable: album, store_id: 26, takedown_at: Time.now)
        create(:salepoint, salepointable: album, store_id: 39, takedown_at: Time.now)
        create(:salepoint_subscription, album: album, effective: effective_date, finalized_at: effective_date)
        params = { album_ids: album.id.to_s, selected_stores: [26, 39] }

        allow(Inventory).to receive(:use!).and_return([])
        allow_any_instance_of(Album).to receive(:process_missing_automator_salepoints)
        expect_any_instance_of(Album).to receive(:process_missing_automator_salepoints)

        MassTakedown.mass_untakedown(params[:album_ids], params[:selected_stores], admin)
      end
    end
  end
end
