require "rails_helper"

describe Transient::PublishingChangeover::PublishingChangeoverMigrator do
  before(:each) do
    # Some records are pre-loaded before this test which need to be destroyed for testing accuracy
    Song.destroy_all
    Composition.destroy_all
    MumaSong.destroy_all
  end

  describe ".migrate" do
    describe "#migrate_composers" do
      context "when there are composers that need primary publishing_composers" do
        it "creates primary publishing_composers for those composers" do
          composer = create(:composer)
          account = composer.account

          expect(account.publishing_composers.count).to eq(0)

          Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

          account.reload

          expect(account.publishing_composers.count).to eq(1)
          expect(account.primary_composers.count).to eq(1)

          publishing_composer = account.primary_composers.first

          expect(publishing_composer.is_primary_composer).to eq(true)
          expect(publishing_composer.is_unknown).to eq(false)

          expect(publishing_composer.person_id).to eq(composer.person_id)
          expect(publishing_composer.account_id).to eq(composer.account_id)
          expect(publishing_composer.lod_id).to eq(composer.lod_id)
          expect(publishing_composer.publisher_id).to eq(composer.publisher_id)
          expect(publishing_composer.publishing_role_id).to eq(composer.publishing_role_id)
          expect(publishing_composer.performing_rights_organization_id).to eq(composer.performing_rights_organization_id)
          expect(publishing_composer.first_name).to eq(composer.first_name)
          expect(publishing_composer.last_name).to eq(composer.last_name)
          expect(publishing_composer.address_1).to eq(composer.address_1)
          expect(publishing_composer.address_2).to eq(composer.address_2)
          expect(publishing_composer.city).to eq(composer.city)
          expect(publishing_composer.state).to eq(composer.state)
          expect(publishing_composer.zip).to eq(composer.zip)
          expect(publishing_composer.cae).to eq(composer.cae)
          expect(publishing_composer.email).to eq(composer.email)
          expect(publishing_composer.phone).to eq(composer.phone)
          expect(publishing_composer.agreed_to_terms_at).to eq(composer.agreed_to_terms_at)
          expect(publishing_composer.name_prefix).to eq(composer.name_prefix)
          expect(publishing_composer.name_suffix).to eq(composer.name_suffix)
          expect(publishing_composer.alternate_email).to eq(composer.alternate_email)
          expect(publishing_composer.alternate_phone).to eq(composer.alternate_phone)
          expect(publishing_composer.middle_name).to eq(composer.middle_name)
          expect(publishing_composer.dob).to eq(composer.dob)
          expect(publishing_composer.nosocial).to eq(composer.nosocial)
          expect(publishing_composer.terminated_at).to eq(composer.terminated_at)
          expect(publishing_composer.provider_identifier).to eq(composer.provider_identifier)
          expect(publishing_composer.perform_live).to eq(composer.perform_live)
          expect(publishing_composer.sync_opted_in).to eq(composer.sync_opted_in)
          expect(publishing_composer.sync_opted_updated_at).to eq(composer.sync_opted_updated_at)
        end
      end

      context "when there are composers that already have primary publishing_composers" do
        it "does not create other primary publishing_composers for those composers" do
          composer = create(:composer)
          account = composer.account

          publishing_composer = create(:publishing_composer, legacy_composer_id: composer.id)
          account.publishing_composers << publishing_composer

          account.save

          expect(PublishingComposer.primary.count).to eq(1)

          expect(account.publishing_composers.first.id).to eq(publishing_composer.id)

          Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

          account.reload

          expect(PublishingComposer.primary.count).to eq(1)

          expect(account.publishing_composers.first.id).to eq(publishing_composer.id)
        end
      end

      context "when there are no composers that need primary publishing_composers" do
        it "does not create publishing_composers" do
          expect(Composer.count).to eq(0)

          expect(PublishingComposer.count).to eq(0)

          Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

          expect(PublishingComposer.count).to eq(0)
        end
      end
    end

    describe "#migrate_cowriters" do
      context "when there are cowriters that need cowriting publishing_composers" do
        it "creates cowriting publishing_composers for those cowriters" do
          cowriter = create(:cowriter)
          account = cowriter.account
          composer = cowriter.composer

          expect(account.cowriting_composers.count).to eq(0)

          Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

          account.reload

          expect(account.cowriting_composers.count).to eq(1)

          publishing_composer = account.cowriting_composers.first

          expect(publishing_composer.is_primary_composer).to eq(false)

          expect(publishing_composer.first_name).to eq(cowriter.first_name)
          expect(publishing_composer.last_name).to eq(cowriter.last_name)
          expect(publishing_composer.provider_identifier).to eq(cowriter.provider_identifier)
          expect(publishing_composer.email).to eq(cowriter.email)
          expect(publishing_composer.is_unknown).to eq(cowriter.is_unknown)
          expect(publishing_composer.person_id).to eq(composer.person_id)
          expect(publishing_composer.account_id).to eq(composer.account_id)
          expect(publishing_composer.lod_id).to eq(composer.lod_id)
          expect(publishing_composer.publisher_id).to eq(composer.publisher_id)
          expect(publishing_composer.publishing_role_id).to eq(composer.publishing_role_id)
          expect(publishing_composer.performing_rights_organization_id).to eq(composer.performing_rights_organization_id)
        end
      end

      context "when there are composers that already have cowriting publishing_composers" do
        it "does not create other cowriting publishing_composers for those composers" do
          cowriter = create(:cowriter)
          account = cowriter.account

          publishing_composer = create(:publishing_composer, :cowriter, legacy_cowriter_id: cowriter.id)
          account.publishing_composers << publishing_composer

          account.save

          expect(PublishingComposer.cowriting.count).to eq(1)

          expect(account.cowriting_composers.first.id).to eq(publishing_composer.id)

          Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

          account.reload

          expect(PublishingComposer.cowriting.count).to eq(1)

          expect(account.cowriting_composers.first.id).to eq(publishing_composer.id)
        end
      end

      context "when there are no composers that need cowriting publishing_composers" do
        it "does not create publishing_composers" do
          expect(Cowriter.count).to eq(0)

          expect(PublishingComposer.count).to eq(0)

          Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

          expect(PublishingComposer.count).to eq(0)
        end
      end
    end

    describe "#migrate_compositions" do
      context "when there are compositions that need publishing_compositions" do
        it "creates publishing_compositions for those compositions" do
          composition = create(:composition, :with_songs)
          song = composition.songs.first

          expect(song.publishing_composition).to be_falsy

          Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

          song.reload

          expect(song.publishing_composition).to be_truthy

          publishing_composition = song.publishing_composition

          expect(publishing_composition.name).to eq(composition.name)
          expect(publishing_composition.iswc).to eq(composition.iswc)
          expect(publishing_composition.is_unallocated_sum).to eq(composition.is_unallocated_sum)
          expect(publishing_composition.state).to eq(composition.state)
          expect(publishing_composition.prev_state).to eq(composition.prev_state)
          expect(publishing_composition.state_updated_on).to eq(composition.state_updated_on)
          expect(publishing_composition.provider_identifier).to eq(composition.provider_identifier)
          expect(publishing_composition.translated_name).to eq(composition.translated_name)
          expect(publishing_composition.verified_date).to eq(composition.verified_date)
        end
      end

      context "when there are compositions that already have publishing_compositions" do
        it "does not create other publishing_compositions for those compositions" do
          composition = create(:composition, :with_songs)
          song = composition.songs.first

          publishing_composition = create(:publishing_composition, legacy_composition_id: composition.id)
          song.publishing_composition = publishing_composition

          song.save

          expect(PublishingComposition.count).to eq(1)

          expect(song.publishing_composition.id).to eq(publishing_composition.id)

          Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

          song.reload

          expect(PublishingComposition.count).to eq(1)

          expect(song.publishing_composition.id).to eq(publishing_composition.id)
        end
      end

      context "when there are no compositions that need publishing_compositions" do
        it "does not create publishing_compositions" do
          expect(Composition.count).to eq(0)

          expect(PublishingComposition.count).to eq(0)

          Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

          expect(PublishingComposition.count).to eq(0)
        end
      end
    end

    describe "#migrate_composer_publishing_splits" do
      before(:each) do
        allow_any_instance_of(Composer).to receive(:create_unallocated_sum_composition).and_return(nil)
        allow_any_instance_of(PublishingComposer).to receive(:create_unallocated_sum_publishing_composition).and_return(nil)
      end

      context "when there are composer publishing_splits that need publishing_composition_splits" do
        it "creates collectable publishing_composition_splits for those publishing_splits" do
          publishing_split = create(:publishing_split)

          expect(PublishingCompositionSplit.collectable.count).to eq(0)

          Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

          expect(PublishingCompositionSplit.collectable.count).to eq(1)

          publishing_composition_split = PublishingCompositionSplit.collectable.first

          expect(publishing_composition_split.percent).to eq(publishing_split.percent)
          expect(publishing_composition_split.right_to_collect).to eq(true)
        end
      end

      context "when there are composer publishing_splits that already have publishing_composition_splits" do
        it "does not create other collectable publishing_composition_splits for those publishing_splits" do
          publishing_split = create(:publishing_split)

          create(:publishing_composition_split, :collectable, legacy_publishing_split_id: publishing_split.id)

          expect(PublishingCompositionSplit.collectable.count).to eq(1)

          Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

          expect(PublishingCompositionSplit.collectable.count).to eq(1)
        end
      end

      context "when there are no composer publishing_splits that need publishing_composition_splits" do
        it "does not create publishing_composition_splits" do
          expect(PublishingSplit.count).to eq(0)

          expect(PublishingCompositionSplit.collectable.count).to eq(0)

          Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

          expect(PublishingCompositionSplit.collectable.count).to eq(0)
        end
      end
    end

    describe "#migrate_cowriter_publishing_splits" do
      before(:each) do
        allow_any_instance_of(Composer).to receive(:create_unallocated_sum_composition).and_return(nil)
        allow_any_instance_of(PublishingComposer).to receive(:create_unallocated_sum_publishing_composition).and_return(nil)
      end

      context "when there are cowriter publishing_splits that need publishing_composition_splits" do
        it "creates non_collectable publishing_composition_splits for those publishing_splits" do
          cowriter = create(:cowriter)
          publishing_split = create(:publishing_split, writer: cowriter)

          expect(PublishingCompositionSplit.non_collectable.count).to eq(0)

          Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

          expect(PublishingCompositionSplit.non_collectable.count).to eq(1)

          publishing_composition_split = PublishingCompositionSplit.non_collectable.first

          expect(publishing_composition_split.percent).to eq(publishing_split.percent)
          expect(publishing_composition_split.right_to_collect).to eq(false)
        end
      end

      context "when there are cowriter publishing_splits that already have publishing_composition_splits" do
        it "does not create other non_collectable publishing_composition_splits for those publishing_splits" do
          cowriter = create(:cowriter)
          publishing_split = create(:publishing_split, writer: cowriter)

          create(:publishing_composition_split, :non_collectable, legacy_publishing_split_id: publishing_split.id)

          expect(PublishingCompositionSplit.non_collectable.count).to eq(1)

          Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

          expect(PublishingCompositionSplit.non_collectable.count).to eq(1)
        end
      end

      context "when there are no cowriter publishing_splits that need publishing_composition_splits" do
        it "does not create publishing_composition_splits" do
          expect(PublishingSplit.count).to eq(0)

          expect(PublishingCompositionSplit.non_collectable.count).to eq(0)

          Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

          expect(PublishingCompositionSplit.non_collectable.count).to eq(0)
        end
      end
    end

    describe "#migrate_legal_documents" do
      before(:each) do
        allow_any_instance_of(Composer).to receive(:create_unallocated_sum_composition).and_return(nil)
        allow_any_instance_of(PublishingComposer).to receive(:create_unallocated_sum_publishing_composition).and_return(nil)
      end

      context "when there are composer legal_documents that need counterpart publishing_composer legal_documents" do
        it "creates counterpart publishing_composer legal_documents for those composer legal_documents" do
          composer = create(:composer)
          create(:legal_document, subject: composer, name: "Very important legal document name harumph harumph")

          publishing_composer = create(:publishing_composer, legacy_composer_id: composer.id, person: composer.person, account: composer.account)

          expect(LegalDocument.where(subject_type: "PublishingComposer").count).to eq(0)

          Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

          expect(LegalDocument.where(subject_type: "PublishingComposer").count).to eq(1)

          legal_document = publishing_composer.legal_documents.where(subject_type: "PublishingComposer").first

          expect(legal_document.name).to eq("Very important legal document name harumph harumph")
        end
      end

      context "when there are composer legal_documents that already have counterpart publishing_composer legal_documents" do
        it "does not create other counterpart publishing_composer legal_documents for those composer legal_documents" do
          composer = create(:composer)
          create(:legal_document, subject: composer)

          publishing_composer = create(:publishing_composer, legacy_composer_id: composer.id, person: composer.person, account: composer.account)
          create(:legal_document, subject: publishing_composer)

          expect(LegalDocument.where(subject_type: "PublishingComposer").count).to eq(1)

          Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

          expect(LegalDocument.where(subject_type: "PublishingComposer").count).to eq(1)
        end
      end
    end

    describe "#backfill_publishing_composer_ids" do
      let(:composer) { create(:composer) }

      it "backfills the publishing_composer_id column for non_tunecore_albums" do
        non_tunecore_album = create(:non_tunecore_album, composer: composer, publishing_composer_id: nil)

        expect(non_tunecore_album.publishing_composer_id).to be_falsy

        Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

        non_tunecore_album.reload

        expect(non_tunecore_album.publishing_composer_id).to be_truthy
        expect(non_tunecore_album.publishing_composer.legacy_composer_id).to eq(composer.id)
      end

      it "backfills the publishing_composer_id column for terminated_composers" do
        terminated_composer = create(:terminated_composer, :fully, composer: composer, publishing_composer_id: nil)

        expect(terminated_composer.publishing_composer_id).to be_falsy

        Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

        terminated_composer.reload

        expect(terminated_composer.publishing_composer_id).to be_truthy
        expect(terminated_composer.publishing_composer.legacy_composer_id).to eq(composer.id)
      end

      it "backfills the publishing_composer_id column for tax_info" do
        tax_info = create(:tax_info, tax_id: '908881234', composer: composer, publishing_composer_id: nil)

        expect(tax_info.publishing_composer_id).to be_falsy

        Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

        tax_info.reload

        expect(tax_info.publishing_composer_id).to be_truthy
        expect(tax_info.publishing_composer.legacy_composer_id).to eq(composer.id)
      end

      it "backfills the publishing_composer_id column for royalty_payments" do
        royalty_payment = create(:royalty_payment, composer: composer, publishing_composer_id: nil)

        expect(royalty_payment.publishing_composer_id).to be_falsy

        Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

        royalty_payment.reload

        expect(royalty_payment.publishing_composer_id).to be_truthy
        expect(royalty_payment.publishing_composer.legacy_composer_id).to eq(composer.id)
      end
    end

    describe "#backfill_publishing_composition_ids" do
      let(:composition) { create(:composition) }

      it "backfills the publishing_composition_id column for non_tunecore_songs" do
        non_tunecore_song = create(:non_tunecore_song, composition: composition, publishing_composition_id: nil)

        expect(non_tunecore_song.publishing_composition_id).to be_falsy

        Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

        non_tunecore_song.reload

        expect(non_tunecore_song.publishing_composition_id).to be_truthy
        expect(non_tunecore_song.publishing_composition.legacy_composition_id).to eq(composition.id)
      end

      it "backfills the publishing_composition_id column for songs" do
        song = create(:song, composition: composition, publishing_composition_id: nil)

        expect(song.publishing_composition_id).to be_falsy

        Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

        song.reload

        expect(song.publishing_composition_id).to be_truthy
        expect(song.publishing_composition.legacy_composition_id).to eq(composition.id)
      end

      it "backfills the publishing_composition_id column for muma_songs" do
        muma_song = create(:muma_song, code: 1, composition: composition, publishing_composition_id: nil)

        expect(muma_song.publishing_composition_id).to be_falsy

        Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

        muma_song.reload

        expect(muma_song.publishing_composition_id).to be_truthy
        expect(muma_song.publishing_composition.legacy_composition_id).to eq(composition.id)
      end

      it "backfills the publishing_composition_id column for recordings" do
        recording = create(:recording, composition: composition, publishing_composition_id: nil)

        expect(recording.publishing_composition_id).to be_falsy

        Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

        recording.reload

        expect(recording.publishing_composition_id).to be_truthy
        expect(recording.publishing_composition.legacy_composition_id).to eq(composition.id)
      end
    end

    describe "#backfill_account_ids" do
      context "when a publishing_composition has a song" do
        it "backfills the account_id column of the publishing_composition" do
          publishing_composition = create(:publishing_composition, :with_songs)
          person = publishing_composition.songs.first.person
          account = create(:account, person: person)

          expect(publishing_composition.account_id).to be_falsy

          Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

          publishing_composition.reload

          expect(publishing_composition.account_id).to eq(account.id)
        end
      end

      context "when a publishing_composition has a non_tunecore_song" do
        it "backfills the account_id column of the publishing_composition" do
          publishing_composition = create(:publishing_composition, :with_non_tunecore_songs)
          non_tunecore_album = publishing_composition.non_tunecore_songs.first.non_tunecore_album

          person = non_tunecore_album.person
          account = create(:account, person: person)

          non_tunecore_album.account = account
          non_tunecore_album.save

          expect(publishing_composition.account_id).to be_falsy

          Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

          publishing_composition.reload

          expect(publishing_composition.account_id).to eq(account.id)
        end
      end

      context "when a publishing_composition has a publishing_composition_split" do
        it "backfills the account_id column of the publishing_composition" do
          publishing_composition = create(:publishing_composition)
          publishing_composer = create(:publishing_composer)
          account = publishing_composer.account

          publishing_composition_split = create(:publishing_composition_split,
            publishing_composer: publishing_composer,
            publishing_composition: publishing_composition
          )

          expect(publishing_composition.account_id).to be_falsy

          Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

          publishing_composition.reload

          expect(publishing_composition.account_id).to eq(account.id)
        end
      end
    end

    describe "#backfill_unpaid_purchases" do
      context "when there are unpaid composer purchases" do
        it "updates the purchase's related_type and related_id to that composer's respective publishing_composer" do
          composer = create(:composer)
          purchase = create(:purchase, :unpaid, related: composer, person_id: composer.publishing_administrator.id)

          expect(Purchase.where(paid_at: nil, related_type: "Composer").count).to eq(1)
          expect(Purchase.where(paid_at: nil, related_type: "PublishingComposer").count).to eq(0)

          expect(purchase.related_type).to eq("Composer")
          expect(purchase.related_id).to eq(composer.id)

          Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate


          expect(Purchase.where(paid_at: nil, related_type: "Composer").count).to eq(0)
          expect(Purchase.where(paid_at: nil, related_type: "PublishingComposer").count).to eq(1)

          purchase.reload

          expect(purchase.related_type).to eq("PublishingComposer")
          expect(purchase.related_id).to eq(PublishingComposer.find_by(legacy_composer_id: composer.id).id)
        end
      end

      context "when there are paid composer purchases" do
        it "does not update the purchase's related_type and related_id" do
          composer = create(:composer, :with_pub_admin_purchase)
          purchase = composer.related_purchases.first

          expect(Purchase.where(related_type: "Composer").count).to eq(1)
          expect(Purchase.where(related_type: "PublishingComposer").count).to eq(0)

          expect(purchase.related_type).to eq("Composer")
          expect(purchase.related_id).to eq(composer.id)

          Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate

          expect(Purchase.where(related_type: "Composer").count).to eq(1)
          expect(Purchase.where(related_type: "PublishingComposer").count).to eq(0)

          purchase.reload

          expect(purchase.related_type).to eq("Composer")
          expect(purchase.related_id).to eq(composer.id)
        end
      end
    end
  end
end
