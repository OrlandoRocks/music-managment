require "rails_helper"

describe Transient::YtsrTrackMonetizationMigrator do
  describe "::migrate" do
    context "when there are songs that need youtube track_monetizations" do
      it "creates youtube track_monetizations for those songs" do
        person = create(:person, :with_ytsr)
        album = create(:album, :paid, :with_salepoint_song, person: person)
        song = album.songs.first

        expect(song.track_monetizations.count).to eq(0)

        Transient::YtsrTrackMonetizationMigrator.migrate

        song.reload

        expect(song.track_monetizations.count).to eq(1)

        expect(song.track_monetizations.first.store_id).to eq(Store::YTSR_STORE_ID)
      end

      it "creates valid youtube track_monetizations with default values" do
        person = create(:person, :with_ytsr)
        album = create(:album, :paid, :with_salepoint_song, person: person)
        song = album.songs.first
        song.salepoint_songs.first.update(state: 'approved')

        expect(song.track_monetizations.count).to eq(0)

        Transient::YtsrTrackMonetizationMigrator.migrate

        song.reload

        track_monetization = song.track_monetizations.find_by(store_id: Store::YTSR_STORE_ID)

        expect(track_monetization.valid?).to be_truthy

        expect(track_monetization.state).to eq("new")
        expect(track_monetization.delivery_type).to eq("full_delivery")
        expect(track_monetization.eligibility_status).to eq("eligible")
      end

      context "when the songs have a ytm_blocked_song attached" do
        it "creates blocked youtube track_monetizations for those songs" do
          person = create(:person, :with_ytsr)
          album = create(:album, :paid, :with_salepoint_song, person: person)
          song = album.songs.first
          song.salepoint_songs.first.update(state: 'blocked')
          create(:ytm_blocked_song, song: song)

          Transient::YtsrTrackMonetizationMigrator.migrate

          song.reload

          expect(song.track_monetizations.first.store_id).to eq(Store::YTSR_STORE_ID)
          expect(song.track_monetizations.first.state).to eq("blocked")
        end
      end

      context "when the songs have a ytm_ineligible_song attached" do
        it "creates ineligible youtube track_monetizations for those songs" do
          person = create(:person, :with_ytsr)
          album = create(:album, :paid, :with_salepoint_song, person: person)
          song = album.songs.first
          song.salepoint_songs.first.update(state: 'rejected')
          create(:ytm_ineligible_song, song: song)

          Transient::YtsrTrackMonetizationMigrator.migrate

          song.reload

          expect(song.track_monetizations.first.store_id).to eq(Store::YTSR_STORE_ID)
          expect(song.track_monetizations.first.eligibility_status).to eq("ineligible")
        end
      end

      context "when a limit is passed in" do
        it "only creates that number of youtube track_monetizations" do
          person = create(:person, :with_ytsr)

          3.times do
            create(:album, :paid, :with_salepoint_song, person: person)
          end

          expect(TrackMonetization.where(store_id: Store::YTSR_STORE_ID).count).to eq(0)

          Transient::YtsrTrackMonetizationMigrator.migrate(limit: 2)

          expect(TrackMonetization.where(store_id: Store::YTSR_STORE_ID).count).to eq(2)
        end

        context "when the limit passed in exceeds the MAX_LIMIT" do
          it "does nothing" do
            person = create(:person, :with_ytsr)
            album = person.albums.first

            3.times do
              create(:song, album: album)
            end

            expect(TrackMonetization.where(store_id: Store::YTSR_STORE_ID).count).to eq(0)

            max_limit = Transient::YtsrTrackMonetizationMigrator::MAX_LIMIT

            Transient::YtsrTrackMonetizationMigrator.migrate(limit: max_limit + 1)

            expect(TrackMonetization.where(store_id: Store::YTSR_STORE_ID).count).to eq(0)
          end
        end
      end
    end

    context "when there are songs that already have youtube track_monetizations" do
      it "does not create other youtube track_monetizations for those songs" do
        person = create(:person, :with_ytsr)
        album = person.albums.first
        song = create(:song, album: album)
        track_monetization = create(:track_monetization, song: song, store_id: Store::YTSR_STORE_ID)

        expect(TrackMonetization.where(store_id: Store::YTSR_STORE_ID).count).to eq(1)

        expect(song.track_monetizations.first.id).to eq(track_monetization.id)

        Transient::YtsrTrackMonetizationMigrator.migrate

        song.reload

        expect(TrackMonetization.where(store_id: Store::YTSR_STORE_ID).count).to eq(1)

        expect(song.track_monetizations.first.id).to eq(track_monetization.id)
      end
    end

    context "when there are no songs that need youtube track_monetizations" do
      it "does not create youtube track_monetizations" do
        person = create(:person, :with_live_album)
        album = person.albums.first
        song = create(:song, album: album)

        expect(TrackMonetization.where(store_id: Store::YTSR_STORE_ID).count).to eq(0)

        Transient::YtsrTrackMonetizationMigrator.migrate

        expect(TrackMonetization.where(store_id: Store::YTSR_STORE_ID).count).to eq(0)
      end
    end
  end
end
