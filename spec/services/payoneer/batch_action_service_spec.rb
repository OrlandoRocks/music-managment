require "rails_helper"

describe Payoneer::BatchActionsService do
  let(:admin)  { create(:person, :admin)}
  let(:person) { create(:person) }
  let(:payout_provider) { create(:payout_provider, person: person) }

  let(:payout_transfer1)        { create(:payout_transfer, tunecore_status: PayoutTransfer::SUBMITTED, payout_provider: payout_provider, amount_cents: 100000) }
  let(:payout_transfer2)        { create(:payout_transfer, tunecore_status: PayoutTransfer::SUBMITTED, payout_provider: payout_provider, amount_cents: 200000) }
  let(:invalid_payout_transfer) { create(:payout_transfer, tunecore_status: PayoutTransfer::APPROVED, payout_provider: payout_provider, amount_cents: 10) }

  describe "#send_actions" do

    it "approves multiple valid forms" do
      admin.roles << Role.find_by(name: "Payout Service")
      transfer_ids = [payout_transfer1.id, payout_transfer2.id]
      service_params = { action: "approve", transfer_ids: transfer_ids, person: admin, referrer_url: "sample-url" }

      submit_payout = double(:submit_payout, {
          success?: true,
          contents: JSON.dump({payout_id: 12}),
          description: "details",
          code: "0"
      })
      fake_api_service = double(:fake_api_service, submit_payout: submit_payout)
      allow(PayoutProvider::ApiService).to receive(:new).and_return(fake_api_service)

      errors = Payoneer::BatchActionsService.send_actions(service_params)

      expect(errors).to eq([])
    end

    it "successfully rejects multiple valid forms" do
      admin.roles << Role.find_by(name: "Payout Service")
      transfer_ids = [payout_transfer1.id, payout_transfer2.id]
      service_params = { action: "reject", transfer_ids: transfer_ids, person: admin }
      errors = Payoneer::BatchActionsService.send_actions(service_params)

      expect(errors).to eq([])
    end

    it "displays errors when an invalid payout transfer is submitted for approval" do
      admin.roles << Role.find_by(name: "Payout Service")
      transfer_ids = [invalid_payout_transfer.id]
      service_params = { action: "approve", transfer_ids: transfer_ids, person: admin, referrer_url: "sample-url" }

      errors = Payoneer::BatchActionsService.send_actions(service_params)

      expect(errors.size).to eq(1)
      expect(errors.first).to eq("Tunecore status is wrong status for transfer ID: #{invalid_payout_transfer.id}")
    end

    it "displays errors when an invalid payout transfer is submitted for rejection" do
      admin.roles << Role.find_by(name: "Payout Service")
      transfer_ids = [invalid_payout_transfer.id]
      service_params = { action: "reject", transfer_ids: transfer_ids, person: admin }
      errors = Payoneer::BatchActionsService.send_actions(service_params)

      expect(errors.size).to eq(1)
      expect(errors.first).to eq("Tunecore status incorrect for transfer ID: #{invalid_payout_transfer.id}")
    end
  end
end

