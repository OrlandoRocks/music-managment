require "rails_helper"

describe Payoneer::TaxApiService do
  include ActiveSupport::Testing::TimeHelpers

  before do
    travel_to Time.now.beginning_of_year
  end

  after do
    travel_back
  end

  subject!(:person) { create(:person, :with_payout_provider) }
  let(:date_format) { "%m/%d/%Y" }
  let(:response_with_one_tax_form) {
    {
      "GetSubmittedTaxFormsForPayee" => {
        "Code" => "000",
        "Description" => "OK (Request accepted)",
        "NumberOfTaxForms" => "1",
        "TaxForms" => {
          "Form" => {
            "TaxFormType"     => tax_form_type,
            "TaxFormStatus"   => tax_form_status,
            "DateOfSignature" => date_of_signature,
            "ValidUntil"      => valid_until,
            "Name"            => tax_entity_name
          }
        }
      }
    }
  }
  let(:multiple_forms_with_one_active_w8) {
    {
      "GetSubmittedTaxFormsForPayee" => {
        "Code" => "000",
        "Description" => "OK (Request accepted)",
        "NumberOfTaxForms" => "2",
        "TaxForms" => {
          "Form" => [
            {
              "TaxFormType"     => "W9 - Individual",
              "TaxFormStatus"   => "Expired",
              "DateOfSignature" => 1.year.ago.strftime(date_format),
              "ValidUntil"      => 1.year.from_now.strftime(date_format),
              "Name"            => person.name
            },
            {
              "TaxFormType"     => "W9 - Entity",
              "TaxFormStatus"   => "Expired",
              "DateOfSignature" => 1.year.ago.strftime(date_format),
              "ValidUntil"      => 1.year.from_now.strftime(date_format),
              "Name"            => person.name
            },
            {
              "TaxFormType"     => "W8BEN",
              "TaxFormStatus"   => "Submitted",
              "DateOfSignature" => 11.months.ago.strftime(date_format),
              "ValidUntil"      => 1.year.from_now.strftime(date_format),
              "Name"            => person.name
            }
          ]
        }
      }
    }
  }
  let(:multiple_forms_with_one_active_w9) {
    {
      "GetSubmittedTaxFormsForPayee" => {
        "Code" => "000",
        "Description" => "OK (Request accepted)",
        "NumberOfTaxForms" => "2",
        "TaxForms" => {
          "Form" => [
            {
              "TaxFormType"     => "W9 - Individual",
              "TaxFormStatus"   => "Expired",
              "DateOfSignature" => 1.year.ago.strftime("%m/%d/%Y"),
              "ValidUntil"      => 1.year.from_now.strftime("%m/%d/%Y")
            },
            {
              "TaxFormType"     => "W8BEN",
              "TaxFormType"     => "W9 - Entity",
              "TaxFormStatus"   => "Expired",
              "DateOfSignature" => 11.months.ago.strftime("%m/%d/%Y"),
              "ValidUntil"      => 1.year.from_now.strftime("%m/%d/%Y")
            },
            {
              "TaxFormType"     => "W9 - Entity",
              "TaxFormStatus"   => "Submitted",
              "DateOfSignature" => 11.months.ago.strftime("%m/%d/%Y"),
              "ValidUntil"      => 1.year.from_now.strftime("%m/%d/%Y")
            }
          ]
        }
      }
    }
  }
  let(:all_expired_w8s) {
    {
      "GetSubmittedTaxFormsForPayee" => {
        "Code" => "000",
        "Description" => "OK (Request accepted)",
        "NumberOfTaxForms" => "2",
        "TaxForms" => {
          "Form" => [
            {
              "TaxFormType"     => "W8BEN",
              "TaxFormStatus"   => "Expired",
              "DateOfSignature" => 1.year.ago.strftime("%m/%d/%Y"),
              "ValidUntil"      => 1.year.from_now.strftime("%m/%d/%Y")
            },
            {
              "TaxFormType"     => "W8BEN",
              "TaxFormStatus"   => "Submitted",
              "DateOfSignature" => 1.year.ago.strftime("%m/%d/%Y"),
              "ValidUntil"      => 1.month.ago.strftime("%m/%d/%Y")
            }
          ]
        }
      }
    }
  }
  let(:all_expired_w9s) {
    {
      "GetSubmittedTaxFormsForPayee" => {
        "Code" => "000",
        "Description" => "OK (Request accepted)",
        "NumberOfTaxForms" => "2",
        "TaxForms" => {
          "Form" => [
            {
              "TaxFormType"     => "W9 - Individual",
              "TaxFormStatus"   => "Expired",
              "DateOfSignature" => 1.year.ago.strftime("%m/%d/%Y"),
              "ValidUntil"      => 1.year.from_now.strftime("%m/%d/%Y")
            },
            {
              "TaxFormType"     => "W9 - Entity",
              "TaxFormStatus"   => "Submitted",
              "DateOfSignature" => 1.year.ago.strftime("%m/%d/%Y"),
              "ValidUntil"      => 1.month.ago.strftime("%m/%d/%Y")
            }
          ]
        }
      }
    }
  }

  describe "#has_active_forms?" do
    it "assembles the post params" do
      response_double = { "GetSubmittedTaxFormsForPayee" => { "TaxForms" => nil } }
      api_client_double = double(post: true, formatted_response: response_double)
      allow(Payoneer::V2::TaxApiClient).to receive(:new).and_return(api_client_double)
      service = Payoneer::TaxApiService.new(PartnerPayeeID: person.client_payee_id)
      tax_params = {
        PartnerPayeeID: person.client_payee_id,
        mname: "GetSubmittedTaxFormsForPayee",
        FromDate: Date.parse("2000-01-01"),
        ToDate: Date.today
      }

      expect(api_client_double).to receive(:post).with(tax_params)
      service.has_active_forms?
    end

    it "returns false if the api service does not return any active tax forms" do
      response_double = { "GetSubmittedTaxFormsForPayee" => {
                            "Code" => "000",
                            "Description" => "OK (Request accepted)",
                            "NumberOfTaxForms" => "0",
                            "TaxForms" => nil
                          }
                        }

      api_client_double = double(post: true, formatted_response: response_double)
      allow(Payoneer::V2::TaxApiClient).to receive(:new).and_return(api_client_double)
      service = Payoneer::TaxApiService.new(PartnerPayeeID: person.client_payee_id)
      expect(service.has_active_forms?).to be false
    end

    it "returns true if the api service returns only expired W9s" do
      response_double = { "GetSubmittedTaxFormsForPayee" => {
                            "Code" => "000",
                            "Description" => "OK (Request accepted)",
                            "NumberOfTaxForms" => "1",
                            "TaxForms" => {
                              "Form" => {
                                "TaxFormType"     => "W9 - Individual",
                                "DateOfSignature" => "12/01/2012",
                                "ValidUntil"      => "12/01/2015",
                                "TaxFormStatus"   => "Expired"
                              }
                            }
                          }
                        }

      api_client_double = double(
        post: true,
        formatted_response: response_double,
        payout_provider_config: double(id: 1, program_id: "sample-program-id")
      )
      allow(Payoneer::V2::TaxApiClient).to receive(:new).and_return(api_client_double)
      service = Payoneer::TaxApiService.new(PartnerPayeeID: person.client_payee_id)
      expect(service.has_active_forms?).to be true
    end

    it "returns false if the api service only returns expired W8s" do
      response_double = { "GetSubmittedTaxFormsForPayee" => {
                            "Code" => "000",
                            "Description" => "OK (Request accepted)",
                            "NumberOfTaxForms" => "1",
                            "TaxForms" => {
                              "Form" => {
                                "TaxFormType"     => "W8BEN",
                                "DateOfSignature" => "12/01/2012",
                                "ValidUntil"      => "12/01/2015",
                                "TaxFormStatus"   => "Expired"
                              }
                            }
                          }
                        }

      api_client_double = double(
        post: true,
        formatted_response: response_double,
        payout_provider_config: double(id: 1, program_id: "sample-program-id")
      )
      allow(Payoneer::V2::TaxApiClient).to receive(:new).and_return(api_client_double)
      service = Payoneer::TaxApiService.new(PartnerPayeeID: person.client_payee_id)
      expect(service.has_active_forms?).to be false
    end
  end

  shared_examples "single tax form" do |create_and_update|
    let(:tax_entity_name) { "Jack Somebody" }
    let(:api_client_double) do
      double(
        new: true,
        post: true,
        formatted_response: response,
        payout_provider_config: double(id: 1, program_id: "sample-program-id")
      )
    end

    before do
      allow(Payoneer::V2::TaxApiClient).to receive(:new).and_return(api_client_double)
    end

    def call_described_method
      Payoneer::TaxApiService.new(PartnerPayeeID: person.client_payee_id).save_tax_form
    end

    context "when Payoneer returns one tax form" do
      let(:response) { response_with_one_tax_form }
      let(:submitted_at) { 1.year.ago }
      let(:date_of_signature) { submitted_at.strftime(date_format) }
      let(:tax_form_type_id) { TaxFormType.where(kind: tax_form_type).pluck(:id).first }
      let(:valid_until) { expires_at.strftime(date_format) }

      before do
        tax_form.save if !create_and_update && tax_form.present?
      end

      shared_examples "saves tax form successfully" do
        it "saves a new tax form record", if: create_and_update do
          expect { call_described_method }.to change(TaxForm, :count).by(1)
        end

        it "updates an existing tax form successfully", unless: create_and_update do
          expect { call_described_method }.to change(TaxForm, :count).by(0)
        end

        it "saves the tax form with expected attributes" do
          call_described_method
          expect(person.reload.tax_forms.first).to have_attributes(
            tax_form_type_id: tax_form_type_id,
            submitted_at: submitted_at,
            expires_at: expires_at,
            provider: PayoutProvider::PAYONEER,
            payout_provider_config_id: 1,
            tax_entity_name: tax_entity_name
          )
        end
      end

      shared_examples "does not save tax form" do
        it "does not save a new tax form" do
          expect { call_described_method }.not_to change(TaxForm, :count)
        end
      end

      shared_examples "does not update existing tax form" do
        it "does not update attributes on an existing tax form" do
          call_described_method
          expect(tax_form).not_to have_attributes(
            tax_entity_name: tax_entity_name
          )
        end
      end

      context "when the tax form is a W9" do
        let(:tax_form_type) { "W9 - Individual" }
        let(:tax_form) do
          build(
            :tax_form, submitted_at: submitted_at, expires_at: expires_at,
            payout_provider_config_id: 1, person: person
          )
        end

        context "when the tax form returned from Payoneer is valid and has not expired" do
          let(:tax_form_status) { "Submitted" }

          context "with a 'valid until' date in the future" do
            let(:expires_at) { 1.year.from_now }

            context "when tax form record exists in DB" do
              include_examples "saves tax form successfully"
              include_examples "does not update existing tax form" unless create_and_update
            end

            context "when tax form record doesn't exist in DB" do
              if create_and_update
                include_examples "saves tax form successfully"
              else
                include_examples "does not save tax form"
              end
            end
          end

          context "with a 'valid until' date in the past" do
            let(:expires_at) { 1.year.ago }

            context "when tax form record exists in DB" do
              include_examples "saves tax form successfully"
              include_examples "does not update existing tax form" unless create_and_update
            end

            context "when tax form record doesn't exist in DB" do
              if create_and_update
                include_examples "saves tax form successfully"
              else
                include_examples "does not save tax form"
              end
            end
          end
        end

        context "when the tax form returned from Payoneer has expired" do
          let(:expires_at) { 1.year.from_now }
          let(:tax_form_status) { "Expired" }

          if create_and_update
            include_examples "saves tax form successfully"
          else
            include_examples "does not save tax form"
          end
          include_examples "does not update existing tax form" unless create_and_update
        end
      end

      context "when the tax form is a W8" do
        let(:tax_form_type) { "W8BEN" }
        let(:tax_form) do
          build(
            :tax_form, :w8, submitted_at: submitted_at, expires_at: expires_at,
            payout_provider_config_id: 1, person: person
          )
        end

        context "when the tax form returned from Payoneed has not expired" do
          let(:tax_form_status) { "Submitted" }

          context "with a 'valid until' date in the future" do
            let(:expires_at) { 1.year.from_now }

            context "when tax form record exists in DB" do
              include_examples "saves tax form successfully"
              include_examples "does not update existing tax form" unless create_and_update
            end

            context "when tax form record doesn't exist in DB" do
              include_examples "saves tax form successfully"
            end
          end

          context "when the 'valid until' date is in the past" do
            let(:expires_at) { 1.year.ago }

            include_examples "does not save tax form"
            include_examples "does not update existing tax form" unless create_and_update
          end
        end

        context "when the tax form returned from Payoneer has expired" do
          let(:expires_at) { 1.year.from_now }
          let(:tax_form_status) { "Expired" }

          include_examples "does not save tax form"
          include_examples "does not update existing tax form" unless create_and_update
        end
      end

      context "when an unknown tax form type is returned from Payoneer" do
        let(:tax_form) do
          build(
            :tax_form, submitted_at: submitted_at, expires_at: expires_at,
            payout_provider_config_id: 1, person: person
          )
        end
        let(:expires_at) { 1.year.from_now }
        let(:tax_form_type) { "Some New Type" }
        let(:tax_form_status) { "Submitted" }

        include_examples "does not save tax form"
        include_examples "does not update existing tax form" unless create_and_update

        it "notifies Airbrake" do
          expect(Airbrake).to receive(:notify).with(
            "Unknown TaxFormType(#{tax_form_type}) received from GetSubmittedTaxFormsForPayee API",
            { person_id: person.id, TaxFormType: tax_form_type }
          )
          call_described_method
        end
      end
    end
  end

  describe "#save_tax_form" do
    context "when Payoneer returns one tax form" do
      context "when saving a new tax form" do
        include_examples "single tax form", true
      end

      context "when updating an existing tax form (in the DB)" do
        include_examples "single tax form", false
      end
    end

    context "when Payoneer returns multiple tax forms" do
      before do
        allow(Payoneer::V2::TaxApiClient).to receive(:new).and_return(api_client_double)
      end

      context "with one active W8 form" do
        let(:api_client_double) do
          double(
            new: true,
            post: true,
            formatted_response: multiple_forms_with_one_active_w8,
            payout_provider_config: double(id: 1, program_id: "sample-program-id")
          )
        end

        it "picks the latest active form to save in the db" do
          service = Payoneer::TaxApiService.new(PartnerPayeeID: person.client_payee_id)
          expect { service.save_tax_form }.to change(TaxForm, :count).by(1)
          expect(person.tax_forms.last).to have_attributes(tax_form_type_id: 1)
        end

        it "updates the existing record when a matching record is found in the db" do
          expect(person.tax_forms.count).to eq(0)
          TaxForm.create(
            submitted_at: 11.months.ago.to_date,
            expires_at: 1.year.from_now.to_date,
            tax_form_type_id: 1,
            payout_provider_config_id: 1,
            person_id: person.id
          )
          expect(person.tax_forms.count).to eq(1)
          Payoneer::TaxApiService.new(PartnerPayeeID: person.client_payee_id).save_tax_form
          expect(person.tax_forms.count).to eq(1)
        end
      end

      context "with all expired w8s" do
        let(:api_client_double) do
          double(
            new: true,
            post: true,
            formatted_response: all_expired_w8s,
            payout_provider_config: double(id: 1, program_id: "sample-program-id")
          )
        end

        it "discards all" do
          service = Payoneer::TaxApiService.new(PartnerPayeeID: person.client_payee_id)
          expect { service.save_tax_form }.not_to change(TaxForm, :count)
        end
      end

      context "with all expired w9s" do
        let(:api_client_double) do
          double(
            new: true,
            post: true,
            formatted_response: all_expired_w9s,
            payout_provider_config: double(id: 1, program_id: "sample-program-id")
          )
        end

        it "picks the latest expired form to save in the db" do
          service = Payoneer::TaxApiService.new(PartnerPayeeID: person.client_payee_id)
          expect { service.save_tax_form }.to change(TaxForm, :count).by(1)
          expect(person.tax_forms.last).to have_attributes(tax_form_type_id: 3)
        end
      end

      context "with one active w8" do
        let(:api_client_double) do
          double(
            new: true,
            post: true,
            formatted_response: multiple_forms_with_one_active_w8,
            payout_provider_config: double(id: 1, program_id: "sample-program-id")
          )
        end

        it "picks the w8" do
          service = Payoneer::TaxApiService.new(PartnerPayeeID: person.client_payee_id)
          expect { service.save_tax_form }.to change(TaxForm, :count).by(1)
          expect(person.tax_forms.last).to have_attributes(tax_form_type_id: 1)
        end
      end

      context "with one active w9" do
        let(:api_client_double) do
          double(
            new: true,
            post: true,
            formatted_response: multiple_forms_with_one_active_w9,
            payout_provider_config: double(id: 1, program_id: "sample-program-id")
          )
        end

        it "picks the w9" do
          service = Payoneer::TaxApiService.new(PartnerPayeeID: person.client_payee_id)
          expect { service.save_tax_form }.to change(TaxForm, :count).by(1)
          expect(person.tax_forms.last).to have_attributes(tax_form_type_id: 3)
        end
      end
    end
  end
end
