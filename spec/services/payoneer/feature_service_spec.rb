require 'rails_helper'

describe Payoneer::FeatureService do

  let(:person) { create(:person, :with_payout_provider, country_website_id: 1, country: "US") }
  let(:canadian_person) { create(:person, country_website_id: 2, country: "CA") }
  let(:australian_person) { create(:person, country_website_id: 5, country: "AU") }
  let(:france_person) { create(:person, country_website_id: 6, country: "FR") }

  describe "#payoneer_payout_enabled?" do
    context "for australian customers" do
      it "should use the feature flag 'payoneer_payout_australia_canada'" do
        allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_payout, australian_person).and_return(true)
        expect(Payoneer::FeatureService.payoneer_payout_enabled?(australian_person)).to eq(true)

        allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_payout, australian_person).and_return(false)
        expect(Payoneer::FeatureService.payoneer_payout_enabled?(australian_person)).to eq(false)
      end
    end

    context "for canadian customers" do
      it "should use the feature flag 'payoneer_payout_australia_canada'" do
        feature_service = Payoneer::FeatureService.new(canadian_person)
        allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_payout, canadian_person).and_return(true)
        expect(Payoneer::FeatureService.payoneer_payout_enabled?(canadian_person)).to eq(true)

        allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_payout, canadian_person).and_return(false)
        expect(Payoneer::FeatureService.payoneer_payout_enabled?(canadian_person)).to eq(false)
      end
    end

    context "for other country users" do
      it "should use the feature flag 'payoneer_payout_others_countries'" do
        feature_service = Payoneer::FeatureService.new(france_person)
        allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_payout, france_person).and_return(true)
        expect(Payoneer::FeatureService.payoneer_payout_enabled?(france_person)).to eq(true)

        allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_payout, france_person).and_return(false)
        expect(Payoneer::FeatureService.payoneer_payout_enabled?(france_person)).to eq(false)
      end
    end
  end

  describe "#enabled_and_active?" do
    context "user is opted out of payoneer" do
      it "should not return true" do
        feature_service = Payoneer::FeatureService.new(person)
        allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_opt_out, person).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_payout, person).and_return(true)

        expect(feature_service.enabled_and_active?).to eq(false)
      end
    end

    context "user is enabled for payoneer" do
      it "returns true" do
        feature_service = Payoneer::FeatureService.new(person)
        allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_opt_out, person).and_return(false)
        allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_payout, person).and_return(true)

        expect(feature_service.enabled_and_active?).to eq(true)
      end
    end
  end

  describe "#payoneer_opted_out?" do
    it "should return the payoneer_opt_out feature flag" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_opt_out, person).and_return(true)
      expect(Payoneer::FeatureService.payoneer_opted_out?(person)).to eq(true)
    end
  end

  describe "#payoneer_currency_dropdown_enabled?" do
    context "for India site user" do
      it "should return false" do
        india_site_user = create(:person, country: "IN")
        allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_payout_usa_withdrawal_currency_dropdown, india_site_user).and_return(true)
        expect(Payoneer::FeatureService.payoneer_currency_dropdown_enabled?(india_site_user)).to eq(false)
      end
    end

    context "for us user" do
      it "should return true" do
        allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_payout_usa_withdrawal_currency_dropdown, person).and_return(true)
        expect(Payoneer::FeatureService.payoneer_currency_dropdown_enabled?(person)).to eq(true)
      end
    end
  end


  describe "payoneer_exclude_tax_check?" do
    it "should use the feature flag 'payoneer_exclude_tax_check'" do
      subject = Payoneer::FeatureService.new(person)

      allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_exclude_tax_check, person).and_return(true)
      expect(subject.payoneer_exclude_tax_check?).to eq(true)

      allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_exclude_tax_check, person).and_return(false)
      expect(subject.payoneer_exclude_tax_check?).to eq(false)
    end
  end
end
