require "rails_helper"

describe Payoneer::TaxFormUpdaterService do
  describe '#call' do
    before do
      travel_to Time.now.beginning_of_year

      # Disable callbacks run when Sidekiq workers are run
      allow_any_instance_of(Believe::ApiClient).to receive(:post).and_return(true)
      allow(Person::AfterCommitCallbacks).to receive(:send_to_zendesk).and_return(true)
      allow_any_instance_of(Delivery::DistributionWorker).to receive(:jid).and_return("1234567")

      allow(Payoneer::V2::TaxApiClient).to receive(:new).and_return(api_client_double)


      # Program IDs are required for finding tax form records to update
      person.tax_forms.update_all(
        payout_provider_config_id: person.payout_provider_config.id,
        submitted_at: submitted_at,
        expires_at: expires_at
      )
    end

    after do
      travel_back
    end

    let(:date_format) { "%m/%d/%Y" }
    let(:program_id) { person.payout_provider_config.program_id }
    let(:tax_form_type) { "W9 - Individual" }
    let(:tax_form_type_id) { TaxFormType.where(kind: "W9 - Individual").pluck(:id).first }
    let(:submitted_at) { 1.year.ago }
    let(:expires_at) { 1.year.from_now }
    let(:response) do
      {
        "GetSubmittedTaxFormsForPayee" => {
          "Code" => "000",
          "Description" => "OK (Request accepted)",
          "NumberOfTaxForms" => "10",
          "TaxForms" => {
            "Form" => tax_forms_response_data
          }
        }
      }
    end
    let(:api_client_double) do
      double(
        new: true,
        post: true,
        formatted_response: response,
        payout_provider_config: double(id: 1, program_id: program_id)
      )
    end

    context "when updating existing tax form records", run_sidekiq_worker: true do
      let(:tax_forms_response_data) do
        {
          "PartnerPayeeId"  => person.client_payee_id,
          "TaxFormType"     => "W9 - Individual",
          "TaxFormStatus"   => "Submitted",
          "DateOfSignature" => submitted_at.strftime(date_format),
          "ValidUntil"      => expires_at.strftime(date_format),
          "Name"            => person.name
        }
      end

      context "when exactly one tax form is returned" do
        subject!(:person) { create(:person, :with_payout_provider, :with_tax_form) }

        it "updates expected attributes on the tax forms" do
          described_class.new(program_id: program_id).call

          expect(person.reload.tax_forms.first).to have_attributes(
            tax_form_type_id: tax_form_type_id,
            submitted_at: submitted_at.localtime,
            expires_at: expires_at.localtime,
            provider: PayoutProvider::PAYONEER,
            payout_provider_config_id: person.payout_provider_config.id,
            tax_entity_name: person.name
          )
        end
      end
    end
  end
end
