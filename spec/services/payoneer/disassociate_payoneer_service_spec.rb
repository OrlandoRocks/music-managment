#frozen_string_literal: true

require "rails_helper"

describe Payoneer::DisassociatePayoneerService do
  let!(:person_with_provider) { create(:person, :with_payout_provider) }
  let!(:person_without_provider) { create(:person) }

  describe "#initialize" do
    context "for a user with a payout provider" do
      let!(:disassociate_service) do
        Payoneer::DisassociatePayoneerService.new(person_with_provider)
      end

      it "sets the person" do
        expect(disassociate_service.person).to eq(person_with_provider)
      end

      it "sets the payout provider" do
        expect(disassociate_service.payout_providers.length).to be(1)
      end

      it "disassociates the payout provider" do
        payout_provider = person_with_provider.payout_providers.first

        expect(payout_provider.provider_status).to eq(PayoutProvider::DECLINED)
        expect(payout_provider.active_provider).to be false
      end
    end

    context "for a user with a payout provider" do
      let!(:disassociate_service) do
        Payoneer::DisassociatePayoneerService.new(person_without_provider)
      end

      it "sets the person" do
        expect(disassociate_service.person).to eq(person_without_provider)
      end

      it "sets the payout provider to an empty array" do
        expect(disassociate_service.payout_providers.length).to be(0)
      end
    end
  end
end
