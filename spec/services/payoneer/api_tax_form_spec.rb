require "rails_helper"

describe Payoneer::APITaxForm do
  describe "expired?" do
    it "returns true when TaxFormStatus is 'Expired'" do
      response = { "TaxFormStatus" => "Expired" }
      expect(described_class.new(response).expired?).to be(true)
    end

    it "returns true when expiry is crossed" do
      response = { "TaxFormStatus" => "Submitted", "ValidUntil" => 1.year.ago.strftime("%m/%d/%Y") }
      expect(described_class.new(response).expired?).to be(true)
    end

    it "returns false when non-expired forms" do
      response = { "TaxFormStatus" => "Submitted", "ValidUntil" => 1.year.from_now.strftime("%m/%d/%Y") }
      expect(described_class.new(response).expired?).to be(false)
    end
  end

  describe "valid?" do
    let(:valid_date) { Time.new(1.year.from_now.year, 1, 1).strftime("%m/%d/%Y") }
    let(:expired_date) { Time.new(2000, 1, 1).strftime("%m/%d/%Y") }
    
    it "returns true for a W9 tax-form" do
      response = { "TaxFormType" => "W9 - Individual" }
      expect(described_class.new(response).valid?).to be(true)
    end

    it "returns true for a non-expired tax-form" do
      response = { "ValidUntil" => valid_date }
      expect(described_class.new(response).valid?).to be(true)
    end

    it "returns false for an expired non-w9 tax-form" do
      response = { "ValidUntil" => expired_date }
      expect(described_class.new(response).valid?).to be(false)
    end
end

  describe "submission_date" do
    it "returns parsed date" do
      response = { "DateOfSignature" => "02/30/2022" }
      expect(described_class.new(response).submission_date).to eq(Date.strptime(response["DateOfSignature"], "%m/%d/%Y"))
    end
  end
end
