require "rails_helper"

describe Payoneer::FeeCalculatorService do
  let(:person) { create(:person) }
  let(:british_person) { create(:person, country: "GB") }
  let(:german_person) { create(:person, country: "DE") }

  describe "#calculate" do
    context "for a user using payout methods that use the payoneer account fee structure to withdraw" do
      it "returns 1 for a US user with a payout method of ACCOUNT" do
        country = person.country_website.country
        fee = Payoneer::FeeCalculatorService.calculate(
          domain: country,
          payoneer_account_domain: country,
          person: person,
          payee_details: OpenStruct.new({withdrawal_currency: "USD"}),
          withdrawal_amount: 500,
          withdrawal_type: PayoutTransfer::ACCOUNT
        )

        expect(fee).to eq(100)
      end


      it "returns 1 for a US user with a payout method of PREPAID_CARD" do
        country = person.country_website.country
        fee = Payoneer::FeeCalculatorService.calculate(
            domain: country,
            payoneer_account_domain: country,
            withdrawal_amount: 500,
            person: person,
            payee_details: OpenStruct.new({withdrawal_currency: "USD"}),
            withdrawal_type: PayoutTransfer::PREPAID_CARD
        )

        expect(fee).to eq(100)
      end
    end

    context "for a user using a check to withdraw" do
      it "returns a value of 5 USD for US customers" do
        country = person.country_website.country
        fee = Payoneer::FeeCalculatorService.calculate(
          domain: country,
          payoneer_account_domain: country,
          withdrawal_amount: 500,
          person: person,
          payee_details: OpenStruct.new({withdrawal_currency: "USD"}),
          withdrawal_type: PayoutTransfer::PAPER_CHECK
        )

        expect(fee).to eq(500)
      end

      it "is not a valid option for non US customers" do
        country = german_person.country_website.country
        fee = Payoneer::FeeCalculatorService.calculate(
          domain: country,
          payoneer_account_domain: country,
          withdrawal_amount: 500,
          person: german_person,
          payee_details: OpenStruct.new({withdrawal_currency: "USD"}),
          withdrawal_type: PayoutTransfer::PAPER_CHECK
        )

        expect(fee).to eq(nil)
      end
    end

    context "for a user using paypal to withdraw through payoneer" do
      context "for when tunecore and payoneer share a US domain" do
        it "returns a fee of 2% of the withdraw amount up to threshold" do
          country = person.country_website.country
          fee = Payoneer::FeeCalculatorService.calculate(
            domain: country,
            payoneer_account_domain: country,
            withdrawal_amount: 500,
            person: person,
            payee_details: OpenStruct.new({withdrawal_currency: "USD"}),
            withdrawal_type: PayoutTransfer::PAYPAL
          )

          expect(fee).to eq(10.0)
        end
      end

      context "for when tunecore and payoneer do not share the same domain" do
        context "for USA Tunecore users with a payoneer account in a different domain" do
          it "returns a fee of 2% of the withdraw amount if the calculated value is under the different domain threshold" do
            country = person.country_website.country
            fee = Payoneer::FeeCalculatorService.calculate(
              domain: country,
              payoneer_account_domain: "DE",
              withdrawal_amount: 75000,
              person: person,
              payee_details: OpenStruct.new({withdrawal_currency: "USD"}),
              withdrawal_type: PayoutTransfer::PAYPAL
            )

            expect(fee).to eq(1500)
          end

          it "returns the max fee ($20) if the requested withdraw amount is over the 2% threshold" do
            country = person.country_website.country
            fee = Payoneer::FeeCalculatorService.calculate(
              domain: country,
              payoneer_account_domain: "DE",
              withdrawal_amount: 100000,
              person: person,
              payee_details: OpenStruct.new({withdrawal_currency: "USD"}),
              withdrawal_type: PayoutTransfer::PAYPAL
            )

            expect(fee).to eq(2000)
          end
        end

        context "for UK Tunecore users with a different payoneer domain" do
          it "returns a fee of 2% of the withdraw amount if the calculated value is under the different domain threshold" do
            country = british_person.country_website.country
            fee = Payoneer::FeeCalculatorService.calculate(
              domain: country,
              payoneer_account_domain: "US",
              withdrawal_amount: 125000,
              person: british_person,
              payee_details: OpenStruct.new({withdrawal_currency: "GBP"}),
              withdrawal_type: PayoutTransfer::PAYPAL
            )

            expect(fee).to eq(2500)
          end

          it "returns the max fee ($30) if the requested withdraw amount is over the 2% threshold" do
            country = british_person.country_website.country
            fee = Payoneer::FeeCalculatorService.calculate(
              domain: country,
              payoneer_account_domain: "US",
              withdrawal_amount: 300000,
              person: british_person,
              payee_details: OpenStruct.new({withdrawal_currency: "GBP"}),
              withdrawal_type: PayoutTransfer::PAYPAL
            )

            expect(fee).to eq(3000)
          end
        end

        context "for EU Tunecore users with a different payoneer domain" do
          it "returns a fee of 2% of the withdraw amount if the calculated value is under the different domain threshold" do
            country = german_person.country_website.country
            fee = Payoneer::FeeCalculatorService.calculate(
              domain: country,
              payoneer_account_domain: "UK",
              withdrawal_amount: 160000,
              person: german_person,
              payee_details: OpenStruct.new({withdrawal_currency: "GBP"}),
              withdrawal_type: PayoutTransfer::PAYPAL
            )

            expect(fee).to eq(3200)
          end

          it "returns the max fee ($35) if the requested withdraw amount is over the 2% threshold" do
            country = german_person.country_website.country
            fee = Payoneer::FeeCalculatorService.calculate(
              domain: country,
              payoneer_account_domain: "US",
              withdrawal_amount: 300000,
              person: german_person,
              payee_details: OpenStruct.new({withdrawal_currency: "USD"}),
              withdrawal_type: PayoutTransfer::PAYPAL
            )

            expect(fee).to eq(3500)
          end
        end
      end
    end

    context "for a user who has selected ACH (BANK) payout method" do
      it "should return the payoneer ach fees" do
        person = create(:person)
        payee_details = OpenStruct.new({widthrawal_currency: "USD"})
        allow(PayoneerAchFee).to receive(:get_fee_cents).with('US', person, payee_details).and_return(400)
        fees_cents = Payoneer::FeeCalculatorService.calculate(
            domain: 'US',
            payoneer_account_domain: 'US',
            withdrawal_type: PayoutTransfer::BANK,
            person: person,
            payee_details: payee_details,
            withdrawal_amount: 1000
        )
        expect(fees_cents).to eq(400)
      end
    end

  end

  describe "#validate args" do
    context "args contains invalid domain" do
      it "should notify airbrake" do
        invalid_domain = "BE"
        expect(Airbrake).to receive(:notify).with("Domain (#{invalid_domain}) not supported.", { domain: invalid_domain, payoneer_account_domain: person.country_website.country })

        Payoneer::FeeCalculatorService.calculate(
            domain: invalid_domain,
            payoneer_account_domain: person.country_website.country,
            withdrawal_amount: 500,
            person: person,
            payee_details: OpenStruct.new({withdrawal_currency: "USD"}),
            withdrawal_type: PayoutTransfer::ACCOUNT
        )
      end
    end

    context "withdrawal type is invalid" do
      it "should notify airbrake" do
        invalid_withdrawal_type = "an_invalid_withdrawal_type"
        expect(Airbrake).to receive(:notify).with("Withdrawal method (#{invalid_withdrawal_type}) not supported.", { withdrawal_type: invalid_withdrawal_type })

        country = person.country_website.country
        Payoneer::FeeCalculatorService.calculate(
          domain: country,
          payoneer_account_domain: country,
          withdrawal_amount: 500,
          person: person,
          payee_details: OpenStruct.new({withdrawal_currency: "USD"}),
          withdrawal_type: invalid_withdrawal_type
        )
      end
    end
  end
end
