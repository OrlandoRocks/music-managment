#frozen_string_literal: true

require "rails_helper"

describe Payoneer::PersonKycBackfillService do
  let!(:tc_person) { create(:person, :with_payout_provider) }
  let!(:bi_person) { create(:person, :with_believe_payout_provider) }
  let!(:payout_provider_configs) {
    { 1 => PayoutProviderConfig.by_env.find_by(program_id: PayoutProviderConfig::TC_US_PROGRAM_ID),
    2 => PayoutProviderConfig.by_env.find_by(program_id: PayoutProviderConfig::BI_US_PROGRAM_ID) }
  }
  let!(:payee_extended_details){ double(:payee_extended_details, withdrawal_type: "ACCOUNT", country: "US") }
  let!(:mock_api_service) { double("payout_api_service") }

  let!(:success_response_object) { { description: "Success" } }
  let!(:success_response) { double("response", body: JSON.dump(success_response_object)) }
  let!(:mock_success_response) { Payoneer::Responses::PayeeExtendedDetail.new(success_response) }

  let!(:fail_response_object) { { description: "Not success" } }
  let!(:fail_response) { double("response", body: JSON.dump(fail_response_object)) }
  let!(:mock_fail_response) { Payoneer::Responses::PayeeExtendedDetail.new(fail_response) }

  describe "#initialize" do
    context "for a user with Payoneer provider configs" do
      let!(:backfill_service) do
        allow(mock_api_service).to receive(:payee_extended_details).with(person: bi_person).and_return(mock_success_response)
        allow(PayoutProvider::ApiService).to receive(:new).and_return(mock_api_service)
        Payoneer::PersonKycBackfillService.new(bi_person)
      end

      it "sets the config" do
        expect(backfill_service.config).to eq(payout_provider_configs[2])
      end

      it "calls api_response" do
        expect(backfill_service.api_response).to eq(mock_success_response)
      end
    end

    context "for a user without Payoneer provider configs" do
      before(:each) do
        allow(mock_api_service).to receive(:payee_extended_details).with(person: bi_person).and_return(mock_fail_response)
        allow(PayoutProvider::ApiService).to receive(:new).and_return(mock_api_service)
      end

      it "the config should be unchanged" do
        expect{ Payoneer::PersonKycBackfillService.new(bi_person).call! }.to raise_error
        expect(bi_person.payout_provider.payout_provider_config).to eq(payout_provider_configs[2])
      end

      it "raises an airbrake error" do
        expect(Airbrake).to receive(:notify)
        expect{ Payoneer::PersonKycBackfillService.new(bi_person).call! }.to raise_error
      end
    end
  end

  describe "#call" do
    context "for a user with Payoneer provider configs" do
      before(:each) do
        allow(mock_api_service).to receive(:payee_extended_details).with(person: tc_person).and_return(mock_success_response)
        allow(PayoutProvider::ApiService).to receive(:new).and_return(mock_api_service)
      end

      it "sets the person's config" do
        Payoneer::PersonKycBackfillService.new(tc_person).call!
        expect(tc_person.payout_provider.payout_provider_config).to eq(payout_provider_configs[2])
      end

      it "calls PayoutProvider::VerifyPayeeComplianceInfo" do
        expect_any_instance_of(PayoutProvider::VerifyPayeeComplianceInfo).to receive(:call)
        Payoneer::PersonKycBackfillService.new(tc_person).call!
      end
    end

    context "for a user without Payoneer provider configs" do
      before(:each) do
        allow(mock_api_service).to receive(:payee_extended_details).with(person: bi_person).and_return(mock_fail_response)
        allow(PayoutProvider::ApiService).to receive(:new).and_return(mock_api_service)
      end

      it "the config should be unchanged" do
        expect{ Payoneer::PersonKycBackfillService.new(bi_person).call! }.to raise_error
        expect(bi_person.payout_provider.payout_provider_config).to eq(payout_provider_configs[2])
      end

      it "raises an airbrake error" do
        expect(Airbrake).to receive(:notify)
        expect{ Payoneer::PersonKycBackfillService.new(bi_person).call! }.to raise_error
      end
    end
  end
end
