require "rails_helper"

describe TaxFormCheckService do
  let(:threshold) { TaxableEarning::TAXABLE_THRESHOLD }

  before do
    response_double = { "GetSubmittedTaxFormsForPayee" => { "TaxForms" => nil } }
    api_client_double = double(post: true, formatted_response: response_double)
    allow(Payoneer::V2::TaxApiClient).to receive(:new).and_return(api_client_double)
  end

  describe ".needs_tax_form?" do
    context "User countries" do
      it "when a person is not from the US or its territories, it immediately returns false" do
        allow_any_instance_of(Payoneer::TaxApiService).to receive(:has_active_forms?).and_return(true)
        person = create(:person, :with_taxable_earnings, country: "FR")

        expect_any_instance_of(Payoneer::TaxApiService).not_to receive(:has_active_forms?)
        expect(TaxFormCheckService.needs_tax_form?(person.id)).to eq false
      end

      it "when a person is the US or its territories, it checks earnings and tax forms" do
        allow_any_instance_of(Payoneer::TaxApiService).to receive(:has_active_forms?).and_return(true)
        allow_any_instance_of(Payoneer::TaxApiService).to receive(:save_tax_form).and_return(true)
        person = create(:person, :with_taxable_earnings, :with_payout_provider, country: "PR")

        expect_any_instance_of(Payoneer::TaxApiService).to receive(:has_active_forms?)
        TaxFormCheckService.needs_tax_form?(person.id)
      end
    end

    context "When a person's earnings do not meet the threshold" do
      it "returns false even if they don't have a tax form" do
        person = create(:person)
        expect(TaxFormCheckService.needs_tax_form?(person.id)).to eq false
      end
    end

    context "when a person's earnings do meet the threshold" do
      it "returns false if they have a tax form logged in the db" do
        person = create(:person, :with_taxable_earnings, :with_tax_form)
        expect(TaxFormCheckService.needs_tax_form?(person.id)).to eq false
      end

      it "returns true if they have a tax form in the db but it has expired" do
        allow_any_instance_of(Payoneer::TaxApiService).to receive(:has_active_forms?).and_return(false)
        allow_any_instance_of(Payoneer::TaxApiService).to receive(:save_tax_form).and_return(true)
        person = create(:person, :with_taxable_earnings, :with_tax_form)
        person.tax_forms.first.update(expires_at: Date.yesterday)

        expect(TaxFormCheckService.needs_tax_form?(person.id)).to eq true
      end

      it "fires off a payoneer api request if there is no tax record in the db" do
        allow_any_instance_of(Payoneer::TaxApiService).to receive(:has_active_forms?).and_return(true)
        allow_any_instance_of(Payoneer::TaxApiService).to receive(:save_tax_form).and_return(true)
        person = create(:person, :with_taxable_earnings, :with_payout_provider)

        expect_any_instance_of(Payoneer::TaxApiService).to receive(:has_active_forms?)

        TaxFormCheckService.needs_tax_form?(person.id)
      end

      context "when secondary_taxform_submission feature flag is set" do
        let(:person) { create(:person, :with_taxable_earnings, :with_payout_provider) }
        let(:primary_service_instance) { OpenStruct.new(has_active_forms?: true, save_tax_form: true) }
        let(:secondary_service_instance) { OpenStruct.new(has_active_forms?: true, save_tax_form: true) }
        let(:secondary_taxform_program_config) { create(:payout_provider_config, :for_secondary_taxform_program) }

        before do
          allow(Payoneer::FeatureService).to receive(:secondary_taxform_submission?).with(person.id).and_return(true)
        end

        it "fetches taxforms from both programs" do
          api_client_double = double(post: true, formatted_response: {})
          allow(Payoneer::V2::TaxApiClient).to receive(:new).and_return(api_client_double)
          expect(Payoneer::TaxApiService).to receive(:new).with(
            PartnerPayeeID: person.client_payee_id
          ).and_return(primary_service_instance)
          expect(Payoneer::TaxApiService).to receive(:new).with(
            PartnerPayeeID: person.client_payee_id, program_id: secondary_taxform_program_config.program_id
          ).and_return(secondary_service_instance)

          expect(primary_service_instance).to receive(:has_active_forms?).twice
          expect(secondary_service_instance).to receive(:has_active_forms?).twice

          TaxFormCheckService.needs_tax_form?(person.id)
        end
      end

      it "returns true if Payoneer::TaxApiService reports no active forms" do
        allow_any_instance_of(Payoneer::TaxApiService).to receive(:has_active_forms?).and_return(false)
        person = create(:person, :with_taxable_earnings)
        expect(TaxFormCheckService.needs_tax_form?(person.id)).to eq true
      end

      it "returns true if it has any years above the threshold in the past 3 tax years" do
        person = create(:person, :with_payout_provider)
        create_list(:taxable_earning, 2, :old, person: person)
        expect(TaxFormCheckService.needs_tax_form?(person.id)).to eq true
      end
    end
  end

  describe ".check_api?" do
    context "user has no payout provider" do
      it "should return false" do
        person = create(:person, :with_taxable_earnings)
        expect(TaxFormCheckService.check_api?(person.id)).to be_falsey
      end
    end

    context "user has payout provider" do
      context "API does return tax form data" do
        it "should return true" do
          person = create(:person, :with_taxable_earnings, :with_payout_provider)
          allow_any_instance_of(Payoneer::TaxApiService).to receive(:has_active_forms?).and_return(true)
          allow_any_instance_of(Payoneer::TaxApiService).to receive(:save_tax_form).and_return(true)

          expect(TaxFormCheckService.check_api?(person.id)).to be_truthy
        end
      end

      context "API does not return tax form data" do
        it "should return false" do
          person = create(:person, :with_taxable_earnings, :with_payout_provider)
          allow_any_instance_of(Payoneer::TaxApiService).to receive(:has_active_forms?).and_return(false)
          allow_any_instance_of(Payoneer::TaxApiService).to receive(:save_tax_form).and_return(true)

          expect(TaxFormCheckService.check_api?(person.id)).to be_falsey
        end
      end
    end
  end

  describe ".person_tax_query" do
    it "returns a taxable person object with the active provider attributes when there are multiple payout providers" do
      allow_any_instance_of(Payoneer::TaxApiService).to receive(:has_active_forms?).and_return(true)
      person = create(:person, :with_taxable_earnings, :with_payout_provider)
      person.payout_provider.disassociate_account

      active_provider = create(:payout_provider, person: person, active_provider: true)

      tax_form_service = TaxFormCheckService.new(person.id)
      expect(tax_form_service.taxable_person.payee_id).to eq(active_provider.client_payee_id)
    end

    it "returns nil for the payee ID when the user does not have an active payout provider" do
      allow_any_instance_of(Payoneer::TaxApiService).to receive(:has_active_forms?).and_return(true)
      person = create(:person, :with_taxable_earnings, :with_payout_provider)
      person.payout_provider.disassociate_account

      tax_form_service = TaxFormCheckService.new(person.id)
      expect(tax_form_service.taxable_person.payee_id).to be_nil
    end
  end
end
