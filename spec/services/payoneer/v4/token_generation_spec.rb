# frozen_string_literal: true
require "rails_helper"

describe Payoneer::V4::TokenGenerationService do

  let(:new_token_response) do
    {
      "token_type" => "Bearer",
      "access_token" => "FOOBAR",
      "expires_in" => 2592000,
      "consented_on" => 1575396682,
      "scope" => "read",
      "refresh_token" => nil,
      "refresh_token_expires_in" => 0,
      "id_token" => nil,
      "error" => nil,
      "error_description" => nil
  }.with_indifferent_access
  end

  let(:revoked_token_response) do
    {
      "status" => "success",
      "error" => nil,
      "error_description" => nil   
    }.with_indifferent_access
  end

  let(:cache_key) do
    Payoneer::V4::TokenGenerationService::CACHE_KEY
  end

  describe "#fetch_token" do
    it "returns a token and puts it into the rails cache" do
      with_clean_caching do
        allow(Payoneer::V4::ApiTokenGenerationClient).to receive(:request_token).and_yield(new_token_response)
        token = Payoneer::V4::TokenGenerationService.fetch_token

        expect(value_in_cache?(token)).to be(true)
      end
    end

    it "assigns it to the correct cache key" do
      with_clean_caching do
        allow(Payoneer::V4::ApiTokenGenerationClient).to receive(:request_token).and_yield(new_token_response)
        token = Payoneer::V4::TokenGenerationService.fetch_token

        expect(value_at_key(cache_key)).to eq(token)
      end
    end
  end


  describe "#revoke_token" do
    it "busts the cache" do
      with_clean_caching do
        allow(Payoneer::V4::ApiTokenGenerationClient).to receive(:request_token).and_yield(new_token_response)
        allow(Payoneer::V4::ApiTokenGenerationClient).to receive(:revoke_token).and_yield(revoked_token_response)
        
        token = Payoneer::V4::TokenGenerationService.fetch_token
        Payoneer::V4::TokenGenerationService.revoke_token

        expect(value_in_cache?(token)).to be(false)
      end
    end

    it "returns :no_token_cached if cache not set empty?" do
      with_clean_caching do

        msg = Payoneer::V4::TokenGenerationService.revoke_token

        expect(msg).to be(:no_token_cached)
      end
    end
  end
end




