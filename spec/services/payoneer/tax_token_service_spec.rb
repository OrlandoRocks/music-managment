require "rails_helper"

describe Payoneer::TaxTokenService do
  let(:person) {FactoryBot.create(:person)}
  describe ".update" do
    context "success from API" do
      before do
        allow_any_instance_of(Payoneer::V2::ApiClient).to receive(:post).with({PartnerPayeeID: person.id, mname: "InitializeTaxFormForPayee"})
        allow_any_instance_of(Payoneer::V2::ApiClient).to receive(:post).with({PartnerPayeeID: person.id, mname: "GetPendingTaxFormInvitedPayees", fromDate: Date.yesterday.to_s, toDate: Date.today.to_s}).and_return("12345")
      end
      it "returns the new token" do
        expect(Payoneer::TaxTokenService.update(person.id)).to eq("12345")
      end
    end

    context "failure from API" do
      before do
        allow_any_instance_of(Payoneer::V2::ApiClient).to receive(:post).and_raise(Payoneer::V2::ApiClient::ApiError)
      end
      it "raises an error" do
        expect(Tunecore::Airbrake).to receive(:notify)
        Payoneer::TaxTokenService.update(person.id)
      end
    end
  end
end
