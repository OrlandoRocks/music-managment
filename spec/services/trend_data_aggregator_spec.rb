require "rails_helper"

describe TrendDataAggregator do
  let(:person)     { FactoryBot.create(:person) }
  let(:start_date) { 30.days.ago.to_date.to_s }
  let(:end_date)   { Date.yesterday.to_date.to_s }
  let(:providers)  { Provider.all.sort_by {|item| item.name} }

  describe ".execute" do
    before(:each) do
      allow_any_instance_of(TrendDataAggregator).to receive(:aggregate_and_store_top_trends)
    end

    it "calls on the #aggregate_and_store_top_trends method" do
      expect_any_instance_of(TrendDataAggregator).to receive(:aggregate_and_store_top_trends)
      TrendDataAggregator.execute(person.id, start_date, end_date)
    end

    it "calls on the #aggregate_and_store_trends_by_day method" do
      expect_any_instance_of(TrendDataAggregator).to receive(:aggregate_and_store_trends_by_day)
      TrendDataAggregator.execute(person.id, start_date, end_date)
    end
  end

  describe "#new" do
    it "sets the user, providers, start and end dates" do
      aggregator = TrendDataAggregator.new(person.id, "2017-01-01", "2017-02-01")
      expect(aggregator.instance_variable_get(:@user)).to eq(person)
      expect(aggregator.instance_variable_get(:@providers)).to eq(providers)
      expect(aggregator.instance_variable_get(:@start_date)).to eq("2017-01-01")
      expect(aggregator.instance_variable_get(:@end_date)).to eq("2017-02-01")
    end

    it "defaults start and end date to a 90 day date range" do
      aggregator = TrendDataAggregator.new(person.id, start_date, end_date)
      expect(aggregator.instance_variable_get(:@start_date).to_date).to eq(start_date.to_date)
      expect(aggregator.instance_variable_get(:@end_date).to_date).to eq(end_date.to_date)
    end
  end

  describe "#aggregate_and_store_top_trends" do
    context "trend data summary queries" do
      after(:each) do
        aggregator ||= TrendDataAggregator.new(person.id, start_date, end_date)
        aggregator.aggregate_and_store_top_trends
        keys = $trends_redis.redis.keys "trends_test*"
        $trends_redis.redis.del(*keys) unless keys.empty?
      end

      it "calls on TrendDataSummary.top_streamed_releases" do
        expect(TrendDataSummary).to receive(:top_streamed_releases).with(person, providers, start_date, end_date)
      end
      it "calls on TrendDataSummary.top_downloaded_releases" do
        expect(TrendDataSummary).to receive(:top_downloaded_releases).with(person, providers, start_date, end_date)
      end
      it "calls on TrendDataSummary.top_streamed_songs" do
        expect(TrendDataSummary).to receive(:top_streamed_songs).with(person, providers, start_date, end_date)
      end
      it "calls on TrendDataSummary.top_downloaded_songs" do
        expect(TrendDataSummary).to receive(:top_downloaded_songs).with(person, providers, start_date, end_date)
      end
      it "calls on TrendDataSummary.top_downloaded_countries" do
        expect(TrendDataSummary).to receive(:top_downloaded_countries).with(person, providers, start_date, end_date)
      end
      it "calls on TrendDataSummary.top_streamed_countries" do
        expect(TrendDataSummary).to receive(:top_streamed_countries).with(person, providers, start_date, end_date)
      end

      it "calls on TrendDataSummary.top_us_markets" do
        expect(TrendDataSummary).to receive(:top_us_markets).with(person, start_date, end_date)
      end
    end

  describe "#aggregate_and_store_trends_by_day" do
    context "trend data summary queries" do
      after(:each) do
        aggregator = TrendDataAggregator.new(person.id, start_date, end_date)
        aggregator.aggregate_and_store_trends_by_day
        keys = $trends_redis.redis.keys "trends_test*"
        $trends_redis.redis.del(*keys) unless keys.empty?
      end

      it "calls on TrendDataSummary.trend_data_counts_by_day" do
        amazon, itunes, spotify = providers.sort_by {|item| item.name}
        expect(TrendDataSummary).to receive(:trend_data_counts_by_day).with(person, [itunes, amazon], start_date, end_date) {[]}
        expect(TrendDataSummary).to receive(:trend_data_counts_by_day).with(person, [itunes], start_date, end_date) {[]}
        expect(TrendDataSummary).to receive(:trend_data_counts_by_day).with(person, [amazon], start_date, end_date) {[]}
        expect(TrendDataSummary).to receive(:trend_data_counts_by_day).with(person, [spotify], start_date, end_date) {[]}
      end
    end
  end

    context "redis interaction" do
      before(:each) do
        aggregator ||= TrendDataAggregator.new(person.id, start_date, end_date)
        aggregator.aggregate_and_store_top_trends
        aggregator.aggregate_and_store_trends_by_day
      end

      after(:each) do
        keys = $trends_redis.redis.keys "trends_test*"
        $trends_redis.redis.del(*keys) unless keys.empty?
      end
      it "stores streamed songs by day from spotify" do
        expect($trends_redis.redis.keys).to include("trends_test:person_#{person.id}:streamed_songs_by_day_spotify:#{start_date}:#{end_date}")
      end
      it "stores downloaded songs by day from itunes and amazon" do
        expect($trends_redis.redis.keys).to include("trends_test:person_#{person.id}:downloaded_songs_by_day_itunes_amazon:#{start_date}:#{end_date}")
      end
      it "stores downloaded songs by day from itunes" do
        expect($trends_redis.redis.keys).to include("trends_test:person_#{person.id}:downloaded_songs_by_day_itunes:#{start_date}:#{end_date}")
      end
      it "stores downloaded songs by day from amazon" do
        expect($trends_redis.redis.keys).to include("trends_test:person_#{person.id}:downloaded_songs_by_day_amazon:#{start_date}:#{end_date}")
      end
      it "stores downloaded releases by day from itunes and amazon" do
        expect($trends_redis.redis.keys).to include("trends_test:person_#{person.id}:downloaded_releases_by_day_itunes_amazon:#{start_date}:#{end_date}")
      end
      it "stores downloaded releases by day from itunes" do
        expect($trends_redis.redis.keys).to include("trends_test:person_#{person.id}:downloaded_releases_by_day_itunes:#{start_date}:#{end_date}")
      end

      it "stores downloaded releases by day from amazon" do
        expect($trends_redis.redis.keys).to include("trends_test:person_#{person.id}:downloaded_releases_by_day_amazon:#{start_date}:#{end_date}")
      end
      it "stores top streamed releases in the namespaced_trends_redis client" do
        expect($trends_redis.redis.keys).to include("trends_test:person_#{person.id}:top_streamed_releases:#{start_date}:#{end_date}")
      end
      it "stores top downloaded releases in the namespaced_trends_redis client" do
        expect($trends_redis.redis.keys).to include("trends_test:person_#{person.id}:top_downloaded_releases:#{start_date}:#{end_date}")
      end
      it "stores top streamed songs in the namespaced_trends_redis client" do
        expect($trends_redis.redis.keys).to include("trends_test:person_#{person.id}:top_streamed_songs:#{start_date}:#{end_date}")
      end
      it "stores top downloaded songs in the namespaced_trends_redis client" do
        expect($trends_redis.redis.keys).to include("trends_test:person_#{person.id}:top_downloaded_songs:#{start_date}:#{end_date}")
      end
      it "stores top streamed countried in the namespaced_trends_redis client" do
        expect($trends_redis.redis.keys).to include("trends_test:person_#{person.id}:top_streamed_countries:#{start_date}:#{end_date}")
      end
      it "stores top downloaded countries in the namespaced_trends_redis client" do
        expect($trends_redis.redis.keys).to include("trends_test:person_#{person.id}:top_downloaded_countries:#{start_date}:#{end_date}")
      end

      it "stores top us markets in the namespaced_trends_redis client" do
        expect($trends_redis.redis.keys).to include("trends_test:person_#{person.id}:top_us_markets:#{start_date}:#{end_date}")
      end
    end
  end
end
