require "rails_helper"

describe TransactionCorrectionService, type: :service do
  let!(:rollover_user) { create(:person) }
  let!(:non_rollover_user) { create(:person) }

  let!(:pre_rollover_intake_transaction) do
    create(:person_transaction, person_id: rollover_user.id, debit: 0.0, credit: 200.0, target_type: "PersonIntake" )
  end

  let!(:taxable_earnings_rollover_debit) do
    create(:person_transaction, person_id: rollover_user.id, debit: 200.0, credit: 0.0, target_type: "TaxableEarningsRollover")
  end

  let!(:transaction_belonging_to_another_person) do
    create(:person_transaction, person_id: non_rollover_user.id, debit: 0.0, credit: 5000.0, target_type: "TaxableEarningsRollover")
  end

  let!(:taxable_earnings_rollover_credit) do
    create(:person_transaction, person_id: rollover_user.id, debit: 0.0, credit: 200.0, target_type: "TaxableEarningsRollover")
  end

  let!(:post_rollover_intake_transaction) do
    create(:person_transaction, person_id: rollover_user.id, debit: 0.0, credit: 300.0, target_type: "PersonIntake" )
  end

  describe "#exec!" do
    context "invalid transactions" do
      it "returns :non_contiguous_transaction if non contiguous transactions are passed in" do
        corrections = [pre_rollover_intake_transaction, post_rollover_intake_transaction].map do |transaction|
          { id: transaction.id, credit: 0.0, debit: 0.0 }
        end
        expect(described_class.new(corrections).exec!).to eq(:non_contiguous_transaction)
      end

      it "returns :net_effect_non_zero if the net effect of corrections is non zero" do
        corrections = [taxable_earnings_rollover_credit, taxable_earnings_rollover_debit].map do |transaction|
          { id: transaction.id, credit: 0.0, debit: 100.0 }
        end

        expect(described_class.new(corrections).exec!).to eq(:net_effect_non_zero)
      end

      it "returns :breaks_transaction_chain if the correction does not match with the person's current balance" do
        corrections = [
          { id: taxable_earnings_rollover_credit.id, credit: 0.0, debit: 20.0 },
          { id: post_rollover_intake_transaction.id, credit: 20.0, debit: 0.0 }
        ]

        expect(described_class.new(corrections).exec!).to eq(:breaks_transaction_chain)
      end
    end

    context "valid transactions" do
      it "updates transactions with the correct debit" do
        corrections = [taxable_earnings_rollover_credit, taxable_earnings_rollover_debit].map do |transaction|
          { id: transaction.id, credit: 0.0, debit: 0.0 }
        end

        described_class.new(corrections).exec!
        expect(taxable_earnings_rollover_debit.reload.debit).to eq(0.0)
      end

      it "updates transactions with correct credit" do
        corrections = [taxable_earnings_rollover_credit, taxable_earnings_rollover_debit].map do |transaction|
          { id: transaction.id, credit: 0.0, debit: 0.0 }
        end

        described_class.new(corrections).exec!
        expect(taxable_earnings_rollover_credit.reload.credit).to eq(0.0)
      end

      it "updates transactions with correct previous_balance" do
        corrections = [
          { id: taxable_earnings_rollover_debit.id, credit: 0.0, debit: 20.0 },
          { id: taxable_earnings_rollover_credit.id, credit: 20.0, debit: 0.0 }
        ]

        described_class.new(corrections).exec!

        expect(taxable_earnings_rollover_debit.reload.previous_balance).to eq(200.0)
        expect(taxable_earnings_rollover_credit.reload.previous_balance).to eq(180.0)
      end

      it "updates transactions to match the previous balance of the next transaction" do
        corrections = [
          { id: taxable_earnings_rollover_debit.id, credit: 0.0, debit: 20.0 },
          { id: taxable_earnings_rollover_credit.id, credit: 20.0, debit: 0.0 }
        ]

        described_class.new(corrections).exec!
        expect(post_rollover_intake_transaction.reload.previous_balance).to eq(200.0)
      end

      it "updates transaction to match the persons current balance if latest transaction is updated" do
        rollover_user.person_balance.update(balance: rollover_user.person_balance.reload.balance - post_rollover_intake_transaction.credit)
        post_rollover_intake_transaction.destroy

        corrections = [
          { id: taxable_earnings_rollover_debit.id, credit: 0.0, debit: 20.0 },
          { id: taxable_earnings_rollover_credit.id, credit: 20.0, debit: 0.0 }
        ]

        expect(described_class.new(corrections).exec!).to eq(:valid_correction)
      end
    end
  end
end
