require "rails_helper"

describe DistributionApi::ValidationService do
  let(:album) { create(:album) }

  before do
    allow(ENV).to receive(:[])
    allow(ENV).to receive(:[]).with('BYTEDANCE_USER_ID').and_return(album.person_id.to_s)
  end

  describe "#source_id_valid?" do
    it 'does not add error message if source id is valid' do
      distribution_api_service = create(:distribution_api_service)
      json_parser = DistributionApi::JsonParser.new({
        source_id: distribution_api_service.uuid
      }.to_json)

      subject = described_class.new(json_parser)
      subject.source_id_valid?

      expect(subject.error_messages.length).to eq(0)
    end

    it 'adds error message if source id is invalid' do
      distribution_api_service = create(:distribution_api_service)
      json_parser = DistributionApi::JsonParser.new({
        source_id: "Not#{distribution_api_service.uuid}"
      }.to_json)

      subject = described_class.new(json_parser)
      subject.source_id_valid?

      expect(subject.error_messages).to eq(["Invalid Source Id"])
      expect(subject.error_messages.length).to eq(1)
    end
  end

  describe "#stores_valid?" do
    it 'does not add error message if stores are valid' do
      json_parser = DistributionApi::JsonParser.new({
        album_stores: [
          store_id: Store.is_active.is_used.pluck(:id).max
        ]
      }.to_json)

      allow(json_parser).to receive(:stores_failing_mp3_validation).and_return([])
      subject = described_class.new(json_parser)
      subject.stores_valid?

      expect(subject.error_messages.length).to eq(0)
    end

    it 'adds error messages if invalid store is passed' do
      json_parser = DistributionApi::JsonParser.new({
        album_stores: [
          store_id: Store.is_active.is_used.pluck(:id).max + 1
        ]
      }.to_json)

      allow(json_parser).to receive(:stores_failing_mp3_validation).and_return([])
      subject = described_class.new(json_parser)
      subject.stores_valid?

      expect(subject.error_messages).to eq(["Invalid store Found"])
      expect(subject.error_messages.length).to eq(1)
    end

    it 'adds error messages if invalid store is passed' do
      json_parser = DistributionApi::JsonParser.new({
        album_stores: [
          store_id: Store.is_active.is_used.pluck(:id).max + 1
        ]
      }.to_json)

      allow(json_parser).to receive(:stores_failing_mp3_validation).and_return(
        [
          Store.is_active.is_used.pluck(:id).max + 1
        ]
      )
      subject = described_class.new(json_parser)
      subject.stores_valid?

      expect(subject.error_messages).to eq(["No store found"])
      expect(subject.error_messages.length).to eq(1)
    end
  end

  describe "#upc_valid?" do
    it 'does not add error message if upc is valid' do
      create(:tunecore_upc, number: "12345")
      json_parser = DistributionApi::JsonParser.new({
        upcs: [
          upc: "34567",
          type: "optional"
        ]
      }.to_json)

      subject = described_class.new(json_parser)
      subject.upc_valid?

      expect(subject.error_messages.length).to eq(0)
    end

    it 'does not add error message if upc is null' do
      json_parser = DistributionApi::JsonParser.new({
        upcs: nil
      }.to_json)

      subject = described_class.new(json_parser)
      subject.upc_valid?

      expect(subject.error_messages.length).to eq(0)
    end

    it 'adds error messages if upc is same and belongs to some different album' do
      create(:tunecore_upc, number: "34567", upcable: album)

      json_parser = DistributionApi::JsonParser.new({
        source_album_id: 'source_album_id',
        source_id: 'source_id',
        upcs: [
          upc: "34567",
          type: "optional"
        ]
      }.to_json)

      subject = described_class.new(json_parser)
      subject.upc_valid?

      expect(subject.error_messages).to eq(["UPC 34567 already exists"])
      expect(subject.error_messages.length).to eq(1)
    end

    it 'adds error messages if upc is same and belongs to some different distribution api album' do
      upc = create(:tunecore_upc, number: "34567", upcable: album)
      distribution_api_service = create(:distribution_api_service, uuid: 'source_id')
      create(:distribution_api_album, source_album_id: 'source_album_id1', album: album, distribution_api_service: distribution_api_service)

      json_parser = DistributionApi::JsonParser.new({
        source_album_id: 'source_album_id',
        source_id: 'source_id',
        upcs: [
          upc: "34567",
          type: "optional"
        ]
      }.to_json)

      subject = described_class.new(json_parser)
      subject.upc_valid?

      expect(subject.error_messages).to eq(["UPC 34567 already exists"])
      expect(subject.error_messages.length).to eq(1)
    end

    it 'adds error messages if upc is same and belongs to same distribution api album but different source id' do
      upc = create(:tunecore_upc, number: "34567", upcable: album)
      distribution_api_service = create(:distribution_api_service, uuid: 'source_id1')
      create(:distribution_api_album, source_album_id: 'source_album_id', album: album, distribution_api_service: distribution_api_service)

      json_parser = DistributionApi::JsonParser.new({
        source_album_id: 'source_album_id',
        source_id: 'source_id',
        upcs: [
          upc: "34567",
          type: "optional"
        ]
      }.to_json)

      subject = described_class.new(json_parser)
      subject.upc_valid?

      expect(subject.error_messages).to eq(["UPC 34567 already exists"])
      expect(subject.error_messages.length).to eq(1)
    end

    it 'does not add error message if upc belongs to the same distribution api album' do
      upc = create(:tunecore_upc, number: "34567", upcable: album)
      distribution_api_service = create(:distribution_api_service, uuid: 'source_id')
      create(:distribution_api_album, source_album_id: 'source_album_id', album: album, distribution_api_service: distribution_api_service)

      json_parser = DistributionApi::JsonParser.new({
        source_album_id: 'source_album_id',
        source_id: 'source_id',
        upcs: [
          upc: "34567",
          type: "optional"
        ]
      }.to_json)

      subject = described_class.new(json_parser)
      subject.upc_valid?

      expect(subject.error_messages).to eq(["UPC 34567 already exists"])
      expect(subject.error_messages.length).to eq(1)
    end

    it 'adds error message if upc belongs to a different tunecore user' do
      allow(ENV).to receive(:[]).with('BYTEDANCE_USER_ID').and_return((album.person_id + 1).to_s)
      upc = create(:tunecore_upc, number: "34567", upcable: album)
      distribution_api_service = create(:distribution_api_service, uuid: 'source_id')
      create(:distribution_api_album, source_album_id: 'source_album_id', album: album, distribution_api_service: distribution_api_service)

      json_parser = DistributionApi::JsonParser.new({
        source_album_id: 'source_album_id',
        source_id: 'source_id',
        upcs: [
          upc: "34567",
          type: "optional"
        ]
      }.to_json)

      subject = described_class.new(json_parser)
      subject.upc_valid?

      expect(subject.error_messages).to eq(["UPC 34567 already exists and belongs to a different tunecore user"])
      expect(subject.error_messages.length).to eq(1)
    end
  end
end
