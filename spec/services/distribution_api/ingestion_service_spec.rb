require "rails_helper"

describe DistributionApi::IngestionService do
  subject { described_class.new }

  let!(:key) { "key" }
  let!(:bucket) { "bucket" }
  let!(:local_json_path) { "local_json_path" }
  let!(:s3_downloader) { double(:s3_downloader) }
  let!(:distribution_api_service) { create(:distribution_api_service) }
  let!(:validation_service) { double(:validation_service) }

  before do
    expect(DistributionApi::S3Downloader).to receive(:new).with(key: key, bucket: bucket).and_return(s3_downloader)
    expect(s3_downloader).to receive(:cleanup)
    allow(File).to receive(:read).and_call_original
    allow(DateTime).to receive(:now).and_return(DateTime.parse("Fri, 17 Sep 2021 13:19:23 +0530"))
    allow(ENV).to receive(:[])
    allow(ENV).to receive(:[]).with('BYTEDANCE_USER_ID').and_return("BYTEDANCE_USER_ID")
  end

  context "S3 downloader is not able to download file" do
    it "takes no action" do
      expect(s3_downloader).to receive(:download).and_return(nil)
      expect_any_instance_of(described_class).to_not receive(:log_and_send_response)
      expect(described_class.call(key, bucket)).to be nil
    end
  end

  context "S3 downloader is able to download the file" do
    before do
      expect(s3_downloader).to receive(:download).and_return(local_json_path)
      expect(File).to receive(:read).with(local_json_path).and_return({
        source_album_id: "source_album_id",
        source_id: distribution_api_service.uuid,
        message_id: "message_id",
        source_contentreview_notes: "source_contentreview_notes",
        delivery_type: "full_delivery"
      }.to_json)
    end

    context "JSON already ingested" do
      it "logs the error in airbrake" do
        create(:distribution_api_ingestion_log,
          distribution_api_service: distribution_api_service,
          json_path: "S3://bucket/key",
          status: :ingested
        )
        expect(Airbrake).to receive(:notify).with("JSON S3://bucket/key already ingested for "\
          "source_album_id: source_album_id and "\
          "distribution_api_service: #{distribution_api_service.uuid}")
        expect_any_instance_of(described_class).to_not receive(:log_and_send_response)

        described_class.call(key, bucket)
      end
    end

    context "New JSON received" do
      before do
        allow_any_instance_of(described_class).to receive(:validation_service).and_return(validation_service)
        allow(validation_service).to receive(:valid?).and_return(true)
      end

      after do
        expect(DistributionApiIngestionLog.last.json_path).to eq("S3://bucket/key")
        expect(DistributionApiIngestionLog.last.distribution_api_service_id).to eq(distribution_api_service.id)
        expect(DistributionApiIngestionLog.last.source_album_id).to eq("source_album_id")
        expect(DistributionApiIngestionLog.last.source_contentreview_notes).to eq("source_contentreview_notes")
        expect(DistributionApiIngestionLog.last.message_id).to eq("message_id")
        expect(DistributionApiIngestionLog.last.delivery_type).to eq("full_delivery")
      end

      context "Invalid JSON" do
        it "returns Invalid JSON error" do
          response_json = {
            message_id: "message_id",
            tc_album_id: nil,
            source_album_id: "source_album_id",
            time: 1631864963,
            state: "error",
            songs: [],
            errors: [
              {
                error_state: "validation_error",
                error_code: nil,
                message: "Validation Error: UPC Already exists."
              }
            ]
          }

          allow(validation_service).to receive(:valid?).and_return(false)
          allow(validation_service).to receive(:error_messages).and_return(["Validation Error: UPC Already exists."])
          expect(DistributionApi::SendResponseService).to receive(:call).with(url: "ingestion", response_json: response_json)
          expect { described_class.call(key, bucket) }.to raise_error("Distribution Api Ingestion failed for S3://bucket/key")
          expect(DistributionApiIngestionLog.last.status).to eq("error")
        end
      end

      context "Album Already Finalized" do
        it "returns Album Already finalized error" do
          album = create(:album, :finalized)
          create(:distribution_api_album,
            album: album,
            distribution_api_service: distribution_api_service,
            source_album_id: "source_album_id",
            source_user_id: "source_user_id"
          )

          response_json = {
            message_id: "message_id",
            tc_album_id: album.id,
            source_album_id: "source_album_id",
            time: 1631864963,
            state: "error",
            songs: [],
            errors: [
              {
                error_state: "validation_error",
                error_code: nil,
                message: "Album already Ingested for s3_path: S3://bucket/key. Please contact Customer Care."
              }
            ]
          }

          expect(DistributionApi::SendResponseService).to receive(:call).with(url: "ingestion", response_json: response_json)
          expect { described_class.call(key, bucket) }.to raise_error("Distribution Api Ingestion failed for S3://bucket/key")
          expect(DistributionApiIngestionLog.last.status).to eq("error")
        end
      end

      context "Album Is Rejected" do
        it "update succeeds for valid reason" do
          validation_service = double(:validation_service)
          album = create(:album)
          create(:distribution_api_album,
            album: album,
            distribution_api_service: distribution_api_service,
            source_album_id: "source_album_id",
            source_user_id: "source_user_id"
          )

          response_json = {
            message_id: "message_id",
            tc_album_id: album.id,
            source_album_id: "source_album_id",
            time: 1631864963,
            state: "ingested",
            errors: [],
            songs: []
          }

          person = create(:person, :admin)
          content_review_role = Role.where(name: 'Content Review Manager')
          person.roles << content_review_role
          review_reason = create(:review_reason, roles: content_review_role, should_unfinalize: true)
          review_audit = create(:review_audit, album: album, event: "REJECTED", person: person, review_reasons: [review_reason])

          allow_any_instance_of(described_class).to receive(:validation_service).and_return(validation_service)
          allow(validation_service).to receive(:valid?).and_return(true)
          expect_any_instance_of(DistributionApi::Update::Base).to receive(:run).and_return(true)
          expect(DistributionApi::SendResponseService).to receive(:call).with(url: "ingestion", response_json: response_json)

          described_class.call(key, bucket)
          expect(DistributionApiIngestionLog.last.status).to eq("ingested")
        end
      end

      context "Able to Ingest but not finalized Tunecore Album already exists" do
        it "ingests album after deleting previous resources" do
          validation_service = double(:validation_service)
          album = create(:album)
          create(:distribution_api_album,
            album: album,
            distribution_api_service: distribution_api_service,
            source_album_id: "source_album_id",
            source_user_id: "source_user_id"
          )

          response_json = {
            message_id: "message_id",
            tc_album_id: album.id,
            source_album_id: "source_album_id",
            time: 1631864963,
            state: "ingested",
            errors: [],
            songs: []
          }

          allow_any_instance_of(described_class).to receive(:validation_service).and_return(validation_service)
          allow(validation_service).to receive(:valid?).and_return(true)

          expect_any_instance_of(DistributionApi::Ingest::Base).to receive(:run).and_return(true)
          expect(DistributionApi::SendResponseService).to receive(:call).with(url: "ingestion", response_json: response_json)
          expect(DistributionApi::DeletionService).to receive(:call).with(album.id)
          described_class.call(key, bucket)
          expect(DistributionApiIngestionLog.last.status).to eq("ingested")
        end
      end

      context "Ingestion throws an error" do
        it "returns generic error and does not ingest album" do
          validation_service = double(:validation_service)
          allow_any_instance_of(described_class).to receive(:validation_service).and_return(validation_service)
          allow(validation_service).to receive(:valid?).and_return(true)

          response_json = {
            message_id: "message_id",
            tc_album_id: nil,
            source_album_id: "source_album_id",
            time: 1631864963,
            state: "error",
            songs: [],
            errors: [
              {
                error_state: "technical_error",
                error_code: nil,
                message: "null pointer exception"
              }
            ]
          }

          expect_any_instance_of(described_class).to receive(:ingest).and_raise(StandardError.new("null pointer exception"))
          expect(DistributionApi::SendResponseService).to receive(:call).with(url: "ingestion", response_json: response_json)
          expect { described_class.call(key, bucket) }.to raise_error("Distribution Api Ingestion failed for S3://bucket/key")
          expect(DistributionApiIngestionLog.last.status).to eq("error")
        end
      end

      context "Ingestion is successful" do
        it "returns successful response message after album ingestion" do
          validation_service = double(:validation_service)
          ingest_service = double(:ingest_service)
          allow_any_instance_of(described_class).to receive(:validation_service).and_return(validation_service)
          allow(validation_service).to receive(:valid?).and_return(true)

          response_json = {
            message_id: "message_id",
            tc_album_id: nil,
            source_album_id: "source_album_id",
            time: 1631864963,
            state: "ingested",
            songs: [],
            errors: []
          }

          allow(DistributionApi::Ingest::Base).to receive(:new).with(any_args).and_return(validation_service)
          allow(validation_service).to receive(:run).and_return(true).and_return(true)
          expect(DistributionApi::SendResponseService).to receive(:call).with(url: "ingestion", response_json: response_json)
          described_class.call(key, bucket)
          expect(DistributionApiIngestionLog.last.status).to eq("ingested")
        end
      end

      context "Ingestion is successful if another distribution api ingestion log is in process" do
        it "returns successful response message after album ingestion" do
          validation_service = double(:validation_service)
          ingest_service = double(:ingest_service)
          allow_any_instance_of(described_class).to receive(:validation_service).and_return(validation_service)
          allow(validation_service).to receive(:valid?).and_return(true)
          create(:distribution_api_ingestion_log,
            distribution_api_service: distribution_api_service,
            json_path: "S3://bucket/key",
            status: :ingested,
            message_id: 'message_id',
            source_album_id: 'source_album_id',
            status: :in_process
          )

          response_json = {
            message_id: "message_id",
            tc_album_id: nil,
            source_album_id: "source_album_id",
            time: 1631864963,
            state: "ingested",
            songs: [],
            errors: []
          }

          allow(DistributionApi::Ingest::Base).to receive(:new).with(any_args).and_return(validation_service)
          allow(validation_service).to receive(:run).and_return(true).and_return(true)
          expect(DistributionApi::SendResponseService).to receive(:call).with(url: "ingestion", response_json: response_json)
          described_class.call(key, bucket)
          expect(DistributionApiIngestionLog.last.status).to eq("ingested")
        end
      end

      context "Ingestion is successful if another distribution api ingestion log is in error state" do
        it "returns successful response message after album ingestion" do
          validation_service = double(:validation_service)
          ingest_service = double(:ingest_service)
          allow_any_instance_of(described_class).to receive(:validation_service).and_return(validation_service)
          allow(validation_service).to receive(:valid?).and_return(true)
          create(:distribution_api_ingestion_log,
            distribution_api_service: distribution_api_service,
            json_path: "S3://bucket/key",
            status: :ingested,
            message_id: 'message_id',
            source_album_id: 'source_album_id',
            status: :error
          )

          response_json = {
            message_id: "message_id",
            tc_album_id: nil,
            source_album_id: "source_album_id",
            time: 1631864963,
            state: "ingested",
            songs: [],
            errors: []
          }

          allow(DistributionApi::Ingest::Base).to receive(:new).with(any_args).and_return(validation_service)
          allow(validation_service).to receive(:run).and_return(true).and_return(true)
          expect(DistributionApi::SendResponseService).to receive(:call).with(url: "ingestion", response_json: response_json)
          described_class.call(key, bucket)
          expect(DistributionApiIngestionLog.last.status).to eq("ingested")
        end
      end
    end
  end

end
