require "rails_helper"

describe DistributionApi::ExternalServiceIdApi::Base do
  let!(:person) { create(:person) }

  let!(:album_1) { create(:album, person: person) }
  let!(:album_2) { create(:album, person: person) }

  let!(:distribution_api_album_1) { create(:distribution_api_album, album: album_1, source_album_id: "source_album_id_1") }
  let!(:distribution_api_album_2) { create(:distribution_api_album, album: album_2, source_album_id: "source_album_id_2") }

  let!(:artist_1) { create(:artist) }
  let!(:artist_2) { create(:artist) }
  let!(:artist_3) { create(:artist) }
  let!(:artist_4) { create(:artist) }

  let!(:creative_1) { create(:creative, creativeable: album_1, artist: artist_1) }
  let!(:creative_2) { create(:creative, creativeable: album_1, artist: artist_2) }
  let!(:creative_3) { create(:creative, creativeable: album_2, artist: artist_3) }
  let!(:creative_4) { create(:creative, creativeable: album_2, artist: artist_4) }

  let!(:distribution_api_service) { create(:distribution_api_service, name: "DistributionApiService")}

  let!(:esid_creation_date_1) { "2020-01-01" }
  let!(:esid_creation_date_2) { "2020-01-02" }

  before do
    allow(ENV).to receive(:[])
    allow(ENV).to receive(:[]).with('BYTEDANCE_USER_ID').and_return(person.id)
    allow(Time).to receive(:now).and_return(Time.parse("2020-01-31"))
  end

  context "user has no external service IDS for the passed date" do
    it "does not call ExternalServiceIdApiSendWorker" do
      expect(DistributionApi::ExternalServiceIdApiSendWorker).to_not receive(:perform_async)
      described_class.run(esid_creation_date: esid_creation_date_1, distribution_api_service_name: "DistributionApiService")
    end
  end

  context "Force Mode" do
    it "deletes existing distribution_api_esid and sends again" do
      create(:external_service_id, linkable: album_1, updated_at: esid_creation_date_1, identifier: "spotify_album_id", service_name: "spotify")
      distribution_api_esid = create(:distribution_api_external_service_id_api_status,
                                      album_id: album_1.id,
                                      creation_date: esid_creation_date_1,
                                      response_status: "response_status",
                                      distribution_api_service: distribution_api_service)
      expect(DistributionApi::ExternalServiceIdApiSendWorker).to receive(:perform_async).with(
        album_id: album_1.id,
        esid_creation_date: esid_creation_date_1,
        json: {
          count_itemized: 1,
          count_total: 1,
          album: {
            artists: [],
            external_service_identifiers: {
              apple: "",
              itunes: "",
              spotify: "spotify_album_id"
            },
            source_album_id: "source_album_id_1",
            tc_album_id: album_1.id
          }
        }
      )
      described_class.run(esid_creation_date: esid_creation_date_1, distribution_api_service_name: "DistributionApiService", force_send_esid: true)
      expect(DistributionApiExternalServiceIdApiStatus.last.id).to_not eq(distribution_api_esid.id)
    end
  end

  context "User has single album" do
    after(:each) do
      expect(DistributionApiExternalServiceIdApiStatus.last.album_id).to eq(album_1.id)
      expect(DistributionApiExternalServiceIdApiStatus.last.creation_date).to eq(Date.parse(esid_creation_date_1))
    end

    context "album has one album external service identifier for the passed date" do
      it "passes external service ids JSON to Distribution Api Service" do
        create(:external_service_id, linkable: album_1, updated_at: esid_creation_date_1, identifier: "spotify_album_id", service_name: "spotify")
        expect(DistributionApi::ExternalServiceIdApiSendWorker).to receive(:perform_async).with(
        album_id: album_1.id,
        esid_creation_date: esid_creation_date_1,
        json: {
          count_itemized: 1,
          count_total: 1,
          album: {
            artists: [],
            external_service_identifiers: {
              apple: "",
              itunes: "",
              spotify: "spotify_album_id"
            },
            source_album_id: "source_album_id_1",
            tc_album_id: album_1.id
          }
        })
        described_class.run(esid_creation_date: esid_creation_date_1, distribution_api_service_name: "DistributionApiService")
      end
    end

    context "album has one album multiple external service identifier for the passed date" do
      it "passes external service ids JSON to Distribution Api Service" do
        ['spotify', 'apple', 'itunes'].each do |type|
          create(:external_service_id, linkable: album_1, updated_at: esid_creation_date_1, identifier: "#{type}_album_id", service_name: type)
        end

        expect(DistributionApi::ExternalServiceIdApiSendWorker).to receive(:perform_async).with(
          album_id: album_1.id,
          esid_creation_date: esid_creation_date_1,
          json: {
            count_itemized: 1,
            count_total: 1,
            album: {
              artists: [],
              external_service_identifiers: {
                apple: "apple_album_id",
                itunes: "itunes_album_id",
                spotify: "spotify_album_id"
              },
              source_album_id: "source_album_id_1",
              tc_album_id: album_1.id
            }
          }
        )
        described_class.run(esid_creation_date: esid_creation_date_1, distribution_api_service_name: "DistributionApiService")
      end
    end

    context "album has artist external service identifier but no album identifier for the passed date" do
      it "passes external service ids JSON to Distribution Api Service" do
        ['spotify', 'apple'].each do |type|
          create(:external_service_id, linkable: creative_1, updated_at: esid_creation_date_1, identifier: "#{type}_artist_id", service_name: type)
        end

        expect(DistributionApi::ExternalServiceIdApiSendWorker).to receive(:perform_async).with(
          album_id: album_1.id,
          esid_creation_date: esid_creation_date_1,
          json: {
            count_itemized: 1,
            count_total: 1,
            album: {
              artists: [{
                apple_artist_id: "apple_artist_id",
                name: artist_1.name,
                tc_creative_id: creative_1.id,
                spotify_artist_id: "spotify_artist_id"
              }],
              external_service_identifiers: {
                apple: "",
                itunes: "",
                spotify: ""
              },
              source_album_id: "source_album_id_1",
              tc_album_id: album_1.id
            }
          }
        )
        described_class.run(esid_creation_date: esid_creation_date_1, distribution_api_service_name: "DistributionApiService")
      end
    end

    context "album has multiple artists with external service identifiers for the passed date" do
      it "passes external service ids JSON to Distribution Api Service" do
        ['spotify', 'apple'].each do |type|
          [creative_1, creative_2].each_with_index do |creative, index|
            create(:external_service_id, linkable: creative, updated_at: esid_creation_date_1, identifier: "#{type}_artist_id_#{index + 1}", service_name: type)
          end
        end

        expect(DistributionApi::ExternalServiceIdApiSendWorker).to receive(:perform_async).with(
          album_id: album_1.id,
          esid_creation_date: esid_creation_date_1,
          json: {
            count_itemized: 1,
            count_total: 1,
            album: {
              artists: [{
                apple_artist_id: "apple_artist_id_1",
                name: artist_1.name,
                tc_creative_id: creative_1.id,
                spotify_artist_id: "spotify_artist_id_1"
              },
              {
                apple_artist_id: "apple_artist_id_2",
                name: artist_2.name,
                tc_creative_id: creative_2.id,
                spotify_artist_id: "spotify_artist_id_2"
              }],
              external_service_identifiers: {
                apple: "",
                itunes: "",
                spotify: ""
              },
              source_album_id: "source_album_id_1",
              tc_album_id: album_1.id
            }
          }
        )
        described_class.run(esid_creation_date: esid_creation_date_1, distribution_api_service_name: "DistributionApiService")
      end
    end

    context "album has multiple artist identifiers for the passed date" do
      it "passes external service ids JSON to Distribution Api Service of that date" do
        ['apple', 'spotify'].each do |type|
          create(:external_service_id, linkable: album_1, updated_at: esid_creation_date_1, identifier: "#{type}_album_id", service_name: type)
        end

        ['spotify', 'apple'].each do |type|
          create(:external_service_id, linkable: creative_1, updated_at: esid_creation_date_1, identifier: "#{type}_artist_id_1", service_name: type)
        end

        create(:external_service_id, linkable: creative_2, updated_at: esid_creation_date_1, identifier: "spotify_artist_id_2", service_name: "spotify")

        # External service id updated on a different date
        create(:external_service_id, linkable: album_1, updated_at: esid_creation_date_2, service_name: "itunes")
        create(:external_service_id, linkable: creative_2, updated_at: esid_creation_date_2, identifier: "apple_artist_id_2", service_name: "apple")

        expect(DistributionApi::ExternalServiceIdApiSendWorker).to receive(:perform_async).with(
          album_id: album_1.id,
          esid_creation_date: esid_creation_date_1,
          json: {
            count_itemized: 1,
            count_total: 1,
            album: {
              artists: [{
                apple_artist_id: "apple_artist_id_1",
                name: artist_1.name,
                tc_creative_id: creative_1.id,
                spotify_artist_id: "spotify_artist_id_1"
              },
              {
                apple_artist_id: "",
                name: artist_2.name,
                tc_creative_id: creative_2.id,
                spotify_artist_id: "spotify_artist_id_2"
              }],
              external_service_identifiers: {
                apple: "apple_album_id",
                itunes: "",
                spotify: "spotify_album_id"
              },
              source_album_id: "source_album_id_1",
              tc_album_id: album_1.id
            }
          }
        )
        described_class.run(esid_creation_date: esid_creation_date_1, distribution_api_service_name: "DistributionApiService")
      end
    end
  end

  context "user has multiple albums and multiple artist identifiers for the passed date" do
    after(:each) do
      expect(DistributionApiExternalServiceIdApiStatus.last(2).pluck(:album_id)).to eq([album_1.id, album_2.id])
    end

    it "passes external service ids JSON to Distribution Api Service of that date" do
      ['spotify', 'itunes', 'apple'].each do |type|
        [album_1, album_2].each.with_index do |album, index|
          create(:external_service_id, linkable: album, updated_at: esid_creation_date_1, identifier: "#{type}_album_id_#{index + 1}", service_name: type)
        end
      end

      ['spotify', 'apple'].each do |type|
        [creative_1, creative_2, creative_3, creative_4].each_with_index do |creative, index|
          create(:external_service_id, linkable: creative, updated_at: esid_creation_date_1, identifier: "#{type}_artist_id_#{index + 1}", service_name: type)
        end
      end

      expect(DistributionApi::ExternalServiceIdApiSendWorker).to receive(:perform_async).with(
        album_id: album_1.id,
        esid_creation_date: esid_creation_date_1,
        json: {
          count_itemized: 1,
          count_total: 2,
          album: {
            artists: [{
              apple_artist_id: "apple_artist_id_1",
              name: artist_1.name,
              tc_creative_id: creative_1.id,
              spotify_artist_id: "spotify_artist_id_1"
            },
            {
              apple_artist_id: "apple_artist_id_2",
              name: artist_2.name,
              tc_creative_id: creative_2.id,
              spotify_artist_id: "spotify_artist_id_2"
            }],
            external_service_identifiers: {
              apple: "apple_album_id_1",
              itunes: "itunes_album_id_1",
              spotify: "spotify_album_id_1"
            },
            source_album_id: "source_album_id_1",
            tc_album_id: album_1.id
          }
        }
      )

      expect(DistributionApi::ExternalServiceIdApiSendWorker).to receive(:perform_async).with(
        album_id: album_2.id,
        esid_creation_date: esid_creation_date_1,
        json: {
          count_itemized: 2,
          count_total: 2,
          album: {
            artists: [{
              apple_artist_id: "apple_artist_id_3",
              name: artist_3.name,
              tc_creative_id: creative_3.id,
              spotify_artist_id: "spotify_artist_id_3"
            },
            {
              apple_artist_id: "apple_artist_id_4",
              name: artist_4.name,
              tc_creative_id: creative_4.id,
              spotify_artist_id: "spotify_artist_id_4"
            }],
            external_service_identifiers: {
              apple: "apple_album_id_2",
              itunes: "itunes_album_id_2",
              spotify: "spotify_album_id_2"
            },
            source_album_id: "source_album_id_2",
            tc_album_id: album_2.id
          }
        }
      )
      described_class.run(esid_creation_date: esid_creation_date_1, distribution_api_service_name: "DistributionApiService")
    end
  end

  context "retry failed esids" do
    it "sends again the failed esids" do
      ['spotify', 'itunes', 'apple'].each do |type|
        [album_1, album_2].each.with_index do |album, index|
          create(:external_service_id, linkable: album, updated_at: esid_creation_date_1, identifier: "#{type}_album_id_#{index + 1}", service_name: type)
        end
      end

      ['spotify', 'apple'].each do |type|
        [creative_1, creative_2, creative_3, creative_4].each_with_index do |creative, index|
          create(:external_service_id, linkable: creative, updated_at: esid_creation_date_1, identifier: "#{type}_artist_id_#{index + 1}", service_name: type)
        end
      end

      create(:distribution_api_external_service_id_api_status,
              album_id: album_1.id,
              creation_date: esid_creation_date_1,
              response_status: "response_status",
              distribution_api_service: distribution_api_service)

      create(:distribution_api_external_service_id_api_status,
              album_id: album_2.id,
              creation_date: esid_creation_date_1,
              distribution_api_service: distribution_api_service)

      expect(DistributionApi::ExternalServiceIdApiSendWorker).to receive(:perform_async).with(
        album_id: album_2.id,
        esid_creation_date: esid_creation_date_1,
        json: {
          count_itemized: 2,
          count_total: 2,
          album: {
            artists: [{
              apple_artist_id: "apple_artist_id_3",
              name: artist_3.name,
              tc_creative_id: creative_3.id,
              spotify_artist_id: "spotify_artist_id_3"
            },
            {
              apple_artist_id: "apple_artist_id_4",
              name: artist_4.name,
              tc_creative_id: creative_4.id,
              spotify_artist_id: "spotify_artist_id_4"
            }],
            external_service_identifiers: {
              apple: "apple_album_id_2",
              itunes: "itunes_album_id_2",
              spotify: "spotify_album_id_2"
            },
            source_album_id: "source_album_id_2",
            tc_album_id: album_2.id
          }
        }
      )

      described_class.run(esid_creation_date: esid_creation_date_1, distribution_api_service_name: "DistributionApiService")
    end

    it "does not do anything if nothing failed" do
      ['spotify', 'itunes', 'apple'].each do |type|
        [album_1, album_2].each.with_index do |album, index|
          create(:external_service_id, linkable: album, updated_at: esid_creation_date_1, identifier: "#{type}_album_id_#{index + 1}", service_name: type)
        end
      end

      ['spotify', 'apple'].each do |type|
        [creative_1, creative_2, creative_3, creative_4].each_with_index do |creative, index|
          create(:external_service_id, linkable: creative, updated_at: esid_creation_date_1, identifier: "#{type}_artist_id_#{index + 1}", service_name: type)
        end
      end

      [album_1, album_2].each do |album|
        create(:distribution_api_external_service_id_api_status,
                album_id: album.id,
                creation_date: esid_creation_date_1,
                response_status: "response_status",
                distribution_api_service: distribution_api_service)
      end

      expect(DistributionApi::ExternalServiceIdApiSendWorker).to_not receive(:perform_async)

      described_class.run(esid_creation_date: esid_creation_date_1, distribution_api_service_name: "DistributionApiService")
    end
  end

  context "delete esids status after 90 days" do
    it "deletes esids status after 90 days" do
      esid_1 = create(:distribution_api_external_service_id_api_status, creation_date: Date.parse("2019-12-31"))
      esid_2 = create(:distribution_api_external_service_id_api_status, creation_date: Date.parse("2019-08-31"))
      described_class.run(esid_creation_date: esid_creation_date_1, distribution_api_service_name: "DistributionApiService")
      expect(DistributionApiExternalServiceIdApiStatus.find_by(id: esid_1.id)).not_to be_nil
      expect(DistributionApiExternalServiceIdApiStatus.find_by(id: esid_2.id)).to be_nil
    end
  end
end
