require "rails_helper"

describe DistributionApi::ExternalServiceIdApi::SendService do
  it "sends api via external service id api and save respnse in distribution_api esid status" do
    album = create(:album)
    esid_creation_date = "2020-01-01"
    json = "json"

    distribution_api_esid_status = create(:distribution_api_external_service_id_api_status, album: album, creation_date: Date.parse(esid_creation_date))

    expect(DistributionApi::SendResponseService).to receive(:call).with(url: "meta_info", response_json: json).and_return("response_status")
    described_class.call({
      album_id: album.id,
      esid_creation_date: esid_creation_date,
      json: json
    })

    distribution_api_esid_status.reload

    expect(distribution_api_esid_status.response_status).to eq("response_status")
  end
end
