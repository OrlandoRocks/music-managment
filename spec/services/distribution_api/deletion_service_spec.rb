require "rails_helper"

describe DistributionApi::DeletionService do
  let!(:album) { create(:album) }
  let!(:song_1) { create(:song, album: album)}
  let!(:song_2) { create(:song, album: album)}
  let!(:song_3) { create(:song, album: album)}
  let!(:artwork) { create(:artwork, album: album) }
  let!(:creative_1) { create(:creative, creativeable: album)}
  let!(:distribution_api_service) { create(:distribution_api_service)}
  let!(:distribution_api_album) { create(:distribution_api_album, album: album, distribution_api_service: distribution_api_service)}
  let!(:distribution_api_song_1) { create(:distribution_api_song, song: song_1, distribution_api_album: distribution_api_album)}
  let!(:distribution_api_song_2) { create(:distribution_api_song, song: song_2, distribution_api_album: distribution_api_album)}
  let!(:distribution_api_song_3) { create(:distribution_api_song, song: song_3, distribution_api_album: distribution_api_album)}
  let!(:store) { create(:store) }
  let!(:salepointable_store) { create(:salepointable_store, store: store) }
  let(:salepoint) { create(:salepoint, salepointable: album, store: store) }
  let!(:purchase) { create(:purchase, related: album, person: album.person, paid_at: nil) }
  let!(:distribution_api_external_service_id_api_status) { create(:distribution_api_external_service_id_api_status, album_id: album.id) }

  describe "#call" do
    before do
      album.salepoints = [salepoint]
    end

    it "deletes all album resources" do
      described_class.call(album.id)
      expect { album.reload }.to raise_error(ActiveRecord::RecordNotFound)
      expect { song_1.reload }.to raise_error(ActiveRecord::RecordNotFound)
      expect { song_2.reload }.to raise_error(ActiveRecord::RecordNotFound)
      expect { song_3.reload }.to raise_error(ActiveRecord::RecordNotFound)
      expect { distribution_api_song_1.reload }.to raise_error(ActiveRecord::RecordNotFound)
      expect { distribution_api_song_2.reload }.to raise_error(ActiveRecord::RecordNotFound)
      expect { distribution_api_song_3.reload }.to raise_error(ActiveRecord::RecordNotFound)
      expect { artwork.reload }.to raise_error(ActiveRecord::RecordNotFound)
      expect { creative_1.reload }.to raise_error(ActiveRecord::RecordNotFound)
      expect { distribution_api_album.reload }.to raise_error(ActiveRecord::RecordNotFound)
      expect { salepoint.reload }.to raise_error(ActiveRecord::RecordNotFound)
      expect { purchase.reload }.to raise_error(ActiveRecord::RecordNotFound)
      expect { distribution_api_external_service_id_api_status.reload }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end
end
