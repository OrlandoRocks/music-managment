require "rails_helper"
include DistributionApiJsonHelper

describe DistributionApi::Update::Song do
  let(:album) { create(:album, :with_songs, :with_song_songwriters, number_of_songs: 2) }
  let(:update_artists) do
    [
      {
        "name": "Update Artist 1",
        "creative_role": "contributor",
        "roles": ["songwriter"],
      },
      {
        "name": "Update Artist 2",
        "creative_role": "primary_artist",
        "roles": ["producer"],
      },
    ]
  end
  let(:update_hash) { JSON.parse(VALID_JSON ) }

  def mock_saved_songs(json_parser)
    json_parser.parsed_json.songs.each_with_object({}) do |s, h|
      h[s.source_song_id.to_s] = album.songs[h.keys.length]
    end
  end

  before do
    allow_any_instance_of(described_class).to receive(:upload_asset).and_return(true)
  end

  describe "#update" do
    context "VALID_JSON" do
      it "updates creatives" do
        original_creatives = album.songs[0].creatives.map(&:name)

        update_hash['songs'][0]['artists'] = update_artists
        update_hash['songs'][1]['artists'] = update_artists
        json_parser = DistributionApi::JsonParser.new(update_hash.to_json)

        instance = described_class.new(
          json_parser: json_parser,
          tunecore_album: album
        )

        saved_songs = mock_saved_songs(json_parser)
        allow(instance).to receive(:saved_songs).exactly(3).times.and_return(saved_songs)

        instance.update
        album.reload
        result = album.songs[0].creatives.map do |c|
          { name: c.name, role: c.role, roles: c.creative_song_roles.map(&:song_role_id) }
        end

        expect(result).to eq([
          {
            :name=>"Update Artist 1",
            :role=>"contributor",
            :roles=>[3]
          },
          {
            :name=>"Update Artist 2",
            :role=>"primary_artist",
            :roles=>[2]
          }
        ])
      end

      it "ignores locked attributes in update JSON" do
        original_track_nums = album.songs.map(&:track_num)

        update_hash['songs'][0]['sequence'] = 4
        update_hash['songs'][1]['sequence'] = 5
        json_parser = DistributionApi::JsonParser.new(update_hash.to_json)

        instance = described_class.new(
          json_parser: json_parser,
          tunecore_album: album
        )

        saved_songs = mock_saved_songs(json_parser)
        allow(instance).to receive(:saved_songs).exactly(3).times.and_return(saved_songs)

        instance.update
        album.reload

        expect(album.songs.map(&:track_num)).to eq original_track_nums
      end

      it "updates non-locked attributes in update JSON" do
        update_title = 'update title'
        expect(album.songs[0].name).not_to eq update_title
        expect(album.songs[1].name).not_to eq "#{update_title} 2"

        update_hash['songs'][0]['title'] = update_title
        update_hash['songs'][1]['title'] = "#{update_title} 2"
        json_parser = DistributionApi::JsonParser.new(update_hash.to_json)

        instance = described_class.new(
          json_parser: json_parser,
          tunecore_album: album
        )

        saved_songs = mock_saved_songs(json_parser)
        allow(instance).to receive(:saved_songs).exactly(3).times.and_return(saved_songs)

        instance.update
        album.reload

        expect(album.songs.map(&:name)).to eq ["Update Title", "Update Title 2"]
      end
    end

    describe("control tunecore_song_attributes changes") do
      it "with a test to ensure LOCKED_SONG_ATTRS are kept up-to-date" do
        json_parser = DistributionApi::JsonParser.new(update_hash.to_json)
        result = json_parser.tunecore_song_attributes(album.id).first.keys.sort

        # If this assertion fails, ensure that new attrs are added to the
        # LOCKED_SONG_ATTRS constant where needed before updating.
        expect(result).to eq [
          :album_id,
          :artists,
          :asset_url,
          :clean_version,
          :copyrights,
          :cover_song,
          :explicit,
          :instrumental,
          :language_code_id,
          :language_name,
          :lyrics,
          :made_popular_by,
          :name,
          :optional_isrc,
          :previously_released_at,
          :song_copyright_claims,
          :song_start_times,
          :source_song_id,
          :track_number,
          :version,
        ]
      end

      it "with a test to ensure hard-coded values are kept up-to-date" do
        controlled_attrs = [
          "copyrights",
          "cover_song",
          "made_popular_by",
          "song_copyright_claims",
          "version"
        ]
        controlled_attrs.each { |attr| update_hash['songs'][0][attr] = 'foo' }
        json_parser = DistributionApi::JsonParser.new(update_hash.to_json)
        result = json_parser.tunecore_song_attributes(123)[0].slice(*controlled_attrs.map(&:to_sym))

        expect(result.values).to eq([nil, false, nil, nil, nil])
      end
    end
  end
end
