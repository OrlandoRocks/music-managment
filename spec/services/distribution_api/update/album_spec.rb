require "rails_helper"
include DistributionApiJsonHelper

describe DistributionApi::Update::Album do
  let(:album) { create(:album) }
  let(:update_hash) { JSON.parse(VALID_JSON) }

  describe "#update" do
    context "VALID_JSON" do
      it "updates the album, updates creatives" do
        original_creatives = album.creatives.dup
        json_parser = DistributionApi::JsonParser.new(VALID_JSON)

        described_class.update(
          json_parser: json_parser,
          tunecore_album: album
        )

        album.reload
        expect(album.creatives.map(&:name)).to eq JSON
                                                  .parse(VALID_JSON)['artists']
                                                  .map { |a| a['name'] }
      end

      it "ignores locked attributes in update JSON" do
        update_hash["release_type"] = 'Ringtone'
        update_hash["optional_upc_number"] = 'optional_upc_number'
        json_parser = DistributionApi::JsonParser.new(update_hash.to_json)

        described_class.update(
          json_parser: json_parser,
          tunecore_album: album
        )

        album.reload
        expect(album.album_type).to eq 'Album'
        expect(album.optional_upc_number).to be nil
      end

      it "updates non-locked attributes in update JSON" do
        update_hash["metadata_language"] = "fr"
        json_parser = DistributionApi::JsonParser.new(update_hash.to_json)

        described_class.update(
          json_parser: json_parser,
          tunecore_album: album
        )

        album.reload
        expect(album.metadata_language_code.code).to eq 'fr'
      end
    end

    describe("control tunecore_album_attributes changes") do
      it "with a test to ensure LOCKED_ALBUM_ATTRS are kept up-to-date" do
        json_parser = DistributionApi::JsonParser.new(update_hash.to_json)
        result = json_parser.tunecore_album_attributes.keys.sort

        # If this assertion fails, ensure that new attrs are added to the
        # LOCKED_ALBUM_ATTRS constant where needed before updating.
        expect(result).to eq [
          :apple_music,
          :clean_version,
          :creatives,
          :golive_date,
          :is_various,
          :label_name,
          :language_code,
          :name,
          :optional_isrc,
          :optional_upc_number,
          :orig_release_year,
          :parental_advisory,
          :primary_genre_id,
          :recording_location,
          :sale_date,
          :secondary_genre_id,
          :specialized_release_type,
          :timed_release_timing_scenario
        ]
      end

      it "with a test to ensure hard-coded values are kept up-to-date" do
        blank_attrs = [
          "optional_isrc",
          "recording_location",
          "sale_date",
          "specialized_release_type"
        ]
        blank_attrs.each { |attr| update_hash[attr] = 'foo' }
        json_parser = DistributionApi::JsonParser.new(update_hash.to_json)
        result = json_parser.tunecore_album_attributes.slice(*blank_attrs.map(&:to_sym))

        expect(result.values.all?(/^$/)).to be true
      end
    end
  end
end
