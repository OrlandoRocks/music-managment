require "rails_helper"
include DistributionApiJsonHelper

describe DistributionApi::Update::Base do
  let(:person)                   { create(:person) }
  let(:update_hash)              { JSON.parse(VALID_JSON) }
  let!(:ts)                      { Time.now.to_i.to_s }
  let(:json)                     { update_hash.merge(source_album_id: ts).to_json }
  let(:json_parser)              { DistributionApi::JsonParser.new(json) }
  let(:distribution_api_service) { create(:distribution_api_service) }

  before do
    distribution_api_service.update(uuid: "distribution_api_source_identifier")
    distribution_api_service.save
  end

  def ingest
    DistributionApi::Ingest::Album.ingest(json_parser, person)
    album = DistributionApiAlbum.find_by(source_album_id: ts).album
    allow(album).to receive(:should_finalize?).and_return true
    allow(album).to receive(:already_paid?).and_return true
    allow_any_instance_of(DistributionApi::Update::Song).to receive(:upload_asset).and_return true
    allow_any_instance_of(DistributionApi::Ingest::Song).to receive(:upload_asset).and_return true
    allow_any_instance_of(DistributionApi::Ingest::Artwork).to receive(:ingest).and_return true
    DistributionApi::Ingest::Song.ingest(json_parser, person, album)
    album
  end

  describe "#update" do
    context "with VALID_JSON" do
      it "Updates the metadata for valid reason" do
        album = ingest

        # Prepare update
        updated_name                                  = "Updated Name"
        updated_title                                 = "Updated Title"
        update_hash                                   = JSON.parse(json)
        update_hash["title"]                          = updated_title
        update_hash["artists"][0]["name"]             = updated_name
        update_hash['songs'].each_with_index { |s, i| s['title'] = updated_title + i.to_s }
        update_hash['songs'][1]['artists'].each_with_index { |a, i| a['name'] = updated_name + i.to_s }
        updated_parser = DistributionApi::JsonParser.new(update_hash.to_json)

        described_class
          .new(json_parser: updated_parser, tunecore_album: album)
          .run
        album.reload

        expect(album.name).to              eq updated_title
        expect(album.creatives[0].name).to eq updated_name

        expect(album.songs.map(&:name)).to              eq ["Updated Title0", "Updated Title1"]
        expect(album.songs[1].creatives.map(&:name)).to eq ["Updated Name0", "Updated Name1"]

        expect(album.finalized?).to         be true
        expect(album.legal_review_state).to eq "NEEDS REVIEW"
      end
    end
  end
end
