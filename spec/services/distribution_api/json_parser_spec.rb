require "rails_helper"
include DistributionApiJsonHelper

describe DistributionApi::JsonParser do
  let!(:distribution_api_service) { create(:distribution_api_service) }

  before do
    distribution_api_service.update(uuid: "distribution_api_source_identifier")
    distribution_api_service.save
  end

  context "VALID_JSON" do
    describe "#tunecore_album_attributes" do
      it "returns album attributes" do
        expect(described_class.new(VALID_JSON).tunecore_album_attributes).to eq({
          apple_music: true,
          clean_version: nil,
          creatives: [{
            name: "Chip Taylor",
            role: "primary_artist",
            apple: {
              identifier: "489827"
            },
            spotify: {
              create_page: true
            }
          }],
          golive_date: {
            day: "13",
            hour: "4",
            meridian: "AM",
            min: "0",
            month: "8",
            year: "2021"
          },
          is_various: false,
          language_code: "en",
          name: "Can I Offer You a Song",
          optional_isrc: "",
          optional_upc_number: "670501008227",
          orig_release_year: "",
          parental_advisory: nil,
          primary_genre_id: 34,
          recording_location: "",
          sale_date: "",
          secondary_genre_id: 28,
          specialized_release_type: "",
          timed_release_timing_scenario: "relative_time",
          label_name: "Train Wreck Records"
        })
      end
    end

    describe "#distribution_api_album_attributes" do
      it "returns distribution api album attributes" do
        expect(described_class.new(VALID_JSON).distribution_api_album_attributes).to eq({
          distribution_api_service_id: distribution_api_service.id,
          source_user_id: "byte_dance_user_id",
          source_album_id: "byte_dance_album_id",
          source_contentreview_notes: "Content Review Notes"
        })
      end
    end

    describe "#tunecore_song_attributes" do
      it "returns tunecore song attributes" do
        album = create(:album)
        expect(described_class.new(VALID_JSON).tunecore_song_attributes(album.id)).to eq([{
          name: "Test Song 1",
          album_id: album.id,
          language_code_id: 1,
          language_name: "en",
          version: nil,
          cover_song: false,
          made_popular_by: nil,
          explicit: false,
          clean_version: false,
          optional_isrc: "USYVC2108201",
          lyrics: nil,
          asset_url: "qa/song1.flac",
          previously_released_at: "2021-09-09",
          instrumental: false,
          track_number: 1,
          artists: [{
            artist_name: "Artist 1",
            credit: "contributor",
            associated_to: "Song",
            name: "Artist 1",
            role_ids: [3]
          },
          {
            artist_name: "Artist 2",
            credit: "primary_artist",
            associated_to: "Song",
            name: "Artist 2",
            role_ids: []
          }],
          copyrights: nil,
          song_copyright_claims: nil,
          song_start_times: [{
            store_id: 87,
            start_time: 70.0
          }],
          source_song_id: 14238608
        }, {
          name: "Test Song 2",
          album_id: album.id,
          language_code_id: 1,
          language_name: "en",
          version: nil,
          cover_song: false,
          made_popular_by: nil,
          explicit: false,
          clean_version: false,
          optional_isrc: "USYVC2108202",
          lyrics: nil,
          asset_url: "qa/song2.flac",
          previously_released_at: "2021-09-09",
          instrumental: false,
          track_number: 2,
          artists: [{
            artist_name: "Artist 3",
            credit: "contributor",
            associated_to: "Song",
            name: "Artist 3",
            role_ids: [3]
          },
          {
            artist_name: "Artist 4",
            credit: "primary_artist",
            associated_to: "Song",
            name: "Artist 4",
            role_ids: []
          }],
          copyrights: nil,
          song_copyright_claims: nil,
          song_start_times: [{
            store_id: 87,
            start_time: 70.0
          }],
          source_song_id: 14238609
        }])
      end
    end
  end

  describe '#album_golive_date' do
    context 'timed_release_type is absolute_time and timed_release_date is present' do
      it 'returns album_golive_date based on timed_release_date' do
        json = {
          "message_id": "byte_dance_defined_message_id",
          "source_user_id": "byte_dance_user_id",
          "source_id": "distribution_api_source_identifier",
          "source_album_id": "byte_dance_album_id",
          "timed_release_date": "2021-08-13T04:00:00Z",
          "release_date": "2021-08-13",
          "timed_release_type": "absolute_time"
        }.to_json

        expect(described_class.new(json).album_golive_date).to eq({
          hour: "4",
          min: "0",
          meridian: "AM",
          day: "13",
          month: "8",
          year: "2021"
        })
      end
    end

    context 'timed_release_type is relative_time and timed_release_date_utc is present' do
      it 'returns album_golive_date based on timed_release_date_utc' do
        json = {
          "message_id": "byte_dance_defined_message_id",
          "source_user_id": "byte_dance_user_id",
          "source_id": "distribution_api_source_identifier",
          "source_album_id": "byte_dance_album_id",
          "timed_release_date_utc": "2021-08-13T16:30:00Z",
          "release_date": "2021-08-13",
          "timed_release_type": "relative_time"
        }.to_json

        expect(described_class.new(json).album_golive_date).to eq({
          hour: "4",
          min: "30",
          meridian: "PM",
          day: "13",
          month: "8",
          year: "2021"
        })
      end
    end

    context 'timed_release_type is null and release_date is present' do
      it 'returns album_golive_date based on release_date' do
        json = {
          "message_id": "byte_dance_defined_message_id",
          "source_user_id": "byte_dance_user_id",
          "source_id": "distribution_api_source_identifier",
          "source_album_id": "byte_dance_album_id",
          "timed_release_date": "2021-08-13T04:00:00Z",
          "release_date": "2021-08-13",
          "timed_release_type": ""
        }.to_json

        expect(described_class.new(json).album_golive_date).to eq({
          hour: "12",
          min: "0",
          meridian: "AM",
          day: "13",
          month: "8",
          year: "2021"
        })
      end
    end

    context 'timed_release_type is null and release_date_utc is present' do
      it 'returns album_golive_date based on release_date_utc' do
        json = {
          "message_id": "byte_dance_defined_message_id",
          "source_user_id": "byte_dance_user_id",
          "source_id": "distribution_api_source_identifier",
          "source_album_id": "byte_dance_album_id",
          "timed_release_date": "2021-08-12T04:00:00Z",
          "release_date_utc": "2021-08-12"
        }.to_json

        expect(described_class.new(json).album_golive_date).to eq({
          hour: "12",
          min: "0",
          meridian: "AM",
          day: "12",
          month: "8",
          year: "2021"
        })
      end
    end
  end

  describe "#stores_failing_mp3_validation" do
    context "when qobuz is in album_stores and no asset has mp3 asset" do
      it "returns valid album store" do
        json = {
          "album_stores": [
            {
                "store_id": 36
            },
            {
                "store_id": 96
            },
            {
                "store_id": 42
            }
          ],
          "songs": [
            {
              "sequence": 1,
              "artists": [],
              "asset_url": "qa/song1.flac"
            },
            {
              "sequence": 2,
              "artists": [],
              "asset_url": "qa/song2.flac"
            }
          ]
        }.to_json
        expect(described_class.new(json).stores.pluck(:id)).to eq([36, 96])
      end
    end

    context "when qobuz is in album_stores and one asset has mp3 asset" do
      it "returns valid album store" do
        json = {
          "album_stores": [
            {
                "store_id": 36
            },
            {
                "store_id": 96
            },
            {
                "store_id": 42
            }
          ],
          "songs": [
            {
              "sequence": 1,
              "artists": [],
              "asset_url": "qa/song1.flac"
            },
            {
              "sequence": 2,
              "artists": [],
              "asset_url": "qa/song2.mp3"
            }
          ]
        }.to_json
        expect(described_class.new(json).stores.pluck(:id)).to eq([36])
      end
    end

    context "when qobuz is in album_stores and all asset have mp3 asset" do
      it "returns valid album store" do
        json = {
          "album_stores": [
            {
                "store_id": 36
            },
            {
                "store_id": 96
            },
            {
                "store_id": 42
            }
          ],
          "songs": [
            {
              "sequence": 1,
              "artists": [],
              "asset_url": "qa/song1.mp3"
            },
            {
              "sequence": 2,
              "artists": [],
              "asset_url": "qa/song2.mp3"
            }
          ]
        }.to_json
        expect(described_class.new(json).stores.pluck(:id)).to eq([36])
      end
    end

    context "when qobuz is not in album_stores" do
      it "returns valid album store" do
        json = {
          "album_stores": [
            {
                "store_id": 36
            },
            {
                "store_id": 48
            },
            {
                "store_id": 42
            }
          ],
          "songs": [
            {
              "sequence": 1,
              "artists": [],
              "asset_url": "qa/song1.mp3"
            },
            {
              "sequence": 2,
              "artists": [],
              "asset_url": "qa/song2.mp3"
            }
          ]
        }.to_json
        expect(described_class.new(json).stores.pluck(:id)).to eq([36, 48])
      end
    end
  end
end
