require "rails_helper"
include DistributionApiJsonHelper

describe DistributionApi::Ingest::Album do
  let!(:person) { create(:person) }

  let!(:distribution_api_service) { create(:distribution_api_service) }

  before do
    distribution_api_service.update(uuid: "distribution_api_source_identifier")
    distribution_api_service.save
  end

  describe "#ingest" do
    context "VALID_JSON" do
      it "Ingests the album" do
        json_parser = DistributionApi::JsonParser.new(VALID_JSON)

        expect { 
          described_class.ingest(json_parser, person)
        }.to change { Album.count }.by(1)
        .and change { ExternalServiceId.count }.by(2)
        .and change { DistributionApiAlbum.count }.by(1)
      end
    end

    context "Album Form error" do
      it "Raises invalid UPC error if UPC already exists" do
        json_parser = DistributionApi::JsonParser.new(VALID_JSON)
        create(:tunecore_upc, number: 670501008227)

        expect {
          described_class.ingest(json_parser, person)
        }.to raise_error(an_instance_of(DistributionApi::Errors::IngestionError))
        .and change { Album.count }.by(0)
        .and change { ExternalServiceId.count }.by(0)
        .and change { DistributionApiAlbum.count }.by(0)
      end
    end
  end
end