require "rails_helper"

describe DistributionApi::Ingest::Song do
  context "with valid songs" do
    let(:person) { create(:person) }
    let(:album) { create(:single, person: person) }

    let(:song) do
      {
        artists: [
          {
            artist_name: album.creatives.first.artist.name,
            credit: 'mainartist',
            associated_to: 'Song',
            name: album.creatives.first.artist.name,
            role_ids: [3]
          }
        ],
        name: 'song name',
        album_id: album.id,
        language_code_id: 1,
        language_name: 'en',
        version: nil,
        cover_song: false,
        made_popular_by: nil,
        explicit: true,
        clean_version: false,
        optional_isrc: '',
        lyrics: nil,
        asset_url: 'song.asset_url',
        previously_released_at: nil,
        instrumental: false,
        track_number: 1,
        copyrights: nil,
        song_copyright_claims: nil,
        song_start_times: nil,
        source_song_id: "song.source_song_id"
      }
    end
    let(:json_parser) do
      double(tunecore_song_attributes: [song])
    end
    let(:distribution_api_service) { create(:distribution_api_service)}

    before do
      create(:distribution_api_album, album: album,
                                      distribution_api_service_id: distribution_api_service.id)
    end

    it "ingests the songs" do
      allow_any_instance_of(described_class).to receive(:upload_asset).and_return(true)
      allow(json_parser).to receive(:release_type).and_return("Single")

      described_class.ingest(json_parser, person, album)
      expect(album.songs.count).to eq 1
      expect(album.distribution_api_songs.count).to eq 1
      expect(album.songs.first.creatives.count).to eq(0)
      expect(album.creatives.count).to eq(1)
    end
  end
end
