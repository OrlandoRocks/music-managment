require "rails_helper"

describe DistributionApi::Ingest::Purchase do
  let(:album) { create(:album, person: person) }

  context "with balance and targeted product" do
    let(:person) { create(:person, :with_balance, amount: 5_000) }
    let(:targeted_offer) do
      create(:targeted_offer, start_date: Time.now - 40.days,
                              expiration_date: Time.now + 10.days,
                              offer_type: "existing",
                              status: "New")
    end
    let!(:targeted_product) do
      create(:targeted_product, product: Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID),
                                targeted_offer: targeted_offer,
                                price_adjustment: 5.00,
                                price_adjustment_type: "dollars_off")
    end

    before do
      create(:salepoint, salepointable: album, finalized_at: DateTime.now)
      create(:artwork, album: album)
      create_list(:song, 2, album: album)
    end

    it "creates purchase and settled invoice with targeted product price" do
      create(:targeted_person, person: person, targeted_offer: targeted_offer)
      targeted_offer.update_attribute(:status, "Active")
      allow(person).to receive(:created_on).and_return(Date.today)

      described_class.create(person, album)
      album.reload
      purchase = album.purchases.first
      invoice = purchase.invoice
      salepoints = album.salepoints

      expect(purchase.discount_cents).to eq 500
      expect(purchase.non_plan_discount?).to be true
      expect(purchase.status).to eq 'processed'
      expect(purchase.targeted_product_id).to eq targeted_product.id
      expect(invoice.settled_at).not_to be nil
      expect(salepoints.all? { |sp| !!sp.finalized_at }).to be true
    end
  end

  context "with balance and without targeted product" do
    let(:person) { create(:person, :with_balance, amount: 5_000) }

    before do
      create(:salepoint, salepointable: album, finalized_at: DateTime.now)
      create(:artwork, album: album)
      create_list(:song, 2, album: album)
    end

    it "creates purchase and settled invoice with regular product price" do
      described_class.create(person, album)
      album.reload
      purchase = album.purchases.first
      invoice = purchase.invoice
      salepoints = album.salepoints

      expect(purchase.status).to eq 'processed'
      expect(purchase.targeted_product_id).to be nil
      expect(invoice.settled_at).not_to be nil
      expect(salepoints.all? { |sp| !!sp.finalized_at }).to be true
    end
  end

  context "without balance" do
    let(:person) { create(:person) }

    before do
      create(:salepoint, salepointable: album, finalized_at: DateTime.now)
      create(:artwork, album: album)
      create_list(:song, 2, album: album)
    end

    it "raises unable to purchase error" do
      expect { described_class.create(person, album) }.to raise_error(
        an_instance_of(DistributionApi::Errors::IngestionError)
      )
    end
  end
end
