require "rails_helper"

describe DistributionApi::Ingest::Salepoints do
  let(:album) { create(:album) }

  context "with valid stores" do
    let(:json_parser) { double(stores: Store.is_active.is_used.all) }

    it "creates paid salepoints" do
      described_class.ingest(json_parser, album)
      album.reload
      expect(album.salepoints.length).to equal(json_parser.stores.length)
    end
  end

  # Because of prior validations, this should never happen
  context "with no stores" do
    let(:json_parser) { double(stores: []) }

    it "raises empty salepoints error" do
      expect { described_class.ingest(json_parser, album) }.to raise_error(/Unable to add salepoints/)
    end
  end

  # Can only happen if store becomes inactive between initial validation and salepoint ingestion
  context "with inactive stores" do
    let(:json_parser) { double(stores: Store.not_active.all) }

    it "raises unable to add salepoints error" do
      expect { described_class.ingest(json_parser, album) }.to raise_error(/Unable to add stores/)
    end
  end

  # Can only happen if store becomes not-in-use between initial validation and salepoint ingestion
  context "with not-in-use stores" do
    let(:json_parser) { double(stores: Store.is_not_used.all) }

    it "raises unable to add salepoints error" do
      expect { described_class.ingest(json_parser, album) }.to raise_error(/Unable to add stores/)
    end
  end
end
