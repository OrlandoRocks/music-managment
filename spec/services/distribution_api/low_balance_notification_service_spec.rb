require "rails_helper"

describe DistributionApi::LowBalanceNotificationService do
  context "person has low balance" do
    let(:person) { create(:person) }
    let(:test_to) { "test_to@example.com" }

    before do
      person.person_balance.update(balance: 100)
    end

    it "creates notification email" do
      amount = '200' # via ENV

      stub_const("DistributionApi::LowBalanceNotificationService::PERSON_ID_VARS_MAP", {
        person.id => { amount: amount, to: test_to }
      })

      expect(LowBalanceMailer).to receive(:low_balance)
                                  .with({ amount: amount.to_i,
                                          person: person,
                                          to: test_to })
                                  .and_return(double("Mail::Message", deliver: double))

      described_class.run
    end

    it "delivers notification email" do
      amount = '200' # via ENV

      stub_const("DistributionApi::LowBalanceNotificationService::PERSON_ID_VARS_MAP", {
        person.id => { amount: amount, to: test_to }
      })

      expect_any_instance_of(Mail::Message).to receive(:deliver).exactly(1).time

      described_class.run
    end

    it "does NOT send notification email" do
      amount = '50'

      stub_const("DistributionApi::LowBalanceNotificationService::PERSON_ID_VARS_MAP", {
        person.id => { amount: amount, to: test_to }
      })

      expect(LowBalanceMailer).not_to receive(:low_balance)

      described_class.run
    end
  end

end
