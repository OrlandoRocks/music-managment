require 'rails_helper'

describe ::Scrapi::Job do
  let(:new_job) {
    <<-EOF
      {"status":{"code":200,"message":"OK","description":"","hostname":"blv-th3-apibk-6013.blvs.io","details":[]},"data":{"id":"createjobid", "uploadType":"aws_tunecore","uploadParams":"bigbox.tunecore.com:2508274/1561164431-song.mp3","status":"waiting_file"}}
    EOF
  }

  let(:new_job_with_song_id) {
    <<-EOF
      {"status":{"code":200,"message":"OK","description":"","hostname":"blv-th3-apibk-6013.blvs.io","details":[]},"data":{"id":"createjobid", "uploadType":"aws_tunecore","uploadParams":"bigbox.tunecore.com:2508274/1561164431-song.mp3","status":"waiting_file"},"song_id":123}
    EOF
  }

  let(:done_job) {
    <<-EOF
      {"data": {"status":"done",
     "id":"donejobid",
     "recognitionAnalysis":
      {"md5":"047956b0225f1a7c105a4d02abc7b418",
       "size":"8957346",
       "content":
        {"fileInfo":
          {
           "size":{"raw":"8957346", "dataOrigin":"ffprobe", "litteral":"8.54 MB"},
           "duration":{"raw":"50.777778", "estimated":false, "dataOrigin":"ffprobe", "litteral":"00:00:50.777778", "truncated":"00:00:50"},
           "format":{"raw":"wav", "litteral":"WAV / WAVE (Waveform Audio)", "dataOrigin":"ffprobe"}
           }}}}}
    EOF
  }

  let(:partial_done_job) {
    <<-EOF
      {"data": {"status":"done",
     "id":"donejobid",
     "recognitionAnalysis":
      {"md5":"047956b0225f1a7c105a4d02abc7b418",
       "size":"8957346",
       "content": "null"
        }}}
    EOF
  }

  describe ".from_content" do
    context "new job" do
      it "sets attributes from content" do
        job = Scrapi::Job.from_content(new_job)
        expect(job.id).to eq("createjobid")
        expect(job.song_id).to be_nil
        expect(job.status).to eq("waiting_file")
        [:size, :duration, :format].each do |sym|
          expect(job.send(sym)).to be_nil
        end
      end
    end
    context "done job" do
      it "sets the attributes from content" do
        job = Scrapi::Job.from_content(done_job)
        expect(job.id).to eq("donejobid")
        expect(job.song_id).to be_nil
        expect(job.status).to eq("done")

        expect(job.size).to eq(8957346)
        expect(job.duration).to eq(51)
        expect(job.format).to eq("wav")
      end
    end

    context "partial done job" do
      it "sets the attributes from content" do
        job = Scrapi::Job.from_content(partial_done_job)
        expect(job.id).to eq("donejobid")
        expect(job.song_id).to be_nil
        expect(job.status).to eq("done")

        expect(job.size).to eq(8957346)
        expect(job.duration).to eq(nil)
        expect(job.format).to eq(nil)
      end
    end
  end

  describe "#save" do
    it "transitions to the failed state" do
      expect(Tunecore::Airbrake).to receive(:notify)
      ScrapiJob.create(job_id: 'createjobid', status: 'waiting_file')
      job = Scrapi::Job.from_id("createjobid")
      job.content = JSON.parse(new_job).deep_merge("data"=>{"status"=>"error"}).to_json
      job.save
    end
  end

  describe ".from_id" do
    it "loads from job" do
      ScrapiJob.create(job_id: 'createjobid', song_id: '123', status: 'waiting')
      job = Scrapi::Job.from_id("createjobid")
      expect(job).to be_present
      expect(job.id).to eq("createjobid")
      expect(job.song_id).to eq(123)
      expect(job.status).to eq('waiting')
    end

    it "raises exception if job is not found" do
      expect { Scrapi::Job.from_id("createjobid") }.to raise_error(RuntimeError, "No job found with id createjobid")
    end

    it "sets waiting_file status with song_id" do
      ScrapiJob.create(job_id: 'createjobid', song_id: '123', status: 'waiting_file')
      job = Scrapi::Job.from_id('createjobid')
      expect(job.id).to eq("createjobid")
      expect(job.status).to eq("waiting_file")
      [:size, :duration, :format].each do |sym|
        expect(job.send(sym)).to be_nil
      end
    end
  end

  describe "#error!" do
    it "transitions scrapi job to failed state" do
      j = described_class.from_content(new_job)
      j.error!
    end
  end

  describe "#to_model" do

    before do
      ScrapiJob.destroy_all
      @successful_response = File.read(File.dirname(__FILE__) + "/successful_response.json")
      @expected_scrapi_job_detail = {
        "track_title" => "Let Me Go",
        "isrc" => "TCAEG1923642",
        "duration" => 224,
        "artists" => [ "XO" ],
        "genres" => [ "R&B/SOUL" ],
        "label" => "XO",
        "album_title" => "Let Me Go",
        "album_upc" => "859732596493",
        "score" => 100
      }
    end

    it "creates model when it does not exist in the database" do
      j = described_class.from_content(@successful_response)
      model = j.to_model
      expect(model.id).to be_present
      expect(model.scrapi_job_details.count).to eq(1)
      expect(model.scrapi_job_details.first.attributes).to include(@expected_scrapi_job_detail)
    end

    it "creates model when scrapi job exists in the database" do
      j = described_class.from_content(@successful_response)
      ScrapiJob.create!(job_id: j.id)
      model = j.to_model
      expect(model.id).to be_present
      expect(model.scrapi_job_details.count).to eq(1)
      expect(model.scrapi_job_details.first.attributes).to include(@expected_scrapi_job_detail)
    end
  end

end
