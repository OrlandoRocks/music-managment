require "rails_helper"

describe Spotify::ArtistIdExtractionService do
  let(:url_with_query)  { "https://open.spotify.com/artist/2eam0iDomRHGBypaDQLwWI?si=wahurN89RgaBW8EjzmDU8A&abc=123" }
  let(:url)             { "https://open.spotify.com/artist/2eam0iDomRHGBypaDQLwWI" }
  let(:app_uri)         { "spotify:artist:2eam0iDomRHGBypaDQLwWI" }
  let(:invalid_url)     { "https://open.spotify.com/artist/2eam0iDomRHGBypaDQLwWIsi=wahurN89RgaBW8EjzmDU8A&abc=123" }
  let(:invalid_app_uri) { "spotify:artist:" }

  describe ".extract_artist_id" do
    context "when given a valid url" do
      context "with a query string" do
        it "returns the spotify artist ID" do
          expect(Spotify::ArtistIdExtractionService.extract_artist_id(url_with_query)).to eq("2eam0iDomRHGBypaDQLwWI")
        end
      end

      context "without a query string" do
        it "returns the spotify artist ID" do
          expect(Spotify::ArtistIdExtractionService.extract_artist_id(url)).to eq("2eam0iDomRHGBypaDQLwWI")
        end
      end
    end

    context "when given an invalid url" do
      it "returns nil" do
        expect(Spotify::ArtistIdExtractionService.extract_artist_id(invalid_url)).to eq(nil)
      end
    end

    context "when given a uri" do
      context "when given a valid app uri" do
        it "should return the spotify artist ID" do
          expect(Spotify::ArtistIdExtractionService.extract_artist_id(app_uri)).to eq("2eam0iDomRHGBypaDQLwWI")
        end
      end

      context "when given an invalid app uri" do
        it "shoudl return nil" do
          expect(Spotify::ArtistIdExtractionService.extract_artist_id(invalid_app_uri)).to eq(nil)
        end
      end
    end
  end
end
