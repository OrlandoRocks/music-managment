require 'rails_helper'
describe Spotify::SpotifyForArtistsService do

  describe '#authenticate_spotify_user' do
    subject(:auth_redirect) { described_class.authenticate_spotify_user }
    let(:redirected_url) { "https://accounts.spotify.com/authorize?client_id"}

    it 'returns a url to direct the user to spotify accounts' do

      expect(subject).to start_with(redirected_url)
    end
  end

  context "After User grants TuneCore access to their profile" do

    before(:each) do
      allow_any_instance_of(Spotify::SpotifyForArtistsService).to receive(:artist_uri).and_return("artist_uri")
      allow_any_instance_of(Spotify::SpotifyForArtistsService).to receive(:album_uris).and_return(["spotify:album:album_uris"])
      allow_any_instance_of(Spotify::SpotifyForArtistsService).to receive(:artist_name).and_return("Lil Icee Drip")
    end

    describe '#request_spotify_for_artists_access' do
      let(:current_user) { Person.first }
      context "when onboarding to spotify for artists is successful" do
        let(:redirect_url) { 'https://artists.spotify.com/c/distributor/invite/artist_uri_to_redirect_to' }
        let(:params) { { "code" => "itsasecretshhhhhhh",
                          "controller"=>"spotify_for_artists",
                          "action"=>"redirect_access",
                          "current_user" => current_user } }

        subject(:access_request) { described_class.request_spotify_for_artists_access(params) }

        before do
          stub_request(:post, "https://creator.wg.spotify.com/s4a-onboarding/v0/external/request").to_return(status: 201,
            body: "{\"url\":\"https://artists.spotify.com/c/distributor/invite/artist_uri_to_redirect_to\"}")

          stub_request(:post, "https://accounts.spotify.com/api/token").to_return(status: 201, body: { 'access_token' => 'asecret' }.to_json )
        end

        it 'sends a request to on board a spotify for artists user and returns a url to artist page' do

          expect(subject).to eq(redirect_url)
        end
      end

      context 'when onboarding is not successful' do
        let(:error_url) { 'https://artists.spotify.com/some_error_endpoint' }
        let(:params) { { "code" => "itsasecretshhhhhhh",
                          "controller"=>"spotify_for_artists",
                          "action"=>"redirect_access",
                          "current_user" => Person.first }
                      }
        subject(:access_request) { described_class.request_spotify_for_artists_access(params) }

        before do
          stub_request(:post, "https://creator.wg.spotify.com/s4a-onboarding/v0/external/request").to_return(status: 201,
            body: "{\"url\":\"https://artists.spotify.com/some_error_endpoint\",\"message\":\"Error creating invite from album data\"}")

          stub_request(:post, "https://accounts.spotify.com/api/token").to_return(status: 400, body: { 'access_token' => 'asecret' }.to_json )
        end

        it 'sends a request to on board a spotify for artists user and returns a message and url to artist page' do

          expect(subject).to eq(error_url)
        end
      end
    end
  end
end
