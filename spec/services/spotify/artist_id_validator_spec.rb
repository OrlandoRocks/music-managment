require "rails_helper"

describe Spotify::ArtistIdValidator do
  before(:each) do
    @artist   = create(:artist, name: "Tracy Jordan")
    album     = create(:album)
    create(:creative, artist_id: @artist.id, creativeable_id: album.id, creativeable_type: 'Album')
  end

  let(:with_id) do
    {
      id: '123',
      name: "Tracy Jordan",
      spotify_name: nil
    }
  end

  let(:name_match) do
    {
      id: nil,
      name: "Tracy Jordan",
      spotify_name: "tracy jordan"
    }
  end

  let(:name_mismatch) do
    {
      id: nil,
      name: "Tracy Jordan",
      spotify_name: "r"
    }
  end

  describe ".valid?" do
    context "when provided name is a match" do
      it "returns true" do
        allow(Spotify::ApiClient).to receive(:new)
        expect(described_class.valid?(name_match)).to eq(true)
      end
    end

    context "when provided name is not a match" do
      it "returns false" do
        allow(Spotify::ApiClient).to receive(:new)
        expect(described_class.valid?(name_mismatch)).to eq(false)
      end
    end

    context "when id points to a match" do
      it "returns true" do
        allow(Spotify::ApiClient).to receive_message_chain(:new, :get_artist_by_id)
                                     .and_return(double(body: { 'name' => 'tracy jordan' }.to_json,
                                                        success?: true))
        expect(described_class.valid?(with_id)).to eq(true)
      end
    end

    context "when id points to a mismatch" do
      it "returns false" do
        allow(Spotify::ApiClient).to receive_message_chain(:new, :get_artist_by_id)
                                     .and_return(double(body: { 'name' => 't' }.to_json,
                                                        success?: true))
        expect(described_class.valid?(with_id)).to eq(false)
      end
    end
  end
end
