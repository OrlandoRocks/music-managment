require "rails_helper"

describe Renderforest::Apple::PurchaseGenerator do
  let(:purchase_date)        { Date.today.to_s }
  let(:person)               { create(:person, :with_tc_social) }
  let(:product)              { FactoryBot.create(:product, :rf_video_download)}
  let(:receipt_data)         { "abcd1234xyz" }
  let(:purchase_token) {"abcdefghjijljljl"}
  let(:product_id) { "com.tunecore.social.rf2.video_download" }
  describe "#generate" do
    context "success" do
      context "when the associated add_on_purchase already exists" do
        before do
          @add_on_purchase_params = {
            product_id: product.id,
            payment_channel: "Apple",
            person: person,
            payment_date: purchase_date,
            receipt_data: receipt_data
          }
          @add_on_purchase = AddOnPurchase.create(@add_on_purchase_params)
          @generator =  Renderforest::Apple::PurchaseGenerator.generate(product, person, purchase_date, receipt_data)
        end
        it "does not creates the add_on_purchase" do
          expect(AddOnPurchase.where(@add_on_purchase_params).size).to eq(1)
          expect(@generator.add_on_purchase.id).to eq(@add_on_purchase.id)
        end
      end

      context "when the associated add_on_purchase does not exist" do
        before(:each) do
          @generator =  Renderforest::Apple::PurchaseGenerator.generate(product, person, purchase_date, receipt_data)
        end
        it "creates the add_on_purchase" do
          expect(@generator.add_on_purchase).to_not eq nil
        end
      end
    end
    context "failure" do
      context "add_on purchase could not be created" do
        it "adds the appropriate error" do
          allow(AddOnPurchase).to receive(:create!).and_return(nil)
          generator = Renderforest::Apple::PurchaseGenerator.generate(product, person, purchase_date, receipt_data)
          expect(generator.errors.messages[:base]).to include('api.tc.message.unable_to_create_add_on_purchase')
        end
      end
    end

  end
end
