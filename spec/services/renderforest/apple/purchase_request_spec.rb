require "rails_helper"

describe Subscription::Apple::PurchaseRequest do
  let(:purchase_date) {Date.today.strftime("%Y-%m-%d %H:%M:%S")}
  let(:product) { FactoryBot.create(:product, :rf_video_download)}
  let(:params) do
   {
     purchase_date: purchase_date,
     receipt_data: "abcdefghijk",
   }
  end

  describe "#new" do
    it "sets attrs" do
      person             = create(:person, :with_tc_social)
      params[:person]    = person
      params[:product]   = product
      purchase_handler   = Renderforest::Apple::PurchaseRequest.new(params)

      purchase_handler.finalize

      expect(purchase_handler.purchase_id).to_not eq nil
      expect(purchase_handler.paid_at).to eq(purchase_date)
    end
  end

  describe "#finalize" do
    context "success" do
      before(:each) do
        @person                = create(:person, :with_tc_social)
        params[:person]        = @person
        params[:product]   = product
        @purchase_handler      = Renderforest::Apple::PurchaseRequest.new(params)
      end

      it "is true create add_on_purchase for person" do
        expect(@purchase_handler.finalize).to eq true
        add_on_purchase = @purchase_handler.add_on_purchase
        expect(add_on_purchase).to_not eq nil
      end
    end

    context "failure" do
      it "is false and doesn't create add_on_purchase" do
        person = create(:person, :with_tc_social)
        params[:person] = person
        params[:product] = product
        allow(AddOnPurchase).to receive(:create!).and_return(nil)
        purchase_handler = Renderforest::Apple::PurchaseRequest.new(params)
        expect(purchase_handler.finalize).to eq false
        expect(purchase_handler.errors).not_to be_empty
      end
    end
  end
end
