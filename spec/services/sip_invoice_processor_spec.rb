require "rails_helper"

describe "sip invoice generation" do
  context 'sip outbound invoice' do
    # TODO outbound invoice generation logic will be changed as part fo GS-8525
    # it "should create outbound invoice" do
    #   person = create(:person)
    #   posting_id = 1
    #   create_intake_records(person, posting_id)
    #   expect(OutboundInvoice.exists?).to eq false
    #   invoice_processor = SipInvoiceProcessor.new(posting_id)
    #   invoice_processor.process!
    #   expect(OutboundInvoice.exists?).to eq true
    # end
  end
end

def create_intake_records(person, posting_id)
  sip_store = create(:sip_store)
  sales_record_master = create(:sales_record_master, sip_store: sip_store, posting_id: posting_id)
  person_intake = create(:person_intake, person: person)
  create(:person_intake_sales_record_master, person_intake: person_intake, sales_record_master: sales_record_master)
end
