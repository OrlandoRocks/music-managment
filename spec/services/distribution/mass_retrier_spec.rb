require "rails_helper"

describe Distribution::MassRetrier do
  describe "#mass_retry" do
    let(:person)       { create(:person, :with_live_distributions) }
    let(:album)        { person.albums.first }
    let(:store_id)     { album.salepoints.first.store_id }
    let(:distribution) { album.salepoints.first.distributions.first }
    let(:remote_ip)    { "127.0.0.1" }
    let(:distro_retry)      { double("distro_retry") }
    let(:distro_retry_form) { double("distro_retry_form") }
    let(:params) do
      { person_id:         person.id,
        album_ids_or_upcs: [album.id.to_s],
        store_ids:         [store_id.to_i],
        metadata_only:     metadata_only,
        remote_ip:         remote_ip }.with_indifferent_access
    end

    let(:sns) { Aws::SNS::Resource.new(client: Aws::SNS::Client.new(stub_responses: true)) }

    before(:each) do
      allow(Aws::SNS::Resource).to receive(:new).and_return(sns)
    end

    context "when there are no albums to be retried" do
      let(:metadata_only) { "on" }

      it "should not call Delivery::Retry or DistributionRetryForm" do
        allow(BulkAlbumFinder).to receive_message_chain(:new, :execute).and_return([])
        expect(Delivery::Retry).not_to receive(:new)
        expect(DistributionRetryForm).not_to receive(:new)
        Distribution::MassRetrier.new(params).mass_retry
      end
    end

    context "when there are albums to be retried" do
      let(:distro_retry_params) do
        { delivery:      distribution,
          priority:      0,
          person:        person,
          delivery_type: delivery_type,
          mass_retry:     1 }
      end
      let(:distro_retry_form_params) do
        { retriable_distributions: [distro_retry],
          ip_address:              remote_ip,
          album:                   album }
      end

      before do
        allow(distro_retry_form).to receive(:save)
      end

      context "when the delivery type is 'metadata_only'" do
        let(:metadata_only) { "on" }
        let(:delivery_type) { "metadata_only" }

        it "should retry the delivery with a delivery type of 'metadata_only'" do
          expect(Delivery::Retry).to receive(:new).with(distro_retry_params)          { distro_retry }
          expect(DistributionRetryForm).to receive(:new).with(distro_retry_form_params) { distro_retry_form }
          Distribution::MassRetrier.new(params).mass_retry
        end
      end

      context "when the delivery type is not 'metadata_only" do
        let(:metadata_only) { nil }
        let(:delivery_type) { "full_delivery" }

        it "should retry the delivery with a delivery type of 'full_delivery'" do
          expect(Delivery::Retry).to receive(:new).with(distro_retry_params)          { distro_retry }
          expect(DistributionRetryForm).to receive(:new).with(distro_retry_form_params) { distro_retry_form }
          Distribution::MassRetrier.new(params).mass_retry
        end
      end
    end

    context "when the albums to be retried include blocked salepoints" do
      let(:metadata_only) { nil }
      let(:delivery_type) { "full_delivery" }
      let(:blocked)       { create(:distribution, :with_salepoint, :store_short_name => "Spotify" ) }

      before :each do
        blocked.update_attribute(:state, "blocked")
        album.salepoints.first.distributions << blocked
      end

      it "should not deliver to the blocked store" do
        Distribution::MassRetrier.new(params).mass_retry

        expect(blocked.state).to eq("blocked")
        expect(Transition.where(state_machine_id: blocked.id).count).to eq(0)
      end

      it "does not raise an error when a retry is attempted on a blocked distribution" do
        expect{
          Distribution::MassRetrier.new(params).mass_retry
          }.not_to raise_error
      end
    end
  end
end

