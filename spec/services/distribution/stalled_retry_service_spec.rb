require "rails_helper"

describe Distribution::StalledRetryService do
  describe ".retry" do
    let(:distro_record) { create(:distribution) }
    before do
      expect(Distribution::StalledRetryService).to receive(:execute_query).and_return([distro_record])
    end

    it "distributes the queried distros via the worker" do
      expect(distro_record).to receive(:start).with(actor: "Distribution::StalledRetryService")
      Distribution::StalledRetryService.retry
    end
  end
end
