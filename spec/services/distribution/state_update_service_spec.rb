require "rails_helper"

describe Distribution::StateUpdateService do
  describe ".update" do

    let(:params) {
      { state: "dequeued", message: "Test Message", backtrace: "Test Backtrace", isrcs: nil }
    }

    it "updates state of distribution if it has changed" do
      distribution = create(:distribution, state: "enqueued")
      Distribution::StateUpdateService.update(distribution, params)
      expect(distribution.state).to eq "dequeued"
    end

    it "doesn't update state if params[:state] same as current state" do
      distribution = create(:distribution, state: "enqueued")
      distribution.update(state: "dequeued")
      updated_at = distribution.updated_at
      Distribution::StateUpdateService.update(distribution, params)
      expect(distribution.state).to eq "dequeued"
      expect(distribution.updated_at).to eq updated_at
    end

    it "creates a transition" do
      distribution = create(:distribution, :enqueued)
      Distribution::StateUpdateService.update(distribution, params)
      expect(distribution.transitions.last.message).to eq params[:message]
      expect(distribution.transitions.last.backtrace).to eq params[:backtrace]
    end

    it "updates distribution_song(s) if any isrcs are passed in" do
      distribution   = create(:distribution, :enqueued, :with_distribution_songs)
      params[:isrcs] = [distribution.distribution_songs.last.salepoint_song.song.isrc.to_s]
      Distribution::StateUpdateService.update(distribution, params)
      expect(distribution.distribution_songs.last.state).to eq "dequeued"
    end

    it "doesnt set distribution to delivered if only one distribution_song is delivered" do
      distribution   = create(:distribution, :with_distribution_songs)
      params[:isrcs] = [distribution.distribution_songs.last.salepoint_song.song.isrc.to_s]
      params[:state] = "delivered"
      Distribution::StateUpdateService.update(distribution, params)
      expect(distribution.state).to eq "new"
    end

    it "sets distribution to delivered if all distribution_songs is delivered" do
      distribution   = create(:distribution, :enqueued, :with_distribution_songs)
      params[:isrcs] = [distribution.distribution_songs.first.salepoint_song.song.isrc.to_s]
      params[:isrcs] += [distribution.distribution_songs.last.salepoint_song.song.isrc.to_s]
      params[:state] = "delivered"
      Distribution::StateUpdateService.update(distribution, params)
      expect(distribution.state).to eq "delivered"
    end

    it "creates transition for distribution song" do
      distribution = create(:distribution, :enqueued, :with_distribution_songs)
      params[:isrcs] = [distribution.distribution_songs.last.salepoint_song.song.isrc.to_s]
      Distribution::StateUpdateService.update(distribution, params)
      expect(distribution.distribution_songs.first.transitions).to be_empty
      expect(distribution.distribution_songs.last.transitions.last.message).to eq params[:message]
      expect(distribution.distribution_songs.last.transitions.last.backtrace).to eq params[:backtrace]
    end

    it "checks other distribution_songs and updates distribution state if it has changed" do
      params = { state: "delivered", message: "delivered, bud" }
      distribution = create(:distribution, :enqueued, :with_distribution_songs)
      dsong_1 = distribution.distribution_songs.first
      dsong_2 = distribution.distribution_songs.last
      Distribution::StateUpdateService.update(dsong_1, params)
      expect(Transition.last.state_machine_id).to eq dsong_1.id
      expect(distribution.state).to eq "enqueued"
      expect {
        Distribution::StateUpdateService.update(dsong_2, params)
      }.to change{
        (distribution.reload.state)
      }.from("enqueued").to("delivered")
    end
  end
end
