require "rails_helper"

describe Distribution::BatchRetrier do
  let(:batch_params) do
    { person_id: "2",
      store_ids: "2,3,4",
      metadata_only: "",
      daily_retry_limit: "3",
      remote_ip: "127.0.0.1",
      current_index: "3",
      uuid: "919b5cd5-3dea-4eaa-af7a-ab02d1b85927",
      batch_key: "distribution:batch-retry:919b5cd5-3dea-4eaa-af7a-ab02d1b85927" }
  end

  describe "#initialize" do
    it "should call super and set instance variables declared in MassRetrier#initialize" do
      batch_retrier = Distribution::BatchRetrier.new(batch_params)
      expect(batch_retrier.person_id).to     eq "2"
      expect(batch_retrier.remote_ip).to     eq "127.0.0.1"
      expect(batch_retrier.metadata_only).to eq ""
    end

    it "should properly set instance variables" do
      batch_retrier = Distribution::BatchRetrier.new(batch_params)
      expect(batch_retrier.uuid).to              eq "919b5cd5-3dea-4eaa-af7a-ab02d1b85927"
      expect(batch_retrier.store_ids).to         eq ["2", "3", "4"]
      expect(batch_retrier.current_index).to     eq 3
      expect(batch_retrier.daily_retry_limit).to eq 3
      expect(batch_retrier.batch_key).to         eq "distribution:batch-retry:919b5cd5-3dea-4eaa-af7a-ab02d1b85927"
    end
  end

  describe "#retry_batch" do
    let(:batch_albums_key)  { "distribution:batch-retry-albums:#{uuid}" }
    let(:uuid)              { batch_params[:uuid] }
    let(:daily_retry_limit) { batch_params[:daily_retry_limit] }
    let(:batch_retrier)     { Distribution::BatchRetrier.new(batch_params) }
    let(:batch_key)         { batch_params[:batch_key] }

    before do
      $redis.flushdb
    end

    context "when there no album_ids_or_upcs" do
      let(:expected_redis_key) { "distribution:batch-retry-complete:#{uuid}" }

      it "should rename the batch redis key" do
        expect($redis).to receive(:rename).with(batch_key, expected_redis_key)
        batch_retrier.retry_batch
      end
    end

    context "when there are album_ids_or_upcs" do
      let(:album_ids_or_upcs) { ["1", "2", "3", "4", "5", "6"] }

      before do
        $redis.rpush(batch_albums_key, album_ids_or_upcs)
      end

      context "when there enough ids to fulfill the daily_retry_limit" do
        it "should max out the batch size" do
          expect(batch_retrier.album_ids_or_upcs).to eq ["4", "5", "6"]
          batch_retrier.retry_batch
        end
      end

      context "when there not enough ids to fulfill the daily_retry_limit" do
        let(:album_ids_or_upcs) { ["1", "2", "3", "4", "5"] }

        it "should return the remaining album_ids_or_upcs" do
          expect(batch_retrier.album_ids_or_upcs).to eq ["4", "5"]
          batch_retrier.retry_batch
        end
      end

      it "should increment the current_index key in redis" do
        expect($redis).to receive(:hincrby).with(batch_key, "current_index", batch_retrier.daily_retry_limit)
        batch_retrier.retry_batch
      end
    end
  end
end
