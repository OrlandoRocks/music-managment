# frozen_string_literal: true
require 'rails_helper'

describe Distribution::TcDistributorBackfillService do

        let(:release_api_response) { 
            
                [OpenStruct.new(:http_body => {
                    'object' => 'releases',
                    'total' => '3467',
                    'offset' => '0',
                    'releases' => [
                        { 
                            'object' => 'release',
                            'id' => '1000',
                            'version' => '2',
                            'state' => 'enqueued',
                            'json_url' => 'https://s3.amazonaws.com/859736171160/1.json',
                            'created_at' => 'Sun, 22 Mar 2020 18:04:52 UTC +00:00',
                            'updated_at' => 'Sun, 22 Mar 2020 18:04:52 UTC +00:00',
                            'album' => {
                                'id' => '1234567',
                                'upc' => '859736171184'
                            }
                        },
                        { 
                            'object' => 'release',
                            'id' => '1001',
                            'version' => '2',
                            'state' => 'enqueued',
                            'json_url' => 'https://s3.amazonaws.com/859736171160/1.json',
                            'created_at' => 'Sun, 22 Mar 2020 18:04:52 UTC +00:00',
                            'updated_at' => 'Sun, 22 Mar 2020 18:04:52 UTC +00:00',
                            'album' => {
                                'id' => '1234568',
                                'upc' => '859736171184'
                            }
                        }]
                    }
                )]
            
        }

        let(:releases_stores_api_response) { 

            [OpenStruct.new(:http_body =>  {
                'object' => 'releases_stores',
                'total' => '3467',
                'offset' => '0',
                'releases_stores' => [
                    { 
                        'object' => 'releases_store',
                        'id' => '1123',
                        'release_id' => '1000',
                        'store_id' => '27',
                        'delivery_type' => 'full_delivery',
                        'state' => 'delivered',
                        'xml_url' => 'https://s3.amazonaws.com/859736171160/1.xml',
                        'created_at' => 'Sun, 22 Mar 2020 18:04:52 UTC +00:00',
                        'updated_at' => 'Sun, 22 Mar 2020 18:04:52 UTC +00:00'
                    },
                    { 
                        'object' => 'releases_store',
                        'id' => '1234',
                        'release_id' => '1001',
                        'store_id' => '27',
                        'delivery_type' => 'full_delivery',
                        'state' => 'delivered',
                        'xml_url' => 'https://s3.amazonaws.com/859736171160/1.xml',
                        'created_at' => 'Sun, 22 Mar 2020 18:04:52 UTC +00:00',
                        'updated_at' => 'Sun, 22 Mar 2020 18:04:52 UTC +00:00'
                    },
                    { 
                        'object' => 'releases_store',
                        'id' => '1234',
                        'release_id' => '1001',
                        'store_id' => '28',
                        'delivery_type' => 'full_delivery',
                        'state' => 'delivered',
                        'xml_url' => 'https://s3.amazonaws.com/859736171160/1.xml',
                        'created_at' => 'Sun, 22 Mar 2020 18:04:52 UTC +00:00',
                        'updated_at' => 'Sun, 22 Mar 2020 18:04:52 UTC +00:00'
                    }
                 ]
                }
             )]
        }

        before do 
            allow(DistributorAPI::Release).to receive(:list).and_return(release_api_response)
            allow(DistributorAPI::ReleasesStore).to receive(:list).and_return(releases_stores_api_response)
        end 

        
    describe '#generate_backfill_data_by_date' do 
        
        let(:expected_formatted_data) {
            [["1234567", "27"], ["1234568", "27,28"]]
          }

        it "generates report containing albums and store ids for given dates" do 
            report = described_class.new({file_name: "test.csv", start_date: "2020-01-01", end_date: "2020-02-01", report_type: :date_range}).generate_backfill_data_by_date
            expect(report).to eq(expected_formatted_data)
        end 
    end 
    
    describe "#create_backfill_report" do 

        subject(:report) { described_class.new({file_name: "test.csv", start_date: "2020-01-01", end_date: "2020-02-01", report_type: :date_range}) } 

        before do 
            allow(RakeDatafileService).to receive(:upload).and_return(true)
        end 
        it "calls method specific to report input" do 
          expect_any_instance_of(Distribution::TcDistributorBackfillService).to receive(:generate_backfill_data_by_date).and_call_original
          subject.create_backfill_report
        end 

        it "calls the RakeDatafileService" do 
            expect(RakeDatafileService).to receive(:upload).once
            subject.create_backfill_report
        end 
    end 
end 