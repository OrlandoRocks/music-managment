require 'rails_helper'

describe Paypal::RefundFailedTransactionService do
  let!(:paypal_transaction) { create(:paypal_transaction) }
  let!(:credentials) { paypal_transaction.person.country_website.paypal_checkout_credentials }
  let!(:mock_paypal_response) { double(
    PayPalSDKCallers::Transaction,
    success: true,
    response: "Success"
  )}

  it "should refund payment if transaction was successful" do
    paypal_transaction.update(ack: "Success", action: "Sale", status: "Completed", amount: 0.51e1)
    refund_reason = "Rollback after error processing charge"
    allow(FeatureFlipper).to receive(:show_feature?).with(:refund_failed_paypal_transactions, paypal_transaction.person).and_return(true)
    allow(PayPalAPI).to receive(:refund).and_return(mock_paypal_response)

    expect(PaypalTransaction).to receive(:record_refunded_transaction).with(mock_paypal_response, paypal_transaction)

    Paypal::RefundFailedTransactionService.refund_if_charged_and_failed(paypal_transaction)
  end

  it "should not refund a declined transaction" do
    paypal_transaction.update(action: "Sale", status: "Failed", amount: 0.51e1)
    refund_reason = "Rollback after error processing charge"
    allow(FeatureFlipper).to receive(:show_feature?).with(:refund_failed_paypal_transactions, paypal_transaction.person).and_return(true)

    expect(PayPalAPI).not_to receive(:refund)
    Paypal::RefundFailedTransactionService.refund_if_charged_and_failed(paypal_transaction)
  end

  it "should not refund transaction if feature flipper is off" do
    paypal_transaction.update(action: "Sale", status: "Completed", amount: 0.51e1)
    refund_reason = "Rollback after error processing charge"
    allow(FeatureFlipper).to receive(:show_feature?).with(:refund_failed_paypal_transactions, paypal_transaction.person).and_return(false)

    expect(PayPalAPI).not_to receive(:refund)
    Paypal::RefundFailedTransactionService.refund_if_charged_and_failed(paypal_transaction)
  end
end
