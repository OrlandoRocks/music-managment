require "rails_helper"

describe ArtistNameTitleizerService do
  describe ".artist_names" do
    it "returns the artist name for one artist" do
      artist_names = ArtistNameTitleizerService.artist_names(["Chance the Rapper"])
      expect(artist_names).to eq("Chance the Rapper")
    end

    it "returns the artist name for two artists" do
      artist_names = ArtistNameTitleizerService.artist_names(["Chance the Rapper", "Saba"])
      expect(artist_names).to eq("Chance the Rapper & Saba")
    end

    it "returns the artist name for multiple artists" do
      artist_names = ArtistNameTitleizerService.artist_names(["Chance the Rapper", "Lil Wayne", "2 Chainz"])
      expect(artist_names).to eq("Chance the Rapper, Lil Wayne, & 2 Chainz")
    end
  end
end
