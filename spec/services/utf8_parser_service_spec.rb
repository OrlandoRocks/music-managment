require "rails_helper"

describe 'Utf8ParserService' do
  describe "#four_byte_chars" do
    context "when a mixture of 3 and 4 byte chars" do
      subject(:parser) { Utf8ParserService.new(str, 'SomeClass') }

      let(:three_byte_chars) { '✂✅✈✉✊✋✌✏✒✔✖✨✳✴❄❇❌❎❓❔❕❗❤➕➖➗➡➰Ⓜ‼⁉™'}
      let(:four_byte_chars) { '🚤🚥🚧🚨🚩🚪🚫🚬🚭🚲🚶🚹🚺🚻🚼🚽🚾🛀🅰🅱🅾🅿🆎🆑🆒🆓🆔🆕🆖🆗🆘🆙🆚🈁🈂🈚🈯🈲🈳🈴🈵🈶🈷🈸🈹🈺🉐🉑🀄🃏🌀🌁🌂🌃🌄🌅🌆🌇🌈🌉🌊🌋🌌🌏🌑🌓' }
      let(:str) { three_byte_chars + four_byte_chars }

      it "returns only the four-byte chars" do
        expected = four_byte_chars.split('')
        expect(parser.four_byte_chars).to eq(expected)
      end
    end

    context "when a mixture of 3 and 4 byte chars and other 4 byte chars" do
      subject(:parser) { Utf8ParserService.new(str, 'SomeClass') }

      let(:three_byte_chars) { '✂✅✈✉✊✋✌✏✒✔✖✨✳✴❄❇❌❎❓❔❕❗❤➕➖➗➡➰Ⓜ‼⁉™'}
      let(:four_byte_chars) { '🚤🚥🚧🚨🚩🚪🚫🚬🚭🚲🚶🚹🚺🚻🚼🚽🚾🛀🅰🅱🅾🅿🆎🆑🆒🆓🆔🆕🆖🆗🆘🆙🆚🈁🈂🈚🈯🈲🈳🈴🈵🈶🈷🈸🈹🈺🉐🉑🀄🃏🌀🌁🌂🌃🌄🌅🌆🌇🌈🌉🌊🌋🌌🌏🌑🌓' }
      let(:three_byte_chinese) { '你好好好' }
      let(:four_byte_chinese) { '𠜎𠜱𠝹𠱓𠱸𠲖𠳏𠳕𠴕𠵼𠵿𠸎𠸏𠹷𠺝𠺢𠻗𠻹𠻺𠼭𠼮𠽌𠾴𠾼𠿪𡁜𡁯𡁵𡁶𡁻𡃁𡃉𡇙𢃇𢞵𢫕𢭃𢯊𢱑𢱕𢳂𢴈𢵌𢵧𢺳𣲷𤓓𤶸𤷪𥄫𦉘𦟌𦧲𦧺𧨾𨅝𨈇𨋢𨳊𨳍𨳒𩶘' }
      let(:str) { three_byte_chars + four_byte_chars + three_byte_chinese + four_byte_chinese }

      it "returns only the four-byte chars" do
        allow(Airbrake).to receive(:notify)
        expected = four_byte_chars.split('') + four_byte_chinese.split('')
        expect(Airbrake).to receive(:notify).once
        expect(parser.four_byte_chars).to eq(expected)
      end
    end

    context "when an empty string" do
      subject(:parser) { Utf8ParserService.new(str, 'SomeClass') }

      let(:str) { '' }

      it "returns an empty array" do
        expected = []
        expect(parser.four_byte_chars).to eq(expected)
      end
    end

    context "when there are no 4 byte chars" do
      subject(:parser) { Utf8ParserService.new(str, 'SomeClass') }

      let(:three_byte_chars) { '✂✅✈✉✊✋✌✏✒✔✖✨✳✴❄❇❌❎❓❔❕❗❤➕➖➗➡➰Ⓜ‼⁉™'}
      let(:three_byte_chinese) { '你好好好' }
      let(:str) { three_byte_chars + three_byte_chinese }

      it "returns an empty array" do
        expected = []
        expect(parser.four_byte_chars).to eq(expected)
      end
    end
  end

  describe "#non_emoji_four_byte_chars" do
    context "when a mixture of 3 and 4 byte chars and other 4 byte chars" do
      subject(:parser) { Utf8ParserService.new(str, 'SomeClass') }

      let(:three_byte_chars) { '✂✅✈✉✊✋✌✏✒✔✖✨✳✴❄❇❌❎❓❔❕❗❤➕➖➗➡➰Ⓜ‼⁉™'}
      let(:four_byte_chars) { '🚤🚥🚧🚨🚩🚪🚫🚬🚭🚲🚶🚹🚺🚻🚼🚽🚾🛀🅰🅱🅾🅿🆎🆑🆒🆓🆔🆕🆖🆗🆘🆙🆚🈁🈂🈚🈯🈲🈳🈴🈵🈶🈷🈸🈹🈺🉐🉑🀄🃏🌀🌁🌂🌃🌄🌅🌆🌇🌈🌉🌊🌋🌌🌏🌑🌓' }
      let(:three_byte_chinese) { '你好好好' }
      let(:four_byte_chinese) { '𠜎𠜱𠝹𠱓𠱸𠲖𠳏𠳕𠴕𠵼𠵿𠸎𠸏𠹷𠺝𠺢𠻗𠻹𠻺𠼭𠼮𠽌𠾴𠾼𠿪𡁜𡁯𡁵𡁶𡁻𡃁𡃉𡇙𢃇𢞵𢫕𢭃𢯊𢱑𢱕𢳂𢴈𢵌𢵧𢺳𣲷𤓓𤶸𤷪𥄫𦉘𦟌𦧲𦧺𧨾𨅝𨈇𨋢𨳊𨳍𨳒𩶘' }
      let(:str) { three_byte_chars + four_byte_chars + three_byte_chinese + four_byte_chinese }

      it "returns only the non-chars four-byte chars" do
        allow(Airbrake).to receive(:notify)
        expected = four_byte_chinese.split('')
        expect(Airbrake).to receive(:notify).with(Utf8ParserService::NonEmojiFourByteCharError, {:class_name=>"SomeClass", :non_emoji_four_byte_chars=>four_byte_chinese.split('')}).once
        expect(parser.non_emoji_four_byte_chars).to eq(expected)
      end
    end

    context "when no non-chars 4 byte chars" do
      subject(:parser) { Utf8ParserService.new(str, 'SomeClass') }

      let(:three_byte_chars) { '✂✅✈✉✊✋✌✏✒✔✖✨✳✴❄❇❌❎❓❔❕❗❤➕➖➗➡➰Ⓜ‼⁉™'}
      let(:four_byte_chars) { '🚤🚥🚧🚨🚩🚪🚫🚬🚭🚲🚶🚹🚺🚻🚼🚽🚾🛀🅰🅱🅾🅿🆎🆑🆒🆓🆔🆕🆖🆗🆘🆙🆚🈁🈂🈚🈯🈲🈳🈴🈵🈶🈷🈸🈹🈺🉐🉑🀄🃏🌀🌁🌂🌃🌄🌅🌆🌇🌈🌉🌊🌋🌌🌏🌑🌓' }
      let(:three_byte_chinese) { '你好好好' }
      let(:str) { three_byte_chars + four_byte_chars + three_byte_chinese }

      it "returns an empty array" do
        expected = []
        expect(parser.non_emoji_four_byte_chars).to eq(expected)
      end
    end

    context "when an empty string" do
      subject(:parser) { Utf8ParserService.new(str, 'SomeClass') }

      let(:str) { '' }

      it "returns an empty array" do
        expected = []
        expect(parser.non_emoji_four_byte_chars).to eq(expected)
      end
    end
  end
end
