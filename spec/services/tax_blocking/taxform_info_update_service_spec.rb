require "rails_helper"

describe TaxBlocking::TaxformInfoUpdateService do
  before(:all) do
    TaxForm.delete_all
  end

  before(:each) do
    PersonEarningsTaxMetadatum.delete_all
  end

  let!(:person) { create(:person) } 
  let!(:tax_forms) do
    date_ranges = [[2018, 2020], [2015, 2017]]
    date_ranges.map do |yrange|
      create(
        :tax_form,
        person: person,
        provider: PayoutProvider::PAYONEER,
        submitted_at: DateTime.new(yrange[0]).beginning_of_year,
        expires_at: DateTime.new(yrange[1]).end_of_year
      )
    end
  end

  describe "#save" do
    context "when a range of years is passed into .new" do
      it "creates person_earnings_tax_metadatum records for every tax_year" do
        TaxBlocking::TaxformInfoUpdateService.new(person.id, 2018..2019).summarize
        expect(person.person_earnings_tax_metadata.pluck(:tax_year)).to match_array([2018, 2019])
      end

      it "attaches valid tax forms for each year" do
        TaxBlocking::TaxformInfoUpdateService.new(person.id, 2018..2019).summarize
        person.person_earnings_tax_metadata.where(tax_year: 2018..2019).includes(:dist_taxform).map do |p_tax_data|
          tax_year_range = DateTime.new(p_tax_data.tax_year).beginning_of_year..DateTime.new(p_tax_data.tax_year).end_of_year
          expect(p_tax_data.dist_taxform.submitted_at).to be < tax_year_range.max
          expect(p_tax_data.dist_taxform.expires_at.to_datetime).to be >= tax_year_range.max
        end
      end

      it "does not populate data for years with no tax form" do
        TaxBlocking::TaxformInfoUpdateService.new(person.id, 2018..2019).summarize
        tax_metadata = person.person_earnings_tax_metadata.includes(:dist_taxform).find_by(tax_year: 2021)
        expect(tax_metadata).to be_nil
      end
    end

    context "when a single year is passed into .new" do
      it "attaches valid tax form for year" do
        TaxBlocking::TaxformInfoUpdateService.new(person.id, 2016).summarize
        tax_metadata = person.person_earnings_tax_metadata.includes(:dist_taxform).find_by(tax_year: 2016)
        tax_year_range = DateTime.new(tax_metadata.tax_year).beginning_of_year..DateTime.new(tax_metadata.tax_year).end_of_year
        expect(tax_metadata.dist_taxform.submitted_at).to be < tax_year_range.max
        expect(tax_metadata.dist_taxform.expires_at).to be >= tax_year_range.max
      end
    end

    it "does not populate data for years with no tax form" do
      TaxBlocking::TaxformInfoUpdateService.new(person.id, 2020).summarize
      tax_metadata = person.person_earnings_tax_metadata.includes(:dist_taxform).find_by(tax_year: 2021)
      expect(tax_metadata).to be_nil
    end
  end
end
