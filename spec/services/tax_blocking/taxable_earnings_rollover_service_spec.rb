require "rails_helper"
Sidekiq::Testing.fake!

describe TaxBlocking::TaxableEarningsRolloverService, type: :service do

  include ActiveSupport::Testing::TimeHelpers

  before do
    PersonTransaction.delete_all
  end


  let(:tax_year) { 2021 }
  let(:march_22) { "#{tax_year}-03-22".to_datetime }
  let(:april_22) { "#{tax_year}-04-22".to_datetime }
  let(:dec_15) { "#{tax_year}-12-15".to_datetime }
  let(:previous_dec_15) { "#{tax_year - 1}-12-15".to_datetime }

  let(:person_only_pub_earnings) do
    person = create(:person, :with_balance, amount: 100)
    ba1 = create(:balance_adjustment, person: person, category: "Songwriter Royalty", posted_by: person, posted_by_name: person.name)
    ba2 = create(:balance_adjustment, person: person, category: "Songwriter Royalty", posted_by: person, posted_by_name: person.name)

    create(:person_earnings_tax_metadatum, tax_blocked: true, tax_year: tax_year, person: person, total_publishing_earnings: 100.00)
    create(:person_transaction, target: ba1, person: person, credit: 50, created_at: march_22)
    create(:person_transaction, target: ba2, person: person, credit: 50, created_at: dec_15)

    person
  end

  let(:person_with_all_earning_types) do
    person = create(:person, :with_balance, amount: 100)
    ba1 = create(:balance_adjustment, person: person, category: "Songwriter Royalty", posted_by: person, posted_by_name: person.name)
    ba2 = create(:balance_adjustment, person: person, category: "Songwriter Royalty", posted_by: person, posted_by_name: person.name)

    create(:person_earnings_tax_metadatum, tax_blocked: true, tax_year: tax_year, person: person, total_publishing_earnings: 100.00, total_distribution_earnings: 100.00)
    create(:person_transaction, target_type: "PersonIntake", person: person, credit: 50, created_at: previous_dec_15)
    create(:person_transaction, target: ba1, person: person, credit: 50, created_at: march_22)
    create(:person_transaction, target_type: "PersonIntake", person: person, credit: 50, created_at: april_22)
    create(:person_transaction, target: ba2, person: person, credit: 50, created_at: dec_15)
    create(:person_transaction, target_type: "PersonIntake", person: person, credit: 50, created_at: dec_15)
    person
  end


  describe "#call" do

    it "is idempotent" do
      travel_to(Time.parse("2022-01-01")) do
        first_pub_only  = TaxBlocking::TaxableEarningsRolloverService.new(person_only_pub_earnings.id, tax_year).call
        second_pub_only = TaxBlocking::TaxableEarningsRolloverService.new(person_only_pub_earnings.id, tax_year).call
        third_pub_only  = TaxBlocking::TaxableEarningsRolloverService.new(person_only_pub_earnings.id, tax_year).call

        first_all_types = TaxBlocking::TaxableEarningsRolloverService.new(person_with_all_earning_types.id, tax_year).call
        second_all_types = TaxBlocking::TaxableEarningsRolloverService.new(person_with_all_earning_types.id, tax_year).call
        third_all_types  = TaxBlocking::TaxableEarningsRolloverService.new(person_with_all_earning_types.id, tax_year).call
        expect([
            first_pub_only,
            second_pub_only,
            third_pub_only,
            first_all_types,
            second_all_types,
            third_all_types
            ]).to eq([
              :ok,
              :earnings_rolledover,
              :earnings_rolledover,
              :ok,
              :earnings_rolledover,
              :earnings_rolledover
              ])
      end
    end

    describe "only publishing earnings" do

      it "creates two new person transactions of the TaxableEarningsRollover type" do
        travel_to(Time.parse("2022-01-01")) do
          TaxBlocking::TaxableEarningsRolloverService.new(person_only_pub_earnings.id, tax_year).call
          count = PersonTransaction.where(target_type: "TaxableEarningsRollover", person: person_only_pub_earnings).count
          expect(count).to eq(2)
        end
      end

      it "creates a TaxableEarningsRollover person transactions where the credit and the debit eq 0" do
        travel_to(Time.parse("2022-01-01")) do
          TaxBlocking::TaxableEarningsRolloverService.new(person_only_pub_earnings.id, tax_year).call
          debit_pt = PersonTransaction.find_by(target_type: "TaxableEarningsRollover", person: person_only_pub_earnings, credit: 0.0)
          credit_pt = PersonTransaction.find_by(target_type: "TaxableEarningsRollover", person: person_only_pub_earnings, debit: 0.0)

          expect((-1 * debit_pt.debit) + credit_pt.credit).to eq(0.0)
        end
      end

      it "updates TaxableEarningsRollover tax_blocked_at to #{TaxBlocking::TaxableEarningsRolloverService::ADV_TAX_BLOCKING_DATE.to_datetime.utc}" do
        travel_to(Time.parse("2022-01-01")) do
          TaxBlocking::TaxableEarningsRolloverService.new(person_only_pub_earnings.id, tax_year).call
          taxable_earning = TaxableEarningsRollover.find_by(person: person_only_pub_earnings, tax_year: tax_year)
          expect(taxable_earning.tax_blocked_at.utc).to eq(TaxBlocking::TaxableEarningsRolloverService::ADV_TAX_BLOCKING_DATE.to_datetime.utc)
        end
      end

      it "rolls over the correct rollover amount only" do
        travel_to(Time.parse("2022-01-01")) do
          TaxBlocking::TaxableEarningsRolloverService.new(person_only_pub_earnings.id, tax_year).call
          taxable_earning = TaxableEarningsRollover.find_by(person: person_only_pub_earnings, tax_year: tax_year)

          expect(Float(taxable_earning.rollover_earnings)).to be_within(0.0001).of(50.0)
        end
      end

      it "should not change balance" do
        travel_to(Time.parse("2022-01-01")) do
          balance  = person_only_pub_earnings.balance
          TaxBlocking::TaxableEarningsRolloverService.new(person_only_pub_earnings.id, tax_year).call
          after_bal = person_only_pub_earnings.reload.balance

          expect(balance).to eq(after_bal)
        end
      end

      it "created debit PersonTransactions of the TaxableEarningsRollover type in tax year" do
        travel_to(Time.parse("2022-01-01")) do
          TaxBlocking::TaxableEarningsRolloverService.new(person_only_pub_earnings.id, tax_year).call
          debit_pt = PersonTransaction.find_by(target_type: "TaxableEarningsRollover", person: person_only_pub_earnings, credit: 0.0)
          expect(debit_pt.created_at.year).to eq(tax_year)
        end
      end

      it "created credit PersonTransactions of the TaxableEarningsRollover type year after tax year" do
        travel_to(Time.parse("2022-01-01")) do
          TaxBlocking::TaxableEarningsRolloverService.new(person_only_pub_earnings.id, tax_year).call
          credit_pt = PersonTransaction.find_by(target_type: "TaxableEarningsRollover", person: person_only_pub_earnings, debit: 0.0)
          expect(credit_pt.created_at.year).to eq(tax_year + 1)
        end
      end
    end

    describe "with all earning types" do
      it "creates two new person transactions of the TaxableEarningsRollover type" do
        travel_to(Time.parse("2022-01-01")) do
          TaxBlocking::TaxableEarningsRolloverService.new(person_with_all_earning_types.id, tax_year).call
          count = PersonTransaction.where(target_type: "TaxableEarningsRollover", person: person_with_all_earning_types).count
          expect(count).to eq(2)
        end
      end

      it "creates a TaxableEarningsRollover person transactions where the credit and the debit eq 0" do
        travel_to(Time.parse("2022-01-01")) do
          TaxBlocking::TaxableEarningsRolloverService.new(person_with_all_earning_types.id, tax_year).call
          debit_pt = PersonTransaction.find_by(target_type: "TaxableEarningsRollover", person: person_with_all_earning_types, credit: 0.0)
          credit_pt = PersonTransaction.find_by(target_type: "TaxableEarningsRollover", person: person_with_all_earning_types, debit: 0.0)

          expect((-1 * debit_pt.debit) + credit_pt.credit).to eq(0.0)
        end
      end

      it "updates TaxableEarningsRollover tax_blocked_at to date of first #{("2021-04-22".to_datetime - 1.second).to_datetime}" do
        travel_to(Time.parse("2022-01-01")) do
          TaxBlocking::TaxableEarningsRolloverService.new(person_with_all_earning_types.id, tax_year).call
          taxable_earning = TaxableEarningsRollover.find_by(person: person_with_all_earning_types, tax_year: tax_year)
          expect(taxable_earning.tax_blocked_at.utc).to eq((april_22 - 1.second).to_datetime)
        end
      end

      it "rolls over the correct rollover amount only" do
        travel_to(Time.parse("2022-01-01")) do
          TaxBlocking::TaxableEarningsRolloverService.new(person_with_all_earning_types.id, tax_year).call
          taxable_earning = TaxableEarningsRollover.find_by(person: person_with_all_earning_types, tax_year: tax_year)

          expect(Float(taxable_earning.rollover_earnings)).to be_within(0.0001).of(150.0)
        end
      end

      it "should not change balance" do
        travel_to(Time.parse("2022-01-01")) do
          balance  = person_with_all_earning_types.balance
          TaxBlocking::TaxableEarningsRolloverService.new(person_with_all_earning_types.id, tax_year).call
          after_bal = person_with_all_earning_types.reload.balance

          expect(balance).to eq(after_bal)
        end
      end

      it "created debit PersonTransactions of the TaxableEarningsRollover type in tax year" do
        travel_to(Time.parse("2022-01-01")) do
          TaxBlocking::TaxableEarningsRolloverService.new(person_with_all_earning_types.id, tax_year).call
          debit_pt = PersonTransaction.find_by(target_type: "TaxableEarningsRollover", person: person_with_all_earning_types, credit: 0.0)
          expect(debit_pt.created_at.year).to eq(tax_year)
        end
      end

      it "created credit PersonTransactions of the TaxableEarningsRollover type year after tax year" do
        travel_to(Time.parse("2022-01-01")) do
          TaxBlocking::TaxableEarningsRolloverService.new(person_with_all_earning_types.id, tax_year).call
          credit_pt = PersonTransaction.find_by(target_type: "TaxableEarningsRollover", person: person_with_all_earning_types, debit: 0.0)
          expect(credit_pt.created_at.year).to eq(tax_year + 1)
        end
      end
    end
  end
end