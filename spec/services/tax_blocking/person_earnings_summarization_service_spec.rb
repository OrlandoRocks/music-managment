require "rails_helper"

describe TaxBlocking::PersonEarningsSummarizationService, type: :service do
  before do
    PersonTransaction.delete_all
  end

  let!(:person) { create(:person, :with_balance, amount: 100) }

  let!(:balance_adjustment1) {
    create(
      :balance_adjustment, person: person, category: "Songwriter Royalty", credit_amount: "12.45",
                           posted_by: person, posted_by_name: person.name
    )
  }
  let!(:balance_adjustment2) {
    create(
      :balance_adjustment, person: person, category: "Songwriter Royalty", credit_amount: "25.70",
                           posted_by: person, posted_by_name: person.name
    )
  }
  let!(:balance_adjustment3) {
    create(
      :balance_adjustment, person: person, category: "Songwriter Royalty", credit_amount: "85.25",
                           posted_by: person, posted_by_name: person.name
    )
  }
  let!(:balance_adjustment4) {
    create(
      :balance_adjustment, person: person, category: "Songwriter Royalty", credit_amount: "1.45",
                           posted_by: person, posted_by_name: person.name
    )
  }
  let!(:publishing_transaction2015) do
    transaction = PersonTransaction.find_by(target_id: balance_adjustment4.id)
    transaction.update(created_at: DateTime.strptime("2015-04-01", "%Y-%m-%d"))
    transaction.reload
  end
  let!(:publishing_transaction2018) do
    transaction = PersonTransaction.find_by(target_id: balance_adjustment1.id)
    transaction.update(created_at: DateTime.strptime("2018-04-01", "%Y-%m-%d"))
    transaction.reload
  end
  let!(:current_year_publishing_transaction1) {
    PersonTransaction.find_by(target_id: balance_adjustment2.id)
  }
  let!(:current_year_publishing_transaction2) {
    PersonTransaction.find_by(target_id: balance_adjustment3.id)
  }

  let!(:distribution_transaction2015) {
    PersonTransaction.create(
      person: person, credit: 10.00, previous_balance: person.balance, target_type: "YouTubeRoyaltyIntake",
      created_at: DateTime.strptime("2015-04-01", "%Y-%m-%d")
    )
  }
  let!(:distribution_transaction2018) {
    PersonTransaction.create(
      person: person, credit: 1.50, previous_balance: person.balance, target_type: "YouTubeRoyaltyIntake",
      created_at: DateTime.strptime("2018-04-01", "%Y-%m-%d")
    )
  }
  let!(:current_year_distribution_transaction1) {
    PersonTransaction.create(
      person: person, credit: 12.15, previous_balance: person.balance, target_type: "PersonIntake"
    )
  }
  let!(:current_year_distribution_transaction2) {
    PersonTransaction.create(
      person: person, credit: 24.60, previous_balance: person.balance, target_type: "PersonIntake"
    )
  }

  describe "#summarize" do
    describe "publishing earnings" do
      it "updates total publishing earnings of a given person for the current year by default" do
        described_class.new(person.id).summarize
        expect(PersonEarningsTaxMetadatum.find_by(person_id: person.id).total_publishing_earnings)
          .to eq([current_year_publishing_transaction1, current_year_publishing_transaction2].sum(&:credit))
      end

      it "updates total publishing earnings of a given person for a given year" do
        ["2018", 2018].each do |year|
          described_class.new(person.id, year).summarize
          expect(
            PersonEarningsTaxMetadatum
              .where(person_id: person.id, tax_year: 2018).sum(:total_publishing_earnings)
          ).to eq(publishing_transaction2018.credit)
        end
      end

      it "updates total publishing earnings of a given person for a list of years" do
        described_class.new(person.id, %w[2015 2016 2017 2018]).summarize
        expect(
          PersonEarningsTaxMetadatum
            .where(person_id: person.id, tax_year: 2015..2018).sum(:total_publishing_earnings)
        ).to eq([publishing_transaction2015, publishing_transaction2018].sum(&:credit))
      end
    end

    describe "distribution earnings" do
      it "updates total distribution earnings of a given person for the current year by default" do
        described_class.new(person.id).summarize
        expect(PersonEarningsTaxMetadatum.find_by(person_id: person.id).total_distribution_earnings)
          .to eq([current_year_distribution_transaction1, current_year_distribution_transaction2].sum(&:credit))
      end

      it "updates total distribution earnings of a given person for a given year" do
        ["2015", 2015].each do |year|
          described_class.new(person.id, year).summarize
          expect(PersonEarningsTaxMetadatum.find_by(person_id: person.id, tax_year: 2015).total_distribution_earnings)
            .to eq(distribution_transaction2015.credit)
        end
      end

      it "updates total distribution earnings of a given person for a list of years" do
        described_class.new(person.id, %w[2015 2016 2017 2018]).summarize
        expect(
          PersonEarningsTaxMetadatum.where(person_id: person.id, tax_year: 2015..2018).sum(:total_distribution_earnings)
        ).to eq([distribution_transaction2015, distribution_transaction2018].sum(&:credit))
      end
    end
  end
end
