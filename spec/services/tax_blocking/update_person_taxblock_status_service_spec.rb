require "rails_helper"

describe TaxBlocking::UpdatePersonTaxblockStatusService, type: :service do
  it_behaves_like 'ruleable'

  let!(:user) { create(:person) }
  subject(:service) do
    TaxBlocking::UpdatePersonTaxblockStatusService.new(user)
  end

  before do
    PersonEarningsTaxMetadatum.delete_all
    allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking).and_return(true)
  end

  let!(:non_user_user) do
    person = create(:person, :with_india_country)
    create(:person_earnings_tax_metadatum, :tax_blocked, person: person)
    person.reload
  end

  # This person has no taxform but has crossed the
  # max earning threshold for being tax blocked
  let!(:person1) do
    person = create(:person, :with_balance, amount: 10)
    create(:person_earnings_tax_metadatum, person: person, total_distribution_earnings: 100.00)
    person.reload
  end

  # person with taxform crossed max earnings threshold
  let!(:person2) do
    person = create(:person, :with_balance, amount: 10)
    create(:person_earnings_tax_metadatum, :with_dist_taxform, person: person, total_distribution_earnings: 100.00)
    person.reload
  end

  # this person has no tax form in previous years
  # and has crossed earning threshold in one of 
  # the previous years, but not the current year
  let!(:person3) do
    person = create(:person, :with_balance, amount: 2) 
    create(:person_earnings_tax_metadatum, person: person, total_publishing_earnings: 1.00)
    create(:person_earnings_tax_metadatum, :for_past_year, person: person, total_publishing_earnings: 200.00)
    person.reload
  end

  # The person has had a tax form for all assessable previous years 
  # and has crossed threshold in previous years
  # but no taxform in the current year but threshold not crossed in current year
  let!(:person4) do
    person = create(:person, :with_balance, amount: 2) 
    create(:person_earnings_tax_metadatum, person: person, total_publishing_earnings: 1.00)
    create(:person_earnings_tax_metadatum, :for_past_year, :with_dist_taxform, person: person, total_publishing_earnings: 200.0)
    person.reload
  end

  let!(:person5) do
    person = create(:person, :with_balance, amount: 2)
    create(:person_earnings_tax_metadatum, person: person, total_publishing_earnings: 2.00)
    person.reload
  end

  describe ":current_year_earnings_threshold rule" do
    it "fails if the current year earnings have exceeded the threshold" do
      evaluation = service.evaluate_rule(name: :current_year_earnings_threshold, category: :tax_blocking, against: person1)
      expect(evaluation).to be(:fail)
    end
    
    it "passes if the current year earnings have not exceeded the threshold" do
      evaluation = service.evaluate_rule(name: :current_year_earnings_threshold, category: :tax_blocking, against: person5)
      expect(evaluation).to be(:ok)
    end
  end

  describe ":past_years_earnings_threshold rule" do
    it "passes if the person has had taxforms for all assessable previous years" do
      evaluation = service.evaluate_rule(name: :past_years_earnings_threshold, category: :tax_blocking, against: person4)
      expect(evaluation).to be(:ok)
    end

    it "fails if the person had no taxform for any previous assessable years and crossed thresholds in previous years but not in current year" do
      evaluation = service.evaluate_rule(name: :past_years_earnings_threshold, category: :tax_blocking, against: person3)
      expect(evaluation).to be(:fail)
    end
  end

  describe "#eligible_to_withdraw?" do
    it "returns true for non us users regardless of tax_blocked status" do
      withdraw_eligibilty = TaxBlocking::UpdatePersonTaxblockStatusService.new(non_user_user).eligible_to_withdraw?
      expect(withdraw_eligibilty).to be(true)
    end

    it "returns true if person has submitted tax form for current year" do
      withdraw_eligibilty = TaxBlocking::UpdatePersonTaxblockStatusService.new(person2).eligible_to_withdraw?
      expect(withdraw_eligibilty).to be(true)
    end
  end

  describe ".call" do
    it "updates the tax-blocking status for all users" do
      allow(person1).to receive(:from_united_states_and_territories?).and_return(true)
      expect_any_instance_of(TaxBlocking::UpdatePersonTaxblockStatusService).to receive(:update_tax_blocking_status!)
      TaxBlocking::UpdatePersonTaxblockStatusService.call(person1)
    end

    context "when the person's taxform is submitted" do
      it "updates the tax-blocking status" do
        allow(person1).to receive(:from_united_states_and_territories?).and_return(true)
        service_double = instance_double(
          TaxBlocking::UpdatePersonTaxblockStatusService,
          not_us_user?: false,
          tax_form_submitted?: true,
          update_tax_blocking_status!: nil
        )
        allow(TaxBlocking::UpdatePersonTaxblockStatusService).to receive(:new).and_return(service_double)
        expect(service_double).to receive(:update_tax_blocking_status!)
        TaxBlocking::UpdatePersonTaxblockStatusService.call(person1)
      end
    end

    context "when the person's taxform is not submitted" do
      it "updates the tax-blocking status" do
        allow(person1).to receive(:from_united_states_and_territories?).and_return(true)
        service_double = instance_double(
          TaxBlocking::UpdatePersonTaxblockStatusService,
          not_us_user?: false,
          tax_form_submitted?: false,
          update_tax_blocking_status!: nil
        )
        allow(TaxBlocking::UpdatePersonTaxblockStatusService).to receive(:new).and_return(service_double)
        expect(service_double).to receive(:update_tax_blocking_status!)
        TaxBlocking::UpdatePersonTaxblockStatusService.call(person1)
      end
    end
  end
end
