require "rails_helper"

describe RenewalsBatchFailureService do
  it "logs a failure" do
    expect do
      RenewalsBatchFailureService.log(Date.today.to_s)
    end.to change(RenewalsBatchFailure, :count).by(1)
  end

  it "creates 1 failure per date" do
    expect do
      RenewalsBatchFailureService.log(Date.today.to_s)
      RenewalsBatchFailureService.log(Date.today.to_s)
    end.to change(RenewalsBatchFailure, :count).by(1)
  end

  it "resolves failures" do
    date = Date.today.to_s
    RenewalsBatchFailureService.log(date)
    RenewalsBatchFailureService.resolve(date)

    expect(RenewalsBatchFailure.find_by(date: date).resolved).to be true
  end
end
