require "rails_helper"

describe ArtworkSuggestion::CoverSuggestionService do
  describe "#suggest_for_genre" do
    it "returns a cover" do
      album = create(:album)
      genre = Genre.find(album.primary_genre_id)
      service = ArtworkSuggestion::CoverSuggestionService.new(genre, album)

      cover = service.suggest_for_genre

      expect(cover.class).to eq Cover
    end
  end
end
