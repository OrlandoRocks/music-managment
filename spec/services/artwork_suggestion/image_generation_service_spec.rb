require "rails_helper"

describe ArtworkSuggestion::ImageGenerationService do
  let(:image_fixture)  { Rails.root.join("spec", "fixtures", "album_artwork.tiff") }
  let(:output_fixture) { "album_artwork_output.tiff" }
  let(:texture)        { File.join(Rails.root, 'public', 'effect-assets', "/antique_02.jpg") }

  after(:each) do
    File.delete(output_fixture) if File.exists?(output_fixture)
  end

  xit ".identify returns settings hash" do
    expect(ArtworkSuggestion::ImageGenerationService).to receive(:exec).with("identify test.png").and_return("test.png PNG 640x480 DirectClass 87kb 0.050u 0:01")
    settings = { filename: "test.png",
      format: "PNG",
      width: 640,
      height: 480,
      class: "DirectClass",
      filesize: "87kb" }
    expect(ArtworkSuggestion::ImageGenerationService.identify("test.png")).to eq settings
  end

  describe ".copy" do
    it "makes a copy of the given file" do
      result = ArtworkSuggestion::ImageGenerationService.copy(image_fixture, output_fixture)
      expect(result).to eq(nil)
      expect(File.exists?(output_fixture)).to be true
    end
  end

  describe ".convert" do
    context "when antique" do
      it "converts the source into the target image with options" do
        result = ArtworkSuggestion::ImageGenerationService.convert(source: image_fixture, target: output_fixture, opts: ["-sepia-tone", "85%"])
        expect(result).to eq("")
        expect(File.exists?(output_fixture)).to be true
      end
    end
    context "when blur" do
      it "converts the source into the target image with options" do
        result = ArtworkSuggestion::ImageGenerationService.convert(source: image_fixture, target: output_fixture, opts: ["-blur", "5,5"])
          expect(result).to eq("")
          expect(File.exists?(output_fixture)).to be true
      end
    end
  end

  describe ".composite" do
    xit "converts the source into the target image with options" do
      ArtworkSuggestion::ImageGenerationService.convert(source: image_fixture, target: output_fixture, opts: ["-sepia-tone", "85%"])
      result = ArtworkSuggestion::ImageGenerationService.composite(output_fixture, texture, "overlay", "center")
      expect(result).to eq(nil)
      expect(File.exists?(output_fixture)).to be true
    end
  end
end
