require 'rails_helper'

describe PersonSubscriptionStatusService do
  describe "#disable_access" do
    let(:person) { create(:person, :with_social_subscription_status_and_event_and_purchase) }
    let(:other_person) { create(:person) }
    let(:admin) { create(:person, :with_role, role_name: "Subscription Admin")}

    it "changes termination_date and grace_period_length for a valid, canceled subscription status" do
      subscription_status = person.person_subscription_statuses.first
      subscription_status.cancel
      subscription_status_service = PersonSubscriptionStatusService.disable_access(subscription_status, admin)
      subscription_status.reload
      expect(subscription_status.termination_date).to be_within(1.day).of subscription_status.canceled_at
      expect(subscription_status.grace_period_length).to be(0)
    end

    it "does not change termination_date or grace_period from an admin with bad credentials and returns an error" do
      subscription_status = person.person_subscription_statuses.first
      subscription_status.cancel
      subscription_status_service = PersonSubscriptionStatusService.disable_access(subscription_status, other_person)
      subscription_status.reload
      expect(subscription_status.termination_date).to_not be_within(1.day).of subscription_status.canceled_at
      expect(subscription_status.grace_period_length).to_not be(0)
      expect(subscription_status.errors.full_messages[0]).to eq("Admin has inappropriate role")
    end

    it "does not change termination_date or grace_period from an uncanceled subscription status and returns an error" do
      subscription_status = person.person_subscription_statuses.first
      subscription_status_service = PersonSubscriptionStatusService.disable_access(subscription_status, admin)
      subscription_status.reload
      expect(subscription_status.termination_date).to_not be_within(1.day).of Time.current
      expect(subscription_status.grace_period_length).to_not be(0)
      expect(subscription_status.errors.full_messages[0]).to eq("Disable access status cannot be updated")
    end

    it "creates a SubscriptionEvent and Note detailing social access is disabled" do
      subscription_status = person.person_subscription_statuses.first
      subscription_status.cancel
      subscription_status_service = PersonSubscriptionStatusService.disable_access(subscription_status, admin)
      subscription_event = SubscriptionEvent.where(person_subscription_status_id: subscription_status.id).last
      expect(subscription_event.event_type).to eq("Access Disabled")
      note = Note.find_by(related_id: person.id, subject: "Subscriptions")
      expect(note.note).to eq("TC Social Access Disabled")
    end
  end
end
