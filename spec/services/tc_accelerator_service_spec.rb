require "rails_helper"

describe TcAcceleratorService do
  let(:person) { create(:person) }
  describe "::opt!" do
    context "when opting in" do
      context "when user is not already opted in" do
        before(:each) do
          allow(person).to receive(:tc_accelerator_opted_in?).and_return(false)
        end
        it "unflags the user" do
          expect(Person::FlagService).to receive(:unflag!)
          TcAcceleratorService.opt!(person, :in)
        end
      end
      context "when user is already opted in" do
        before(:each) do
          allow(person).to receive(:tc_accelerator_opted_in?).and_return(true)
        end
        it "does nothing" do
          expect(Person::FlagService).not_to receive(:unflag!)
          TcAcceleratorService.opt!(person, :in)
        end
      end
    end
    context "when opting out" do
      context "when user is not already opted out" do
        before(:each) do
          allow(person).to receive(:tc_accelerator_opted_out?).and_return(false)
        end
        it "unflags the user" do
          expect(Person::FlagService).to receive(:flag!)
          TcAcceleratorService.opt!(person, :out)
        end
      end
      context "when user is already opted out" do
        before(:each) do
          allow(person).to receive(:tc_accelerator_opted_out?).and_return(true)
        end
        it "does nothing" do
          expect(Person::FlagService).not_to receive(:flag!)
          TcAcceleratorService.opt!(person, :out)
        end
      end
    end
  end
  describe "::do_not_notify!" do
    context "when user selects Do Not Notify" do
      context "and has not already selected" do
        before(:each) do
          allow(person).to receive(:tc_accelerator_do_not_notify?).and_return(false)
        end
        it "unflags the user" do
          expect(Person::FlagService).to receive(:flag!)
          TcAcceleratorService.do_not_notify!(person, nil)
        end
      end
      context "and has already selected" do
        before(:each) do
          allow(person).to receive(:tc_accelerator_do_not_notify?).and_return(true)
        end
        it "does nothing" do
          expect(Person::FlagService).not_to receive(:flag!)
          TcAcceleratorService.do_not_notify!(person, nil)
        end
      end
    end
  end
end
