require "rails_helper"

describe Plans::DowngradeRequestCreationService do
    subject { Plans::DowngradeRequestCreationService.call(
        person_id: person&.id,
        reason_id: reason&.id,
        status: status,
        requested_plan_id: plan&.id,
        downgrade_other_reason: downgrade_other_reason
    )}

    let!(:person) { create(:person, :with_person_plan)}
    let!(:reason) { DowngradeCategoryReason.first }
    let!(:plan) { Plan.first }
    let!(:status) { nil }
    let!(:downgrade_other_reason) { nil }

    context "invalid inputs" do
        
        context "no person" do
            let!(:person) { nil }
            it "defers error handling to PlanDowngradeRequest model validations" do
                expect(PlanDowngradeRequest).to receive(:new).and_call_original
                expect(subject.save).to be(false)
            end
        end

        context "no reason" do
            let!(:reason) { nil }
            it "defers error handling to PlanDowngradeRequest model validations" do
                expect(PlanDowngradeRequest).to receive(:new).and_call_original
                expect(subject.save).to be(false)
            end
        end

    end

    context "valid inputs" do
        before do
            [
                PlanDowngradeRequest::CANCELED_STATUS,
                PlanDowngradeRequest::PROCESSED_STATUS,
                PlanDowngradeRequest::ACTIVE_STATUS
            ].each {|stat| PlanDowngradeRequest.create(
                person_id: person&.id,
                status: stat,
                reason_id: reason&.id,
                requested_plan_id: plan&.id
            )}
        end

        context "transactions" do
            context "an error is raised" do
                it "rolls back all changes" do
                    # contrived error condition
                    expect_any_instance_of(PlanDowngradeRequest).to receive(:save).and_raise("Error")
                    expect{ subject }.to raise_error
                    expect(person.plan_downgrade_requests.length).to eq(3)
                    expect(person.plan_downgrade_requests.active.length).to eq(1)
                end
            end
    
            context "successful call - no error is raised" do
                it "commits all changes" do
                    expect_any_instance_of(PlanDowngradeRequest).to receive(:save).and_call_original
                    subject
                    expect(person.plan_downgrade_requests.length).to eq(4)
                end
            end
        end

        context "with nil status parameter" do
            it "creates new plan_downgrade_request with 'Active' status" do
                result = subject
                expect(result.status).to eq(PlanDowngradeRequest::ACTIVE_STATUS)
                expect(result.errors.empty?).to be(true)
            end

            it "cancels older plan_downgrade_requests for that user" do
                expect(person.plan_downgrade_requests.active.length).to eq(1)
                old_active_request = person.plan_downgrade_requests.active.first
                result = subject
                expect(person.plan_downgrade_requests.active.length).to eq(1)
                expect(person.plan_downgrade_requests.active.first).to eq(result)
                expect(result).to_not eq(old_active_request)
            end
        end

        context "with 'Active' status parameter" do
            let!(:status) { PlanDowngradeRequest::ACTIVE_STATUS }
            it "creates new plan_downgrade_request with 'Active' status" do
                result = subject
                expect(result.status).to eq(PlanDowngradeRequest::ACTIVE_STATUS)
                expect(result.errors.empty?).to be(true)
            end

            it "cancels older plan_downgrade_requests for that user" do
                expect(person.plan_downgrade_requests.active.length).to eq(1)
                old_active_request = person.plan_downgrade_requests.active.first
                result = subject
                expect(person.plan_downgrade_requests.active.length).to eq(1)
                expect(person.plan_downgrade_requests.active.first).to eq(result)
                expect(result).to_not eq(old_active_request)
            end
        end

        context "with valid non-'Active' status parameter" do
            let!(:status) { PlanDowngradeRequest::CANCELED_STATUS }
            it "creates new plan_downgrade_request with specified valid status" do
                result = subject
                expect(result.status).to eq(PlanDowngradeRequest::CANCELED_STATUS)
                expect(result.errors.empty?).to be(true)
            end

            it "does not cancel older plan_downgrade_requests for that user" do
                expect(person.plan_downgrade_requests.active.length).to eq(1)
                old_active_request = person.plan_downgrade_requests.active.first
                result = subject
                expect(person.plan_downgrade_requests.active.length).to eq(1)
                expect(person.plan_downgrade_requests.active.first).to_not eq(result)
                expect(person.plan_downgrade_requests.active.first).to eq(old_active_request)
            end
        end

        context "With valid reason_id provided (non-'Other')" do
            let!(:downgrade_other_reason) { "Some not-so compelling reason" }
            it "Does not create a new downgrade_other_reason from provided text" do
                result = subject
                expect(result.reason).to eq(reason)
                expect(result.downgrade_other_reason).to be(nil)
                expect(DowngradeOtherReason.all.count).to eq(0)
            end
        end

        context "With 'Other' reason" do
            let!(:reason) { DowngradeCategoryReason.find_by(name: "Other") }
            let!(:downgrade_other_reason) {"Some very compelling reason"}
            it "creates a new downgrade_other_reason from the provided text" do
                result = subject
                expect(result.reason).to eq(reason)
                expect(result.downgrade_other_reason).to_not be(nil)
                expect(result.downgrade_other_reason.description).to eq(downgrade_other_reason)
                expect(DowngradeOtherReason.all.count).to eq(1)
            end
        end
    end
end
