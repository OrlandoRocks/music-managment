require "rails_helper"

describe Plans::DowngradeService do
  subject { described_class.call(person, target_plan, options) }

  let!(:person) {create(:person)}
  let!(:existing_person_plan) {
    create(
        :person_plan,
        plan_id: Plan.third.id,
        person: person
    ) if person.present? && person.instance_of?(Person)
  }
  let!(:target_plan) {Plan.first}
  let!(:purchase) {
    create(:purchase,
        person: person,
        product: Product.find(500),
        related: Product.find(500)
    )}
  let!(:options) { {purchase: purchase} }

  context "invalid inputs" do

    context "no user provided" do
        let!(:person) { nil }
        let!(:purchase) {create(:purchase)}
        it "raises error" do
            expect { subject }.to raise_error(Plans::DowngradeService::NoUserError)
        end
    end

    context "invalid user value" do
        let(:person) { "some other class"}
        let!(:purchase){ create(:purchase)}
        it "raises error" do
            expect { subject }.to raise_error(Plans::DowngradeService::NoUserError)
        end
    end

    context "user is not a plan user" do
        let(:existing_person_plan) { nil }
        it "raises error" do
            expect { subject }.to raise_error(Plans::DowngradeService::NotPlanUserError)
        end
    end

    context "no target plan provided" do
        let(:target_plan) { nil }
        it "uses default plan?" do
            expect { subject }.to raise_error(Plans::DowngradeService::NoPlanError)
        end
    end

    context "invalid plan value" do
        let(:target_plan) { ["not a plan", 0] }
        it "raises error" do
            expect { subject }.to raise_error(Plans::DowngradeService::NoPlanError)
        end
    end

    context "target plan is not a downgrade" do
        let!(:target_plan) {Plan.fourth}
        it "raises error" do
            expect { subject }.to raise_error(Plans::DowngradeService::NotDowngradeError)
        end
    end

    context "no purchase provided" do
        let!(:purchase){ nil }
        it "service does not raise error" do
            allow(PersonPlanCreationService).to receive(:call)
            expect { subject }.to_not raise_error
        end
    end
  end

  context "valid inputs" do
    discovery_platform_store = Store.find_by(is_active:true, discovery_platform:true)
    let!(:album){create(:album, :paid, :live, person: person)}
    # ensure there is at least one salepoint for free and active store
    # for integration testing purposes
    let!(:free_salepoint){ create(:salepoint, store: discovery_platform_store, album: album )}
    context "plan user without live releases" do
        let(:album){create(:album, person: person)}
        it "downgrades plan only" do
            expect(Plans::AdditionalArtistsTakedownService).to_not receive(:downgrade_takedown!)
            expect(Plans::StoreAutomatorTakedownService).to_not receive(:call)
            expect(Plans::DspTakedownService).to_not receive(:call)
            expect(PersonPlanCreationService).to receive(:call)
            subject
        end

    end

    context "plan user with additional artists" do
        let!(:additional_artist){create(:plan_addon, :additional_artist_addon, :paid, person: person)}

        context "no note_opts provided" do
            let!(:options){{purchase: purchase, note_opts:nil}}
            it "passes the nil value to the AdditionalArtistsTakedownService? to be handled there" do
                expect(Plans::AdditionalArtistsTakedownService).to receive(:downgrade_takedown!)
                subject
            end
        end

        context "moving to another plan that allows additional artists" do
            it "does not call Plans::AdditionalArtistsTakedownService" do
                allow(Plans::CanDoService).to receive(:can_do?)
                allow(Plans::CanDoService).to receive(:can_do?).with(target_plan, :buy_additional_artists).and_return(true)
                expect(Plans::AdditionalArtistsTakedownService).to_not receive(:downgrade_takedown!)
                subject
            end
        end

        context "moving to plan that does not allow additional artists" do
            it "calls Plans::AdditionalArtistsTakedownService" do
                expect(Plans::AdditionalArtistsTakedownService).to receive(:downgrade_takedown!)
                subject
            end
        end

    end

    context "plan user with a store automator enabled plan" do

        context "moving to another plan with store automator" do
            it "does not call Plans::StoreAutomatorTakedownService" do
                allow(Plans::CanDoService).to receive(:can_do?)
                allow(Plans::CanDoService).to receive(:can_do?).with(person.plan, :store_automator).and_return(true)
                allow(Plans::CanDoService).to receive(:can_do?).with(target_plan, :store_automator).and_return(true)
                expect(Plans::StoreAutomatorTakedownService).to_not receive(:call)
                subject
            end
        end

        context "moving to plan that does not include store automator" do
            it "calls Plans::StoreAutomatorTakedownService" do
                expect(Plans::StoreAutomatorTakedownService).to receive(:call)
                subject
            end
        end

    end

    context "plan user with a plan that includes distribution to paid dsps" do
        let!(:album){create(:album, :paid, :live, :approved, :with_salepoints, person: person)}
        context "moving to another plan that includes distribution to paid dsps" do
            let!(:target_plan) {Plan.second}
            it "does not call Plans::DspTakedownService" do
                expect(Plans::DspTakedownService).to_not receive(:call)
                subject
            end
        end

        context "moving to another plan that does not include distribution to paid dsps" do
            it "calls Plans::DspTakedownService" do
                expect(Plans::DspTakedownService).to receive(:call)
                subject
            end
        end

        context "integration testing for transaction" do
            context "no errors" do
                receive_count = 0
                it "updates db records" do
                    allow_any_instance_of(Salepoint).to receive(:send_sns_notification_takedown) do
                        receive_count += 1
                    end
                    expect { subject }.to_not raise_error
                    expect(person.salepoints.where(takedown_at:nil).length).to eq(1)
                    expect(receive_count).to eq(4)
                end
            end

            context "raises error" do
                receive_count = 0
                it "rolls back" do
                    allow_any_instance_of(Salepoint).to receive(:send_sns_notification_takedown) do
                        receive_count += 1
                        # allow first call to go through normally
                        # and raise error on second call
                        if receive_count > 1
                            raise "ERROR!"
                        end
                    end
                    expect { subject }.to raise_error
                    expect(person.salepoints.where(takedown_at:nil).length).to eq(5)
                    expect(receive_count).to eq(2)
                end
            end
        end

    end

    context "testing db transaction" do

        context "using transaction" do
            let!(:album){create(:album, :paid, :live, :approved, :with_salepoints, person: person)}
            let!(:additional_artist){create(:plan_addon, :additional_artist_addon, :paid, person: person)}
            context "no errors" do
                it "persists db records" do
                    expect(Plans::AdditionalArtistsTakedownService).to receive(:downgrade_takedown!) do
                        # create an unrelated purchase record mid-transaction and check to see if it exists afterwards
                        create(:purchase, person: person)
                    end
                    expect(Plans::StoreAutomatorTakedownService).to receive(:call)
                    expect(Plans::DspTakedownService).to receive(:call)
                    expect(PersonPlanCreationService).to receive(:call)
                    subject
                    expect(person.purchases.count).to eq(2)
                end
            end
        
            context "raises error" do
                it "rolls back" do
                    expect(Plans::AdditionalArtistsTakedownService).to receive(:downgrade_takedown!) do
                        # create an unrelated purchase record mid-transaction and check to see if it exists afterwards
                        create(:purchase, person: person)
                    end
                    expect(Plans::StoreAutomatorTakedownService).to receive(:call).and_raise("an error")
                    expect(Plans::DspTakedownService).to_not receive(:call)
                    expect(PersonPlanCreationService).to_not receive(:call)
                    expect { subject }.to raise_error
                    expect(person.purchases.count).to eq(1)
                end
            end
        end

    end

  end

end
