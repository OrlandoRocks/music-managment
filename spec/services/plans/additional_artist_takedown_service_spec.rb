# frozen_string_literal: true

require "rails_helper"

describe Plans::AdditionalArtistsTakedownService do
  describe "#downgrade_takedown!" do
    let!(:plan_person) { create(:person) }
    let!(:person_plan) { create(:person_plan, person: plan_person, plan: Plan.find(Plan::PROFESSIONAL)) }
    let!(:admin_person) { create(:person, :admin) }
    let!(:valid_note_options) do
      {user_id: admin_person.id, subject: "Plan Downgrade :(", note: "Bye Bye Album CYA"}
    end
    let!(:invalid_note_options) do
      {usury: admin_person.id, subjectivity: "Plan Downgrade :(", notation: "Bye Bye Album CYA"}
    end
    context "plan_user doesn't have a plan" do
      let!(:no_plan_person) { create(:person) }
      it "raises an error" do
        error = Plans::AdditionalArtistsTakedownService::NoPlanError
        options= [no_plan_person, valid_note_options]
        expect{ Plans::AdditionalArtistsTakedownService.downgrade_takedown!(*options) }.to raise_error(error)
      end
    end
    context "plan_user has a plan" do
      context "with no releases" do
        it "raises an error" do
          error = Plans::AdditionalArtistsTakedownService::NoReleasesError
          options = [plan_person, valid_note_options]
          expect{ Plans::AdditionalArtistsTakedownService.downgrade_takedown!(*options) }.to raise_error(error)
        end
      end
      context "with releases" do
        let!(:creative1) { {name: "Star Laser Beam", role: Creative::PRIMARY_ARTIST_ROLE}.with_indifferent_access }
        let!(:creative2) { {name: "World Sized Drill", role: Creative::PRIMARY_ARTIST_ROLE}.with_indifferent_access }
        let!(:album1) { create(:album, :paid, :with_salepoints, creatives: [creative1], person: plan_person) }
        let!(:album2) { create(:album, :paid, :with_salepoints, creatives: [creative2], person: plan_person) }
        let!(:artist1) { album1.artists.first }
        let!(:artist2) { album2.artists.first }
        before(:each) do
          create(:petri_bundle, album: album1)
          create(:petri_bundle, album: album2)
        end
        context "with invalid note options" do
          it "raises an error" do
            error = Plans::AdditionalArtistsTakedownService::NoteOptionsError
            options = [plan_person, invalid_note_options]
            expect{ Plans::AdditionalArtistsTakedownService.downgrade_takedown!(*options) }.to raise_error(error)
          end
        end
        context "with no preserved artist provided" do
          it "defaults to the oldest primary artist" do
            options = [plan_person, valid_note_options]
            Plans::AdditionalArtistsTakedownService.downgrade_takedown!(*options)

            expect(album2.reload.takedown_at).not_to be(nil)
            expect(album1.reload.takedown_at).to be(nil)
          end
        end
        context "with multiple artists on at least one release" do
          let!(:album3) do
            al = create(
              :album,
              :paid,
              :with_salepoints,
              creatives: [],
              person: plan_person,
              is_various: true
            )
            al.creatives << create(:creative, creativeable: al, artist: artist1)
            al.creatives << create(:creative, creativeable: al, artist: artist2)
            al
          end
          before{ PriorityArtist.create(artist:artist1, person:plan_person)}
          it "takes down all but the preserved artist" do
            options = [plan_person, valid_note_options]
            Plans::AdditionalArtistsTakedownService.downgrade_takedown!(*options)

            expect(album3.reload.takedown_at).not_to be(nil)
            expect(album2.reload.takedown_at).not_to be(nil)
            expect(album1.reload.takedown_at).to be(nil)
          end
        end
        context "with single artists on all releases" do
          context "all one artist" do
            before{ PriorityArtist.create(artist:artist1, person:plan_person)}
            let!(:all_for_one) { create(:person) }
            let!(:person_plan) { create(:person_plan, person: all_for_one, plan: Plan.find(Plan::PROFESSIONAL)) }
            let!(:album4) do
              al = create(:album, :paid, :with_salepoints, creatives: [creative1], person: all_for_one)
              al.creatives << create(:creative, creativeable: al, artist: artist1)
              al
            end
            let!(:album5) do
              al = create(:album, :paid, :with_salepoints, creatives: [creative1], person: all_for_one)
              al.creatives << create(:creative, creativeable: al, artist: artist1)
              al
            end
            it "doesn't take anything down" do
              options = [all_for_one, valid_note_options]
              Plans::AdditionalArtistsTakedownService.downgrade_takedown!(*options)

              expect(album4.reload.takedown_at).to be(nil)
              expect(album5.reload.takedown_at).to be(nil)
            end
          end
          context "different artists" do
            before{ PriorityArtist.create(artist:artist2, person:plan_person)}
            it "takes down all albums but the preserved artist" do
              options = [plan_person, valid_note_options]
              Plans::AdditionalArtistsTakedownService.downgrade_takedown!(*options)

              expect(album2.reload.takedown_at).to be(nil)
              expect(album1.reload.takedown_at).not_to be(nil)
            end
          end
        end
      end
    end
  end
end
