require "rails_helper"

describe Plans::ForcedDowngradeCartRemovalService do
  let!(:person ){ create(:person) }
  before(:each) do
    3.times do
      plan_products_ids = PlansProduct.where(plan_id: 4).pluck(:product_id)
      plan_product = Product.find_by(id: plan_products_ids, country_website_id: person.country_website_id)
      purchase = create(:purchase, product: plan_product, related: plan_product, person: person, cost_cents: 3500)
      renewal = create(:renewal, :plan_renewal, :no_takedown_at, purchase: purchase, person: person)
      create(:purchase, :unpaid, person: person, related: renewal, product: renewal.item_to_renew.renewal_product)
      album = create(:album, person: person)
      create(:purchase, :unpaid, person: person, related: album)
    end
  end
  describe "#call" do
    subject { described_class.call(person) }
    it "destroys plan related cart purchases, and keeps other purchases in the cart" do
      expect(person.purchases.unpaid.count).to be(6)
      subject
      expect(person.purchases.unpaid.count).to be(3)
    end
  end
end
