require "rails_helper"

describe Plans::RequestedDowngradeService do
  subject { described_class.call(person, renewals, downgrade_request) }
  DOWNGRADE_REASON_ID = DowngradeCategoryReason.first.id
  REQUESTED_PLAN_ID = Plan.first.id
  let!(:person) { create(:person, :with_plan_renewal)}
  let!(:renewals) { person&.renewals.to_a || []}
  let!(:downgrade_request) { PlanDowngradeRequest.create(
    person_id: person&.id,
    status: PlanDowngradeRequest::ACTIVE_STATUS,
    reason_id: DOWNGRADE_REASON_ID,
    requested_plan_id: REQUESTED_PLAN_ID
  )}

  context "invalid inputs" do
    context "no person" do
        let!(:person) { nil }
        it "raises NoPersonError" do
            expect { subject }.to raise_error(Plans::RequestedDowngradeService::NoPersonError)
        end
    end

    context "no downgrade request" do
        let!(:downgrade_request) { nil }
        it "raises NoDowngradeRequestError" do
            expect { subject }.to raise_error(Plans::RequestedDowngradeService::NoDowngradeRequestError)
        end
    end

    context "empty renewals array" do
        let(:renewals) { [] }

        it "returns an empty array and no downgrade is performed" do
            expect_any_instance_of(Plans::DowngradeService).to_not receive(:call)
            expect(subject).to eq([])
            expect(downgrade_request.status).to eq(PlanDowngradeRequest::ACTIVE_STATUS)
        end
    end

    context "nil renewals" do
        let(:renewals) { nil }

        it "raises error" do
            expect{ subject }.to raise_error(NoMethodError)
        end
    end

    context "no ProductItem for specified plan" do
        # ensure ProductItem cannot be found
        it "raises NoPlanProductItemError" do
            expect(person).to receive(:country_website_id).and_return(999)
            expect { subject }.to raise_error(Plans::RequestedDowngradeService::NoPlanProductItemError)
        end
    end

  end

  context "valid inputs" do
    context "renewals array contains no plan renewals" do
        let!(:person) { create(:person) }
        let!(:product) { create(:product) }
        let!(:product_item) { create(
            :product_item,
            product: product,
            renewal_duration: 1,
            renewal_interval: "year"
        )}
        let!(:renewals) { [
            create(
            :renewal,
            person: person,
            item_to_renew: product_item,
            takedown_at: nil
        )]}

        it "Returns a copy of the renewals array unchanged and does not downgrade plan" do
            expect_any_instance_of(Plans::DowngradeService).to_not receive(:call)
            expect(subject).to eq(renewals)
            expect(subject).to_not be(renewals)
            expect(downgrade_request.status).to eq(PlanDowngradeRequest::ACTIVE_STATUS)
        end
    end

    context "renewals array contains plan renewal" do

        it "uses expected inputs" do
            # asserting factory trait logic hasn't changed in a meaningful way
            expect(renewals.find(&:plan_related?)).to_not be(nil)
            expect(person.plan).to eq(Plan.find(4))
        end

        it "updates the renewal to point to new plan" do
            subject
            expect(renewals.length).to eq(1)
            expect(renewals.first.item_to_renew.product.display_name).to eq("New Artist - Unlimited Plan")  
        end

        it "creates a new PersonPlanHistory with correct dates change type" do
            old_expires_at = renewals.first.expires_at
            expect(PersonPlanHistory.where(person:person).count).to eq(0)
            subject
            expect(PersonPlanHistory.where(person:person).count).to eq(1)
            history = PersonPlanHistory.find_by(person:person)
            expect(history.change_type).to eq(PersonPlanHistory::REQUESTED_DOWNGRADE)
            expect(history.plan_end_date).to eq(old_expires_at + 1.year)
            expect(history.plan_start_date).to eq(old_expires_at) 
        end

        it "does not extend the renewal" do
            expect(renewals.count).to eq(1)
            old_renewal = renewals.last
            expect(old_renewal.renewal_history.count).to eq(1)
            old_start = old_renewal.starts_at
            old_end = old_renewal.expires_at
            subject
            expect(renewals.count).to eq(1)
            expect(old_renewal).to eq(renewals.last)
            expect(old_renewal.starts_at).to eq(old_start)
            expect(old_renewal.expires_at).to eq(old_end)
            expect(old_renewal.renewal_history.count).to eq(1)
        end

        it "updates plan_downgrade_request with 'Completed' status" do
            subject
            expect(downgrade_request.status).to eq(PlanDowngradeRequest::PROCESSED_STATUS)
        end

        it "returns the original array with the modified renewal" do
            expect(subject.first).to be(renewals.first) 
        end

    end
  end

  context "integration testing for nested transaction behavior" do
    let!(:album){create(:album, :paid, :live, :approved, :with_salepoints, person: person)}
    let!(:additional_artist){create(:plan_addon, :additional_artist_addon, :paid, person: person)}
    context "no errors" do
        it "persists db records" do
            expect(Plans::AdditionalArtistsTakedownService).to receive(:downgrade_takedown!) do
                # create an unrelated purchase record mid-transaction and check to see if it exists afterwards
                create(:purchase, person: person, related: album)
            end
            expect(Plans::StoreAutomatorTakedownService).to receive(:call)
            expect(Plans::DspTakedownService).to receive(:call)
            expect(PersonPlanCreationService).to receive(:call)
            subject
            expect(person.purchases.count).to eq(1)
        end
    end

    context "raises error in described service" do
        it "rolls back" do
            expect(Plans::AdditionalArtistsTakedownService).to receive(:downgrade_takedown!) do
                # create an unrelated purchase record mid-transaction and check to see if it exists afterwards
                create(:purchase, person: person, related: album)
            end
            expect(downgrade_request).to receive(:update).and_raise("error")
            expect(Plans::StoreAutomatorTakedownService).to receive(:call)
            expect(Plans::DspTakedownService).to receive(:call)
            expect(PersonPlanCreationService).to receive(:call)
            expect { subject }.to raise_error
            expect(person.purchases.count).to eq(0)
        end
    end

    context "raises error in DowngradeService level transaction" do
        it "rolls back" do
            expect(Plans::AdditionalArtistsTakedownService).to receive(:downgrade_takedown!) do
                # create an unrelated purchase record mid-transaction and check to see if it exists afterwards
                create(:purchase, person: person, related: album)
            end
            expect(Plans::StoreAutomatorTakedownService).to receive(:call).and_raise("an error")
            expect(Plans::DspTakedownService).to_not receive(:call)
            expect(PersonPlanCreationService).to_not receive(:call)
            expect { subject }.to raise_error
            expect(person.purchases.count).to eq(0)
        end
    end

    context "raises error in DspTakedownService level transaction" do
        receive_count = 0
        it "rolls back" do
            allow_any_instance_of(Salepoint).to receive(:send_sns_notification_takedown) do
                receive_count += 1
                # allow first call to go through normally
                # and raise error on second call
                if receive_count > 1
                    raise "ERROR!"
                end
            end
            expect { subject }.to raise_error
            expect(person.salepoints.where(takedown_at:nil).length).to eq(4)
            expect(receive_count).to eq(2)
        end
    end
  end
end
