require "rails_helper"

describe Plans::ForcedDowngradeService do
  let!(:person ){ create(:person, :with_person_plan) }
  subject { described_class.new(person) }
  describe "#initialize" do
    let!(:person) { create(:person) }
    it "raises error if the user doesn't have a plan" do
      expect{ subject }.to raise_error(Plans::ForcedDowngradeService::NoPlanError)
    end
  end
  describe "#call" do
    it "calls Plans::DowngradeService and Plans::ForcedDowngradeCartRemovalService" do
      expect(Plans::DowngradeService).to receive(:call)
      expect(Plans::ForcedDowngradeCartRemovalService).to receive(:call)
      subject.call
    end
  end
  describe "#downgrade_purchase" do
    it "returns an instance of purchase" do
      expect(subject.downgrade_purchase.instance_of?(Purchase)).to be(true)
    end
    it "settles the invoice associated with that purchase" do
      expect(subject.downgrade_purchase.invoice.settled?).to be(true)
    end
  end
end
