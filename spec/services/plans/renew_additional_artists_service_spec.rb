require "rails_helper"

describe Plans::RenewAdditionalArtistsService do
  let!(:person) { create(:person) }
  let!(:plan_products_ids) { PlansProduct.where(plan_id: 4).pluck(:product_id) }
  let!(:plan_product) do
    Product.find_by(id: plan_products_ids, country_website_id: person.country_website_id)
  end
  let!(:product_item) { ProductItem.find_by(product_id: plan_product.id) }
  let!(:person_plan) { create(:person_plan, person: person, plan: Plan.find(Plan::PROFESSIONAL)) }
  let!(:renewal) do
    create(:renewal,
           person: person,
           item_to_renew: product_item)
  end
  let!(:renewal_item) {create(:renewal_item, renewal: renewal, related: person_plan)}
  let!(:product) { renewal.item_to_renew.renewal_product }
  let!(:creative1) { {name: "Star Laser Beam", role: Creative::PRIMARY_ARTIST_ROLE}.with_indifferent_access }
  let!(:creative2) { {name: "World Sized Drill", role: Creative::PRIMARY_ARTIST_ROLE}.with_indifferent_access }
  let!(:creative3) { {name: "Drill Sized Star", role: Creative::PRIMARY_ARTIST_ROLE}.with_indifferent_access }
  let!(:album1) { create(:album, :paid, :with_salepoints, creatives: [creative1], person: person) }
  let!(:album2) { create(:album, :paid, :with_salepoints, creatives: [creative2], person: person) }
  let!(:album3) { create(:album, :paid, :with_salepoints, creatives: [creative3], person: person) }
  let!(:addtl_artist_1) { create(:plan_addon, :additional_artist_addon, :paid, person: person) }
  let!(:addtl_artist_2) { create(:plan_addon, :additional_artist_addon, :paid, person: person) }

  describe "::plan_and_additional_artist_purchases!" do
    context "user has multiple active primary artists" do
      subject { described_class.plan_and_additional_artist_purchases!(person, renewal, product) }
      it "returns an array" do
        expect(subject.instance_of?(Array)).to be(true)
      end
      it "returns purchases for the plan renewal and the additional primary artists" do
        plan_purchase = subject.first
        additional_artist_1_purchase = subject.second
        additional_artist_2_purchase = subject.third

        expect(subject.length).to be(3)
        expect(plan_purchase.related.is_plan?).to be(true)
        expect(additional_artist_1_purchase.related.additional_artist?).to be(true)
        expect(additional_artist_2_purchase.related.additional_artist?).to be(true)
      end
      context "one of the artists isnt live" do
        before(:each) do
          create(:petri_bundle, album: album3)
          album3.takedown!
        end
        it "returns purchases for the plan renewal and only the live additional primary artists" do
          plan_purchase = subject.first
          additional_artist_1_purchase = subject.second

          expect(subject.length).to be(2)
          expect(plan_purchase.related.is_plan?).to be(true)
          expect(additional_artist_1_purchase.related.additional_artist?).to be(true)
        end
        context "canceled additional_artist" do
          before(:each) do
            addtl_artist_1.update(canceled_at: Time.current)
          end
          it "renews the active additional artist" do
            plan_purchase = subject.first
            additional_artist_1_purchase = subject.second

            expect(subject.length).to be(2)
            expect(plan_purchase.related.is_plan?).to be(true)
            expect(additional_artist_1_purchase.related.additional_artist?).to be(true)
            expect(additional_artist_1_purchase.related.id).to be(addtl_artist_2.id)
          end
        end
      end
    end
    context "user only has 1 active primary artist" do
      # before(:each) do
      #   allow(Product).to receive(:add_to_cart).and_return(double(Purchase, invoice_id: nil))
      # end
      let!(:one_artist_person) { create(:person) }
      let!(:one_artist_person_plan) do
        create(:person_plan, person: one_artist_person, plan: Plan.find(Plan::PROFESSIONAL))
      end
      let!(:one_artist_purchase) do
        create(:purchase,
               product: plan_product,
               related: plan_product,
               person: one_artist_person,
               cost_cents: 3500)
      end
      let!(:one_artist_product) { one_artist_renewal.item_to_renew.renewal_product }
      let!(:one_artist_person_plan) { create(:person_plan, person: one_artist_person, plan: Plan.find(Plan::PROFESSIONAL)) }
      let!(:one_artist_product) { renewal.item_to_renew.renewal_product }
      let!(:one_artist_renewal) do
        create(:renewal,
               person: one_artist_person,
               purchase: one_artist_purchase,
               item_to_renew: product_item)
      end
      let!(:renewal_item) {create(:renewal_item, renewal: one_artist_renewal, related: one_artist_person_plan)}

      subject do
        described_class.plan_and_additional_artist_purchases!(one_artist_person, one_artist_renewal, one_artist_product)
      end
      it "returns an array" do
        expect(subject.instance_of?(Array)).to be(true)
      end
      it "returns one purchase only for the plan renewal" do
        plan_purchase = subject.first

        expect(subject.length).to be(1)
        expect(plan_purchase.related.is_plan?).to be(true)
      end
    end
    context "invalid inputs" do
      context "no person provided" do
        it "raises error" do
          expect{ Plans::RenewAdditionalArtistsService.plan_and_additional_artist_purchases!(nil, renewal, product) }
            .to raise_error(Plans::RenewAdditionalArtistsService::NoPersonError)
        end
      end
      context "no renewal provided" do
        it "raises error" do
          expect{ Plans::RenewAdditionalArtistsService.plan_and_additional_artist_purchases!(person, nil, product) }
            .to raise_error(Plans::RenewAdditionalArtistsService::NoRenewalError)
        end
      end
      context "no product provided" do
        it "raises error" do
          expect{ Plans::RenewAdditionalArtistsService.plan_and_additional_artist_purchases!(person, renewal, nil) }
            .to raise_error(Plans::RenewAdditionalArtistsService::NoProductError)
        end
      end
      context "product is not for a plan" do
        let!(:non_plan_product) { Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID) }
        it "raises error" do
          expect do
            Plans::RenewAdditionalArtistsService
              .plan_and_additional_artist_purchases!(person, renewal, non_plan_product)
          end.to raise_error(Plans::RenewAdditionalArtistsService::NonPlanProductError)
        end
      end
      context "person doesn't have a plan" do
        let!(:non_plan_person) { (create :person) }
        it "raises error" do
          expect do
            Plans::RenewAdditionalArtistsService.plan_and_additional_artist_purchases!(non_plan_person, renewal, product)
          end.to raise_error(Plans::RenewAdditionalArtistsService::NoPlanError)
        end
      end
    end
  end
end
