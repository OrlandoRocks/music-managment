require "rails_helper"

describe Plans::RequiredStoresService do
  let!(:person) { create(:person) }
  context "base_query" do
    subject { described_class.new(person, :combined).call }

    let!(:paid_album) { create(:album, :approved, :with_salepoints, person: person) }
    before(:each) do
      paid_album.salepoints.each(&:takedown!)
    end
    it "does not include stores from salepoints that are takendown" do
      expect(subject.empty?).to be(true)
    end
  end
  context "combined scope" do
    subject { described_class.new(person, :combined).call }

    let!(:paid_album) { create(:album, :approved, :with_salepoints, person: person) }
    let!(:cart_album) { create(:album, :social_only, person: person) }
    let(:plan_credit_usage) { create(:plan_credit_usage, related: cart_album, person: person) }
    before do
      create(:purchase, :unpaid, related: plan_credit_usage, person: person)
    end
    it "returns store_ids that are paid and store_ids from the cart and excludes inactive stores" do
      cart_active_stores = cart_album
                             .salepoints
                             .joins(:store)
                             .where(stores: { is_active: true })
                             .order(:store_id)
                             .pluck(:store_id)
      paid_active_stores = paid_album
                             .salepoints
                             .joins(:store)
                             .where(stores: { is_active: true })
                             .order(:store_id)
                             .pluck(:store_id)
      expect(subject.sort).to eq(cart_active_stores.concat(paid_active_stores).sort)
      expect(
        (paid_album.stores.where(is_active: false).length > 0) ||
        (cart_album.stores.where(is_active: false).length > 0)
      ).to be(true)
    end
  end
  context "cart scope" do
    subject { described_class.new(person, :cart).call }

    let!(:album) { create(:album, :with_salepoints, person: person) }
    let(:plan_credit_usage) { create(:plan_credit_usage, related: album, person: person) }
    context "stores in cart" do
      before do
        create(:purchase, :unpaid, related: plan_credit_usage, person: person)
      end
      it "returns the store_ids from the cart and excludes inactive stores" do
        active_stores = album
                          .salepoints
                          .joins(:store)
                          .where(stores: { is_active: true })
                          .order(:store_id)
                          .pluck(:store_id)
        expect(subject.sort).to eq(active_stores)
        expect(album.stores.where(is_active: false).length).to be > 0
      end
    end
    context "stores not in cart" do
      it "returns empty array" do
        expect(subject).to eq([])
      end
    end
  end

  context "takedown_related scope" do
    subject { described_class.new(person, :takedown_related).call }

    let!(:album) { create(:album, :approved, :with_salepoints, person: person) }

    context "paid stores" do

      it "returns store_ids that are paid, including those for inactive stores" do
        active_stores = album
                          .salepoints
                          .joins(:store)
                          .order(:store_id)
                          .pluck(:store_id)
        expect(subject.sort).to eq(active_stores)
        expect(album.stores.where(is_active: false).length).to be > 0
      end
    end
  end
end
