require "rails_helper"

describe Plans::AdditionalArtistService do
  let!(:creative1) { { "name" => "default artist", "role" => "primary_artist" } }
  let!(:creative2) { { "name" => "new kid on tha block artist", "role" => "primary_artist" } }
  let!(:product) { Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID) }
  let!(:purchased_plan_person) { create(:person) }
  let!(:person_plan) { create(:person_plan, person: purchased_plan_person)}

  describe "#adjust_additional_artists" do

    let!(:carted_plan_person) do
      person = create(:person)
      plan = Plan.last
      product = plan.products.find_by!(country_website: person.country_website)
      Product.add_to_cart(person, product, product)
      person
    end

    let!(:carted_freemium_person) do
      person = create(:person)
      plan = Plan.first
      product = plan.products.find_by!(country_website: person.country_website)
      Product.add_to_cart(person, product, product)
      person
    end

    let!(:legacy_person) { create(:person) }

    it "returns nil if user has no plan (is on legacy pricing)" do
      expect(Plans::AdditionalArtistService.adjust_additional_artists(legacy_person)).to be nil
    end

    it "removes carted artists if they exist and count is incorrect" do
      Plans::AdditionalArtistService.new(carted_plan_person).create_additional_artist_purchase
      expect(carted_plan_person.purchases.unpaid.count).to eq(2)

      Plans::AdditionalArtistService.adjust_additional_artists(carted_plan_person)
      expect(carted_plan_person.purchases.unpaid.count).to eq(1)
    end

    it "does not remove carted artists if the count is correct" do
      Plans::AdditionalArtistService.new(carted_plan_person).create_additional_artist_purchase

      album = create(:album, :purchaseable, person: carted_plan_person, creatives: [creative1, creative2])
      Product.add_to_cart(album.person, album, product) # PlanCreditUsage

      expect(carted_plan_person.purchases.unpaid.count).to eq(3)

      Plans::AdditionalArtistService.adjust_additional_artists(carted_plan_person)
      expect(carted_plan_person.purchases.unpaid.count).to eq(3)
    end

    it "adds additional artists to cart if needed" do
      album = create(:album, :purchaseable, person: carted_plan_person, creatives: [creative1, creative2])
      Product.add_to_cart(album.person, album, product)
      expect(carted_plan_person.purchases.unpaid.count).to eq(2)

      Plans::AdditionalArtistService.adjust_additional_artists(carted_plan_person)
      expect(carted_plan_person.purchases.unpaid.count).to eq(3)
    end

    it "adds additional artists to cart if needed due to new artist on ringtone" do
      album = create(:album, :purchaseable,
                             person: carted_plan_person,
                             creatives: [creative1, creative2],
                             album_type: "Ringtone")
      Product.add_to_cart(album.person, album, product)
      expect(carted_plan_person.purchases.unpaid.count).to eq(2)

      Plans::AdditionalArtistService.adjust_additional_artists(carted_plan_person)
      expect(carted_plan_person.purchases.unpaid.count).to eq(3)
    end
    context "albums taken down" do
      let!(:creative3) { {name: "Star Laser Beam", role: Creative::PRIMARY_ARTIST_ROLE}.with_indifferent_access }
      let!(:creative4) { {name: "World Sized Drill", role: Creative::PRIMARY_ARTIST_ROLE}.with_indifferent_access }
      let!(:creative5) { {name: "Drill Sized Star", role: Creative::PRIMARY_ARTIST_ROLE}.with_indifferent_access }
      let!(:album1) { create(:album, :approved, :with_salepoints, creatives: [creative3], person: purchased_plan_person) }
      let!(:album2) { create(:album, :approved, :with_salepoints, creatives: [creative4], person: purchased_plan_person) }
      let!(:album3) { create(:album, :approved, :with_salepoints, creatives: [creative5], person: purchased_plan_person) }
      let!(:addtl_artist_1) { create(:plan_addon, :additional_artist_addon, :paid, person: purchased_plan_person) }
      let!(:addtl_artist_2) { create(:plan_addon, :additional_artist_addon, :paid, person: purchased_plan_person) }
      before(:each) do
        create(:petri_bundle, album: album3)
        album3.takedown!
      end
      context "taken down additional artist is not canceled" do
        it "does not add any additional artists to cart" do
          album = create(:album, :purchaseable, person: purchased_plan_person, creatives: [creative1])
          Product.add_to_cart(album.person, album, product)
          expect(purchased_plan_person.purchases.unpaid.count).to eq(1)

          Plans::AdditionalArtistService.adjust_additional_artists(purchased_plan_person)
          expect(purchased_plan_person.purchases.unpaid.count).to eq(1)
        end
      end
      context "taken down additional artist is canceled" do
        before(:each) do
          addtl_artist_2.update(canceled_at: Time.current)
        end
        it "adds and additional artist to cart" do
          album = create(:album, :purchaseable, person: purchased_plan_person, creatives: [creative1])
          Product.add_to_cart(album.person, album, product)
          expect(purchased_plan_person.purchases.unpaid.count).to eq(1)

          Plans::AdditionalArtistService.adjust_additional_artists(purchased_plan_person)
          expect(purchased_plan_person.purchases.unpaid.count).to eq(2)
        end
      end
    end
  end

  describe "#calculate_new_artists" do
    let!(:album) { create(:album, person: purchased_plan_person, creatives: [creative1, creative2]) }

    it "returns the count of novel Primary Artists on a release" do
      Product.add_to_cart(album.person, album, product)

      expect(Plans::AdditionalArtistService.calculate_new_artists(purchased_plan_person, album)).to be 2
    end

    it "does not count duplicate Primary Artists on a release" do
      another_album = create(:album, :paid, :with_songs, legal_review_state: "APPROVED", creatives: [creative1], person: purchased_plan_person)

      Product.add_to_cart(album.person, album, product)

      expect(Plans::AdditionalArtistService.calculate_new_artists(purchased_plan_person, album)).to be 1
    end
  end

  describe "#clear_unpaid_additional_artist_purchases!" do
    let(:plan_person) { create(:person) }
    let(:person_plan) { create(:person_plan, person: plan_person)}
    let(:add_artist_product) { Product.find(503) }
    
    context "unpaid additional artist purchase in cart" do   
      let(:unpaid_addtl_artist) { create(:plan_addon, :additional_artist_addon, person: plan_person) }

      let!(:unpaid_addtl_artist_purchase) do
        purchase = Product.add_to_cart(plan_person, unpaid_addtl_artist, add_artist_product)
        purchase.unprocessed!
        purchase
      end

      before { Plans::AdditionalArtistService.clear_unpaid_additional_artist_purchases!(plan_person) }

      it "destroys the purchase and related additional artist" do
        expect { unpaid_addtl_artist.reload }.to raise_error(ActiveRecord::RecordNotFound)
        expect { unpaid_addtl_artist_purchase.reload }.to raise_error(ActiveRecord::RecordNotFound)
        expect(plan_person.purchases.length).to eq(0)
      end
    end

    context "canceled unpaid additional artist purchase in cart" do   
      let(:canceled_unpaid_addtl_artist) { create(:plan_addon, :additional_artist_addon, person: plan_person) }

      let!(:canceled_unpaid_addtl_artist_purchase) do
        purchase = Product.add_to_cart(plan_person, canceled_unpaid_addtl_artist, add_artist_product)
        purchase.unprocessed!
        canceled_unpaid_addtl_artist.update!(canceled_at:DateTime.now)
        purchase
      end

      before { Plans::AdditionalArtistService.clear_unpaid_additional_artist_purchases!(plan_person) }

      it "does not destroy the purchase or related additional artist" do
        expect { canceled_unpaid_addtl_artist.reload }.to_not raise_error(ActiveRecord::RecordNotFound)
        expect { canceled_unpaid_addtl_artist_purchase.reload }.to_not raise_error(ActiveRecord::RecordNotFound)
        expect(plan_person.purchases.length).to eq(1)
      end
    end

    context "renewing an previously paid additional artist" do   
      
      let(:paid_addtl_artist) { create(:plan_addon, :additional_artist_addon, :paid, person: plan_person) }

      let!(:paid_addtl_artist_purchase) do
        purchase = Product.add_to_cart(plan_person, paid_addtl_artist, add_artist_product)
        purchase.unprocessed!
        purchase
      end

      before { Plans::AdditionalArtistService.clear_unpaid_additional_artist_purchases!(plan_person) }

      it "does not destroy the purchase but not related additional artist" do
        expect { paid_addtl_artist.reload }.to_not raise_error(ActiveRecord::RecordNotFound)
        expect { paid_addtl_artist_purchase.reload }.to raise_error(ActiveRecord::RecordNotFound)
        expect(plan_person.purchases.length).to eq(0)
      end
    end

    context "different type of purchase in cart" do   
      let(:album) { create(:album, person: plan_person) }
      let!(:album_purchase) { Product.add_to_cart(plan_person, album, product) }
      before { Plans::AdditionalArtistService.clear_unpaid_additional_artist_purchases!(plan_person) }

      it "does not destroy the purchase or related" do
        expect { album.reload }.to_not raise_error(ActiveRecord::RecordNotFound)
        expect { album_purchase.reload }.to_not raise_error(ActiveRecord::RecordNotFound)
        expect(plan_person.purchases.length).to eq(1)
      end
    end

  end
end
