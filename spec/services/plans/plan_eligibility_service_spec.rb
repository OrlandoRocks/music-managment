require "rails_helper"

describe Plans::PlanEligibilityService do
  let(:user) { create(:person) }

  let(:album_1) { create(:album, :paid, :approved, person: user) }
  let(:album_2) { create(:album, :paid, :approved, person: user) }

  let(:plans) { Plan.purchasable_by_user }

  before do
      allow(FeatureFlipper).to receive(:show_feature?)
      allow(FeatureFlipper).to receive(:show_feature?).with(:discovery_platforms, an_instance_of(Person)).and_return(true)
  end

  describe "#plans_by_upgrade_status" do
    subject { described_class.new(user.id).plans_by_upgrade_status }
    let!(:automator) {  create(:salepoint_subscription, album: album_1, is_active: false, finalized_at: Time.now) }

    context "artist count scenarios" do
      let(:fresh_user) { create(:person) }

      additional_artist_plans_result = [[1, false], [2, false], [3, false], [4, true]]
      all_plans_result =               [[1, true], [2, true], [3, true], [4, true]]

      context "when a fresh user has no releases" do
        it "all plans are eligible" do
          expect(subject.map { |p| [p[:plan].id, p[:eligible]] })
          .to match(all_plans_result)
        end
      end

      context "when a user has a paid album with two primary artists" do
        it "returns 2" do
          add_primary_artist!

          expect(subject.map { |p| [p[:plan].id, p[:eligible]] })
          .to match(additional_artist_plans_result)
        end
      end

      context "when a user has a carted album with 1 primary artists" do
        it "returns 1" do
          album_1.create_purchase_with_credit

          expect(subject.map { |p| [p[:plan].id, p[:eligible]] })
          .to match(all_plans_result)
        end
      end

      context "when a user has two carted albums with different primary artist names" do
        it "returns 2" do
          album_1.create_purchase_with_credit
          album_2.create_purchase_with_credit

          expect(subject.map { |p| [p[:plan].id, p[:eligible]] })
          .to match(additional_artist_plans_result)
        end
      end
    end

    context "when a user only has one primary artist" do
      context "when a user's releases only has social platforms" do

        before do
          create_salepoints(Plan::NEW_ARTIST)
        end

        let(:expected_enabled_plans) {
          expected_plans_status(plans)
        }

        it "returns an array of objects with all Plans Eligible" do
          expect(subject).to match(expected_enabled_plans)
        end
      end

      context "when a user's releases have stores social platforms and any stores from Rising artists" do

        before do
          create_salepoints(Plan::RISING_ARTIST)
        end

        let(:expected_enabled_plans) {
          expected_plans_status(plans.where.not(id: Plan::NEW_ARTIST))
        }

        it "returns an array of objects with all Plans except New Artist Eligible" do
          expect(subject).to match(expected_enabled_plans)
        end
      end

      context "when a user's releases has stores from New Artist, Rising Artist Plans, Breakout Artist Plans" do

        before do
          create_salepoints(Plan::BREAKOUT_ARTIST)
        end

        let(:expected_enabled_plans) {
          expected_plans_status(plans.where.not(id: [Plan::NEW_ARTIST]))
        }

        it "returns an array of objects with all plans but New Artist" do
          expect(subject).to match(expected_enabled_plans)
        end
      end

      context "when a user's releases has stores from either Professional Artist or Legend Plans" do

        before do
          create_salepoints(Plan::PROFESSIONAL)
        end

        let(:expected_enabled_plans) {
          expected_plans_status(plans.where.not(id: [Plan::NEW_ARTIST]))
        }

        it "returns an array of objects with only Professional Artist and Legend Plans" do
          expect(subject).to match(expected_enabled_plans)
        end
      end

      context "when a user's releases has stores from either Breakout Artist or Legend Plans" do

        before do
          create_salepoints(Plan::LEGEND)
        end

        let(:expected_enabled_plans) {
          expected_plans_status(plans.where.not(id: [Plan::NEW_ARTIST]))
        }

        it "returns an array of objects with all plans but New Artist" do
          expect(subject).to match(expected_enabled_plans)
        end
      end
    end

    context "when a user has more than one artist" do

      before { add_primary_artist! }

      let(:expected_enabled_plans) {
        expected_plans_status(plans.where.not(id: [Plan::NEW_ARTIST, Plan::RISING_ARTIST, Plan::BREAKOUT_ARTIST]))
      }

      context "when a user's releases only has stores from New Artist Plan" do

        before do
          create_salepoints(Plan::NEW_ARTIST)
        end

        it "returns an array of objects with only Professional Artist and Legend Plans" do
          expect(subject).to match(expected_enabled_plans)
        end
      end

      context "when a user's releases has stores from New Artist, Rising Artist" do

        before do
          create_salepoints(Plan::RISING_ARTIST)
        end

        it "returns an array of objects with only Professional Artist and Legend Planss" do
          expect(subject).to match(expected_enabled_plans)
        end
      end

      context "when a user's releases has stores from New Artist, Rising Artist and Breakout Artist Plans" do

        before do
          create_salepoints(Plan::BREAKOUT_ARTIST)
        end

        it "returns an array of objects with only Professional Artist and Legend Plans" do
          expect(subject).to match(expected_enabled_plans)
        end
      end

      context "when a user's releases has stores from either Professional Artist or Legend Plans" do

        before do
          create_salepoints(Plan::PROFESSIONAL)
        end

        it "returns an array of objects with only Professional Artist and Legend Plans" do
          expect(subject).to match(expected_enabled_plans)
        end
      end

      context "when a user's releases has stores from either Professional Artist or Legend Plans" do

        before do
          create_salepoints(Plan::LEGEND)
        end

        it "returns an array of objects with only Professional Artist and Legend Plans" do
          expect(subject).to match(expected_enabled_plans)
        end
      end
    end

    context "when a user has automator" do

      before do
        automator.update(is_active: true)
      end

      let(:expected_enabled_plans) {
        expected_plans_status(plans.where.not(id: [Plan::NEW_ARTIST, Plan::RISING_ARTIST]))
      }

      context "when a user's releases has stores from New Artist" do

        before do
          create_salepoints(Plan::RISING_ARTIST)
        end

        it "returns an array of objects with only Rising Artist, Professional Artist and Legend Plans" do
          expect(subject).to match(expected_enabled_plans)
        end
      end

      context "when a user's releases has stores from New Artist, Rising Artist" do

        before do
          create_salepoints(Plan::RISING_ARTIST)
        end

        it "returns an array of objects with only Rising Artist, Professional Artist and Legend Plans" do
          expect(subject).to match(expected_enabled_plans)
        end
      end

      context "when a user's releases has stores from New Artist, Rising Artist and Breakout Artist Plans" do

        before do
          create_salepoints(Plan::BREAKOUT_ARTIST)
        end

        it "returns an array of objects with only Rising Artist, Professional Artist and Legend Plans" do
          expect(subject).to match(expected_enabled_plans)
        end
      end

      context "when a user's releases has stores from either Professional Artist or Legend Plans" do

        before do
          create_salepoints(Plan::PROFESSIONAL)
        end

        it "returns an array of objects with  only Rising Artist, Professional Artist and Legend Plans" do
          expect(subject).to match(expected_enabled_plans)
        end
      end

      context "when a user's releases has stores from either Breakout Artist or Legend Plans" do

        before do
          create_salepoints(Plan::LEGEND)
        end

        it "returns an array of objects with  only Rising Artist, Professional Artist and Legend Plans" do
          expect(subject).to match(expected_enabled_plans)
        end
      end
    end

    context "when a user has automator and has more than one primary artist" do

      before do
        automator.update(is_active: true)
        add_primary_artist!
      end

      let(:expected_enabled_plans) {
        expected_plans_status(plans.where(id: [Plan::PROFESSIONAL, Plan::LEGEND]))
      }

      context "when a user's releases only has social platforms" do

        before do
            create_salepoints(Plan::NEW_ARTIST)
        end

        it "returns an array of objects with only Professional Artist and Legend Plans" do
            expect(subject).to match(expected_enabled_plans)
        end
      end

      context "when a user's releases has stores from New Artist, Rising Artist" do

        before do
          create_salepoints(Plan::RISING_ARTIST)
        end

        it "returns an array of objects with only Professional Artist and Legend Plans" do
          expect(subject).to match(expected_enabled_plans)
        end
      end

      context "when a user's releases has stores from New Artist, Rising Artist and Breakout Artist Plans" do

        before do
          create_salepoints(Plan::BREAKOUT_ARTIST)
        end

        it "returns an array of objects with only Professional Artist and Legend Plans" do

          expect(subject).to match(expected_enabled_plans)
        end
      end

      context "when a user's releases has stores from either Professional Artist or Legend Plans" do

        before do
          create_salepoints(Plan::PROFESSIONAL)
        end

        it "returns an array of objects with only Professional Artist and Legend Plans" do
          expect(subject).to match(expected_enabled_plans)
        end
      end

      context "when a user's releases has stores from either Breakout Artist or Legend Plans" do

        before do
          create_salepoints(Plan::LEGEND)
        end

        it "returns an array of objects with only Professional Artist and Legend Plans" do
          expect(subject).to match(expected_enabled_plans)
        end
      end
    end
  end

  describe "#lower_plans_by_downgrade_status" do
    subject { described_class.new(user.id).lower_plans_by_downgrade_status }
    let!(:automator) {  create(:salepoint_subscription, album: album_1, is_active: false, finalized_at: Time.now) }

    context "with all valid lower plans" do
      let!(:person_plan) { create(:person_plan, person: user, plan: Plan.last)}
      it "returns an array of objects with all Plans Eligible" do
        expect(subject).to be_an_instance_of(Array)
        expect(subject.length).to eq(Plan.all.length - 1)
      end
    end


    context "when a user only has one primary artist" do
      context "when a user is in new artist plan" do

        before do
          create_person_plan(Plan::NEW_ARTIST)
        end

        let(:expected_lower_plans) {
          expected_downgrade_plans_status([])
        }

        it "returns an array of objects with all lower Plans" do
          expect(subject).to match(expected_lower_plans)
        end
      end

      context "when a user is in Rising artist plan" do

        before do
          create_person_plan(Plan::RISING_ARTIST)
        end

        let(:expected_lower_plans) {
          expected_downgrade_plans_status(Plan.where(id: Plan::NEW_ARTIST))
        }

        it "returns an array of objects with all lower Plans" do
          expect(subject).to match(expected_lower_plans)
        end
      end

      context "when a user is in breakout artist plan" do

        before do
          create_person_plan(Plan::BREAKOUT_ARTIST)
        end

        let(:expected_lower_plans) {
          expected_downgrade_plans_status(Plan.where(id: [Plan::NEW_ARTIST, Plan::RISING_ARTIST]))
        }

        it "returns an array of objects with all lower Plans" do
          expect(subject).to match(expected_lower_plans)
        end

      end

      context "when a user is in professional artist plan" do

        before do
          create_person_plan(Plan::PROFESSIONAL)
        end

        let(:expected_lower_plans) {
          expected_downgrade_plans_status(Plan.where(id: [Plan::NEW_ARTIST, Plan::RISING_ARTIST, Plan::BREAKOUT_ARTIST]))
        }

        it "returns an array of objects with all lower Plans" do
          expect(subject).to match(expected_lower_plans)
        end
      end

      context "when a user is in legend plan" do

        before do
          create_person_plan(Plan::LEGEND)
        end

        let(:expected_lower_plans) {
          expected_downgrade_plans_status(Plan.where(id: [Plan::NEW_ARTIST, Plan::RISING_ARTIST, Plan::BREAKOUT_ARTIST, Plan::PROFESSIONAL]))
        }

        it "returns an array of objects with all lower Plans" do
          expect(subject).to match(expected_lower_plans)
        end
      end
    end

    context "when a user has more than one artist" do

      before { add_primary_artist! }

      context "when a user is in new artist plan" do

        before do
          create_person_plan(Plan::NEW_ARTIST)
        end

        let(:expected_lower_plans) {
          expected_downgrade_plans_status([])
        }

        it "returns an array of objects with all lower Plans" do
          expect(subject).to match(expected_lower_plans)
        end
      end

      context "when a user is in Rising artist plan" do

        before do
          create_person_plan(Plan::RISING_ARTIST)
        end

        let(:expected_lower_plans) {
          expected_downgrade_plans_status([], "artist")
        }

        it "returns an array of objects with all lower Plans" do
          expect(subject).to match(expected_lower_plans)
        end
      end

      context "when a user is in breakout artist plan" do

        before do
          create_person_plan(Plan::BREAKOUT_ARTIST)
        end

        let(:expected_lower_plans) {
          expected_downgrade_plans_status([], "artist")
        }

        it "returns an array of objects with all lower Plans" do
          expect(subject).to match(expected_lower_plans)
        end

      end

      context "when a user is in professional artist plan" do

        before do
          create_person_plan(Plan::PROFESSIONAL)
        end

        let(:expected_lower_plans) {
          expected_downgrade_plans_status([], "artist")
        }

        it "returns an array of objects with all lower Plans" do
          expect(subject).to match(expected_lower_plans)
        end
      end

      context "when a user is in legend plan" do

        before do
          create_person_plan(Plan::LEGEND)
        end

        let(:expected_lower_plans) {
          expected_downgrade_plans_status(Plan.where(id: [Plan::PROFESSIONAL]), "artist")
        }

        it "returns an array of objects with all lower Plans" do
          expect(subject).to match(expected_lower_plans)
        end
      end

    end


    context "when a user has automator" do

      before do
        automator.update(is_active: true)
      end

      context "when a user is in new artist plan" do

        before do
          create_person_plan(Plan::NEW_ARTIST)
        end

        let(:expected_lower_plans) {
          expected_downgrade_plans_status([])
        }

        it "returns an array of objects with all lower Plans" do
          expect(subject).to match(expected_lower_plans)
        end
      end

      context "when a user is in rising artist plan" do

        before do
          create_person_plan(Plan::RISING_ARTIST)
        end

        let(:expected_lower_plans) {
          expected_downgrade_plans_status([], "automator")
        }

        it "returns an array of objects with all lower Plans" do
          expect(subject).to match(expected_lower_plans)
        end
      end

      context "when a user is in breakout artist plan" do

        before do
          create_person_plan(Plan::BREAKOUT_ARTIST)
        end

        let(:expected_lower_plans) {
          expected_downgrade_plans_status(Plan.where(id: []), "automator")
        }

        it "returns an array of objects with all lower Plans" do
          expect(subject).to match(expected_lower_plans)
        end

      end

      context "when a user is in professional artist plan" do

        before do
          create_person_plan(Plan::PROFESSIONAL)
        end

        let(:expected_lower_plans) {
          expected_downgrade_plans_status(Plan.where(id: [Plan::BREAKOUT_ARTIST]), "automator")
        }

        it "returns an array of objects with all lower Plans" do
          expect(subject).to match(expected_lower_plans)
        end
      end

      context "when a user is in legend plan" do

        before do
          create_person_plan(Plan::LEGEND)
        end

        let(:expected_lower_plans) {
          expected_downgrade_plans_status(Plan.where(id: [Plan::BREAKOUT_ARTIST, Plan::PROFESSIONAL]), "automator")
        }

        it "returns an array of objects with all lower Plans" do
          expect(subject).to match(expected_lower_plans)
        end
      end
    end

    # These specs depend on specific stores the album is distributed to, currently Plans 2,3,4,5 all have access to the same stores
    # If those change, all you have to do is add/remove the expected_downgrade_plans_status

    context "when a user has stores" do
      context "when a user is in new artist plan" do

        before do
          create_person_plan(Plan::NEW_ARTIST)
          create_salepoints(Plan::NEW_ARTIST)
        end

        let(:expected_lower_plans) {
          expected_downgrade_plans_status([])
        }

        it "returns an array of objects with all lower Plans" do
          expect(subject).to match(expected_lower_plans)
        end
      end

      context "when a user is in rising artist plan" do

        before do
          create_person_plan(Plan::RISING_ARTIST)
          create_salepoints(Plan::RISING_ARTIST)
        end

        let(:expected_lower_plans) {
          expected_downgrade_plans_status([], "store")
        }

        it "returns an array of objects with all lower Plans" do
          expect(subject).to match(expected_lower_plans)
        end
      end

      context "when a user is in rising artist plan" do

        before do
          create_person_plan(Plan::BREAKOUT_ARTIST)
          create_salepoints(Plan::BREAKOUT_ARTIST)
        end

        let(:expected_lower_plans) {
          expected_downgrade_plans_status(Plan.where(id: [Plan::RISING_ARTIST]), "store")
        }

        it "returns an array of objects with all lower Plans" do
          expect(subject).to match(expected_lower_plans)
        end

      end

      context "when a user is in breakout artist plan" do

        before do
          create_person_plan(Plan::PROFESSIONAL)
          create_salepoints(Plan::PROFESSIONAL)
        end

        let(:expected_lower_plans) {
          expected_downgrade_plans_status(Plan.where(id: [Plan::RISING_ARTIST, Plan::BREAKOUT_ARTIST]), "store")
        }

        it "returns an array of objects with all lower Plans" do
          expect(subject).to match(expected_lower_plans)
        end
      end

      context "when a user is in legend plan" do

        before do
          create_salepoints(Plan::LEGEND)
          create_person_plan(Plan::LEGEND)
        end

        let(:expected_lower_plans) {
          expected_downgrade_plans_status(Plan.where(id: [Plan::RISING_ARTIST, Plan::BREAKOUT_ARTIST, Plan::PROFESSIONAL]), "store")
        }

        it "returns an array of objects with all lower Plans" do
          expect(subject).to match(expected_lower_plans)
        end
      end
    end

  end


  #
  # Helpers
  #

  def expected_plans_status(plans)
    Plan.purchasable_by_user.map do |plan|
      eligible = plan.in? plans
      {
        plan: plan,
        eligible: eligible
      }
    end
  end

  # we need to pass downgrade_eligible to the arguments for specific cases in the map,
  # so professional will be artist_ineligibility = true
  def expected_downgrade_plans_status(plans, ineligibility_string = nil)
    Plan.where(id: user.plan.lower_plan_ids).map do |plan|
      downgrade_eligible = plan.in? (plans)
      {
        plan_name: plan.name,
        plan: plan,
        downgrade_eligible: downgrade_eligible,
        automator_ineligibility: ineligibility_string == "automator" && !plans.include?(plan) ? true : false,
        artist_ineligibility: ineligibility_string == "artist" && !plans.include?(plan) ? true : false,
        active_release_ineligibility: ineligibility_string == "store" && !plans.include?(plan) ? true : false
      }
    end
  end

  def create_salepoints(plan_id)
    Store.joins(:plans_stores).is_active.where(plans_stores: { plan_id: plan_id }).each do |store|
      Salepoint.create!(
        salepointable: album_1,
        store: store,
        variable_price: store.default_variable_price,
        has_rights_assignment: store.needs_rights_assignment
      )
    end
  end

  def create_person_plan(plan_id)
    PersonPlan.create(person: user, plan: Plan.find_by(id: plan_id))
  end

  def add_primary_artist!
    new_artist = create(:artist)

    create(:creative,
      artist: new_artist,
      creativeable: album_1,
      role: "primary_artist",
      person: user
    )
  end
end
