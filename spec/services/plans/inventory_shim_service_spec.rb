require "rails_helper"

describe Plans::InventoryShimService do
  let(:person) { create(:person) }

  context "user by plan status" do 
    let(:release) { create(:album, person: person) }
    let(:usage) { create(:plan_credit_usage, related: release, person: person, applies_to_type: release.album_type) }
    let(:purchase) { create(:purchase, related: usage, person: person) }
    let(:purchases) { [purchase] }
    subject { described_class.new(person, purchases) }
    
    before do 
      allow_any_instance_of(described_class).to receive(:create_inventories).and_return(:true)
    end 

    context "legacy user" do
      it "creates NO inventories" do
        expect(subject).not_to receive(:create_inventories)
        subject.call
      end
    end

    context "user purchasing a plan" do
      before { allow(plan_purchase).to receive(:is_plan?).and_return(true) }
      let(:plan_purchase) { instance_double("Purchase") }
      let(:purchases) { [purchase, plan_purchase] }
      
      it "creates inventories" do 
        expect(subject).to receive(:create_inventories).exactly(purchases.count).times
        subject.call
      end
    end 

    context "user has plan" do 
      before { create(:person_plan, person: person) }
      
      it "creates inventories" do
        expect(subject).to receive(:create_inventories).exactly(purchases.count).times
        subject.call
      end
    end 
  end
    
    
    
  context "plan user" do 
    before { create(:person_plan, person: person) }

    truthy_attrs = %i[
      inventory_type
      person_id
      product_item_id
      purchase_id
      quantity
      title
    ]

    false_attrs = %i[
      quantity_adjustment
      quantity_used
    ] 
    context "inventory release type" do
      it "creates inventories with correct attributes" do
        ["Album", "Single", "Ringtone"].each do |type|
          release   = create(type.downcase.to_sym, person: person)
          usage     = create(:plan_credit_usage, related: release, person: person, applies_to_type: type)
          purchase  = create(:purchase, related: usage, person: person)
          purchases = [purchase]
          unlimited = type == "Album"

          described_class.new(person, purchases).call

          inventories = Inventory.where(purchase_id: purchase.id)
          expect(inventories.count).to eq 3

          release_inv, salepoint_inv, song_inv = inventories

          [
            release_inv,
            song_inv,
            salepoint_inv
          ].each do |inventory|
            truthy_attrs.each { |a| expect(inventory[a]).to be_truthy }
            false_attrs.each { |a| expect(inventory[a].zero?).to be true }

            product_item = ProductItem.find(inventory.product_item_id)
            expect(product_item.name).to eq release_inv.title
          end

          expect(release_inv.title).to eq "#{type} Distribution"
          expect(release_inv.parent_id).to be_nil
          expect(release_inv.unlimited).to be false

          expect(song_inv.title).to eq "Song"
          expect(song_inv.parent_id).to eq release_inv.id
          expect(song_inv.unlimited).to be unlimited

          expect(salepoint_inv.title).to eq "Salepoint"
          expect(salepoint_inv.parent_id).to eq release_inv.id
          expect(salepoint_inv.unlimited).to be true
        end
      end

      it "notifies Airbrake of a rollback" do
        type = "Album"
        release   = create(type.downcase.to_sym, person: person)
        usage     = create(:plan_credit_usage, related: release, person: person, applies_to_type: type)
        purchase  = create(:purchase, related: usage, person: person)
        purchases = [purchase]

        allow_any_instance_of(Inventory).to receive(:save!)
          .and_raise(ActiveRecord::ActiveRecordError)

        expect(Airbrake).to receive(:notify).with("Plans::InventoryShimService", { purchase: purchase })

        described_class.new(person, purchases).call
      end
    end
  end
end
