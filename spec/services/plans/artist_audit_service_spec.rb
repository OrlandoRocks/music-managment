require "rails_helper"

describe Plans::ArtistAuditService do
  let!(:person) { create(:person) }

  subject { described_class.new(person).current_required_artist_count }

  describe "#current_required_artist_count," do
    before(:each) do
      create(:album, :with_creatives, :finalized, :paid, person: person)
      cart_album = create(:album, :with_creatives, person: person)
      product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
      Product.add_to_cart(cart_album.person, cart_album, product)
    end
    it "counts albums in the cart and albums that are finalized" do
      expect(subject).to eq(2)
    end
  end
end
