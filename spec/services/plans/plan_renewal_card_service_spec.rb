require "rails_helper"

describe Plans::PlanRenewalCardService do
  subject { Plans::PlanRenewalCardService.new(person) }

  describe "methods" do
    let!(:person) { create(:person) }

    context "with plan" do
      let!(:person_plan) { create(:person_plan, person: person) }
      let!(:plan_products_ids) { PlansProduct.where(plan_id: 4).pluck(:product_id) }
      let!(:plan_product) do
        Product.find_by(id: plan_products_ids, country_website_id: person.country_website_id)
      end

      let!(:purchase) do
        create(:purchase,
          product: plan_product,
          related: plan_product,
          person: person,
          cost_cents: 3500)
      end

      let!(:product_item) { ProductItem.find_by(product_id: plan_product.id) }
      let!(:renewal) do
        create(:renewal,
          person: person,
          purchase: purchase,
          item_to_renew: product_item)
      end

      before do
        create(:renewal_item, renewal: renewal, related: person_plan)
      end

      context "with active plan" do
        it "returns correct values" do

          methods = %i[
            currency
            plan
            plan_user?
            renewal
            renewal_date
            renewal_price
            plan_renewals
          ]

          results = methods.each_with_object({}) { |method, h| h[method] = subject.send(method) }

          expect(results[:currency]).to        eq "USD"
          expect(results[:plan]).to            be_an_instance_of(Plan)
          expect(results[:plan_user?]).to      eq true
          expect(results[:renewal]).to         be_an_instance_of(Renewal)
          expect(results[:renewal_date]).to    eq Date.today() + 1.year
          expect(results[:renewal_price]).to   eq "$#{plan_product.price}"
          expect(results[:plan_renewals].length).to eq 1
        end
      end

      context "with expired plan" do
        before do
          renewal.renewal_history.first.update!(starts_at: Date.today() - 2.days,
                                                expires_at: Date.today() - 1.day)
        end

        it "returns correct values" do
          expect(subject.plan_user?).to eq true
        end
      end
    end

    context "without plan" do
      it "returns correct values" do
        expect(subject.plan_user?).to eq false
      end
    end
  end
end
