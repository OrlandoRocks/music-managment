require "rails_helper"

describe Plans::CanDoService do


  describe "methods" do
    describe "#can_do?" do
      let!(:featured_plan) { Plan.first }
      let!(:featureless_plan) { Plan.last }
      let!(:plan_feature) { create(:plan_feature) }
      let!(:plan) { create(:plans_plan_feature, plan: featured_plan, plan_feature: plan_feature) }

      it "returns true for plan with the feature" do
        expect(Plans::CanDoService.can_do?(featured_plan, plan_feature.feature.to_sym)).to be(true)
      end
      it "returns false for plan without the feature" do
        expect(Plans::CanDoService.can_do?(featureless_plan, plan_feature.feature.to_sym)).to be(false)
      end
      it "raises error when not given symbol" do
        expect{Plans::CanDoService.can_do?(featureless_plan, plan_feature.feature)}.to raise_error(Plans::CanDoService::NotSymbolFeatureError)
      end
    end
  end
end
