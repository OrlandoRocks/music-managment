require "rails_helper"

describe Plans::StoreAutomatorTakedownService do
  let!(:person) { create(:person) }
  let!(:album){ create(:album, :paid, :live, :approved, :with_salepoints, person: person) }
  let!(:person_plan) { create(:person_plan, person: person, plan: Plan.last) }
  let!(:salepoint_subscription) { create(:salepoint_subscription, album: album, is_active: true) }

  subject { described_class.call(person) }

  context "no user" do
    it "raises a NoUserError" do
      expect{ described_class.call(nil) }.to raise_error(Plans::StoreAutomatorTakedownService::NoUserError)
    end
  end


  context "Valid Inputs and user with salepoint subscriptions" do
    it "deactivates the salepoint_subscription" do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
      salepoint_subscription.finalize!

      subject
      salepoint_subscription.reload

      expect(salepoint_subscription.is_active).to be false
    end
  end

  context "Valid Inputs and user with no salepoint subscriptions" do
    it "does nothing & does not raise an error" do
      salepoint_subscription.destroy!

      expect { subject }.to_not raise_error
    end
  end
end
