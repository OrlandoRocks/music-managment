require "rails_helper"

describe Plans::CancelAdditionalArtistService do
  describe "#cancel_unrenewed_artists" do
    context "albums taken down" do
      let!(:person) { create(:person) }
      let!(:plan_products_ids) { PlansProduct.where(plan_id: 4).pluck(:product_id) }
      let!(:plan_product) do
        Product.find_by(id: plan_products_ids, country_website_id: person.country_website_id)
      end
      let!(:product_item) { ProductItem.find_by(product_id: plan_product.id) }
      let!(:person_plan) { create(:person_plan, person: person, plan: Plan.find(Plan::PROFESSIONAL)) }
      let!(:renewal) do
        create(:renewal,
               person: person,
               item_to_renew: product_item)
      end
      let!(:renewal_item) {create(:renewal_item, renewal: renewal, related: person_plan)}
      let!(:product) { renewal.item_to_renew.renewal_product }
      let!(:creative1) { {name: "Star Laser Beam", role: Creative::PRIMARY_ARTIST_ROLE}.with_indifferent_access }
      let!(:creative2) { {name: "World Sized Drill", role: Creative::PRIMARY_ARTIST_ROLE}.with_indifferent_access }
      let!(:creative3) { {name: "Drill Sized Star", role: Creative::PRIMARY_ARTIST_ROLE}.with_indifferent_access }
      let!(:album1) { create(:album, :paid, :with_salepoints, creatives: [creative1], person: person) }
      let!(:album2) { create(:album, :paid, :with_salepoints, creatives: [creative2], person: person) }
      let!(:album3) { create(:album, :paid, :with_salepoints, creatives: [creative3], person: person) }
      let!(:addtl_artist_1) { create(:plan_addon, :additional_artist_addon, :paid, person: person) }
      let!(:addtl_artist_2) { create(:plan_addon, :additional_artist_addon, :paid, person: person) }

      before(:each) do
        create(:petri_bundle, album: album3)
        album3.takedown!
      end

      it "cancels taken down, un-renewed additional artists" do
        purchases = Plans::RenewAdditionalArtistsService.plan_and_additional_artist_purchases!(person, renewal, product)
        invoice = create(:invoice, purchases: purchases, person: person)
        purchases.each do |purchase|
          invoice.settlement_received(purchase, purchase.purchase_total_cents)
        end
        invoice.settled!

        expect(person.additional_artists.paid.active.count).to eq(2)
        Plans::CancelAdditionalArtistService.cancel_unrenewed_artists(person, invoice.purchases)
        expect(person.additional_artists.paid.active.count).to eq(1)
      end

      it "does not take down active, renewed additional artists" do
        purchases = Plans::RenewAdditionalArtistsService.plan_and_additional_artist_purchases!(person, renewal, product)
        invoice = create(:invoice, purchases: purchases, person: person)
        purchases.each do |purchase|
          invoice.settlement_received(purchase, purchase.purchase_total_cents)
        end
        invoice.settled!

        expect(person.additional_artists.paid.active.count).to eq(2)
        Plans::CancelAdditionalArtistService.cancel_unrenewed_artists(person, invoice.purchases)

        renewed_additional_artist_id = person.additional_artists.paid.active.pluck(:id)
        expect(renewed_additional_artist_id).to eq(invoice.purchases.active_additional_artists.pluck(:related_id))
      end
    end
  end
end
