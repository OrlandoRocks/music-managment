require "rails_helper"

describe Plans::SufficientPlanService do
  let!(:person) { create(:person) }

  before do
    allow(FeatureFlipper).to receive(:show_feature?) do |arg|
      arg == :plans_pricing
    end
  end

  context "legacy user," do
    subject { described_class.new(person, plan, :foo) }

    let!(:plan) { nil }

    it "returns false" do
      expect(subject.call).to be false
    end
  end

  context ":combined scope," do
    subject { described_class.new(person, plan, :combined) }

    context "new artist plan," do
      let!(:person_plan) { create(:person_plan, person: person, plan: Plan.first) }
      let!(:plan) { person_plan.plan }

      context "store checks," do
        context "with no releases," do
          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "with a social-store-only release," do
          let!(:album) { create(:album, :approved, :social_only, person: person) }

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        # This is a bad state because the new_artist user can't have purchased digital stores.
        #
        # The test only ensures that the combined scope checks paid records and the cart.
        context "with a digital stores release," do
          let!(:album) { create(:album, :approved, :with_salepoints, person: person) }

          it "returns false" do
            expect(subject.call).to be false
          end
        end

        context "with a carted digital stores release," do
          let!(:album) { create(:album, :with_salepoints, person: person) }
          let(:plan_credit_usage) { create(:plan_credit_usage, related: album, person: person) }

          before do
            create(:purchase, :unpaid, related: plan_credit_usage, person: person)
          end

          it "returns false" do
            expect(subject.call).to be false
          end
        end

        context "with a rejected digital stores release," do
          let!(:album) { create(:album, :paid, :with_salepoints, :rejected, person: person) }

          it "returns true" do
            expect(subject.call).to be true
          end
        end
      end # store checks

      context "artist checks," do
        context "with one artist," do
          let!(:album) { create(:album, :approved, :social_only, person: person) }

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        # This is a bad state because the new_artist user can't have purchased multiple artists.
        #
        # The test only ensures that the cart scope is ignored,
        # and instead the combined scope is used for artist counting.
        context "with multiple artists," do
          let!(:album) { create(:album, :approved, :social_only, :with_creatives, person: person) }

          it "returns false" do
            expect(subject.call).to be false
          end
        end

        context "with multiple artists with the same name," do
          before do
            album   = create(:album, :approved, :social_only, person: person)
            album_2 = create(:album, :approved, :social_only, person: person)
            album_2.creatives.first.update(name: album.creatives.first.name)
          end

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "with carted multiple artists," do
          let!(:album) { create(:album, :social_only, :with_creatives, person: person) }
          let(:plan_credit_usage) { create(:plan_credit_usage, related: album, person: person) }

          before do
            create(:purchase, :unpaid, related: plan_credit_usage, person: person)
          end

          it "returns false" do
            expect(subject.call).to be false
          end
        end
      end # artist checks

      context "feature checks," do
        context "#scheduled_release? false," do
          let!(:album) { create(:album, :approved, :social_only, person: person) }

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "#scheduled_release? true for paid release," do
          let!(:album) { create(:album, :approved, :social_only, sale_date: Date.tomorrow, person: person) }

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "#scheduled_release? true for carted release," do
          let!(:album) { create(:album, :social_only, sale_date: Date.tomorrow, person: person) }
          let(:plan_credit_usage) { create(:plan_credit_usage, related: album, person: person) }

          before do
            create(:purchase, :unpaid, related: plan_credit_usage, person: person)
          end

          it "returns false" do
            expect(subject.call).to be false
          end
        end

        context "with a carted automator product," do
          before do
            create(:purchase, :unpaid, related_type: SalepointSubscription.name, person: person)
          end

          it "returns false" do
            expect(subject.call).to be false
          end
        end
      end # feature checks
    end # new artist plan

    context "professional" do
      let!(:person_plan) { create(:person_plan, person: person, plan: Plan.fourth) }
      let!(:plan) { person_plan.plan }

      context "store checks," do
        context "with no releases," do
          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "with a social-store-only release," do
          let!(:album) { create(:album, :approved, :social_only, person: person) }

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "with a digital stores release," do
          let!(:album) { create(:album, :approved, :with_salepoints, person: person) }

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "with a carted digital stores release," do
          let!(:album) { create(:album, :with_salepoints, person: person) }
          let(:plan_credit_usage) { create(:plan_credit_usage, related: album, person: person) }

          before do
            create(:purchase, :unpaid, related: plan_credit_usage, person: person)
          end

          it "returns true" do
            expect(subject.call).to be true
          end
        end
      end # store checks

      context "artist checks," do
        context "with one artist," do
          let!(:album) { create(:album, :approved, :social_only, person: person) }

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "with multiple artists," do
          let!(:album) { create(:album, :approved, :social_only, :with_creatives, person: person) }

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "with carted multiple artists," do
          let!(:album) { create(:album, :social_only, :with_creatives, person: person) }
          let(:plan_credit_usage) { create(:plan_credit_usage, related: album, person: person) }

          before do
            create(:purchase, :unpaid, related: plan_credit_usage, person: person)
          end

          it "returns true" do
            expect(subject.call).to be true
          end
        end
      end # artist checks

      context "feature checks," do
        context "#scheduled_release? true for paid release," do
          let!(:album) { create(:album, :approved, :social_only, sale_date: Date.tomorrow, person: person) }

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "#scheduled_release? true for carted release," do
          let!(:album) { create(:album, :social_only, sale_date: Date.tomorrow, person: person) }
          let(:plan_credit_usage) { create(:plan_credit_usage, related: album, person: person) }

          before do
            create(:purchase, :unpaid, related: plan_credit_usage, person: person)
          end

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "with a carted automator product," do
          before do
            create(:purchase, :unpaid, related_type: SalepointSubscription.name, person: person)
          end

          it "returns true" do
            expect(subject.call).to be true
          end
        end
      end # feature checks
    end # professional
  end # :combined scope

  context ":cart scope" do
    subject { described_class.new(person, plan, :cart) }

    context "new artist plan," do
      let!(:person_plan) { create(:person_plan, person: person, plan: Plan.first) }
      let!(:plan) { person_plan.plan }

      context "store checks," do
        context "with no releases," do
          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "with a social-store-only release," do
          let!(:album) { create(:album, :approved, :social_only, person: person) }

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        # This is a bad state because the new_artist user can't have purchased digital stores.
        #
        # The test only ensures that the cart scope only checks the cart.
        context "with a digital stores release," do
          let!(:album) { create(:album, :approved, :with_salepoints, person: person) }

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "with a carted digital stores release," do
          let!(:album) { create(:album, :with_salepoints, person: person) }
          let(:plan_credit_usage) { create(:plan_credit_usage, related: album, person: person) }

          before do
            create(:purchase, :unpaid, related: plan_credit_usage, person: person)
          end

          it "returns false" do
            expect(subject.call).to be false
          end
        end
      end # store checks

      context "artist checks," do
        context "with one artist," do
          let!(:album) { create(:album, :approved, :social_only, person: person) }

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        # This is a bad state because the new_artist user can't have purchased multiple artists.
        #
        # The test only ensures that the cart scope is ignored,
        # and instead the combined scope is used for artist counting.
        context "with multiple artists," do
          let!(:album) { create(:album, :approved, :social_only, :with_creatives, person: person) }

          it "returns false" do
            expect(subject.call).to be false
          end
        end

        context "with multiple artists with the same name," do
          before do
            album   = create(:album, :approved, :social_only, person: person)
            album_2 = create(:album, :approved, :social_only, person: person)
            album_2.creatives.first.update(name: album.creatives.first.name)
          end

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "with carted multiple artists," do
          let!(:album) { create(:album, :social_only, :with_creatives, person: person) }
          let(:plan_credit_usage) { create(:plan_credit_usage, related: album, person: person) }

          before do
            create(:purchase, :unpaid, related: plan_credit_usage, person: person)
          end

          it "returns false" do
            expect(subject.call).to be false
          end
        end
      end # artist checks

      context "feature checks," do
        context "#scheduled_release? true for paid release," do
          let!(:album) { create(:album, :approved, :social_only, sale_date: Date.tomorrow, person: person) }

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "#scheduled_release? true for carted release," do
          let!(:album) { create(:album, :social_only, sale_date: Date.tomorrow, person: person) }
          let(:plan_credit_usage) { create(:plan_credit_usage, related: album, person: person) }

          before do
            create(:purchase, :unpaid, related: plan_credit_usage, person: person)
          end

          it "returns false" do
            expect(subject.call).to be false
          end
        end

        context "with a carted automator product," do
          before do
            create(:purchase, :unpaid, related_type: SalepointSubscription.name, person: person)
          end

          it "returns false" do
            expect(subject.call).to be false
          end
        end

        context "#custom_label? true for carted release," do
          let!(:album) { create(:album, :social_only, person: person) }
          let(:plan_credit_usage) { create(:plan_credit_usage, related: album, person: person) }

          before do
            album.update(label_name: "foo")
            create(:purchase, :unpaid, related: plan_credit_usage, person: person)
          end

          it "returns false" do
            expect(subject.call).to be false
          end
        end

        context "#recording_location? true for carted release," do
          let!(:album) { create(:album, :social_only, person: person, recording_location: "foo") }
          let(:plan_credit_usage) { create(:plan_credit_usage, related: album, person: person) }

          before do
            create(:purchase, :unpaid, related: plan_credit_usage, person: person)
          end

          it "returns false" do
            expect(subject.call).to be false
          end
        end

        context "#territory_restrictions? true for carted release," do
          let!(:album) { create(:album, :social_only, person: person) }
          let(:plan_credit_usage) { create(:plan_credit_usage, related: album, person: person) }

          before do
            AlbumCountry.create(
              album: album, country: Country.where(is_sanctioned: false).first,
              relation_type: "exclude"
            )
            create(:purchase, :unpaid, related: plan_credit_usage, person: person)
          end

          it "returns false" do
            expect(subject.call).to be false
          end
        end

        context "#upc? true for carted release," do
          let!(:album) { create(:album, :social_only, person: person) }
          let(:plan_credit_usage) { create(:plan_credit_usage, related: album, person: person) }

          before do
            create(:purchase, :unpaid, related: plan_credit_usage, person: person)
            create(:optional_upc, upcable: album)
          end

          it "returns false" do
            expect(subject.call).to be false
          end
        end
      end # feature checks
    end # new artist plan

    context "professional" do
      let!(:person_plan) { create(:person_plan, person: person, plan: Plan.fourth) }
      let!(:plan) { person_plan.plan }

      context "store checks," do
        context "with no releases," do
          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "with a social-store-only release," do
          let!(:album) { create(:album, :approved, :social_only, person: person) }

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "with a digital stores release," do
          let!(:album) { create(:album, :approved, :with_salepoints, person: person) }

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "with a carted digital stores release," do
          let!(:album) { create(:album, :with_salepoints, person: person) }
          let(:plan_credit_usage) { create(:plan_credit_usage, related: album, person: person) }

          before do
            create(:purchase, :unpaid, related: plan_credit_usage, person: person)
          end

          it "returns true" do
            expect(subject.call).to be true
          end
        end
      end # store checks

      context "artist checks," do
        context "with one artist," do
          let!(:album) { create(:album, :approved, :social_only, person: person) }

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "with multiple artists," do
          let!(:album) { create(:album, :approved, :social_only, :with_creatives, person: person) }

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "with carted multiple artists," do
          let!(:album) { create(:album, :social_only, :with_creatives, person: person) }
          let(:plan_credit_usage) { create(:plan_credit_usage, related: album, person: person) }

          before do
            create(:purchase, :unpaid, related: plan_credit_usage, person: person)
          end

          it "returns true" do
            expect(subject.call).to be true
          end
        end
      end # artist checks

      context "feature checks," do
        context "#scheduled_release? false," do
          let!(:album) { create(:album, :approved, :social_only, person: person) }

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "#scheduled_release? true," do
          let!(:album) { create(:album, :approved, :social_only, sale_date: Date.tomorrow, person: person) }

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "with a carted automator product," do
          before do
            create(:purchase, :unpaid, related_type: SalepointSubscription.name, person: person)
          end

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "#custom_label? true for carted release," do
          let!(:album) { create(:album, :social_only, person: person) }
          let(:plan_credit_usage) { create(:plan_credit_usage, related: album, person: person) }

          before do
            album.update(label_name: "foo")
            create(:purchase, :unpaid, related: plan_credit_usage, person: person)
          end

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "#recording_location? true for carted release," do
          let!(:album) { create(:album, :social_only, person: person, recording_location: "foo") }
          let(:plan_credit_usage) { create(:plan_credit_usage, related: album, person: person) }

          before do
            create(:purchase, :unpaid, related: plan_credit_usage, person: person)
          end

          it "returns true" do
            expect(subject.call).to be true
          end
        end

        context "#territory_restrictions? true for carted release," do
          let!(:album) { create(:album, :social_only, person: person) }
          let(:plan_credit_usage) { create(:plan_credit_usage, related: album, person: person) }

          before do
            AlbumCountry.create(
              album: album, country: Country.where(is_sanctioned: false).first,
              relation_type: "exclude"
            )
            create(:purchase, :unpaid, related: plan_credit_usage, person: person)
          end

          it "returns false" do
            expect(subject.call).to be true
          end
        end

        context "#upc? true for carted release," do
          let!(:album) { create(:album, :social_only, person: person) }
          let(:plan_credit_usage) { create(:plan_credit_usage, related: album, person: person) }

          before do
            create(:purchase, :unpaid, related: plan_credit_usage, person: person)
            create(:optional_upc, upcable: album)
          end

          it "returns true" do
            expect(subject.call).to be true
          end
        end
      end # feature checks
    end # professional
  end # :cart scope

  describe "feature-flagged feature gating" do
    before do
      allow(FeatureFlipper).to receive(:show_feature?)
        .with(:plans_pricing, instance_of(Person))
        .and_return false
    end

    subject { described_class.new(person, plan, :cart) }

    context "new artist plan," do
      let!(:person_plan) { create(:person_plan, person: person, plan: Plan.first) }
      let!(:plan) { person_plan.plan }

      context "#recording_location? true for carted release," do
        let!(:album) { create(:album, :social_only, person: person, recording_location: "foo") }
        let(:plan_credit_usage) { create(:plan_credit_usage, related: album, person: person) }

        before do
          create(:purchase, :unpaid, related: plan_credit_usage, person: person)
        end

        it "returns true" do
          expect(subject.call).to be true
        end
      end
    end
  end # feature-flagged feature gating
end
