require "rails_helper"

describe Plans::DspTakedownService do
  let!(:person) {create(:person)}
  let!(:album){create(:album, :paid, :live, :approved, :with_salepoints, person: person)}
  let!(:stores_to_remove){[]}
  subject { described_class.call(person, stores_to_remove) }

  context "Invalid values for stores_to_remove" do

    context "nil stores to remove" do
        let!(:stores_to_remove){nil}
        it "replaces with empty array" do
            expect{ subject }.to_not raise_error(NoMethodError)
            expect(described_class.new(person, stores_to_remove).stores_to_remove).to eq([])
        end
    end
    
    context "boolean true" do
        let!(:stores_to_remove){true}
        it "raises error" do
            expect{ subject }.to raise_error(Plans::DspTakedownService::InvalidStoreIdsError)
        end
    end
    
    context "boolean true" do
        let!(:stores_to_remove){false}
        it "raises error" do
            expect{ subject }.to raise_error(Plans::DspTakedownService::InvalidStoreIdsError)
        end
    end

    context "string" do
        let!(:stores_to_remove){"nah"}
        it "raises" do
            expect{ subject }.to raise_error(Plans::DspTakedownService::InvalidStoreIdsError)
        end
    end

    context "hash" do
        let!(:stores_to_remove){{}}
        it "replaces with empty array" do
            expect{ subject }.to raise_error(Plans::DspTakedownService::InvalidStoreIdsError)
        end
    end

    context "other object" do
        let!(:stores_to_remove){create(:album)}
        it "replaces with empty array" do
            expect{ subject }.to raise_error(Plans::DspTakedownService::InvalidStoreIdsError)
        end
    end
  end

  context "no user" do
    let!(:person) {nil}
    let!(:album) {nil}
    it "raises error" do
        expect{ subject }.to raise_error(Plans::DspTakedownService::NoUserError)
    end
  end

  context "Valid Inputs and user with releases" do
    

    context "in one of the specified stores" do
        let(:stores_to_remove){[41,42]}

        it "calls Salepoint#takedown! and Salepoint#send_sns_notification_takedown" do
            takedown_salepoint_count = 0
            sns_salepoint_count = 0
            allow_any_instance_of(Salepoint).to receive(:takedown!) {takedown_salepoint_count += 1}
            allow_any_instance_of(Salepoint).to receive(:send_sns_notification_takedown) {sns_salepoint_count += 1}
            subject
            expect(takedown_salepoint_count).to eq(2)
            expect(sns_salepoint_count).to eq(2)
        end

        it "takes down the release from the specified stores" do
            subject
            expect(person.salepoints.where(takedown_at:nil).length).to eq(2)
        end
    end

    context "not in one of the specified stores" do
        let(:stores_to_remove){[51,53]}
        it "does not takedown release from any stores" do
            expect_any_instance_of(Plans::DspTakedownService).to receive(:call)
            subject
            expect(person.salepoints.where(takedown_at:nil).length).to eq(4)
        end

        it "does not call Salepoint#takedown! or Salepoint#send_sns_notification_takedown " do
            expect_any_instance_of(Salepoint).to_not receive(:takedown!)
            expect_any_instance_of(Salepoint).to_not receive(:send_sns_notification_takedown)
            subject
        end
    end

    context "with empty array of stores to remove" do
        it "does not takedown release from any stores" do
            expect_any_instance_of(Plans::DspTakedownService).to receive(:call)
            subject
            expect(person.salepoints.length).to eq(4)
        end

        it "does not call Salepoint#takedown! or Salepoint#send_sns_notification_takedown " do
            expect_any_instance_of(Salepoint).to_not receive(:takedown!)
            expect_any_instance_of(Salepoint).to_not receive(:send_sns_notification_takedown)
            subject
        end
    end

    context "testing db transaction" do
        let(:stores_to_remove){[41,42]}
        context "no errors" do
            receive_count = 0
            it "persists db records" do
                allow_any_instance_of(Salepoint).to receive(:send_sns_notification_takedown) do
                    receive_count += 1
                end
                expect { subject }.to_not raise_error
                expect(person.salepoints.where(takedown_at:nil).length).to eq(2)
                expect(receive_count).to eq(2)
            end
        end

        context "raises error" do
            receive_count = 0
            it "rolls back" do
                allow_any_instance_of(Salepoint).to receive(:send_sns_notification_takedown) do
                    receive_count += 1
                    # allow first call to go through normally
                    # and raise error on second call
                    if receive_count > 1
                        raise "ERROR!"
                    end
                end
                expect { subject }.to raise_error
                expect(person.salepoints.where(takedown_at:nil).length).to eq(4)
                expect(receive_count).to eq(2)
            end
        end
    end
end
end
