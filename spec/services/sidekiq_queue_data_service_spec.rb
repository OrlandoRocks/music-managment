require "rails_helper"

describe SidekiqQueueDataService do
  describe "#number_by_store_in_queue" do
    it "shows the number on a specific queue by store short name" do
      distros = [
        create(:distribution, :with_salepoint, :enqueued, store_short_name: "M_Island"),
        create(:distribution, :with_salepoint, :enqueued, store_short_name: "M_Island"),
        create(:distribution, :with_salepoint, :enqueued, store_short_name: "7Digital"),
        create(:track_monetization, state: "enqueued", store: Store.find_by(short_name: "FBTracks"))
      ]

      jobs = distros.map do |distro|
        double(:job, args: [ distro.class.name, distro.id ])
      end

      allow(Sidekiq::Queue).to receive(:new).with("delivery-pause").and_return(jobs)
      distros_by_store = SidekiqQueueDataService.new(2).number_by_store_in_queue("delivery-pause")

      expect(distros_by_store["M_Island"]).to eq(2)
      expect(distros_by_store["7Digital"]).to eq(1)
      expect(distros_by_store["FBTracks"]).to eq(1)
    end
  end

  describe "#number_by_store_in_workers" do
    it "shows the number on a specific queue by store short name" do
      distros = [
        create(:distribution, :with_salepoint, :enqueued, store_short_name: "M_Island"),
        create(:distribution, :with_salepoint, :enqueued, store_short_name: "M_Island"),
        create(:distribution, :with_salepoint, :enqueued, store_short_name: "7Digital"),
        create(:track_monetization, state: "enqueued", store: Store.find_by(short_name: "FBTracks"))
      ]

      jobs = distros.map do |distro|
        [
          nil,
          nil,
          {
            "queue" => "delivery-default",
            "payload" => {
              "args" => [distro.class.name, distro.id]
            }
          }
        ]
      end

      allow(Sidekiq::Workers).to receive(:new).and_return(jobs)
      distros_by_store = SidekiqQueueDataService.new.number_by_store_in_workers

      expect(distros_by_store["M_Island"]).to eq(2)
      expect(distros_by_store["7Digital"]).to eq(1)
      expect(distros_by_store["FBTracks"]).to eq(1)
    end
  end

  describe "#distro_jobs_by_store_in_queue" do
    it "shows the number on a specific queue by store short name" do
      distros = [
        create(:distribution, :with_salepoint, :enqueued, store_short_name: "M_Island"),
        create(:distribution, :with_salepoint, :enqueued, store_short_name: "M_Island"),
        create(:distribution, :with_salepoint, :enqueued, store_short_name: "7Digital"),
        create(:track_monetization, state: "enqueued", store: Store.find_by(short_name: "FBTracks"))
      ]

      jobs = distros.map do |distro|
        double(:job, args: [ distro.class.name, distro.id ])
      end

      allow(Sidekiq::Queue).to receive(:new).with("delivery-pause").and_return(jobs)
      distros_and_jobs_by_store = SidekiqQueueDataService.new(2).distro_jobs_by_store_in_queue("delivery-pause")

      expect(distros_and_jobs_by_store["M_Island"][0][0]).to eq(distros[0])
      expect(distros_and_jobs_by_store["M_Island"].size).to eq(2)
      expect(distros_and_jobs_by_store["7Digital"][0][0]).to eq(distros[2])
      expect(distros_and_jobs_by_store["FBTracks"][0][0]).to eq(distros[3])
    end
  end

  describe "#distros_by_store_in_workers" do
    it "shows the number on a specific queue by store short name" do
      distros = [
        create(:distribution, :with_salepoint, :enqueued, store_short_name: "M_Island"),
        create(:distribution, :with_salepoint, :enqueued, store_short_name: "M_Island"),
        create(:distribution, :with_salepoint, :enqueued, store_short_name: "7Digital"),
        create(:track_monetization, state: "enqueued", store: Store.find_by(short_name: "FBTracks"))
      ]

      jobs = distros.map do |distro|
        [
          nil,
          nil,
          {
            "queue" => "delivery-default",
            "payload" => {
              "args" => [distro.class.name, distro.id]
            }
          }
        ]
      end

      allow(Sidekiq::Workers).to receive(:new).and_return(jobs)
      distros_and_jobs_by_store = SidekiqQueueDataService.new.distros_by_store_in_workers

      expect(distros_and_jobs_by_store["M_Island"][0][0]).to eq(distros[0])
      expect(distros_and_jobs_by_store["M_Island"].size).to eq(2)
      expect(distros_and_jobs_by_store["7Digital"][0][0]).to eq(distros[2])
      expect(distros_and_jobs_by_store["FBTracks"][0][0]).to eq(distros[3])
    end
  end
end