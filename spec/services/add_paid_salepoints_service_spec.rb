require "rails_helper"

describe AddPaidSalepointsService do
  describe ".call" do
    let(:person) { create(:person) }
    let(:album) { create(:album, :finalized) }
    let(:salepoint) { create(:salepoint, salepointable: album) }
    let(:salepoint_params) {
      {
        salepoint.store_id => {
          salepoint: {
            id: salepoint.id,
            store_id: salepoint.store_id,
            variable_price_id: nil
          }
        }
      }
    }

    before do
      create(:petri_bundle, album: album)
    end
    
    context "when apple music is not selected" do 
      
      let(:apple_music) { false }

      it "should invoke the SalepointService" do
        expect(SalepointService).to receive(:call).with(person, album, salepoint_params, apple_music)

        described_class.call(person, album, [salepoint], apple_music)
      end

      it "should finalize and mark salepoints as paid" do
        described_class.call(person, album, [salepoint], apple_music)

        expect(salepoint.reload.payment_applied).to be true
        expect(salepoint.finalized_at).not_to be nil
      end

      it "should not have added apple_music" do 
        described_class.call(person, album, [salepoint], apple_music)
        expect(album.reload.apple_music).to eq(false)
      end 
  
      it "should kick off a delivery for the salepoints" do
        expect(Delivery::Salepoint).to receive(:deliver).with(salepoint.id)
  
        described_class.call(person, album, [salepoint], apple_music)
      end
    end

    context "when apple music is selected" do 
      
      let(:apple_music) { true }

      it "should invoke the SalepointService" do
        expect(SalepointService).to receive(:call).with(person, album, salepoint_params, apple_music)

        described_class.call(person, album, [salepoint], apple_music)
      end

      it "should finalize and mark salepoints as paid" do
        described_class.call(person, album, [salepoint], apple_music)
  
        expect(salepoint.reload.payment_applied).to be true
        expect(salepoint.finalized_at).not_to be nil
      end

      it "should not have added apple_music" do 
        described_class.call(person, album, [salepoint], apple_music)
        expect(album.reload.apple_music).to eq(true)
      end 
  
      it "should kick off a delivery for the salepoints" do
        expect(Delivery::Salepoint).to receive(:deliver).with(salepoint.id)
  
        described_class.call(person, album, [salepoint])
      end
    end
  end
end
