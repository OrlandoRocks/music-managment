require "rails_helper"

describe CrtCache::Invalidator do
  before(:each) do
    allow(Believe::Person).to receive(:add_or_update).and_return(true)
    allow(Believe::PersonWorker).to receive(:perform_async).and_return(true)
  end

  describe ".invalidate_cache_if_changed (for an Album)" do
    before(:each) do
      @album = create(:album, orig_release_year: Time.now - 7.days, sale_date: Time.now, artwork: create(:artwork))
      @finalized_album = create(:album, :finalized)
      allow(FeatureFlipper).to receive(:show_feature?).with(:crt_cache, nil).and_return(true)
    end

    context "a field changes on an unfinalized album" do
      it "should not invalidate the album" do
        expect(Tunecore::Crt).not_to receive(:invalidate_album_in_cache)
        @album.finalized_at = nil
        @album.sale_date = Time.now
        @album.save
      end
    end

    context "an album becomes unfinalized" do
      it "should invalidate the album" do
        allow(FeatureFlipper).to receive(:show_feature?).with(:crt_cache, nil).and_return(true)
        expect(Tunecore::Crt).to receive(:invalidate_album_in_cache)
        @finalized_album.finalized_at = nil
        @finalized_album.save
      end
    end

    context "an observed field changes" do
      it "should invalidate the album" do
        expect(Tunecore::Crt).to receive(:invalidate_album_in_cache).with(@finalized_album).once

        sale_date = @finalized_album.sale_date
        @finalized_album.sale_date = sale_date + 1.day
        @finalized_album.save
      end

      it "should invalidate album referenced by album country record" do
        expect(Tunecore::Crt).to receive(:invalidate_album_in_cache).with(@album).once
        @album.finalized_at = Time.now
        @album.save
        create(:album_country, album: @album, country: Country.first, relation_type: "include")
      end
    end

    context "a non observed field changes" do
      it "should not invalidate the album" do
        expect(Tunecore::Crt).not_to receive(:invalidate_album_in_cache)
        @album.finalized_at = Time.now
        @album.known_live_on = Time.now
        @album.save
      end
    end
  end
end
