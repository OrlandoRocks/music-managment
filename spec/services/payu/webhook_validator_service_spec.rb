require "rails_helper"

describe Payu::WebhookValidatorService do
  subject { described_class.valid?(params) }

  context "valid params" do
    let!(:params) { valid_params }

    it "is valid" do
      expect(subject).to be true
    end
  end

  context "invalid hash" do
    let!(:params) { valid_params.merge({ hash: "foo" }) }

    it "is invalid" do
      expect(subject).to be false
    end
  end

  context "wrong amount" do
    let!(:params) { valid_params.merge({ amount: "foo" }) }

    it "is invalid" do
      expect(subject).to be false
    end
  end

  context "missing hash param" do
    let!(:params) { valid_params.except(:hash) }

    it "is invalid" do
      expect(subject).to be false
    end
  end

  def valid_params
    {
      "mihpayid"=>"403993715526293257",
      "mode"=>"CC",
      "status"=>"success",
      "key"=>"0k3tQh",
      "txnid"=>"11089047",
      "amount"=>"997.90",
      "addedon"=>"2022-05-27 03:39:10",
      "productinfo"=>"Your TuneCore Payment",
      "firstname"=>"mike",
      "lastname"=>"",
      "address1"=>"",
      "address2"=>"",
      "city"=>"",
      "state"=>"",
      "country"=>"",
      "zipcode"=>"",
      "email"=>"main@tc.com",
      "phone"=>"",
      "udf1"=>"",
      "udf2"=>"",
      "udf3"=>"",
      "udf4"=>"",
      "udf5"=>"",
      "udf6"=>"",
      "udf7"=>"",
      "udf8"=>"",
      "udf9"=>"",
      "udf10"=>"",
      "card_token"=>"",
      "card_no"=>"512345XXXXXX2346",
      "field0"=>"",
      "field1"=>"540651",
      "field2"=>"638509",
      "field3"=>"20220527",
      "field4"=>"0",
      "field5"=>"653212946853",
      "field6"=>"00",
      "field7"=>"AUTHPOSITIVE",
      "field8"=>"Approved or completed successfully",
      "field9"=>"No Error",
      "payment_source"=>"payu",
      "PG_TYPE"=>"CC-PG",
      "error"=>"E000",
      "error_Message"=>"No Error",
      "net_amount_debit"=>"997.9",
      "unmappedstatus"=>"captured",
      "hash"=>"e98725bb43527dd202948ad090aae609db2ff396292bda8e8b051546f0964b73685b693dc04f8c73b26560bd80949c4c5ab4320593603e6acd16e8820113a040",
      "bank_ref_no"=>"540651",
      "bank_ref_num"=>"540651",
      "bankcode"=>"CC",
      "surl"=>"http://dev3.tunecore.in/cart/finalize_after_redirect?invoice_id=11089047",
      "curl"=>"http://dev3.tunecore.in/cart/finalize_after_redirect?failed=true&invoice_id=11089047",
      "furl"=>"http://dev3.tunecore.in/cart/finalize_after_redirect?failed=true&invoice_id=11089047",
      "card_hash"=>"515a2cb0f0e6711f6a3d2c4704cc691d212d4dc0e065c7c8d3441a6b5fc23e97"
    }.with_indifferent_access
  end
end
