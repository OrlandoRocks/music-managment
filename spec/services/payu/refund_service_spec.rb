require "rails_helper"

RSpec.describe Payu::RefundService, type: :service do
  let(:invoice) { create(:invoice, :settled, :with_payu_settlement) }
  let(:refund) { create(:refund, invoice: invoice) }
  let(:payu_transaction) {
    invoice
      .invoice_settlements
      .find_by(source_type: "PayuTransaction")
      .source
  }

  describe "#process!" do
    context "success" do
      it "process PayU refund" do
        amount_cents = 10
        amount_in_local_currency =
          if invoice.pegged_rate.present?
            (amount_cents * invoice.pegged_rate).fdiv(100)
          else
            amount_cents.fdiv(100)
          end

        allow_any_instance_of(Payu::ApiClient)
          .to receive(:send_request)
          .with(:verify_refund, request_id: "132752849")
          .and_return(
            Payu::Responses::VerifyRefund.new(
              Faraday::Response.new(
                body: {
                  status: 1,
                  transaction_details: {
                    "132752849": {
                      "132752849": {
                        status: "success",
                        mode: "CC"
                      }
                    }
                  }
                }.to_json
              )
            )
          )

        allow_any_instance_of(Payu::ApiClient)
          .to receive(:send_request)
          .with(:refund_transaction, refund_id: refund.id, amount: amount_in_local_currency)
          .and_return(
            Payu::Responses::RefundTransaction.new(
              Faraday::Response.new(
                body: {
                  status: 1,
                  msg: "Refund Request Queued",
                  error_code: "102",
                  payu_id: "403993715524411419",
                  request_id: "132752849"
                }.to_json
              )
            )
          )

        transaction = Payu::RefundService.new(refund, amount_cents, payu_transaction).process!
        expect(transaction.status).to be(true)
        expect(transaction.error_code).to eq("102")
      end
    end

    context "failure" do
      it "raises PayuRefundTransactionError" do
        amount_cents = 10000
        amount_in_local_currency =
          if invoice.pegged_rate.present?
            (amount_cents * invoice.pegged_rate).fdiv(100)
          else
            amount_cents.fdiv(100)
          end

        allow_any_instance_of(Payu::ApiClient)
          .to receive(:send_request)
          .with(:refund_transaction, refund_id: refund.id, amount: amount_in_local_currency)
          .and_return(
            Payu::Responses::RefundTransaction.new(
              Faraday::Response.new(
                body: {
                  status: 0,
                  msg: "Refund FAILURE - Invalid amount",
                  error_code: "105",
                  payu_id: "403993715524411419"
                }.to_json
              )
            )
          )

        subject = Payu::RefundService.new(refund, amount_cents, payu_transaction)
        expect { subject.process! }.to raise_error(PayuTransaction::PayuRefundTransactionError)
        transaction = subject.payu_transaction
        expect(transaction.status).to be(false)
        expect(transaction.error_code).to eq("105")
      end
    end
  end
end
