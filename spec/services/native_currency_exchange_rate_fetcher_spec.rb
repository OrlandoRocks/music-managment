require "rails_helper"

RSpec.describe NativeCurrencyExchangeRateFetcher, type: :service do
  describe "#foreign_exchange_rate" do
    it "returns amount converted to native currency" do
      country = "ID"
      currency = "IDR"

      fx_rate = create(
        :foreign_exchange_rate,
        source_currency: "USD",
        target_currency: currency,
        exchange_rate: 14_645.3
      )

      expect(
        described_class.new(country_iso_code: country, source_currency: "USD").foreign_exchange_rate
      ).to eq(fx_rate)
    end

    it "raises ExchangeRateNotFoundError if exchange rate not found in db" do
      country = "ID"

      allow(ForeignExchangeRate)
        .to receive(:latest_by_currency!)
        .and_raise(ActiveRecord::RecordNotFound)

      expect {
        described_class.new(country_iso_code: country, source_currency: "USD").foreign_exchange_rate
      }.to raise_error NativeCurrencyExchangeRateFetcher::ExchangeRateNotFoundError
    end

    it "raises CountryNativeCurrencyMapNotFound if country not mapped with native currency" do
      country = "XX"

      expect {
        described_class.new(country_iso_code: country, source_currency: "USD").foreign_exchange_rate
      }.to raise_error NativeCurrencyExchangeRateFetcher::CountryNativeCurrencyMapNotFoundError
    end
  end
end
