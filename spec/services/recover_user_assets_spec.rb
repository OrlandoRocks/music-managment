require "rails_helper"

describe RecoverUserAssets do
  describe ".recover" do
    let(:person1) { create(:person) }
    let(:person2) { create(:person) }
    let(:album1) { create(:album, person: person2) }
    let(:recover_asset) { create(:recover_asset, person: person2, admin: person1, state: 'enqueued')}

    it "successfuly downloads and zips" do
      recover_user_assets = RecoverUserAssets.new(recover_asset.id)
      recover_user_assets.recover
      expect(recover_asset.reload.state).to eq('completed')
    end
  end
end
