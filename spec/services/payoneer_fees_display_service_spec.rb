require "rails_helper"

describe PayoneerFeesDisplayService do
  context "dual currency domain" do
    let!(:indian_person) { create(:person, :with_india_country_website) }
    let!(:service) { PayoneerFeesDisplayService.new(indian_person) }
    before(:each) do
      allow_any_instance_of(CurrencyHelper).to receive(:balance_to_currency).and_return("")
    end
    describe "#payout_type_compiler" do
      it "calls #balance_to_currency" do
        expect_any_instance_of(CurrencyHelper).to receive(:balance_to_currency).once
        service.payout_type_compiler(service.programs(PayoutProgram::PAYPAL))
      end
    end
    describe "#amount_cents_compiler" do
      it "calls #balance_to_currency" do
        expect_any_instance_of(CurrencyHelper).to receive(:balance_to_currency).once
        service.amount_cents_compiler(service.programs(PayoutProgram::PAYONEER))
      end
    end
    describe "#ach_amount_cents_compiler" do
      it "calls #balance_to_currency" do
        expect_any_instance_of(CurrencyHelper).to receive(:balance_to_currency).once
        service.ach_amount_cents_compiler(100)
      end
    end
  end
  context "single currency domain" do
    let!(:person) { create(:person) }
    let!(:service) { PayoneerFeesDisplayService.new(person) }
    describe "#payout_type_compiler" do
      it "calls #balance_to_currency" do
        expect_any_instance_of(CurrencyHelper).not_to receive(:balance_to_currency)
        service.payout_type_compiler(service.programs(PayoutProgram::PAYPAL))
      end
    end
    describe "#amount_cents_compiler" do
      it "calls #balance_to_currency" do
        expect_any_instance_of(CurrencyHelper).not_to receive(:balance_to_currency)
        service.amount_cents_compiler(service.programs(PayoutProgram::PAYONEER))
      end
    end
    describe "#ach_amount_cents_compiler" do
      it "calls #balance_to_currency" do
        expect_any_instance_of(CurrencyHelper).not_to receive(:balance_to_currency)
        service.ach_amount_cents_compiler(100)
      end
    end
  end
end
