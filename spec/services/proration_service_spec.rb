require "rails_helper"

include ActiveSupport::Testing::TimeHelpers

# Using the same album purchase cost for all tests
FIXED_ALBUM_COST = 4500

describe ProrationService do
  subject { ProrationService.new(person) }

  let(:vat_rate) { 10 }

  before do
    travel_to Time.local(2020)

    allow(FeatureFlipper).to receive(:show_feature?) do |arg|
      if arg.in? [:plans_pricing, :vat_tax]
        true
      else
        false
      end
    end

    allow_any_instance_of(TcVat::Calculator).to receive(:tax_rate).and_return(vat_rate)
  end

  after do
    travel_back
  end

  describe "#apply_proration_discounts!," do
    let!(:person) { create(:person) }
    let!(:plan_products_ids) { PlansProduct.where(plan_id: 4).pluck(:product_id) }
    let!(:plan_product) do
      Product.find_by(id: plan_products_ids, country_website_id: person.country_website_id)
    end

    let!(:artist_product) do
      Product.find_by(country_website_id: person.country_website_id,
                      display_name: "additional_artist")
    end

    context "scenario: first_plan," do
      context "who makes non-plan purchase," do
        let!(:album) { create(:album, person: person) }

        before do
          create(:purchase, :unpaid, related: album, person: person)
        end

        it "does NOT call #first_plan_discount," do
          expect(subject).not_to receive(:first_plan_discount)

          subject.apply_proration_discounts!
        end
      end

      context "with active renewals," do
        let!(:album) { create(:album, person: person) }
        let!(:invoice) { create(:invoice, person: person) }

        before do
          create(:purchase,
                 :unpaid,
                 product: plan_product,
                 related: plan_product,
                 person: person,
                 cost_cents: 3500)

          15.times do
            create(:purchase,
                   :unpaid,
                   product: artist_product,
                   related_id: artist_product.id,
                   related_type: "Product",
                   person: person)
          end

          purchase = create(:purchase,
                            person: person,
                            product: product,
                            related: album,
                            cost_cents: FIXED_ALBUM_COST,
                            invoice: invoice)
          renewal  = create(:renewal,
                            person: person,
                            item_to_renew: product.product_items.first,
                            purchase: purchase,
                            takedown_at: nil)
          create(:renewal_item, renewal: renewal, related: album)
        end

        context "when VAT is applicable" do
          context "with any number of years left on a renewal" do
            let(:album_products) { [Product::US_FIVE_YEAR_ALBUM_PRODUCT_ID, Product::US_TWO_YEAR_ALBUM_PRODUCT_ID, Product::US_ONE_YEAR_ALBUM_PRODUCT_ID].sample }
            let!(:product) { Product.find(album_products) }

            it "sets the vat_amount_cents for each purchase" do
              subject.apply_proration_discounts!

              expected_discounts = person.purchases.unpaid.map do |purchase|
                #same calculation vat services uses
                ((purchase.cost_cents - purchase.discount_cents) * vat_rate / 100.0).round
              end

              expect(person.purchases.unpaid.map(&:vat_amount_cents)).to eq(expected_discounts)
            end
          end
        end

        context "with 5 years left on a renewal," do
          context "purchases plan," do
            let!(:product) { Product.find(Product::US_FIVE_YEAR_ALBUM_PRODUCT_ID) }

            it "calls #first_plan_discount" do
              expect(subject).to receive(:first_plan_discount).and_return 0

              subject.apply_proration_discounts!
            end

            it "applies discounts based on renewals value," do
              subject.apply_proration_discounts!

              expect(person.purchases.unpaid.map(&:discount_cents)).to eq [
                3500, 99, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
              ]
            end

            context "when vat is applicable" do
              it "sets the vat_amount_cents for each purchase" do
                subject.apply_proration_discounts!

                expect(person.purchases.unpaid.map(&:vat_amount_cents)).to eq [
                 0, 90, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100
                ]
              end
            end
          end # with 5 year renewal

          context "method called twice," do
            let!(:product) { Product.find(Product::US_FIVE_YEAR_ALBUM_PRODUCT_ID) }

            it "applies discounts ONCE based on renewals value" do
              subject.apply_proration_discounts!

              expect(person.purchases.unpaid.map(&:discount_cents)).to eq [
                3500, 99, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
              ]

              subject.apply_proration_discounts!

              expect(person.purchases.unpaid.map(&:discount_cents)).to eq [
                3500, 99, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
              ]
            end
          end # method called twice
        end

        context "with 2 renewals, each with 5 years left," do
          context "purchases plan and album," do
            let!(:product) { Product.find(Product::US_FIVE_YEAR_ALBUM_PRODUCT_ID) }

            before do
              album_2  = create(:album, person: person)
              purchase = create(:purchase,
                                person: person,
                                product: product,
                                related: album_2,
                                cost_cents: FIXED_ALBUM_COST,
                                invoice: invoice)
              renewal  = create(:renewal,
                                person: person,
                                item_to_renew: product.product_items.first,
                                purchase: purchase,
                                takedown_at: nil)
              create(:renewal_item, renewal: renewal, related: album_2)
            end

            it "calls #first_plan_discount" do
              expect(subject).to receive(:first_plan_discount).and_return 0

              subject.apply_proration_discounts!
            end

            it "applies discounts based on renewals value" do
              subject.apply_proration_discounts!

              expect(person.purchases.unpaid.map(&:discount_cents)).to eq [
                3500, 999, 999, 999, 701, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
              ]
            end
          end
        end # with 2 5 year renewals

        context "with 2 renewals, each with 2 years left," do
          before do
            album_2  = create(:album, person: person)
            purchase = create(:purchase,
                              person: person,
                              product: product,
                              related: album_2,
                              cost_cents: FIXED_ALBUM_COST,
                              invoice: invoice)
            renewal  = create(:renewal,
                              person: person,
                              item_to_renew: product.product_items.first,
                              purchase: purchase,
                              takedown_at: nil)
            create(:renewal_item, renewal: renewal, related: album_2)
          end

          context "purchases plan," do
            let!(:product) { Product.find(Product::US_TWO_YEAR_ALBUM_PRODUCT_ID) }

            it "calls #first_plan_discount" do
              expect(subject).to receive(:first_plan_discount).and_return 0

              subject.apply_proration_discounts!
            end

            it "applies discounts based on renewals value" do
              subject.apply_proration_discounts!

              expect(person.purchases.unpaid.map(&:discount_cents)).to eq [
                3500, 994, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
              ]
            end

            it "should have a discount_reason for a discounted purchase" do
              subject.apply_proration_discounts!

              discounted_purchase = person.purchases.find {|p| !p.discount_cents.zero? }
              expect(discounted_purchase.discount_reason).to eq("pre_plan_proration")
            end
          end
        end # 2 distinct 2 year renewals

        context "with 2 years left on a renewal," do
          context "purchases plan," do
            let!(:product) { Product.find(Product::US_TWO_YEAR_ALBUM_PRODUCT_ID) }

            it "calls #first_plan_discount" do
              expect(subject).to receive(:first_plan_discount).and_return 0

              subject.apply_proration_discounts!
            end

            it "applies discounts based on renewals value" do
              subject.apply_proration_discounts!

              expect(person.purchases.unpaid.map(&:discount_cents)).to eq [
                2247, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
              ]
            end
          end
        end # 2 year renewal

        context "with an expired 2 year renewal," do
          context "purchases plan," do
            let!(:product) { Product.find(Product::US_TWO_YEAR_ALBUM_PRODUCT_ID) }

            before do
              rh = person.renewals.first.renewal_history.first
              rh.update!(starts_at: DateTime.now - 2.years, expires_at: DateTime.now - 1.year)
            end

            it "calls #first_plan_discount" do
              expect(subject).to receive(:first_plan_discount).and_return 0

              subject.apply_proration_discounts!
            end

            it "does not create discounts from expired renewals" do
              subject.apply_proration_discounts!

              expect(person.purchases.unpaid.map(&:discount_cents)).to eq [
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
              ]
            end

            it "should not have a discount_reason for purchases without a discount" do
              subject.apply_proration_discounts!

              purchase_without_discount = person.purchases.find {|p| p.discount_cents.zero? }
              expect(purchase_without_discount.no_discount?).to be true
            end
          end
        end # 2 year renewal

        context "with 1 year left on a renewal," do
          context "purchases plan," do
            let!(:product) { Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID) }

            it "calls #first_plan_discount" do
              expect(subject).to receive(:first_plan_discount).and_return 0

              subject.apply_proration_discounts!
            end

            it "applies NO discount" do
              subject.apply_proration_discounts!

              expect(person.purchases.unpaid.map(&:discount_cents)).to eq [
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
              ]
            end
          end
        end # 1 year renewal

        context "with a 'takendown' renewal," do
          context "purchases plan," do
            let!(:product) { Product.find(Product::US_TWO_YEAR_ALBUM_PRODUCT_ID) }

            before { person.renewals.first.update(takedown_at: DateTime.now) }

            it "calls #first_plan_discount" do
              expect(subject).to receive(:first_plan_discount).and_return 0

              subject.apply_proration_discounts!
            end

            it "applies discounts based on renewals value" do
              subject.apply_proration_discounts!

              expect(person.purchases.unpaid.map(&:discount_cents)).to eq [
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
              ]
            end
          end
        end # with a 'takendown' renewal
      end # plan purchase tests
    end # scenario: first_plan

    context "scenario: upgrading_plan," do
      let!(:product) do
        Product.find_by(id: plan_products_ids, country_website_id: person.country_website_id)
      end
      let!(:purchase) do
        create(:purchase, :unpaid, product: product, related: product, person: person)
      end
      let!(:rising_plan) { Plan.second }
      let!(:current_plan_purchase) do
        create(:purchase,
                product: rising_plan.products.where(country_website_id: 1).first,
                related: rising_plan.products.where(country_website_id: 1).first,
                person: person,
                cost_cents: 500)
      end

      before do
        create(:invoice, :settled, person: person, purchases: [current_plan_purchase])
      end

      context "when VAT is applicable" do
        before do
          create(:person_plan, plan: rising_plan, person: person)
          create(:renewal, :plan_renewal, person: person, expires_at: DateTime.now + 6.months)
        end

        context "when upgrading a plan with upgrade discount" do
          it "sets the vat_amount_cents for each purchase" do
            subject.apply_proration_discounts!

            expected_discounts = person.purchases.unpaid.map do |purchase|
              ((purchase.cost_cents - purchase.discount_cents) * vat_rate / 100.0).round
            end

            expect(person.purchases.unpaid.map(&:vat_amount_cents)).to eq(expected_discounts)
          end
        end
      end

      context "applies upgrade discount to upgrade purchase," do
        before do
          create(:person_plan, plan: rising_plan, person: person)
          create(:renewal, :plan_renewal, person: person, expires_at: Date.today + 6.months)
        end

        it "does NOT call #first_plan_discount" do
          expect(subject).not_to receive(:first_plan_discount)

          subject.apply_proration_discounts!
        end

        it "calls #upgrade_discount" do
          expect(subject).to receive(:upgrade_discount).and_return 0.0

          subject.apply_proration_discounts!
        end
      end

      context "applies upgrade discount based on current product price" do
        before do
          create(:person_plan, plan: rising_plan,
                               person: person)
          create(:renewal, :plan_renewal, person: person, expires_at: DateTime.now.beginning_of_day + (1.year - 1.day))
          current_plan_purchase.update(discount_cents: 1000)
        end

        it "calls #upgrade_discount" do
          subject.apply_proration_discounts!

          expect(person.purchases.unpaid.map(&:discount_cents)).to eq [1499]
        end
      end

      context "with 6 months left on plan," do
        before do
          create(:person_plan, plan: rising_plan, person: person)
          create(:renewal, :plan_renewal, person: person, expires_at: DateTime.now + 6.months)
        end

        it "discounts plan" do
          subject.apply_proration_discounts!

          expect(person.purchases.unpaid.map(&:discount_cents)).to eq [747]
        end

        context "with additional artists," do
          before do
            15.times do
              create(:purchase,
                    :unpaid,
                    product: artist_product,
                    related_id: artist_product.id,
                    related_type: "Product",
                    person: person)
            end
          end

          it "discounts ONLY the plan" do
            subject.apply_proration_discounts!

            expect(person.purchases.unpaid.map(&:discount_cents)).to eq [
              747, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
            ]
          end
        end # with additional artists
      end # with 6 months left

      context "with no time left on plan," do
        before do
          create(:person_plan, plan: rising_plan, person: person)
          create(:renewal, :plan_renewal, person: person, expires_at: DateTime.now)
        end

        it "discounts plan" do
          subject.apply_proration_discounts!

          expect(person.purchases.unpaid.map(&:discount_cents)).to eq [0]
        end
      end # with no time left

      context "with overdue plan," do
        before do
          create(:person_plan, plan: rising_plan, person: person)
          create(:renewal, :plan_renewal, person: person, expires_at: DateTime.now - 1.year)
        end

        it "discounts plan" do
          subject.apply_proration_discounts!

          expect(person.purchases.unpaid.map(&:discount_cents)).to eq [0]
        end
      end # with overdue plan

      context "method called twice," do
        before do
          create(:person_plan, plan: rising_plan, person: person)
          create(:renewal, :plan_renewal, person: person, expires_at: DateTime.now + 3.months)
        end

        it "applies discount once" do
          subject.apply_proration_discounts!
          subject.apply_proration_discounts!

          expect(person.purchases.unpaid.map(&:discount_cents)).to eq [374]
        end
      end # method called twice
    end # scenario: upgrading_plan

    context "scenario: only_adding_artists," do

      context "when VAT is applicable" do
        before do
          create(:person_plan, person: person)
          create(:renewal, :plan_renewal, person: person, expires_at: DateTime.now + 6.months)
          15.times do
            create(:purchase,
                  :unpaid,
                  product: artist_product,
                  related_id: artist_product.id,
                  related_type: "Product",
                  person: person)
            end
        end

        context "when upgrading a plan with upgrade discount" do
          it "sets the vat_amount_cents for each purchase" do
            subject.apply_proration_discounts!

            expected_discounts = person.purchases.unpaid.map do |purchase|
              ((purchase.cost_cents - purchase.discount_cents) * vat_rate / 100.0).round
            end

            expect(person.purchases.unpaid.map(&:vat_amount_cents)).to eq(expected_discounts)
          end
        end
      end

      context "applies artist discount to artist purchase," do
        before do
          create(:person_plan, person: person)
          create(:renewal, :plan_renewal, person: person, expires_at: DateTime.now + 6.months)
          15.times do
            create(:purchase,
                  :unpaid,
                  product: artist_product,
                  related_id: artist_product.id,
                  related_type: "Product",
                  person: person)
          end
        end

        it "does NOT call #first_plan_discount" do
          expect(subject).not_to receive(:first_plan_discount)

          subject.apply_proration_discounts!
        end

        it "calls #artist_discount" do
          expect(subject).to receive(:artist_discount).and_return 0.0

          subject.apply_proration_discounts!
        end
      end

      context "with non-multi-artist plan," do
        before do
          create(:person_plan, plan: Plan.first, person: person)
          create(:renewal, :plan_renewal, person: person, expires_at: DateTime.now + 6.months)
        end

        context "with additional artists," do
          before do
            15.times do
              create(:purchase,
                    :unpaid,
                    product: artist_product,
                    related_id: artist_product.id,
                    related_type: "Product",
                    person: person)
            end
          end

          it "does NOT discount the artists" do
            subject.apply_proration_discounts!

            expect(person.purchases.unpaid.map(&:discount_cents)).to eq [
              0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
            ]
          end
        end
      end

      context "with expired plan," do
        before do
          create(:person_plan, person: person)
          create(:renewal, :plan_renewal, person: person, expires_at: DateTime.now - 6.months)
        end

        context "with additional artists," do
          before do
            15.times do
              create(:purchase,
                    :unpaid,
                    product: artist_product,
                    related_id: artist_product.id,
                    related_type: "Product",
                    person: person)
            end
          end

          it "does NOT discount the artists" do
            subject.apply_proration_discounts!

            expect(person.purchases.unpaid.map(&:discount_cents)).to eq [
              0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
            ]
          end
        end
      end

      context "with 3 months left on plan," do
        before do
          create(:person_plan, person: person)
          create(:renewal, :plan_renewal, person: person, expires_at: DateTime.now + 3.months)
        end

        context "with additional artists," do
          before do
            15.times do
              create(:purchase,
                    :unpaid,
                    product: artist_product,
                    related_id: artist_product.id,
                    related_type: "Product",
                    person: person)
            end
          end

          it "discounts the artists" do
            subject.apply_proration_discounts!

            expect(person.purchases.unpaid.map(&:discount_cents)).to eq [
              750, 750, 750, 750, 750, 750, 750, 750, 750, 750, 750, 750, 750, 750, 750
            ]
          end
        end
      end

      context "with 6 months left on plan," do
        before do
          create(:person_plan, person: person)
          create(:renewal, :plan_renewal, person: person, expires_at: DateTime.now + 6.months)
        end

        context "with additional artists," do
          before do
            15.times do
              create(:purchase,
                    :unpaid,
                    product: artist_product,
                    related_id: artist_product.id,
                    related_type: "Product",
                    person: person)
            end
          end

          it "discounts the artists" do
            subject.apply_proration_discounts!

            expect(person.purchases.unpaid.map(&:discount_cents)).to eq [
              501, 501, 501, 501, 501, 501, 501, 501, 501, 501, 501, 501, 501, 501, 501
            ]
          end
        end
      end

      context "with 9 months left on plan," do
        before do
          create(:person_plan, person: person)
          create(:renewal, :plan_renewal, person: person, expires_at: DateTime.now + 9.months)
        end

        context "with additional artists," do
          before do
            15.times do
              create(:purchase,
                    :unpaid,
                    product: artist_product,
                    related_id: artist_product.id,
                    related_type: "Product",
                    person: person)
            end
          end

          it "discounts the artists" do
            subject.apply_proration_discounts!

            expect(person.purchases.unpaid.map(&:discount_cents)).to eq [
              249, 249, 249, 249, 249, 249, 249, 249, 249, 249, 249, 249, 249, 249, 249
            ]
          end
        end
      end

      context "with full term left on plan," do
        before do
          create(:person_plan, person: person)
          create(:renewal, :plan_renewal, person: person, expires_at: DateTime.now.beginning_of_day + (1.year - 1.day))
        end

        context "with additional artists," do
          before do
            15.times do
              create(:purchase,
                    :unpaid,
                    product: artist_product,
                    related_id: artist_product.id,
                    related_type: "Product",
                    person: person)
            end
          end

          it "does NOT discount the artists" do
            subject.apply_proration_discounts!

            expect(person.purchases.unpaid.map(&:discount_cents)).to eq [
              0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
            ]
          end
        end
      end
    end # scenario: only_adding_artists
  end

  context "When there is a targeted offer" do
    let(:person) { create(:person, :with_canada_country_website) }
    let(:breakout_artist_product) {
      PlansProduct.joins(:product).find_by(
        plan_id: Plan::BREAKOUT_ARTIST,
        'products.country_website_id': CountryWebsite::CANADA,
        ).product
    }

    before do
      @targeted_offer   = create(:targeted_offer, status: "Inactive", country_website_id: CountryWebsite::CANADA,
                                 country: "CA", start_date: Time.now - 40.days, expiration_date: Time.now + 10.days)
      @targeted_product = create(:targeted_product, targeted_offer: @targeted_offer, product: breakout_artist_product)
      create(:targeted_person, person: person, targeted_offer: @targeted_offer, usage_count: 1)
      @targeted_offer.update(status: "Active")
      allow(TargetedProduct).to receive(:targeted_product_for_active_offer).and_return(@targeted_product)
    end

    context "When targeted offer discount > proration discount" do
      it "should apply targeted offer discount" do
        purchase = Product.add_to_cart(person, breakout_artist_product, breakout_artist_product)
        subject.apply_proration_discounts!
        expect(purchase.discount_cents).to eq (@targeted_product.price_adjustment * 100)
        expect(purchase.no_discount?).to be false
        expect(purchase.non_plan_discount?).to be true
      end
    end

    context "When proration discount > targeted offer discount" do
      let!(:rising_artist_product) do
        PlansProduct.joins(:product).find_by(
          plan_id: Plan::RISING_ARTIST,
          'products.country_website_id': CountryWebsite::CANADA,
          ).product
      end

      let!(:purchase) do
        create(:purchase, product: rising_artist_product, related: rising_artist_product, person: person)
      end

      before do
        create(:invoice, :settled, person: person, purchases: [purchase])
        create(:person_plan, person_id: person.id, plan_id: Plan::RISING_ARTIST, created_at: DateTime.now - 1.month)
        create(:renewal, :plan_renewal, person: person, expires_at: DateTime.now + 11.months)
      end

      it "should apply proration discount" do
        purchase = Product.add_to_cart(person, breakout_artist_product, breakout_artist_product)
        subject.apply_proration_discounts!
        purchase.reload
        expect(purchase.discount_cents).to be > (@targeted_product.price_adjustment * 100)
        expect(purchase.discount_reason).to eq Purchase::PLAN_PRORATION.gsub(" ", "_")
      end
    end
  end
end
