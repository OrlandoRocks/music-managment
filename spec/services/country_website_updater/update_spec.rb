require "rails_helper"

describe CountryWebsiteUpdater::Update do
  describe '.update' do
    let(:person)          { create(:person) }
    let(:france_website)  { create(:country_website, :france) }

    let(:person_notifier_response) { double(deliver: true) }

    before do
      allow(PersonNotifier)
        .to receive(:lead_migrated)
        .and_return(person_notifier_response)
    end

    subject do
      person.country_website = france_website
      CountryWebsiteUpdater::Update.update(person: person)
    end

    it "resets the person's accepted_terms_and_conditions_on" do
      subject

      expect(person.accepted_terms_and_conditions_on).to be_nil
    end

    it "updates the person_balance's country to match the person's country" do
      subject

      expect(person.person_balance.currency).to eq france_website.currency
    end

    it "destroys any unfinalized purchases" do
      purchase = create(:purchase, person: person, paid_at: nil)

      expect { subject }
        .to change { Purchase.count }
        .by(-1)
    end

    # it "sends a notification to the user" do
    #   subject

    #   expect(PersonNotifier)
    #     .to have_received(:lead_migrated)
    #     .with(person)

    #   expect(person_notifier_response)
    #     .to have_received(:deliver)
    # end

    context "when an associated record fails to be modified" do
      before do
        allow(person)
          .to receive(:person_balance)
          .and_raise(ActiveRecord::RecordInvalid)
      end

      it "aborts the transaction" do
        expect { subject }
          .to raise_exception(ActiveRecord::RecordInvalid)
      end
    end
  end
end
