require "rails_helper"

describe "FailedTakedowns" do

  before(:each) do
    @album    = create(:album)
    @product  = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
    @purchase = create(:purchase, person_id: @album.person.id, related: @album)
    @renewal  = create(:renewal, :no_takedown_at, person_id: @album.person.id, item_to_renew: @product.product_items.first, purchase: @purchase)
    allow(@renewal).to receive(:has_first_renewal_settings?).and_return(false)
    @renewal_item    = create(:renewal_item, renewal: @renewal, related_id: @album.id, related_type: "Album")
    @renewal_history = RenewalHistory.find_by(renewal_id: @renewal.id)
  end

  context "cancelled renewals" do

    context "that expire today" do
      it "does NOT select them" do
        @renewal.update_attribute("canceled_at", 2.months.ago)
        @renewal_history.update_attribute("expires_at", Date.today)
        results = FailedTakedowns.new
        expect(results.failed_takedowns.map{|ft| ft["album_id"]}).to_not include(@album.id)
        expect(results.failed_takedowns.map{|ft| ft["renewal_id"]}).to_not include(@renewal.id)
      end
    end

    context "that expired yesterday" do
      it "selects them" do
        @renewal.update_attribute("canceled_at", 2.months.ago)
        @renewal_history.update_attribute("expires_at", Date.yesterday)
        results = FailedTakedowns.new
        expect(results.failed_takedowns.map{|ft| ft["album_id"]}).to include(@album.id)
        expect(results.failed_takedowns.map{|ft| ft["renewal_id"]}).to include(@renewal.id)
      end
    end

    context "that expired 1 month ago" do
      it "selects them" do
        @renewal.update_attribute("canceled_at", 6.months.ago)
        @renewal_history.update_attribute("expires_at", 1.month.ago)
        results = FailedTakedowns.new
        expect(results.failed_takedowns.map{|ft| ft["album_id"]}).to include(@album.id)
        expect(results.failed_takedowns.map{|ft| ft["renewal_id"]}).to include(@renewal.id)
      end
    end
  end

  context "non-cancelled renewals" do

    context "that reached the end of the grace period today" do
      it "does NOT select them" do
        date = Date.today - (Renewal::GRACE_DAYS_BEFORE_TAKEDOWN).days
        @renewal_history.update_attribute("expires_at", date)
        results = FailedTakedowns.new
        expect(results.failed_takedowns.map{|ft| ft["album_id"]}).to_not include(@album.id)
        expect(results.failed_takedowns.map{|ft| ft["renewal_id"]}).to_not include(@renewal.id)
      end
    end

    context "that reached the end of the grace period yesterday" do
      it "selects them" do
        date = Date.today - (Renewal::GRACE_DAYS_BEFORE_TAKEDOWN + 2).days # we add 1 day to the grace period already ¯\_(ツ)_/¯
        @renewal_history.update_attribute("expires_at", date)
        results = FailedTakedowns.new
        expect(results.failed_takedowns.map{|ft| ft["album_id"]}).to include(@album.id)
        expect(results.failed_takedowns.map{|ft| ft["renewal_id"]}).to include(@renewal.id)
      end
    end

    context "that reached the end of the grace period 1 month ago" do
      it "selects them" do
        date = Date.today - (Renewal::GRACE_DAYS_BEFORE_TAKEDOWN + 30).days
        @renewal_history.update_attribute("expires_at", date)
        results = FailedTakedowns.new
        expect(results.failed_takedowns.map{|ft| ft["album_id"]}).to include(@album.id)
        expect(results.failed_takedowns.map{|ft| ft["renewal_id"]}).to include(@renewal.id)
      end
    end
  end

  describe "#grace_period_cutoff_date" do
    it "is set correctly" do
      service = FailedTakedowns.new
      expected_result = (Date.today - (Renewal::GRACE_DAYS_BEFORE_TAKEDOWN + 1).days).to_s
      expect(service.grace_period_cutoff_date).to eq expected_result
    end
  end

  describe "#collect_album_ids" do
    it "returns a list of album ids as strings" do
      @renewal.update_attribute("canceled_at", 6.months.ago)
      @renewal_history.update_attribute("expires_at", 1.month.ago)
      results = FailedTakedowns.new
      expect(results.album_ids_strings).to eq ([@album.id.to_s])
    end
  end
end
