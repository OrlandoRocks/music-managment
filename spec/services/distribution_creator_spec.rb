require "rails_helper"

describe DistributionCreator do
  let(:album)        { create(:album) }
  let!(:petri_bundle) { create(:petri_bundle, album: album) }
  let(:params)       { { converter_class: "MusicStores::Shazam::Converter", delivery_type: "metadata_only" } }

  describe ".create" do
    it "creates a distribution and associates salepoints" do
      distribution = DistributionCreator.create(album, "Shazam", params).distribution
      expect(distribution.persisted?).to eq true
      expect(distribution.reload.salepoints.last.store.name).to eq "Shazam"
    end

    it "sets the distribution to new" do
      creator = DistributionCreator.create(album, "Shazam", params)
      expect(creator.distribution.state).to eq "new"
    end

    it "sets the delivery_type to full_delivery if not passed in" do
      creator = DistributionCreator.create(album, "Shazam", params)
      expect(creator.distribution.delivery_type).to eq params[:delivery_type]
    end

    it "sets the converter_class" do
      creator = DistributionCreator.create(album, "Shazam", params)
      expect(creator.distribution.converter_class).to eq params[:converter_class]
    end

    it "sets the converter_class to the default converter_class" do
      creator = DistributionCreator.create(album, "Shazam")
      expect(creator.distribution.converter_class).to eq params[:converter_class]
    end

    it "sets the delivery_type to passed in delivery_type" do
      params[:delivery_type] = nil
      creator = DistributionCreator.create(album, "Shazam", params)
      expect(creator.distribution.delivery_type).to eq "full_delivery"
    end

    it "associates the created distribution to the petri_bundle" do
      creator = DistributionCreator.create(album, "Shazam", params)
      expect(creator.petri_bundle.distributions).to include creator.distribution
    end

    it "starts the distribution if its ready" do
      allow(album).to receive(:approved_for_distribution?).and_return(true)
      album.finalized_at = Date.today
      creator = DistributionCreator.create(album, "Shazam", params)
      expect(creator.distribution.state).to eq "enqueued"
    end

    it "doesn't start distribution if not ready" do
      allow(album).to receive(:ready_for_distribution?).and_return(false)
      creator = DistributionCreator.create(album, "Shazam", params)
      expect(creator.distribution.state).to eq "new"
    end

    it "does not create a duplicate Believe salepoint" do
      believe_store = Store.find_by(short_name: "BelieveLiv")
      album.salepoints << create(:salepoint, store_id: believe_store.id)
      creator = DistributionCreator.create(album, "BelieveLiv", converter_class: "MusicStores::Believe::Converter", delivery_type: "full_delivery" )
      expect(album.salepoints.where(store_id: 75).count).to eq(1)
    end

    context "with DDEX converter" do
      let(:params) { { converter_class: "MusicStores::DDEX::Converter" } }
      it "creates a distribution and associates salepoints" do
        creator = DistributionCreator.create(album, "Slacker", params)
        expect(creator.distribution.persisted?).to eq true
        expect(creator.distribution.reload.salepoints.last.store.name).to eq "Slacker"
      end

      it "sets the converter_class" do
        creator = DistributionCreator.create(album, "Slacker", params)
        expect(creator.distribution.converter_class).to eq params[:converter_class]
      end

      it "uses already create distribution/salepoint if they exists" do
        distribution = create(:distribution, petri_bundle: petri_bundle, converter_class: "MusicStores::DDEX::Converter")
        salepoint    = create(:salepoint, salepointable: album, store: Store.find_by(short_name: "Slacker"))
        distribution.salepoints << salepoint
        creator = DistributionCreator.create(album, "Slacker", params)
        expect(creator.distribution).to eq distribution
        expect(creator.distribution.reload.salepoints).to include salepoint
        expect(creator.distribution.reload.salepoints.last.store.name).to eq "Slacker"
      end
    end
  end
end
