require 'rails_helper'

describe CashAdvanceRegistrationService do
  let(:person) {create(:person)}
  let(:inconspicuous_person_with_connections) {create(:person, override_advance_ineligibility: true)}
  let(:suspicious_person) {create(:person, status: "Suspicious")}
  let(:suspicious_person_with_connections) {create(:person, status: "Suspicious", override_advance_ineligibility: true)}
  before(:each) do
    allow(FeatureFlipper).to receive(:show_feature?).with(:lyric_advance, anything).and_return(true)
    allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_exclude_tax_check, anything).and_return(false)
    allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking).and_return(false)
    allow(CashAdvances::Registration).to receive(:register).and_return({})
  end

  describe "token" do
    context "with eligible person" do
      let(:person) {create(:person)}
      context "with eligibile record and tax form, no suspicion" do
        it "returns success status" do
          create(:direct_advance_eligibility_record, person_id: person.id, eligible: true)
          create(:tax_form, person_id: person.id)
          response = CashAdvanceRegistrationService.new(person).token

          expect(response[:status]).to eq(200)
        end
      end

      context "with override" do
        context "with suspicion" do
          it "returns success status" do
            create(:direct_advance_eligibility_record, person_id: suspicious_person_with_connections.id, eligible: true)
            create(:tax_form, person_id: suspicious_person_with_connections.id)
            response = CashAdvanceRegistrationService.new(suspicious_person_with_connections).token

            expect(response[:status]).to eq(200)
          end
        end
        context "with no eligibility record" do
          it "returns success status" do
            create(:tax_form, person_id: inconspicuous_person_with_connections.id)
            response = CashAdvanceRegistrationService.new(inconspicuous_person_with_connections).token

            expect(response[:status]).to eq(200)
          end
        end
        context "with no tax form" do
          it "returns success status" do
            create(:direct_advance_eligibility_record, person_id: inconspicuous_person_with_connections.id,
                   eligible: true)
            response = CashAdvanceRegistrationService.new(inconspicuous_person_with_connections).token

            expect(response[:status]).to eq(200)
          end
        end
        context "with inactive tax form" do
          it "returns success status" do
            create(:direct_advance_eligibility_record, person_id: inconspicuous_person_with_connections.id,
                   eligible: true)
            create(:tax_form, person_id: inconspicuous_person_with_connections.id, expires_at: Time.now - 1.day)
            response = CashAdvanceRegistrationService.new(inconspicuous_person_with_connections).token

            expect(response[:status]).to eq(200)
          end
        end
      end

      context "when the Lyric API fails" do
        before(:each) do
          allow(CashAdvances::Registration).to receive(:register).and_return(nil)
        end

        it "returns a failure status" do
          create(:direct_advance_eligibility_record, person_id: person.id, eligible: true)
          create(:tax_form, person_id: person.id)
          response = CashAdvanceRegistrationService.new(person).token

          expect(response[:status]).to eq(500)
        end
      end
    end

    context "with ineligible person" do
      context "person without any eligiblility record" do
        it "does not return success status" do
          create(:tax_form, person_id: person.id)
          response = CashAdvanceRegistrationService.new(person).token

          expect(response[:status]).to_not eq(200)
        end
      end
      context "person with ineligible eligiblility record" do
        it "does not return success status" do
          create(:direct_advance_eligibility_record, person_id: person.id, eligible: false)
          create(:tax_form, person_id: person.id)
          response = CashAdvanceRegistrationService.new(person).token

          expect(response[:status]).to_not eq(200)
        end
      end
      context "with no tax form" do
        it "does not return success status" do
          create(:direct_advance_eligibility_record, person_id: person.id, eligible: true)
          response = CashAdvanceRegistrationService.new(person).token

          expect(response[:status]).to_not eq(200)
        end
      end
      context "with inactive tax form" do
        it "does not return success status" do
          create(:direct_advance_eligibility_record, person_id: person.id, eligible: true)
          create(:tax_form, :w8, person_id: person.id, expires_at: Time.now - 1.day)
          response = CashAdvanceRegistrationService.new(person).token

          expect(response[:status]).to_not eq(200)
        end
      end
      context "suspicious person with eligibility record and tax form" do
        it "does not return success status" do
          create(:direct_advance_eligibility_record, person_id: suspicious_person.id, eligible: true)
          create(:tax_form, person_id: suspicious_person.id)
          response = CashAdvanceRegistrationService.new(suspicious_person).token

          expect(response[:status]).to_not eq(200)
        end
      end
    end
  end

  describe "advance_display" do
    let(:person) {create(:person)}
    let(:inconspicuous_person_with_connections) {create(:person, override_advance_ineligibility: true)}
    let(:suspicious_person) {create(:person, status: "Suspicious")}
    let(:suspicious_person_with_connections) do create(:person, status: "Suspicious",
                                                       override_advance_ineligibility: true)
    end
    before do
      allow(FeatureFlipper).to receive(:show_feature?).with(:lyric_advance, anything).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_exclude_tax_check, anything).and_return(false)
      allow(CashAdvances::Registration).to receive(:register).and_return({})
    end

    context "with override" do
      context "with suspicion" do
        it "returns :advance" do
          create(:direct_advance_eligibility_record, person_id: suspicious_person_with_connections.id, eligible: true)
          create(:tax_form, person_id: suspicious_person_with_connections.id)
          display = CashAdvanceRegistrationService.new(suspicious_person_with_connections).advance_display
          expect(display).to eq(:advance)
        end
      end
      context "without eligibility record" do
        it "returns :advance" do
          create(:tax_form, person_id: inconspicuous_person_with_connections.id)
          display = CashAdvanceRegistrationService.new(inconspicuous_person_with_connections).advance_display
          expect(display).to eq(:advance)
        end
      end
      context "with ineligible eligibility record" do
        it "returns :advance" do
          create(:direct_advance_eligibility_record, person_id: inconspicuous_person_with_connections.id,
                 eligible: false)
          create(:tax_form, person_id: inconspicuous_person_with_connections.id)
          display = CashAdvanceRegistrationService.new(inconspicuous_person_with_connections).advance_display
          expect(display).to eq(:advance)
        end
      end
      context "without tax form" do
        it "returns :advance" do
          create(:direct_advance_eligibility_record, person_id: inconspicuous_person_with_connections.id,
                 eligible: true)
          display = CashAdvanceRegistrationService.new(inconspicuous_person_with_connections).advance_display
          expect(display).to eq(:advance)
        end
      end
      context "without inactive tax form" do
        it "returns :advance" do
          create(:direct_advance_eligibility_record, person_id: inconspicuous_person_with_connections.id,
                 eligible: true)
          create(:tax_form, person_id: inconspicuous_person_with_connections.id, expires_at: Time.now - 1.day)
          display = CashAdvanceRegistrationService.new(inconspicuous_person_with_connections).advance_display
          expect(display).to eq(:advance)
        end
      end
    end

    context "without override" do
      context "with eligible record" do
        context "with suspicion" do
          it "returns :under_review" do
            create(:direct_advance_eligibility_record, person_id: suspicious_person.id, eligible: true)
            create(:tax_form, person_id: suspicious_person.id)
            display = CashAdvanceRegistrationService.new(suspicious_person).advance_display
            expect(display).to eq(:under_review)
          end
        end
        context "without suspicion" do
          it "returns :advance" do
            create(:direct_advance_eligibility_record, person_id: person.id, eligible: true)
            create(:tax_form, person_id: person.id)
            display = CashAdvanceRegistrationService.new(person).advance_display
            expect(display).to eq(:advance)
          end
        end
      end
      context "without eligiblity record" do
        it "returns :none" do
          create(:tax_form, person_id: person.id)
          display = CashAdvanceRegistrationService.new(person).advance_display
          expect(display).to eq(:none)
        end
      end
      context "with ineligible eligibility record" do
        it "returns :none" do
          create(:direct_advance_eligibility_record, person_id: person.id, eligible: false)
          create(:tax_form, person_id: person.id)
          display = CashAdvanceRegistrationService.new(person).advance_display
          expect(display).to eq(:none)
        end
      end
      context "without tax form" do
        it "returns :none" do
          create(:direct_advance_eligibility_record, person_id: person.id, eligible: true)
          display = CashAdvanceRegistrationService.new(person).advance_display
          expect(display).to eq(:none)
        end
      end
      context "without active tax form" do
        it "returns :none" do
          create(:direct_advance_eligibility_record, person_id: person.id, eligible: true)
          create(:tax_form, :w8, person_id: person.id, expires_at: Time.now - 1.day)
          display = CashAdvanceRegistrationService.new(person).advance_display
          expect(display).to eq(:none)
        end
      end
    end
  end

  describe "user_needed_override?" do
    context "without override" do
      context "with suspicion" do
        it "returns false" do
          needs = CashAdvanceRegistrationService.new(suspicious_person).user_needed_override?
          expect(needs).to be(false)
        end
      end
      context "with ineligibility" do
        it "returns false" do
          create(:direct_advance_eligibility_record, person_id: person.id, eligible: false)
          needs = CashAdvanceRegistrationService.new(person).user_needed_override?
          expect(needs).to be(false)
        end
      end
      context "without tax form" do
        it "returns false" do
          create(:direct_advance_eligibility_record, person_id: person.id, eligible: true)
          needs = CashAdvanceRegistrationService.new(person).user_needed_override?
          expect(needs).to be(false)
        end
      end
      context "without active tax form" do
        it "returns false" do
          create(:direct_advance_eligibility_record, person_id: person.id, eligible: true)
          create(:tax_form, :w8, person_id: person.id, expires_at: Time.now - 1.day)
          needs = CashAdvanceRegistrationService.new(person).user_needed_override?
          expect(needs).to be(false)
        end
      end
      context "eligible, with tax form, and not suspicious" do
        it "returns false" do
          create(:direct_advance_eligibility_record, person_id: person.id, eligible: true)
          create(:tax_form, person_id: person.id)
          needs = CashAdvanceRegistrationService.new(person).user_needed_override?
          expect(needs).to be(false)
        end
      end
    end
    context "with override" do
      context "with suspicion" do
        it "returns true" do
          create(:direct_advance_eligibility_record, person_id: suspicious_person_with_connections.id, eligible: true)
          create(:tax_form, person_id: suspicious_person_with_connections.id)
          needs = CashAdvanceRegistrationService.new(suspicious_person_with_connections).user_needed_override?
          expect(needs).to be(true)
        end
      end
      context "with ineligibility" do
        it "returns true" do
          create(:direct_advance_eligibility_record, person_id: inconspicuous_person_with_connections.id,
                 eligible: false)
          create(:tax_form, person_id: inconspicuous_person_with_connections.id)
          needs = CashAdvanceRegistrationService.new(inconspicuous_person_with_connections).user_needed_override?
          expect(needs).to be(true)
        end
      end
      context "without tax form" do
        it "returns true" do
          create(:direct_advance_eligibility_record, person_id: inconspicuous_person_with_connections.id,
                 eligible: true)
          needs = CashAdvanceRegistrationService.new(inconspicuous_person_with_connections).user_needed_override?
          expect(needs).to be(true)
        end
      end
      context "without active tax form" do
        it "returns true" do
          create(:direct_advance_eligibility_record, person_id: inconspicuous_person_with_connections.id,
                 eligible: true)
          create(:tax_form, :w8, person_id: inconspicuous_person_with_connections.id, expires_at: Time.now - 1.day)
          needs = CashAdvanceRegistrationService.new(inconspicuous_person_with_connections).user_needed_override?
          expect(needs).to be(true)
        end
      end
    end
    context "eligible, with tax form, and not suspicious" do
      it "returns false" do
        create(:direct_advance_eligibility_record, person_id: person.id, eligible: true)
        create(:tax_form, person_id: person.id, expires_at: Time.now - 1.day)
        needs = CashAdvanceRegistrationService.new(person).user_needed_override?
        expect(needs).to be(false)
      end
    end
  end
end
