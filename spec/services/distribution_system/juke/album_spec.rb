require "rails_helper"

describe DistributionSystem::Juke::Album do
  before(:each) do
    allow(Time).to receive(:now).and_return(
      double(iso8601: "2016-01-01T12:00:00-05:00", getlocal: "2016-01-01T12:00:00-05:00")
    )
  end

  describe "#to_xml" do
    let(:album) do
      YAML.load_file("spec/distribution_fixtures/converted_albums/1340271_11233376_juke.yml")
    end

    let(:fixture_xml) do
      Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/1340271_juke.xml"))
    end

    before(:each) do
      allow(DistributionSystem::CheckSumHelper).to receive(:checksum_file).and_return("8e1705d57c56c106a7c54c0a26be4717")
      allow(album).to receive(:get_uuid).and_return('858c4a2099b60132804612890e781ad7')
      album.tracks.each do |track|
        allow(track).to receive(:iso8601).and_return("PT2M29S")
        allow(track).to receive(:audio_file).and_return("859714246774_01_1.flac")
      end
      #puts Diffy::Diff.new(fixture_xml.to_s, Nokogiri::XML(album.to_xml, nil, 'utf-8').to_s, :context => 2)
    end

    it "matches xml fixture" do
      expect(
        EquivalentXml.equivalent?(
          Nokogiri::XML(album.to_xml, nil, 'utf-8'),
          fixture_xml,
          :element_order => false,
          :normalize_whitespace => true
        )
      ).to eq true
    end
  end
end
