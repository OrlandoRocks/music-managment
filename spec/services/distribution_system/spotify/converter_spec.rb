require "rails_helper"

describe DistributionSystem::Spotify::Converter do
  describe "#convert" do
    before :each do
      @store = Store.find_by(short_name: "Spotify")
      allow(@store).to receive(:delivery_service).and_return("tunecore")
      Timecop.freeze(DateTime.now)
      @golive_date = DateTime.now
      @sale_date = 2.days.from_now
      @album = create(
        :album,
        :with_artwork,
        primary_genre: Genre.find(1),
        golive_date: @golive_date,
        sale_date: @sale_date
      )
    end

    after :each do
      Timecop.return
    end

    it "sets the takedown date" do
      takedown_date = Date.today
      salepoint = Salepoint.create(
        store_id: @store.id,
        salepointable_id: @album.id,
        salepointable_type: "Album",
        payment_applied: true,
        finalized_at: Date.today - 11.days,
        takedown_at: takedown_date
      )
      allow(salepoint).to receive(:store).and_return(@store)
      allow(@album).to receive(:salepoints).and_return([salepoint])

      converted_album = DistributionSystem::Spotify::Converter.new.convert(
        @album, @album.salepoints, "metadata_only"
      )
      converted_takedown_date = takedown_date.midnight.iso8601[0..18]

      expect(converted_album.takedown_at).to eq(converted_takedown_date)
    end

    context("when feature flipper is set") do
      before :each do
        @store = Store.find_by(short_name: "Spotify")
        @sale_date = 2.days.from_now
        @album = create(:album, :with_artwork, primary_genre: Genre.find(1), sale_date: @sale_date, golive_date: @golive_date)
        salepoint = Salepoint.create(
          store_id: @store.id,
          salepointable_id: @album.id,
          salepointable_type: "Album",
          payment_applied: true,
          finalized_at: Date.today - 11.days
        )
        allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
        allow(salepoint).to receive(:store).and_return(@store)
        allow(@album).to receive(:salepoints).and_return([salepoint])
      end

      context("when timed release timing scenario is set to absolute_time") do
        it "sets actual release date to time release in utc" do
          @album.timed_release_timing_scenario = "absolute_time"
          converted_album = DistributionSystem::Spotify::Converter.new.convert(
            @album, @album.salepoints, "metadata_only"
          )

          expect(converted_album.actual_release_date).to eq(@golive_date.utc.iso8601)
        end
      end

      context("when timed release timing scenario is set to relative_time") do
        it "sets actual release date to time release in eastern time" do
          @album.timed_release_timing_scenario = "relative_time"
          converted_album = DistributionSystem::Spotify::Converter.new.convert(
            @album, @album.salepoints, "metadata_only"
          )
          expect(converted_album.actual_release_date).to eq(@golive_date.in_time_zone('Eastern Time (US & Canada)').iso8601[0..18])
        end
      end

      context("when timed release timing scenario is set to null") do
        it "sets actual release date to the sale date" do
          @album.timed_release_timing_scenario = nil
          converted_album = DistributionSystem::Spotify::Converter.new.convert(
            @album, @album.salepoints, "metadata_only"
          )
          expect(converted_album.actual_release_date).to eq(@sale_date.midnight.iso8601[0..18])
        end
      end
    end

    context 'rollup track artists' do
      let(:album) {
        create(
          :album,
          :finalized,
          :with_artwork,
          :with_salepoints,
          abbreviation: ['sp'],
        )
      }

      before :each do
        song = create(:song, :with_upload, album: album)
        album.reload
        [
          'composer',
          'conductor',
          'orchestra',
        ].each do |role|
          album.songs.each do |song|
            ddex_role = DDEXRole.find_by(role_type: role)
            song_role = create(:song_role, role_type: role)
            ddex_song_role = create(:ddex_song_role, song_role: song_role, ddex_role: ddex_role)
            creative  = create(:creative, role: role, creativeable: song)
            create(:creative_song_role, song: song, song_role: song_role, creative: creative)
          end
        end
      end

      it 'should be successful' do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
        converted_album = DistributionSystem::Spotify::Converter.new.convert(
          album, album.salepoints, :full_delivery
        )

        expect(converted_album.rollup_track_artists.length).to eq(3)
      end
    end

    context "spoken word genres" do
      before :each do
        @store = Store.find_by(short_name: "Spotify")
        @sale_date = 2.days.from_now
        @album = create(:album, :with_artwork, primary_genre: Genre.find(1), sale_date: @sale_date, golive_date: @golive_date)
        salepoint = Salepoint.create(
          store_id: @store.id,
          salepointable_id: @album.id,
          salepointable_type: "Album",
          payment_applied: true,
          finalized_at: Date.today - 11.days
        )
        allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
        allow(salepoint).to receive(:store).and_return(@store)
        allow(@album).to receive(:salepoints).and_return([salepoint])
      end

      context "when feature flag is set" do
        before do
          allow(FeatureFlipper).to receive(:show_feature?).with(:spoken_word_genres, @album.person).and_return(true)
        end

        it "do not remove US countries if spoken word genre is not set" do
          converted_album = DistributionSystem::Spotify::Converter.new.convert(
            @album, @album.salepoints, "metadata_only"
          )
          expect((["PR", "GU", "AS", "US", "UM", "VI"] - converted_album.countries).length).to eq 0
        end

        it "remove US countries if spoken word genre is set" do
          @album.primary_genre = Genre.find_by(name: "Spoken Word")
          @album.save
          converted_album = DistributionSystem::Spotify::Converter.new.convert(
            @album, @album.salepoints, "metadata_only"
          )
          expect((["PR", "GU", "AS", "US", "UM", "VI"] - converted_album.countries).length).to eq 6
        end
      end

      context "when feature flag is not set" do
        before do
          allow(FeatureFlipper).to receive(:show_feature?).with(:spoken_word_genres, @album.person).and_return(false)
        end

        it "do not remove US countries if spoken word genre is set" do
          @album.primary_genre = Genre.find_by(name: "Spoken Word")
          @album.save
          converted_album = DistributionSystem::Spotify::Converter.new.convert(
            @album, @album.salepoints, "metadata_only"
          )
          expect((["PR", "GU", "AS", "US", "UM", "VI"] - converted_album.countries).length).to eq 0
        end

        it "do not remove the countries if spoken work genre is not set" do
          converted_album = DistributionSystem::Spotify::Converter.new.convert(
            @album, @album.salepoints, "metadata_only"
          )
          expect((["PR", "GU", "AS", "US", "UM", "VI"] - converted_album.countries).length).to eq 0
        end
      end
    end
  end
end
