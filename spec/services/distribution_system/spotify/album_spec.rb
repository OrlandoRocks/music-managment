require "rails_helper"

describe DistributionSystem::Spotify::Album do
  let(:main_artist)     { build(:spotify_distribution_system_artist, name: "Main Artist", spotify_id: 'main12345') }
  let(:featured_artist) { build(:spotify_distribution_system_artist, :is_featured, name: "Featured album-level", spotify_id: "featured12345") }
  let(:songwriter)      { build(:spotify_distribution_system_artist, :with_artist_roles, name: "Main Artist", role_types: ["songwriter"]) }
  let(:composer_lyricist) { build(:spotify_distribution_system_artist, :with_artist_roles, name: "Main Artist", role_types: ["composer_lyricist"]) }
  let(:album_producer)  { build(:spotify_distribution_system_artist, :with_artist_roles, name: "Producer album-level", roles: nil, role_types: ["producer"]) }
  let(:album_remixer)   { build(:spotify_distribution_system_artist, :with_artist_roles, name: "Remixer album-level", roles: nil, role_types: ["remixer"], spotify_id: "remixer12345") }
  let(:track_composer)  { build(:spotify_distribution_system_artist, :with_artist_roles, name: "Composer track-level", roles: nil, role_types: ["composer"]) }
  let(:track_lyricist)  { build(:spotify_distribution_system_artist, :with_artist_roles, name: "Lyricist track-level", roles: nil, role_types: ["lyricist"]) }
  let(:multi_role_artist) {
    build(
      :spotify_distribution_system_artist,
      :with_artist_roles,
      name: 'Multi Role',
      roles: nil,
      role_types: [
        'remixer',
        'composer',
        'lyricist',
        'producer',
        'conductor',
        'arranger',
        'orchestra',
        'actor',
      ],
      spotify_id: 'multi12345'
    )
  }
  let(:multi_role_featured_artist) {
    build(
      :spotify_distribution_system_artist,
      :with_artist_roles,
      name: 'Multi Role',
      roles: ['featuring'],
      role_types: [
        'remixer',
        'composer',
        'lyricist',
        'producer',
        'conductor',
        'arranger',
        'orchestra',
        'actor',
      ],
      spotify_id: 'multifea12345'
    )
  }
  let(:multi_role_main_artist) {
    build(
      :spotify_distribution_system_artist,
      :with_artist_roles,
      name: "Main Artist",
      roles: ['primary'],
      role_types: [
        'remixer',
        'composer',
        'lyricist',
        'producer',
        'conductor',
        'arranger',
        'orchestra',
        'actor',
      ],
      spotify_id: 'multimain12345'
    )
  }
  let(:new_artist) {
    build(
      :spotify_distribution_system_artist,
      :with_artist_roles,
      name: "New Artist",
      roles: ['contributor'],
      role_types: [
        'remixer',
      ],
      spotify_id: 'NEW'
    )
  }


  let(:track_1) do
    build(:distribution_system_track,
          artists: [main_artist, featured_artist, album_producer, album_remixer, composer_lyricist, songwriter, track_composer, track_lyricist],
          duration: "PT00H00M22S",
          isrc: "TCAAA1800040",
          title: "First Song",
          available_for_streaming: true,
          audio_file: "859700000373_01_1.flac")
  end

  let(:track_2) do
    build(:distribution_system_track,
        artists: [main_artist, featured_artist, album_producer, album_remixer, songwriter, new_artist],
        duration: "PT00H00M22S",
        isrc: "TCAAA1800041",
        title: "Second Song",
        available_for_streaming: true,
        audio_file: "859700000373_01_2.flac",
        number: 2)
  end

  let(:album) do
    build(:spotify_distribution_album,
          :with_legacy_cover_image,
          album_type: "Album",
          artists: [main_artist],
          copyright_pline: "2018 Main Artist",
          delivery_type: "full_delivery",
          genres: ["Alternative"],
          genre: 1,
          label_name: "Main Artist",
          release_date: "2018-12-17",
          actual_release_date: "2018-12-17T00:00:00",
          store_id: Store.find_by(abbrev: "sp").id,
          store_name: "Spotify",
          title: "Spotify Test 17 Dec 2018",
          tracks: [track_1, track_2],
          upc: "859700000373",
          uuid: "1f07a530e4780136efe73dfd75779dd7",
          countries: ["BH"],
          explicit_lyrics: true,
          test_delivery: true)
  end


  context "when full_delivery" do

    before(:each) do
      StoreDeliveryConfig.readonly(false).for("Spotify").update(test_delivery: true)
      allow(Digest::MD5).to receive(:file).and_return("77c2eef169963a7ed5ec05761223333e")
      track_1.album = album
      track_2.album = album
      allow(album).to receive(:xml_timestamp).and_return("2018-12-17T22:04:24+00:00")
      allow(album).to receive(:get_uuid).and_return("a3717db0e4750136efe23dfd75779dd7")
      allow(album).to receive(:is_ep?).and_return(false)
      allow(track_1).to receive(:formatted_duration).and_return("PT00H00M22S")
      allow(track_2).to receive(:formatted_duration).and_return("PT00H00M22S")
    end

    describe "#valid?" do
      it "returns true if valid" do
        expect(album.valid?).to be_truthy
      end
    end

    describe "#validate_xml" do
      it "returns true if the xml is valid" do
        expect(album.validate_xml).to be_truthy
      end
    end

    describe "#to_xml" do
      let(:fixture_xml) do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/spotify_full_delivery.xml"))
      end

      it "returns true if the xml is valid" do
        #diff = Diffy::Diff.new(fixture_xml.to_s, Nokogiri::XML(album.to_xml, nil, 'utf-8').to_s, context: 2)
        #puts JSON.parse(diff.diff)
        result = compare_xml(album.to_xml, fixture_xml)
        expect(result).to be_truthy
      end
    end
  end

  context "when takedown" do
    before(:each) do
      StoreDeliveryConfig.readonly(false).for("Spotify").update(test_delivery: true)
      album.takedown = true
      album.takedown_at = "2018-12-17T00:00:00"
      album.delivery_type = "metadata_only"
      track_1.album = album
      track_1.available_for_streaming = false
      track_2.album = album
      allow(album).to receive(:xml_timestamp).and_return("2018-12-17T22:22:11+00:00")
      allow(album).to receive(:get_uuid).and_return("1f07a530e4780136efe73dfd75779dd7")
      allow(album).to receive(:is_ep?).and_return(false)
      allow(track_1).to receive(:formatted_duration).and_return("PT00H00M22S")
      allow(track_2).to receive(:formatted_duration).and_return("PT00H00M22S")
    end

    describe "#valid?" do
      it "returns true if valid" do
        expect(album.valid?).to be_truthy
      end
    end

    describe "#validate_xml" do
      it "returns true if the xml is valid" do
        expect(album.validate_xml).to be_truthy
      end
    end

    describe "#to_xml" do
      let(:fixture_xml) do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/spotify_takedown.xml"))
      end

      it "returns true if the xml is valid" do
        #diff = Diffy::Diff.new(fixture_xml.to_s, Nokogiri::XML(album.to_xml, nil, 'utf-8').to_s, context: 2)
        #puts JSON.parse(diff.diff)
        result = compare_xml(album.to_xml, fixture_xml)
        expect(result).to be_truthy
      end
    end
  end

  context "rollup track-level contributors to the album" do
    context 'successfully when contributors on all tracks' do
      let :tracks do
        2.times.map do |i|
          build(:distribution_system_track,
                artists: [
                  main_artist,
                  featured_artist,
                  album_producer,
                  album_remixer,
                  composer_lyricist,
                  track_composer,
                  track_lyricist
          ],
          duration: "PT00H00M22S",
          isrc: "TCAAA180004#{i}",
          title: "Song #{i}",
          available_for_streaming: true,
          audio_file: "859700000373_01_#{i+1}.flac",
         )
        end
      end
      let :album do
        build(:spotify_distribution_album,
              :with_legacy_cover_image,
              album_type: "Album",
              artists: [main_artist],
              copyright_pline: "2018 Main Artist",
              delivery_type: :full_delivery,
              genres: ["Alternative"],
              genre: 1,
              label_name: "Main Artist",
              release_date: "2018-12-17",
              actual_release_date: "2018-12-17T00:00:00",
              store_id: Store.find_by(abbrev: "sp").id,
              store_name: "Spotify",
              title: "Spotify Test 12 July 2019",
              tracks: tracks,
              upc: "859700000373",
              uuid: "1f07a530e4780136efe73dfd75779dd7",
              countries: ["BH"],
              explicit_lyrics: true,
              test_delivery: true
             )
      end
      let :fixture_xml do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/spotify_rollup_common_contributors.xml"))
      end

      before :each do
        StoreDeliveryConfig.readonly(false).for("Spotify").update(test_delivery: true)
        allow(Digest::MD5).to receive(:file).and_return("77c2eef169963a7ed5ec05761223333e")
        allow(album).to receive(:xml_timestamp).and_return("2018-12-17T22:04:24+00:00")
        allow(album).to receive(:get_uuid).and_return("a3717db0e4750136efe23dfd75779dd7")
        allow(album).to receive(:is_ep?).and_return(false)
        tracks.each do |t|
          t.album = album
          allow(t).to receive(:formatted_duration).and_return("PT00H00M22S")
        end
      end

      it 'should be a valid album' do
        expect(album).to be_valid
      end

      it 'should have valid xml' do
        expect(album.validate_xml).to be_truthy
      end

      it 'should generate the correct xml with contributors rolled up to album' do
        result = compare_xml(album.to_xml, fixture_xml)
        expect(result).to be_truthy
      end
    end

    context "successfully when producer only on one track" do
      let :tracks do
        2.times.map do |i|
          build(:distribution_system_track,
                artists: [
                  main_artist,
                  featured_artist,
                  album_remixer,
                  composer_lyricist,
                  track_composer,
                  track_lyricist
                ].concat(i == 0 ? [album_producer] : []),
          duration: "PT00H00M22S",
          isrc: "TCAAA180004#{i}",
          title: "Song #{i}",
          available_for_streaming: true,
          audio_file: "859700000373_01_#{i+1}.flac",
         )
        end
      end
      let :album do
        build(:spotify_distribution_album,
              :with_legacy_cover_image,
              album_type: "Album",
              artists: [main_artist],
              copyright_pline: "2018 Main Artist",
              delivery_type: :full_delivery,
              genres: ["Alternative"],
              genre: 1,
              label_name: "Main Artist",
              release_date: "2018-12-17",
              actual_release_date: "2018-12-17T00:00:00",
              store_id: Store.find_by(abbrev: "sp").id,
              store_name: "Spotify",
              title: "Spotify Test 12 July 2019",
              tracks: tracks,
              upc: "859700000373",
              uuid: "1f07a530e4780136efe73dfd75779dd7",
              countries: ["BH"],
              explicit_lyrics: true,
              test_delivery: true
             )
      end
      let :fixture_xml do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/spotify_rollup_contributors_no_producer.xml"))
      end

      before :each do
        StoreDeliveryConfig.readonly(false).for("Spotify").update(test_delivery: true)
        allow(Digest::MD5).to receive(:file).and_return("77c2eef169963a7ed5ec05761223333e")
        allow(album).to receive(:xml_timestamp).and_return("2018-12-17T22:04:24+00:00")
        allow(album).to receive(:get_uuid).and_return("a3717db0e4750136efe23dfd75779dd7")
        allow(album).to receive(:is_ep?).and_return(false)
        tracks.each do |t|
          t.album = album
          allow(t).to receive(:formatted_duration).and_return("PT00H00M22S")
        end
      end

      it 'should be a valid album' do
        expect(album).to be_valid
      end

      it 'should have valid xml' do
        expect(album.validate_xml).to be_truthy
      end

      it 'should generate the correct xml with all contributors except producer on album' do
        result = compare_xml(album.to_xml, fixture_xml)
        expect(result).to be_truthy
      end
    end

    context "successfully with multi role artist" do
      let :tracks do
        [
          build(
            :distribution_system_track,
            artists: [
              main_artist,
              featured_artist,
              multi_role_artist,
            ],
            duration: "PT00H00M22S",
            isrc: "TCAAA1800040",
            title: "Song 1",
            available_for_streaming: true,
            audio_file: "859700000373_01_1.flac",
          )
        ]
      end
      let :album do
        build(
          :spotify_distribution_album,
          :with_legacy_cover_image,
          album_type: "Album",
          artists: [main_artist],
          copyright_pline: "2018 Main Artist",
          delivery_type: :full_delivery,
          genres: ["Alternative"],
          genre: 1,
          label_name: "Main Artist",
          release_date: "2018-12-17",
          actual_release_date: "2018-12-17T00:00:00",
          store_id: Store.find_by(abbrev: "sp").id,
          store_name: "Spotify",
          title: "Spotify Test 12 July 2019",
          tracks: tracks,
          upc: "859700000373",
          uuid: "1f07a530e4780136efe73dfd75779dd7",
          countries: ["BH"],
          explicit_lyrics: true,
          test_delivery: true
        )
      end

      let :fixture_xml do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/spotify_rollup_contributors_multi_role_artist.xml"))
      end

      before :each do
        StoreDeliveryConfig.readonly(false).for("Spotify").update(test_delivery: true)
        allow(Digest::MD5).to receive(:file).and_return("77c2eef169963a7ed5ec05761223333e")
        allow(album).to receive(:xml_timestamp).and_return("2018-12-17T22:04:24+00:00")
        allow(album).to receive(:get_uuid).and_return("a3717db0e4750136efe23dfd75779dd7")
        allow(album).to receive(:is_ep?).and_return(false)
        tracks.each do |t|
          t.album = album
          allow(t).to receive(:formatted_duration).and_return("PT00H00M22S")
        end
      end

      it 'should be a valid album' do
        expect(album).to be_valid
      end

      it 'should have valid xml' do
        expect(album.validate_xml).to be_truthy
      end

      it 'should generate the correct xml with all contributors except producer on album' do
        result = compare_xml(album.to_xml, fixture_xml)
        expect(result).to be_truthy
      end
    end

    context 'successfully when contributors on all tracks' do
      let :tracks do
        2.times.map do |i|
          build(:distribution_system_track,
                artists: [
                  main_artist,
                  featured_artist,
                  album_producer,
                  album_remixer,
                  composer_lyricist,
                  track_composer,
                  track_lyricist
          ],
          duration: "PT00H00M22S",
          isrc: "TCAAA180004#{i}",
          title: "Song #{i}",
          available_for_streaming: true,
          audio_file: "859700000373_01_#{i+1}.flac",
               )
        end
      end
      let :album do
        build(:spotify_distribution_album,
              :with_legacy_cover_image,
              album_type: "Album",
              artists: [main_artist],
              copyright_pline: "2018 Main Artist",
              delivery_type: :full_delivery,
              genres: ["Alternative"],
              genre: 1,
              label_name: "Main Artist",
              release_date: "2018-12-17",
              actual_release_date: "2018-12-17T00:00:00",
              store_id: Store.find_by(abbrev: "sp").id,
              store_name: "Spotify",
              title: "Spotify Test 12 July 2019",
              tracks: tracks,
              upc: "859700000373",
              uuid: "1f07a530e4780136efe73dfd75779dd7",
              countries: ["BH"],
              explicit_lyrics: true,
              test_delivery: true
             )
      end
      let :fixture_xml do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/spotify_rollup_common_contributors.xml"))
      end

      before :each do
        StoreDeliveryConfig.readonly(false).for("Spotify").update(test_delivery: true)
        allow(Digest::MD5).to receive(:file).and_return("77c2eef169963a7ed5ec05761223333e")
        allow(album).to receive(:xml_timestamp).and_return("2018-12-17T22:04:24+00:00")
        allow(album).to receive(:get_uuid).and_return("a3717db0e4750136efe23dfd75779dd7")
        allow(album).to receive(:is_ep?).and_return(false)
        tracks.each do |t|
          t.album = album
          allow(t).to receive(:formatted_duration).and_return("PT00H00M22S")
        end
      end

      it 'should be a valid album' do
        expect(album).to be_valid
      end

      it 'should have valid xml' do
        expect(album.validate_xml).to be_truthy
      end

      it 'should generate the correct xml with contributors rolled up to album' do
        result = compare_xml(album.to_xml, fixture_xml)
        expect(result).to be_truthy
      end
    end

    context "successfully when producer only on one track" do
      let :tracks do
        2.times.map do |i|
          build(:distribution_system_track,
                artists: [
                  main_artist,
                  featured_artist,
                  album_remixer,
                  composer_lyricist,
                  track_composer,
                  track_lyricist
          ].concat(i == 0 ? [album_producer] : []),
          duration: "PT00H00M22S",
          isrc: "TCAAA180004#{i}",
          title: "Song #{i}",
          available_for_streaming: true,
          audio_file: "859700000373_01_#{i+1}.flac",
               )
        end
      end
      let :album do
        build(:spotify_distribution_album,
              :with_legacy_cover_image,
              album_type: "Album",
              artists: [main_artist],
              copyright_pline: "2018 Main Artist",
              delivery_type: :full_delivery,
              genres: ["Alternative"],
              genre: 1,
              label_name: "Main Artist",
              release_date: "2018-12-17",
              actual_release_date: "2018-12-17T00:00:00",
              store_id: Store.find_by(abbrev: "sp").id,
              store_name: "Spotify",
              title: "Spotify Test 12 July 2019",
              tracks: tracks,
              upc: "859700000373",
              uuid: "1f07a530e4780136efe73dfd75779dd7",
              countries: ["BH"],
              explicit_lyrics: true,
              test_delivery: true
             )
      end
      let :fixture_xml do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/spotify_rollup_contributors_no_producer.xml"))
      end

      before :each do
        StoreDeliveryConfig.readonly(false).for("Spotify").update(test_delivery: true)
        allow(Digest::MD5).to receive(:file).and_return("77c2eef169963a7ed5ec05761223333e")
        allow(album).to receive(:xml_timestamp).and_return("2018-12-17T22:04:24+00:00")
        allow(album).to receive(:get_uuid).and_return("a3717db0e4750136efe23dfd75779dd7")
        allow(album).to receive(:is_ep?).and_return(false)
        tracks.each do |t|
          t.album = album
          allow(t).to receive(:formatted_duration).and_return("PT00H00M22S")
        end
      end

      it 'should be a valid album' do
        expect(album).to be_valid
      end

      it 'should have valid xml' do
        expect(album.validate_xml).to be_truthy
      end

      it 'should generate the correct xml with all contributors except producer on album' do
        result = compare_xml(album.to_xml, fixture_xml)
        expect(result).to be_truthy
      end
    end

    context "successfully with multi role artist" do
      let :tracks do
        [
          build(
            :distribution_system_track,
            artists: [
              main_artist,
              featured_artist,
              multi_role_artist,
            ],
            duration: "PT00H00M22S",
            isrc: "TCAAA1800040",
            title: "Song 1",
            available_for_streaming: true,
            audio_file: "859700000373_01_1.flac",
          )
        ]
      end
      let :album do
        build(
          :spotify_distribution_album,
          :with_legacy_cover_image,
          album_type: "Album",
          artists: [main_artist],
          copyright_pline: "2018 Main Artist",
          delivery_type: :full_delivery,
          genres: ["Alternative"],
          genre: 1,
          label_name: "Main Artist",
          release_date: "2018-12-17",
          actual_release_date: "2018-12-17T00:00:00",
          store_id: Store.find_by(abbrev: "sp").id,
          store_name: "Spotify",
          title: "Spotify Test 12 July 2019",
          tracks: tracks,
          upc: "859700000373",
          uuid: "1f07a530e4780136efe73dfd75779dd7",
          countries: ["BH"],
          explicit_lyrics: true,
          test_delivery: true
        )
      end

      let :fixture_xml do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/spotify_rollup_contributors_multi_role_artist.xml"))
      end

      before :each do
        StoreDeliveryConfig.readonly(false).for("Spotify").update(test_delivery: true)
        allow(Digest::MD5).to receive(:file).and_return("77c2eef169963a7ed5ec05761223333e")
        allow(album).to receive(:xml_timestamp).and_return("2018-12-17T22:04:24+00:00")
        allow(album).to receive(:get_uuid).and_return("a3717db0e4750136efe23dfd75779dd7")
        allow(album).to receive(:is_ep?).and_return(false)
        tracks.each do |t|
          t.album = album
          allow(t).to receive(:formatted_duration).and_return("PT00H00M22S")
        end
      end

      it 'should be a valid album' do
        expect(album).to be_valid
      end

      it 'should have valid xml' do
        expect(album.validate_xml).to be_truthy
      end

      it 'should generate the correct xml with all contributors except producer on album' do
        result = compare_xml(album.to_xml, fixture_xml)
        expect(result).to be_truthy
      end
    end

    context "successfully with multi role featured artist" do
      let :tracks do
        [
          build(
            :distribution_system_track,
            artists: [
              main_artist,
              featured_artist,
              multi_role_featured_artist,
            ],
            duration: "PT00H00M22S",
            isrc: "TCAAA1800040",
            title: "Song 1",
            available_for_streaming: true,
            audio_file: "859700000373_01_1.flac",
          )
        ]
      end
      let :album do
        build(
          :spotify_distribution_album,
          :with_legacy_cover_image,
          album_type: "Album",
          artists: [main_artist],
          copyright_pline: "2018 Main Artist",
          delivery_type: :full_delivery,
          genres: ["Alternative"],
          genre: 1,
          label_name: "Main Artist",
          release_date: "2018-12-17",
          actual_release_date: "2018-12-17T00:00:00",
          store_id: Store.find_by(abbrev: "sp").id,
          store_name: "Spotify",
          title: "Spotify Test 12 July 2019",
          tracks: tracks,
          upc: "859700000373",
          uuid: "1f07a530e4780136efe73dfd75779dd7",
          countries: ["BH"],
          explicit_lyrics: true,
          test_delivery: true
        )
      end

      let :fixture_xml do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/spotify_rollup_contributors_multi_role_featured_artist.xml"))
      end

      before :each do
        StoreDeliveryConfig.readonly(false).for("Spotify").update(test_delivery: true)
        allow(Digest::MD5).to receive(:file).and_return("77c2eef169963a7ed5ec05761223333e")
        allow(album).to receive(:xml_timestamp).and_return("2018-12-17T22:04:24+00:00")
        allow(album).to receive(:get_uuid).and_return("a3717db0e4750136efe23dfd75779dd7")
        allow(album).to receive(:is_ep?).and_return(false)
        tracks.each do |t|
          t.album = album
          allow(t).to receive(:formatted_duration).and_return("PT00H00M22S")
        end
      end

      it 'should be a valid album' do
        expect(album).to be_valid
      end

      it 'should have valid xml' do
        expect(album.validate_xml).to be_truthy
      end

      it 'should generate the correct xml with all contributors including multiple roles for the featured artist' do
        result = compare_xml(album.to_xml, fixture_xml)
        expect(result).to be_truthy
      end
    end

    context "successfully with multi role main artist" do
      let :tracks do
        [
          build(
            :distribution_system_track,
            artists: [
              multi_role_main_artist,
              featured_artist,
              multi_role_featured_artist,
            ],
            duration: "PT00H00M22S",
            isrc: "TCAAA1800040",
            title: "Song 1",
            available_for_streaming: true,
            audio_file: "859700000373_01_1.flac",
          )
        ]
      end
      let :album do
        build(
          :spotify_distribution_album,
          :with_legacy_cover_image,
          album_type: "Album",
          artists: [multi_role_main_artist],
          copyright_pline: "2018 Main Artist",
          delivery_type: :full_delivery,
          genres: ["Alternative"],
          genre: 1,
          label_name: "Main Artist",
          release_date: "2018-12-17",
          actual_release_date: "2018-12-17T00:00:00",
          store_id: Store.find_by(abbrev: "sp").id,
          store_name: "Spotify",
          title: "Spotify Test 12 July 2019",
          tracks: tracks,
          upc: "859700000373",
          uuid: "1f07a530e4780136efe73dfd75779dd7",
          countries: ["BH"],
          explicit_lyrics: true,
          test_delivery: true
        )
      end

      let :fixture_xml do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/spotify_rollup_contributors_multi_role_main_artist.xml"))
      end

      before :each do
        StoreDeliveryConfig.readonly(false).for("Spotify").update(test_delivery: true)
        allow(Digest::MD5).to receive(:file).and_return("77c2eef169963a7ed5ec05761223333e")
        allow(album).to receive(:xml_timestamp).and_return("2018-12-17T22:04:24+00:00")
        allow(album).to receive(:get_uuid).and_return("a3717db0e4750136efe23dfd75779dd7")
        allow(album).to receive(:is_ep?).and_return(false)
        tracks.each do |t|
          t.album = album
          allow(t).to receive(:formatted_duration).and_return("PT00H00M22S")
        end
      end

      it 'should be a valid album' do
        expect(album).to be_valid
      end

      it 'should have valid xml' do
        expect(album.validate_xml).to be_truthy
      end

      it 'should generate the correct xml with all contributors and separate section for main artists other roles ' do
        # diff = Diffy::Diff.new(fixture_xml.to_s, Nokogiri::XML(album.to_xml, nil, 'utf-8').to_s, context: 2)
        # puts JSON.parse(diff.diff)
        # File.open('valid.xml', 'w') do |file|
        #   file.puts album.to_xml
        # end
        result = compare_xml(album.to_xml, fixture_xml)
        expect(result).to be_truthy
      end
    end
  end
end
