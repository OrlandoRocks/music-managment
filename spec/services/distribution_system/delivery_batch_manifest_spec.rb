require "rails_helper"

describe DistributionSystem::DeliveryBatchManifest do
  describe ".write_manifest" do
    it "generates a manifest xml from a delivery batch id" do
      distributable = create(:distribution)
      album         = build(:distribution_system_album, :with_tracks, :with_booklet, :with_artwork, store_name: "Spotify", store_id: 26)
      bundle        = build(:believe_bundle, skip_batching: true, use_manifest: true, album: album)
      bundle.create_bundle
      batch_item = create(:delivery_batch_item,deliverable: distributable)
      d_batch = create(:delivery_batch, delivery_batch_items: [batch_item.reload])
      item_params = {
        xml_remote_path: "metadata_path",
        uuid: bundle.album.get_uuid,
        upc: bundle.album.upc,
        takedown: bundle.album.takedown,
        xml_hashsum: "hashum, yo"
      }
      batch_item.mark_as_sent
      batch_item.add_to_manifest(item_params)

      manifest_doc = Nokogiri::XML(DistributionSystem::DeliveryBatchManifest.write_manifest(d_batch))
      xml_uuid     = manifest_doc.at_xpath("//MessageInBatch//MessageId").children.to_s
      expect(xml_uuid).to eq(bundle.album.get_uuid)

      recipient_id = manifest_doc.at_xpath("//MessageHeader//MessageRecipient/PartyId").children.to_s
      expect(recipient_id).to eq(album.party_id)
    end
  end
end
