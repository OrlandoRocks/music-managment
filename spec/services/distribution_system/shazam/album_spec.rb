require "rails_helper"
RSpec.describe DistributionSystem::Shazam::Album do
  describe "#to_xml" do

    let(:album) do
      YAML.load_file("spec/distribution_fixtures/converted_albums/1340271_11233379_shazam.yml")
    end

    let(:fixture_xml) do
      Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/1340271_shazam.xml"))
    end

    before(:each) do
      expect(DistributionSystem::CheckSumHelper).to receive(:checksum_file).exactly(2).times.and_return("8e1705d57c56c106a7c54c0a26be4717")
      album.artwork_file.asset = "spec/distribution_fixtures/assets/cover.jpg"
      album.tracks.each do |track|
        allow(track).to receive(:audio_file).exactly(4).times.and_return("spec/distribution_fixtures/assets/track.wav")
      end
    end

    it "matches xml fixture" do
      expect(
        EquivalentXml.equivalent?(
          Nokogiri::XML(album.to_xml, nil, 'utf-8'),
          fixture_xml,
          :element_order => false,
          :normalize_whitespace => true
        )
      ).to eq true
    end
  end
end
