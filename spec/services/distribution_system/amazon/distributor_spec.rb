require "rails_helper"

describe DistributionSystem::Amazon::Distributor do
  let(:work_dir) { DELIVERY_CONFIG["work_dir"] }
  let(:album) do
    YAML.load_file("spec/distribution_fixtures/converted_albums/amazon_wholesale_price.yml")
  end
  let(:dir_name) { FileUtils.mkdir_p("#{work_dir}/Amazon/#{album.upc}").first}

  let(:config) do
    YAML.load_file(Rails.root.join("config", "delivery.yml"))[Rails.env]
  end

  let(:shell) do
    DistributionSystem::Shell.new
  end

  let(:transcoder) do
    DistributionSystem::Transcoder.new({
      mp3_decoder:  "lame",
      alac_decoder: "alac",
      aac_decoder:  "faad",
      flac_encoder: "flac",
      flac_decoder: "flac",
      aac_encoder:  "faac",
      shell:        shell,
      tempdir:      DistributionSystem::TempDir.new
    })
  end

  let(:s3) do
    DistributionSystem::S3.new("asset_bucket")
  end

  let(:distribution) { FactoryBot.create(:distribution) }

  let(:bundle) do
    DistributionSystem::Amazon::Bundle.new(
     work_dir,
     transcoder,
     s3,
     album,
     StoreDeliveryConfig.for(album.store_name)
    )
  end

  let(:distributor) do
    DistributionSystem::Amazon::Distributor.new({
          transcoder:   transcoder,
          s3:           s3,
          work_dir:     work_dir,
          bundle:       bundle
        })
  end

  describe "#distribute" do
    context "valid bundle" do
      before(:each) do
        allow(Digest::MD5).to receive(:file).and_return("rando-hash")
        allow(bundle).to receive(:collect_files).and_return(true)
        allow(DistributionSystem::CheckSumHelper).to receive(:checksum_file).and_return('4e7425289bb57391f358c3d24adf0228')
      end

      it "collects the files for the bundle" do
        expect(distributor).to receive(:send_bundle).and_return(true)
        expect(bundle).to receive(:collect_files)
        distributor.distribute(album, distribution)
      end
      it "writes the XML for the bundle" do
        expect(distributor).to receive(:send_bundle).and_return(true)
        expect(bundle).to receive(:write_metadata)
        distributor.distribute(album, distribution)
      end
      it "validates the bundle" do
        expect(distributor).to receive(:do_send_bundle).and_return(true)
        distributor.distribute(album, distribution)
      end

      before do
        allow(album).to receive(:delivery_type).and_return("full_delivery")
      end

      it "sends the bundle to the remote server" do
        time_now     = bundle.timestamp
        bundle_id    = bundle.album.upc
        remote_dir   = "#{bundle.remote_dir}/#{bundle_id}_#{time_now}"
        resource_dir = "#{remote_dir}/resources"
        sftp         = double("net_sftp").as_null_object
        interpreter  = DistributionSystem::Sftp::Interpreter.new(sftp)
        expect(Net::SFTP).to receive(:start).and_return(sftp)
        expect(DistributionSystem::Sftp::Interpreter).to receive(:new).and_return(interpreter)

        expect(interpreter).to receive(:mkdir_p).ordered.with(remote_dir)
        expect(interpreter).to receive(:mkdir_p).ordered.with(resource_dir)

        expect(interpreter).to receive(:upload).ordered.with(File.join(dir_name, "#{bundle_id}.jpg"),File.join(resource_dir, "#{bundle_id}.jpg"))

        album.tracks.each do |track|
          track_file        = File.join(dir_name, "#{bundle_id}_01_#{track.number}.flac")
          remote_track_file = File.join(resource_dir, "#{bundle_id}_01_#{track.number}.flac")
          expect(interpreter).to receive(:upload).with(track_file, remote_track_file)
        end
        metadata_filename        = File.join(dir_name, "#{bundle_id}.xml")
        remote_metadata_filename = File.join(remote_dir, "#{bundle_id}.xml")
        local_complete_file      = File.join(dir_name, "delivery.complete")
        complete_file            = File.join(remote_dir, "delivery.complete")
        expect(interpreter).to receive(:upload).ordered.with(metadata_filename, remote_metadata_filename)

        expect(interpreter).not_to receive(:upload).ordered.with(local_complete_file, complete_file)

        expect(distributor).not_to receive(:send_complete_file)

        distributor.distribute(album, distribution)
      end
    end
  end
end
