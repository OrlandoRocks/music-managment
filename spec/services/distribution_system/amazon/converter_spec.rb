require "rails_helper"

describe DistributionSystem::Amazon::Converter do
  describe ".convert" do
    let(:store)          { Store.find(13) }
    let(:variable_price) { create(:variable_price, price_code: "FRONTLINE", store_id: 13, price_code_display: "Frontline", active: 1, position: 1, user_editable: 1, price: 0.99) }

    context "album has creatives" do
      before(:each) do
        allow(store).to receive(:delivery_service).and_return("tunecore")
        @album    = create(:album, :with_artwork, primary_genre: Genre.find(1))
        artist2   = Artist.last
        song      = create(:song, :with_upload, album_id: @album.id)
        creative2 = create(:creative, creativeable_type: "Song", creativeable_id: song.id, artist_id: artist2.id, role: "with")
        CreativeSongRole.create(creative_id: creative2.id, song_role_id: 2, song_id: song.id)
        petri_bundle = create(:petri_bundle, album_id: @album.id)
        distribution = create(:distribution, :with_salepoint, petri_bundle_id: petri_bundle.id, converter_class: "Musicstores::Amazon::Converter", state: "new", delivery_type: "metadata_only")
        salepoint = distribution.salepoints.first
        allow(salepoint).to receive(:store).and_return(store)
        allow(distribution).to receive(:store).and_return(store)
        @converted_album = Delivery::AlbumConverter.convert(distribution, "metadata_only")
      end

      it "generates an album with the correct artist roles" do
        artist = @converted_album.artists.first
        expect(artist.roles).to include("primary_artist")
      end

      it "generates album tracks with the correct artist roles" do
        artist = @converted_album.tracks.first.artists.first
        expect(artist.roles).to include("with", "producer")
      end

      it "track knows who primary artist is" do
        track = @converted_album.tracks.first
        expect(track.primary_artist.name).to eq(@album.artist_name)
      end

      it "artist knows if it is primary" do
        artist = @converted_album.artists.first
        expect(artist.is_primary?).to eq(true)
      end
    end

    context "album is various artists" do
      before(:each) do
        allow(store).to receive(:delivery_service).and_return("tunecore")
        album = create(:album, :with_artwork, primary_genre: Genre.find(1), is_various: true)
        song  = create(:song, :with_upload, album_id: album.id)
        petri_bundle = create(:petri_bundle, album_id: album.id)
        distribution = create(:distribution, :with_salepoint, petri_bundle_id: petri_bundle.id, converter_class: "Musicstores::Amazon::Converter", state: "new", delivery_type: "metadata_only")
        salepoint = distribution.salepoints.first
        allow(salepoint).to receive(:store).and_return(store)
        allow(distribution).to receive(:store).and_return(store)
        @converted_album = Delivery::AlbumConverter.convert(distribution, "metadata_only")
      end

      it "builds an album with one artist with the name Various Artists and roles including 'primary'" do
        artist = @converted_album.artists.first
        expect(artist.name).to eq("Various Artists")
        expect(artist.roles).to match(["primary"])
      end
    end
  end
end
