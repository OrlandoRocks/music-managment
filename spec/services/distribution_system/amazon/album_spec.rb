require "rails_helper"

RSpec.describe DistributionSystem::Amazon::Album do
  describe "#to_xml" do
    before(:each) do
      allow(DistributionSystem::CheckSumHelper).to receive(:checksum_file).and_return(123456)
    end

    context "without variable pricing" do
      let(:album) do
        YAML.load_file("spec/distribution_fixtures/converted_albums/amazon_wholesale_price.yml")
      end

      it "generates valid DDEX v.3.7 xml for albums" do
        album.vendor_code = "TUCI"
        album.set_data_from_config(StoreDeliveryConfig.for(album.store_name))
        expect(album.validate_xml).to be true
      end
    end

    context "with variable pricing" do
      let(:album) do
        YAML.load_file("spec/distribution_fixtures/converted_albums/amazon_variable_price.yml")
      end

      it "generates valid DDEX v.3.7 xml for albums" do
        album.vendor_code = "TUCI"
        album.set_data_from_config(StoreDeliveryConfig.for(album.store_name))
        expect(album.validate_xml).to be true
      end
    end

    context "Generates Amazon-approved xml for" do
      let(:amazon_config){ StoreDeliveryConfig.for("amazon") }

      context "when the genre is instrumental" do 
        it "genreates xml with IsInstrumental node and omits LanguageOfPerformance node" do
          album = YAML.load_file("spec/distribution_fixtures/converted_albums/amazon_album_only.yml")
          album.delivery_type = "metadata_only"
          album.genre = Genre::INSTRUMENTAL_GENRE_ID
          album.set_data_from_config(amazon_config)
          allow(album).to receive(:get_uuid).and_return("4c2cd210c24601357c060ecc0b9da1a7")
          allow(album).to receive(:xml_timestamp).and_return("2017-12-13T15:14:54+00:00")
          approved_xml = Nokogiri::XML(File.open("spec/distribution_fixtures/converted_albums/amazon_instrumental.xml"))
          #diff = Diffy::Diff.new(approved_xml.to_s, Nokogiri::XML(album.to_xml, nil, 'utf-8').to_s, :context => 2)
          expect(album.validate_xml).to be true
          expect(compare_xml(album.to_xml, approved_xml)).to be true 
        end 
      end 

      it "an album with album_only tracks" do
        album = YAML.load_file("spec/distribution_fixtures/converted_albums/amazon_album_only.yml")
        album.delivery_type = "metadata_only"
        album.set_data_from_config(amazon_config)
        allow(album).to receive(:get_uuid).and_return("4c2cd210c24601357c060ecc0b9da1a7")
        allow(album).to receive(:xml_timestamp).and_return("2017-12-13T15:14:54+00:00")
        approved_xml = Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/amazon_album_only.xml"))
        #diff = Diffy::Diff.new(approved_xml.to_s, Nokogiri::XML(album.to_xml, nil, 'utf-8').to_s, :context => 2)
        expect(album.validate_xml).to be true
        expect(compare_xml(album.to_xml, approved_xml)).to be true
      end


      it "an album with a free track" do
        album = YAML.load_file("spec/distribution_fixtures/converted_albums/amazon_free_track.yml")
        album.delivery_type = "metadata_only"
        album.set_data_from_config(amazon_config)
        allow(album).to receive(:get_uuid).and_return("4c2cd210c24601357c060ecc0b9da1a7")
        allow(album).to receive(:xml_timestamp).and_return("2017-12-13T15:14:54+00:00")
        approved_xml = Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/amazon_free_track.xml"))
        #diff = Diffy::Diff.new(approved_xml.to_s, Nokogiri::XML(album.to_xml, nil, 'utf-8').to_s, :context => 2)
        expect(album.validate_xml).to be true
        expect(compare_xml(album.to_xml, approved_xml)).to be true
      end

      it "an album with territory_specific_cost tiers" do
        album = YAML.load_file("spec/distribution_fixtures/converted_albums/amazon_territory_specific_cost.yml")
        album.delivery_type = "metadata_only"
        album.set_data_from_config(amazon_config)
        allow(album).to receive(:get_uuid).and_return("4c2cd210c24601357c060ecc0b9da1a7")
        allow(album).to receive(:xml_timestamp).and_return("2017-12-13T15:14:54+00:00")
        approved_xml = Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/amazon_territory_specific_cost.xml"))
        #diff = Diffy::Diff.new(approved_xml.to_s, Nokogiri::XML(album.to_xml, nil, 'utf-8').to_s, :context => 2)
        expect(album.validate_xml).to be true
        expect(compare_xml(album.to_xml, approved_xml)).to be true
      end

      it "an album with a track specific_cost tier" do
        album = YAML.load_file("spec/distribution_fixtures/converted_albums/amazon_track_specific_cost.yml")
        album.delivery_type = "metadata_only"
        album.set_data_from_config(amazon_config)
        allow(album).to receive(:get_uuid).and_return("4c2cd210c24601357c060ecc0b9da1a7")
        allow(album).to receive(:xml_timestamp).and_return("2017-12-13T15:14:54+00:00")
        approved_xml = Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/amazon_track_specific_cost.xml"))
        #diff = Diffy::Diff.new(approved_xml.to_s, Nokogiri::XML(album.to_xml, nil, 'utf-8').to_s, :context => 2)
        expect(album.validate_xml).to be true
        expect(compare_xml(album.to_xml, approved_xml)).to be true
      end

      it "an album with a pdf booklet" do
        album = YAML.load_file("spec/distribution_fixtures/converted_albums/amazon_pdf_booklet.yml")
        album.delivery_type = "metadata_only"
        album.set_data_from_config(amazon_config)
        allow(album).to receive(:get_uuid).and_return("4c2cd210c24601357c060ecc0b9da1a7")
        allow(album).to receive(:xml_timestamp).and_return("2017-12-13T15:14:54+00:00")
        approved_xml = Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/amazon_pdf_booklet.xml"))
        expect(album.validate_xml).to be true
        expect(compare_xml(album.to_xml, approved_xml)).to be true
      end

      it "an album with a territory-specific takedown" do
        album = YAML.load_file("spec/distribution_fixtures/converted_albums/amazon_country_takedown.yml")
        album.delivery_type = "metadata_only"
        album.set_data_from_config(amazon_config)
        allow(album).to receive(:get_uuid).and_return("4c2cd210c24601357c060ecc0b9da1a7")
        allow(album).to receive(:xml_timestamp).and_return("2017-12-13T15:14:54+00:00")
        approved_xml = Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/amazon_country_takedown.xml"))
        expect(album.validate_xml).to be true
        expect(compare_xml(album.to_xml, approved_xml)).to be true
      end
    end

    context "various artists" do
      describe "#to_xml" do
        it "does not display 'Various Artists' on the track level in the node DisplayArtistName" do
          album = YAML.load_file("spec/distribution_fixtures/converted_albums/amazon_various_artists.yml")
          album.delivery_type = "metadata_only"
          album.set_data_from_config(StoreDeliveryConfig.for(album.store_name))
          artist_name = Nokogiri::XML(album.to_xml).at_xpath("//Release[position()>1]//ReleaseDetailsByTerritory//DisplayArtistName").children.to_s
          expect(artist_name).not_to eq("Various Artists")
        end
      end
    end
  end
end
