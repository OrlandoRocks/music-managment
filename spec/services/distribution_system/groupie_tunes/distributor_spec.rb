require "rails_helper"
require 'net/ftp'

describe DistributionSystem::GroupieTunes::Distributor do
  let(:date)     { "20170906" }
  let(:work_dir) { DELIVERY_CONFIG["work_dir"] }
  let(:album) do
    YAML.load_file("spec/distribution_fixtures/converted_albums/groupie_tunes.yml")
  end
  let(:delivery_config) {StoreDeliveryConfig.for(album.store_name)}

  let(:config) do
    YAML.load_file(Rails.root.join("config", "delivery.yml"))[Rails.env]
  end

  let(:sftp_config) {
    {
      host:     delivery_config["hostname"],
      username: delivery_config["username"],
      password: delivery_config["password"],
      port:     delivery_config["port"],
      logger:   logger
    }
  }


  let(:shell) do
    DistributionSystem::Shell.new
  end

  let(:transcoder) do
    DistributionSystem::Transcoder.new({
      mp3_decoder:  "lame",
      alac_decoder: "alac",
      aac_decoder:  "faad",
      flac_encoder: "flac",
      flac_decoder: "flac",
      aac_encoder:  "faac",
      shell:        shell,
      tempdir:      DistributionSystem::TempDir.new
    })
  end

  let(:s3) do
    DistributionSystem::S3.new("asset_bucket")
  end

  let(:distribution) { FactoryBot.create(:distribution) }
  let(:bundle)       { DistributionSystem::GroupieTunes::Bundle.new(
                          work_dir,
                          transcoder,
                          s3,
                          album,
                          delivery_config
                        )
                      }

  let(:distributor)   {
    DistributionSystem::GroupieTunes::Distributor.new({
      schema:       DistributionSystem::Schema.new(
                      Rails.root.join("lib", "distribution_system", "groupie_tunes", "manifest.dtd")),
      bundle:       bundle
    })
  }

  describe "#distribute" do
    before(:each) do
      StoreDeliveryConfig.for("GroupieTun").update(transfer_type: "ftp")
      allow(bundle).to receive(:collect_files).and_return(true)
    end

    it "collects the bundle files" do
      expect(bundle).to receive(:collect_files)
      allow(distributor).to receive(:write_metadata)
      allow(distributor).to receive(:send_bundle)
      distributor.distribute(album, distribution)
    end

    it "writes the bundle metadata" do
      expect(bundle).to receive(:write_metadata)
      allow(distributor).to receive(:send_bundle)
      distributor.distribute(album, distribution)
    end

    it "sends to GroupieTunes" do
      allow_any_instance_of(DistributionSystem::Server).to receive(:prevent_external_deliveries?).and_return(false)
      ftp     = double("net_ftp").as_null_object
      session = double("session").as_null_object
      allow(bundle).to receive(:write_metadata)
      allow(distributor).to receive(:folder_name).and_return(date)
      allow(distributor).to receive(:validate_bundle).and_return(true)
      allow_any_instance_of(DistributionSystem::Ftp).to receive(:initialize_ftp).and_return(ftp)
      allow(DistributionSystem::Ftp::Session).to receive(:new).with(ftp).and_return(session)
      expect(session).to receive(:cp_r).with("#{work_dir}/GroupieTun/#{album.upc}", date, bundle.album.delivery_type, bundle.album.takedown)
      distributor.distribute(album, distribution)
    end
  end
end
