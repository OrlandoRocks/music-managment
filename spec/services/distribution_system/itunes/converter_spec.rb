require "rails_helper"

describe DistributionSystem::Itunes::Converter do
  describe ".convert" do
    before(:each) do
      itunes = Store.find(36)
      allow(itunes).to receive(:delivery_service).and_return("tunecore")
      album     = create(:album, :with_artwork, primary_genre: Genre.find(1))
      artist    = Artist.first
      artist2   = Artist.last
      song     = create(:song, :with_upload, album_id: album.id)
      lyric     = create(:lyric, song_id: song.id, content: "Let's get schwifty in here")
      creative1 = create(:creative, creativeable_type: "Album", creativeable_id: album.id, role: "primary_artist", artist_id: artist.id)
      creative2 = create(:creative, creativeable_type: "Song", creativeable_id: song.id, artist_id: artist2.id, role: "with")
      ExternalServiceId.create(service_name: "apple", linkable_id: creative1.id, linkable_type: "Creative", identifier: "12345")
      ExternalServiceId.create(service_name: "apple", linkable_id: creative2.id, linkable_type: "Creative", identifier: "12345")
      CreativeSongRole.create(creative_id: creative2.id, song_role_id: 2, song_id: song.id)
      petri_bundle = create(:petri_bundle, album_id: album.id)
      distribution = create(:distribution, :with_salepoint, petri_bundle_id: petri_bundle.id, converter_class: "Musicstores::Itunes::Converter", state: "new", delivery_type: "metadata_only")
      salepoint = distribution.salepoints.first
      allow(salepoint).to receive(:store).and_return(itunes)
      allow(distribution).to receive(:store).and_return(itunes)
      @converted_album = Delivery::AlbumConverter.convert(distribution, "metadata_only")
    end

    it "generates an album with the correct artist roles" do
      creative = @converted_album.creatives.first
      expect(creative.primary).to be true
      expect(creative.roles).to eq(["Performer"])
    end

    it "generates tracks with the correct artist roles" do
      creatives = @converted_album.tracks.first.creatives
      expect(creatives.first.primary).to be true
      expect(creatives.first.roles).to eq(["Performer"])
      expect(creatives.last.primary).to be false
      expect(creatives.last.roles).to include("With")
    end

    it "generates tracks with lyrics" do
      lyrics = @converted_album.tracks.first.lyrics
      expect(lyrics).to_not eq nil
      expect(lyrics.content).to eq("Let's get schwifty in here")
      expect(lyrics.track_title).to eq(@converted_album.tracks.first.title)
      expect(lyrics.language_code).to eq(@converted_album.language_code)
    end

    it "generates creatives with apple artist IDs" do
      expect(@converted_album.creatives.last.apple_artist_id).to eq("12345")
    end

    context "song does not have its own language code" do
      it "generates tracks with the album's language code" do
        track = @converted_album.tracks.first
        expect(track.language_code).to eq(@converted_album.language_code)
      end
    end

    context "when artist has a curated artist" do
      let(:artist_names) { @converted_album.creatives.map(&:name) }
      before do
        Creative.where(id: @converted_album.creatives.collect(&:id))
              .update(curated_artist_flag: true)
      end

      it "populates curated_artists" do
        expect(@converted_album.curated_artists.pluck(:name)).to eq(artist_names)
      end
    end

    context "song has its own language code" do
      it "generates tracks with language code" do
        album        = create(:album, :with_uploaded_song_and_artwork)
        petri_bundle = create(:petri_bundle, album: album)
        distribution = create(:distribution, :with_salepoint, {
          converter_class: "Musicstores::Itunes::Converter",
          state:           "new",
          delivery_type:   "metadata_only",
          petri_bundle:    petri_bundle
        })
        salepoint = distribution.salepoints.first
        itunes    = Store.find_by(name: "iTunes")
        allow(salepoint).to receive(:store).and_return(itunes)
        allow(distribution).to receive(:store).and_return(itunes)

        converted_album = Delivery::AlbumConverter.convert(distribution, "metadata_only")

        track              = converted_album.tracks.first
        song_language_code = album.songs.first.metadata_language_code.code
        expect(track.language_code).to eq(song_language_code)
      end
    end

    context "song doesn't have any language codes" do
      it "gets it from the album's metadata_language_code" do
        album        = create(:album, :with_uploaded_song_and_artwork, metadata_language_code: LanguageCode.find_by(code: 'no'))
        album.songs.each {|s| s.update!(metadata_language_code: nil)}

        petri_bundle = create(:petri_bundle, album: album)
        distribution = create(:distribution, :with_salepoint, {
          converter_class: "Musicstores::Itunes::Converter",
          state:           "new",
          delivery_type:   "metadata_only",
          petri_bundle:    petri_bundle
        })
        salepoint = distribution.salepoints.first
        itunes    = Store.find_by(name: "iTunes")
        allow(salepoint).to receive(:store).and_return(itunes)
        allow(distribution).to receive(:store).and_return(itunes)

        converted_album = Delivery::AlbumConverter.convert(distribution, "metadata_only")

        track              = converted_album.tracks.first
        expect(track.language_code).to eq(album.metadata_language_code.code)
      end
    end
  end

  context "takedowns" do
    it "includes all sanctioned territories for iTunesWW salepoints" do
      album         = create(:album, :with_artwork, primary_genre: Genre.find(16))
      allow_any_instance_of(Album).to receive(:all_country_iso_codes).and_return(Country.unscoped.all.map(&:iso_code))

      petri_bundle  = create(:petri_bundle, album_id: album.id)
      distribution  = create(:distribution, :with_salepoint, petri_bundle_id: petri_bundle.id, converter_class: "Musicstores::Itunes::Converter", state: "new", delivery_type: "metadata_only")
      salepoint     = distribution.salepoints.first
      store = Store.find(36)
      allow(salepoint).to receive(:store).and_return(store)
      allow(distribution).to receive(:store).and_return(store)
      album.takedown!
      converted_album = Delivery::AlbumConverter.convert(distribution, "metadata_only")
      sanctioned  = Country::SANCTIONED_COUNTRIES

      territories = converted_album.products.map(&:territory)
      expect(sanctioned.all?{|c| territories.include?(c)}).to be true
    end
  end

  context "when artist apple id is 'unavailable'" do
    it "parses 'unavailable' id to nil" do
      itunes = Store.find(36)
      allow(itunes).to receive(:delivery_service).and_return("tunecore")
      album = create(:album, :with_artwork, primary_genre: Genre.find(1))

      ExternalServiceId.create(
        service_name: "apple",
        linkable_id: album.creatives.first.id,
        linkable_type: "Creative",
        identifier: "unavailable"
      )

      petri_bundle = create(:petri_bundle, album_id: album.id)
      distribution = create(:distribution, :with_salepoint, petri_bundle_id: petri_bundle.id, converter_class: "Musicstores::Itunes::Converter", state: "new", delivery_type: "metadata_only")
      salepoint = distribution.salepoints.first
      allow(salepoint).to receive(:store).and_return(itunes)
      allow(distribution).to receive(:store).and_return(itunes)
      converted_album = Delivery::AlbumConverter.convert(distribution, "metadata_only")

      expect(converted_album.creatives.map(&:apple_artist_id)).to eq([nil])
    end

    it "parses unavailable id for new_artist to nil" do
      itunes = Store.find(36)
      allow(itunes).to receive(:delivery_service).and_return("tunecore")
      album = create(:album, :with_artwork, primary_genre: Genre.find(1))

      ExternalServiceId.create(
        service_name: "apple",
        linkable_id: album.creatives.first.id,
        linkable_type: "Creative",
        identifier: "unavailable",
        state: "new_artist"
      )

      petri_bundle = create(:petri_bundle, album_id: album.id)
      distribution = create(:distribution, :with_salepoint, petri_bundle_id: petri_bundle.id, converter_class: "Musicstores::Itunes::Converter", state: "new", delivery_type: "metadata_only")
      salepoint = distribution.salepoints.first
      allow(salepoint).to receive(:store).and_return(itunes)
      allow(distribution).to receive(:store).and_return(itunes)
      converted_album = Delivery::AlbumConverter.convert(distribution, "metadata_only")

      expect(converted_album.creatives.map(&:apple_artist_id)).to eq([nil])
    end


    it "does not parse other vals to nil" do
      itunes = Store.find(36)
      allow(itunes).to receive(:delivery_service).and_return("tunecore")
      album = create(:album, :with_artwork, primary_genre: Genre.find(1))

      ExternalServiceId.create(
        service_name: "apple",
        linkable_id: album.creatives.first.id,
        linkable_type: "Creative",
        identifier: "derp",
        state: "new_artist"
      )

      petri_bundle = create(:petri_bundle, album_id: album.id)
      distribution = create(:distribution, :with_salepoint, petri_bundle_id: petri_bundle.id, converter_class: "Musicstores::Itunes::Converter", state: "new", delivery_type: "metadata_only")
      salepoint = distribution.salepoints.first
      allow(salepoint).to receive(:store).and_return(itunes)
      allow(distribution).to receive(:store).and_return(itunes)
      converted_album = Delivery::AlbumConverter.convert(distribution, "metadata_only")

      expect(converted_album.creatives.map(&:apple_artist_id)).to eq(['derp'])
    end
  end
end
