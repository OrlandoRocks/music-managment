require "rails_helper"
RSpec.describe DistributionSystem::Itunes::Album do
  describe "#to_xml" do
    it "matches the fixture" do
      album = build(:distribution_system_itunes_album, :with_booklet, :with_tracks, :with_creatives, :with_artwork, :with_products)
      itunes_xml_fixture = Rails.root.join("spec", "distribution_fixtures", "metadata", "itunes_5.3.xml")
      track = album.tracks.first
      track.language_code = "de"

      expect(
        EquivalentXml.equivalent?(
          Nokogiri::XML(album.to_xml, nil, 'utf-8'),
          Nokogiri::XML(File.open(itunes_xml_fixture)),
          :element_order => false,
          :normalize_whitespace => true
        )
      ).to be_truthy
    end

    it "doesn't include apple_artist_id when nil" do
      album = build(:distribution_system_itunes_album, :with_booklet, :with_tracks, :with_creatives, :with_artwork, :with_products)
      album.creatives.each { |c| c.apple_artist_id = nil }
      album.tracks.each { |t| t.creatives.each { |c| c.apple_artist_id = nil } }
      xml = album.to_xml

      expect(xml.include?('apple_id')).to be_falsey
    end

    it "includes apple_artist_id when not nil" do
      album = build(:distribution_system_itunes_album, :with_booklet, :with_tracks, :with_creatives, :with_artwork, :with_products)
      xml = album.to_xml

      expect(xml.include?('apple_id')).to be_truthy
    end

    context "when a track is instrumental" do
      it "matches the fixture" do
        album = build(:distribution_system_itunes_album, :with_booklet, :with_tracks, :with_creatives, :with_artwork, :with_products)
        itunes_xml_fixture = Rails.root.join("spec", "distribution_fixtures", "metadata", "itunes_instrumental_track.xml")
        track = album.tracks.first
        track.instrumental = true

        expect(
          EquivalentXml.equivalent?(
            Nokogiri::XML(album.to_xml, nil, 'utf-8'),
            Nokogiri::XML(File.open(itunes_xml_fixture)),
            element_order: false,
            normalize_whitespace: true
          )
        ).to be_truthy
      end
    end
  end

  context "track language requires locales" do
    it "creates locales" do
      album = build(:distribution_system_itunes_album, :with_booklet, :with_tracks, :with_creatives, :with_artwork, :with_products)
      itunes_with_locales_xml_fixture = Rails.root.join("spec", "distribution_fixtures", "metadata","itunes_with_locales.xml")
      track = album.tracks.first
      track.language_code = "ko"
      track.english_title = "english translation"

      expect(
        EquivalentXml.equivalent?(
          Nokogiri::XML(album.to_xml, nil, 'utf-8'),
          Nokogiri::XML(File.open(itunes_with_locales_xml_fixture)),
          :element_order => false,
          :normalize_whitespace => true
        )
      ).to be_truthy
    end
  end
end
