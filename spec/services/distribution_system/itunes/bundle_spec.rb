require "rails_helper"

describe DistributionSystem::Itunes::Bundle do
  let(:work_dir) { DELIVERY_CONFIG["work_dir"] }
  let(:transcoder) do
    DistributionSystem::Transcoder.new({
      mp3_decoder:  "lame",
      alac_decoder: "alac",
      aac_decoder:  "faad",
      flac_encoder: "flac",
      flac_decoder: "flac",
      aac_encoder:  "faac",
      shell:        double.as_null_object,
      tempdir:      DistributionSystem::TempDir.new
    })
  end

  let(:s3) do
    DistributionSystem::S3.new("asset_bucket")
  end

  let(:album)   { build(:distribution_system_itunes_album,
                        :with_tracks, :with_creatives,
                        :with_artwork, :with_products)
                }
  let(:bundle)  { DistributionSystem::Itunes::Bundle.new(work_dir, transcoder, s3, album) }

  context "metadata_only update" do
    describe "#write_metadata" do
      let(:bundle_dir) { File.join(work_dir, "Itunes", "#{album.upc}.itmsp")}
      before(:each) do
        bundle.create_bundle
        allow(album).to receive(:album_length).and_return(500)
        allow(album).to receive(:is_single?).and_return(true)
        bundle.write_metadata
      end

      it "writes the album metadata file" do
        expect(Dir.entries(bundle_dir)).to include("metadata.xml")
      end

      it "write the lyrics metadata files" do
        isrc = album.tracks.first.isrc
        expect(Dir.entries(bundle_dir)).to include("#{isrc}.ttml")
      end
    end
  end

  context "file names" do
    before :each do
      @new_alb = build(:distribution_system_itunes_album,
                        :with_tracks, :with_creatives,
                        :with_artwork, :with_products)

      @new_bundle = DistributionSystem::Itunes::Bundle.new(work_dir, transcoder, s3, @new_alb)
    end

    it "uses the tracks's ISRC as the lyrics filename" do
      allow(File).to receive(:size).with(any_args).and_return("full_hundo")
      allow(DistributionSystem::CheckSumHelper).to receive(:checksum_file).with(any_args).and_return("checksum, yo")
      @new_bundle.create_bundle

      lyrics_params = {
        file_name: "#{@new_alb.tracks.first.isrc}.ttml",
        file_size: "full_hundo",
        file_checksum: "checksum, yo"
      }

      expect_any_instance_of(DistributionSystem::Itunes::Lyric).to receive(:update).with(lyrics_params)
      @new_bundle.write_lyrics_metadata
    end
  end
end
