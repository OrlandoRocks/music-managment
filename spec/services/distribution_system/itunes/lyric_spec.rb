require "rails_helper"

describe DistributionSystem::Itunes::Lyric do
  let(:fixture)    { Rails.root.join("spec", "distribution_fixtures", "metadata", "itunes_lyrics.xml") }
  let(:lyrics)     { DistributionSystem::Itunes::Lyric.new(
                       content: "Test lyric\nOh yea\n\nTest lyric second verse\nOh yea",
                       track_title: "Even If I Couldn't Have Tried",
                       language_code: "en")
                   }
  let(:lyrics_xml) { Nokogiri::XML(File.open(fixture)) }

  describe "#to_xml" do
    it "generates the lyrics XML" do
      expect(
        EquivalentXml.equivalent?(
          Nokogiri::XML(lyrics.to_xml),
          lyrics_xml,
          element_order: false,
          normalize_whitespace: true
        )
      ).to eq true
    end
  end
end
