require "rails_helper"

describe DistributionSystem::Itunes::Distributor do
  let(:work_dir) { DELIVERY_CONFIG["work_dir"] }
  let(:album) do
    build(:distribution_system_itunes_album,
            :with_booklet, :with_tracks,
            :with_creatives, :with_artwork,
            :with_products
    )
  end

  let(:config) do
    YAML.load_file(Rails.root.join("config", "delivery.yml"))[Rails.env]
  end

  let(:shell) do
    DistributionSystem::Shell.new
  end

  let(:transcoder) do
    DistributionSystem::Transcoder.new({
      mp3_decoder:  "lame",
      alac_decoder: "alac",
      aac_decoder:  "faad",
      flac_encoder: "flac",
      flac_decoder: "flac",
      aac_encoder:  "faac",
      shell:        shell,
      tempdir:      DistributionSystem::TempDir.new
    })
  end

  let(:s3) do
    DistributionSystem::S3.new("asset_bucket")
  end

  let(:distribution) { FactoryBot.create(:distribution) }
  let(:bundle) do
    DistributionSystem::Itunes::Bundle.new(
      work_dir,
      transcoder,
      s3,
      album
    )
  end

  let(:distributor)   do
    DistributionSystem::Itunes::Distributor.new({
      bundle: bundle
    })
  end

  subject(:distribute) { 
    distributor.distribute(album, distribution)
  }
  
  describe "#distribute" do
    context "valid iTunes transporter response" do
      before(:each) do
        allow(album).to receive(:delivery_type).and_return("metadata_only")
        allow($apple_transporter_client).to receive(:validate_package).and_return({valid: true, output: "success"})
        allow($apple_transporter_client).to receive(:send_package).with(bundle).and_return({success: true})
      end
      context "valid bundle" do
        it "collects the bundle files" do
          expect(distributor).to receive(:send_to_itunes).and_return(true)
          distribute
        end

        it "writes the bundle metadata" do
          expect(distributor).to receive(:send_to_itunes).and_return(true)
          expect(bundle).to receive(:write_metadata)
          distribute
        end

        it "delivers to iTunes via iTMStransporter" do
          expect($apple_transporter_client).to receive(:validate_package).with(bundle)
          expect($apple_transporter_client).to receive(:send_package).with(bundle)
          distribute
        end
      end

      context "invalid bundle" do
        context "when album is not approved for distribution" do 
          before do 
            album.is_approved = false 
            allow($apple_transporter_client).to receive(:validate_package).with(bundle).and_return({valid: false, output: "failure"})
          end 
    
          it "calls discard on the distribution" do 
            expect(distribution).to receive(:discard!).once
            distribute
          end 

          it "updates the state of the distribution to 'discarded'" do 
            previous_state = distribution.state
            expect { distribute }.to change { distribution.state }.from(previous_state).to('discarded')
          end 
        end 

        it "raises an error" do
          allow($apple_transporter_client).to receive(:validate_package).with(bundle).and_return({valid: false, output: "failure"})
          expect { distribute }.to raise_error
        end
      end

      context "#curated_artists" do
        before do
          allow_any_instance_of(DistributionSystem::Distributor).to receive(:collect_files).and_return(true)
          allow_any_instance_of(DistributionSystem::Distributor).to receive(:write_metadata).and_return(true)
        end

        it "raises an error if there are curated artists" do
          bundle.album.curated_artists = ["J-Lo"]
          allow(distributor).to receive(:validate_curated_artists).and_return(%w[Artist1 Artist2])
          allow_any_instance_of(DistributionSystem::Distributor).to receive(:full_delivery?).and_return(true)
          expect { distribute }.to raise_error(RuntimeError, /Itunes package verify failed/)
        end

        it "doesn't raise if there are no curated artists" do
          allow_any_instance_of(DistributionSystem::Distributor).to receive(:full_delivery?).and_return(true)
          expect { distribute }.not_to raise_error(RuntimeError, /Itunes package verify failed/)
        end

        it "doesn't raise an error if there are curated artists but it's not a full delivery" do
          allow(distributor).to receive(:validate_curated_artists).and_return(%w[Artist1 Artist2])
          allow_any_instance_of(DistributionSystem::Distributor).to receive(:full_delivery?).and_return(false)
          expect { distribute }.not_to raise_error(RuntimeError, /Itunes package verify failed/)
        end
      end
    end

    context "invalid response from iTunes transporter"
      let(:external_apple_id) { album.creatives.first.apple_artist_id }

      before(:each) do
        create(:itunes_external_service_id, linkable_id: album.creatives.first.id, identifier: album.creatives.first.apple_artist_id)

        apple_xml = %Q(
            <?xml version="1.0" encoding="UTF-8"?>
              <itunes_transporter>

                <log level="error">
                  <message>
                    ERROR ITMS-4088: &quot;There&apos;s no artist associated with the Apple ID &apos;#{external_apple_id}&apos;.&quot; at Album/Artists/Artist
                  </message>
                </log>
              </itunes_transporter>
            )

        allow(album).to receive(:delivery_type).and_return("metadata_only")
        allow($apple_transporter_client).to receive(:validate_package).and_return({valid: false, output: apple_xml})
        allow($apple_transporter_client).to receive(:send_package).with(bundle).and_return({success: true})
      end

      context "#remove_invalid_apple_ids" do
        it "removes any bad ids" do
          allow(distribution).to receive(:retry).and_return(true)

          distribute

          expect(ExternalServiceId.where(identifier: external_apple_id, linkable_id: album.creatives.first.id).count).to eq(0)
        end


        it "triggers a retry on the distribution" do
          allow(distribution).to receive(:retry).and_return(true)

          expect(distribution).to receive(:retry)

          distribute
        end
      end
  end
end
