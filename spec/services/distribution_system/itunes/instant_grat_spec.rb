require "rails_helper"

describe "InstantGrat Bundle" do
  let(:work_dir) { DELIVERY_CONFIG["work_dir"] }
  let(:transcoder) do
    DistributionSystem::Transcoder.new({
      mp3_decoder:  "lame",
      alac_decoder: "alac",
      aac_decoder:  "faad",
      flac_encoder: "flac",
      flac_decoder: "flac",
      aac_encoder:  "faac",
      shell:        double.as_null_object,
      tempdir:      DistributionSystem::TempDir.new
    })
  end

  let(:s3) do
    DistributionSystem::S3.new("asset_bucket")
  end


  it "sets sales start date at the album level and not at track level" do
    album = build(:distribution_system_itunes_album,
                  :with_tracks, :with_creatives,
                  :with_artwork, :with_products)
    album.products.first.product_type = "album"
    album.tracks << build(:distribution_system_itunes_track, 
                          :with_products, :with_lyrics, :with_artists, 
                          number: 2, 
                          artist_count: 1, album: album, 
                          original_release_date: album.release_date)
    bundle = DistributionSystem::Itunes::Bundle.new(work_dir, transcoder, s3, album) 
    bundle.create_bundle
    allow(album).to receive(:album_length).and_return(500)
    allow(album).to receive(:is_single?).and_return(true)
    bundle.write_metadata

    bundle_dir =  File.join(work_dir, "Itunes", "#{album.upc}.itmsp")
    expect(Dir.entries(bundle_dir)).to include("metadata.xml")

    xml = Nokogiri::XML(File.open(File.join(bundle_dir, "metadata.xml")))
    expect(xml.css('package > album > products > product > sales_start_date').text).to eq('2015-02-15')
    expect(xml.css('package > album > products > product > stream_start_date').text).to eq('2016-09-15')
    expect(xml.css('package > album > products > product > preorder_sales_start_date').text).to eq('2016-09-15')
    expect(xml.css('package > album > tracks  product').size).to eq(2)
    expect(xml.css('package > album > tracks  product').first.css('sales_start_date').empty?).to be(true)
    expect(xml.css('package > album > tracks  product').first.css('stream_start_date').text).to eq('2016-09-15')
    expect(xml.css('package > album > tracks  product')[1].css('stream_start_date').text).to eq('2015-02-15')
  end
end
