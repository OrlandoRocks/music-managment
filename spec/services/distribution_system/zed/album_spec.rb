require "rails_helper"

RSpec.describe DistributionSystem::Believe::Album do
  let(:validate_path) { Rails.root.join("app", "services", "distribution_system", "schemas", "ddex382.xsd") }
  let(:output_path) { "spec/distribution_fixtures/metadata_output" }

  context "when exporting Zed XML" do
    let(:album) do
      build(:distribution_system_album,
            :single,
            :with_tracks,
            :with_artists,
            :with_legacy_cover_image,
            delivery_type: "metadata_only",
            takedown: true,
            uuid: "63b0f7506f9c01333b5e22000a9f151d",
            store_name: "Zed",
            store_id: 95)
    end

    let(:sdc) { StoreDeliveryConfig.for("Zed") }

    describe "#valid?" do
      it "returns boolean" do
        expect(album.valid?).to be true
      end
    end

    describe "#validate_xml" do
      it "is true" do
        expect(album.validate_xml).to be true
      end
    end

    describe "#to_xml for metadata_only" do
      let(:fixture_xml) do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/zed_export_metadata_only.xml"))
      end

      xit "matches xml fixture" do
        album.set_data_from_config(sdc)
        album.artists.first.name = "Album Artist Name"
        album.tracks.first.artists.first.name = "Track Artist Name"
        allow(album).to receive(:xml_timestamp).and_return("2018-05-24T18:49:17+00:00")
        result = compare_xml(album.to_xml, fixture_xml)

        expect(result).to eq true
      end
    end

    describe "xml fields" do
      let(:result) do
        album.to_xml
      end

      it "use SubscriptionModel for CommercialModelType" do
        expect(result.scan('SubscriptionModel').size).to eq(2)
      end
    end
  end
end
