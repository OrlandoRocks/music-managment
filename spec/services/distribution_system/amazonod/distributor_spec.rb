require "rails_helper"

describe DistributionSystem::Amazonod::Distributor do
  let(:work_dir) { DELIVERY_CONFIG["work_dir"] }
  let(:album) do
    YAML.load_file("spec/distribution_fixtures/converted_albums/amazonod.yml")
  end
  let(:delivery_config) {StoreDeliveryConfig.for(album.store_name)}

  let(:config) do
    YAML.load_file(Rails.root.join("config", "delivery.yml"))[Rails.env]
  end

  let(:sftp_config) {
    {
      host:     delivery_config["hostname"],
      username: delivery_config["username"],
      password: delivery_config["password"],
      port:     delivery_config["port"]
    }
  }


  let(:shell) do
    DistributionSystem::Shell.new
  end

  let(:transcoder) do
    DistributionSystem::Transcoder.new({
      mp3_decoder:  "lame",
      alac_decoder: "alac",
      aac_decoder:  "faad",
      flac_encoder: "flac",
      flac_decoder: "flac",
      aac_encoder:  "faac",
      shell:        shell,
      tempdir:      DistributionSystem::TempDir.new
    })
  end

  let(:s3) do
    DistributionSystem::S3.new("asset_bucket")
  end

  let(:distribution) { FactoryBot.create(:distribution) }
  let(:bundle)       { DistributionSystem::Amazonod::Bundle.new(
                          work_dir,
                          transcoder,
                          s3,
                          album,
                          delivery_config
                        )
                      }
  let(:distributor)   {
    DistributionSystem::Amazonod::Distributor.new({
      bundle:       bundle
    })
  }

  describe "#distribute" do
    before(:each) do
      allow(bundle).to receive(:collect_files).and_return(true)
    end

    it "collects the bundle files" do
      expect(bundle).to receive(:collect_files)
      allow(distributor).to receive(:write_metadata)
      allow(distributor).to receive(:send_bundle)
      distributor.distribute(album, distribution)
    end

    it "writes the bundle metadata" do
      expect(bundle).to receive(:write_metadata)
      allow(distributor).to receive(:send_bundle)
      distributor.distribute(album, distribution)
    end

    it "sends to Amazonod" do
     dir_name = FileUtils.mkdir_p("#{work_dir}/AmazonOD/#{album.upc}")
     allow(bundle).to receive(:write_metadata)
     allow(distributor).to receive(:validate_bundle).and_return(true)
     zipname          = DistributionSystem::Amazonod::Bundle::ZIPPED_TRACKS
     bundle_id        = bundle.album.upc
     remote_bundle_id = bundle.album.physical_upc
     remote_dir       = "standard-deliveries/#{remote_bundle_id}"
     local_zipfile    = File.join(dir_name, zipname)
     remote_zipfile   = File.join(remote_dir, zipname)

     sftp = double("net_sftp").as_null_object
     interpreter = DistributionSystem::Sftp::Interpreter.new(sftp)
     allow(DistributionSystem::Sftp::Interpreter).to receive(:new).and_return(interpreter)
     expect(interpreter).to receive(:mkdir_p).with(remote_dir)

     artwork_file        = File.join(dir_name, "#{bundle_id}.jpg")
     artwork_1500_file   = File.join(dir_name, "#{bundle_id}_1500.jpg")
     remote_artwork_file = File.join(remote_dir, "AssetImage0.jpg")
     remote_detail_page_artwork_file = File.join(remote_dir, "DetailPageImage1.jpg")

     expect(interpreter).to receive(:upload).with(artwork_file, remote_artwork_file)
     expect(interpreter).to receive(:upload).with(artwork_1500_file, remote_detail_page_artwork_file)
     expect(interpreter).to receive(:upload).with(local_zipfile, remote_zipfile)

     metadata_filename        = File.join(dir_name, "AudioCDMetadata_#{bundle_id}.xml")
     remote_metadata_filename = File.join(remote_dir, "AudioCDMetadata_#{bundle_id}.xml")

     expect(interpreter).to receive(:upload).with(metadata_filename, remote_metadata_filename)
     distributor.distribute(album, distribution)
   end
  end
end
