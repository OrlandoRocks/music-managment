require "rails_helper"

describe DistributionSystem::Amazonod::Album do
  let(:fixture_album) { YAML.load_file("./spec/distribution_fixtures/converted_albums/amazonod.yml") }
  let(:fixture_xml) { Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/amazonod.xml"))}

  describe "#upc" do
    xit "is physical_upc if album has physical upc" do
      album = build(
        :amazonod_album,
        store_name: "Amazonod")
      expect(album.upc).to eq album.physical_upc
    end

    xit "is tunecore upc if album does not have physical upc" do
      album = build(
        :amazonod_album,
        physical_upc: nil,
        store_name: "Amazonod")
      expect(album.upc).not_to eq album.physical_upc
    end
  end

  it "includes all the required attributes" do
    album = build(:aod_album)
    album.artwork_file = double(asset: "spec/distribution_fixtures/assets/cover.jpg", bucket: "whut_bucket", aod_template: 4)
    expect(album.valid?).to be true
  end

  it "validates against the schema" do
    album = build(:aod_album)
    album.artwork_file = double(asset: "spec/distribution_fixtures/assets/cover.jpg", bucket: "whut_bucket", aod_template: 4)
    expect(album.validate_against_schema).to be true
  end

  xit "matches xml fixture for a regular delivery" do
    allow(DistributionSystem::CheckSumHelper).to receive(:checksum_file).and_return("1f40c13a7836bb57299e06f4a9ed5677")
    fixture_album.delivery_type = "metadata_only"
    # puts Diffy::Diff.new(fixture_xml.to_s, Nokogiri::XML(fixture_album.to_xml, nil, 'utf-8').to_s, :context => 2).diff
    expect(compare_xml(fixture_album.to_xml, fixture_xml)).to be true
  end
end
