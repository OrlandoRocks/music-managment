require "rails_helper"

RSpec.describe DistributionSystem::Believe::Album do
  let(:validate_path) {Rails.root.join("app", "services", "distribution_system", "schemas", "ddex341.xsd")}
  let(:output_path) {"spec/distribution_fixtures/metadata_output"}

  context "Believe Export" do
    let(:album) do
      build(:distribution_system_album,
            :single,
            :with_tracks,
            :with_artists,
            :with_legacy_cover_image,
            delivery_type: "metadata_only",
            takedown: true,
            uuid: "63b0f7506f9c01333b5e22000a9f151d",
            store_name: "BelieveExp",
            store_id: 73)
    end

    let(:sdc) { StoreDeliveryConfig.for("BelieveExp") }

    describe "#valid?" do
      it "returns boolean" do
        expect(album.valid?).to be true
      end
    end

    describe "#validate_xml" do
      it "is true" do
        expect(album.validate_xml).to be true
      end
    end

    describe "#to_xml for a metadata_only" do
      let(:fixture_xml) do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/believe_export_metadata_only.xml"))
      end

      it "matches xml fixture" do
        album.set_data_from_config(sdc)
        album.artists.first.name = "Album Artist Name"
        album.tracks.first.artists.first.name = "Track Artist Name"
        allow(album).to receive(:xml_timestamp).and_return("2018-05-24T18:49:17+00:00")
# puts Diffy::Diff.new(fixture_xml.to_s, Nokogiri::XML(album.to_xml, nil, 'utf-8').to_s, :context => 2).diff
        result = compare_xml(album.to_xml, fixture_xml)
        expect(result).to eq true
      end
    end
  end

  context "Believe Live" do
    let(:album) do
      build(:distribution_system_album,
            :with_tracks,
            :with_artists,
            :with_legacy_cover_image,
            uuid: "63b0f7506f9c01333b5e22000a9f151d",
            store_name: "BelieveLiv",
            store_id: 75)
    end
    describe "#valid?" do
      it "returns boolean" do
        expect(album.valid?).to be true
      end
    end

    describe "#to_xml for a metadata_only" do
      let(:album) do
        build(:distribution_system_album,
              :single,
              :with_tracks,
              :with_artists,
              :with_legacy_cover_image,
              delivery_type: "metadata_only",
              takedown: true,
              uuid: "63b0f7506f9c01333b5e22000a9f151d",
              store_name: "BelieveLiv",
              store_id: 75)
      end

      let(:sdc) { StoreDeliveryConfig.for("BelieveLiv") }

      let(:fixture_xml) do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/believe_live_metadata_only.xml"))
      end

      it "matches xml fixture" do

        allow(album).to receive(:xml_timestamp).and_return("2018-05-24T18:01:55+00:00")
        album.set_data_from_config(sdc)
        result = compare_xml(album.to_xml, fixture_xml)
        expect(result).to eq true
      end
    end

    describe "#validate_xml" do
      let(:album) do
        build(:distribution_system_album,
              :single,
              :with_tracks,
              :with_artists,
              :with_legacy_cover_image,
              delivery_type: "metadata_only",
              takedown: true,
              uuid: "63b0f7506f9c01333b5e22000a9f151d",
              store_name: "BelieveLiv",
              store_id: 75)
      end
      it "is true" do
        expect(album.validate_xml).to be true
      end
    end

    describe "#to_xml" do
      context "KKBox store" do
        let(:album) do
          build(:distribution_system_album,
                :with_tracks,
                :with_artists,
                :with_legacy_cover_image,
                uuid: "858c4a2099b60132804612890e781ad7",
                store_name: "KKBox",
                store_id: 53)
        end

        let(:fixture_xml) do
          Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/1340271_ddex.xml"))
        end

        it "matches xml fixture" do
          allow(album).to receive(:xml_timestamp).and_return("2016-01-01T12:00:00-05:00")
          result = compare_xml(album.to_xml, fixture_xml)
          expect(result).to eq true
        end

        it "is valid" do
          expect(album.validate_xml).to eq(true)
        end
      end

      context "Zune store" do
        let(:album) do
          build(:distribution_system_album,
                :with_tracks,
                :with_artists,
                :with_legacy_cover_image,
                uuid: "858c4a2099b60132804612890e781ad7",
                store_name: "Zune",
                store_id: 24)
        end

        let(:fixture_xml) do
          Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/1340271_ddex_xbox.xml"))
        end

        it "matches xml fixture" do
          allow(album).to receive(:xml_timestamp).and_return("2016-01-01T12:00:00-05:00")
          result = compare_xml(album.to_xml, fixture_xml)
          expect(result).to eq true
        end

        it "is valid" do
          expect(album.validate_xml).to be true
        end
      end
      context "Qsic store" do

        let(:album) do
          build(:distribution_system_album,
                :with_tracks,
                :with_artists,
                :with_legacy_cover_image,
                uuid: "858c4a2099b60132804612890e781ad7",
                store_name: "Qsic",
                store_id: 71)
        end

        let(:fixture_xml) do
          Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/1340271_ddex_qsic.xml"))
        end

        it "matches xml fixture" do
          allow(album).to receive(:xml_timestamp).and_return("2016-01-01T12:00:00-05:00")
          result = compare_xml(album.to_xml, fixture_xml)
          expect(result).to eq true
        end

        it "is valid" do
          expect(album.validate_xml).to eq(true)
        end

      end
      context "Sound Exchange store" do
        let(:album) do
          build(:distribution_system_album,
                :with_tracks,
                :with_artists,
                :with_legacy_cover_image,
                uuid: "858c4a2099b60132804612890e781ad7",
                store_name: "SndXchg",
                store_id: 80)
        end

        let(:fixture_xml) do
          Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/1340271_ddex_sound_exchange.xml"))
        end

        it "matches xml fixture" do
          allow(album).to receive(:xml_timestamp).and_return("2016-01-01T12:00:00-05:00")
          result = compare_xml(album.to_xml, fixture_xml)
          expect(result).to eq true
        end

        it "is valid" do
          expect(album.validate_xml).to be true
        end
      end


      context "Musicload" do
        let(:album) do
          build(:distribution_system_album,
                :single,
                :with_tracks,
                :with_artists,
                :with_legacy_cover_image,
                uuid: "ca071c90d9e70133fc3b0ebc481438eb",
                store_name: "Musicload",
                store_id: 76
               )
        end

        let(:fixture_xml) do
          Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/139023_musicload.xml"))
        end

        it "matches xml fixture" do
          allow(album).to receive(:xml_timestamp).and_return("2016-01-01T12:00:00-05:00")
          result = compare_xml(album.to_xml, fixture_xml)
          expect(result).to eq true
        end

        it "is valid" do
          expect(album.validate_xml).to eq(true)
        end
      end

      context "RhapsodyRH" do
        let(:album) do
          YAML.load_file("spec/distribution_fixtures/converted_albums/rhapsody.yml")
        end

        let(:fixture_xml) do
          Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/873605_rhapsody.xml"))
        end

        before(:each) do
          allow(DistributionSystem::CheckSumHelper).to receive(:checksum_file).and_return("bc52fedc767e5efcaf68164b52877025")
          album.delivery_type = 'metadata_only' # the fixture was a metadata_only

          album.tracks.each do |track|
            allow(track).to receive(:iso8601).and_return("PT216S")
            allow(track).to receive(:audio_file).and_return("859709363271_01_1.flac")
          end

          allow(album).to receive(:get_uuid).and_return("044ca83092d701330c2b2cbc32b1fa2d")

          album.set_data_from_config(StoreDeliveryConfig.by_store_name(album.store_name).first)
          allow(album).to receive(:xml_timestamp).and_return("2016-01-01T12:00:00-05:00")
          @fixture_xml = fixture_xml.to_s
          @album_xml   = album.to_xml.to_s
        end

        it "is valid 341 DDEX" do
          expect(album.validate_xml).to eq true
        end
      end

      context "UMA-BOOM" do
        let(:album) do
          build(:distribution_system_album,
                :with_tracks,
                :with_artists,
                :with_legacy_cover_image,
                uuid: "ca071c90d9e70133fc3b0ebc481438eb",
                store_name: "UMA",
                image_size: 1500,
                party_id: 'PADPIDA20150803045',
                party_full_name: 'United Media Agency',
                ddex_version: '382'
          )
        end

        let(:fixture_xml) do
          Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/uma.xml"))
        end

        it "matches xml fixture" do
          allow(album).to receive(:xml_timestamp).and_return("2016-01-01T12:00:00-05:00")
          result = compare_xml(album.to_xml, fixture_xml)
          expect(result).to eq true
        end

        it "is valid" do
          expect(album.validate_xml).to eq(true)
        end
      end

      context "TDC/Nuuday" do
        let(:album) do
          build(:distribution_system_album,
                :with_tracks,
                :with_artists,
                :with_legacy_cover_image,
                uuid: "ca071c90d9e70133fc3b0ebc481438eb",
                store_name: "Juke"
          )
        end

        let(:fixture_xml) do
          Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/tdc.xml"))
        end

        it "matches xml fixture" do
          allow(album).to receive(:xml_timestamp).and_return("2016-01-01T12:00:00-05:00")
          album.image_size = 1600
          album.party_id = 'PADPIDA2011090104S'
          album.party_full_name = 'TDC'
          album.ddex_version = '382'
          result = compare_xml(album.to_xml, fixture_xml)
          expect(result).to eq true
        end

        it "is valid" do
          expect(album.validate_xml).to eq(true)
        end
      end

      context "NetEase" do
        let(:album) do
          build(:distribution_system_album,
                :with_tracks,
                :with_artists,
                :with_legacy_cover_image,
                uuid: "ca071c90d9e70133fc3b0ebc481438eb",
                store_name: "NetEase",
                image_size: 1500,
                party_id: 'PADPIDA2015122102I',
                party_full_name: 'NetEase(Hangzhou) Network Co.,Ltd.',
                ddex_version: '382'
          )
        end

        let(:fixture_xml) do
          Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/netease.xml"))
        end

        it "matches xml fixture" do
          allow(album).to receive(:xml_timestamp).and_return("2016-01-01T12:00:00-05:00")
          result = compare_xml(album.to_xml, fixture_xml)
          expect(result).to eq true
        end

        it "is valid" do
          expect(album.validate_xml).to eq(true)
        end
      end

      context "JOOX" do
        let(:album) do
          build(:distribution_system_album,
                :with_artists,
                :with_legacy_cover_image,
                uuid: "ca071c90d9e70133fc3b0ebc481438eb",
                store_name: "JOOX",
                image_size: 1600,
                ddex_version: '382',
                language_code: 'en')
        end
        let(:track1) do
          build(:distribution_system_track,
                :with_artists,
                number: 1,
                artist_count: 1,
                album: album,
                language_code: 'fr')
        end
        let(:track2) do
          build(:distribution_system_track,
                :with_artists,
                number: 2,
                artist_count: 1,
                album: album,
                language_code: album.language_code)
        end
        let(:fixture_xml) do
          Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/joox.xml"))
        end

        before do
          album.tracks = [track1, track2]
        end

        it "matches xml fixture" do
          allow(album).to receive(:xml_timestamp).and_return("2016-01-01T12:00:00-05:00")
          result = compare_xml(album.to_xml, fixture_xml)
          expect(result).to eq true
        end

        it "is valid" do
          expect(album.validate_xml).to eq(true)
        end
      end

      context "Gaana" do
        let(:album) do
          build(:distribution_system_album,
                :with_artists,
                :with_legacy_cover_image,
                uuid: "ca071c90d9e70133fc3b0ebc481438eb",
                store_name: "Gaana",
                image_size: 1600,
                party_id: "PADPIDA2012081610S",
                party_full_name: 'Gaana',
                ddex_version: '382',
                language_code: 'en')
        end
        let(:track1) do
          build(:distribution_system_track,
                :with_artists,
                number: 1,
                artist_count: 1,
                album: album,
                language_code: 'fr')
        end
        let(:track2) do
          build(:distribution_system_track,
                :with_artists,
                number: 2,
                artist_count: 1,
                album: album,
                language_code: album.language_code)
        end
        let(:fixture_xml) do
          Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/gaana.xml"))
        end

        before do
          album.tracks = [track1, track2]
        end

        it "is valid" do
          expect(album.validate_xml).to eq(true)
        end
      end

      context "TikTok Bytedance" do
        let(:album) do
          build(:distribution_system_album,
                :with_artists,
                :with_legacy_cover_image,
                uuid: "ca071c90d9e70133fc3b0ebc481438eb",
                store_name: "Bytedance",
                image_size: 1600,
                party_id: 'PADPIDA2018082301A,PADPIDA2018111407I',
                party_full_name: 'TikTok / Bytedance,Project M / Bytedance',
                ddex_version: '382',
                display_track_language: true)
        end
        let(:track1) do
          build(:distribution_system_track,
                :with_artists,
                number: 1,
                artist_count: 1,
                album: album,
                song_start_time: 22)
        end
        let(:track2) do
          build(:distribution_system_track,
                :with_artists,
                number: 2,
                artist_count: 1,
                album: album)
        end
        let(:fixture_xml) do
          Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/tiktok.xml"))
        end

        before(:each) do
          album.tracks = [track1, track2]
        end

        it "matches xml fixture" do
          allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
          allow(album).to receive(:xml_timestamp).and_return("2016-01-01T12:00:00-05:00")
          result = compare_xml(album.to_xml, fixture_xml)
          expect(result).to eq true
        end

        it "is valid" do
          expect(album.validate_xml).to eq(true)
        end

        it "has StartPoint node only for track with song_start_time" do
          expect(album.to_xml.scan('StartPoint').size).to eq(2)
        end
      end

      context "iHeartRadio" do
        let(:album) do
          build(:distribution_system_album,
                :with_tracks,
                :with_artists,
                :with_legacy_cover_image,
                uuid: "ca071c90d9e70133fc3b0ebc481438eb",
                store_name: "Thumbplay"
          )
        end

        let(:fixture_xml) do
          Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/iheartradio.xml"))
        end

        it "matches xml fixture" do
          allow(album).to receive(:xml_timestamp).and_return("2016-01-01T12:00:00-05:00")
          album.image_size = 1400
          album.party_id = 'PADPIDA2010121501F'
          album.party_full_name = 'iheartradio'
          album.ddex_version = '382'
          result = compare_xml(album.to_xml, fixture_xml)
          expect(result).to eq true
        end

        it "is valid" do
          expect(album.validate_xml).to eq(true)
        end
      end

      context "album is a ringtone" do
        let(:album) do
          YAML.load_file("spec/distribution_fixtures/converted_albums/ringtone.yml")
        end
        it "is valid" do
          album.ddex_version = "37"
          expect(album.validate_xml).to be true
        end
      end
    end

    describe "#primary_and_featured_artists" do
      it "returns the albums primary and featured artists" do
        album.artists << build(:distribution_system_artist, :is_featured)
        expect(album.primary_and_featured_artists).to match album.artists
      end
    end
  end
end

describe DistributionSystem::Believe::Album do
  let(:config) { StoreDeliveryConfig.where(store_id: 75).first }
  let(:composer_lyricist) do
     DistributionSystem::Believe::ArtistRole.new(double({role_type: "composer_lyricist", resource_contributor_role: false, indirect_contributor_role: true, user_defined: false}))
  end

  let(:composer) do
    DistributionSystem::Believe::ArtistRole.new(double({role_type: "composer", resource_contributor_role: false, indirect_contributor_role: true, user_defined: false}))
  end

  let(:songwriter) do
    DistributionSystem::Believe::ArtistRole.new(double({role_type: "songwriter", resource_contributor_role: false, indirect_contributor_role: true, user_defined: true}))
  end

  let(:remixer) do
    DistributionSystem::Believe::ArtistRole.new(double({role_type: "remixer", resource_contributor_role: true, indirect_contributor_role: false, user_defined: true}))
  end

  let(:track) do
    build(:distribution_system_track,
      album: album,
      isrc: "TCAAA1800037",
      title: "First Song",
      song_id: 37)
  end

  let(:sophie)  { build(:distribution_system_artist, name: "Sophie") }
  let(:sophie_for_track) { build(:distribution_system_artist, name: "Sophie", roles: [])}
  let(:dana)    { build(:distribution_system_artist, name: "Dana")}
  let(:frank)   { build(:distribution_system_artist, :is_featured, name: "Frank") }
  let(:charlie) { build(:distribution_system_artist, name: "Charlie", roles: ["contributor"])}

  before(:each) do
    sophie_for_track.artist_roles  = [composer_lyricist, songwriter]
    frank.artist_roles   = [remixer]
    dana.artist_roles    = [remixer, composer]
    charlie.artist_roles = [composer]
    album.tracks         = [track]
    album.artists        = [sophie]
    track.artists        = [sophie_for_track, dana, frank, charlie]
    allow(album).to receive(:xml_timestamp).and_return('2018-06-12T18:20:56+00:00')
    album.set_data_from_config(config)
  end

  context "multiple artist roles" do
    let(:fixture_xml) { Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/believe_multiple_artist_roles.xml")) }
    let(:album) do
      build(:distribution_system_album,
        delivery_type: "metadata_only",
        uuid: "45a98e60509b013645e70faa327fe128",
        upc: "859700000366",
        store_name: "BelieveLiv",
        countries: ["AF", "AX"],
        copyright_name: "Sophie",
        release_date: "2018-06-12",
        genre: 4,
        customer_id: 367972,
        store_id: 75,
        title: "My Great Album")
    end

    it "is valid" do
      expect(album.validate_xml).to be true
    end

    it "matches the fixture" do
      result = compare_xml(album.to_xml, fixture_xml)
      expect(result).to eq true
    end
  end

  context "single with multiple artist roles" do
    let(:album) do
      build(:distribution_system_album,
        :single,
        delivery_type: "metadata_only",
        uuid: "45a98e60509b013645e70faa327fe128",
        upc: "859700000366",
        store_name: "BelieveLiv",
        countries: ["AF", "AX"],
        copyright_name: "Sophie",
        release_date: "2018-06-12",
        genre: 4,
        customer_id: 367972,
        store_id: 75,
        title: "My Great Album")
    end

    it "is valid" do
      expect(album.validate_xml).to be true
    end
  end

  context "album is various and has multiple artist roles" do
    let(:various_artist) { build(:distribution_system_artist, name: "Various Artists") }
    let(:album) do
      build(:distribution_system_album,
        is_various: true,
        delivery_type: "metadata_only",
        uuid: "45a98e60509b013645e70faa327fe128",
        upc: "859700000366",
        store_name: "BelieveLiv",
        countries: ["AF", "AX"],
        copyright_name: "Sophie",
        release_date: "2018-06-12",
        genre: 4,
        customer_id: 367972,
        store_id: 75,
        title: "My Great Album")
    end

    before(:each) do
      album.artists = [various_artist]
    end

    it "is valid" do
      expect(album.validate_xml).to be true
    end
  end

  context "album artist has feat. in the name" do
    let(:album) do
      build(:distribution_system_album,
        delivery_type: "metadata_only",
        uuid: "45a98e60509b013645e70faa327fe128",
        upc: "859700000366",
        store_name: "BelieveLiv",
        countries: ["AF", "AX"],
        copyright_name: "Sophie",
        release_date: "2018-06-12",
        genre: 4,
        customer_id: 367972,
        store_id: 75,
        title: "My Great Album")
    end

    before(:each) do
      sophie.name = "Sophie (feat. Dana)"
    end

    it "should use primary artist name as display name" do
      expect(album.artist_display_name).to eq "Sophie (feat. Dana)"
    end

    it "is valid" do
      expect(album.validate_xml).to be true
    end
  end
end
