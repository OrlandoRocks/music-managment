require "rails_helper"

describe DistributionSystem::Believe::Bundle do
  let(:album) { build(:distribution_system_album, :with_tracks, :with_booklet, :with_artwork, store_name: "7Digital", store_id: 300) }
  let(:bundle) { build(:believe_bundle, skip_batching: true, album: album) }

  context "Non-Believe-Live albums" do
    describe "#resources_for" do
      it "only returns xml and complete resources when metadata_only" do
        bundle.create_bundle
        expect(bundle.resources_for("metadata_only").first[:local_file]).to match /.xml$/
        expect(bundle.resources_for("metadata_only").count).to eq 1
      end

      it "returns all tracks, xml and image when full_delivery" do
        bundle.create_bundle
        expect(bundle.resources_for("full_delivery").count).to eq 2 + album.tracks.count
      end

      it "does not return the booklet" do
        bundle.create_bundle
        resources = bundle.resources_for("full_delivery")
        expect(resources.select{|i| i[:type]==:booklet}).to be_empty
      end
    end

    it "returns all tracks, xml and image when full_delivery" do
      bundle.create_bundle
      expect(bundle.resources_for("full_delivery").count).to eq 2 + album.tracks.count
    end

    describe "#resources" do
      it "is an array" do
        bundle.create_bundle
        expect(bundle.resources).to be_a Array
      end

      it "has a remote_file name as album.tunecore_id_upc_file_name_suffix.extension" do
        bundle.create_bundle
        expected_file_name = File.join(
          bundle.remote_dir,
          Date.today.strftime("%Y%m%d"),
          "#{bundle.instance_variable_get(:@timestamp)}_#{album.tunecore_id}_#{album.upc}",
          "resources",
          "#{album.upc}_01_#{album.tracks.last.number}.#{bundle.album.audio_codec_type}"
        )
        expect(bundle.resources.first[:remote_file]).to eq expected_file_name
      end

      it "has a remote_file name as album.upc" do
        album  = build(:distribution_system_album, :with_tracks, :with_booklet, store_name: "Zune", store_id: 24) # not timestamped
        bundle = build(:believe_bundle, skip_batching: true, batch_id: nil, album: album)
        bundle.use_resource_dir = true
        bundle.create_bundle
        expected_file_name = File.join(
          bundle.remote_dir,
          album.upc.to_s,
          "resources",
          "#{album.upc}_01_#{album.tracks.last.number}.#{bundle.album.audio_codec_type}"
        )
        expect(bundle.resources.first[:remote_file]).to eq expected_file_name
      end

      it "has a local_file name as upc_01_track_number.extension" do
        bundle.create_bundle
        expected_file_name = File.join(
          bundle.dir.path,
          "#{album.upc}_01_#{album.tracks.last.number}.#{bundle.album.audio_codec_type}"
        )
        expect(bundle.resources.first[:local_file]).to eq expected_file_name
      end
    end

    describe "#complete_file_name" do
      it "is delivery.complete if not batching" do
        album  = build(:distribution_system_album, :with_tracks, store_name: "Zune", store_id: 24)
        bundle = build(:believe_bundle, skip_batching: true, batch_id: nil, album: album)
        expect(bundle.complete_file_name).to eq "delivery.complete"
      end

      it "is BatchComplete_batch_id.xml if batching" do
        album  = build(:distribution_system_album, :with_tracks, store_name: "Saavn", store_id: 68)
        bundle = build(:believe_bundle, skip_batching: false, album: album)
        expect(bundle.complete_file_name).to eq "BatchComplete_#{bundle.batch_id}.xml"
      end
    end
  end

  context "BelieveLiv albums" do
    let(:album) { build(:distribution_system_album, :with_tracks, :with_booklet, :with_artwork, store_name: "BelieveLiv", store_id: 75) }
    let(:bundle) { build(:believe_bundle, skip_batching: true, album: album) }

    it "#resources only include xml, even for a full delivery" do
      bundle.create_bundle
      full_deliv = bundle.resources_for("full_delivery").count
      meta_only  = bundle.resources_for("metadata_only").count
      expect(full_deliv).to eq meta_only
    end

    it "has a remote_file name as album.tunecore_id_upc_file_name_suffix.extension" do
      bundle.create_bundle
      expected_file_name = File.join(
        bundle.remote_dir,
        Date.today.strftime("%Y%m%d"),
        "#{bundle.instance_variable_get(:@timestamp)}_#{album.tunecore_id}_#{album.upc}",
        "#{album.upc}.xml"
      )
      expect(bundle.resources.count).to eq 1
      expect(bundle.resources.first[:remote_file]).to eq expected_file_name
    end
  end

  describe "#dirname, #path_id, #dir" do
    it "should be album upc if track_only is false" do
      transcoder = double
      song = double(isrc: "0987654321")
      album = double(upc: "1234567890", tracks: [song], store_name: "Amazon", track_only: false)
      allow(album).to receive(:set_data_from_config).and_return(true)
      config = {}
      s3 = double

      bundle = DistributionSystem::Believe::Bundle.new("./work", s3, transcoder, album, config)

      expect(bundle.dirname).to eq "./work/#{album.store_name}/#{album.upc}"
      expect(bundle.dir.path).to eq "./work/#{album.store_name}/#{album.upc}"
      expect(bundle.path_id).to eq album.upc
    end

    it "should be song isrc nested in the upc directory if track_only is true" do
      transcoder = double
      song = double(isrc: "0987654321")
      album = double(upc: "1234567890", tracks: [song], store_name: "Amazon", track_only: true)
      allow(album).to receive(:set_data_from_config).and_return(true)
      config = {}
      s3 = double

      bundle = DistributionSystem::Believe::Bundle.new("./work", s3, transcoder, album, config)

      expect(bundle.dirname).to eq "./work/#{album.store_name}/#{album.upc}/#{song.isrc}"
      expect(bundle.dir.path).to eq "./work/#{album.store_name}/#{album.upc}/#{song.isrc}"
      expect(bundle.path_id).to eq song.isrc
    end
  end

  describe "#write_metadata" do
    it "generates a hashsum from the completed metadata file if the store requires a manifest" do
      config = StoreDeliveryConfig.readonly(false).for("Spotify")
      config.update(use_manifest: true)
      album = build(:spotify_distribution_album, :with_artists, :with_tracks, :with_booklet, :with_artwork, store_name: "Spotify", store_id: 300)
      bundle = build(:spotify_bundle, album: album)

      bundle.create_bundle
      bundle.write_metadata
      expect(bundle.manifest_params[:xml_hashsum]).not_to be nil
    end
  end

  describe "#write_complete_file" do
    it "obtains the manifest file for stores that require it" do
      manifest_bundle = build(:believe_bundle, skip_batching: true, use_manifest: true, album: album)
      d_batch         = double(:delivery_batch)
      manifest        = "dummy string"
      allow(DistributionSystem::DeliveryBatchManifest).to receive(:write_manifest).and_return(manifest)

      expect(DistributionSystem::DeliveryBatchManifest).to receive(:write_manifest).with(d_batch)

      manifest_bundle.write_complete_file(d_batch)
    end

    it "writes the manifest file to the local filepath" do
      manifest_bundle = build(:believe_bundle, skip_batching: true, use_manifest: true, album: album)
      manifest_filepath = File.join(bundle.dir.path, bundle.complete_file_name)
      d_batch         = double(:delivery_batch)
      manifest        = "dummy string"
      allow(DistributionSystem::DeliveryBatchManifest).to receive(:write_manifest).and_return(manifest)

      manifest_bundle.write_complete_file(d_batch)

      expect(File.exists?(manifest_filepath)).to be true
    end
  end

  describe '#handle_transcoding' do
    context 'When track has flac asset' do
      it "copies the audio file to the final destination prior to upload" do
        track = bundle.album.tracks.first
        track.s3_key = "S3_key_to_track.flac"
        original_file = File.join(bundle.dir.path, track.temp_filename)
        expect(bundle).to receive(:move_file_to_destination)
        bundle.handle_transcoding(track, original_file)
      end
    end

    context 'When track does not have a flac asset' do
      it "transcodes the audio file before delivery" do
        track = bundle.album.tracks.first
        track.s3_key = "not_a_flac.m4a"
        track.audio_file = "not_a_flac.m4a"
        original_file = File.join(bundle.dir.path, track.temp_filename)
        expect(bundle).to receive(:transcode_track)
        bundle.handle_transcoding(track, original_file)
      end
    end
  end
end
