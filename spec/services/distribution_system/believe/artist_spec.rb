require "rails_helper"

describe DistributionSystem::Believe::Artist do
  describe "#primary_or_featured_role_name" do
    context "when a primary artist" do
      let(:artist) { build(:distribution_system_artist) }

      it "returns MainArtist" do
        expect(artist.primary_or_featured_role_name).to eq "MainArtist"
      end
    end

    context "when a featured artist" do
      let(:artist) { build(:distribution_system_artist, :is_featured) }

      it "returns FeaturedArtist" do
        expect(artist.primary_or_featured_role_name).to eq "FeaturedArtist"
      end
    end
  end
end
