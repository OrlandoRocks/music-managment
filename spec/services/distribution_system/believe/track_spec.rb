 require "rails_helper"

 describe DistributionSystem::Believe::Track do
   let(:album) do
     build(:distribution_system_album,
           :single,
           :with_legacy_cover_image,
           delivery_type: "metadata_only",
           takedown: true,
           uuid: "63b0f7506f9c01333b5e22000a9f151d",
           store_name: "BelieveExp",
           store_id: 73)
   end

   context "when a track does not have a primary artist" do
     let(:track)           { build(:distribution_system_track, album: album) }
     let(:primary_artist)  { build(:distribution_system_artist) }
     let(:featured_artist) { build(:distribution_system_artist, :is_featured) }

     before(:each) do
       album.artists = [primary_artist]
       track.artists = [featured_artist]
     end

     describe "#primary_artist" do
       it "returns the albums primary artist" do
         expect(track.primary_artist).to eq album.primary_artist
       end
     end

     describe "#primary_and_featured_artists" do
       it "returns the albums primary artist, albums featured artist and the tracks featured artist" do
         expect(track.primary_and_featured_artists).to match [primary_artist, featured_artist]
       end
     end
   end

   context "when a track has primary artist" do
     let(:track)            { build(:distribution_system_track, album: album) }
     let(:primary_artist)   { build(:distribution_system_artist) }
     let(:featured_artist)  { build(:distribution_system_artist, :is_featured) }
     let(:featured_artist2) { build(:distribution_system_artist, :is_featured, name: 'Featured 2') }
     let(:primary_artist2)  { build(:distribution_system_artist) }

     before(:each) do
       track.artists = [primary_artist, featured_artist]
       album.artists = [primary_artist2, featured_artist2]
     end

     describe "#primary_artist" do
       it "returns the tracks primary artist" do
         expect(track.primary_artist).to eq primary_artist
       end
     end

     describe "#primary_and_featured_artists" do
       it "returns the track's primary artist and the track's featured artists and the album's featured artists" do
         expect(track.primary_and_featured_artists).to match([primary_artist, featured_artist2, featured_artist])
       end

       describe "#contributors_for" do
         before(:each) do
           track.artists << build(:distribution_system_artist, :with_artist_roles)
         end
         it "returns the contributors for the given resource type" do
           expect(track.contributors_for("resource")).to eq([{name: "Artist Name", role: "Performer", user_defined: true}])
           expect(track.contributors_for("indirect")).to eq([{name: "Artist Name", role: "Songwriter", user_defined: true}])
         end
       end
     end
   end

   context "when a track album has various artists" do
     let(:track)            { build(:distribution_system_track, album: album) }
     let(:primary_artist)   { build(:distribution_system_artist) }
     let(:featured_artist)  { build(:distribution_system_artist, :is_featured) }
     let(:primary_artist2)  { build(:distribution_system_artist, name: "Various Artists") }
     let(:featured_artist2) { build(:distribution_system_artist, :is_featured) }

     before(:each) do
       track.artists = [primary_artist, featured_artist]
       album.artists = [primary_artist2, featured_artist2]
     end

     describe "primary artists" do
       it 'should not add the various artists to the track' do
         expect(track.primary_and_featured_artists).to_not include(primary_artist2)
       end
     end
   end
 end
