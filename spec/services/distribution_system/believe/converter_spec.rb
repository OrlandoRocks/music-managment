require "rails_helper"

describe DistributionSystem::Believe::Converter do
  describe ".convert" do
    context "album has creatives" do
      before(:each) do
        store     = Store.find(store_id)
        allow(store).to receive(:delivery_service).and_return("tunecore")
        @album    = create(:album, :with_artwork, primary_genre: Genre.find(1))
        @artist2   = Artist.last
        song      = create(:song, :with_upload, album_id: @album.id)
        creative2 = create(:creative, creativeable_type: "Song", creativeable_id: song.id, artist_id: @artist2.id, role: "with")
        CreativeSongRole.create(creative_id: creative2.id, song_role_id: 80, song_id: song.id)
        petri_bundle = create(:petri_bundle, album_id: @album.id)
        distribution = create(:distribution, :with_salepoint, petri_bundle_id: petri_bundle.id, converter_class: "MusicStores::#{store_name}::Converter", state: "new", delivery_type: "metadata_only")
        salepoint    = distribution.salepoints.first
        allow(salepoint).to receive(:store).and_return(store)
        allow(distribution).to receive(:store).and_return(store)
        @converted_album = Delivery::AlbumConverter.convert(distribution, "metadata_only")
      end

      context "belive store" do
        let(:store_id) { 75 }
        let(:store_name) { "Believe" }
        it "generates an album with the correct artist roles" do
          artist = @converted_album.artists.first
          expect(artist.roles).to include("primary_artist")
        end

        it "generates album tracks with the correct artist roles" do
          artist = @converted_album.tracks.first.artists.first
          expect(artist.roles).to include("with", "composer")
        end

        it "track knows who primary artist is" do
          track = @converted_album.tracks.first
          expect(track.primary_artist.name).to eq(@album.artist_name)
        end

        it "artist knows if it is primary" do
          artist = @converted_album.artists.first
          expect(artist.is_primary?).to eq(true)
        end

        it "generates artists with artist_roles" do
          track  = @converted_album.tracks.first
          artist = track.artists.first
          expect(artist.artist_roles.length).to eq(1)
          expect(artist.artist_roles.first.role_type.downcase).to eq("composer")
          expect(track.contributors_for("indirect")).to eq([{name: @artist2.name, role: "Composer", user_defined: false}])
        end
      end

      context "deezer store" do
        let(:store_id) { 32 }
        let(:store_name) { "Deezer" }
        it "generates an album with the correct artist roles" do
          artist = @converted_album.artists.first
          expect(artist.roles).to include("primary_artist")
        end

        it "generates album tracks with the correct artist roles" do
          artist = @converted_album.tracks.first.artists.first
          expect(artist.roles).to include("with", "composer")
        end

        it "track knows who primary artist is" do
          track = @converted_album.tracks.first
          expect(track.primary_artist.name).to eq(@album.artist_name)
        end

        it "artist knows if it is primary" do
          artist = @converted_album.artists.first
          expect(artist.is_primary?).to eq(true)
        end
      end

      context "Kkbox store" do
        let(:store_id) { 53 }
        let(:store_name) { "Kkbox" }
        it "generates an album with the correct artist roles" do
          artist = @converted_album.artists.first
          expect(artist.roles).to include("primary_artist")
        end

        it "generates album tracks with the correct artist roles" do
          artist = @converted_album.tracks.first.artists.first
          expect(artist.roles).to include("with", "composer")
        end

        it "track knows who primary artist is" do
          track = @converted_album.tracks.first
          expect(track.primary_artist.name).to eq(@album.artist_name)
        end

        it "artist knows if it is primary" do
          artist = @converted_album.artists.first
          expect(artist.is_primary?).to eq(true)
        end
      end

      context "Akazoo store" do
        let(:store_id) { 51 }
        let(:store_name) { "Akazoo" }
        it "generates an album with the correct artist roles" do
          artist = @converted_album.artists.first
          expect(artist.roles).to include("primary_artist")
        end

        it "generates album tracks with the correct artist roles" do
          artist = @converted_album.tracks.first.artists.first
          expect(artist.roles).to include("with", "composer")
        end

        it "track knows who primary artist is" do
          track = @converted_album.tracks.first
          expect(track.primary_artist.name).to eq(@album.artist_name)
        end

        it "artist knows if it is primary" do
          artist = @converted_album.artists.first
          expect(artist.is_primary?).to eq(true)
        end
      end

      context "Neurotic Media store" do
        let(:store_id) { 56 }
        let(:store_name) { "NeuroticMedia" }
        it "generates an album with the correct artist roles" do
          artist = @converted_album.artists.first
          expect(artist.roles).to include("primary_artist")
        end

        it "generates album tracks with the correct artist roles" do
          artist = @converted_album.tracks.first.artists.first
          expect(artist.roles).to include("with", "composer")
        end

        it "track knows who primary artist is" do
          track = @converted_album.tracks.first
          expect(track.primary_artist.name).to eq(@album.artist_name)
        end

        it "artist knows if it is primary" do
          artist = @converted_album.artists.first
          expect(artist.is_primary?).to eq(true)
        end
      end

      context "Slacker store" do
        let(:store_id) { 47 }
        let(:store_name) { "Slacker" }
        it "generates an album with the correct artist roles" do
          artist = @converted_album.artists.first
          expect(artist.roles).to include("primary_artist")
        end

        it "generates album tracks with the correct artist roles" do
          artist = @converted_album.tracks.first.artists.first
          expect(artist.roles).to include("with", "composer")
        end

        it "track knows who primary artist is" do
          track = @converted_album.tracks.first
          expect(track.primary_artist.name).to eq(@album.artist_name)
        end

        it "artist knows if it is primary" do
          artist = @converted_album.artists.first
          expect(artist.is_primary?).to eq(true)
        end
      end

      context "Anghami store" do
        let(:store_id) { 52 }
        let(:store_name) { "Anghami" }
        it "generates an album with the correct artist roles" do
          artist = @converted_album.artists.first
          expect(artist.roles).to include("primary_artist")
        end

        it "generates album tracks with the correct artist roles" do
          artist = @converted_album.tracks.first.artists.first
          expect(artist.roles).to include("with", "composer")
        end

        it "track knows who primary artist is" do
          track = @converted_album.tracks.first
          expect(track.primary_artist.name).to eq(@album.artist_name)
        end

        it "artist knows if it is primary" do
          artist = @converted_album.artists.first
          expect(artist.is_primary?).to eq(true)
        end
      end

      context "Rhapsody store" do
        let(:store_id) { 7 }
        let(:store_name) { "Rhapsody" }
        it "generates an album with the correct artist roles" do
          artist = @converted_album.artists.first
          expect(artist.roles).to include("primary_artist")
        end

        it "generates album tracks with the correct artist roles" do
          artist = @converted_album.tracks.first.artists.first
          expect(artist.roles).to include("with", "composer")
        end

        it "track knows who primary artist is" do
          track = @converted_album.tracks.first
          expect(track.primary_artist.name).to eq(@album.artist_name)
        end

        it "artist knows if it is primary" do
          artist = @converted_album.artists.first
          expect(artist.is_primary?).to eq(true)
        end
      end

      context "Spinlet store" do
        let(:store_id) { 55 }
        let(:store_name) { "Spinlet" }
        it "generates an album with the correct artist roles" do
          artist = @converted_album.artists.first
          expect(artist.roles).to include("primary_artist")
        end

        it "generates album tracks with the correct artist roles" do
          artist = @converted_album.tracks.first.artists.first
          expect(artist.roles).to include("with", "composer")
        end

        it "track knows who primary artist is" do
          track = @converted_album.tracks.first
          expect(track.primary_artist.name).to eq(@album.artist_name)
        end

        it "artist knows if it is primary" do
          artist = @converted_album.artists.first
          expect(artist.is_primary?).to eq(true)
        end
      end

      context "Youtube Music store" do
        let(:store_id)   { 45 }
        let(:store_name) { "YoutubeMusic" }

        it "generates an album with the correct artist roles" do
          artist = @converted_album.artists.first
          expect(artist.roles).to include("primary_artist")
        end

        it "generates album tracks with the correct artist roles" do
          artist = @converted_album.tracks.first.artists.first
          expect(artist.roles).to include("with", "composer")
        end

        it "track knows who primary artist is" do
          track = @converted_album.tracks.first
          expect(track.primary_artist.name).to eq(@album.artist_name)
        end

        it "artist knows if it is primary" do
          artist = @converted_album.artists.first
          expect(artist.is_primary?).to eq(true)
        end
      end

      context "Spotify store" do
        let(:store_id) { 26 }
        let(:store_name) { "Spotify"}
        it "sets the xml format to :believe" do
          store     = Store.find(26)
          @album    = create(:album, :with_artwork, primary_genre: Genre.find(1))
          @artist2   = Artist.last
          song      = create(:song, :with_upload, album_id: @album.id)
          creative2 = create(:creative, creativeable_type: "Song", creativeable_id: song.id, artist_id: @artist2.id, role: "with")
          CreativeSongRole.create(creative_id: creative2.id, song_role_id: 80, song_id: song.id)
          petri_bundle = create(:petri_bundle, album_id: @album.id)
          distribution = create(:distribution, :with_salepoint, petri_bundle_id: petri_bundle.id, converter_class: "MusicStores::Spotify::Converter", state: "new", delivery_type: "metadata_only")
          salepoint    = distribution.salepoints.first
          allow(salepoint).to receive(:store).and_return(store)
          allow(distribution).to receive(:store).and_return(store)
          @converted_album = Delivery::AlbumConverter.convert(distribution, "metadata_only")

          expect(@converted_album.formatter.send(:template_format)).to eq("believe")

          pathname = @converted_album.formatter.validation_path.split.pop.to_s
          expect(pathname.include?("ddex")).to be true
        end
      end
    end

    context "album is various artists" do
      context "when the converter class is MusicStores" do
        before(:each) do
          store     = Store.find(store_id)
          allow(store).to receive(:delivery_service).and_return("tunecore")
          album = create(:album, :with_artwork, primary_genre: Genre.find(1), is_various: true)
          song  = create(:song, :with_upload, album_id: album.id)
          petri_bundle = create(:petri_bundle, album_id: album.id)
          distribution = create(:distribution, :with_salepoint, petri_bundle_id: petri_bundle.id, converter_class: "MusicStores::#{store_name}::Converter", state: "new", delivery_type: "metadata_only")
          salepoint = distribution.salepoints.first
          allow(salepoint).to receive(:store).and_return(store)
          allow(distribution).to receive(:store).and_return(store)
          @converted_album = Delivery::AlbumConverter.convert(distribution, "metadata_only")
        end

        context "believe" do
          let(:store_id) { 75 }
          let(:store_name) { "Believe" }
          it "builds an album with one artist with the name Various Artists and roles including 'primary'" do
            artist = @converted_album.artists.first
            expect(artist.name).to eq("Various Artists")
            expect(artist.roles).to match(["primary"])
          end
        end

        context "deezer" do
          let(:store_id) { 32 }
          let(:store_name) { "Deezer" }
          it "builds an album with one artist with the name Various Artists and roles including 'primary'" do
            artist = @converted_album.artists.first
            expect(artist.name).to eq("Various Artists")
            expect(artist.roles).to match(["primary"])
          end
        end

        context "Kkbox" do
          let(:store_id) { 53 }
          let(:store_name) { "Kkbox" }
          it "builds an album with one artist with the name Various Artists and roles including 'primary'" do
            artist = @converted_album.artists.first
            expect(artist.name).to eq("Various Artists")
            expect(artist.roles).to match(["primary"])
          end
        end

        context "Akazoo" do
          let(:store_id) { 51 }
          let(:store_name) { "Akazoo" }
          it "builds an album with one artist with the name Various Artists and roles including 'primary'" do
            artist = @converted_album.artists.first
            expect(artist.name).to eq("Various Artists")
            expect(artist.roles).to match(["primary"])
          end
        end

        context "Neurotic Media" do
          let(:store_id) { 56 }
          let(:store_name) { "NeuroticMedia" }
          it "builds an album with one artist with the name Various Artists and roles including 'primary'" do
            artist = @converted_album.artists.first
            expect(artist.name).to eq("Various Artists")
            expect(artist.roles).to match(["primary"])
          end
        end

        context "Slacker" do
          let(:store_id) { 47 }
          let(:store_name) { "Slacker" }
          it "builds an album with one artist with the name Various Artists and roles including 'primary'" do
            artist = @converted_album.artists.first
            expect(artist.name).to eq("Various Artists")
            expect(artist.roles).to match(["primary"])
          end
        end

        context "Anghami" do
          let(:store_id) { 52 }
          let(:store_name) { "Anghami" }
          it "builds an album with one artist with the name Various Artists and roles including 'primary'" do
            artist = @converted_album.artists.first
            expect(artist.name).to eq("Various Artists")
            expect(artist.roles).to match(["primary"])
          end
        end

        context "Rhapsody" do
          let(:store_id) { 7 }
          let(:store_name) { "Rhapsody" }
          it "builds an album with one artist with the name Various Artists and roles including 'primary'" do
            artist = @converted_album.artists.first
            expect(artist.name).to eq("Various Artists")
            expect(artist.roles).to match(["primary"])
          end
        end

        context "Spinlet" do
          let(:store_id) { 55 }
          let(:store_name) { "Spinlet" }
          it "builds an album with one artist with the name Various Artists and roles including 'primary'" do
            artist = @converted_album.artists.first
            expect(artist.name).to eq("Various Artists")
            expect(artist.roles).to match(["primary"])
          end
        end

        context "Youtube Music" do
          let(:store_id)   { 45 }
          let(:store_name) { "YoutubeMusic" }

          it "builds an album with one artist with the name Various Artists and roles including 'primary'" do
            artist = @converted_album.artists.first
            expect(artist.name).to eq("Various Artists")
            expect(artist.roles).to match(["primary"])
          end
        end
      end

      context "when the converter class is DistributionSystem" do
        before(:each) do
          store     = Store.find(store_id)
          allow(store).to receive(:delivery_service).and_return("tunecore")
          album = create(:album, :with_artwork, primary_genre: Genre.find(1), is_various: true)
          song  = create(:song, :with_upload, album_id: album.id)
          petri_bundle = create(:petri_bundle, album_id: album.id)
          distribution = create(:distribution, :with_salepoint, petri_bundle_id: petri_bundle.id, converter_class: "DistributionSystem::DDEX::Converter", state: "new", delivery_type: "metadata_only")
          salepoint = distribution.salepoints.first
          allow(salepoint).to receive(:store).and_return(store)
          allow(distribution).to receive(:store).and_return(store)
          allow(store).to receive(:in_use_flag).and_return(true)
          @converted_album = Delivery::AlbumConverter.convert(distribution, "metadata_only")
        end

        context "Touch Tunes" do
          let(:store_id)   { 82 }

          it "builds an album with one artist with the name Various Artists and roles including 'primary'" do
            artist = @converted_album.artists.first
            expect(artist.name).to eq("Various Artists")
            expect(artist.roles).to match(["primary"])
          end
        end
      end
    end

    context "with various language_code cases" do
      let(:store_id) { 75 }
      let(:store_name) { "Believe" }

      it "sets the track language_code to song's code" do
        store = Store.find(store_id)
        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
        allow(store).to receive(:delivery_service).and_return("tunecore")
        @album = create(:album, :with_artwork, primary_genre: Genre.find(1))
        @artist2 = Artist.last
        language = create(:language_code, description: 'French', code: 'fr')
        song = create(:song, :with_upload, album_id: @album.id, lyric_language_code_id: language.id)
        creative2 = create(:creative, creativeable_type: "Song", creativeable_id: song.id, artist_id: @artist2.id, role: "with")
        CreativeSongRole.create(creative_id: creative2.id, song_role_id: 80, song_id: song.id)
        petri_bundle = create(:petri_bundle, album_id: @album.id)
        distribution = create(:distribution, :with_salepoint, petri_bundle_id: petri_bundle.id, converter_class: "MusicStores::#{store_name}::Converter", state: "new", delivery_type: "metadata_only")
        salepoint    = distribution.salepoints.first
        allow(salepoint).to receive(:store).and_return(store)
        allow(distribution).to receive(:store).and_return(store)
        @converted_album = Delivery::AlbumConverter.convert(distribution, "metadata_only")

        track = @converted_album.tracks.first
        expect(track.language_code).to eq('fr')
      end

      it "sets the track language_code to album's code" do
        store = Store.find(store_id)
        allow(store).to receive(:delivery_service).and_return("tunecore")
        language = create(:language_code, description: 'English', code: 'en')
        @album = create(:album, :with_artwork, primary_genre: Genre.find(1), language_code: language.code)
        @artist2 = Artist.last
        song = create(:song, :with_upload, album_id: @album.id)
        creative2 = create(:creative, creativeable_type: "Song", creativeable_id: song.id, artist_id: @artist2.id, role: "with")
        CreativeSongRole.create(creative_id: creative2.id, song_role_id: 80, song_id: song.id)
        petri_bundle = create(:petri_bundle, album_id: @album.id)
        distribution = create(:distribution, :with_salepoint, petri_bundle_id: petri_bundle.id, converter_class: "MusicStores::#{store_name}::Converter", state: "new", delivery_type: "metadata_only")
        salepoint    = distribution.salepoints.first
        allow(salepoint).to receive(:store).and_return(store)
        allow(distribution).to receive(:store).and_return(store)
        @converted_album = Delivery::AlbumConverter.convert(distribution, "metadata_only")

        track = @converted_album.tracks.first
        expect(track.language_code).to eq('en')
      end
    end
  end
end
