require "rails_helper"
describe DistributionSystem::Believe::GenreHelper do

  describe '#get_genre' do
    subject(:genre)  { described_class.get_genre(genre_id, store) }

    context "when store is not specified in genre.yml" do
      let(:store) { "Spotify" }

      context "when genre id is an indian genre" do
        let(:genre_id) { 52 }
        it "uses world genre of default mapping" do
          expect(subject).to eq("World")
        end
      end

      context "when genre id is not an indian genre" do
        let(:genre_id) { 1 }
        it "uses default mapping value" do
          expect(subject).to eq("Alternative")
        end
      end
    end

    context "When store is specified in genre.yml" do
      let(:store) { "Akazoo" }

      context "when genre id is not an indian genre" do
        let(:genre_id) { 1 }
        it "uses custom mapping value" do
          expect(subject).to eq("ALTERNATIVE/INDIE")
        end
      end

      context "when genre id is an indian genre" do
        let(:genre_id) { 87 }
        it "uses world genre of custom mapping" do
          expect(subject).to eq("WORLD")
        end
      end
    end
  end
end
