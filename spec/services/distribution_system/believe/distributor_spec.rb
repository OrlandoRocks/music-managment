require "rails_helper"
require 'net/ftp'

describe DistributionSystem::Believe::Distributor do
  let(:album) do
    build(:distribution_system_album,
          :single,
          :with_tracks,
          :with_artists,
          :with_legacy_cover_image,
          delivery_type: "metadata_only",
          takedown: true,
          uuid: "63b0f7506f9c01333b5e22000a9f151d",
          store_name: "BelieveExp",
          store_id: 73)
  end

  let(:server_config) do
    { hostname:     "ftp.tunecore.com",
      username:     "petridelivery",
      port:         "22",
      password:     "AQfa!hBb" }
  end

  let(:timestamp)     { Time.now.strftime("%Y%m%d%H%M%S%L") }
  let(:config)        { StoreDeliveryConfig.where(store_id: 73).first }
  let(:petri_config)  { YAML.load_file(Rails.root.join("config", "delivery.yml"))[Rails.env] }
  let(:deliverer)     { DistributionSystem::Deliverer.new(petri_config) }
  let(:distributor)   { DistributionSystem::DistributorFactory.distributor_for(album, deliverer) }
  let(:distribution)  { FactoryBot.create(:distribution) }

  describe "#distribute" do

    before(:each) do
      DeliveryBatch.destroy_all
      album.set_data_from_config(config)
      allow(distributor.bundle).to receive(:skip_batching).and_return(false)
      allow(distributor.bundle).to receive(:collect_files).and_return(true)
      allow(distributor.bundle).to receive(:write_metadata).and_return(true)
      allow_any_instance_of(DeliveryBatch).to receive(:timestamp).and_return(timestamp)
    end

    context "store supports batching" do

      before(:each) do
        allow(DistributionSystem::BatchManager).to receive(:entire_batch_sent?).and_return(true)
      end

      it "sets the correct batch ID on the bundle" do
        allow(distributor).to receive(:do_send_bundle).and_return(true)
        distributor.distribute(album, distribution)
        batch = DeliveryBatch.where(store_id: 73, active: true).first
        expect(distributor.bundle.batch_id).to eq(batch.batch_id)
        expect(distributor.bundle.batch_id).to eq(timestamp)
      end

      it "increments the batch count" do
        allow(distributor).to receive(:do_send_bundle).and_return(true)
        distributor.distribute(album, distribution)
        batch = DeliveryBatch.where(store_id: 73, active: true).first
        expect(batch.count).to eq 1
        expect(batch.active).to be true
      end

      context "batch is complete" do
        before(:each) do
          ftp     = double("net_ftp").as_null_object
          session = double("session").as_null_object
          bundle_id    = distributor.bundle.album.upc
          remote_dir   = timestamp
          resource_dir = remote_dir

          allow_any_instance_of(DistributionSystem::Ftp).to receive(:initialize_ftp).and_return(ftp)
          allow(DistributionSystem::Ftp::Session).to receive(:new).with(ftp).and_return(session)
          DeliveryBatch.where(store_id: 73).destroy_all
          DeliveryBatch.create(store_id: 73, active: true, count: 99, processed_count: 99)
        end

        it "delivers the batch complete file" do
          expect(distributor).to receive(:send_complete_file)
          distributor.distribute(album, distribution)
        end
      end

      context "batch is not complete" do
        before(:each) do
          ftp     = double("net_ftp").as_null_object
          session = double("session").as_null_object
          bundle_id    = distributor.bundle.album.upc
          remote_dir   = timestamp
          resource_dir = remote_dir
          allow_any_instance_of(DistributionSystem::Ftp).to receive(:initialize_ftp).and_return(ftp)
          allow(DistributionSystem::Ftp::Session).to receive(:new).with(ftp).and_return(session)
        end

        it "does not deliver the batch complete file" do
          distributor.distribute(album, distribution)
          expect(distributor).to_not receive(:send_complete_file)
        end
      end

      context "distribution errors" do
        before(:each) do
          allow(distributor).to receive(:do_send_bundle).and_raise("error")
        end

        it "decrements the batch count" do
          begin
            distributor.distribute(album, distribution)
          rescue => e
          end
          batch = DeliveryBatch.where(store_id: 73, active: true).first
          expect(batch.count).to eq(0)
        end

        it 'should invoke send_batch_complete_file' do
          ftp     = double("net_ftp").as_null_object
          session = double("session").as_null_object
          allow_any_instance_of(DistributionSystem::Ftp).to receive(:initialize_ftp).and_return(ftp)
          allow(DistributionSystem::Ftp::Session).to receive(:new).with(ftp).and_return(session)
          allow(distributor).to receive(:should_send_batch_complete?).and_return(true)
          expect(distributor).to receive(:send_complete_file)
          begin
            distributor.distribute(album, distribution)
          rescue => e
          end
        end
      end
    end
    context "Deezer album" do
      let(:album) do
        build(:distribution_system_album,
              :single,
              :with_tracks,
              :with_artists,
              :with_artwork,
              delivery_type: "metadata_only",
              takedown: true,
              uuid: "63b0f7506f9c01333b5e22000a9f151d",
              store_name: "Deezer",
              store_id: 32)
      end
      let(:timestamp)   { Time.now.strftime("%Y%m%d%H%M%S%L") }
      let(:config)      { StoreDeliveryConfig.where(store_id: 32).first }
      let(:petri_config){ YAML.load_file(Rails.root.join("config", "delivery.yml"))[Rails.env] }
      let(:deliverer)   { DistributionSystem::Deliverer.new(petri_config) }
      let(:distributor) { DistributionSystem::DistributorFactory.distributor_for(album, deliverer) }
      let(:bundle)      { distributor.bundle }
      let(:distribution){ FactoryBot.create(:distribution, delivery_type: "full_delivery") }
      let(:work_dir)    { File.join(Rails.root, DELIVERY_CONFIG["work_dir"]) }
      let(:dir_name)    { FileUtils.mkdir_p("#{work_dir}/Deezer/#{album.upc}").first }

      it "distributes the content to Deezer" do
        allow_any_instance_of(DeliveryBatch).to receive(:timestamp).and_return(timestamp)
        allow(distributor).to receive(:full_delivery?).and_return(true)
        allow(distributor.bundle.album).to receive(:delivery_type).and_return("full_delivery")

        ftp     = double("net_ftp").as_null_object
        session = double("session").as_null_object
        bundle_id    = distributor.bundle.album.upc
        remote_dir   = "./#{timestamp}/#{bundle_id}"
        resource_dir = "#{remote_dir}/resources"
        allow(distributor).to receive(:assign_resource_dir).and_return(resource_dir)
        allow_any_instance_of(DistributionSystem::Ftp).to receive(:initialize_ftp).and_return(ftp)
        allow(DistributionSystem::Ftp::Session).to receive(:new).with(ftp).and_return(session)
        expect(session).to receive(:mkdir_p).with(remote_dir)
        expect(session).to receive(:mkdir_p).with(resource_dir)

        bundle.album.tracks.each do |track|
          track_file        = File.join(dir_name, "#{bundle.album.upc}_01_#{track.number}.flac")
          remote_track_file = File.join(resource_dir, "#{bundle.album.upc}_01_#{track.number}.flac")
          expect(session).to receive(:upload).with(track_file,remote_track_file)
        end

        artwork_file        = File.join(dir_name, "#{bundle.album.upc}.jpg")
        remote_artwork_file = File.join(resource_dir, "#{bundle.album.upc}.jpg")
        expect(session).to receive(:upload).with(artwork_file, remote_artwork_file)

        metadata_filename        = File.join(dir_name, "#{bundle.album.upc}.xml")
        remote_metadata_filename = File.join(remote_dir, "#{bundle.album.upc}.xml")
        expect(session).to receive(:upload).with(metadata_filename, remote_metadata_filename)
        distributor.distribute(album, distribution)
      end
    end
  end

  describe "#mark_batch_item_as_sent" do
    it "sets the current batch item to sent" do
      distributable = create(:distribution)
      album         = build(:distribution_system_album, :with_tracks, :with_booklet, :with_artwork, store_name: "Spotify", store_id: 26)
      bundle        = build(:believe_bundle, skip_batching: true, use_manifest: true, album: album)
      bundle.create_bundle
      distributor   = DistributionSystem::Believe::Distributor.new(bundle: bundle)
      distributor.distributable = distributable

      distributor.mark_batch_item_as_sent

      expect(distributor.delivery_batch.delivery_batch_items.pluck(:status).uniq.first).to eq "sent"
    end

    it "adds delivery manifest info the distribution batch item if the store requires a manifest" do
      distributable = create(:distribution)
      album         = build(:distribution_system_album, :with_tracks, :with_booklet, :with_artwork, store_name: "Spotify", store_id: 26)
      bundle        = build(:believe_bundle, skip_batching: true, use_manifest: true, album: album)
      bundle.create_bundle
      distributor  = DistributionSystem::Believe::Distributor.new(bundle: bundle)
      distributor.distributable = distributable
      batch_item  = distributor.delivery_batch.delivery_batch_items
                   .where(deliverable_type: distributable.class.name,
                          deliverable_id: distributable.id)
                   .first

      distributor.mark_batch_item_as_sent
      batch_item.reload

      item_params = {
        xml_remote_path: batch_item.xml_remote_path,
        uuid: batch_item.uuid,
        upc: batch_item.upc,
        takedown: batch_item.takedown
      }

      expect(item_params).to eq(distributor.bundle.manifest_params)
    end
  end
end
