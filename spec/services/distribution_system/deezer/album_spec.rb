require "rails_helper"

describe DistributionSystem::Believe::Album do
  context "when metadata_only" do
    let(:artist) { build(:distribution_system_artist, name: "Boston") }
    let(:track) do
      build(:distribution_system_track,
            artists: [artist],
            duration: "PT99H01M01S",
            isrc: "TCAAA1800045",
            title: "This Is a Song")
    end
    let(:album) do
      build(:distribution_system_album,
            album_type: "Album",
            artists: [artist],
            copyright_pline: "2018 Boston",
            countries: ["AF", "AX"],
            delivery_type: "metadata_only",
            genre: nil,
            label_name: "Boston",
            release_date: "2018-05-27",
            store_id: Store.find_by(abbrev: "sk").id,
            store_name: "Deezer",
            title: "Paths of Glory",
            tracks: [track],
            upc: "859718576822",
            uuid: "6b7e80a043fd013603b20a982a9c89f3")
    end

    before(:each) do
      track.album = album
      allow(album).to receive(:xml_timestamp).and_return("2018-05-27T17:00:45+00:00")
      allow(track).to receive(:formatted_duration).and_return("PT99H01M01S")
    end

    describe "#valid?" do
      it "returns true if valid" do
        expect(album).to be_valid
      end
    end

    describe "#validate_xml" do
      it "returns true if the xml is valid" do
        expect(album.validate_xml).to be_truthy
      end
    end

    describe "#to_xml" do
      let(:fixture_xml) do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/deezer_metadata_only.xml"))
      end

      it "returns true if the xml is valid" do
        #diff = Diffy::Diff.new(fixture_xml.to_s, Nokogiri::XML(album.to_xml, nil, 'utf-8').to_s, context: 2)
        #puts JSON.parse(diff.diff)
        result = compare_xml(album.to_xml, fixture_xml)
        expect(result).to be_truthy
      end
    end
  end

  context "when takedown" do
    let(:artist) { build(:distribution_system_artist, name: "Boston") }
    let(:track) do
      build(:distribution_system_track,
            isrc: "TCAAA1800045",
            duration: "PT99H01M01S",
            title: "This Is a Song",
            artists: [artist])
    end
    let(:album) do
      build(:distribution_system_album,
            album_type: "Album",
            artists: [artist],
            copyright_pline: "2018 Boston",
            countries: ["AF", "AX"],
            delivery_type: "metadata_only",
            genre: nil,
            label_name: "Boston",
            release_date: "2018-05-27",
            store_id: Store.find_by(abbrev: "sk").id,
            store_name: "Deezer",
						takedown: true,
            title: "Paths of Glory",
            tracks: [track],
            upc: "859726298624",
            uuid: "ab8edb00440c013674b871a75e23b5d8")
    end

    before(:each) do
      track.album = album
      allow(album).to receive(:xml_timestamp).and_return("2018-05-27T18:49:55+00:00")
      allow(track).to receive(:formatted_duration).and_return("PT99H01M01S")
    end

    describe "#valid?" do
      it "returns true if valid" do
        expect(album).to be_valid
      end
    end

    describe "#validate_xml" do
      it "returns true if the xml is valid" do
        expect(album.validate_xml).to be_truthy
      end
    end

    describe "#to_xml" do
      let(:fixture_xml) do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/deezer_takedown.xml"))
      end

      it "returns true if the xml is valid" do
        #diff = Diffy::Diff.new(fixture_xml.to_s, Nokogiri::XML(album.to_xml, nil, 'utf-8').to_s, context: 2)
        #puts JSON.parse(diff.diff)
        result = compare_xml(album.to_xml, fixture_xml)
        expect(result).to be_truthy
      end
    end
  end

  context "various artists" do
    let(:artist1) { build(:distribution_system_artist, name: "MF Doom") }
    let(:various) { build(:distribution_system_artist, name: "Various Artists") }

    let(:track1) do
      build(:distribution_system_track,
            artists: [artist1],
            duration: "PT99H01M01S",
            isrc: "TCAAA1800045",
            title: "This Is a Song")
    end

    let(:album) do
      build(:distribution_system_album,
            album_type: "Album",
            artists: [various],
            copyright_pline: "2018 Boston",
            countries: ["AF", "AX"],
            delivery_type: "metadata_only",
            genre: nil,
            label_name: "Boston",
            release_date: "2018-05-27",
            store_id: Store.find_by(abbrev: "sk").id,
            store_name: "Deezer",
            title: "Paths of Glory",
            tracks: [track1],
            upc: "859718576822",
            uuid: "6b7e80a043fd013603b20a982a9c89f3",
            is_various: true)
    end

    before(:each) do
      track1.album = album
      allow(album).to receive(:xml_timestamp).and_return("2018-05-27T17:00:45+00:00")
      allow(track1).to receive(:formatted_duration).and_return("PT99H01M01S")
    end

    describe "#valid?" do
      it "returns true if valid" do
        expect(album).to be_valid
      end
    end

    describe "#validate_xml" do
      it "returns true if the xml is valid" do
        expect(album.validate_xml).to be_truthy
      end
    end

    describe "#to_xml" do
      let(:album_xml) {album.to_xml}
      let(:fixture_xml) {Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/deezer_various_artists.xml"))}

      it "does not render 'Various Artists' on the track level as a main artist" do
        xml = Nokogiri::XML(album_xml)
        full_name = xml.at_xpath("//SoundRecordingDetailsByTerritory//DisplayArtist//PartyName//FullName").children.to_s
        expect(full_name).not_to eq("Various Artists")
      end

      it "returns true if the xml is valid" do
        result = compare_xml(album_xml, fixture_xml)
        expect(result).to be_truthy
      end
    end
  end
end
