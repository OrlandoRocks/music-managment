require "rails_helper"
include DistributionSystem::TrackLength
RSpec.describe DistributionSystem::TrackLength do
    describe "#get_duration_in_sec" do
        context "on uploading an audio with duration less than 1 min" do
            let(:mediainfo) {" Duration  : 30 s 20 ms "}
            it "should return the correct duration in seconds" do
                expect(get_duration_in_sec(mediainfo)).to eq(30)
            end
        end

        context "on uploading an audio with duration less than an hour but more than a min" do
            let(:mediainfo) {" Duration  : 1 min 35 s 10 ms "}
            it "should return the correct duration in seconds" do
                expect(get_duration_in_sec(mediainfo)).to eq(95)
            end
        end

        context "on uploading an audio with duration more than an hour" do
            let(:mediainfo) {" Duration  : 1 h 10 min 20 s 2 ms "}
            it "should return the correct duration in seconds" do
                expect(get_duration_in_sec(mediainfo)).to eq(4220)
            end
        end
    end
end