require "rails_helper"

describe DistributionSystem::Believe::Album do
  context "when metadata_only" do
    let(:artist) { build(:distribution_system_artist, name: "Earth Wind and Fire") }
    let(:track) do
      build(:distribution_system_track,
            artists: [artist],
            duration: "PT99H01M01S",
            isrc: "TCAAA1800044",
            title: "This Is a Song")
    end
    let(:album) do
      build(:distribution_system_album,
            album_type: "Album",
            artists: [artist],
            copyright_pline: "2018 Earth Wind and Fire",
            countries: ["AF", "AX"],
            delivery_type: "metadata_only",
            genre: nil,
            label_name: "Earth Wind and Fire",
            release_date: "2018-05-26",
            store_id: Store.find_by(abbrev: "sk").id,
            store_name: "Slacker",
            title: "If Not Now, When?",
            tracks: [track],
            upc: "859718576815",
            uuid: "d5495cc043650136a06c191cfbae72b7")
    end

    before(:each) do
      track.album = album
      allow(album).to receive(:xml_timestamp).and_return("2018-05-26T22:55:39+00:00")
      allow(track).to receive(:formatted_duration).and_return("PT99H01M01S")
    end

    describe "#valid?" do
      it "returns true if valid" do
        expect(album.valid?).to be_truthy
      end
    end

    describe "#validate_xml" do
      it "returns true if the xml is valid" do
        expect(album.validate_xml).to be_truthy
      end
    end

    describe "#to_xml" do
      let(:fixture_xml) do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/slacker_metadata_only.xml"))
      end

      it "returns true if the xml is valid" do
        # diff = Diffy::Diff.new(fixture_xml.to_s, Nokogiri::XML(album.to_xml, nil, 'utf-8').to_s, context: 2)
        # puts JSON.parse(diff.diff)
        result = compare_xml(album.to_xml, fixture_xml)
        expect(result).to be_truthy
      end
    end
  end
end
