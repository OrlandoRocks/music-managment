require "rails_helper"

describe DistributionSystem::Bundle do
  context "transcoding tracks" do
    context "stores inheriting from Believe" do
      describe "#transcode_track" do
        it "requests bitrate conversion for stores requiring normalization when the track's bit depth does not match the store's" do
          @config = StoreDeliveryConfig.readonly(false).for("Spotify")
          @config.update(normalize_bit_depth: true)
          @spot_album = build(:spotify_distribution_album, :with_tracks, store_name: "Spotify", store_id: 26)
          @bundle = build(:believe_bundle, skip_batching: true, album: @spot_album)
          @track = @spot_album.tracks.first
          @track.temp_filename = "#{@spot_album.upc}_#{@track.isrc}_#{@track.audio_file}"
          @audio_file = "#{Rails.root}/work/Spotify/859000000000/859000000000_01_1.flac"
          @track.audio_file = @audio_file
          @original_file = File.join(@bundle.dir.path, @track.temp_filename)

          allow(@bundle.transcoder).to receive(:get_bit_depth).with(File.join(@bundle.dirname, @track.temp_filename)).and_return("16")

          expect(@bundle.transcoder).to receive(:transcode).with(@original_file, @audio_file, normalize_bit_depth: true, bit_depth: @config.bit_depth)

          @bundle.transcode_track(@track)
        end

        it "requests a bit depth conversion when the transcoder reports a bitrate of nil and the store requires normalization" do
          @config = StoreDeliveryConfig.readonly(false).for("Spotify")
          @config.update(normalize_bit_depth: true)
          @spot_album = build(:spotify_distribution_album, :with_tracks, store_name: "Spotify", store_id: 26)
          @bundle = build(:believe_bundle, skip_batching: true, album: @spot_album)
          @track = @spot_album.tracks.first
          @track.temp_filename = "#{@spot_album.upc}_#{@track.isrc}_#{@track.audio_file}"
          @audio_file = "#{Rails.root}/work/Spotify/859000000000/859000000000_01_1.flac"
          @track.audio_file = @audio_file
          @original_file = File.join(@bundle.dir.path, @track.temp_filename)

          allow(@bundle.transcoder).to receive(:get_bit_depth).with(File.join(@bundle.dirname, @track.temp_filename)).and_return("nil")
          original_file = File.join(@bundle.dir.path, @track.temp_filename)

          expect(@bundle.transcoder).to receive(:transcode).with(@original_file, @audio_file, normalize_bit_depth: true, bit_depth: @config.bit_depth)

          @bundle.transcode_track(@track)
        end

        it "requests a bit depth conversion when the transcoder reports a bitrate of nil and the store does not require normalization" do
          @config = StoreDeliveryConfig.readonly(false).for("Spotify")
          @config.update(normalize_bit_depth: true)
          @spot_album = build(:spotify_distribution_album, :with_tracks, store_name: "Spotify", store_id: 26)
          @bundle = build(:believe_bundle, skip_batching: true, album: @spot_album)
          @track = @spot_album.tracks.first
          @track.temp_filename = "#{@spot_album.upc}_#{@track.isrc}_#{@track.audio_file}"
          @audio_file = "#{Rails.root}/work/Spotify/859000000000/859000000000_01_1.flac"
          @track.audio_file = @audio_file
          @original_file = File.join(@bundle.dir.path, @track.temp_filename)

          @spot_album.normalize_bit_depth = false
          allow(@bundle.transcoder).to receive(:get_bit_depth).with(File.join(@bundle.dirname, @track.temp_filename)).and_return("nil")
          original_file = File.join(@bundle.dir.path, @track.temp_filename)

          expect(@bundle.transcoder).to receive(:transcode).with(@original_file, @audio_file, normalize_bit_depth: true, bit_depth: @config.bit_depth)

          @bundle.transcode_track(@track)
        end

        it "does not request a bit depth conversion when the track has a bit depth and the store does not require normalization" do
          @config = StoreDeliveryConfig.readonly(false).for("Spotify")
          @config.update(normalize_bit_depth: true)
          @spot_album = build(:spotify_distribution_album, :with_tracks, store_name: "Spotify", store_id: 26)
          @bundle = build(:believe_bundle, skip_batching: true, album: @spot_album)
          @track = @spot_album.tracks.first
          @track.temp_filename = "#{@spot_album.upc}_#{@track.isrc}_#{@track.audio_file}"
          @audio_file = "#{Rails.root}/work/Spotify/859000000000/859000000000_01_1.flac"
          @track.audio_file = @audio_file
          @original_file = File.join(@bundle.dir.path, @track.temp_filename)

          @spot_album.normalize_bit_depth = false
          allow(@bundle.transcoder).to receive(:get_bit_depth).with(File.join(@bundle.dirname, @track.temp_filename)).and_return("16")
          original_file = File.join(@bundle.dir.path, @track.temp_filename)

          expect(@bundle.transcoder).to receive(:transcode).with(@original_file, @audio_file, normalize_bit_depth: false, bit_depth: @config.bit_depth)

          @bundle.transcode_track(@track)
        end

        it "requests a bit depth conversion for 32-bit wav files being converted to flac" do
          @config = StoreDeliveryConfig.readonly(false).for("Spotify")
          @config.update(normalize_bit_depth: true)
          @spot_album = build(:spotify_distribution_album, :with_tracks, store_name: "Spotify", store_id: 26)
          @bundle = build(:believe_bundle, skip_batching: true, album: @spot_album)
          @track = @spot_album.tracks.first
          @track.temp_filename = "#{@spot_album.upc}_#{@track.isrc}_#{@track.audio_file}"
          @audio_file = "#{Rails.root}/work/Spotify/859000000000/859000000000_01_1.flac"
          @track.audio_file = @audio_file
          @original_file = File.join(@bundle.dir.path, @track.temp_filename)

          @spot_album.normalize_bit_depth = false
          allow(@bundle.transcoder).to receive(:get_bit_depth).with(File.join(@bundle.dirname, @track.temp_filename)).and_return("32")
          original_file = File.join(@bundle.dir.path, @track.temp_filename)

          expect(@bundle.transcoder).to receive(:transcode).with(@original_file, @audio_file, normalize_bit_depth: true, bit_depth: @config.bit_depth)

          @bundle.transcode_track(@track)
        end
      end
    end

    context "stores inheriting from the top-level DistributionSystem Bundle" do
      describe "#transcode_track" do
        it "requests bitrate conversion for stores requiring normalization when the track's bit depth does not match the store's" do
          @config = StoreDeliveryConfig.readonly(false).for("Google")
          @config.update(normalize_bit_depth: true, bit_depth: 24)
          @album = build(:distribution_system_album, :with_tracks, store_name: "Google", store_id: 28)
          @bundle = build(:non_believe_bundle, album: @album)
          @bundle.create_bundle
          @track = @album.tracks.first
          @track.temp_filename = "#{@album.upc}_#{@track.isrc}_#{@track.audio_file}"
          @audio_file = "#{Rails.root}/work/Google/859000000000/859000000000_01_1.flac"
          @track.audio_file = @audio_file
          @original_file = File.join(@bundle.dir.path, @track.temp_filename)


          allow(@bundle.transcoder).to receive(:get_bit_depth).with(File.join(@bundle.dirname, @track.temp_filename)).and_return("16")

          expect(@bundle.transcoder).to receive(:transcode).with(@original_file, @audio_file, normalize_bit_depth: true, bit_depth: @config.bit_depth)

          @bundle.transcode_track(@track)
        end

        it "requests a bit depth conversion when the transcoder reports a bit depth of nil and the store requires normalization" do
          @config = StoreDeliveryConfig.readonly(false).for("Google")
          @config.update(normalize_bit_depth: true, bit_depth: 16)
          @album = build(:distribution_system_album, :with_tracks, store_name: "Google", store_id: 28)
          @bundle = build(:non_believe_bundle, album: @album)
          @bundle.create_bundle
          @track = @album.tracks.first
          @track.temp_filename = "#{@album.upc}_#{@track.isrc}_#{@track.audio_file}"
          @audio_file = "#{Rails.root}/work/Google/859000000000/859000000000_01_1.flac"
          @track.audio_file = @audio_file
          @original_file = File.join(@bundle.dir.path, @track.temp_filename)

          allow(@bundle.transcoder).to receive(:get_bit_depth).with(File.join(@bundle.dirname, @track.temp_filename)).and_return("nil")
          original_file = File.join(@bundle.dir.path, @track.temp_filename)

          expect(@bundle.transcoder).to receive(:transcode).with(@original_file, @audio_file, normalize_bit_depth: true, bit_depth: @config.bit_depth)

          @bundle.transcode_track(@track)
        end

        it "requests a bit depth conversion when the transcoder reports a bitrate of nil and the store does not require normalization" do
          @config = StoreDeliveryConfig.readonly(false).for("Google")
          @config.update(normalize_bit_depth: true, bit_depth: 16)
          @album = build(:distribution_system_album, :with_tracks, store_name: "Google", store_id: 28)
          @bundle = build(:non_believe_bundle, album: @album)
          @bundle.create_bundle
          @track = @album.tracks.first
          @track.temp_filename = "#{@album.upc}_#{@track.isrc}_#{@track.audio_file}"
          @audio_file = "#{Rails.root}/work/Google/859000000000/859000000000_01_1.flac"
          @track.audio_file = @audio_file
          @original_file = File.join(@bundle.dir.path, @track.temp_filename)

          @album.normalize_bit_depth = false
          allow(@bundle.transcoder).to receive(:get_bit_depth).with(File.join(@bundle.dirname, @track.temp_filename)).and_return("nil")
          original_file = File.join(@bundle.dir.path, @track.temp_filename)

          expect(@bundle.transcoder).to receive(:transcode).with(@original_file, @audio_file, normalize_bit_depth: true, bit_depth: @config.bit_depth)

          @bundle.transcode_track(@track)
        end

        it "does not request a bit depth conversion when the track has a bit depth and the store does not require normalization" do
          @config = StoreDeliveryConfig.readonly(false).for("Juke")
          @album = build(:distribution_system_album, :with_tracks, store_name: "Juke", store_id: 44)
          @bundle = build(:non_believe_bundle, album: @album)
          @bundle.create_bundle
          @track = @album.tracks.first
          @track.temp_filename = "#{@album.upc}_#{@track.isrc}_#{@track.audio_file}"
          @audio_file = "#{Rails.root}/work/Juke/859000000000/859000000000_01_1.flac"
          @track.audio_file = @audio_file
          @original_file = File.join(@bundle.dir.path, @track.temp_filename)

          @album.normalize_bit_depth = false
          allow(@bundle.transcoder).to receive(:get_bit_depth).with(File.join(@bundle.dirname, @track.temp_filename)).and_return("16")
          original_file = File.join(@bundle.dir.path, @track.temp_filename)

          expect(@bundle.transcoder).to receive(:transcode).with(@original_file, @audio_file, normalize_bit_depth: false, bit_depth: 16)

          @bundle.transcode_track(@track)
        end

        it "requests a bit depth conversion for 32-bit wav files being converted to flac" do
          @config = StoreDeliveryConfig.readonly(false).for("Juke")
          @album = build(:distribution_system_album, :with_tracks, store_name: "Juke", store_id: 44)
          @bundle = build(:non_believe_bundle, album: @album)
          @bundle.create_bundle
          @track = @album.tracks.first
          @track.temp_filename = "#{@album.upc}_#{@track.isrc}_#{@track.audio_file}"
          @audio_file = "#{Rails.root}/work/Juke/859000000000/859000000000_01_1.flac"
          @track.audio_file = @audio_file
          @original_file = File.join(@bundle.dir.path, @track.temp_filename)

          allow(@bundle.transcoder).to receive(:get_bit_depth).with(File.join(@bundle.dirname, @track.temp_filename)).and_return("32")
          original_file = File.join(@bundle.dir.path, @track.temp_filename)

          expect(@bundle.transcoder).to receive(:transcode).with(@original_file, @audio_file, normalize_bit_depth: true, bit_depth: 16)

          @bundle.transcode_track(@track)
        end
      end
    end
  end
end
