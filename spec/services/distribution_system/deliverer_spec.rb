require "rails_helper"

describe DistributionSystem::Deliverer do
  let(:distribution_system) do
    DistributionSystem::Deliverer.new(DELIVERY_CONFIG)
  end
  let(:distribution) { create(:distribution, :with_salepoint) }
  let(:album) do
    DistributionSystem::Believe::Album.new(
      store_name: "Amazon",
      upc: "123e45678",
      track_only: false
    )
  end
  
  let(:distributor) do
    DistributionSystem::Believe::Distributor.new(
      {
        bundle: DistributionSystem::Believe::Bundle.new(
          distribution_system.work_dir,
          distribution_system.transcoder,
          distribution_system.s3_connection,
          album,
          StoreDeliveryConfig.for(album.store_name)
        )
      }
    )
  end

  before(:each) do
    allow(Store)
      .to receive(:new)
      .and_return(create(:store, delivery_service: "tunecore"))
    allow(distributor)
      .to receive(:distribute)
      .and_return(true) # must come after Store stub above
  end

  describe "#new" do
    it "sets the work dir" do
      expect(distribution_system.work_dir)
        .to eq("#{Rails.root}/#{DELIVERY_CONFIG["work_dir"]}")
    end

    it "configures the S3 client" do
      expect(DistributionSystem::S3)
        .to receive(:new)
        .with(DELIVERY_CONFIG["asset_bucket"])
        .and_return(double.as_null_object)
      expect(DistributionSystem::S3)
        .to receive(:new)
        .with('streaming.tunecore.com')
        .and_return(double.as_null_object)
      expect(DistributionSystem::S3)
        .to receive(:new)
        .with(DELIVERY_CONFIG["archive_bucket"])
        .and_return(double.as_null_object)

      DistributionSystem::Deliverer.new(DELIVERY_CONFIG)
    end
  end

  describe "distribute" do
    it "creates the correct distributor" do
      expect(DistributionSystem::Believe::Distributor)
        .to receive(:new)
        .and_return(distributor)

      distribution_system.distribute(album, distribution)
    end
  end

  describe "clean up" do
    it "cleans up via the distributor" do
      allow(DistributionSystem::Believe::Distributor)
        .to receive(:new)
        .and_return(distributor)
      allow(distributor)
        .to receive(:distribute)
        .and_return(true)

      expect(distributor)
        .to receive(:clean_up)

      distribution_system.distribute(album, distribution)
      distribution_system.clean_up
    end
  end

  describe "save metadata" do
    it "writes to the s3 metadata archive bucket" do
      allow(DistributionSystem::Believe::Distributor)
        .to receive(:new)
        .and_return(distributor)
      allow(distributor).to receive(:distribute)
        .and_return(true)

      expect(distributor).to receive(:archive_metadata)
        .with(
          distribution_system.metadata_archive_bucket,
          "Believe",
          album.upc,
          distribution.sqs_message_id
        )

      distribution_system.distribute(album, distribution)
      distribution_system.archive_metadata(
                            "Believe",
                            album.upc,
                            distribution.sqs_message_id
                          )
    end
  end
end
