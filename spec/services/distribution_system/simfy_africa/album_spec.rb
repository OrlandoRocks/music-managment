require "rails_helper"
RSpec.describe DistributionSystem::SimfyAfrica::Album do
  before(:each) do
    allow(Time).to receive(:now).and_return(double({iso8601: "2016-01-01T12:00:00-05:00", getlocal: "2016-01-01T12:00:00-05:00"}))
  end
  describe "#to_xml" do

    let(:album) do
      YAML.load_file("spec/distribution_fixtures/converted_albums/874215_10351548_SimfyAfrica.yml")
    end

    let(:fixture_xml) do
      Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/874215_simfyafrica.xml"))
    end

    before(:each) do
      expect(DistributionSystem::CheckSumHelper).to receive(:checksum_file).exactly(2).times.and_return("bc52fedc767e5efcaf68164b52877025")
      cover_image = DistributionSystem::Itunes::CoverImage.new(
                      asset: "spec/distribution_fixtures/assets/cover.jpg"
                    )
      expect(album).to receive(:artwork_file).exactly(2).times.and_return(cover_image)
      album.tracks.each do |track|
        expect(track).to receive(:seconds).and_return(2)
        expect(track).to receive(:audio_file).exactly(2).times.and_return('spec/distribution_fixtures/assets/track.wav')
      end
      #puts Diffy::Diff.new(fixture_xml.to_s, Nokogiri::XML(album.to_xml, nil, 'utf-8').to_s, :context => 2)
    end

    it "matches xml fixture" do
      expect(
        EquivalentXml.equivalent?(
          Nokogiri::XML(album.to_xml, nil, 'utf-8'),
          fixture_xml,
          :element_order => false,
          :normalize_whitespace => true
        )
      ).to eq true
    end
  end
end
