require "rails_helper"

describe DistributionSystem::SimfyAfrica::Distributor do
  let(:delivery_config) { StoreDeliveryConfig.for(album.store_name) }
  let(:work_dir)        { DELIVERY_CONFIG["work_dir"] }
  let(:album) do
    YAML.load_file("spec/distribution_fixtures/converted_albums/874215_10351548_SimfyAfrica.yml")
  end

  let(:config) do
    YAML.load_file(Rails.root.join("config", "delivery.yml"))[Rails.env]
  end

  let(:shell) do
    DistributionSystem::Shell.new
  end

  let(:datestamp) { '20170908' }
  let(:timestamp) { '2017090812000' }

  let(:transcoder) do
    DistributionSystem::Transcoder.new({
      mp3_decoder:  "lame",
      alac_decoder: "alac",
      aac_decoder:  "faad",
      flac_encoder: "flac",
      flac_decoder: "flac",
      aac_encoder:  "faac",
      shell:        shell,
      tempdir:      DistributionSystem::TempDir.new
    })
  end

  let(:s3) do
    DistributionSystem::S3.new("asset_bucket")
  end

  let(:distribution) { FactoryBot.create(:distribution) }
  let(:bundle)       { DistributionSystem::SimfyAfrica::Bundle.new(
                          work_dir,
                          transcoder,
                          s3,
                          album,
                          delivery_config
                        )
                      }

  let(:dir_name) { FileUtils.mkdir_p("#{work_dir}/SimfyAF/#{album.upc}").first }

  let(:distributor)   {
    DistributionSystem::SimfyAfrica::Distributor.new({
      bundle:       bundle
    })
  }

  describe "#distribute" do
    before(:each) do
      allow(bundle).to receive(:collect_files).and_return(true)
      allow(bundle).to receive(:write_metadata).and_return(true)
    end
    it "collects the bundle files" do
      expect(bundle).to receive(:collect_files)
      allow(distributor).to receive(:send_bundle)
      distributor.distribute(album, distribution)
    end

    it "writes the bundle metadata" do
      expect(bundle).to receive(:write_metadata)
      allow(distributor).to receive(:send_bundle)
      distributor.distribute(album, distribution)
    end

    it "sends to SimfyAfrica" do
      allow_any_instance_of(DistributionSystem::Server).to receive(:prevent_external_deliveries?).and_return(false)
      sftp         = double("sftp_cli").as_null_object
      expect(DistributionSystem::SftpCli).to receive(:new).with(delivery_config).and_return(sftp)

      distributor = DistributionSystem::SimfyAfrica::Distributor.new({ bundle:       bundle })
      allow(distributor).to receive(:datestamp).and_return(datestamp)
      allow(distributor).to receive(:timestamp).and_return(timestamp)

      bundle_id    = bundle.album.upc
      batch_dir    = "#{datestamp}_#{timestamp}"
      remote_dir   = "#{batch_dir}/#{bundle_id}"
      resource_dir = remote_dir


      expect(sftp).to receive(:mkdir_p).with(batch_dir)
      expect(sftp).to receive(:mkdir_p).with(remote_dir)

      allow_any_instance_of(DistributionSystem::SftpCli).to receive(:start_processing).and_return(true)

      artwork_file        = File.join(dir_name, "#{bundle.album.upc}.jpg")
      remote_artwork_file = File.join(resource_dir, "#{bundle.album.upc}.jpg")

      expect(sftp).to receive(:upload).with(artwork_file, remote_artwork_file)

      bundle.album.tracks.each do |track|
        track_file        = File.join(dir_name, "#{bundle.album.upc}_01_#{track.number}.mp3")
        remote_track_file = File.join(resource_dir, "#{bundle.album.upc}_01_#{track.number}.mp3")
        expect(sftp).to receive(:upload).with(track_file,remote_track_file)
      end

      metadata_filename        = File.join(dir_name, "#{bundle.album.upc}.xml")
      remote_metadata_filename = File.join(resource_dir, "#{bundle.album.upc}.xml")
      expect(sftp).to receive(:upload).with(metadata_filename, remote_metadata_filename)
      expect(sftp).to receive(:upload).with('/tmp/delivery.complete', "#{remote_dir}/delivery.complete")
      distributor.distribute(album, distribution)
    end
  end
end
