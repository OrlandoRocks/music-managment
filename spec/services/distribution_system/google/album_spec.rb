require "rails_helper"

RSpec.describe DistributionSystem::Google::Album do
  before(:each) do
    allow_any_instance_of(DistributionSystem::Google::Artist).to receive(:tunecore_id).and_return("12345_6789")
    allow(Time).to receive(:now).and_return(double({iso8601: "2016-01-01T12:00:00-05:00", getlocal: "2016-01-01T12:00:00-05:00"}))
  end
  describe "#to_xml" do
    let(:album) do
      YAML.load_file("spec/distribution_fixtures/converted_albums/874215_4290329_Google.yml")
    end

    let(:all_countries) do
      YAML.load_file("spec/distribution_fixtures/converted_albums/all_countries.yml")
    end

    let(:fixture_xml) do
      Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/874215_google.xml"))
    end

    before(:each) do
      expect(DistributionSystem::CheckSumHelper).to receive(:checksum_file).exactly(2).times.and_return(123456)
      expect(album).to receive(:get_uuid).and_return('something')
      album.tracks.each do |track|
        expect(track).to receive(:audio_file).exactly(2).times.and_return('file.wav')
      end
    end

    it "is valid" do
      schema = DistributionSystem::Schema.new("app/services/distribution_system/schemas/ddex37.xsd")
      xml = Nokogiri::XML(album.to_xml, nil, 'utf-8')
      expect(schema.validate_input(xml)).to eq true
    end

    it "matches xml fixture" do
      expect(
        EquivalentXml.equivalent?(
          Nokogiri::XML(album.to_xml, nil, 'utf-8'),
          fixture_xml,
          :element_order => false,
          :normalize_whitespace => true
        )
      ).to eq true
    end

    context "with no album country" do
      before do
        allow(album).to receive(:countries).and_return(all_countries)
      end

      let(:fixture_xml_worldwide) do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/874215_google_worldwide.xml"))
      end

      it "returns a valid schema" do
        schema = DistributionSystem::Schema.new("app/services/distribution_system/schemas/ddex37.xsd")
        album.worldwide = true

        xml = Nokogiri::XML(album.to_xml, nil, 'utf-8')

        expect(schema.validate_input(xml)).to eq true
      end

      it "matches xml fixture" do
        schema = DistributionSystem::Schema.new("app/services/distribution_system/schemas/ddex37.xsd")
        album.worldwide = true
        expect(
          EquivalentXml.equivalent?(
            Nokogiri::XML(album.to_xml, nil, 'utf-8'),
            fixture_xml_worldwide,
            :element_order => false,
            :normalize_whitespace => true
          )
        ).to eq true
      end
    end

    context "with youtube oac" do
      before do
        allow_any_instance_of(DistributionSystem::Google::Artist).to receive(:channel_id).and_return("12345678")
        allow_any_instance_of(DistributionSystem::Google::Artist).to receive(:tunecore_id).and_return("12345_6789")
        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
      end

      let(:fixture_xml) do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/yt_oac.xml"))
      end


      it "matches xml fixture with channel and tunecore ids" do
        expect(
        EquivalentXml.equivalent?(
          Nokogiri::XML(album.to_xml, nil, 'utf-8'),
          fixture_xml,
          :element_order => false,
          :normalize_whitespace => true
        )
      ).to eq true
      end
    end

    context "with featured_artists" do
      let(:album) do
        YAML.load_file("spec/distribution_fixtures/converted_albums/874215_4290329_Google.yml")
      end

      let(:fixture_xml) do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/yt_featured_artists.xml"))
      end

      before do
        allow_any_instance_of(DistributionSystem::Google::Artist).to receive(:tunecore_id)
          .and_return("12345_6789")

        album.tracks.first.featured_artist_names = %w[
          FeaturedArtist1
          FeaturedArtist2
        ]
      end

      it "matches xml fixture with featured artists" do

        expect(
          EquivalentXml.equivalent?(
            Nokogiri::XML(album.to_xml, nil, 'utf-8'),
            fixture_xml,
            element_order: false,
            normalize_whitespace: true
          )
        ).to eq true
      end
    end

  end

  context "takedown" do
    let(:album) do
      YAML.load_file("spec/distribution_fixtures/converted_albums/874215_4290329_Google.yml")
    end

    let(:fixture_xml) do
      Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/google_takedown.xml"))
    end

    before do
      allow_any_instance_of(DistributionSystem::Google::Artist).to receive(:tunecore_id).and_return("12345_6789")
      allow(Time).to receive(:now).and_return(double({iso8601: "2016-01-01T12:00:00-05:00", getlocal: "2016-01-01T12:00:00-05:00"}))

      album.takedown = true
      album.takedown_at = "2018-12-17"

      expect(album).to receive(:get_uuid).and_return('something')
    end

    it "is valid" do
      schema = DistributionSystem::Schema.new("app/services/distribution_system/schemas/ddex37.xsd")
      xml = Nokogiri::XML(album.to_xml, nil, 'utf-8')
      expect(schema.validate_input(xml)).to eq true
    end

    it "matches xml fixture" do
      expect(
        EquivalentXml.equivalent?(
          Nokogiri::XML(album.to_xml, nil, 'utf-8'),
          fixture_xml,
          :element_order => false,
          :normalize_whitespace => true
        )
      ).to eq true
    end
  end

  describe "all_tracks_are_streamable?" do
    it "returns true if each track has availble_for_streaming set to true" do
      album = YAML.load_file("spec/distribution_fixtures/converted_albums/874215_4290329_Google_streaming.yml")
      expect(album.all_tracks_are_streamable?).to eq true
    end

    it "returns false if any track has available_for_streamin set to false" do
      album = YAML.load_file("spec/distribution_fixtures/converted_albums/874215_4290329_Google_no_streaming.yml")
      expect(album.all_tracks_are_streamable?).to eq false
    end
  end
end
