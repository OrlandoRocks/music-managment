require 'rails_helper'

describe DistributionSystem::Google::Converter do
  describe ".convert" do
    context "album has creatives" do
        let(:store_id) { 28 }
        let(:store_name) { "Google" }
        let(:store)  { Store.find(store_id) }
        let(:album) { create(:album, :with_artwork, primary_genre: Genre.find(1)) }

        let(:artist2) { Artist.last }
        let(:asset) { create(:s3_asset, key: "flac_key", bucket: "flac_bucket") }
        let!(:song) { create(:song, :with_upload, album_id: album.id, s3_flac_asset: asset) }
        let!(:creative2) { create(:creative, creativeable_type: "Song", creativeable_id: song.id, artist_id: artist2.id, role: "with") }
        let!(:song_role)  { create(:creative_song_role, creative_id: creative2.id, song_role_id: 80, song_id: song.id) }
        let(:petri_bundle) { create(:petri_bundle, album_id: album.id) }
        let(:distribution) { create(:distribution, :with_salepoint, petri_bundle_id: petri_bundle.id, converter_class: "MusicStores::#{store_name}::Converter", state: "new", delivery_type: "metadata_only") }
        let(:salepoint)  { distribution.salepoints.first }


        before(:each) do
          allow(store).to receive(:delivery_service).and_return("tunecore")
          allow(salepoint).to receive(:store).and_return(store)
          allow(distribution).to receive(:store).and_return(store)
        end

      context "google store" do
        let(:converted_album) { Delivery::AlbumConverter.convert(distribution, "metadata_only") }
        it "generates an album with the correct artist roles" do
          artist = converted_album.artist
          expect(artist.role).to eq("Performer")
        end

        it "track knows who primary artist is" do
          track = converted_album.tracks.first
          expect(track.artist.primary).to eq(true)
        end

        it "sets the s3 bucket and key to flac asset values" do
          track = converted_album.tracks.first
          expect(track.s3_key).to eq("flac_key")
          expect(track.s3_bucket).to eq("flac_bucket")
        end

        it "sets the tempfile with correct flac extension" do
          track = converted_album.tracks.first
          expect(track.temp_filename.split(".").last).to eq("flac")
        end

        context "when album country is empty" do
          before do
            album&.album_countries&.destroy
          end

          let(:expected_territories) { Country.unscoped.where.not(iso_code: "XK").pluck(:iso_code) }

          it "returns all unscoped territories" do
            convert_album = Delivery::AlbumConverter.convert(distribution, "metadata_only")
            expect(convert_album.countries).to eq(expected_territories)
          end
        end
      end

      context "takedown" do
        let(:converted_album) { Delivery::AlbumConverter.convert(distribution, "metadata_only") }

        context "salepoint takedown" do
          before do
            salepoint.takedown_at = Date.today
          end

          it "applies salepoint takedown date" do
            expected = converted_album.takedown_at
            expect(expected).to eq(salepoint.takedown_at.to_s.split(' ')[0])
          end
        end

        context "album takedown" do
          let(:album) { create(:album, :with_artwork, :taken_down, primary_genre: Genre.find(1)) }

          it "applies album takedown date" do
            expected = converted_album.takedown_at
            expect(expected).to eq(album.takedown_at.to_s.split(' ')[0])
          end
        end
      end
    end
  end
end
