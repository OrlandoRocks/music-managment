require "rails_helper"

describe DistributionSystem::Google::Distributor do
  let(:time)            { "20170905" }
  let(:delivery_config) { StoreDeliveryConfig.for(album.store_name) }
  let(:work_dir)        { DELIVERY_CONFIG["work_dir"] }

  let(:album) do
    YAML.load_file("spec/distribution_fixtures/converted_albums/874215_4290329_Google.yml")
  end

  let(:dir_name) { FileUtils.mkdir_p("#{work_dir}/Google/#{album.upc}").first }

  let(:config) do
    YAML.load_file(Rails.root.join("config", "delivery.yml"))[Rails.env]
  end

  let(:shell) do
    DistributionSystem::Shell.new
  end

  let(:transcoder) do
    DistributionSystem::Transcoder.new({
      mp3_decoder:  "lame",
      alac_decoder: "alac",
      aac_decoder:  "faad",
      flac_encoder: "flac",
      flac_decoder: "flac",
      aac_encoder:  "faac",
      shell:        shell,
      tempdir:      DistributionSystem::TempDir.new
    })
  end

  let(:s3) do
    DistributionSystem::S3.new("asset_bucket")
  end
  let(:schema) {
    DistributionSystem::Schema.new(
      Rails.root.join("app", "services", "distribution_system", "schemas", "ddex37.xsd"))
  }
  let(:distribution) { FactoryBot.create(:distribution) }
  let(:bundle)       { DistributionSystem::Google::Bundle.new(
    work_dir,
    transcoder,
    s3,
    album,
    StoreDeliveryConfig.for("Google")
  )
  }

  let(:distributor)   {
    DistributionSystem::Google::Distributor.new({
      bundle:       bundle,
      schema:       schema
    })
  }

  describe "#distribute" do
    before(:each) do
      allow(bundle).to receive(:collect_files).and_return(true)
    end
    it "collects the bundle files" do
      expect(bundle).to receive(:collect_files)
      allow(distributor).to receive(:write_metadata)
      allow(distributor).to receive(:send_bundle)
      distributor.distribute(album, distribution)
    end

    it "writes the bundle metadata" do
      expect(bundle).to receive(:write_metadata)
      allow(distributor).to receive(:send_bundle)
      distributor.distribute(album, distribution)
    end

    it "sends to Google" do
      track = double.as_null_object
      allow_any_instance_of(DistributionSystem::Google::Track).to receive(:audio_file).and_return(track)
      allow(DistributionSystem::CheckSumHelper).to receive(:checksum_file).and_return('4e7425289bb57391f358c3d24adf0228')
      allow(distributor).to receive(:set_batch_id).and_return(time)
      sftp        = double("net_sftp").as_null_object
      interpreter = DistributionSystem::Sftp::Interpreter.new(sftp)
      expect(Net::SFTP).to receive(:start).and_return(sftp)
      expect(DistributionSystem::Sftp::Interpreter).to receive(:new).and_return(interpreter)

      batch_dir  = "NewRelease/#{time}"
      bundle_id  = bundle.album.upc
      bundle_dir = "#{batch_dir}/#{bundle_id}"
      resource_dir  = "#{bundle_dir}/resources"

      expect(interpreter).to receive(:mkdir_p).with(bundle_dir)
      expect(interpreter).to receive(:mkdir_p).with(resource_dir)

      artwork_file        = File.join(dir_name, "#{bundle_id}.jpg")
      remote_artwork_file = File.join(resource_dir, "#{bundle_id}.jpg")
      expect(interpreter).to receive(:upload).with(artwork_file, remote_artwork_file)


      bundle.album.tracks.each do |track|
        track_file        = File.join(dir_name, "#{bundle.album.upc}_01_#{track.number}.flac")
        remote_track_file = File.join(resource_dir, "#{bundle.album.upc}_01_#{track.number}.flac")
        expect(interpreter).to receive(:upload).with(track_file,remote_track_file)
      end

      metadata_filename        = File.join(dir_name, "#{bundle.album.upc}.xml")
      remote_metadata_filename = File.join(bundle_dir, "#{bundle.album.upc}.xml")
      expect(interpreter).to receive(:upload).with(metadata_filename, remote_metadata_filename)
      expect(interpreter).to receive(:upload).with("/tmp/BatchComplete_#{time}.xml", "#{batch_dir}/BatchComplete_#{time}.xml")
      distributor.distribute(album, distribution)
    end
  end
end
