require "rails_helper"
RSpec.describe DistributionSystem::Believe::Album do
  describe "#to_xml" do

    let(:album) do
      YAML.load_file("spec/distribution_fixtures/converted_albums/zune.yml")
    end

    before(:each) do
      album.audio_codec_type="test"
      allow(DistributionSystem::CheckSumHelper).to receive(:checksum_file).and_return(123456)
      allow(Digest::MD5).to receive(:file).and_return("4329xxx999999999")
      allow_any_instance_of(DistributionSystem::Believe::Track).to receive(:formatted_duration).and_return("PT1H2M3S")
      allow_any_instance_of(DistributionSystem::Believe::Track).to receive(:audio_file).and_return(double.as_null_object)
    end

    it "is valid DDEX" do
      expect(album.valid?).to eq(true)
    end
  end
end
