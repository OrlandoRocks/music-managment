require "rails_helper"

describe DistributionSystem::YoutubeSr::Distributor do
  context "for YouTube Money/YTSR/ContetnID albums" do
    let(:time)     { Time.now.strftime("%H%M%S") }
    let(:work_dir) { DELIVERY_CONFIG["work_dir"] }
    let(:album) do
      YAML.load_file("spec/distribution_fixtures/converted_albums/ytsr_full_delivery.yml")
    end
    let(:delivery_config) {StoreDeliveryConfig.for(album.store_name)}

    let(:config) do
      StoreDeliveryConfig.for("YoutubeSR")
    end

    let(:sftp_config) {
      {
        host:     delivery_config["hostname"],
        username: delivery_config["username"],
        password: delivery_config["password"],
        port:     delivery_config["port"]
      }
    }

    let(:shell) do
      DistributionSystem::Shell.new
    end

    let(:transcoder) do
      DistributionSystem::Transcoder.new({
        mp3_decoder:  "lame",
        alac_decoder: "alac",
        aac_decoder:  "faad",
        flac_encoder: "flac",
        flac_decoder: "flac",
        aac_encoder:  "faac",
        shell:        shell,
        tempdir:      DistributionSystem::TempDir.new
      })
    end

    let(:s3) do
      DistributionSystem::S3.new("asset_bucket")
    end

    let(:distribution) { FactoryBot.create(:distribution, :with_distribution_songs).distribution_songs.first }
    let(:bundle)       { DistributionSystem::YoutubeSr::Bundle.new(
                            work_dir,
                            transcoder,
                            s3,
                            album,
                            config
                          )
                        }

    let(:distributor)   {
      DistributionSystem::YoutubeSr::Distributor.new({
        bundle:       bundle
      })
    }

    describe "#distribute" do
      before(:each) do
        allow(bundle).to receive(:collect_files).and_return(true)
        bundle.timestamp = time
      end

      it "collects the bundle files" do
        expect(bundle).to receive(:collect_files)
        allow(distributor).to receive(:write_metadata)
        allow(distributor).to receive(:send_bundle)
        distributor.distribute(album, distribution)
      end

      it "writes the bundle metadata" do
        expect(bundle).to receive(:write_metadata)
        allow(distributor).to receive(:send_bundle)
        distributor.distribute(album, distribution)
      end

      it "sends to YouTube" do
        allow(bundle).to receive(:write_metadata)
        allow(distributor).to receive(:validate_bundle).and_return(true)

        isrc        = album.tracks.first.isrc
        upc         = album.upc
        track_num   = album.tracks.first.number
        sftp        = double("net_sftp").as_null_object
        interpreter = DistributionSystem::Sftp::Interpreter.new(sftp)
        allow(Net::SFTP).to receive(:start).exactly(2).times.and_return(sftp)
        expect(DistributionSystem::Sftp::Interpreter).to receive(:new).exactly(1).times.and_return(interpreter)

        bundle_id    = bundle.album.upc
        date = Date.today.strftime("%Y%m%d")
        remote_dir   = "tunecore_contentID/#{date}/#{time}_#{album.tunecore_id}_#{isrc}"
        resource_dir = "#{remote_dir}/resources"

        expect(interpreter).to receive(:mkdir_p).ordered.with(remote_dir)
        expect(interpreter).to receive(:mkdir_p).ordered.with(resource_dir)

        track_file        = "./work/YoutubeSR/#{upc}/#{isrc}/#{isrc}_01_#{track_num}.flac"
        remote_track_file = "#{resource_dir}/#{isrc}_01_#{track_num}.flac"

        expect(interpreter).to receive(:upload).ordered.with(track_file, remote_track_file)

        metadata_filename        = "./work/YoutubeSR/#{upc}/#{isrc}/#{isrc}.xml"
        remote_metadata_filename = "#{remote_dir}/#{isrc}.xml"

        expect(interpreter).to receive(:upload).ordered.with(metadata_filename, remote_metadata_filename)

        batch_complete_file = FileUtils.touch(File.join(Rails.root, "tmp", "BatchComplete_#{bundle_id}.xml")).first
        remote_batch_complete_file = File.join(remote_dir, "BatchComplete_#{bundle_id}.xml")
        expect(interpreter).to receive(:upload).ordered.with(batch_complete_file, remote_batch_complete_file)

        distributor.distribute(album, distribution)
      end

      it "should rescue and raise delivery error " do
        allow(bundle).to receive(:write_metadata)
        allow(distributor).to receive(:validate_bundle).and_return(true)
        sftp        = double("net_sftp").as_null_object
        interpreter = DistributionSystem::Sftp::Interpreter.new(sftp)
        allow(Net::SFTP).to receive(:start).exactly(2).times.and_return(sftp)
        expect(DistributionSystem::Sftp::Interpreter).to receive(:new).exactly(1).times.and_return(interpreter)
        allow(distributor).to receive(:send_batch_complete_file_to_release).and_raise('error connecting to server')
        
        expect { distributor.distribute(album, distribution) }.to raise_error(DistributionSystem::DeliveryError)
      end
    end
  end
end
