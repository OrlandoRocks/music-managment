require "rails_helper"

describe DistributionSystem::YoutubeSr::Album do
  describe "to_xml" do
    let(:sdconfig) { StoreDeliveryConfig.for("YoutubeSr") }

    let(:full_delivery) { YAML.load_file("spec/distribution_fixtures/converted_albums/ytsr_full_delivery.yml") }
    let(:metadata_only) { YAML.load_file("spec/distribution_fixtures/converted_albums/ytsr_metadata_only.yml") }
    let(:takedown)      { YAML.load_file("spec/distribution_fixtures/converted_albums/ytsr_takedown.yml") }
    let(:asset_label)   { YAML.load_file("spec/distribution_fixtures/converted_albums/ytsr_full_delivery.yml") }

    let(:full_xml)          { Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/ytsr_full_delivery.xml")) }
    let(:metadata_only_xml) { Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/ytsr_metadata_only.xml")) }
    let(:takedown_xml)      { Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/ytsr_takedown.xml")) }
    let(:asset_label_xml)   { Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/ytsr_full_delivery_with_asset_label.xml")) }

    it "validates against the xml schema" do
      allow_any_instance_of(DistributionSystem::Believe::Track).to receive(:audio_file).and_return("x/USTC90987251_01_12.flac")
      allow_any_instance_of(DistributionSystem::Believe::Track).to receive(:formatted_duration).and_return("PT00H01M34S")
      [full_delivery, metadata_only, takedown].each do |album|
        album.set_data_from_config(sdconfig)
        expect(album.validate_xml).to be true
        expect(album.xml_validation_errors.present?).to be false
      end
    end

    context "it generates youtube-approved xml for " do
      it "a full delivery" do
        allow_any_instance_of(DistributionSystem::Believe::Track).to receive(:audio_file).and_return("x/USTC90987251_01_12.flac")
        allow_any_instance_of(DistributionSystem::Believe::Track).to receive(:formatted_duration).and_return("PT00H01M34S")
        sdconfig.test_delivery = true
        full_delivery.set_data_from_config(sdconfig)
        allow(full_delivery).to receive(:get_uuid).and_return("3df381b0afc1013601950242ac110003")
        allow(full_delivery).to receive(:xml_timestamp).and_return("2018-10-11T15:22:04-05:00")
        #diff = Diffy::Diff.new(full_xml, Nokogiri::XML(full_delivery.to_xml, nil, 'utf-8').to_s, :context => 2)
        # JSON.parse(diff.diff)
        expect(compare_xml(full_delivery.to_xml, full_xml)
        ).to eq true
      end

      it "a metadata_only delivery" do
        sdconfig.test_delivery = true
        metadata_only.set_data_from_config(sdconfig)
        allow(metadata_only).to receive(:get_uuid).and_return("c20518f0afb50136ce9f27499b29f855")
        allow(metadata_only).to receive(:xml_timestamp).and_return("2018-10-11T18:59:52+00:00")
        expect(compare_xml(metadata_only.to_xml, metadata_only_xml)
        ).to eq true
      end

      it "a takedown" do
        sdconfig.test_delivery = true
        takedown.set_data_from_config(sdconfig)
        allow(takedown).to receive(:get_uuid).and_return("0d03d9e0afb70136cea127499b29f855")
        allow(takedown).to receive(:xml_timestamp).and_return("2018-10-11T19:09:07+00:00")

        expect(compare_xml(takedown.to_xml, takedown_xml)
        ).to eq true
      end

      it "a full delivery with asset labels" do
        album_person = Person.first
        album_person.update_columns(country: "VN")

        allow_any_instance_of(DistributionSystem::Believe::Track).to receive(:audio_file).and_return("x/USTC90987251_01_12.flac")
        allow_any_instance_of(DistributionSystem::Believe::Track).to receive(:formatted_duration).and_return("PT00H01M34S")
        sdconfig.test_delivery = true
        asset_label.set_data_from_config(sdconfig)
        allow(asset_label).to receive(:get_uuid).and_return("3df381b0afc1013601950242ac110003")
        allow(asset_label).to receive(:xml_timestamp).and_return("2018-10-11T15:22:04-05:00")
        allow(Person).to receive(:find_by).and_return(album_person)

        expect(compare_xml(asset_label.to_xml, asset_label_xml)).to eq(true)
      end
    end

    context "when an album does not have album countries" do  
      it "is valid" do
        allow_any_instance_of(DistributionSystem::Believe::Track).to receive(:audio_file).and_return("x/USTC90987251_01_12.flac")
        allow_any_instance_of(DistributionSystem::Believe::Track).to receive(:formatted_duration).and_return("PT00H01M34S")
        sdconfig.test_delivery = true

        full_delivery.worldwide = true
        full_delivery.set_data_from_config(sdconfig)

        expect(full_delivery.validate_xml).to be true
        expect(full_delivery.xml_validation_errors.present?).to be false
      end
    end
  end
end
