require "rails_helper"

describe DistributionSystem::YoutubeSr::Converter do
  describe "#convert" do
    before { allow_any_instance_of(Song).to receive(:duration).and_return(3600) }
    context "When track is YTSR" do
      let(:album) { create(:album, :with_artwork) }
      let!(:petri_bundle) { create(:petri_bundle, album: album)}
      let(:song) { create(:song, :with_upload, album: album) }
      let(:track_monetization) { create(:track_monetization, song: song.reload, store_id: Store::YTSR_STORE_ID) }
      subject(:converted_album) { described_class.new.convert(track_monetization) }

      it "returns an instance of a distribution system album" do
        subject
        expect(converted_album.class.name).to eq "DistributionSystem::YoutubeSr::Album"
        expect(converted_album.tunecore_id).to eq album.id
        expect(converted_album.genre).to eq album.genres.first.id
        expect(converted_album.tracks.last.song_id).to eq song.id
      end

      context "when the album is not taken down" do
        it "calls deliver_art_track on YoutubeSrUpdateService" do
          expect(TrackMonetization::YoutubeSrUpdateService).to receive(:send_track_album)
          subject
        end
      end

      context "when the album is taken down" do
        before { album.takedown! }
        it "does not call YoutubeSrUpdateService" do
          expect(TrackMonetization::YoutubeSrUpdateService).not_to receive(:send_track_album)
        end
      end

      context "when the salepoint for Google Play is taken down" do
        let(:salepoint) { create(:salepoint, store_id: Store::GOOGLE_STORE_ID, salepointable: album)}
        let(:distribution) { create(:distribution, petri_bundle: album.petri_bundle) }

        before do
          distribution.salepoints << salepoint
          salepoint.takedown!
        end

        it "does not call YoutubeSrUpdateService" do
          expect(TrackMonetization::YoutubeSrUpdateService).to_not receive(:send_track_album)
        end
      end

      context "when album country is empty" do
        before do
          album&.album_countries&.destroy
        end

        let(:expected_territories) { Country.unscoped.where.not(iso_code: "XK").pluck(:iso_code) }

        it "returns all unscoped territories" do
          expect(subject.countries).to eq(expected_territories)
        end
      end
    end
  end
end
