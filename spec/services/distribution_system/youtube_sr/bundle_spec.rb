require "rails_helper"

describe DistributionSystem::YoutubeSr::Bundle do
  let(:config) { DELIVERY_CONFIG }
  let(:album)  { YAML.load_file("spec/distribution_fixtures/converted_albums/ytsr_full_delivery.yml") }
  let(:bundle) { DistributionSystem::YoutubeSr::Bundle.new(
                  config["work_dir"],
                  config["transcoder"],
                  config["s3_connection"],
                  album,
                  StoreDeliveryConfig.for(album.store_name)
                ) }
  let(:date)   { Date.today.strftime("%Y%m%d") }
  let(:time)   { Time.now.strftime("%H%M%S") }

  describe "#track_filename" do
    it "uses the track's isrc to assemble its filename" do
      track_number    = album.tracks.first.number
      track_isrc      = album.tracks.first.isrc
      local_filename  = bundle.track_filename(album.audio_codec_type, "01_#{track_number}" )
      expect(local_filename.include?(track_isrc)).to be true
      expect(local_filename.include?(album.upc)).to be false
    end
  end

  describe "#full_remote_dir" do
    before :each do
      bundle.timestamp = time
      @full_remote_dir = bundle.full_remote_dir
      @dir_array       = @full_remote_dir.split("/")
    end

    it "specifies a top-level directory labeled with today's date" do
      expect(@dir_array.first).to eq(date)
    end

    it "specifies the distribution's directory using isrc, album_id & timestamp" do
      expected_dirname = "#{time}_#{album.tunecore_id}_#{album.tracks.first.isrc}"
      expect(@dir_array.last).to eq(expected_dirname)
    end
  end
end
