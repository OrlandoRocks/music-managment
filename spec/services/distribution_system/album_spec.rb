require "rails_helper"

describe DistributionSystem::Album do
  let(:album) { build(:distribution_system_album, store_name: "Spotify") }
  let(:track1) { build(:distribution_system_track, album: album, duration: 30) }
  let(:track2) { build(:distribution_system_track, album: album, duration: nil) }
  let(:track3) { build(:distribution_system_track, album: album, duration: 20) }

  describe "#duration" do
    context "when all tracks have a duration value from bigbox metadata" do
      before(:each) do
        album.tracks = [track1, track3]
      end
      it "it should return the correct album duration" do
        expect(album.duration).to eq 50
      end
    end

    context "when some tracks have a duration value of nil from bigbox metadata" do
      before(:each) do
        album.tracks = [track1, track2, track3]
      end
      it "it should return the correct album duration" do
        expect(album.duration).to eq 50
      end
    end
  end

end