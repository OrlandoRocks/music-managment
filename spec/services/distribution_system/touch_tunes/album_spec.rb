require "rails_helper"

describe DistributionSystem::Believe::Album do
  context "when metadata_only" do
    let(:artist) { build(:distribution_system_artist, name: "whut") }
    let(:track) do
      build(:distribution_system_track,
            artists: [artist],
            duration: "PT00H03M48S",
            isrc: "TCADJ1738079",
            explicit_lyrics: true,
            title: "Test Song")
    end
    let(:album) do
      build(:distribution_system_album,
            album_type: "Single",
            artists: [artist],
            copyright_pline: "2017 whut",
            countries: ["AF", "AX"],
            delivery_type: "metadata_only",
            genre: 1,
            label_name: "whut",
            release_date: "2017-11-20",
            store_id: Store.find_by(abbrev: "ttunes").id,
            store_name: "TouchTunes",
            title: "Test Song",
            tracks: [track],
            upc: "859724145074",
            explicit_lyrics: true,
            uuid: "1c2ed4507ca90136f3440242ac110002")
    end

    before(:each) do
      track.album = album
      allow(album).to receive(:xml_timestamp).and_return("2018-08-07T14:50:50-05:00")
      allow(track).to receive(:formatted_duration).and_return("PT00H03M48S")
    end

    describe "#valid?" do
      it "returns true if valid" do
        expect(album.valid?).to be_truthy
      end
    end

    describe "#validate_xml" do
      it "returns true if the xml is valid" do
        expect(album.validate_xml).to be_truthy
      end
    end

    describe "#to_xml" do
      let(:fixture_xml) do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/touchtunes_metadata_only.xml"))
      end

      it "returns true if the xml is valid" do
        # diff = Diffy::Diff.new(fixture_xml.to_s, Nokogiri::XML(album.to_xml, nil, 'utf-8').to_s, context: 2)
        # puts JSON.parse(diff.diff)
        result = compare_xml(album.to_xml, fixture_xml)
        expect(result).to be_truthy
      end
    end
  end

  context "when full_delivery" do
    let(:artist) { build(:distribution_system_artist, name: "whut") }
    let(:track) do
      build(:distribution_system_track,
            artists: [artist],
            audio_file: "859724145074_01_1.mp3",
            duration: "PT00H03M49S",
            isrc: "TCADJ1738079",
            explicit_lyrics: true,
            title: "Test Song")
    end
    let(:album) do
      build(:distribution_system_album,
            album_type: "Single",
            artists: [artist],
            artwork_file: double.as_null_object,
            copyright_pline: "2017 whut",
            countries: ["AF", "AX"],
            delivery_type: "full_delivery",
            genre: 1,
            label_name: "whut",
            release_date: "2017-11-20",
            store_id: Store.find_by(abbrev: "ttunes"),
            store_name: "TouchTunes",
            title: "Test Song",
            tracks: [track],
            upc: "859724145074",
            explicit_lyrics: true,
            uuid: "c3636f807c8f0136f33d0242ac110002")
    end

    before(:each) do
      track.album = album
      allow(album).to receive(:xml_timestamp).and_return("2018-08-07T11:49:24-05:00")
      allow(track).to receive(:formatted_duration).and_return("PT00H03M49S")
      allow(Digest::MD5).to receive(:file).and_return("74a6052dc2343175818e27aed0de9089")
    end

    describe "#valid?" do
      it "returns true if valid" do
        expect(album.valid?).to be_truthy
      end
    end

    describe "#validate_xml" do
      it "returns true if the xml is valid" do
        expect(album.validate_xml).to be_truthy
      end
    end

    describe "#to_xml" do
      let(:fixture_xml) do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/touchtunes_full_delivery.xml"))
      end

      it "returns true if the xml is valid" do
        # diff = Diffy::Diff.new(fixture_xml.to_s, Nokogiri::XML(album.to_xml, nil, 'utf-8').to_s, context: 2)
        # puts JSON.parse(diff.diff)
        result = compare_xml(album.to_xml, fixture_xml)
        expect(result).to be_truthy
      end
    end
  end

  context "when takedown" do
    let(:artist) { build(:distribution_system_artist, name: "whut") }
    let(:track) do
      build(:distribution_system_track,
            artists: [artist],
            duration: "PT00H03M48S",
            isrc: "TCADJ1738079",
            explicit_lyrics: true,
            title: "Test Song")
    end
    let(:album) do
      build(:distribution_system_album,
            album_type: "Single",
            artists: [artist],
            copyright_pline: "2017 whut",
            countries: ["AF", "AX"],
            delivery_type: "metadata_only",
            genre: 1,
            label_name: "whut",
            release_date: "2017-11-20",
            store_id: Store.find_by(abbrev: "ttunes").id,
            store_name: "TouchTunes",
            takedown: true,
            title: "Test Song",
            tracks: [track],
            upc: "859724145074",
            explicit_lyrics: true,
            uuid: "7f202ca07c900136f33f0242ac110002")
    end

    before(:each) do
      track.album = album
      allow(album).to receive(:xml_timestamp).and_return("2018-08-07T11:54:39-05:00")
      allow(track).to receive(:formatted_duration).and_return("PT00H03M48S")
    end

    describe "#valid?" do
      it "returns true if valid" do
        expect(album.valid?).to be_truthy
      end
    end

    describe "#validate_xml" do
      it "returns true if the xml is valid" do
        expect(album.validate_xml).to be_truthy
      end
    end

    describe "#to_xml" do
      let(:fixture_xml) do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/touchtunes_takedown.xml"))
      end

      it "returns true if the xml is valid" do
        # diff = Diffy::Diff.new(fixture_xml.to_s, Nokogiri::XML(album.to_xml, nil, 'utf-8').to_s, context: 2)
        # puts JSON.parse(diff.diff)
        result = compare_xml(album.to_xml, fixture_xml)
        expect(result).to be_truthy
      end
    end
  end
end
