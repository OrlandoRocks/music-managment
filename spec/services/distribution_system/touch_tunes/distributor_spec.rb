require "rails_helper"

describe DistributionSystem::Believe::Distributor do
  let(:delivery_config) { StoreDeliveryConfig.for(album.store_name) }
  let(:work_dir)        { DELIVERY_CONFIG["work_dir"] }
  let(:dir_name)        { FileUtils.mkdir_p("#{work_dir}/TouchTunes/#{album.upc}").first }
  let(:config) do
    YAML.load_file(Rails.root.join("config", "delivery.yml"))[Rails.env]
  end

  let(:shell) { DistributionSystem::Shell.new }
  let(:transcoder) do
    DistributionSystem::Transcoder.new({
      aac_decoder:  "faad",
      aac_encoder:  "faac",
      alac_decoder: "alac",
      flac_decoder: "flac",
      flac_encoder: "flac",
      mp3_decoder:  "lame",
      shell:        shell,
      tempdir:      DistributionSystem::TempDir.new
    })
  end

  let(:s3) do
    DistributionSystem::S3.new("asset_bucket")
  end

  let(:distribution) { FactoryBot.create(:distribution) }
  let(:batch_id)     { Time.now.strftime("%Y%m%d%H%M%S%L") }
  let(:bundle) do
    DistributionSystem::Believe::Bundle.new(
      work_dir,
      transcoder,
      s3,
      album,
      delivery_config
    )
  end

  let(:distributor) do
    DistributionSystem::Believe::Distributor.new({ bundle: bundle })
  end

  let(:artist) { build(:distribution_system_artist, name: "Touch Tunes Artist") }
  let(:track) do
    build(:distribution_system_track,
          artists: [artist],
          duration: "PT00H00M02S",
          isrc: "TCAAA1800040",
          audio_file: "859726298624.mp3",
          title: "Touch Tunes Test")
  end

  let(:album) do
    build(:distribution_system_album,
          album_type: "Single",
          artists: [artist],
          artwork_file: double.as_null_object,
          copyright_pline: "2018 Touch Tunes Artist",
          countries: ["AF", "AX"],
          delivery_type: "full_delivery",
          genre: 1,
          label_name: "Touch Tunes Artist",
          release_date: "2018-05-21",
          store_id: Store.find_by(abbrev: "ttunes").id,
          store_name: "TouchTunes",
          title: "Touch Tunes Test",
          tracks: [track],
          upc: "859700000397",
          uuid: "851ada603f66013660c323dd5ac173f9")
  end

  before(:each) do
    track.album = album
    allow(track).to receive(:formatted_duration).and_return("PT00H00M22S")
    album
  end

  describe "#distribute" do
    before(:each) do
      allow(Digest::MD5).to receive(:file).and_return("rando-hash")
      allow(bundle).to receive(:collect_files).and_return(true)
      allow(album).to receive(:xml_timestamp).and_return("2018-04-26T20:15:37+00:00")
      allow(DistributionSystem::CheckSumHelper).to receive(:checksum_file)
        .and_return('4e7425289bb57391f358c3d24adf0228')
    end

    it "collects the bundle files" do
      expect(distributor).to receive(:send_bundle).and_return(true)
      expect(bundle).to receive(:collect_files)
      allow(distributor).to receive(:write_metadata)
      distributor.distribute(album, distribution)
    end

    it "writes the XML for the bundle" do
      expect(distributor).to receive(:send_bundle).and_return(true)
      expect(bundle).to receive(:write_metadata)
      distributor.distribute(album, distribution)
    end

    it "validates the bundle" do
      allow(album).to receive(:delivery_type).and_return("metadata_only")
      expect(distributor).to receive(:do_send_bundle).and_return(true)
      distributor.distribute(album, distribution)
    end

    it "sends the bundle to the remote server" do
      allow(distributor).to receive(:batch_full).and_return(true)
      allow_any_instance_of(DistributionSystem::Believe::Track)
        .to receive(:audio_file).and_return("859709363271.mp3")
      allow_any_instance_of(DistributionSystem::Believe::Track)
        .to receive(:iso8601).and_return("PT216S")
      allow(bundle).to receive(:batch_id).and_return(batch_id)

      sftp        = double("net_sftp").as_null_object
      interpreter = DistributionSystem::Sftp::Interpreter.new(sftp)
      expect(Net::SFTP).to receive(:start).exactly(2).times.and_return(sftp)
      expect(DistributionSystem::Sftp::Interpreter).to receive(:new).exactly(2).times.and_return(interpreter)
      expect(distributor).to receive(:should_send_batch_complete?).and_return(true)

      bundle_id    = bundle.album.upc
      remote_dir   = "incoming/#{batch_id}/#{bundle_id}"
      resource_dir = "#{remote_dir}/resources"

      expect(interpreter).to receive(:mkdir_p).ordered.with(remote_dir)
      expect(interpreter).to receive(:mkdir_p).ordered.with(resource_dir)

      track_file        = File.join(dir_name, "#{bundle_id}_01_1.mp3")
      remote_track_file = File.join(resource_dir, "#{bundle_id}_01_1.mp3")
      expect(interpreter).to receive(:upload).ordered.with(track_file, remote_track_file)

      artwork_file        = File.join(dir_name, "#{bundle_id}.jpg")
      remote_artwork_file = File.join(resource_dir, "#{bundle_id}.jpg")
      expect(interpreter).to receive(:upload).ordered.with(artwork_file, remote_artwork_file)

      metadata_file        = File.join(dir_name, "#{bundle_id}.xml")
      remote_metadata_file = File.join(remote_dir, "#{bundle_id}.xml")
      expect(interpreter).to receive(:upload).ordered.with(metadata_file, remote_metadata_file)

      batch_complete_file        = File.join(dir_name, "BatchComplete_#{bundle.batch_id}.xml")
      remote_batch_complete_file = File.join("incoming/#{batch_id}", "/BatchComplete_#{bundle.batch_id}.xml")
      expect(interpreter).to receive(:upload).with(batch_complete_file, remote_batch_complete_file)

      distributor.distribute(album, distribution)
    end
  end
end
