require "rails_helper"
RSpec.describe DistributionSystem::Nokia::Album do
  describe "#to_xml" do

    let(:album) do
      YAML.load_file("spec/distribution_fixtures/converted_albums/874215_4290324_Nokia.yml")
    end

    let(:fixture_xml) do
      Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/874215_nokia.xml"))
    end

    before(:each) do
      album.tracks.each do |track|
        expect(track).to receive(:seconds).and_return(134)
      end
    end

    it "matches xml fixture" do
      expect(
        EquivalentXml.equivalent?(
          Nokogiri::XML(album.to_xml, nil, 'utf-8'),
          fixture_xml,
          :element_order => false,
          :normalize_whitespace => true
        )
      ).to eq true
    end
  end
end
