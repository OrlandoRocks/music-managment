require "rails_helper"

describe DistributionSystem::YoutubeMusic::Distributor do
  let(:delivery_config) { StoreDeliveryConfig.for(album.store_name) }
  let(:work_dir)        { DELIVERY_CONFIG["work_dir"] }
  let(:dir_name)        { FileUtils.mkdir_p("#{work_dir}/YTMusic/#{album.upc}").first }
  let(:config) do
    YAML.load_file(Rails.root.join("config", "delivery.yml"))[Rails.env]
  end

  let(:shell) { DistributionSystem::Shell.new }
  let(:transcoder) do
    DistributionSystem::Transcoder.new({
      aac_decoder:  "faad",
      aac_encoder:  "faac",
      alac_decoder: "alac",
      flac_decoder: "flac",
      flac_encoder: "flac",
      mp3_decoder:  "lame",
      shell:        shell,
      tempdir:      DistributionSystem::TempDir.new
    })
  end

  let(:s3) do
    DistributionSystem::S3.new("asset_bucket")
  end

  let(:distribution) { FactoryBot.create(:distribution) }
  let(:bundle) do
    DistributionSystem::Believe::Bundle.new(
      work_dir,
      transcoder,
      s3,
      album,
      delivery_config
    )
  end

  let(:distributor) do
    DistributionSystem::YoutubeMusic::Distributor.new({ bundle: bundle })
  end

  let(:album) do
    artist = build(:distribution_system_artist,
                   name: "Sweet Lew")

    track1 = build(:distribution_system_track,
                   isrc: "TCADP1816553",
                   duration: "PT99H01M01S",
                   title: "Deez Tracks",
                   audio_file: "859726298624_01_1.flac",
                   number: 1,
                   explicit_lyrics: true,
                   artists: [artist])

    album = build(:distribution_system_album,
                  delivery_type: "full_delivery",
                  uuid: "81ee04e02bbc01367c290ecc0b9da1a7",
                  store_name: "YTMusic",
                  title: "Ytm 4eva",
                  release_date: "2018-04-26",
                  genre: 1,
                  upc: "859726298624",
                  label_name: "Sweet Lew",
                  artists: [artist],
                  tracks: [track1],
                  copyright_pline: "2018 Sweet Lew",
                  countries: ["AF", "AX"],
                  artwork_file: double.as_null_object,
                  explicit_lyrics: true,
                  store_id: 45)

    track1.album = album
    allow(track1).to receive(:formatted_duration).and_return("PT00H00M22S")
    album
  end

  describe "#distribute" do
    before(:each) do
      allow(Digest::MD5).to receive(:file).and_return("rando-hash")
      allow(bundle).to receive(:collect_files).and_return(true)
      allow(album).to receive(:xml_timestamp).and_return("2018-04-26T20:15:37+00:00")
      allow(DistributionSystem::CheckSumHelper).to receive(:checksum_file)
        .and_return('4e7425289bb57391f358c3d24adf0228')
    end

    it "collects the bundle files" do
      expect(distributor).to receive(:send_bundle).and_return(true)
      expect(bundle).to receive(:collect_files)
      allow(distributor).to receive(:write_metadata)
      distributor.distribute(album, distribution)
    end

    it "writes the XML for the bundle" do
      expect(distributor).to receive(:send_bundle).and_return(true)
      expect(bundle).to receive(:write_metadata)
      distributor.distribute(album, distribution)
    end

    it "validates the bundle" do
      allow(album).to receive(:delivery_type).and_return("metadata_only")
      expect(distributor).to receive(:do_send_bundle).and_return(true)
      distributor.distribute(album, distribution)
    end

    it "sends the bundle to the remote server" do
      allow_any_instance_of(DistributionSystem::Believe::Track)
        .to receive(:audio_file).and_return("859709363271_01_1.flac")
      allow_any_instance_of(DistributionSystem::Believe::Track)
        .to receive(:iso8601).and_return("PT216S")

      sftp        = double("net_sftp").as_null_object
      interpreter = DistributionSystem::Sftp::Interpreter.new(sftp)
      expect(Net::SFTP).to receive(:start).exactly(1).times.and_return(sftp)
      expect(DistributionSystem::Sftp::Interpreter).to receive(:new).exactly(1).times.and_return(interpreter)
      expect(distributor).to receive(:should_send_batch_complete?).and_return(false)

      todays_date =  Time.now.strftime("%Y%m%d")
      time_now     = bundle.timestamp
      bundle_id    = bundle.album.upc
      album_id     = bundle.album.tunecore_id
      remote_dir   = "./#{todays_date}/#{time_now}_#{album_id}_#{bundle_id}"
      resource_dir = "#{remote_dir}/resources"

      expect(interpreter).to receive(:mkdir_p).ordered.with(remote_dir)
      expect(interpreter).to receive(:mkdir_p).ordered.with(resource_dir)

      track_file        = File.join(dir_name, "#{bundle_id}_01_1.flac")
      remote_track_file = File.join(resource_dir, "#{bundle_id}_01_1.flac")
      expect(interpreter).to receive(:upload).ordered.with(track_file, remote_track_file)

      artwork_file        = File.join(dir_name, "#{bundle_id}.jpg")
      remote_artwork_file = File.join(resource_dir, "#{bundle_id}.jpg")
      expect(interpreter).to receive(:upload).ordered.with(artwork_file, remote_artwork_file)

      metadata_file        = File.join(dir_name, "#{bundle_id}.xml")
      remote_metadata_file = File.join(remote_dir, "#{bundle_id}.xml")
      expect(interpreter).to receive(:upload).ordered.with(metadata_file, remote_metadata_file)

      batch_complete_file = FileUtils.touch(File.join(Rails.root, "tmp", "BatchComplete_#{bundle_id}.xml")).first
      remote_batch_complete_file = File.join(remote_dir, "BatchComplete_#{bundle_id}.xml")
      expect(interpreter).to receive(:upload).ordered.with(batch_complete_file, remote_batch_complete_file)

      distributor.distribute(album, distribution)
    end
  end
end
