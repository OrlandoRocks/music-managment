require "rails_helper"

describe DistributionSystem::YoutubeMusic::Album do
  context "when a metadata_only" do
    before(:each) do
      artist = build(:distribution_system_artist,
                     name: "Craig Stuart Garfinkle")

      track = build(:distribution_system_track,
                    isrc: "TCABL1219401",
                    duration: "PT99H01M01S",
                    title: "Psycho Bells",
                    artists: [artist])

      @album = build(:distribution_system_album,
                     delivery_type: "metadata_only",
                     uuid: "7b25a6403129013635522dd5476c7426",
                     store_name: "YTMusic",
                     title: "Psycho Bells",
                     release_date: "2012-12-20",
                     genre: 6,
                     upc: "859709363271",
                     label_name: "Craig Stuart Garfinkle",
                     artists: [artist],
                     tracks: [track],
                     copyright_pline: "2012 Craig Stuart Garfinkle",
                     album_type: "Single",
                     countries: ["AF", "AX"],
                     store_id: 45)

      track.album = @album
      allow(@album).to receive(:xml_timestamp).and_return("2018-05-03T17:58:17+00:00")
      allow(track).to receive(:formatted_duration).and_return("PT99H01M01S")
    end

    describe "#valid?" do
      it "returns true if valid" do
        expect(@album.valid?).to be_truthy
      end
    end

    describe "#validate_xml" do
      it "returns true if the xml is valid" do
        expect(@album.validate_xml).to be_truthy
      end
    end

    describe "#to_xml" do
      let(:fixture_xml) do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/youtube_music_metadata_only.xml"))
      end

      it "returns true if the xml is valid" do
        #diff = Diffy::Diff.new(fixture_xml.to_s, Nokogiri::XML(@album.to_xml, nil, 'utf-8').to_s, :context => 2)
        #puts JSON.parse(diff.diff)
        result = compare_xml(@album.to_xml, fixture_xml)
        expect(result).to be_truthy
      end
    end
  end

  context "when full_delivery" do
    before(:each) do
      artist = build(:distribution_system_artist,
                     name: "Sweet Lew")

      track1 = build(:distribution_system_track,
                    isrc: "TCADP1816553",
                    duration: "PT99H01M01S",
                    title: "Deez Tracks",
                    audio_file: "859726298624_01_1.flac",
                    number: 1,
                    explicit_lyrics: true,
                    artists: [artist])

      track2 = build(:distribution_system_track,
                    isrc: "TCADP1816554",
                    duration: "PT99H01M01S",
                    title: "Deez Other Tracks",
                    audio_file: "859726298624_01_2.flac",
                    number: 2,
                    explicit_lyrics: true,
                    artists: [artist])

      @album = build(:distribution_system_album,
                     delivery_type: "full_delivery",
                     uuid: "81ee04e02bbc01367c290ecc0b9da1a7",
                     store_name: "YTMusic",
                     title: "Ytm 4eva",
                     release_date: "2018-04-26",
                     genre: 1,
                     upc: "859726298624",
                     label_name: "Sweet Lew",
                     artists: [artist],
                     tracks: [track1, track2],
                     copyright_pline: "2018 Sweet Lew",
                     countries: ["AF", "AX"],
                     artwork_file: double.as_null_object,
                     explicit_lyrics: true,
                     store_id: 45)

      track1.album = @album
      track2.album = @album
      allow(@album).to receive(:xml_timestamp).and_return("2018-04-26T20:15:37+00:00")
      allow(track1).to receive(:formatted_duration).and_return("PT00H00M22S")
      allow(track2).to receive(:formatted_duration).and_return("PT00H00M22S")
      allow(Digest::MD5).to receive(:file).and_return("13df40eadcfab48f1fc2b1c1379f95f8")
    end

    describe "#valid?" do
      it "returns true if valid" do
        expect(@album.valid?).to be_truthy
      end
    end

    describe "#validate_xml" do
      it "returns true if the xml is valid" do
        expect(@album.validate_xml).to be_truthy
      end
    end

    describe "#to_xml" do
      let(:fixture_xml) do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/youtube_music_full_delivery.xml"))
      end

      it "returns true if the xml is valid" do
        result = compare_xml(@album.to_xml, fixture_xml)
        expect(result).to be_truthy
      end
    end
  end

  context "when the album is a takedown" do
    before(:each) do
      artist = build(:distribution_system_artist,
                     name: "FOXKEEPER")

      track = build(:distribution_system_track,
                    isrc: "TCADO1823740",
                    duration: "PT99H01M01S",
                    title: "Rdy or Not",
                    number: 1,
                    artists: [artist])

      @album = build(:distribution_system_album,
                     delivery_type: "metadata_only",
                     uuid: "de4595302c73013626d4671c493c4f85",
                     store_name: "YTMusic",
                     title: "Rdy or Not",
                     release_date: "2018-04-13",
                     upc: "859725938552",
                     label_name: "FOXKEEPER",
                     artists: [artist],
                     genre: 23,
                     tracks: [track],
                     copyright_pline: "2018 FOXKEEPER",
                     album_type: "Single",
                     countries: ["AF", "AX"],
                     takedown: true,
                     store_id: 45)

      track.album = @album
      allow(@album).to receive(:xml_timestamp).and_return("2018-04-27T18:08:10+00:00")
      allow(track).to receive(:formatted_duration).and_return("PT99H01M01S")
    end

    describe "#valid?" do
      it "returns true if valid" do
        expect(@album.valid?).to be_truthy
      end
    end

    describe "#validate_xml" do
      it "returns true if the xml is valid" do
        expect(@album.validate_xml).to be_truthy
      end
    end

    describe "#to_xml" do
      let(:fixture_xml) do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/youtube_music_takedown.xml"))
      end

      it "returns true if the xml is valid" do
        result = compare_xml(@album.to_xml, fixture_xml)
        expect(result).to be_truthy
      end
    end
  end
end
