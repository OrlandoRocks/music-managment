require "rails_helper"

describe DistributionSystem::DistributorFactory do
  let(:env) {
    OpenStruct.new({
      work_dir: "/work/dir",
      transcoder: Object.new,
      s3_connection: Object.new
    })
  }

  context "Akazoo album" do
    let(:album)       {YAML.load_file("spec/distribution_fixtures/converted_albums/802661_akazoo.yml")}
    let(:bundle)      {double({})}

    describe "#distributor_for" do
      it "instantiates the distributor" do

        expect(DistributionSystem::Believe::Bundle).to receive(:new).with(
          env.work_dir,
          env.transcoder,
          env.s3_connection,
          album,
          StoreDeliveryConfig.for(album.store_name)
        ).and_return(bundle)

        expect(DistributionSystem::Believe::Distributor).to receive(:new).with({
          bundle:      bundle
        })

        DistributionSystem::DistributorFactory.distributor_for(album, env)
      end
    end
  end

  context "ITunes ablum" do
    let(:album) { build(:distribution_system_itunes_album, :with_booklet, :with_tracks, :with_creatives, :with_artwork, :with_products)}

    let(:bundle){ double }

    it "instantiates the distributor" do
      expect(DistributionSystem::Itunes::Bundle).to receive(:new).with(
        env.work_dir,
        env.transcoder,
        env.s3_connection,
        album
      ).and_return(bundle)
       expect(DistributionSystem::Itunes::Distributor).to receive(:new).with({
          bundle:       bundle
        })

      DistributionSystem::DistributorFactory.distributor_for(album, env)
    end
  end

  context "SevenDigital album" do
    let(:album){YAML.load_file("spec/distribution_fixtures/converted_albums/seven_digital_basic.yml")}

    let(:bundle) { double }

    it "instantiates the distributor" do
      expect(DistributionSystem::SevenDigital::Bundle).to receive(:new).with(
        env.work_dir,
        env.transcoder,
        env.s3_connection,
        album,
        StoreDeliveryConfig.for(album.store_name)
      ).and_return(bundle)

      expect(DistributionSystem::Believe::Distributor).to receive(:new).with({
        bundle: bundle
      })
      DistributionSystem::DistributorFactory.distributor_for(album, env)
    end
  end

  context "YTSR album" do
    let(:album){YAML.load_file("spec/distribution_fixtures/converted_albums/ytsr_metadata_only.yml")}
    let(:bundle) { double }

    it "instantiates the distributor" do
      expect(DistributionSystem::YoutubeSr::Bundle).to receive(:new).with(
        env.work_dir,
        env.transcoder,
        env.s3_connection,
        album,
        StoreDeliveryConfig.for(album.store_name)
      ).and_return(bundle)

      expect(DistributionSystem::Believe::Distributor).to receive(:new).with({
        bundle: bundle})
      DistributionSystem::DistributorFactory.distributor_for(album, env)
    end
  end
end
