require "rails_helper"

describe DistributionSystem::BatchManager do
  let(:timestamp)   { Time.now.strftime("%Y%m%d%H%M%S%L") }
  let(:distribution){ FactoryBot.create(:distribution, :with_salepoint) }
  let(:store)       { distribution.salepoints.first.store }

  before(:each) do
    allow_any_instance_of(DeliveryBatch).to receive(:timestamp).and_return(timestamp)
  end

  describe ".join_batch" do
    before(:each) do
      DeliveryBatch.destroy_all
    end

    context "when there is an open batch" do
      it "returns the active batch" do
        batch = DistributionSystem::BatchManager.join_batch(distribution.class.name, distribution.id, store.id)
        expect(batch.batch_id).to eq(timestamp)
      end

      it "increments the count for that batch_id" do
        batch = DistributionSystem::BatchManager.join_batch(distribution.class.name, distribution.id, store.id)
        expect(batch.count).to eq 1
      end

      context "when this release is the last release in the batch" do
        before(:each) do
          batch = DistributionSystem::BatchManager.join_batch(distribution.class.name, distribution.id, store.id)
          batch.update(count: 99)
        end
        it "sets the batch's active attribute to false" do
          batch = DistributionSystem::BatchManager.join_batch(distribution.class.name, distribution.id, store.id)
          expect(batch.active).to eq false
          expect(batch.count).to eq 100
        end
      end
    end

    context "when there is no open batch" do
      before(:each) do
        @inactive_batch = DeliveryBatch.create(store_id: store.id, active: false, count: 100)
      end
      it "creates a new batch" do
        batch = DistributionSystem::BatchManager.join_batch(distribution.class.name, distribution.id, store.id)
        expect(batch.id).to_not eq(@inactive_batch.id)
      end
    end
  end

  describe ".remove_from_batch" do
    it "decrements the count for the batch" do
      batch = DistributionSystem::BatchManager.join_batch(distribution.class.name, distribution.id, store.id)
      expect(batch.count).to eq(1)
      DistributionSystem::BatchManager.remove_from_batch(distribution.class.name, distribution.id, batch.id)
      batch.reload
      expect(batch.count).to eq(0)
    end

    context "when this removal brings the batch from full to not full" do
      it "sets active to false" do
        batch = DistributionSystem::BatchManager.join_batch(distribution.class.name, distribution.id, store.id)
        batch.update(count: 100, active: false)
      end
    end
  end

  describe ".entire_batch_sent?" do
    it "is true when all batch items are sent" do
      batch = DistributionSystem::BatchManager.join_batch(distribution.class.name, distribution.id, store.id)
      batch.delivery_batch_items.update_all(status: "sent")
      expect(DistributionSystem::BatchManager.entire_batch_sent?(batch)).to eq true
    end

    it "is retries when some batch items are not sent" do
      batch = DistributionSystem::BatchManager.join_batch(distribution.class.name, distribution.id, store.id)
      expect(DistributionSystem::BatchManager.entire_batch_sent?(batch)).to eq false
    end
  end

  describe ".close_batch" do
    context "when the store requires a delivery manifest" do
      it "generates a batch complete file and returns the path" do
        allow_any_instance_of(DistributionSystem::Sftp::Interpreter).to receive(:upload).with(any_args).and_return(true)
        store  = Store.find_by(short_name: "Spotify")
        distro = create(:distribution, :with_salepoint, store_short_name: store.short_name)
        batch  = DistributionSystem::BatchManager.join_batch(distro.class.name, distro.id, store.id)
        local_path = "#{Rails.root}/tmp/BatchComplete_#{batch.batch_id}.xml"
        remote_path = "tunecore/#{batch.batch_id}/BatchComplete_#{batch.batch_id}.xml"
        expect(DistributionSystem::DeliveryBatchManifest).to receive(:write_manifest).with(batch)
        expect_any_instance_of(DistributionSystem::Sftp::Interpreter).to receive(:upload).with(local_path, remote_path)
        DistributionSystem::BatchManager.close_batch(batch.batch_id)
      end
    end
  end

  describe ".close_stale_batches" do
    it "closes batches if the batch count matches the processed count" do
      delivery_batch = create(:delivery_batch, :with_delivery_batch_items,
                                               batch_size: 25,
                                               processed_count: 25,
                                               max_batch_size: 100,
                                               count: 25,
                                               created_at: 4.hours.ago)

      expect(DistributionSystem::BatchManager).to receive(:close_batch).with(delivery_batch.batch_id)
      DistributionSystem::BatchManager.close_stale_batches(delivery_batch.store.id, 180, nil)
    end

    it "closes batches that do not have currently processing distributions" do
      delivery_batch = create(:delivery_batch, :with_delivery_batch_items,
                                               batch_size: 25,
                                               processed_count: 20,
                                               max_batch_size: 100,
                                               count: 25,
                                               created_at: 4.hours.ago)

      expect(DistributionSystem::BatchManager).to receive(:close_batch).with(delivery_batch.batch_id)
      DistributionSystem::BatchManager.close_stale_batches(delivery_batch.store.id, 180, nil)
    end

    it "does not include batches that are created more than 25 hours ago" do
      delivery_batch = create(:delivery_batch, :with_delivery_batch_items,
                                               batch_size: 25,
                                               processed_count: 20,
                                               max_batch_size: 100,
                                               count: 25,
                                               created_at: Date.today - 1.day)

      expect(DistributionSystem::BatchManager).not_to receive(:close_batch).with(delivery_batch.batch_id)
      DistributionSystem::BatchManager.close_stale_batches(delivery_batch.store.id, 180, nil)
    end

    it "includes batches that are created more than 25 hours ago" do
      delivery_batch = create(:delivery_batch, :with_delivery_batch_items,
                                               batch_size: 25,
                                               processed_count: 20,
                                               max_batch_size: 100,
                                               count: 25,
                                               created_at: Date.today - 1.day)

      expect(DistributionSystem::BatchManager).to receive(:close_batch).with(delivery_batch.batch_id)
      DistributionSystem::BatchManager.close_stale_batches(delivery_batch.store.id, 180, 100)
    end

    it "does not include batches that are created more than 47 hours ago" do
      delivery_batch = create(:delivery_batch, :with_delivery_batch_items,
                                               batch_size: 25,
                                               processed_count: 20,
                                               max_batch_size: 100,
                                               count: 25,
                                               created_at: Date.today - 2.days)

      expect(DistributionSystem::BatchManager).not_to receive(:close_batch).with(delivery_batch.batch_id)
      DistributionSystem::BatchManager.close_stale_batches(delivery_batch.store.id, 180, 47)
    end
  end
end
