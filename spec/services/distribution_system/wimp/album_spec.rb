require "rails_helper"
RSpec.describe DistributionSystem::Believe::Album do
  let(:album) do
    build(:distribution_system_album,
          :single,
          :with_tracks,
          :with_artists,
          :with_legacy_cover_image,
          delivery_type: "metadata_only",
          takedown: true,
          uuid: "63b0f7506f9c01333b5e22000a9f151d",
          store_name: "Wimp",
          store_id: 39)
  end

  describe "#validate_xml" do
    it "is true" do
      expect(album.validate_xml).to be true
    end
  end
end
