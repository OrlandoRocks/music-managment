require "rails_helper"

describe DistributionSystem::Transcoder do
  let(:album)       { build(:distribution_system_album, :with_tracks, :with_booklet, store_name: "BelieveLiv", store_id: 75) }
  let(:bundle)      { build(:believe_bundle, skip_batching: true, album: album) }
  let(:transcoder)  { bundle.transcoder }
  let(:output_file) { "./tmp/output.flac" }
  let(:wav_track)   { "spec/distribution_fixtures/assets/track.wav" }
  let(:mp3_track)   { "spec/distribution_fixtures/assets/track.mp3" }
  let(:bad_sample_rate_mp3) { "spec/distribution_fixtures/assets/bad_sample_rate_track.mp3" }
  let(:bad_sample_rate_wav) { "spec/distribution_fixtures/assets/bad_sample_rate_track.wav" }


  before(:each) do
    File.new(output_file, "w") unless File.exists?(output_file)
  end

  context "given an mp3" do
    context "with the wrong sample rate" do
      it "tries to resample" do
        expect(transcoder).to receive(:resample_audio)
        transcoder.transcode(bad_sample_rate_mp3, output_file)
      end

      it "successfully resamples to 44.1 KHz" do
        expect(transcoder.get_sample_rate(bad_sample_rate_mp3)).to eq "48.0"
        transcoder.transcode(bad_sample_rate_mp3, output_file)
        expect(transcoder.get_sample_rate(output_file)).to eq "44.1"
      end
    end

    context "with the correct sample rate" do
      it "does not try to resample" do
        expect(transcoder).to_not receive(:resample_audio)
        transcoder.transcode(mp3_track, output_file)
      end
    end
  end

  context "given a wav file" do
    context "with the wrong sample rate" do
      it "tries to resample" do
        expect(transcoder).to receive(:resample_audio)
        transcoder.transcode(bad_sample_rate_wav, output_file)
      end

      it "successfully resamples to 44.1 KHz" do
        expect(transcoder.get_sample_rate(bad_sample_rate_wav)).to eq "48.0"
        transcoder.transcode(bad_sample_rate_wav, output_file)
        expect(transcoder.get_sample_rate(output_file)).to eq "44.1"
      end
    end

    context "with the correct sample rate" do
      it "does not try to resample" do
        expect(transcoder).to_not receive(:resample_audio)
        transcoder.transcode(wav_track, output_file)
      end
    end

    context "correcting the bit depth" do
      fixture_path = "#{Rails.root}/spec/distribution_fixtures/assets/"

      before do
        FileUtils.copy_file("#{fixture_path}/track.wav", "#{fixture_path}/track.wav.back")
      end

      it "converts the bit depth to the given bit" do
        transcoder.convert_bit_depth_to(wav_track, "24")
        expect(transcoder.get_bit_depth(wav_track)).to eq("24")
      end

      after do
        FileUtils.copy_file("#{fixture_path}/track.wav.back", "#{fixture_path}/track.wav")
        File.delete("#{Rails.root}/spec/distribution_fixtures/assets/track.wav.back")
      end
    end
  end
end
