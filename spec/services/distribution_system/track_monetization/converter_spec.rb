require "rails_helper"

describe DistributionSystem::TrackMonetization::Converter do
  describe "#convert" do
    it "returns an instance of a distribution system album" do
      album              = create(:album, :with_artwork)
      song               = create(:song, :with_upload, album: album)
      track_monetization = create(:track_monetization, song: song.reload)
      converted_album    = subject.convert(track_monetization)
      expect(converted_album.class.name).to eq "DistributionSystem::TrackMonetization::Album"
      expect(converted_album.tunecore_id).to eq album.id
      expect(converted_album.genre).to eq album.genres.first.id
      expect(converted_album.tracks.last.song_id).to eq song.id
    end
  end
end
