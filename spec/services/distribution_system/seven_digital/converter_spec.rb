require "rails_helper"

describe DistributionSystem::SevenDigital::Converter do
  let(:converter) { DistributionSystem::SevenDigital::Converter.new }

  let!(:tc_album) { create(:album, :finalized, :with_artwork, :with_uploaded_song, :with_salepoints, abbreviation: ["sd"]) }

  describe "#convert" do
    it "creates a new DistributionSystem::SevenDigital::Album" do
      converted_album = converter.convert(tc_album, tc_album.salepoints, "metadata_only")
      expect(converted_album).to be_a(DistributionSystem::SevenDigital::Album)
    end

    let(:converted_album) { converter.convert(tc_album, tc_album.salepoints) }

    it "sets the store_id" do
      expect(converted_album.store_id).not_to be(nil)
    end

    it "sets the album_type" do
      expect(["Album", "Single"].include?(converted_album.album_type)).to be true
    end

    it "sets the language code instead of the language description" do
      expect(converted_album.language_code).not_to eq(tc_album.language.description)
      expect(converted_album.language_code).to eq(tc_album.language_code)
    end

    it "adds tracks" do
      expect(converted_album.tracks.count).to eq(tc_album.songs.count)
    end

    it "adds artists with roles" do
      expect(converted_album.artists.first.roles).to match(["primary_artist"])
    end

    context "SevenDigital-specific track attributes" do
      let(:converted_album) { converter.convert(tc_album, tc_album.salepoints) }

      it "creates new SevenDigital tracks" do
        tracks_class_check = converted_album.tracks.all? do |t|
          t.is_a?( DistributionSystem::SevenDigital::Track)
        end
        expect(tracks_class_check).to be true
      end

      it "sets the label name" do
        label_test = converted_album.tracks.all? do | track |
          track.label_name == tc_album.label_name
        end

        expect(label_test).to be true
      end

      it "adds artists with roles" do
        song = tc_album.songs.first
        create(:creative, creativeable_id: song.id, creativeable_type: "Song", role: "primary_artist")
        converted_album = converter.convert(tc_album, tc_album.salepoints, "metadata_only")
        artists = converted_album.tracks.first.artists
        expect(artists.second.roles).to match(["primary_artist"])
      end
    end
  end
end
