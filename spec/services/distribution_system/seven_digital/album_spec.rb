require "rails_helper"

RSpec.describe DistributionSystem::SevenDigital::Album do
  describe "#to_xml" do

    let(:album) do
      YAML.load_file("spec/distribution_fixtures/converted_albums/seven_digital_basic.yml")
    end

    let(:fixture_xml) do
      Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/seven_digital_basic.xml"))
    end

    let(:sdconfig) do
      StoreDeliveryConfig.for("7Digital")
    end

    it "matches xml fixture for a regular delivery" do
      album.set_data_from_config(sdconfig)
      album.delivery_type = "metadata_only"
      allow(album).to receive(:get_uuid).and_return("903ef4b0da05013567c50bb8c855af5c")
      allow(album).to receive(:xml_timestamp).and_return("2018-01-12T20:31:59+00:00")
      # puts Diffy::Diff.new(fixture_xml.to_s, Nokogiri::XML(album.to_xml, nil, 'utf-8').to_s, :context => 2).diff
      expect(compare_xml(album.to_xml, fixture_xml)
      ).to eq true
    end

    it "generates valid DDEX v3.7 xml" do
      album.set_data_from_config(sdconfig)
      album.delivery_type = "metadata_only"
      expect(album.valid?).to be true
      expect(album.formatter.validation_errors.empty?).to be true
    end

    context "generates Seven Digital-approved xml for" do
      it "A full takedown of the above release." do
        takedown     = YAML.load_file("spec/distribution_fixtures/converted_albums/seven_digital_takedown.yml")
        takedown_xml = Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/seven_digital_takedown.xml"))

        takedown.set_data_from_config(sdconfig)
        takedown.delivery_type = "metadata_only"
        allow(takedown).to receive(:get_uuid).and_return("8e96fa50166e01367cf62f98abbeadd6")
        allow(takedown).to receive(:xml_timestamp).and_return("2018-03-30T17:34:43+00:00")
        # puts Diffy::Diff.new(takedown_xml.to_s, Nokogiri::XML(takedown.to_xml, nil, 'utf-8').to_s, :context => 2, ).diff
        expect(compare_xml(takedown.to_xml, takedown_xml)
        ).to eq true
        expect(takedown.formatter.validation_errors.empty?).to be true
      end

      it "A salepoint takedown of a release." do
        sp_takedown = YAML.load_file("spec/distribution_fixtures/converted_albums/seven_digital_salepoint_takedown.yml")
        sp_takedown_xml = Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/seven_digital_salepoint_takedown.xml"))

        sp_takedown.set_data_from_config(sdconfig)
        sp_takedown.delivery_type = "metadata_only"
        allow(sp_takedown).to receive(:get_uuid).and_return("a70967c03f4b013665e30092e96e47a0")
        allow(sp_takedown).to receive(:xml_timestamp).and_return("2018-05-21T17:38:09+00:00")
        sp_takedown.tracks.each {|track| allow(track).to receive(:show_display_name?).and_return(false)}
        # puts Diffy::Diff.new(sp_takedown_xml.to_s, Nokogiri::XML(sp_takedown.to_xml, nil, 'utf-8').to_s, :context => 2 ).diff
        expect(compare_xml(sp_takedown.to_xml, sp_takedown_xml)
        ).to eq true
        expect(sp_takedown.formatter.validation_errors.empty?).to be true
      end
    end
  end
end
