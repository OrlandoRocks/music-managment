require "rails_helper"

describe DistributionSystem::Believe::Distributor do
  context "for SevenDigital albums" do
    let(:date)     { "20171227155401" }
    let(:work_dir) { DELIVERY_CONFIG["work_dir"] }
    let(:album) do
      YAML.load_file("spec/distribution_fixtures/converted_albums/1340271_11233380_seven_digital.yml")
    end
    let(:delivery_config) {StoreDeliveryConfig.for(album.store_name)}

    let(:config) do
      StoreDeliveryConfig.for("7Digital")
    end

    let(:sftp_config) {
      {
        host:     delivery_config["hostname"],
        username: delivery_config["username"],
        password: delivery_config["password"],
        port:     delivery_config["port"]
      }
    }

    let(:shell) do
      DistributionSystem::Shell.new
    end

    let(:transcoder) do
      DistributionSystem::Transcoder.new({
        mp3_decoder:  "lame",
        alac_decoder: "alac",
        aac_decoder:  "faad",
        flac_encoder: "flac",
        flac_decoder: "flac",
        aac_encoder:  "faac",
        shell:        shell,
        tempdir:      DistributionSystem::TempDir.new
      })
    end

    let(:s3) do
      DistributionSystem::S3.new("asset_bucket")
    end

    let(:distribution) { FactoryBot.create(:distribution) }
    let(:bundle)       { DistributionSystem::SevenDigital::Bundle.new(
                            work_dir,
                            transcoder,
                            s3,
                            album,
                            config
                          )
                        }

    let(:dir_name) { FileUtils.mkdir_p("#{work_dir}/7Digital/#{album.upc}").first }

    let(:distributor)   {
      DistributionSystem::Believe::Distributor.new({
        bundle:       bundle
      })
    }

    describe "#distribute" do
      before(:each) do
        allow(bundle).to receive(:collect_files).and_return(true)
        bundle.timestamp = date
      end

      it "collects the bundle files" do
        expect(bundle).to receive(:collect_files)
        allow(distributor).to receive(:write_metadata)
        allow(distributor).to receive(:send_bundle)
        distributor.distribute(album, distribution)
      end

      it "writes the bundle metadata" do
        expect(bundle).to receive(:write_metadata)
        allow(distributor).to receive(:send_bundle)
        distributor.distribute(album, distribution)
      end

      it "sends to SevenDigital" do
        allow(bundle).to receive(:write_metadata)
        allow(distributor).to receive(:validate_bundle).and_return(true)

        sftp         = double("net_sftp").as_null_object
        interpreter  = DistributionSystem::Sftp::Interpreter.new(sftp)
        allow(Net::SFTP).to receive(:start).and_return(sftp)
        expect(DistributionSystem::Sftp::Interpreter).to receive(:new).and_return(interpreter)

        bundle_id    = bundle.album.upc
        remote_dir   = "7digital/#{bundle_id}_20171227155401"
        resource_dir = "#{remote_dir}/resources"

        expect(interpreter).to receive(:mkdir_p).ordered.with(remote_dir)
        expect(interpreter).to receive(:mkdir_p).ordered.with(resource_dir)

        artwork_file        = File.join(dir_name, "#{bundle_id}.jpg")
        remote_artwork_file = File.join(resource_dir, "#{bundle_id}.jpg")

        expect(interpreter).to receive(:upload).ordered.with(artwork_file, remote_artwork_file)


        bundle.album.tracks.each do |track|
          track_file        = File.join(dir_name, "#{bundle.album.upc}_01_#{track.number}.flac")
          remote_track_file = File.join(resource_dir, "#{bundle.album.upc}_01_#{track.number}.flac")
          expect(interpreter).to receive(:upload).with(track_file,remote_track_file)
        end

        metadata_filename        = File.join(dir_name, "#{bundle.album.upc}.xml")
        remote_metadata_filename = File.join(remote_dir, "#{bundle.album.upc}.xml")

        expect(interpreter).to receive(:upload).ordered.with(metadata_filename, remote_metadata_filename)

        expect(distributor).not_to receive(:send_complete_file)

        expect(interpreter).not_to receive(:upload).ordered.with("/tmp/delivery.complete", "#{remote_dir}/DELIVERY.COMPLETE.dat")

        distributor.distribute(album, distribution)
      end
    end
  end
end
