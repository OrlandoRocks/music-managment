require "rails_helper"

describe DistributionSystem::Believe::Album do
  context "when metadata_only" do
    let(:artist) { build(:distribution_system_artist, name: "Duran Duran") }
    let(:track) do
      build(:distribution_system_track,
            artists: [artist],
            duration: "PT99H01M01S",
            isrc: "TCAAA1800002",
            explicit_lyrics: false,
            title: "This Is a Song")
    end
    let(:album) do
      build(:distribution_system_album,
            artists: [artist],
            copyright_pline: "2018 Duran Duran",
            countries: ["AF", "AX"],
            delivery_type: "metadata_only",
            genre: 1,
            label_name: "Duran Duran",
            release_date: "2018-08-22",
            store_id: Store.find_by(abbrev: "ig").id,
            store_name: "Instagram",
            title: "The Golden Bowl",
            tracks: [track],
            upc: "859700000021",
            explicit_lyrics: false,
            uuid: "7d6dc3008877013675d20ef3efe01f4a")
    end

    let(:sdc) { StoreDeliveryConfig.for("Instagram")}

    before(:each) do
      track.album = album
      allow(album).to receive(:xml_timestamp).and_return("2018-08-22T20:25:52+00:00")
      allow(track).to receive(:formatted_duration).and_return("PT99H01M01S")
    end

    describe "#valid?" do
      it "returns true if valid" do
        expect(album.valid?).to be_truthy
      end
    end

    describe "#validate_xml" do
      it "returns true if the xml is valid" do
        expect(album.validate_xml).to be_truthy
      end
    end

    describe "#to_xml" do
      let(:fixture_xml) do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/instagram_metadata_only.xml"))
      end

      it "returns true if the xml is valid" do
        album.set_data_from_config(sdc)
        # diff = Diffy::Diff.new(fixture_xml.to_s, Nokogiri::XML(album.to_xml, nil, 'utf-8').to_s, context: 2)
        # puts JSON.parse(diff.diff)
        result = compare_xml(album.to_xml, fixture_xml)
        expect(result).to be_truthy
      end
    end
  end

  context "when full_delivery" do
    let(:artist) { build(:distribution_system_artist, name: "Arturo Roque") }
    let(:track) do
      build(:distribution_system_track,
            artists: [artist],
            audio_file: "859726842551_01_1.flac",
            duration: "PT00H03M19S",
            isrc: "TCADQ1855091",
            explicit_lyrics: false,
            title: "Problemas Con El Amor")
    end
    let(:album) do
      build(:distribution_system_album,
            album_type: "Single",
            artists: [artist],
            artwork_file: double.as_null_object,
            copyright_pline: "2018 Independiente",
            countries: ["AF", "AX"],
            delivery_type: "full_delivery",
            genre: 17,
            label_name: "Independiente",
            release_date: "2018-05-21",
            store_id: Store.find_by(abbrev: "ig").id,
            store_name: "Instagram",
            title: "Problemas Con El Amor",
            tracks: [track],
            upc: "859726842551",
            explicit_lyrics: false,
            uuid: "e5307f40832d0136c92c0242ac110003")
    end

    let(:sdc) { StoreDeliveryConfig.for("Instagram")}

    before(:each) do
      track.album = album
      allow(album).to receive(:xml_timestamp).and_return("2018-08-15T21:56:28-05:00")
      allow(track).to receive(:formatted_duration).and_return("PT00H03M19S")
      allow(Digest::MD5).to receive(:file).and_return("e538d466bc812b33786b00882bff6950")
    end

    describe "#valid?" do
      it "returns true if valid" do
        expect(album.valid?).to be_truthy
      end
    end

    describe "#validate_xml" do
      it "returns true if the xml is valid" do
        expect(album.validate_xml).to be_truthy
      end
    end

    describe "#to_xml" do
      let(:fixture_xml) do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/instagram_full_delivery.xml"))
      end

      it "returns true if the xml is valid" do
        album.set_data_from_config(sdc)
        # diff = Diffy::Diff.new(fixture_xml.to_s, Nokogiri::XML(album.to_xml, nil, 'utf-8').to_s, context: 2)
        # puts JSON.parse(diff.diff)
        result = compare_xml(album.to_xml, fixture_xml)
        expect(result).to be_truthy
      end
    end
  end

  context "when takedown" do
    let(:artist) { build(:distribution_system_artist, name: "Troy Denkinger") }
    let(:featured) { build(:distribution_system_artist, name: "TD", roles: ["featuring"])}
    let(:track) do
      build(:distribution_system_track,
            artists: [artist, featured],
            duration: "PT00H00M22S",
            isrc: "TCAAA1800037",
            explicit_lyrics: true,
            title: "Velvet Raindrops")
    end
    let(:album) do
      build(:distribution_system_album,
            album_type: "Single",
            artists: [artist, featured],
            copyright_pline: "2018 Troy Denkinger",
            countries: ["US"],
            delivery_type: "metadata_only",
            genre: 12,
            label_name: "Troy Denkinger",
            release_date: "2018-08-30",
            store_id: Store.find_by(abbrev: "ig").id,
            store_name: "Instagram",
            takedown: true,
            title: "Velvet Raindrops",
            tracks: [track],
            upc: "859700000397",
            explicit_lyrics: true,
            uuid: "3bb5b5608ea10136c93619c99f2f6049")
    end

    let(:sdc) { StoreDeliveryConfig.for("Instagram")}

    before(:each) do
      track.album = album
      allow(album).to receive(:xml_timestamp).and_return("2018-08-30T16:39:48+00:00")
      allow(track).to receive(:formatted_duration).and_return("PT00H00M22S")
    end

    describe "#valid?" do
      it "returns true if valid" do
        expect(album.valid?).to be_truthy
      end
    end

    describe "#validate_xml" do
      it "returns true if the xml is valid" do
        expect(album.validate_xml).to be_truthy
      end
    end

    describe "#to_xml" do
      let(:fixture_xml) do
        Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/instagram_takedown.xml"))
      end

      it "returns true if the xml is valid" do
        album.set_data_from_config(sdc)
        # diff = Diffy::Diff.new(fixture_xml.to_s, Nokogiri::XML(album.to_xml, nil, 'utf-8').to_s, context: 2)
        # puts JSON.parse(diff.diff)
        result = compare_xml(album.to_xml, fixture_xml)
        expect(result).to be_truthy
      end
    end
  end
end
