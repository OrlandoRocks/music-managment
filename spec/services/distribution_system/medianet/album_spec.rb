require "rails_helper"
RSpec.describe DistributionSystem::Medianet::Album do
  before(:each) do
    allow(Time).to receive(:now).and_return(double({iso8601: "2016-01-01T12:00:00-05:00", getlocal: "2016-01-01T12:00:00-05:00"}))
  end
  describe "#to_xml (for Medianet store only)" do

    let(:album) do
      YAML.load_file("spec/distribution_fixtures/converted_albums/874215_4290326_Medianet.yml")
    end

    let(:fixture_xml) do
      Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/874215_medianet.xml"))
    end

    before(:each) do
      expect(album).to receive(:get_duration).and_return("02:14")
      album.tracks.each do |track|
        expect(track).to receive(:get_duration).and_return("02:14")
      end
      album.supported_countries = ['BE', 'BR', 'CN', 'DE', 'ES',
                                   'FR', 'IE', 'IN', 'JP', 'KR',
                                   'MX', 'MY', 'SG', 'TW', 'US',
                                   'AU', 'CA', 'GB', 'ZA', 'IT',
                                   'NL', 'NZ']
      album.stores = ["Medianet"]
      album.corp_code = "TUN" # set in bundle_helper
    end

    it "matches xml fixture" do
      expect(
        EquivalentXml.equivalent?(
          Nokogiri::XML(album.to_xml, nil, 'utf-8'),
          fixture_xml,
          :element_order => false,
          :normalize_whitespace => true
        )
      ).to eq true
    end
  end

  describe "#to_xml (for CUR store only)" do

    let(:album) do
      YAML.load_file("spec/distribution_fixtures/converted_albums/1468117_cur.yml")
    end

    let(:fixture_xml) do
      Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/1468117_cur.xml"))
    end

    before(:each) do
      expect(album).to receive(:get_duration).and_return("02:14")
      album.tracks.each do |track|
        expect(track).to receive(:get_duration).and_return("02:14")
      end

      album.supported_countries = ["US"]
      album.corp_code = "TUN"
      album.stores = ["Cur"]
    end

    it "matches xml fixture" do
      expect(
        EquivalentXml.equivalent?(
          Nokogiri::XML(album.to_xml, nil, 'utf-8'),
          fixture_xml,
          :element_order => false,
          :normalize_whitespace => true
        )
      ).to eq true
    end
  end

  describe "#to_xml (for Medianet and Cur Store)" do

    let(:album) do
      YAML.load_file("spec/distribution_fixtures/converted_albums/1468117_cur.yml")
    end

    let(:fixture_xml) do
      Nokogiri::XML(File.open("spec/distribution_fixtures/metadata/1468117_cur_and_medianet.xml"))
    end

    before(:each) do
      expect(album).to receive(:get_duration).and_return("02:14")
      album.tracks.each do |track|
        expect(track).to receive(:get_duration).and_return("02:14")
      end
      album.supported_countries = ["US"]
      album.stores = ["Medianet", "Cur"]
      album.corp_code = "TUN" # set in bundle_helper
    end

    it "matches xml fixture" do
      expect(
        EquivalentXml.equivalent?(
          Nokogiri::XML(album.to_xml, nil, 'utf-8'),
          fixture_xml,
          :element_order => false,
          :normalize_whitespace => true
        )
      ).to eq true
    end
  end
end
