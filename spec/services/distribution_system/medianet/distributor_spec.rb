require "rails_helper"

describe DistributionSystem::Medianet::Distributor do
  let(:work_dir) { DELIVERY_CONFIG["work_dir"] }
  let(:date) {Time.now}
  let(:album) do
    YAML.load_file("spec/distribution_fixtures/converted_albums/874215_4290326_Medianet.yml")
  end
  let(:delivery_config) {StoreDeliveryConfig.for(album.store_name)}

  let(:config) do
    YAML.load_file(Rails.root.join("config", "delivery.yml"))[Rails.env]
  end

  let(:sftp_config) {
    {
      host:     delivery_config["hostname"],
      username: delivery_config["username"],
      password: delivery_config["password"],
      port:     delivery_config["port"]
    }
  }

  let(:shell) do
    DistributionSystem::Shell.new
  end

  let(:transcoder) do
    DistributionSystem::Transcoder.new({
      mp3_decoder:  "lame",
      alac_decoder: "alac",
      aac_decoder:  "faad",
      flac_encoder: "flac",
      flac_decoder: "flac",
      aac_encoder:  "faac",
      shell:        shell,
      tempdir:      DistributionSystem::TempDir.new
    })
  end

  let(:s3) do
    DistributionSystem::S3.new("asset_bucket")
  end

  let(:distribution) { FactoryBot.create(:distribution) }
  let(:bundle)       { DistributionSystem::Medianet::Bundle.new(
                          work_dir,
                          transcoder,
                          s3,
                          album,
                          delivery_config
                        )
                      }

  let(:dir_name) { FileUtils.mkdir_p("#{work_dir}/Medianet/#{album.upc}").first }

  let(:distributor)   {
    DistributionSystem::Medianet::Distributor.new({
      bundle:       bundle
    })
  }

  describe "#distribute" do
    before(:each) do
      allow(bundle).to receive(:collect_files).and_return(true)
    end
    it "collects the bundle files" do
      expect(bundle).to receive(:collect_files)
      allow(distributor).to receive(:write_metadata)
      allow(distributor).to receive(:send_bundle)
      distributor.distribute(album, distribution)
    end

    it "writes the bundle metadata" do
      expect(bundle).to receive(:write_metadata)
      allow(distributor).to receive(:send_bundle)
      distributor.distribute(album, distribution)
    end

    it "sends to Medianet" do
      allow(bundle).to receive(:write_metadata)
      allow(distributor).to receive(:validate_bundle).and_return(true)

      bundle_id    = bundle.album.upc
      remote_dir   = "#{delivery_config.remote_dir}/#{bundle_id}"
      resource_dir = remote_dir

      allow_any_instance_of(DistributionSystem::SftpCli).to receive(:start_processing).and_return(true)

      expect_any_instance_of(DistributionSystem::SftpCli).to receive(:mkdir_p).with(remote_dir)
      artwork_file        = File.join(dir_name, "#{bundle_id}.jpg")
      remote_artwork_file = File.join(resource_dir, "#{bundle_id}.jpg")

      expect_any_instance_of(DistributionSystem::SftpCli).to receive(:upload).with(artwork_file, remote_artwork_file)


      bundle.album.tracks.each do |track|
        track_file        = File.join(dir_name, "#{bundle.album.upc}_01_#{track.number}.flac")
        remote_track_file = File.join(resource_dir, "#{bundle.album.upc}_01_#{track.number}_FLAC.flac")
        expect_any_instance_of(DistributionSystem::SftpCli).to receive(:upload).with(track_file,remote_track_file)
      end

      metadata_filename        = File.join(dir_name, "#{bundle.album.upc}.xml")
      remote_metadata_filename = File.join(remote_dir, "#{bundle.album.upc}.xml")

      expect_any_instance_of(DistributionSystem::SftpCli).to receive(:upload).with(metadata_filename, remote_metadata_filename)

      expect_any_instance_of(DistributionSystem::SftpCli).to receive(:upload).with("/tmp/delivery.complete", "#{remote_dir}/delivery.complete")

      distributor.distribute(album, distribution)
    end
  end
end
