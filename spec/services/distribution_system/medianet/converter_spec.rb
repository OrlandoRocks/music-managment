require "rails_helper"

describe DistributionSystem::Medianet::Converter do
  describe "#add_track" do
    context "on track creation" do
      context "when duration_in_seconds is nil" do
        before do
          allow_any_instance_of(Song).to receive(:duration).and_return(120)
          album = create(:album, :with_uploaded_songs_and_artwork)
          store = Store.find_by(short_name: 'Medianet')
          album.add_stores([store])
          salepoint = album.salepoints.first
          petri_bundle = create(:petri_bundle, album_id: album.id)
          distribution = create(:distribution, petri_bundle_id: petri_bundle.id, converter_class: "DistributionSystem::Medianet::Converter", state: "new", delivery_type: "metadata_only")
          distribution.salepoints << salepoint
          @converted_album = distribution.convert(:metadata_only)
        end

        it "should pass the duration to Track" do
          expect(@converted_album.tracks[0].duration).to eq(120)
        end
      end

      context "when duration_in seconds is populated on the songs table" do
        before do
          allow_any_instance_of(Song).to receive(:duration).and_return(120000)
          album = create(:album, :with_uploaded_songs_and_artwork)
          album.songs.first.update(duration_in_seconds: 1200)
          store = Store.find_by(short_name: 'Medianet')
          album.add_stores([store])
          salepoint = album.salepoints.first
          petri_bundle = create(:petri_bundle, album_id: album.id)
          distribution = create(:distribution, petri_bundle_id: petri_bundle.id, converter_class: "DistributionSystem::Medianet::Converter", state: "new", delivery_type: "metadata_only")
          distribution.salepoints << salepoint
          @converted_album = distribution.convert(:metadata_only)
        end

        it "should take duration from songs table" do
          expect(@converted_album.tracks[0].duration).to eq(1200)
        end
      end
    end
  end
end
