require "rails_helper"

describe PetriUpload::Gen2 do
  let(:distro)      {FactoryBot.create(:distribution, :with_salepoint)}
  let(:converted_album) {double}
  let(:queue)       {double(message_id: 1)}
  let(:queues)      { double(named: queue) }
  let(:sqs)         {double("sqs", queues: queues)}
  let(:message_yml) {
    "---\n:message_ver: YAML.v1\n:bucket: petri-messages.tunecore.com.test\n:key: #{distro.id}\n:retry_count: 0\n:distribution_type: Album\n:provider: TuneCoreUS\n"
  }

  describe ".upload" do
    before(:each) do
      expect(queue).to receive(:send_message).with(message_yml).and_return(queue)
      allow(queue).to receive(:id).and_return("1234")
      stub_const("SQS_CLIENT", sqs)
    end

    it "should set the correct distribution_class in the message" do
      allow_any_instance_of(PetriUpload::Gen2).to receive(:get_queue).and_return(queue)
      allow_any_instance_of(PetriUpload::Gen2).to receive(:message_body).and_return(message_yml)
      PetriUpload::Gen2.upload(distro, converted_album, queue)
    end

    context "when queue_name param is passed" do
      it "should populate the correct queue name" do
        expect(queues).to receive(:named).with("test-queue").and_return(queue)
        PetriUpload::Gen2.upload(distro, converted_album, "test-queue")
      end
    end

    context "when no queue_name param is passed" do
      it "should populate the correct queue name" do
        expect(queues).to receive(:named).with(PETRI_QUEUE).and_return(queue)
        PetriUpload::Gen2.upload(distro, converted_album, nil)
      end
    end

    context "when no queue_name param and no constant defined" do
      it "should populate the correct queue name" do
        stub_const("PETRI_QUEUE", nil)
        expect(queues).to receive(:named).with("petri-#{distro.store.short_name}")
        PetriUpload::Gen2.upload(distro, converted_album, nil)
      end
    end
  end
end
