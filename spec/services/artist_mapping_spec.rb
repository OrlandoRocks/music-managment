require "rails_helper"

describe ArtistMapping do
  describe ".create_or_update_external_service_ids" do
    before(:each) do
      @person   = create(:person)
      @artist   = create(:artist)
      album    = create(:album)
      create(:creative, artist_id: @artist.id, creativeable_id: album.id, creativeable_type: 'Album').update(person_id: @person.id)
    end

    let(:artwork) { ExternalServiceIdArtwork.new }

    let(:with_artwork) {
      {
        person_id: @person.id,
        artist_id: @artist.id,
        service_name: "apple",
        artwork: artwork
      }
    }

    let(:without_artwork) {
      {
        person_id: @person.id,
        artist_id: @artist.id,
        service_name: "apple"
      }
    }

    let(:with_identifier) {
      {
        person_id: @person.id,
        artist_id: @artist.id,
        service_name: "apple",
        identifier: "12345"
      }
    }

    before(:each) do
      allow(ExternalServiceIdArtwork).to receive(:create).and_return(true)
    end
    context "with artwork" do
      it "creatives the External Service Ids and artworks" do
        expect(ExternalServiceIdArtwork).to receive(:create).with({asset: artwork})
        ArtistMapping.create_or_update_external_service_ids(with_artwork)
        external_service_ids = ExternalServiceId.artist_ids_for(@person.id, @artist.id, "apple")
        external_service_ids.each do |es|
          expect(es.service_name).to eq("apple")
          expect(es.state).to eq("processing")
        end
      end
    end

    context "without artwork" do
      it "creatives the External Service Ids, does not create artworks" do
        expect(ExternalServiceIdArtwork).to receive(:create).with({asset: artwork})
        ArtistMapping.create_or_update_external_service_ids(with_artwork)
        external_service_ids = ExternalServiceId.artist_ids_for(@person.id, @artist.id, "apple")
        external_service_ids.each do |es|
          expect(es.service_name).to eq("apple")
          expect(es.state).to eq("processing")
        end
      end
    end

    context "with identifier" do
      it "creaitves External Service Ids with the identifier" do
        ArtistMapping.create_or_update_external_service_ids(with_identifier)
         external_service_ids = ExternalServiceId.artist_ids_for(@person.id, @artist.id, "apple")
        external_service_ids.each do |es|
          expect(es.service_name).to eq("apple")
          expect(es.state).to eq("processing")
          expect(es.identifier).to eq("12345")
        end
      end
    end

    context "with existing External Service Ids" do
      it "updates the relevant External Service Ids" do
        ExternalServiceId.create(identifier: "6789", linkable_type: "Creative", linkable_id: Creative.last.id, service_name: "apple")
        ArtistMapping.create_or_update_external_service_ids(with_identifier)
        external_service_ids = ExternalServiceId.artist_ids_for(@person.id, @artist.id, "apple")
        external_service_ids.each do |es|
          expect(es.service_name).to eq("apple")
          expect(es.state).to eq("processing")
          expect(es.identifier).to eq("12345")
        end
      end
    end
  end
end
