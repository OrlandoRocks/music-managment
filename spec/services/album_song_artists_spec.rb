require "rails_helper"

describe AlbumSongArtists do
  describe ".song_contributors" do
    before :each do
      @album = create(:album)
      5.times { create(:song, album: @album) }
      cello    = SongRole.find(15)
      congas   = SongRole.find(19)
      ukulele  = SongRole.find(61)
      main_guy = create(:artist)
      cameo    = create(:artist)

      @album.reload

      @album.songs.each do |song|
        song_creative = create(:creative, creativeable: song, artist: main_guy)
        CreativeSongRole.create(creative_id: song_creative.id, song_role_id: cello.id, song_id: song.id)
      end

      @cellist = @album.songs.first.creatives.first

      first_song = @album.songs.first
      @conga_player = create(:creative, creativeable: first_song, artist: cameo)
      CreativeSongRole.create(creative_id: @conga_player.id, song_role_id: congas.id, song_id: first_song.id)

      last_song = @album.songs.last
      @ukulele_player = create(:creative, creativeable: last_song, artist: main_guy)
      CreativeSongRole.create(creative_id: @ukulele_player.id, song_role_id: ukulele.id, song_id: last_song.id)
    end

    it "returns all song creatives who appear on every song on the album" do
      album_contributors = AlbumSongArtists.song_contributors(@album)
      expect(album_contributors.count).to eq 1
      expect(album_contributors.first.role_types).to include("cello")
      expect(album_contributors.map(&:role_types)).not_to include("congas")
    end

    it "returns only song creatives who have the same song role on every song on the album" do
      album_contributors = AlbumSongArtists.song_contributors(@album)
      expect(album_contributors.first.artist_name).to eq(@cellist.artist.name)
      expect(album_contributors.first.role_types).not_to eq("ukulele")
    end

    it "does not return creatives who have different roles on every song on the album" do
      additional_roles = SongRole.where("id not in (19, 19, 61)").limit(4)
      additional_role_types = additional_roles.map(&:role_type)

      additional_roles.each_with_index do |role, index|
        player = create(:creative, creativeable: @album.songs[index], artist: @ukulele_player.artist)
        CreativeSongRole.create(creative_id: player.id, song_role_id: role.id, song_id: @album.songs[index].id)
      end

      album_contributors = AlbumSongArtists.song_contributors(@album.reload)
      expect(album_contributors.count).to eq 1
      expect(album_contributors.map(&:role_types).flatten).not_to include(additional_role_types)
    end
  end
end
