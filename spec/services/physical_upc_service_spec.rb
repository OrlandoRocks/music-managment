require 'rails_helper'

describe PhysicalUpcService do
  it "should raise NoPrefixes error if no prefixes are provided" do
    expect { PhysicalUpcService.new(nil) }.to raise_error(PhysicalUpcService::NoPrefixes)
    expect { PhysicalUpcService.new([]) }.to raise_error(PhysicalUpcService::NoPrefixes)
  end
  describe "#reserve_physical_upc" do
    subject do
      PhysicalUpcService.new(["1111","22222"])
    end

    context "given all sequences are used up" do
      before do
        base1 = "11119999999"
        base2 = "22222999999"
        [base1, base2].each do |base|
          Upc.create!(
            upcable:  create(:album),
            number:   subject.checksummed(base),
            upc_type: "physical"
          )
        end
      end
      it "should raise a PhysicalUpcLimit error" do
        album = create(:album)
        expect { subject.reserve_physical_upc(album) }.to raise_error(Upc::PhysicalUpcLimit)
      end
    end

    context "given an existing album with a physical UPC" do
      before do
        base = "1111567890"
        @random_end = (0..8).to_a.sample
        upc_value = base + @random_end.to_s
        upc_number = subject.checksummed(upc_value)
        Upc.create!(
          upcable: create(:album),
          number: upc_number,
          upc_type: "physical"
        )
      end
      it "should reserve a physical upc" do
        album = create(:album)

        upc = subject.reserve_physical_upc(album)

        expect(upc).to be_persisted
        expect(upc.number).to start_with("1111")
        expect(upc.number[0..-2]).to end_with("0#{@random_end+1}")
        expect(upc.upc_type).to eq("physical")
        expect(upc.upcable).to eq(album)
      end
    end

    context "given total upc numbers has exceeded the threshold" do
      before do
        base1 = "11119999999"
        base2 = "22222984999"
        [base1, base2].each do |base|
          Upc.create!(
            upcable:  create(:album),
            number:   subject.checksummed(base),
            upc_type: "physical"
          )
        end
      end

      it "should notify airbrake" do
        expect(Airbrake).to receive(:notify)
        album = create(:album)

        upc = subject.reserve_physical_upc(album)
        expect(upc).to be_persisted
      end
    end

    context "given an existing album with the maximum upc serial" do
      before do
        base = "11119999999"
        upc_number = base + subject.checksum(base).to_s
        Upc.create!(
          upcable: create(:album),
          number: upc_number,
          upc_type: "physical"
        )
      end
      it "rolls over to the other sequence" do
        expect(Airbrake).not_to receive(:notify)
        album = create(:album)

        upc = subject.reserve_physical_upc(album)

        expect(upc).to be_persisted
        expect(upc.number).to start_with("22222")
        expect(upc.number[0..-2]).to end_with("0")
        expect(upc.upc_type).to eq("physical")
        expect(upc.upcable).to eq(album)
      end
    end
  end
end
