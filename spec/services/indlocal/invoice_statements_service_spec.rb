require 'rails_helper'

describe Indlocal::InvoiceStatementsService do
  let!(:intrastate_gst_config) {GstConfig.find_by(igst:nil)}
  let!(:interstate_gst_config) {GstConfig.find_by(cgst:nil)}
  let!(:intrastate_state) {CountryState.find_by(gst_config: intrastate_gst_config)}
  let!(:interstate_state) {CountryState.find_by(gst_config: interstate_gst_config)}
  let!(:admin) {create(:person, :admin)}
  let!(:person_1) {create(:person, country: 'IN', state: intrastate_state.name)}
  let!(:person_2) {create(:person, country: 'IN', state: interstate_state.name)}
  let!(:person_3) do 
    # we have to do this update business because we hardcode country website based on country selection
    person = create(:person, country: 'IN', state: nil)
    person.update(country: 'US')
    person.reload
  end
  let!(:person_4) do 
    person = create(:person, country: 'IN', state: nil)
    person.update(country: 'US')
    person.reload
  end
  let!(:invoice_1) do create(:invoice, person: person_1, final_settlement_amount_cents: 10000,
                             settled_at: Date.current - 2.days, created_at: Date.current - 2.days,
                             foreign_exchange_pegged_rate_id: ForeignExchangePeggedRate.first.id,
                             gst_config: intrastate_gst_config)
  end
  let!(:invoice_2) do create(:invoice, person: person_2, final_settlement_amount_cents: 10000,
                             settled_at: Date.current - 3.days, created_at: Date.current - 3.days,
                             foreign_exchange_pegged_rate_id: ForeignExchangePeggedRate.first.id,
                             gst_config: interstate_gst_config)
  end
  let!(:invoice_3) do create(:invoice, person: person_3, final_settlement_amount_cents: 10000,
                             settled_at: Date.current - 4.days, created_at: Date.current - 4.days,
                             foreign_exchange_pegged_rate_id: ForeignExchangePeggedRate.first.id,
                             gst_config: nil)
  end
  let!(:invoice_4) do create(:invoice, person: person_4, final_settlement_amount_cents: 10000,
                             settled_at: Date.current - 5.days, created_at: Date.current - 5.days,
                             foreign_exchange_pegged_rate_id: ForeignExchangePeggedRate.first.id,
                             gst_config: nil)
  end
  let!(:balance_adjustment_1) do create(:balance_adjustment, person: person_1, posted_by_id: admin.id,
                                        posted_by_name: admin.name, created_at: Date.current - 1.day)
  end
  let!(:balance_adjustment_2) do create(:balance_adjustment, person: person_2, posted_by_id: admin.id,
                                        posted_by_name: admin.name, category: 'Refund - Other',
                                        created_at: Date.current - 3.day)
  end
  let!(:balance_adjustment_3) do create(:balance_adjustment, person: person_3, posted_by_id: admin.id,
                                        posted_by_name: admin.name, category: 'Refund - Other',
                                        created_at: Date.current - 5.day)
  end
  let!(:balance_adjustment_4) do create(:balance_adjustment, person: person_4, posted_by_id: admin.id,
                                        posted_by_name: admin.name, category: 'Refund - Other',
                                        created_at: Date.current - 7.day)
  end
  let!(:cc_1) {create(:stored_credit_card, :payos_card, person_id: person_1.id)}
  let!(:cc_2) {create(:stored_credit_card, :payos_card, person_id: person_2.id)}
  let!(:cc_3) {create(:stored_credit_card, :payos_card, person_id: person_3.id)}
  let!(:cc_4) {create(:stored_credit_card, :payos_card, person_id: person_4.id)}
  let!(:payments_os_transaction_1) do create(:payments_os_transaction, :successful_refund, person: person_1,
                                             stored_credit_card: cc_1, invoice: invoice_1, action: "refund",
                                             created_at: Date.current - 2.day, updated_at: Date.current - 2.day)
  end
  let!(:payments_os_transaction_2) do create(:payments_os_transaction, :successful_refund, person: person_2,
                                             stored_credit_card: cc_2, invoice: invoice_2, action: "refund",
                                             created_at: Date.current - 4.day, updated_at: Date.current - 4.day)
  end
  let!(:payments_os_transaction_3) do create(:payments_os_transaction, :successful_refund, person: person_3,
                                             stored_credit_card: cc_3, invoice: invoice_3, action: "refund",
                                             created_at: Date.current - 6.day, updated_at: Date.current - 6.day)
  end
  let!(:payments_os_transaction_4) do create(:payments_os_transaction, :successful_refund, person: person_4,
                                             stored_credit_card: cc_4, invoice: invoice_4, action: "refund",
                                             created_at: Date.current - 8.day, updated_at: Date.current - 8.day)
  end
  let!(:payment_payments_os_transaction_1) do create(:payments_os_transaction, :successful_refund, person: person_1,
                                             stored_credit_card: cc_1, invoice: invoice_1, action: "sale",
                                             created_at: Date.current - 2.day, updated_at: Date.current - 2.day)
  end
  let!(:payment_payments_os_transaction_2) do create(:payments_os_transaction, :successful_refund, person: person_2,
                                             stored_credit_card: cc_2, invoice: invoice_2, action: "sale",
                                             created_at: Date.current - 4.day, updated_at: Date.current - 4.day)
  end
  let!(:payment_payments_os_transaction_3) do create(:payments_os_transaction, :successful_refund, person: person_3,
                                             stored_credit_card: cc_3, invoice: invoice_3, action: "sale",
                                             created_at: Date.current - 6.day, updated_at: Date.current - 6.day)
  end
  let!(:payment_payments_os_transaction_4) do create(:payments_os_transaction, :successful_refund, person: person_4,
                                             stored_credit_card: cc_4, invoice: invoice_4, action: "sale",
                                             created_at: Date.current - 8.day, updated_at: Date.current - 8.day)
  end
  let!(:service) {Indlocal::InvoiceStatementsService.new(Date.current)}

  describe "#purchase_query" do
    it "returns query with custom attributes" do
      expected_result = [
          [invoice_4.id, person_4.id, person_4.state],
          [invoice_3.id, person_3.id, person_3.state],
          [invoice_2.id, person_2.id, person_2.state],
          [invoice_1.id, person_1.id, person_1.state]
      ]
      actual_result = service.purchase_query.map do |item|
        [item.tunecore_invoice_id, item.person_id, item.state]
      end
      expect(expected_result).to eq(actual_result)
    end
  end
  describe "#balance_adjustment_query" do
    it "returns query with custom attributes" do
      expected_result = [
          [balance_adjustment_4.id, (balance_adjustment_4.credit_amount - balance_adjustment_4.debit_amount),
           person_4.id],
          [balance_adjustment_3.id, (balance_adjustment_3.credit_amount - balance_adjustment_3.debit_amount),
           person_3.id],
          [balance_adjustment_2.id, (balance_adjustment_2.credit_amount - balance_adjustment_2.debit_amount),
           person_2.id],
          [balance_adjustment_1.id, (balance_adjustment_1.credit_amount - balance_adjustment_1.debit_amount),
           person_1.id]
      ]
      actual_result = service.balance_adjustment_query.map do |item|
        [item.credit_note_id, item.usd_amount_of_credit, item.person_id]
      end
      expect(expected_result).to eq(actual_result)
    end
  end
  describe "#payments_os_transaction_query" do
    it "returns query with custom attributes" do
      expected_result = [
          [payments_os_transaction_4.id, payments_os_transaction_4.amount / 100, person_4.id],
          [payments_os_transaction_3.id, payments_os_transaction_3.amount / 100, person_3.id],
          [payments_os_transaction_2.id, payments_os_transaction_2.amount / 100, person_2.id],
          [payments_os_transaction_1.id, payments_os_transaction_1.amount / 100, person_1.id]
      ]
      actual_result = service.payments_os_transaction_query.map do |item|
        [item.credit_note_id, item.usd_amount_of_credit, item.person_id]
      end
      expect(expected_result).to eq(actual_result)
    end
  end
  describe "#get_purchase_csv_array" do
    it "creates an array of arrays" do
      all_arrays = service.get_purchase_csv_array.all? {|item| item.is_a?(Array)}
      expect(all_arrays).to be(true)
    end
  end
  describe "#get_credit_note_csv_array" do
    it "creates an array of arrays" do
      all_arrays = service.get_credit_note_csv_array.all? {|item| item.is_a?(Array)}
      expect(all_arrays).to be(true)
    end
    it "combines balance adjustments and payments os transactions" do
      expect(service.get_credit_note_csv_array.length).to be(8)
    end
    it "sorts the collection of balance adjustments and payments os transactions" do
      expected_id_order = [payments_os_transaction_4.id, balance_adjustment_4.id,
                           payments_os_transaction_3.id, balance_adjustment_3.id,
                           payments_os_transaction_2.id, balance_adjustment_2.id,
                           payments_os_transaction_1.id, balance_adjustment_1.id]
      actual_id_order = service.get_credit_note_csv_array.map {|credit_note|  credit_note.first }
      expect(expected_id_order).to eq(actual_id_order)
    end
  end
  describe "#send_invoice_statements_email" do
    it "calls S3StreamingService#write_and_upload twice" do
      expect(S3StreamingService).to receive(:write_and_upload)
      expect(S3StreamingService).to receive(:write_and_upload)
      service.send_invoice_statements_email
    end
    it "calls IndlocalMailer#monthly_invoice_statements" do
      allow(IndlocalMailer).to receive(:monthly_invoice_statements).and_return(double({deliver:true}))
      expect(IndlocalMailer).to receive(:monthly_invoice_statements)
      service.send_invoice_statements_email
    end
  end
end
