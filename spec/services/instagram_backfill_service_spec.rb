require "rails_helper"

describe InstagramBackfillService do
  let!(:fully_monetized) { alb = create(:album, :paid, :approved, :with_songs, number_of_songs: 2)
                           full_petri = create(:petri_bundle, album: alb)
                           full_distro = create(:distribution, :delivered_to_spotify, petri_bundle: full_petri)
                           alb.songs.each do |song|
                             tm = create(:track_monetization, :eligible, song: song)
                             tm.update(state: "delivered")
                           end
                           alb
                         }

  let!(:part_monetized)  { alb = create(:album, :paid, :approved, :with_songs, number_of_songs: 3)
                           part_petri = create(:petri_bundle, album: alb)
                           part_distro = create(:distribution, :delivered_to_spotify, petri_bundle: part_petri)
                           alb.songs.first do |song|
                             tm = create(:track_monetization, :eligible, song: song)
                             tm.update(state: "delivered")
                           end
                           alb
                         }

  let!(:none_monetized)  { alb = create(:album, :paid, :approved, :with_songs, number_of_songs: 3)
                           none_petri = create(:petri_bundle, album: alb)
                           none_distro = create(:distribution, :delivered_to_spotify, petri_bundle: none_petri)
                           alb
                         }

  let!(:part_delivered)  { alb = create(:album, :paid, :approved, :with_songs, number_of_songs: 3)
                           part_petri = create(:petri_bundle, album: alb)
                           part_distro = create(:distribution, :delivered_to_spotify, petri_bundle: part_petri)
                           create(:track_monetization, :eligible, song: alb.songs.first, state: "delivered")
                           create(:track_monetization, :eligible, song: alb.songs[1], state: "delivered")
                           create(:track_monetization, :eligible, song: alb.songs.last)
                           alb
                         }

  let!(:blocked)         { alb = create(:album, :paid, :approved, :with_songs, number_of_songs: 3)
                           blocked_petri = create(:petri_bundle, album: alb)
                           blocked_distro = create(:distribution, :delivered_to_spotify, petri_bundle: blocked_petri)
                           create(:track_monetization, :eligible, song: alb.songs.last, state: "delivered")
                           create(:track_monetization, :eligible, song: alb.songs.first, state: "blocked")
                           alb
                         }

  let!(:taken_down)      { alb = create(:album, :paid, :approved, :with_songs, number_of_songs: 3)
                           takedown_petri = create(:petri_bundle, album: alb)
                           takedown_distro = create(:distribution, :delivered_to_spotify, petri_bundle: takedown_petri)
                           alb.songs.each do |song|
                             create(:track_monetization, :taken_down, song: song)
                           end
                           alb
                         }

  let!(:svc)             { InstagramBackfillService.new }

  let!(:eligible_albums) { svc.collect_eligible_albums }

  describe ".eligible_albums" do
    it "returns albums for which all songs have been monetized on facebook" do
      expect(eligible_albums.length).not_to eq(0)
      expect(eligible_albums.map(&:id)).to include(fully_monetized.id)
      expect(eligible_albums).not_to include(none_monetized)
    end

    it "doesn't return albums that already have instagram salepoints" do
      alb = none_monetized
      alb.songs.each do |song|
        tm = create(:track_monetization, :eligible, song: song)
        tm.update(state: "delivered")
      end

      elig_first = svc.collect_eligible_albums
      expect(elig_first.map(&:id)).to include(alb.id)

      ig  = Store.find_by(short_name: "Instagram")
      sp  = alb.add_stores([ig]).first.first
      alb.petri_bundle.add_salepoints([sp])

      elig_second = svc.collect_eligible_albums
      expect(elig_second.map(&:id)).not_to include(alb.id)
    end

    it "doesn't return albums that have been only partially monetized" do
      expect(eligible_albums).not_to include(part_monetized)
    end

    it "doesn't return albums with blocked track_monetizations" do
      expect(eligible_albums).not_to include(blocked)
    end

    it "doesn't return albums that have been taken down" do
      expect(eligible_albums).not_to include(taken_down)
    end
  end

  describe "#add_store_to_albums" do
    it "creates instagram salepoints for eligible albums" do
      ig = Store.find_by(short_name: "Instagram")
      svc.add_store_to_albums
      salepoint_count = Salepoint.where(store_id: ig.id, salepointable_id: eligible_albums.map(&:id), salepointable_type: "Album").count
      expect(salepoint_count).to eq(eligible_albums.length)
    end

    it "does not create new purchases" do
      ig = Store.find_by(short_name: "Instagram")
      expect{
        svc.add_store_to_albums
      }.not_to change(Purchase, :count)
    end
  end

  describe "#distribute_salepoints" do
    it "creates an Instagram distribution" do
      eligible_albums = svc.collect_eligible_albums
      ig = Store.find_by(short_name: "Instagram")
      svc.add_store_to_albums
      svc.distribute_salepoints
      ig_distros = eligible_albums.map do |alb|
        alb.salepoints.where(store_id: ig.id).map(&:distributions).flatten!
      end
      ig_distros.flatten!
      expect(ig_distros.count).to eq(eligible_albums.length)
      expect(ig_distros.all? { |d| d.state == "enqueued" }).to be true
    end
  end

  describe ".backfill_all" do
    it "collects and enqueues all eligible IG albums" do
      3.times do
        alb = create(:album, :paid, :approved, :with_songs, number_of_songs: 2)
        petri = create(:petri_bundle, album: alb)
        create(:distribution, :delivered_to_spotify, petri_bundle: petri)

        alb.songs.each do |song|
          tm = create(:track_monetization, :eligible, song: song)
          tm.update(state: "delivered")
        end
      end

      expect { InstagramBackfillService.backfill_all }.to change(Distribution, :count).by(4)
    end
  end

  describe ".backfill_by_person" do
    it "distributes all instagrammable albums by a particular user" do
      ig_person = taken_down.person
      5.times do
        alb = create(:album, :paid, :approved, :with_songs, person: ig_person, number_of_songs: 2)
        petri = create(:petri_bundle, album: alb)
        create(:distribution, :delivered_to_spotify, petri_bundle: petri)

        alb.songs.each do |song|
          tm = create(:track_monetization, :eligible, song: song)
          tm.update(state: "delivered")
        end
      end

      expect(ig_person.albums.count).to eq(6)
      expect { InstagramBackfillService.backfill_by_person(ig_person.id) }.to change(Distribution, :count).by(5)
      expect(ig_person.albums.taken_down.count).to eq(0)
    end
  end
end
