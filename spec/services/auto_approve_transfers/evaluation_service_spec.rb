require "rails_helper"
Sidekiq::Testing.fake!

describe AutoApproveTransfers::EvaluationService do

  include ActiveSupport::Testing::TimeHelpers

  let!(:non_us_person)     { create(:person, :with_autorenewal_and_current_cc, :with_live_albums, country: "UK") }
  let!(:login_event)       { create(:login_event, person: non_us_person, created_at: Time.current, country: "United Kingdom") }
  let!(:payout_provider)   { create(:payout_provider, person: non_us_person) }
  let!(:transfer)          { create(:payout_transfer, payout_provider: payout_provider) }
  let!(:transfer_metadata) { create(:transfer_metadatum, auto_approved: false, trackable: transfer, login_event: login_event)}
  let!(:administrator)     { create(:person, :with_all_roles) }

  describe ".call!" do
    subject       { AutoApproveTransfers::EvaluationService.new(transfer_metadata) }
    it "is valid" do
      expect(subject).to be_valid
    end

    it "marks a transfer auto_approved true if the rules pass" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:auto_approved_withdrawals).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:vat_tax, any_args).and_return(true)
      allow_any_instance_of(subject.class).to receive(:all_rules_pass?).and_return(true)
      allow_any_instance_of(AutoApproveTransfers::WorkflowWorker).to receive(:handle_approval_service!).and_return(:true)
      allow_any_instance_of(subject.class).to receive(:time_in).and_return(0)
      Sidekiq::Testing.inline! { subject.call! }

      expect(transfer_metadata.auto_approved).to be(true)
    end

    it "marks a transfer auto_approved false if the rules do not pass" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:auto_approved_withdrawals).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:vat_tax, any_args).and_return(true)
      allow_any_instance_of(subject.class).to receive(:all_rules_pass?).and_return(false)
      allow_any_instance_of(AutoApproveTransfers::WorkflowWorker).to receive(:auto_approve_admin).and_return(administrator)
      allow_any_instance_of(subject.class).to receive(:time_in).and_return(0)
      Sidekiq::Testing.inline! { subject.call! }

      expect(transfer_metadata.auto_approved).to be(false)
    end

    it "schedules worker if rules pass" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:auto_approved_withdrawals).and_return(true)
      allow_any_instance_of(subject.class).to receive(:all_rules_pass?).and_return(true)
      allow_any_instance_of(subject.class).to receive(:time_in).and_return(0)

      expect { subject.call! }.to change(AutoApproveTransfers::WorkflowWorker.jobs, :size).by(1)
      expect(AutoApproveTransfers::WorkflowWorker.jobs.last['args']).to eq([transfer_metadata.id])
    end

    it "does not schedule a worker if the rules fail" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:auto_approved_withdrawals).and_return(true)
      allow_any_instance_of(subject.class).to receive(:all_rules_pass?).and_return(false)
      allow_any_instance_of(subject.class).to receive(:time_in).and_return(0)

      expect { subject.call! }.to change(AutoApproveTransfers::WorkflowWorker.jobs, :size).by(0)
    end

    it 'schedules for monday if rules pass and its a friday' do
      travel_to(Time.parse("2021-06-04")) do
        allow(FeatureFlipper).to receive(:show_feature?).with(:auto_approved_withdrawals).and_return(true)
        allow_any_instance_of(subject.class).to receive(:all_rules_pass?).and_return(true)
        subject.call!
        day_of_the_week = Time.at(AutoApproveTransfers::WorkflowWorker.jobs.last['at'].to_i).strftime("%A").downcase.to_sym

        expect(day_of_the_week).to be(:monday)
      end
    end

    it 'schedules for monday if rules pass and its a saturday' do
      travel_to(Time.parse("2021-06-05")) do
        allow(FeatureFlipper).to receive(:show_feature?).with(:auto_approved_withdrawals).and_return(true)
        allow_any_instance_of(subject.class).to receive(:all_rules_pass?).and_return(true)
        subject.call!
        day_of_the_week = Time.at(AutoApproveTransfers::WorkflowWorker.jobs.last['at'].to_i).strftime("%A").downcase.to_sym

        expect(day_of_the_week).to be(:monday)
      end
    end

    it 'schedules for monday if rules pass and its a sunday' do
      travel_to(Time.parse("2021-06-06")) do
        allow(FeatureFlipper).to receive(:show_feature?).with(:auto_approved_withdrawals).and_return(true)
        allow_any_instance_of(subject.class).to receive(:all_rules_pass?).and_return(true)
        subject.call!
        day_of_the_week = Time.at(AutoApproveTransfers::WorkflowWorker.jobs.last['at'].to_i).strftime("%A").downcase.to_sym

        expect(day_of_the_week).to be(:monday)
      end
    end

    it 'schedules for tuesday if rules pass and its a monday' do
      travel_to(Time.parse("2021-06-07")) do
        allow(FeatureFlipper).to receive(:show_feature?).with(:auto_approved_withdrawals).and_return(true)
        allow_any_instance_of(subject.class).to receive(:all_rules_pass?).and_return(true)
        subject.call!
        day_of_the_week = Time.at(AutoApproveTransfers::WorkflowWorker.jobs.last['at'].to_i).strftime("%A").downcase.to_sym

        expect(day_of_the_week).to be(:tuesday)
      end
    end

    it 'schedules for wednesday if rules pass and its a tuesday' do
      travel_to(Time.parse("2021-06-08")) do
        allow(FeatureFlipper).to receive(:show_feature?).with(:auto_approved_withdrawals).and_return(true)
        allow_any_instance_of(subject.class).to receive(:all_rules_pass?).and_return(true)
        subject.call!
        day_of_the_week = Time.at(AutoApproveTransfers::WorkflowWorker.jobs.last['at'].to_i).strftime("%A").downcase.to_sym

        expect(day_of_the_week).to be(:wednesday)
      end
    end

    it 'schedules for thursday if rules pass and its a wednesday' do
      travel_to(Time.parse("2021-06-09")) do
        allow(FeatureFlipper).to receive(:show_feature?).with(:auto_approved_withdrawals).and_return(true)
        allow_any_instance_of(subject.class).to receive(:all_rules_pass?).and_return(true)
        subject.call!
        day_of_the_week = Time.at(AutoApproveTransfers::WorkflowWorker.jobs.last['at'].to_i).strftime("%A").downcase.to_sym

        expect(day_of_the_week).to be(:thursday)
      end
    end

    it 'schedules for friday if rules pass and its a thursday' do
      travel_to(Time.parse("2021-06-10")) do
        allow(FeatureFlipper).to receive(:show_feature?).with(:auto_approved_withdrawals).and_return(true)
        allow_any_instance_of(subject.class).to receive(:all_rules_pass?).and_return(true)
        subject.call!
        day_of_the_week = Time.at(AutoApproveTransfers::WorkflowWorker.jobs.last['at'].to_i).strftime("%A").downcase.to_sym

        expect(day_of_the_week).to be(:friday)
      end
    end
  end
end
