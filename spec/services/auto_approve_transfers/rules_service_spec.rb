require "rails_helper"

describe AutoApproveTransfers::RulesService do

  it_behaves_like 'ruleable'
  let(:transfer) { create(:payout_transfer) }
  subject(:service) do
    AutoApproveTransfers::RulesService.new(transfer)
  end

  describe "2fa rule" do

    let(:vip_person_no_2fa) { create(:person, :with_tc_social, country: "Luxembourg", vip: true) }
    let(:payout_transfer) { create(:payout_transfer, person: vip_person_no_2fa) }

    let(:person_no_2fa) { create(:person, :with_tc_social, country: "Luxembourg", vip: false) }
    let(:sketchy_transfer) { create(:payout_transfer, person: person_no_2fa) }

    let(:person_with_2fa_no_vip) {create(:person, :with_two_factor_auth, country: "Luxembourg", vip: false)}
    let(:two_fa_transfer)  { create(:payout_transfer, person: person_with_2fa_no_vip) }

    let(:person_with_2fa_vip) { create(:person, :with_two_factor_auth, country: "Luxembourg", vip: true)}
    let(:two_fa_vip_transfer)  { create(:payout_transfer, person: person_with_2fa_vip) }


    subject(:service) do
      AutoApproveTransfers::RulesService.new(payout_transfer)
    end

    it 'passes if the person is a VIP and does not have active 2fa' do
      evaluation = service.evaluate_rule(name: :enabled_2fa_unless_vip, category: :auto_approve_transfers, against: payout_transfer)

      expect(vip_person_no_2fa.vip).to be(true)
      expect(evaluation).to be(:ok)
    end

    it "passes if the person has active 2fa and is not a VIP" do
      evaluation = service.evaluate_rule(name: :enabled_2fa_unless_vip, category: :auto_approve_transfers, against: two_fa_transfer)

      expect(person_with_2fa_no_vip.vip).to be(false)
      expect(evaluation).to be(:ok)
    end

    it "passes if the person has active 2fa and is a VIP" do
      evaluation = service.evaluate_rule(name: :enabled_2fa_unless_vip, category: :auto_approve_transfers, against: two_fa_vip_transfer)

      expect(person_with_2fa_vip.vip).to be(true)
      expect(evaluation).to be(:ok)
    end

    it "does not pass if the person does not have 2fa and is not a VIP" do
      evaluation = service.evaluate_rule(name: :enabled_2fa_unless_vip, category: :auto_approve_transfers, against: sketchy_transfer)

      expect(person_no_2fa.vip).to be(false)
      expect(evaluation).to be(:fail)
    end
  end

  describe "withdrawals_using_same_payout_method rule" do

    let!(:person_a) { create(:person, country: "Luxembourg", vip: true) }
    let!(:first_transfer_a)  { create(:payout_transfer, provider_status: :completed, tunecore_status: :approved, person_id: person_a.id) }
    let!(:second_transfer_a) { create(:payout_transfer, provider_status: :completed, tunecore_status: :approved, person_id: person_a.id) }
    let!(:third_transfer_a)  { create(:payout_transfer, provider_status: :completed, tunecore_status: :approved, person_id: person_a.id) }
    let!(:forth_transfer_a)  { create(:payout_transfer, provider_status: :pending, tunecore_status: :submitted, person_id: person_a.id) }


    let!(:person_b) { create(:person, country: "Luxembourg", vip: false) }
    let!(:first_transfer_b)  { create(:payout_transfer, provider_status: :completed, tunecore_status: :approved, person_id: person_b.id) }
    let!(:second_transfer_b) { create(:payout_transfer, provider_status: :completed, tunecore_status: :approved, person_id: person_b.id) }

    let!(:person_c) { create(:person, country: "Luxembourg", vip: true) }
    let!(:paypal_transfer_1) { create(:paypal_transfer, person_id: person_c.id, transfer_status: :completed)}
    let!(:paypal_transfer_2) { create(:paypal_transfer, person_id: person_c.id, transfer_status: :completed)}
    let!(:paypal_transfer_3) { create(:paypal_transfer, person_id: person_c.id, transfer_status: :completed)}
    let!(:paypal_transfer_4) { create(:paypal_transfer, person_id: person_c.id, transfer_status: :pending)}

    let!(:person_d) { create(:person, country: "Luxembourg", vip: true) }
    let!(:paypal_transfer_1a) { create(:paypal_transfer, person_id: person_c.id, transfer_status: :completed)}
    let!(:paypal_transfer_2a) { create(:paypal_transfer, person_id: person_c.id, transfer_status: :completed)}
    let!(:paypal_transfer_3a) { create(:paypal_transfer, person_id: person_c.id, transfer_status: :completed)}
    let!(:paypal_transfer_4a) do
      create(
        :paypal_transfer,
        person_id: person_c.id,
        transfer_status: :pending,
        paypal_address_confirmation: "suspicious@email.com",
        paypal_address: "suspicious@email.com"
      )
    end

    it "passes if the person has had 3 successful_withdrawals as per config[\"successful_withdrawals\"] from the same type payoneer" do
      evaluation = service.evaluate_rule(name: :withdrawals_using_same_payout_method, category: :auto_approve_transfers, against: forth_transfer_a)

      expect(evaluation).to be(:ok)
    end

    it "passes if the person has had 3 successful_withdrawals as per config[\"successful_withdrawals\"] from the same type paypal" do
      evaluation = service.evaluate_rule(name: :withdrawals_using_same_payout_method, category: :auto_approve_transfers, against: paypal_transfer_4)

      expect(evaluation).to be(:ok)
    end

    it "fails if the person has had 2 or less successful_withdrawals as per config[\"successful_withdrawals\"] from the same type" do
      evaluation = service.evaluate_rule(name: :withdrawals_using_same_payout_method, category: :auto_approve_transfers, against: second_transfer_b)

      expect(evaluation).to be(:fail)
    end

    it "fails if the person has different paypal address than other 3 successful withdrawals" do
      evaluation = service.evaluate_rule(name: :withdrawals_using_same_payout_method, category: :auto_approve_transfers, against: paypal_transfer_4a)

      expect(evaluation).to be(:fail)
    end
  end

  describe "request_approved_within rule" do
    let!(:person_with_successful_prev_txn) { create(:person, country: "Luxembourg") }
    let!(:person_without_successful_prev_txn) { create(:person, country: "Luxembourg") }

    let!(:first_payoneer_transaction) {
      create(
        :payout_transfer,
        provider_status: :completed,
        tunecore_status: :approved,
        person_id: person_with_successful_prev_txn.id,
        created_at: DateTime.now.ago(6.days),
        updated_at: DateTime.now.ago(6.days)
      )
    }

    let!(:first_paypal_transaction) {
      create(
        :paypal_transfer,
        transfer_status: :completed,
        person_id: person_with_successful_prev_txn.id,
        created_at: DateTime.now.ago(6.days),
        updated_at: DateTime.now.ago(6.days)
      )
    }

    let!(:payoneer_transaction_for_user_with_history) {
      create(:payout_transfer, person_id: person_with_successful_prev_txn.id)
    }

    let!(:payoneer_transaction_for_user_with_no_history) {
      create(:payout_transfer, person_id: person_without_successful_prev_txn.id)
    }

    let!(:paypal_transaction_for_user_with_history) {
      create(:paypal_transfer, person_id: person_with_successful_prev_txn.id)
    }

    let!(:paypal_transaction_for_user_with_no_history) {
      create(:paypal_transfer, person_id: person_without_successful_prev_txn.id)
    }

    it "payoneer - fails if an approved withdrawal exists within the last 7(configurable) days" do
      evaluation = service.evaluate_rule(
        name: :request_approved_within,
        category: :auto_approve_transfers,
        against: payoneer_transaction_for_user_with_history
      )
      expect(evaluation).to be(:fail)
    end

    it "payoneer - passes if no approved withdrawal exists within the last 7(configurable) days" do
      evaluation = service.evaluate_rule(
        name: :request_approved_within,
        category: :auto_approve_transfers,
        against: payoneer_transaction_for_user_with_no_history
      )
      expect(evaluation).to be(:ok)
    end

    it "paypal - fails if an approved withdrawal exists within the last 7(configurable) days" do
      evaluation = service.evaluate_rule(
        name: :request_approved_within,
        category: :auto_approve_transfers,
        against: paypal_transaction_for_user_with_history
      )
      expect(evaluation).to be(:fail)
    end

    it "paypal - passes if no approved withdrawal exists within the last 7(configurable) days" do
      evaluation = service.evaluate_rule(
        name: :request_approved_within,
        category: :auto_approve_transfers,
        against: paypal_transaction_for_user_with_no_history
      )
      expect(evaluation).to be(:ok)
    end
  end

  describe "currency_limit rule" do
    let!(:person) { create(:person, :with_balance, amount: 1_000_00) }
    let!(:low_value_paypal_transfer) { create(:paypal_transfer, person: person, payment_cents: 99_00) }
    let!(:high_value_paypal_transfer) { create(:paypal_transfer, person: person, payment_cents: 100_00) }
    let!(:payout_provider) { create(:payout_provider, person: person) }
    let!(:low_value_payoneer_transfer) { create(:payout_transfer, person: person, amount_cents: 99_00) }
    let!(:high_value_payoneer_transfer) { create(:payout_transfer, person: person, amount_cents: 100_00) }

    it "passes if the requested withdrawal amount is less than the value specified in config[\"max_amount\"]" do
      [low_value_paypal_transfer, low_value_payoneer_transfer].each do |transfer|
        evaluation = service.evaluate_rule(name: :currency_limit, category: :auto_approve_transfers, against: transfer)

        expect(evaluation).to be(:ok)
      end
    end

    it "fails if the requested withdrawal amount is greater or equal to the value specified in config[\"max_amount\"]" do
      [high_value_paypal_transfer, high_value_payoneer_transfer].each do |transfer|
        evaluation = service.evaluate_rule(name: :currency_limit, category: :auto_approve_transfers, against: transfer)

        expect(evaluation).to be(:fail)
      end
    end
  end

  describe "not_multiple_occupancy_payoneer_unless_vip rule" do
    before(:all) { PayoutProvider.destroy_all }

    let(:provider_id_1) { "12345" }
    let(:provider_id_2) { "9876540" }
    let(:person_1) { create(:person) }
    let(:person_2) { create(:person) }
    let(:person_3) { create(:person) }
    let!(:vip_person_1) { create(:person, vip: true) }
    let!(:vip_person_2) { create(:person, vip: true) }
    let!(:payout_provider_for_person_1) do
      create(
        :payout_provider,
        :approved,
        :skip_validation,
        person: person_1,
        provider_identifier: provider_id_1,
        active_provider: true
      )
    end
    let!(:payout_provider_for_person_2) do
      create(
        :payout_provider,
        :approved,
        :skip_validation,
        person: person_2,
        provider_identifier: provider_id_2,
        active_provider: true
      )
    end
    let!(:payout_provider_for_person_3a) do
      create(
        :payout_provider,
        :approved,
        :skip_validation,
        person: person_3,
        provider_identifier: 'aaaaa',
        active_provider: true
      )
    end
    let!(:payout_provider_for_person_3b) do
      create(
        :payout_provider,
        :approved,
        :skip_validation,
        person: person_3,
        provider_identifier: 'aaaaa',
        active_provider: true
      )
    end
    let!(:payout_provider_for_vip_person_1) do
      create(
        :payout_provider,
        :approved,
        :skip_validation,
        person: vip_person_1,
        provider_identifier: provider_id_1,
        active_provider: true
      )
    end
    let!(:payout_provider_for_vip_person_2) do
      create(
        :payout_provider,
        :approved,
        :skip_validation,
        person: vip_person_2,
        provider_identifier: provider_id_1,
        active_provider: true
      )
    end
    let(:payout_transfer_for_vip_person_1) { create(:payout_transfer, payout_provider: payout_provider_for_vip_person_1) }
    let(:payout_transfer_for_person_1) { create(:payout_transfer, payout_provider: payout_provider_for_person_1) }
    let(:payout_transfer_for_person_2) { create(:payout_transfer, payout_provider: payout_provider_for_person_2) }
    let(:payout_transfer_for_person_3) { create(:payout_transfer, payout_provider: payout_provider_for_person_3a) }

    context "with VIP person" do
      it "may have any number of accounts (people) connected to the same Payoneer payout provider (provider_identifier)" do
        evaluation = service.evaluate_rule(
          name: :not_multiple_occupancy_payoneer_unless_vip,
          category: :auto_approve_transfers,
          against: payout_transfer_for_vip_person_1
        )
        expect(evaluation).to be(:ok)
      end
    end

    context "with a non-VIP person" do
      it "must have a exactly 1 person with an active payout provider connected to the same Payoneer payout provider (provider_identifier)" do
        evaluation = service.evaluate_rule(
          name: :not_multiple_occupancy_payoneer_unless_vip,
          category: :auto_approve_transfers,
          against: payout_transfer_for_person_2
        )
        expect(evaluation).to be(:ok)
      end

      it "must not have more than 1 person with an active payout provider connected to the same Payoneer payout provider (provider_identifier)" do
        evaluation = service.evaluate_rule(
          name: :not_multiple_occupancy_payoneer_unless_vip,
          category: :auto_approve_transfers,
          against: payout_transfer_for_person_1
        )
        expect(evaluation).to be(:fail)
      end

      it "may have one person with more than one active payout provider connected to the same Payoneer payout provider (provider_identifier)" do
        evaluation = service.evaluate_rule(
          name: :not_multiple_occupancy_payoneer_unless_vip,
          category: :auto_approve_transfers,
          against: payout_transfer_for_person_3
        )
        expect(evaluation).to be(:ok)
      end
    end
  end

  describe "account_not_held rule" do
    it "must have a person whose account is not held (has a copyright claim, locked, or suspicious)" do
      person = create(:person)
      payout_transfer = create(:payout_transfer, person: person)

      evaluation = service.evaluate_rule(
        name: :account_not_held,
        category: :auto_approve_transfers,
        against: payout_transfer
      )
      expect(evaluation).to be(:ok)
    end

    it "must not have a person whose account has an active copyright claim on it" do
      person = create(:person, :with_active_copyright_claim)
      payout_transfer = create(:payout_transfer, person: person)

      evaluation = service.evaluate_rule(
        name: :account_not_held,
        category: :auto_approve_transfers,
        against: payout_transfer
      )
      expect(evaluation).to be(:fail)
    end

    it "must not have a person whose account is locked" do
      person = create(:person, :with_locked_account)
      payout_transfer = create(:payout_transfer, person: person)

      evaluation = service.evaluate_rule(
        name: :account_not_held,
        category: :auto_approve_transfers,
        against: payout_transfer
      )
      expect(evaluation).to be(:fail)
    end

    it "must not have a person whose account is suspicious" do
      person = create(:person, :with_suspicious_account)
      payout_transfer = create(:payout_transfer, person: person)

      evaluation = service.evaluate_rule(
        name: :account_not_held,
        category: :auto_approve_transfers,
        against: payout_transfer
      )
      expect(evaluation).to be(:fail)
    end
  end

  describe "transfer_login_match_previous rule" do
    let(:person) { create(:person, :with_balance, amount: 1_000_00) }
    let(:paypal_transfer) { create(:paypal_transfer, person: person, payment_cents: 99_00) }
    let(:payoneer_transfer) { create(:payout_transfer, person: person, amount_cents: 99_00) }

    before(:each) do
      (1..5).map do |i|
        create(:login_event, person: person, subdivision: "state_#{i}", city: "city_#{i}", country: "country_#{i}")
      end
      allow(FeatureFlipper).to receive(:show_feature?).with(:auto_approved_withdrawals, person.id).and_return(true)
    end

    it "passes if the transfer is requested from a location that matches at least one of last n logins" do
      create(:login_event, person: person, subdivision: "state_1", city: "city_1", country: "country_1")

      [paypal_transfer, payoneer_transfer].each do |transfer|
        service = AutoApproveTransfers::RulesService.new(transfer)
        evaluation = service.evaluate_rule(
          name: :transfer_login_match_previous,
          category: :auto_approve_transfers,
          against: transfer
        )

        expect(evaluation).to be(:ok)
      end
    end

    it "fails if the transfer is requested from a location different from last n logins" do
      [paypal_transfer, payoneer_transfer].each do |transfer|
        service = AutoApproveTransfers::RulesService.new(transfer)
        evaluation = service.evaluate_rule(
          name: :transfer_login_match_previous,
          category: :auto_approve_transfers,
          against: transfer
        )

        expect(evaluation).to be(:fail)
      end
    end
  end

  describe "not_currently_flagged rule" do
    let!(:person) { create(:person, :with_balance, amount: 1_000_00) }
    let!(:paypal_transfer) { create(:paypal_transfer, person: person) }
    let!(:payout_transfer) { create(:payout_transfer, person: person) }

    it "passes when the transfer is not flagged" do
      [paypal_transfer, payout_transfer].each do |transfer|
        allow(transfer).to receive(:flagged?).and_return(false)
        evaluation = service.evaluate_rule(name: :not_currently_flagged, category: :auto_approve_transfers, against: transfer)

        expect(evaluation).to be(:ok)
      end
    end

    it "fails when the transfer is flagged" do
      person.mark_as_suspicious!(create(:note), "Credit Card Fraud")
      [paypal_transfer, payout_transfer].each do |transfer|
        allow(transfer).to receive(:flagged?).and_return(true)
        evaluation = service.evaluate_rule(name: :not_currently_flagged, category: :auto_approve_transfers, against: transfer)

        expect(evaluation).to be(:fail)
      end
    end
  end
end
