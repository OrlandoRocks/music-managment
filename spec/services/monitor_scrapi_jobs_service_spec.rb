require "rails_helper"

describe MonitorScrapiJobsService do
  context "scrapi jobs are not created" do
    it "sends an email alert if no scrapi jobs are created in the last 12 hours" do
      expect(ScrapiJobFailureSummaryMailer).to receive(:scrapi_jobs_errors_email)
                                                 .with("Scrapi Jobs Error", "There's no scrapi job added to the database in the last 12 hours")
                                                 .and_return(double(deliver: true))
      MonitorScrapiJobsService.notify
    end
  end

  context "scrapi jobs are created" do
    let!(:scrapi_job) { create(:scrapi_job, created_at: 1.hour.ago) }
    it "does send an email alert if scrapi jobs are created in the last 12 hours" do
      expect(ScrapiJobFailureSummaryMailer).to_not receive(:scrapi_jobs_errors_email)
      MonitorScrapiJobsService.notify
    end
  end

  context "any scrapi job is successfully processed" do
    let!(:scrapi_job) { create(:scrapi_job, status: "in_queue", created_at: 1.hour.ago) }
    let!(:scrapi_job) { create(:scrapi_job, status: "in_queue", created_at: 2.hour.ago) }
    let!(:scrapi_job) { create(:scrapi_job, created_at: 3.hour.ago) }
    it "does not send an email alert if any scrapi job is successfully processed in the last 24 hours" do
      expect(ScrapiJobFailureSummaryMailer).to_not receive(:scrapi_jobs_errors_email)
      MonitorScrapiJobsService.notify
    end
  end

  context "no scrapi job is successfully processed" do
    let!(:scrapi_job) { create(:scrapi_job, status: "in_queue", created_at: 1.hour.ago) }
    let!(:scrapi_job) { create(:scrapi_job, status: "in_queue", created_at: 2.hour.ago) }
    it "sends an email alert if no scrapi job is successfully processed in the last 24 hours" do
      expect(ScrapiJobFailureSummaryMailer).to receive(:scrapi_jobs_errors_email)
                                                 .with("Scrapi Jobs Error", "There are no processed scrapi jobs for the last 24 hours")
                                                 .and_return(double(deliver: true))
      MonitorScrapiJobsService.notify
    end
  end
end
