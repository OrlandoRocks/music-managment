# frozen_string_literal: true

require "rails_helper"

describe Transfers::ApproveService, type: :model do
  let!(:admin) { Person.joins(:roles).find_by(roles: { name: "Admin" }) }
  let!(:person) { create(:person, :with_balance, amount: 50_000, country_website_id: 2, country: "CA")}
  let!(:paypal_transfer1) {create(:paypal_transfer, person: person, payment_cents: 500,  currency: "CAD") }
  
  let!(:paypal_transfer2) { create(:paypal_transfer, person: person, payment_cents: 1_000,  currency: "CAD") }
  let!(:payout_provider) { create(:payout_provider, person: person) }
  let!(:payout_transfer1) {
    create(
      :payout_transfer,
      tunecore_status: PayoutTransfer::SUBMITTED,
      payout_provider: payout_provider,
      amount_cents: 200,
      currency: "GBP"
    )
  }
  let!(:payout_transfer2) {
    create(
      :payout_transfer,
      tunecore_status: PayoutTransfer::SUBMITTED,
      payout_provider: payout_provider,
      amount_cents: 1000,
      currency: "GBP"
    )
  }
  let!(:paypal_approve_options) {
    {
      action: "approve",
      transfer_ids: [paypal_transfer1.id, paypal_transfer2.id],
      person: admin,
      provider: "paypal"
    }
  }
  let!(:payoneer_approve_options) {
    {
      action: "approve",
      transfer_ids: [payout_transfer1.id, payout_transfer2.id],
      person: admin,
      provider: "payoneer",
      referrer_url: "sample-url"
    }
  }
  let!(:paypal_reject_options) { paypal_approve_options.merge(action: "reject") }
  let!(:payoneer_reject_options) { payoneer_approve_options.merge(action: "reject") }

  subject { Transfers::ApproveService.new(paypal_approve_options) }

  it "is valid" do
    expect(subject).to be_valid
  end

  describe "#approve" do

    before do
      allow(FeatureFlipper).to receive(:show_feature?).with(:new_paypal_mass_pay_handler).and_return(false)
    end

    # TODO: remove PaypalTransfer.do_mass_pay, replace with Transfers::ApproveService.call
    context "Paypal" do
      it "returns success when transfers are approved" do
        allow(PaypalTransfer).to receive(:do_mass_pay)
          .with([paypal_transfer1, paypal_transfer2], admin.id)
          .and_return([])
        expect(Transfers::ApproveService.call(paypal_approve_options)).to eq({ success: true, errors: [] })
      end

      it "returns errors when transfers approval fails" do
        allow(PaypalTransfer).to receive(:do_mass_pay)
          .with([paypal_transfer1, paypal_transfer2], admin.id)
          .and_return(["boom", "boom"])
        expect(Transfers::ApproveService.call(paypal_approve_options)).to eq({ success: false, errors: ["boom", "boom"] })
      end
    end

    context "Payoneer" do
      it "returns success when transfers are approved" do
        allow(Payoneer::BatchActionsService).to receive(:send_actions)
          .with(
            {
              action: payoneer_approve_options[:action],
              transfer_ids: payoneer_approve_options[:transfer_ids],
              person: payoneer_approve_options[:person],
              referrer_url: "sample-url"
            }
          )
          .and_return([])
        expect(Transfers::ApproveService.call(payoneer_approve_options)).to eq({ success: true, errors: [] })
      end

      it "returns errors when transfers approval fails" do
        allow(Payoneer::BatchActionsService).to receive(:send_actions)
          .with(
            {
              action: payoneer_approve_options[:action],
              transfer_ids: payoneer_approve_options[:transfer_ids],
              person: payoneer_approve_options[:person],
              referrer_url: payoneer_approve_options[:referrer_url]
            }
          )
          .and_return(["boom", "boom"])
        expect(Transfers::ApproveService.call(payoneer_approve_options))
          .to eq({ success: false, errors: ["boom", "boom"] })
      end
    end
  end

  describe "#reject" do
    context "Paypal" do
      it "returns success when transfers are rejected" do
        allow_any_instance_of(PaypalTransfer).to receive(:cancel).and_return(true)
        expect(Transfers::ApproveService.call(paypal_reject_options)).to eq({ success: true, errors: [] })
      end

      it "returns errors when transfers rejection fails" do
        allow_any_instance_of(PaypalTransfer).to receive(:cancel).and_return(false)
        expect(Transfers::ApproveService.call(paypal_reject_options))
          .to eq({ success: false, errors: [paypal_transfer1.id, paypal_transfer2.id] })
      end
    end

    context "Payoneer" do
      it "returns success when transfers are approved" do
        allow(Payoneer::BatchActionsService).to receive(:send_actions)
          .with(
            {
              action: payoneer_reject_options[:action],
              transfer_ids: payoneer_reject_options[:transfer_ids],
              person: payoneer_reject_options[:person],
              referrer_url: "sample-url"
            }
          )
          .and_return([])
        expect(Transfers::ApproveService.call(payoneer_reject_options)).to eq({ success: true, errors: [] })
      end

      it "returns errors when transfers approval fails" do
        allow(Payoneer::BatchActionsService).to receive(:send_actions)
          .with(
            {
              action: payoneer_reject_options[:action],
              transfer_ids: payoneer_reject_options[:transfer_ids],
              person: payoneer_reject_options[:person],
              referrer_url: "sample-url"
            }
          )
          .and_return(["boom", "boom"])
        expect(Transfers::ApproveService.call(payoneer_reject_options))
          .to eq({ success: false, errors: ["boom", "boom"] })
      end
    end
  end

  describe "State Transitions" do
    context "PayPal" do
      before(:each) do
        allow_any_instance_of(PayPalBusiness::API)
          .to receive(:mass_pay)
          .and_return(double(:response, ack: "Success", correlationID: "1234", timestamp: Time.now))

          allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
          allow_any_instance_of(Person).to receive(:vat_applicable?).and_return(false)
          allow_any_instance_of(PayoutTransfer).to receive(:vat_tax_adjustment).and_return(nil)
      end

      it "allows approving pending transfers" do
        Sidekiq::Testing.inline! do
          expect(paypal_transfer1.transfer_status).to eq("pending")
          expect(Transfers::ApproveService.call(paypal_approve_options)).to eq({ success: true, errors: [] })
          expect(paypal_transfer1.reload.transfer_status).to eq("completed")

        end
      end

      it "doesn't allow approving approved transfers" do
        Sidekiq::Testing.inline! do
          paypal_transfer1.mark_as_processing!
          paypal_transfer1.mark_as_completed!

          expect {
            Transfers::ApproveService.call(paypal_approve_options).to raise_error(Workflow::NoTransitionAllowed)
          }
          
          expect(paypal_transfer1.reload.transfer_status).to eq("completed")
        end
      end

      it "allows rejecting pending transfers" do
        Sidekiq::Testing.inline! do
          expect(paypal_transfer1.transfer_status).to eq("pending")
          expect(Transfers::ApproveService.call(paypal_reject_options)).to eq({ success: true, errors: [] })
          expect(paypal_transfer1.reload.transfer_status).to eq("cancelled")
        end
      end

      it "doesn't allow rejecting approved transfers" do
        Sidekiq::Testing.inline! do

          paypal_transfer1.mark_as_processing!
          paypal_transfer1.mark_as_completed!
          options = paypal_reject_options.merge(transfer_ids: [paypal_transfer1.id])
          Transfers::ApproveService.call(options)
          
          expect(paypal_transfer1.reload.transfer_status).to eq("completed")
        end
      end
    end

    context "Payoneer" do
      before(:each) do
        admin.roles << Role.find_by(name: "Payout Service")
        submit_payout = double(
          :submit_payout,
          {
            success?: true,
            contents: JSON.dump({ payout_id: 12 }),
            description: "details",
            code: "0"
          }
        )
        fake_api_service = double(:fake_api_service, submit_payout: submit_payout)
        allow(PayoutProvider::ApiService).to receive(:new).and_return(fake_api_service)
      end

      it "allows approving submitted transfers" do
        expect(Transfers::ApproveService.call(payoneer_approve_options)).to eq({ success: true, errors: [] })
      end

      it "doesn't allow approving approved transfers" do
        payout_transfer1.update(tunecore_status: "approved")
        expect(Transfers::ApproveService.call(payoneer_approve_options))
          .to eq(
            {
              success: false,
              errors: ["Tunecore status is wrong status for transfer ID: #{payout_transfer1.id}"]
            }
          )
      end

      it "allows rejecting submitted transfers" do
        expect(Transfers::ApproveService.call(payoneer_reject_options)).to eq({ success: true, errors: [] })
      end

      it "doesn't allow rejecting approved transfers" do
        payout_transfer1.update(tunecore_status: "approved")
        options = payoneer_reject_options.merge(transfer_ids: [payout_transfer1.id])
        expect(Transfers::ApproveService.call(options))
          .to eq(
            {
              success: false,
              errors: ["Tunecore status incorrect for transfer ID: #{payout_transfer1.id}"]
            }
          )
      end
    end
  end
end
