require "rails_helper"

RSpec.describe Adyen::NewSessionService, type: :service do
  describe "#details" do
    let(:person) { create(:person, country: "ID") }
    let(:invoice) { create(:invoice, person: person) }
    let!(:adyen_merchant_config) { create(:adyen_merchant_config) }
    it "should return a hash with session_data and session_id if the country is associated with BI Luxembourg and adyen_integration,indonesian_wallets flags are enabled" do
      allow_any_instance_of(Adyen::PaymentsApiClient)
        .to receive(:send_request)
        .and_return(Adyen::Responses::NewSession.new(
                      Faraday::Response.new(
                        body: {
                          allowedPaymentMethods: ["cup"],
                          amount: { currency: "PHP", value: 1575 },
                          countryCode: "PH",
                          expiresAt: "2022-06-22T14:04:38+02:00",
                          id: "CS006DF69F60D07B16",
                          merchantAccount: "TuneCoreECOM",
                          recurringProcessingModel: "CardOnFile",
                          reference: "3",
                          returnUrl: "http://development.tunecore.local/cart/adyen_finalize_after_redirect?invoice_id=3",
                          shopperEmail: "adyenina@test.com",
                          shopperInteraction: "Ecommerce",
                          shopperReference: "11",
                          storePaymentMethod: true,
                          sessionData: "aqrtuxNl+IrfeY/2cRQFi/mP7pIsMCGiNZ7KEIeixdOer5XukUAu8io6uRtvaGflc8TRP5kjIXws5MbhW27+",
                        }.to_json
                      )
                    ))
      allow_any_instance_of(Adyen::Responses::NewSession)
        .to receive(:session_data)
        .and_return("aqrtuxNl+IrfeY/2cRQFi/mP7pIsMCGiNZ7KEIeixdOer5XukUAu8io6uRtvaGflc8TRP5kjIXws5MbhW27+")
      allow_any_instance_of(Adyen::Responses::NewSession)
        .to receive(:id)
        .and_return("CS006DF69F60D07B16")
      allow_any_instance_of(Adyen::Responses::NewSession)
        .to receive(:id)
        .and_return("CS006DF69F60D07B16")
      allow(FeatureFlipper).to receive(:show_feature?).with(:indonesian_wallets, person).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:adyen_integration, person).and_return(true)
      session_info = Adyen::NewSessionService.new(invoice, "cup").details
      expect(session_info).to include(
        session_id: "CS006DF69F60D07B16",
        session_data: "aqrtuxNl+IrfeY/2cRQFi/mP7pIsMCGiNZ7KEIeixdOer5XukUAu8io6uRtvaGflc8TRP5kjIXws5MbhW27+",
      )
    end
    it "should return a an empty string for both session_data and session_id if the country is associated with BI Luxembourg and adyen_integration flag is disabled" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:adyen_integration, person).and_return(false)
      session_info = Adyen::NewSessionService.new(invoice, "cup").details
      expect(session_info).to include(session_id: "", session_data: "")
    end

    it "should return a an empty string for both session_data and session_id if the country is not associated with BI Luxembourg" do
      person = create(:person, country: "US")
      invoice = create(:invoice, person: person)
      allow(FeatureFlipper).to receive(:show_feature?).with(:adyen_integration, person).and_return(true)
      session_info = Adyen::NewSessionService.new(invoice, "cup").details
      expect(session_info).to include(session_id: "", session_data: "")
    end

    it "should return a an empty string for both session_data and session_id if errorCode is present in the response" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:indonesian_wallets, person).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:adyen_integration, person).and_return(true)
      allow_any_instance_of(Adyen::PaymentsApiClient)
        .to receive(:send_request)
        .and_return(Adyen::Responses::NewSession.new(
                      Faraday::Response.new(
                        body: {
                          "status" => 422,
                          "errorCode" => "14_0408"
                        }.to_json
                      )
                    ))
      session_info = Adyen::NewSessionService.new(invoice, "cup").details
      expect(session_info).to include(session_id: "", session_data: "")
    end
  end
end
