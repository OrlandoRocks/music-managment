require "rails_helper"

RSpec.describe Adyen::AdyenRenewalService, type: :service do
  let(:person) { create(:person, country: "PH") }
  let(:adyen_transaction) { create(:adyen_transaction, person: person, country: Country.find_by(iso_code: "PH")) }
  let(:invoice) { create(:invoice, person: person) }
  let(:cable_auth_token) { create(:cable_auth_token, person: person) }
  let!(:adyen_merchant_config) { create(:adyen_merchant_config) }

  describe "#process_renewal" do
    it "should update Adyen Transaction if response is valid" do
      allow_any_instance_of(Person)
        .to receive(:cable_auth_token)
        .and_return(cable_auth_token)
      allow_any_instance_of(Adyen::PaymentsApiClient)
        .to receive(:send_request)
        .and_return(Adyen::Responses::Renewal.new(
                      Faraday::Response.new(
                        body: {
                          pspReference: "NC6HT9CRT65ZGN82",
                          resultCode: "Authorised",
                          merchantReference: invoice.id
                        }.to_json
                      )
                    ))

      expect(adyen_transaction.result_code).to be_nil
      Adyen::AdyenRenewalService.new(adyen_transaction, invoice).process_renewal
      expect(adyen_transaction.result_code).to eq("Authorised")
      expect(adyen_transaction.psp_reference).to eq("NC6HT9CRT65ZGN82")
      expect(adyen_transaction.merchant_reference).to eq("#{invoice.id}")
    end
  end
end
