require "rails_helper"

RSpec.describe Adyen::PersonPreferenceUpdateService, type: :service do
  describe "#update!" do
    let(:person) { create(:person) }
    let(:stored_payment_method) { create(:adyen_stored_payment_method) }

    it "returns nil if user doesn't have required flags" do
      allow(FeatureFlipper)
        .to receive(:show_feature?)
        .with(:adyen_integration, person)
        .and_return(false)

      allow(FeatureFlipper)
        .to receive(:show_feature?)
        .with(:adyen_renewals, person)
        .and_return(false)

      expect(described_class.new(person, stored_payment_method).update!)
        .to be(nil)
    end

    it "updates person preference with given payment method" do
      allow(FeatureFlipper)
        .to receive(:show_feature?)
        .with(:adyen_integration, person)
        .and_return(true)

      allow(FeatureFlipper)
        .to receive(:show_feature?)
        .with(:adyen_renewals, person)
        .and_return(true)

      described_class.new(person, stored_payment_method).update!
      preference = person.person_preference
      expect(preference.preferred_payment_type).to eq(AdyenTransaction::ADYEN_NAME)
      expect(preference.preferred_adyen_payment_method_id).to eq(stored_payment_method.id)
    end
  end
end
