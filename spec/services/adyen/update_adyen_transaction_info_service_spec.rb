require "rails_helper"

RSpec.describe Adyen::UpdateAdyenTransactionInfoService, type: :service do
  describe "#update_transaction" do
    let(:person) { create(:person) }
    let(:cable_auth_token) { create(:cable_auth_token, person: person) }
    let(:adyen_transaction) {
      create(
        :adyen_transaction, person: person,
                            country: Country.find_by(iso_code: "ID"), session_id: "CS2B109FC3C3AC5CE4"
      )
    }
    let(:adyen_payment_method_info) { create(:adyen_payment_method_info) }
    let!(:adyen_payment_method_info_apple) {
      create(
        :adyen_payment_method_info,
        payment_method_name: 'applepay'
        )
    }

    it "should update record return accepted if all adyen feature flags are enabled" do
      allow(FeatureFlipper)
        .to receive(:show_feature?)
        .and_call_original
      allow_any_instance_of(Person)
        .to receive(:cable_auth_token)
        .and_return(cable_auth_token)
      allow(FeatureFlipper).to receive(:show_feature?).with(:adyen_integration, person).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:indonesian_wallets, person).and_return(true)
      allow(AdyenTransaction)
        .to receive(:find_by)
        .and_return(adyen_transaction)
      response = Adyen::UpdateAdyenTransactionInfoService.new(
        {
          "notificationItems" =>
            [
              {
                "NotificationRequestItem" =>
                     {
                       "paymentMethod" => "cup",
                       "additionalData" => {
                         "checkoutSessionId" => "CS2B109FC3C3AC5CE4",
                         "recurring.recurringDetailReference" => "SCFWXHMMD6KXWD82"
                       },
                       "pspReference" => "J9QQTFTHB8NKGK82",
                       "success" => "true",
                       "amount" => { "currency" => "EUR", "value" => 3509 }
                     }
              }
            ]
        }
      ).update_transaction
      expect(response).to eq("authorised")
    end

    it "should return rejected if adyen_integration feature flag is disabled" do
      allow(FeatureFlipper)
        .to receive(:show_feature?)
        .and_call_original
      allow(FeatureFlipper).to receive(:show_feature?).with(:adyen_integration, person).and_return(false)
      allow(AdyenTransaction)
        .to receive(:find_by)
        .and_return(adyen_transaction)
      response = Adyen::UpdateAdyenTransactionInfoService.new(
        {
          "notificationItems" =>
            [
              {
                "NotificationRequestItem" =>
                     {
                       "paymentMethod" => "cup",
                       "additionalData" => {
                         "checkoutSessionId" => "CS2B109FC3C3AC5CE4",
                         "recurring.recurringDetailReference" => "SCFWXHMMD6KXWD82"
                       },
                       "pspReference" => "J9QQTFTHB8NKGK82",
                       "success" => "true",
                       "amount" => { "currency" => "EUR", "value" => 3509 }
                     }
              }
            ]
        }
      ).update_transaction
      expect(response).to eq("refused")
    end

    it "should return rejected if the country is indonesia and indonesian_wallets feature flags is disabled" do
      allow(FeatureFlipper)
        .to receive(:show_feature?)
        .and_call_original
      allow(FeatureFlipper).to receive(:show_feature?).with(:adyen_integration, person).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:indonesian_wallets, person).and_return(false)
      allow(AdyenTransaction)
        .to receive(:find_by)
        .and_return(adyen_transaction)
      response = Adyen::UpdateAdyenTransactionInfoService.new(
        {
          "notificationItems" =>
            [
              {
                "NotificationRequestItem" =>
                     {
                       "paymentMethod" => "gcash",
                       "additionalData" => {
                         "checkoutSessionId" => "CS2B109FC3C3AC5CE4",
                         "recurring.recurringDetailReference" => "SCFWXHMMD6KXWD82"
                       },
                       "pspReference" => "J9QQTFTHB8NKGK82",
                       "success" => "true",
                       "amount" => { "currency" => "EUR", "value" => 3509 }
                     }
              }
            ]
        }
      ).update_transaction
      expect(response).to eq("refused")
    end

    it "should update payment method to applepay even if varient is passed" do
      allow(FeatureFlipper)
        .to receive(:show_feature?)
        .and_call_original
      allow_any_instance_of(Person)
        .to receive(:cable_auth_token)
        .and_return(cable_auth_token)
      allow(FeatureFlipper)
        .to receive(:show_feature?)
        .with(:adyen_integration, person)
        .and_return(true)
      allow(FeatureFlipper)
        .to receive(:show_feature?)
        .with(:indonesian_wallets, person)
        .and_return(true)
      allow(AdyenTransaction)
        .to receive(:find_by)
        .and_return(adyen_transaction)

      response = Adyen::UpdateAdyenTransactionInfoService.new(
        {
          "notificationItems" =>
            [
              {
                "NotificationRequestItem" =>
                  {
                    "paymentMethod" =>  AdyenPaymentMethodInfo::APPLEPAY_VARIANTS.first,
                    "additionalData" =>
                      {
                        "checkoutSessionId" => "CS2B109FC3C3AC5CE4",
                        "recurring.recurringDetailReference" => "SCFWXHMMD6KXWD82"
                      },
                    "pspReference" => "J9QQTFTHB8NKGK82",
                    "success" => "true",
                    "amount" =>
                      {
                        "currency" => "EUR", "value" => 3509
                      }
                  }
              }
            ]
        }
      ).update_transaction
      expect(response).to eq("authorised")
      expect(adyen_transaction.adyen_payment_method_info.payment_method_name).to eq(AdyenPaymentMethodInfo::APPLEPAY_PAYMENT_METHOD)
    end
  end
end
