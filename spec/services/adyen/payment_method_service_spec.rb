require "rails_helper"

RSpec.describe Adyen::PaymentMethodService, type: :service do
  describe "#payment_methods" do
    let(:person) { create(:person) }
    let!(:adyen_merchant_config) { create(:adyen_merchant_config, corporate_entity_id: 2) }
    it "should return a non-empty array if the country is associated with BI Luxembourg and adyen_integration flag is enabled" do
      allow_any_instance_of(Adyen::PaymentsApiClient)
        .to receive(:send_request)
        .and_return(Adyen::Responses::PaymentMethod.new(
                      Faraday::Response.new(
                        body: {
                          paymentMethods: [
                            { "brands" => ["cup"], "name" => "Credit Card", "type" => "scheme" },
                            { "name" => "GCash", "type" => "gcash" },
                            { "name" => "Maya Wallet", "type" => "paymaya_wallet" }
                          ],
                        }.to_json
                      )
                    ))
      allow_any_instance_of(Adyen::Responses::PaymentMethod)
        .to receive(:payment_methods)
        .and_return([
                      { "brands" => ["cup"], "name" => "Credit Card", "type" => "scheme" },
                      { "name" => "GCash", "type" => "gcash" },
                      { "name" => "Maya Wallet", "type" => "paymaya_wallet" }
                    ])
      allow(FeatureFlipper).to receive(:show_feature?).with(:adyen_integration, person).and_return(true)
      payment_methods = Adyen::PaymentMethodService.new("PH", person).payment_methods
      expect(payment_methods).not_to be_empty
    end

    it "should return an array with cup payment method if the country is indonesia and indonesian_wallets feature flag is disabled" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:indonesian_wallets, person).and_return(false)
      payment_methods = Adyen::PaymentMethodService.new(
        "ID",
        person
      ).payment_methods
      expect(payment_methods).not_to be_empty
    end

    it "should return a non-empty array if the country is indonesia and indonesian_wallets feature flag is enabled" do
      allow_any_instance_of(Adyen::PaymentsApiClient)
        .to receive(:send_request)
        .and_return(Adyen::Responses::PaymentMethod.new(
                      Faraday::Response.new(
                        body: {
                          paymentMethods: [
                            {
                              "name" => "DANA",
                              "type" => "dana"
                            },
                            {
                              "brands" => [
                                "cup"
                              ],
                              "name" => "Credit Card",
                              "type" => "scheme"
                            },
                            {
                              "name" => "GoPay Wallet",
                              "type" => "gopay_wallet"
                            }
                          ],
                        }.to_json
                      )
                    ))
      allow_any_instance_of(Adyen::Responses::PaymentMethod)
        .to receive(:payment_methods)
        .and_return([
                      { "name" => "DANA", "type" => "dana" },
                      {
                        "brands" => ["amex", "cup", "diners", "discover", "mc", "visa"],
                        "name" => "Credit Card",
                        "type" => "scheme"
                      },
                      { "name" => "GoPay Wallet", "type" => "gopay_wallet" }
                    ])
      allow(FeatureFlipper).to receive(:show_feature?).with(:indonesian_wallets, person).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:adyen_integration, person).and_return(true)
      payment_methods = Adyen::PaymentMethodService.new(
        "ID",
        person
      ).payment_methods
      expect(payment_methods).not_to be_empty
    end

    it "should return an empty array if the country belongs to TuneCore US" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:adyen_integration, person).and_return(true)
      payment_methods = Adyen::PaymentMethodService.new(
        "US",
        person
      ).payment_methods
      expect(payment_methods).to be_empty
    end
  end
end