require "rails_helper"

RSpec.describe Adyen::RefundTransactionService, type: :service do
  describe "#call" do
    let(:person) { create(:person, country: "PH") }
    let(:adyen_payment_method_info) { create(:adyen_payment_method_info, payment_method_name: "gcash") }
    let(:foreign_exchange_rate) { create(:foreign_exchange_rate, :php) }
    let(:adyen_transaction) { create(:adyen_transaction, person: person, country: Country.find_by(iso_code: "PH"),
                                     adyen_payment_method_info: adyen_payment_method_info,
                                     foreign_exchange_rate: foreign_exchange_rate) }
    let!(:adyen_merchant_config) { create(:adyen_merchant_config) }

    it "should create adyen_transaction for refund" do
      allow_any_instance_of(Adyen::PaymentsApiClient)
        .to receive(:send_request)
        .and_return(Adyen::Responses::Refund.new(
                      Faraday::Response.new(
                        body: {
                          pspReference: "NC6HT9CRT65ZGN82",
                          status: "received"
                        }.to_json
                      )
                    ))

      refund_transaction = Adyen::RefundTransactionService.new(adyen_transaction, 29.99).call
      expect(refund_transaction).to be_instance_of(AdyenTransaction)
    end

    it "should raise error if refund failed" do
      allow_any_instance_of(Adyen::PaymentsApiClient)
        .to receive(:send_request)
        .and_return(Adyen::Responses::Refund.new(
                      Faraday::Response.new(
                        body: {
                          pspReference: "NC6HT9CRT65ZGN82",
                          status: "refused"
                        }.to_json
                      )
                    ))

      refund_transaction = Adyen::RefundTransactionService.new(adyen_transaction, 29.99)
      expect { refund_transaction.call }.to raise_error(AdyenError::AdyenTransactionRefundError)
    end
  end
end
