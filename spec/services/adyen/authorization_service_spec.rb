require "rails_helper"

RSpec.describe Adyen::AuthorizationService, type: :service do
  let!(:adyen_merchant_config) { create(:adyen_merchant_config) }
  describe "#authorize!" do
    it "return true for successful authorization" do
      params = {
        "live" => "false",
        "notificationItems" => [
          {
            "NotificationRequestItem" => {
              "additionalData" => {
                "expiryDate" => "10/2030",
                "authCode" => "036938",
                "recurring.recurringDetailReference" => "VZCQPCV6B6KXWD82",
                "cardSummary" => "0000",
                "threeds2.cardEnrolled" => "false",
                "checkoutSessionId" => "CSA77D6D249E170BD1",
                "recurringProcessingModel" => "CardOnFile",
                "recurring.shopperReference" => "ADYEN-ID-12",
                "hmacSignature" => "coqCmt/IZ4E3CzPvMY8zTjQVL5hYJUiBRg8UU+iCWo0="
              },
              "amount" => { "currency" => "EUR", "value" => 1130 },
              "eventCode" => "AUTHORISATION",
              "eventDate" => "2022-04-19T19:13:17+02:00",
              "merchantAccountCode" => "TestMerchant",
              "merchantReference" => "TestPayment-1407325143704",
              "operations" => ["CANCEL", "CAPTURE", "REFUND"],
              "paymentMethod" => "cup",
              "pspReference" => "7914073381342284",
              "reason" => "036938:0000:10/2030",
              "success" => "true"
            }
          }
        ]
      }

      session_info = Adyen::AuthorizationService.new(params).authorize!
      expect(session_info).to eq(true)
    end

    it "return false for unsuccessful authorization" do
      params = {
        "live" => "false",
        "notificationItems" => [
          {
            "NotificationRequestItem" => {
              "additionalData" => {
                "expiryDate" => "10/2030",
                "authCode" => "036938",
                "recurring.recurringDetailReference" => "VZCQPCV6B6KXWD82",
                "cardSummary" => "0000",
                "threeds2.cardEnrolled" => "false",
                "checkoutSessionId" => "CSA77D6D249E170BD1",
                "recurringProcessingModel" => "CardOnFile",
                "recurring.shopperReference" => "ADYEN-ID-12",
                "hmacSignature" => "Cmt/IZ4E3CzPvMY8zTjQVL5hYJUiBRg8UU+iCWo0="
              },
              "amount" => { "currency" => "EUR", "value" => 1130 },
              "eventCode" => "AUTHORISATION",
              "eventDate" => "2022-04-19T19:13:17+02:00",
              "merchantAccountCode" => "TestMerchant",
              "merchantReference" => "TestPayment-1407325143704",
              "operations" => ["CANCEL", "CAPTURE", "REFUND"],
              "paymentMethod" => "cup",
              "pspReference" => "7914073381342284",
              "reason" => "036938:0000:10/2030",
              "success" => "true"
            }
          }
        ]
      }

      session_info = Adyen::AuthorizationService.new(params).authorize!
      expect(session_info).to eq(false)
    end
  end
end
