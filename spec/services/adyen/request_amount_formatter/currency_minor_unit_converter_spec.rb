require "rails_helper"

RSpec.describe Adyen::RequestAmountFormatter::CurrencyMinorUnitConverter, type: :service do
  describe "#adjust_minor_unit_for" do
    it "returns amount in minor unit for target currency" do
      subject1 = described_class.new("USD", "IDR")
      expect(subject1.adjust_minor_unit_for(100)).to eq(1.to_d)

      subject2 = described_class.new("IDR", "USD")
      expect(subject2.adjust_minor_unit_for(1)).to eq(100.to_d)
    end
  end
end
