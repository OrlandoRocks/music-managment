require "rails_helper"

RSpec.describe Adyen::RequestAmountFormatter::NativeCurrencyFormatter, type: :service do
  describe "#amount" do
    context "with foreign_exchange_rate" do
      it "returns amount converted to native currency with minor unit conversion" do
        params = {
          original_amount: 100,
          foreign_exchange_rate: create(:foreign_exchange_rate, exchange_rate: 10)
        }
        subject = described_class.new(params)

        expect(subject.amount).to eq(1000)
      end
    end

    context "without foreign_exchange_rate" do
      it "returns amount converted to native currency with minor unit conversion" do
        create(:foreign_exchange_rate, :php, exchange_rate: 10)

        params = {
          original_amount: 100,
          country_code: "PH",
          original_currency: CurrencyCodeType::USD
        }

        subject = described_class.new(params)

        expect(subject.amount).to eq(1000)
      end
    end
  end

  describe "#currency" do
    it "returns native currency" do
        params = {
          original_amount: 100,
          foreign_exchange_rate: create(:foreign_exchange_rate, :php)
        }
        subject = described_class.new(params)

        expect(subject.currency).to eq(CurrencyCodeType::PHP)
    end
  end

  describe "#native_converter?" do
    it "returns true" do
        params = {
          original_amount: 100,
          foreign_exchange_rate: create(:foreign_exchange_rate, :php)
        }
        subject = described_class.new(params)

        expect(subject.native_converter?).to be true
    end
  end
end
