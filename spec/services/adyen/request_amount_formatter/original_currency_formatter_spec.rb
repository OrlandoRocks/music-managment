require "rails_helper"

RSpec.describe Adyen::RequestAmountFormatter::OriginalCurrencyFormatter, type: :service do
  let(:subject) { described_class.new(original_amount: 100.0, original_currency: "USD") }
  describe "#amount" do
    it "returns original amount as integer" do
      expect(subject.amount).to eq(100)
    end
  end

  describe "currency" do
    it "returns original currency" do
      expect(subject.currency).to eq("USD")
    end
  end

  describe "native_converter?" do
    it "returns false" do
      expect(subject.native_converter?).to be false
    end
  end
end
