require "rails_helper"

RSpec.describe Adyen::RequestAmountFormatter::Factory, type: :spec do
  describe "#formatter" do
    it "returns instance of OriginalCurrencyFormatter for payment methods with out conversion requirement" do
      params = {
        payment_method: "cup",
        original_amount: "10",
        original_currency: "USD"
      }
      subject = described_class.new(params)

      expect(subject.formatter).to be_a Adyen::RequestAmountFormatter::OriginalCurrencyFormatter
    end

    it "returns instance of NativeCurrencyFormatter for payment methods with conversion requirement" do
      params = {
        payment_method: "gcash",
        original_amount: "10",
        foreign_exchange_rate: create(:foreign_exchange_rate)
      }
      subject = described_class.new(params)

      expect(subject.formatter).to be_a Adyen::RequestAmountFormatter::NativeCurrencyFormatter
    end

    it "raises error if payment method does not specify the conversion requirement" do
      params = {
        payment_method: "new_payment",
      }
      subject = described_class.new(params)

      expect { subject.formatter }.to raise_error("Conversion requirement not specified for new_payment")
    end
  end
end
