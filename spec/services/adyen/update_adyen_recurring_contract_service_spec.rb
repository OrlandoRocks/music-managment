require "rails_helper"

RSpec.describe Adyen::UpdateAdyenRecurringContractService, type: :service do
  describe "#update_transaction" do
    let(:person) { create(:person) }
    let(:cable_auth_token) { create(:cable_auth_token, person: person) }
    let(:adyen_transaction) {
      create(
        :adyen_transaction, person: person,
                            country: Country.find_by(iso_code: "NL"), session_id: "CS194D76FBB17F78F8"
      )
    }

    it "should update record return accepted if adyen_transaction" do
      allow_any_instance_of(Person)
        .to receive(:cable_auth_token)
        .and_return(cable_auth_token)
      allow(AdyenTransaction)
        .to receive(:find_by)
        .and_return(adyen_transaction)
      response = Adyen::UpdateAdyenRecurringContractService.new(
        {
          "notificationItems" =>
          [
            {
              "NotificationRequestItem" =>
                  {
                    "additionalData" => {
                      "shopperReference" => "PERSON-49"
                    },
                    "amount" => {
                      "currency" => "EUR",
                      "value" => 1000
                    },
                    "eventCode" => "RECURRING_CONTRACT",
                    "originalReference" => "MFWW3T9D36ZW8N82",
                    "paymentMethod" => "ideal",
                    "pspReference" => "NPKNNCB5K6KXWD82",
                    "success" => "true"
                  }
            }
          ]
        }
      ).update_transaction
      expect(response).to eq("authorised")
    end

    it "should return rejected if adyen_integration is nil" do
      allow(AdyenTransaction)
        .to receive(:find_by)
        .and_return(nil)
      response = Adyen::UpdateAdyenRecurringContractService.new(
        {
          "notificationItems" =>
          [
            {
              "NotificationRequestItem" =>
                  {
                    "additionalData" => {
                      "shopperReference" => "PERSON-49"
                    },
                    "amount" => {
                      "currency" => "EUR",
                      "value" => 1000
                    },
                    "eventCode" => "RECURRING_CONTRACT",
                    "originalReference" => "MFWW3T9D36ZW8N82",
                    "paymentMethod" => "ideal",
                    "pspReference" => "NPKNNCB5K6KXWD82",
                    "success" => "true"
                  }
            }
          ]
        }
      ).update_transaction
      expect(response).to eq("refused")
    end
  end
end
