require "rails_helper"

RSpec.describe Adyen::RefreshAdyenTransactionService, type: :service do
  let(:person) { create(:person, country: "PH") }
  let(:adyen_transaction) { create(:adyen_transaction, person: person, country: Country.find_by(iso_code: "PH")) }
  let(:redirect_result) { "ABC" }
  let(:cable_auth_token) { create(:cable_auth_token, person: person) }
  let!(:adyen_merchant_config) { create(:adyen_merchant_config) }

  describe "#call" do
    it "should update Adyen Transaction if response is valid" do
      allow_any_instance_of(Person)
        .to receive(:cable_auth_token)
        .and_return(cable_auth_token)
      allow_any_instance_of(Adyen::PaymentsApiClient)
        .to receive(:send_request)
        .and_return(Adyen::Responses::PaymentDetail.new(
                      Faraday::Response.new(
                        body: {
                          pspReference: "NC6HT9CRT65ZGN82",
                          resultCode: "Authorised",
                          merchantReference: "430"
                        }.to_json
                      )
                    ))

      expect(adyen_transaction.result_code).to be_nil
      Adyen::RefreshAdyenTransactionService.call(adyen_transaction, redirect_result)
      expect(adyen_transaction.result_code).to eq("Authorised")
      expect(adyen_transaction.psp_reference).to eq("NC6HT9CRT65ZGN82")
      expect(adyen_transaction.merchant_reference).to eq("430")
    end

    it "should raise Adyen Transaction failed if resultCode returned is not equal to  Authorized" do
      allow_any_instance_of(Adyen::PaymentsApiClient)
        .to receive(:send_request)
        .and_return(Adyen::Responses::PaymentDetail.new(
                      Faraday::Response.new(
                        body: {
                          pspReference: "NC6HT9CRT65ZGN82",
                          resultCode: "Refused",
                          merchantReference: "430"
                        }.to_json
                      )
                    ))

      expect { Adyen::RefreshAdyenTransactionService.call(adyen_transaction, redirect_result) }.to raise_error
    end
  end
end
