require "rails_helper"

RSpec.describe Adyen::UpdateAdyenRefundTransactionService, type: :service do
  describe "#update_transaction" do
    let(:person) { create(:person, country: "PH") }
    let(:cable_auth_token) { create(:cable_auth_token, person: person) }

    let(:invoice) { create(:invoice, :with_adyen_settlement, :settled) }
    let!(:refund) { create(:refund, invoice: invoice, status: "pending") }
    let(:adyen_transaction) {
      create(
        :adyen_transaction, person: person, psp_reference: "J9QQTFTHB8NKGK82", invoice: invoice,
                            country: Country.find_by(iso_code: "PH")
      )
    }

    let(:refunded_invoice) { create(:invoice, :with_adyen_settlement, :settled) }
    let!(:successful_refund) { create(:refund, invoice: refunded_invoice, status: "success") }
    let(:refunded_adyen_transaction) {
      create(
        :adyen_transaction, person: person, psp_reference: "J9QQTFTHB8NKGK83", invoice: refunded_invoice,
                            country: Country.find_by(iso_code: "PH")
      )
    }

    it "should return accepted if the adyen_integration feature flag is enabled " do
      invoice.reload
      allow(AdyenTransaction).to receive(:find_by).and_return(adyen_transaction)
      allow(FeatureFlipper)
        .to receive(:show_feature?)
        .and_call_original
      allow_any_instance_of(Person)
        .to receive(:cable_auth_token)
        .and_return(cable_auth_token)
      allow(FeatureFlipper).to receive(:show_feature?).with(:adyen_integration, person).and_return(true)

      response = Adyen::UpdateAdyenRefundTransactionService.new(
        {
          "notificationItems" =>
              [
                {
                  "NotificationRequestItem" =>
                       {
                         "paymentMethod" => "cup",
                         "additionalData" => {
                           "checkoutSessionId" => "CS2B109FC3C3AC5CE4",
                           "recurring.recurringDetailReference" => "SCFWXHMMD6KXWD82"
                         },
                         "pspReference" => "J9QQTFTHB8NKGK82",
                         "success" => "true",
                         "amount" => {
                           "currency" => "EUR",
                           "value" => 3509
                         },
                         "reason" => "sample-reason"
                       }
                }
              ]
        }
      ).update_transaction
      expect(response).to eq("authorised")
    end

    it "should not create duplicate refund_settlements for refunded invoice" do
      allow(AdyenTransaction).to receive(:find_by).and_return(refunded_adyen_transaction)
      allow(FeatureFlipper)
        .to receive(:show_feature?)
        .and_call_original
      allow_any_instance_of(Person)
        .to receive(:cable_auth_token)
        .and_return(cable_auth_token)
      allow(FeatureFlipper).to receive(:show_feature?).with(:adyen_integration, person).and_return(true)
      settlement_count = successful_refund.refund_settlements.count

      Adyen::UpdateAdyenRefundTransactionService.new(
        {
          "notificationItems" =>
              [
                {
                  "NotificationRequestItem" =>
                       {
                         "paymentMethod" => "cup",
                         "additionalData" => {
                           "checkoutSessionId" => "CS2B109FC3C3AC5CE4",
                           "recurring.recurringDetailReference" => "SCFWXHMMD6KXWD83"
                         },
                         "pspReference" => "J9QQTFTHB8NKGK83",
                         "success" => "true",
                         "amount" => {
                           "currency" => "EUR",
                           "value" => 3509
                         },
                         "reason" => "sample-reason"
                       }
                }
              ]
        }
      ).update_transaction

      successful_refund.reload
      expect(successful_refund.refund_settlements.count).to eq(settlement_count)
    end
  end
end
