require "rails_helper"

RSpec.describe AddressLockedCountry::Finder, type: :service do
  let!(:person) { create(:person) }

  describe ".call" do
    context "payoneer kyc completed user" do
      it "returns kyc country" do
        reason = FlagReason
                 .address_lock_flag_reasons
                 .find_by(reason: FlagReason::PAYONEER_KYC_COMPLETED)
        person.lock_address!(reason: reason)

        subject = described_class.call(person)

        expect(subject).to eq(person.kyc_country)
      end
    end

    context "self identified address locked user" do
      it "returns kyc country" do
        reason = FlagReason
                 .address_lock_flag_reasons
                 .find_by(reason: FlagReason::SELF_IDENTIFIED_ADDRESS_LOCKED)
        person.lock_address!(reason: reason)

        subject = described_class.call(person)

        expect(subject).to eq(person.self_identified_country)
      end
    end

    context "person without address_lock" do
      it "raises AddressLockNotFoundError" do
        expect { described_class.call(person) }
          .to raise_error(
            AddressLockedCountry::Exceptions::AddressLockNotFoundError,
            "Address lock not found for person"
          )
      end
    end

    context "country not mapped to flag_reason" do
      it "raises CountryNotAssignedError" do
        flag = Flag.find_by(Flag::ADDRESS_LOCKED)
        new_reason = create(:flag_reason, flag: flag, reason: "new_reason")
        person.lock_address!(reason: new_reason)

        expect { described_class.call(person) }
          .to raise_error(
            AddressLockedCountry::Exceptions::CountryNotMappedError,
            "Country is not mapped for reason - #{new_reason.reason}"
          )
      end
    end
  end
end
