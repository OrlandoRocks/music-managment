require "rails_helper"

describe "BulkSongFinder" do
  describe "#execute" do
    context "when finding by song" do
      context "when given a properly spaced list of comma separated song IDs" do
        it "returns the songs with the given IDs" do
          song1 = create(:song)
          song2 = create(:song)
          song3 = create(:song)

          song_ids = "#{song1.id}, #{song2.id}"
          bulk_song_finder = BulkSongFinder.new(song_ids)
          expect(bulk_song_finder.execute).to match_array([song1, song2])
        end
      end

      context "when given an improperly spaced list of comma separated song IDs" do
        it "returns the songs with the given IDs" do
          song1 = create(:song)
          song2 = create(:song)
          song3 = create(:song)

          song_ids = "#{song1.id},#{song2.id}"
          bulk_song_finder = BulkSongFinder.new(song_ids)
          expect(bulk_song_finder.execute).to match_array([song1, song2])
        end
      end

      context "when given a list of not comma separated song IDs" do
        it "returns the songs with the given IDs" do
          song1 = create(:song)
          song2 = create(:song)
          song3 = create(:song)

          song_ids = "#{song1.id} #{song2.id}"
          bulk_song_finder = BulkSongFinder.new(song_ids)
          expect(bulk_song_finder.execute).to match_array([song1, song2])
        end
      end

      context "when given a list of ISRC numbers" do
        it "returns the songs with at least one of the given isrc numbers" do
          song1 = create(:song)
          song2 = create(:song)
          song3 = create(:song)

          song_isrcs = "#{song1.isrc}, #{song2.isrc}"
          bulk_song_finder = BulkSongFinder.new(song_isrcs)
          expect(bulk_song_finder.execute).to match_array([song1, song2])
        end
      end

      context "when given a list that includes an song that does not exist" do
        it "sets an error message" do
          song1 = create(:song)
          song2 = create(:song)
          song3 = create(:song)

          song_ids = "#{song1.id}, 0"
          bulk_song_finder = BulkSongFinder.new(song_ids)
          expect(bulk_song_finder.execute).to match_array([song1])
          expect(bulk_song_finder.messages).to eq({"0" => "Song with ID of 0 or song ISRC of 0 not found."})
        end
      end
    end

    context "when finding by album" do
      context "when given a properly spaced list of comma separated album IDs" do
        it "returns the songs of the albums with the given IDs" do
          album1 = create(:album)
          album2 = create(:album)
          album3 = create(:album)
          song1 = create(:song, album: album1)
          song2 = create(:song, album: album1)
          song3 = create(:song, album: album2)
          song4 = create(:song, album: album2)
          song5 = create(:song, album: album3)
          song6 = create(:song, album: album3)

          album_ids = "#{album1.id}, #{album2.id}"
          bulk_song_finder = BulkSongFinder.new(album_ids, find_by: 'album')
          expect(bulk_song_finder.execute).to match_array([song1, song2, song3, song4])
        end
      end

      context "when given an improperly spaced list of comma separated song IDs" do
        it "returns the songs of the albums with the given IDs" do
          album1 = create(:album)
          album2 = create(:album)
          album3 = create(:album)
          song1 = create(:song, album: album1)
          song2 = create(:song, album: album1)
          song3 = create(:song, album: album2)
          song4 = create(:song, album: album2)
          song5 = create(:song, album: album3)
          song6 = create(:song, album: album3)

          album_ids = "#{album2.id},#{album3.id}"
          bulk_song_finder = BulkSongFinder.new(album_ids, find_by: 'album')
          expect(bulk_song_finder.execute).to match_array([song3, song4, song5, song6])
        end
      end

      context "when given a list of not comma separated song IDs" do
        it "returns the songs of the albums with the given IDs" do
          album1 = create(:album)
          album2 = create(:album)
          album3 = create(:album)
          song1 = create(:song, album: album1)
          song2 = create(:song, album: album1)
          song3 = create(:song, album: album2)
          song4 = create(:song, album: album2)
          song5 = create(:song, album: album3)
          song6 = create(:song, album: album3)

          album_ids = "#{album1.id} #{album3.id}"
          bulk_song_finder = BulkSongFinder.new(album_ids, find_by: 'album')
          expect(bulk_song_finder.execute).to match_array([song1, song2, song5, song6])
        end
      end

      context "when given a list of UPC numbers" do
        it "returns the songs with at least one song with one of the given isrc numbers" do
          album1 = create(:album)
          album2 = create(:album)
          album3 = create(:album)
          upc1 = create(:tunecore_upc, upcable: album1)
          upc2 = create(:tunecore_upc, upcable: album2)
          upc3 = create(:tunecore_upc, upcable: album3)
          song1 = create(:song, album: album1)
          song2 = create(:song, album: album1)
          song3 = create(:song, album: album2)
          song4 = create(:song, album: album2)
          song5 = create(:song, album: album3)
          song6 = create(:song, album: album3)

          album_upcs = "#{upc2.number}, #{upc3.number}"
          bulk_song_finder = BulkSongFinder.new(album_upcs, find_by: 'album')
          expect(bulk_song_finder.execute).to match_array([song3, song4, song5, song6])
        end
      end

      context "when given a list that includes an album that does not exist" do
        it "sets an error message" do
          album1 = create(:album)
          album2 = create(:album)
          song1 = create(:song, album: album1)
          song2 = create(:song, album: album1)
          song3 = create(:song, album: album2)
          song4 = create(:song, album: album2)

          album_ids = "#{album1.id}, 0"
          bulk_song_finder = BulkSongFinder.new(album_ids, find_by: 'album')
          expect(bulk_song_finder.execute).to match_array([song1, song2])
          expect(bulk_song_finder.messages).to eq({"0" => "Song with album ID of 0 or album UPC of 0 not found."})
        end
      end
    end
  end
end
