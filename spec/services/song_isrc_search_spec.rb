require "rails_helper"

describe SongIsrcSearch do
  let(:song1) { create(:song) }
  let(:song2) { create(:song) }

  context "given a comma delimited string of isrcs" do
    it "formats the isrcs correctly and returns the correct results" do
      isrc_string = "#{song1.isrc}, #{song2.isrc}"
      results = SongIsrcSearch.search(isrc_string)
      expect(results).to match_array([song1, song2])
    end
  end

  context "given a space delimited string of isrcs" do
    it "formats the isrcs correctly and returns the correct results" do
      isrc_string = "#{song1.isrc} #{song2.isrc}"
      results = SongIsrcSearch.search(isrc_string)
      expect(results).to match_array([song1, song2])
    end
  end

  context "given an array of isrcs" do
    it "formats the isrcs correctly and returns the correct results" do
      isrc_array = [song1.isrc, song2.isrc]
      results = SongIsrcSearch.search(isrc_array)
      expect(results).to match_array([song1, song2])
    end
  end
end
