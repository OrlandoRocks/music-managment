require "rails_helper"

describe MassMonetizationBlockService do
  describe ".block" do
    context "when the legacy ytsr infrastructure is disabled" do
      it "adds TrackMonetizationBlockers to each song for each store" do
        song_1 = create(:song)
        song_2 = create(:song)

        song_ids = [song_1.id, song_2.id]
        store_ids = Store::TRACK_MONETIZATION_STORES

        MassMonetizationBlockService.block(song_ids: song_ids, store_ids: store_ids)

        expect(song_1.reload.track_monetization_blockers.count).to eq(store_ids.length)
        expect(song_2.reload.track_monetization_blockers.count).to eq(store_ids.length)
      end
    end

    context "when the songs are already blocked" do
      it "does not create any new blockers" do
        song_1 = create(:song)
        song_2 = create(:song)

        store = create(:store)

        create(:track_monetization_blocker, song: song_1, store_id: store.id)
        create(:track_monetization_blocker, song: song_2, store_id: store.id)

        track_monetization_blocker_count = TrackMonetizationBlocker.count

        expect(track_monetization_blocker_count).to_not eq(0)

        song_ids = [song_1.id, song_2.id]
        store_ids = [store.id]

        MassMonetizationBlockService.block(song_ids: song_ids, store_ids: store_ids)

        expect(TrackMonetizationBlocker.count).to eq(track_monetization_blocker_count)
      end
    end

    context "when an admin and ip address is passed in" do
      it "creates notes for each song that has been blocked for each store" do
        admin = create(:person, :admin)
        ip_address = "127.0.0.1"

        song_1 = create(:song)
        song_2 = create(:song)

        store_1 = create(:store)
        store_2 = create(:store)

        song_ids = [song_1.id, song_2.id]
        store_ids = [store_1.id, store_2.id]

        MassMonetizationBlockService.block(
          song_ids: song_ids,
          store_ids: store_ids,
          admin: admin,
          ip_address: ip_address
        )

        expect(song_1.album.notes.count).to eq(store_ids.length)
        expect(song_2.album.notes.count).to eq(store_ids.length)
      end

      it "includes the name of the store and isrc in the description of each song's blocker notes" do
        admin = create(:person, :admin)
        ip_address = "127.0.0.1"

        song = create(:song)

        store = create(:store)

        song_ids = [song.id]
        store_ids = [store.id]

        MassMonetizationBlockService.block(
          song_ids: song_ids,
          store_ids: store_ids,
          admin: admin,
          ip_address: ip_address
        )

        note_description = song.album.notes.first.note

        expect(note_description).to include(store.name)
        expect(note_description).to include(song.isrc)
      end
    end
  end
end
