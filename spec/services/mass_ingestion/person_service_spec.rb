require "rails_helper"

describe MassIngestion::PersonService do
  describe ".ingest" do

    context "tunecore optin email matches existing person's email" do
      it "updates the person's referral campaign" do
        filepath  = Rails.root.join("spec", "fixtures", "mass_ingestion", "accounts.csv").to_s
        source    = "stem"
        person    = create(:person, email: "bird@paddys.pub")

        expect(person.referral_campaign).to be_nil
        MassIngestion::PersonService.ingest(filepath, source)
        expect(person.reload.referral_campaign).to eq "tc-stem"
      end
    end

    context "source email matches existing person's email" do
      it "updates the person's referral campaign" do
        filepath  = Rails.root.join("spec", "fixtures", "mass_ingestion", "accounts.csv").to_s
        source    = "stem"
        person    = create(:person, email: "PepeSilvia@wild.card")

        expect(person.referral_campaign).to be_nil
        MassIngestion::PersonService.ingest(filepath, source)
        expect(person.reload.referral_campaign).to eq "tc-stem"
      end
    end

    context "neither the source email or tunecore optin email matches an existing person's email" do
      it "creates a new person" do
        filepath  = Rails.root.join("spec", "fixtures", "mass_ingestion", "accounts.csv").to_s
        source    = "stem"
        create(:person, email: "bird@paddys.pub")
        create(:person, email: "PepeSilvia@wild.card")

        expect(Person.exists?(email: "imsorry@rum.ham")).to be_falsey
        MassIngestion::PersonService.ingest(filepath, source)
        expect(Person.exists?(email: "imsorry@rum.ham")).to be_truthy
      end

      it "verifies the person" do
        filepath = Rails.root.join("spec", "fixtures", "mass_ingestion", "accounts.csv").to_s
        source   = "stem"
        email    = "PepeSilvia@wild.card"

        expect(Person.where(email: email)).not_to exist
        MassIngestion::PersonService.ingest(filepath, source)
        new_person = Person.where(email: email).first
        expect(new_person.is_verified).to be_truthy
      end

      context "when the name is less than 5 characters long" do
        it "adds spaces to the name until it reaches 5 characters" do
          filepath = Rails.root.join("spec", "fixtures", "mass_ingestion", "accounts.csv").to_s
          source   = "stem"
          email    = "dee@rum.ham"

          MassIngestion::PersonService.ingest(filepath, source)
          new_person = Person.find_by(email: email)
          expect(new_person.name).to eq "Dee  "
        end
      end
    end
  end
end
