require "rails_helper"
include StemCSVMockHelper

def get_stem_csv_parser(rows = [])
  mock_rows = rows.map {|r| stem_mock_csv_row(r) }
  mock_csv_io = stem_mock_csv_io(mock_rows)
  MassIngestion::StemCsvParser.new(mock_csv_io)
end

describe MassIngestion::StemCsvParser do
  describe "#person" do
    context "When there is no user matching tunecore_optin_email_address" do
      it "should parse and lookup user based on stem_account_email_address." do
        tunecore_optin_email_address = "tunecore_optin@example.com"
        stem_account_email_address = "stem_user@example.com"
        person = create(:person, email: stem_account_email_address)
        row1 = {
          tunecore_optin_email_address: tunecore_optin_email_address,
          stem_account_email_address: stem_account_email_address
        }
        parser = get_stem_csv_parser([row1])

        expect(Person.exists?(email: tunecore_optin_email_address)).to be false
        expect(parser.person).to eq(person)
      end
    end
  end

  describe "#primary_genre_id" do
    context "When there is no genre specified" do
      it "should set 'Rock' as the default genre." do
        rock_genre = Genre.find_by(name: "Rock")
        parser = get_stem_csv_parser([{genre: ""}])

        album_attributes = parser.album_attributes
        expect(album_attributes[:primary_genre_id]).to eq(rock_genre.id)
      end
    end

    scenario "When STEM passes genre='Christian & Gospel' it should return id for 'Christian/Gospel'" do
      christian_gospel_genre = Genre.find_by(name: "Christian/Gospel")
      parser = get_stem_csv_parser([{genre: "Christian & Gospel"}])

      album_attributes = parser.album_attributes
      expect(album_attributes[:primary_genre_id]).to eq(christian_gospel_genre.id)
    end
  end

  describe "#apple_music" do
    it "should set apple music to true" do
      parser = get_stem_csv_parser([{apple_music: true}])

      album_attributes = parser.album_attributes
      expect(album_attributes[:apple_music]).to be true
    end
  end

  describe "#language_code" do
    context "When there is a language specified" do
      it "should parse and return the language_code" do
        language_code = "fr"
        parser = get_stem_csv_parser([{album_language: language_code}])

        expect(parser.language_code).to eq(language_code)
      end

      it "should return language_code as 'cmn-Hant' if 'zh' was supplied" do
        language_code = "zh"
        parser = get_stem_csv_parser([{album_language: language_code}])

        expect(parser.language_code).to eq("cmn-Hant")
      end
    end
  end

  describe "#album_creatives" do
    context "When there are creatives for an album" do
      it "should parse and return the creatives as primary_artist." do
        album_artist = "Artist 1:Artist 2"
        album_role = "primary:featured"
        parser = get_stem_csv_parser([{album_artist: album_artist, album_role: album_role}])
        album_creatives = parser.album_creatives

        expect(album_creatives.length).to eq(1)
        expect(album_creatives[0]).to eq({name: "Artist 1", role: "primary_artist"}.stringify_keys)
      end
    end
  end

  describe "#artist_urls" do
    context "When there are creatives with spotify/itunes id specified" do
      it "should parse and return the spotify/itunes id" do
        artist_1 = "Artist 1"
        artist_2 = "Artist 2"
        artist_3 = "Artist 3"
        album_artist = [artist_1, artist_2, artist_3].join(":")
        album_role = "primary:primary:primary"

        apple_artist_id2 = "63346553"
        apple_artist_id = [nil, apple_artist_id2, nil].join(":")

        spotify_artist_id3 = "73sIBHcqh3Z3NyqHKZ7FOL"
        spotify_artist_id = [nil, nil, spotify_artist_id3].join(":")

        row1 = {
          album_artist: album_artist,
          album_role: album_role,
          apple_artist_id: apple_artist_id,
          spotify_artist_id: spotify_artist_id}
        parser = get_stem_csv_parser([row1])
        artist_urls = parser.artist_urls

        expect(artist_urls.length).to eq(3)
        expect(artist_urls[0]).to eq({artist_name: artist_1, apple: "", spotify: ""})
        expect(artist_urls[1]).to eq({artist_name: artist_2, apple: MassIngestion::StemCsvParser::APPLE_ID_URL_BASE.(apple_artist_id2), spotify: ""})
        expect(artist_urls[2]).to eq({artist_name: artist_3, apple: "", spotify: MassIngestion::StemCsvParser::SPOTIFY_ID_URL_BASE.(spotify_artist_id3)})
      end
    end
  end

  describe "#album_types" do
    it "should return Single/Album based on the csv rows" do
      parser = get_stem_csv_parser([{album_artist: "Art1"}])
      expect(parser.album_type).to eq('Single')

      parser = get_stem_csv_parser([{album_artist: "Art1"}, {album_artist: "Art1"}])
      expect(parser.album_type).to eq('Album')
    end
  end

  describe "#song_data_form_artist" do
    context "Album" do
      it "should not return track artist if they are also primary at Album level" do
        row = {
          album_artist: "SPB",
          album_role: "primary",
          track_artist: "SPB:Hariharan",
          track_role: "primary:featured"
        }
        parser = get_stem_csv_parser([row, row])

        track_artist_expectation = {
          artist_name: "Hariharan",
          associate_to: "Song",
          credit: "featuring",
          role_ids: []}

        expect(parser.song_data_form_artists(parser.csv_rows[0])).to eq([track_artist_expectation])
      end

      it "should not return the track artist if they are featured in the track level but primary at the Album level" do
        row = {
          album_artist: "Art 1:Art 2",
          album_role: "primary:primary",
          track_artist: "Art 1:Art 3",
          track_role: "featured:featured"
        }
        parser = get_stem_csv_parser([row, row])

        track_artist_expectation = {
          artist_name: "Art 3",
          associate_to: "Song",
          credit: "featuring",
          role_ids: []}

        expect(parser.song_data_form_artists(parser.csv_rows[0])).to eq([track_artist_expectation])
      end

      it "should return track artist if they are primary only the track level" do
        row = {
          album_artist: "Art 1:Art 2",
          album_role: "primary:primary",
          track_artist: "Bruno Mars:Childish Gambino",
          track_role: "primary:featured"
        }

        track_artists_expectation = [
          {
            artist_name: "Bruno Mars",
            associate_to: "Song",
            credit: "primary_artist",
            role_ids: []
          },
          {
            artist_name: "Childish Gambino",
            associate_to: "Song",
            credit: "featuring",
            role_ids: []
          }
        ]

        parser = get_stem_csv_parser([row, row])

        expect(parser.song_data_form_artists(parser.csv_rows[0])).to eq(track_artists_expectation)
      end

      it "should set song_role_id with credit contributor if they happen to be a songwriter
          and they are primary artist at the Album level" do
        row = {
          album_artist: "Rihanna:Taylor Swift",
          album_role: "primary:primary",
          track_artist: "Adele",
          track_role: "featured",
          songwriter: "Cardi B"
        }
        songwriter_song_role_id = SongRole.find_by(role_type: "songwriter").id
        track_artists_expectation = [
          {
            artist_name: "Adele",
            associate_to: "Song",
            credit: "featuring",
            role_ids: []
          },
          {
            artist_name: "Cardi B",
            associate_to: "Song",
            credit: "contributor",
            role_ids: [songwriter_song_role_id]
          }
        ]
        parser = get_stem_csv_parser([row, row])

        expect(parser.song_data_form_artists(parser.csv_rows[0])).to eq(track_artists_expectation)
      end

      it "should set song_role_id with credit featuring if they are songwriter and
        they are featuring artist at track level" do
        row = {
          album_artist: "Eminem",
          album_role: "primary",
          track_artist: "Tupac",
          track_role: "featured",
          songwriter: "Tupac"
        }
        songwriter_song_role_id = SongRole.find_by(role_type: "songwriter").id
        track_artists_expectation = [
          {
            artist_name: "Tupac",
            associate_to: "Song",
            credit: "featuring",
            role_ids: [songwriter_song_role_id]
          }
        ]
        parser = get_stem_csv_parser([row, row])

        expect(parser.song_data_form_artists(parser.csv_rows[0])).to eq(track_artists_expectation)
      end

      it "should set song_role_id with credit primary if they are songwriter and
        they are featuring artist at track level" do
        row = {
          album_artist: "Eminem",
          album_role: "primary",
          track_artist: "Tupac",
          track_role: "primary",
          songwriter: "Tupac"
        }
        songwriter_song_role_id = SongRole.find_by(role_type: "songwriter").id
        track_artists_expectation = [
          {
            artist_name: "Tupac",
            associate_to: "Song",
            credit: "primary_artist",
            role_ids: [songwriter_song_role_id]
          }
        ]
        parser = get_stem_csv_parser([row, row])

        expect(parser.song_data_form_artists(parser.csv_rows[0])).to eq(track_artists_expectation)
      end
    end

    context "Single" do
      it "should return all the creatives at the track level since they will all be moved to the album level" do
        artists = "SPB:Hariharan:Unni"
        roles = "primary:featured:primary"
        songwriter = "Pa vijay"

        row = {
          album_artist: artists,
          album_role: roles,
          track_artist: artists,
          track_role: roles,
          songwriter: songwriter
        }

        songwriter_song_role_id = SongRole.find_by(role_type: "songwriter").id
        track_artists_expectation = [
          {
            artist_name: "SPB",
            associate_to: "Song",
            credit: "primary_artist",
            role_ids: []
          },
          {
            artist_name: "Hariharan",
            associate_to: "Song",
            credit: "featuring",
            role_ids: []
          },
          {
            artist_name: "Unni",
            associate_to: "Song",
            credit: "primary_artist",
            role_ids: []
          },
          {
            artist_name: "Pa vijay",
            associate_to: "Song",
            credit: "contributor",
            role_ids: [songwriter_song_role_id]
          }
        ]

        parser = get_stem_csv_parser([row])
        expect(parser.song_data_form_artists(parser.csv_rows[0])).to eq(track_artists_expectation)
      end

      it "should assign single_role_id to the existing creative attrs if they are either primary or feature and also songwriter" do
        artists = "Hariharan:Unni"
        roles = "featured:primary"
        songwriter = "Hariharan:Pa vijay"

        row = {
          album_artist: artists,
          album_role: roles,
          track_artist: artists,
          track_role: roles,
          songwriter: songwriter
        }

        songwriter_song_role_id = SongRole.find_by(role_type: "songwriter").id
        track_artists_expectation = [
          {
            artist_name: "Hariharan",
            associate_to: "Song",
            credit: "featuring",
            role_ids: [songwriter_song_role_id]
          },
          {
            artist_name: "Unni",
            associate_to: "Song",
            credit: "primary_artist",
            role_ids: []
          },
          {
            artist_name: "Pa vijay",
            associate_to: "Song",
            credit: "contributor",
            role_ids: [songwriter_song_role_id]
          }
        ]

        parser = get_stem_csv_parser([row])
        expect(parser.song_data_form_artists(parser.csv_rows[0])).to eq(track_artists_expectation)
      end
    end
  end

  describe "#available_stores" do
    it "should return respective stores." do
      parser = get_stem_csv_parser([{album_title: 'Coffee Before Talkie'}])

      iTunesWW_store = Store.find_by(short_name: 'iTunesWW')
      iTunes_all_stores_expect_iTunesWW = Store.where('short_name like ?', '%iTunes%')
                                            .where('short_name != ?', 'iTunesWW')
      beliveLiv_store = Store.find_by(short_name: 'BelieveLiv')

      stores = parser.available_stores

      # should include BeliveLiv even though if it is not active.
      expect(stores).to include(beliveLiv_store)

      # should include only iTunesWW for iTunes
      expect(stores).to include(iTunesWW_store)
      expect(stores).not_to include(*iTunes_all_stores_expect_iTunesWW)

      expect(MassIngestion::StemCsvParser::STORES_TO_BE_SKIPPED).to contain_exactly("AmazonOD", "YoutubeSR", "FBTracks", "Instagram")
      expect(stores).not_to include(MassIngestion::StemCsvParser::STORES_TO_BE_SKIPPED)

      expect((stores - [beliveLiv_store]).map(&:is_active).inject(:&)).to be true
    end

    it "should not return 'Shazam' if primary_genre=spoken_word" do
      shazam_store = Store.find_by(short_name: "Shazam")
      parser1 = get_stem_csv_parser([{genre: "Alternative"}])
      parser2 = get_stem_csv_parser([{genre: "Spoken Word"}])

      expect(parser1.available_stores).to include(shazam_store)
      expect(parser2.available_stores).not_to include(shazam_store)
    end
  end

  describe "#variable_price_for_store" do
    let(:parser) { get_stem_csv_parser([{album_title: 'Coffee Before Talkie'}]) }

    context "store without variable price" do
      it "should return nil" do
        non_itunes_amazon = Store.where.not(short_name: ['iTunesWW', 'Amazon'])
        variable_price = parser.variable_price_for_store(non_itunes_amazon.first)

        expect(variable_price).to be_nil
      end
    end

    context "store with variable price" do
      context "invalid variable pricing value" do
        it "should return nil" do
          parser.first_csv_row[:variable_pricing] = "vertical"

          iTunesWW_store = Store.find_by(short_name: 'iTunesWW')

          variable_price = parser.variable_price_for_store(iTunesWW_store)

          expect(variable_price).to be_nil
        end
      end

      context "valid variable pricing value" do
        it "should return mapped variable price record" do
          parser.first_csv_row[:variable_pricing] = "front"
          iTunesWW_store = Store.find_by(short_name: 'iTunesWW')

          mapped_price_code = MassIngestion::StemCsvParser::STORE_VARIABLE_PRICE_MAP["iTunesWW"]["front"]
          returned_price = parser.variable_price_for_store(iTunesWW_store)

          expect(returned_price.price_code).to eq(mapped_price_code)
        end
      end
    end
  end

  describe "#countries" do
    it "should return all available countries if territory is 'WW'" do
      parser = get_stem_csv_parser([{territory: "WW"}])
      expect(parser.countries).to contain_exactly(*Country.available_countries)
    end

    it "should return only the available countries and remove sanctioned countries from territory" do
      parser = get_stem_csv_parser([{territory: "IN:US:CA:IR"}])

      # IR is excluded because it is a sanctioned country
      expect(parser.countries.map(&:iso_code)).to contain_exactly("IN", "US", "CA")
    end
  end

  describe "#is_various?" do
    subject { get_stem_csv_parser([{album_artist: "Various Artists"}]) }
    it { should be_is_various }

    context "When album_artist is not 'Various Artists'" do
      subject { get_stem_csv_parser([{album_artist: "Ed Sheeran"}]) }
      it { should_not be_is_various }
    end
  end
end
