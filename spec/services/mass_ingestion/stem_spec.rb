require "rails_helper"

describe MassIngestion::Stem do
  describe "Constants" do
    it "should define free trial days" do
      expect(MassIngestion::Stem::FREE_TRIAL_DAYS).to eq(90)
    end
  end

  describe ".free_trial_days_left" do
    it "should return free trial days + rollout date" do
      allow(MassIngestion::Stem).to receive(:rollout_date).and_return(Date.today)

      expect(MassIngestion::Stem.free_trial_days_left).to eq(90)
    end
  end

  describe ".rollout_date" do
    context "ROLLOUT_DATE is defined in ENV" do
      it "should return the date in ENV" do
        stub_const('ENV', ENV.to_hash.merge("STEM_ROLLOUT_DATE" => "12-May-2019"))

        expect(MassIngestion::Stem.rollout_date.to_s).to eq('2019-05-12')
      end
    end

    context "ROLLOUT_DATE is not defined in ENV" do
      it "should return hardcoded 29 july" do
        stub_const('ENV', ENV.to_hash.merge("STEM_ROLLOUT_DATE" => nil))

        expect(MassIngestion::Stem.rollout_date.to_s).to eq('2019-07-29')
      end
    end
  end
end
