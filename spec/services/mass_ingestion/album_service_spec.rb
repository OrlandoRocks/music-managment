require "rails_helper"
include StemCSVMockHelper
ENV["STEM_ROLLOUT_DATE"] ||= Date.current.strftime("%d-%m-%Y")

shared_context "setup_csv_and_audio_file_mocks" do
  before do
    @bucket_name = "dummy_bucket_name"
    @bucket_album_dir = "bucket_prefix/#{@album_attributes[:upc]}"
    @mock_csv_io = stem_mock_csv_io(@rows)
    @album_service = MassIngestion::AlbumService.new(@bucket_name, @bucket_album_dir)

    allow_any_instance_of(S3Asset).to receive(:set_asset_metadata!)
    allow_any_instance_of(Scrapi::CreateJobWorker).to receive(:perform)
    allow(@album_service).to receive(:logger).and_return(spy(:logger))
    allow(@album_service).to receive(:download).and_return(true)
    allow(@album_service).to receive(:artwork_file).and_return(File.open(File.join(Rails.root, "spec/files/albumcover.jpg")))
    allow(RestClient::Request).to receive(:execute) do
      {
        "response" => {
          "key" => Faker::Lorem.unique.word,
          "uuid" => "s3_uuid",
          "bucket" => "s3_bucket",
          "orig_filename" => "asdf-#{Faker::Lorem.word}",
          "bit_rate" => "45000"
        }
      }.to_json
    end

    allow_any_instance_of(Song).to receive(:create_and_store_content_fingerprint!).and_return(true)
    allow(@album_service).to receive(:csv_file) { @mock_csv_io }
    temp_audio_files = @rows.map { |r| Tempfile.new([Faker::Lorem.word, ".wav"])}
    allow(@album_service).to receive(:audio_files).and_return(temp_audio_files)
  end

  let(:expected_country_iso_codes) { Country.unsanctioned.map(&:iso_code) }
end

shared_examples "failed album ingestion" do
  before do
    allow_any_instance_of(S3Asset).to receive(:set_asset_metadata!)
    expect(@album_service).to receive(:status_log).with("failed").at_least(:once)
    expect(@album_service).to receive(:remove_files_in_upc_download_dir).twice
    trace_log = spy('trace_log')
    allow(@album_service).to receive(:trace_log).and_return(trace_log)
    expect(@album_service).to receive(:trace_log).with("boom").at_least(:once)

  end

  xit "should record status and clean Album and Artwork" do
    expect { @album_service.process }
      .to not_change{klass.count}
      .and not_change{Artwork.count}
      .and not_change{Creative.count}
      .and not_change{Upc.count}
      .and not_change{Song.count}
      .and not_change{Salepoint.count}

    mass_ingestion_status = MassIngestionStatus.failed.where(identifier: @album_service.upc_str, identifier_type: 'UPC').first
    expect(mass_ingestion_status).to be_present
    info = JSON.parse(mass_ingestion_status.info)
    expect(info).to eq({"bucket_name" => @bucket_name,
                        "bucket_album_dir" => @bucket_album_dir,
                        "parser" => @album_service.parser.class.to_s,
                        "error" => "boom",
                        "error_message" => "boom"})
  end
end

shared_examples "album/single attributes" do
  before do
    allow_any_instance_of(S3Asset).to receive(:set_asset_metadata!)
  end
  xit "should parse and save the attributes from the CSV" do
    expect(@album_service).to receive(:status_log).with("successful").at_least(:once)
    expect(@album_service).to receive(:remove_files_in_upc_download_dir).twice

    expect { @album_service.process }.to change{klass.count}.by(1)

    #album/single atrributes
    resource = klass.find_by(name: @title)
    resource.reload
    expect(resource.person).to eq(@person)
    expect(resource.label_name).to eq(@label_name)
    expect(resource.orig_release_date.strftime(@stem_date_format)).to eq(@original_release)
    expect(resource.golive_date.strftime(@stem_date_format)).to eq(MassIngestion::StemCsvParser::GOLIVE_DATE_STR)
    expect(resource.sale_date).to eq(MassIngestion::StemCsvParser::GOLIVE_DATE)
    expect(resource.language_code).to eq(@language_name)
    expect(resource.source).to eq(MassIngestion::StemCsvParser::SOURCE)
    expect(resource.primary_genre).to eq(Genre.find_by(name: @genre))
    expect(resource.album_type).to eq(klass.to_s)
    expect(resource.countries.map(&:iso_code)).to contain_exactly(*expected_country_iso_codes)
    expect(resource.apple_music).to be true

    #upc
    expect(resource.upcs.count).to eq(2)
    expect(resource.tunecore_upc).to be_present
    expect(resource.optional_upc.number).to eq(@upc)

    #stores
    expect(@album_service.parser.available_stores.select(&:is_active).length).to be > 1
    expect(resource.stores.map(&:short_name)).to contain_exactly(*@album_service.parser.available_stores.map(&:short_name))

    #artwork
    expect(resource.artwork).to be_persisted
    expect(resource.artwork).to be_uploaded

    #note
    expect(Note.where(subject: "Ingested for STEM", related_id: resource.id, note_created_by_id: @person.id)).to exist

    #product association
    product_ids = {
      "Single" => Product::US_ONE_YEAR_SINGLE_PRODUCT_ID,
      "Album" => Product::US_ONE_YEAR_ALBUM_PRODUCT_ID
    }
    expect(resource.renewals.length).to be 1
    expect(resource.renewal_histories.length).to be 1
    expect(resource.renewal_histories[0].expires_at.to_date).to eq MassIngestion::Stem.free_trial_end_date

    expect(resource.purchases.length).to be 1
    expect(resource.purchases[0].invoice).to be_present
    expect(resource.purchases[0].product_id).to eq(product_ids[resource.album_type])
    expect(resource.purchases[0].price_adjustment_histories[0].note).to eq(MassIngestion::StemCsvParser::PRICE_ADJUSTMENT_TEXT)
    expect(resource.legal_review_state).to eq("NEEDS REVIEW")

    #mass_ingestion_status
    mass_ingestion_status = MassIngestionStatus.successful.where(identifier: @album_service.upc_str, identifier_type: 'UPC').first
    expect(mass_ingestion_status).to be_present
    info = JSON.parse(mass_ingestion_status.info)
    expect(info).to eq({
                         "bucket_name" => @bucket_name,
                         "bucket_album_dir" => @bucket_album_dir,
                         "parser" => @album_service.parser.class.to_s})
  end

  xit "should set variable price id from parser" do
    itunes_store = Store.find_by(short_name: "iTunesWW")
    test_variable_price = itunes_store.variable_prices.first

    # Stubbing stores to not have all stores be processed and also for salepoint validation.
    allow_any_instance_of(MassIngestion::StemCsvParser)
      .to receive(:available_stores)
            .and_return([itunes_store])

    allow_any_instance_of(MassIngestion::StemCsvParser)
      .to receive(:variable_price_for_store)
            .and_return(test_variable_price)

    @album_service.process

    resource = klass.find_by(name: @title)
    expect(resource.salepoints.map(&:variable_price_id).uniq).to eq([test_variable_price.id])
  end

  describe "When an exception happens inside transaction block" do
    before do
      #create_note is called inside the transaction block
      expect(@album_service).to receive(:create_note).and_raise("boom")
    end
    it_behaves_like "failed album ingestion"
  end

  describe "When an exception happens outside trasaction block" do
    before do
      #create_artwork is called outside the transaction block
      expect(@album_service).to receive(:create_artwork).and_raise("boom")
    end
    it_behaves_like "failed album ingestion"
  end

  ["US", "UK", "CA", "IT", "DE", "FR"].each do |country_domain|
    xit "should select the appropriate product for #{country_domain}" do
      @person.country_website_id = CountryWebsite.find_by(country: country_domain).id
      @person.save!

      expect(@person.reload.country_domain).to eq(country_domain)
      expect { @album_service.process }
        .to change{klass.count}.by(1)

      resource = @album_service.resource
      expect(resource.purchases[0].product_id).to eq(@products[resource.album_type][country_domain])
    end
  end
end

describe MassIngestion::AlbumService do
  before do
    allow(Person::AfterCommitCallbacks).to receive(:send_to_believe).and_return(true)
    allow(Person::AfterCommitCallbacks).to receive(:send_to_zendesk).and_return(true)
    allow(Apple::ArtistIdValidator).to receive(:new).and_return(double(valid?: true))
    allow_any_instance_of(S3Asset).to receive(:set_asset_metadata!)
    stub_request(:any, /https:\/\/s3.amazonaws.com/).to_return(status: 200, body: "", headers: {}) #artwork s3 stub

    @email_address = "testuser@example.com"
    @person = create(:person, email: @email_address)
    @title = "Black water"
    @label_name = "Another label"
    @original_release = "09/05/2004"
    @upc = "5033319000183"
    @language_name = "fr"
    @genre = "Pop"
    @stem_date_format = MassIngestion::StemCsvParser::DATE_FORMAT

    @artist1 = {
      name: "Mouse",
      apple_artist_id: "63346553",
      spotify_artist_id: nil,
      role: 'primary'
    }
    @artist2 = {
      name: "Keyboard",
      apple_artist_id: nil,
      spotify_artist_id: "73sIBHcqh3Z3NyqHKZ7FOL",
      role: 'primary'
    }
    @artist3 = {
      name: "Mug",
      apple_artist_id: nil,
      spotify_artist_id: nil,
      role: 'featured'
    }

    @products = {
      "Album" => {
        "AU" => Product::AU_ONE_YEAR_ALBUM_PRODUCT_ID,
        "CA" => Product::CA_ONE_YEAR_ALBUM_PRODUCT_ID,
        "DE" => Product::DE_ONE_YEAR_ALBUM_PRODUCT_ID,
        "FR" => Product::FR_ONE_YEAR_ALBUM_PRODUCT_ID,
        "IT" => Product::IT_ONE_YEAR_ALBUM_PRODUCT_ID,
        "UK" => Product::UK_ONE_YEAR_ALBUM_PRODUCT_ID,
        "US" => Product::US_ONE_YEAR_ALBUM_PRODUCT_ID,
      },
      "Single" => {
        "AU" => Product::AU_ONE_YEAR_SINGLE_PRODUCT_ID,
        "CA" => Product::CA_ONE_YEAR_SINGLE_PRODUCT_ID,
        "DE" => Product::DE_ONE_YEAR_SINGLE_PRODUCT_ID,
        "FR" => Product::FR_ONE_YEAR_SINGLE_PRODUCT_ID,
        "IT" => Product::IT_ONE_YEAR_SINGLE_PRODUCT_ID,
        "UK" => Product::UK_ONE_YEAR_SINGLE_PRODUCT_ID,
        "US" => Product::US_ONE_YEAR_SINGLE_PRODUCT_ID,
      }
    }
  end

  describe "#process" do
    context "Single", run_sidekiq_worker: true do
      before do
        @artists =  [@artist1, @artist2, @artist3]

        artist_names_str = @artists.map {|a| a[:name] }.join(":")
        apple_artist_ids_str = @artists.map {|a| a[:apple_artist_id] }.join(":")
        spotify_artist_ids_str = @artists.map {|a| a[:spotify_artist_id] }.join(":")
        artist_roles_str = @artists.map {|a| a[:role] }.join(":")
        songwriters = "Mug:Sw1"

        @album_attributes = {
          album_title: @title,
          album_artist: artist_names_str,
          album_role: artist_roles_str,
          stem_account_email_address: @email_address,
          tunecore_optin_email_address: @email_address,
          label: @label_name,
          territory: "WW",
          album_language: @language_name,
          original_release: @original_release,
          apple_artist_id: apple_artist_ids_str,
          spotify_artist_id: spotify_artist_ids_str,
          upc: @upc,
          genre: @genre
        }

        @track1 = {
          track_title: "Track 1",
          track_language: "en",
          lyric: "Track 1 lyrics",
          isrc: "USRC17607839",
          clean_version: "FALSE",
          track_artist: artist_names_str,
          track_role: artist_roles_str,
          songwriter: songwriters
        }
      end

      let(:klass) { Single }

      describe "When CSV has data for a Single" do
        before do
          @title = @track1[:track_title]
          row1 = stem_mock_csv_row(@album_attributes.merge(@track1))
          @rows = [row1]
        end

        include_context "setup_csv_and_audio_file_mocks"
        include_examples "album/single attributes"

        xit "should parse and create a single and its song" do
          expect { @album_service.process }.to change{klass.count}.by(1)
          single = klass.find_by(name: @title)

          #song_attributes
          expect(single.songs.length).to eq(1)
          song = single.song

          expect(song.name).to eq(@track1[:track_title])
          expect(song.lyric.content).to eq(@track1[:lyric])
          expect(song.optional_isrc).to eq(@track1[:isrc])
          expect(song.parental_advisory).to be true
        end

        xit "should parse and create the single and its creatives." do
          expect { @album_service.process }
            .to change{klass.count}.by(1)
            .and change{Creative.count}.by(4)

          single = klass.find_by(name: @title)

          #creatives
          single_creatives = single.creatives.joins(:artist)
          single_primary_creatives = single_creatives.where(role: :primary_artist)
          single_featured_creatives = single_creatives.where(role: :featuring)
          single_contributor_creatives = single_creatives.where(role: :contributor)

          expect(single_creatives.length).to eq(4)
          expect(single.song.creatives).to be_empty
          expect(single_primary_creatives.length).to eq(2)
          expect(single_featured_creatives.length).to eq(1)
          expect(single_contributor_creatives.length).to eq(1)

          #primary artists
          ["Mouse", "Keyboard"].each do |art_name|
            primary_creative = single_primary_creatives.where('artists.name': art_name).first
            expect(primary_creative.song_roles).to be_empty
          end

          #external_service_ids
          @artists.each do |artist|
            creative = single_creatives.where('artists.name = ?', artist[:name]).first
            expect(creative.external_id_for("apple")).to eq(artist[:apple_artist_id])
            expect(creative.external_id_for("spotify")).to eq(artist[:spotify_artist_id])
          end

          #featuring artist
          featured_creative = single_featured_creatives.where('artists.name': 'Mug').first
          expect(featured_creative.song_roles.where(role_type: 'songwriter')).to exist

          #songwriters
          contributor_creative1 = single_contributor_creatives.where('artists.name': "Sw1").first
          expect(contributor_creative1.song_roles.where(role_type: 'songwriter')).to exist
        end
      end

      describe "When CSV has Various Artists" do
        before do
          @title = @track1[:track_title]
          @track1.merge!(
            album_artist: "Various Artists",
            album_role: "primary",
            track_artist: "Benny Dayal:Tippu",
            track_role: "primary:featured",
            songwriter: "Thamarai"
          )
          row1 = stem_mock_csv_row(@album_attributes.merge(@track1))
          @rows = [row1]
        end

        include_context "setup_csv_and_audio_file_mocks"

        xit "should create the single and is_various should be set as false." do
          allow(@album_service).to receive(:trace_log).and_return(spy(:logger))
          expect(@album_service).to receive(:status_log).with("failed").at_least(:once)

          expect { @album_service.process }
            .to not_change{klass.count}
            .and not_change{Creative.count}

          expect(@album_service).to have_received(:trace_log).with(
            "Validation failed: Creatives You must have a Main Artist!, Single cannot be marked as a compilation"
          ).at_least(:once)
        end
      end
    end

    context "Album", run_sidekiq_worker: true do
      before do

        @artists =  [@artist1, @artist2]
        artist_names_str = @artists.map {|a| a[:name] }.join(":")
        apple_artist_ids_str = @artists.map {|a| a[:apple_artist_id] }.join(":")
        spotify_artist_ids_str = @artists.map {|a| a[:spotify_artist_id] }.join(":")
        album_role_str = @artists.map {|a| a[:role] }.join(":")

        @album_attributes = {
          album_title: @title,
          album_artist: artist_names_str,
          album_role: album_role_str,
          stem_account_email_address: @email_address,
          tunecore_optin_email_address: @email_address,
          label: @label_name,
          territory: "WW",
          album_language: @language_name,
          original_release: @original_release,
          apple_artist_id: apple_artist_ids_str,
          spotify_artist_id: spotify_artist_ids_str,
          upc: @upc,
          genre: @genre
        }

        @track1 = {
          track_title: "Track 1",
          track_language: "en",
          lyric: "Track 1 lyrics",
          isrc: "USRC17607839",
          clean_version: "FALSE",
          track_artist: "TArtist 1:TArtist 2",
          track_role: "primary:featured",
          songwriter: "TArtist 1:Writer 1"
        }

        @track2 = {
          track_title: "Track 2",
          track_language: "de",
          lyric: "Lyrics for Track 2",
          isrc: "USRC17607840",
          clean_version: "TRUE",
          track_artist: "Mouse:TArtist 3:TArtist 4",
          track_role: "primary:primary:featured",
          songwriter: "Mouse:TArtist 4:Writer 5"
        }
      end

      let(:klass) { Album }

      describe "When CSV has data for an Album" do
        before do
          territory = "IN:US:CA"
          @album_attributes.merge!({territory: territory})
          row1 = stem_mock_csv_row(@album_attributes.merge(@track1))
          row2 = stem_mock_csv_row(@album_attributes.merge(@track2))
          @rows = [row1, row2]
        end

        include_context "setup_csv_and_audio_file_mocks" do
          let(:expected_country_iso_codes) { ["IN", "US", "CA"] }
        end

        include_examples "album/single attributes"

        xit "should parse and create the album and its songs." do
          expect { @album_service.process }.to change{klass.count}.by(1)
          album = klass.find_by(name: @title)

          #songs
          expect(album.songs.length).to eq(2)
          song1, song2 = album.songs

          #song 1 attributes
          expect(song1.name).to eq(@track1[:track_title])
          expect(song1.lyric.content).to eq(@track1[:lyric])
          expect(song1.optional_isrc).to eq(@track1[:isrc])
          expect(song1.parental_advisory).to be true

          #song2 attributes
          expect(song2.name).to eq(@track2[:track_title])
          expect(song2.lyric.content).to eq(@track2[:lyric])
          expect(song2.optional_isrc).to eq(@track2[:isrc])
          expect(song2.parental_advisory).to be false
        end

        xit "should parse and create album and song level creatives." do
          expect { @album_service.process }
            .to change{klass.count}.by(1)
            .and change{Creative.count}.by(9)

          album = klass.find_by(name: @title)

          # album level creatives
          album_creatives = album.creatives.joins(:artist)
          expect(album_creatives.length).to eq(2)
          album_primary_creatives = album_creatives.where(role: :primary_artist)

          expect(album_primary_creatives.length).to eq(2)
          @artists.each do |artist|
            creative = album_primary_creatives.where('artists.name = ?', artist[:name]).first
            expect(creative.external_id_for("apple")).to eq(artist[:apple_artist_id])
            expect(creative.external_id_for("spotify")).to eq(artist[:spotify_artist_id])
          end

          #songs
          expect(album.songs.length).to eq(2)
          song1, song2 = album.songs

          #song1 creatives
          song1_creatives = song1.creatives.joins(:artist)
          expect(song1_creatives.length).to eq(3)

          s1_primary_creatives = song1_creatives.where(role: :primary_artist)
          expect(s1_primary_creatives.length).to eq(1)
          expect(s1_primary_creatives[0].artist.name).to eq("TArtist 1")
          expect(s1_primary_creatives[0].song_roles.where(role_type: "songwriter")).to exist

          s1_featured_creatives = song1_creatives.where(role: :featuring)
          expect(s1_featured_creatives.length).to eq(1)
          expect(s1_featured_creatives[0].artist.name).to eq("TArtist 2")
          expect(s1_featured_creatives[0].song_roles).to be_empty

          s1_contributor_creatives = song1_creatives.where(role: :contributor)
          expect(s1_contributor_creatives.length).to eq(1)
          expect(s1_contributor_creatives[0].artist.name).to eq("Writer 1")
          expect(s1_primary_creatives[0].song_roles.where(role_type: "songwriter")).to exist

          #song2 creatives
          song2_creatives = song2.creatives.joins(:artist)
          expect(song2_creatives.length).to eq(4)

          s2_primary_creatives = song2_creatives.where(role: :primary_artist)
          expect(s2_primary_creatives.length).to eq(1)
          expect(s2_primary_creatives[0].artist.name).to eq("TArtist 3")
          expect(s2_primary_creatives[0].song_roles).to be_empty

          s2_featured_creatives = song2_creatives.where(role: :featuring)
          expect(s2_featured_creatives.length).to eq(1)
          expect(s2_featured_creatives[0].artist.name).to eq("TArtist 4")
          expect(s2_featured_creatives[0].song_roles.where(role_type: "songwriter")).to exist

          s2_contributor_creatives = song2_creatives.where(role: :contributor)
          expect(s2_contributor_creatives.length).to eq(2)
          expect(s2_contributor_creatives.map{|c| c.artist.name }).to contain_exactly("Mouse", "Writer 5")
          s2_contributor_creatives.map do |creative|
            expect(creative.song_roles.where(role_type: "songwriter")).to exist
          end
        end
      end

      describe "When CSV has Various Artists" do
        before do
          @album_attributes.merge!(
            album_artist: "Various Artists",
            album_role: "primary"
          )
          @track1.merge!(
            track_artist: "MJ:Queen",
            track_role: "primary:featured",
            songwriter: ""
          )
          row1 = stem_mock_csv_row(@album_attributes.merge(@track1))

          @track2.merge!(
            track_artist: "Rihanna:Adele:Cardi B",
            track_role: "primary:primary:featured",
            songwriter: "Adele:Drake"
          )
          row2 = stem_mock_csv_row(@album_attributes.merge(@track2))

          @rows = [row1, row2]
        end

        include_context "setup_csv_and_audio_file_mocks"

        xit "should parse and create an is_various album" do
          expect { @album_service.process }
            .to change{klass.count}.by(1)
            .and change{Creative.count}.by(6)

          album = klass.find_by(name: @title)

          expect(album.is_various).to be true
          expect(album.artist_name).to eq('Various Artists')
          expect(album.creatives.joins(:artist).length).to eq(0)

          song1, song2 = album.songs
          expect(song1.creatives.length).to eq 2
          expect(song1.primary_artists.length).to eq 1
          expect(song1.primary_artists[0].name).to eq "MJ"
          expect(song1.featured_artists.length).to eq 1
          expect(song1.featured_artists[0].name).to eq "Queen"

          expect(song2.creatives.length).to eq 4
          expect(song2.primary_artists.length).to eq 2
          expect(song2.primary_artists.map(&:name)).to contain_exactly("Rihanna", "Adele")
          expect(song2.featured_artists.length).to eq 1
          expect(song2.featured_artists.map(&:name)).to contain_exactly("Cardi B")
          expect(song2.contributors.length).to eq 1
          expect(song2.contributors.map(&:name)).to contain_exactly("Drake")
        end
      end

      describe "When CSV contains UPC that already exists" do
        before do
          existing_upc = create(:optional_upc)
          @title = @track1[:track_title]
          @album_attributes.merge!({upc: existing_upc.number})
          row1 = stem_mock_csv_row(@album_attributes.merge(@track1))
          row2 = stem_mock_csv_row(@album_attributes.merge(@track2))
          @rows = [row1, row2]
        end

        include_context "setup_csv_and_audio_file_mocks"

        xit "should not get ingested and status should be set as skipped" do
          expect(@album_service).to receive(:status_log).with("skipped").at_least(:once)
          expect { @album_service.process }.to not_change{klass.count}
          expect(MassIngestionStatus.skipped.where(identifier: @album_service.upc_str, identifier_type: 'UPC')).to exist
        end

        xit "should get ingested if skip_upc_check_and_optional_upc_creation?=true" do
          allow(@album_service).to receive(:skip_upc_check_and_optional_upc_creation?).and_return(true)
          expect { @album_service.process }.to change{klass.count}

          album = Album.find_by(name: @album_service.album_attributes[:name])
          expect(album.optional_upc).to be_nil
          expect(album.tunecore_upc).to be_present

          expect(MassIngestionStatus.successful.where(identifier: @album_service.upc_str, identifier_type: 'UPC')).to exist
        end
      end

      describe "When upc_download_dir contains files with case-insensitive extensions" do
        before do
          @rows = [stem_mock_csv_row(@album_attributes), stem_mock_csv_row(@album_attributes), stem_mock_csv_row(@album_attributes)]
          @mock_csv_io = stem_mock_csv_io(@rows)
          @album_service = MassIngestion::AlbumService.new("dummy_bucket", "dummy_prefix/#{@album_attributes[:upc]}")
          @upc_download_dir = @album_service.upc_download_dir

          FileUtils.mkdir_p(@upc_download_dir)

          @audio_filenames = {
            audio1_filename: "01 #{@album_service.upc_str}.Mp3",
            audio2_filename: "02 #{@album_service.upc_str}.fLaC",
            audio3_filename: "03 #{@album_service.upc_str}.WAV"
          }

          @asset_filenames = {
            csv_filename: "#{@album_service.upc_str}.CSV",
            artwork_filename: "#{@album_service.upc_str}.JPeG"
          }.merge(@audio_filenames)

          @asset_filepaths = @asset_filenames.map do |type, filename|
            filepath = File.join(@upc_download_dir, filename)
            FileUtils.touch(filepath)
          end
        end

        xit "should get csv,audio and artwork files irrespective of the file ext case sensitiveness" do
          expect(File.basename(@album_service.csv_file)).to eq(@asset_filenames[:csv_filename])
          expect(File.basename(@album_service.artwork_file)).to eq(@asset_filenames[:artwork_filename])

          audio_files = @album_service.audio_files
          audio_files.each do |f|
            expect(@audio_filenames.values).to include(File.basename(f))
          end
        end

        after { FileUtils.rm_f(@upc_download_dir) }
      end
    end
  end
end
