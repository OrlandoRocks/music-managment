require "rails_helper"

describe ReferAFriendNotification do
  let(:invoice)  { FactoryBot.create(:invoice) }
  describe ".send_notification" do
    before do
      @referral = ReferAFriend::PurchaseHandler.new(invoice, true)
      allow(Invoice).to receive(:find).with(invoice.id).and_return(invoice)
      allow(ReferAFriend::PurchaseHandler).to receive(:new).with(invoice, true).and_return(@referral)
    end
    it "calls on the refer a friend api client" do
      allow($refer_a_friend_api_client).to receive(:post).with(@referral.prepare_data, '/purchases')
      expect($refer_a_friend_api_client).to receive(:post).with(@referral.prepare_data, '/purchases')
      ReferAFriendNotification.send_notification(invoice.id, true)
    end
  end
end
