require "rails_helper"

describe MyArtist do
  let(:person) { create(:person) }
  let(:artist) { create(:artist) }

  context "artists" do
    let(:artist2) { create(:artist) }
    let!(:album) do
      create(:album, person: person, creatives: [
        { "role" => "primary_artist", "name" => artist.name }
      ])
    end

    let!(:song) do
      create(:song, album: album, creatives: [
        { "role" => "contributor", "name" => artist2.name }
      ])
    end

    describe ".title_artists_for" do
      it "returns a list of artists for a person" do
        artists = MyArtist.title_artists_for(person.id)
        expect(artists.first.artist_name).to eq(artist.name)
        expect(artists.map(&:artist_name)).not_to include(artist2.name)
      end
    end

    describe ".all_artists_for" do
      it "returns a list of artists for a person" do
        artists = MyArtist.all_artists_for(person.id)
        expect(artists.map(&:artist_name)).to include(artist2.name)
      end
    end
  end

  context "releases" do
    let!(:album) do
      create(:album, person: person, creatives: [
          { "role" => "primary_artist", "name" => artist.name }
      ])
    end

    describe "#releases" do
      let(:person2) { create(:person) }
      let(:artist2) { create(:artist) }

      let!(:album2) do
        create(:album, person: person2, creatives: [
          { "role" => "primary_artist", "name" => artist.name }
        ])
      end

      let!(:single) do
        create(:single, person: person, creatives: [
          { "role" => "primary_artist", "name" => artist.name },
          { "role" => "featuring", "name" => artist2.name }
        ])
      end

      it "returns all releases for an artist for a person" do
        releases = MyArtist.new(person.id, artist.id).releases
        expect(releases.count).to eq(2)
      end

      it "does not return any releases for an artist not released by a user" do
        releases = MyArtist.new(person2.id, artist2.id).releases
        expect(releases).to be_empty
      end
    end

    describe "#releases_for" do
      let!(:external_service_id) { create(:external_service_id, linkable: album, identifier: "100001023", service_name: "apple") }
      it "returns all releases for an artist for a person for a specific service" do
        my_artist = MyArtist.new(person.id, artist.id)
        releases = my_artist.releases_for("apple")
        expect(releases.first.album_name).to eq(album.name)
      end
    end
  end
end
