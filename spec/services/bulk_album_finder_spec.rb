require "rails_helper"

describe "BulkAlbumFinder" do

  describe "#execute" do
    let(:person) { FactoryBot.create(:person) }
    let(:album1) { FactoryBot.create(
              :album,
              :finalized,
              :with_uploaded_songs_and_artwork,
               number_of_songs: 2,
               person: person
            ) }

    let(:album2) { FactoryBot.create(
              :album,
              :finalized,
              :with_uploaded_songs_and_artwork,
               number_of_songs: 2,
               person: person
            ) }

    let(:album1_upc) { album1.upcs.first.number }
    let(:album2_upc) { album2.upcs.first.number }
    let(:album1_song_isrc) { album1.songs.first.tunecore_isrc }
    let(:album2_song_isrc) { album2.songs.second.tunecore_isrc }

    context "when given a properly spaced list of comma separated album IDs" do
      it "returns the albums with the given IDs" do
        album_ids = "#{album1.id}, #{album2.id}"
        bulk_album_finder = BulkAlbumFinder.new(album_ids)
        expect(bulk_album_finder.execute).to eq([album1, album2])
      end
    end

    context "when given an improperly spaced list of comma separated album IDs" do
      it "returns the albums with the given IDs" do
        album_ids = "#{album1.id},#{album2.id}"
        bulk_album_finder = BulkAlbumFinder.new(album_ids)
        expect(bulk_album_finder.execute).to eq([album1, album2])
      end
    end

    context "when given a list of not comma separated album IDs" do
      it "returns the albums with the given IDs" do
        album_ids = "#{album1.id} #{album2.id}"
        bulk_album_finder = BulkAlbumFinder.new(album_ids)
        expect(bulk_album_finder.execute).to eq([album1, album2])
      end
    end

    context "when given a list of UPC numbers" do
      it "returns the albums with the given Upc numbers" do
        album_upcs = "#{album1_upc}, #{album2_upc}"
        bulk_album_finder = BulkAlbumFinder.new(album_upcs)
        expect(bulk_album_finder.execute).to eq([album1, album2])
      end
    end

    context "when given a list of ISRC numbers" do
      it "returns the albums with at least one song with one of the given isrc numbers" do
        song_isrcs = "#{album1_song_isrc}, #{album2_song_isrc}"
        bulk_album_finder = BulkAlbumFinder.new(song_isrcs)
        expect(bulk_album_finder.execute).to eq([album1, album2])
      end
    end

    context "when given a list that includes an album that does not exist" do
      it "sets an error message" do
        album_ids = "#{album1.id}, 0"
        bulk_album_finder = BulkAlbumFinder.new(album_ids)
        expect(bulk_album_finder.execute).to eq([album1])
        expect(bulk_album_finder.messages).to eq({"0" => "Album with ID of 0 or UPC of 0 or song ISRC of 0 not found."})
      end
    end
  end
end
