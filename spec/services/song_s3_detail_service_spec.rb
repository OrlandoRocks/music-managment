require "rails_helper"

describe SongS3DetailService do
  let(:wav_low_bitrate_metadata) do
    {"streams"=>
      [
        {
          "codec_type"=>S3Detail::CODEC_TYPE_AUDIO,
          "sample_rate"=>"44100",
          "bits_per_sample"=>8
        }
      ]
    }
  end

  let(:wav_low_sample_rate_metadata) do
    {"streams"=>
      [
        {
          "codec_type"=>S3Detail::CODEC_TYPE_AUDIO,
          "sample_rate"=>"22100",
          "bits_per_sample"=>16
        }
      ]
    }
  end

  let(:flac_low_bitrate_metadata) do
    {"streams"=>
      [
        {
          "codec_type"=>S3Detail::CODEC_TYPE_AUDIO,
          "sample_rate"=>"44100",
          "bits_per_sample"=>0,
          "bits_per_raw_sample"=>"12"
        }
      ]
    }
  end

  let(:flac_low_sample_rate_metadata) do
    {"streams"=>
      [
        {
          "codec_type"=>S3Detail::CODEC_TYPE_AUDIO,
          "sample_rate"=>"22100",
          "bits_per_sample"=>0,
          "bits_per_raw_sample"=>"12"
        }
      ]
    }
  end

  let(:with_non_audio_stream) do
    {"streams"=>
      [
        {
          "codec_type"=>'jpeg',
          "sample_rate"=>"22100",
          "bits_per_sample"=>0,
          "bits_per_raw_sample"=>"12"
        },
        {
          "codec_type"=>S3Detail::CODEC_TYPE_AUDIO,
          "sample_rate"=>"22100",
          "bits_per_sample"=>0,
          "bits_per_raw_sample"=>"12"
        }
      ]
    }
  end

  let(:song1) { create(:song) }
  let(:song2) { create(:song) }
  let(:asset1) { create(:s3_asset, song: song1) }
  let(:asset2) { create(:s3_asset, song: song2) }
  let!(:detail1) { create(:s3_detail, s3_asset: asset1, file_type: S3Detail::MP3_TYPE) }

  describe "#mp3s" do
    it "filters songs having mp3 source asset" do
      detail2 = create(:s3_detail, s3_asset: asset2, file_type: S3Detail::WAV_TYPE)

      expected = SongS3DetailService.new([song1.id, song2.id]).mp3s
      expect(expected.length).to eq(1)
    end
  end

  describe "#low_bitrate" do
    it "filters wav songs with low bitrate source asset" do
      detail2 = create(:s3_detail, s3_asset: asset2, file_type: S3Detail::WAV_TYPE, metadata_json: wav_low_bitrate_metadata)

      expected = SongS3DetailService.new([song1.id, song2.id]).low_bitrate
      expect(expected.length).to eq(1)
    end

    it "filters flac songs with low bitrate source asset" do
      detail2 = create(:s3_detail, s3_asset: asset2, file_type: S3Detail::WAV_TYPE, metadata_json: wav_low_bitrate_metadata)

      expected = SongS3DetailService.new([song1.id, song2.id]).low_bitrate
      expect(expected.length).to eq(1)
    end
  end

  describe "#low_sample_rate" do
    it "filters wav songs with low bitrate source asset" do
      detail2 = create(:s3_detail, s3_asset: asset2, file_type: S3Detail::WAV_TYPE, metadata_json: flac_low_sample_rate_metadata)

      expected = SongS3DetailService.new([song1.id, song2.id]).low_sample_rate
      expect(expected.length).to eq(1)
    end

    it "filters flac songs with low bitrate source asset" do
      detail2 = create(:s3_detail, s3_asset: asset2, file_type: S3Detail::WAV_TYPE, metadata_json: flac_low_sample_rate_metadata)

      expected = SongS3DetailService.new([song1.id, song2.id]).low_sample_rate
      expect(expected.length).to eq(1)
    end
  end

  describe "non-audio streams" do
    it "ignores non-audio streams" do
      detail2 = create(:s3_detail, s3_asset: asset2, file_type: S3Detail::WAV_TYPE, metadata_json: with_non_audio_stream)

      expected = SongS3DetailService.new([song1.id, song2.id]).low_sample_rate
      expect(expected.length).to eq(1)
    end
  end

  describe "#lo_fi" do
    it "filters songs with low bitrate, low sample rate, or mp3 source asset" do
      song3 = create(:song)
      song4 = create(:song)
      asset3 = create(:s3_asset, song: song3)
      asset4 = create(:s3_asset, song: song4)
      detail2 = create(:s3_detail, s3_asset: asset2, file_type: S3Detail::WAV_TYPE, metadata_json: wav_low_bitrate_metadata)
      detail3 = create(:s3_detail, s3_asset: asset3, file_type: S3Detail::MP3_TYPE)
      detail4 = create(:s3_detail, s3_asset: asset4, file_type: S3Detail::WAV_TYPE)

      expected = SongS3DetailService.new([song1.id, song2.id, song3.id, song4.id]).lo_fi
      expect(expected.length).to eq(3)
    end
  end

  context "missing S3Details" do
    it "handles missing s3_details gracefully" do
      detail1.destroy
      expected = SongS3DetailService.new([song1.id, song2.id]).lo_fi
      expect(expected.length).to eq(0)
    end
  end
end
