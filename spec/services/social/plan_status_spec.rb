require "rails_helper"
include ActiveSupport::Testing::TimeHelpers

describe Social::PlanStatus do
  describe "#fetch_plan" do
    context "with active subscription" do
      let(:person)    { create(:person, :with_active_social_subscription_status) }
      let(:plan_status) { Social::PlanStatus.for(person) }

      it "sets the plan name to pro" do
        expect(plan_status.plan).to eq("pro")
      end

      it "sets the expiration date" do
        expect(plan_status.plan_expires_at).to eq(PersonSubscriptionStatus.last.termination_date.to_date)
      end

      it "has a grace period 5 days after termination date" do
        expect(plan_status.grace_period_ends_on).to eq(PersonSubscriptionStatus.last.termination_date.to_date + DEFAULT_SUBSCRIPTION_GRACE_PERIOD)
      end

      it "has a grace period 1 days after termination date" do
        status = person.person_subscription_statuses.last
        status.grace_period_length = 1
        status.save
        plan_status = Social::PlanStatus.for(person)
        expect(plan_status.grace_period_ends_on).to eq(PersonSubscriptionStatus.last.termination_date.to_date + 1)
      end
    end

    context "with no subscription and active distro" do
      let(:person)          { create(:person, :with_live_album) }
      let(:expiration_date) { 1.year.from_now.to_date }

      before do
        travel_to Time.local(2020)
      end

      after do
        travel_back
      end

      it "sets the plan name to free" do
        plan_status = Social::PlanStatus.new(person)
        expect(plan_status.plan).to eq("free")
      end

      it "sets the expiration date to max takedown date" do
        plan_status = Social::PlanStatus.new(person)
        expect(plan_status.plan_expires_at).to eq(expiration_date)
      end
    end

    context "with no subscription and no distro" do
      let(:person) { create(:person) }
      let(:plan_status) { Social::PlanStatus.for(person) }

      it "sets the plan name to nil" do
        expect(plan_status.plan).to be_nil
      end

      it "sets the expriation date to nil" do
        expect(plan_status.plan_expires_at).to be_nil
      end
    end

    context "with expired subscription and active distro" do
      let(:person)  {create(:person, :with_live_album, :with_inactive_social_subscription_status)}

      before(:each) do
        @plan_status = Social::PlanStatus.for(person)
      end

      it "sets the plan name to pro_expired_free" do
        expect(@plan_status.plan).to eq("pro_expired_free")
      end

      it "sets the expiration date to max takedown date" do
        expect(@plan_status.plan_expires_at).to eq(person.live_releases.last.renewal.expires_at.to_date)
      end
    end

    context "with expired subscription and no distro" do
      let(:person)  {create(:person, :with_inactive_social_subscription_status)}

      before(:each) do
        @plan_status = Social::PlanStatus.for(person)
      end

      it "sets the plan name to pro_expired" do
        expect(@plan_status.plan).to eq("pro_expired")
      end

      it "sets the expiration date to max takedown date" do
        expect(@plan_status.plan_expires_at).to eq(person.subscription_status_for("Social").termination_date.to_date)
      end
    end

    context "with no subscription and inactive distro" do
      let(:person) {create(:person, :with_taken_down_album)}

      before(:each) do
        @plan_status = Social::PlanStatus.new(person)
      end

      it "sets the plan name to free_expired" do
        expect(@plan_status.plan).to eq("free_expired")
      end

      it "sets the expiration date to the expired distro takedown date" do
        expect(@plan_status.plan_expires_at).to eq(person.albums.where("takedown_at < ?", Date.today).last.takedown_at.to_date)
      end
    end
  end
end
