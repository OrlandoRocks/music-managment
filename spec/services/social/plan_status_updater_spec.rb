require "rails_helper"

describe Social::PlanStatusUpdater do
  let(:person)      { create(:person, :with_active_social_subscription_status) }
  let(:token)       { SessionEncryptionEngine.encrypt64(person.tc_social_token) }
  let(:plan_status) { Social::PlanStatus.for(person) }
  let(:params)      { { users: {"#{token}" => [plan_status.plan, plan_status.plan_expires_at] } } }

  describe ".update" do
    it "is true of API call returns with success response" do
      response    = double(status: 200, body: {status: "success"}.to_json)
      expect($social_api_client).to receive(:post).with("api/bulk_plan_update", params).and_return(response)
      expect(Social::PlanStatusUpdater.update(person)).to eq true
    end

    it "is false if API returns error" do
      error      = "Unable to update plan for user: #{person.email}"
      response   = double(status: 200, body: {status: "failure", data: [error]}.to_json)
      expect($social_api_client).to receive(:post).with("api/bulk_plan_update", params).and_return(response)
      expect { Social::PlanStatusUpdater.update(person) }.to raise_error(Social::PlanStatusUpdater::PlanStatusError)
    end
  end
end
