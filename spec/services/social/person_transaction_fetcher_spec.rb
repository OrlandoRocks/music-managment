require "rails_helper"

describe Social::PersonTransactionFetcher do
  let(:person) {FactoryBot.create(:person, :with_tc_social)}
  let(:params) {
    {
      start_date: "2017-01-01",
      end_date: "2017-02-01"
    }
  }
  let(:default_fetcher) {Social::PersonTransactionFetcher.new(person.id, params)}
  describe "#execute" do
    it "creates an instance of Social::PersonTransactionFetcher" do
      fetcher = Social::PersonTransactionFetcher.new(person.id, params)
      expect(Social::PersonTransactionFetcher).to receive(:new) {fetcher}
      Social::PersonTransactionFetcher.execute(person.id, params)
    end

    it "calls on the #get_transactions instance method" do
      expect_any_instance_of(Social::PersonTransactionFetcher).to receive(:get_transactions)
      Social::PersonTransactionFetcher.execute(person.id, params)
    end
  end

  describe "#get_transactions" do
    it "queries for the user's transactions in the given date range" do
      expect(PersonTransaction).to receive(:where).with(person_id: person.id, created_at: "2017-01-01".."2017-02-01") { person.person_transactions }
      fetcher = Social::PersonTransactionFetcher.new(person.id, params)
      fetcher.get_transactions
    end

    it "orders them newest first, oldest last" do
      last_year = Time.current - 1.year
      yesterday = Time.current - 1.day

      5.times do
        created_at = rand(last_year..yesterday)
        create(:person_transaction, person: person, created_at: created_at)
      end

      year_params = { start_date: (Date.current - 1.year).to_s, end_date: Date.current.to_s }
      transactions = Social::PersonTransactionFetcher.new(person.id, year_params).get_transactions
      sorted_transactions = transactions.sort_by(&:created_at).reverse

      expect(transactions).to eq sorted_transactions
    end

    context "pagination" do
      before(:each) do
        21.times do
          create(:person_transaction, person: person, created_at: rand(Date.today..Date.tomorrow))
        end
        @total_transactions = PersonTransaction.where(person_id: person.id, created_at: Date.today..Date.tomorrow).order(created_at: :desc)
      end

      context "only page num specified" do
        it "returns the correct page num with the default page size" do
          fetcher = Social::PersonTransactionFetcher.new(person.id, {
            start_date: Date.today,
            end_date: Date.tomorrow,
            page_num: 2
          })
          fetcher.get_transactions
          expect(fetcher.transactions.size).to eq(1)
          expect(fetcher.transactions).to eq [@total_transactions.reload.last]
        end
      end

      context "only page size specified" do
        it "returns the correct page size the default of the first page" do
          fetcher = Social::PersonTransactionFetcher.new(person.id, {
            start_date: Date.today,
            end_date: Date.tomorrow,
            page_size: 2
          })
          fetcher.get_transactions
          expect(fetcher.transactions.size).to eq(2)
          expect(fetcher.transactions).to eq(@total_transactions[0..1])
        end
      end

      context "no pagination with page_size of 0" do
        it "does not paginate results" do
          fetcher = Social::PersonTransactionFetcher.new(person.id, {
            start_date: Date.today,
            end_date: Date.tomorrow,
            page_size: 0
          })
          fetcher.get_transactions
          expect(fetcher.transactions.size).to eq(21)
          expect(fetcher.transactions).to match_array @total_transactions
        end
      end

      context "no pagination params included" do
        it "defaults to page 1 with a page size of 20" do
          fetcher = Social::PersonTransactionFetcher.new(person.id, {
            start_date: Date.today,
            end_date: Date.tomorrow
          })
          fetcher.get_transactions
          expect(fetcher.transactions.size).to eq(20)
          expect(fetcher.transactions).to eq(@total_transactions[0..-2])
        end
      end

      context "page num of 0" do
        it "defaults to page 1" do
          fetcher = Social::PersonTransactionFetcher.new(person.id, {
            start_date: Date.today,
            end_date: Date.tomorrow,
            page_num: 0
          })
          fetcher.get_transactions
          expect(fetcher.transactions.size).to eq(20)
          expect(fetcher.transactions).to eq(@total_transactions[0..-2])
        end
      end
    end
  end
end
