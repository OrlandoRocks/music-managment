require "rails_helper"

describe TransactionalEmail::Base do
  before(:each) do
    @body = {
      emailId: 12345,
      message: {
        to: "test@nope.lol"
      },
      customProperties: [
        name: "this",
        value: "that"
      ]
    }
  end

  describe "self.deliver" do
    it "instantiates and delivers a TransactionalEmail object" do
      expect_any_instance_of(TransactionalEmail::ForgotPassword).to receive(:deliver)
      TransactionalEmail::ForgotPassword.deliver(create(:person))
    end

  end

  describe "#deliver" do
    before :each do
      @mailer = TransactionalEmail::Base.new
      @mailer.body = @body
    end

    it "posts the body to hubspot" do
      expect(@mailer.deliver.status).to be 200
    end

    it "logs the response from hubspot" do
      expect_any_instance_of(TransactionalEmail::Base).to receive(:log_response)
      @mailer.deliver
    end
  end

  describe "#format_request_props" do
    before(:each) do
      props = { whut: "prop1", which: "prop2" }
      @custom_props = TransactionalEmail::Base.new.format_request_props(props)
    end

    it "responds with an array of objects" do
      expect(@custom_props.class).to eq(Array)
    end

    it "breaks each param into an individual object" do
      expect(@custom_props[1][:name]).to eq("which")
      expect(@custom_props[1][:value]).to eq("prop2")
    end
  end

  describe "#log_response" do
    before(:each) do
      @base = TransactionalEmail::Base.new
    end

    it "adds the Hubspot response to the rails logger" do
      good_response =
        double(
          status: 200,
          body: {
            status: "success",
            message: "YUP",
            sendResult: "FOO"
          }.to_json)
      message = "HUBSPOT: FOO: YUP"
      expect(Rails.logger).to receive(:info).with(message)
      @base.log_response(good_response)
    end

    it "doesn't raise an error on success" do
      good_response =
        double(
          status: 200,
          body: {
            status: "success",
            message: "YUP",
            sendResult: "FOO"
          }.to_json)
      expect {
        @base.log_response(good_response)
      }.not_to raise_error
    end

    it "raises an error on an unsuccessful request" do
      error_response =
        double(
          status: 401,
          body: {
            status: "error",
            message: "NOPE",
            sendResult: "LOL"
          }.to_json)
      expect {
        @base.log_response(error_response)
      }.to raise_error
    end
  end
end
