require "rails_helper"

describe TransactionalEmail::ForgotPassword do
  let(:user) { create(:person) }

  it "calls #format_request_props at least once" do
    expect_any_instance_of(described_class).to receive(:format_request_props)
    described_class.new(user)
  end

  it "sets the correct email template key" do
    body = described_class.new(user).body
    expect(body[:emailId]).to eq(TransactionalEmail::ForgotPassword::EMAIL_ID)
  end
end
