require 'rails_helper'

describe CurrencyDisplayService do
  before(:each) do
    @indian_person = create(:person, country: 'IN')
    @indian_service = CurrencyDisplayService.new(@indian_person)
    @american_person = create(:person)
    @american_service = CurrencyDisplayService.new(@american_person)
  end
  it "sets country domain appropriately" do
    expect(@indian_service.instance_variable_get(:@country_website)).to eq(CountryWebsite.find_by(country: 'IN'))
    expect(@american_service.instance_variable_get(:@country_website)).to eq(CountryWebsite.find_by(country: 'US'))
  end
  describe "#multiple_currency_domain?" do
    it "detects multiple currency domains" do
      expect(@indian_service.multiple_currency_domain?).to be(true)
      expect(@american_service.multiple_currency_domain?).to be(false)
    end
  end
  describe "#display_currency" do
    context "multiple currency domains" do
      it "returns display currency" do
        expect(@indian_service.display_currency).to eq('INR')
      end
    end
    context "single currency domains" do
      it "returns user's currency" do
        expect(@american_service.display_currency).to eq(@american_person.currency)
      end
    end
  end
  describe "#transaction_currency" do
    context "multiple currency domains" do
      it "returns transaction currency" do
        expect(@indian_service.transaction_currency).to eq('USD')
      end
    end
    context "single currency domains" do
      it "returns user's currency" do
        expect(@american_service.transaction_currency).to eq(@american_person.currency)
      end
    end
  end
  context "single currency domain" do
    it "should have the same display and transaction currency" do
      expect(@american_service.transaction_currency).to eq(@american_service.display_currency)
    end
  end
  context "multiple currency domain" do
    it "should have the same display and transaction currency" do
      expect(@indian_service.transaction_currency).to_not eq(@indian_service.display_currency)
    end
  end
end
