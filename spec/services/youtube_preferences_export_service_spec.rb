require "rails_helper"

describe YoutubePreferencesExportService do
  describe ".export" do
    let(:export) { double.as_null_object }
    let(:mailer) { double.as_null_object }

    before(:each) do
      create_list(:youtube_preference, 10)
    end

    it "writes the export and reset csv files" do
      exporter = YoutubePreferencesExportService.export
      expect(exporter.reset_csv).to_not be nil
      expect(exporter.export_csv).to_not be nil
    end

    it "sends an email" do
      allow(YoutubePreferencesExport).to receive(:create).and_return(export)
      expect(AdminNotifier).to receive(:youtube_preferences_export).and_return(mailer)
      YoutubePreferencesExportService.export
    end
  end
end
