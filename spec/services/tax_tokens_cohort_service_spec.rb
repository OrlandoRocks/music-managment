require "rails_helper"

describe TaxTokensCohortService do
  let(:file)   { fixture_file_upload("/tax_tokens_user_list.csv", 'text/csv') }

  let(:tax_tokens_cohort_params) {
    {
      name: "Example Cohort",
      start_date: Date.today,
      end_date: 4.days.from_now,
      file: file
    }
  }

  let(:tax_tokens_cohort_form) { TaxTokensCohortForm.new(tax_tokens_cohort_params) }

  describe ".create" do
    it "creates a tax tokens cohort" do
      expect{TaxTokensCohortService.create(
        name: tax_tokens_cohort_form.name,
        start_date: tax_tokens_cohort_form.start_date,
        end_date: tax_tokens_cohort_form.end_date,
        file: tax_tokens_cohort_form.file
      )}.to change{TaxTokensCohort.count}.by(1)
    end

    it "creates tax tokens for the tax tokens cohort" do
      TaxTokensCohortService.create(
        name: tax_tokens_cohort_form.name,
        start_date: tax_tokens_cohort_form.start_date,
        end_date: tax_tokens_cohort_form.end_date,
        file: tax_tokens_cohort_form.file
      )
      tax_tokens_cohort = TaxTokensCohort.first
      expect(tax_tokens_cohort.tax_tokens.count).to eq(3)
    end

    it "enqueues a tax token cohort mailer worker for today's date" do
      expect(TaxTokensCohortsMailWorker).to receive(:perform_async)
      TaxTokensCohortService.create(
        name: tax_tokens_cohort_form.name,
        start_date: tax_tokens_cohort_form.start_date,
        end_date: tax_tokens_cohort_form.end_date,
        file: tax_tokens_cohort_form.file
      )
    end

    it "enqueues a tax token cohort mailer worker in the future" do
      expect(TaxTokensCohortsMailWorker).to receive(:perform_in).with(
        be_within(1.day.to_i).of(4.days.to_i), kind_of(Numeric)
      )
      expect(TaxTokensCohortsMailWorker).to receive(:perform_in).with(
        be_within(1.day.to_i).of(2.days.to_i), kind_of(Numeric)
      )
      TaxTokensCohortService.create(
        name: tax_tokens_cohort_form.name,
        start_date: 2.days.from_now,
        end_date: tax_tokens_cohort_form.end_date,
        file: tax_tokens_cohort_form.file
      )
    end
  end
end
