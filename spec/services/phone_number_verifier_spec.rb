require "rails_helper"

describe PhoneNumberVerifier do
  describe "#register_authy_user" do
    it "sends register new user request to Authy" do
      params = {
        email:          "test@tunecore.com",
        phone_code:     "001",
        phone_number:   "123456789"
      }
      expect(Authy::API).to receive(:register_user).with(
        email:          "test@tunecore.com",
        country_code:   "001",
        cellphone:      "123456789"
      )
      PhoneNumberVerifier.register_authy_user(params)
    end
  end

  describe "#request_sms" do
    it "requests verification via sms" do
      allow(Authy::API).to receive(:register_user).and_return({"id" => "mock_authy_id"})
      expected_args = { id: "mock_authy_id", force: true }
      expect(Authy::API).to receive(:request_sms).with(expected_args)
      PhoneNumberVerifier.request_sms("mock_authy_id")
    end
  end

  describe "#verify_auth_code" do
    before(:each) do
      @authy_id = "22222"
      @auth_code = "12345"
      @mock_response = double(ok?: true)
      @expected_args = { token: @auth_code, id: @authy_id }
    end

    it "sends code to Authy for verification" do
      expect(Authy::API).to receive(:verify).with(@expected_args).and_return(@mock_response)
      PhoneNumberVerifier.verify_auth_code(@authy_id, @auth_code)
    end

    context "when auth code is accepted" do
      it "returns successful submission" do
        expect(Authy::API).to receive(:verify).with(@expected_args).and_return(@mock_response)
        response = PhoneNumberVerifier.verify_auth_code(@authy_id, @auth_code)
        expect(response.ok?).to eq true
      end
    end

    context "when auth code is denied" do
      it "returns failed submission" do
        mock_response = double(ok?: false)
        expect(Authy::API).to receive(:verify).with(@expected_args).and_return(mock_response)
        response = PhoneNumberVerifier.verify_auth_code(@authy_id, @auth_code)
        expect(response.ok?).to eq false
      end
    end
  end
end
