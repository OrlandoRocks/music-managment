require "rails_helper"

describe Delivery::Believe::ChangeStoreRestriction do
  let(:album) { FactoryBot.create(:album) }

  describe ".change_store_restriction" do
    before do
      @time = Time.now
      allow(Time).to receive(:now).and_return(@time)
      allow($believe_api_client).to receive(:post)
    end

    it "calls on the believe_api_client" do
      expect($believe_api_client).to receive(:post).with(
        "delivery/changestorerestriction",
        {
          idAlbumRemote:    album.id,
          idPlatformRemote: album.get_finalized_store_ids,
          actionType:       "allow",
          actionTimestamp:  @time.utc.to_i.to_s
        }
      )
      Delivery::Believe::ChangeStoreRestriction.change_store_restriction(album.id, "allow")
    end
  end
end
