require "rails_helper"

describe Delivery::Believe::NotifyDelivery do
  let(:album)     { FactoryBot.create(:album) }
  let(:store_ids) { album.salepoints.pluck(:store_id) }

  describe ".notify_delivery" do
    before(:each) do
      allow($believe_api_client).to receive(:post)
    end

    it "calls on the believe_api_client" do
      expect($believe_api_client).to receive(:post).with(
        "delivery/notify",
        {
          idAlbumRemote:    album.id,
          idPlatformRemote: store_ids,
          actionType:       "INSERT"
        }
      )
      Delivery::Believe::NotifyDelivery.notify_delivery(album.id, store_ids)
    end
  end
end
