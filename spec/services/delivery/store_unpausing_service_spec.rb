require "rails_helper"

describe Delivery::StoreUnpausingService do

  PAUSED_SNS_QUEUE = "delivery-sns-pause".freeze

  describe '#unpause' do
    before do
      @original_redis = $redis
      $redis = double(:redis)
      @person = create(:person)
      @remote_ip = '172.168.1.1'
      ENV['BATCH_RETRY_DAILY_LIMIT'] = '2000'
      allow(SecureRandom).to receive(:uuid).and_return('SecureRandomuuid')
      allow(Time).to receive(:now).and_return("Thu, 01 Oct 2020 11:26:12 +0000".to_datetime)
    end

    after do
      $redis = @original_redis
    end

    context 'when store is paused' do
      it 'does not get messages from the paused sns queue' do
        store = create(:store)
        create(:store_delivery_config, store: store, paused: true)

        expect_any_instance_of(described_class).to_not receive(:schedule_tc_distributor_paused_distributions!)
        Delivery::StoreUnpausingService.schedule_backfill!(store.id, @person.id, @remote_ip)
      end
    end

    context 'when store is unpaused but pause sns queue has no album' do
      it 'does not add any message to REDIS' do
        store = create(:store)
        create(:store_delivery_config, store: store, paused: false)

        job = double("job", args: [])
        sidekiq_queue = [ job ]
        allow(Sidekiq::Queue).to receive(:new).with(PAUSED_SNS_QUEUE).and_return(sidekiq_queue)

        expect_any_instance_of(described_class).to receive(:schedule_tc_distributor_paused_distributions!).and_call_original
        expect_any_instance_of(described_class).to receive(:fetch_albums_from_pause_queue).and_call_original
        expect_any_instance_of(described_class).to_not receive(:add_albums_to_batch_retrier_redis)
        expect_any_instance_of(described_class).to_not receive(:delete_jobs_from_pause_queue)

        Delivery::StoreUnpausingService.schedule_backfill!(store.id, @person.id, @remote_ip)
      end
    end

    context 'when store is unpaused and pause sns queue has albums for one store' do
      it 'add items to redis and delete sidekiq jobs' do
        store = create(:store)
        create(:store_delivery_config, store: store, paused: false)

        job1 = double("job", args: [[101, 102], store.id], jid: 1)
        job2 = double("job", args: [[101, 104], store.id], jid: 2)
        job3 = double("job", args: [[105, 106], store.id], jid: 3)
        sidekiq_queue = [ job1, job2, job3 ]

        allow(Sidekiq::Queue).to receive(:new).with(PAUSED_SNS_QUEUE).and_return(sidekiq_queue)

        expect_any_instance_of(described_class).to receive(:schedule_tc_distributor_paused_distributions!).and_call_original
        expect_any_instance_of(described_class).to receive(:fetch_albums_from_pause_queue).and_call_original
        expect_any_instance_of(described_class).to receive(:add_albums_to_batch_retrier_redis).and_call_original
        expect_any_instance_of(described_class).to receive(:delete_jobs_from_pause_queue).and_call_original

        expect(job1).to receive(:delete)
        expect(job2).to receive(:delete)
        expect(job3).to receive(:delete)

        expect($redis).to receive(:rpush).with(
          "distribution:batch-retry-albums:SecureRandomuuid",
          [101, 102, 104, 105, 106]
        )

        expect($redis).to receive(:hmset).with(
          "distribution:batch-retry:SecureRandomuuidStoreUnpause#{store.name.capitalize}1601551572",
          "person_id",
          @person.id,
          "store_ids",
          [store.id],
          "daily_retry_limit",
          "2000",
          "remote_ip",
          "172.168.1.1",
          "current_index",
          0,
          "uuid",
          "SecureRandomuuid",
          "job_name",
          "StoreUnpause#{store.name.capitalize}1601551572",
          "use_sns_only",
          true
        )

        Delivery::StoreUnpausingService.schedule_backfill!(store.id, @person.id, @remote_ip)
      end
    end

    context 'when store is unpaused and pause sns queue has albums for multiple stores' do
      it 'adds items to redis and delete items for only called store' do
        store = create(:store)
        create(:store_delivery_config, store: store, paused: false)

        job1 = double("job", args: [[101, 102], store.id], jid: 1)
        job2 = double("job", args: [[101, 104], store.id + 1], jid: 2)
        job3 = double("job", args: [[105, 106], store.id], jid: 3)
        sidekiq_queue = [ job1, job2, job3 ]

        allow(Sidekiq::Queue).to receive(:new).with(PAUSED_SNS_QUEUE).and_return(sidekiq_queue)

        expect_any_instance_of(described_class).to receive(:schedule_tc_distributor_paused_distributions!).and_call_original
        expect_any_instance_of(described_class).to receive(:fetch_albums_from_pause_queue).and_call_original
        expect_any_instance_of(described_class).to receive(:add_albums_to_batch_retrier_redis).and_call_original
        expect_any_instance_of(described_class).to receive(:delete_jobs_from_pause_queue).and_call_original

        expect(job1).to receive(:delete)
        expect(job2).to_not receive(:delete)
        expect(job3).to receive(:delete)

        expect($redis).to receive(:rpush).with(
          "distribution:batch-retry-albums:SecureRandomuuid",
          [101, 102, 105, 106]
        )

        expect($redis).to receive(:hmset).with(
          "distribution:batch-retry:SecureRandomuuidStoreUnpause#{store.name.capitalize}1601551572",
          "person_id",
          @person.id,
          "store_ids",
          [store.id],
          "daily_retry_limit",
          "2000",
          "remote_ip",
          "172.168.1.1",
          "current_index",
          0,
          "uuid",
          "SecureRandomuuid",
          "job_name",
          "StoreUnpause#{store.name.capitalize}1601551572",
          "use_sns_only",
          true
        )

        Delivery::StoreUnpausingService.schedule_backfill!(store.id, @person.id, @remote_ip)
      end
    end
  end
end
