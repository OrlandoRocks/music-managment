require 'rails_helper'

describe Delivery::Believe do
  let(:person)       { Person.find(42) }
  let(:album)        { create(:album, :approved) }
  let(:petri_bundle) { create(:petri_bundle, album: album) }
  let(:distribution) { create(:distribution, petri_bundle: petri_bundle, converter_class: "MusicStores::Facebook::Converter") }

  describe ".deliver" do
    before(:each) do
      with_versioning do
        Timecop.travel(Time.local(1985, 10, 21, 4, 29, 0))
        album.created_at = Time.now
        @song = create(:song, album: album, created_on: album.created_at, songstatus: "paid")
        @song_distribution_option = create(:song_distribution_option, song: @song)
        distribution.update_column(:updated_at, Time.now)
        Timecop.travel(Time.local(2015, 10, 21, 4, 29, 0))
        allow(Distribution).to receive(:find).with(distribution.id).and_return(distribution)
      end
    end

    after do
      Timecop.return
    end
    context "album with updated attributes" do
      it "retries the distribution" do
        with_versioning do
          album.update(name: "New Name")
          expect(distribution).to receive(:retry)
          Delivery::Believe.deliver(distribution.id, album.id, person.id)
        end
      end
    end

    context "songs with updated attributes" do
      it "retries the distribution" do
        @song.update(clean_version: true)
        expect(distribution).to receive(:retry)
        Delivery::Believe.deliver(distribution.id, album.id, person.id)
      end
    end

    context "song distributions with updated attributes" do
      it "retries the distribution" do
        @song_distribution_option.update(option_value: false)
        expect(distribution).to receive(:retry)
        Delivery::Believe.deliver(distribution.id, album.id, person.id)
      end
    end
  end
end
