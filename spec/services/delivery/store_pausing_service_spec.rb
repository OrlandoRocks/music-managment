require "rails_helper"

describe Delivery::StorePausingService do
  context "pausing" do
    it "pauses track monetizations" do
      album = create(:album, :purchaseable, :paid, :with_salepoint_song)
      song = album.songs.last
      store = Store.joins(:store_delivery_config).where(store_delivery_configs: { distributable_class: "TrackMonetization" }).first
      track_monetization = create(:track_monetization, :eligible, person: album.person, song: song, store: store)
      track_monetization.update_attribute(:state, "enqueued")

      Delivery::StorePausingService.pause!(store.id)

      expect(track_monetization.reload.state).to eq("paused")
    end

    it "pauses distributions" do
      distribution = create(:distribution, :with_salepoint, :enqueued)
      store = distribution.store

      Delivery::StorePausingService.pause!(store.id)

      expect(distribution.reload.state).to eq("paused")
    end

    it "deletes jobs from the queue" do
      distribution = create(:distribution, :with_salepoint, :enqueued)
      store = distribution.store
      job = double("job", args: [ distribution.class.name, distribution.id ])
      sidekiq_queue = [ job ]
      allow(Sidekiq::Queue).to receive(:new).with(store.queue).and_return(sidekiq_queue)
      expect(job).to receive(:delete)
      Delivery::StorePausingService.pause!(store.id)
    end
  end
end
