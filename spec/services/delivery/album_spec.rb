require "rails_helper"

describe Delivery::Album do
  let(:album) { create(:album, :with_salepoints, :paid, legal_review_state: "DO NOT REVIEW") }
  let!(:petri_bundle) { create(:petri_bundle, album: album) }

  before(:each) do
    purchase  = create(
      :purchase,
      product: Product.find_by(country_website: album.person.country_website),
      person: album.person,
      related: album)
    inventory = create(:salepoint_inventory, person: album.person, purchase: purchase)
    album.salepoints.each do |sp|
      create(:inventory_usage, inventory: inventory, related: sp)
    end
  end

  describe ".deliver" do
    it "should mark the album as NEEDS REVIEW" do
      Delivery::Album.deliver(album.id)
      expect(album.reload.legal_review_state).to eq "NEEDS REVIEW"
    end

    it "should create a distribution for all salepoints that are ready to distribute" do
      Delivery::Album.deliver(album.id)
      album.salepoints.each do |salepoint|
        expect(salepoint.reload.finalized_at).not_to eq nil
        expect(salepoint.reload.payment_applied).to eq true
      end
    end

    it "should call deliver on Delivery::Salepoint for each salepoint" do
      album.salepoints.each do |salepoint|
        expect(Delivery::Salepoint).to receive(:deliver).with(salepoint.id)
      end
      Delivery::Album.deliver(album.id)
    end
  end
end

