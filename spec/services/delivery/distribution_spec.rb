require "rails_helper"

describe Delivery::Distribution do
  let(:distributable) { create(:distribution, :with_distribution_songs) }

  it "calls deliver on a Distribution and enqueues it" do
    allow(Distribution).to receive(:find).and_return(distributable)
    expect(distributable).to receive(:deliver)
    Delivery::Distribution.deliver(distributable.class.name, distributable.id)
  end

  it "calls deliver on a DistributionSong" do
    distributable_song = distributable.distribution_songs.last
    allow(DistributionSong).to receive(:find).and_return(distributable_song)
    expect(distributable_song).to receive(:deliver)
    Delivery::Distribution.deliver(distributable_song.class.name, distributable_song.id)
  end

  it "uses specified delivery_type and queue" do
    allow(Distribution).to receive(:find).and_return(distributable)
    expect(distributable).to receive(:deliver).with("metadata_only", "queue")
    Delivery::Distribution.deliver(distributable.class.name, distributable.id, "metadata_only", "queue")
  end

  it "does not deliver a distributable to a blocked salepoint" do
    distributable.update(state: "blocked")
    Delivery::Distribution.deliver(distributable.class.name, distributable.id, "metadata_only", "queue", "ABCDEFG")
    expect(distributable.state).to eq("blocked")
    expect(distributable.reload.job_id).to eq("ABCDEFG")
  end

  it "sets the job id on the distributable if it is passed in" do
    track_monetization = create(:track_monetization)
    Delivery::Distribution.deliver(track_monetization.class.name, track_monetization.id, "metadata_only", "queue", "ABCDEFG")
    expect(track_monetization.reload.job_id).to eq("ABCDEFG")
  end
end
