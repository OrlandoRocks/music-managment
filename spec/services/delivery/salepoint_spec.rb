require "rails_helper"

describe Delivery::Salepoint do
  let(:album) { create(:album, :with_salepoints) }
  let!(:petri_bundle) { create(:petri_bundle, album: album) }

  describe ".execute" do
    before(:each) do
      allow(Album).to receive(:find).and_return(album)
      allow(album).to receive(:salepoints_to_distribute).and_return(album.salepoints)
    end

    it "should create a distribution for all salepoints that are ready to distribute" do
      Delivery::Salepoint.deliver(album.salepoints.first.id)
      expect(album.salepoints.first.distributions.length).to eq(1)
    end
  end
end

