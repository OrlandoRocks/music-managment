require "rails_helper"

describe Delivery::Retry do
  let(:admin) { create(:person, :admin) }
  let(:album) { create(:album) }
  let(:petri_bundle) { create(:petri_bundle, album: album) }
  let(:distribution) { create(:distribution, petri_bundle: petri_bundle, converter_class: "MusicStores::Facebook::Converter") }
  let(:track_monetization) { create(:track_monetization, :eligible, :delivered) }
  let(:distribution_retry) { Delivery::Retry.new({ delivery: distribution, delivery_type: "metadata_only", priority: 0, person: admin}) }
  let(:distribution_retry_takedown) { Delivery::Retry.new({ delivery: distribution, delivery_type: "metadata_only", priority: 0, person: admin}) }
  let(:track_monetization_retry) {Delivery::Retry.new({ delivery: track_monetization, delivery_type: "metadata_only", priority: 1, person: admin}) }
  let(:converter) {double(requires_track_distribution?: false)}
  let(:salepoint) { create(:salepoint, :salepointable_type => album.class.name, :salepointable_id => album.id) }

  before(:each) do
    allow(converter).to receive(:convert).and_return(true)
    allow_any_instance_of(Album).to receive(:genres).and_return([])
    s3_asset = double("s3_asset")
    allow(s3_asset).to receive(:key).and_return(double("key"))
    artwork = double("artwork")
    allow(artwork).to receive(:s3_asset).and_return(s3_asset)
    allow_any_instance_of(Album).to receive(:artwork).and_return(artwork)
    Sns::Notifier.instance_variable_set("@sns_client", nil)
    Sns::Notifier.instance_variable_set("@topic", nil)
    distribution.salepoints = [salepoint]
  end

  context "already delivered" do

    before(:each) do
      allow_any_instance_of(Distribution).to receive(:has_been_delivered?).and_return(true)
      allow_any_instance_of(Album).to receive(:takedown?).and_return(true)
    end

    it "retries a takedown delivery when already delivered" do
      expect(distribution_retry_takedown.instance_variable_get(:@delivery_type)).to eq('metadata_only')
      expect(distribution_retry_takedown.delivery).to receive(:takedown?).once.and_call_original
      expect(distribution_retry_takedown.delivery).to receive(:secondary_delivery).once.and_call_original
      expect(distribution_retry_takedown.delivery).to receive(:primary_delivery).and_call_original
      distribution_retry_takedown.retry
      expect(distribution_retry_takedown.delivery.state).to eql('enqueued')
    end

    it "retries a takedown delivery when never delivered and tc-distributor managed store" do
      allow_any_instance_of(Store).to receive(:delivered_by_tc_distributor?).and_return(true)
      expect(distribution_retry_takedown.instance_variable_get(:@delivery_type)).to eq('metadata_only')
      expect(distribution_retry_takedown.delivery).not_to receive(:takedown?)
      expect(distribution_retry_takedown.delivery).not_to receive(:secondary_delivery)
      expect(distribution_retry_takedown.delivery).not_to receive(:primary_delivery)
      expect(distribution_retry_takedown.delivery).to receive(:delivered_by_tc_distributor).with(any_args).and_call_original
      distribution_retry_takedown.retry
      expect(distribution_retry_takedown.delivery.state).to eql('delivered_via_tc_distributor')
    end

  end

  context "never delivered" do

    it "retries full delivery even when never delivered" do
      allow(distribution_retry.delivery).to receive(:never_delivered?).and_return true
      allow(distribution_retry.delivery).to receive(:metadata_update?).and_return false
      expect(distribution_retry.delivery).to receive(:retry)
      distribution_retry.retry
    end

    it "skips metadata_update when never delivered" do
      allow(distribution_retry.delivery).to receive(:never_delivered?).and_return true
      allow(distribution_retry.delivery).to receive(:metadata_update?).and_return true
      expect(distribution_retry.delivery).not_to receive(:retry)
      distribution_retry.retry
    end

    it "retries a takedown delivery when never delivered" do
      allow_any_instance_of(Album).to receive(:takedown?).and_return(true)
      expect(distribution_retry_takedown.instance_variable_get(:@delivery_type)).to eq('metadata_only')
      expect(distribution_retry_takedown.delivery).to receive(:takedown?).once.and_call_original
      expect(distribution_retry_takedown.delivery).to receive(:secondary_delivery).once.and_call_original
      expect(distribution_retry_takedown.delivery).not_to receive(:primary_delivery)
      distribution_retry_takedown.retry
      expect(distribution_retry_takedown.delivery.state).to eql('discarded')
    end

    it "retries a takedown delivery when never delivered and tc-distributor managed store" do
      allow_any_instance_of(Album).to receive(:takedown?).and_return(true)
      allow_any_instance_of(Store).to receive(:delivered_by_tc_distributor?).and_return(true)
      expect(distribution_retry_takedown.instance_variable_get(:@delivery_type)).to eq('metadata_only')
      expect(distribution_retry_takedown.delivery).not_to receive(:takedown?)
      expect(distribution_retry_takedown.delivery).not_to receive(:secondary_delivery)
      expect(distribution_retry_takedown.delivery).not_to receive(:primary_delivery)
      expect(distribution_retry_takedown.delivery).to receive(:delivered_by_tc_distributor).with(any_args).and_call_original
      distribution_retry_takedown.retry
      expect(distribution_retry_takedown.delivery.state).to eql('delivered_via_tc_distributor')
    end

  end

  context "priority == 0 (distributions only)" do
    it "sets distribution delivery state to enqueued if priority is 0" do
      distribution_retry.retry
      expect(distribution_retry.delivery.state).to eq "enqueued"
    end

    it "calls retry on distribution delivery" do
      expect(distribution_retry.delivery).to receive(:retry)
      distribution_retry.retry
    end
  end

  context "priority == 1 (track monetization retries are always priority)" do
    before { allow_any_instance_of(Delivery::Retry).to receive(:send_sns_notification) }
    before(:each) do
      distribution_retry.priority = 1
      allow_any_instance_of(Store).to receive(:delivered_by_tc_distributor?).and_return(false)
    end

    it "sets delivery state to enqueued" do
      distribution_retry.retry
      expect(distribution_retry.delivery.state).to eq "enqueued"

      track_monetization_retry.retry
      expect(track_monetization_retry.delivery.state).to eq "enqueued"
    end

    it "enqueues a Distribution retry via the Delivery::DistributionWorker using the delivery-critical queue" do
      distribution = distribution_retry.delivery
      expect(Delivery::DistributionWorker)
        .to receive(:set)
              .with(queue: "delivery-critical")
              .and_return(Delivery::DistributionWorker)
      expect(Delivery::DistributionWorker)
        .to receive(:perform_async)
              .with(distribution.class.name, distribution.id, "full_delivery")
      distribution_retry.retry
    end

    it "enqueues a TrackMonetization retry via the Delivery::DistributionWorker using the delivery-critical queue" do
      track_monetization = track_monetization_retry.delivery
      expect(Delivery::DistributionWorker)
        .to receive(:set)
        .with(queue: "delivery-critical")
        .and_return(Delivery::DistributionWorker)

      expect(Delivery::DistributionWorker)
        .to receive(:perform_async)
        .with(track_monetization.class.name, track_monetization.id, "full_delivery")

      track_monetization_retry.retry
    end

    it "enqueues a cleanup worker to remove any current distributions enqueued" do
      expect(Delivery::DistributionCleanupWorker).to receive(:perform_async).with(distribution.class.name, distribution.id)
      distribution_retry.retry
    end

    it "sends sns notification" do
      album_store_ids = [distribution.salepoints.first.store_id]
      excluded_store_ids = Store::EXCLUDED_FROM_NOTIFICATION_IDS
      valid_store_ids = (album_store_ids - excluded_store_ids).sort

      @sns_message = double
      @sns_client = instance_double(Aws::SNS::Resource)
      @topic = instance_double(Aws::SNS::Topic)

      sns_expected_message = {
        message: {
          album_id: album.id,
          stores: valid_store_ids.map { |store_id| { id: store_id, delivery_type: 'metadata_only', delivered: false }},
          person_id: admin.id
        }.to_json
      }

      allow_any_instance_of(Delivery::Retry).to receive(:send_sns_notification).and_call_original
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
      expect(Aws::SNS::Resource).to receive(:new).and_return(@sns_client)
      expect(@sns_client).to receive(:topic).with(any_args).and_return(@topic)
      expect(@topic).to receive(:publish).with(sns_expected_message)

      distribution_retry.retry
    end
  end
end
