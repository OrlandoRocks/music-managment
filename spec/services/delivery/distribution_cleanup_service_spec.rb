require "rails_helper"

describe Delivery::DistributionCleanupService do
  let(:job) { double(:job, args: [ "Distribution", 1 ], delete: true)}

  context "when cleaning up a distribution that has a job in the sidekiq dead set" do
    before do
      allow(Sidekiq::DeadSet).to receive(:new).and_return([job])
    end

    it "should delete the dead job" do
      expect(job).to receive(:delete)
      Delivery::DistributionCleanupService.cleanup("Distribution", 1)
    end
  end

  context "when cleaning up a distribution that has a job in the sidekiq delivery default set" do
    before do
      allow(Sidekiq::Queue).to receive(:new).and_return([])
      allow(Sidekiq::Queue).to receive(:new).with("delivery-default").and_return([job])
    end

    it "should delete the enqueued job" do
      expect(job).to receive(:delete)
      Delivery::DistributionCleanupService.cleanup("Distribution", 1)
    end
  end
end
