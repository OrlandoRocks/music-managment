require "rails_helper"

describe DistributionSong do
  describe ".deliver" do
    let(:album)          { create(:album, :with_salepoint_song) }
    let(:salepoint_song) { album.salepoints.first.salepoint_songs.first }
    let(:distribution)   { album.petri_bundle.distributions.where(converter_class: MusicStores::Spotify::Converter).first_or_create! }

    it "should create a DistributionSong" do
      expect {
        Delivery::DistributionSong.deliver(salepoint_song.id)
      }.to change(::DistributionSong, :count).by(1)
    end

    context "when the distribution_song album has not been approved" do
      it "should not deliver the distribution_song" do
        expect(Delivery::DistributionWorker).not_to receive(:perform_async)
        Delivery::DistributionSong.deliver(salepoint_song.id)
      end
    end

    context "when the distribution_song album was approved but not finalized " do
      it "should not deliver the distribution_song" do
        expect(Delivery::DistributionWorker).not_to receive(:perform_async)
        Delivery::DistributionSong.deliver(salepoint_song.id)
      end
    end

    context "when the distribution_song album was approved and finalized" do
      it "should deliver the distribution_song" do
        album.update(finalized_at: Time.now,
                                payment_applied: 1,
                                legal_review_state: "APPROVED")
        expect(Delivery::DistributionWorker).to receive_message_chain(:set, :perform_async)
        Delivery::DistributionSong.deliver(salepoint_song.id)
      end
    end

    context "when the song has a ytm_blocked_song" do
      it "should not deliver the distribution_song" do
        song = salepoint_song.song
        create(:ytm_blocked_song, song: song)
        expect(Delivery::DistributionWorker).not_to receive(:perform_async)
        Delivery::DistributionSong.deliver(salepoint_song.id)
      end
    end
  end
end

