require "rails_helper"

describe Delivery::AlbumConverter do
  let(:distro)      { create(:distribution, :with_distribution_songs, :with_salepoint, delivery_type: "full_delivery") }

  before(:each) do
    s3_asset = double("s3_asset")
    allow(s3_asset).to receive(:key).and_return(double("key"))
    allow(s3_asset).to receive(:bucket).and_return(double("bucket"))
    artwork = double("artwork")
    allow(artwork).to receive(:s3_asset).and_return(s3_asset)
    allow_any_instance_of(Album).to receive(:artwork).and_return(artwork)
  end

  describe "#has_valid_converter_class?" do
    it "checks that the converter_class is valid" do
      distro = create(:distribution, :with_salepoint, converter_class: "MusicStorez::Debunk::Converter")
      expect(Delivery::AlbumConverter.new(distro).has_valid_converter_class?).to eq false
    end
  end

  describe "#convert" do
    context "distribution" do
      it "returns the converted album" do
        converted_album = Delivery::AlbumConverter.new(distro).convert
        expect(converted_album.class.name).to eq "DistributionSystem::Believe::Album"
      end
    end

    context "#track_monetization" do
      it "returns the converted album" do
        album              = create(:album, :with_artwork)
        song               = create(:song, :with_upload, album: album)
        track_monetization = create(:track_monetization, song: song.reload)
        converted_album    = Delivery::AlbumConverter.new(track_monetization).convert
        expect(converted_album.class.name).to eq "DistributionSystem::TrackMonetization::Album"
      end
    end
  end
end
