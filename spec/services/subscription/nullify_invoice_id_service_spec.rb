require "rails_helper"

describe Subscription::NullifyInvoiceIDService do

  let!(:person)       { create(:person) }
  let!(:product)      { create(:product) }
  let!(:product_item) { create(:product_item, product: product) }
  let!(:invoice)      { create(:invoice, person: person, final_settlement_amount_cents: nil) }
  let!(:renewal)  do
    create(:renewal,
           person: person,
           item_to_renew_id: product_item.id,
           item_to_renew_type: product_item.class.name)
  end
  let!(:purchase) do
    create(:purchase,
            :unpaid,
            person: person,
            related: renewal,
            status: "failed",
            invoice: invoice)
  end

  subject { described_class.new(purchase) }

  it "should nullify an invoice_id on a failed auto-renewal purchase" do
    subject.nullify!

    expect(purchase.invoice_id).to be_nil
  end

  it "should NOT nullify an invoice_id on related settled invoice" do
    invoice.update(settled_at: DateTime.now)
    subject.nullify!

    expect(purchase.invoice_id).to be_truthy
  end

  it "should NOT nullify an invoice_id on related non-visible invoice" do
    invoice.update(batch_status: "processing_in_batch")
    subject.nullify!

    expect(purchase.invoice_id).to be_truthy
  end

  it "should NOT nullify an invoice_id on related invoice with final_settlement_amount_cents" do
    invoice.update(final_settlement_amount_cents: 0)
    subject.nullify!

    expect(purchase.invoice_id).to be_truthy
  end

  it "should NOT nullify an invoice_id on related invoice with final_settlement_amount_cents" do
    InvoiceSettlement.create(invoice: invoice)
    subject.nullify!

    expect(purchase.invoice_id).to be_truthy
  end

  context "non-renewal purchase" do
    let!(:album) { create(:album, person: person) }
    let!(:purchase) do
      create(:purchase,
              :unpaid,
              person: person,
              related: album,
              status: "failed",
              invoice: invoice)
    end

    it "raises an exception" do
      expect { subject.nullify! }
        .to raise_error(Subscription::NullifyInvoiceIDService::NullifyInvoiceIDServiceError)
    end
  end
end
