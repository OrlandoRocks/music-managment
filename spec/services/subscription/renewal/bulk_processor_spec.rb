require "rails_helper"

describe Subscription::Renewal::BulkProcessor do
  let!(:person) { create(:person, :with_braintree_blue_stored_credit_card) }

  describe ".renew" do
    context "Tunecore payment channel" do
      let!(:person_preference)     { create(:person_preference, person: person, preferred_credit_card: person.stored_credit_cards.first) }
      let!(:subscription_purchase) { create(:subscription_purchase, :social_us, person: person, payment_channel: "Tunecore") }
      let!(:subscription_status)   { create(:person_subscription_status, subscription_type: "Social", person: person) }
      let!(:subscription_event)    { create(:subscription_event, :social, person_subscription_status: subscription_status, subscription_purchase: subscription_purchase, event_type: "Renewal") }
      it "creates renewal for user with active subscription expiring today" do
        mock_braintree_blue_purchase
        subscription_status.effective_date   = 1.month.ago
        subscription_status.termination_date = Date.today
        subscription_status.save

        processor = Subscription::Renewal::BulkProcessor.renew([subscription_status], "Tunecore")
        expect(subscription_status.reload.termination_date.to_date).to eq Date.today + 1.month
        expect(processor.renewed).to include(person)
      end

      it "doesn't create renewal for user when credit card fails" do
        mock_braintree_blue_purchase("20")
        subscription_status.effective_date   = 2.months.ago
        subscription_status.termination_date = Date.today
        subscription_status.save

        processor = Subscription::Renewal::BulkProcessor.renew([subscription_status], "Tunecore")

        expect(subscription_status.reload.termination_date.to_date).to eq subscription_status.termination_date.to_date
        expect(processor.declined).to include(person)
      end
    end

    context "Apple payment channel" do
      let(:purchase_date) {Date.today}
      let(:expires_date)  {1.month.from_now}
      let!(:subscription_purchase) { create(:subscription_purchase, :social_us, person: person, payment_channel: "Apple") }
      let!(:subscription_status)   { create(:person_subscription_status, subscription_type: "Social", person: person) }
      let!(:subscription_event)    { create(:subscription_event, :social, person_subscription_status: subscription_status, subscription_purchase: subscription_purchase, event_type: "Renewal") }
      let!(:payment_channel_receipt) { create(:payment_channel_receipt, person_id: person.id, subscription_purchase_id: subscription_purchase.id)}

      it "creates renewal for user with active subscription expiring today" do
        fake_apple_api(
          body: {
            "status" => 0,
            "receipt" => {
              "in_app" => [
                {
                  "purchase_date" => purchase_date,
                  "expires_date" => expires_date
                }
              ]
            },
            "latest_receipt" => "abcdefg12345"
          },
          method_call: :make_post
        )

        subscription_status.effective_date   = 1.month.ago
        subscription_status.termination_date = Date.today
        subscription_status.save

        processor = Subscription::Renewal::BulkProcessor.renew([subscription_status], "Apple")
        expect(subscription_status.reload.termination_date.to_date).to eq Date.today + 1.month
        expect(processor.renewed).to include(person)
      end

      it "doesn't create renewal for user who did not renew with Apple" do
        fake_apple_api(
          body: {
            "status" => 0,
            "receipt" => {
              "in_app" => [
                {
                  "purchase_date" => purchase_date,
                  "expires_date" => Date.today
                }
              ]
            },
            "latest_receipt" => "abcdefg12345"
          },
          method_call: :make_post
        )
        subscription_status.effective_date   = 2.months.ago
        subscription_status.termination_date = Date.today
        subscription_status.save

        processor = Subscription::Renewal::BulkProcessor.renew([subscription_status], "Apple")
        expect(subscription_status.reload.termination_date.to_date).to eq subscription_status.termination_date.to_date
        expect(processor.declined).to include(person)
      end
    end
  end
end
