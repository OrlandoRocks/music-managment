require "rails_helper"

describe Subscription::Renewal::QueryBuilder do
  let!(:person)                { create(:person, :with_braintree_blue_stored_credit_card) }

  describe ".build" do
    context "Tunecore payment channel" do
      let!(:person_preference)     { create(:person_preference, person: person, preferred_credit_card: person.stored_credit_cards.first) }
      let!(:subscription_purchase) { create(:subscription_purchase, :social_us, person: person, payment_channel: "Tunecore") }
      let!(:subscription_status)   { create(:person_subscription_status, subscription_type: "Social", person: person) }
      let!(:subscription_event)    { create(:subscription_event, :social, person_subscription_status: subscription_status, subscription_purchase: subscription_purchase, event_type: "Renewal") }
      context ":today" do
        it "includes active subscription expiring today" do
          subscription_status.effective_date   = DateTime.now - 1.month
          subscription_status.termination_date = DateTime.now
          subscription_status.save

          builder = Subscription::Renewal::QueryBuilder.build(:today, "Social", "Tunecore")

          expect(builder.subscriptions).to include(subscription_status)
        end

        it "doesn't include subscriptions terminated before today" do
          subscription_status.effective_date   = DateTime.now - 2.months
          subscription_status.termination_date = DateTime.now - 1.month
          subscription_status.save

          builder = Subscription::Renewal::QueryBuilder.build(:today, "Social", "Tunecore")

          expect(builder.subscriptions).not_to include(subscription_status)
        end
      end

      context ":all" do
        it "includes subscription in the past" do
          subscription_status.effective_date   = DateTime.now - 2.months
          subscription_status.termination_date = DateTime.now - 1.month
          subscription_status.save

          builder = Subscription::Renewal::QueryBuilder.build(:all, "Social", "Tunecore")

          expect(builder.subscriptions).to include(subscription_status)
        end

        it "doesn't include today" do
          subscription_status.effective_date   = DateTime.now - 1.month
          subscription_status.termination_date = DateTime.now
          subscription_status.save

          builder = Subscription::Renewal::QueryBuilder.build(:all, "Social", "Tunecore")

          expect(builder.subscriptions).not_to include(subscription_status)
        end
      end

      context ":grace_period" do
        it "includes subscriptions that are in their grace_period" do
          subscription_status.effective_date   = DateTime.now - 1.month
          subscription_status.termination_date = DateTime.now - 1.day
          subscription_status.save

          builder = Subscription::Renewal::QueryBuilder.build(:grace_period, "Social", "Tunecore")

          expect(builder.subscriptions).to include(subscription_status)
        end

        it "doesn't include subscriptions expiring today" do
          subscription_status.effective_date   = DateTime.now - 1.month
          subscription_status.termination_date = DateTime.now
          subscription_status.save

          builder = Subscription::Renewal::QueryBuilder.build(:grace_period, "Social", "Tunecore")

          expect(builder.subscriptions).not_to include(subscription_status)
        end

        it "doesn't include subscriptions not in their grace period" do
          subscription_status.effective_date      = DateTime.now - 1.month
          subscription_status.termination_date    = DateTime.now + 10.days
          subscription_status.grace_period_length = 5
          subscription_status.save

          builder = Subscription::Renewal::QueryBuilder.build(:grace_period, "Social", "Tunecore")

          expect(builder.subscriptions).not_to include(subscription_status)
        end
      end

      context ":today_and_grace_period" do
        it "includes subscriptions expiring today" do
          subscription_status.effective_date   = DateTime.now - 1.month
          subscription_status.termination_date = DateTime.now
          subscription_status.save

          builder = Subscription::Renewal::QueryBuilder.build(:today_and_grace_period, "Social", "Tunecore")

          expect(builder.subscriptions).to include(subscription_status)
        end

        it "includes subscriptions that are in their grace_period" do
          subscription_status.effective_date      = DateTime.now - 1.month
          subscription_status.termination_date    = DateTime.now - 3.days
          subscription_status.save

          builder = Subscription::Renewal::QueryBuilder.build(:today_and_grace_period, "Social", "Tunecore")

          expect(builder.subscriptions).to include(subscription_status)
        end
      end
    end

    context "Apple payment channel" do
      let!(:subscription_purchase) { create(:subscription_purchase, :social_us, person: person, payment_channel: "Apple") }
      let!(:subscription_status)   { create(:person_subscription_status, subscription_type: "Social", person: person) }
      let!(:subscription_event)    { create(:subscription_event, :social, person_subscription_status: subscription_status, subscription_purchase: subscription_purchase, event_type: "Renewal") }
      let!(:payment_channel_receipt) { create(:payment_channel_receipt, person_id: person.id, subscription_purchase_id: subscription_purchase.id)}

      context ":today" do
        it "includes subscriptions expiring today" do
          subscription_status.effective_date   = DateTime.now - 1.month
          subscription_status.termination_date = DateTime.now
          subscription_status.save

          builder = Subscription::Renewal::QueryBuilder.build(:today, "Social", "Apple")

          expect(builder.subscriptions).to include(subscription_status)
        end

        it "doesn't include subscriptions terminated before today" do
          subscription_status.effective_date   = DateTime.now - 2.months
          subscription_status.termination_date = DateTime.now - 1.month
          subscription_status.save

          builder = Subscription::Renewal::QueryBuilder.build(:today, "Social", "Tunecore")

          expect(builder.subscriptions).not_to include(subscription_status)
        end
      end

      context ":all" do
        it "includes subscriptions in the past" do
          subscription_status.effective_date   = DateTime.now - 2.month
          subscription_status.termination_date = DateTime.now - 1.month
          subscription_status.save

          builder = Subscription::Renewal::QueryBuilder.build(:all, "Social", "Apple")

          expect(builder.subscriptions).to include(subscription_status)
        end

        it "doesn't include subscriptions expiring today" do
          subscription_status.effective_date   = DateTime.now - 1.month
          subscription_status.termination_date = DateTime.now
          subscription_status.save

          builder = Subscription::Renewal::QueryBuilder.build(:all, "Social", "Apple")

          expect(builder.subscriptions).not_to include(subscription_status)
        end
      end

      context ":grace_period" do
        it "includes subscriptions that are in their grace_period" do
          subscription_status.effective_date   = DateTime.now - 1.month
          subscription_status.termination_date = DateTime.now - 1.day
          subscription_status.save

          builder = Subscription::Renewal::QueryBuilder.build(:grace_period, "Social", "Apple")

          expect(builder.subscriptions).to include(subscription_status)
        end

        it "doesn't include subscriptions expiring today" do
          subscription_status.effective_date   = DateTime.now - 1.month
          subscription_status.termination_date = DateTime.now
          subscription_status.save

          builder = Subscription::Renewal::QueryBuilder.build(:grace_period, "Social", "Apple")

          expect(builder.subscriptions).not_to include(subscription_status)
        end

        it "doesn't include subscriptions not in their grace period" do
          subscription_status.effective_date      = DateTime.now - 1.month
          subscription_status.termination_date    = DateTime.now - 10.days
          subscription_status.grace_period_length = 5
          subscription_status.save

          builder = Subscription::Renewal::QueryBuilder.build(:grace_period, "Social", "Apple")

          expect(builder.subscriptions).not_to include(subscription_status)
        end
      end
    end
  end
end
