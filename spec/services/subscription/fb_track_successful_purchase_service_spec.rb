require "rails_helper"

describe Subscription::FbTrackSuccessfulPurchaseService do
  describe "#handle" do
    it "Enqueues a track monetization subscription worker" do
      person = create(:person, :with_live_album, :with_fb_tracks)

      purchase_request = double(person_id: person.id, product_name: "FBTracks", send_all: false)

      expect{
        Subscription::FbTrackSuccessfulPurchaseService.new(purchase_request).handle
      }.to change(TrackMonetization::SubscriptionWorker.jobs, :size).by(1)
    end
  end

end
