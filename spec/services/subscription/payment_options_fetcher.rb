require "rails_helper"

describe Subscription::PaymentOptionsFetcher do
  let(:person) {FactoryBot.create(:person, :with_tc_social, :with_stored_paypal_account, :with_stored_credit_card)}
  describe "#fetch_payment_options" do
    it "sets the #paypal and #credit_cards attributes" do
      fetcher = Subscription::PaymentOptionsFetcher.new(person)
      fetcher.fetch_payment_options
      expect(fetcher.paypal).to eq(person.stored_paypal_accounts.where(archived_at: nil).first)
      expect(fetcher.credit_cards).to eq(person.stored_credit_cards)
    end
  end
end
