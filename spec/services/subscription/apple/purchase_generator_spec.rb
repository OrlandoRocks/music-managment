require "rails_helper"

describe Subscription::Apple::PurchaseGenerator do
  let(:purchase_date)        { Date.today.to_s }
  let(:expires_date)         { 1.month.from_now.to_date.to_s }
  let(:person)               { create(:person, :with_tc_social) }
  let(:receipt_data)         { "abcd1234xyz" }
  let(:subscription_product) {
    SubscriptionProduct.active_by_product_type_for(person, "Social", "monthly")
  }
  describe "#generate" do
    context "success" do
      context "when the associated person_subscription_status already exists, was not canceled" do
        before do
          @person_subscription_status = PersonSubscriptionStatus.create(person_id: person.id, subscription_type: "Social")
          @generator =  Subscription::Apple::PurchaseGenerator.generate(subscription_product, person, purchase_date, expires_date, receipt_data)
        end
        it "creates the subscription_purchase" do
          expect(@generator.subscription_purchase).to_not eq nil
        end

        it "updates the person_subscription_status" do
          @person_subscription_status.reload
          expect(@person_subscription_status.effective_date.to_date.to_s).to eq(purchase_date)
          expect(@person_subscription_status.termination_date.to_date.to_s).to eq(expires_date)
        end

        it "creates a renewal event" do
          event = SubscriptionEvent.where(subscription_purchase_id: @generator.subscription_purchase.id, person_subscription_status_id: @person_subscription_status.id).first
          expect(event.event_type).to eq("Renewal")
        end
      end

      context "when the subscription purchase already exists" do
        before do
          @sub_purchase = SubscriptionPurchase.create(person_id: person.id, subscription_product_id: subscription_product.id, termination_date: expires_date)
          @generator = Subscription::Apple::PurchaseGenerator.generate(subscription_product, person, purchase_date, expires_date, receipt_data)
        end
        it "does not create any new records" do
          expect(SubscriptionPurchase.where(person_id: person.id, subscription_product_id: subscription_product.id, termination_date: expires_date).size).to eq(1)
          expect(@generator.subscription_purchase.id).to eq(@sub_purchase.id)
        end
      end

      context "when the associated person_subscription_status already exists, was canceled" do
        before do
          @person_subscription_status = PersonSubscriptionStatus.create(person_id: person.id, subscription_type: "Social", canceled_at: 1.month.ago)
          allow_any_instance_of(SubscriptionPurchase).to receive(:find_related_person_subscription_status) {@person_subscription_status}
          allow(@person_subscription_status).to receive(:canceled_at?) {true}
          @generator =  Subscription::Apple::PurchaseGenerator.generate(subscription_product, person, purchase_date, expires_date, receipt_data)
        end
        it "creates the subscription_purchase" do
          expect(@generator.subscription_purchase).to_not eq nil
        end

        it "updates the person_subscription_status" do
          @person_subscription_status.reload
          expect(@person_subscription_status.effective_date.to_date.to_s).to eq(purchase_date)
          expect(@person_subscription_status.termination_date.to_date.to_s).to eq(expires_date)
        end

        it "creates a purchase event" do
          event = SubscriptionEvent.where(subscription_purchase_id: @generator.subscription_purchase.id, person_subscription_status_id: @person_subscription_status.id).first
          expect(event.event_type).to eq("Purchase")
        end

        it "creates a payment channel receipt" do
          receipt = PaymentChannelReceipt.where(subscription_purchase_id: @generator.subscription_purchase.id, person_id: person.id, receipt_data: receipt_data).first
          expect(receipt).to_not eq nil
        end
      end

      context "when the associated person_subscription_status does not exist" do
        before(:each) do
          @generator =  Subscription::Apple::PurchaseGenerator.generate(subscription_product, person, purchase_date, expires_date, receipt_data)
        end
        it "creates the subscription_purchase" do
          expect(@generator.subscription_purchase).to_not eq nil
        end

        it "creates the person_subscription_status" do
          expect(@generator.subscription_purchase.find_related_person_subscription_status(subscription_product.product_name)).to_not eq nil
        end

        it "creates a purchase event" do
          person_subscription_status = @generator.subscription_purchase.find_related_person_subscription_status(subscription_product.product_name)
          event = SubscriptionEvent.where(subscription_purchase_id: @generator.subscription_purchase.id, person_subscription_status_id: person_subscription_status.id).first
          expect(event.event_type).to eq("Purchase")
        end
      end
    end
    context "failure" do
      context "subscription purchase could not be created" do
        it "adds the appropriate error" do
          allow(SubscriptionPurchase).to receive(:create!).and_return(nil)
          generator = Subscription::Apple::PurchaseGenerator.generate(subscription_product, person, purchase_date, expires_date, receipt_data)
          expect(generator.errors.messages[:base]).to include("Unable to create subscription purchase.")
        end
      end

      context "person person_subscription_status could not be created" do
        it "adds the appropriate error" do
          allow(PersonSubscriptionStatus).to receive(:create!).and_return(nil)
          generator = Subscription::Apple::PurchaseGenerator.generate(subscription_product, person, purchase_date, expires_date, receipt_data)
          expect(generator.errors.messages[:base]).to include("Unable to create person subscription status.")
        end
      end

       context "person person_subscription_status could not be updated" do
        before do
          @person_subscription_status = PersonSubscriptionStatus.create(person_id: person.id, subscription_type: "Social")
        end
        it "adds the appropriate error" do
          allow_any_instance_of(PersonSubscriptionStatus).to receive(:update).and_return(false)
          generator = Subscription::Apple::PurchaseGenerator.generate(subscription_product, person, purchase_date, expires_date, receipt_data)
          expect(generator.errors.messages[:base]).to include("Unable to update person subscription status.")
        end
      end

      context "subscription event could not be created" do
        it "adds the appropriate error" do
          allow(SubscriptionEvent).to receive(:create!).and_return(nil)
          generator = Subscription::Apple::PurchaseGenerator.generate(subscription_product, person, purchase_date, expires_date, receipt_data)
          expect(generator.errors.messages[:base]).to include("Unable to create subscription event.")
        end
      end
    end

  end
end
