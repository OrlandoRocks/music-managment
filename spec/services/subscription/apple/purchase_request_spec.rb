require "rails_helper"

describe Subscription::Apple::PurchaseRequest do
  let(:purchase_date) {Date.yesterday.to_s}
  let(:expires_date)  {1.month.from_now.to_date.to_s}
  let(:params) do
   {
     expires_date: expires_date,
     purchase_date: purchase_date,
     product_name: "Social",
     product_type: "monthly",
     receipt_data: "1234abcd"
   }
  end

  before(:each) do
    allow(Person::AfterCommitCallbacks).to receive(:send_to_zendesk).and_return(true)
  end

  describe "#new" do
    it "sets attrs" do
      person             = create(:person, :with_tc_social)
      params[:person_id] = person.id
      purchase_handler   = Subscription::Apple::PurchaseRequest.new(params)

      purchase_handler.finalize

      expect(purchase_handler.plan).to eq "pro"
      expect(purchase_handler.product_name).to eq "Social"
      expect(purchase_handler.person).to eq person
      expect(purchase_handler.plan_expires_at.to_date.to_s).to eq(expires_date)
    end

    it "adds error if unknown product name" do
      person                  = create(:person, :with_tc_social)
      params[:product_name]   = "social"
      params[:person_id]      = person.id
      purchase_handler        = Subscription::Apple::PurchaseRequest.new(params)

      purchase_handler.finalize

      expect(purchase_handler.errors[:base].last).to eq "Invalid product name."
    end
  end

  describe "#finalize", run_sidekiq_worker: true do
    context "success" do
      before(:each) do
        allow_any_instance_of(Believe::ApiClient).to receive(:post).and_return(true)
        @person                = create(:person, :with_tc_social)
        params[:person_id]     = @person.id
        @purchase_handler      = Subscription::Apple::PurchaseRequest.new(params)
      end

      it "is true create subscription for person" do
        expect(@purchase_handler.finalize).to eq true
        subscription_purchase = @purchase_handler.subscription_purchase
        subscription_status   = PersonSubscriptionStatus.where(person_id: @person.id, subscription_type: "Social").first
        subscription_event    = SubscriptionEvent.where(subscription_purchase_id: subscription_purchase.id, person_subscription_status_id: subscription_status.id).first

        expect(@purchase_handler.person.has_active_subscription_product_for?("Social")).to eq true
        expect(subscription_event).to_not eq nil
      end

      it "sends upgrade email" do
        fake_mail = double(deliver: true)
        expect(SocialNotifier).to receive(:apple_purchase).and_return(fake_mail)
        @purchase_handler.finalize
      end
    end

    context "failure" do
      it "is false and doesn't create subscription" do
        allow_any_instance_of(Believe::ApiClient).to receive(:post).and_return(true)
        person = create(:person, :with_tc_social)
        params[:person_id]    = person.id
        params[:product_name] = "wrong"
        purchase_handler = Subscription::Apple::PurchaseRequest.new(params)
        expect(purchase_handler.finalize).to eq false
        expect(purchase_handler.errors).not_to be_empty
        expect(purchase_handler.person.has_active_subscription_product_for?("Social")).to eq false
      end
    end
  end
end
