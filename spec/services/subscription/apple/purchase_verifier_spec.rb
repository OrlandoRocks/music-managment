require "rails_helper"

describe Subscription::Apple::PurchaseVerifier do
  let(:person)        { FactoryBot.create(:person, :with_tc_social) }
  let(:purchase_date) {Date.today.to_s}
  let(:expires_date)  {1.year.from_now.to_date.to_s}
  let(:receipt_data)  { "abcdefg12345" }

  let(:verifier) {Subscription::Apple::PurchaseVerifier.new("abcdefg12345")}

  describe ".execute" do
    before(:each) do
      fake_apple_api(
          body: {
            "status" => 0,
            "receipt" => {
              "in_app" => [
                {
                  "purchase_date" => purchase_date,
                  "expires_date" => expires_date
                }
              ]
            },
            "latest_receipt" => "abcdefg12345"
          },
          method_call: :make_post
        )
    end
    it "instantiates an instance of the Subscription::Apple::PurchaseVerifier class" do
      expect(Subscription::Apple::PurchaseVerifier).to receive(:new).with("abcdefg12345") {double({verify!: true, verification_details: {}})}
      Subscription::Apple::PurchaseVerifier.execute("abcdefg12345")
    end

    it "calls the #verify! method" do
      expect_any_instance_of(Subscription::Apple::PurchaseVerifier).to receive(:verify!)
      Subscription::Apple::PurchaseVerifier.execute("abcdefg12345")
    end

    it "returns the payment verification details" do
      details = Subscription::Apple::PurchaseVerifier.execute("abcdefg12345")
      expect(details).to eq({
        purchase_date: purchase_date,
        expires_date: expires_date,
        receipt_data: "abcdefg12345"
      })
    end
  end

  describe "#verify!" do
    let(:purchase_verifier) {Subscription::Apple::PurchaseVerifier.new("abcdefg12345")}
    it "POSTS to the Apple Itunes API" do
      fake_apple_api(
          body: {
            "status" => 0,
            "receipt" => {
              "in_app" => [
                {
                  "purchase_date" => purchase_date,
                  "expires_date" => expires_date
                }
              ]
            },
            "latest_receipt" => "abcdefg12345"
          },
          method_call: :make_post
        )
      expect($apple_client).to receive(:post) {double({is_successful: true, data: {}})}
      purchase_verifier.verify!
    end

    context "success from apple" do
      before do
        fake_apple_api(
          body: {
            "status" => 0,
            "receipt" => {
              "in_app" => [
                {
                  "purchase_date" => purchase_date,
                  "expires_date" => expires_date
                }
              ]
            },
            "latest_receipt" => "abcdefg12345"
          },
          method_call: :make_post
        )
      end
      it "sets the payment verification details" do
        purchase_verifier.verify!
        expect(purchase_verifier.verification_details).to eq({
          purchase_date: purchase_date,
          expires_date: expires_date,
          receipt_data: "abcdefg12345"
        })
      end
    end

    context "failure from apple" do
      before do
        fake_apple_api(
          body: {
            status: 2100
          },
          method_call: :make_post
        )
      end
      it "raises an error" do
        expect{purchase_verifier.verify!}.to raise_error Subscription::Apple::PurchaseVerifier::VerificationError
      end
    end
  end
end
