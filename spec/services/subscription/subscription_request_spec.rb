require "rails_helper"

describe Subscription::SubscriptionRequest do
  context "person has TCS subscription" do
    let(:person) { create(:person, :with_social_subscription_status_and_event_and_purchase) }
    let(:request){ Subscription::SubscriptionRequest.get(person, "Social") }
    describe ".get" do
      it "sets the subscription" do
        request = Subscription::SubscriptionRequest.get(person, "Social")
        expect(request.subscription.payment_plan).to eq("monthly")
        expect(request.subscription.payment_channel).to eq("Tunecore")
        expect(request.subscription.effective_date).to eq(person.subscription_status_for("Social").effective_date)
        expect(request.subscription.termination_date).to eq(person.subscription_status_for("Social").termination_date)
        expect(request.subscription.canceled_at).to eq(nil)
      end

      it "calculates the user's plan and plan_expires_at attributes" do
        expect(request.subscription.plan).to eq("pro")
      end
    end
    describe '.cancel' do
      let(:request) {Subscription::SubscriptionRequest.cancel(person, "Social")}
      it 'cancels the subscription' do
        expect(request.subscription.payment_plan).to eq("monthly")
        expect(request.subscription.payment_channel).to eq("Tunecore")
        expect(request.subscription.effective_date).to eq(person.subscription_status_for("Social").effective_date)
        expect(request.subscription.termination_date.to_date).to eq(person.subscription_status_for("Social").termination_date.to_date)
        expect(request.subscription.canceled_at.to_date).to eq(Date.today)
      end

      it "is successful" do
        expect(request.success).to eq(true)
      end
    end
  end

  context "person does not have TCS subscription" do
    let(:person) {create(:person, :with_tc_social)}
    describe ".get" do
      it "sets the subscription" do
        request = Subscription::SubscriptionRequest.get(person, "Social")
        expect(request.subscription.payment_plan).to eq(nil)
        expect(request.subscription.payment_channel).to eq(nil)
        expect(request.subscription.canceled_at).to eq(nil)
        expect(request.subscription.effective_date).to eq(nil)
        expect(request.subscription.termination_date).to eq(nil)
        expect(request.subscription.plan).to eq(nil)
        expect(request.subscription.plan_expires_at).to eq(nil)
      end
    end

    describe ".cancel" do
      before(:each) do
        allow(person).to receive(:subscription_status_for).and_return nil
        @request = Subscription::SubscriptionRequest.cancel(person, "Social")
      end
      it "sets an error" do
        expect(@request.errors[:base]).to include("could not cancel subscription")
      end

      it "is not successful" do
        expect(@request.success).to eq(nil)
      end
    end
  end
end
