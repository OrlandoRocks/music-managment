require "rails_helper"

describe Subscription::Subscription do
  let(:person) { create(:person, :with_social_subscription_status_and_event_and_purchase)}
  let(:person_subscription_status) { person.subscription_status_for("Social") }
  describe ".new" do
    it "sets the subscription's attributes" do
      subscription = Subscription::Subscription.new(person)
      expect(subscription.user).to eq(person)
      expect(subscription.plan).to eq("pro")
      expect(subscription.plan_expires_at).to eq(person_subscription_status.termination_date.to_date)
      expect(subscription.payment_plan).to eq("monthly")
      expect(subscription.payment_channel).to eq("Tunecore")
      expect(subscription.effective_date).to eq(person_subscription_status.effective_date)
      expect(subscription.termination_date).to eq(person_subscription_status.termination_date)
      expect(subscription.canceled_at).to eq(nil)
    end
  end

  describe "#cancel" do
    let(:subscription) {Subscription::Subscription.new(person)}
    context "success" do
      it "returns true" do
        expect(subscription.cancel).to eq(true)
      end

      it "sets the cancellation date of the subscription and the person_subscription_status" do
        subscription.cancel
        expect(subscription.canceled_at.to_date).to eq(Date.today)
        expect(person_subscription_status.canceled_at.to_date).to eq(Date.today)
      end

      it "creates a cancellation event" do
          expect(SubscriptionEvent).to receive(:create).with({
          subscription_purchase_id: subscription.purchase.id,
          person_subscription_status_id: person_subscription_status.id,
          event_type: "Cancellation",
          subscription_type: person_subscription_status.subscription_type
        })
        subscription.cancel
      end

      it "sends the cancellation email" do
        expect(SocialNotifierWorker).to receive(:perform_async).with(:pro_user_cancellation, [person.id])
        subscription.cancel
      end

      it "creates a note if the user has included one" do
        subscription.cancel("cancel")
        note = Note.last
        expect(note.related_id).to eq(SubscriptionEvent.last.id)
        expect(note.related_type).to eq("SubscriptionEvent")
        expect(note.note).to eq("cancel")
      end
    end

    context "failure" do
      it "returns false" do
        allow(subscription.plan_status.subscription_status).to receive(:cancel).and_return(false)
        expect(subscription.cancel).to eq(false)
      end
    end
  end
end
