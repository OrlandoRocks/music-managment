require "rails_helper"

describe Subscription::Tunecore::PurchaseRequest do
  before(:each) do
    mock_braintree_blue_purchase
  end

  let(:params) do
    {
      payment_method: "credit_card",
      product_name: "Social",
      product_type: "monthly",
      request_ip: "0.0.0.0" }
  end

  describe "#finalize" do
    it "sets attrs" do
      person             = create(:person, :with_tc_social)
      params[:person_id] = person.id
      purchase_handler   = Subscription::Tunecore::PurchaseRequest.new(params)
      purchase_handler.finalize

      expect(purchase_handler.payment_method).to eq "credit_card"
      expect(purchase_handler.product_name).to eq "Social"
      expect(purchase_handler.person).to eq person
    end

    it "sets the payment manager for credit cards" do
      person                = create(:person, :with_tc_social, :with_braintree_blue_stored_credit_card)
      params[:last_four_cc] = person.stored_credit_cards.last.last_four
      params[:person_id]    = person.id

      purchase_handler = Subscription::Tunecore::PurchaseRequest.new(params)

      purchase_handler.finalize

      expect(purchase_handler.manager.strategies).to include(purchase_handler.payment_strategy)
    end

    it "sets the payment manager for paypal" do
      person                  = create(:person, :with_tc_social, :with_stored_paypal_account)
      params[:payment_method] = "paypal"
      params[:person_id]      = person.id
      purchase_handler        = Subscription::Tunecore::PurchaseRequest.new(params)
      paypal_transaction = double(
        PayPalSDKCallers::Transaction,
        success: true,
        response: "Success"
      )
      expect(PayPalAPI).to receive(:reference_transaction).and_return(paypal_transaction)

      purchase_handler.finalize

      expect(purchase_handler.manager.strategies).to include(purchase_handler.payment_strategy)
    end

    it "cleans up any unpaid Social purchases" do
      create(:purchase, related_type: "SubscriptionPurchase", related_id: 0, paid_at: nil)
      person                = create(:person, :with_tc_social, :with_braintree_blue_stored_credit_card)
      params[:person_id]    = person.id
      params[:last_four_cc] = 3456
      purchase_handler      = Subscription::Tunecore::PurchaseRequest.new(params)

      purchase_handler.finalize
      expect(purchase_handler.subscription_purchase.payment_channel).to eq "Tunecore"
    end

    it "adds an error if the payment could not be processed" do
      person = create(:person, :with_tc_social, :with_braintree_blue_stored_credit_card)
      params[:person_id]    = person.id
      params[:last_four_cc] = 1234
      purchase_handler      = Subscription::Tunecore::PurchaseRequest.new(params)
      allow_any_instance_of(Invoice).to receive(:settled?).and_return(false)
      allow_any_instance_of(BraintreeTransaction).to receive(:save).and_return(true)

      purchase_handler.finalize

      expect(purchase_handler.errors[:base].last).to eq "Unable to finalize purchase"
    end

    it "adds error if unknown product name" do
      person                  = create(:person, :with_tc_social, :with_braintree_blue_stored_credit_card)
      params[:product_name]   = "social"
      params[:person_id]      = person.id
      purchase_handler        = Subscription::Tunecore::PurchaseRequest.new(params)

      purchase_handler.finalize

      expect(purchase_handler.errors[:base].last).to eq "Invalid product name."
    end

    it "adds error if unsupported payment type" do
      person                  = create(:person, :with_tc_social, :with_braintree_blue_stored_credit_card)
      params[:payment_method] = "balance"
      params[:person_id]      = person.id
      purchase_handler        = Subscription::Tunecore::PurchaseRequest.new(params)

      purchase_handler.finalize

      expect(purchase_handler.errors[:base].last).to eq "Unsupported payment type for Social."
    end

    it "adds error if no payment payment_strategy" do
      person                = create(:person, :with_tc_social, :with_braintree_blue_stored_credit_card)
      params[:person_id]    = person.id
      params[:last_four_cc] = 3456
      purchase_handler      = Subscription::Tunecore::PurchaseRequest.new(params)

      purchase_handler.finalize

      expect(purchase_handler.errors[:base].last).to eq "Unable to add payment strategy."
    end

    it "create subscription_purchase with payment_channel of Tunecore" do
      person                = create(:person, :with_tc_social, :with_braintree_blue_stored_credit_card)
      params[:person_id]    = person.id
      params[:last_four_cc] = 3456
      purchase_handler      = Subscription::Tunecore::PurchaseRequest.new(params)

      purchase_handler.finalize

      expect(purchase_handler.subscription_purchase.payment_channel).to eq "Tunecore"
    end
    context "success" do
      before(:each) do
        @person                = create(:person, :with_tc_social, :with_braintree_blue_stored_credit_card)
        params[:person_id]    = @person.id
        params[:last_four_cc] = @person.stored_credit_cards.last.last_four
        @mail                  = double("mail", deliver: true)
        allow(SocialNotifier).to receive(:upgrade_confirmation).and_return(@mail)
        @purchase_handler = Subscription::Tunecore::PurchaseRequest.new(params)
      end

      it "is true create subscription for person" do
        expect(@mail).to receive(:deliver)
        expect(@purchase_handler.finalize).to eq true
        expect(@purchase_handler.person.has_active_subscription_product_for?("Social")).to eq true
      end

      it "is true enques 3 trends data aggregation jobs for the date ranges: 7 day, 30 days, 90 days" do
        expect(TrendDataAggregationWorker).to receive(:perform_async).with(@person.id, 90.days.ago.to_date.to_s, Date.yesterday.to_s)
        expect(TrendDataAggregationWorker).to receive(:perform_async).with(@person.id, 30.days.ago.to_date.to_s, Date.yesterday.to_s)
        expect(TrendDataAggregationWorker).to receive(:perform_async).with(@person.id, 7.days.ago.to_date.to_s, Date.yesterday.to_s)
        allow(@purchase_handler).to receive(:handle_successful_purchase).and_return(true)
        @purchase_handler.finalize
      end

      it "does not fail if Gtm::Invoice raises an error" do
        expect(Gtm::Invoice).to receive(:build_user_data).and_raise("Error in Gtm::Invoice")
        expect(@purchase_handler.finalize).to eq true
      end
    end

    context "failure" do
      it "is false and doesn't create subscription" do
        person = create(:person, :with_tc_social, :with_braintree_blue_stored_credit_card)
        params[:person_id]    = person.id
        params[:last_four_cc] = person.stored_credit_cards.last.last_four
        mock_braintree_blue_purchase(processor_response_code = "20")
        purchase_handler = Subscription::Tunecore::PurchaseRequest.new(params)
        allow_any_instance_of(Invoice).to receive(:settled?).and_return(false)
        allow_any_instance_of(BraintreeTransaction).to receive(:save).and_return(true)

        expect(purchase_handler.finalize).to eq false
        expect(purchase_handler.errors).not_to be_empty
        expect(purchase_handler.person.has_active_subscription_product_for?("Social")).to eq false
      end
    end
  end
end
