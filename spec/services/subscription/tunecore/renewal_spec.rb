require "rails_helper"

describe Subscription::Tunecore::Renewal do
  let(:person)                 { create(:person) }
  let!(:subscription_purchase) { create(:subscription_purchase, :social_us, person: person) }
  let(:subscription_status)    { create(:person_subscription_status, :with_subscription_event_and_purchase, person: person) }
  let!(:stored_credit_card)    { create(:stored_credit_card, person: person) }
  let!(:person_preference)     { create(:person_preference, person: person, pay_with_balance: 1, preferred_credit_card: stored_credit_card) }

  before do
    @fake_manager = double(process!: true, successful?: true, add_strategy: true, invoice: double(destroy: true))
    @fake_strat = double
    allow(Cart::Payment::Manager).to receive(:new).and_return(@fake_manager)
  end

  describe "#renew" do
    it "creates a new subscription_purchase with same product as previous" do
      renewal = Subscription::Tunecore::Renewal.new(subscription_status)
      renewal.renew
      expect(renewal.subscription_purchase.subscription_product_id).to eq subscription_purchase.subscription_product_id
    end

    it "create a new subscription_purchase with payment_channel = 'Tunecore'" do
      renewal = Subscription::Tunecore::Renewal.new(subscription_status)
      renewal.renew
      expect(renewal.subscription_purchase.payment_channel).to eq "Tunecore"
    end

    it "creates a new purchase record" do
      renewal = Subscription::Tunecore::Renewal.new(subscription_status)
      renewal.renew
      expect(renewal.purchase.persisted?).to eq true
    end

    it "uses credit card when preference set" do
      fake_cc_strat = double
      renewal = Subscription::Tunecore::Renewal.new(subscription_status)
      expect(Cart::Payment::Strategies::CreditCard).to receive(:new)
        .with(stored_credit_card.id).and_return(fake_cc_strat)
      expect(@fake_manager).to receive(:add_strategy).with(fake_cc_strat)
      renewal.renew
    end

    it "uses paypal when preference set" do
      stored_paypal_account = create(:stored_paypal_account, person: person)
      person_preference.preferred_payment_type = "PayPal"
      person_preference.preferred_paypal_account = stored_paypal_account
      person_preference.save
      @fake_strat = double
      renewal = Subscription::Tunecore::Renewal.new(subscription_status)
      expect(Cart::Payment::Strategies::Paypal).to receive(:new)
        .with(stored_paypal_account, person).and_return(@fake_strat)
      expect(@fake_manager).to receive(:add_strategy).with(@fake_strat)
      renewal.renew
    end
  end

  describe "#successful?" do
    it "is true when payment succeeds" do
      renewal = Subscription::Tunecore::Renewal.new(subscription_status)
      renewal.renew
      expect(renewal.successful?).to eq true
    end

    it "is false when payment fails" do
      allow(@fake_manager).to receive(:successful?).and_return(false)
      renewal = Subscription::Tunecore::Renewal.new(subscription_status)
      renewal.renew
      expect(renewal.successful?).to eq false
    end

    it "deletes subscription_purchase" do
      allow(@fake_manager).to receive(:successful?).and_return(false)
      renewal = Subscription::Tunecore::Renewal.new(subscription_status)
      renewal.renew
      expect(renewal.subscription_purchase.persisted?).to eq false
    end
  end

  context "targeted offers" do
    before(:each) do
      social_product    = subscription_purchase.subscription_product.product
      @targeted_offer   = create(:targeted_offer)
      @targeted_product = create(:targeted_product, :one_free_month, targeted_offer: @targeted_offer, product: social_product)
      @targeted_offer.update_attribute(:status, "Active")
      @targeted_person  = create(:targeted_person, person: person, targeted_offer: @targeted_offer)
    end

    context "for 1 free month" do
      before(:each) do
        @targeted_offer.update_attribute("usage_limit", 1)
      end

      it "gives discount for one month only" do
        # create renewal, check discount
        renewal_1 = Subscription::Tunecore::Renewal.new(subscription_status)
        renewal_1.renew
        expect(renewal_1.purchase.discount_cents).to eq 799
        expect(renewal_1.purchase.purchase_total_cents).to eq 0

        # create and settle invoice (to mark targeted offer as used)
        invoice_1 = create(:invoice, person: person)
        renewal_1.purchase.attach_to_invoice(invoice_1)
        invoice_1.settlement_received(renewal_1.purchase, renewal_1.purchase.purchase_total_cents)
        invoice_1.settled!

        # final amt should be 0 bc of targeted_offer
        expect(invoice_1.final_settlement_amount_cents).to eq 0

        renewal_2 = Subscription::Tunecore::Renewal.new(subscription_status)
        renewal_2.renew
        expect(renewal_2.purchase.discount_cents).to eq 0
        expect(renewal_2.purchase.purchase_total_cents).to eq 799

        invoice_2 = create(:invoice, person: person)
        renewal_2.purchase.attach_to_invoice(invoice_2)
        invoice_2.settlement_received(renewal_2.purchase, renewal_2.purchase.cost_cents)
        invoice_2.settled!

        # final amt should be full price bc targeted_person reached targeted_offer usage_limit
        expect(invoice_2.final_settlement_amount_cents).to eq 799
      end
    end

    context "for 6 free months" do
      before(:each) do
        @targeted_offer.update_attribute("usage_limit", 6)
      end

      it "gives discount for six month only" do
        6.times do
          # create renewal, check discount
          renewal  = Subscription::Tunecore::Renewal.new(subscription_status)
          renewal.renew
          expect(renewal.purchase.discount_cents).to eq 799
          expect(renewal.purchase.purchase_total_cents).to eq 0

          # create and settle invoice (to mark targeted offer as used)
          invoice = create(:invoice, person: person)
          renewal.purchase.attach_to_invoice(invoice)
          invoice.settlement_received(renewal.purchase, renewal.purchase.purchase_total_cents)
          invoice.settled!

          # final amt should be 0 bc of targeted_offer
          expect(invoice.final_settlement_amount_cents).to eq 0
        end

        renewal_7 = Subscription::Tunecore::Renewal.new(subscription_status)
        renewal_7.renew
        expect(renewal_7.purchase.discount_cents).to eq 0
        expect(renewal_7.purchase.purchase_total_cents).to eq 799

        invoice_7 = create(:invoice, person: person)
        renewal_7.purchase.attach_to_invoice(invoice_7)
        invoice_7.settlement_received(renewal_7.purchase, renewal_7.purchase.cost_cents)
        invoice_7.settled!

        # final amt should be full price bc targeted_person reached targeted_offer usage_limit
        expect(invoice_7.final_settlement_amount_cents).to eq 799
      end
    end

    context "for $5 off for 2 months" do
      before(:each) do
        @targeted_offer.update_attribute("usage_limit", "2")
        @targeted_product.update({price_adjustment_type: "dollars_off", price_adjustment: 5})
      end

      it "gives discount for six month only" do
        2.times do
          # create renewal, check discount
          renewal  = Subscription::Tunecore::Renewal.new(subscription_status)
          renewal.renew
          expect(renewal.purchase.discount_cents).to eq 500
          expect(renewal.purchase.purchase_total_cents).to eq 299

          # create and settle invoice (to mark targeted offer as used)
          invoice = create(:invoice, person: person)
          renewal.purchase.attach_to_invoice(invoice)
          invoice.settlement_received(renewal.purchase, renewal.purchase.purchase_total_cents)
          invoice.settled!

          # final amt should be 0 bc of targeted_offer
          expect(invoice.final_settlement_amount_cents).to eq 299
        end

        renewal_7 = Subscription::Tunecore::Renewal.new(subscription_status)
        renewal_7.renew
        expect(renewal_7.purchase.discount_cents).to eq 0
        expect(renewal_7.purchase.purchase_total_cents).to eq 799

        invoice_7 = create(:invoice, person: person)
        renewal_7.purchase.attach_to_invoice(invoice_7)
        invoice_7.settlement_received(renewal_7.purchase, renewal_7.purchase.cost_cents)
        invoice_7.settled!

        # final amt should be full price bc targeted_person reached targeted_offer usage_limit
        expect(invoice_7.final_settlement_amount_cents).to eq 799
      end
    end
  end
end
