require "rails_helper"

describe Subscription::SocialSuccessfulPurchaseService do
  before :each do
    mock_braintree_blue_purchase

    @purchase_person  = create(:person, :with_tc_social, :with_braintree_blue_stored_credit_card)
    sub_params = {
      person_id: @purchase_person.id,
      last_four_cc: @purchase_person.stored_credit_cards.last.last_four,
      payment_method: "credit_card",
      product_name: "Social",
      product_type: "monthly",
      request_ip: "0.0.0.0"
    }

    @purchase_request = Subscription::Tunecore::PurchaseRequest.new(sub_params)
    @purchase_request.run_callbacks(:finalize)
    @purchase_request.manager.process!
    @success = Subscription::SocialSuccessfulPurchaseService.new(@purchase_request)

    @mail = double("mail", deliver: true)
    allow(SocialNotifier).to receive(:upgrade_confirmation).and_return(@mail)

    allow_any_instance_of(Cart::Payment::Manager).to receive(:process!).and_return(true)
  end

  describe "#handle" do
    it "updates person analytics" do
      expect_any_instance_of(Invoice).to receive(:update_person_analytics)
      @success.handle
    end

    it "sends a confirmation email" do
      notifier_params = {
        plan_expires_at: @purchase_request.subscription_purchase.reload.termination_date.strftime("%Y-%m-%d %H:%M:%S"),
        renewal_period:  @purchase_request.product_type,
        payment_amount:  @purchase_request.manager.invoice.final_settlement_amount_cents
      }

      expect(SocialNotifier).to receive(:upgrade_confirmation).with(@purchase_person.id, notifier_params, @purchase_person.domain)

      @success.handle
    end

  end
end
