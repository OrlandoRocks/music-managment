require "rails_helper"

describe Subscription::Android::HandleAccountStatus do
  let (:subscription_id) {"com.tunecore.social.pro.monthly"}
  let (:purchase_token) {"abcdefghjijljljl"}
  let (:expires_date) {(DateTime.now + 30.days).beginning_of_day}
  let(:person) { FactoryBot.create(:person, :with_tc_social) }

  context "Notification type RECOVERED or RESTARTED" do
    it "updates subscription_status" do
    
      params = {
        "notificationType" => 1,
        "subscriptionId" => subscription_id,
        "purchaseToken" => purchase_token
      }
      purchase_response = {
        purchase_date: Date.today,
        expires_date:  expires_date,
        purchase_token: params[:purchase_token],
        product_id: params[:product_id]
      }
      subscription_product = SubscriptionProduct.active_by_product_type_for(person, "Social", "monthly")

      subscription = SubscriptionPurchase.create!({
        subscription_product_id: subscription_product.id,
        payment_channel: "Android",
        person: person,
        effective_date: Date.today.prev_month ,
        termination_date: Date.today.to_s
      })

      PaymentChannelReceipt.create!({
        person_id: person.id,
        subscription_purchase_id: subscription.id,
        receipt_data: "#{purchase_token} #{subscription_id}"
      })

      subscription_status = PersonSubscriptionStatus.create!({
        person: person,
        effective_date: Date.today.prev_month,
        termination_date: Date.today.to_s,
        subscription_type: subscription_product.product_name,
        grace_period_length: 0
      })
      allow_any_instance_of(Subscription::Android::PurchaseVerifier).to receive(:purchase_verifier).and_return(purchase_response)
      Subscription::Android::HandleAccountStatus.new(params).process
      subscription_status.reload
      expect(subscription_status.termination_date.to_datetime).to eq expires_date
    end  
  end
end
