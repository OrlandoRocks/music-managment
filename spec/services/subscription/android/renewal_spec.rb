require 'rails_helper'

describe 'renewal' do
  let(:person)        { FactoryBot.create(:person, :with_tc_social) }
  let(:purchase_date) {Date.today.to_s}
  let(:expires_date_one_year)  {1.year.from_now.to_date.to_s}
  let(:expires_date_one_month)  {1.month.from_now.to_date.to_s}
  let(:purchase_token) {"njjpnooplbfcnhddjngmcfhe.AO-J1OwzPBAcnF8dG9efGa1lJ3UMpGpFZ05EWlfPlrfkzJFf1C1kDcuDu84CLoTu47C4CdgQy34tJ46JKC5fO-gVXkAEPp2YTU1YUTz1EiCvAM6LdstpH4YtQN6AzYymrezxnn4-3DuTHhx_D9KYNd4TcGo9J8UQkw"}
  let(:product_id) { "com.tunecore.social.pro.monthly" }
  let(:receipt_data) {"njjpnooplbfcnhddjngmcfhe.AO-J1OwzPBAcnF8dG9efGa1lJ3UMpGpFZ05EWlfPlrfkzJFf1C1kDcuDu84CLoTu47C4CdgQy34tJ46JKC5fO-gVXkAEPp2YTU1YUTz1EiCvAM6LdstpH4YtQN6AzYymrezxnn4-3DuTHhx_D9KYNd4TcGo9J8UQkw com.tunecore.social.pro.monthly"}
  let(:product_type) {"monthly"}
  let(:purchase_details_renewed) {
    {
      purchase_date: purchase_date,
      expires_date: expires_date_one_month,
      purchase_token: purchase_token,
      product_id: product_id
    }
  }
  let(:initial_purchase_date) { Date.today.prev_month }
  let(:initial_expire_date) { Date.today.to_s }
  let(:subscription_product) {
    SubscriptionProduct.active_by_product_type_for(person, "Social", "monthly")
  }
  let(:purchase_details_not_renewed){
    {
      purchase_date: initial_purchase_date,
      expires_date: initial_expire_date,
      purchase_token: purchase_token,
      product_id: product_id
    }
  }

  before do
    @subscription = SubscriptionPurchase.create!({
        subscription_product_id: subscription_product.id,
        payment_channel: "Android",
        person: person,
        effective_date: initial_purchase_date,
        termination_date: initial_expire_date
      })
    person_subscription_status = PersonSubscriptionStatus.create!({
      person: person,
      effective_date: initial_purchase_date,
      termination_date: initial_expire_date,
      subscription_type: subscription_product.product_name,
      grace_period_length: 0
    })
    subscription_event = SubscriptionEvent.create!({
      subscription_purchase: @subscription,
      person_subscription_status: person_subscription_status,
      subscription_type: subscription_product.product_name,
      event_type: "Purchase"
    })
    payment_channel_receipt = PaymentChannelReceipt.create!({
      person_id: person.id,
      subscription_purchase_id: @subscription.id,
      receipt_data: receipt_data
    })
    @subscription = Subscription::Renewal::QueryBuilder.build("today_and_grace_period", "social", "Android").subscriptions.first
  end

  context 'success' do

    it 'fetch purchase token and product' do
      receipt = @subscription.subscription_events.last.subscription_purchase.payment_channel_receipt
      expect(receipt.receipt_data.split(' ')[0]).to eq(purchase_token)
      expect(receipt.receipt_data.split(' ')[1]).to eq(product_id)
    end

    it 'gets renewal data from android' do
      params = Subscription::Android::ParamsBuilder.build(purchase_details_renewed, person.id)
      expect(params).to eq({
          :purchase_date=>purchase_date,
          :expires_date=>expires_date_one_month ,
          :product_id=>"com.tunecore.social.pro.monthly",
          :purchase_token=>purchase_token,
          :person_id=>person.id,
          :product_type=> "monthly",
          :product_name=> "Social"
        })
    end

    it 'sets subscription product' do
      subscription_product = SubscriptionProduct.active_by_product_type_for(person, "Social", "monthly")
      expect(subscription_product.product_name).to eq("Social")
    end

    it 'generates renewal' do
      @generator = Subscription::Android::PurchaseGenerator.generate(
        subscription_product, person, purchase_date, expires_date_one_month, purchase_token, product_id
      )
      expect(@generator.subscription_purchase.termination_date > Time.now)
    end
  end

  context 'failure' do

    it 'should not renew if purchase is not renewed' do
      params = Subscription::Android::ParamsBuilder.build(purchase_details_not_renewed, person.id)
      subscription_product = SubscriptionProduct.active_by_product_type_for(person, "Social", "monthly")
      @generator = Subscription::Android::PurchaseGenerator.generate(
        subscription_product, person, params[:purchase_date], params[:expires_date], params[:purchase_token], params[:product_id]
      )
      expect(@generator.subscription_purchase.termination_date <= Time.now)
    end
  end
end
