require "rails_helper"
describe Subscription::Android::ParamsBuilder do
  let(:person)        { FactoryBot.create(:person, :with_tc_social) }
  let(:purchase_date) {Date.today.to_s}
  let(:expires_date_one_year)  {1.year.from_now.to_date.to_s}
  let(:expires_date_one_month)  {1.month.from_now.to_date.to_s}

  let(:purchase_details_yearly) {
    {
      :purchase_date=>purchase_date,
      :expires_date=>expires_date_one_year,
    }
  }

  let(:purchase_details_monthly) {
    {
      :purchase_date=>purchase_date,
      :expires_date=>expires_date_one_month,
    }
  }
  context "with playstore result expires date one month from now" do
    describe ".build" do
      it "builds the correct params" do
        params = Subscription::Android::ParamsBuilder.build(purchase_details_monthly, person.id)
        expect(params).to eq({
        :purchase_date=>purchase_date,
        :expires_date=>expires_date_one_month,
        :product_name=> "Social",
        :product_type=> "monthly",
        :person_id=>person.id
      })
      end
    end
  end

  context "with playstore result expires date greater than one month from now" do
    describe ".build" do
      it "builds the correct params" do
        params = Subscription::Android::ParamsBuilder.build(purchase_details_yearly, person.id)
        expect(params).to eq({
        :purchase_date=>purchase_date,
        :expires_date=>expires_date_one_year,
        :product_name=> "Social",
        :product_type=> "annually",
        :person_id=>person.id
      })
      end
    end
  end
end
