require "rails_helper"

describe Subscription::PlansFetcher do
  let(:person) { create(:person, :with_tc_social, :with_live_album) }

  describe "#fetch_plan" do
    it "sets the #plans attribute to the collection of social plans" do
      plans_fetcher = Subscription::PlansFetcher.new(person)
      plans_fetcher.fetch_plans
      expect(plans_fetcher.plans.length).to eq(2)
      expect(plans_fetcher.plans.first).to be_a(SubscriptionProduct)
      expect(plans_fetcher.plans.second).to be_a(SubscriptionProduct)
    end

    context "when a targeted offer exists for user and plan" do
      before(:each) do
        product  = Product.where(display_name: "tc_social_monthly", country_website_id: person.country_website_id).first
        t_offer   = create(:targeted_offer)
        create(:targeted_product, targeted_offer: t_offer, product: product)
        create(:targeted_person, person: person, targeted_offer: t_offer)
        t_offer.update_attribute(:status, "Active")
      end

      it "adds discount offer details to the subscription_product that has a targeted offer" do
        plans_fetcher = Subscription::PlansFetcher.new(person)
        plans_fetcher.fetch_plans
        expect(plans_fetcher.plans.first.discount).to_not be_nil
        expect(plans_fetcher.plans.second.discount).to be_nil
        expect(plans_fetcher.plans.first.discount.keys).to match_array(["full_price", "discount_price", "show_discount_price", "flag_text", "price_adj_description"])
      end
    end
  end
end
