require "rails_helper"

describe TrackMonetization::RetryService do
  before :each do
    @person     = create(:person, :admin)
    @track_mon  = create(:track_monetization, :eligible, :delivered)
    @params     = {
      current_user_id:  @person.id,
      track_mon_id:     @track_mon.id,
      delivery_type:    "metadata_only"
    }
    allow_any_instance_of(Delivery::Retry).to receive(:send_sns_notification)
  end

  it "handles Track Monetization full_delivery retries" do
    @track_mon.update(delivery_type: "metadata_only")
    @params[:delivery_type] = "full_delivery"

    TrackMonetization::RetryService.retry(@params)

    expect(@track_mon.reload.delivery_type).to eq("full_delivery")
    expect(@track_mon.state).to eq("enqueued")
  end

  it "handles Track Monetization metadata_only retries" do
    TrackMonetization::RetryService.retry(@params)

    expect(@track_mon.reload.delivery_type).to eq("metadata_only")
    expect(@track_mon.state).to eq("enqueued")
  end

  it "does not retry if the Track Monetization is ineligible" do
    @track_mon.update(eligibility_status: "ineligible")
    TrackMonetization::RetryService.retry(@params)

    expect(@track_mon.delivery_type).to eq("full_delivery")
    expect(@track_mon.state).to eq("delivered")
  end

  it "enqueues a job via the Delivery::DistributionWorker when eligible" do
    expect{
      TrackMonetization::RetryService.retry(@params)
    }.to change(Delivery::DistributionWorker.jobs, :size).by 1
  end

  it "increments the retry_count by 1" do
    expect{
      TrackMonetization::RetryService.retry(@params)
    }.to change{ @track_mon.reload.retry_count }.by 1
  end

  context "when the track_monetization has a retry_count of nil" do
    it "sets the retry_count to 1" do
      person = create(:person, :admin)
      track_monetization = create(:track_monetization, :eligible, :delivered, retry_count: nil)
      params = {
        current_user_id: person.id,
        track_mon_id: track_monetization.id,
        delivery_type: "metadata_only"
      }

      TrackMonetization::RetryService.retry(params)

      expect(track_monetization.reload.retry_count).to eq(1)
    end
  end
end
