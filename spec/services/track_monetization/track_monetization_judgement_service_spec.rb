require "rails_helper"

describe TrackMonetization::TrackMonetizationJudgementService do
  describe "::judge" do
    let!(:current_user) { create(:person) }

    context "when there are eligible_track_monetizations present" do
      context "when those track_monetizations are blocked" do
        context "when those track_monetizations have associated track_monetization_blockers" do
          it "destroys the track_monetization_blockers" do
            track_monetization = create(:track_monetization, :blocked, :with_monetization_blocked_song)
            
            params = { eligible_track_monetizations: [track_monetization], current_user: current_user}

            expect(track_monetization.reload.manual_approval).to be_nil
 
            TrackMonetizationJudgementService.judge(params)

            expect(track_monetization.reload.manual_approval).not_to be_nil
            expect(track_monetization.reload.track_monetization_blocker).to eq(nil)
          end

          it "sets their states to new" do
            track_monetization = create(:track_monetization, :blocked, :with_monetization_blocked_song)

            params = { eligible_track_monetizations: [track_monetization], current_user: current_user}

            expect(track_monetization.reload.manual_approval).to be_nil

            TrackMonetizationJudgementService.judge(params)

            expect(track_monetization.reload.manual_approval).not_to be_nil
            expect(track_monetization.reload.state).to eq("new")
          end

          it "sets their eligibility_status to eligible" do
            track_monetization = create(:track_monetization, :blocked, :with_monetization_blocked_song)

            params = { eligible_track_monetizations: [track_monetization], current_user: current_user}

            expect(track_monetization.reload.manual_approval).to be_nil

            TrackMonetizationJudgementService.judge(params)


            expect(track_monetization.reload.manual_approval).not_to be_nil
            expect(track_monetization.reload.eligibility_status).to eq("eligible")
          end
        end

        context "when those track_monetizations don't have associated track_monetization_blockers" do
          it "sets their states to new" do
            track_monetization = create(:track_monetization, :blocked)

            params = { eligible_track_monetizations: [track_monetization], current_user: current_user }

            TrackMonetizationJudgementService.judge(params)

            expect(track_monetization.reload.state).to eq("new")
          end

          it "sets their eligibility_status to eligible" do
            track_monetization = create(:track_monetization, :blocked)

            params = { eligible_track_monetizations: [track_monetization], current_user: current_user }
            TrackMonetizationJudgementService.judge(params)

            expect(track_monetization.reload.eligibility_status).to eq("eligible")
          end
        end
      end

      context "when those track_monetizations are not blocked" do
        context "when those track_monetizations do not have an eligible eligibility_status" do
          it "sets their eligibility_status to eligible" do
            track_monetization = create(:track_monetization, :ineligible)

            params = { eligible_track_monetizations: [track_monetization], current_user: current_user }

            TrackMonetizationJudgementService.judge(params)

            expect(track_monetization.reload.eligibility_status).to eq("eligible")
          end
        end
      end
    end

    context "when there are ineligible_track_monetizations present" do
      context "when those track_monetizations don't have associated track_monetization_blockers" do
        it "creates track_monetization_blockers" do
          track_monetization = create(:track_monetization, :eligible, state: "new")
          manual_approval = create(:manual_approval, approveable_type: track_monetization.class, approveable_id: track_monetization.id,  created_by_id: current_user.id)

          expect(track_monetization.reload.manual_approval).not_to be_nil

          params = { ineligible_track_monetizations: [track_monetization] }

          TrackMonetizationJudgementService.judge(params)

          expect(track_monetization.reload.manual_approval).to be_nil
          expect(track_monetization.reload.track_monetization_blocker).to be_an_instance_of(TrackMonetizationBlocker)
        end

        it "sets their states to blocked" do
          track_monetization = create(:track_monetization, :eligible, state: "new")
          manual_approval = create(:manual_approval, approveable_type: track_monetization.class, approveable_id: track_monetization.id,  created_by_id: current_user.id)

          expect(track_monetization.reload.manual_approval).not_to be_nil

          params = { ineligible_track_monetizations: [track_monetization] }

          TrackMonetizationJudgementService.judge(params)

          expect(track_monetization.reload.manual_approval).to be_nil
          expect(track_monetization.reload.state).to eq("blocked")
        end

        it "sets their eligibility_status to ineligible" do
          track_monetization = create(:track_monetization, :eligible, state: "new")
          manual_approval = create(:manual_approval, approveable_type: track_monetization.class, approveable_id: track_monetization.id,  created_by_id: current_user.id)

          expect(track_monetization.reload.manual_approval).not_to be_nil

          params = { ineligible_track_monetizations: [track_monetization] }

          TrackMonetizationJudgementService.judge(params)

          expect(track_monetization.reload.manual_approval).to be_nil
          expect(track_monetization.reload.eligibility_status).to eq("ineligible")
        end
      end

      context "when those track_monetizations have associated track_monetization_blockers" do
        it "sets their states to blocked" do
          track_monetization = create(:track_monetization, :with_monetization_blocked_song, :eligible, state: "new")
          manual_approval = create(:manual_approval, approveable_type: track_monetization.class, approveable_id: track_monetization.id,  created_by_id: current_user.id)

          expect(track_monetization.reload.manual_approval).not_to be_nil

          params = { ineligible_track_monetizations: [track_monetization] }

          TrackMonetizationJudgementService.judge(params)

          expect(track_monetization.reload.manual_approval).to be_nil
          expect(track_monetization.reload.state).to eq("blocked")
        end

        it "sets their eligibility_status to ineligible" do
          track_monetization = create(:track_monetization, :with_monetization_blocked_song, :eligible, state: "new")
          manual_approval = create(:manual_approval, approveable_type: track_monetization.class, approveable_id: track_monetization.id,  created_by_id: current_user.id)

          expect(track_monetization.reload.manual_approval).not_to be_nil

          params = { ineligible_track_monetizations: [track_monetization] }

          TrackMonetizationJudgementService.judge(params)

          expect(track_monetization.reload.manual_approval).to be_nil
          expect(track_monetization.reload.eligibility_status).to eq("ineligible")
        end
      end
    end

    context "when there are pending_track_monetizations present" do
      context "when those track_monetizations are blocked" do
        context "when those track_monetizations have associated track_monetization_blockers" do
          it "destroys the track_monetization_blockers" do
            track_monetization = create(:track_monetization, :blocked, :with_monetization_blocked_song)
            manual_approval = create(:manual_approval, approveable_type: track_monetization.class, approveable_id: track_monetization.id,  created_by_id: current_user.id)

            expect(track_monetization.reload.manual_approval).not_to be_nil

            params = { pending_track_monetizations: [track_monetization] }

            TrackMonetizationJudgementService.judge(params)

            expect(track_monetization.reload.manual_approval).to be_nil
            expect(track_monetization.reload.track_monetization_blocker).to eq(nil)
          end

          it "sets their states to pending_approval" do
            track_monetization = create(:track_monetization, :blocked, :with_monetization_blocked_song)
            manual_approval = create(:manual_approval, approveable_type: track_monetization.class, approveable_id: track_monetization.id,  created_by_id: current_user.id)

            expect(track_monetization.reload.manual_approval).not_to be_nil

            params = { pending_track_monetizations: [track_monetization] }

            TrackMonetizationJudgementService.judge(params)

            expect(track_monetization.reload.manual_approval).to be_nil
            expect(track_monetization.reload.state).to eq("pending_approval")
          end

          it "sets their eligibility_status to pending" do
            track_monetization = create(:track_monetization, :blocked, :with_monetization_blocked_song)
            manual_approval = create(:manual_approval, approveable_type: track_monetization.class, approveable_id: track_monetization.id,  created_by_id: current_user.id)

            expect(track_monetization.reload.manual_approval).not_to be_nil

            params = { pending_track_monetizations: [track_monetization] }

            TrackMonetizationJudgementService.judge(params)

            expect(track_monetization.reload.manual_approval).to be_nil
            expect(track_monetization.reload.eligibility_status).to eq("pending")
          end
        end

        context "when those track_monetizations do not have associated track_monetization_blockers" do
          it "sets their states to pending_approval" do
            track_monetization = create(:track_monetization, :blocked)
            manual_approval = create(:manual_approval, approveable_type: track_monetization.class, approveable_id: track_monetization.id,  created_by_id: current_user.id)

            expect(track_monetization.reload.manual_approval).not_to be_nil

            params = { pending_track_monetizations: [track_monetization] }

            TrackMonetizationJudgementService.judge(params)

            expect(track_monetization.reload.manual_approval).to be_nil
            expect(track_monetization.reload.state).to eq("pending_approval")
          end

          it "sets their eligibility_status to pending" do
            track_monetization = create(:track_monetization, :blocked)
            manual_approval = create(:manual_approval, approveable_type: track_monetization.class, approveable_id: track_monetization.id,  created_by_id: current_user.id)

            expect(track_monetization.reload.manual_approval).not_to be_nil

            params = { pending_track_monetizations: [track_monetization] }

            TrackMonetizationJudgementService.judge(params)

            expect(track_monetization.reload.manual_approval).to be_nil
            expect(track_monetization.reload.eligibility_status).to eq("pending")
          end
        end
      end

      context "when those track_monetizations are not blocked" do
        it "sets their states to pending_approval" do
          track_monetization = create(:track_monetization, :eligible, state: "new")
          manual_approval = create(:manual_approval, approveable_type: track_monetization.class, approveable_id: track_monetization.id,  created_by_id: current_user.id)

          expect(track_monetization.reload.manual_approval).not_to be_nil

          params = { pending_track_monetizations: [track_monetization] }

          TrackMonetizationJudgementService.judge(params)

          expect(track_monetization.reload.manual_approval).to be_nil
          expect(track_monetization.reload.state).to eq("pending_approval")
        end

        it "sets their eligibility_status to pending" do
          track_monetization = create(:track_monetization, :eligible, state: "new")
          manual_approval = create(:manual_approval, approveable_type: track_monetization.class, approveable_id: track_monetization.id,  created_by_id: current_user.id)

          expect(track_monetization.reload.manual_approval).not_to be_nil

          params = { pending_track_monetizations: [track_monetization] }

          TrackMonetizationJudgementService.judge(params)

          expect(track_monetization.reload.manual_approval).to be_nil
          expect(track_monetization.reload.eligibility_status).to eq("pending")
        end
      end
    end
  end
end
