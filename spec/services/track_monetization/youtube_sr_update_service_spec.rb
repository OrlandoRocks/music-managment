require "rails_helper"

describe TrackMonetization::YoutubeSrUpdateService do
  describe '#deliver_metadata_update' do
    let!(:song) { create(:song)}
    let!(:album) { song.album }
    let!(:salepoint) { create(:salepoint, store: Store.find_by(short_name: "google"), salepointable: album)}
    subject(:update_metadata) { described_class.deliver_sr_metadata_update(album) }

    context "when album is take down and track monetization is for youtube" do
      before do
        album.takedown!
        create(:track_monetization, :eligible, :delivered, song: song,  store_id: Store::YTSR_STORE_ID)
      end

      it "updates the delivery type to metadata_update" do
        subject
        expect(song.track_monetizations.first.delivery_type).to eq("metadata_only")
      end

      it "enqueues a takedown metadata update in the Delivery::DistributionWorker" do
         expect{ subject }.to change(Delivery::DistributionWorker.jobs, :size).by 1
      end
    end

    context "when salepoint is take down and track monetization is for youtube" do
      before do
        salepoint.takedown!
        create(:track_monetization, :eligible, :delivered, song: song,  store_id: Store::YTSR_STORE_ID)
      end

      it "it updates the delivery type to metadata_update" do
        subject
        expect(song.track_monetizations.first.delivery_type).to eq("metadata_only")
      end

      it "enqueues a takedown metadata update in the Delivery::DistributionWorker" do
         expect{ subject }.to change(Delivery::DistributionWorker.jobs, :size).by 1
      end
    end


    context "when album is not a take down and track monetization is for youtube" do
      before do
        create(:track_monetization, :eligible, :delivered, song: song,  store_id: Store::YTSR_STORE_ID)
      end

      it "it updates the delivery type to metadata_update" do
        subject
        expect(song.track_monetizations.first.delivery_type).to eq("metadata_only")
      end

      it "enqueues a metadata_update" do
         expect{ subject }.to change(Delivery::DistributionWorker.jobs, :size).by 1
      end
    end

    context "when album is take down and no track monetizations are for youtube" do
      before do
        album.takedown!
        create(:track_monetization, :eligible, :delivered, song: song,  store_id: Store::FBTRACKS_STORE_ID)
      end

      it "it does not change the delivery type to metadata_update" do
        subject
        expect(song.track_monetizations.first.delivery_type).to eq("full_delivery")
      end


      it "does not enqueue a delivery in the Delivery::DistributionWorker" do
        expect{ subject }.to change(Delivery::DistributionWorker.jobs, :size).by 0
      end
    end

    context "when there are no track monetizations" do

      it "does not enqueue a deliver in the Delivery::DistributionWorker" do
        expect{ subject }.to change(Delivery::DistributionWorker.jobs, :size).by 0
      end
    end

    context "when album track_monetization is not eligible" do
      before do
        create(:track_monetization, :delivered, song: song, store_id: Store::YTSR_STORE_ID)
      end

      it "it does not change the delivery type to metadata_update" do
        subject
        expect(song.track_monetizations.first.delivery_type).to eq("full_delivery")
      end

      it "does not enqueue a delivery in the Delivery::DistributionWorker" do
        expect{ subject }.to change(Delivery::DistributionWorker.jobs, :size).by 0
      end
    end

    context "when track_monetization is not delivered" do

      before do
        create(:track_monetization, :eligible, song: song, store_id: Store::YTSR_STORE_ID)
      end

      it "it does not change the delivery type to metadata_update" do
        subject
        expect(song.track_monetizations.first.delivery_type).to eq("full_delivery")
      end

      it "does not enqueue a delivery in the Delivery::DistributionWorker" do
        expect{ subject }.to change(Delivery::DistributionWorker.jobs, :size).by 0
      end
    end
  end

  describe '#send_track_album' do
    let!(:song) { create(:song)}
    let!(:album) { song.album }
    let!(:petri_bundle) { create(:petri_bundle, album: album)}
    before { allow_any_instance_of(DistributionCreator).to receive(:ready_to_distribute?).and_return(true) }

    subject(:send_track_album) { described_class.send_track_album(album) }
    context "when the track's album has not been distributed to Google Play/Youtube Music" do
      it "creates a Google Distribution" do
       subject
       expect(album.distributions.last.converter_class).to eq("MusicStores::Google::Converter")
      end

       it "creates a Google salepoint" do
        subject
        expect(album.salepoints.last.store_id).to eq(Store::GOOGLE_STORE_ID)
      end

      it "enqueues a Distribution" do
        expect_any_instance_of(Distribution).to receive(:start).once
        subject
      end
    end

    context "when the track's album has been distributed to Google Play/Youtube Music" do
      let(:salepoint) { create(:salepoint, store_id: Store::GOOGLE_STORE_ID, salepointable: album, finalized_at: Time.now) }
      let(:distribution) { create(:distribution, petri_bundle: album.petri_bundle) }

      before do
        distribution.salepoints << salepoint
        distribution.update(state: "delivered")
      end

      context "when the album has been delivered more than 24 hours earlier" do

        before { distribution.update(updated_at: 3.days.ago) }

        it "starts a metadata update for the album distribution" do
          expect_any_instance_of(TrackMonetization::YoutubeSrUpdateService).to receive(:deliver_metadata_update)
          subject
        end

        it "enques a metadata update" do
          expect{ subject }.to change(Delivery::DistributionWorker.jobs, :size).by 1
        end
      end

      context "when the album has been delivered less than 24 hours earlier" do

        it "logs a message notifying no update was sent" do
          expect(Rails.logger).to receive(:info).with("No Art Track Update was triggered for album id #{album.id}")
          subject
        end

        it "does not enqueue a metadata update" do
          expect{ subject }.to change(Delivery::DistributionWorker.jobs, :size).by 0
        end
      end
    end
  end
end
