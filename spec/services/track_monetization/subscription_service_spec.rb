require "rails_helper"

describe TrackMonetization::SubscriptionService do
  describe ".FBTracks" do
    let(:person)      { create(:person) }
    let!(:live_album)  { create(
      :album, :paid, :with_uploaded_songs, :with_salepoints, :with_petri_bundle,
      number_of_songs: 2,
      person: person,
      legal_review_state: "APPROVED"
    ) }

    let(:unfinalized)  { create(:album, :with_uploaded_songs, number_of_songs: 2, person: person) }

    it "creates Track Monetizations for each finalized, distributed song on all the user's albums" do
      params = {
        person_id: person.id,
        service: "FBTracks"
      }.with_indifferent_access

      expect{
        TrackMonetization::SubscriptionService.subscribe(params)
      }.to change(TrackMonetization, :count).by(2)
    end

    it "does not create a TrackMonetization object for songs on unfinalized albums" do
      track_mons = TrackMonetization.where(person_id: person.id).pluck(:song_id)

      expect(track_mons).not_to include(unfinalized.songs.pluck(:id))
    end

    it "does not create a TrackMonetization object for songs on taken down albums" do
      takedown      = create(:album, :with_uploaded_songs, :taken_down)
      takedown_song = takedown.songs.first
      person.albums << takedown
      params = {
        person_id: person.id,
        service: "FBTracks"
      }.with_indifferent_access

      TrackMonetization::SubscriptionService.subscribe(params)

      expect(person.track_monetizations.pluck(:song_id)).not_to include(takedown_song.id)
    end

    it "checks the eligibility of each track_monetization" do
      potential_ids = person.potentially_monetizable_songs.map(&:id)
      params = {
        person_id: person.id,
        service: "FBTracks"
      }.with_indifferent_access
      TrackMonetization::SubscriptionService.subscribe(params)
      person_track_mons = TrackMonetization.where(song_id: [potential_ids])

      expect(person_track_mons.none?{|tm| tm.eligibility_status == "pending"}).to be true
    end

    it "does not enqueue any tracks" do
      params = {
        person_id: person.id,
        service: "FBTracks"
      }.with_indifferent_access
      expect{
        TrackMonetization::SubscriptionService.subscribe(params)
      }.not_to change(Delivery::DistributionWorker.jobs, :count)

      expect(TrackMonetization.all.all?{|tm| tm.state == "new"}).to be true
    end

    context "send all" do
      it "checks the eligibility of each track immediately" do
        params = {
          send_all: true,
          person_id: person.id,
          service: "FBTracks"
        }.with_indifferent_access
        person_track_mons = TrackMonetization.where(person_id: person.id)

        expect(person_track_mons.any?{|tm| tm.eligibility_status == "pending"}).to be false
      end

      it "enqueues all eligible tracks" do
        params = {
          send_all: true,
          person_id: person.id,
          service: "FBTracks"
        }.with_indifferent_access
        TrackMonetization::SubscriptionService.subscribe(params)
        tm_states = TrackMonetization.where(person_id: person.id).pluck(:state)

        expect(tm_states.any?{|state| state == "new"}).to be false
      end

      it "also enqueues album deliveries for Instagram if Track Mon service is 'FBTracks'" do
        ig = Store.find_by(short_name: "Instagram")
        params = {
          send_all: true,
          person_id: person.id,
          service: "FBTracks"
        }.with_indifferent_access

        expect {
          TrackMonetization::SubscriptionService.subscribe(params)
        }.to change(live_album.salepoints, :count).by(1)
         .and change(live_album.distributions, :count).by(1)

        live_album.reload
        expect(live_album.salepoints.any?{ |sp| sp.store_id == ig.id }).to be true
        expect(live_album.distributions.any?{ |d| d.store == ig }).to be true
      end
    end
  end

  describe ".YTTracks" do
    let(:person)      { create(:person) }

    context "blocked track_monetizations" do
      it 'should create track_monetizations for albums with blocked Genre' do
        params = {
          person_id: person.id,
          service: "YTTracks"
        }.with_indifferent_access
        create(
          :album, :paid, :with_uploaded_songs, :with_salepoints, :with_petri_bundle,
          number_of_songs: 2,
          person: person,
          legal_review_state: "APPROVED",
          primary_genre: Genre.find_by(name: "Audiobooks")
        )
        create(
          :album, :paid, :with_uploaded_songs, :with_salepoints, :with_petri_bundle,
          number_of_songs: 2,
          person: person,
          legal_review_state: "APPROVED",
        )

        expect{
          TrackMonetization::SubscriptionService.subscribe(params)
        }.to change(TrackMonetization, :count).by(4)
        track_mons = person.youtube_track_monetizations
        expect(track_mons.map(&:state)).to include 'blocked', 'new'
        expect(track_mons.map(&:eligibility_status)).to include 'eligible', 'ineligible'
        expect{
          TrackMonetization::SubscriptionService.subscribe(params)
        }.to change(TrackMonetization, :count).by(0)
      end

      it 'should create track_monetizations for albums with unblocked genre' do
        params = {
          person_id: person.id,
          service: "YTTracks"
        }.with_indifferent_access
        create(
          :album, :paid, :with_uploaded_songs, :with_salepoints, :with_petri_bundle,
          number_of_songs: 2,
          person: person,
          legal_review_state: "APPROVED",
        )

        expect{
          TrackMonetization::SubscriptionService.subscribe(params)
        }.to change(TrackMonetization, :count).by(2)

        track_mons = person.youtube_track_monetizations
        expect(track_mons.map(&:state)).to include 'new'
        expect(track_mons.map(&:eligibility_status)).to include 'eligible'
      end
    end
  end
end
