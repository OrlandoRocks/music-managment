require "rails_helper"

describe TrackMonetization::EligibilityEngineService do
  describe "#validate!" do
    let!(:person) { create(:person) }
    let(:store) { Store.find_by(short_name: "FBTracks") }

    let!(:ineligibility_rule_1) do
      create(:ineligibility_rule,
        property: "album_name",
        operator: "contains",
        value: "Classical Music",
        store: store
      )
    end

    let!(:ineligibility_rule_2) do
      create(:ineligibility_rule,
        property: "song_name",
        operator: "contains",
        value: "Beethoven",
        store: store
      )
    end

    let(:album) { create(:album, name: "Rock of the Century", person: person) }
    let(:song) { create(:song, name: "I Wanna Rock and Roll", album: album) }

    let(:track_monetization) do
      build(:track_monetization,
        song: song,
        person: person,
        store: store
      )
    end

    context "when a track monetization matches at least one ineligibility rule" do
      before(:each) do
        album.update(name: "Classical Music of the 1800's in B Minor")
      end

      context "when that track monetization matches multiple ineligibility rules" do
        before(:each) do
          song.update(name: "Beethoven's 5th Symphony")
        end

        it "adds all those ineligibility rules to that track monetization" do
          eligibility_engine = TrackMonetization::EligibilityEngineService.new(track_monetization)
          eligibility_engine.validate!

          track_monetization.reload

          expect(track_monetization).not_to be_eligible
          expect(track_monetization.ineligibility_rules).to include(ineligibility_rule_1, ineligibility_rule_2)
        end

        it "sets that track_monetization's state to blocked" do
          eligibility_engine = TrackMonetization::EligibilityEngineService.new(track_monetization)
          eligibility_engine.validate!

          track_monetization.reload

          expect(track_monetization.state).to eq("blocked")
        end

        it "sets that track_monetization's eligibility_status to ineligible" do
          eligibility_engine = TrackMonetization::EligibilityEngineService.new(track_monetization)
          eligibility_engine.validate!

          track_monetization.reload

          expect(track_monetization.eligibility_status).to eq("ineligible")
        end
      end

      it "adds at least one ineligibility rule to that track monetization" do
        eligibility_engine = TrackMonetization::EligibilityEngineService.new(track_monetization)
        eligibility_engine.validate!

        track_monetization.reload

        expect(track_monetization).not_to be_eligible
        expect(track_monetization.ineligibility_rules).not_to be_empty
      end

      it "sets that track_monetization's state to blocked" do
        eligibility_engine = TrackMonetization::EligibilityEngineService.new(track_monetization)
        eligibility_engine.validate!

        track_monetization.reload

        expect(track_monetization.state).to eq("blocked")
      end

      it "sets that track_monetization's eligibility_status to ineligible" do
        eligibility_engine = TrackMonetization::EligibilityEngineService.new(track_monetization)
        eligibility_engine.validate!

        track_monetization.reload

        expect(track_monetization.eligibility_status).to eq("ineligible")
      end

      it "creates a note" do
        eligibility_engine = TrackMonetization::EligibilityEngineService.new(track_monetization)

        expect { eligibility_engine.validate!
        }.to change {
          song.album.notes.count
        }.by(1)

        note = song.reload.album.notes.last
        store_name = track_monetization.store.name
        note_text = "Song isrc: #{song.isrc} for store, #{store_name}, was blocked " \
          "due to #{ineligibility_rule_1.property} #{ineligibility_rule_1.operator} " \
          "#{ineligibility_rule_1.value}"

        expect(note.subject).to eq "Automatically Blocked Track Monetization"
        expect(note.note).to eq note_text
      end
    end

    context "when a track monetization does not match any ineligibility rules" do
      it "does not add ineligibility rules to that track monetization" do
        eligibility_engine = TrackMonetization::EligibilityEngineService.new(track_monetization)
        eligibility_engine.validate!

        track_monetization.reload

        expect(track_monetization).to be_eligible
        expect(track_monetization.ineligibility_rules).to be_empty
      end

      it "does not set that track_monetization's state to blocked" do
        eligibility_engine = TrackMonetization::EligibilityEngineService.new(track_monetization)
        eligibility_engine.validate!

        track_monetization.reload

        expect(track_monetization.state).to_not eq("blocked")
      end

      it "does not set that track_monetization's eligibility_status to ineligible" do
        eligibility_engine = TrackMonetization::EligibilityEngineService.new(track_monetization)
        eligibility_engine.validate!

        track_monetization.reload

        expect(track_monetization.eligibility_status).to_not eq("ineligible")
      end

      it "does not create a note" do
        eligibility_engine = TrackMonetization::EligibilityEngineService.new(track_monetization)

        expect { eligibility_engine.validate! }.to change { song.album.notes.count }.by(0)
      end
    end
  end
end
