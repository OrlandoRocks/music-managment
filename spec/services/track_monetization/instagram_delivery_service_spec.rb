require "rails_helper"

describe TrackMonetization::InstagramDeliveryService do
  before :each do
      @person     = create(:person)
      @album = create(:album, :paid, :with_uploaded_songs, :with_salepoints, :with_petri_bundle,
        number_of_songs: 2,
        person: @person,
        legal_review_state: "APPROVED")
      @other_album = create(:album, :paid, :with_uploaded_songs, :with_salepoints, :with_petri_bundle,
        number_of_songs: 2,
        person: @person,
        legal_review_state: "APPROVED")
      @params = { person_name: @person.name, albums: [@album, @other_album] }
      @ig = Store.find_by(short_name: "Instagram")
  end

  it "creates an Instagram salepoint for the album if it doesn't yet have one" do
    expect(@album.salepoints.pluck(:store_id)).not_to include(@ig.id)

    TrackMonetization::InstagramDeliveryService.deliver(@params)

    expect(@album.salepoints.pluck(:store_id)).to include(@ig.id)
  end

  it "enqueues album deliveries for each uniq Facebook Monetization album" do
    expect {
      TrackMonetization::InstagramDeliveryService.deliver(@params)
    }.to change(@album.distributions, :count).by(1)
      .and change(@other_album.distributions, :count).by(1)

    @album.reload
    @other_album.reload

    expect(@album.distributions.any?{ |d| d.store == @ig }).to be true
    expect(@other_album.distributions.any?{ |d| d.store == @ig }).to be true
  end
end
