require "rails_helper"

describe TrackMonetization::TakedownService do
  before :each do
    @person    = create(:person)
    @track_mon = create(:track_monetization, :eligible, :delivered)
    @params    = {current_user: @person, track_mons: [@track_mon] }.with_indifferent_access
  end

  context "takedown" do
    it "sets the takedown_at field and changes delivery_type to 'metadata_only'" do
      expect(@track_mon).to receive(:start).and_return(true)

      TrackMonetization::TakedownService.takedown(@params)

      expect(@track_mon.takedown_at.present?).to be(true)
      expect(@track_mon.delivery_type).to eq("metadata_only")
    end

    it "enqueues a job via the Delivery::DistributionWorker" do
      expect{
        TrackMonetization::TakedownService.takedown(@params)
      }.to change(Delivery::DistributionWorker.jobs, :size).by 1
    end

    it "creates a takedown note for the appropriate store" do
      TrackMonetization::TakedownService.takedown(@params)

      note_for_store = TrackMonetization::TakedownService::NOTE_BY_STORE[:takedown][@track_mon.store.short_name]
      note_message =  note_for_store + ": Song Isrc - #{@track_mon.song.isrc}"

      note = @track_mon.song.album.notes.last
      expect(note.note).to eq note_message
      expect(note.subject).to eq note_for_store
      expect(note.note_created_by).to eq @person
    end
  end

  context "remove_takedown" do
    it "nullifies the takedown_at field and changes delivery_type to 'metadata_only'" do
      expect(@track_mon).to receive(:start).and_return(true)

      TrackMonetization::TakedownService.remove_takedown(@params)

      expect(@track_mon.takedown_at.nil?).to be(true)
      expect(@track_mon.delivery_type).to eq("full_delivery")
    end

    it "enqueues a job via the Delivery::DistributionWorker" do
      expect{
        TrackMonetization::TakedownService.remove_takedown(@params)
      }.to change(Delivery::DistributionWorker.jobs, :size).by 1
    end

    it "creates a remove takedown note for the appropriate store" do
      TrackMonetization::TakedownService.remove_takedown(@params)

      note_for_store = TrackMonetization::TakedownService::NOTE_BY_STORE[:remove_takedown][@track_mon.store.short_name]
      note_message =  note_for_store + ": Song Isrc - #{@track_mon.song.isrc}"

      note = @track_mon.song.album.notes.last
      expect(note.note).to eq note_message
      expect(note.subject).to eq note_for_store
      expect(note.note_created_by).to eq @person
    end
  end
end
