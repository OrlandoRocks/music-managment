require "rails_helper"

describe TrackMonetization::StateUpdater do
  let!(:admin) { create(:person, :admin) }
  let!(:album) { create(:album, :purchaseable, :paid, :with_salepoint_song, :approved, :taken_down) }
  let!(:song1) { album.songs.first }
  let!(:song2) { album.songs.last }

  let!(:store) { create(:store, :tc_distributor) }
  let!(:store_delivery_config) { create(:store_delivery_config, store_id: store.id, distributable_class: "TrackMonetization") }

  let!(:datetime_current){ (DateTime.current).localtime }

  let!(:track_monetization1) {
    create(:track_monetization, :eligible,
                                :delivered_via_tc_distributor,
                                store_id: store.id,
                                song_id: song1.id)
  }
  let!(:track_monetization2) {
    create(:track_monetization, :eligible,
                                :delivered_via_tc_distributor,
                                store_id: store.id,
                                song_id: song2.id)
  }

  let!(:track_monetizations){ TrackMonetization.where(id: [track_monetization1, track_monetization2]) }

  let!(:track_monetizations_by_album_store_delivery_types) {
    {
      { album_id: album.id, delivery_type: "full_delivery", store_id: store.id } => [track_monetization1, track_monetization2]
    }
  }

  let!(:releases_stores_json) {
    [
      {
        http_body: {
          total: 3,
          releases_stores: [{
            id: 1,
            song_id: song1.id,
            store_id: store.id,
            state: "delivered",
            created_at: datetime_current
          }, {
            id: 2,
            song_id: song2.id,
            store_id: store.id,
            state: "error",
            created_at: datetime_current-1.hour
          }]
        }
      }
    ].to_json
  }

  before(:each) do
    allow(DateTime).to receive(:current).and_return(datetime_current)
    expect(DistributorAPI::ReleasesStore).to receive(:list).with({ song_id: [song1.id, song2.id], greatest_song: true })
    .and_return(JSON.parse(releases_stores_json, object_class: OpenStruct)
    )
    
  end

  it "#releases_stores" do
    expect(described_class.new(track_monetizations: track_monetizations).send("releases_stores"))
      .to eql(JSON.parse(releases_stores_json, object_class: OpenStruct)[0].http_body&.dig("releases_stores"))
  end

  it "#updatable_track_monetizations" do
    expect(
      described_class.build(track_monetizations: track_monetizations).send("updatable_track_monetizations")
    ).to eql(
        [
          {
            id: track_monetization1.id,
            tc_distributor_state: "delivered",
            created_at: track_monetization1.created_at,
            updated_at: datetime_current
          },
          {
            id: track_monetization2.id,
            tc_distributor_state: "error",
            created_at: track_monetization2.created_at,
            updated_at: datetime_current
          }
        ]
    )
  end

  it "update state" do
    
    described_class.build(track_monetizations: track_monetizations).call

    expect(track_monetization1.reload.tc_distributor_state).to eql("delivered")
    expect(track_monetization1.updated_at.to_i).to eql(datetime_current.to_i)
    expect(track_monetization2.reload.tc_distributor_state).to eql("error")
    expect(track_monetization2.updated_at.to_i).to eql(datetime_current.to_i)
  end
end