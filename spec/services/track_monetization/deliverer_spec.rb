require "rails_helper"

describe TrackMonetization::Deliverer do

  let!(:admin) { create(:person, :admin) }
  let!(:album) { create(:album, :purchaseable, :paid, :with_salepoint_song, :approved, :taken_down) }
  let!(:song1) { album.songs.first }
  let!(:song2) { album.songs.last }


  context "with TC-Distributor delivery" do

    let!(:store) { create(:store, :tc_distributor) }
    let!(:store_delivery_config) { create(:store_delivery_config, store_id: store.id, distributable_class: "TrackMonetization") }
    let!(:track_monetization1) {
      create(:track_monetization, :eligible,
                                  :delivered_via_tc_distributor,
                                  store_id: store.id,
                                  song_id: song1.id) }
    let!(:track_monetization2) {
      create(:track_monetization, :eligible,
                                  :delivered_via_tc_distributor,
                                  store_id: store.id,
                                  song_id: song2.id) }

    let!(:track_monetizations_by_album_store_delivery_types) {
      {
        { album_id: album.id, delivery_type: "full_delivery", store_id: store.id } => [track_monetization1, track_monetization2]
      }
    }

    it "enqueue delivery" do
      #expect(TrackMonetization.for_tc_distributor_delivery).to eql("")
      

      expect(described_class).to receive(:new).with(track_monetizations_by_album_store_delivery_types: track_monetizations_by_album_store_delivery_types).and_call_original
      expect(Sns::Notifier).to receive(:perform).with(any_args).once

      deliverer = described_class.build.call

      expect(track_monetization1.reload.tc_distributor_state).to eql("enqueued")
      expect(track_monetization2.reload.tc_distributor_state).to eql("enqueued")
    end

  end


  context "with TC-WWW delivery" do
    let!(:store) { create(:store, :tunecore) }
    let!(:store_delivery_config) { create(:store_delivery_config, store_id: store.id, distributable_class: "TrackMonetization") }
    let!(:track_monetization1) {
      create(:track_monetization, :eligible,
                                  tc_distributor_state: "start",
                                  store_id: store.id,
                                  song_id: song1.id) }
    let!(:track_monetization2) {
      create(:track_monetization, :eligible,
                                  tc_distributor_state: "start",
                                  store_id: store.id,
                                  song_id: song2.id) }

    let!(:track_monetizations_by_album_store_delivery_types) {
      {
        { album_id: album.id, delivery_type: "full_delivery", store_id: store.id } => [track_monetization1, track_monetization2]
      }
    }

    it "enqueue delivery" do
      #expect(TrackMonetization.for_tc_distributor_delivery).to eql("")
      

      expect(described_class).to receive(:new).with(track_monetizations_by_album_store_delivery_types: track_monetizations_by_album_store_delivery_types).and_call_original
      expect(Sns::Notifier).to receive(:perform).with(any_args).once

      deliverer = described_class.build.call

      expect(track_monetization1.reload.tc_distributor_state).to eql("new")
      expect(track_monetization2.reload.tc_distributor_state).to eql("new")
    end
  end

end