require "rails_helper"

describe TrackMonetization::CreationService do
  let(:person) { create(:person) }
  let!(:live_album) {
    create(
      :album, :paid, :with_uploaded_songs, :with_salepoints, :with_petri_bundle,
      number_of_songs: 2,
      person: person,
      legal_review_state: "APPROVED"
    )
  }

  describe "#create_monetizations" do
    let(:params) {
      {
        album_id: live_album.id,
        store_id: Store::FBTRACKS_STORE_ID
      }
    }
    subject(:service) { described_class.new(params).create_monetizations }

    context "when there are no track monetizations" do
      it "creates track monetizations for the album" do
        expect { subject }.to change { TrackMonetization.count }.by(2)
        expect(live_album.track_monetizations.where(store_id: 83).count).to eq(2)
      end

      it "distributes the new track monetizations" do
        start_count = 0
        allow_any_instance_of(TrackMonetization).to receive(:start) { start_count += 1 }
        subject
        expect(start_count).to eq(2)
      end
    end

    context "when track monetizations exist" do
      before do
        live_album.songs.each do |song|
          create(
            :track_monetization,
            song_id: song.id,
            person_id: live_album.person.id,
            store_id: Store::FBTRACKS_STORE_ID,
            delivery_type: "full_delivery",
            eligibility_status: "eligible"
          )
        end
      end

      let!(:track1) { live_album.songs.first.track_monetizations.first }
      let!(:track2) { live_album.songs.last.track_monetizations.first }

      it "checks eligbility of track monetizations" do
        subject
        track1.reload
        track2.reload
        expect(track1.state).to eq("enqueued")
        expect(track2.state).to eq("enqueued")
      end

      it "skips further processing if track monetization is already delivered and eligible" do
        track1.delivered!
        subject
        track1.reload
        track2.reload
        expect(track1.state).to eq("delivered")
        expect(track2.state).to eq("enqueued")
      end
    end
  end
end
