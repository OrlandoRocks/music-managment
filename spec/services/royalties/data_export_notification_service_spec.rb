require "rails_helper"

describe Royalties::DataExportNotificationService do
  posting_id = "posting_id"
  sip_posting_ids = [12, 34]
  let(:s3)         { double("s3") }
  let(:bucket)    { double("buckets") }
  let(:object)    { double("objects") }
  let(:person) { create(:person) }
  let(:related) { create(:song) }

  before(:each) do
    allow(ENV).to receive(:fetch).with("SLACK_WEBHOOK_SIP_DEV_CHANNEL", nil).and_return("")
    allow(ENV).to receive(:fetch).with("SYMPHONY_CSV_BUCKET").and_return("tunecore.symphony.com")
    allow(Aws::S3::Resource).to receive(:new).and_return(s3)
    allow(s3).to receive(:bucket).and_return(bucket)
    allow(bucket).to receive(:object).and_return(object)
    allow(object).to receive(:put)
  end


  describe "When posting data is exported from the retool tool" do


    it "Generate report when presigned url for the data when it does not exists" do
      display_group = create(:sip_stores_display_group, name: "TestDisplayGroup")
      sip_store = create(:sip_store, name: "TestStore", display_group_id: display_group.id)
      srm = create(:sales_record_master_new, sip_store: sip_store, amount_currency: "EUR", period_sort: "2022-01-01")
      create(
        :sales_record_new, person_id: person.id, related_id: related.id, sales_record_master_id: srm.id,
                           amount: 100
      )
      described_class.run(posting_id)

      expect(Royalties::DataExportService.new.generate_csv.split(/\r/)).to eq(
        [
          "Sales Period,Store Name,Corporate Entity Id,Exchange Rate,Amount,Amount Currency\n",
          "2022-01-01,TestDisplayGroup,1,1,100.0,EUR\n"
        ])
    end

  end
end
