require "rails_helper"

describe Royalties::SyncService do
  posting_id = "posting_id"
  batch_id = 'batch_id'
  let(:persons) { create_list(:person, 2) }
  let(:person_ids) { persons.pluck(:id) }

  describe "Posting is not in specified state" do
    before(:each) do
      Royalties::Posting.add(posting_id)
    end

    after(:each) do
      Royalties::Posting.remove
    end

    it "does not update posting state" do
      Royalties::SyncService.run(
        Royalties::BatchService::SPLITS_COMPUTED,
        Royalties::Posting::BATCHES_CREATED,
        Royalties::Posting::CALCULATIONS_COMPLETED,
        'Royalties::NotificationWorker'
      )

      expect(Royalties::Posting.get_state).to eq Royalties::Posting::STARTED
    end
  end

  describe "Posting is in the specified state" do
    before(:each) do
      Royalties::Posting.add(posting_id)
      Royalties::Posting.update_state(Royalties::Posting::BATCHES_CREATED)
    end

    after(:each) do
      Royalties::Posting.remove
    end

    describe "batch state is not in specified state" do
      before(:each) do
        Royalties::BatchService.add_to_batch(batch_id, person_ids)
        Royalties::BatchService.update_batch_state(batch_id, Royalties::BatchService::PERSON_ROYALTIES_COMPUTED)
      end

      after(:each) do
        Royalties::BatchService.delete_existing_batches
      end

      it "does not update posting state" do
        Royalties::SyncService.run(
          Royalties::BatchService::SPLITS_COMPUTED,
          Royalties::Posting::BATCHES_CREATED,
          Royalties::Posting::CALCULATIONS_COMPLETED,
          'Royalties::NotificationWorker'
        )

        expect(Royalties::Posting.get_state).to eq Royalties::Posting::BATCHES_CREATED
      end
    end

    describe "batch state is in specified state" do
      before(:each) do
        Royalties::BatchService.add_to_batch(batch_id, person_ids)
        Royalties::BatchService.update_batch_state(batch_id, Royalties::BatchService::SPLITS_COMPUTED)
      end

      after(:each) do
        Royalties::BatchService.delete_existing_batches
      end

      it "updates posting state" do
        Royalties::SyncService.run(
          Royalties::BatchService::SPLITS_COMPUTED,
          Royalties::Posting::BATCHES_CREATED,
          Royalties::Posting::CALCULATIONS_COMPLETED,
          'Royalties::NotificationWorker'
        )

        expect(Royalties::Posting.get_state).to eq Royalties::Posting::CALCULATIONS_COMPLETED
      end
    end
  end
end
