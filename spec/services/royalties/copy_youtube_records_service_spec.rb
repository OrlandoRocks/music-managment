require "rails_helper"

describe Royalties::CopyYoutubeRecordsService do
  posting_id = "posting_id"
  batch_id = "batch_id"
  let(:persons) { create_list(:person, 10) }
  let(:person_ids) { persons.pluck(:id) }
  let(:related) { create(:song) }
  let(:you_tube_royalty_intakes) { create_list(:you_tube_royalty_intake, 10) }

  hash =
    [
      { "base_currency" => "USD", "currency" => "USD", "fx_rate" => 2, "coverage_rate" => 0 }
    ]

  before(:each) do
    YouTubeRoyaltyRecord.delete_all
    Royalties::Posting.add_currency_hash(hash)
    Royalties::Posting.add(posting_id)
  end

  after(:each) do
    Royalties::Posting.remove
    Royalties::BatchService.delete_existing_batches
  end

  describe "all users are marked as copied over" do
    before(:each) do
      persons.each do |person|
        create(:you_tube_royalty_record_new, person_id: person.id, song_id: related.id)
        create(:temp_person_royalty,
          person_id: person.id,
          youtube_records_copied: 1,
          you_tube_amount_in_user_currency: 10,
          user_currency: "USD")
      end
    end

    it "does not copy over records from sales_records_new" do
      described_class.run

      expect(YouTubeRoyaltyRecord.count).to eq 0
    end
  end

  describe "has records left to be copied over" do
    before(:each) do
      Royalties::BatchService.add_to_batch(batch_id, person_ids)

      persons.each_with_index do |person, i|
        create_list(:you_tube_royalty_record_new, 2, person_id: person.id, net_revenue: 10, song_id: related.id)
        create(
          :temp_person_royalty, person_id: person.id, youtube_records_copied: 0, user_currency: "USD",
                                you_tube_amount_in_user_currency: 10,
                                you_tube_royalty_intake_id: you_tube_royalty_intakes[i].id
        )
      end
    end

    it "copies over records from you_tube_royalty_records_new" do
      described_class.run

      expect(YouTubeRoyaltyRecord.count).to eq 20
    end

    it "does currency conversion" do
      described_class.run

      expect(YouTubeRoyaltyRecord.first.net_revenue).to eq 20
    end

    it "stores exchange rate" do
      described_class.run

      expect(Float(YouTubeRoyaltyRecord.first.exchange_rate)).to eq 2.0
    end

    it "marks records as copied in temp person royalties" do
      described_class.run

      expect(TempPersonRoyalty.where(youtube_records_copied: 1).count).to eq 10
    end

    it "updates batch state" do
      described_class.run

      expect(Royalties::BatchService.get_batch_state(batch_id)).to eq Royalties::BatchService::YOUTUBE_RECORDS_COPIED
    end

    it "updates psoting state" do
      described_class.run

      expect(Royalties::Posting.get_state).to eq Royalties::Posting::YOUTUBE_RECORDS_COPIED
    end

    it "copies youtube royalty records" do
      described_class.run

      expect(
        YouTubeRoyaltyRecord.all.pluck(:you_tube_royalty_intake_id).uniq
      ).to eq(
        you_tube_royalty_intakes.pluck(:id)
      )
    end
  end

  describe "you_tube_records that are not copied over have exchange rate" do
    let(:person_with_exchange_rate) { create(:person) }

    before(:each) do
      Royalties::BatchService.delete_existing_batches
      record = create(:you_tube_royalty_record_new, person_id: person_with_exchange_rate.id, song_id: related.id, exchange_rate: 2)
      Royalties::BatchService.add_to_batch(batch_id, [person_with_exchange_rate.id])
      create(:temp_person_royalty, person_id: person_with_exchange_rate.id, youtube_records_copied: 0, you_tube_amount_in_user_currency: 10)
    end

    it "stores cummulative exchange rate" do
      described_class.run

      expect(Float(YouTubeRoyaltyRecord.find_by(person_id: person_with_exchange_rate.id).exchange_rate)).to eq 4.0
    end
  end

  it "schedules the copy sales records worker" do
    expect(Royalties::CopySalesRecordsWorker).to receive(:perform_async)

    described_class.run
  end
end
