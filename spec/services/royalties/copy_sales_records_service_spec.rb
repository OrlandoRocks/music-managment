require "rails_helper"

describe Royalties::CopySalesRecordsService do
  posting_id = 'posting_id'
  batch_id = 'batch_id'
  let(:persons) { create_list(:person, 10) }
  let(:person_ids) { persons.pluck(:id) }
  let(:related) { create(:song) }
  hash =
    [
      {"base_currency" => 'USD', "currency" => 'USD', "fx_rate" => 2, "coverage_rate" => 0}
    ]

  before(:each) do
    SalesRecord.delete_all
    Royalties::Posting.add_currency_hash(hash)
    Royalties::Posting.add(posting_id)
  end

  after(:each) do
    Royalties::Posting.remove
    Royalties::BatchService.delete_existing_batches
  end

  describe "all users are marked as copied over" do

    before(:each) do
      create(:sales_record_master_new, id: 500, amount_currency: 'USD')
      create(:sales_record_master, id: 500, amount_currency: 'USD')

      persons.each do |person|
        create(:sales_record_new, person_id: person.id, sales_record_master_id: 500, related_id: related.id)
        create(:temp_person_royalty, person_id: person.id, sales_records_copied: 1)
      end
    end

    it "does not copy over records from sales_records_new" do
      described_class.run

      expect(SalesRecord.count).to eq 0
    end
  end

  describe "has records left to be copied over" do
    before(:each) do
      Royalties::BatchService.add_to_batch('batch_id', person_ids)
      create(:sales_record_master_new, id: 500, amount_currency: 'USD')
      create(:sales_record_master, id: 500, amount_currency: 'USD')

      persons.each do |person|
        create_list(:sales_record_new, 2, person_id: person.id, sales_record_master_id: 500, related_id: related.id, amount: 10)
        create(:temp_person_royalty, person_id: person.id, sales_records_copied: 0)
      end
    end

    it "copies over records from sales_records_new" do
      described_class.run

      expect(SalesRecord.count).to eq 20
    end

    it "does currency conversion" do
      described_class.run

      expect(SalesRecord.first.amount).to eq 20
    end

    it "marks records as copied in temp person royalties" do
      described_class.run

      expect(TempPersonRoyalty.where(sales_records_copied: 1).count).to eq 10
    end

    it "updates batch state" do
      described_class.run

      expect(Royalties::BatchService.get_batch_state('batch_id')).to eq Royalties::BatchService::SALES_RECORDS_COPIED
    end

    it "updates posting state" do
      described_class.run

      expect(Royalties::Posting.get_state). to eq Royalties::Posting::SALES_RECORDS_COPIED
    end

    it "schedules the notify sip worker" do
      expect(Royalties::NotifySipWorker).to receive(:perform_async)

      described_class.run
    end
  end
end
