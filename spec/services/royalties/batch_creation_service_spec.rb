require "rails_helper"

describe Royalties::BatchCreationService do
  let(:persons) { create_list(:person, 5) }
  let(:person_ids) { persons.pluck(:id) }

  before do
    Rails.cache.write(
      "ROYALTIES_POSTING",
      {
        posting_id: "posting_id",
        state: "started"
      }.to_json
    )
  end

  after do
    Rails.cache.delete("ROYALTIES_POSTING")
  end

  describe "#run" do
    context "posting is in started state" do
      it "creates batches and schedules batches" do
        batch_creation_service = described_class.new
        expect(batch_creation_service).to receive(:create_batches)
        expect(batch_creation_service).to receive(:schedule_batch_processing)
        batch_creation_service.run
        expect(JSON.parse(Rails.cache.fetch("ROYALTIES_POSTING"))["state"]).to eq("batches_created")
      end
    end

    context "posting is not in started state" do
      it "does not create batches and schedule batches" do
        Rails.cache.write(
          "ROYALTIES_POSTING",
          {
            posting_id: "posting_id",
            state: "batches_created"
          }.to_json
        )
        batch_creation_service = described_class.new
        expect(batch_creation_service).to_not receive(:create_batches)
        expect(batch_creation_service).to receive(:schedule_batch_processing)
        batch_creation_service.run
        expect(JSON.parse(Rails.cache.fetch("ROYALTIES_POSTING"))["state"]).to eq("batches_created")
      end
    end
  end

  describe "#create_batches" do
    before(:each) do
      allow(SecureRandom).to receive(:uuid).and_return("SecureRandom")
    end

    it "should create 2 batches" do
      stub_const("Royalties::BatchCreationService::WORKER_THRESHOLD", 10)
      stub_const("Royalties::BatchCreationService::PERSON_THRESHOLD", 4)

      (1..20).each do |_i|
        sales_record_master = create(:sales_record_master)
        related = create(:song)
        create(
          :sales_record_new, sales_record_master_id: sales_record_master.id, related_id: related.id,
                             person_id: person_ids[2]
        )
        create(:you_tube_royalty_record_new, person_id: person_ids[4], song_id: related.id)
      end

      (1..30).each do |_i|
        sales_record_master = create(:sales_record_master)
        related = create(:song)
        create(
          :sales_record_new, sales_record_master_id: sales_record_master.id, related_id: related.id,
                             person_id: person_ids[1]
        )
        create(:you_tube_royalty_record_new, person_id: person_ids[3], song_id: related.id)
      end

      (1..60).each do |_i|
        sales_record_master = create(:sales_record_master)
        related = create(:song)
        create(
          :sales_record_new, sales_record_master_id: sales_record_master.id, related_id: related.id,
                             person_id: person_ids[0]
        )
        create(:you_tube_royalty_record_new, person_id: person_ids[0], song_id: related.id)
      end

      expect(Royalties::BatchService).to receive(:add_to_batch).with("SecureRandom", [person_ids[0]])
      expect_any_instance_of(described_class).to receive(:add_to_temp_person_royalties).with([person_ids[0]])
      expect(Royalties::BatchService).to receive(:add_to_batch).with("SecureRandom", [person_ids[1], person_ids[3], person_ids[2], person_ids[4]])
      expect_any_instance_of(described_class).to receive(
        :add_to_temp_person_royalties
      ).with([person_ids[1], person_ids[3], person_ids[2], person_ids[4]])

      described_class.new.send(:create_batches)
    end

    it "should create 2 batches" do
      stub_const("Royalties::BatchCreationService::WORKER_THRESHOLD", 10)
      stub_const("Royalties::BatchCreationService::PERSON_THRESHOLD", 2)

      (1..10).each do |_i|
        sales_record_master = create(:sales_record_master)
        related = create(:song)
        create(
          :sales_record_new, sales_record_master_id: sales_record_master.id, related_id: related.id,
                             person_id: person_ids[2]
        )
      end

      (1..30).each do |_i|
        sales_record_master = create(:sales_record_master)
        related = create(:song)
        create(
          :sales_record_new, sales_record_master_id: sales_record_master.id, related_id: related.id,
                             person_id: person_ids[1]
        )
      end

      (1..60).each do |_i|
        sales_record_master = create(:sales_record_master)
        related = create(:song)
        create(
          :sales_record_new, sales_record_master_id: sales_record_master.id, related_id: related.id,
                             person_id: person_ids[0]
        )
      end

      expect(Royalties::BatchService).to receive(:add_to_batch).with("SecureRandom", [person_ids[0]])
      expect(Royalties::BatchService).to receive(:add_to_batch).with("SecureRandom", [person_ids[1], person_ids[2]])
      expect_any_instance_of(described_class).to receive(:add_to_temp_person_royalties).with([person_ids[0]])
      expect_any_instance_of(described_class).to receive(:add_to_temp_person_royalties).with(
        [
          person_ids[1],
          person_ids[2]
        ]
      )

      described_class.new.send(:create_batches)
    end
  end

  describe "#schedule_batch_processing" do
    it "schedules batches for processing" do
      Rails.cache.delete_matched("ROYALTIES_BATCH_*")

      key = "ROYALTIES_BATCH_batch_id1"
      batch = { person_ids: [1, 2, 3], state: Royalties::BatchService::BATCH_CREATED }.to_json
      Rails.cache.write(key, batch)
      key = "ROYALTIES_BATCH_batch_id2"
      batch = { person_ids: [4, 5, 6], state: Royalties::BatchService::BATCH_CREATED }.to_json
      Rails.cache.write(key, batch)
      key = "ROYALTIES_BATCH_batch_id3"
      batch = { person_ids: [7, 8, 9], state: Royalties::BatchService::PERSON_ROYALTIES_COMPUTED }.to_json
      Rails.cache.write(key, batch)

      expect(Royalties::ComputePersonRoyaltiesWorker).to receive(:perform_async).with("batch_id1")
      expect(Royalties::ComputePersonRoyaltiesWorker).to receive(:perform_async).with("batch_id2")
      expect(Royalties::ComputePersonRoyaltiesWorker).to receive(:perform_async).with("batch_id3")
      described_class.new.send(:schedule_batch_processing)
    end
  end

  describe "#delete_existing_batches" do
    it "deletes existing batches" do
      batch_creation_service = described_class.new
      expect(Royalties::BatchService).to receive(:delete_existing_batches)
      expect(TempPersonRoyalty).to receive(:delete_all)
      batch_creation_service.send(:delete_existing_batches)
    end
  end

  describe "#add_to_temp_person_royalties" do
    before do
      TempPersonRoyalty.delete_all
      allow(Time).to receive(:current).and_return(DateTime.parse("Fri, 17 Sep 2021 13:19:23 +0530"))
    end

    it "adds person to temp person royalty table" do
      person1 = create(:person)
      person2 = create(:person, :with_india_country_website)
      person3 = create(:person, :with_canada_country_website)

      expect(TempPersonRoyalty).to receive(:insert_all).with(
        [
          {
            corporate_entity_id: 1,
            person_id: person1.id,
            created_at: DateTime.parse("Fri, 17 Sep 2021 13:19:23 +0530"),
            updated_at: DateTime.parse("Fri, 17 Sep 2021 13:19:23 +0530"),
            user_currency: "USD"
          },
          {
            corporate_entity_id: 2,
            person_id: person2.id,
            created_at: DateTime.parse("Fri, 17 Sep 2021 13:19:23 +0530"),
            updated_at: DateTime.parse("Fri, 17 Sep 2021 13:19:23 +0530"),
            user_currency: "USD"
          },
          {
            corporate_entity_id: 2,
            person_id: person3.id,
            created_at: DateTime.parse("Fri, 17 Sep 2021 13:19:23 +0530"),
            updated_at: DateTime.parse("Fri, 17 Sep 2021 13:19:23 +0530"),
            user_currency: "CAD"
          }
        ]
      )
      described_class.new.send(:add_to_temp_person_royalties, [person1.id, person2.id, person3.id])
    end
  end
end
