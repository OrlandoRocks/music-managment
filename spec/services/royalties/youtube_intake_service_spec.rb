require "rails_helper"

describe Royalties::YoutubeIntakeService do
  let(:person) { create(:person, :with_balance, amount: 50_000) }
  let(:related) { create(:song) }

  before(:each) do
    stub_const("Royalties::ComputePersonRoyaltiesService::BATCH_SIZE", 3)
    key = "ROYALTIES_BATCH_batch_id"

    Rails.cache.delete(key)

    batch = { person_ids: [person.id], state: Royalties::BatchService::PERSON_INTAKE_COMPLETED }.to_json

    Rails.cache.write(key, batch)
  end

  after(:each) do
    key = "ROYALTIES_BATCH_batch_id"
    batch_details = JSON.parse(Rails.cache.fetch(key))
    expect(batch_details["state"]).to eq("youtube_intake_completed")
    Rails.cache.delete(key)
  end

  context "users without you tube royalties" do
    before do
      create(
        :temp_person_royalty,
        person_id: person.id,
        total_amount_in_user_currency: 10,
        you_tube_amount_in_user_currency: 0
      )
    end

    it "does not create youtube intakes" do
      prev_count = YouTubeRoyaltyIntake.count

      described_class.run("batch_id")
      intake_count = YouTubeRoyaltyIntake.count

      expect(intake_count).to eq prev_count
    end
  end

  context "users with youtube royalties" do
    before do
      create(
        :temp_person_royalty,
        person_id: person.id,
        total_amount_in_user_currency: 10,
        you_tube_amount_in_user_currency: 2,
        encumbrance_amount_in_user_currency: -4
      )

      create(:you_tube_royalty_intake)
    end

    it "create youtube royalty intakes" do
      described_class.run("batch_id")
      intake = YouTubeRoyaltyIntake.last

      expect(intake.person_id).to eq(person.id)
      expect(Float(intake.amount)).to eq(2.0)
      expect(intake.currency).to eq("USD")
    end

    it "create person transactions" do
      described_class.run("batch_id")
      person_transaction = PersonTransaction.last
      expect(person_transaction.person_id).to eq(person.id)
      expect(person_transaction.debit).to eq(0)
      expect(person_transaction.credit).to eq(2)
      expect(person_transaction.previous_balance).to eq(50_000)
      expect(person_transaction.target_id).to eq(YouTubeRoyaltyIntake.last.id)
      expect(person_transaction.target_type).to eq("YouTubeRoyaltyIntake")
      expect(person_transaction.currency).to eq("USD")
    end

    it "updates person balance based on amount in temp person royalty" do
      described_class.run("batch_id")
      person.reload
      expect(Float(person.balance)).to eq(50_002)
    end

    it "update youtube intake id to temp person royalty" do
      described_class.run("batch_id")
      temp_person_royalty = TempPersonRoyalty.find_by(person_id: person.id)
      expect(temp_person_royalty.you_tube_royalty_intake_id).to eq(YouTubeRoyaltyIntake.last.id)
    end
  end

  it "schedules the summarization worker for the batch" do
    expect(Royalties::PersonIntakeWorker).to receive(:perform_async).with("batch_id")

    described_class.run("batch_id")
  end
end
