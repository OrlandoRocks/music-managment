require "rails_helper"

describe Royalties::NotifySipService do
  let(:response) { double("response", body: @response_body.to_json, success?: true) }
  let(:client) { double("Royalties::ApiClient", finalize: response) }
  posting_id = "id123"
  currency_hash = [
    { "base_currency": "USD",currency: "USD",fx_rate: 2},
    { "base_currency": "USD",currency: "EUR",fx_rate: 2}
  ]

  before do
    Royalties::Posting.add(posting_id, [123,1234])
  end

  after do
    Royalties::Posting.remove
  end

  describe "Posting is no tin sales records copied" do
    it "Does not update posting state" do
      described_class.run

      expect(Royalties::Posting.get_state).to eq Royalties::Posting::STARTED
    end
  end

  describe "Posting is in sales records copied state" do
    before(:each) do
      allow(Royalties::ApiClient).to receive(:new).and_return(client)

      allow(ENV).to receive(:fetch).with("SIP_BASE_URL").and_return("http://XXX")
      allow(ENV).to receive(:fetch).with("SIP_API_KEY").and_return("KEY")

      Royalties::Posting.update_state(Royalties::Posting::SALES_RECORDS_COPIED)
    end

    it "updates posting state" do
      described_class.run

      expect(Royalties::Posting.get_state).to eq Royalties::Posting::COMPLETED
    end

    it "schedules the clean up worker" do
      expect(Royalties::CleanUpWorker).to receive(:perform_async)

      described_class.run
    end

    it "notifies sip" do
      expect(client).to receive(:finalize).with(array_including(123,1234))

      described_class.run
    end
  end
end
