require "rails_helper"

describe Royalties::DataExportService do
  let(:persons) { create_list(:person, 10) }
  let(:believe_persons) { create_list(:person, 10, :with_believe_corporate_entity) }

  let(:related) { create(:song) }

  before do
    # Create exchange rates in REDIS
    Rails.cache.write(
      "ROYALTIES_CURRENCY_HASH", [
        {
          currency: "USD",
          fx_rate: 2,
          coverage_rate: 0
        },
        {
          currency: "EUR",
          fx_rate: 3,
          coverage_rate: 0
        }
      ].to_json
    )
  end

  after do
    Rails.cache.delete(
      "ROYALTIES_CURRENCY_HASH"
    )
  end

  context "no data exists" do
    it "return empty enumerator" do
      data = described_class.new.generate_csv
      expect(data.split(/\r/)).to eq(
        [
          "Sales Period,Store Name,Corporate Entity Id,Exchange Rate,Amount,Amount Currency\n"
        ]
      )
    end
  end

  context "sales data exist but no youtube data" do
    it "returns enumerator with only sales data" do
      display_group = create(:sip_stores_display_group, name: "TestDisplayGroup")
      sip_store = create(:sip_store, name: "TestStore", display_group_id: display_group.id)
      srm = create(:sales_record_master_new, sip_store: sip_store, amount_currency: "EUR", period_sort: "2022-01-01")
      create(
        :sales_record_new, person_id: persons[0].id, related_id: related.id, sales_record_master_id: srm.id,
                           amount: 100
      )

      data = described_class.new.generate_csv
      expect(data.split(/\r/)).to eq(
        [
          "Sales Period,Store Name,Corporate Entity Id,Exchange Rate,Amount,Amount Currency\n",
          "2022-01-01,TestDisplayGroup,1,3.0,300.0,EUR\n"
        ]
      )
    end
  end

  context "youtube data exists but no sales data" do
    it "returns enumerator with only youtube data" do
      create(
        :you_tube_royalty_record_new,
        person_id: persons[0].id,
        song_id: related.id,
        sales_period_start: "2022-01-01",
        exchange_rate: 1,
        net_revenue: 100,
        net_revenue_currency: "USD"
      )

      data = described_class.new.generate_csv
      expect(data.split(/\r/)).to eq(
        [
          "Sales Period,Store Name,Corporate Entity Id,Exchange Rate,Amount,Amount Currency\n",
          "2022-01-01,Youtube,1,2.0,200.0,USD\n"
        ]
      )
    end
  end

  context "both youtube and sales data exist" do
    it "returns enumerator with both youtube and sales data" do
      believe_person = Person.where(corporate_entity_id: 2).last
      # Youtube records
      create(
        :you_tube_royalty_record_new,
        person_id: persons[0].id,
        song_id: related.id,
        sales_period_start: "2022-01-01",
        exchange_rate: 1,
        net_revenue: 100,
        net_revenue_currency: "USD"
      )
      create(
        :you_tube_royalty_record_new,
        person_id: believe_person.id,
        song_id: related.id,
        sales_period_start: "2022-01-01",
        exchange_rate: 1,
        net_revenue: 100,
        net_revenue_currency: "USD"
      )
      create(
        :you_tube_royalty_record_new,
        person_id: believe_person.id,
        song_id: related.id,
        sales_period_start: "2022-03-01",
        exchange_rate: 1,
        net_revenue: 100,
        net_revenue_currency: "EUR"
      )
      create(
        :you_tube_royalty_record_new,
        person_id: persons[0].id,
        song_id: related.id,
        sales_period_start: "2022-02-01",
        exchange_rate: 1,
        net_revenue: 100,
        net_revenue_currency: "EUR"
      )

      # Sales Records
      display_group = create(:sip_stores_display_group, name: "TestDisplayGroup")
      sip_store = create(:sip_store, name: "TestStore", display_group_id: display_group.id)
      srm = create(:sales_record_master_new, sip_store: sip_store, amount_currency: "EUR", period_sort: "2022-01-01")
      create(
        :sales_record_new, person_id: persons[0].id, related_id: related.id, sales_record_master_id: srm.id,
                           amount: 100
      )
      srm1 = create(:sales_record_master_new, sip_store: sip_store, amount_currency: "USD", period_sort: "2022-01-01")
      create(
        :sales_record_new, person_id: persons[0].id, related_id: related.id, sales_record_master_id: srm1.id,
                           amount: 100
      )

      display_group1 = create(:sip_stores_display_group, name: "TestDisplayGroup1")
      sip_store1 = create(:sip_store, name: "TestStore1", display_group_id: display_group1.id)
      srm2 = create(:sales_record_master_new, sip_store: sip_store1, amount_currency: "EUR", period_sort: "2022-01-01")
      create(
        :sales_record_new, person_id: persons[0].id, related_id: related.id, sales_record_master_id: srm2.id,
                           amount: 100
      )
      srm3 = create(:sales_record_master_new, sip_store: sip_store1, amount_currency: "EUR", period_sort: "2022-01-01")
      create(
        :sales_record_new, person_id: persons[0].id, related_id: related.id, sales_record_master_id: srm3.id,
                           amount: 100
      )
      create(
        :sales_record_new, person_id: believe_person.id, related_id: related.id,
                           sales_record_master_id: srm3.id, amount: 100
      )
      create(
        :sales_record_new, person_id: believe_person.id, related_id: related.id,
                           sales_record_master_id: srm3.id, amount: 100
      )

      data = described_class.new.generate_csv
      expect(data.split(/\r/)).to eq(
        [
          "Sales Period,Store Name,Corporate Entity Id,Exchange Rate,Amount,Amount Currency\n",
          "2022-01-01,Youtube,1,2.0,200.0,USD\n",
          "2022-01-01,Youtube,2,2.0,200.0,USD\n",
          "2022-02-01,Youtube,1,3.0,300.0,EUR\n",
          "2022-03-01,Youtube,2,3.0,300.0,EUR\n",
          "2022-01-01,TestDisplayGroup,1,3.0,300.0,EUR\n",
          "2022-01-01,TestDisplayGroup,1,2.0,200.0,USD\n",
          "2022-01-01,TestDisplayGroup1,1,3.0,600.0,EUR\n",
          "2022-01-01,TestDisplayGroup1,2,3.0,600.0,EUR\n"
        ]
      )
    end
  end
end
