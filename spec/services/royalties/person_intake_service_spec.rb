require "rails_helper"

describe Royalties::PersonIntakeService do
  let(:person) { create(:person, :with_balance, amount: 50_000) }
  let(:related) { create(:song) }

  let(:last_person_intake) { create(:person_intake) }
  let(:last_encumbrance_detail) { create(:encumbrance_detail) }
  let(:encumbrance_summary) { create(:encumbrance_summary, person_id: person.id, outstanding_amount: 100) }

  before(:each) do
    stub_const("Royalties::ComputePersonRoyaltiesService::BATCH_SIZE", 3)
    key = "ROYALTIES_BATCH_batch_id"

    Rails.cache.delete(key)

    batch = { person_ids: [person.id], state: Royalties::BatchService::PERSON_INTAKE_COMPLETED }.to_json

    Rails.cache.write(key, batch)
  end

  after(:each) do
    key = "ROYALTIES_BATCH_batch_id"
    batch_details = JSON.parse(Rails.cache.fetch(key))
    expect(batch_details["state"]).to eq("person_intake_completed")
    Rails.cache.delete(key)
  end

  context "users without you tube royalties" do
    before do
      create(
        :temp_person_royalty,
        person_id: person.id,
        total_amount_in_user_currency: 10,
        you_tube_amount_in_user_currency: nil,
        encumbrance_amount_in_user_currency: 0
      )
    end

    it "create person intakes" do
      described_class.run("batch_id")
      person_intake = PersonIntake.last

      expect(person_intake.person_id).to eq(person.id)
      expect(Float(person_intake.amount)).to eq(10.0)
      expect(person_intake.currency).to eq("USD")
    end

    it "create person transactions" do
      described_class.run("batch_id")
      person_transaction = PersonTransaction.last
      expect(person_transaction.person_id).to eq(person.id)
      expect(person_transaction.debit).to eq(0)
      expect(person_transaction.credit).to eq(10)
      expect(person_transaction.previous_balance).to eq(50_000)
      expect(person_transaction.target_id).to eq(PersonIntake.last.id)
      expect(person_transaction.target_type).to eq("PersonIntake")
      expect(person_transaction.currency).to eq("USD")
    end

    it "updates person balance based on amount in temp person royalty" do
      described_class.run("batch_id")
      person.reload
      expect(Float(person.balance)).to eq(50_010)
    end

  end

  context "users without encumbrance" do
    before do
      create(
        :temp_person_royalty,
        person_id: person.id,
        total_amount_in_user_currency: 10,
        you_tube_amount_in_user_currency: 2,
        encumbrance_amount_in_user_currency: 0
      )
    end

    it "create person intakes" do
      described_class.run("batch_id")
      person_intake = PersonIntake.last

      expect(person_intake.person_id).to eq(person.id)
      expect(Float(person_intake.amount)).to eq(10.0)
      expect(person_intake.currency).to eq("USD")
    end

    it "create person transactions" do
      described_class.run("batch_id")
      person_transaction = PersonTransaction.last
      expect(person_transaction.person_id).to eq(person.id)
      expect(person_transaction.debit).to eq(0)
      expect(person_transaction.credit).to eq(10)
      expect(person_transaction.previous_balance).to eq(50_000)
      expect(person_transaction.target_id).to eq(PersonIntake.last.id)
      expect(person_transaction.target_type).to eq("PersonIntake")
      expect(person_transaction.currency).to eq("USD")
    end

    it "updates person balance based on amount in temp person royalty" do
      described_class.run("batch_id")
      person.reload
      expect(Float(person.balance)).to eq(50_010)
    end

    it "update person intake id to temp person royalty" do
      described_class.run("batch_id")
      temp_person_royalty = TempPersonRoyalty.find_by(person_id: person.id)
      expect(temp_person_royalty.person_intake_id).to eq(PersonIntake.last.id)
    end
  end

  context "users with encumbrance" do
    before do
      create(
        :temp_person_royalty,
        person_id: person.id,
        total_amount_in_user_currency: 20,
        you_tube_amount_in_user_currency: 5,
        encumbrance_amount_in_user_currency: -10
      )

      create(
        :temp_encumbrance_detail, encumbrance_summary_id: encumbrance_summary.id, person_id: person.id,
                                  transaction_amount: -10
      )
    end

    it "add to encumbrance details" do
      described_class.run("batch_id")
      encumbrance_detail = EncumbranceDetail.last
      expect(encumbrance_detail.encumbrance_summary_id).to eq(encumbrance_summary.id)
      expect(encumbrance_detail.transaction_amount).to eq(-10)
    end

    it "update encumbrance summary" do
      described_class.run("batch_id")
      encumbrance_summary.reload
      expect(encumbrance_summary.outstanding_amount).to eq(90)
    end

    it "create encumbrance person transaction" do
      described_class.run("batch_id")
      transaction = PersonTransaction.last
      expect(transaction.person_id).to eq(person.id)
      expect(transaction.debit).to eq(10)
      expect(transaction.credit).to eq(0)
      expect(transaction.currency).to eq("USD")
      expect(transaction.previous_balance).to eq(50_020)
      expect(transaction.target_id).to eq(EncumbranceDetail.last.id)
      expect(transaction.target_type).to eq("EncumbranceDetail")
      expect(transaction.comment).to eq("TuneCore Direct Advance Recoupment")
    end

    it "updates person balance" do
      described_class.run("batch_id")
      person.reload
      expect(Float(person.balance)).to eq(50_010)
    end
  end

  context "users with only youtube royalties" do
    before do
      create(
        :temp_person_royalty,
        person_id: person.id,
        total_amount_in_user_currency: nil,
        you_tube_amount_in_user_currency: 10,
        encumbrance_amount_in_user_currency: 0
      )
    end

    it "does create person intakes" do
      prev_count = PersonIntake.count

      described_class.run("batch_id")
      intake_count = PersonIntake.count

      expect(intake_count).to eq prev_count
    end
  end

  it "schedules the summarization worker for the batch" do
    expect(Royalties::SummarizationWorker).to receive(:perform_async).with("batch_id")

    described_class.run("batch_id")
  end
end
