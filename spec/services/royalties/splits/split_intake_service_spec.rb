require "rails_helper"

describe Royalties::Splits::SplitIntakeService do
  posting_id = 'posting_id'
  batch_id = 'batch_id'
  let(:person) { create(:person, :with_balance, amount: 50_000) }
  let(:related) { create(:song) }
  let(:last_person_intake) { create(:split_intake, person_id: person.id) }
  let(:royalty_split) { create(:royalty_split, owner_id: person.id) }

  hash = [{
      "base_currency" => 'USD',
      "currency" => 'USD',
      "fx_rate" => 2,
      "coverage_rate" => 0
    }]

  before(:each) do
    TempRoyaltySplitDetail.delete_all
    RoyaltySplitIntake.delete_all

    Royalties::Posting.add_currency_hash(hash)
    Royalties::Posting.add(posting_id)

    create(
        :temp_royalty_split_detail,
        person_id: person.id,
        owner_id: person.id,
        song_id: related.id,
        transaction_amount_in_usd:-30,
        royalty_split_id: royalty_split.id,
        royalty_split_title: royalty_split.title,
        processed: 0
      )
      create(:temp_person_royalty, person_id: person.id, person_intake_id: '123')
  end

  after(:each) do
    expect(Royalties::Posting.get_state).to eq Royalties::Posting::SPLIT_INTAKE_COMPLETED

    Royalties::Posting.remove
  end

  it "creates split intakes" do
    described_class.run
    intake = RoyaltySplitIntake.last

    expect(intake.person_id).to eq(person.id)
    expect(Float(intake.amount)).to eq(-60.0)
    expect(intake.currency).to eq("USD")
  end

  it "creates split detials" do
    described_class.run
    split_detail = RoyaltySplitDetail.last

    expect(split_detail.person_id).to eq(person.id)
    expect(split_detail.royalty_split_intake_id).to eq(RoyaltySplitIntake.last.id)
    expect(split_detail.royalty_split_id).to eq(royalty_split.id)
    expect(split_detail.royalty_split_title).to eq(royalty_split.title)
    expect(split_detail.song_id).to eq(related.id)
    expect(split_detail.person_intake_id).to eq(123)
    expect(split_detail.amount).to eq(-60)
    expect(split_detail.currency).to eq('USD')
    expect(split_detail.owner_id).to eq(person.id)
  end

  it "create person transactions" do
    described_class.run

    person_transaction = PersonTransaction.last
    expect(person_transaction.person_id).to eq(person.id)
    expect(person_transaction.debit).to eq(60)
    expect(person_transaction.credit).to eq(0)
    expect(person_transaction.previous_balance).to eq(50_000)
    expect(person_transaction.target_id).to eq(RoyaltySplitIntake.last.id)
    expect(person_transaction.target_type).to eq("SplitRoyaltyIntake")
    expect(person_transaction.currency).to eq("USD")
  end

  it "updates person balance based on amount in temp person royalty" do
    described_class.run
    person.reload
    expect(Float(person.balance)).to eq(49_940)
  end


  it "marks as processed" do
    described_class.run

    expect(TempRoyaltySplitDetail.where(processed: 0)).to be_empty
  end
end
