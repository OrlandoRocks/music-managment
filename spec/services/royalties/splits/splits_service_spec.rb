require "rails_helper"

describe Royalties::Splits::SplitsService do
  batch_id = "batch_id"
  let(:owner) { create(:person) }
  let(:person1) { create(:person) }
  let(:person2) { create(:person) }
  let(:person3) { create(:person) }

  before do
    [owner.id, person1.id, person2.id, person3.id].each do |person_id|
      create(
        :temp_person_royalty,
        person_id: person_id,
        total_amount_in_usd: 10,
        encumbrance_amount_in_usd: 0.0,
        encumbrance_percentage: 0,
        split_computed: false
      )
    end

    stub_const("Royalties::Splits::SplitsService::BATCH_SIZE", 3)

    Royalties::BatchService.add_to_batch(batch_id, [owner.id, person1.id, person2.id, person3.id])
    Royalties::BatchService.update_batch_state(batch_id, Royalties::BatchService::PERSON_ROYALTIES_COMPUTED)

    expect(Royalties::SyncWorker).to receive(
      :perform_in
    ).with(
      5.minutes,
      Royalties::BatchService::SPLITS_COMPUTED,
      Royalties::Posting::BATCHES_CREATED,
      Royalties::Posting::CALCULATIONS_COMPLETED,
      Royalties::NotificationWorker
    )
  end

  after(:each) do
    expect(Royalties::BatchService.get_batch_state(batch_id)).to eq("splits_computed")

    Royalties::BatchService.delete_existing_batches
  end

  context "user has total_amount_in_usd and splits not yet computed" do
    it "calls ComputeBatchSplitService for all users" do
      expect(Royalties::Splits::ComputeBatchSplitsService).to receive(
        :run
      ).with(
        [owner.id, person1.id, person2.id]
      )
      expect(Royalties::Splits::ComputeBatchSplitsService).to receive(
        :run
      ).with(
        [person3.id]
      )

      described_class.run("batch_id")
    end
  end

  context "user does not have total_amount_in_usd" do
    it "does not call ComputeBatchSplitService for users without total_amount_in_usd" do
      TempPersonRoyalty.where(person_id: [person2.id, person3.id]).update(
        total_amount_in_usd: nil
      )
      expect(Royalties::Splits::ComputeBatchSplitsService).to receive(
        :run
      ).with(
        [owner.id, person1.id]
      )

      described_class.run("batch_id")
    end
  end

  context "user has total_amount_in_usd and encumbrance" do
    it "does not call ComputeBatchSplitService for users having encumbrance" do
      TempPersonRoyalty.where(person_id: [person2.id, person3.id]).update(
        encumbrance_amount_in_usd: -10
      )
      expect(Royalties::Splits::ComputeBatchSplitsService).to receive(
        :run
      ).with(
        [owner.id, person1.id]
      )

      described_class.run("batch_id")
    end
  end

  context "idempotency check" do
    it "does not call ComputeBatchSplitService for users with splits already computed" do
      TempPersonRoyalty.where(person_id: [person2.id, person3.id]).update(
        split_computed: nil
      )
      expect(Royalties::Splits::ComputeBatchSplitsService).to receive(
        :run
      ).with(
        [owner.id, person1.id]
      )
      described_class.run("batch_id")
    end
  end

  context "user's status is Suspicious" do
    it "does not call ComputeBatchSplitService for users whose state in people table is suspicious" do
      Person.where(id: [person1.id, person2.id]).update(status: "Suspicious")
      expect(Royalties::Splits::ComputeBatchSplitsService).to receive(
        :run
      ).with(
        [owner.id, person3.id]
      )

      described_class.run("batch_id")
    end

    it "does not call ComputeBatchSplitService for users flagged as suspicious" do
      flag = Flag.find_by(Flag::SUSPICIOUS)
      flag_reason = FlagReason.suspicious_flag_reasons.first
      create(:people_flag, person: person1, flag: flag, flag_reason: flag_reason)

      expect(Royalties::Splits::ComputeBatchSplitsService).to receive(
        :run
      ).with(
        [owner.id, person2.id, person3.id]
      )

      described_class.run("batch_id")
    end
  end
end
