require "rails_helper"

describe Royalties::Splits::ComputeBatchSplitsService do
  let(:person1) { create(:person) }
  let(:person2) { create(:person) }
  let(:person3) { create(:person) }

  let(:album) { create(:album) }
  let(:related1) { create(:song, album: album) }
  let(:related2) { create(:song, album: album) }

  before(:each) do
    TempRoyaltySplitDetail.delete_all
  end

  context "Person has no split config" do
    it "does not create any temp royalty split detail" do
      expect {
        described_class.run(
          [
            person1.id, person2.id, person3.id
          ]
        )
      }.to change {
             TempRoyaltySplitDetail.count
           }.by(0)
    end
  end

  context "Person has split config but no track with royalty" do
    it "does not create any temp royalty split detail" do
      royalty_split = create(:royalty_split, owner_id: person1.id)
      create(:royalty_split_song, royalty_split: royalty_split, song_id: related1.id)
      create(
        :royalty_split_recipient,
        royalty_split: royalty_split,
        person_id: person2.id,
        percent: 25,
        accepted_at: Time.now()
      )

      create(
        :royalty_split_recipient,
        royalty_split: royalty_split,
        person_id: person3.id,
        percent: 25,
        accepted_at: Time.now()
      )
      royalty_split.owner_recipient.update(percent: 50)

      expect {
        described_class.run(
          [
            person1.id, person2.id, person3.id
          ]
        )
      }.to change {
             TempRoyaltySplitDetail.count
           }.by(0)
    end
  end

  context "Person has split config and track with royalty" do
    before do
      royalty_split = create(:royalty_split, title: "Test", owner_id: person1.id)
      create(:royalty_split_song, royalty_split: royalty_split, song_id: related1.id)
      create(
        :royalty_split_recipient,
        royalty_split: royalty_split,
        person_id: person2.id,
        percent: 25,
        accepted_at: Time.now()
      )

      create(
        :royalty_split_recipient,
        royalty_split: royalty_split,
        person_id: person3.id,
        percent: 25,
        accepted_at: Time.now()
      )
      royalty_split.owner_recipient.update(percent: 50)

      sales_record_master = create(:sales_record_master)
      create(
        :sales_record_new,
        person_id: person1.id,
        sales_record_master_id: sales_record_master.id,
        related_id: related1.id,
        related_type: "Song",
        amount: 100
      )
      create(
        :sales_record_new,
        person_id: person1.id,
        sales_record_master_id: sales_record_master.id,
        related_id: related1.id,
        related_type: "Song",
        amount: 100
      )
    end

    it "create 3 temp royalty split detail" do
      expect {
        described_class.run(
          [
            person1.id, person2.id, person3.id
          ]
        )
      }.to change {
             TempRoyaltySplitDetail.count
           }.by(3)
    end

    it "updates transaction amounts for account holders" do
      described_class.run(
        [
          person1.id, person2.id, person3.id
        ]
      )
      expect(
        Float(TempRoyaltySplitDetail.find_by(person_id: person1.id).transaction_amount_in_usd)
      ).to eq(-100)
      expect(
        Float(TempRoyaltySplitDetail.find_by(person_id: person2.id).transaction_amount_in_usd)
      ).to eq(50)
      expect(
        Float(TempRoyaltySplitDetail.find_by(person_id: person3.id).transaction_amount_in_usd)
      ).to eq(50)
    end

    it "adds owner id for temp royalty split detail" do
      described_class.run(
        [
          person1.id, person2.id, person3.id
        ]
      )

      expect(
        TempRoyaltySplitDetail.pluck(:owner_id).uniq
      ).to eq([person1.id])
    end

    it "adds related id for temp royalty split detail" do
      described_class.run(
        [
          person1.id, person2.id, person3.id
        ]
      )

      expect(
        TempRoyaltySplitDetail.pluck(:song_id).uniq
      ).to eq([related1.id])
    end

    it "adds royalty split id and title for temp royalty split detail" do
      described_class.run(
        [
          person1.id, person2.id, person3.id
        ]
      )

      expect(
        TempRoyaltySplitDetail.pluck(:royalty_split_id).uniq
      ).to eq([RoyaltySplit.last.id])
      expect(
        TempRoyaltySplitDetail.pluck(:royalty_split_title).uniq
      ).to eq([RoyaltySplit.last.title])
    end

    it "add royalty split config to temp split detail" do
      described_class.run(
        [
          person1.id, person2.id, person3.id
        ]
      )

      royalty_split_config = TempRoyaltySplitDetail.pluck(:royalty_split_config).uniq
      expect(royalty_split_config).to eq(
        [
          {
            title: "Test",
            owner_id: person1.id,
            recipients: [
              {
                person_id: person1.id,
                percent: "50.0",
                active: true,
                email: nil
              },
              {
                person_id: person2.id, percent: "25.0", active: true, email: nil
              },
              {
                person_id: person3.id,
                percent: "25.0",
                active: true,
                email: nil
              }
            ]
          }.to_json
        ]
      )
    end
  end

  context "Has multiple active royalty split and tracks" do
    before do
      royalty_split = create(:royalty_split, title: "Test", owner_id: person1.id)
      create(:royalty_split_song, royalty_split: royalty_split, song_id: related1.id)
      create(
        :royalty_split_recipient,
        royalty_split: royalty_split,
        person_id: person2.id,
        percent: 25,
        accepted_at: Time.now()
      )
      create(
        :royalty_split_recipient,
        royalty_split: royalty_split,
        person_id: person3.id,
        percent: 25,
        accepted_at: Time.now()
      )
      royalty_split.owner_recipient.update(percent: 50)
      sales_record_master = create(:sales_record_master)
      create(
        :sales_record_new,
        person_id: person1.id,
        sales_record_master_id: sales_record_master.id,
        related_id: related1.id,
        related_type: "Song",
        amount: 100
      )

      royalty_split1 = create(:royalty_split, title: "Test1", owner_id: person2.id)
      create(:royalty_split_song, royalty_split: royalty_split1, song_id: related2.id)
      create(
        :royalty_split_recipient,
        royalty_split: royalty_split1,
        person_id: person1.id,
        percent: 25,
        accepted_at: Time.now()
      )
      create(
        :royalty_split_recipient,
        royalty_split: royalty_split1,
        person_id: person3.id,
        percent: 25,
        accepted_at: Time.now()
      )
      royalty_split1.owner_recipient.update(percent: 50)
      sales_record_master1 = create(:sales_record_master)
      create(
        :sales_record_new,
        person_id: person2.id,
        sales_record_master_id: sales_record_master1.id,
        related_id: related2.id,
        related_type: "Song",
        amount: 200
      )
    end
    it "create temp royalty split detail for all users" do
      expect {
        described_class.run(
          [
            person1.id, person2.id, person3.id
          ]
        )
      }.to change {
             TempRoyaltySplitDetail.count
           }.by(6)
    end

    it "updates transaction amounts for account holders" do
      described_class.run(
        [
          person1.id, person2.id, person3.id
        ]
      )
      expect(
        TempRoyaltySplitDetail.where(person_id: person2.id).pluck(:transaction_amount_in_usd).map(&:to_f)
      ).to eq([25, -100])
      expect(
        TempRoyaltySplitDetail.where(person_id: person1.id).pluck(:transaction_amount_in_usd).map(&:to_f)
      ).to eq([-50, 50])
      expect(
        TempRoyaltySplitDetail.where(person_id: person3.id).pluck(:transaction_amount_in_usd).map(&:to_f)
      ).to eq([25, 50])
    end

    it "create royalty split config for temp split details" do
      described_class.run(
        [
          person1.id, person2.id, person3.id
        ]
      )

      royalty_split_config = TempRoyaltySplitDetail.pluck(:royalty_split_config).uniq
      expect(royalty_split_config).to match_array(
        [
          {
            title: "Test",
            owner_id: person1.id,
            recipients: [
              {
                person_id: person1.id, percent: "50.0", active: true, email: nil
              },
              {
                person_id: person2.id, percent: "25.0", active: true, email: nil
              },
              {
                person_id: person3.id, percent: "25.0", active: true, email: nil
              }
            ]
          }.to_json,
          {
            title: "Test1",
            owner_id: person2.id,
            recipients: [
              {
                person_id: person2.id, percent: "50.0", active: true, email: nil
              },
              {
                person_id: person1.id, percent: "25.0", active: true, email: nil
              },
              {
                person_id: person3.id, percent: "25.0", active: true, email: nil
              }
            ]
          }.to_json
        ]
      )
    end
  end

  context "Has inactive royalty split" do
    before do
      royalty_split = create(:royalty_split, title: "Test", owner_id: person1.id, deactivated_at: Time.now - 2)
      create(:royalty_split_song, royalty_split: royalty_split, song_id: related1.id)
      create(
        :royalty_split_recipient,
        royalty_split: royalty_split,
        person_id: person2.id,
        percent: 50,
        accepted_at: Time.now()
      )
      royalty_split.owner_recipient.update(percent: 50)

      sales_record_master = create(:sales_record_master)
      create(
        :sales_record_new,
        person_id: person1.id,
        sales_record_master_id: sales_record_master.id,
        related_id: related1.id,
        related_type: "Song",
        amount: 100
      )
    end

    it "does not create temp royalty split detail for all users" do
      expect {
        described_class.run(
          [
            person1.id, person2.id, person3.id
          ]
        )
      }.to change {
             TempRoyaltySplitDetail.count
           }.by(0)
    end
  end

  context "Has inactive recipients" do
    before do
      royalty_split = create(:royalty_split, title: "Test", owner_id: person1.id)
      create(:royalty_split_song, royalty_split: royalty_split, song_id: related1.id)
      create(
        :royalty_split_recipient,
        royalty_split: royalty_split,
        person_id: person2.id,
        percent: 25
      )
      create(
        :royalty_split_recipient,
        royalty_split: royalty_split,
        person_id: person3.id,
        percent: 25,
        accepted_at: Time.now()
      )
      royalty_split.owner_recipient.update(percent: 50)
      sales_record_master = create(:sales_record_master)
      create(
        :sales_record_new,
        person_id: person1.id,
        sales_record_master_id: sales_record_master.id,
        related_id: related1.id,
        related_type: "Song",
        amount: 100
      )
    end
    it "create temp royalty split detail for all users" do
      expect {
        described_class.run(
          [
            person1.id, person2.id, person3.id
          ]
        )
      }.to change {
             TempRoyaltySplitDetail.count
           }.by(2)
    end

    it "updates transaction amounts for account holders" do
      described_class.run(
        [
          person1.id, person2.id, person3.id
        ]
      )
      expect(
        Float(TempRoyaltySplitDetail.where(person_id: person1.id).pluck(:transaction_amount_in_usd).first)
      ).to eq(-25)
      expect(
        Float(TempRoyaltySplitDetail.where(person_id: person3.id).pluck(:transaction_amount_in_usd).first)
      ).to eq(25)
    end

    it "create royalty split config for temp split details" do
      described_class.run(
        [
          person1.id, person2.id, person3.id
        ]
      )

      royalty_split_config = TempRoyaltySplitDetail.pluck(:royalty_split_config).uniq
      expect(royalty_split_config).to match_array(
        [
          {
            title: "Test",
            owner_id: person1.id,
            recipients: [
              {
                person_id: person1.id, percent: "50.0", active: true, email: nil
              },
              {
                person_id: person2.id, percent: "25.0", active: false, email: nil
              },
              {
                person_id: person3.id, percent: "25.0", active: true, email: nil
              }
            ]
          }.to_json
        ]
      )
    end
  end

  context "Person has split config and both songs and albums with royalty" do
    before do
      royalty_split = create(:royalty_split, title: "Test", owner_id: person1.id)
      create(:royalty_split_song, royalty_split: royalty_split, song_id: related1.id)
      create(:royalty_split_song, royalty_split: royalty_split, song_id: related2.id)
      create(
        :royalty_split_recipient,
        royalty_split: royalty_split,
        person_id: person2.id,
        percent: 50,
        accepted_at: Time.now
      )
      royalty_split.owner_recipient.update(percent: 50)

      sales_record_master = create(:sales_record_master)
      create(
        :sales_record_new,
        person_id: person1.id,
        sales_record_master_id: sales_record_master.id,
        related_id: album.id,
        related_type: "Album",
        amount: 100
      )
      create(
        :sales_record_new,
        person_id: person1.id,
        sales_record_master_id: sales_record_master.id,
        related_id: related2.id,
        related_type: "Song",
        amount: 100
      )
    end

    it "create 2 temp royalty split detail" do
      expect {
        described_class.run(
          [
            person1.id, person2.id, person3.id
          ]
        )
      }.to change {
             TempRoyaltySplitDetail.count
           }.by(4)
    end

    it "updates transaction amounts for account holders" do
      described_class.run(
        [
          person1.id, person2.id, person3.id
        ]
      )
      expect(
        Float(TempRoyaltySplitDetail.find_by(person_id: person1.id, song_id: related1.id).transaction_amount_in_usd)
      ).to eq(-25)
      expect(
        Float(TempRoyaltySplitDetail.find_by(person_id: person1.id, song_id: related2.id).transaction_amount_in_usd)
      ).to eq(-75)
      expect(
        Float(TempRoyaltySplitDetail.find_by(person_id: person2.id, song_id: related1.id).transaction_amount_in_usd)
      ).to eq(25)
      expect(
        Float(TempRoyaltySplitDetail.find_by(person_id: person2.id, song_id: related2.id).transaction_amount_in_usd)
      ).to eq(75)
    end
  end
end
