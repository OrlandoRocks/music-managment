require "rails_helper"

describe Royalties::SummarizationService do
  posting_id = 'posting_id'
  batch_id = 'batch_id'
  let(:persons) { create_list(:person, 10) }
  let(:person_ids) { persons.pluck(:id) }
  let(:related) { create(:song) }
  hash =
    [
      {"base_currency" => 'USD', "currency" => 'USD', "fx_rate" => 2, "coverage_rate" => 0}
    ]

  before(:each) do
    SalesRecordSummary.delete_all
    Royalties::Posting.add_currency_hash(hash)
    Royalties::Posting.add(posting_id)
    Royalties::BatchService.add_to_batch(batch_id, person_ids)
    create(:sales_record_master_new, id: 1, amount_currency: 'USD')

    persons.each do |person|
      create_list(
        :sales_record_new,
        2,
        person_id: person.id,
        sales_record_master_id: 1,
        related_id: related.id,
        related_type: 'Song',
        amount: 10,
        distribution_type: 'Download',
        quantity: 9
      )
      create(:temp_person_royalty, person_id: person.id, summarized: 0, total_amount_in_usd: 10)
    end
  end

  after(:each) do
    Royalties::Posting.remove
    Royalties::BatchService.delete_existing_batches
  end

  it "inserts summarized records into sales record summaries" do
    described_class.run(batch_id)

    expect(SalesRecordSummary.count).to eq 10
  end

  it "does currency conversion" do
    described_class.run(batch_id)

    expect(SalesRecordSummary.first.download_amount).to eq 40
    expect(SalesRecordSummary.first.downloads_sold).to eq 18
    expect(SalesRecordSummary.first.release_type).to eq 'Album'
  end

  it "marks records as summarized in temp person royalties" do
    described_class.run(batch_id)

    expect(TempPersonRoyalty.where(summarized: 1).count).to eq 10
  end

  it "updates batch state" do
    described_class.run(batch_id)

    expect(Royalties::BatchService.get_batch_state(batch_id)).to eq Royalties::BatchService::SUMMARIZED
  end

  it "schedules the sync worker" do
    expect(Royalties::SyncWorker).to receive(:perform_in).with(5.minutes,
            Royalties::BatchService::SUMMARIZED,
            Royalties::Posting::SALES_RECORD_MASTERS_COPIED,
            Royalties::Posting::SUMMARIZATION_COMPLETED,
            Royalties::Splits::SplitIntakeWorker)

    described_class.run("batch_id")
  end
end
