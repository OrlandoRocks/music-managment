require "rails_helper"

describe Royalties::CleanUpService do
  posting_id = "posting_id"
  batch_id = "batch_id"

  describe "Posting is in started state" do
    before(:each) do
      Royalties::Posting.add(posting_id)
    end

    after(:each) do
      Royalties::Posting.remove
    end

    it "will not clean up" do
      described_class.run

      expect(Royalties::Posting.get_state).to eq Royalties::Posting::STARTED
    end
  end

  describe "Posting is in completed state" do
    let(:persons) { create_list(:person, 10) }
    let(:person_ids) { persons.pluck(:id) }

    before(:each) do
      Royalties::Posting.add(posting_id)
      Royalties::Posting.update_state(Royalties::Posting::COMPLETED)

      Royalties::BatchService.add_to_batch(batch_id, person_ids)
      Royalties::BatchService.update_batch_state(batch_id, Royalties::BatchService::YOUTUBE_RECORDS_COPIED)

      persons.each do |person|
        create(:temp_person_royalty, person_id: person.id, total_amount_in_usd: 10)
        create(:temp_encumbrance_detail, person_id: person.id)
        royalty_split = royalty_split_with_recipients(owner: person)
        album = create(:album, :with_uploaded_songs, person: person)
        song = create(:song, album: album)

        create(:temp_royalty_split_detail,
          royalty_split_id: royalty_split.id,
          song_id: song.id,
          person_id: person.id,
          owner_id: person.id
        )
      end
    end

    it "deletes posting" do
      described_class.run

      expect(Royalties::Posting.get_id).to be nil
    end

    it "deletes batches" do
      described_class.run

      expect(Royalties::BatchService.get_all_batch_ids.count).to be 0
    end

    it "deletes data in temp person royalties" do
      described_class.run

      expect(TempPersonRoyalty.count).to be 0
    end

    it "deletes data in temp encumbrance details" do
      described_class.run

      expect(TempEncumbranceDetail.count).to be 0
    end

    it "deletes data in temp royalty split details" do
      described_class.run

      expect(TempRoyaltySplitDetail.count).to be 0
    end

    describe "tax feature switch is turned on" do
      before(:each) do
        allow(FeatureFlipper).to receive(:show_feature?).and_return true
      end

      it "schedules the sip batch worker" do
        expect(TaxWithholdings::SipBatchWorker).to receive(:perform_async)

        described_class.run
      end
    end

    describe "tax feature switch is turned off" do
      before(:each) do
        allow(FeatureFlipper).to receive(:show_feature?).and_return false
      end

      it "does not schedules the sip batch worker" do
        expect(TaxWithholdings::SipBatchWorker).not_to receive(:perform_async)

        described_class.run
      end
    end

    describe "clean tc-www tables" do
      before do
        related = create(:song)
        person = create(:person)
        sip_store = create(:sip_store)
        create_list(
          :you_tube_royalty_record_new,
          10,
          person_id: person.id,
          song_id: related.id,
          sales_period_start: "2022-02-01",
          exchange_rate: 1,
          net_revenue: 100,
          net_revenue_currency: "EUR"
        )
        srm = create(:sales_record_master_new, sip_store: sip_store, amount_currency: "EUR", period_sort: "2022-01-01")
        create_list(
          :sales_record_new, 5, person_id: person.id, related_id: related.id, sales_record_master_id: srm.id,
                                amount: 100
        )
      end

      after(:each) do
        Rails.cache.delete(Royalties::Posting::POSTING_KEY)
      end

      it "does not cleanup if source of posting is SIP" do
        posting = {
          posting_id: "123",
          state: Royalties::Posting::COMPLETED,
          sip_posting_ids: [],
          source: "sip"
        }.to_json

        Rails.cache.write(Royalties::Posting::POSTING_KEY, posting)

        described_class.run
        expect(SalesRecordNew.count).to eq(5)
        expect(SalesRecordMasterNew.count).to eq(1)
        expect(YouTubeRoyaltyRecordNew.count).to eq(10)
      end

      it "cleanup the _new tables if the source of posting is symphony" do
        posting = {
          posting_id: "123",
          state: Royalties::Posting::COMPLETED,
          sip_posting_ids: [],
          source: "symphony"
        }.to_json

        Rails.cache.write(Royalties::Posting::POSTING_KEY, posting)

        described_class.run
        expect(SalesRecordNew.count).to eq(0)
        expect(SalesRecordMasterNew.count).to eq(0)
        expect(YouTubeRoyaltyRecordNew.count).to eq(0)
      end
    end
  end
end
