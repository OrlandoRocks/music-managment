require "rails_helper"

describe Royalties::CopySalesRecordMastersService do
  posting_id = "posting_id"

  describe "Posting is in started state" do
    before(:each) do
      Royalties::Posting.add(posting_id)
    end

    after(:each) do
      Royalties::Posting.remove
    end

    it "will not copy  records" do
      described_class.run

      expect(Royalties::Posting.get_state).to eq Royalties::Posting::STARTED
    end
  end

  describe "Posting is in finance approved state" do
    before(:each) do
      Royalties::Posting.add(posting_id)
      Royalties::Posting.update_state(Royalties::Posting::FINANCE_APPROVED)
      SalesRecordMaster.delete_all
      create_list(:sales_record_master_new, 15)
    end

    after(:each) do
      Royalties::Posting.remove
    end

    it "updates posting state" do
      described_class.run

      expect(Royalties::Posting.get_state).to eq Royalties::Posting::SALES_RECORD_MASTERS_COPIED
    end

    it "copies sales record masters" do
      described_class.run

      expect(SalesRecordMaster.count).to eq 15
    end

    it "copied sales record masters with exchange rates" do
      hash =
        [
          { "base_currency" => "USD", "currency" => "USD", "fx_rate" => 2, "coverage_rate" => 0 }
        ]

      Royalties::Posting.add_currency_hash(hash)
      described_class.run

      expect(SalesRecordMaster.all.pluck(:exchange_rate).uniq.map { |i| Float(i) }).to eq([2.0])
    end
  end

  it "schedules the encumbrance worker for the batch" do
    allow(Royalties::BatchService).to receive(:get_all_batch_ids).and_return(["id1", "id2"])

    expect { described_class.run }.to change(Royalties::CurrencyConversionWorker.jobs, :size).by(2)
    expect(Royalties::CurrencyConversionWorker.jobs.last["args"]).to eq(["id2"])
  end
end
