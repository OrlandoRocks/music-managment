require "rails_helper"

describe Royalties::ComputeEncumbranceService do
  batch_id = 'batch_id'
  let(:persons) { create_list(:person, 10) }
  let(:person_ids) { persons.pluck(:id) }

  before(:each) do
    stub_const("Royalties::ComputeEncumbranceService::BATCH_SIZE", 3)
    Royalties::BatchService.add_to_batch(batch_id, person_ids)
    Royalties::BatchService.update_batch_state(batch_id, Royalties::BatchService::PERSON_ROYALTIES_COMPUTED)

    persons.each do |person|
      create(:temp_person_royalty, person_id: person.id, total_amount_in_usd: 10, you_tube_amount_in_usd: nil)
      create(:encumbrance_summary, person: person, outstanding_amount: 0)
    end
  end

  after(:each) do
    expect(Royalties::BatchService.get_batch_state(batch_id)).to eq("encumbrance_computed")
    Royalties::BatchService.delete_existing_batches
  end

  it "updates encumbrance as zero for users without encumbrances" do
    described_class.run("batch_id")

    temp_person_royalty = TempPersonRoyalty.find_by(person_id: person_ids.first)

    expect(Float(temp_person_royalty.encumbrance_amount_in_usd)).to eq 0
    expect(Float(temp_person_royalty.encumbrance_percentage)).to eq 0
  end

  it "idempotency check: should not run for already processed records" do
    already_processed_person_ids = [persons.first.id, persons.last.id]

    TempPersonRoyalty.where(person_id: already_processed_person_ids).update(
      encumbrance_amount_in_usd: 10,
      encumbrance_percentage: 45
    )

    described_class.run("batch_id")

    already_processed_person_ids.each do |person_id|
      temp_person_royalty = TempPersonRoyalty.find_by(person_id: person_id)

      expect(Float(temp_person_royalty.encumbrance_amount_in_usd)).to eq 10
      expect(Float(temp_person_royalty.encumbrance_percentage)).to eq 45
    end
  end

  describe "users with outstanding encumbrance" do
    before(:each) do
      encumbrance_person_ids = [persons[3].id, persons[4].id]
      encumbrance_person_ids.each do |person_id|
        EncumbranceSummary.where(person_id: person_id).update(
          outstanding_amount: 200
        )
      end
    end

    it "calculates encumbrance" do
      encumbrance_person_ids = [persons[3].id, persons[4].id]
      described_class.run("batch_id")

      encumbrance_person_ids.each do |person_id|
        temp_person_royalty = TempPersonRoyalty.find_by(person_id: person_id)
        expect(Float(temp_person_royalty.encumbrance_amount_in_usd)).to eq -10
        expect(Float(temp_person_royalty.encumbrance_percentage)).to eq 100
      end
    end

    it "add entries to temp encumbrance details" do
      encumbrance_person_ids = [persons[3].id, persons[4].id]
      described_class.run("batch_id")

      encumbrance_person_ids.each do |person_id|
        encumbrance_detail = TempEncumbranceDetail.where(person_id: person_id).first

        expect(Float(encumbrance_detail.transaction_amount)).to eq -10
      end
    end

    it "users with only youtube earnings - do not calculate encumbrance" do
      TempPersonRoyalty.find_by(person_id: persons[3].id).update({
        total_amount_in_usd: nil,
        you_tube_amount_in_usd: 10
      })

      described_class.run("batch_id")

      temp_person_royalty = TempPersonRoyalty.find_by(person_id: persons[3].id)
      expect(temp_person_royalty.encumbrance_amount_in_usd).to be_nil
    end
  end

  it "should schedule the splits worker" do
    expect(Royalties::Splits::ComputeSplitsWorker).to receive(:perform_async).with("batch_id")

    described_class.run("batch_id")
  end
end
