require "rails_helper"

describe Royalties::Posting do
  posting_id = "id123"
  sip_posting_ids = [1,2,3]
  posting = { posting_id: posting_id, state: Royalties::Posting::STARTED, external_posting_ids: sip_posting_ids, source: "symphony"}.to_json
  currency_hash = [
    { "base_currency": "USD",currency: "USD",fx_rate: 2},
    { "base_currency": "USD",currency: "EUR",fx_rate: 2}
  ]

  after(:each) do
    Rails.cache.delete(Royalties::Posting::POSTING_KEY)
  end

  it "gets saved in Rails cache to access it throughout the posting process" do
    expect(Rails.cache).to receive(:write).with(Royalties::Posting::POSTING_KEY, posting)

    Royalties::Posting.add(posting_id, sip_posting_ids, "symphony")
  end

  describe "A posting is stored in Rails cache" do
    before(:each) do
      Rails.cache.write(Royalties::Posting::POSTING_KEY, posting)
    end

    it "gets posting details from Rails cache" do
      details_from_rails_cache = Royalties::Posting.get_details

      expect(details_from_rails_cache["posting_id"]).to eq posting_id
      expect(details_from_rails_cache["state"]).to eq Royalties::Posting::STARTED
      expect(details_from_rails_cache["external_posting_ids"]).to eq sip_posting_ids
    end

    it "updates posting state" do
      state = Royalties::Posting::CALCULATIONS_COMPLETED

      Royalties::Posting.update_state(state)

      expect(Royalties::Posting.get_details["state"]). to eq state
    end

    it "gets posting id from rails cache" do
      expect(Royalties::Posting.get_id).to eq posting_id
    end

    it "gets posting state from rails cache" do
      expect(Royalties::Posting.get_state).to eq Royalties::Posting::STARTED
    end

    it "gets sip posting_ids from rails cache" do
      expect(Royalties::Posting.get_external_posting_ids).to eq sip_posting_ids
    end

    it "gets sip source from rails cache" do
      expect(Royalties::Posting.get_source).to eq "symphony"
    end

    it "deletes posting and currency from rails cache" do
      Royalties::Posting.remove

      expect(Royalties::Posting.get_id). to be_nil
    end
  end

  describe "currency hash" do
    it "stores currency hash in rails cache" do
      expect(Rails.cache).to receive(:write) do |key, hash|
        expect(key).to eq Royalties::Posting::CURRENCY_HASH_KEY
        expect(JSON.parse(hash).count).to eq currency_hash.count
      end

      Royalties::Posting.add_currency_hash(currency_hash)
    end

    describe "currency hash is stored in rails cache" do
      before(:each) do
        Royalties::Posting.add_currency_hash(currency_hash)
      end

      it "fetches fx_rate for existing currency" do
        expect(Royalties::Posting.get_fx_rate_for("USD")).to be 2.to_f
      end

      it "returns default fx_rate for non-existent currency" do
        expect(Royalties::Posting.get_fx_rate_for("AUD")).to eq 1
      end
    end
  end
end
