require "rails_helper"

describe Royalties::ComputePersonRoyaltiesService do
  let(:persons) { create_list(:person, 10) }
  let(:person_ids) { persons.pluck(:id) }
  let(:related) { create(:song) }

  before(:each) do
    stub_const("Royalties::ComputePersonRoyaltiesService::BATCH_SIZE", 3)
    key = "ROYALTIES_BATCH_batch_id"

    Rails.cache.delete(key)

    batch = { person_ids: person_ids, state: Royalties::BatchService::BATCH_CREATED }.to_json

    Rails.cache.write(key, batch)
  end

  after(:each) do
    key = "ROYALTIES_BATCH_batch_id"
    batch_details = JSON.parse(Rails.cache.fetch(key))
    expect(batch_details["state"]).to eq("person_royalties_computed")
    Rails.cache.delete(key)
  end

  context "user has only sales records new" do
    before(:each) do
      person_ids.each do |person_id|
        create(:temp_person_royalty, person_id: person_id)
        create(:sales_record_new, person_id: person_id, sales_record_master_id: person_id, related_id: related.id)
        create(:sales_record_new, person_id: person_id, sales_record_master_id: person_id + 1, related_id: related.id)
      end
    end

    it "compute person royalties for saved batches" do
      described_class.run("batch_id")

      person_ids.each do |person_id|
        temp_person_royalty = TempPersonRoyalty.find_by(person_id: person_id)
        expect(Float(temp_person_royalty.total_amount_in_usd)).to eq 1.4
        expect(temp_person_royalty.you_tube_amount_in_usd).to eq nil
        expect(temp_person_royalty.sales_record_master_ids).to eq([person_id, person_id + 1].join(","))
      end
    end

    it "idempotency check: should not run for already populated records" do
      already_populated_person_id = [persons.first.id, persons.last.id]

      TempPersonRoyalty.where(person_id: already_populated_person_id).update(
        total_amount_in_usd: 10
      )

      described_class.run("batch_id")

      (person_ids - already_populated_person_id).each do |person_id|
        temp_person_royalty = TempPersonRoyalty.find_by(person_id: person_id)

        expect(Float(temp_person_royalty.total_amount_in_usd)).to eq 1.4
      end

      already_populated_person_id.each do |person_id|
        temp_person_royalty = TempPersonRoyalty.find_by(person_id: person_id)

        expect(Float(temp_person_royalty.total_amount_in_usd)).to eq 10
      end
    end
  end

  context "user has only youtube royalty records new" do
    before(:each) do
      person_ids.each do |person_id|
        create(:temp_person_royalty, person_id: person_id)
        create(:you_tube_royalty_record_new, person_id: person_id, song_id: related.id)
      end
    end

    it "compute person royalties for saved batches" do
      described_class.run("batch_id")

      person_ids.each do |person_id|
        temp_person_royalty = TempPersonRoyalty.find_by(person_id: person_id)

        expect(temp_person_royalty.person_id).to be person_id
        expect(temp_person_royalty.total_amount_in_usd).to be_nil
        expect(Float(temp_person_royalty.you_tube_amount_in_usd)).to eq 40.0
      end
    end
  end

  context "user has both youtube royalty records new and sales records new" do
    before(:each) do
      person_ids.each do |person_id|
        create(:temp_person_royalty, person_id: person_id)
        create(:sales_record_new, person_id: person_id, sales_record_master_id: 1, related_id: related.id)
        create(:you_tube_royalty_record_new, person_id: person_id, song_id: related.id)
      end
    end

    it "compute person royalties for saved batches" do
      described_class.run("batch_id")

      person_ids.each do |person_id|
        temp_person_royalty = TempPersonRoyalty.find_by(person_id: person_id)

        expect(Float(temp_person_royalty.total_amount_in_usd)).to eq 0.7
        expect(Float(temp_person_royalty.you_tube_amount_in_usd)).to eq 40.0
      end
    end
  end

  it "schedules the encumbrance worker for the batch" do
    expect(Royalties::ComputeEncumbranceWorker).to receive(:perform_async).with("batch_id")

    described_class.run("batch_id")
  end
end
