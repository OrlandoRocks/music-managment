require "rails_helper"

describe Royalties::BatchService do
  batch_id = "id123"
  key = Royalties::BatchService::BATCH_PREFIX + '_' + batch_id
  person_ids = ["1", "2", "3"]
  batch = { person_ids: person_ids, state: Royalties::BatchService::BATCH_CREATED }.to_json

  after(:each) do
    Rails.cache.delete_matched("#{Royalties::BatchService::BATCH_PREFIX}_*")
  end

  it "add batch details to rails cache" do
    expect(Rails.cache).to receive(:write).with(key, batch)

    Royalties::BatchService.add_to_batch(batch_id, person_ids)
  end

  describe "a batch is stored in rails cache" do
    before(:each) do
      Rails.cache.write(key, batch)
    end

    it "updates batch state" do
      state = Royalties::BatchService::PERSON_ROYALTIES_COMPUTED

      Royalties::BatchService.update_batch_state(batch_id, state)

      expect(Royalties::BatchService.get_batch_details(batch_id)["state"]). to eq state
    end

    it "gets person ids asociated with the batch" do
      expect(Royalties::BatchService.get_batch_person_ids(batch_id)).to eq person_ids
    end

    it "gets batch state from rails cache" do
      expect(Royalties::BatchService.get_batch_state(batch_id)).to eq Royalties::BatchService::BATCH_CREATED
    end
  end

  describe "a few batches are stored in rails cache" do
    batch_id2 = "id321"
    key2 = Royalties::BatchService::BATCH_PREFIX + '_' + batch_id2
    person_ids2 = ["3_1", "2_1"]
    batch2 = { person_ids: person_ids2, state: Royalties::BatchService::PERSON_ROYALTIES_COMPUTED }.to_json
    before(:each) do
      Rails.cache.write(key, batch)
      Rails.cache.write(key2, batch2)
    end

    it "gets unique batch states" do
      batch_states = Royalties::BatchService.get_uniq_batch_states

      expect(batch_states).to contain_exactly(Royalties::BatchService::PERSON_ROYALTIES_COMPUTED, Royalties::BatchService::BATCH_CREATED)
    end

    it "gets sorted bacth ids" do
      batch_ids_from_rails_cache = Royalties::BatchService.get_all_batch_ids

      expect(batch_ids_from_rails_cache[0]).to eq batch_id2
      expect(batch_ids_from_rails_cache[1]).to eq batch_id
    end
  end
end