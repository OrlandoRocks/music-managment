require "rails_helper"

describe Royalties::UpdateSalesRecordMastersService do
  let(:sales_record_masters) { create_list(:sales_record_master, 10) }

  before do
    Rails.cache.write(
      "ROYALTIES_POSTING",
      {
        posting_id: "posting_id",
        state: "started"
      }.to_json
    )
  end

  after do
    Rails.cache.delete("ROYALTIES_POSTING")
  end

  it "marks sales record masters as summarized and posted" do
    sales_record_masters.each do |sales_record_master|
      create(:sales_record_master_new, id: sales_record_master.id)
    end

    described_class.run
    expect(
      SalesRecordMaster.where(
        id: sales_record_masters.pluck(:id),
        status: "posted",
        summarized: 1
      ).length
    ).to eq(sales_record_masters.length)

    expect(JSON.parse(Rails.cache.fetch("ROYALTIES_POSTING"))["state"]).to eq("sales_record_masters_updated")
  end
end
