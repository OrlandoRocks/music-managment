require "rails_helper"

describe Royalties::NotificationService do
  posting_id = "posting_id"
  sip_posting_ids = [12, 34]
  let(:s3)         { double("s3") }
  let(:bucket)    { double("buckets") }
  let(:object)    { double("objects") }

  before(:each) do
    allow(ENV).to receive(:fetch).with("SLACK_WEBHOOK_SIP_DEV_CHANNEL", nil).and_return("")
    allow(ENV).to receive(:fetch).with("SYMPHONY_CSV_BUCKET").and_return("tunecore.symphony.com")
    allow(ENV).to receive(:fetch).with("DEPLOY_ENV").and_return("test")
    allow(Aws::S3::Resource).to receive(:new).and_return(s3)
    allow(s3).to receive(:bucket).and_return(bucket)
    allow(bucket).to receive(:object).and_return(object)
    allow(object).to receive(:put)
  end

  describe "Posting is in started state" do
    before(:each) do
      Royalties::Posting.add(posting_id)
    end

    after(:each) do
      Royalties::Posting.remove
    end

    it "will not update state" do
      described_class.run

      expect(Royalties::Posting.get_state).to eq Royalties::Posting::STARTED
    end
  end

  describe "Posting is in calculations completed state" do
    let(:persons) { create_list(:person, 10, { country: "US" }) }
    let(:person_ids) { persons.pluck(:id) }

    before(:each) do
      Royalties::Posting.add(posting_id, sip_posting_ids)
      Royalties::Posting.update_state(Royalties::Posting::CALCULATIONS_COMPLETED)

      persons.each do |person|
        create(:temp_person_royalty,
          person_id: person.id,
          total_amount_in_usd: 10,
          you_tube_amount_in_usd: 4,
          encumbrance_amount_in_usd: 5
        )
      end

      TempPersonRoyalty.where(person_id: person_ids.first).update(user_currency: "CAD")
    end

    after(:each) do
      Royalties::Posting.remove
    end

    it "update posting state" do
      described_class.run

      expect(Royalties::Posting.get_state).to eq Royalties::Posting::FINANCE_APPROVAL_REQUESTED
    end

    context "Split details are not present" do
      it "populate posting royalties" do
        described_class.run

        posting_royalty = PostingRoyalty.where(currency: 'USD').first
        expect(posting_royalty.posting_id).to eq posting_id
        expect(posting_royalty.total_amount_in_usd).to eq 90
        expect(posting_royalty.total_you_tube_amount_in_usd).to eq 36
        expect(posting_royalty.encumbrance_amount_in_usd).to eq 45
        expect(posting_royalty.split_adjustments_in_usd).to eq 0
        expect(posting_royalty.state).to eq PostingRoyalty::PENDING

        posting_royalty = PostingRoyalty.where(currency: 'CAD').first
        expect(posting_royalty.total_amount_in_usd).to eq 10
        expect(posting_royalty.encumbrance_amount_in_usd).to eq 5
        expect(posting_royalty.total_you_tube_amount_in_usd).to eq 4
        expect(posting_royalty.split_adjustments_in_usd).to eq 0
      end
    end

    context "Split details present" do

      before(:each) do
        logged_in_user = persons.first
        royalty_split = royalty_split_with_recipients(owner: logged_in_user)
        albums = create_list(:album, 2, :with_uploaded_songs, person: logged_in_user)
        songs = create_list(:song, 3, album: albums.sample)

        create(:temp_royalty_split_detail,
          royalty_split_id: royalty_split.id,
          song_id: songs.first.id,
          person_id: logged_in_user.id,
          owner_id: logged_in_user.id
        )
        create(:temp_royalty_split_detail,
          royalty_split_id: royalty_split.id,
          song_id: songs.first.id,
          person_id: logged_in_user.id,
          transaction_amount_in_usd: 2,
          currency: 'CAD',
          owner_id: logged_in_user.id
        )
      end

      it "populate posting royalties" do
        described_class.run

        posting_royalty = PostingRoyalty.where(currency: 'USD').first
        expect(posting_royalty.posting_id).to eq posting_id
        expect(posting_royalty.total_amount_in_usd).to eq 90
        expect(posting_royalty.total_you_tube_amount_in_usd).to eq 36
        expect(posting_royalty.encumbrance_amount_in_usd).to eq 45
        expect(posting_royalty.split_adjustments_in_usd).to eq 0.134e1
        expect(posting_royalty.state).to eq PostingRoyalty::PENDING

        posting_royalty = PostingRoyalty.where(currency: 'CAD').first
        expect(posting_royalty.total_amount_in_usd).to eq 10
        expect(posting_royalty.encumbrance_amount_in_usd).to eq 5
        expect(posting_royalty.total_you_tube_amount_in_usd).to eq 4
        expect(posting_royalty.split_adjustments_in_usd).to eq 2
      end
    end

    it "populates posting mapping" do
      described_class.run

      mappings = PostingMapping.where(posting_id: posting_id)
      expect(mappings.count).to eq 2
      expect(mappings.pluck(:external_posting_id)).to include(sip_posting_ids.first)
      expect(mappings.pluck(:external_posting_id)).to include(sip_posting_ids.last)
    end
  end
end
