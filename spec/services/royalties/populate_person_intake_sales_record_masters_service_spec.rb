require "rails_helper"

describe Royalties::PopulatePersonIntakeSalesRecordMastersService do
  let(:persons) { create_list(:person, 10) }
  let(:person_intakes) { create_list(:person_intake, 10) }
  let(:sales_record_masters) { create_list(:sales_record_master, 20) }
  let(:person_ids) { persons.pluck(:id) }

  before(:each) do
    Rails.cache.write(
      "ROYALTIES_POSTING",
      {
        posting_id: "posting_id",
        state: "started"
      }.to_json
    )

    stub_const("Royalties::ComputePersonRoyaltiesService::BATCH_SIZE", 3)
    key = "ROYALTIES_BATCH_batch_id"

    Rails.cache.delete(key)

    batch = { person_ids: person_ids, state: Royalties::BatchService::BATCH_CREATED }.to_json

    Rails.cache.write(key, batch)

    person_ids.each_with_index do |person_id, i|
      create(
        :temp_person_royalty, person_id: person_id, person_intake_id: person_intakes[i].id,
                              sales_record_master_ids: (
                                [sales_record_masters[i].id] + [sales_record_masters[i + 10].id]
                              ).join(",")
      )
    end
  end

  after(:each) do
    key = "ROYALTIES_BATCH_batch_id"
    batch_details = JSON.parse(Rails.cache.fetch(key))
    expect(batch_details["state"]).to eq("person_intake_sales_record_masters_populated")
    Rails.cache.delete(key)
    Rails.cache.delete("ROYALTIES_POSTING")
  end

  it "adds to person intakes sales record masters" do
    described_class.run
    (0..person_intakes.length - 1).each do |i|
      expect(
        PersonIntakeSalesRecordMaster.where(
          person_intake_id: person_intakes[i].id
        ).pluck(:sales_record_master_id)
      ).to eq(([sales_record_masters[i].id] + [sales_record_masters[i + 10].id]))
    end
  end

  it "skip if record already exists" do
    create(
      :person_intake_sales_record_master,
      person_intake_id: person_intakes[0].id,
      sales_record_master_id: sales_record_masters[0].id
    )

    described_class.run

    (0..person_intakes.length - 1).each do |i|
      expect(
        PersonIntakeSalesRecordMaster.where(
          person_intake_id: person_intakes[i].id
        ).pluck(:sales_record_master_id)
      ).to eq(([sales_record_masters[i].id] + [sales_record_masters[i + 10].id]))
    end
  end

  describe "users earned only via you tube" do
    before(:each) do
      TempPersonRoyalty.delete_all
      person = create(:person)
      create(
        :temp_person_royalty,
        person_id: person.id,
        person_intake_id: nil,
        sales_record_master_ids: nil,
        you_tube_royalty_intake_id: 1234
      )
    end

    it "skip if person does not have person intake id" do
      prev_count = PersonIntakeSalesRecordMaster.count

      described_class.run

      expect(PersonIntakeSalesRecordMaster.count).to eq prev_count
    end
  end


end
