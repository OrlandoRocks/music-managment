require "rails_helper"

describe Royalties::CurrencyConversionService do
  let(:persons) { create_list(:person, 2) }

  before do
    key = "ROYALTIES_BATCH_batch_id"
    Rails.cache.delete(key)
    batch = { person_ids: persons.pluck(:id), state: Royalties::BatchService::SPLITS_COMPUTED }.to_json
    Rails.cache.write(key, batch)

    Rails.cache.write(
      "ROYALTIES_CURRENCY_HASH", [
        {
          currency: "USD",
          fx_rate: 2,
          coverage_rate: 0
        },
        {
          currency: "AUD",
          fx_rate: 3,
          coverage_rate: 0
        }
      ].to_json
    )
  end

  it "update amounts in user currency in temp person royalty" do
    temp_person_royalty1 = create(
      :temp_person_royalty,
      person_id: persons[0].id,
      user_currency: "USD",
      total_amount_in_usd: 10.0,
      you_tube_amount_in_usd: 5.0,
      encumbrance_amount_in_usd: 2.0
    )
    temp_person_royalty2 = create(
      :temp_person_royalty,
      person_id: persons[1].id,
      user_currency: "AUD",
      total_amount_in_usd: 10.0,
      you_tube_amount_in_usd: 5.0,
      encumbrance_amount_in_usd: 2.0
    )

    described_class.run("batch_id")

    temp_person_royalty1.reload
    temp_person_royalty2.reload

    expect(Float(temp_person_royalty1.total_amount_in_user_currency)).to eq(20.0)
    expect(Float(temp_person_royalty1.you_tube_amount_in_user_currency)).to eq(10.0)
    expect(Float(temp_person_royalty1.encumbrance_amount_in_user_currency)).to eq(4.0)
    expect(Float(temp_person_royalty1.fx_rate)).to eq(2.0)
    expect(Float(temp_person_royalty2.total_amount_in_user_currency)).to eq(30.0)
    expect(Float(temp_person_royalty2.you_tube_amount_in_user_currency)).to eq(15.0)
    expect(Float(temp_person_royalty2.encumbrance_amount_in_user_currency)).to eq(6.0)
    expect(Float(temp_person_royalty2.fx_rate)).to eq(3.0)

    expect(JSON.parse(Rails.cache.fetch("ROYALTIES_BATCH_batch_id"))["state"] == "currency_conversion_completed")
  end

  it "does not run upsert_all if record is already updated" do
    create(
      :temp_person_royalty,
      person_id: persons[0].id,
      fx_rate: 2.0
    )
    create(
      :temp_person_royalty,
      person_id: persons[1].id,
      fx_rate: 1.0
    )

    expect(TempPersonRoyalty).to_not receive(:upsert_all)
    described_class.run("batch_id")
  end

  it "schedules the person intake worker for the batch" do
    expect(Royalties::YoutubeIntakeWorker).to receive(:perform_async).with("batch_id")

    described_class.run("batch_id")
  end
end
