require "rails_helper"

describe Royalties::PostFinanceApprovalService do
  posting_id = "posting_id"
  hash =
    [
      {"base_currency" => 'USD', "currency" => 'AUD', "fx_rate" => 0.5, "coverage_rate" => 1},
      {"base_currency" => 'USD', "currency" => 'EUR', "fx_rate" => 2, "coverage_rate" => 0},
      {"base_currency" => 'USD', "currency" => 'USD', "fx_rate" => 2, "coverage_rate" => 0}
    ]

  describe "Posting is in started state" do
    before(:each) do
      Royalties::Posting.add(posting_id)
    end

    after(:each) do
      Royalties::Posting.remove
    end

    it "will not be valid" do
      expect(described_class.new(hash).valid?).to be false
    end
  end

  describe "Posting is in finance approval requested state" do

    before(:each) do
      Royalties::Posting.add(posting_id)
      Royalties::Posting.update_state(Royalties::Posting::FINANCE_APPROVAL_REQUESTED)

      ["USD", "AUD", "EUR"].each do |c|
        create(:posting_royalty, currency: c)
      end
    end

    after(:each) do
      Royalties::Posting.remove
    end

    describe "rates hash contains all currencies" do
      it "will be valid" do
        expect(described_class.new(hash).valid?).to be true
      end

      it "will update posting state" do
        described_class.new(hash).process

        expect(Royalties::Posting.get_state).to eq("finance_approved")
      end

      it "will convert amounts in Posting Royalties" do
        PostingRoyalty.update_all(
          posting_id: posting_id,
          total_amount_in_usd: 100,
          total_you_tube_amount_in_usd: 3,
          encumbrance_amount_in_usd: -2,
          split_adjustments_in_usd: 0,
          currency: 'USD'
        )

        described_class.new(hash).process

        PostingRoyalty.all.each do |royalty|
          expect(royalty.base_fx_rate).to eq 2
          expect(royalty.fx_coverage_rate).to eq 0
          expect(royalty.fx_rate).to eq 2
          expect(royalty.converted_amount).to eq 202.to_f
          expect(royalty.state).to eq 'approved'
          expect(royalty.approved_by).to eq 'rake_user'
        end
      end

      it "schedules the copy sales record masters worker for the batch" do
        expect(Royalties::CopySalesRecordMastersWorker).to receive(:perform_async)

        described_class.new(hash).process
      end
    end


    describe "rates hash does not have all currencies in the posting" do
      it "will not be valid" do
        incomplete_hash = [{"base_currency" => 'USD', "currency" => 'CAD', "fx_rate" => 0.5, "coverage_rate" => 1}]

        expect(described_class.new(incomplete_hash).valid?).to be false
      end
    end
  end
end
