require 'rails_helper'

describe Braintree::ConfigGatewayService do
  describe "#initialize" do
    it "instantiates the gateway according to the config passed in" do
      config = PayinProviderConfig.first
      expect(Braintree::Gateway).to receive(:new).with(environment: :sandbox,
                                                       merchant_id: config.merchant_id,
                                                       public_key: config.public_key,
                                                       private_key: config.decrypted_private_key,)
      Braintree::ConfigGatewayService.new(config)
    end
    context "values of config are empty" do
      it "adds descriptive errors when there are inappropriate values" do
        config = OpenStruct.new(merchant_id: "", public_key: "", decrypted_private_key: "")
        service = Braintree::ConfigGatewayService.new(config)
        expected_errors = [
          "merchant_id but be present",
          "public key must be present",
          "decrypted private key must be present"
        ]
        expect(service.errors[:config]).to eq(expected_errors)
      end
    end
    context "config is missing appropriate keys" do
      it "adds descriptive errors when there are inappropriate values" do
        real_config = PayinProviderConfig.first
        fake_config = OpenStruct.new(merchant_id: real_config.merchant_id, public_key: real_config.public_key)
        service = Braintree::ConfigGatewayService.new(fake_config)
        expect(service.errors[:config]).to eq(["decrypted private key must be present"])
      end
    end
  end
end
