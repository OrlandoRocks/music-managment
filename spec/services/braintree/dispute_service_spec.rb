require 'rails_helper'

describe Braintree::DisputeService do
  let(:dispute) { create(:dispute) }
  it "should accept dispute" do
    allow_any_instance_of(Braintree::DisputeService).to receive(:gateway).and_return(
      gateway
    )
    result=Braintree::DisputeService.new(dispute).accept
    expect(result.is_a?(Hash)).to eq(true)
  end

  it "should upload file evidence" do
    allow_any_instance_of(Braintree::DisputeService).to receive(:gateway).and_return(
      gateway
    )
    evidence_file = File.open(File.join(Rails.root, "spec/files/albumcover-undersize.jpg"))
    result=Braintree::DisputeService.new(dispute).upload_document(evidence_file)
    expect(result.is_a?(Hash)).to eq(true)
  end

  it "should upload text evidence" do
    allow_any_instance_of(Braintree::DisputeService).to receive(:gateway).and_return(
      gateway
    )
    evidence_text = 'sample text'
    result=Braintree::DisputeService.new(dispute).attach_text_evidence(evidence_text)
    expect(result.is_a?(Hash)).to eq(true)
  end

  it "should finalise dispute" do
    allow_any_instance_of(Braintree::DisputeService).to receive(:gateway).and_return(
      gateway
    )
    result=Braintree::DisputeService.new(dispute).finalize
    expect(result.is_a?(Hash)).to eq(true)
  end

  def gateway
    config = PayinProviderConfig.first
    Braintree::ConfigGatewayService.new(config).gateway
  end
end
