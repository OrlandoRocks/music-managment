require 'rails_helper'

describe Braintree::AccountUpdaterService do

  it 'should update the credit card details for "New expiry date" type update' do
    stored_credit_card = create(:stored_credit_card, bt_token: 'dn5jg62')

    expect {
      account_updater_service = Braintree::AccountUpdaterService.new
      account_updater_service.process("dn5jg62", "New expiry date", "5568", "Sep-19")
    }.to change{BraintreeAccountUpdaterLog.count}.by(1)

    stored_credit_card.reload
    expect(stored_credit_card.last_four).to eq("5568")
    expect(stored_credit_card.expiration_month).to eq(9)
    expect(stored_credit_card.expiration_year).to eq(2019)

    log = BraintreeAccountUpdaterLog.last
    expect(log.token).to eq("dn5jg62")
    expect(log.update_type).to eq("New expiry date")
    expect(log.cc_last_four_digits).to eq("5568")
    expect(log.expiration_date).to eq("Sep-19")
    expect(log.status).to eq("success")
    expect(log.message).to eq("Credit card details updated")
  end

  it 'should update the credit card details for "New account number" type update' do
    stored_credit_card = create(:stored_credit_card, bt_token: 'dn5jg62')

    expect {
      account_updater_service = Braintree::AccountUpdaterService.new
      account_updater_service.process("dn5jg62", "New account number", "5568", "Nov-21")
    }.to change{BraintreeAccountUpdaterLog.count}.by(1)

    stored_credit_card.reload
    expect(stored_credit_card.last_four).to eq("5568")
    expect(stored_credit_card.expiration_month).to eq(11)
    expect(stored_credit_card.expiration_year).to eq(2021)

    log = BraintreeAccountUpdaterLog.last
    expect(log.token).to eq("dn5jg62")
    expect(log.update_type).to eq("New account number")
    expect(log.cc_last_four_digits).to eq("5568")
    expect(log.expiration_date).to eq("Nov-21")
    expect(log.status).to eq('success')
    expect(log.message).to eq("Credit card details updated")
  end

  it 'should update the credit card details even for the expiration_date is in mm/yy format' do
    stored_credit_card = create(:stored_credit_card, bt_token: 'dn5jg62')

    expect {
      account_updater_service = Braintree::AccountUpdaterService.new
      account_updater_service.process("dn5jg62", "New account number", "5568", "11/21")
    }.to change{BraintreeAccountUpdaterLog.count}.by(1)

    stored_credit_card.reload
    expect(stored_credit_card.last_four).to eq("5568")
    expect(stored_credit_card.expiration_month).to eq(11)
    expect(stored_credit_card.expiration_year).to eq(2021)

    log = BraintreeAccountUpdaterLog.last
    expect(log.token).to eq("dn5jg62")
    expect(log.update_type).to eq("New account number")
    expect(log.cc_last_four_digits).to eq("5568")
    expect(log.expiration_date).to eq("11/21")
    expect(log.status).to eq('success')
    expect(log.message).to eq("Credit card details updated")
  end


  it 'should return error object if no record found for given token' do
    expect {
      account_updater_service = Braintree::AccountUpdaterService.new
      account_updater_service.process("dn5jg62", "New account number", "5568", "Nov-21")
    }.to change{BraintreeAccountUpdaterLog.count}.by(1)

    log = BraintreeAccountUpdaterLog.last
    expect(log.token).to eq("dn5jg62")
    expect(log.update_type).to eq("New account number")
    expect(log.cc_last_four_digits).to eq("5568")
    expect(log.expiration_date).to eq("Nov-21")
    expect(log.status).to eq('error')
    expect(log.message).to eq("Stored credit card record not found")
  end

  it 'should return error object if unexpected Account Updater Event Description is encountered' do
    create(:stored_credit_card, bt_token: 'dn5jg62')
    expect {
      account_updater_service = Braintree::AccountUpdaterService.new
      account_updater_service.process("dn5jg62", "Call customer - Account Closed", "5568", "Nov-21")
    }.to change{BraintreeAccountUpdaterLog.count}.by(1)

    log = BraintreeAccountUpdaterLog.last
    expect(log.token).to eq("dn5jg62")
    expect(log.update_type).to eq("Call customer - Account Closed")
    expect(log.cc_last_four_digits).to eq("5568")
    expect(log.expiration_date).to eq("Nov-21")
    expect(log.status).to eq('error')
    expect(log.message).to eq("Not configured to process this update_type")
  end

end