require "rails_helper"

describe Braintree::ThreeDSecureInfoUpdateService do
  describe "process" do
    it "should update 3D Secure info after a successful 3D secure transaction" do
      transaction = double(
        "braintree blue transaction",
      )
      three_d_secure_info = {
        enrolled: "Y",
        liability_shifted: true,
        liability_shift_possible: true,
        status: "authenticate_successful",
        ds_transaction_id: SecureRandom.hex
      }

      transaction_info = Braintree::Transaction._new(
        :gateway,
        three_d_secure_info: three_d_secure_info,
      )

      transaction_id = SecureRandom.hex
      allow(transaction).to receive(:id).and_return(transaction_id)
      braintree_successful_result = Braintree::SuccessfulResult.new
      allow(braintree_successful_result).to receive(:transaction).and_return(transaction)
      braintree_transaction = build(:braintree_transaction)
      braintree_transaction.save

      allow_any_instance_of(Braintree::ConfigGatewayService).to receive(:find_transaction).and_return(braintree_successful_result.transaction)
      allow(braintree_successful_result.transaction).to receive(:three_d_secure_info).and_return(transaction_info.three_d_secure_info)

      three_d_secure_info_update_service = Braintree::ThreeDSecureInfoUpdateService.new
      three_d_secure_info_update_service.process(braintree_transaction.id)

      braintree_transaction.reload
      expect(braintree_transaction.liability_shifted).to eq(three_d_secure_info[:liability_shifted])
      expect(braintree_transaction.liability_shifted_at).to_not be_nil
      expect(braintree_transaction.ds_transaction_id).to eq(three_d_secure_info[:ds_transaction_id])
    end
  end
end
