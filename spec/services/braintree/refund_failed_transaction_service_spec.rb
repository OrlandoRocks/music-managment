require 'rails_helper'

describe Braintree::RefundFailedTransactionService do
  let!(:braintree_transaction) { create(:braintree_transaction) }

  it "should refund payment if transaction was successful" do
    braintree_transaction.update(response_code: "1000", status: "success", amount: 0.51e1)
    refund_reason = "Rollback after error processing charge"
    allow(FeatureFlipper).to receive(:show_feature?).and_call_original
    allow(FeatureFlipper).to receive(:show_feature?).with(:autorefund_failed_braintree_transactions, braintree_transaction.person).and_return(true)

    expect(BraintreeTransaction).to receive(:process_refund).with(braintree_transaction.id, braintree_transaction.amount, refund_reason: refund_reason)
    Braintree::RefundFailedTransactionService.refund_if_charged_and_failed(braintree_transaction)
  end

  it "should not refund a declined transaction" do
    braintree_transaction.update(response_code: "2003", status: "declined", amount: 0.51e1)
    refund_reason = "Rollback after error processing charge"
    allow(FeatureFlipper).to receive(:show_feature?).with(:autorefund_failed_braintree_transactions, braintree_transaction.person).and_return(true)

    expect(BraintreeTransaction).not_to receive(:process_refund)
    Braintree::RefundFailedTransactionService.refund_if_charged_and_failed(braintree_transaction)
  end

  it "should not refund transaction if feature flipper is off" do
    braintree_transaction.update(response_code: "1000", status: "success", amount: 0.51e1)
    refund_reason = "Rollback after error processing charge"
    allow(FeatureFlipper).to receive(:show_feature?).with(:autorefund_failed_braintree_transactions, braintree_transaction.person).and_return(false)

    expect(BraintreeTransaction).not_to receive(:process_refund)
    Braintree::RefundFailedTransactionService.refund_if_charged_and_failed(braintree_transaction)
  end
end
