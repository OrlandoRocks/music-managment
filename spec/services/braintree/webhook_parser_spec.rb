require "rails_helper"

RSpec.describe Braintree::WebhookParser, type: :service do
  let(:payin_provider_config) do
    PayinProviderConfig.find_by(name: PayinProviderConfig::BRAINTREE_NAME)
  end

  let(:gateway) do
    Braintree::ConfigGatewayService.new(payin_provider_config).gateway
  end

  describe "#parse!" do
    context "success" do
      it "parses braintree webhook" do
        webhook_notification = gateway.webhook_testing.sample_notification(
          Braintree::WebhookNotification::Kind::SubscriptionWentPastDue,
          "my_id"
        )

        parsed_response = Braintree::WebhookParser.new.parse!(
          webhook_notification[:bt_signature], webhook_notification[:bt_payload]
        )

        expect(parsed_response.subscription.id).to eq("my_id")
      end
    end

    context "failure" do
      it "raises exception with invalid payload or signature" do
        subject = Braintree::WebhookParser.new
        expect { subject.parse!("wrong", "wrong") }
          .to raise_error(Braintree::InvalidSignature)
      end
    end
  end
end
