require 'rails_helper'

describe Braintree::RemoveCardService do
  let!(:person_1) {create(:person)}
  let!(:bt_card_1) {create(:stored_credit_card, :braintree_card, person: person_1)}
  let!(:payu_card) {create(:stored_credit_card, :payos_card, person: person_1)}
  let!(:person_2) {create(:person)}
  let!(:bt_card_2) {create(:stored_credit_card, :braintree_card, person: person_2)}

  describe "#delete_all_cards" do
    before(:each) do
      create(:braintree_vault_transaction, person: person_1, related: bt_card_1)
      create(:braintree_vault_transaction, person: person_2, related: bt_card_2)
    end

    it "should delete all bt_cards" do
      Braintree::RemoveCardService.process([person_1.id, person_2.id])

      bt_card_1.reload
      bt_card_2.reload
      expect(bt_card_1.deleted_at).not_to be(nil)
      expect(bt_card_2.deleted_at).not_to be(nil)
    end

    it "should not delete payos cards" do
      Braintree::RemoveCardService.process([person_1.id, person_2.id])

      payu_card.reload
      expect(payu_card.deleted_at).to be(nil)
    end
  end
end
