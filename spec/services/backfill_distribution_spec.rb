require "rails_helper"

describe BackfillDistribution do
  let!(:album) { create(:album, :with_uploaded_songs_and_artwork, :with_songwriter) }

  let!(:store_akazoo) { Store.find_by(short_name: "Akazoo") }

  let!(:store_kkbox) { Store.find_by(short_name: "KKBox") }

  let!(:salepoint_akazoo) {
    create(:salepointable_store, store: store_akazoo)
    create(
      :salepoint,
      salepointable: album,
      store_id: store_akazoo.id,
      finalized_at: Time.now
    )
  }

  let!(:salepoint_kkbox) {
    create(:salepointable_store, store: store_kkbox)
    create(
      :salepoint,
      salepointable: album,
      store_id: store_kkbox.id,
      finalized_at: Time.now
    )
  }

  context "with run" do
    it "create distribution if distribution not exist for destination salepoint" do
      petri_bundle = create(:petri_bundle, album: album)
      distro = create(
        :distribution,
        :delivered_distro,
        petri_bundle: petri_bundle,
        converter_class: store_akazoo.converter_class.to_s
      )
      distro.salepoints << salepoint_akazoo
      backfill_distribution_object = BackfillDistribution.new(store_akazoo.id, store_kkbox.id)

      expect {
        backfill_distribution_object.run
      }.to change { Distribution.count }
        .by(1)
    end

    it "do not create distribution if distribution exist for destination salepoint" do
      petri_bundle = create(:petri_bundle, album: album)
      distro = create(
        :distribution,
        :delivered_distro,
        petri_bundle: petri_bundle,
        converter_class: store_kkbox.converter_class.to_s
      )
      distro.salepoints << salepoint_kkbox
      backfill_distribution_object = BackfillDistribution.new(store_akazoo.id, store_kkbox.id)

      expect {
        backfill_distribution_object.run
      }.to change { Distribution.count }
        .by(0)
    end

    it "do not create distribution if distribution dose not exist for source salepoint and destination salepoint" do
      backfill_distribution_object = BackfillDistribution.new(store_akazoo.id, store_kkbox.id)

      expect {
        backfill_distribution_object.run
      }.to change { Distribution.count }
        .by(0)
    end
  end

  context "validate wrong input" do
    it "source_store_id is blank" do
      expect {
        BackfillDistribution.new(nil, store_kkbox.id).run
      }.to raise_error(RuntimeError, "Please provide source_store_id")
    end

    it "destination_store_id is blank" do
      expect {
        BackfillDistribution.new(store_akazoo.id, nil).run
      }.to raise_error(RuntimeError, "Please provide destination_store_id")
    end

    it "source_store_id and destination_store_id are same" do
      expect {
        BackfillDistribution.new(store_kkbox.id, store_kkbox.id).run
      }.to raise_error(RuntimeError, "Source_store_id and destination_store_id should be different")
    end

    it "source_store_id store doesn't exist" do
      not_exist_store_id = Store.last.id + 1
      expect {
        BackfillDistribution.new(not_exist_store_id, store_kkbox.id).run
      }.to raise_error(RuntimeError, "Source store does not exist")
    end

    it "destination_store_id store doesn't exist" do
      not_exist_store_id = Store.last.id + 1
      expect {
        BackfillDistribution.new(store_akazoo.id, not_exist_store_id).run
      }.to raise_error(RuntimeError, "Destination store does not exist")
    end

    it "destination_store_id is blank" do
      expect {
        BackfillDistribution.new(store_akazoo.id, "").run
      }.to raise_error(RuntimeError, "Destination store does not exist")
    end

    it "source_store_id is blank" do
      expect {
        BackfillDistribution.new("", store_kkbox.id).run
      }.to raise_error(RuntimeError, "Source store does not exist")
    end
  end
end
