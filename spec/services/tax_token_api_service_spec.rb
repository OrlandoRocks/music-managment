require "rails_helper"

describe TaxTokenApiService do
  let(:person) { create(:person) }
  let(:tax_token)  { create(:tax_token, tax_form_type: "W2") }
  describe ".update" do
    before do
      allow_any_instance_of(Payoneer::V2::ApiClient).to receive(:post).with({PartnerPayeeID: person.id, mname: "InitializeTaxFormForPayee"})
      allow_any_instance_of(Payoneer::V2::ApiClient).to receive(:post).with({PartnerPayeeID: person.id, mname: "GetPendingTaxFormInvitedPayees", fromDate: Date.yesterday.to_s, toDate: Date.today.to_s}).and_return("12345")
    end
    it "returns the updated tax token" do
      token = TaxTokenApiService.update(person.id, tax_token)
      expect(token.token).to eq("12345")
      expect(token.tax_form_type).to eq(nil)
    end
  end
end
