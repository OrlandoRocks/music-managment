require "rails_helper"

describe MassAdjustmentSchedulerService do
  context "#schedule" do
    it "should schedule sidekiq job for all the unprocessed mass adjustment entries" do
      batch = create(:mass_adjustment_batch, status: MassAdjustmentBatch::APPROVED)
      create(:mass_adjustment_entry, mass_adjustment_batch: batch)
      create(:mass_adjustment_entry, mass_adjustment_batch: batch)
      create(:mass_adjustment_entry, mass_adjustment_batch: batch, status: MassAdjustmentEntry::ERROR)
      expect(MassBalanceAdjustment::BatchEntryWorker).to receive(:perform_async).twice

      MassAdjustmentSchedulerService.schedule(batch)
      expect(batch.reload.status).to eq(MassAdjustmentBatch::SCHEDULED)
    end

    it "should not schedule if the status of the batch is not approved" do
      batch = create(:mass_adjustment_batch, status: MassAdjustmentBatch::DECLINED)
      create(:mass_adjustment_entry, mass_adjustment_batch: batch)
      expect(MassBalanceAdjustment::BatchEntryWorker).not_to receive(:perform_async)

      MassAdjustmentSchedulerService.schedule(batch)
      expect(batch.reload.status).to eq(MassAdjustmentBatch::DECLINED)
    end
  end
end
