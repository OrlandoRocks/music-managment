require "rails_helper"

describe Salepoints::AfterCommitAllowSalepointInBelieveApi do
  describe ".call" do
    let(:salepoint) { create(:salepoint, salepointable: album, store: store) }
    let(:album) { create(:album, :finalized) }
    let(:store) { Store.find_by(abbrev: "ang") } # anghami

    before(:each) do
      allow(Delivery::Believe::ChangeStoreRestrictionWorker)
        .to receive(:perform_async)
        .and_return(true)

      allow_any_instance_of(Believe::ApiClient)
        .to receive(:post)
        .and_return(true)

      create(:salepointable_store, store: store)
    end

    it "should not call change_store_restriction when finalized_at does not change" do
      expect(Delivery::Believe::ChangeStoreRestrictionWorker)
        .not_to receive(:perform_async)

      salepoint.status = "NEW"
      salepoint.save
    end

    context "when legal review state is NOT APPROVED and finalized_at changes" do
      it "should not call change_store_restriction " do
        expect(Delivery::Believe::ChangeStoreRestrictionWorker)
          .not_to receive(:perform_async)

        salepoint.finalized_at = Time.now
        album.update(legal_review_state: "DO NOT REVIEW")

        salepoint.save
      end
    end

    context "when legal review state is APPROVED and finalized_at changes" do
      it "should call change_store_restriction " do
        expect(Delivery::Believe::ChangeStoreRestrictionWorker)
          .to receive(:perform_async)
          .and_return(:true)

        album.legal_review_state = "APPROVED"
        salepoint.finalized_at = Time.now

        salepoint.save
      end
    end
  end
end
