require "rails_helper"

RSpec.describe AutoRefunds::Utils::RefundAmount, type: :service do
  describe "#tax_amount_cents" do
    it "calculates tax_amount in cents for purchases with vat" do
      request_amount = 100
      purchase = create(:purchase, :with_vat_tax)
      tax_inclusive = true
      subject = described_class.new(request_amount, purchase, tax_inclusive, Refund::FIXED_AMOUNT)
      tax_rate = purchase.purchase_tax_information.tax_rate.fdiv(100)

      base_amount_cents = (request_amount * 100).fdiv(1 + tax_rate)
      tax_amount_cents = base_amount_cents * tax_rate

      expect(subject.tax_amount_cents)
        .to eq(tax_amount_cents)
    end

    it "returns 0 for purchases without tax" do
      request_amount = 100
      purchase = create(:purchase, :without_vat_tax)
      tax_inclusive = true
      subject = described_class.new(request_amount, purchase, tax_inclusive, Refund::FIXED_AMOUNT)

      expect(subject.tax_amount_cents)
        .to eq(0)
    end
  end

  describe "#base_amount_cents" do
    context "tax_inclusive refund" do
      it "returns difference between tax_amount_cents and request_amount_cents as base_amount_cents" do
        request_amount = 100
        purchase = create(:purchase, :with_vat_tax)
        tax_inclusive = true
        subject = described_class.new(request_amount, purchase, tax_inclusive, Refund::FIXED_AMOUNT)

        base_amount_cents = request_amount * 100 - subject.tax_amount_cents

        expect(subject.base_amount_cents)
          .to eq(base_amount_cents)
      end
    end

    context "tax_exclusive refund" do
      it "returns request_amount_cents as base_amount_cents" do
        request_amount = 100
        purchase = create(:purchase, :with_vat_tax)
        tax_inclusive = false
        subject = described_class.new(request_amount, purchase, tax_inclusive, Refund::FIXED_AMOUNT)

        expect(subject.base_amount_cents)
          .to eq(request_amount * 100)
      end
    end
  end
end
