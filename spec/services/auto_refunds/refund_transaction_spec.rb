require "rails_helper"

RSpec.describe AutoRefunds::RefundTransaction, type: :service do
  describe "#create_refund_record!" do
    it "creates refund record with given attributes" do
      invoice = create(:invoice, :with_two_purchases, :settled)
      admin = create(:person, :with_role, role_name: Role::REFUNDS)
      refund_reason = create(:refund_reason)

      args = {
        invoice_id: invoice.id,
        refund_level: Refund::INVOICE,
        refund_type: Refund::FULL_REFUND,
        request_amount: 100,
        tax_inclusive: true,
        refund_reason_id: refund_reason.id,
        refunded_by_id: admin.id
      }

      subject = described_class.new(args).create_refund_record!
      invoice.reload

      expect(invoice.refunds).to include(subject)
    end
  end

  describe "#create_refund_items!" do
    context "invoice_level refund" do
      it "creates refund_item for all purchases in the invoice from refund" do
        invoice = create(:invoice, :with_two_purchases, :settled)
        admin = create(:person, :with_role, role_name: Role::REFUNDS)
        refund_reason = create(:refund_reason)

        args = {
          invoice_id: invoice.id,
          refund_level: Refund::INVOICE,
          refund_type: Refund::FULL_REFUND,
          request_amount: 100,
          tax_inclusive: true,
          refund_reason_id: refund_reason.id,
          refunded_by_id: admin.id
        }

        subject = described_class.new(args)
        refund = subject.create_refund_record!
        subject.create_refund_items!
        refund.reload

        expect(invoice.purchase_ids).to eq(refund.refund_items.pluck(:purchase_id))
      end
    end

    context "line_item_level refund" do
      it "creates refund_items for given purchases" do
        invoice = create(:invoice, :with_two_purchases, :settled)
        admin = create(:person, :with_role, role_name: Role::REFUNDS)
        refund_reason = create(:refund_reason)

        args = {
          invoice_id: invoice.id,
          refund_level: Refund::LINE_ITEM,
          refund_reason_id: refund_reason.id,
          refunded_by_id: admin.id,
          tax_inclusive: true,
          refund_items: [
            {
              purchase_id: invoice.purchases.first.id,
              refund_type: Refund::FULL_REFUND,
              request_amount: 10,
            }
          ]
        }

        subject = described_class.new(args)
        refund = subject.create_refund_record!
        subject.create_refund_items!
        refund.reload

        expect(refund.refund_items.count).to eq(1)
        expect(invoice.purchase_ids.first).to eq(refund.refund_items.first.purchase_id)
      end
    end
  end
end
