require 'rails_helper'

RSpec.describe AutoRefunds::TransactionsService, type: :service do
  let!(:person) { create(:person) }
  let!(:invoice_1) { create(:invoice, :settled, person: person) }
  let!(:invoice_2) { create(:invoice, :settled, person: person) }

  describe "#invoices_for_refund" do
    it "should return invoice based on id" do
      params = ActionController::Parameters.new({ id: invoice_1.id })
      invoices = AutoRefunds::TransactionsService.new(params).invoices_for_refund
      expect(invoices.first.id).to eq invoice_1.id
    end

    it "should return invoices based on user email or name" do
      params = ActionController::Parameters.new({ email_or_name: person.email })
      invoices = AutoRefunds::TransactionsService.new(params).invoices_for_refund
      expect(invoices.count).to eq 2
    end
  end
end
