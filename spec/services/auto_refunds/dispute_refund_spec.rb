require 'rails_helper'

RSpec.describe AutoRefunds::DisputeRefund, type: :service do
  describe "#process" do
    let!(:dispute) { create(:dispute, status: "Accepted") }

    before(:each) do
      allow(FeatureFlipper)
        .to receive(:show_feature?)
        .and_call_original

      allow(FeatureFlipper)
        .to receive(:show_feature?)
        .with(:new_refunds, dispute.processed_by)
        .and_return(true)

      allow(FeatureFlipper)
        .to receive(:show_feature?)
        .with(:chargebacks, dispute.processed_by)
        .and_return(true)
    end

    context "refundable" do
      it "creates refund for given dispute" do
        described_class.new(dispute).process
        subject = dispute.refund

        expect(subject).to_not be_nil
        expect(subject.refund_reason.reason).to eql("Chargebacks")
      end
    end

    context "non-refundable" do
      it "creates refund for given dispute" do
        non_refundable_status = DisputeStatus.find_by(status: "Won")
        dispute.update(dispute_status: non_refundable_status)

        described_class.new(dispute).process
        subject = dispute.refund

        expect(subject).to be_nil
      end
    end
  end
end
