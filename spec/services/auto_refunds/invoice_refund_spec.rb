require "rails_helper"

RSpec.describe AutoRefunds::InvoiceRefund, type: :service do
  let(:refund) { create(:refund) }

  describe "#create" do
    it "creates refunds for all purchases in the invoice" do
      request_amount = 100
      refund_type = Refund::FULL_REFUND

      described_class.new(refund, request_amount, refund_type).create!
      refund.reload

      expect(refund.refund_items.count).to eq(refund.invoice.purchases.count)
    end
  end
  context "full_refund" do
    it "refunds full amount of invoice" do
      request_amount = 100
      refund_type = Refund::FULL_REFUND

      described_class.new(refund, request_amount, refund_type).create!
      refund.reload

      expect(refund.total_amount_cents).to eq(refund.invoice.final_settlement_amount_cents)
    end
  end

  context "fixed_amount" do
    it "refunds all purchases in same ratio" do
      request_amount = 10
      refund_type = Refund::FIXED_AMOUNT
      refund_ratio = (request_amount * 100).fdiv(refund.invoice.final_settlement_amount_cents)

      first_purchase_amount = refund.invoice.purchases.first.cost_cents
      second_purchase_amount = refund.invoice.purchases.second.cost_cents

      described_class.new(refund, request_amount, refund_type).create!
      refund.reload

      expect(refund.refund_items.first.base_amount_cents)
        .to eq (first_purchase_amount * refund_ratio).round
      expect(refund.refund_items.second.base_amount_cents)
        .to eq (second_purchase_amount * refund_ratio).round
    end
  end

  context "percentage" do
    it "refunds all purchases in same ratio" do
      request_amount = 100
      refund_type = Refund::PERCENTAGE
      refund_ratio = request_amount.fdiv(100)

      first_purchase_amount = refund.invoice.purchases.first.cost_cents
      second_purchase_amount = refund.invoice.purchases.second.cost_cents

      described_class.new(refund, request_amount, refund_type).create!
      refund.reload

      expect(refund.refund_items.first.base_amount_cents).to eq(first_purchase_amount * refund_ratio)
      expect(refund.refund_items.second.base_amount_cents).to eq(second_purchase_amount * refund_ratio)
    end
  end
end
