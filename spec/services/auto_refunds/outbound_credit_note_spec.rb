require "rails_helper"

RSpec.describe AutoRefunds::OutboundCreditNote, type: :service do
  let(:invoice) {
    create(
      :invoice,
      :with_two_purchases,
      :settled,
      :with_balance_settlement,
      :with_outbound_vat
    )
  }

  let(:refund) { create(:refund, :with_refund_items, :with_balance_settlement) }
  let(:refund_settlement) { refund.refund_settlements.first }

  before(:each) do
    described_class.new(
      refund,
      invoice.invoice_settlements.first,
      refund_settlement
    ).process!
  end

  let(:outbound_refund) { refund.outbound_refund }
  let(:refund_vat_adjustment) { outbound_refund.vat_tax_adjustment }

  describe "outbound_refunds" do
    it "should create outbound_refunds" do
      expect(outbound_refund).to be_present
    end

    it "should match the settlements" do
      settlement = outbound_refund.refund_settlement

      expect(settlement).to eq refund_settlement
    end

    it "user prefix should be same as person prefix" do
      person_prefix = refund.person.cr_outbound_invoice_prefix

      expect(outbound_refund.user_invoice_prefix).to eq person_prefix
    end
  end

  describe "vat_tax_adjustment" do
    it "should create the vat_tax_adjustment for vat debit" do
      expect(refund_vat_adjustment).to be_present
      expect(refund_vat_adjustment).to be_an_instance_of VatTaxAdjustment
    end

    it "tax amout should match " do
      total_amount = refund_settlement.settlement_amount_cents
      tax_rate = 17.0
      base_amount = Integer((total_amount * 100 / (100.0 + tax_rate)).round)
      calculate_tax = Integer((base_amount * tax_rate / 100.0).round)

      expect(refund_vat_adjustment.tax_rate).to eq tax_rate
      expect(refund_vat_adjustment.amount).to eq calculate_tax
    end
  end
end
