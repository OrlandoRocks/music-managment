require "rails_helper"

RSpec.describe AutoRefunds::LineItemRefund, type: :service do
  describe "create!" do
    it "creates refund_item for purchase" do
      purchase_cost_cents = 100_00
      purchase = create(:purchase, cost_cents: purchase_cost_cents)
      refund = create(:refund)
      request_amount = 100
      refund_type = Refund::FIXED_AMOUNT

      subject = described_class
                .new(refund, purchase.id, request_amount, refund_type, refund.tax_inclusive)
                .create!
      purchase.reload

      expect(subject).to be_instance_of(RefundItem)
      expect(purchase.refundable_amount_cents)
        .to eq(purchase_cost_cents - subject.total_amount_cents)
    end
  end
end
