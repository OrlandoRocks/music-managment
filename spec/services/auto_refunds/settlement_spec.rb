require "rails_helper"

RSpec.describe AutoRefunds::Settlement, type: :service do
  let!(:admin) { create(:person, :with_role, role_name: Role::REFUNDS) }
  let(:refund_reason) { create(:refund_reason) }

  context "person transaction settlement" do
    let!(:invoice) { create(:invoice, :with_two_purchases, :settled, :with_balance_settlement) }
    let!(:refund)  { create_refund(invoice) }

    before(:each) do
      described_class.new(refund, service_args(invoice)).settle!
    end

    it "should refund the amount to person balance" do
      expect(refund.refund_settlements.find_by(source_type: "PersonTransaction")).to be_present
      expect(refund.settled?).to be_truthy
    end

    it "settled amount should be same as invoice settlement amount for full refund" do
      transaction = refund.invoice.invoice_settlements.find_by(source_type: "PersonTransaction")
      refund_settlement = refund.refund_settlements.find_by(source_type: "PersonTransaction")

      expect(transaction.settlement_amount_cents).to eq refund_settlement.settlement_amount_cents
    end
  end

  context "braintree settlement" do
    let!(:person)  { create(:person, :with_braintree_blue_stored_credit_card) }
    let!(:invoice) { create(:invoice, :with_two_purchases, :settled, :with_braintree_settlement, person: person) }
    let!(:refund)  { create_refund(invoice) }

    before(:each) do
      refund_transaction = create(:braintree_transaction, :refund)
      allow(BraintreeTransaction).to receive(:process_refund).and_return(refund_transaction)

      described_class.new(refund, service_args(invoice)).settle!
    end

    it "should refund the amount to credit card" do
      expect(refund.settled?).to be_truthy
      expect(refund.refund_settlements.first.source_type).to eq "BraintreeTransaction"
    end

    it "refund amount should not exceed the invoice amount" do
      transaction = refund.invoice.invoice_settlements.find_by(source_type: "BraintreeTransaction")
      refund_settlement = refund.refund_settlements.find_by(source_type: "BraintreeTransaction")

      expect(transaction.settlement_amount_cents).to eq refund_settlement.settlement_amount_cents
    end
  end

  context "paypal settlement" do
    let!(:person)  { create(:person, :with_stored_paypal_account) }
    let!(:invoice) { create(:invoice, :with_two_purchases, :settled, :with_paypal_settlement, person: person) }
    let!(:refund)  { create_refund(invoice) }

    before(:each) do
      refund_transaction = create(:paypal_transaction, :refund_transaction)
      allow(PaypalTransaction).to receive(:process_auto_refund!).and_return(refund_transaction)

      described_class.new(refund, service_args(invoice)).settle!
    end

    it "should refund the amount to paypal account" do
      expect(refund.settled?).to be_truthy
      expect(refund.refund_settlements.first.source_type).to eq "PaypalTransaction"
    end
  end

  context "balance and credit card" do
    let!(:person)  { create(:person, :with_braintree_blue_stored_credit_card) }
    let!(:invoice) { create(:invoice, :with_two_purchases, :settled, :with_balance_and_cc_settlement, person: person) }
    let!(:refund)  { create_refund(invoice) }

    before(:each) do
      refund_transaction = create(:braintree_transaction, :refund, amount: Float(invoice.final_settlement_amount) / 2)
      allow(BraintreeTransaction).to receive(:process_refund).and_return(refund_transaction)

      described_class.new(refund, service_args(invoice)).settle!
    end

    it "should settle the whole amount to balance and credit card" do
      expect(refund.settled?).to be_truthy
      expect(refund.refund_settlements.count).to eq 2
    end

    it "balance refund should match settlement" do
      balance_settlement = invoice.invoice_settlements.find_by(source_type: "PersonTransaction")
      balance_refund = refund.refund_settlements.find_by(source_type: "PersonTransaction")

      expect(balance_settlement.settlement_amount_cents).to eq balance_refund.settlement_amount_cents
    end

    it "amount refunded to credit card must be same as the settled amount" do
      cc_settlement = invoice.invoice_settlements.find_by(source_type: "BraintreeTransaction")
      cc_refund = refund.refund_settlements.find_by(source_type: "BraintreeTransaction")

      expect(cc_settlement.settlement_amount_cents).to eq cc_refund.settlement_amount_cents
    end
  end

  context "disputes" do
    let(:person)  { create(:person, :with_braintree_blue_stored_credit_card) }
    let(:invoice) { create(:invoice, :with_two_purchases, :settled, :with_balance_and_cc_settlement, person: person) }
    let(:refund)  { create_refund(invoice) }
    let(:dispute) { create(:dispute) }

    it "create settlement for dispute" do
      args = service_args(invoice).tap { |r| r[:dispute] = dispute }
      described_class.new(refund, args).settle!

      expect(refund.refund_settlements.count).to eq(1)
      expect(refund.refund_settlements.pluck(:source_id)).to include(dispute.id)
      expect(refund.refund_settlements.pluck(:source_type)).to include(dispute.class_name)
    end
  end

  private

  def service_args(invoice)
    {
      invoice_id: invoice.id,
      refund_level: Refund::INVOICE,
      refund_type: Refund::FULL_REFUND,
      request_amount: 100,
      tax_inclusive: true,
      refund_reason_id: refund_reason.id,
      refunded_by_id: admin.id
    }
  end

  # Create the refunds and refund_items only
  def create_refund(invoice)
    refund_transaction = AutoRefunds::RefundTransaction.new(service_args(invoice))
    refund = refund_transaction.create_refund_record!
    refund_transaction.create_refund_items!

    refund
  end
end
