require "rails_helper"

describe ExternalServiceIds::CreationService do
  let(:album) { create(:album) }
  let(:creatives) { album.creatives }

  apple      = ExternalServiceId::APPLE_SERVICE
  spotify    = ExternalServiceId::SPOTIFY_SERVICE
  new_artist = ExternalServiceId::NEW_ARTIST

  describe "External Service ID handling" do
    let(:param1) do
      {
        "id"      => album.creatives.first.id,
        "role"    => "primary_artist",
        "apple"   => { "identifier" => "xyz" },
        "spotify" => { "identifier" => "abc" }
      }
    end

    let(:create_apple_page) do
      {
        "id"      => album.creatives.first.id,
        "role"    => "primary_artist",
        "apple"   => { "create_page" => true },
        "spotify" => {}
      }
    end

    let(:create_spotify_page) do
      {
        "id"      => album.creatives.first.id,
        "role"    => "primary_artist",
        "apple"   => {},
        "spotify" => { "create_page" => true }
      }
    end

    let(:empty) do
      {
        "id"      => album.creatives.first.id,
        "role"    => "primary_artist",
        "apple"   => {},
        "spotify" => {}
      }
    end

    let(:no_id) do
      {
        "id"      => nil,
        "name"    => creatives.first.name,
        "role"    => "primary_artist",
        "apple"   => { "identifier" => "xyz" },
        "spotify" => { "identifier" => "abc" }
      }
    end

    let(:no_name) do
      {
        "id"      => album.creatives.first.id,
        "name"    => nil,
        "role"    => "primary_artist",
        "apple"   => { "identifier" => "xyz" },
        "spotify" => { "identifier" => "abc" }
      }
    end

    context "when ESID creation is a great success," do
      before do
        allow(Apple::ArtistIdValidator).to receive(:new).and_return(double(valid?: true))
        allow(Spotify::ArtistIdValidator).to receive(:valid?).and_return(true)
      end

      it "creates a record with the apple id" do
        params = [param1]
        creative = album.creatives.first

        expect(Apple::ArtistIdValidator).to receive(:new).with(
          {
            'artist_id'  => creative.artist.id,
            'identifier' => param1[apple]['identifier'],
            'name'       => creative.name
          }
        )

        described_class.create_update_match(creatives, params)
        got = creatives.first.external_service_ids.find { |r| r['service_name'] == apple }

        expect(got).to have_attributes('identifier' => 'xyz')
      end

      it "doesn't attempt to validate ID when missing" do
        params = [no_id]

        described_class.create_update_match(creatives, params)

        expect(Apple::ArtistIdValidator).not_to receive(:new)
      end

      it "creates a record with the spotify id" do
        params = [param1]

        described_class.create_update_match(creatives, params)
        got = creatives.first.external_service_ids.find { |r| r['service_name'] == spotify }

        expect(got).to have_attributes('identifier' => 'abc')
      end

      it "flags a record for apple identifier creation" do
        params = [create_apple_page]

        described_class.create_update_match(creatives, params)
        got = creatives.first.external_service_ids.find { |r| r['service_name'] == apple }

        expect(got).to have_attributes('identifier' => nil, 'state' => new_artist)
      end

      it "flags a record for spotify identifier creation" do
        params = [create_spotify_page]

        described_class.create_update_match(creatives, params)
        got = creatives.first.external_service_ids.find { |r| r['service_name'] == spotify }

        expect(got).to have_attributes('identifier' => nil, 'state' => new_artist)
      end

      it "creates stubs with nil values" do
        params = [empty]

        described_class.create_update_match(creatives, params)
        got = creatives.first.external_service_ids

        expect(got[0]).to have_attributes('identifier' => nil, 'state' => nil)
        expect(got[1]).to have_attributes('identifier' => nil, 'state' => nil)
      end

      context "when matching creatives and params," do
        it "matches unpersisted creatives with params by name" do
          params = [no_id]

          described_class.create_update_match(creatives, params)
          got_apple = creatives.first.external_service_ids
                               .find { |r| r['service_name'] == apple }
          got_spotify = creatives.first.external_service_ids
                                 .find { |r| r['service_name'] == spotify }

          expect(got_apple).to have_attributes('identifier' => 'xyz', 'state' => nil)
          expect(got_spotify).to have_attributes('identifier' => 'abc', 'state' => nil)
        end

        it "matches persisted creatives with params by id" do
          params = [no_name]

          described_class.create_update_match(creatives, params)
          got_apple = creatives.first.external_service_ids
                               .find { |r| r['service_name'] == apple }
          got_spotify = creatives.first.external_service_ids
                                 .find { |r| r['service_name'] == spotify }

          expect(got_apple).to have_attributes('identifier' => 'xyz', 'state' => nil)
          expect(got_spotify).to have_attributes('identifier' => 'abc', 'state' => nil)
        end
      end

      context "when given a bad ID," do
        before do
          allow(Apple::ArtistIdValidator).to receive(:new).and_return(double(valid?: false))
          allow(Spotify::ArtistIdValidator).to receive(:valid?).and_return(false)
        end

        it "creates a spotify ESID without an identifier" do
          params = [param1]

          described_class.create_update_match(creatives, params)
          got_spotify = creatives.first.external_service_ids
                                 .find { |r| r['service_name'] == spotify }

          expect(got_spotify).to have_attributes('identifier' => nil, 'state' => nil)
        end

        it "creates an apple ESID without an identifier" do
          params = [param1]

          described_class.create_update_match(creatives, params)
          got_apple = creatives.first.external_service_ids
                               .find { |r| r['service_name'] == apple }

          expect(got_apple).to have_attributes('identifier' => nil, 'state' => nil)
        end
      end

      context "when given a ESID for Spotify that is new_artist" do 
        let(:new_album) {create(:album, artist: album.artist)}
        let!(:esi_with_spotify_id) { create(:external_service_id, linkable: new_album.creatives.first, identifier: "spotify_id", service_name: "spotify") }
        
        context "When existing Spotify ESID already exists for the artist" do 
          let(:new_spotify_id) { album.creatives.first.external_service_ids.find_by(service_name: "spotify") }
          let(:existing_spotify_id) { new_album.creatives.first.external_service_ids.find_by(service_name: "spotify") }

          it "creates a ESID for the new artist creative and preserves the old ESID records" do 
            new_album.creatives.update(person_id: album.person_id)
            described_class.create_update_match(creatives, [create_spotify_page])
            
            expect(new_spotify_id).to have_attributes(state: "new_artist", identifier: nil)
            expect(existing_spotify_id).to have_attributes(identifier: "spotify_id")
          end 
        end
      end 

      context "when given a ESID for Apple that is new_artist" do 
        let(:new_album) {create(:album, artist: album.artist, person: album.person)}
        let!(:esi_with_apple_id) { create(:external_service_id, linkable: new_album.creatives.first, identifier: "iPodNano", service_name: "apple") }
        
        context "When existing Aple ESID already exists for the artist" do 
          let(:new_apple_id) { album.creatives.first.external_service_ids.find_by(service_name: "apple") }
          let(:existing_apple_id) { new_album.creatives.first.external_service_ids.find_by(service_name: "apple") }

          it "matches the existing ESID for the new artist creative ESID record" do 
            new_album.creatives.update(person_id: album.person_id)
            described_class.create_update_match(creatives, [create_apple_page])
 
            expect(new_apple_id).to have_attributes(state: "matched", identifier: "iPodNano")
            expect(existing_apple_id).to have_attributes(identifier: "iPodNano")
          end 
        end
      end 
    end
  end
end
