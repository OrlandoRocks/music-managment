require "rails_helper"

describe Sns::Notifier do

  let!(:sns_entry_message) {
    {
      album_id: 123,
      stores: [
        { id: 12, delivery_type: 'metadata_only', delivered: false },
        { id: 13, delivery_type: 'metadata_only', delivered: false }
      ],
      person_id: 42
    }
  }

  let!(:sns_message_metadata_only1) {
    { 
      message: sns_entry_message.to_json
    }
  }

  let!(:sns_message_metadata_only2) {
    {
      message: {
        album_id: 54,
        stores: [
          { id: 12, delivery_type: 'metadata_only', delivered: false },
          { id: 13, delivery_type: 'metadata_only', delivered: false }
        ],
        person_id: 42
      }.to_json
    }
  }  

  let!(:sns_message_metadata_only3) {
    {
      message: {
        album_id: 98,
        stores: [
          { id: 12, delivery_type: 'metadata_only', delivered: false },
          { id: 13, delivery_type: 'metadata_only', delivered: false }
        ],
        person_id: 42
      }.to_json
    }
  }  

  let!(:sns_message_full_delivery) {
    {
      message: {
        album_id: 123,
        stores: [
          { id: 12, delivery_type: 'full_delivery', delivered: false },
          { id: 13, delivery_type: 'full_delivery', delivered: false }
        ],
        person_id: 42
      }.to_json
    }
  } 

  let!(:sns_message_full_delivery_one_store_paused) {
    {
      message: {
        album_id: 123,
        stores: [
          { id: 13, delivery_type: 'full_delivery', delivered: false }
        ],
        person_id: 42
      }.to_json
    }
  }

  let!(:sns_message_full_delivery_one_store_inactive) {
    {
      message: {
        album_id: 123,
        stores: [
          { id: 13, delivery_type: 'full_delivery', delivered: false }
        ],
        person_id: 42
      }.to_json
    }
  }

  let!(:sns_message_full_delivery_one_store_not_in_use) {
    {
      message: {
        album_id: 123,
        stores: [
          { id: 13, delivery_type: 'full_delivery', delivered: false }
        ],
        person_id: 42
      }.to_json
    }
  }

  let!(:sns_message_full_delivery_paused_priority_release) {
    {
      message: {
        album_id: 123,
        stores: [
          { id: 12, delivery_type: 'full_delivery', delivered: false },
          { id: 13, delivery_type: 'full_delivery', delivered: false }
        ],
        person_id: 42
      }.to_json
    }
  }

  let!(:sns_message_full_delivery_with_song_ids) {
    {
      message: {
        album_id: 123,
        stores: [
          { id: 12, delivery_type: 'full_delivery', delivered: false },
          { id: 13, delivery_type: 'full_delivery', delivered: false }
        ],
        person_id: 42,
        song_ids: [1, 2]
      }.to_json
    }
  } 

  let!(:album_123) { TCFactory.create(:album, id: 123) }
  let!(:album_54) { TCFactory.create(:album, id: 54) }
  let!(:album_98) { TCFactory.create(:album, id: 98) }

  before(:each) do
    @sns_message = double
    @sns_client = instance_double(Aws::SNS::Resource)
    @topic = instance_double(Aws::SNS::Topic)

    described_class.instance_variable_set("@sns_client", nil)
    described_class.instance_variable_set("@topic", nil)
    stub_const('ENV', ENV.to_hash.merge('SNS_RELEASE_UPDATE_URGENT_TOPIC' => 'not_topic_name'))
  end


  it 'formats sns_message hash and JSON encode' do
    sns_message = described_class.sns_message(**sns_entry_message)
    expect(sns_message).to be_a(String)
    expect(sns_message).to eq(sns_entry_message.to_json)
  end

  describe '.perform' do

    context 'when message has one album id' do

      it 'calls sns client only once' do
        expect(Aws::SNS::Resource).to receive(:new).and_return(@sns_client).once
        expect(@sns_client).to receive(:topic).with(any_args).and_return(@topic)
        expect(@topic).to receive(:publish).with(sns_message_full_delivery)
        expect(Delivery::SnsNotifierPausedWorker).to_not receive(:perform_async)

        described_class.perform(topic_arn: 'topic_name',
                              album_ids_or_upcs: [album_123.id],
                              store_ids: [12, 13],
                              person_id: 42)
      end

      it 'calls sns client only once when passed album UPC' do
        expect(Aws::SNS::Resource).to receive(:new).and_return(@sns_client).once
        expect(@sns_client).to receive(:topic).with(any_args).and_return(@topic)
        expect(@topic).to receive(:publish).with(sns_message_full_delivery)
        expect(Delivery::SnsNotifierPausedWorker).to_not receive(:perform_async)

        described_class.perform(topic_arn: 'topic_name',
                              album_ids_or_upcs: [album_123.upc.number],
                              store_ids: [12, 13],
                              person_id: 42)
      end

      it 'gets topic only once' do
        expect(Aws::SNS::Resource).to receive(:new).and_return(@sns_client)
        expect(@sns_client).to receive(:topic).with(any_args).and_return(@topic).once
        expect(@topic).to receive(:publish).with(sns_message_metadata_only1)
        expect(Delivery::SnsNotifierPausedWorker).to_not receive(:perform_async)

        described_class.perform(topic_arn: 'topic_name',
                              album_ids_or_upcs: [album_123.id],
                              store_ids: [12, 13],
                              delivery_type: 'metadata_only',
                              person_id: 42)
      end

      it "publishes message with delivery_type 'full_delivery' when delivery_type is missing" do
        expect(Aws::SNS::Resource).to receive(:new).and_return(@sns_client)
        expect(@sns_client).to receive(:topic).with(any_args).and_return(@topic)
        expect(@topic).to receive(:publish).with(sns_message_full_delivery)
        expect(Delivery::SnsNotifierPausedWorker).to_not receive(:perform_async)

        described_class.perform(topic_arn: 'topic_name',
                              album_ids_or_upcs: [album_123.id],
                              store_ids: [12, 13],
                              person_id: 42)
      end

      it "publishes message with delivery_type from params" do
        expect(Aws::SNS::Resource).to receive(:new).and_return(@sns_client)
        expect(@sns_client).to receive(:topic).with(any_args).and_return(@topic)
        expect(@topic).to receive(:publish).with(sns_message_metadata_only1)
        expect(Delivery::SnsNotifierPausedWorker).to_not receive(:perform_async)

        described_class.perform(topic_arn: 'topic_name',
                              album_ids_or_upcs: [album_123.id],
                              store_ids: [12, 13],
                              delivery_type: 'metadata_only',
                              person_id: 42)
      end

      context "with song_ids" do
        it 'calls sns client only once' do
          expect(Aws::SNS::Resource).to receive(:new).and_return(@sns_client).once
          expect(@sns_client).to receive(:topic).with(any_args).and_return(@topic)
          expect(@topic).to receive(:publish).with(sns_message_full_delivery_with_song_ids)
          expect(Delivery::SnsNotifierPausedWorker).to_not receive(:perform_async)
  
          described_class.perform(topic_arn: 'topic_name',
                                album_ids_or_upcs: [album_123.id],
                                store_ids: [12, 13],
                                person_id: 42,
                                song_ids: [1, 2])
        end
      end

    end

    context 'when message has many album ids' do

      it 'calls sns client only once' do
        expect(Aws::SNS::Resource).to receive(:new).and_return(@sns_client).once
        expect(@sns_client).to receive(:topic).with(any_args).and_return(@topic)
        expect(@topic).to receive(:publish).with(any_args).at_least(:once)
        expect(Delivery::SnsNotifierPausedWorker).to_not receive(:perform_async)

        described_class.perform(topic_arn: 'topic_name',
                              album_ids_or_upcs: [album_123.id, album_54.id, album_98.id],
                              store_ids: [12, 13],
                              delivery_type: 'metadata_only',
                              person_id: 42)
      end

      it 'gets topic only once' do
        expect(Aws::SNS::Resource).to receive(:new).and_return(@sns_client)
        expect(@sns_client).to receive(:topic).with(any_args).and_return(@topic).once
        expect(@topic).to receive(:publish).with(any_args).at_least(:once)
        expect(Delivery::SnsNotifierPausedWorker).to_not receive(:perform_async)

        described_class.perform(topic_arn: 'topic_name',
                              album_ids_or_upcs: [album_123.id, album_54.id, album_98.id],
                              store_ids: [12, 13],
                              delivery_type: 'metadata_only')
      end

      it 'publishes exaclty as many messages than album ids' do
        expect(Aws::SNS::Resource).to receive(:new).and_return(@sns_client)
        expect(@sns_client).to receive(:topic).with(any_args).and_return(@topic)
        expect(@topic).to receive(:publish).with(any_args).thrice
        expect(Delivery::SnsNotifierPausedWorker).to_not receive(:perform_async)

        described_class.perform(topic_arn: 'topic_name',
                              album_ids_or_upcs: [album_123.id, album_54.id, album_98.id],
                              store_ids: [12, 13],
                              delivery_type: 'metadata_only',
                              person_id: 42)
      end

      it 'publishes exactly one message per album in order' do
        expect(Aws::SNS::Resource).to receive(:new).and_return(@sns_client)
        expect(@sns_client).to receive(:topic).with(any_args).and_return(@topic)
        expect(@topic).to receive(:publish).with(sns_message_metadata_only1).once.ordered
        expect(@topic).to receive(:publish).with(sns_message_metadata_only2).once.ordered
        expect(@topic).to receive(:publish).with(sns_message_metadata_only3).once.ordered
        expect(Delivery::SnsNotifierPausedWorker).to_not receive(:perform_async)

        described_class.perform(topic_arn: 'topic_name',
                              album_ids_or_upcs: [album_123.id, album_54.id, album_98.id],
                              store_ids: [12, 13],
                              delivery_type: 'metadata_only',
                              person_id: 42)
      end

      it 'publishes exactly one message per album in order when passed album UPC' do
        expect(Aws::SNS::Resource).to receive(:new).and_return(@sns_client)
        expect(@sns_client).to receive(:topic).with(any_args).and_return(@topic)
        expect(@topic).to receive(:publish).with(sns_message_metadata_only1).once.ordered
        expect(@topic).to receive(:publish).with(sns_message_metadata_only2).once.ordered
        expect(@topic).to receive(:publish).with(sns_message_metadata_only3).once.ordered
        expect(Delivery::SnsNotifierPausedWorker).to_not receive(:perform_async)

        described_class.perform(topic_arn: 'topic_name',
                              album_ids_or_upcs: [album_123.upc.number, album_54.upc.number, album_98.upc.number],
                              store_ids: [12, 13],
                              delivery_type: 'metadata_only',
                              person_id: 42)
      end

      it 'publishes exactly one message per album in order when passed album and UPC mixed' do
        expect(Aws::SNS::Resource).to receive(:new).and_return(@sns_client)
        expect(@sns_client).to receive(:topic).with(any_args).and_return(@topic)
        expect(@topic).to receive(:publish).with(sns_message_metadata_only1).once.ordered
        expect(@topic).to receive(:publish).with(sns_message_metadata_only2).once.ordered
        expect(@topic).to receive(:publish).with(sns_message_metadata_only3).once.ordered
        expect(Delivery::SnsNotifierPausedWorker).to_not receive(:perform_async)

        described_class.perform(topic_arn: 'topic_name',
                              album_ids_or_upcs: [album_123.id, album_54.upc.number, album_98.upc.number],
                              store_ids: [12, 13],
                              delivery_type: 'metadata_only',
                              person_id: 42)
      end

      it 'publishes message with delivery_type from albums' do
        expect(Aws::SNS::Resource).to receive(:new).and_return(@sns_client)
        expect(@sns_client).to receive(:topic).with(any_args).and_return(@topic)
        expect(@topic).to receive(:publish).with(sns_message_metadata_only1).ordered
        expect(@topic).to receive(:publish).with(sns_message_metadata_only2).ordered
        expect(@topic).to receive(:publish).with(sns_message_metadata_only3).ordered
        expect(Delivery::SnsNotifierPausedWorker).to_not receive(:perform_async)

        described_class.perform(topic_arn: 'topic_name',
                              album_ids_or_upcs: [album_123.id, album_54.id, album_98.id],
                              store_ids: [12, 13],
                              delivery_type: 'metadata_only',
                              person_id: 42)
      end

    end

    context 'when stores are paused' do
      it 'calls sns for one store and paused sns queue for another when half stores are paused and unpaused' do
        expect(Aws::SNS::Resource).to receive(:new).and_return(@sns_client).once
        expect(@sns_client).to receive(:topic).with(any_args).and_return(@topic)
        expect(@topic).to receive(:publish).with(sns_message_full_delivery_one_store_paused)
        expect(DistributorAPI::Store).to receive(:pause).with(12)

        Store.find(12).store_delivery_config.pause!

        expect(Delivery::SnsNotifierPausedWorker).to receive(:perform_async).with([album_123.id], 12)
        described_class.perform(topic_arn: 'topic_name',
                              album_ids_or_upcs: [album_123.id],
                              store_ids: [12, 13],
                              person_id: 42)
      end

      it 'does not call sns and add to paused sns queue when all stores are paused' do
        expect(Aws::SNS::Resource).not_to receive(:new)
        expect(@sns_client).not_to receive(:topic).with(any_args)
        expect(@topic).not_to receive(:publish)

        expect(DistributorAPI::Store).to receive(:pause).with(12)
        expect(DistributorAPI::Store).to receive(:pause).with(13)
        Store.find(12).store_delivery_config.pause!
        Store.find(13).store_delivery_config.pause!

        expect(Delivery::SnsNotifierPausedWorker).to receive(:perform_async).with([album_123.id, album_54.id], 12)
        expect(Delivery::SnsNotifierPausedWorker).to receive(:perform_async).with([album_123.id, album_54.id], 13)

        described_class.perform(topic_arn: 'topic_name',
                              album_ids_or_upcs: [album_123.id, album_54.id],
                              store_ids: [12, 13],
                              person_id: 42)
      end
    end

    context 'when store is inactive or not in use' do
      it 'calls sns for one store and nothing for another when half stores are inactive' do
        expect(Aws::SNS::Resource).to receive(:new).and_return(@sns_client).once
        expect(@sns_client).to receive(:topic).with(any_args).and_return(@topic)
        expect(@topic).to receive(:publish).with(sns_message_full_delivery_one_store_inactive)

        Store.find(12).update(is_active: false)

        expect(Delivery::SnsNotifierPausedWorker).to_not receive(:perform_async)
        described_class.perform(topic_arn: 'topic_name',
                              album_ids_or_upcs: [album_123.id],
                              store_ids: [12, 13],
                              person_id: 42)
      end

      it 'calls sns for one store and nothing for another when half stores are not in use' do
        expect(Aws::SNS::Resource).to receive(:new).and_return(@sns_client).once
        expect(@sns_client).to receive(:topic).with(any_args).and_return(@topic)
        expect(@topic).to receive(:publish).with(sns_message_full_delivery_one_store_not_in_use)

        Store.find(12).update(in_use_flag: false)

        expect(Delivery::SnsNotifierPausedWorker).to_not receive(:perform_async)
        described_class.perform(topic_arn: 'topic_name',
                              album_ids_or_upcs: [album_123.id],
                              store_ids: [12, 13],
                              person_id: 42)
      end

      it 'Do not add to paused sns queue when all store is paused but inactive' do
        expect(Aws::SNS::Resource).not_to receive(:new)
        expect(@sns_client).not_to receive(:topic).with(any_args)
        expect(@topic).not_to receive(:publish)

        expect(DistributorAPI::Store).to receive(:pause).with(12)
        expect(DistributorAPI::Store).to receive(:pause).with(13)
        Store.find(12).store_delivery_config.pause!
        Store.find(13).store_delivery_config.pause!

        Store.find(12).update(is_active: false)
        expect(Delivery::SnsNotifierPausedWorker).to receive(:perform_async).with([album_123.id, album_54.id], 13).once

        described_class.perform(topic_arn: 'topic_name',
                              album_ids_or_upcs: [album_123.id, album_54.id],
                              store_ids: [12, 13],
                              person_id: 42)
      end

      it 'Do not add to paused sns queue when all store is paused but not in use' do
        expect(Aws::SNS::Resource).not_to receive(:new)
        expect(@sns_client).not_to receive(:topic).with(any_args)
        expect(@topic).not_to receive(:publish)

        expect(DistributorAPI::Store).to receive(:pause).with(12)
        expect(DistributorAPI::Store).to receive(:pause).with(13)
        Store.find(12).store_delivery_config.pause!
        Store.find(13).store_delivery_config.pause!

        Store.find(12).update(in_use_flag: false)
        expect(Delivery::SnsNotifierPausedWorker).to receive(:perform_async).with([album_123.id, album_54.id], 13).once

        described_class.perform(topic_arn: 'topic_name',
                              album_ids_or_upcs: [album_123.id, album_54.id],
                              store_ids: [12, 13],
                              person_id: 42)
      end

      it 'Do not add to paused sns queue when store is paused but inactive and not in use' do
        expect(Aws::SNS::Resource).not_to receive(:new)
        expect(@sns_client).not_to receive(:topic).with(any_args)
        expect(@topic).not_to receive(:publish)

        expect(DistributorAPI::Store).to receive(:pause).with(12)
        expect(DistributorAPI::Store).to receive(:pause).with(13)
        Store.find(12).store_delivery_config.pause!
        Store.find(13).store_delivery_config.pause!

        Store.find(12).update(in_use_flag: false, is_active: false)
        expect(Delivery::SnsNotifierPausedWorker).to receive(:perform_async).with([album_123.id, album_54.id], 13).once

        described_class.perform(topic_arn: 'topic_name',
                              album_ids_or_upcs: [album_123.id, album_54.id],
                              store_ids: [12, 13],
                              person_id: 42)
      end
    end

    context 'when store is paused and release is a priority release' do
      it 'Do not add to pause SNS queue if one store is paused' do
        ENV["SNS_RELEASE_UPDATE_URGENT_TOPIC"] = 'update_urgent_topic'

        expect(Aws::SNS::Resource).to receive(:new).and_return(@sns_client).once
        expect(@sns_client).to receive(:topic).with(any_args).and_return(@topic)
        expect(@topic).to receive(:publish).with(sns_message_full_delivery_paused_priority_release)
        expect(DistributorAPI::Store).to receive(:pause).with(12)

        Store.find(12).store_delivery_config.pause!

        expect(Delivery::SnsNotifierPausedWorker).to_not receive(:perform_async)
        described_class.perform(topic_arn: 'update_urgent_topic',
                              album_ids_or_upcs: [album_123.id],
                              store_ids: [12, 13],
                              person_id: 42)
      end
    end
  end

end
