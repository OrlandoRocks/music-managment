require "rails_helper"

describe LegalDocument::Docusign do
  describe "#document_url" do
    it "returns the docusign s3 legal document url" do
      legal_document      = create(:legal_document)
      document_id         = "%03d" % legal_document.id
      asset_path          = "legal_documents/assets/000/000/#{document_id}/original/letter_of_direction.pdf"
      s3_document_url     = "https://s3.amazonaws.com/documents.tunecore.com/l"
      s3_asset            = double(:s3_asset, url_for: s3_document_url)
      documents_s3_bucket = double(:docusigns_s3_bucket, objects: { asset_path.to_s => s3_asset })
      api_client          = double(:api_client)
      envelopes_api       = double(:envelopes_api)
      docusign_document   = File.new(Rails.root + 'spec/assets/booklet.pdf')

      allow_any_instance_of(Paperclip::Attachment).to receive(:save)         { docusign_document }
      allow(DocuSign::ApiClient).to receive_message_chain(:new, :api_client) { api_client }
      allow(DocuSign_eSign::EnvelopesApi).to receive(:new)                   { envelopes_api }
      allow(envelopes_api).to receive(:get_document)                         { docusign_document }

      docusign_document_url = LegalDocument::Docusign.new(legal_document, documents_s3_bucket).document_url
      expect(docusign_document_url).to eq s3_document_url
    end
  end
end
