require "rails_helper"

describe Stores::BackfillStartService do
  describe ".start" do
    it "starts the backfill of distributions" do
      store = Store.find_by(short_name: "Spotify")
      album = create(:album, :purchaseable, :paid, :with_petri_bundle)
      distro_creator = DistributionCreator.create(album, store.short_name)
      distribution = distro_creator.distribution
      distribution.update_attribute(:state, "paused")
      distro_worker = double(:distribution_worker)
      expect(Delivery::DistributionWorker).to receive(:set).with(queue: "delivery-default", unique_for: false).and_return(distro_worker)
      expect(distro_worker).to receive(:perform_async).with("Distribution", distribution.id)

      Stores::BackfillStartService.start(store.id)
    end
  end
end
