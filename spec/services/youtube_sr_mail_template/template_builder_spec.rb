require "rails_helper"

describe YoutubeSrMailTemplate::TemplateBuilder do
  describe ".build" do
    it "builds a youtube sr mail template email" do
      person = create(:person)
      template = create(:youtube_sr_mail_template, country_website: person.country_website)
      song_1  = create(:song, person: person)
      song_2 = create(:song, :with_optional_isrc, person: person)
      message_data = [
        {
          song_id: song_1.id,
          custom_fields: {
            "1" => {third_party: "apple"}
          }
        },
        {
          song_id: song_2.id,
          custom_fields: {
            "1" => {third_party: "google"}
          }
        }
      ]
      email_template = YoutubeSrMailTemplate::TemplateBuilder.build(person.id, template.template_name, message_data)
      expect(email_template.to_email).to eq(person.email)
      expect(email_template.header).to eq("<p>Hello #{person.name},</p><p>We have been contacted because you are too cool.</p>")
    end
  end
end
