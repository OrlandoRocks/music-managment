require "rails_helper"

describe SalepointService do

  before(:each) do
    @person = create(:person)
    @album  = create(:album, person: @person)
    @album_finalized  = create(:album, person: @person, finalized_at: Date.yesterday)

    @store = Store.find_by(name: "Amazon Music")
    SalepointableStore.create!(store: @store, salepointable_type: "Album")
    @salepoint = @album.salepoints.create!(store: @store, variable_price: @store.variable_prices.first, has_rights_assignment: true)
    expect(@store.variable_prices.first).not_to eq(@store.variable_prices.last)
  end

  describe ".call" do
    it "should update existing salepoints" do
      salepoint_params = { @salepoint.store_id => {salepoint: {id: @salepoint.id, store_id: @salepoint.store_id, variable_price_id: @store.variable_prices.last.id }}}
      SalepointService.call(@person, @album, salepoint_params)
      expect(@salepoint.reload.variable_price_id).to eq(@store.variable_prices.last.id)
    end

    it "should update the apple music flag" do
      store = Store.find(36)
      SalepointableStore.create!(store: store, salepointable_type: "Album")
      salepoint = @album.salepoints.create!( store: store, variable_price: store.default_variable_price )

      salepoint_params = { 36 => { salepoint: { id: salepoint.id, store_id: store.id, variable_price_id: store.default_variable_price.id } } }
      apple_music_opt_in = "on"
      SalepointService.call(@person, @album, salepoint_params, apple_music_opt_in)
      expect(@album.reload.apple_music).to eq(true)

      apple_music_opt_in = nil
      SalepointService.call(@person, @album, salepoint_params, apple_music_opt_in)
      expect(@album.reload.apple_music).to eq(false)
    end

    it "should create new salepoints" do
      store = Store.find_by(name: "iTunes Mexico")
      SalepointableStore.create!(store: store, salepointable_type: "Album")

      salepoint_params = {
        @salepoint.store_id=>{ salepoint: { id: @salepoint.id, store_id: @salepoint.store_id, variable_price_id: @store.variable_prices.first.id } },
        store.id=>{salepoint: {store_id: store.id,variable_price_id: store.variable_prices.first.id}}
      }

      expect {
        SalepointService.call(@person, @album, salepoint_params)
      }.to change(Salepoint, :count).by(1)

      expect(@album.salepoints.reload.last.store).to eq(store)
    end

    it "should provide an album instance variable with errors when erroring" do
      store = Store.find_by(name: "iTunes Mexico")
      SalepointableStore.create!(store: store, salepointable_type: "Album")

      salepoint_params = {
        @salepoint.store_id=>{salepoint: {id: @salepoint.id, store_id: @salepoint.store_id, variable_price_id: @store.variable_prices.first.id }},
        store.id=>{salepoint: {store_id: store.id}}
      }

      expect {
        SalepointService.call(@person, @album, salepoint_params)
      }.to_not change(Salepoint, :count)

      @album.valid?
      expect(@album.errors.full_messages).to eq ["Salepoints distribution iTunes Mexico requires a variable price selection"]
    end

    it "should allow removal of a salepoint" do
      store = Store.find_by(name: "iTunes Mexico")

      SalepointableStore.create!(store: store, salepointable_type: "Album")

      salepoint_params = {
        store.id=>{salepoint: {store_id: store.id, variable_price_id: store.variable_prices.first.id}}
      }

      expect {
        SalepointService.call(@person, @album, salepoint_params)
      }.to_not change(Salepoint, :count)

      expect(@album.salepoints.where(store_id: @store.id).count).to eq(0)
      expect(@album.salepoints.reload.last.store).to eq(store)
    end

    it "should use the itunes us variable price when upgrading to itunes WW" do
      # #Purchase an album
      TCFactory.generate_album_purchase(@person)
      @album = @person.albums.last

      #Create an itunes us salepoint with a non default variable price
      @itunes_us = Store.find(1)
      SalepointableStore.create!(store: @itunes_us, salepointable_type: "Album")
      @us_salepoint = @album.salepoints.first
      @us_salepoint.variable_price = @itunes_us.variable_prices.last
      @us_salepoint.save!

      @itunes_ww = Store.find(36)
      SalepointableStore.create!(store: @itunes_ww, salepointable_type: "Album")

      salepoint_params = {
        @itunes_ww.id=>{salepoint: {store_id: @itunes_ww.id,variable_price_id: @itunes_ww.default_variable_price.id}}
      }

      expect {
        SalepointService.call(@person, @album, salepoint_params)
      }.to change(Salepoint, :count).by(1)

      #Assert we use the itunes us variable price and not the itunes ww variable price
      expect(@album.salepoints.last.store).to eq(@itunes_ww)
      expect(@album.salepoints.last.variable_price).to     eq(@us_salepoint.reload.variable_price)
      expect(@album.salepoints.last.variable_price).not_to eq(@itunes_ww.default_variable_price)
    end

    it "should not use the itunes us variable price when resaving with itunes WW" do
      #Purchase an album
      TCFactory.generate_album_purchase(@person)
      @album = @person.albums.last

      #Create an itunes us salepoint with a default variable price
      @itunes_us = Store.find(1)
      @us_salepoint = @album.salepoints.first
      @us_salepoint.variable_price = @itunes_us.default_variable_price
      @us_salepoint.save!

      #Create an itunes ww salepoint with a non-default variable price
      @itunes_ww = Store.find(36)
      SalepointableStore.create!(store: @itunes_ww, salepointable_type: "Album")
      @ww_salepoint = @album.salepoints.create!(store: @itunes_ww, variable_price: @itunes_ww.variable_prices.last )

      salepoint_params = {
        @itunes_ww.id=>{salepoint: {id: @ww_salepoint.id, store_id: @ww_salepoint.store_id, variable_price_id: @itunes_ww.variable_prices.last.id }}
      }

      expect {
        SalepointService.call(@person, @album, salepoint_params)
      }.to_not change(Salepoint, :count)

      #Assert we use the itunes ww variable price and not the itunes us variable price
      expect(@album.salepoints.last.store).to eq(@itunes_ww)
      expect(@album.salepoints.last.variable_price).to     eq(@itunes_ww.variable_prices.last)
      expect(@album.salepoints.last.variable_price).not_to eq(@us_salepoint.variable_price)
    end
  end

  describe "#amazon_on_demand_new?" do
    it "should return true if Amazon-on-Demand was just added" do
      @distributed_album = create(:album, :finalized, person: @person)
      @physical_store    = Store.find_by(short_name: "AmazonOD")

      salepoint_params  = { @physical_store.id => {salepoint: {store_id: @physical_store.id, variable_price_id: @physical_store.variable_prices.last.id }}}
      salepoint_service = SalepointService.new(@person, @album, salepoint_params)
      salepoint_service.call

      expect(salepoint_service.aod_just_added).to be_truthy
    end

    it "should return false if Amazon-on-Demand wasn't just added" do
      @distributed_album = create(:album, :finalized, person: @person)
      @physical_store    = Store.find_by(short_name: "AmazonOD")
      create(:salepoint, :aod_store, salepointable_id: @album.id, salepointable_type: 'Album')

      salepoint_params  = { @physical_store.id => {salepoint: {store_id: @physical_store.id, variable_price_id: @physical_store.variable_prices.last.id }}}
      salepoint_service = SalepointService.new(@person, @album, salepoint_params)
      salepoint_service.call

      expect(salepoint_service.aod_just_added).to be_falsey
    end
  end

  describe "#aod_salepoint" do
    it "should return the Amazon-on-Demand salepoint if present" do
      @distributed_album = create(:album, :finalized, person: @person)
      @physical_store    = Store.find_by(short_name: "AmazonOD")

      salepoint_params  = { @physical_store.id => {salepoint: {store_id: @physical_store.id, variable_price_id: @physical_store.variable_prices.last.id }}}
      salepoint_service = SalepointService.new(@person, @album, salepoint_params)
      salepoint_service.call

      expect(salepoint_service.aod_salepoint).to be_truthy
      expect(salepoint_service.aod_salepoint&.store).to eq(@physical_store)
    end
  end
end
