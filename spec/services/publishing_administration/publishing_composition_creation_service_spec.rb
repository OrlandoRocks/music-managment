require "rails_helper"

describe PublishingAdministration::PublishingCompositionCreationService do
  describe ".create" do
    it "should create publishing_compositions for all the publishing_composer's songs that don't have publishing_compositions associated" do
      person = create(:person, :with_live_album, :with_publishing_composer_with_purchases)
      person.account = create(:account)
      person.albums.first.songs << create_list(:song, 5)
      publishing_composer = person.publishing_composers.first
      publishing_composer.update(account_id: person.account.id)

      expect(person.songs.pluck(:publishing_composition_id).compact.size).to eq 0
      PublishingAdministration::PublishingCompositionCreationService.create(publishing_composer.id)
      expect(person.songs.pluck(:publishing_composition_id).compact.size).to eq person.songs.size
    end
  end
end
