require "rails_helper"

describe PublishingAdministration::PublishingParamsComposerService do
  describe ".compose" do
    let(:publishing_composer) { create(:publishing_composer) }

    context "when a publishing_composer does not have a publisher" do
      let(:expected_params) do
        {
          name_prefix: publishing_composer.name_prefix,
          first_name: publishing_composer.first_name,
          middle_name: publishing_composer.middle_name,
          last_name: publishing_composer.last_name,
          name_suffix: publishing_composer.name_suffix,
          dob: publishing_composer.dob,
          publishing_role_id: publishing_composer.publishing_role_id,
          composer_cae: publishing_composer.cae,
          composer_pro_id: publishing_composer.performing_rights_organization_id,
          person_id: publishing_composer.person_id,
          id: publishing_composer.id,
          email: publishing_composer.current_email
        }.with_indifferent_access
      end

      it "should return a hash of publishing_composer attributes" do
        params_composer_service = PublishingAdministration::PublishingParamsComposerService.compose(publishing_composer.id)
        expect(params_composer_service.with_indifferent_access).to eq expected_params
      end
    end

    context "when a composer has a publisher" do
      let(:publisher) { create(:publisher) }
      let(:publishing_composer)  { create(:publishing_composer, publisher: publisher) }
      let(:expected_params) do
        {
          name_prefix: publishing_composer.name_prefix,
          first_name: publishing_composer.first_name,
          middle_name: publishing_composer.middle_name,
          last_name: publishing_composer.last_name,
          name_suffix: publishing_composer.name_suffix,
          dob: publishing_composer.dob,
          publishing_role_id: publishing_composer.publishing_role_id,
          composer_cae: publishing_composer.cae,
          composer_pro_id: publishing_composer.performing_rights_organization_id,
          person_id: publishing_composer.person_id,
          email: publishing_composer.current_email,
          id: publishing_composer.id,
          publisher_cae: publisher.cae,
          publisher_name: publisher.name,
          publisher_pro_id: publishing_composer.performing_rights_organization_id
        }.with_indifferent_access
      end

      it "should return a hash of publishing_composer and publisher attributes" do
        params_composer_service = PublishingAdministration::PublishingParamsComposerService.compose(publishing_composer.id)
        expect(params_composer_service.with_indifferent_access).to eq expected_params
      end
    end
  end
end
