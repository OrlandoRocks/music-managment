require "rails_helper"

describe PublishingAdministration::NtcBulkUploadService do
  describe ".sample_upload_file" do
    it "should return CSV with the headers" do
    end
  end

  describe "#process" do
    let(:publishing_composer) { create(:publishing_composer) }
    let(:publishing_composition_row) do
      {
        title: "publishing_composition 1",
        artist_name: "artist 1",
        release_date: "10/10/2020",
        album_name: "rock album",
        record_label: "fancy record label",
        isrc: "VIS369800002",
        percent: "50",
        cowriter_1_name: "Cowriter 1",
        cowriter_1_share: "10",
        cowriter_2_name: "Cowriter 2",
        cowriter_2_share: "20",
        cowriter_3_name: "",
        cowriter_3_share: "",
        public_domain: "false",
      }
    end

    let(:rows) { [publishing_composition_row.values] }

    let(:csv_file) do
      CSV.generate(headers: true) do |csv|
        csv << PublishingAdministration::NtcBulkUploadService::HEADERS.values
        rows.each do |row|
          csv << row
        end
      end
    end

    let(:service) do
      PublishingAdministration::NtcBulkUploadService.new(publishing_composer.person_id, publishing_composer.id, csv_file)
    end

    context "has exceeded maximum length" do
      it "should return with error set" do
        stub_const("PublishingAdministration::NtcBulkUploadService::MAXIMUM_COMPOSITIONS", 0)

        service.process

        expect(service.errors).to include(:publishing_compositions)
      end
    end

    describe "individual validations" do
      it "should return error when title is blank" do
        publishing_composition_row[:title] = nil
        service.process

        expect(service.errors).to include(:publishing_compositions)
      end

      it "should return error when artist name is blank" do
        publishing_composition_row[:artist_name] = nil
        service.process

        expect(service.errors).to include(:publishing_compositions)
      end

      it "should return error when percent is invalid" do
        publishing_composition_row[:percent] = '103'
        service.process

        expect(service.errors).to include(:publishing_compositions)
      end

      it "should return error when any cowriter share is more than 100" do
        publishing_composition_row[:cowriter_1_share] = '103'
        service.process

        expect(service.errors).to include(:publishing_compositions)
      end

      it "should return error when release date is invalid" do
        publishing_composition_row[:release_date] = 'invalid date'
        service.process

        expect(service.errors).to include(:publishing_compositions)
      end

      it "should return error when total percent exceeds 100" do
        publishing_composition_row[:percent] = '90'
        service.process

        expect(service.errors).to include(:publishing_compositions)
      end

      it "should return error when public domain is not true or false" do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

        publishing_composition_row[:public_domain] = 'bad value'
        service.process

        expect(service.errors).to include(:publishing_compositions)
      end
    end

    describe "bulk validations" do
      context "isrc is duplicate within the sheet" do
        it "should set the error" do
          new_row = publishing_composition_row
          new_row[:title] = 'publishing_composition 2'
          rows << new_row

          service.process

          expect(service.errors).to include(:publishing_compositions)
        end
      end

      context "isrc is in use in distro songs" do
        it "should set the error" do
          album = create(:album, person: publishing_composer.person)
          create(:song, :with_optional_isrc, album: album, optional_isrc: publishing_composition_row[:isrc])
          service.process

          expect(service.errors).to include(:publishing_compositions)
        end
      end

      context "isrc is in use in ntc songs" do
        it "should set the error" do
          ntc_album = create(:non_tunecore_album, publishing_composer: publishing_composer)
          create(:non_tunecore_song, non_tunecore_album: ntc_album, isrc: publishing_composition_row[:isrc])
          service.process

          expect(service.errors).to include(:publishing_compositions)
        end
      end

      context "title is duplicate within the sheet" do
        it "should set the error" do
          new_row = publishing_composition_row
          new_row[:isrc] = 'xyz123'
          rows << new_row

          service.process

          expect(service.errors).to include(:publishing_compositions)
        end
      end

      context "title is already in use" do
        it "should set the error" do
          ntc_album = create(:non_tunecore_album, publishing_composer: publishing_composer)
          publishing_composition = create(
            :publishing_composition,
            name: publishing_composition_row[:title],
            account: publishing_composer.account
          )

          create(:non_tunecore_song, non_tunecore_album: ntc_album, publishing_composition: publishing_composition)

          service.process

          expect(service.errors).to include(:publishing_compositions)
        end
      end
    end

    it "should create publishing_compositions with correct params" do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

      expected_params = {
        title: "publishing_composition 1",
        artist_name: "artist 1",
        album_name: "rock album",
        record_label: "fancy record label",
        isrc_number: "VIS369800002",
        percent: "50",
        release_date: Date.parse("10/10/2020"),
        person_id: publishing_composer.person_id,
        publishing_composer_id: publishing_composer.id,
        cowriter_params: [
          {
            first_name: "Cowriter",
            last_name: "1",
            cowriter_share: "10"
          },
          {
            first_name: "Cowriter",
            last_name: "2",
            cowriter_share: "20"
          }
        ],
        public_domain: "false",
      }

      expect(PublishingAdministration::NtcCreationWorker).to receive(:perform_async).with(expected_params)
      service.process
    end
    describe "#valid_isrc?" do
      let(:publishing_composition_isrcs) do
        {
          valid_isrcs: [
            "AZAFZ0000000",
            "AZAF00000000",
            "AZA0Z0000000",
            "AZA000000000",
            "AZ0FZ0000000",
            "AZ0F00000000",
            "AZ00Z0000000",
            "AZ0000000000",
            ""
          ],
          invalid_isrcs: [
            "A0AAA0000000",
            "A0AA00000000",
            "A0A000000000",
            "A00000000000",
            "AA0000A00000",
            "ASD",
            "DFFSE1233"
          ]        
        }
      end
      it "should return true for all valid isrcs" do
        publishing_composition_isrcs[:valid_isrcs].each do |isrc|
          expect(service.send(:valid_isrc?, {isrc: isrc})).to eq true
        end
      end
      it "should return true for all valid isrcs" do
        publishing_composition_isrcs[:invalid_isrcs].each do |isrc|
          expect(service.send(:valid_isrc?, {isrc: isrc})).to eq false
        end
      end
    end
  end
end
