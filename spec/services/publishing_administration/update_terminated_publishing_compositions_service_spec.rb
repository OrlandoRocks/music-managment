require "rails_helper"

describe PublishingAdministration::UpdateTerminatedPublishingCompositionsService do
  describe ".update" do
    let!(:account)             { create(:account) }
    let!(:publishing_composer) { create(:publishing_composer, account: account, person: account.person) }
    let!(:terminated_composer) { create(:terminated_composer, :partially, publishing_composer: publishing_composer, alt_provider_acct_id: "1") }
    let(:album)                { create(:album, person: account.person) }
    let(:publishing_composition) { create(:publishing_composition, provider_identifier: "DMC0000000W", account: account) }

    it "updates the publishing_composition's state to 'terminated'" do
      publishing_composer.reload
      work = double(:work, work_code: publishing_composition.provider_identifier)

      work_list = double(:work_list, works: [work])
      allow(PublishingAdministration::ApiClientServices::FetchWorkListService).to receive(:fetch) { work_list }

      expect(publishing_composition.state).not_to eq "terminated"
      described_class.update(account.reload)
      expect(publishing_composition.reload.state).to eq "terminated"
    end
  end
end
