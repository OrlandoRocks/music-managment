require "rails_helper"

describe PublishingAdministration::ApiClientServices::FetchWorkListService do
  describe ".fetch" do
    it "returns the recording list for the composition" do
      composition = build(:composition)

      recording = double(:recording)
      recording_list = PublishingAdministration::ApiClientServices::FetchRecordingListService.new(composition)

      api_client = double(:api_client)
      recording_list.instance_variable_set(:@api_client, api_client)

      response = double(:response,
                        body: { "Records" => { "recording" => recording } },
                        recordings: [recording],
                        errors?: true,
                        total_count: 1)
      allow(api_client).to receive(:send_request) { response }

      recording_list.fetch

      expect(recording_list.recordings.first).to eq recording
    end
  end
end
