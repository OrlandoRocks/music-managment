require "rails_helper"

describe PublishingAdministration::ApiClientServices::WriterCodeService do
  describe "#create_or_update" do
    context "when the account is not blocked" do
      context "when the account does not have a provider_account_id" do
        it "invokes the ArtistAccountService" do
          composer = create(:composer)
          account = composer.account

          service = PublishingAdministration::ApiClientServices::WriterCodeService.new(composer)

          api_client = double(:api_client)
          service.instance_variable_set(:@api_client, api_client)
          allow(PublishingAdministration::ApiClientServices::WriterListService).to receive(:get_writer_code)
          allow(api_client).to receive(:send_request)

          expect(PublishingAdministration::ApiClientServices::ArtistAccountService)
            .to receive(:find_or_create_artist)
            .with(account: account)

          service.create_or_update
        end
      end

      context "when the composer's writer_code exists in RightsApp" do
        it "updates the composer's writer_code" do
          account = create(:account, provider_account_id: "TUN0000034473A")
          composer = create(:composer, account: account)
          writer_code = "00002LENW"

          service = PublishingAdministration::ApiClientServices::WriterCodeService.new(composer)

          api_client = double(:api_client)
          service.instance_variable_set(:@api_client, api_client)
          allow(PublishingAdministration::ApiClientServices::WriterListService).to receive(:get_writer_code).and_return(writer_code)
          allow(api_client).to receive(:send_request)

          service.create_or_update

          expect(composer.reload.provider_identifier).to eq writer_code
        end
      end

      context "when the composer does not have a writer_code on RightsApp" do
        it "creates a new writer_code" do
          account = create(:account, provider_account_id: "TUN0000034473A")
          composer = create(:composer, account: account)
          writer_code = "00002LENW"

          service = PublishingAdministration::ApiClientServices::WriterCodeService.new(composer)

          api_client = double(:api_client)
          service.instance_variable_set(:@api_client, api_client)
          allow(PublishingAdministration::ApiClientServices::WriterListService).to receive(:get_writer_code)
          allow(api_client).to receive(:send_request).and_return(double(:response, writer_code: writer_code))

          service.create_or_update

          expect(composer.reload.provider_identifier).to eq writer_code
        end
      end
    end

    context "when the account is blocked" do
      context "when the account does not have a provider_account_id" do
        it "does not invoke the ArtistAccountService" do
          composer = create(:composer)
          account = composer.account
          account.update(blocked: true)

          service = PublishingAdministration::ApiClientServices::WriterCodeService.new(composer)

          api_client = double(:api_client)
          service.instance_variable_set(:@api_client, api_client)
          allow(PublishingAdministration::ApiClientServices::WriterListService).to receive(:get_writer_code)
          allow(api_client).to receive(:send_request)

          expect(PublishingAdministration::ApiClientServices::ArtistAccountService)
            .to_not receive(:find_or_create_artist)

          service.create_or_update
        end
      end

      context "when the composer's writer_code exists in RightsApp" do
        it "does not update the composer's writer_code" do
          account = create(:account, provider_account_id: "TUN0000034473A", blocked: true)
          composer = create(:composer, account: account)
          writer_code = "00002LENW"

          service = PublishingAdministration::ApiClientServices::WriterCodeService.new(composer)

          api_client = double(:api_client)
          service.instance_variable_set(:@api_client, api_client)
          allow(PublishingAdministration::ApiClientServices::WriterListService).to receive(:get_writer_code).and_return(writer_code)
          allow(api_client).to receive(:send_request)

          service.create_or_update

          expect(composer.reload.provider_identifier).to_not eq writer_code
        end
      end

      context "when the composer does not have a writer_code on RightsApp" do
        it "does not create a new writer_code" do
          account = create(:account, provider_account_id: "TUN0000034473A", blocked: true)
          composer = create(:composer, account: account)
          writer_code = "00002LENW"

          service = PublishingAdministration::ApiClientServices::WriterCodeService.new(composer)

          api_client = double(:api_client)
          service.instance_variable_set(:@api_client, api_client)
          allow(PublishingAdministration::ApiClientServices::WriterListService).to receive(:get_writer_code)
          allow(api_client).to receive(:send_request).and_return(double(:response, writer_code: writer_code))

          service.create_or_update

          expect(composer.reload.provider_identifier).to_not eq writer_code
        end
      end
    end
  end
end
