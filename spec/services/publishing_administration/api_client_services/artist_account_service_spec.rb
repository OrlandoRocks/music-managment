require "rails_helper"

describe PublishingAdministration::ApiClientServices::ArtistAccountService do
  let(:account)           { create(:account) }
  let(:artist_acct_code)  { "TCM0000047717A" }
  let(:service)           { PublishingAdministration::ApiClientServices::ArtistAccountService }
  let(:params)            { { account: account } }
  let(:api_client)        { $rights_app_api_client }
  let(:response_double)   { double(:response_double) }

  let(:response) {
    { ArtistAccountCode: artist_acct_code,
      AccountName: "Dana Czinsky",
      FacebookPageUrl: nil,
      TwitterProfileUrl: nil,
      SoundcloudPageUrl: nil,
      ArtistAccountRegistrationDate: "2018-09-07T16:34:57.5684858Z",
      ReleasedToServiceOrStore: false,
      PerformedLiveLastSixMonths: false,
      CatalogueCode: "TCM0000000001C",
      IsDeleted: false,
      Messages: [] }.to_json
  }

  context "#find_or_create_artist" do
    before :each do
      allow(api_client).to receive(:send_request).and_return(response_double)
      allow(response_double).to receive(:body).and_return(response)
      allow(response_double).to receive(:artist_account_code).and_return(artist_acct_code)
      allow(response_double).to receive(:is_deleted).and_return(false)
      allow(response_double).to receive(:retry?).and_return(false)
    end

    context "when the account is not blocked" do
      it "creates a Sentric artist, receives a Rights App ArtistAccountCode, and saves it on the Composer's Account" do
        expect(account.provider_account_id.present?).to be false

        expect {
          service.find_or_create_artist(params)
        }.to change(account, :provider_account_id).from(nil)

        expect(account.provider_account_id).to eq artist_acct_code
      end

      it "creates a Sentric artist when the Account's provider_account_id differs from ArtistAccountCode" do
        account.update(provider_account_id: "ABCD1234")

        expect {
          service.find_or_create_artist(params)
        }.to change(account, :provider_account_id).from("ABCD1234")

        expect(account.provider_account_id).to eq artist_acct_code
      end

      it "gets the Sentric artist and does nothing if the provider_account_id matches the ArtistAccountCode" do
        account.update(provider_account_id: artist_acct_code)

        expect{
          service.find_or_create_artist(params)
        }.not_to change(account, :provider_account_id).from(account.provider_account_id)

        expect(account.provider_account_id).to eq artist_acct_code
      end
    end

    context "when the account is blocked" do
      let(:account) { create(:account, blocked: true) }

      it "does not create a Sentric artist, receive a Rights App ArtistAccountCode, and save it on the Composer's Account" do
        expect(account.provider_account_id.present?).to be false

        expect {
          service.find_or_create_artist(params)
        }.to_not change(account, :provider_account_id).from(nil)

        expect(account.provider_account_id).to_not eq artist_acct_code
      end

      it "does not create a Sentric artist when the Account's provider_account_id differs from ArtistAccountCode" do
        account.update(provider_account_id: "ABCD1234")

        expect {
          service.find_or_create_artist(params)
        }.to_not change(account, :provider_account_id).from("ABCD1234")

        expect(account.provider_account_id).to_not eq artist_acct_code
      end

      it "does not get the Sentric artist and does nothing if the provider_account_id matches the ArtistAccountCode" do
        account.update(provider_account_id: artist_acct_code)

        expect{
          service.find_or_create_artist(params)
        }.to_not change(account, :provider_account_id).from(account.provider_account_id)

        expect(account.provider_account_id).to eq artist_acct_code
      end
    end
  end
end
