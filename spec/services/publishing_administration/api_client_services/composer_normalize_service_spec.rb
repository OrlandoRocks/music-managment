require "rails_helper"

describe PublishingAdministration::ApiClientServices::ComposerNormalizeService do
  let(:api_client)  { $rights_app_api_client }
  let(:service)     { PublishingAdministration::ApiClientServices::ComposerNormalizeService }
  let(:account)     { create(:account, provider_account_id: "DMC123") }
  let(:publisher)   { create(:publisher) }
  let(:writer_code) { Faker::Number.number(digits: 8) }
  let(:composer) do
    create(
      :composer,
      account:              account,
      publisher:            publisher,
      cae:                  "987654321",
      provider_identifier:  writer_code,
      performing_rights_organization: publisher.performing_rights_organization
    )
  end

  describe "#reconcile_composers" do
    context "when the account is not blocked" do
      context "Rights App Writer matches TC composer" do
        it "updates the TC composer if matched by WorkCode/provider_identifier" do
          response = double(:response, {
            first_name: "Michelle",
            middle_name: '',
            last_name: "Obama",
            writer_code: writer_code,
            society_id: '',
            cae: '',
            publisher_name: '',
            publisher_cae: '',
            needs_update?: true,
            unknown?: false,
          })
          allow(api_client).to receive(:send_request).and_return(response)

          params = { account: account, composers: [composer], api_composers: [] }

          expect { service.reconcile_composers(params) }.to_not change { account.composers.count }
          expect(composer.reload.first_name).to eq response.first_name
        end

        it "updates the TC composer if matched by first name and last name" do
          first_name = "Michelle"
          last_name = "Obama"
          new_writer_code = Faker::Number.number(digits: 8)
          composer.update({
            first_name: first_name,
            last_name: last_name,
            provider_identifier: Faker::Number.number(digits: 8)
          })
          response = double(:response, {
            first_name: "Michelle",
            middle_name: '',
            last_name: "Obama",
            writer_code: new_writer_code,
            society_id: '',
            cae: '',
            publisher_name: '',
            publisher_cae: '',
            needs_update?: true,
            unknown?: false,
          })
          allow(api_client).to receive(:send_request).and_return(response)

          params = { account: account, composers: [composer], api_composers: [] }

          expect { service.reconcile_composers(params) }.to_not change { account.composers.count }
          expect(composer.reload.provider_identifier).to eq new_writer_code.to_s
        end
      end

      context "Rights App Writer does not match TC composer" do
        it "compares TC composers with RightsApp composers and creates a new TC composer if needed" do
          first_name = "Michelle"
          last_name = "Obama"
          new_writer_code = Faker::Number.number(digits: 8)
          new_composer = double(:api_composer, {
            first_name: first_name,
            last_name: last_name,
            writer_code: new_writer_code,
          })
          response = double(:response, {
            first_name: "Michelle",
            middle_name: '',
            last_name: "Obama",
            writer_code: new_writer_code,
            society_id: '',
            cae: '',
            publisher_name: '',
            publisher_cae: '',
            needs_update?: true,
            unknown?: false,
          })
          allow(api_client).to receive(:send_request).and_return(response)

          params = {
            account:        account,
            composers:      [],
            api_composers:  [new_composer]
          }

          create_composer_params = {
            account: account,
            api_response: response,
            free_purchase: false
          }

          expect(PublishingAdministration::CreateComposerService)
            .to receive(:create_composer)
            .with(create_composer_params)

          service.reconcile_composers(params)
        end
      end

      describe "#names_match?" do
        context "when a composer's first name is nil" do
          it "returns false" do
            composer = build(:composer, first_name: nil, last_name: nil)

            service = PublishingAdministration::ApiClientServices::ComposerNormalizeService.new({})

            expect(service.send(:names_match?, composer, double(:response))).to be false
          end
        end

        context "when a composer's first and last name is present and the names match the response" do
          it "returns true" do
            first_name = Faker::Name.first_name
            last_name = Faker::Name.last_name
            composer = build(:cowriter, first_name: first_name, last_name: last_name)
            response = double(:response, first_name: first_name, last_name: last_name)

            service = PublishingAdministration::ApiClientServices::ComposerNormalizeService.new({})

            expect(service.send(:names_match?, composer, response)).to be true
          end
        end
      end
    end

    context "when the account is blocked" do
      let(:account) { create(:account, provider_account_id: "DMC123", blocked: true) }

      context "Rights App Writer matches TC composer" do
        it "does not update the TC composer if matched by WorkCode/provider_identifier" do
          response = double(:response, {
            first_name: "Michelle",
            middle_name: '',
            last_name: "Obama",
            writer_code: writer_code,
            society_id: '',
            cae: '',
            publisher_name: '',
            publisher_cae: '',
            needs_update?: true,
            unknown?: false,
          })
          allow(api_client).to receive(:send_request).and_return(response)

          params = { account: account, composers: [composer], api_composers: [] }

          expect { service.reconcile_composers(params) }.to_not change { account.composers.count }
          expect(composer.reload.first_name).to_not eq response.first_name
        end

        it "does not update the TC composer if matched by first name and last name" do
          first_name = "Michelle"
          last_name = "Obama"
          new_writer_code = Faker::Number.number(digits: 8)
          composer.update({
            first_name: first_name,
            last_name: last_name,
            provider_identifier: Faker::Number.number(digits: 8)
          })
          response = double(:response, {
            first_name: "Michelle",
            middle_name: '',
            last_name: "Obama",
            writer_code: new_writer_code,
            society_id: '',
            cae: '',
            publisher_name: '',
            publisher_cae: '',
            needs_update?: true,
            unknown?: false,
          })
          allow(api_client).to receive(:send_request).and_return(response)

          params = { account: account, composers: [composer], api_composers: [] }

          expect { service.reconcile_composers(params) }.to_not change { account.composers.count }
          expect(composer.reload.provider_identifier).to_not eq new_writer_code.to_s
        end
      end

      context "Rights App Writer does not match TC composer" do
        it "does not compare TC composers with RightsApp composers and create a new TC composer if needed" do
          first_name = "Michelle"
          last_name = "Obama"
          new_writer_code = Faker::Number.number(digits: 8)
          new_composer = double(:api_composer, {
            first_name: first_name,
            last_name: last_name,
            writer_code: new_writer_code,
          })
          response = double(:response, {
            first_name: "Michelle",
            middle_name: '',
            last_name: "Obama",
            writer_code: new_writer_code,
            society_id: '',
            cae: '',
            publisher_name: '',
            publisher_cae: '',
            needs_update?: true,
            unknown?: false,
          })
          allow(api_client).to receive(:send_request).and_return(response)

          params = {
            account:        account,
            composers:      [],
            api_composers:  [new_composer]
          }

          create_composer_params = {
            account: account,
            api_response: response,
            free_purchase: false
          }

          expect(PublishingAdministration::CreateComposerService)
            .to_not receive(:create_composer)
            .with(create_composer_params)

          service.reconcile_composers(params)
        end
      end
    end
  end
end

