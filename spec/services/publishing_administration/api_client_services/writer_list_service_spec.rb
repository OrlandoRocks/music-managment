require "rails_helper"

describe PublishingAdministration::ApiClientServices::WriterListService do
  let(:artist_account_code) { "ABCD1234" }
  let(:account)           { create(:account, provider_account_id: artist_account_code)}
  let(:composer)          { create(:composer, account: account) }
  let(:cowriter)          { create(:cowriter, composer: composer) }
  let(:service)           { PublishingAdministration::ApiClientServices::WriterListService }
  let(:params)            { { writer: composer } }
  let(:api_client)        { $rights_app_api_client }
  let(:body) do
    {
      ArtistAccountCode: artist_account_code,
      Records: [
        {
          WriterCode: "000011",
          FirstName: composer.first_name,
          MiddleName: composer.middle_name.nil? ? "" : composer.middle_name,
          LastName: composer.last_name
        },
        {
          WriterCode: "000022",
          FirstName: cowriter.first_name,
          LastName: cowriter.last_name
        }
      ],
      TotalCount: 2,
      Messages: []
    }.with_indifferent_access
  end

  before do
    json_response = double(:response, body: body.to_json)
    @response = RightsApp::Responses::WriterList.new(json_response)
  end

  context "#get_list" do
    it "gets a list of Sentric Writers from Rights App" do
      allow(api_client).to receive(:send_request).and_return(@response)

      expect(service.get_list(params)).to eq body
    end
  end

  context "#get_writer_code" do
    it "finds the Sentric Writer that matches the TC composer" do
      allow(api_client).to receive(:send_request).and_return(@response)

      writer_code = service.get_writer_code(params)
      expect(writer_code).to eq "000011"
    end

    it "returns nil if there are two Sentric Writers with the same name" do
      other_writer = {
        WriterCode: "000033",
        FirstName: composer.first_name.downcase,
        LastName: composer.last_name.downcase
      }

      body[:Records] = [body[:Records].first, other_writer]
      json_response = double(:response, body: body.to_json)

      response = RightsApp::Responses::WriterList.new(json_response)
      allow(api_client).to receive(:send_request).and_return(response)

      writer_code = service.get_writer_code(params)
      expect(writer_code).to be_nil
    end
  end
end
