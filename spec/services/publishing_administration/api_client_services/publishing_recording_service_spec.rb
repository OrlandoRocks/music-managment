require "rails_helper"

describe PublishingAdministration::ApiClientServices::PublishingRecordingService do
  let(:account)         { create(:account, provider_account_id: "TCM0000000028A") }
  let(:publishing_composer) { create(:publishing_composer, account: account, provider_identifier: "00000008N") }
  let(:publishing_composition) { create(:publishing_composition, provider_identifier: "TCM0000000104S", account: account) }
  let(:album)           { create(:album) }
  let(:song)            { create(:song, album: album, publishing_composition_id: publishing_composition.id) }
  let(:recording_id)    { "39e92091-f0b9-ac83-9f1c-bb23695b4cde" }
  let(:params)          { { publishing_composition: publishing_composition, publishing_composer: publishing_composer } }
  let(:api_client)      { double(:api_client) }
  let(:recording_response) do
    {
      RecordingId: recording_id,
      WorkCode: publishing_composition.provider_identifier,
      Title: song.name,
      PerformingArtist: song.artist.name,
      Isrc: song.isrc,
      RecordLabel: album.label.name,
      ReleaseDate: album.orig_release_date,
      IsDeleted: false,
      IsLocked: false,
      ChangesPending: false,
      Messages: []
    }
  end
  let(:response_dbl) { double(body: recording_response.to_json, headers: { etag: "etag" }) }

  describe "#post_recording" do
    before :each do
      response = RightsApp::Responses::Recording.new(response_dbl)
      allow(api_client).to receive(:send_request).and_return(double(e_tag: "etag" ), response)
    end

    context "when the account is not blocked" do
      context "publishing_composition has associated song or non_tunecore_song" do
        it "creates a recording and sets the recording_code and recordable" do
          service = PublishingAdministration::ApiClientServices::PublishingRecordingService.new(params)
          service.instance_variable_set(:@api_client, api_client)

          expect {
            service.post_recording
          }.to change {
            publishing_composition.recordings.count
          }.by(1)

          recording = publishing_composition.reload.recordings.first
          expect(recording.recording_code).to eq recording_id
          expect(recording.recordable).to eq song
        end

        it "creates a Sentric Work if one does not exist for the publishing_composition" do
          publishing_composition.update(provider_identifier: nil)
          service = PublishingAdministration::ApiClientServices::PublishingRecordingService.new(params)
          service.instance_variable_set(:@api_client, api_client)

          expect(PublishingAdministration::ApiClientServices::PublishingWorkCreationService).to receive(:post_work).with(params)
          service.post_recording
        end
      end

      context "publishing_composition has a recording without a recording_code" do
        it "creates a recording_code for the recording" do
          service = PublishingAdministration::ApiClientServices::PublishingRecordingService.new(params)
          service.instance_variable_set(:@api_client, api_client)
          recording = publishing_composition.recordings.create(recording_code: nil, recordable: publishing_composition.song)
          publishing_composition.reload

          service.post_recording

          expect(recording.reload.recording_code).not_to be nil
        end
      end
    end

    context "when the account is blocked" do
      let(:account) { create(:account, provider_account_id: "TCM0000000028A", blocked: true) }

      context "publishing_composition has associated song or non_tunecore_song" do
        it "does not create a recording and set the recording_code and recordable" do
          service = PublishingAdministration::ApiClientServices::PublishingRecordingService.new(params)
          service.instance_variable_set(:@api_client, api_client)

          expect {
            service.post_recording
          }.to_not change {
            publishing_composition.recordings.count
          }
        end

        it "does not create a Sentric Work if one does not exist for the publishing_composition" do
          publishing_composition.update(provider_identifier: nil)
          service = PublishingAdministration::ApiClientServices::PublishingRecordingService.new(params)
          service.instance_variable_set(:@api_client, api_client)

          expect(PublishingAdministration::ApiClientServices::PublishingWorkCreationService).to_not receive(:post_work).with(params)
          service.post_recording
        end
      end

      context "publishing_composition is marked ineligible" do
        it "does not create a Sentric Work if one does not exist for the publishing_composition" do
          publishing_composition.update(provider_identifier: nil, state: :ineligible)
          service = PublishingAdministration::ApiClientServices::PublishingRecordingService.new(params)
          service.instance_variable_set(:@api_client, api_client)

          expect(PublishingAdministration::ApiClientServices::PublishingWorkCreationService).to_not receive(:post_work).with(params)
          service.post_recording
        end
      end

      context "publishing_composition has a recording without a recording_code" do
        it "does not create a recording_code for the recording" do
          service = PublishingAdministration::ApiClientServices::PublishingRecordingService.new(params)
          service.instance_variable_set(:@api_client, api_client)
          recording = publishing_composition.recordings.create(recording_code: nil, recordable: publishing_composition.song)
          publishing_composition.reload

          service.post_recording

          expect(recording.reload.recording_code).to be nil
        end
      end
    end
  end

  context "publishing_compositions does not have an associated song or non_tunecore_song" do
    it "does not do anything" do
      other_publishing_composition = create(:publishing_composition, account: account)
      params = { publishing_composition: other_publishing_composition, publishing_composer: publishing_composer }
      service = PublishingAdministration::ApiClientServices::PublishingRecordingService.new(params)
      service.instance_variable_set(:@api_client, api_client)

      expect { service.post_recording }.not_to change { publishing_composition.recordings.count }
    end
  end
end
