require "rails_helper"

describe PublishingAdministration::ApiClientServices::PublishingWriterService do
  let(:publisher) { create(:publisher) }
  let(:account) { create(:account, provider_account_id: "ABCD1234") }
  let(:writer_code) { "00001018" }
  let(:service) { PublishingAdministration::ApiClientServices::PublishingWriterService }
  let(:api_client) { $rights_app_api_client }
  let(:response_double) { double(:response_double) }

  let(:publishing_composer) do
    create(
      :publishing_composer,
      account: account,
      publisher: publisher,
      cae: "987654321",
      performing_rights_organization: publisher.performing_rights_organization
    )
  end

  let(:response) do
    {
      WriterCode: writer_code,
      FirstName: publishing_composer.first_name,
      MiddleName: publishing_composer.middle_name.nil? ? "" : publishing_composer.middle_name,
      LastName: publishing_composer.last_name,
      SocietyId: publishing_composer.performing_rights_organization.provider_identifier,
      CaeipiNumber: publishing_composer.cae,
      PublisherName: publishing_composer.publisher.name,
      PublisherCaeipi: publishing_composer.publisher.cae,
      Messages: []
    }
  end

  context "#create_or_update" do
    before :each do
      allow(api_client).to receive(:send_request).and_return(response_double)
      allow(response_double).to receive(:body).and_return(response)
      allow(response_double).to receive(:writer_code).and_return(writer_code)
      allow(response_double).to receive(:e_tag).and_return("i am an etag")
    end

    context "when the account is not blocked" do
      it "checks Writer List for Writer and saves the WriterCode before attempting to POST to Rights App" do
        allow(PublishingAdministration::ApiClientServices::WriterListService)
          .to receive(:get_writer_code)
          .and_return(writer_code)

        expect(publishing_composer.provider_identifier.present?).to be false

        expect { service.create_or_update(publishing_composer) }
          .to change(publishing_composer, :provider_identifier).from(nil).to(writer_code)

        expect(api_client).not_to receive(:send_request)
      end

      it "creates a writer in Rights App and saves the WriterCode if provider_identifier is nil" do
        allow(PublishingAdministration::ApiClientServices::WriterListService)
          .to receive(:get_writer_code)
          .and_return(nil)

        expect(publishing_composer.provider_identifier.present?).to be false

        expect { service.create_or_update(publishing_composer) }
          .to change(publishing_composer, :provider_identifier).from(nil).to(writer_code)
      end

      it "gets the Sentric writer and sends an update to the Rights App writer if the response doesn't match TC Composer's data" do
        publishing_composer.update(provider_identifier: writer_code, cae: "55555")
        response = {
          WriterCode: writer_code,
          FirstName: publishing_composer.first_name,
          MiddleName: publishing_composer.middle_name.nil? ? "" : publishing_composer.middle_name,
          LastName: publishing_composer.last_name,
          SocietyId: publishing_composer.performing_rights_organization.provider_identifier,
          CaeipiNumber: publishing_composer.cae,
          PublisherName: publishing_composer.publisher.name,
          PublisherCaeipi: publishing_composer.publisher.cae,
          Messages: []
        }

        allow(response_double).to receive(:body).and_return(response)
        allow(response_double).to receive(:needs_update?).and_return(true)

        expect(service.create_or_update(publishing_composer)).to eq response
      end

      it "gets the Sentric writer and does nothing if the response matches the TC Composer's data" do
        allow(response_double).to receive(:needs_update?).and_return(false)
        publishing_composer.update(provider_identifier: writer_code)

        expect { service.create_or_update(publishing_composer) }
          .not_to change(publishing_composer, :cae).from(publishing_composer.cae)

        expect(publishing_composer.provider_identifier).to eq writer_code
      end
    end

    context "when the account is blocked" do
      let(:account) { create(:account, provider_account_id: "ABCD1234", blocked: true) }

      it "does not check Writer List for Writer and save the WriterCode before attempting to POST to Rights App" do
        allow(PublishingAdministration::ApiClientServices::WriterListService)
          .to receive(:get_writer_code)
          .and_return(writer_code)

        expect(publishing_composer.provider_identifier.present?).to be false

        expect { service.create_or_update(publishing_composer) }
          .to_not change(publishing_composer, :provider_identifier)

        expect(api_client).not_to receive(:send_request)
      end

      it "does not create a writer in Rights App and save the WriterCode if provider_identifier is nil" do
        allow(PublishingAdministration::ApiClientServices::WriterListService)
          .to receive(:get_writer_code)
          .and_return(nil)

        expect(publishing_composer.provider_identifier.present?).to be false

        expect { service.create_or_update(publishing_composer) }
          .to_not change(publishing_composer, :provider_identifier)
      end

      it "does not get the Sentric writer and send an update to the Rights App writer if the response doesn't match TC Composer's data" do
        publishing_composer.update(provider_identifier: writer_code, cae: "55555")
        response = {
          WriterCode: writer_code,
          FirstName: publishing_composer.first_name,
          MiddleName: publishing_composer.middle_name.nil? ? "" : publishing_composer.middle_name,
          LastName: publishing_composer.last_name,
          SocietyId: publishing_composer.performing_rights_organization.provider_identifier,
          CaeipiNumber: publishing_composer.cae,
          PublisherName: publishing_composer.publisher.name,
          PublisherCaeipi: publishing_composer.publisher.cae,
          Messages: []
        }

        allow(response_double).to receive(:body).and_return(response)
        allow(response_double).to receive(:needs_update?).and_return(true)

        expect(service.create_or_update(publishing_composer)).to_not eq response
      end
    end
  end
end
