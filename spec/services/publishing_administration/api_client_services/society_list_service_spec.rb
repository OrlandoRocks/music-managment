require "rails_helper"

describe PublishingAdministration::ApiClientServices::SocietyListService do
  let(:api_client)      { $rights_app_api_client }
  let(:response_double) { double(response) }
  let(:ascap)           { PerformingRightsOrganization.find_by(name: "ASCAP") }
  let(:amcos_pro) do
    {
      id:         "39ea3ed9-5b98-cbfa-90b7-6f1de7d857d6",
      name:       "AMCOS",
      cicaccode:  21,
      isdeleted:  false
    }.with_indifferent_access
  end
  let(:response) do
    {
      Records: [
        {
          id:         "39ea3ed9-5b98-d775-661d-e356f7851ed0",
          name:       "ASCAP",
          cicaccode:  10,
          isdeleted:  false
        },
        {
          id:         "39ea3ed9-5b98-c09f-32a4-2c7d9bdb8361",
          name:       "ASSCAT",
          cicaccode:  21,
          isdeleted:  false
        },
        amcos_pro
      ],
      TotalCount: 3
    }.with_indifferent_access
  end

  context "#list" do

    before :each do
      allow(api_client).to receive(:send_request).and_return(response_double)
      allow(response_double).to receive(:errors?).and_return(false)
      allow(response_double).to receive(:total_count).and_return(response[:TotalCount])
      allow(response_double)
        .to receive(:societies)
        .and_return(response[:Records])
    end

    it "updates an existing PRO's provider identifier to match the Rights App Society ID" do
      PublishingAdministration::ApiClientServices::SocietyListService.list
      expect(ascap.provider_identifier).to eq response[:Records].first[:id]
    end

    it "creates a new PRO if it does not exist in Tunecore" do
      expect(PerformingRightsOrganization.exists?(name: "ASSCAT")).to be false
      PublishingAdministration::ApiClientServices::SocietyListService.list
      expect(PerformingRightsOrganization.exists?(name: "ASSCAT")).to be true
    end
  end

  it "does not create certain PRO's listed in the PRO_NAMES_TO_SKIP contant" do
    expect(PerformingRightsOrganization)
      .not_to receive(:create)
      .with(
        name: amcos_pro["name"],
        provider_identifier: amcos_pro["id"],
        is_deleted: amcos_pro["isdeleted"],
        cicaccode: amcos_pro["cicaccode"],
       )

    PublishingAdministration::ApiClientServices::SocietyListService.list
  end
end
