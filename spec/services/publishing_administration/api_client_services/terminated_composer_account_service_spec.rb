require "rails_helper"

describe PublishingAdministration::ApiClientServices::TerminatedComposerAccountService do
  let(:account)             { create(:account) }
  let(:composer)            { create(:composer, account: account) }
  let(:ft_composer)         { create(:terminated_composer, :fully, composer: composer) }
  let(:pt_composer)         { create(:terminated_composer, :partially, composer: composer) }
  let(:artist_acct_code)    { "TCM0000047717A" }
  let(:service)             { PublishingAdministration::ApiClientServices::TerminatedComposerAccountService }
  let(:api_client)          { $rights_app_api_client }
  let(:response_double)     { double(:response_double) }
  let(:response) {
    { ArtistAccountCode: artist_acct_code,
      AccountName: "Dana Czinsky",
      FacebookPageUrl: nil,
      TwitterProfileUrl: nil,
      SoundcloudPageUrl: nil,
      ArtistAccountRegistrationDate: "2018-12-07T16:34:57.5684858Z",
      ReleasedToServiceOrStore: false,
      PerformedLiveLastSixMonths: false,
      CatalogueCode: "TCM0000000001C",
      IsDeleted: false,
      Messages: [] }.to_json
  }

  context "#post_terminated_composer" do
    before :each do
      allow(api_client).to receive(:send_request).and_return(response_double)
      allow(response_double).to receive(:body).and_return(response)
      allow(response_double).to receive(:artist_account_code).and_return(artist_acct_code)
    end

    context "when the account is not blocked" do
      it "creates a new Sentric Artist Account for a partially terminated composer" do
        expect(pt_composer.alt_provider_acct_id.present?).to be false

        expect{
          service.post_terminated_composer(terminated_composer: pt_composer)
        }.to change(pt_composer, :alt_provider_acct_id).from(nil)

        expect(pt_composer.alt_provider_acct_id).to eq artist_acct_code
      end
    end

    context "when the account is blocked" do
      let(:account) { create(:account, blocked: true) }

      it "does not create a new Sentric Artist Account for a partially terminated composer" do
        expect(pt_composer.alt_provider_acct_id.present?).to be false

        expect{
          service.post_terminated_composer(terminated_composer: pt_composer)
        }.to_not change(pt_composer, :alt_provider_acct_id).from(nil)

        expect(pt_composer.alt_provider_acct_id).to_not eq artist_acct_code
      end
    end


    it "does nothing with a fully terminated composer" do
      expect(ft_composer.alt_provider_acct_id.present?).to be false

      expect{
        service.post_terminated_composer(terminated_composer: ft_composer)
      }.not_to change(ft_composer, :alt_provider_acct_id).from(nil)
    end
  end

end
