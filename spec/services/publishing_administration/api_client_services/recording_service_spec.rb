require "rails_helper"

describe PublishingAdministration::ApiClientServices::RecordingService do
  let(:account)         { create(:account, provider_account_id: "TCM0000000028A") }
  let(:composer)        { create(:composer, account: account, provider_identifier: "00000008N") }
  let(:composition)     { create(:composition, provider_identifier: "TCM0000000104S") }
  let(:album)           { create(:album) }
  let(:song)            { create(:song, album: album, composition_id: composition.id) }
  let(:recording_id)    { "39e92091-f0b9-ac83-9f1c-bb23695b4cde" }
  let(:params)          { { composition: composition, composer: composer } }
  let(:api_client)      { double(:api_client) }
  let(:recording_response) do
    { RecordingId: recording_id,
      WorkCode: composition.provider_identifier,
      Title: song.name,
      PerformingArtist: song.artist.name,
      Isrc: song.isrc,
      RecordLabel: album.label.name,
      ReleaseDate: album.orig_release_date,
      IsDeleted: false,
      IsLocked: false,
      ChangesPending: false,
      Messages: [] }
  end
  let(:response_dbl) { double(body: recording_response.to_json, headers: { etag: "etag" }) }

  describe "#post_recording" do
    before :each do
      response = RightsApp::Responses::Recording.new(response_dbl)
      allow(api_client).to receive(:send_request).and_return(double(e_tag: "etag" ), response)
    end

    context "when the account is not blocked" do
      context "composition has associated song or non_tunecore_song" do
        it "creates a recording and sets the recording_code and recordable" do
          service = PublishingAdministration::ApiClientServices::RecordingService.new(params)
          service.instance_variable_set(:@api_client, api_client)

          expect {
            service.post_recording
          }.to change {
            composition.recordings.count
          }.by(1)

          recording = composition.reload.recordings.first
          expect(recording.recording_code).to eq recording_id
          expect(recording.recordable).to eq song
        end

        it "creates a Sentric Work if one does not exist for the composition" do
          composition.update(provider_identifier: nil)
          service = PublishingAdministration::ApiClientServices::RecordingService.new(params)
          service.instance_variable_set(:@api_client, api_client)

          expect(PublishingAdministration::ApiClientServices::WorkCreationService).to receive(:post_work).with(params)
          service.post_recording
        end
      end

      context "composition has a recording without a recording_code" do
        it "creates a recording_code for the recording" do
          service = PublishingAdministration::ApiClientServices::RecordingService.new(params)
          service.instance_variable_set(:@api_client, api_client)
          recording = composition.recordings.create(recording_code: nil, recordable: composition.song)
          composition.reload

          service.post_recording

          expect(recording.reload.recording_code).not_to be nil
        end
      end
    end

    context "when the account is blocked" do
      let(:account) { create(:account, provider_account_id: "TCM0000000028A", blocked: true) }

      context "composition has associated song or non_tunecore_song" do
        it "does not create a recording and set the recording_code and recordable" do
          service = PublishingAdministration::ApiClientServices::RecordingService.new(params)
          service.instance_variable_set(:@api_client, api_client)

          expect {
            service.post_recording
          }.to_not change {
            composition.recordings.count
          }
        end

        it "does not create a Sentric Work if one does not exist for the composition" do
          composition.update(provider_identifier: nil)
          service = PublishingAdministration::ApiClientServices::RecordingService.new(params)
          service.instance_variable_set(:@api_client, api_client)

          expect(PublishingAdministration::ApiClientServices::WorkCreationService).to_not receive(:post_work).with(params)
          service.post_recording
        end
      end

      context "composition is marked ineligible" do
        it "does not create a Sentric Work if one does not exist for the composition" do
          composition.update(provider_identifier: nil, state: :ineligible)
          service = PublishingAdministration::ApiClientServices::RecordingService.new(params)
          service.instance_variable_set(:@api_client, api_client)

          expect(PublishingAdministration::ApiClientServices::WorkCreationService).to_not receive(:post_work).with(params)
          service.post_recording
        end
      end

      context "composition has a recording without a recording_code" do
        it "does not create a recording_code for the recording" do
          service = PublishingAdministration::ApiClientServices::RecordingService.new(params)
          service.instance_variable_set(:@api_client, api_client)
          recording = composition.recordings.create(recording_code: nil, recordable: composition.song)
          composition.reload

          service.post_recording

          expect(recording.reload.recording_code).to be nil
        end
      end
    end
  end

  context "compositions does not have an associated song or non_tunecore_song" do
    it "does not do anything" do
      other_composition = create(:composition)
      params = { composition: other_composition, composer: composer }
      service = PublishingAdministration::ApiClientServices::RecordingService.new(params)
      service.instance_variable_set(:@api_client, api_client)

      expect { service.post_recording }.not_to change { composition.recordings.count }
    end
  end
end
