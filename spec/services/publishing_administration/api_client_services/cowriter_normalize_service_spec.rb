require "rails_helper"

describe PublishingAdministration::ApiClientServices::CowriterNormalizeService do
  let(:api_client)  { $rights_app_api_client }
  let(:service)     { PublishingAdministration::ApiClientServices::CowriterNormalizeService }
  let(:writer_code) { Faker::Number.number(digits: 8) }
  let(:account)     { create(:account) }
  let(:composer)    { create(:composer, account: account, provider_identifier:  Faker::Number.number(digits: 8)) }
  let(:cowriter)    { create(:cowriter, composer: composer, provider_identifier: writer_code) }

  describe "#reconcile_cowriters" do
    context "Rights App Writer matches TC cowriter" do
      it "updates the TC cowriter if matched by WorkCode/provider_identifier" do
        response = double(:response, {
          first_name: "Michelle",
          last_name: "Obama",
          writer_code: writer_code,
          needs_update?: true,
          unknown?: false
        })
        allow(api_client).to receive(:send_request).and_return(response)

        params = {
          account: account,
          cowriters: [cowriter],
          api_cowriters: [],
          api_composers: []
        }

        expect { service.reconcile_cowriters(params) }.to_not change { composer.cowriters.count }
        expect(cowriter.reload.first_name).to eq response.first_name
        expect(cowriter.last_name).to eq response.last_name
      end

      it "updates the TC cowriter if matched by first name and last name" do
        first_name = "Michelle"
        last_name = "Obama"
        new_writer_code = Faker::Number.number(digits: 8)
        cowriter.update({
          first_name: first_name,
          last_name: last_name,
          provider_identifier: Faker::Number.number(digits: 8)
        })
        response = double(:response, {
          first_name: first_name,
          last_name: last_name,
          writer_code: new_writer_code,
          needs_update?: true,
          unknown?: false
        })
        allow(api_client).to receive(:send_request).and_return(response)

        params = {
          account:        account,
          cowriters:      [cowriter],
          api_cowriters:  [],
          api_composers:  []
        }

        expect { service.reconcile_cowriters(params) }.to_not change { composer.cowriters.count }
        expect(cowriter.reload.provider_identifier).to eq new_writer_code.to_s
      end
    end

    context "Rights App Writer does not match TC cowriter" do
      it "compares TC cowriters with RightsApp cowriters and creates a new TC cowriter if needed" do
        first_name = "Michelle"
        last_name = "Obama"
        new_writer_code = Faker::Number.number(digits: 8)
        new_cowriter = double(:api_cowriter, {
          first_name: first_name,
          last_name: last_name,
          writer_code: new_writer_code,
        })
        response = double(:response, {
          first_name: first_name,
          last_name: last_name,
          writer_code: new_writer_code,
          needs_update?: true,
          unknown?: false
        })
        allow(api_client).to receive(:send_request).and_return(response)

        params = {
          account:        account,
          cowriters:      [],
          api_cowriters:  [new_cowriter],
          api_composers:  [double(:api_composers, writer_code: composer.provider_identifier)]
        }

        expect { service.reconcile_cowriters(params) }.to change { Cowriter.count }.by(1)
        expect(Cowriter.exists?({
          first_name: first_name,
          last_name: last_name,
          provider_identifier: new_writer_code
        })).to be_truthy
      end
    end

    describe "#names_match?" do
      context "when a cowriter's first name is nil" do
        it "returns false" do
          cowriter = build(:cowriter, first_name: nil, last_name: nil)

          service = PublishingAdministration::ApiClientServices::CowriterNormalizeService.new({})

          expect(service.send(:names_match?, cowriter, double(:response))).to be false
        end
      end

      context "when a cowriter's first and last name is present and the names match the response" do
        it "returns true" do
          first_name = Faker::Name.first_name
          last_name = Faker::Name.last_name
          cowriter = build(:cowriter, first_name: first_name, last_name: last_name)
          response = double(:response, first_name: first_name, last_name: last_name)

          service = PublishingAdministration::ApiClientServices::CowriterNormalizeService.new({})

          expect(service.send(:names_match?, cowriter, response)).to be true
        end
      end
    end
  end
end
