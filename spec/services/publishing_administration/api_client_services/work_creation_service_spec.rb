require "rails_helper"

describe PublishingAdministration::ApiClientServices::WorkCreationService do
  let(:work_code)       { "TCM0000000104S" }
  let(:account)         { create(:account, provider_account_id: "TCM0000000028A") }
  let(:composer)        { create(:composer, account: account, provider_identifier: "00000008N") }
  let(:cowriter)        { create(:cowriter, composer: composer, provider_identifier: "0000000DC") }
  let(:composition)     { create(:composition, name: "Night Man Cometh") }
  let(:service)         { PublishingAdministration::ApiClientServices::WorkCreationService }
  let(:params)          { { composer: composer, composition: composition } }
  let(:api_client)      { $rights_app_api_client }
  let(:response_double) { double(:response_double) }
  let!(:composer_split) do
    create(
      :publishing_split,
      composition_id: composition.id,
      composer: composer,
      writer_id: composer.id,
      writer_type: "Composer",
      percent: 50.50
    )
  end
  let!(:cowriter_split) do
    create(
      :publishing_split,
      composition_id: composition.id,
      composer: composer,
      writer_id: cowriter.id,
      writer_type: "Cowriter",
      percent: 49.50
    )
  end
  let(:response) do
    {
      WorkCode:           work_code,
      ArtistAccountCode:  account.provider_account_id,
      Title:              composition.name,
      IsRemix:            false,
      ContainsSamples:    false,
      WriterSplits: [
        {
          WriterCode:             composer.provider_identifier,
          WriterDesignationCode:  2,
          RightToCollect:         true,
          WriterShare:            composer_split.percent
        },
        {
          WriterCode:             cowriter.provider_identifier,
          WriterDesignationCode:  1,
          RightToCollect:         false,
          WriterShare:            cowriter_split.percent
        }
      ]
    }
  end

  describe "#post_work" do
    context "when the account is not blocked" do
      context "composition already exists in RA" do
        it "updates the provider identifier to the existing work code" do
          work_code = Faker::Number.number(digits: 8)
          response = double(:response, already_exists?: true, existing_work_code: work_code, body: nil)
          allow(api_client).to receive(:send_request).and_return(response)

          expect {
            service.post_work(params)
          }.to change {
            composition.provider_identifier
          }.from(nil).to(work_code.to_s)
        end
      end

      context "composition has valid splits" do
        it "creates a Sentric Work, receives a WorkCode, and sets it as the Composition's provider_identifier" do
          allow(api_client).to receive(:send_request).and_return(response_double)
          allow(response_double).to receive(:body).and_return(response)
          allow(response_double).to receive(:work_code).and_return(work_code)
          allow(response_double).to receive(:already_exists?).and_return(false)

          expect { service.post_work(params) }.to change(composition, :provider_identifier).from(nil)
        end

        it "creates Sentric writers for unsent cowriters" do
          allow(api_client).to receive(:send_request).and_return(response_double)
          allow(response_double).to receive(:body).and_return(response)
          allow(response_double).to receive(:work_code).and_return(work_code)
          allow(response_double).to receive(:already_exists?).and_return(false)

          cowriter.update(provider_identifier: nil)

          expect(PublishingAdministration::ApiClientServices::WriterService)
            .to receive(:create_or_update)
            .and_return(true)

          service.post_work(params)
        end
      end

      context "composition has a split with 0 percent" do
        it "does not create a Sentric Work" do
          allow(api_client).to receive(:send_request).and_return(response_double)
          allow(response_double).to receive(:body).and_return(response)
          allow(response_double).to receive(:work_code).and_return(work_code)
          allow(response_double).to receive(:already_exists?).and_return(false)

          other_cowriter = create(:cowriter, composer: composer, provider_identifier: "0000000CZ")
          create(
            :publishing_split,
            composition_id: composition.id,
            composer: composer,
            writer: other_cowriter,
            percent: 0.0
          )

          expect { service.post_work(params) }.not_to change(composition, :provider_identifier).from nil
        end
      end
    end

    context "when the account is blocked" do
      let(:account) { create(:account, provider_account_id: "TCM0000000028A", blocked: true) }

      context "composition already exists in RA" do
        it "does not update the provider identifier to the existing work code" do
          work_code = Faker::Number.number(digits: 8)
          response = double(:response, already_exists?: true, existing_work_code: work_code, body: nil)
          allow(api_client).to receive(:send_request).and_return(response)

          expect {
            service.post_work(params)
          }.to_not change {
            composition.provider_identifier
          }
        end
      end

      context "composition has valid splits" do
        it "does not create a Sentric Work, receive a WorkCode, and set it as the Composition's provider_identifier" do
          allow(api_client).to receive(:send_request).and_return(response_double)
          allow(response_double).to receive(:body).and_return(response)
          allow(response_double).to receive(:work_code).and_return(work_code)
          allow(response_double).to receive(:already_exists?).and_return(false)

          expect { service.post_work(params) }.to_not change(composition, :provider_identifier).from(nil)
        end

        it "does not create Sentric writers for unsent cowriters" do
          allow(api_client).to receive(:send_request).and_return(response_double)
          allow(response_double).to receive(:body).and_return(response)
          allow(response_double).to receive(:work_code).and_return(work_code)
          allow(response_double).to receive(:already_exists?).and_return(false)

          cowriter.update(provider_identifier: nil)

          expect(PublishingAdministration::ApiClientServices::WriterService)
            .to_not receive(:create_or_update)

          service.post_work(params)
        end
      end

      context "composition has a split with 0 percent" do
        it "does not create a Sentric Work" do
          allow(api_client).to receive(:send_request).and_return(response_double)
          allow(response_double).to receive(:body).and_return(response)
          allow(response_double).to receive(:work_code).and_return(work_code)
          allow(response_double).to receive(:already_exists?).and_return(false)

          other_cowriter = create(:cowriter, composer: composer, provider_identifier: "0000000CZ")
          create(
            :publishing_split,
            composition_id: composition.id,
            composer: composer,
            writer: other_cowriter,
            percent: 0.0
          )

          expect { service.post_work(params) }.to_not change(composition, :provider_identifier).from nil
        end
      end
    end

    context "composition is marked ineligible" do
      it "should not send post work api call" do
        composition.update(state: :ineligible)

        expect(api_client).to_not receive(:send_request)
        service.post_work(params)
      end
    end
  end

  describe "#update_work" do
    let(:params) { { composition: composition } }

    context "when the account is not blocked" do
      context "composition has a split with 0 percent" do
        it "does not create a Sentric Work" do
          allow(api_client).to receive(:send_request).and_return(response_double)

          other_cowriter = create(:cowriter, composer: composer, provider_identifier: "0000000CZ")
          create(
            :publishing_split,
            composition_id: composition.id,
            composer: composer,
            writer: other_cowriter,
            percent: 0.0
          )

          expect(api_client).to_not receive(:send_request)
          service.update_work(params)
        end
      end

      context "composition has valid splits" do
        before do
          e_tag_response = response_double
          allow(api_client).to receive(:send_request).with("GetWork", anything).and_return(e_tag_response)
          allow(e_tag_response).to receive(:e_tag).and_return('abc123')
        end

        it "updates Sentric about the update" do
          allow(api_client).to receive(:send_request).with("UpdateWork", anything).and_return(response_double)
          allow(response_double).to receive(:body).and_return(response)

          service.update_work(params)
        end

        it "creates Sentric writers for unsent cowriters" do
          allow(api_client).to receive(:send_request).and_return(response_double)
          allow(response_double).to receive(:body).and_return(response)
          allow(response_double).to receive(:work_code).and_return(work_code)
          allow(response_double).to receive(:already_exists?).and_return(false)

          cowriter.update(provider_identifier: nil)

          expect(PublishingAdministration::ApiClientServices::WriterService)
            .to receive(:create_or_update)
            .and_return(true)

          service.update_work(params)
        end
      end
    end

    context "when the account is blocked" do
      let(:account) { create(:account, provider_account_id: "TCM0000000028A", blocked: true) }

      context "composition has a split with 0 percent" do
        it "does not create a Sentric Work" do
          allow(api_client).to receive(:send_request).and_return(response_double)

          other_cowriter = create(:cowriter, composer: composer, provider_identifier: "0000000CZ")
          create(
            :publishing_split,
            composition_id: composition.id,
            composer: composer,
            writer: other_cowriter,
            percent: 0.0
          )

          expect(api_client).to_not receive(:send_request)
          service.update_work(params)
        end
      end

      context "composition has valid splits" do
        before do
          e_tag_response = response_double
          allow(api_client).to receive(:send_request).with("GetWork", anything).and_return(e_tag_response)
          allow(e_tag_response).to receive(:e_tag).and_return('abc123')
        end

        it "does not update Sentric about the update" do
          expect(api_client).to_not receive(:send_request).with("UpdateWork", anything)

          service.update_work(params)
        end

        it "does not create Sentric writers for unsent cowriters" do
          allow(api_client).to receive(:send_request).and_return(response_double)
          allow(response_double).to receive(:body).and_return(response)
          allow(response_double).to receive(:work_code).and_return(work_code)
          allow(response_double).to receive(:already_exists?).and_return(false)

          cowriter.update(provider_identifier: nil)

          expect(PublishingAdministration::ApiClientServices::WriterService)
            .to_not receive(:create_or_update)

          service.update_work(params)
        end
      end
    end

    context "composition is marked ineligible" do
      it "should not send update work api call" do
        composition.update(state: :ineligible)

        expect(api_client).to_not receive(:send_request)
        service.update_work(params)
      end
    end
  end
end
