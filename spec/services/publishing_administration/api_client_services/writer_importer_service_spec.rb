require "rails_helper"

describe PublishingAdministration::ApiClientServices::WriterImporterService do
  describe ".import" do
    context "when the account is not blocked" do
      context "when the writer does NOT need to be updated" do
        it "should not update the composer" do
          composer = create(:composer, :with_provider_identifier)

          importer = PublishingAdministration::ApiClientServices::WriterImporterService.new(composer)

          api_client = double(:api_client)
          importer.instance_variable_set(:@api_client, api_client)

          response = double(:response, needs_update?: false)
          allow(api_client).to receive(:send_request) { response }

          expect(composer).not_to receive(:update)

          importer.import
        end
      end

      context "when the writer needs to be updated" do
        it "should update the composer" do
          composer = create(:composer,
            :with_provider_identifier,
            first_name: "Tom",
            last_name: "Brady",
            cae: nil,
            performing_rights_organization: nil,
          )

          importer = PublishingAdministration::ApiClientServices::WriterImporterService.new(composer)

          api_client = double(:api_client)
          importer.instance_variable_set(:@api_client, api_client)
          society_id = double(:society_id, id: 1)
          allow(PerformingRightsOrganization).to receive(:find_by) { society_id }

          response = double(:response,
            first_name: "Aaron",
            middle_name: "",
            last_name:  "Rodgers",
            cae: "123456789",
            society_id: society_id,
            writer_code: "00002LENW",
            needs_update?: true,
            has_publisher?: false
          )
          allow(api_client).to receive(:send_request) { response }

          importer.import

          expect(composer.reload.first_name).to eq response.first_name
          expect(composer.last_name).to eq response.last_name
          expect(composer.cae).to eq response.cae
          expect(composer.provider_identifier).to eq response.writer_code
          expect(composer.performing_rights_organization_id).to eq society_id.id
        end
      end

      context "when the writer needs to be updated and has publisher information" do
        it "should update the publisher" do
          composer = create(:composer, :with_provider_identifier)

          importer = PublishingAdministration::ApiClientServices::WriterImporterService.new(composer)

          api_client = double(:api_client)
          importer.instance_variable_set(:@api_client, api_client)

          response = double(:response,
            first_name: "Aaron",
            middle_name: "",
            last_name:  "Rodgers",
            cae: "123456789",
            society_id: "39e6a3a9-52db-f127-0de7-b56cc1266c30",
            writer_code: "00002LENW",
            publisher_name: "Pub Company",
            publisher_cae: "123456789",
            needs_update?: true,
            has_publisher?: true
          )
          allow(api_client).to receive(:send_request) { response }

          importer.import

          expect(composer.reload.publisher.name).to eq response.publisher_name
          expect(composer.publisher.cae).to eq response.publisher_cae
        end
      end

      context "when the writer does NOT have a writer code" do
        it "creates a writer code" do
          composer = create(:composer, provider_identifier: nil)

          importer = PublishingAdministration::ApiClientServices::WriterImporterService.new(composer)

          api_client = double(:api_client)
          importer.instance_variable_set(:@api_client, api_client)
          response = double(:response, needs_update?: false)
          allow(api_client).to receive(:send_request) { response }

          expect(PublishingAdministration::ApiClientServices::WriterCodeService).to receive(:create_or_update).with(composer)

          importer.import
        end
      end
    end

    context "when the account is blocked" do
      let(:account) { create(:account, blocked: true) }

      context "when the writer does NOT need to be updated" do
        it "should not update the composer" do
          composer = create(:composer, account: account)

          importer = PublishingAdministration::ApiClientServices::WriterImporterService.new(composer)

          api_client = double(:api_client)
          importer.instance_variable_set(:@api_client, api_client)

          response = double(:response, needs_update?: false)
          allow(api_client).to receive(:send_request) { response }

          expect(composer).not_to receive(:update)

          importer.import
        end
      end

      context "when the writer needs to be updated" do
        it "should not update the composer" do
          composer = create(:composer,
            first_name: "Tom",
            last_name: "Brady",
            cae: nil,
            provider_identifier: nil,
            performing_rights_organization: nil,
            account: account,
          )

          importer = PublishingAdministration::ApiClientServices::WriterImporterService.new(composer)

          api_client = double(:api_client)
          importer.instance_variable_set(:@api_client, api_client)
          society_id = double(:society_id, id: 1)
          allow(PerformingRightsOrganization).to receive(:find_by) { society_id }

          response = double(:response,
            first_name: "Aaron",
            middle_name: "",
            last_name:  "Rodgers",
            cae: "123456789",
            society_id: society_id,
            writer_code: "00002LENW",
            needs_update?: true,
            has_publisher?: false
          )
          allow(api_client).to receive(:send_request) { response }

          importer.import

          expect(composer.reload.first_name).to_not eq response.first_name
          expect(composer.last_name).to_not eq response.last_name
          expect(composer.cae).to_not eq response.cae
          expect(composer.provider_identifier).to_not eq response.writer_code
          expect(composer.performing_rights_organization_id).to_not eq society_id.id
        end
      end

      context "when the writer needs to be updated and has publisher information" do
        it "should not update the publisher" do
          composer = create(:composer, account: account)

          importer = PublishingAdministration::ApiClientServices::WriterImporterService.new(composer)

          api_client = double(:api_client)
          importer.instance_variable_set(:@api_client, api_client)

          response = double(:response,
            first_name: "Aaron",
            middle_name: "",
            last_name:  "Rodgers",
            cae: "123456789",
            society_id: nil,
            writer_code: "00002LENW",
            publisher_name: "Pub Company",
            publisher_cae: "123456789",
            needs_update?: true,
            has_publisher?: true
          )
          allow(api_client).to receive(:send_request) { response }

          importer.import

          expect(composer.reload.publisher).to eq(nil)
        end
      end

      context "when the writer does NOT have a writer code" do
        it "does not create a writer code" do
          composer = create(:composer, provider_identifier: nil, account: account)

          importer = PublishingAdministration::ApiClientServices::WriterImporterService.new(composer)

          api_client = double(:api_client)
          importer.instance_variable_set(:@api_client, api_client)
          response = double(:response, needs_update?: false)
          allow(api_client).to receive(:send_request) { response }

          expect(PublishingAdministration::ApiClientServices::WriterCodeService).to_not receive(:create_or_update).with(composer)

          importer.import
        end
      end
    end
  end
end
