require "rails_helper"

describe PublishingAdministration::ApiClientServices::CowritingPublishingComposerNormalizeService do
  let(:api_client)  { $rights_app_api_client }
  let(:service)     { PublishingAdministration::ApiClientServices::CowritingPublishingComposerNormalizeService }
  let(:writer_code) { Faker::Number.number(digits: 8) }
  let(:account)     { create(:account) }
  let(:publishing_composer) { create(:publishing_composer, account: account, provider_identifier: Faker::Number.number(digits: 8)) }
  let(:cowriting_publishing_composer) { create(:publishing_composer, :cowriter, account: account, provider_identifier: writer_code) }

  describe "#reconcile_cowriting_publishing_composers" do
    context "Rights App Writer matches TC cowriting_publishing_composer" do
      it "updates the TC cowriting_publishing_composer if matched by WorkCode/provider_identifier" do
        response = double(:response, {
          first_name: "Michelle",
          last_name: "Obama",
          writer_code: writer_code,
          needs_update?: true,
          unknown?: false
        })
        allow(api_client).to receive(:send_request).and_return(response)

        params = {
          account: account,
          cowriting_publishing_composers: [cowriting_publishing_composer],
          api_cowriting_publishing_composers: [],
          api_publishing_composers: []
        }

        expect { service.reconcile_cowriting_publishing_composers(params) }.to_not change { account.cowriting_composers.count }
        expect(cowriting_publishing_composer.reload.first_name).to eq response.first_name
        expect(cowriting_publishing_composer.last_name).to eq response.last_name
      end

      it "updates the TC cowriting_publishing_composer if matched by first name and last name" do
        first_name = "Michelle"
        last_name = "Obama"
        new_writer_code = Faker::Number.number(digits: 8)
        cowriting_publishing_composer.update({
          first_name: first_name,
          last_name: last_name,
          provider_identifier: Faker::Number.number(digits: 8)
        })
        response = double(:response, {
          first_name: first_name,
          last_name: last_name,
          writer_code: new_writer_code,
          needs_update?: true,
          unknown?: false
        })
        allow(api_client).to receive(:send_request).and_return(response)

        params = {
          account:        account,
          cowriting_publishing_composers:      [cowriting_publishing_composer],
          api_cowriting_publishing_composers:  [],
          api_publishing_composers:  []
        }

        expect { service.reconcile_cowriting_publishing_composers(params) }.to_not change { account.cowriting_composers.count }
        expect(cowriting_publishing_composer.reload.provider_identifier).to eq new_writer_code.to_s
      end
    end

    context "Rights App Writer does not match TC cowriting_publishing_composer" do
      it "compares TC cowriting_publishing_composers with RightsApp cowriting_publishing_composers and creates a new TC cowriting_publishing_composer if needed" do
        first_name = "Michelle"
        last_name = "Obama"
        new_writer_code = Faker::Number.number(digits: 8)
        new_cowriting_publishing_composer = double(:api_cowriting_publishing_composer, {
          first_name: first_name,
          last_name: last_name,
          writer_code: new_writer_code,
        })
        response = double(:response, {
          first_name: first_name,
          last_name: last_name,
          writer_code: new_writer_code,
          needs_update?: true,
          unknown?: false
        })
        allow(api_client).to receive(:send_request).and_return(response)

        params = {
          account:        account,
          cowriting_publishing_composers:      [],
          api_cowriting_publishing_composers:  [new_cowriting_publishing_composer],
          api_publishing_composers:  [double(:api_publishing_composers, writer_code: publishing_composer.provider_identifier)]
        }

        expect { service.reconcile_cowriting_publishing_composers(params) }.to change { PublishingComposer.cowriting.count }.by(1)
        expect(PublishingComposer.cowriting.exists?({
          first_name: first_name,
          last_name: last_name,
          provider_identifier: new_writer_code
        })).to be_truthy
      end
    end

    describe "#names_match?" do
      context "when a cowriting_publishing_composer's first name is nil" do
        it "returns false" do
          cowriting_publishing_composer = build(:publishing_composer, :cowriter, first_name: nil, last_name: nil)

          service = PublishingAdministration::ApiClientServices::CowritingPublishingComposerNormalizeService.new({})

          expect(service.send(:names_match?, cowriting_publishing_composer, double(:response))).to be false
        end
      end

      context "when a cowriting_publishing_composer's first and last name is present and the names match the response" do
        it "returns true" do
          first_name = Faker::Name.first_name
          last_name = Faker::Name.last_name
          cowriting_publishing_composer = build(:publishing_composer, :cowriter, first_name: first_name, last_name: last_name)
          response = double(:response, first_name: first_name, last_name: last_name)

          service = PublishingAdministration::ApiClientServices::CowritingPublishingComposerNormalizeService.new({})

          expect(service.send(:names_match?, cowriting_publishing_composer, response)).to be true
        end
      end
    end
  end
end
