require "rails_helper"

describe PublishingAdministration::ApiClientServices::GetPublishingWorkService do
  describe "#writers_with_right_to_collect" do
    it "retrieves the writers on the work with the right to collect" do
      writers_with_right_to_collect = [
        {"WriterCode"=>"00001CKJW", "WriterDesignationCode"=>2, "WriterShare"=>50.0, "RightToCollect"=>true, "WriterShareId"=>"39f01960-b179-f489-2f2f-c1da06614e1f"},
        {"WriterCode"=>"00001CKKW", "WriterDesignationCode"=>2, "WriterShare"=>20.0, "RightToCollect"=>true, "WriterShareId"=>"39f01960-b1a8-2276-d4e3-d766a229e0a9"}
      ]
      writer_without_right_to_collect = [
        {"WriterCode"=>"00001CKMW", "WriterDesignationCode"=>0, "WriterShare"=>10.0, "RightToCollect"=>false, "WriterShareId"=>"39f01960-b1b7-02b8-67ee-470a23610e6f"}
      ]
      work = {
        "WorkCode"=>"TCM0000003360S",
        "Title"=>"Rights App Song",
        "WriterSplits"=>[
          writers_with_right_to_collect,
          writer_without_right_to_collect
        ].flatten
      }

      response   = double(:response, body: work)
      api_client = double(:api_client)

      work_code = "TCM0000003360S"
      work_service = PublishingAdministration::ApiClientServices::GetPublishingWorkService.new(work_code)
      work_service.instance_variable_set(:@api_client, api_client)

      allow(api_client).to receive(:send_request) { response }

      result = work_service.writers_with_right_to_collect

      expect(result).to match_array writers_with_right_to_collect
      expect(result).not_to match_array writer_without_right_to_collect
    end
  end
end
