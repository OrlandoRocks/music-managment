require "rails_helper"

describe PublishingAdministration::ApiClientServices::FetchWorkListService do
  describe "#fetch" do
    let(:account)     { create(:account, :with_account_id) }
    let(:composer)    { create(:composer, account: account) }
    let(:album)       { create(:album, person: account.person) }
    let(:composition) { create(:composition, provider_identifier: "DMC0000000W") }
    let(:song)        { create(:song, album: album, composition_id: composition) }

    it "returns a list of works for the associated rights app account" do
      works = [
        {
          WorkCode: composition.provider_identifier,
          Title: "No Soggy Bottoms",
          Writers: [
            {
              FirstName:      "Mary",
              LastName:       "Berry",
              UnknownWriter:  false,
              WriterShare:    100
            }
          ],
          Recordings: 1
        }.with_indifferent_access
      ]

      response   = double(:response, works: works, errors?: true, total_count: 1)
      api_client = double(:api_client)

      work_list = PublishingAdministration::ApiClientServices::FetchWorkListService.new(account.provider_account_id)
      work_list.instance_variable_set(:@api_client, api_client)

      allow(api_client).to receive(:send_request) { response }

      work_list.fetch

      expect(work_list.works.flatten).to eq(response.works)
    end
  end

  describe "#valid?" do
    it "returns false if the response has errors" do
      account    = build(:account, :with_account_id)
      response   = double(:response, works: [{"WorkCode" => "1"}], errors?: true, total_count: 1)
      api_client = double(:api_client)

      work_list = PublishingAdministration::ApiClientServices::FetchWorkListService.new(account.provider_account_id)
      work_list.instance_variable_set(:@api_client, api_client)

      allow(api_client).to receive(:send_request) { response }

      work_list.fetch

      expect(work_list.valid?).to be_falsey
    end

    it "returns true if the response does NOT have errors" do
      account    = build(:account, :with_account_id)
      response   = double(:response, works: [{"WorkCode" => "1"}], errors?: false, total_count: 1)
      api_client = double(:api_client)

      work_list = PublishingAdministration::ApiClientServices::FetchWorkListService.new(account.provider_account_id)
      work_list.instance_variable_set(:@api_client, api_client)

      allow(api_client).to receive(:send_request) { response }

      work_list.fetch

      expect(work_list.valid?).to be_truthy
    end
  end

  describe "#works" do
    it "returns an empty array if the work list is blank" do
      account    = build(:account, :with_account_id)
      response   = double(:response, works: [[]], errors?: false, total_count: 1)
      api_client = double(:api_client)

      work_list = PublishingAdministration::ApiClientServices::FetchWorkListService.new(account.provider_account_id)
      work_list.instance_variable_set(:@api_client, api_client)

      allow(api_client).to receive(:send_request) { response }

      expect(work_list.works).to be_empty
    end

    it "returns an empty array if the work list is nil" do
      account    = build(:account, :with_account_id)
      response   = double(:response, works: [[nil]], errors?: false, total_count: 1)
      api_client = double(:api_client)

      work_list = PublishingAdministration::ApiClientServices::FetchWorkListService.new(account.provider_account_id)
      work_list.instance_variable_set(:@api_client, api_client)

      allow(api_client).to receive(:send_request) { response }

      expect(work_list.works).to be_empty
    end
  end

  describe "#work_codes" do
    it "returns a list of work codes" do
      account    = build(:account, :with_account_id)
      api_client = double(:api_client)

      work_list_service = PublishingAdministration::ApiClientServices::FetchWorkListService.new(account.provider_account_id)
      work_list_service.instance_variable_set(:@api_client, api_client)

      works = [
        double(:work, work_code: Faker::Number.number(digits: 8)),
        double(:work, work_code: Faker::Number.number(digits: 8)),
      ]
      response = double(:response, works: works, errors?: false, total_count: 1)

      allow(api_client).to receive(:send_request) { response }

      result = work_list_service.work_codes
      expect(result).to eq works.map(&:work_code)
    end

    it "returns an empty array if the account ID is not present" do
      work_list_service = PublishingAdministration::ApiClientServices::FetchWorkListService.new(nil)
      result = work_list_service.work_codes

      expect(result).to be_empty
    end
  end
end
