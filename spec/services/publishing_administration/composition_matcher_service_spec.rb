require "rails_helper"

describe PublishingAdministration::CompositionMatcherService do
  let(:title)                 { Faker::Book.title }
  let(:api_client)            { $rights_app_api_client }
  let(:work_code)             { "0000000DMC" }
  let!(:composer)             { create(:composer) }
  let!(:cowriter)             { create(:cowriter, composer: composer) }
  let!(:album1)               { create(:album, person: composer.person) }
  let!(:ntc_album)            { create(:non_tunecore_album, composer: composer) }
  let!(:composition1)         { create(:composition, name: title, provider_identifier: work_code) }
  let!(:comp_with_same_name)  { create(:composition, name: title.upcase) }
  let(:params)                { { composer: composer, composition: comp_with_same_name } }
  let!(:song1) do
    create(
      :song,
      album:          album1,
      name:           title,
      composition_id: composition1.id
    )
  end
  let!(:ntc_song) do
    create(
      :non_tunecore_song,
      non_tunecore_album: ntc_album,
      name:               title,
      composition_id:     comp_with_same_name.id
    )
  end
  let!(:composer_split1) do
    create(
      :publishing_split,
      composition_id: composition1.id,
      composer_id:    composer.id,
      writer_type:    "Composer",
      writer_id:      composer.id,
      percent:        50.to_d
    )
  end
  let!(:cowriter_split1) do
    create(
      :publishing_split,
      composition_id: composition1.id,
      composer_id:    composer.id,
      writer_type:    "Cowriter",
      writer_id:      cowriter.id,
      percent:        50.to_d
    )
  end
  let!(:composer_split2) do
    create(
      :publishing_split,
      composition_id: comp_with_same_name.id,
      composer_id:    composer.id,
      writer_type:    "Composer",
      writer_id:      composer.id,
      percent:        50.to_d
    )
  end
  let!(:cowriter_split2) do
    create(
      :publishing_split,
      composition_id: comp_with_same_name.id,
      composer_id:    composer.id,
      writer_type:    "Cowriter",
      writer_id:      cowriter.id,
      percent:        50.to_d
    )
  end

  describe "#match" do
    before :each do
      allow(PublishingAdministration::ApiClientServices::RecordingService).to receive(:post_recording)
    end

    context "composition matches another composition with the same name and publishing splits" do

      it "updates the composition's provider_identifier" do
        expect(comp_with_same_name.provider_identifier).to eq nil
        PublishingAdministration::CompositionMatcherService.match(params)
        expect(comp_with_same_name.provider_identifier).to eq work_code
      end

      it "works the same whether the composition is associated to a song or a non_tunecore_song" do
        comp_with_same_name.update(provider_identifier: work_code)
        composition1.update(provider_identifier: nil)
        params[:composition] = composition1

        expect(composition1.provider_identifier).to eq nil
        PublishingAdministration::CompositionMatcherService.match(params)
        expect(composition1.provider_identifier).to eq work_code
      end

      it "works whether or not the composer has a person_id" do
        composer.update(person_id: nil)
        comp_with_same_name.update(provider_identifier: work_code)

        other_composition = create(:composition, name: title)
        other_params      = { composer: composer, composition: other_composition }
        other_ntc_album   = create(:non_tunecore_album, name: title, composer: composer)
        create(
          :non_tunecore_song,
          non_tunecore_album: other_ntc_album,
          name:               title,
          composition_id:     other_composition.id
        )
        create(
          :publishing_split,
          composition_id:   other_composition.id,
          composer_id:      composer.id,
          writer_type:      "Composer",
          writer_id:        composer.id,
          percent:          50.to_d
        )
        create(
          :publishing_split,
          composition_id:   other_composition.id,
          composer_id:      composer.id,
          writer_type:      "Cowriter",
          writer_id:        cowriter.id,
          percent:          50.to_d
        )

        expect(other_composition.provider_identifier).to eq nil
        PublishingAdministration::CompositionMatcherService.match(other_params)
        expect(other_composition.reload.provider_identifier).to eq work_code
      end

      it "works regardless of writer name casing" do
        composer.update(first_name: composer.first_name.upcase)
        expect(comp_with_same_name.provider_identifier).to eq nil
        PublishingAdministration::CompositionMatcherService.match(params)
        expect(comp_with_same_name.provider_identifier).to eq work_code
      end
    end

    context "composition does not match another composition by name or splits" do

      it "does not update the composition if name differs" do
        comp_with_same_name.update(name: "what's in a name?")
        PublishingAdministration::CompositionMatcherService.match(params)
        expect(comp_with_same_name.provider_identifier).to eq nil
      end

      it "does not update the composition if splits differ" do
        composer_split2.update_attribute(:percent, 75.to_d)
        cowriter_split2.update_attribute(:percent, 25.to_d)

        PublishingAdministration::CompositionMatcherService.match(params)
        expect(comp_with_same_name.provider_identifier).to eq nil
      end

    end

  end
end
