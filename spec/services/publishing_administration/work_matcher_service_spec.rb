require "rails_helper"

describe PublishingAdministration::WorkMatcherService do
  describe ".match" do
    context "when the Work Code matches a composition's provider identifier" do
      it "should include the composition in the matched compositions array" do
        account = create(:account)
        create(:composer, account: account)
        album       = create(:album, person: account.person)
        composition = create(
          :composition,
          name: "No Soggy Bottoms",
          provider_identifier: "DMC0000000W"
        )
        create(:song, album: album, composition: composition)
        work = double(:work, work_code: composition.provider_identifier, title: "No Soggy Bottoms")

        work_matcher = PublishingAdministration::WorkMatcherService.match(account, [work])

        expect(work_matcher.matched_compositions).to include([composition, work])
      end
    end

    context "when the Work Title matches a compositon's name" do
      it "should include the composition in the matched compositions array irregardless of caseing" do
        account = create(:account)
        create(:composer, account: account)
        album       = create(:album, person: account.person)
        composition = create(
          :composition,
          name: "Composition name",
          provider_identifier: nil
        )
        create(:song, album: album, composition: composition)
        work = double(:work, work_code: Faker::Number.number(digits: 8), title: "Composition Name")

        work_matcher = PublishingAdministration::WorkMatcherService.match(account, [work])

        expect(work_matcher.matched_compositions).to include([composition, work])
      end

      context "when the composition is associated to a NonTunecoreSong" do
        it "should be included in the matched array if the composition belongs to an NTC Song" do
          account     = create(:account)
          composer    = create(:composer, account: account)
          composition = create(:composition, name: "Shame about that bake")
          ntc_album   = create(:non_tunecore_album, composer: composer)
          create(:non_tunecore_song, non_tunecore_album: ntc_album, composition: composition)
          work = double(:work, work_code: Faker::Number.number(digits: 8), title: composition.name)

          work_matcher = PublishingAdministration::WorkMatcherService.match(account, [work])

          expect(work_matcher.matched_compositions).to include([composition, work])
        end
      end
    end

    context "when the Work Title matches a compositon's translated name" do
      it "should include the composition in the matched compositions array" do
        account = create(:account)
        create(:composer, account: account)
        album       = create(:album, person: account.person)
        composition = create(
          :composition,
          name: "Amarillo Bodak",
          translated_name: "Bodak Yellow"
        )
        create(:song, album: album, composition: composition)
        work = double(:work, work_code: Faker::Number.number(digits: 8), title: composition.translated_name)

        work_matcher = PublishingAdministration::WorkMatcherService.match(account, [work])

        expect(work_matcher.matched_compositions).to include([composition, work])
      end
    end

    context "when the Work Title matches a the title of a muma song record" do
      it "should include the composition in the matched compositions array" do
        composition = create(:composition, name: "Scrummy")
        account     = create(:account)
        album       = create(:album, person: account.person)
        create(:song, album: album, composition: composition, name: "not scrummy")
        create(:composer, account: account)
        create(:muma_song, composition: composition, title: "Scrummy")
        work = double(:work, work_code: Faker::Number.number(digits: 8), title: "$crummy")

        work_matcher = PublishingAdministration::WorkMatcherService.match(account, [work])

        expect(work_matcher.matched_compositions).to include([composition, work])
      end
    end

    context "when the work doesn't match any records in our database" do
      it "should include the work in the unmatched works array" do
        account = create(:account)
        create(:composer, account: account)
        composition = create(:composition, name: "Number One in the Technical")
        album       = create(:album, person: account.person)
        create(:song, album: album, composition: composition)
        work = double(:work, work_code: Faker::Number.number(digits: 8), title: "Star Baker")

        work_matcher = PublishingAdministration::WorkMatcherService.match(account, [work])

        expect(work_matcher.matched_compositions).to be_empty
        expect(work_matcher.unmatched_works).to include(work)
      end
    end
  end
end
