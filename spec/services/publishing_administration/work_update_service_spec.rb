require 'rails_helper'
include ActiveSupport::Testing::TimeHelpers

describe PublishingAdministration::WorkUpdateService do
  describe ".update" do
    it "should reconcile composers and cowriters" do
      composition = create(:composition, :with_songs)
      person = composition.songs.first.album.person
      account = create(:account, person: person)
      composer = create(:composer, account: account)
      create(:publishing_split, composer: composer, composition: composition)
      create(:publishing_split, composer: composer, composition: composition)
      composer_split = double(:composer_split, right_to_collect: true, share: 0)
      cowriter_split = double(:cowriter_split, right_to_collect: false, share: 0)
      writer_params = {
        account:       account,
        api_composers: [composer_split],
        api_cowriters: [cowriter_split],
        composers:     [composer],
        cowriters:     [],
        free_purchase: false
      }

      work = double(:work,
                    title: Faker::Name.name,
                    writers: [composer_split, cowriter_split],
                    verified_date: Time.current,
                    work_code: '',
                    is_deactivated?: false,
                    number_of_recordings: 0
                   )

      expect(PublishingAdministration::ApiClientServices::ComposerNormalizeService).to receive(:reconcile_composers).with(writer_params)
      expect(PublishingAdministration::ApiClientServices::CowriterNormalizeService).to receive(:reconcile_cowriters).with(writer_params)

      PublishingAdministration::WorkUpdateService.update(composition, work)
    end

    before do
      allow(PublishingAdministration::ApiClientServices::ComposerNormalizeService).to receive(:reconcile_composers)
      allow(PublishingAdministration::ApiClientServices::CowriterNormalizeService).to receive(:reconcile_cowriters)
    end

    context "when comparing splits" do
      let(:composition) { create(:composition, :with_songs) }
      let(:person) { composition.songs.first.album.person }
      let(:account) { create(:account, person: person) }
      let(:composer_writer_code) { Faker::Number.number(digits: 8) }
      let(:cowriter_writer_code) { Faker::Number.number(digits: 8) }
      let(:composer) { create(:composer, account: account, provider_identifier: composer_writer_code) }

      it "should update the split if a writer is associated to a split" do
        cowriter = create(:cowriter, composer: composer)
        composer_pub_split = create(:publishing_split, writer: composer, composer: composer, composition: composition)
        cowriter_pub_split = create(:publishing_split, writer: cowriter, composer: composer, composition: composition, percent: 0)
        composer_split = double(:composer_split, writer_code: composer_writer_code, right_to_collect: true, share: 50)
        cowriter_split = double(:cowriter_split, writer_code: nil, right_to_collect: false, share: 50)
        work = double(:work,
                      title: Faker::Name.name,
                      writers: [composer_split, cowriter_split],
                      verified_date: Time.current,
                      work_code: '',
                      is_deactivated?: false,
                      number_of_recordings: 0
                     )

        PublishingAdministration::WorkUpdateService.update(composition, work)

        expect(composer_pub_split.reload.percent).to eq 50
        expect(cowriter_pub_split.reload.percent).to eq 50
      end

      it "should create a new split if the writer is not associated to the split" do
        cowriter = create(:cowriter, composer: composer, provider_identifier: cowriter_writer_code)
        create(:publishing_split, composer: composer, composition: composition, percent: 50)
        composer_split = double(:composer_split, writer_code: composer_writer_code, right_to_collect: true, share: 50)
        cowriter_split = double(:cowriter_split, writer_code: cowriter_writer_code, right_to_collect: false, share: 50)
        work = double(:work,
                      title: Faker::Name.name,
                      writers: [composer_split, cowriter_split],
                      work_code: '',
                      verified_date: Time.current,
                      is_deactivated?: false,
                      number_of_recordings: 0
                     )

        PublishingAdministration::WorkUpdateService.update(composition, work)

        cowriter_pub_split = cowriter.reload.publishing_splits.first
        expect(cowriter_pub_split.composition).to eq composition
        expect(cowriter_pub_split.writer).to eq cowriter
        expect(cowriter_pub_split.percent).to eq cowriter_split.share
      end
    end

    context "when updating the composition" do
      let(:composition) { create(:composition, :with_songs) }
      let(:person) { composition.songs.first.album.person }
      let(:account) { create(:account, person: person) }
      let(:writer_code) { Faker::Number.number(digits: 8) }
      let(:composer) { create(:composer, account: account, provider_identifier: writer_code) }
      let(:composer_split) { double(:composer_split, writer_code: writer_code, right_to_collect: true, share: 50) }
      let(:work_code) { Faker::Number.number(digits: 12) }
      let(:work) do
        double(:work,
               title: Faker::Name.name,
               work_code: work_code,
               writers: [composer_split],
               verified_date: Time.current,
               is_deactivated?: false,
               number_of_recordings: 0)
      end

      before do
        travel_to Time.local(2020)
      end

      after do
        travel_back
      end

      it "should update the composition if the verified_date is not set" do
        composition.update(verified_date: nil)
        create(:publishing_split, writer: composer, composer: composer, composition: composition)

        PublishingAdministration::WorkUpdateService.update(composition, work)

        expect(composition.reload.name).to eq work.title
        expect(composition.verified_date.to_date).to eq work.verified_date.to_date
        expect(composition.provider_identifier).to eq work.work_code.to_s
        expect(composition.status).to eq "Accepted"
      end

      it "should update the composition if the verified_date is earlier than the verified_date in RightsApp" do
        composition.update(verified_date: Time.current.yesterday)
        create(:publishing_split, writer: composer, composer: composer, composition: composition)

        PublishingAdministration::WorkUpdateService.update(composition, work)

        expect(composition.reload.name).to eq work.title
        expect(composition.verified_date.to_date).to eq work.verified_date.to_date
      end

      it "should not update the composition status if the work does not have a verified_date" do
        work = double(:work,
                      title: Faker::Name.name,
                      work_code: work_code,
                      writers: [composer_split],
                      verified_date: nil,
                      is_deactivated?: false,
                      number_of_recordings: 0)

        expect(composition).not_to receive(:verified!)

        PublishingAdministration::WorkUpdateService.update(composition, work)
      end

      it "should invoke the RecordingUpdateService if the number of recordings is greater than 0" do
        create(:publishing_split, writer: composer, composer: composer, composition: composition)
        work = double(:work,
                      title: Faker::Name.name,
                      writers: [composer_split],
                      work_code: '',
                      verified_date: Time.current,
                      is_deactivated?: false,
                      number_of_recordings: 1)


        expect(PublishingAdministration::RecordingUpdateService).to receive(:update).with(composition)

        PublishingAdministration::WorkUpdateService.update(composition, work)
      end
    end

    context "when updating the state of a composition" do
      let(:composition) { create(:composition, :with_songs) }
      let(:composer_split) { double(:composer_split, writer_code: Faker::Number.number(digits: 8), right_to_collect: true, share: 50) }

      it "updates the state to 'terminated' if the work is deactivated" do
        work = double(:work,
               title: Faker::Name.name,
               writers: [composer_split],
               work_code: '',
               verified_date: Time.current,
               is_deactivated?: true,
               number_of_recordings: 0)

        PublishingAdministration::WorkUpdateService.update(composition, work)

        expect(composition.reload.state).to eq "terminated"
      end

      it "updates the state to 'verified' if the composition is terminated and the work is not deactivated" do
        composition.update(state: "terminated")
        work = double(:work,
               title: Faker::Name.name,
               writers: [composer_split],
               verified_date: Time.current,
               work_code: '',
               is_deactivated?: false,
               number_of_recordings: 0)

        PublishingAdministration::WorkUpdateService.update(composition, work)

        expect(composition.reload.state).to eq "verified"
      end
    end
  end
end
