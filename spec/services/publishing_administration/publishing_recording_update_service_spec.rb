require "rails_helper"

describe PublishingAdministration::PublishingRecordingUpdateService do
  describe ".update" do
    it 'should return if the publishing_composition does not have a work code' do
      publishing_composition = create(:publishing_composition, provider_identifier: nil)
      allow(PublishingAdministration::ApiClientServices::FetchRecordingListService).to receive(:fetch).and_return({})

      expect(publishing_composition).not_to receive(:recordings)

      PublishingAdministration::PublishingRecordingUpdateService.update(publishing_composition)
    end

    it 'should return if the recording_list is invalid' do
      publishing_composition = create(:publishing_composition, provider_identifier: nil)
      response = double(:response, valid?: true)
      allow(PublishingAdministration::ApiClientServices::FetchRecordingListService).to receive(:fetch).and_return(response)

      expect(publishing_composition).not_to receive(:recordings)

      PublishingAdministration::PublishingRecordingUpdateService.update(publishing_composition)
    end

    context "when a publishing_composition does not have a recording with a recording_code returned from the recording list" do
      it 'creates the recording with the recording_code and recordable' do
        publishing_composition = create(:publishing_composition, provider_identifier: Faker::Number.number(digits: 8))
        song = create(:song, publishing_composition: publishing_composition)
        recording_id = Faker::Number.number(digits: 8)
        response = double(:response,
                          valid?: true,
                          recordings: [ double(:recording, recording_id: recording_id, isrc: nil) ])

        allow(PublishingAdministration::ApiClientServices::FetchRecordingListService).to receive(:fetch).and_return(response)

        expect {
          PublishingAdministration::PublishingRecordingUpdateService.update(publishing_composition)
        }.to change {
          publishing_composition.recordings.count
        }.by(1)

        recording = publishing_composition.reload.recordings.first
        expect(recording.recording_code.to_s).to eq recording_id.to_s
        expect(recording.recordable).to eq song
      end
    end

    context "when a publishing_composition has a recording with a recording_code returned from the recording list" do
      it 'does not create the recording' do
        recording_id = Faker::Number.number(digits: 8)
        publishing_composition = create(:publishing_composition, provider_identifier: Faker::Number.number(digits: 8))
        create(:recording, publishing_composition: publishing_composition, recording_code: recording_id)
        response = double(:response,
                          valid?: true,
                          recordings: [ double(:recording, recording_id: recording_id) ])

        allow(PublishingAdministration::ApiClientServices::FetchRecordingListService).to receive(:fetch).and_return(response)

        expect {
          PublishingAdministration::PublishingRecordingUpdateService.update(publishing_composition)
        }.not_to change {
          publishing_composition.recordings.count
        }
      end
    end

    context "when the song associated to the publishing_composition is not a NonTunecoreSong" do
      it "should not update the song" do
        publishing_composition = create(:publishing_composition, provider_identifier: Faker::Number.number(digits: 8))
        song = create(:song, publishing_composition: publishing_composition)
        response = double(:response,
                          valid?: true,
                          recordings: [ double(:recording, recording_id: Faker::Number.number(12), isrc: nil) ])

        allow(PublishingAdministration::ApiClientServices::FetchRecordingListService).to receive(:fetch).and_return(response)

        expect {
          PublishingAdministration::PublishingRecordingUpdateService.update(publishing_composition)
        }.not_to change { song }
      end
    end

    context "when the song associated to the publishing_composition is not a NonTunecoreSong" do
      it "should update the song's isrc number" do
        publishing_composition = create(:publishing_composition, provider_identifier: Faker::Number.number(digits: 8))
        isrc_number = Faker::Number.number(digits: 8)
        ntc_album = create(:non_tunecore_album)
        ntc_song = create(
          :non_tunecore_song,
          non_tunecore_album: ntc_album,
          publishing_composition: publishing_composition,
          isrc: nil
        )
        response = double(:response,
                          valid?: true,
                          recordings: [ double(:recording, recording_id: Faker::Number.number(12), isrc: isrc_number) ])

        allow(PublishingAdministration::ApiClientServices::FetchRecordingListService).to receive(:fetch).and_return(response)
        allow(NonTunecoreSong).to receive(:find_by).with(publishing_composition_id: publishing_composition.id).and_return(ntc_song)

        expect {
          PublishingAdministration::PublishingRecordingUpdateService.update(publishing_composition)
        }.to change { ntc_song.isrc }.from(nil).to(isrc_number.to_s)
      end
    end

    context "when a publishing_composition has a extraneous recordings" do
      it 'deletes the recording' do
        recording_id = Faker::Number.number(digits: 8)
        extraneous_recording_id = Faker::Number.number(digits: 8)
        publishing_composition = create(:publishing_composition, provider_identifier: Faker::Number.number(digits: 8))
        create(:recording, publishing_composition: publishing_composition, recording_code: recording_id)
        create(:recording, publishing_composition: publishing_composition, recording_code: extraneous_recording_id)
        response = double(:response,
                          valid?: true,
                          recordings: [ double(:recording, recording_id: recording_id) ])

        allow(PublishingAdministration::ApiClientServices::FetchRecordingListService).to receive(:fetch).and_return(response)

        PublishingAdministration::PublishingRecordingUpdateService.update(publishing_composition)

        recordings = publishing_composition.reload.recordings
        expect(recordings.where(recording_code: recording_id)).to exist
        expect(recordings.where(recording_code: extraneous_recording_id)).not_to exist
      end
    end
  end
end
