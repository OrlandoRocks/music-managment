require "rails_helper"

describe PublishingAdministration::WorkListService do
  describe ".list" do
    let(:work_code) { Faker::Number.number(digits: 10) }
    let(:writer_code) { Faker::Number.number(digits: 8) }
    let!(:account)  { create(:account, provider_account_id: Faker::Number.number(digits: 8)) }
    let!(:composer) { create(:composer, account: account, provider_identifier: writer_code) }

    before do
      allow(PublishingAdministration::ApiClientServices::FetchWorkListService).to receive(:fetch) { double(:response, valid?: true, works: []) }
      allow(PublishingAdministration::WorkUpdateService).to receive(:update)
      allow(PublishingAdministration::WorkMatcherService).to receive(:match) { response }
    end

    context "when there are unmatched works" do
      let(:unmatched_works) do
        [
          OpenStruct.new(
            title: "New Composition",
            work_code: work_code,
            writers: [
              OpenStruct.new(writer_code: writer_code, right_to_collect: true)
            ]
          )
        ]
      end

      let(:response) do
        double(:response,
               valid?: true,
               unmatched_works: unmatched_works,
               matched_compositions: []
              )
      end

      it "creates a composition" do
        expect(PublishingAdministration::NonTunecoreService)
          .to receive(:create_ntc_album_and_song)
        expect(PublishingAdministration::WorkUpdateService)
          .to receive(:update)

        expect {
          PublishingAdministration::WorkListService.list(account)
        }.to change {
          Composition.count
        }.by(1)
      end
    end

    context "when there are matched compositions" do
      let(:composition) { create(:composition, name: "No Soggy Bottoms", provider_identifier: work_code) }

      let(:matched_compositions) do
        [
          [
            composition,
            OpenStruct.new(
              title: "No Soggy Bottoms",
              work_code: work_code,
            )
          ]
        ]
      end

      let(:response) do
        double(:response,
               valid?: true,
               unmatched_works: [],
               matched_compositions: matched_compositions
              )
      end

      it "invokes the WorkUpdateService" do
        expect(PublishingAdministration::WorkUpdateService).to receive(:update)

        PublishingAdministration::WorkListService.list(account)
      end
    end

    context "when updating terminated compositions state" do
      let(:response) do
        double(:response,
               valid?: true,
               unmatched_works: [],
               matched_compositions: [])
      end

      it "invokes the UpdateTerminatedCompositionsService" do
        expect(PublishingAdministration::UpdateTerminatedCompositionsService).to receive(:update).with(account)

        PublishingAdministration::WorkListService.list(account)
      end
    end
  end
end
