require "rails_helper"

describe PublishingAdministration::CreatePublishingComposerService do
  let(:account)   { create(:account) }
  let(:purchase)  { create(:purchase, :pub_admin, person_id: account.person_id) }
  let(:service)   { PublishingAdministration::CreatePublishingComposerService }
  let(:publishing_composer) do
    {
      WriterCode:       "00000DMC",
      FirstName:        "Paul",
      MiddleName:       "John",
      LastName:         "Hollywood",
      SocietyId:        "39e6a3a9-52db-bf7a-32d5-f5abddf2503b",
      CaeipiNumber:     "5166800503",
      PublisherName:    "GBBO",
      PublisherCaeipi:  "5168977925",
      Messages:         []
    }
  end

  let(:json_response) { double(body: publishing_composer.to_json, headers: { etag: "etag" }) }
  let(:response)      { RightsApp::Responses::Writer.new(json_response) }
  let(:params)        { { account: account, api_response: response } }

  before :each do
    allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
  end

  context "create publishing_composer based on Sentric Writer response" do
    it "should create or update the publisher" do
      service.create_publishing_composer(params)
      expect(Publisher.exists?(name: "GBBO", cae: "5168977925")).to be true
    end

    it "should create a new publishing_composer" do
      service.create_publishing_composer(params)
      expect(PublishingComposer.exists?(first_name: "Paul", last_name: "Hollywood")).to be true
    end

    it "should create a purchase for the newly created publishing_composer" do
      service.create_publishing_composer(params)
      publishing_composer = PublishingComposer.find_by(provider_identifier: "00000DMC")
      expect(Purchase.exists?(related_id: publishing_composer.id, related_type: "PublishingComposer")).to be true
    end

    describe "#free_purchase is true" do
      it "should create a $0 purchase for the newly created publishing_composer" do
        service.create_publishing_composer(params.merge(free_purchase: true))
        publishing_composer = PublishingComposer.find_by(provider_identifier: "00000DMC")
        purchase = Purchase.where(related_id: publishing_composer.id, related_type: "PublishingComposer").first
        expect(purchase.cost_cents).to eq 0
      end

      it "should create an invoice for the $0 purchase" do
        service.create_publishing_composer(params.merge(free_purchase: true))
        publishing_composer = PublishingComposer.find_by(provider_identifier: "00000DMC")
        purchase = Purchase.where(related_id: publishing_composer.id, related_type: "PublishingComposer").first
        expect(purchase.invoice_id.present?).to be true
      end
    end
  end

end
