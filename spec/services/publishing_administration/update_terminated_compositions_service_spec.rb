require "rails_helper"

describe PublishingAdministration::UpdateTerminatedCompositionsService do
  describe ".update" do
    let!(:account)             { create(:account) }
    let!(:composer)            { create(:composer, account: account, person: account.person) }
    let!(:terminated_composer) { create(:terminated_composer, :partially, composer: composer, alt_provider_acct_id: "1") }
    let(:album)                { create(:album, person: account.person) }
    let(:composition)          { create(:composition, provider_identifier: "DMC0000000W") }

    it "updates the composition's state to 'terminated'" do
      composer.reload
      work = double(:work, work_code: composition.provider_identifier)

      work_list = double(:work_list, works: [work])
      allow(PublishingAdministration::ApiClientServices::FetchWorkListService).to receive(:fetch) { work_list }

      expect(composition.state).not_to eq "terminated"
      described_class.update(account.reload)
      expect(composition.reload.state).to eq "terminated"
    end
  end
end
