require 'rails_helper'

describe PublishingAdministration::PublishingWorkUpdateService do
  describe ".update" do
    it "should reconcile publishing_composers and cowriting_publishing_composers" do
      account = create(:account)
      publishing_composition = create(:publishing_composition, :with_songs, account: account)
      person = publishing_composition.songs.first.album.person
      account.update(person: person)
      publishing_composer = create(:publishing_composer, account: account)
      create(:publishing_composition_split, publishing_composer: publishing_composer, publishing_composition: publishing_composition)
      create(:publishing_composition_split, publishing_composer: publishing_composer, publishing_composition: publishing_composition)
      publishing_composer_split = double(:publishing_composer_split, right_to_collect: true, share: 0)
      cowriting_publishing_composer_split = double(:cowriting_publishing_composer_split, right_to_collect: false, share: 0)
      writer_params = {
        account: account,
        api_publishing_composers: [publishing_composer_split],
        api_cowriting_publishing_composers: [cowriting_publishing_composer_split],
        publishing_composers: [publishing_composer],
        cowriting_publishing_composers:     [],
        free_purchase: false
      }

      work = double(:work,
                    title: Faker::Name.name,
                    writers: [publishing_composer_split, cowriting_publishing_composer_split],
                    verified_date: Time.current,
                    work_code: '',
                    is_deactivated?: false,
                    number_of_recordings: 0,
                    should_not_update?: false
                   )

      expect(PublishingAdministration::ApiClientServices::PrimaryPublishingComposerNormalizeService).to receive(:reconcile_publishing_composers).with(writer_params)
      expect(PublishingAdministration::ApiClientServices::CowritingPublishingComposerNormalizeService).to receive(:reconcile_cowriting_publishing_composers).with(writer_params)

      PublishingAdministration::PublishingWorkUpdateService.update(publishing_composition, work, account)
    end

    before do
      allow(PublishingAdministration::ApiClientServices::PrimaryPublishingComposerNormalizeService).to receive(:reconcile_publishing_composers)
      allow(PublishingAdministration::ApiClientServices::CowritingPublishingComposerNormalizeService).to receive(:reconcile_cowriting_publishing_composers)
    end

    context "when comparing splits" do
      let!(:account) { create(:account) }
      let(:publishing_composition) { create(:publishing_composition, :with_songs, account: account) }
      let(:person) { publishing_composition.songs.first.album.person }
      let(:publishing_composer_writer_code) { Faker::Number.number(digits: 8) }
      let(:cowriting_publishing_composer_writer_code) { Faker::Number.number(digits: 8) }
      let(:publishing_composer) { create(:publishing_composer, account: account, provider_identifier: publishing_composer_writer_code) }

      before(:each) do
        account.update(person: person)
      end

      it "should update the split if a writer is associated to a split" do
        cowriting_publishing_composer = create(:publishing_composer, :cowriter)
        publishing_composer_pub_split = create(:publishing_composition_split, publishing_composer: publishing_composer, publishing_composition: publishing_composition)
        cowriting_publishing_composer_pub_split = create(:publishing_composition_split, publishing_composer: cowriting_publishing_composer, publishing_composition: publishing_composition, percent: 0, right_to_collect: false)
        publishing_composer_split = double(:publishing_composer_split, writer_code: publishing_composer_writer_code, right_to_collect: true, share: 50)
        cowriting_publishing_composer_split = double(:cowriting_publishing_composer_split, writer_code: nil, right_to_collect: false, share: 50)
        work = double(:work,
                      title: Faker::Name.name,
                      writers: [publishing_composer_split, cowriting_publishing_composer_split],
                      verified_date: Time.current,
                      work_code: '',
                      is_deactivated?: false,
                      number_of_recordings: 0,
                      should_not_update?: false
                     )

        PublishingAdministration::PublishingWorkUpdateService.update(publishing_composition, work, account)

        expect(publishing_composer_pub_split.reload.percent).to eq 50
        expect(cowriting_publishing_composer_pub_split.reload.percent).to eq 50
      end

      it "should create a new split if the writer is not associated to the split" do
        cowriting_publishing_composer = create(:publishing_composer, :cowriter, provider_identifier: cowriting_publishing_composer_writer_code)
        create(:publishing_composition_split, publishing_composer: publishing_composer, publishing_composition: publishing_composition, percent: 50)
        publishing_composer_split = double(:publishing_composer_split, writer_code: publishing_composer_writer_code, right_to_collect: true, share: 50)
        cowriting_publishing_composer_split = double(:cowriting_publishing_composer_split, writer_code: cowriting_publishing_composer_writer_code, right_to_collect: false, share: 50)
        work = double(:work,
                      title: Faker::Name.name,
                      writers: [publishing_composer_split, cowriting_publishing_composer_split],
                      work_code: '',
                      verified_date: Time.current,
                      is_deactivated?: false,
                      number_of_recordings: 0,
                      should_not_update?: false
                     )

        PublishingAdministration::PublishingWorkUpdateService.update(publishing_composition, work, account)

        cowriting_publishing_composer_pub_split = cowriting_publishing_composer.reload.publishing_composition_splits.first
        expect(cowriting_publishing_composer_pub_split.publishing_composition).to eq publishing_composition
        expect(cowriting_publishing_composer_pub_split.publishing_composer).to eq cowriting_publishing_composer
        expect(cowriting_publishing_composer_pub_split.percent).to eq cowriting_publishing_composer_split.share
      end
    end

    context "when updating the publishing_composition" do
      let!(:account) { create(:account) }
      let(:publishing_composition) { create(:publishing_composition, :with_songs, account: account) }
      let(:person) { publishing_composition.songs.first.album.person }
      let(:writer_code) { Faker::Number.number(digits: 8) }
      let(:publishing_composer) { create(:publishing_composer, account: account, provider_identifier: writer_code) }
      let(:publishing_composer_split) { double(:publishing_composer_split, writer_code: writer_code, right_to_collect: true, share: 50) }
      let(:work_code) { Faker::Number.number(digits: 12) }
      let(:work) do
        double(:work,
               title: Faker::Name.name,
               work_code: work_code,
               writers: [publishing_composer_split],
               verified_date: Time.current,
               is_deactivated?: false,
               number_of_recordings: 0,
               should_not_update?: false
              )
      end

      before(:each) do
        account.update(person: person)
      end

      it "should update the publishing_composition if the verified_date is not set" do
        publishing_composition.update(verified_date: nil)
        create(:publishing_composition_split, publishing_composer: publishing_composer, publishing_composition: publishing_composition)

        PublishingAdministration::PublishingWorkUpdateService.update(publishing_composition, work, account)

        expect(publishing_composition.reload.name).to eq work.title
        expect(publishing_composition.verified_date.to_date).to eq work.verified_date.to_date
        expect(publishing_composition.provider_identifier).to eq work.work_code.to_s
        expect(publishing_composition.status).to eq "Accepted"
      end

      it "should update the publishing_composition if the verified_date is earlier than the verified_date in RightsApp" do
        publishing_composition.update(verified_date: Time.current.yesterday)
        create(:publishing_composition_split, publishing_composer: publishing_composer, publishing_composition: publishing_composition)

        PublishingAdministration::PublishingWorkUpdateService.update(publishing_composition, work, account)

        expect(publishing_composition.reload.name).to eq work.title
        expect(publishing_composition.verified_date.to_date).to eq work.verified_date.to_date
      end

      context "when the work does not have a verified_date" do
        context "when the publishing_composition does not have verified_date set" do
          it "should not update the publishing_composition status" do
            work = double(:work,
                          title: Faker::Name.name,
                          work_code: work_code,
                          writers: [publishing_composer_split],
                          verified_date: nil,
                          is_deactivated?: false,
                          number_of_recordings: 0,
                          should_not_update?: false
                        )

            expect(publishing_composition).not_to receive(:verified!)

            PublishingAdministration::PublishingWorkUpdateService.update(publishing_composition, work, account)
          end
        end

        context "when the publishing_composition has verified_date set" do
          before(:each) do
            publishing_composition.update(verified_date: Time.current.yesterday)
          end

          it "should not update the publishing_composition status" do
            work = double(:work,
                          title: Faker::Name.name,
                          work_code: work_code,
                          writers: [publishing_composer_split],
                          verified_date: nil,
                          is_deactivated?: false,
                          number_of_recordings: 0,
                          should_not_update?: false
                         )

            expect(publishing_composition).not_to receive(:verified!)

            PublishingAdministration::PublishingWorkUpdateService.update(publishing_composition, work, account)
          end
        end
      end

      context "when the work should not be updated" do
        let(:work) do

          double(:work,
                 title: Faker::Name.name,
                 writers: [publishing_composer_split],
                 work_code: '',
                 verified_date: Time.current,
                 is_deactivated?: false,
                 number_of_recordings: 1,
                 should_not_update?: true
                )
        end

        it "should not invoke the RecordingUpdateService" do
          create(:publishing_composition_split, publishing_composer: publishing_composer, publishing_composition: publishing_composition)

          expect(PublishingAdministration::PublishingRecordingUpdateService).to_not receive(:update)

          PublishingAdministration::PublishingWorkUpdateService.update(publishing_composition, work, account)
        end
      end

      it "should invoke the RecordingUpdateService if the number of recordings is greater than 0" do
        create(:publishing_composition_split, publishing_composer: publishing_composer, publishing_composition: publishing_composition)
        work = double(:work,
                      title: Faker::Name.name,
                      writers: [publishing_composer_split],
                      work_code: '',
                      verified_date: Time.current,
                      is_deactivated?: false,
                      number_of_recordings: 1,
                      should_not_update?: false
                     )

        expect(PublishingAdministration::PublishingRecordingUpdateService).to receive(:update).with(publishing_composition)

        PublishingAdministration::PublishingWorkUpdateService.update(publishing_composition, work, account)
      end
    end

    context "when updating the state of a publishing_composition" do
      let!(:account) { create(:account) }
      let(:publishing_composition) { create(:publishing_composition, :with_songs, account: account) }
      let(:publishing_composer_split) { double(:publishing_composer_split, writer_code: Faker::Number.number(digits: 8), right_to_collect: true, share: 50) }

      it "updates the state to 'terminated' if the work is deactivated" do
        work = double(:work,
               title: Faker::Name.name,
               writers: [publishing_composer_split],
               work_code: '',
               verified_date: Time.current,
               is_deactivated?: true,
               number_of_recordings: 0,
               should_not_update?: false
              )

        PublishingAdministration::PublishingWorkUpdateService.update(publishing_composition, work, account)

        expect(publishing_composition.reload.state).to eq "terminated"
      end

      it "updates the state to 'verified' if the publishing_composition is terminated and the work is not deactivated" do
        publishing_composition.update(state: "terminated")
        work = double(:work,
               title: Faker::Name.name,
               writers: [publishing_composer_split],
               verified_date: Time.current,
               work_code: '',
               is_deactivated?: false,
               number_of_recordings: 0,
               should_not_update?: false
              )

        PublishingAdministration::PublishingWorkUpdateService.update(publishing_composition, work, account)

        expect(publishing_composition.reload.state).to eq "verified"
      end
    end
  end
end
