require "rails_helper"

describe PublishingAdministration::NonTunecoreService do
  let!(:artist)     { create(:artist, name: "DJ Big Ma$e") }
  let(:composer)    { create(:composer, first_name: "Jason", last_name: "Allen") }
  let(:composition) { create(:composition, name: "House Sessions") }
  let(:work) { { "PerformingArtists" => [{ "Id" => "1234", "Name" => artist.name }] } }
  let(:params)  { { composer: composer, composition: composition, work: work } }

  before do
    allow(PublishingAdministration::ApiClientServices::GetWorkService)
      .to receive(:get_work)
      .and_return(work)
  end

  it "creates a non_tunecore_album" do
    PublishingAdministration::NonTunecoreService.create_ntc_album_and_song(params)
    expect(NonTunecoreAlbum.exists?(name: composition.name, composer_id: composer.id)).to be_truthy
  end

  it "creates a non_tunecore_song" do
    PublishingAdministration::NonTunecoreService.create_ntc_album_and_song(params)
    expect(NonTunecoreSong.exists?(artist_id: artist.id, composition_id: composition.id)).to be_truthy
  end
end
