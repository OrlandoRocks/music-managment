require "rails_helper"

describe PublishingAdministration::IngestionBlocker do
  describe "#multiple_writers_with_right_to_collect?" do
    context "when the account does NOT have multiple writers with the right to collect" do
      it "returns false" do
        account = create(:account, provider_account_id: "12345")

        work_codes_res = ["TCM0000003360S"]
        allow(PublishingAdministration::ApiClientServices::FetchWorkListService)
          .to receive(:work_codes)
          .and_return(work_codes_res)

        writers_with_right_to_collect_res = [{
          "WriterCode"=>"00001CKJW",
          "WriterDesignationCode"=>2,
          "WriterShare"=>50.0,
          "RightToCollect"=>true,
          "WriterShareId"=>"39f01960-b179-f489-2f2f-c1da06614e1f"
        }]
        allow(PublishingAdministration::ApiClientServices::GetWorkService)
          .to receive(:writers_with_right_to_collect)
          .with(work_codes_res.first)
          .and_return(writers_with_right_to_collect_res)

        result = PublishingAdministration::IngestionBlocker
                   .multiple_writers_with_right_to_collect?(account.provider_account_id)

        expect(result).to be_falsey
      end
    end

    context "when the account has multiple writers with the right to collect" do
      it "returns true" do
        account = create(:account, provider_account_id: "12345")

        work_codes_res = ["TCM0000003360S"]
        allow(PublishingAdministration::ApiClientServices::FetchWorkListService)
          .to receive(:work_codes)
          .and_return(work_codes_res)

        writers_with_right_to_collect_res = [
          {
            "WriterCode"=>"00001CKJW",
            "WriterDesignationCode"=>2,
            "WriterShare"=>50.0,
            "RightToCollect"=>true,
            "WriterShareId"=>"39f01960-b179-f489-2f2f-c1da06614e1f"
          }, {
            "WriterCode"=>"00001CKKW",
            "WriterDesignationCode"=>2,
            "WriterShare"=>20.0,
            "RightToCollect"=>true,
            "WriterShareId"=>"39f01960-b1a8-2276-d4e3-d766a229e0a9"
          }
        ]
        allow(PublishingAdministration::ApiClientServices::GetWorkService)
          .to receive(:writers_with_right_to_collect)
          .with(work_codes_res.first)
          .and_return(writers_with_right_to_collect_res)

        result = PublishingAdministration::IngestionBlocker
                   .multiple_writers_with_right_to_collect?(account.provider_account_id)

        expect(result).to be_truthy
      end
    end
  end
end
