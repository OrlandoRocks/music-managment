require "rails_helper"

describe PublishingAdministration::RecordingListService do
  describe "#list" do
    context "when a song is associated to the composition" do
      it "invokes the recording update service with the composition" do
        account = create(:account, provider_account_id: Faker::Number.number(digits: 10))
        album = create(:album, person: account.person)
        composition = create(:composition, provider_identifier: Faker::Number.number(digits: 8))
        create(:song, album: album, composition: composition)

        expect(PublishingAdministration::RecordingUpdateService)
          .to receive(:update)
          .with(composition)

        PublishingAdministration::RecordingListService.list(account)
      end
    end

    context "when a non_tunecore_song is associated to the composition" do
      it "invokes the recording update service with the composition" do
        account = create(:account, provider_account_id: Faker::Number.number(digits: 10))
        composer = create(:composer, account: account)
        composition = create(:composition, provider_identifier: Faker::Number.number(digits: 8))
        ntc_album = create(:non_tunecore_album, composer: composer)
        create(
          :non_tunecore_song,
          non_tunecore_album: ntc_album,
          composition: composition
        )

        expect(PublishingAdministration::RecordingUpdateService)
          .to receive(:update)
          .with(composition)

        PublishingAdministration::RecordingListService.list(account)
      end
    end
  end
end
