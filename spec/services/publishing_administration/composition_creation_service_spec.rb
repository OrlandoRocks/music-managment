require "rails_helper"

describe PublishingAdministration::CompositionCreationService do
  describe ".create" do
    it "should create compositions for all the composer's songs that don't have compositions associated" do
      person = create(:person, :with_live_album, :with_composer_with_purchases)
      person.account = create(:account)
      person.albums.first.songs << create_list(:song, 5)
      composer = person.composers.first
      composer.update(account_id: person.account.id)

      expect(person.songs.pluck(:composition_id).compact.size).to eq 0
      PublishingAdministration::CompositionCreationService.create(composer.id)
      expect(person.songs.pluck(:composition_id).compact.size).to eq person.songs.size
    end
  end
end
