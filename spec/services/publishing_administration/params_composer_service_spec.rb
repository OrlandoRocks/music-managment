require "rails_helper"

describe PublishingAdministration::ParamsComposerService do
  describe ".compose" do
    let(:composer) { create(:composer) }

    context "when a composer does not have a publisher" do
      let(:expected_params) do
        {
          name_prefix: composer.name_prefix,
          first_name: composer.first_name,
          middle_name: composer.middle_name,
          last_name: composer.last_name,
          name_suffix: composer.name_suffix,
          dob: composer.dob,
          publishing_role_id: composer.publishing_role_id,
          composer_cae: composer.cae,
          composer_pro_id: composer.performing_rights_organization_id,
          person_id: composer.person_id,
          id: composer.id,
          email: composer.current_email
        }.with_indifferent_access
      end

      it "should return a hash of composer attributes" do
        params_composer_service = PublishingAdministration::ParamsComposerService.compose(composer.id)
        expect(params_composer_service.with_indifferent_access).to eq expected_params
      end
    end

    context "when a composer has a publisher" do
      let(:publisher) { create(:publisher) }
      let(:composer)  { create(:composer, publisher: publisher) }
      let(:expected_params) do
        {
          name_prefix: composer.name_prefix,
          first_name: composer.first_name,
          middle_name: composer.middle_name,
          last_name: composer.last_name,
          name_suffix: composer.name_suffix,
          dob: composer.dob,
          publishing_role_id: composer.publishing_role_id,
          composer_cae: composer.cae,
          composer_pro_id: composer.performing_rights_organization_id,
          person_id: composer.person_id,
          email: composer.current_email,
          id: composer.id,
          publisher_cae: publisher.cae,
          publisher_name: publisher.name,
          publisher_pro_id: composer.performing_rights_organization_id
        }.with_indifferent_access
      end

      it "should return a hash of composer and publisher attributes" do
        params_composer_service = PublishingAdministration::ParamsComposerService.compose(composer.id)
        expect(params_composer_service.with_indifferent_access).to eq expected_params
      end
    end
  end
end
