require "rails_helper"

describe PublishingAdministration::PublishingCompositionMatcherService do
  let(:title)                 { Faker::Book.title }
  let(:api_client)            { $rights_app_api_client }
  let(:work_code)             { "0000000DMC" }
  let!(:account)              { create(:account) }
  let!(:publishing_composer)  { create(:publishing_composer, account: account) }
  let!(:cowriter)             { create(:publishing_composer, :cowriter, account: account) }
  let!(:album1)               { create(:album, person: publishing_composer.person) }
  let!(:ntc_album)            { create(:non_tunecore_album, publishing_composer: publishing_composer) }
  let!(:publishing_composition1) { create(:publishing_composition, name: title, provider_identifier: work_code, account: account) }
  let!(:comp_with_same_name)  { create(:publishing_composition, name: title.upcase, account: account) }
  let(:params)                { { publishing_composer: publishing_composer, publishing_composition: comp_with_same_name } }
  let!(:song1) do
    create(
      :song,
      album: album1,
      name: title,
      publishing_composition_id: publishing_composition1.id
    )
  end
  let!(:ntc_song) do
    create(
      :non_tunecore_song,
      non_tunecore_album: ntc_album,
      name: title,
      publishing_composition_id: comp_with_same_name.id
    )
  end
  let!(:publishing_composer_split1) do
    create(
      :publishing_composition_split,
      publishing_composition_id: publishing_composition1.id,
      publishing_composer_id: publishing_composer.id,
      percent: 50.to_d
    )
  end
  let!(:cowriter_split1) do
    create(
      :publishing_composition_split,
      publishing_composition_id: publishing_composition1.id,
      publishing_composer_id: cowriter.id,
      right_to_collect: false,
      percent: 50.to_d
    )
  end
  let!(:publishing_composer_split2) do
    create(
      :publishing_composition_split,
      publishing_composition_id: comp_with_same_name.id,
      publishing_composer_id: publishing_composer.id,
      percent: 50.to_d
    )
  end
  let!(:cowriter_split2) do
    create(
      :publishing_composition_split,
      publishing_composition_id: comp_with_same_name.id,
      publishing_composer_id: cowriter.id,
      right_to_collect: false,
      percent: 50.to_d
    )
  end

  describe "#match" do
    before :each do
      allow(PublishingAdministration::ApiClientServices::RecordingService).to receive(:post_recording)
    end

    context "publishing_composition matches another publishing_composition with the same name and publishing splits" do

      it "updates the publishing_composition's provider_identifier" do
        expect(comp_with_same_name.provider_identifier).to eq nil
        PublishingAdministration::PublishingCompositionMatcherService.match(params)
        expect(comp_with_same_name.provider_identifier).to eq work_code
      end

      it "works the same whether the publishing_composition is associated to a song or a non_tunecore_song" do
        comp_with_same_name.update(provider_identifier: work_code)
        publishing_composition1.update(provider_identifier: nil)
        params[:publishing_composition] = publishing_composition1

        expect(publishing_composition1.provider_identifier).to eq nil
        PublishingAdministration::PublishingCompositionMatcherService.match(params)
        expect(publishing_composition1.provider_identifier).to eq work_code
      end

      it "works whether or not the publishing_composer has a person_id" do
        publishing_composer.update(person_id: nil)
        comp_with_same_name.update(provider_identifier: work_code)

        other_publishing_composition = create(:publishing_composition, name: title, account: account)
        other_params      = { publishing_composer: publishing_composer, publishing_composition: other_publishing_composition }
        other_ntc_album   = create(:non_tunecore_album, name: title, publishing_composer: publishing_composer)
        create(
          :non_tunecore_song,
          non_tunecore_album: other_ntc_album,
          name: title,
          publishing_composition_id: other_publishing_composition.id
        )
        create(
          :publishing_composition_split,
          publishing_composition_id: other_publishing_composition.id,
          publishing_composer_id: publishing_composer.id,
          percent: 50.to_d
        )
        create(
          :publishing_composition_split,
          publishing_composition_id: other_publishing_composition.id,
          publishing_composer_id: cowriter.id,
          right_to_collect: false,
          percent: 50.to_d
        )

        expect(other_publishing_composition.provider_identifier).to eq nil
        PublishingAdministration::PublishingCompositionMatcherService.match(other_params)
        expect(other_publishing_composition.reload.provider_identifier).to eq work_code
      end

      it "works regardless of writer name casing" do
        publishing_composer.update(first_name: publishing_composer.first_name.upcase)
        expect(comp_with_same_name.provider_identifier).to eq nil
        PublishingAdministration::PublishingCompositionMatcherService.match(params)
        expect(comp_with_same_name.provider_identifier).to eq work_code
      end
    end

    context "publishing_composition does not match another publishing_composition by name or splits" do

      it "does not update the publishing_composition if name differs" do
        comp_with_same_name.update(name: "what's in a name?")
        PublishingAdministration::PublishingCompositionMatcherService.match(params)
        expect(comp_with_same_name.provider_identifier).to eq nil
      end

      it "does not update the publishing_composition if splits differ" do
        publishing_composer_split2.update_attribute(:percent, 75.to_d)
        cowriter_split2.update_attribute(:percent, 25.to_d)

        PublishingAdministration::PublishingCompositionMatcherService.match(params)
        expect(comp_with_same_name.provider_identifier).to eq nil
      end

    end

  end
end
