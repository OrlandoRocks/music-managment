require "rails_helper"

describe PublishingAdministration::PublishingWorkMatcherService do
  describe ".match" do
    context "when the Work Code matches a publishing_composition's provider identifier" do
      it "should include the publishing_composition in the matched publishing_compositions array" do
        account = create(:account)
        create(:publishing_composer, account: account)
        album       = create(:album, person: account.person)
        publishing_composition = create(
          :publishing_composition,
          name: "No Soggy Bottoms",
          provider_identifier: "DMC0000000W"
        )
        create(:song, album: album, publishing_composition: publishing_composition)
        work = double(:work, work_code: publishing_composition.provider_identifier, title: "No Soggy Bottoms")

        work_matcher = PublishingAdministration::PublishingWorkMatcherService.match(account, [work])

        expect(work_matcher.matched_publishing_compositions).to include([publishing_composition, work])
      end
    end

    context "when the Work Title matches a compositon's name" do
      it "should include the publishing_composition in the matched publishing_compositions array irregardless of caseing" do
        account = create(:account)
        create(:publishing_composer, account: account)
        album       = create(:album, person: account.person)
        publishing_composition = create(
          :publishing_composition,
          name: "PublishingComposition name",
          provider_identifier: nil
        )
        create(:song, album: album, publishing_composition: publishing_composition)
        work = double(:work, work_code: Faker::Number.number(digits: 8), title: "PublishingComposition Name")

        work_matcher = PublishingAdministration::PublishingWorkMatcherService.match(account, [work])

        expect(work_matcher.matched_publishing_compositions).to include([publishing_composition, work])
      end

      context "when the publishing_composition is associated to a NonTunecoreSong" do
        it "should be included in the matched array if the publishing_composition belongs to an NTC Song" do
          account     = create(:account)
          publishing_composer    = create(:publishing_composer, account: account)
          publishing_composition = create(:publishing_composition, name: "Shame about that bake", account: account)
          ntc_album   = create(:non_tunecore_album, publishing_composer: publishing_composer)
          create(:non_tunecore_song, non_tunecore_album: ntc_album, publishing_composition: publishing_composition)
          work = double(:work, work_code: Faker::Number.number(digits: 8), title: publishing_composition.name)

          work_matcher = PublishingAdministration::PublishingWorkMatcherService.match(account, [work])

          expect(work_matcher.matched_publishing_compositions).to include([publishing_composition, work])
        end
      end
    end

    context "when the Work Title matches a compositon's translated name" do
      it "should include the publishing_composition in the matched publishing_compositions array" do
        account = create(:account)
        create(:publishing_composer, account: account)
        album       = create(:album, person: account.person)
        publishing_composition = create(
          :publishing_composition,
          name: "Amarillo Bodak",
          translated_name: "Bodak Yellow"
        )
        create(:song, album: album, publishing_composition: publishing_composition)
        work = double(:work, work_code: Faker::Number.number(digits: 8), title: publishing_composition.translated_name)

        work_matcher = PublishingAdministration::PublishingWorkMatcherService.match(account, [work])

        expect(work_matcher.matched_publishing_compositions).to include([publishing_composition, work])
      end
    end

    context "when the Work Title matches a the title of a muma song record" do
      it "should include the publishing_composition in the matched publishing_compositions array" do
        account     = create(:account)
        publishing_composition = create(:publishing_composition, name: "Scrummy", account: account)
        album       = create(:album, person: account.person)
        create(:song, album: album, publishing_composition: publishing_composition, name: "not scrummy")
        create(:publishing_composer, account: account)
        create(:muma_song, publishing_composition: publishing_composition, title: "Scrummy")
        work = double(:work, work_code: Faker::Number.number(digits: 8), title: "$crummy")

        work_matcher = PublishingAdministration::PublishingWorkMatcherService.match(account, [work])

        expect(work_matcher.matched_publishing_compositions).to include([publishing_composition, work])
      end
    end

    context "when the work doesn't match any records in our database" do
      it "should include the work in the unmatched works array" do
        account = create(:account)
        create(:publishing_composer, account: account)
        publishing_composition = create(:publishing_composition, name: "Number One in the Technical", account: account)
        album       = create(:album, person: account.person)
        create(:song, album: album, publishing_composition: publishing_composition)
        work = double(:work, work_code: Faker::Number.number(digits: 8), title: "Star Baker")

        work_matcher = PublishingAdministration::PublishingWorkMatcherService.match(account, [work])

        expect(work_matcher.matched_publishing_compositions).to be_empty
        expect(work_matcher.unmatched_works).to include(work)
      end
    end

    context "when the song has a muma song record in our database but an incorrect title" do
      it "should include one work in the matched array" do
        account = create(:account)
        create(:publishing_composer, account: account)
        publishing_composition = create(:publishing_composition, name: "Wrong Title", account: account)
        muma_song = create(:muma_song, title: "Number One in the Technical", publishing_composition: publishing_composition)
        album       = create(:album, person: account.person)
        create(:song, album: album, publishing_composition: publishing_composition)
        work = double(:work, work_code: Faker::Number.number(digits: 8), title: "Number One in the Technical")

        work_matcher = PublishingAdministration::PublishingWorkMatcherService.match(account, [work])

        expect(work_matcher.matched_publishing_compositions).to include([publishing_composition,work])
        expect(work_matcher.matched_publishing_compositions.size).to eq(1)
        expect(work_matcher.unmatched_works).to be_empty
      end
    end
  end
end
