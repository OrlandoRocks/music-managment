require "rails_helper"

describe PublishingAdministration::PostApprovalService do
  let(:account) { create(:account) }
  let(:composer) { create(:composer, account: account, person: account.person) }
  let(:publishing_composer) { create(:publishing_composer, account: account, person: account.person) }

  let(:album) { create(:album, person: account.person) }
  let!(:song1) { create(:song, :with_tunecore_isrc, name: 'SONG 1', album: album, composition_id: nil, publishing_composition_id: nil) }
  let!(:song2) { create(:song, :with_optional_isrc, name: 'SONG 2', album: album, composition_id: nil, publishing_composition_id: nil) }

  describe ".create" do
    context "with publishing_changeover feature flag enabled" do
      before(:each) do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
        allow(FeatureFlipper).to receive(:show_feature?).with(:publishing_changeover, account.person).and_return(true)
      end

      context "when an album with unique songs is passed in" do
        describe "for the first time" do
          it "creates publishing_compositions" do
            PublishingAdministration::PostApprovalService.create(album.id)

            expect(song1.reload.publishing_composition).to be_truthy
            expect(song2.reload.publishing_composition).to be_truthy
          end
        end

        context "and then the same album is passed in again" do
          it "does not create duplicate publishing_compositions" do
            PublishingAdministration::PostApprovalService.create(album.id)

            expect{
              PublishingAdministration::PostApprovalService.create(album.id)
            }.not_to change(PublishingComposition, :count)
          end
        end
      end

      context "when a new album with non-unique/duplicate songs is passed in" do
        let(:album2) { create(:album, person: account.person) }

        let!(:song3) do
          initial_song = create(:song, name: 'SONG 3', album: album2, composition_id: nil, publishing_composition_id: nil)
          initial_song.update_columns(tunecore_isrc: song1.isrc)

          initial_song
        end

        let!(:song4) do
          initial_song = create(:song, name: 'SONG 4', album: album2, composition_id: nil, publishing_composition_id: nil)
          initial_song.update_columns(optional_isrc: song2.isrc)

          initial_song
        end

        before(:each) do
          PublishingAdministration::PostApprovalService.create(album.id)
          album2.reload
        end

        describe "for the first time" do
          it "creates publishing_compositions even with matching isrc's to their respective songs' original counterparts" do
            PublishingAdministration::PostApprovalService.create(album2.id)

            expect(song3.reload.publishing_composition).to be_truthy
            expect(song4.reload.publishing_composition).to be_truthy

            expect(song3.isrc).to be_truthy
            expect(song4.isrc).to be_truthy

            expect(song3.isrc).to eq(song1.isrc)
            expect(song4.isrc).to eq(song2.isrc)
          end

          it "hides each of the publishing_compositions it creates" do
            PublishingAdministration::PostApprovalService.create(album2.id)

            expect(song3.reload.publishing_composition.state).to eq("hidden")
            expect(song4.reload.publishing_composition.state).to eq("hidden")
          end
        end
      end
    end
  end
end
