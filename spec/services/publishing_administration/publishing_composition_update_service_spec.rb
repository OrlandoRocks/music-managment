require "rails_helper"

describe PublishingAdministration::PublishingCompositionUpdateService do
  describe ".update" do
    it "should update all compositions with the name of the publishing_composer" do
      person = create(:person, :with_live_album, :with_publishing_composer_with_purchases)
      person.account = create(:account)
      person.albums.first.songs << create_list(:song, 5)

      publishing_composer = person.publishing_composers.first
      publishing_composer.update(account_id: person.account.id, first_name: "Updated", last_name: "Name")

      publishing_composition = publishing_composer.publishing_compositions.first

      publishing_composer.reload
      publishing_composition.reload

      expect(publishing_composer.name).to_not eq(publishing_composition.name)

      PublishingAdministration::PublishingCompositionUpdateService.update(publishing_composer.id)

      publishing_composer.reload
      publishing_composition.reload

      expect(publishing_composer.name).to eq(publishing_composition.name)
    end
  end
end
