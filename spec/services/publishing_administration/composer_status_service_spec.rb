require "rails_helper"

describe PublishingAdministration::ComposerStatusService do
  before(:each) do
    allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
  end

  context "when a composer is passed in" do
    describe "#needs_cae_number?" do
      context "when the composer does NOT have a PRO or a CAE" do
        let(:composer) { create(:composer) }

        it "returns false" do
          expect(PublishingAdministration::ComposerStatusService.new(composer).needs_cae_number?).to be_truthy
        end
      end

      context "when the composer has a PRO but does NOT have a CAE " do
        let(:composer) { 
          create(
            :composer,
            performing_rights_organization_id: 1) 
          }

        it "returns false" do
          expect(PublishingAdministration::ComposerStatusService.new(composer).needs_cae_number?).to be_truthy
        end
      end

      context "when the composer has a CAE but does NOT have a PRO " do
        let(:composer) { 
          create(
            :composer,
            cae: "123456789") 
          }

        it "returns false" do
          expect(PublishingAdministration::ComposerStatusService.new(composer).needs_cae_number?).to be_falsey
        end
      end

      context "when the composer has a PRO and has a CAE number" do
        let(:composer) { create(:composer, performing_rights_organization_id: 1, cae: "123456789") }

        it "returns false" do
          expect(PublishingAdministration::ComposerStatusService.new(composer).needs_cae_number?).to be_falsey
        end
      end
    end

    describe "#needs_pro?" do
      context "when the composer does NOT have a PRO or a CAE" do
        let(:composer) { create(:composer) }

        it "returns false" do
          expect(PublishingAdministration::ComposerStatusService.new(composer).needs_pro?).to be_truthy
        end
      end

      context "when the composer has a PRO but does NOT have a CAE " do
        let(:composer) { 
          create(
            :composer,
            performing_rights_organization_id: 1)
          }

        it "returns false" do
          expect(PublishingAdministration::ComposerStatusService.new(composer).needs_pro?).to be_falsey
        end
      end

      context "when the composer has a CAE but does NOT have a PRO " do
        let(:composer) { 
          create(
            :composer,
            cae: "123456789")
          }

        it "returns false" do
          expect(PublishingAdministration::ComposerStatusService.new(composer).needs_pro?).to be_truthy
        end
      end

      context "when the composer has a PRO and has a CAE number" do
        let(:composer) { create(:composer, performing_rights_organization_id: 1, cae: "123456789") }

        it "returns false" do
          expect(PublishingAdministration::ComposerStatusService.new(composer).needs_pro?).to be_falsey
        end
      end
    end

    describe "#paid_for?" do
      context "when the composer has NOT purchased pub admin" do
        let(:composer) { create(:composer) }

        it "returns false" do
          expect(PublishingAdministration::ComposerStatusService.new(composer).paid_for?).to be_falsey
        end
      end

      context "when the composer has purchased pub admin" do
        let(:account)   { create(:account) }
        let(:composer)  { create(:composer, :with_pub_admin_purchase,
                                person: account.person,
                                account: account)
                        }

        it "returns true" do
          expect(PublishingAdministration::ComposerStatusService.new(composer).paid_for?).to be_truthy
        end
      end
    end

    describe "#pub_admin_product_in_cart?" do
      context "when the pub admin purchase is NOT in the cart" do
        let(:composer) { create(:composer) }

        it "returns false" do
          expect(PublishingAdministration::ComposerStatusService.new(composer).pub_admin_product_in_cart?).to be_falsey
        end
      end

      context "when the pub admin purchase is in the cart" do
        let(:account)   { create(:account) }
        let(:composer)  { create(:composer, :with_pub_admin_purchase,
                                person: account.person,
                                account: account)
                        }

        it "returns true" do
          composer.related_purchases.first.update(invoice_id: nil, paid_at: nil)
          expect(PublishingAdministration::ComposerStatusService.new(composer).pub_admin_product_in_cart?).to be_truthy
        end
      end
    end

    describe "#state" do
      context "when the composer is terminated" do
        let(:account) { create(:account) }
        let(:composer) do
          create(
            :composer,
            :with_pub_admin_purchase,
            person: account.person,
            account: account,
            terminated_at: DateTime.now,
            performing_rights_organization_id: 1,
            cae: "123456789"
          )
        end
        let!(:terminated_composer) { create(:terminated_composer, :fully, composer: composer) }

        it "return terminated" do
          expect(PublishingAdministration::ComposerStatusService.new(composer.reload).state).to eq :terminated
        end
      end

      context "when the composer is not paid for" do
        let(:account) { create(:account) }
        let(:composer) do
          create(
            :composer,
            person: account.person,
            account: account,
            performing_rights_organization_id: 1,
            cae: "123456789"
          )
        end

        it "returns pending" do
          expect(PublishingAdministration::ComposerStatusService.new(composer).state).to eq :pending
        end
      end

      context "when the composer does not have a valid LOD" do
        let(:account) { create(:account) }
        let(:composer) do
          create(
            :composer,
            :with_pub_admin_purchase,
            person: account.person,
            account: account,
            performing_rights_organization_id: 1,
            cae: "123456789"
          )
        end
        let(:lod) { create(:legal_document, signed_at: nil) }

        it "returns pending when LOD is not signed yet" do
          expect(composer).to receive(:letter_of_direction).and_return(lod)
          expect(PublishingAdministration::ComposerStatusService.new(composer).state).to eq :pending
        end
      end

      context "when the composer does has a valid LOD" do
        let(:account) { create(:account) }
        let(:composer) do
          create(
            :composer,
            :active,
            :with_pub_admin_purchase,
            person: account.person,
            account: account,
            performing_rights_organization_id: 1,
            cae: "123456789"
          )
        end
        let(:lod) { create(:legal_document, signed_at: DateTime.now) }

        it "returns active when LOD is signed" do
          expect(composer).to receive(:letter_of_direction).and_return(lod)
          expect(PublishingAdministration::ComposerStatusService.new(composer).state).to eq :active
        end
      end

      describe "when the composer" do
        let(:account) { create(:account) }
        let(:composer) do
          create(
            :composer,
            :with_pub_admin_purchase,
            person: account.person,
            account: account,
            performing_rights_organization_id: 1,
            cae: "123456789"
          )
        end

        context "does NOT have a PRO or a CAE number" do
          it "returns pending" do
            composer.update_attribute(:cae, nil)
            composer.update_attribute(:performing_rights_organization_id, nil)
            expect(PublishingAdministration::ComposerStatusService.new(composer).state).to eq :pending
          end
        end

        context "has a PRO but does NOT have a CAE number" do
          it "returns pending" do
            composer.update_attribute(:cae, nil)
            expect(PublishingAdministration::ComposerStatusService.new(composer).state).to eq :pending
          end
        end

        context "has a CAE but does NOT have a PRO" do
          it "returns pending" do
            composer.update_attribute(:performing_rights_organization_id, nil)
            expect(PublishingAdministration::ComposerStatusService.new(composer).state).to eq :pending
          end
        end

        context "has a PRO and has a CAE number" do
          it "returns active" do
            expect(PublishingAdministration::ComposerStatusService.new(composer).state).to eq :active
          end
        end
      end
    end
  end

  context "when a publishing_composer is passed in" do
    describe "#needs_cae_number?" do
      context "when the publishing_composer does NOT have a PRO or a CAE" do
        let(:publishing_composer) { create(:publishing_composer) }

        it "returns false" do
          expect(PublishingAdministration::ComposerStatusService.new(publishing_composer).needs_cae_number?).to be_truthy
        end
      end

      context "when the publishing_composer has a PRO but does NOT have a CAE " do
        let(:publishing_composer) { 
          create(
            :publishing_composer, 
            :skip_cae_validation,
            performing_rights_organization_id: 1)
          }

        it "returns false" do
          expect(PublishingAdministration::ComposerStatusService.new(publishing_composer).needs_cae_number?).to be_truthy
        end
      end

      context "when the publishing_composer has a CAE but does NOT have a PRO " do
        let(:publishing_composer) { 
          create(
            :publishing_composer, 
            :skip_cae_validation,
            cae: "123456789")
        }

        it "returns false" do
          expect(PublishingAdministration::ComposerStatusService.new(publishing_composer).needs_cae_number?).to be_falsey
        end
      end

      context "when the publishing_composer has a PRO and has a CAE number" do
        let(:publishing_composer) { create(:publishing_composer, performing_rights_organization_id: 1, cae: "123456789") }

        it "returns false" do
          expect(PublishingAdministration::ComposerStatusService.new(publishing_composer).needs_cae_number?).to be_falsey
        end
      end
    end

    describe "#needs_pro?" do
      context "when the publishing_composer does NOT have a PRO or a CAE" do
        let(:publishing_composer) { create(:publishing_composer) }

        it "returns false" do
          expect(PublishingAdministration::ComposerStatusService.new(publishing_composer).needs_pro?).to be_truthy
        end
      end

      context "when the publishing_composer has a PRO but does NOT have a CAE " do
        let(:publishing_composer) { 
          create(
            :publishing_composer, 
            :skip_cae_validation,
            performing_rights_organization_id: 1) 
          }

        it "returns false" do
          expect(PublishingAdministration::ComposerStatusService.new(publishing_composer).needs_pro?).to be_falsey
        end
      end

      context "when the publishing_composer has a CAE but does NOT have a PRO " do
        let(:publishing_composer) { 
          create(
            :publishing_composer, 
            :skip_cae_validation,
            cae: "123456789")
          }

        it "returns false" do
          expect(PublishingAdministration::ComposerStatusService.new(publishing_composer).needs_pro?).to be_truthy
        end
      end

      context "when the publishing_composer has a PRO and has a CAE number" do
        let(:publishing_composer) { create(:publishing_composer, performing_rights_organization_id: 1, cae: "123456789") }

        it "returns false" do
          expect(PublishingAdministration::ComposerStatusService.new(publishing_composer).needs_pro?).to be_falsey
        end
      end
    end

    describe "#paid_for?" do
      context "when the publishing_composer has NOT purchased pub admin" do
        let(:publishing_composer) { create(:publishing_composer) }

        it "returns false" do
          expect(PublishingAdministration::ComposerStatusService.new(publishing_composer).paid_for?).to be_falsey
        end
      end

      context "when a publishing_composer has purchased pub admin" do
        let(:account)   { create(:account) }
        let(:publishing_composer)  { create(:publishing_composer, :with_pub_admin_purchase,
                                person: account.person,
                                account: account)
                        }

        it "returns true" do
          expect(PublishingAdministration::ComposerStatusService.new(publishing_composer).paid_for?).to be_truthy
        end
      end
    end

    describe "#pub_admin_product_in_cart?" do
      context "when the pub admin purchase is NOT in the cart" do
        let(:publishing_composer) { create(:publishing_composer) }

        it "returns false" do
          expect(PublishingAdministration::ComposerStatusService.new(publishing_composer).pub_admin_product_in_cart?).to be_falsey
        end
      end

      context "when the pub admin purchase is in the cart" do
        let(:account)   { create(:account) }
        let(:publishing_composer)  { create(:publishing_composer, :with_pub_admin_purchase,
                                person: account.person,
                                account: account)
                        }

        it "returns true" do
          publishing_composer.related_purchases.first.update(invoice_id: nil, paid_at: nil)
          expect(PublishingAdministration::ComposerStatusService.new(publishing_composer).pub_admin_product_in_cart?).to be_truthy
        end
      end
    end

    describe "#state" do
      context "when the publishing_composer is terminated" do
        let(:account) { create(:account) }
        let(:publishing_composer) do
          create(
            :publishing_composer,
            :with_pub_admin_purchase,
            person: account.person,
            account: account,
            terminated_at: DateTime.now,
            performing_rights_organization_id: 1,
            cae: "123456789"
          )
        end
        let!(:terminated_composer) { create(:terminated_composer, :fully, publishing_composer: publishing_composer) }

        it "return terminated" do
          expect(PublishingAdministration::ComposerStatusService.new(publishing_composer.reload).state).to eq :terminated
        end
      end

      context "when the publishing_composer is not paid for" do
        let(:account) { create(:account) }
        let(:publishing_composer) do
          create(
            :publishing_composer,
            person: account.person,
            account: account,
            performing_rights_organization_id: 1,
            cae: "123456789"
          )
        end

        it "returns pending" do
          expect(PublishingAdministration::ComposerStatusService.new(publishing_composer).state).to eq :pending
        end
      end

      context "when the publishing_composer does not have a valid LOD" do
        let(:account) { create(:account) }
        let(:publisher) { create(:publisher) }
        let(:publishing_composer) do
          create(
            :publishing_composer,
            :with_pub_admin_purchase,
            person: account.person,
            account: account,
            publisher: publisher,
            performing_rights_organization_id: 1,
            cae: "123456789"
          )
        end
        let(:lod) { create(:legal_document, signed_at: nil) }

        it "returns pending when LOD is not signed yet" do
          expect(publishing_composer).to receive(:letter_of_direction).and_return(lod)
          expect(PublishingAdministration::ComposerStatusService.new(publishing_composer).state).to eq :pending
        end
      end

      context "when the publishing_composer does has a valid LOD" do
        let(:account) { create(:account) }
        let(:publisher) { create(:publisher) }
        let(:publishing_composer) do
          create(
            :publishing_composer,
            :with_pub_admin_purchase,
            person: account.person,
            account: account,
            publisher: publisher,
            performing_rights_organization_id: 1,
            cae: "123456789"
          )
        end
        let(:lod) { create(:legal_document, signed_at: DateTime.now) }

        it "returns active when LOD is signed" do
          expect(publishing_composer).to receive(:letter_of_direction).and_return(lod)
          expect(PublishingAdministration::ComposerStatusService.new(publishing_composer).state).to eq :active
        end
      end

      describe "when the publishing_composer" do
        let(:account) { create(:account) }
        let(:publishing_composer) do
          create(
            :publishing_composer,
            :with_pub_admin_purchase,
            person: account.person,
            account: account,
            performing_rights_organization_id: 1,
            cae: "123456789"
          )
        end

        context "does NOT have a PRO or a CAE number" do
          it "returns pending" do
            publishing_composer.update_attribute(:cae, nil)
            publishing_composer.update_attribute(:performing_rights_organization_id, nil)
            expect(PublishingAdministration::ComposerStatusService.new(publishing_composer).state).to eq :pending
          end
        end

        context "has a PRO but does NOT have a CAE number" do
          it "returns pending" do
            publishing_composer.update_attribute(:cae, nil)
            expect(PublishingAdministration::ComposerStatusService.new(publishing_composer).state).to eq :pending
          end
        end

        context "has a CAE but does NOT have a PRO" do
          it "returns pending" do
            publishing_composer.update_attribute(:performing_rights_organization_id, nil)
            expect(PublishingAdministration::ComposerStatusService.new(publishing_composer).state).to eq :pending
          end
        end

        context "has a PRO and has a CAE number" do
          it "returns active" do
            expect(PublishingAdministration::ComposerStatusService.new(publishing_composer).state).to eq :active
          end
        end
      end
    end
  end
end
