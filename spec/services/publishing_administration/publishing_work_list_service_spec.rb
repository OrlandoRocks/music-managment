require "rails_helper"

describe PublishingAdministration::PublishingWorkListService do
  describe ".list" do
    let(:work_code) { Faker::Number.number(digits: 10) }
    let(:writer_code) { Faker::Number.number(digits: 8) }
    let!(:account)  { create(:account, provider_account_id: Faker::Number.number(digits: 8)) }
    let!(:publishing_composer) { create(:publishing_composer, account: account, provider_identifier: writer_code) }

    before do
      allow(PublishingAdministration::ApiClientServices::FetchWorkListService).to receive(:fetch) { double(:response, valid?: true, works: []) }
      allow(PublishingAdministration::PublishingWorkUpdateService).to receive(:update)
      allow(PublishingAdministration::PublishingWorkMatcherService).to receive(:match) { response }
    end

    context "when there are unmatched works" do
      let(:unmatched_works) do
        [
          OpenStruct.new(
            title: "New PublishingComposition",
            work_code: work_code,
            writers: [
              OpenStruct.new(writer_code: writer_code, right_to_collect: true)
            ]
          )
        ]
      end

      let(:response) do
        double(:response,
               valid?: true,
               unmatched_works: unmatched_works,
               matched_publishing_compositions: []
              )
      end

      it "creates a publishing_composition" do
        expect(PublishingAdministration::PublishingNonTunecoreService)
          .to receive(:create_ntc_album_and_song)
        expect(PublishingAdministration::PublishingWorkUpdateService)
          .to receive(:update)

        expect {
          PublishingAdministration::PublishingWorkListService.list(account)
        }.to change {
          PublishingComposition.count
        }.by(1)
      end
    end

    context "when there are matched publishing_compositions" do
      let(:publishing_composition) { create(:publishing_composition, name: "No Soggy Bottoms", provider_identifier: work_code) }

      let(:matched_publishing_compositions) do
        [
          [
            publishing_composition,
            OpenStruct.new(
              title: "No Soggy Bottoms",
              work_code: work_code,
            )
          ]
        ]
      end

      let(:response) do
        double(:response,
               valid?: true,
               unmatched_works: [],
               matched_publishing_compositions: matched_publishing_compositions
              )
      end

      it "invokes the PublishingWorkUpdateService" do
        expect(PublishingAdministration::PublishingWorkUpdateService).to receive(:update)

        PublishingAdministration::PublishingWorkListService.list(account)
      end
    end

    context "when updating terminated publishing_compositions state" do
      let(:response) do
        double(:response,
               valid?: true,
               unmatched_works: [],
               matched_publishing_compositions: [])
      end

      it "invokes the UpdateTerminatedPublishingCompositionsService" do
        expect(PublishingAdministration::UpdateTerminatedPublishingCompositionsService).to receive(:update).with(account)

        PublishingAdministration::PublishingWorkListService.list(account)
      end
    end
  end
end
