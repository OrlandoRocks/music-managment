require "rails_helper"

describe PublishingAdministration::ErrorService do
  describe '.log_errors' do
    let(:composer)    { create(:composer) }
    let(:composition) { create(:composition) }
    let(:params)      { { composer: composer, composition: composition } }

    context "the request does not have a requestable" do
      it "does not create a rights app error" do
        request = RightsApp::Requests::CreateWork.new(params)
        allow(request).to receive(:requestable).and_return(nil)
        service = PublishingAdministration::ErrorService.new(request, nil)
        allow(service).to receive(:raise_rights_app_error)

        expect(RightsAppError).not_to receive(:create!)

        service.log_errors
      end
    end

    context "the request has a requestable" do
      it "creates a rights app error associated to the requestable" do
        request = RightsApp::Requests::CreateWork.new(params)
        api_endpoint = "https://uatapi.rightsapp.co.uk/api/v1/Writers"
        status = 400

        json_response = double(:json_response, :status => status)
        response = RightsApp::Responses::Work.new(json_response)
        allow(response).to receive(:api_endpoint).and_return(api_endpoint)
        allow(response).to receive(:body).and_return({})

        requestable = request.requestable

        expect(requestable.rights_app_errors).to be_empty
        expect {
          PublishingAdministration::ErrorService.log_errors(request, response)
        }.to raise_error(PublishingAdministration::ErrorService::ApiError)

        rights_app_error = requestable.reload.rights_app_errors.first
        expect(rights_app_error.api_endpoint).to eq api_endpoint
        expect(rights_app_error.http_status_code).to eq status
        expect(rights_app_error.http_method).to eq "POST"
        expect(rights_app_error.requestable).to eq requestable
      end
    end
  end
end
