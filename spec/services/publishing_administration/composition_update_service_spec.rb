require "rails_helper"

describe PublishingAdministration::CompositionUpdateService do
  describe ".update" do
    it "should update all compositions with the name of the composer" do
      person = create(:person, :with_live_album, :with_composer_with_purchases)
      person.account = create(:account)
      person.albums.first.songs << create_list(:song, 5)
      composer = person.composers.first
      composer.update(account_id: person.account.id, first_name: "Updated", last_name: "Name")
      composition = composer.compositions.first

      composer.reload
      composition.reload

      expect(composer.name).to_not eq(composition.name)
      expect(composer.name).to_not eq(composition.cwr_name)

      PublishingAdministration::CompositionUpdateService.update(composer.id)

      composer.reload
      composition.reload

      expect(composer.name).to eq(composition.name)
      expect(composer.name).to eq(composition.cwr_name)
    end
  end
end
