require "rails_helper"

describe PublishingAdministration::PublishingNonTunecoreService do
  let!(:artist)     { create(:artist, name: "DJ Big Ma$e") }
  let!(:account)               { create(:account) }
  let(:publishing_composer)    { create(:publishing_composer, first_name: "Jason", last_name: "Allen", account: account) }
  let(:publishing_composition) { create(:publishing_composition, name: "House Sessions", account: account) }
  let(:work) { { "PerformingArtists" => [{ "Id" => "1234", "Name" => artist.name }] } }
  let(:params)  { { publishing_composer: publishing_composer, publishing_composition: publishing_composition, work: work } }

  before do
    allow(PublishingAdministration::ApiClientServices::GetPublishingWorkService)
      .to receive(:get_work)
      .and_return(work)
  end

  it "creates a non_tunecore_album" do
    PublishingAdministration::PublishingNonTunecoreService.create_ntc_album_and_song(params)
    expect(NonTunecoreAlbum.exists?(name: publishing_composition.name, publishing_composer_id: publishing_composer.id)).to be_truthy
  end

  it "creates a non_tunecore_song" do
    PublishingAdministration::PublishingNonTunecoreService.create_ntc_album_and_song(params)
    expect(NonTunecoreSong.exists?(artist_id: artist.id, publishing_composition_id: publishing_composition.id)).to be_truthy
  end
end
