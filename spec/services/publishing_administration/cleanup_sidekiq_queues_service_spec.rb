require "rails_helper"

describe PublishingAdministration::CleanupSidekiqQueuesService do
  let(:job) { double(:job, klass: "PublishingAdministration::WorkReconciliationWorker", delete: true) }

  it "should delete any jobs in the Sidekiq dead set" do
    allow(Sidekiq::DeadSet).to receive(:new).and_return([job])
    expect(job).to receive(:delete)
    PublishingAdministration::CleanupSidekiqQueuesService.cleanup
  end
end 
