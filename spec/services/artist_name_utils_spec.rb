require "rails_helper"

describe ArtistNameUtils do
  describe 'parsing for comparison' do
    [
      ['Tracy                Jordan', 'Tracy   Jordan'],
      ['Hém', 'Hem'],
      ['Mỹ Tâmü', 'My Tamu'],
      ['НИСЫ', 'Нисы']
    ].each do |a, b|
      it "finds the names to be equal" do
        expect(described_class.parse_for_comparison(a) == described_class.parse_for_comparison(b)).to eq true
      end
    end
  end
end
