require "rails_helper"

describe NegativeBalanceAlertingService do
  let(:person) { create(:person) }
  before do
    person.person_balance.update(balance: -301)
    allow_any_instance_of(NegativeBalanceAlertingService).to receive(:query_person_balances).and_return([person.person_balance])
  end
  describe '.alert' do
    it "kicks off a mailer with all person balances less than -200 from the past day" do
      expect(NegativeBalanceAlertMailer).to receive(:alert_email).with([person.person_balance], Date.yesterday).and_return(double(deliver: true))
      NegativeBalanceAlertingService.alert
    end
  end
end
