require "rails_helper"
describe CorporateEntityMappingService do
  before(:each) do
    csv = CSV.new("", headers: true)
    csv << ['country_iso', 'corporate_entity_id']
    csv << ['GK', '1']
    csv << ['TI', '2']

    allow(CSV).to receive(:open).and_return(csv)
  end

  let!(:greg_land) do create(:country,
                             iso_code: 'GK',
                             name: 'Kingdom of Greg',
                             iso_code_3: "KOG",
                             corporate_entity_id: 2)
  end
  let!(:tunecore_land) do create(:country,
                                 iso_code: 'TI',
                                 name: 'TuneCore Imperial Quarters',
                                 iso_code_3: "TIP",
                                 corporate_entity_id: 1)
  end

  context "with countries that have not yet been updated" do
    it "updates the countries" do
      CorporateEntityMappingService.new("data-file-key").map_to_countries
      greg_land.reload
      tunecore_land.reload

      expect(greg_land.corporate_entity_id).to be(1)
      expect(tunecore_land.corporate_entity_id).to be(2)
    end
  end
  context "with countries that have already been updated" do
    it "updates the countries" do
      greg_land.update(corporate_entity_id: 1)
      tunecore_land.update(corporate_entity_id: 2)
      CorporateEntityMappingService.new("data-file-key").map_to_countries

      expect(greg_land).not_to receive(:update)
      expect(tunecore_land).not_to receive(:update)
    end
  end
end
