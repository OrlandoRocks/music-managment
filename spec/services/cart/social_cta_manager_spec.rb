require "rails_helper"

describe Cart::SocialCtaManager do
  describe "#manage" do
    let(:person)    { create(:person) }

    before(:each) do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
    end

    describe "#show_cta" do
      it "is true if available in country" do
        allow(Social::PlanStatus).to receive_message_chain(:for, :plan).and_return("free")
        social_cta = described_class.manage(person, [])
        expect(social_cta.show_cta?).to eq true
      end

      it "is false if not available in country" do
        allow(Social::PlanStatus).to receive_message_chain(:for, :plan).and_return("free")
        person.country_website_id = 5
        social_cta = described_class.manage(person, [])
        expect(social_cta.show_cta?).to eq false
      end
    end

    context "cta_type" do

      it "is 'upgrade' if a person has a free plan AND a token" do
        allow(Social::PlanStatus).to receive_message_chain(:for, :plan).and_return("free")
        allow(person).to receive(:tc_social_token).and_return("fake_token")

        social_cta = described_class.manage(person, [])
        expect(social_cta.cta_type).to eq "upgrade"
      end

      it "is 'buy' if a person has a free plan and NO token" do
        allow(Social::PlanStatus).to receive_message_chain(:for, :plan).and_return("free")
        allow(person).to receive(:tc_social_token).and_return(nil)

        social_cta = described_class.manage(person, [])
        expect(social_cta.cta_type).to eq "buy"
      end

      it "is 'buy' if a person has a free_expired plan" do
        allow(Social::PlanStatus).to receive_message_chain(:for, :plan).and_return("free_expired")

        social_cta = described_class.manage(person, [])
        expect(social_cta.cta_type).to eq "buy"
      end

      it "is 'buy' if a person has no plan" do
        allow(Social::PlanStatus).to receive_message_chain(:for, :plan).and_return(nil)

        social_cta = described_class.manage(person, [])
        expect(social_cta.cta_type).to eq "buy"
      end

      it "is 'none' if a person has a pro plan" do
        allow(Social::PlanStatus).to receive_message_chain(:for, :plan).and_return("pro")

        social_cta = described_class.manage(person, [])
        expect(social_cta.cta_type).to eq "none"
      end

      it "is 'none' if a person has a pro_expired plan" do
        allow(Social::PlanStatus).to receive_message_chain(:for, :plan).and_return("pro_expired")

        social_cta = described_class.manage(person, [])
        expect(social_cta.cta_type).to eq "none"
      end

      it "is 'none' if Social is already in the cart" do
        purchase = create(:purchase, paid_at: nil)
        social_product_id = Product.where(display_name: "tc_social_monthly", country_website_id: person.country_website_id).first.id
        allow(purchase).to receive(:product_id).and_return(social_product_id)

        social_cta = described_class.manage(person, [purchase])
        expect(social_cta.cta_type).to eq "none"
      end
    end

    context "product_id" do

      it "gets the correct product id for US customer" do
        allow(person).to receive(:country_website_id).and_return(1)
        social_product = Product.where(display_name: "tc_social_monthly", country_website_id: person.country_website_id).first

        social_cta = described_class.manage(person, [])
        expect(social_cta.social_product).to eq social_product
      end

      it "gets the correct product id for AU customer" do
        allow(person).to receive(:country_website_id).and_return(4)
        social_product = Product.where(display_name: "tc_social_monthly", country_website_id: person.country_website_id).first

        social_cta = described_class.manage(person, [])
        expect(social_cta.social_product).to eq social_product
      end
    end
  end
end
