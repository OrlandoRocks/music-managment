require "rails_helper"

describe Cart::Payment::Manager do

  describe "#initialize" do
    before(:each) do
      @person = TCFactory.create(:person)
      @manager = Cart::Payment::Manager.new(@person, [], '0.0.0.0')
    end

    it "should set successful to false" do
      expect(@manager.successful?).to eq(false)
    end
  end

  describe "#reauthorize" do
    before(:each) do
      @person = TCFactory.create(:person)
      @manager = Cart::Payment::Manager.new(@person, [], '0.0.0.0')

      @balance = double(Cart::Payment::Strategies::Balance)
      @credit = double(Cart::Payment::Strategies::CreditCard)

      @manager.add_strategy(@balance)
      @manager.add_strategy(@credit)
    end

    it "should call authorizer on all strategies if none return authorizer" do
      expect(@balance).to receive(:authorizer).and_return(nil)
      expect(@credit).to receive(:authorizer).and_return(nil)

      expect(@manager.reauthorize.blank?).to eq(true)
    end

    it "should return first authorizer and not call others" do
      expect(@balance).to receive(:authorizer).twice.and_return(:balance)
      expect(@credit).not_to receive(:authorizer)

      expect(@manager.reauthorize).to eq(:balance)
    end
  end

  describe "#process!" do

    before(:each) do
      @person  = FactoryBot.create(:person)
      @purchase = FactoryBot.create(:purchase, person: @person)
      @manager = Cart::Payment::Manager.new(@person, [@purchase], '0.0.0.0')
      @balance = double(Cart::Payment::Strategies::Balance)
      @credit  = double(Cart::Payment::Strategies::CreditCard)
      allow(@balance).to receive(:rollback_handler)
      allow(@credit).to receive(:rollback_handler)
    end

    def add_balance_and_credit_to_manager
      @manager.add_strategy(@balance)
      @manager.add_strategy(@credit)
    end

    it "should call process on each payment method if needed" do
      album  = create(:album, person: @person)
      invoice = create(:invoice, :with_purchase, person: @person, related: album)
      expect(Invoice).to receive(:factory).and_return(invoice)
      allow(invoice).to receive(:settled?).and_return(false)
      allow(invoice).to receive(:reload)
      allow(invoice).to receive(:can_settle?).and_return(false)

      add_balance_and_credit_to_manager

      expect(@balance).to receive(:process)
      expect(@credit).to receive(:process) { allow(invoice).to receive(:settled?).and_return(true) }
      @manager.process!
    end

    it "should not call process on remaining strategies if settled" do
      invoice = double(Invoice)
      expect(Invoice).to receive(:factory).and_return(invoice)
      allow(invoice).to receive(:settled?).and_return(true)
      allow(invoice).to receive(:reload)
      allow(invoice).to receive(:can_settle?).and_return(false)
      allow(invoice).to receive(:purchases).and_return(@purchase)

      add_balance_and_credit_to_manager

      expect(@balance).to receive(:process)
      expect(@credit).not_to receive(:process)

      @manager.process!
    end

    it "should call #rollback_handler on strategies on errors" do
      album  = create(:album, person: @person)
      invoice = create(:invoice, :with_purchase, person: @person, related: album)
      expect(Invoice).to receive(:factory).and_return(invoice)
      allow(invoice).to receive(:settled?).and_return(true)
      allow(invoice).to receive(:reload)
      allow(invoice).to receive(:can_settle?).and_return(false)

      add_balance_and_credit_to_manager

      expect(@balance).to receive(:process) { raise "this is an issue" }
      expect(@credit).not_to receive(:process)

      expect(@balance).to receive(:rollback_handler)
      expect(@credit).to receive(:rollback_handler)

      @manager.process!
    end

    it "should mark manager as successful if invoice settled" do
      invoice = double(Invoice)
      expect(Invoice).to receive(:factory).and_return(invoice)
      allow(invoice).to receive(:settled?).and_return(false)
      allow(invoice).to receive(:reload)
      allow(invoice).to receive(:can_settle?).and_return(false)
      allow(invoice).to receive(:purchases).and_return(@purchase)

      add_balance_and_credit_to_manager

      expect(@balance).to receive(:process)
      expect(@credit).to receive(:process) { allow(invoice).to receive(:settled?).and_return(true) }

      @manager.process!
      expect(@manager.successful?).to eq(true)
    end

    it "should not use payment methods if invoice is free" do
      balance = double(Cart::Payment::Strategies::Balance)
      credit = double(Cart::Payment::Strategies::CreditCard)

      invoice = double(Invoice)
      expect(Invoice).to receive(:factory).and_return(invoice)
      allow(invoice).to receive(:settled?).and_return(true)
      allow(invoice).to receive(:reload)
      allow(invoice).to receive(:can_settle?).and_return(true)
      expect(invoice).to receive(:settled!)
      allow(invoice).to receive(:purchases).and_return(@purchase)

      add_balance_and_credit_to_manager

      expect(balance).not_to receive(:process)
      expect(credit).not_to receive(:process)

      @manager.process!
      expect(@manager.successful?).to eq(true)
    end
    

    context 'payment processing' do
      context 'paypal' do
        let(:person) { create(:person, :with_stored_paypal_account) }
        let(:album) { create(:album, person: person) }
        let(:product) { Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID) }
        let(:purchase) { create(:purchase, :unpaid, person: person, product: product, related: album) }
        let(:renewal) { create(:renewal, person: person, item_to_remew: product.product_items.first, purchase: purchase) }
        let(:renewal_item) { create(:renewal_item, renewal: renewal, related: album) }
        let(:manager) { Cart::Payment::Manager.new(person, [purchase], '0.0.0.0') }
        let(:paypal_strategy) { Cart::Payment::Strategies::Paypal.new(person.stored_paypal_accounts.first, person) }

        context 'declined transaction' do
          before do
            paypal_api_transaction = double
            allow(paypal_api_transaction).to receive(:response).and_return({
              'AMT' => 100,
              'ACK' => 'Failure',
              'PAYMENTSTATUS' => 'Declined',
            })
            allow(PayPalAPI).to receive(:reference_transaction).and_return(paypal_api_transaction)
            manager.add_strategy(paypal_strategy)
          end

          it 'should save the paypal transaction' do
            expect { manager.process! }
              .to change { PaypalTransaction.count }.by(1)

            purchase.reload
            expect(purchase.paid?).to be false
            expect(purchase.invoice).to be_nil
            expect(manager.successful?).to be false
          end

          it 'should not settle invoice' do
            expect { manager.process! }
              .to change { Invoice.count }.by(0)

            purchase.reload
            expect(purchase.paid?).to be false
            expect(purchase.invoice_id).to be_nil
            expect(manager.successful?).to be false
          end

          it 'should mark the purchase as failed' do
            expect { manager.process! }
              .to change { purchase.status }.from('unprocessed').to('failed')

            purchase.reload
            expect(purchase.paid?).to be false
            expect(purchase.invoice_id).to be_nil
            expect(manager.successful?).to be false
          end
          it "should log error with notice" do
            account  = create(:account, person: person)
            composer = create(:composer, person: person, account: account)
            invoice  = create(:invoice, :with_purchase, person: person, related: composer)

            invoice_error_message = "\ninvoice errors: none"
            notice_msg = "\nnotice: An error occurred while processing your PayPal payment. Please contact customer support."
            error_msg  = "Transaction Failed: invoice not settled! invoice: #{invoice.to_json}#{invoice_error_message}#{notice_msg}"

            back_trace = ["error/path/one", "error/path/two"]

            allow(invoice).to receive(:valid?).and_return(true)
            allow_any_instance_of(Cart::Payment::Manager).to receive(:invoice).and_return(invoice)
            allow_any_instance_of(RuntimeError).to receive(:backtrace).and_return(back_trace)

            expect(Rails.logger).to receive(:error).once.with(error_msg)
            expect(Rails.logger).to receive(:error).once.with(back_trace)

            manager.process!
          end
          it 'should notify Airbrake' do
            error_message = "invoice not settled!"
            notice = "An error occurred while processing your PayPal payment. Please contact customer support."

            account  = create(:account, person: person)
            composer = create(:composer, person: person, account: account)
            invoice  = create(:invoice, :with_purchase, person: person, related: composer)

            allow_any_instance_of(Cart::Payment::Manager).to receive(:invoice).and_return(invoice)

            expect(Airbrake).to receive(:notify).with(duck_type(:message, :backtrace),
                                                      invoice: invoice,
                                                      invoice_error: "none",
                                                      notice: notice)
            manager.process!
          end
          it "should create an InvoiceLog" do
            account  = create(:account, person: person)
            composer = create(:composer, person: person, account: account)
            invoice  = create(:invoice, :with_purchase, person: person, related: composer)
            purchase = invoice.purchases.first

            allow_any_instance_of(Cart::Payment::Manager).to receive(:invoice).and_return(invoice)

            manager.process!

            log = InvoiceLog.first

            expected_options = {
                person_id: person.id,
                current_method_name: "process!",
                truncated_message: "Transaction Failed: invoice not settled! invoice:",
                invoice_id: invoice.id,
                purchase_id: purchase.id,
                invoice_created_at: invoice.created_at,
                invoice_final_settlement_amount_cents: invoice.final_settlement_amount_cents,
                invoice_currency: invoice.currency,
                invoice_batch_status: invoice.batch_status,
                invoice_settled_at: invoice.settled_at,
                invoice_vat_amount_cents: invoice.vat_amount_cents,
                invoice_foreign_exchange_pegged_rate_id: invoice.foreign_exchange_pegged_rate_id,
                purchase_created_at: purchase.created_at,
                purchase_product_id: purchase.product_id,
                purchase_cost_cents: purchase.cost_cents,
                purchase_discount_cents: purchase.discount_cents,
                purchase_currency: purchase.currency,
                purchase_paid_at: purchase.paid_at,
                purchase_salepoint_id: purchase.salepoint_id,
                purchase_no_purchase_album_id: purchase.no_purchase_album_id,
                purchase_related_id: purchase.related_id,
                purchase_related_type: purchase.related_type,
                purchase_targeted_product_id: purchase.targeted_product_id,
                purchase_price_adjustment_histories_count: purchase.price_adjustment_histories_count,
                purchase_vat_amount_cents: purchase.vat_amount_cents,
                purchase_status: purchase.status
            }
            actual_options = {
                person_id: log.person_id,
                current_method_name: log.current_method_name,
                truncated_message: log.message[0,49],
                invoice_id: log.invoice_id,
                purchase_id: log.purchase_id,
                invoice_created_at: log.invoice_created_at,
                invoice_final_settlement_amount_cents: log.invoice_final_settlement_amount_cents,
                invoice_currency: log.invoice_currency,
                invoice_batch_status: log.invoice_batch_status,
                invoice_settled_at: log.invoice_settled_at,
                invoice_vat_amount_cents: log.invoice_vat_amount_cents,
                invoice_foreign_exchange_pegged_rate_id: log.invoice_foreign_exchange_pegged_rate_id,
                purchase_created_at: log.purchase_created_at,
                purchase_product_id: log.purchase_product_id,
                purchase_cost_cents: log.purchase_cost_cents,
                purchase_discount_cents: log.purchase_discount_cents,
                purchase_currency: log.purchase_currency,
                purchase_paid_at: log.purchase_paid_at,
                purchase_salepoint_id: log.purchase_salepoint_id,
                purchase_no_purchase_album_id: log.purchase_no_purchase_album_id,
                purchase_related_id: log.purchase_related_id,
                purchase_related_type: log.purchase_related_type,
                purchase_targeted_product_id: log.purchase_targeted_product_id,
                purchase_price_adjustment_histories_count: log.purchase_price_adjustment_histories_count,
                purchase_vat_amount_cents: log.purchase_vat_amount_cents,
                purchase_status: log.purchase_status
            }

            expect(expected_options).to eq(actual_options)
          end
        end

        context 'successful transaction' do
          before do
            paypal_api_transaction = double
            allow(paypal_api_transaction).to receive(:response).and_return({
              'AMT' => 100,
              'ACK' => 'Success',
              'PAYMENTSTATUS' => 'Completed',
            })
            allow(PayPalAPI).to receive(:reference_transaction).and_return(paypal_api_transaction)
            manager.add_strategy(paypal_strategy)
          end

          it 'should settle invoice' do
            expect { manager.process! }
              .to change { Invoice.count }.by(1)

            purchase.reload
            expect(purchase.paid?).to be true
            expect(purchase.invoice_id).to_not be_nil
            expect(manager.successful?).to be true
          end

          it 'should save the paypal transaction' do
            expect { manager.process! }
              .to change { PaypalTransaction.count }.by(1)

            purchase.reload
            expect(purchase.paid?).to be true
            expect(purchase.invoice_id).to_not be_nil
            expect(manager.successful?).to be true
          end

          it 'should mark the purchase as processed' do
            expect { manager.process! }
              .to change { purchase.status }.from('unprocessed').to('processed')

            purchase.reload
            expect(purchase.paid?).to be true
            expect(purchase.invoice_id).to_not be_nil
            expect(manager.successful?).to be true
          end

          context 'failed to process purchase' do
            it 'should not settle invoice' do
              allow(Purchase).to receive(:process_purchases_for_invoice).and_raise('boom')

              expect { manager.process! }
                .to change { Invoice.count }.by(0)
                .and change { PaypalTransaction.count }.by(1)
                .and change { purchase.status }.from('unprocessed').to('failed')

              purchase.reload
              expect(purchase.paid?).to be false
              expect(purchase.invoice_id).to be_nil
              expect(manager.successful?).to be false
            end
          end
        end
      end

      context 'credit card processing using braintree' do
        let(:person) { create(:person) }
        let(:album) { create(:album, person: person) }
        let(:product) { Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID) }
        let(:purchase) { create(:purchase, :unpaid, person: person, product: product, related: album) }
        let(:renewal) { create(:renewal, person: person, item_to_remew: product.product_items.first, purchase: purchase) }
        let(:renewal_item) { create(:renewal_item, renewal: renewal, related: album) }
        let(:manager) { Cart::Payment::Manager.new(person, [purchase], '0.0.0.0') }
        let(:credit_card) { create(:stored_credit_card, person: person) }
        let(:credit_strategy) { Cart::Payment::Strategies::CreditCard.new(credit_card.id) }

        context 'declined braintree transaction' do
          before do
            allow_any_instance_of(ActiveMerchant::Billing::BraintreeOrangeGateway).to receive(:purchase).and_return(ActiveMerchant::Billing::Response.new(true, 'yay', {
              currency: 'USD',
              response_code: 200 # Also declined, refer to app/models/braintree_transaction.rb:280
            }))
            manager.add_strategy(credit_strategy)
          end

          it 'should save the braintree transaction' do
            expect { manager.process! }
              .to change { BraintreeTransaction.count }.by(1)

            purchase.reload
            expect(purchase.paid?).to be false
            expect(purchase.invoice).to be_nil
            expect(manager.successful?).to be false
          end

          it 'should not settle invoice' do
            expect { manager.process! }
              .to change { Invoice.count }.by(0)

            purchase.reload
            expect(purchase.paid?).to be false
            expect(purchase.invoice_id).to be_nil
            expect(manager.successful?).to be false
          end

          it 'should mark the purchase as failed' do
            expect { manager.process! }
              .to change { purchase.status }.from('unprocessed').to('failed')

            purchase.reload
            expect(purchase.paid?).to be false
            expect(purchase.invoice_id).to be_nil
            expect(manager.successful?).to be false
          end
          it "should log error with notice" do
            account  = create(:account, person: person)
            composer = create(:composer, person: person, account: account)
            invoice  = create(:invoice, :with_purchase, person: person, related: composer)

            invoice_error_message = "\ninvoice errors: none"
            notice_msg = "\nnotice: Your credit card was denied. Please check that you've entered your card information correctly."
            error_msg  = "Transaction Failed: invoice not settled! invoice: #{invoice.to_json}#{invoice_error_message}#{notice_msg}"

            back_trace = ["error/path/one", "error/path/two"]

            allow(invoice).to receive(:valid?).and_return(true)
            allow_any_instance_of(Cart::Payment::Manager).to receive(:invoice).and_return(invoice)
            allow_any_instance_of(RuntimeError).to receive(:backtrace).and_return(back_trace)

            expect(Rails.logger).to receive(:error).once.with(error_msg)
            expect(Rails.logger).to receive(:error).once.with(back_trace)

            manager.process!
          end
          it 'should notify Airbrake' do
            error_message = "invoice not settled!"
            notice = "Your credit card was denied. Please check that you've entered your card information correctly."

            account  = create(:account, person: person)
            composer = create(:composer, person: person, account: account)
            invoice  = create(:invoice, :with_purchase, person: person, related: composer)

            allow_any_instance_of(Cart::Payment::Manager).to receive(:invoice).and_return(invoice)
            expect(Airbrake).to receive(:notify).with(duck_type(:message, :backtrace),
                                                      invoice: invoice,
                                                      invoice_error: "none",
                                                      notice: notice)
            manager.process!
          end
          it "should create an InvoiceLog" do
            account  = create(:account, person: person)
            composer = create(:composer, person: person, account: account)
            invoice  = create(:invoice, :with_purchase, person: person, related: composer)
            purchase = invoice.purchases.first

            allow_any_instance_of(Cart::Payment::Manager).to receive(:invoice).and_return(invoice)

            manager.process!

            log = InvoiceLog.first

            expected_options = {
                person_id: person.id,
                current_method_name: "process!",
                truncated_message: "Transaction Failed: invoice not settled! invoice:",
                invoice_id: invoice.id,
                purchase_id: purchase.id,
                invoice_created_at: invoice.created_at,
                invoice_final_settlement_amount_cents: invoice.final_settlement_amount_cents,
                invoice_currency: invoice.currency,
                invoice_batch_status: invoice.batch_status,
                invoice_settled_at: invoice.settled_at,
                invoice_vat_amount_cents: invoice.vat_amount_cents,
                invoice_foreign_exchange_pegged_rate_id: invoice.foreign_exchange_pegged_rate_id,
                purchase_created_at: purchase.created_at,
                purchase_product_id: purchase.product_id,
                purchase_cost_cents: purchase.cost_cents,
                purchase_discount_cents: purchase.discount_cents,
                purchase_currency: purchase.currency,
                purchase_paid_at: purchase.paid_at,
                purchase_salepoint_id: purchase.salepoint_id,
                purchase_no_purchase_album_id: purchase.no_purchase_album_id,
                purchase_related_id: purchase.related_id,
                purchase_related_type: purchase.related_type,
                purchase_targeted_product_id: purchase.targeted_product_id,
                purchase_price_adjustment_histories_count: purchase.price_adjustment_histories_count,
                purchase_vat_amount_cents: purchase.vat_amount_cents,
                purchase_status: purchase.status
            }
            actual_options = {
                person_id: log.person_id,
                current_method_name: log.current_method_name,
                truncated_message: log.message[0,49],
                invoice_id: log.invoice_id,
                purchase_id: log.purchase_id,
                invoice_created_at: log.invoice_created_at,
                invoice_final_settlement_amount_cents: log.invoice_final_settlement_amount_cents,
                invoice_currency: log.invoice_currency,
                invoice_batch_status: log.invoice_batch_status,
                invoice_settled_at: log.invoice_settled_at,
                invoice_vat_amount_cents: log.invoice_vat_amount_cents,
                invoice_foreign_exchange_pegged_rate_id: log.invoice_foreign_exchange_pegged_rate_id,
                purchase_created_at: log.purchase_created_at,
                purchase_product_id: log.purchase_product_id,
                purchase_cost_cents: log.purchase_cost_cents,
                purchase_discount_cents: log.purchase_discount_cents,
                purchase_currency: log.purchase_currency,
                purchase_paid_at: log.purchase_paid_at,
                purchase_salepoint_id: log.purchase_salepoint_id,
                purchase_no_purchase_album_id: log.purchase_no_purchase_album_id,
                purchase_related_id: log.purchase_related_id,
                purchase_related_type: log.purchase_related_type,
                purchase_targeted_product_id: log.purchase_targeted_product_id,
                purchase_price_adjustment_histories_count: log.purchase_price_adjustment_histories_count,
                purchase_vat_amount_cents: log.purchase_vat_amount_cents,
                purchase_status: log.purchase_status
            }

            expect(expected_options).to eq(actual_options)
          end
        end

        context 'successful braintree transaction' do
          before do
            allow_any_instance_of(ActiveMerchant::Billing::BraintreeOrangeGateway).to receive(:purchase).and_return(ActiveMerchant::Billing::Response.new(true, 'yay', {
              currency: 'USD',
              response_code: 100,
            }))
              manager.add_strategy(credit_strategy)
          end

          it 'should settle invoice' do
            expect { manager.process! }
              .to change { Invoice.count }.by(1)

            purchase.reload
            expect(purchase.paid?).to be true
            expect(purchase.invoice_id).to_not be_nil
            expect(manager.successful?).to be true
          end

          it 'should save the braintree transaction' do
            expect { manager.process! }
              .to change { BraintreeTransaction.count }.by(1)

            purchase.reload
            expect(purchase.paid?).to be true
            expect(purchase.invoice_id).to_not be_nil
            expect(manager.successful?).to be true
          end

          it 'should mark the purchase as processed' do
            expect { manager.process! }
              .to change { purchase.status }.from('unprocessed').to('processed')

            purchase.reload
            expect(purchase.paid?).to be true
            expect(purchase.invoice_id).to_not be_nil
            expect(manager.successful?).to be true
          end

          context 'failed to process purchase' do
            it 'should not settle invoice' do
              allow(Purchase).to receive(:process_purchases_for_invoice).and_raise('boom')

              expect { manager.process! }
                .to change { Invoice.count }.by(0)
                .and change { BraintreeTransaction.count }.by(1)
                .and change { purchase.status }.from('unprocessed').to('failed')

              purchase.reload
              expect(purchase.paid?).to be false
              expect(purchase.invoice_id).to be_nil
              expect(manager.successful?).to be false
            end
          end
        end
      end

      context "credit card processing using payments os" do
        let(:person) { create(:person, country_website_id: CountryWebsite::INDIA, country: 'IN') }
        let(:album) { create(:album, person: person) }
        let(:product) { Product.find(Product::IN_ONE_YEAR_RINGTONE_PRODUCT_ID) }
        let(:purchase) { create(:purchase, :unpaid, person: person, product: product, related: album) }
        let(:manager) { Cart::Payment::Manager.new(person, [purchase], '0.0.0.0') }
        let(:credit_strategy) { Cart::Payment::Strategies::PaymentsOSCard.new(0, 123) }

        it "should redirect for OTP for successful transaction" do
          allow(credit_strategy).to receive(:process).and_return(nil)
          allow(credit_strategy).to receive(:success?).and_return(true)
          allow(credit_strategy).to receive(:otp_redirect_url).and_return("https://redirect.com")
          manager.add_strategy(credit_strategy)

          manager.process!
          expect(manager.needs_otp_redirect?).to be_truthy
        end
      end
    end
  end
end
