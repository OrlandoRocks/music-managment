require "rails_helper"

describe Cart::Payment::Strategies::Balance do

  describe "#process" do
    before(:each) do
      @balance = Cart::Payment::Strategies::Balance.new

      @person = create(:person, :with_balance, amount: 5000.00)
      @person_balance = @person.person_balance
      album = create(:album, person: @person)
      @invoice = create(:invoice, :with_purchase, related: album, person: @person)
      @purchase = @invoice.purchases.first
      @manager = Cart::Payment::Manager.new(@person, [@purchase], "an ip address")
      allow(@manager).to receive(:invoice).and_return(@invoice)
    end
    context "invoice already settled" do
      before(:each) do
        allow(@invoice).to receive(:settled?).and_return(true)
      end
      it "should not attempt transaction" do
        expect(PersonTransaction).not_to receive(:create!)

        @balance.process(@manager)
      end
      it "should return nil" do
        expect(@balance.process(@manager)).to be(nil)
      end
      it "should create an invoice log" do
        initial_invoice_log_count = InvoiceLog.count

        @balance.process(@manager)

        log = InvoiceLog.all[-1]
        expected_log = {
            person_id: @person.id,
            invoice_id: @invoice.id,
            purchase_id: @purchase.id,
            message: "invoice already settled, balance: #{@person_balance.balance}"
        }
        actual_log = {
            person_id: log.person_id,
            invoice_id: log.invoice_id,
            purchase_id: log.purchase_id,
            message: log.message
        }

        expect(expected_log).to eq(actual_log)
        expect(initial_invoice_log_count + 1).to eq(InvoiceLog.count)
      end
    end

    context "balance is < .01" do
      before(:each) do
        @person_balance.balance = 0.001
      end
      it "should not attempt transaction" do
        expect(PersonTransaction).not_to receive(:create!)

        @balance.process(@manager)
      end
      it "should return nil" do
        expect(@balance.process(@manager)).to be(nil)
      end
      it "should create an invoice log" do
        initial_invoice_log_count = InvoiceLog.count

        @balance.process(@manager)

        log = InvoiceLog.all[-1]
        expected_log = {
            person_id: @person.id,
            invoice_id: @invoice.id,
            purchase_id: @purchase.id,
            message: "balance less than a penny, balance: #{@person_balance.balance}"
        }
        actual_log = {
            person_id: log.person_id,
            invoice_id: log.invoice_id,
            purchase_id: log.purchase_id,
            message: log.message
        }

        expect(expected_log).to eq(actual_log)
        expect(initial_invoice_log_count + 1).to eq(InvoiceLog.count)
      end
    end
    context "balance covered invoice" do
      before(:each) do
        allow(@invoice).to receive(:can_settle?).and_return(true)
      end
      it "should settle invoice" do
        expect(PersonTransaction).to receive(:create!)
        expect(@invoice).to receive(:settlement_received)
        expect(@invoice).to receive(:settled!)

        @balance.process(@manager)
      end
      it "should create an invoice log" do
        allow(@invoice).to receive(:settled!).and_return(true)
        initial_invoice_log_count = InvoiceLog.count

        @balance.process(@manager)

        log = InvoiceLog.all[-1]
        expected_log = {
            person_id: @person.id,
            invoice_id: @invoice.id,
            purchase_id: @purchase.id
        }
        actual_log = {
            person_id: log.person_id,
            invoice_id: log.invoice_id,
            purchase_id: log.purchase_id
        }

        expect(expected_log).to eq(actual_log)
        expect(initial_invoice_log_count + 1).to eq(InvoiceLog.count)
      end

      it 'should create outbound invoice' do
        allow_any_instance_of(Person).to receive(:vat_applicable?).and_return(true)
        allow_any_instance_of(TcVat::OutboundVatCalculator)
          .to receive(:fetch_vat_info)
          .and_return({ customer_location: 'Luxembourg',
                        vat_registration_number: 'LU22326250',
                        tax_rate: '17.0',
                        place_of_supply: 'Luxembourg' })
        expect(@invoice).to receive(:settled!)
        @balance.process(@manager)
        balance_settlement = @invoice.person_transaction_settlement

        expect(balance_settlement.vat_tax_adjustment).to be_present
        expect(balance_settlement.outbound_invoice).to be_present
      end
    end
  end
end
