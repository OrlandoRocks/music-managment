require "rails_helper"

describe Cart::Payment::Strategies::CreditCard do

  describe "#process" do
    before(:each) do
      @credit = Cart::Payment::Strategies::CreditCard.new(0)
      @person = create(:person)
      @purchase = create(:purchase, person: @person)
      @invoice = create(:invoice, person: @person)
      @card = create(:stored_credit_card, person: @person)
      @mock_txn = double(BraintreeTransaction)
      @bt_tx = create(:braintree_transaction, person: @person)
      @manager = Cart::Payment::Manager.new(@person, [@purchase], "ip address")
      allow(@manager).to receive(:invoice).and_return(@invoice)
    end
    context "sufficient conditions" do
      before(:each) do
        allow(@invoice).to receive(:settled?).and_return(false)
        allow(@invoice).to receive(:outstanding_amount_cents)

        allow(@mock_txn).to receive(:success?).and_return(true)
        allow(@mock_txn).to receive(:id).and_return(@bt_tx.id)

        allow(@person).to receive_message_chain(:stored_credit_cards, :find).and_return(@card)
      end
      it "should create braintree transaction" do
        expect(BraintreeTransaction).to receive(:process_purchase_with_stored_card).and_return(@mock_txn)

        @credit.process(@manager)
      end
      it "should create an invoice_log" do
        allow(BraintreeTransaction).to receive(:process_purchase_with_stored_card).and_return(@bt_tx)
        initial_invoice_log_count = InvoiceLog.count

        @credit.process(@manager)

        expect(initial_invoice_log_count + 1).to eq(InvoiceLog.count)
      end
    end
    context "invoice already settled" do
      before(:each) do
        allow(@invoice).to receive(:settled?).and_return(true)
      end
      it "should not attempt transaction if invoice already settled" do
        expect(BraintreeTransaction).not_to receive(:process_purchase_with_stored_card)

        @credit.process(@manager)
      end
      it "should create an invoice log" do
        allow(BraintreeTransaction).to receive(:process_purchase_with_stored_card).and_return(@bt_tx)
        initial_invoice_log_count = InvoiceLog.count

        @credit.process(@manager)

        expect(initial_invoice_log_count + 1).to eq(InvoiceLog.count)
      end
    end
    context "transaction failure" do
      before(:each) do
        allow(@invoie).to receive(:settled?).and_return(false)
        allow(@invoice).to receive(:outstanding_amount_cents)

        allow(@person).to receive_message_chain(:stored_credit_cards, :find).and_return(@card)
        allow(BraintreeTransaction).to receive(:process_purchase_with_stored_card).and_return(@bt_tx)
        allow(@bt_tx).to receive(:success?).and_return(false)
      end
      it "should set the notice message on manager if transaction failure" do
        expect(@manager).to receive(:notice=)

        @credit.process(@manager)
      end
      it "should create an invoice log" do
        allow(BraintreeTransaction).to receive(:process_purchase_with_stored_card).and_return(@bt_tx)
        initial_invoice_log_count = InvoiceLog.count

        @credit.process(@manager)

        expect(initial_invoice_log_count + 1).to eq(InvoiceLog.count)
      end
    end
  end

  describe "#rollback_handler" do
    before :each do
      @credit = Cart::Payment::Strategies::CreditCard.new(0)
      @stored_credit_card = create(:stored_credit_card)
      @person = create(:person)
      @txn = create(:braintree_transaction, :declined, person: @person, stored_credit_card: @stored_credit_card)
      allow(FeatureFlipper).to receive(:show_feature?).and_call_original
      allow(@credit).to receive(:txn).and_return(@txn)
    end
    it "should create transaction when present" do
      expect(@txn).to receive(:save)

      @credit.rollback_handler
    end
    it "should create a note if transaction has a fraud status" do
      @txn.update(response_code: "2013", status: "fraud")

      @credit.rollback_handler
      expect(@person.notes.first.subject).to eq("Possibly Fraudulent Purchase")
    end
    it "should create an invoice log" do
      allow(BraintreeTransaction).to receive(:process_purchase_with_stored_card).and_return(@bt_tx)
      initial_invoice_log_count = InvoiceLog.count

      @credit.rollback_handler
      expect(initial_invoice_log_count + 1).to eq(InvoiceLog.count)
    end

    it "should refund payment if transaction was successfull" do
      @txn.update(response_code: "1000", status: "success", amount: 0.51e1)
      refund_reason = "Rollback after error processing charge"
      allow(FeatureFlipper).to receive(:show_feature?).with(:autorefund_failed_braintree_transactions, @person).and_return(true)

      expect(BraintreeTransaction).to receive(:process_refund).with(@txn.id, @txn.amount, refund_reason: refund_reason)
      @credit.rollback_handler
    end

    it "should not refund payment if transaction was successfull & feature flipper is off" do
      @txn.update(response_code: "1000", status: "success", amount: 0.51e1)
      refund_reason = "Rollback after error processing charge"
      allow(FeatureFlipper).to receive(:show_feature?).with(:autorefund_failed_braintree_transactions, @person).and_return(false)

      expect(BraintreeTransaction).not_to receive(:process_refund).with(@txn.id, @txn.amount, refund_reason: refund_reason)
      @credit.rollback_handler
    end
  end

end
