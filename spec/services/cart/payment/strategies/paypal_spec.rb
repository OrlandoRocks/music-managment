require "rails_helper"

describe Cart::Payment::Strategies::CreditCard do

  describe "#process" do

    it "should not attempt transaction if invoice already settled" do
      @mock_person = double(Person)
      paypal = Cart::Payment::Strategies::Paypal.new(0, @mock_person)

      mock_invoice = double(Invoice)
      allow(mock_invoice).to receive(:settled?).and_return(true)

      mock_manager = double(Cart::Payment::Manager)
      allow(mock_manager).to receive(:invoice).and_return(mock_invoice)
      allow(mock_manager).to receive(:person)

      expect(PaypalTransaction).not_to receive(:process_reference_transaction)

      paypal.process(mock_manager)
    end

    it "should create paypal transaction" do
      @mock_person = double(Person)
      paypal = Cart::Payment::Strategies::Paypal.new(0, @mock_person)

      mock_invoice = double(Invoice)
      allow(mock_invoice).to receive(:settled?).and_return(false)

      mock_txn = double(PaypalTransaction)
      allow(mock_txn).to receive(:success?).and_return(true)

      mock_manager = double(Cart::Payment::Manager)
      allow(mock_manager).to receive(:invoice).and_return(mock_invoice)
      allow(mock_manager).to receive(:person)

      expect(PaypalTransaction).to receive(:process_reference_transaction).and_return(mock_txn)

      paypal.process(mock_manager)
    end

  end

  describe "#process, setting notice properly" do
    before(:each) do
      @mock_stored = double(StoredPaypalAccount)
      @mock_person = double(Person)
      @paypal = Cart::Payment::Strategies::Paypal.new(@mock_stored, @mock_person)

      @mock_invoice = double(Invoice)
      allow(@mock_invoice).to receive(:settled?).and_return(false)

      @mock_txn = double(PaypalTransaction)
      allow(@mock_txn).to receive(:success?).and_return(false)

      @mock_manager = double(Cart::Payment::Manager)
      allow(@mock_manager).to receive(:invoice).and_return(@mock_invoice)
      allow(@mock_manager).to receive(:person)

      expect(PaypalTransaction).to receive(:process_reference_transaction).and_return(@mock_txn)
    end

    it "should set the notice message on manager if transaction failure" do
      expect(@mock_txn).to receive(:error_code_message).and_return("testing")
      expect(@mock_manager).to receive(:notice=).with("testing")
      expect(@mock_manager).to receive(:notice).and_return("testing")

      @paypal.process(@mock_manager)
    end

    it "should set notice with default message if error code message nil" do
      expect(@mock_txn).to receive(:error_code_message).and_return(nil)
      expect(@mock_manager).to receive(:notice=).with(nil)
      expect(@mock_manager).to receive(:notice).and_return(nil)
      expect(@mock_manager).to receive(:notice=).with("An error occurred while processing your PayPal payment. Please contact customer support.")

      @paypal.process(@mock_manager)
    end
  end

  describe "#rollback_handler" do
    before(:each) do
      @mock_country_website = double(CountryWebsite, id: 1, paypal_checkout_credentials: PAYPAL_CREDS['USD'])
      @mock_person = double(Person, id: 1, country_website: @mock_country_website)
      @paypal_config = double(PayinProviderConfig)

      @mock_txn = double(
        PaypalTransaction,
        id: 1,
        person: @mock_person,
        transaction_id: "THEB52SLIKETHEBANDNOTTHEPLANE",
        raw_response: { test: "Test" }
      )
      @mock_paypal_response = double(
        PayPalSDKCallers::Transaction,
        success: true,
        response: "Success"
      )
      allow(@mock_txn).to receive(:person).and_return(@mock_person)
      allow(@mock_txn).to receive(:invoice)
      allow(@mock_txn).to receive(:dup)
      allow(@mock_txn).to receive(:save)
      @paypal = Cart::Payment::Strategies::Paypal.new(@mock_stored, @mock_person)

      allow(@paypal).to receive(:txn).and_return(@mock_txn)
    end

    it "should resave the txn and call the error handler" do
      allow(@mock_txn).to receive(:sale?).and_return(false)
      allow(@mock_txn).to receive(:completed?).and_return(false)
      expect(@paypal).to receive(:error_handler)

      @paypal.rollback_handler
    end

    it "should create an invoice log" do
      expect(@paypal).to receive(:error_handler)
      allow(@mock_txn).to receive(:sale?).and_return(false)
      allow(@mock_txn).to receive(:completed?).and_return(false)

      initial_invoice_log_count = InvoiceLog.count
      @paypal.rollback_handler
      expect(initial_invoice_log_count + 1).to eq(InvoiceLog.count)
    end


    it "should refund payment if transaction was successfull" do
      allow(@mock_txn).to receive(:sale?).and_return(true)
      allow(@mock_txn).to receive(:completed?).and_return(true)
      allow(@mock_txn).to receive(:payin_provider_config).and_return(@paypal_config)
      allow(FeatureFlipper).to receive(:show_feature?).with(:refund_failed_paypal_transactions, @mock_person).and_return(true)
      allow(PayPalAPI).to receive(:refund).with(@paypal_config, any_args).and_return(@mock_paypal_response)

      expect(PaypalTransaction).to receive(:record_refunded_transaction).with(@mock_paypal_response, @mock_txn)
      @paypal.rollback_handler
    end

    it "should not refund payment if transaction was successfull & feature flipper is off" do
      allow(@mock_txn).to receive(:sale?).and_return(true)
      allow(@mock_txn).to receive(:completed?).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:refund_failed_paypal_transactions, @mock_person).and_return(false)

      expect(PaypalTransaction).not_to receive(:record_refunded_transaction).with(@mock_paypal_response, @mock_txn )
      @paypal.rollback_handler
    end

  end

  describe "#error_handler" do
    before(:each) do
      @mock_person = double(Person)
      @mock_stored = double(StoredPaypalAccount)
      @paypal = Cart::Payment::Strategies::Paypal.new(@mock_stored, @mock_person)
      @mock_txn = double(PaypalTransaction)


      expect(@paypal).to receive(:txn).and_return(@mock_txn)
    end

    it "should destroy saved paypal account on 10201" do
      expect(@mock_txn).to receive(:raw_response).and_return("L_ERRORCODE0=10201")
      expect(@mock_stored).to receive(:destroy).with( @mock_person, { skip_validation: true } )
      expect(@paypal).to receive(:authorizer=).with(:paypal)

      @paypal.send(:error_handler)
    end

    it "should not destroy card for other errors" do
      expect(@mock_txn).to receive(:raw_response).and_return("L_ERRORCODE0=10401")
      expect(@mock_stored).not_to receive(:destroy)

      @paypal.send(:error_handler)
    end

  end

end
