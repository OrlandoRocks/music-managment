require "rails_helper"

RSpec.describe InvoiceSettlementFormatter::Factory, type: :service do
  describe ".for" do
    context "Adyen settlement" do
      it "should return formatter for Adyen" do
        source = AdyenTransaction.new
        expect(described_class.for(source)).to be_instance_of(InvoiceSettlementFormatter::Formatters::Adyen)
      end
    end

    context "Braintree settlement" do
      it "should return formatter for Braintree" do
        source = BraintreeTransaction.new
        expect(described_class.for(source)).to be_instance_of(InvoiceSettlementFormatter::Formatters::CreditCard)
      end
    end

    context "PaymentsOS settlement" do
      it "should return formatter for PaymentsOS" do
        source = PaymentsOSTransaction.new
        expect(described_class.for(source)).to be_instance_of(InvoiceSettlementFormatter::Formatters::CreditCard)
      end
    end

    context "PayPal settlement" do
      it "should return formatter for PayPal" do
        source = PaypalTransaction.new
        expect(described_class.for(source)).to be_instance_of(InvoiceSettlementFormatter::Formatters::Paypal)
      end
    end

    context "Payu settlement" do
      it "should return formatter for payu" do
        source = PayuTransaction.new
        expect(described_class.for(source)).to be_instance_of(InvoiceSettlementFormatter::Formatters::Payu)
      end
    end

    context "Balance settlement" do
      it "should return formatter for person balance" do
        source = PersonTransaction.new
        expect(described_class.for(source)).to be_instance_of(InvoiceSettlementFormatter::Formatters::Balance)
      end
    end

    context "Undefined Settlement" do
      it "should raise exception if settlement formatter is not defined for source" do
        source = double()
        expect { described_class.for(source) }
          .to raise_error(InvoiceSettlementFormatter::Factory::FormatterNotImplementedError)
      end
    end
  end
end
