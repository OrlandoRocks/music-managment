require "rails_helper"

RSpec.describe InvoiceSettlementFormatter::Formatters::Base, type: :service do
  describe "#invoice_description" do
    it "raises not implemented exception" do
      source = BraintreeTransaction.new

      expect do
        described_class.new(source).invoice_description
      end.to raise_error(NoMethodError)
    end
  end

  describe "#credit_note_invoice_description" do
    it "raises not implemented exception" do
      source = BraintreeTransaction.new

      expect do
        described_class.new(source).credit_note_invoice_description
      end.to raise_error(NoMethodError)
    end
  end
end
