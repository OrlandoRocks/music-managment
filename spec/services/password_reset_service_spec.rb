require "rails_helper"

describe PasswordResetService do
  before :each do
    allow(TransactionalEmail::ForgotPassword).to receive(:deliver).and_return(true)
  end

  describe ".reset" do
    it "generates a new invite code" do
      person      = create(:person)
      invite_code = person.invite_code
      PasswordResetService.reset(person.email)
      expect(person.reload.invite_code).not_to eq invite_code
    end

    it "unlocks person's account" do
      person = create(:person, account_locked_until: Date.today + 2.days)
      PasswordResetService.reset(person.email)
      expect(person.reload.account_locked?).to eq false
    end

    it "is true with valid email" do
      person = create(:person)
      expect(PasswordResetService.reset(person.email)).to eq true
    end

    it "it false with invalid email" do
      expect(PasswordResetService.reset("notauser@tunecore.com")).to eq false
    end
  end

  it "sends via sendgrid as default" do
    person = create(:person)
    allow_any_instance_of(PasswordResetMailer).to receive(:reset).and_return(true)
    expect_any_instance_of(PasswordResetMailer).to receive(:reset)
    service = PasswordResetService.new(person.email)
    service.reset
    expect(service.email_delivery_service).to eq :sendgrid
  end

  it "sends via hubspot service if hubspot is enabled" do
    person = create(:person)
    allow(TransactionalEmail::ForgotPassword).to receive(:deliver).and_return(true)
    expect(TransactionalEmail::ForgotPassword).to receive(:deliver)
    service = PasswordResetService.new(person.email, :hubspot)
    service.reset
    expect(service.email_delivery_service).to eq :hubspot
  end

  it "sets up the correct password reset url" do
    person = create(:person)
    service = PasswordResetService.new(person.email)
    expect(service.fallback_url.split("key=").pop).to eq("#{person.reload.invite_code}&person_id=#{person.id}")
  end

  it "sets up the correct password reset url from tunecore_social" do
    person = create(:person)
    service = PasswordResetService.new(person.email)
    service.reset(:social)
    expect(service.fallback_url.include?(ENV['TC_SOCIAL_REDIRECT_URL'])).to eq(true)
  end

  it "passes on initiator to mailer" do
    person = create(:person)
    service = PasswordResetService.new(person.email)

    expect_any_instance_of(PasswordResetMailer).to receive(:reset).with(person, url: service.fallback_url, initiator: :admin)

    service.reset(:admin)
  end
end
