# frozen_string_literal: true

require "rails_helper"

describe TaxReports::TaxFormMappingService, type: :service do

  before(:each) { TaxFormRevenueStream.destroy_all }

  let!(:person_with_w9_taxform) {
    create(
      :person, 
      :with_valid_w9_taxform
      )
  }

  let!(:person_with_multiple_w9_taxforms) {
    create(
      :person, 
      :with_multiple_valid_w9_taxforms
      )
  }

  describe "#call" do
    it "creates default mapping for users with taxforms" do
      described_class.call!(person_with_w9_taxform.id)
      tax_form_revenue_streams = TaxFormRevenueStream.where(tax_form: person_with_w9_taxform.tax_forms)
      expect(tax_form_revenue_streams.size).to be(2)
    end

    it "returns :cannot_override_usermap if default mapped and user mapped" do
      # user maps
      described_class.call!(person_with_w9_taxform.id, kind: :user)

      # Then default mapping is triggered  in error
      guard_clause_code = described_class.call!(person_with_w9_taxform.id, kind: :default)

      expect(guard_clause_code).to be(:cannot_override_usermap)
    end

    it "returns :no_such_person if person missing" do
      guard_clause_code = described_class.call!(0, kind: :default)

      expect(guard_clause_code).to be(:no_such_person)
    end

    it "returns :user_mapped_successfully if user_mapping successful" do
      guard_clause_code = described_class.call!(person_with_w9_taxform.id, kind: :user)

      expect(guard_clause_code).to be(:user_mapped_successfully)
    end

    it "returns :default_mapped_successfully if default_mapping successful" do
      guard_clause_code = described_class.call!(person_with_w9_taxform.id, kind: :default)

      expect(guard_clause_code).to be(:default_mapped_successfully)
    end

    it "returns :user_mapped_successfully if user_mapping successful" do
      described_class.call!(
        person_with_multiple_w9_taxforms.id, 
          kind: :user,
          mapping_definition: [
            {
              revenue_stream: RevenueStream.distribution,
              tax_form: person_with_multiple_w9_taxforms.tax_forms.first
            },
            {
              revenue_stream: RevenueStream.publishing,
              tax_form: person_with_multiple_w9_taxforms.tax_forms.last
            }
          ]
        )

      tax_form_revenue_streams = 
        TaxFormRevenueStream.where(tax_form: person_with_multiple_w9_taxforms.tax_forms)
                            .where.not(user_mapped_at: nil)
      expect(tax_form_revenue_streams.size).to be(2)
    end
  end
end