require "rails_helper"

describe "BulkAlbumsForUsers" do

  describe "#execute" do
    let(:person1) { FactoryBot.create(:person) }
    let(:person2) { FactoryBot.create(:person) }
    let(:album1) { FactoryBot.create(
              :album,
              :finalized,
              :with_uploaded_songs_and_artwork,
               number_of_songs: 2,
               person: person1
            ) }

    let(:album2) { FactoryBot.create(
              :album,
              :finalized,
              :with_uploaded_songs_and_artwork,
               number_of_songs: 2,
               person: person1
            ) }

    let(:album3) { FactoryBot.create(
            :album,
            :finalized,
            :with_uploaded_songs_and_artwork,
             number_of_songs: 2,
             person: person2
          ) }

      let(:album4) { FactoryBot.create(
            :album,
            :finalized,
            :with_uploaded_songs_and_artwork,
             number_of_songs: 2,
             person: person2
          ) }

    context "when given a list of properly spaced comma separated user IDs" do
      it "returns the albums belonging to the users with the given IDs" do
        user_ids = "#{person1.id}, #{person2.id}"
        expect(BulkAlbumsForUsers.execute(user_ids)).to eq([album1, album2, album3, album4])
      end
    end

    context "when given a list of improperly spaced comma separated user IDs" do
      it "returns the albums belonging to the users with the given IDs" do
        user_ids = "#{person1.id},#{person2.id}"
        expect(BulkAlbumsForUsers.execute(user_ids)).to eq([album1, album2, album3, album4])
      end
    end

    context "when given a list of not comma separated user IDs" do
      it "returns the albums belonging to the users with the given IDs" do
        user_ids = "#{person1.id} #{person2.id}"
        expect(BulkAlbumsForUsers.execute(user_ids)).to eq([album1, album2, album3, album4])
      end
    end
  end
end
