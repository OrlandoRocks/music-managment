require "rails_helper"

RSpec.describe V2::Authentication::SharedSignInSuccessCallback, ".call" do
  it "resets plans redirect" do
    person = create(:person)
    request = build_request
    cookies = build_cookies(request)
    params = build_params(person)

    described_class.call(person, request, cookies, params)

    expect(cookies[:plans_redirect]).to be(true)
  end

  it "creates a logged in event" do
    person = create(:person)
    request = build_request
    cookies = build_cookies(request)
    params = build_params(person)
    ip_address = "1.2.3.4"
    request.remote_ip = ip_address
    user_agent = "Firefox?"
    request.user_agent = user_agent

    expect {
      described_class.call(person, request, cookies, params)
    }.to change { person.login_events.count }.by(1)

    login_event = person.login_events.last
    expect(login_event.ip_address).to eql(ip_address)
    expect(login_event.user_agent).to eql(request.user_agent)
  end

  it "updates the recent_login on the person" do
    person = create(:person)
    request = build_request
    cookies = build_cookies(request)
    params = build_params(person)
    remote_ip = "1.2.3.4"
    request.remote_ip = remote_ip

    freeze_time do
      described_class.call(person, request, cookies, params)

      person.reload
      expect(person.recent_login.to_i).to eql(Time.zone.now.to_i)
      expect(person.dormant).to be(false)
      expect(person.last_logged_in_ip).to eql(remote_ip)
    end
  end

  it "adds an SSO cookie" do
    person = create(:person)
    request = build_request
    cookies = build_cookies(request)
    params = build_params(person)

    described_class.call(person, request, cookies, params)

    expect(cookies[:tc_pid]).to be_present
    expect(cookies[:tc_pid]).to start_with(person.id.to_s)
  end

  it "creates referral data" do
    person = create(:person)
    request = build_request
    cookies = build_cookies(request)
    params = build_params(person)
    time = Time.now
    params[:person] = {
      referral_data_attributes: {
        "0" => {
          key: "key",
          value: "value",
          page: "page",
          source: "source",
          timestamp: time.iso8601
        }
      }
    }

    expect {
      described_class.call(person, request, cookies, params)
    }.to change { person.referral_data.count }.by(1)

    referral_data = person.referral_data.last
    expect(referral_data.key).to eql("key")
    expect(referral_data.value).to eql("value")
    expect(referral_data.page).to eql("page")
    expect(referral_data.source).to eql("source")
    expect(referral_data.timestamp.to_i).to eql(time.to_i)
  end

  it "creates an active session cookie" do
    person = create(:person)
    request = build_request
    cookies = build_cookies(request)
    params = build_params(person)

    described_class.call(person, request, cookies, params)

    expect(cookies[:tc_active_session]).to be_present
  end

  it "clears referral cookies" do
    person = create(:person)
    request = build_request
    cookies = build_cookies(request, {
      "tunecore_wp" => {
        value: 'value',
        expires: 1.year,
        domain: 'example.com'
      }
    })
    params = build_params(person)

    described_class.call(person, request, cookies, params)

    expect(cookies["tunecore_wp"]).to be_blank
  end

  it "retrieves publishing data" do
    person = create(:person)
    account = create(:account, person: person)
    request = build_request
    cookies = build_cookies(request)
    params = build_params(person)
    allow(described_class::RetrievePublishingData)
      .to receive(:call)

    described_class.call(person, request, cookies, params)

    expect(described_class::RetrievePublishingData)
      .to have_received(:call)
  end

  it "reconciles tier eligibility" do
    person = create(:person)
    request = build_request
    cookies = build_cookies(request)
    params = build_params(person)
    allow(RewardSystem::TierEligibilityReconcileWorker)
      .to receive(:perform_async)

    described_class.call(person, request, cookies, params)

    expect(RewardSystem::TierEligibilityReconcileWorker)
      .to have_received(:perform_async).with(person.id)
  end

  def build_env
    Rack::MockRequest.env_for("http://example.com")
  end

  def build_request(env = build_env)
    ActionController::TestRequest.new(
      env,
      ActionController::TestSession.new,
      ApplicationController
    )
  end

  def build_params(person)
    ActionController::Parameters.new({
      email: person.email,
      password: person.password
    })
  end

  def build_cookies(request, cookies = {})
    ActionDispatch::Cookies::CookieJar.build(request, cookies)
  end
end
