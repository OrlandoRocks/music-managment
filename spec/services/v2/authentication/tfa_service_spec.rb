require "rails_helper"

describe V2::Authentication::TFAService do
  include V2::Authentication::Helpers

  let!(:person)            { create(:person, :with_two_factor_auth) }
  let!(:tfa_session_token) { create_tfa_session_token }
  let!(:env)               { Rack::MockRequest.env_for("http://example.com") }
  let!(:request) do
    ActionController::TestRequest.new(
      env,
      ActionController::TestSession.new,
      ApplicationController
    )
  end
  let!(:params) { { tfa_session_token: tfa_session_token } }

  describe "#checked_person," do
    context "when auth check succeeds," do
      it "returns person" do
        result = described_class.new(params, request, person)
        allow(result).to receive_message_chain(:auth_code_check, :successful?)
          .and_return true

        expect(result.checked_person[:id]).to eq(person.id)
      end
    end

    context "when auth check fails," do
      it "returns nil" do
        result = described_class.new(params, request, person)
        allow(result).to receive_message_chain(:auth_code_check, :successful?)
          .and_return false

        expect(result.checked_person).to be_nil
      end
    end
  end

  describe "#new_auth_code_required?," do
    before do
      request.session[:admin] = person.id
    end

    context "when params has an auth code" do
      it "returns false" do
        params[:auth_code] = :foo
        result = described_class.new(params, request, person).new_auth_code_required?

        expect(result).to be false
      end
    end

    context "when under_admin_control" do
      it "returns false" do
        request.session[:admin] = 'some other id'
        result = described_class.new(params, request, person).new_auth_code_required?

        expect(result).to be false
      end
    end

    context "when tfa disabled" do
      it "returns false" do
        allow(ENV).to receive(:[]).with("TWO_FACTOR_AUTHENTICATION_ENABLED").and_return 'false'
        result = described_class.new(params, request, person).new_auth_code_required?

        expect(result).to be false
      end
    end

    context "when not tfa user" do
      before { person.two_factor_auth.destroy }

      it "returns false" do
        person.reload
        result = described_class.new(params, request, person).new_auth_code_required?

        expect(result).to be false
      end
    end

    context "when tfa NOT expired" do
      it "returns false" do
        allow_any_instance_of(TwoFactorAuth).to receive(:within_authentication_window?)
          .and_return true
        result = described_class.new(params, request, person).new_auth_code_required?

        expect(result).to be false
      end
    end

    context "when tfa is expired" do
      it "returns true" do
        result = described_class.new(params, request, person).new_auth_code_required?

        expect(result).to be true
      end
    end
  end
end
