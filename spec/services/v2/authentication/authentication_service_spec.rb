require "rails_helper"

describe V2::Authentication::AuthenticationService do
  include V2::Authentication::Helpers

  subject { described_class.call(params: params, request: request, cookies: cookies) }

  let!(:person) { create(:person) }
  let!(:params) { { email: "foo", password: "bar" } }
  let!(:env)    { Rack::MockRequest.env_for("http://example.com") }
  let!(:request) do
    ActionController::TestRequest.new(
      env,
      ActionController::TestSession.new,
      ApplicationController
    )
  end
  let!(:cookies) { ActionDispatch::Cookies::CookieJar.build(request, {"baz": "arf"}) }

  before(:each) do
    allow(FeatureFlipper).to receive(:show_feature?).with(:verify_2fa, person).and_return(true)
    allow_any_instance_of(TwoFactorAuth::ApiClient).to receive(:log).and_return(true)
  end

  def add_tfa_session_token_to_params(person, params)
    params[:tfa_session_token] = create_tfa_session_token

    nil
  end

  context "with valid credentials," do
    context "when 2FA NOT required" do
      it "saves a new token and nonce and returns them" do
        allow(Person).to receive(:authenticate).with(params).and_return(person)
        allow_any_instance_of(Person).to receive(:is_verified?).and_return true

        result = subject

        jwt = result[:jwt]
        digested = digest(jwt)

        nonce = result[:nonce]

        expect(token_user(digested)).to eq person.id
        expect(nonce_user(nonce)).to eq person.id
      end
    end

    context "when 2FA required" do
      before do
        create(:two_factor_auth, person: person)
      end

      it "sends 2FA code and returns 2FA session token" do
        allow(Person).to receive(:authenticate).with(params).and_return(person)
        allow_any_instance_of(Person).to receive(:is_verified?).and_return true
        allow_any_instance_of(V2::Authentication::TFAService)
          .to receive(:new_auth_code_required?).and_return true

        expect(TwoFactorAuthEvent).to receive(:create)
          .with(hash_including(action: 'send', two_factor_auth_id: person.two_factor_auth.id))

        result = subject

        expect(result[:tfa_session_token]).to be_truthy
        expect(result[:tfa_required]).to eq true
      end
    end

    context "when valid 2FA auth code provided" do
      before do
        create(:two_factor_auth, person: person)

        params[:auth_code] = 'valid code'
        add_tfa_session_token_to_params(person, params)
      end

      it "saves a new token and nonce and returns them" do
        allow(Person).to receive(:authenticate).with(params).and_return(person)
        allow_any_instance_of(Person).to receive(:is_verified?).and_return true

        auth_result = TwoFactorAuth::ApiClient::AuthCodeSubmission.new(true)
        allow_any_instance_of(TwoFactorAuth::ApiClient)
          .to receive(:submit_auth_code).and_return(auth_result)
        expect_any_instance_of(V2::Authentication::TFAService)
          .to receive(:log_auth_check_result).with(person, auth_result)

        result = subject

        jwt = result[:jwt]
        digested = digest(jwt)

        nonce = result[:nonce]

        expect(token_user(digested)).to eq person.id
        expect(nonce_user(nonce)).to eq person.id
      end
    end

    context "when INVALID 2FA auth code provided" do
      before do
        create(:two_factor_auth, person: person)

        params[:auth_code] = 'valid code'
        add_tfa_session_token_to_params(person, params)
      end

      it "returns an error" do
        allow(Person).to receive(:authenticate).with(params).and_return(person)
        allow_any_instance_of(Person).to receive(:is_verified?).and_return true

        auth_result = TwoFactorAuth::ApiClient::AuthCodeSubmission.new(false)
        allow_any_instance_of(TwoFactorAuth::ApiClient)
          .to receive(:submit_auth_code).and_return(auth_result)
        expect_any_instance_of(V2::Authentication::TFAService)
          .to receive(:log_auth_check_result).with(person, auth_result)

        expect(subject).to eq({
          errors: "tfa error",
          jwt: nil,
          nonce: nil,
          tfa_required: false,
          tfa_session_token: nil
        })
      end
    end

    context "when missing tfa_session_token" do
      before do
        create(:two_factor_auth, person: person)

        params[:auth_code] = 'valid code'
        payload = { person_id: person.id }
      end

      it "returns an error" do
        allow(Person).to receive(:authenticate).with(params).and_return(person)
        allow_any_instance_of(Person).to receive(:is_verified?).and_return true
        allow_any_instance_of(TwoFactorAuth::ApiClient)
          .to receive_message_chain(:submit_auth_code, :successful?).and_return(false)

        expect(subject).to eq({
          errors: "tfa error",
          jwt: nil,
          nonce: nil,
          tfa_required: false,
          tfa_session_token: nil
        })
      end
    end

    it "calls the shared login success callback" do
      allow(Person).to receive(:authenticate).with(params).and_return(person)
      allow_any_instance_of(Person).to receive(:is_verified?).and_return true
      allow(V2::Authentication::SharedSignInSuccessCallback).to receive(:call)

      subject

      expect(V2::Authentication::SharedSignInSuccessCallback).to have_received(:call)
    end
  end

  context "with invalid credentials," do
    it "returns error" do
      allow(Person).to receive(:authenticate).with(params).and_return(nil)

      expect(subject).to eq({
        errors: "tfa error",
        jwt: nil,
        nonce: nil,
        tfa_required: false,
        tfa_session_token: nil
      })
    end
  end
end
