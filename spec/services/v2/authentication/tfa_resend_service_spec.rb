require "rails_helper"

describe V2::Authentication::TfaResendService do
  include V2::Authentication::Helpers

  let!(:person) { create(:person) }

  before(:each) do
    allow(FeatureFlipper).to receive(:show_feature?).with(:verify_2fa, person).and_return(true)
    allow_any_instance_of(TwoFactorAuth::ApiClient).to receive(:log).and_return(true)
  end

  before { create(:two_factor_auth, person: person) }

  context "with valid tfa_session_token," do
    let!(:params) { { tfa_session_token: create_tfa_session_token } }

    it "sends new auth code and returns new session token" do
      allow_any_instance_of(Person).to receive(:is_verified?).and_return true
      expect(TwoFactorAuthEvent).to receive(:create)
        .with(hash_including(action: 'resend', two_factor_auth_id: person.two_factor_auth.id))

      result = described_class.call(params: params)

      expect(result[:tfa_session_token]).to be_truthy
    end
  end

  context "with INVALID tfa_session_token," do
    let!(:params) { { tfa_session_token: "foo" } }

    it "returns nil" do
      allow_any_instance_of(Person).to receive(:is_verified?).and_return true

      result = described_class.call(params: params)

      expect(result).to be_nil
    end
  end
end
