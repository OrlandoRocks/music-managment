require "rails_helper"

RSpec.describe V2::Authentication::SharedSignInSuccessCallback::RetrievePublishingData, ".call" do
  it "retrieves publishing data" do
    person = create(:person)
    account = create(:account, person: person)
    request = build_request
    params = build_params
    allow(PublishingAdministration::PublishingIngestionBlockerWorker)
      .to receive(:perform_async)

    described_class.call(person, request, params)

    expect(PublishingAdministration::PublishingIngestionBlockerWorker)
      .to have_received(:perform_async).with(account.id)
  end

  it "does nothing if the person doesn't have an account" do
    person = create(:person)
    params = build_params
    request = build_request
    allow(PublishingAdministration::PublishingIngestionBlockerWorker)
      .to receive(:perform_async)

    described_class.call(person, request, params)

    expect(PublishingAdministration::PublishingIngestionBlockerWorker)
      .to_not have_received(:perform_async)
  end

  it "does nothing if the rights_app feature is not enabled" do
    person = create(:person)
    account = create(:account, person: person)
    request = build_request
    params = build_params
    allow(PublishingAdministration::PublishingIngestionBlockerWorker)
      .to receive(:perform_async)
    allow_any_instance_of(CountryWebsite)
      .to receive(:feature_enabled?).with(:rights_app).and_return(false)

    described_class.call(person, request, params)

    expect(PublishingAdministration::PublishingIngestionBlockerWorker)
      .to_not have_received(:perform_async)
  end

  def build_request
    ActionController::TestRequest.new(
      Rack::MockRequest.env_for("http://example.com"),
      ActionController::TestSession.new,
      ApplicationController
    )
  end

  def build_params(params = {})
    ActionController::Parameters.new(params)
  end
end
