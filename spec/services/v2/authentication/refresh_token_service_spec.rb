require "rails_helper"

describe V2::Authentication::RefreshTokenService do
  include V2::JwtHelper

  subject { described_class.refresh(request) }

  let!(:id) { 123 }
  let!(:payload) do
    {
      id: id,
      email: "foo",
      name: "bar",
      roles: ["baz"]
    }
  end

  after do
    delete_digest(digest(token))
  end

  context "with valid header token," do
    let!(:token) { generate_jwt(payload: payload) }
    let!(:request) { double('request', env: { 'HTTP_AUTHORIZATION' => token } )}

    it "saves a new token and returns it" do
      result     = subject

      jwt        = result[:jwt]
      nonce      = result[:nonce]
      jwt_user   = token_user(digest(jwt))
      nonce_user = nonce_user(nonce)

      expect(jwt_user).to        eq id
      expect(nonce_user.to_i).to eq id
    end
  end

  context "with invalid header token," do
    let!(:token) { "qux" }
    let!(:request) { double('request', env: { 'HTTP_AUTHORIZATION' => token } )}
    let!(:old_token_hash) { "foo" }

    it "does nothing" do
      result = subject

      expect(result).to be_nil
    end
  end
end
