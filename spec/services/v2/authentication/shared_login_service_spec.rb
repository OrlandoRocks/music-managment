describe V2::Authentication::SharedLoginService do
  subject { described_class.new(request) }

  context "success" do
    let!(:cookies) { { v2_login: "true" } }
    let!(:id)      { 123 }
    let!(:env)     { Rack::MockRequest.env_for("http://web.tunecore.com/?id=#{id}&nonce=nonce") }
    let!(:request) do
      ActionController::TestRequest.new(
        env,
        ActionController::TestSession.new,
        ApplicationController
      )
    end

    before do
      allow(FeatureFlipper).to receive(:show_feature?).with(:v2_login).and_return true
      allow_any_instance_of(V2::JwtHelper).to receive(:permitted_nonce?).and_return true
      request.cookies["v2_login"] = "true"
      stub_const("COOKIE_DOMAIN", ".tunecore.com")
    end

    context "no referer" do
      it "does NOT set person_id" do
        expect(subject.person_id).to be nil
      end
    end

    context "good referer" do
      before do
        request.env["HTTP_REFERER"] = "http://development.tunecore.local"
      end

      it "sets person_id" do
        expect(subject.person_id).to eq id
      end
    end
  end

  context "failure" do
    let!(:cookies) { { v2_login: "true" } }
    let!(:id)      { 123 }
    let!(:env)     { Rack::MockRequest.env_for("http://foo.bar/?id=#{id}&nonce=nonce") }
    let!(:request) do
      ActionController::TestRequest.new(
        env,
        ActionController::TestSession.new,
        ApplicationController
      )
    end

    before do
      request.cookies["v2_login"] = "true"
      allow(FeatureFlipper).to receive(:show_feature?).with(:v2_login).and_return true
      allow_any_instance_of(V2::JwtHelper).to receive(:permitted_nonce?).and_return true
    end

    context "no v2 cookie" do
      before do
        request.cookies["v2_login"] = "false"
      end

      it "does NOT set person_id" do
        expect(subject.person_id).to be_nil
      end
    end

    context "no nonce" do
      before do
        request.params[:nonce] = nil
      end

      it "does NOT set person_id" do
        expect(subject.person_id).to be_nil
      end
    end

    context "no id" do
      before do
        request.params[:id] = nil
      end

      it "does NOT set person_id" do
        expect(subject.person_id).to be_nil
      end
    end

    context "nonce not permitted" do
      before do
        allow_any_instance_of(V2::JwtHelper).to receive(:permitted_nonce?).and_return false
      end

      it "does NOT set person_id" do
        expect(subject.person_id).to be_nil
      end
    end

    context "feature flag inactive" do
      before do
        allow(FeatureFlipper).to receive(:show_feature?).with(:v2_login).and_return false
      end

      it "does NOT set person_id" do
        expect(subject.person_id).to be_nil
      end
    end

    context "bad referer" do
      before do
        request.env["HTTP_REFERER"]= "http://tunecore.foo"
      end

      it "does NOT set person_id" do
        expect(subject.person_id).to be_nil
      end
    end
  end
end
