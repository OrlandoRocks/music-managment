require "rails_helper"

describe WithdrawalMismatchReportService do
  describe "#generate_report" do
    context "when no mismatched transactions exist" do
      before(:each) do
        PayoutTransfer.delete_all
      end

      it "returns :no_mismatched_transactions_found" do
        person              = create(:person)
        transaction         = create(
          :payout_transfer, person: person, amount_cents: 40_000,
                            tunecore_processed_at: DateTime.current.ago(1.hour)
        )
        transaction_api_log = create(:payout_transfer_api_log, :response, payout_transfer: transaction)
        expect(WithdrawalMismatchReportService.new.generate_report).to eq(:no_mismatched_transactions_found)
      end

      it "returns CSV of mismatched_transactions if mismatched_transactions exist" do
        person              = create(:person)
        transaction         = create(
          :payout_transfer, person: person, amount_cents: 100,
                            tunecore_processed_at: DateTime.current.ago(1.hour),
                            provider_status: "requested"
        )
        transaction_api_log = create(:payout_transfer_api_log, :response, payout_transfer: transaction)
        csv                 = CSV.parse(WithdrawalMismatchReportService.new.generate_report, headers: true)
        csv_transaction     = csv.entries.find { |withdrawal| withdrawal["transaction_id"] == String(transaction.id) }
        expect(Float(csv_transaction["discrepency_amount"])).to be > 0
        expect(csv.entries.size).to eq(1)
      end

      it "does not process transactions that are errored" do
        person              = create(:person)
        transaction         = create(
          :payout_transfer,
          person: person,
          provider_status: "errored",
          amount_cents: 400,
          tunecore_processed_at: DateTime.current.ago(1.hour)
        )

        transaction_api_log = create(:payout_transfer_api_log, :response, payout_transfer: transaction)
        expect(WithdrawalMismatchReportService.new.generate_report).to eq(:no_mismatched_transactions_found)
      end
    end
  end
end
