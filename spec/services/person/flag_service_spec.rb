# frozen_string_literal: true
require "rails_helper"

describe Person::FlagService do
  let!(:person) { create(:person) }
  let!(:options) do
    {
      person: person,
      flag_attributes: Flag::ADDRESS_LOCKED,
      flag_reason: FlagReason.payoneer_kyc_completed_address_lock
    }
  end
  describe "#flag!" do
    context "given person and flag" do
      it "unflags the person" do
        expect(person.flags.first).to be(nil)
        Person::FlagService.flag!(options)
        person.reload

        expect(person.flags.first).to eq(Flag.find_by(Flag::ADDRESS_LOCKED))
      end
    end
    context "no person key" do
      it "raises error" do
        expect{Person::FlagService.flag!({flag_attributes: Flag::ADDRESS_LOCKED})}.to raise_error
      end
    end
    context "nil value on person key" do
      it "raises error" do
        expect{Person::FlagService.flag!({person: nil, flag_attributes: Flag::ADDRESS_LOCKED})}.to raise_error
      end
    end
    context "not given flag" do
      it "raises error" do
        expect{Person::FlagService.flag!({person: person})}.to raise_error
      end
    end
    context "flag provided invalid" do
      it "adds error to person" do
        Person::FlagService.flag!({person: person, flag_attributes: nil})

        expect(person.reload.errors.present?).to be(true)
      end
    end
  end
  describe "#unflag!" do
    context "given person and flag" do
      it "unflags the person" do
        Person::FlagService.flag!(options)
        person.reload
        expect(person.flags.first).to eq(Flag.find_by(Flag::ADDRESS_LOCKED))

        Person::FlagService.unflag!(options)
        person.reload
        expect(person.flags.first).to be(nil)
      end
    end
    context "no person key" do
      it "raises error" do
        expect{Person::FlagService.unflag!({flag_attributes: Flag::ADDRESS_LOCKED})}.to raise_error
      end
    end
    context "nil value on person key" do
      it "raises error" do
        expect{Person::FlagService.unflag!({person: nil, flag_attributes: Flag::ADDRESS_LOCKED})}.to raise_error
      end
    end
    context "not given flag" do
      it "raises error" do
        expect{Person::FlagService.unflag!({person: person})}.to raise_error
      end
    end
    context "flag provided invalid" do
      it "adds error to person" do
        Person::FlagService.unflag!({person: person, flag_attributes: nil})

        expect(person.reload.errors.present?).to be(true)
      end
    end
  end
  describe "#person_flag_exists?" do
    context "given person and flag" do
      context "person has the flag provided" do
        it "returns true" do
          Person::FlagService.flag!(options)
          expect(Person::FlagService.person_flag_exists?(options)).to be(true)
        end
      end
      context "person does not have the flag provided" do
        it "returns false" do
          expect(Person::FlagService.person_flag_exists?(options)).to be(false)
        end
      end
    end
    context "no person key" do
      it "raises error" do
        expect{Person::FlagService.person_flag_exists?({flag_attributes: Flag::ADDRESS_LOCKED})}.to raise_error
      end
    end
    context "nil value on person key" do
      it "raises error" do
        expect{Person::FlagService.person_flag_exists?({person: nil, flag_attributes: Flag::ADDRESS_LOCKED})}.to raise_error
      end
    end
    context "not given flag" do
      it "raises error" do
        expect{Person::FlagService.person_flag_exists?({person: person})}.to raise_error
      end
    end
    context "flag provided invalid" do
      it "adds error to person" do
        Person::FlagService.person_flag_exists?({person: person, flag_attributes: nil})

        expect(person.reload.errors.present?).to be(true)
      end
    end
  end
end
