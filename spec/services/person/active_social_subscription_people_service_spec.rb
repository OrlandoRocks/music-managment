require "rails_helper"

describe Person::ActiveSocialSubscriptionPeopleService do
  describe ".call" do
    # We send the same email as auto-renewals, so we check for the same flag.
    let(:flag) { Flag.find_by(Flag::AUTO_RENEWAL_EMAIL_SENT) }
    let!(:tc_social_people) do
      people = []
      [Time.now, 6.months.ago, 12.months.ago, 24.months.ago, 36.months.ago].each do |date|
        people << create(:person, :with_active_social_subscription_status, :with_login_events, :with_live_albums, login_created_at: date, country: "US")
        people << create(:person, :with_active_social_subscription_status, :with_login_events, :with_live_albums, login_created_at: date, country: "UK")
        people << create(:person, :with_login_events, :with_live_albums, login_created_at: date, country: "US")
        people << create(:person, :with_login_events, :with_live_albums, login_created_at: date, country: "UK")
      end
    end
    let!(:person_with_sent_email) do
      create(:person, :with_autorenewal_and_current_cc, :with_login_events, :with_live_album, country: "US") do |person|
        person.flags << flag
      end
    end
    let(:tc_us_corporate_entity) { CorporateEntity.find_by(name: CorporateEntity::TUNECORE_US_NAME) }
    let(:bi_corporate_entity) { CorporateEntity.find_by(name: CorporateEntity::BI_LUXEMBOURG_NAME) }

    it "selects only people who are non-US (corporate entity not 'TuneCore US')" do
      people = described_class.new.call

      expect(people.length).not_to eq(0)
      expect(people.select { |person| person.corporate_entity_id = bi_corporate_entity.id }).to match_array(people)
    end

    it "selects only people who have auto-renewal set up and have an active CC or PayPal or partial balance" do
      people = described_class.new.call

      expect(people.length).not_to eq(0)
      expect(people.select(&:can_autorenew?)).to match_array(people)
    end

    it "selects only people who have not logged into the site in the last 6 months up to 12 months" do
      people = described_class.new(last_logged_in_before: 6.months.ago, last_logged_in_after: 12.months.ago).call

      expect(people.length).not_to eq(0)
      expect(people.select do |person|
        person.last_logged_in_at <= 6.months.ago &&
        person.last_logged_in_at > 12.months.ago
      end).to match_array(people)
    end

    it "selects only people who have not logged into the site in the last 24 months" do
      people = described_class.new(last_logged_in_before: 24.months.ago).call

      expect(people.length).not_to eq(0)
      expect(people.select do |person|
        person.last_logged_in_at <= 24.months.ago
      end).to match_array(people)
    end

    it "selects up to a limit number of records" do
      people = described_class.new(limit: 3).call

      expect(people.length).not_to eq(0)
      expect(people.length).to eq(3)
    end

    # We send the same email as auto-renewals, so we must check if that
    # e-mail has already been sent to this person.
    it "selects only people who have not already received an auto-renewal e-mail" do
      people = described_class.new.call

      expect(people).not_to include(person_with_sent_email)
    end
  end
end
