require "rails_helper"

describe Person::EmailChangeService do
  let!(:person) { create(:person) }
  let!(:params) do {
      person: person.password,
      original_password: "Testpass123!",
      email: "new_email@test.com",
      email_confirmation: "new_email@test.com"
  }
  end

  describe "#call" do
    context "given a correct person and params" do
      it "calls EmailChangeWorker " do
        expect(EmailChangeWorker).to receive(:perform_async).with(person.id, params[:email])
        Person::EmailChangeService.call(person, params)
      end
      it "returns email_reset_success_modal.js" do
        email_change_modal = Person::EmailChangeService.call(person, params)
        expect(email_change_modal).to eq("email_reset_success_modal.js")
      end
    end

    context "given email already in use" do
      before(:each) do
        create(:person, email: params[:email])
      end

      it "returns email_reset_failure_modal.js" do
        email_change_modal = Person::EmailChangeService.call(person, params)
        expect(email_change_modal).to eq("email_reset_failure_modal.js")
      end
    end

    context "given wrong password" do
      before(:each) do
        params[:original_password] = "fakepassword!"
      end

      it "returns nil" do
        email_change_modal = Person::EmailChangeService.call(person, params)
        expect(email_change_modal).to eq(nil)
      end
    end

    context "given incorrect email but correct email confirmation" do
      before(:each) do
        params[:email] = "fakeemail@test.com"
      end
      it "returns email_reset_failure_modal.js" do
        email_change_modal = Person::EmailChangeService.call(person, params)
        expect(email_change_modal).to eq("email_reset_failure_modal.js")
      end
    end

    context "given incorrect email confirmation" do
      before(:each) do
        params[:email_confirmation] = "fakeemail@test.com"
      end
      it "returns email_reset_failure_modal.js" do
        email_change_modal = Person::EmailChangeService.call(person, params)
        expect(email_change_modal).to eq("email_reset_failure_modal.js")
      end
    end

    context "given no email param" do
      before(:each) do
        params.delete(:email)
      end
      it "returns email_reset_failure_modal.js" do
        email_change_modal = Person::EmailChangeService.call(person, params)
        expect(email_change_modal).to eq(nil)
      end
    end
  end
end
