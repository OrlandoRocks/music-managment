# frozen_string_literal: true
require "rails_helper"

describe Person::CountryBackfillService do
  let!(:country_backfill_service) { Person::CountryBackfillService.new }
  let!(:invalid_default) { create(:person, :with_invalid_country) }

  describe "backfilling invalid users" do
    context "#backfill_via_two_factor_auth" do
      let!(:invalid_with_tfa_france)  { create(:person, :with_invalid_country, :with_two_factor_auth_france) }
      let!(:invalid_with_tfa_us)  { create(:person, :with_invalid_country, :with_two_factor_auth) }
      let!(:backfill_using_two_factor) do
        country_backfill_service.invalid_country_people = [invalid_with_tfa_france, invalid_with_tfa_us, invalid_default]
        country_backfill_service.backfill_via_two_factor_auth
      end

      it "backfills invalid users using their tfa country" do
        expect(invalid_with_tfa_france.country).to eq("France")
        expect(invalid_with_tfa_france[:country]).to eq("FR")
      end

      it "does not backfill users having ambiguous phone code country" do
        expect(invalid_with_tfa_us.country).to eq("Unidentified")
      end

      it "does not backfill users having no tfa country" do
        expect(invalid_default.country).to eq("Unidentified")
      end
    end

    context "#backfill_via_ip" do
      let!(:invalid_with_ip)  { create(:person, :with_invalid_country, :with_login_events) }
      let!(:backfill_using_ip) do
        country_backfill_service.invalid_country_people = [invalid_with_ip, invalid_default]
        country_backfill_service.backfill_via_ip
      end

      it "backfills invalid users using their ip history" do
        expect(invalid_with_ip.country).to eq("United States")
        expect(invalid_with_ip[:country]).to eq("US")
      end

      it "does not backfill users having no ip country" do
        expect(invalid_default.country).to eq("Unidentified")
      end
    end

    context "#backfill_via_credit_card" do
      let!(:invalid_with_stored_cc)  { create(:person, :with_invalid_country, :with_stored_credit_card) }
      let!(:backfill_using_cc) do
        country_backfill_service.invalid_country_people = [invalid_with_stored_cc, invalid_default]
        country_backfill_service.backfill_via_credit_card
      end

      it "backfills invalid users using their stored credit card" do
        expect(invalid_with_stored_cc.country).to eq("United States")
        expect(invalid_with_stored_cc[:country]).to eq("US")
      end

      it "does not backfill users having no stored credit card" do
        expect(invalid_default.country).to eq("Unidentified")
      end
    end

    context "#backfill_default_to_country_us" do
      let!(:backfill_using_cc) do
        country_backfill_service.invalid_country_people = [invalid_default]
        country_backfill_service.backfill_default_to_country_us
      end

      it "backfills invalid users using country of US" do
        expect(invalid_default.country).to eq("United States")
        expect(invalid_default[:country]).to eq("US")
      end
    end
  end

  describe "backfilling country_audits" do
    it "writes to the country_audits table" do
      expect(country_backfill_service).to receive(:write_to_country_audits_table)
      country_backfill_service.backfill_country_audit(limit: 1)
    end
  end
end
