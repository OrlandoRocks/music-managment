require "rails_helper"

describe BulkAddAlbumsToStores do
  describe ".add" do
    let(:person) { FactoryBot.create(:person) }

    let(:albums) {
      Array.new.tap do |a|
        3.times do
          a << FactoryBot.create(
            :album,
            :finalized,
            :with_uploaded_songs_and_artwork,
             number_of_songs: 2,
             person: person
          )
        end
      end
    }
    let(:store) {
      Store.first
    }

    def build_cart
      purchases = person.purchases.unpaid.not_in_invoice
      related_products = Product.related_products_for_purchases(person, person.purchases, 2)
      cart_data = Cart::Purchases::Manager.new(person, purchases, related_products)
    end

    it "creates a new salepoint for each album and its corresponding product" do
      albums.each do |album|
        expect(album.salepoints.length).to eq(0)
      end
      BulkAddAlbumsToStores.add(store, albums)
      albums.each do |album|
        expect(album.salepoints.length).to eq(1)
      end
    end

    it "does not create duplicative salepoints and therefore add to store twice" do
      album = albums.first
      album.salepoints.create(store: store)
      expect(album.salepoints.length).to eq(1)
      BulkAddAlbumsToStores.add(store, [album])
      expect(album.salepoints.length).to eq(1)
    end

    it "creates a new purchase for each user + album + salepoint" do
      albums.each do |album|
        expect(album.purchases.length).to eq(0)
      end
      BulkAddAlbumsToStores.add(store, albums)
      albums.each do |album|
        expect(album.salepoints.first.purchase).not_to be_nil
      end
    end

    it "creates a new album in cart for the purchase and person" do
      BulkAddAlbumsToStores.add(store, albums)
      cart_data = build_cart
      expect(cart_data.purchases).to eq(albums.map(&:salepoints).flatten.map(&:purchase))
    end

    context "Instagram" do
      it "does not create a purchase when adding Instagram" do
        ig = Store.find_by(short_name: "Instagram")
        expect{
          BulkAddAlbumsToStores.add(ig, albums)
        }.not_to change(Purchase, :count)
      end
    end
  end
end
