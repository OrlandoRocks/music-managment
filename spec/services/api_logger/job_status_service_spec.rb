require "rails_helper"

describe ApiLogger::JobStatusService do
  describe "#check_status" do
    let(:person) { create(:person) }
    let(:http_client) { double(:http_client) }
    let(:job_status_service) { ApiLogger::JobStatusService.new }

    before do
      job_status_service.instance_variable_set(:@http_client, http_client)
    end

    context "when there are no ApiLoggers in a state of 'processing'" do
      it "should not make a request to Zendesk" do
        expect(http_client).not_to receive(:get)

        job_status_service.check_status
      end
    end

    context "when there are ApiLoggers in a state of 'processing'" do
      it "should not make a request to Zendesk" do
        allow(Zendesk::Person).to receive(:create_apilogger)
        api_loggers = create_list(:api_logger, 2, :with_job_id)
        body = {
          "job_statuses": [
            { "id": api_loggers.first.job_id, "status": "completed"  },
            { "id": api_loggers.last.job_id, "status": "completed"  }
          ]
        }.to_json
        response = double("response", body: body)
        allow(http_client).to receive(:get).and_return(response)

        job_status_service.check_status

        api_loggers_state = api_loggers.map(&:reload).pluck(:state).uniq

        expect(api_loggers_state).to match(["completed"])
      end
    end
  end
end
