require "rails_helper"

describe ApiLogger::InitLoggerService do
  describe ".call" do
    let(:person)  { create(:person, is_verified: false) }

    context "when response is successful" do
      it "should call zendesk api for the requested changes and update the api_logger" do
        response = double("response", body: {'user': {'id': 123, 'name': 'Test', 'email': 'test'}}.to_json, success?: true, headers: {"content-type"=>"application/json; charset=UTF-8"}, status: 200)
        allow_any_instance_of(Faraday::Connection).to receive(:post).and_return(response)
        options = {
          external_service_name: 'Zendesk',
          requestable_id: person.id,
          requestable_type: 'Person',
          request_body: Zendesk::Person.create_client_data(person, {}),
          code_location: [__FILE__, __LINE__],
          state: 'enqueued'
        }
        api_logger = ApiLogger.create(options)
        ApiLogger::InitLoggerService.new(api_logger.id).call
        expect(api_logger.reload.state).to eq('processing')
      end

      it "should check if the user is present in zendesk, if not post request should be sent" do
        response = double("response", body: {'user': {'id': 123, 'name': 'Test', 'email': 'test'}}.to_json, success?: true, headers: {"content-type"=>"application/json; charset=UTF-8"}, status: 200)
        get_response = double("response", body: {'users': []}.to_json, success?: true, headers: {"content-type"=>"application/json; charset=UTF-8"}, status: 200)
        allow_any_instance_of(Faraday::Connection).to receive(:post).and_return(response)
        allow_any_instance_of(Faraday::Connection).to receive(:get).and_return(get_response)
        options = {
          external_service_name: 'Zendesk',
          requestable_id: person.id,
          requestable_type: 'Person',
          request_body: Zendesk::Person.create_client_data(person, {'email': [person.email, person.email + '1']}),
          code_location: [__FILE__, __LINE__],
          state: 'enqueued'
        }
        api_logger = ApiLogger.create(options)
        ApiLogger::InitLoggerService.new(api_logger.id).call
        expect(api_logger.reload.state).to eq('processing')
      end

      it "should check if the user is present in zendesk, if yes update request should be sent" do
        response = double("response", body: {'user': {'id': 123, 'name': 'Test', 'email': 'test'}}.to_json, success?: true, headers: {"content-type"=>"application/json; charset=UTF-8"}, status: 200)
        get_response = double("response", body: {'users': [{'id'=>'123', 'email'=>'test@gmail.com'}]}.to_json, success?: true, headers: {"content-type"=>"application/json; charset=UTF-8"}, status: 200)
        allow_any_instance_of(Faraday::Connection).to receive(:put).and_return(response)
        allow_any_instance_of(Faraday::Connection).to receive(:get).and_return(get_response)
        options = {
          external_service_name: 'Zendesk',
          requestable_id: person.id,
          requestable_type: 'Person',
          request_body: Zendesk::Person.create_client_data(person, {'email': [person.email, person.email + '1']}),
          code_location: [__FILE__, __LINE__],
          state: 'enqueued'
        }
        api_logger = ApiLogger.create(options)
        ApiLogger::InitLoggerService.new(api_logger.id).call
        expect(api_logger.reload.state).to eq('completed')
      end
    end

    context "when the request is not successful" do
      it "should save api_logger state as error" do
        response = double("response", body: {message: 'Not authenticated'}.to_json, success?: false, status: 401, headers: {"content-type"=>"application/json; charset=UTF-8"})
        allow_any_instance_of(Faraday::Connection).to receive(:post).and_return(response)
        options = {
          external_service_name: 'Zendesk',
          requestable_id: person.id,
          requestable_type: 'Person',
          request_body: Zendesk::Person.create_client_data(person, {}),
          code_location: [__FILE__, __LINE__],
          state: 'enqueued'
        }
        api_logger = ApiLogger.create(options)
        ApiLogger::InitLoggerService.new(api_logger.id).call
        expect(api_logger.reload.state).to eq('error')
      end
    end
  end
end
