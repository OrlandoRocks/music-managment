require "rails_helper"

describe ApiLogger::CreateLoggerService do
  describe ".log" do
    let(:person) { create(:person, is_verified: false) }
    let(:service_options) do
      {
        external_service_name: "Zendesk",
        requestable_id: person.id,
        requestable_type: "Person",
        request_body: Zendesk::Person.create_client_data(person),
        code_location: [__FILE__, __LINE__],
        state: "enqueued"
      }
    end

    before(:each) {
      allow(ApiLoggerWorker).to receive(:perform_async).with(anything).and_return(nil)
    }

    after(:each) {
      ApiLogger.destroy_all
    }

    it "enqueues the ApiLoggerWorker" do
      expect(ApiLoggerWorker).to receive(:perform_async)

      Sidekiq::Testing.inline! do
        ApiLogger::CreateLoggerService.new(service_options).log
      end
    end

    it "creates a new api_logger with the options provided" do
      Sidekiq::Testing.inline! do
        ApiLogger::CreateLoggerService.new(service_options).log
      end


      api_logger = ApiLogger.last

      expect(api_logger.requestable_id).to eq(person.id)
    end

    it "sets the external_service_name to Zendesk" do
      Sidekiq::Testing.inline! do
        ApiLogger::CreateLoggerService.new(service_options).log
      end

      api_logger = ApiLogger.last
      expect(api_logger.external_service_name).to eq("Zendesk")
    end

    it "increments the api_loggers table by 1" do
      count = ApiLogger.count
      Sidekiq::Testing.inline! do
        ApiLogger::CreateLoggerService.new(service_options).log
      end

      expect(ApiLogger.count).to eq(count + 1)
    end
  end
end
