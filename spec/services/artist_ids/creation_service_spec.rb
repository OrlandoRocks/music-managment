require "rails_helper"

describe ArtistIds::CreationService do
  let(:album) { create(:album) }
  let(:album2) { create(:album) }

  describe ".create" do
    let(:artist_urls) do
      [{ "apple" =>       apple_artist_url,
        "spotify" =>     spotify_artist_url,
        "artist_name" => album.artist.name }]
    end

    context "with an apple artist url" do
      let(:artist_id)          { "1168770775" }
      let(:apple_artist_url)   { "https://itunes.apple.com/us/artist/ryan-moe/#{artist_id}" }
      let(:spotify_artist_url) { "" }

      it "creates an external_service_id with the correct identifier" do
        allow(Apple::ArtistIdValidator).to receive(:new).and_return(double( valid?: true))
        ArtistIds::CreationService.create(album, artist_urls)
        expect(album.creatives.first.external_service_ids.size).to eq 1

        esi      = album.creatives.first.external_service_ids.first
        creative = album.creatives.first
        expect(esi.service_name).to eq "apple"
        expect(esi.identifier).to   eq artist_id
        expect(esi.linkable_id).to  eq creative.id
      end

      context "ESI matching" do
        before do
          allow(Apple::ArtistIdValidator).to receive(:new).and_return(double( valid?: true))
        end

        context "and a matching spotify ESI" do
          before do
            creative1 = album.creatives.first
            creative1.update(person_id: 123)
            creative2 = create(:creative, creativeable: album2, artist: creative1.artist)
            creative2.update(person_id: 123)
            create(:external_service_id, identifier: '7', service_name: 'spotify', linkable_id: creative2.id, linkable_type: 'Creative')
          end

          it "also creates a new ESI using the matching ESI" do
            ArtistIds::CreationService.create(album, artist_urls)
            album.reload

            esi = album.creatives.first.external_service_ids.second
            expect(esi.identifier).to   eq '7'
            expect(esi.service_name).to eq 'spotify'
            expect(esi.linkable_id).to eq album.creatives.first.id
          end
        end

        context "and a matching youtube_channel_id ESI" do
          before do
            creative1 = album.creatives.first
            creative1.update(person_id: 123)
            creative2 = create(:creative, creativeable: album2, artist: creative1.artist)
            creative2.update(person_id: 123)
            create(:external_service_id, identifier: '8', service_name: 'youtube_channel_id', linkable_id: creative2.id, linkable_type: 'Creative')
          end

          it "also creates a new ESI using the matching ESI" do
            ArtistIds::CreationService.create(album, artist_urls)
            album.reload

            esi = album.creatives.first.external_service_ids.second
            expect(esi.identifier).to   eq '8'
            expect(esi.service_name).to eq 'youtube_channel_id'
            expect(esi.linkable_id).to eq album.creatives.first.id
          end
        end

        context "and a matching youtube_authorization ESI" do
          before do
            creative1 = album.creatives.first
            creative1.update(person_id: 123)
            creative2 = create(:creative, creativeable: album2, artist: creative1.artist)
            creative2.update(person_id: 123)
            create(:external_service_id, identifier: '8', service_name: 'youtube_authorization', linkable_id: creative2.id, linkable_type: 'Creative')
          end

          it "also creates a new ESI using the matching ESI" do
            ArtistIds::CreationService.create(album, artist_urls)
            album.reload

            esi = album.creatives.first.external_service_ids.second
            expect(esi.identifier).to   eq '8'
            expect(esi.service_name).to eq 'youtube_authorization'
            expect(esi.linkable_id).to eq album.creatives.first.id
          end
        end

        context "and secondary creative without ESIs, with one matching ESI" do
          before do
            creative2 = create(:creative, creativeable: album)
            creative2.update(person_id: 123)
            creative3 = create(:creative, creativeable: album2, artist: creative2.artist)
            creative3.update(person_id: 123)
            create(:external_service_id, identifier: '7', service_name: 'apple', linkable_id: creative3.id, linkable_type: 'Creative')
            album.creatives << creative2
          end

          it "also creates a new ESI for the secondary creative with the matching ESI" do
            ArtistIds::CreationService.create(album, artist_urls)
            album.reload
            expect(album.creatives.first.external_service_ids.size).to eq 1

            esis2 = album.creatives.second.external_service_ids
            expect(esis2.size).to                  eq 1
            expect(esis2[0].service_name).to       eq 'apple'
            expect(esis2[0].identifier).to         eq '7'
            expect(ExternalServiceId.all.count).to eq 3
          end
        end

        context "and secondary creative without ESIs, no matching ESI due to name mismatch" do
          before do
            creative2 = create(:creative, creativeable: album)
            creative2.update(person_id: 123)
            creative3 = create(:creative, creativeable: album2)
            creative3.update(person_id: 123)
            create(:external_service_id, identifier: '7', service_name: 'apple', linkable_id: creative3.id, linkable_type: 'Creative')
            album.creatives << creative2
          end

          it "does not create a new ESI" do
            ArtistIds::CreationService.create(album, artist_urls)
            album.reload
            expect(album.creatives.first.external_service_ids.size).to eq 1

            esis2 = album.creatives.second.external_service_ids
            expect(esis2.size).to eq 0
          end
        end

        context "and secondary creative without ESIs, no matching ESI due to person mismatch" do
          let(:creative2)          { create(:creative, creativeable: album) }
          let(:artist3) {
            a = build(:artist, name: creative2.artist.name)
            a.save(validate: false)
            a
          }

          before do
            creative2.update(person_id: 123)
            creative3 = create(:creative, creativeable: album2, artist: artist3)
            creative3.update(person_id: 333)
            create(:external_service_id, identifier: '7', service_name: 'apple', linkable_id: creative3.id, linkable_type: 'Creative')
            album.creatives << creative2
          end

          it "does not create a new ESI" do
            ArtistIds::CreationService.create(album, artist_urls)
            album.reload
            expect(album.creatives.first.external_service_ids.size).to eq 1

            esis2 = album.creatives.second.external_service_ids
            expect(esis2.size).to eq 0
          end
        end

        context "and secondary creative without ESIs, no matching ESI due to type mismatch" do
          let(:creative2)          { create(:creative, creativeable: album) }
          let(:artist3) {
            a = build(:artist, name: creative2.artist.name)
            a.save(validate: false)
            a
          }

          before do
            creative2.update(person_id: 123)
            creative3 = create(:creative, creativeable: album2, artist: artist3)
            creative3.update(person_id: 333)
            create(:external_service_id, identifier: '7', service_name: 'apple', linkable_id: creative3.id, linkable_type: 'Song')
            album.creatives << creative2
          end

          it "does not create a new ESI" do
            ArtistIds::CreationService.create(album, artist_urls)
            album.reload
            expect(album.creatives.first.external_service_ids.size).to eq 1

            esis2 = album.creatives.second.external_service_ids
            expect(esis2.size).to eq 0
          end
        end

        context "and secondary creative with a bad ESI, for which there is a matching ESI" do
          before do
            creative2 = create(:creative, creativeable: album)
            creative2.update(person_id: 123)
            creative3 = create(:creative, creativeable: album2, artist: creative2.artist)
            creative3.update(person_id: 123)
            create(:external_service_id, identifier: nil, service_name: 'spotify', linkable_id: creative2.id, linkable_type: 'Creative', state: nil)
            create(:external_service_id, identifier: '7', service_name: 'spotify', linkable_id: creative3.id, linkable_type: 'Creative')
            album.creatives << creative2
          end

          it "updates the bad ESI with the matching ESI's values, instead of creating an additional ESI" do
            ArtistIds::CreationService.create(album, artist_urls)
            album.reload
            expect(album.creatives.first.external_service_ids.size).to eq 1

            esis2 = album.creatives.second.external_service_ids
            expect(esis2.size).to                  eq 1
            expect(esis2[0].service_name).to       eq 'spotify'
            expect(esis2[0].identifier).to         eq '7'
            expect(ExternalServiceId.all.count).to eq 3
          end
        end
      end
    end

    context "with both an apple and spotify url" do
      let(:apple_artist_id)    { "1168770775" }
      let(:apple_artist_url)   { "https://itunes.apple.com/us/artist/ryan-moe/#{apple_artist_id}" }
      let(:spotify_artist_id)  { "2eam0iDomRHGBypaDQLwWI" }
      let(:spotify_artist_url) { "https://open.spotify.com/artist/#{spotify_artist_id}?si=wahurN89RgaBW8EjzmDU8A" }

      it "creates two external_service_ids with the correct identifiers" do
        allow(Apple::ArtistIdValidator).to receive(:new).and_return(double( valid?: true))
        ArtistIds::CreationService.create(album, artist_urls)

        apple_esi   = album.creatives.first.external_service_ids.first
        spotify_esi = album.creatives.first.external_service_ids.last
        creative    = album.creatives.first
        creative_2  = album.creatives.last

        expect(apple_esi.service_name).to eq "apple"
        expect(apple_esi.identifier).to   eq apple_artist_id
        expect(apple_esi.linkable_id).to  eq creative.id

        expect(spotify_esi.service_name).to eq "spotify"
        expect(spotify_esi.identifier).to   eq spotify_artist_id
        expect(spotify_esi.linkable_id).to  eq creative_2.id
      end
    end

    context "when updating an existing external_service_id and no others exist for artist" do
      let(:apple_artist_id)    { "321" }
      let(:apple_artist_url)   { "https://itunes.apple.com/us/artist/ryan-moe/#{apple_artist_id}" }
      let(:spotify_artist_url) { "" }

      it "uses the new artist identifier" do
        allow(Apple::ArtistIdValidator).to receive(:new).and_return(double( valid?: true))
        creative = album.creatives.first
        create(:external_service_id, linkable: creative, service_name: "apple", identifier: "123")
        expect(album.creatives.first.external_service_ids.first.identifier).to eq "123"

        ArtistIds::CreationService.create(album, artist_urls)
        apple_esi = album.creatives.first.external_service_ids.first

        expect(apple_esi.service_name).to eq "apple"
        expect(apple_esi.identifier).to   eq "321"
        expect(apple_esi.linkable_id).to  eq creative.id
      end
    end

      context "when both artist_urls are blank" do
        let(:single) { create(:single) }
        let(:apple_artist_url)   { "" }
        let(:spotify_artist_url) { "" }

      it "should not create external_service_ids" do
        ArtistIds::CreationService.create(album, artist_urls)
        expect(single.creatives.first.external_service_ids).to be_empty
      end
    end

    context "when given an invalid artist url" do
      let(:apple_artist_url)   { "https://itunes.apple.com/us/invalid/" }
      let(:spotify_artist_url) { "" }

      it "should not create external_service_ids" do
        ArtistIds::CreationService.create(album, artist_urls)
        expect(album.creatives.first.external_service_ids).to be_empty
      end
    end

    context "with a single" do
      let(:single)             { create(:single) }
      let(:artist_id)          { "1168770775" }
      let(:apple_artist_url)   { "https://itunes.apple.com/us/artist/ryan-moe/#{artist_id}" }
      let(:spotify_artist_url) { "" }

      it "creates an external_service_id with the correct identifier" do
        allow(Apple::ArtistIdValidator).to receive(:new).and_return(double( valid?: true))
        artist_urls.first["artist_name"] = "default artist"
        ArtistIds::CreationService.create(single, artist_urls)
        expect(single.creatives.first.external_service_ids.size).to eq 1

        esi      = single.creatives.first.external_service_ids.first
        creative = single.creatives.first

        expect(esi.service_name).to eq "apple"
        expect(esi.identifier).to   eq artist_id
        expect(esi.linkable_id).to  eq creative.id
      end

      it "does not create an external service ID for an invalid artist URL" do
        allow(Apple::ArtistIdValidator).to receive(:new).and_return(double( valid?: false))
        ArtistIds::CreationService.create(single, artist_urls)
        expect(single.creatives.first.external_service_ids.first).to be_nil
      end
    end

    context "with a new artist" do
      let(:artist_urls) do
        [
          {
            apple: "",
            spotify: "",
            artist_name: album.artist.name,
          }.with_indifferent_access
        ]
      end

      before do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
      end


      context "for spotify" do
        context "when artist has no other assoicated spotify ids "
          it "creates an external service id with state new_artist" do
            artist_urls.first[:spotify_new_artist] = "true"

            expect {
              ArtistIds::CreationService.create(album, artist_urls)
            }.to change(ExternalServiceId, :count).by(1)

            latest_id = ExternalServiceId.last

            expect(latest_id.service_name).to eq("spotify")
            expect(latest_id.state).to eq("new_artist")
            expect(latest_id.identifier).to eq("unavailable")
          end

        context "when artist has a spotify ids from another release" do
          context "when new artist is requested" do 
            before do
              artist = album.artist
              person = album.person
         
              album_with_id = create(:album, name: "new_album", artist: artist, person: person)
              creative = create(:creative, artist: artist, creativeable: album_with_id, person: person, creativeable_type: "Album")
              create(:external_service_id, linkable: creative, service_name: 'spotify', identifier: "90210", state: "matched")
            end

            it "does not use the existing spotify id" do
              artist_urls.first[:spotify_new_artist] = "true"

              artist_urls.first[:apple_new_artist] = "true"

              ArtistIds::CreationService.create(album, artist_urls)

              esi = album.creatives.first.external_service_id.first

              expect(esi.service_name).to eq("spotify")
              expect(esi.state).to eq("new_artist")
              expect(esi.identifier).to eq("unavailable")
            end
          end 
        end
      end

      context "for apple" do
        context "when artist has no other assoicated apple ids" do
          it "creates an external service id with state new_artist" do
            artist_urls.first[:apple_new_artist] = "true"

            expect {
              ArtistIds::CreationService.create(album, artist_urls)
            }.to change(ExternalServiceId, :count).by(1)

            latest_id = ExternalServiceId.last

            expect(latest_id.service_name).to eq("apple")
            expect(latest_id.state).to eq("new_artist")
            expect(latest_id.identifier).to eq("unavailable")
          end
        end

        context "when artist has an existing apple id created on a different album" do
          before do
            artist = album.artist
            person = album.person
            artist.person = album.person
            album_with_id = create(:album, name: "new_album", artist: artist, person: person)
            creative = create(:creative, artist: artist, creativeable: album_with_id, person: person, creativeable_type: "Album")
            create(:external_service_id, linkable: creative, service_name: 'apple', identifier: "90210", state: "matched")
          end

          it "uses the existing apple id to populate the esi on the new album" do
            artist_urls.first[:apple_new_artist] = "true"
            ArtistIds::CreationService.create(album, artist_urls)

            esi = album.creatives.first.external_service_id.first

            expect(esi.service_name).to eq("apple")
            expect(esi.state).to eq("matched")
            expect(esi.identifier).to eq("90210")
          end
        end

        context "with an apple artist url that fails validation" do
          let(:apple_artist_url)   { "https://itunes.apple.com/us/artist/test/123" }
          let(:spotify_artist_url) { "" }
          let(:artist_urls) do
            [{ "apple"       => apple_artist_url,
               "spotify"     => spotify_artist_url,
               "artist_name" => album.artist.name }]
          end

          it "creates ESID with nil identifier" do
            allow(Apple::ArtistIdValidator).to receive(:new).and_return(double( valid?: false))
            ArtistIds::CreationService.create(album, artist_urls)

            esi = album.creatives.first.external_service_ids.first
            expect(esi.service_name).to eq "apple"
            expect(esi.identifier.nil?).to be_truthy
            expect(esi.state.nil?).to be_truthy
          end
        end
      end
    end
  end
end
