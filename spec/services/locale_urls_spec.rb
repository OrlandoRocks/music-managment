require "rails_helper"

describe LocaleUrls do
  describe ".build_locale_urls" do
    it "returns the FR website for web" do
      url_hash = LocaleUrls.build_locale_urls("web")
      expect(url_hash["FR"]).to eq("web.tunecore.fr")
    end

    it "returns the us website for studio" do
      url_hash = LocaleUrls.build_locale_urls("studio")
      expect(url_hash["US"]).to eq("studio.tunecore.com")
    end
  end
end
