require "rails_helper"

describe Api::CertService do
  describe "remove_cert" do
    let(:person) { create(:person, :with_unpaid_purchases) }
    let(:purchase) { person.purchases.last }
    it "removes cert" do
      cert = Cert.last
      cert.cert_engine = "DefaultRenewalPercent"
      cert.expiry_date = 30.days.from_now
      cert.save
      verified = Cert.verify verify_opts(entered_code: cert.cert)
      expect(verified).to be_a(Cert)
      expect(purchase.cert).not_to be_nil
      expect(purchase.cert).to be_instance_of Cert
      Api::CertService.new(purchase).remove_cert
      expect(purchase.cert).to be_nil
      expect(TargetedProduct.targeted_product_for_active_offer(purchase.person, purchase.product)).to be_blank
    end
    def verify_opts(options = {})
      {
        entered_code: "0001",
        purchase: purchase,
        current_user: person
      }.merge(options)
    end
  end
end
