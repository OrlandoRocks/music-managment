require 'rails_helper'

describe Api::PeopleFlags::SearchService do
  describe '#search' do
    let(:suspicious) { Flag::SUSPICIOUS[:name] }

    context "when a list of person ids are separated by a comma" do
      it "returns the correct people" do
        person1 = create(:person, status: 'Suspicious')
        person2 = create(:person)
        person3 = create(:person, status: 'Suspicious')
        person1.flags << Flag.suspicious_flag
        person3.flags << Flag.suspicious_flag

        results = Api::PeopleFlags::SearchService.new(
          person_ids: "#{person1.id},#{person2.id},#{person3.id}",
          flag_name: suspicious,
          limit: 25,
          offset: 0
        ).search

        expect(results).to include(person1)
        expect(results).to include(person3)
        expect(results).not_to include(person2)
      end
    end

    context "when a list of person ids are separated by a new line" do
      it "returns the correct people" do
        person1 = create(:person, status: 'Suspicious')
        person2 = create(:person)
        person3 = create(:person, status: 'Suspicious')
        person1.flags << Flag.suspicious_flag
        person3.flags << Flag.suspicious_flag

        results = Api::PeopleFlags::SearchService.new(
          person_ids: "#{person1.id}\n#{person2.id}\n#{person3.id}",
          flag_name: suspicious,
          limit: 25,
          offset: 0
        ).search

        expect(results).to include(person1)
        expect(results).to include(person3)
        expect(results).not_to include(person2)
      end
    end

    context "when a list of person emails are separated by a new line" do
      it "returns the correct people" do
        person1 = create(:person, status: 'Suspicious')
        person2 = create(:person)
        person3 = create(:person, status: 'Suspicious')
        person1.flags << Flag.suspicious_flag
        person3.flags << Flag.suspicious_flag

        results = Api::PeopleFlags::SearchService.new(
          person_ids: "#{person1.email}\n#{person2.email}\n#{person3.email}",
          flag_name: suspicious,
          limit: 25,
          offset: 0
        ).search

        expect(results).to include(person1)
        expect(results).to include(person3)
        expect(results).not_to include(person2)
      end
    end

    context "when a person has a suspicious and blocked from distribution people flag" do
      it "returns both the suspicious and blocked_from_distribution people_flag" do
        person1 = create(:person, status: 'Suspicious')
        person2 = create(:person, status: 'Suspicious')
        person1.flags << [Flag.suspicious_flag, Flag.blocked_from_distribution_flag]
        person2.flags << Flag.suspicious_flag

        results = Api::PeopleFlags::SearchService.new(
          person_ids: "#{person1.email}\n#{person2.email}",
          flag_name: suspicious,
          limit: 25,
          offset: 0
        ).search

        person1_flags = results.find { |person| person.email == person1.email }.people_flags
        suspicious_flag = person1_flags.select { |f| f.flag == Flag.suspicious_flag }
        blocked_from_distro_flag = person1_flags.select do |person_flag|
          person_flag.flag == Flag.blocked_from_distribution_flag
        end
        expect(suspicious_flag).to be_present
        expect(blocked_from_distro_flag).to be_present

        person2_flags = results.find { |person| person.email == person2.email }.people_flags
        suspicious_flag = person2_flags.select { |f| f.flag == Flag.suspicious_flag }
        blocked_from_distro_flag = person2_flags.select do |person_flag|
          person_flag.flag == Flag.blocked_from_distribution_flag
        end
        expect(suspicious_flag).to be_present
        expect(blocked_from_distro_flag).not_to be_present
      end
    end
  end
end
