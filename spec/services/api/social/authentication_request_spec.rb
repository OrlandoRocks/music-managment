require "rails_helper"

describe Api::Social::AuthenticationRequest do
  let(:user) { FactoryBot.create(:person, :with_tc_social, password: "Testpass123!") }
  let(:user_without_tcs) {FactoryBot.create(:person, password: "Testpass123!")}
  it "authenticates a person upon intialization" do
    authenticator = Api::Social::AuthenticationRequest.new({email: user.email, password: "Testpass123!"})
    expect(authenticator).to be_granted
  end

  describe "#granted?" do
    context "when authenticated" do
      context "usre with TCS account" do
        before(:each) do
          @authenticator = Api::Social::AuthenticationRequest.new({email: user.email, password: "Testpass123!"})
        end
        it "returns true" do
          expect(@authenticator.granted?).to eq(true)
        end

        it "sets the token attribute to the authenticated user's tc social token" do
          @authenticator.granted?
          expect(@authenticator.token).to eq user.tc_social_token
        end
      end

      context "user without TCS account" do
        before(:each) do
          @authenticator = Api::Social::AuthenticationRequest.new({email: user_without_tcs.email, password: "Testpass123!"})
        end
        it "generates a new OAuth token for user's without tokens" do
          expect(user_without_tcs.tc_social_token).to eq(nil)
          @authenticator.granted?
          expect(user_without_tcs.tc_social_token).to_not eq(nil)
          expect(@authenticator.token).to eq(user_without_tcs.tc_social_token)
        end
      end

      context "user has not verified their Tunecore account" do
         before(:each) do
          allow(Person).to receive(:authenticate) {user_without_tcs}
          @authenticator = Api::Social::AuthenticationRequest.new({email: user_without_tcs.email, password: "Testpass123!"})
        end
        it "is false" do
          allow(user_without_tcs).to receive(:is_verified?) {false}
          expect(@authenticator.granted?).to eq(false)
        end
      end
    end

    context "when not authenticated" do
      it "returns false" do
        authenticator = Api::Social::AuthenticationRequest.new({email: user.email, password: "wrong-password"})
        expect(authenticator).not_to be_granted
      end
    end
  end
end
