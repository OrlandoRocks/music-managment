require "rails_helper"

describe Api::Linkshare::SongsService do
  describe "#retrieve_songs" do
    it "fetches songs having esi" do
      album1 = create(:album, :purchaseable)
      song1 = create(:song, :with_spotify_song_esi, album: album1)
      album2 = create(:album, :purchaseable)
      song2 = create(:song, :with_spotify_song_esi, album: album2, name: "the song")
      song3 = create(:song, :with_spotify_song_esi, album: album2, name: "the song")
      albums = [album1, album2]
      songs = [song1, song2, song3]
      allow(Api::Linkshare::SongsService.new({}, albums)).to receive(:retrieve_songs).and_return(songs)
    end
  end
end
