require "rails_helper"

describe Api::Linkshare::FetchStoresService do
  let(:distributor_double) { double }
  let(:api_client_double) { double }
  let(:napster_client_double) { double }
  let(:store_order) do
    "{ spotify: 1, apple: 2, amazon: 3, itunes: 4, pandora: 5, google: 6, youtube: 7, deezer: 8, tidal: 9, napster:10 }"
  end
  let(:odesli_response) { JSON.parse(file_fixture("tcs_odesli_response.json").read) }
  let(:tc_distributor_response) do
    {
      123 => OpenStruct.new(
        id: 11_049_516, store_id: 13, delivery_type: "full_delivery",
        delivery_service: "tc_distributor", state: "delivered"
      )
    }
  end
  let(:spotify_identifier) { "53A2MsBqPdDzZzP3Dm8rfR" }

  before do
    allow(distributor_double).to receive(:latest_releases_stores).and_return([])
    allow(TcDistributor::Base).to receive(:new).and_return(distributor_double)
    allow(api_client_double).to receive(:get).and_return(odesli_response)
    allow(Odesli::ApiClient).to receive(:new).and_return(api_client_double)
    allow(napster_client_double).to receive(:fetch_data).and_return(nil)
    allow(ENV).to receive(:[])
    allow(ENV).to receive(:[]).with("STORE_ORDER").and_return(store_order)
    allow(ENV).to receive(:[]).with("LINKSHARE_STORES_API").and_return("1")
  end

  describe "#fetch_odesli_url" do
    let(:album) { create(:album, :purchaseable, :with_apple_music) }
    let(:distro1) { create(:distribution, :delivered_to_spotify) }
    let(:distro2) { create(:distribution, :delivered_to_apple) }
    let(:distro3) { create(:distribution, :delivered_to_amazon) }
    let(:song) { create(:song, :with_spotify_song_esi, album: album) }

    subject { Api::Linkshare::FetchStoresService.new(song, "1").fetch_odesli_url }

    before do
      distro1.petri_bundle.update(album: album)
      distro2.petri_bundle.update(album: album)
      distro3.petri_bundle.update(album: album)
      song.external_service_ids.last.update(identifier: spotify_identifier)
    end

    context "tc-www-delivered" do
      it "creates new external_service_ids for songs" do
        expect { subject }.to change(song.external_service_ids, :count).by(1)
      end
    end

    context "tc-distributor-delivered" do
      it "creates new external_service_ids for songs" do
        allow(distributor_double).to receive(:latest_releases_stores).and_return(tc_distributor_response)
        # Amazon Music (Store Id: 13) is delivered via tc-distributor. So setting state to 'new'.
        distro3.update(state: "new")
        expect { subject }.to change(song.external_service_ids, :count).by(1)
      end
    end

    it "immediate return if store URLs have already been fetched" do
      Api::Linkshare::FetchStoresService::STORES_NAMES.each do |store|
        create(
          :external_service_id, {
            linkable: song,
            identifier: rand(9**9).to_s,
            service_name: store,
            linkable_type: "Song"
          }
        )
      end
      expect(Odesli::ApiClient).not_to receive(:new)
      subject
    end

    it "will not creates new external_service_ids for songs if no delivered distributions present" do
      album.distributions.update(state: "new")
      expect { subject }.to change(song.external_service_ids, :count).by(0)
    end

    it "creates new external_service_ids for song even if Spotify ESI created_at is nil" do
      song.external_service_ids.find_by(service_name: "spotify").update(created_at: nil)
      create(
        :external_service_id,
        linkable: song,
        service_name: "tidal",
        identifier: Faker::Lorem.characters[0..22],
        created_at: 13.days.ago
      )
      expect { subject }.to change(song.external_service_ids, :count).by(1)
    end
  end
end
