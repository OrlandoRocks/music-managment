require "rails_helper"

describe Api::Linkshare::StoresService do
  STORE_ORDER= "{ spotify: 1, apple: 2, amazon: 3, itunes: 4, pandora: 5, google: 6, youtube: 7, deezer: 8, tidal: 9, napster:10 }"
  API_STORE_ORDER="{ spotify: 1, apple: 2, amazon: 3, amazon_music: 4, itunes: 5, pandora: 6, google: 7, youtube: 8, deezer: 9, tidal: 10, napster:11 }"
  SPOTIFY_AFFILIATE_LINK='https://prf.hn/click/camref:11ltXk'
  SPOTIFY_STORE_BASE_URL='https://open.spotify.com'

  describe "#fetch_urls" do
    before do
      allow(ENV).to receive(:[])
      allow(ENV).to receive(:[]).with('SPOTIFY_AFFILIATE_LINK').and_return(SPOTIFY_AFFILIATE_LINK)
      allow(ENV).to receive(:[]).with('SPOTIFY_STORE_BASE_URL').and_return(SPOTIFY_STORE_BASE_URL)
      allow(ENV).to receive(:[]).with('STORE_ORDER').and_return(STORE_ORDER)
      allow(ENV).to receive(:[]).with('API_STORE_ORDER').and_return(API_STORE_ORDER)
    end

    let(:song_name) {"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis,"}
    let(:album) { create(:album, :purchaseable) }
    let(:song) { create(:song, :with_spotify_song_esi, album: album, name: song_name) }
    let(:spotify_store) {
      create(:external_service_id, {
        linkable: song,
        identifier: "100001022",
        service_name: ExternalServiceId::SPOTIFY_SERVICE,
        linkable_type: "Creative"
      })
    }
    let(:song1) do
      artist_name = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. standard dummy text ever since the 1500s"
      OpenStruct.new(name: 'song1', album: OpenStruct.new(person_id: 123456), artist: OpenStruct.new(name: artist_name))
    end
    let(:song2) do
      artist_name = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ra"
      OpenStruct.new(name: 'song2', album: OpenStruct.new(person_id: 123456), artist: OpenStruct.new(name: artist_name))
    end
    let(:song3) do
      artist_name = "artist3"
      song = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ra"
      OpenStruct.new(name: song, album: OpenStruct.new(person_id: 123456), artist: OpenStruct.new(name: artist_name))
    end

    it "fetch spotify uri" do
      external_service_ids = ExternalServiceId.where(linkable: song.album)
      spotify_uri = Api::Linkshare::StoresService.new(external_service_ids, [spotify_store], song).fetch_urls[0][:uri]
      expect(spotify_uri).to include(SPOTIFY_AFFILIATE_LINK)
      expect(URI.unescape(spotify_uri)).to include(SPOTIFY_STORE_BASE_URL)
    end

    it "pubref string must <= 255" do
      # condition - artistid, userid, songname equal to 255
      external_service_ids = ExternalServiceId.where(linkable: song.album)
      spotify_stores_service = Api::Linkshare::StoresService.new(external_service_ids, [spotify_store], song)
      spotify_uri = spotify_stores_service.spotify_pubref(song)
      expect(spotify_uri.length).to be <= 255

      # condition - artist name and user id > 255 to truncat and return 255 char truncat, omit song name
      spotify_uri1 = spotify_stores_service.spotify_pubref(song1)
      expect(spotify_uri1.length).to be <= 255
      expect(spotify_uri1).not_to include(song1.name)

      # condition - for Eliminate symbol check with artist name and user id between 255, 254
      spotify_uri2 = spotify_stores_service.spotify_pubref(song2)
      expect(spotify_uri2.length).to be <= 255
      expect(spotify_uri2).not_to include("-#{song2.name}")

      # condition - artistid, userid, songname where exceeded 255 truncating song name
      spotify_uri3 = spotify_stores_service.spotify_pubref(song3)
      expect(spotify_uri3.length).to be <= 255
      expect(spotify_uri3).not_to include("-#{song3.name}")
      truncated_song = "-Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen "
      expect(spotify_uri3).to include(truncated_song)
    end
  end
end
