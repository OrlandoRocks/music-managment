require 'rails_helper'

describe Api::SongsService do
  describe 'songs_sort' do
    it "returns a string 'name asc' when sort_param is asc" do
      sort_param = "asc"
      expect(Api::SongsService.new(sort_param, nil).song_sort).to eq("songs.name asc")
    end

    it "returns a string 'name desc' when sort_param is desc" do
      sort_param = "desc"
      expect(Api::SongsService.new(sort_param, nil).song_sort).to eq("songs.name desc")
    end

    it "returns a string 'albums.sale_date asc' when sort_param is old" do
      sort_param = "old"
      expect(Api::SongsService.new(sort_param, nil).song_sort).to eq("albums.sale_date asc")
    end

    it "returns a string 'albums.sale_date desc' when sort_param is new " do
      sort_param = "new"
      expect(Api::SongsService.new(sort_param, nil).song_sort).to eq("albums.sale_date desc")
    end
  end
end
