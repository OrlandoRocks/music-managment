require "rails_helper"

describe SongData::ParamsScrubber do
  before { allow_any_instance_of(ReviewAudit).to receive(:send_sns_notification) }

  let(:person)        { create(:person)}
  let(:admin) {
    person = create(:person)
    person.roles << Role.all
    person
  }
  let(:album)         { create(:album, person_id: person.id) }
  let(:album_with_review_audit) { create(:album, :with_petri_bundle) }
  let!(:review_audit) { create(:review_audit, album: album_with_review_audit, event: "APPROVED", person: admin) }

  describe ".scrub" do
    context "non nil album is submitted" do
      it "should return all parameters in the original state if album is editable" do
        mock_params = {upc: "012345678901", isrc: "1122334455", album_id: album.id, other_data: 'other data'}
        mock_params = SongData::ParamsScrubber.scrub(mock_params, album)
        expect(mock_params).to eq({upc: "012345678901", isrc: "1122334455", album_id: album.id, other_data: 'other data'}.with_indifferent_access)
      end

      it "should return all the parameters except for the ISRC and UPC if album is uneditable" do
        mock_params = {upc: "012345678901", isrc: "1122334455", album_id: album_with_review_audit.id, other_data: 'other data'}
        mock_params = SongData::ParamsScrubber.scrub(mock_params, album_with_review_audit)
        expect(mock_params).to eq({album_id: album_with_review_audit.id, other_data: "other data"}.with_indifferent_access)
      end
    end

    context "a nil album is submitted" do
      it "should return the parameters in it's original state" do
        mock_params = {upc: "012345678901", isrc: "1122334455", album_id: nil, other_data: 'other data'}
        mock_params = SongData::ParamsScrubber.scrub(mock_params, nil)
        expect(mock_params).to eq({upc: "012345678901", isrc: "1122334455", album_id: nil, other_data: 'other data'}.with_indifferent_access)
      end
    end

    context "optional_upc_field param needs to be scrubbed" do
      it "should remove the optional_upc_field if the album param exists" do
        mock_params = {
          other_data: "other data",
          optional_upc_number: "012345678901",
          more_other_data: "more other data"

        }
        mock_params = SongData::ParamsScrubber.scrub(mock_params, album_with_review_audit)
        expect(mock_params).to eq({ other_data: "other data", more_other_data: "more other data" }.with_indifferent_access)
      end

      it "should return the original params if the album param does not exist" do
        mock_params = { other_data: "other data" }
        mock_params = SongData::ParamsScrubber.scrub(mock_params, album_with_review_audit)
        expect(mock_params).to eq({ other_data: "other data" }.with_indifferent_access)
      end
    end
  end
end
