require "rails_helper"

describe SongData::CompletionReport do
  let(:person)        { create(:person) }
  let(:album)         { create(:album, person_id: person.id) }
  let(:songwriter_id) { SongRole.where(role_type: "songwriter").first.id }
  let(:song_artists)  { [double(associated_to: "Album", credit: "primary_artist", role_ids: songwriter_id.to_s, name: "artist name")] }
  let(:song)          { create(:song, album_id: album.id) }
  let(:song_data)     { SongDataBuilder.build(person.id, song) }

  context "required fields are completed" do
    before(:each) do
      allow(song_data).to receive(:name?).and_return(true)
      allow(song_data).to receive(:asset_url?).and_return(true)
      allow(song_data).to receive(:artists).and_return(song_artists)
      @report = SongData::CompletionReport.new(song_data)
    end

    it "has required_fields_completed? true" do
      expect(@report.completed_required_fields?).to be true
    end
    it "generates the correct report breakdown" do
      expect(@report.breakdown[:required_fields]).to eq(
        name:       true,
        asset_url:  true,
        songwriter: true
      )
    end
  end

  context "suggested fields are completed" do
    before(:each) do
      allow(song_data).to receive(:name?).and_return(true)
      allow(song_data).to receive(:asset_url?).and_return(true)
      allow(song_data).to receive(:artists).and_return(song_artists)
      @report = SongData::CompletionReport.new(song_data)
    end

    it "has suggested_fields_completed? true" do
      expect(@report.completed_suggested_fields?).to be true
    end

    it "generates the correct report breakdown" do
      expect(@report.breakdown[:suggested_fields]).to eq(artists: true)
    end
  end

  context "required fields are not completed" do
    let(:report) { SongData::CompletionReport.new(song_data) }
    it "has required_fields_completed? false" do
      expect(report.completed_required_fields?).to be false
    end
    it "generates the correct report breakdown" do
      expect(report.breakdown[:required_fields]).to eq(
        name: true,
        asset_url: false,
        songwriter: false
      )
    end
  end

  context "suggested fields are not completed" do
    let(:report) { SongData::CompletionReport.new(song_data) }
    it "has suggested_fields_completed? false" do
      expect(report.completed_suggested_fields?).to be false
    end
    it "generates the correct report breakdown" do
      expect(report.breakdown[:suggested_fields]).to eq(artists: false)
    end
  end
end
