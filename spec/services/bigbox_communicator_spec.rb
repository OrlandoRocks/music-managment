require "rails_helper"

describe BigboxCommunicator do
  describe ".enqueue_for_bigbox" do
    it "puts it in the queue" do
      song   = create(:song)
      sqs    = double
      queue  = double
      queues = double(named: queue)
      allow(sqs).to receive(:queues).and_return(queues)
      allow(sqs).to receive(:queue).and_return(queue)
      stub_const("SQS_CLIENT", sqs)
      expect(queue).to receive(:send_message)
      BigboxCommunicator.enqueue_for_bigbox("test queue", "test job", "test locker", ["http://imgr.com/blahblah"], "xxxxx", "xxxxx", {package_name: "test package", song_id: song.id, person_id: song.album.person.id})
    end
  end
end
