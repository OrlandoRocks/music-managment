require "rails_helper"

describe YoutubeMonetizations::SyncYoutubeMonetizationChangesToBelieveApi do
  describe ".call (as a callback)" do
    let(:person)   { Person.find(69) } # Active customer
    let(:youtube_monetization) { create(:youtube_monetization, person: person) }

    before(:each) do
      allow(Believe::PersonWorker)
        .to receive(:perform_async)
        .and_return(true)

      allow(Believe::Person)
        .to receive(:add_or_update)
        .and_return(true)
    end

    it "calls believe api if effective_date has changed" do
      expect(Believe::PersonWorker).to receive(:perform_async)

      youtube_monetization.effective_date = 6.months.ago
      youtube_monetization.save
    end

    it "calls believe api if termination_date has changed" do
      expect(Believe::PersonWorker).to receive(:perform_async)

      youtube_monetization.termination_date = 6.months.ago
      youtube_monetization.save
    end

    it "doesn't call believe api if effective_date has not changed" do
      expect(Believe::PersonWorker).not_to receive(:perform_async)

      youtube_monetization.save
    end

    it "doesn't call believe api if termination_date has not changed" do
      expect(Believe::PersonWorker).not_to receive(:perform_async)

      youtube_monetization.save
    end
  end
end
