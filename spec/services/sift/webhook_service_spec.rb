require "rails_helper"

describe Sift::WebhookService do
  include_context "sift_webhook_shared_context"

  let(:invalid_note_options) {
    {
      "related_id"=>"",
      "related_type"=>"",
      "ip_address"=>"127.0.0.1",
      "note"=>"Sift Automated Decision Marked as Suspicious".freeze,
      "subject"=>"Sift Identified - Will Not Distribute".freeze,
      "note_created_by_id"=>0
    }
  }

  let(:nil_note_double) {
    double(
      valid?: false, 
      errors: double(
        messages: {
          :subject=>["can't be blank"],
          :note=>["can't be blank"],
          :related_id=>["can't be blank"],
          :related_type=>["can't be blank"],
          :note_created_by_id=>["can't be blank"]
        }
      )
    )
  }

  let(:invalid_note_double) {
    double(
      valid?: false, 
      errors: double(
        messages: {
          :related_id=>["can't be blank"],
          :related_type=>["can't be blank"]
        }
      )
    )
  }

  describe "#initialize" do
    context "options should" do
      it "not be nil because feature flag 'sift_webhooks' is enabled" do
        service = Sift::WebhookService.new(valid_serializer_json)
        expect(service.options).to eq(valid_serializer_json)
      end
      it "be nil because feature flag 'sift_webhooks' is disabled" do
        allow(FeatureFlipper).to receive(:show_feature?).with(:sift_webhooks).and_return(false)
        service = Sift::WebhookService.new(valid_serializer_json)
        expect(service.options).to_not eq(valid_serializer_json)
      end
    end
  end

  describe "#process" do
    context "should call #mark_as_suspicious because" do
      it "releases_active is 0" do
        expect_any_instance_of(Sift::WebhookService).to receive(:mark_as_suspicious)
        Sift::WebhookService.new(valid_serializer_json).process
      end

      it "releases_active is not 0" do
        expect_any_instance_of(Sift::WebhookService).to receive(:mark_as_suspicious)
        valid_serializer_response["releases_active"] = 1
        Sift::WebhookService.new(valid_serializer_response).process
      end
    end

    context "should not call #mark_as_suspicious" do
      it "because feature flag 'sift_decisions' is disabled" do
        expect(Airbrake).to_not receive(:notify)
        allow(FeatureFlipper).to receive(:show_feature?).with(:sift_decisions).and_return(false)
        Sift::WebhookService.new(valid_serializer_json).process
      end

      context "because" do
        before(:each) do
          expect(Airbrake).to receive(:notify)
          expect_any_instance_of(Sift::WebhookService).to_not receive(:mark_as_suspicious)
        end

        it "person is nil" do
          valid_serializer_json.delete "person_id"
          Sift::WebhookService.new(valid_serializer_json).process
        end

        context "note_options is" do
          it "nil" do
            allow(Note).to receive(:create).with(nil).and_return(nil_note_double)
            valid_serializer_json.delete "note_options"

            Sift::WebhookService.new(valid_serializer_json).process
          end
          it "invalid" do
            allow(Note).to receive(:create).with(invalid_note_options).and_return(invalid_note_double)
            valid_serializer_json["note_options"] = invalid_note_options

            Sift::WebhookService.new(valid_serializer_json).process
          end
        end
      end
    end
  end

  describe "#mark_as_suspicious" do
    it "person should be marked as suspicious" do
      expect_any_instance_of(Person).to receive(:mark_as_suspicious!)
      Sift::WebhookService.new(valid_serializer_json).process
    end
  end
end
