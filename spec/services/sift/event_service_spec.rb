require "rails_helper"

describe Sift::EventService do
  let(:user) {
    create(:person)
  }

  describe "#update_account" do
    it "#update_account_email should set update_type to :email" do
      expect(Sift::EventService).to receive(:update_account).with(hash_including(update_type: :email))
      Sift::EventService.update_account_email(nil, nil, nil)
    end

    it "#update_account_password should set update_type to :password" do
      expect(Sift::EventService).to receive(:update_account).with(hash_including(update_type: :password))
      Sift::EventService.update_account_password(nil, nil, nil)
    end

    it "#update_account_contact_info should set update_type to :contact_info" do
      expect(Sift::EventService).to receive(:update_account).with(hash_including(update_type: :contact_info))
      Sift::EventService.update_account_contact_info(user, {}, nil, nil)
    end

    it "#update_account_payment_methods should set update_type to :payment_methods" do
      expect(Sift::EventService).to receive(:update_account).with(hash_including(update_type: :payment_methods))
      Sift::EventService.update_account_payment_methods(nil, nil, nil)
    end
  end

  describe "create_order flow" do
    let(:order_status_body) {
      {
        "$user_id" => user.id.to_s,
        "$order_id" => "Test_Order_Id",
        "$order_status" => "$cancelled"
      }
    }

    let(:create_order_body) {
      {
        "$user_id" => user.id.to_s,
        "$order_id" => "Test_Order_Id"
      }
    }

    let(:transactions_bodies) {
      [{}]
    }

    subject(:event_service) {
      Sift::EventService.new({
        order_status_body: order_status_body,
        create_order_body: create_order_body,
        transaction_bodies: transactions_bodies
      }.with_indifferent_access)
    }

    describe "#create_order" do
      before :each do
        allow_any_instance_of(Sift::EventService).to receive(:should_create_order?).and_return(false)
        allow_any_instance_of(Sift::EventService).to receive(:should_cancel_previous_order?).and_return(false)
      end

      describe "should create_order" do
        before :each do
          allow_any_instance_of(Sift::EventService).to receive(:should_create_order?).and_return(true)
        end

        it "should send transaction events" do
          expect(event_service).to_not receive(:send_async).with(Sift::EventService::ORDER_STATUS, any_args)
          expect(Sift::ApiClient).to receive(:send_request).with(Sift::EventService::CREATE_ORDER, create_order_body, any_args)
          expect(event_service).to receive(:send_async).with(Sift::EventService::TRANSACTION, any_args)
          event_service.create_order
        end

        describe "should send order_status event" do
          before :each do
            allow_any_instance_of(Sift::EventService).to receive(:should_cancel_previous_order?).and_return(true)
          end

          it "with no transaction events being sent" do
            expect(event_service).to receive(:send_async).with(Sift::EventService::ORDER_STATUS, any_args)
            expect(Sift::ApiClient).to receive(:send_request).with(Sift::EventService::CREATE_ORDER, create_order_body, any_args)
            expect(event_service).to_not receive(:send_async).with(Sift::EventService::TRANSACTION, any_args)
            event_service.instance_variable_set(:@body, {
              order_status_body: order_status_body,
              create_order_body: create_order_body
            })
            event_service.create_order
          end

          it "with transaction events sent" do
            expect(event_service).to receive(:send_async).with(Sift::EventService::ORDER_STATUS, any_args)
            expect(Sift::ApiClient).to receive(:send_request).with(Sift::EventService::CREATE_ORDER, create_order_body, any_args)
            expect(event_service).to receive(:send_async).with(Sift::EventService::TRANSACTION, any_args)
            event_service.create_order
          end
        end
      end

      it "should only call transaction (re-attempt purchase)" do
        expect(event_service).to_not receive(:send_async).with(Sift::EventService::ORDER_STATUS, any_args)
        expect(Sift::ApiClient).to_not receive(:send_request).with(Sift::EventService::CREATE_ORDER, create_order_body, any_args)
        expect(Sift::ApiClient).to receive(:send_request).with(Sift::EventService::TRANSACTION, any_args)
        event_service.create_order
      end
    end

    describe "#should_create_order? should return" do
      it "true" do
        allow(PersonSiftScore).to receive(:find_by).and_return(double(sift_order_id: "New_Order_Id"))
        expect(event_service.send("should_create_order?")).to eq(true)
      end

      it "false" do
        allow(PersonSiftScore).to receive(:find_by).and_return(double(sift_order_id: "Test_Order_Id"))
        expect(event_service.send("should_create_order?")).to eq(false)
      end
    end

    describe "#should_cancel_previous_order? should return" do
      describe "false if should_create_order is" do
        it "true and there is no pending order" do
          allow(PersonSiftScore).to receive(:find_by).and_return(double(sift_order_id: nil, pending_v1_order?: false))
          expect(event_service.send("should_cancel_previous_order?")).to eq(false)
        end

        it "false otherwise" do
          allow(PersonSiftScore).to receive(:find_by).and_return(double(sift_order_id: nil, pending_v1_order?: false))
          expect(event_service.send("should_cancel_previous_order?")).to eq(false)
        end
      end

      describe "true" do
        it "when should_create_order is true and there is a pending order" do
          allow(PersonSiftScore).to receive(:find_by).and_return(double(sift_order_id: "Pending_Order_Id", pending_v1_order?: false))
          expect(event_service.send("should_cancel_previous_order?")).to eq(true)
        end
      end
    end
  end
end
