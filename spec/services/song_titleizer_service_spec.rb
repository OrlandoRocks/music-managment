require "rails_helper"

describe SongTitleizerService do
  describe ".titleize" do
    it "returns the artist name for multiple artists" do
      featured_artists = [ build(:artist, name: "Lil Wayne"), build(:artist, name: "2 Chainz") ]
      title = SongTitleizerService.titleize("no problem",featured_artists)
      expect(title).to eq("No Problem (feat. Lil Wayne & 2 Chainz)")
    end
  end
end
