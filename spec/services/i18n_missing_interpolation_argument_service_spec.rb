require "rails_helper"

describe I18nMissingInterpolationArgumentService do
  let!(:wildcard_args) do
    [
      :amount,
      { another_key: "Test!" },
      "This is a string %{amount}"
    ]
  end

  describe ".call" do
    it "raises an airbrake error" do
      expect(Airbrake).to receive(:notify)
      I18nMissingInterpolationArgumentService.new.call(*wildcard_args)
    end

    it "returns the key" do
      response = I18nMissingInterpolationArgumentService.new.call(*wildcard_args)
      expect(response).to eq("amount")
    end
  end
end
