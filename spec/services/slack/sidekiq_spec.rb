require "rails_helper"

describe Slack::Sidekiq do
  describe ".notify" do
    let(:context_hash) { OpenStruct.new(item: job_data, score: "12345678") }
    let(:job_data) do
      {
        retry: 1,
        class: "Ytsr::TrackWorker",
        args: [6],
        jid: "a8b36cf220ae635658fa5fe2",
        error_message: "undefined local variable or method `youtube' for #<Ytsr::TrackWorker:0x007f9fe2552a38>",
        error_class: "NameError",
        failed_at: 1508531301.6104262,
        retry_count: 1,
        error_backtrace: ["Error occured in \"occurred\" in Error", ""]
      }.with_indifferent_access
    end

    context "when a job is retriable" do
      context "and has not specified a retry limit" do
        it "should not send a message to slack" do
          context_hash.item[:retry] = true
          allow(Sidekiq::DeadSet).to receive_message_chain(:new, :find_job).and_return(context_hash)
          expect(Faraday).not_to receive(:new)
          Slack::Sidekiq.new(job_data).notify
        end
      end

      context "and has specified a retry limit" do
        it "should not send a message to slack" do
          context_hash.item[:retry] = 2
          allow(Sidekiq::DeadSet).to receive_message_chain(:new, :find_job).and_return(context_hash)
          expect(Faraday).not_to receive(:new)
          Slack::Sidekiq.new(job_data).notify
        end
      end
    end
  end
end
