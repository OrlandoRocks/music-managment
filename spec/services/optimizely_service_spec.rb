require "rails_helper"
include OptimizelyService

describe OptimizelyService do

  describe "#record_optimizely_event" do

    it "should get to optimizely" do
      expect(Net::HTTP).to receive(:get).with(OPTIMIZELY_URL, "/event?a=7620104&n=purchase&u=123&x140278788=135314952&v=500")
      record_optimizely_event( "purchase", 123, "%7B%22140278788%22%3A%22135314952%22%7D", :v=>500 )
    end

    it "should catch an exception" do
      expect(Net::HTTP).to receive(:get).and_raise(StandardError)
      expect(Rails.logger).to receive(:error)
      record_optimizely_event( "purchase", 123, "%7B%22140278788%22%3A%22135314953%22%7D", :v=>500 )
    end

  end

end
