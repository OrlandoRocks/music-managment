require "rails_helper"

describe SuspiciousLoginService do
  describe "#investigate" do
    let(:person) { create(:person) }
    let(:login_event) { create(:login_event, person: person) }
    let(:login_event_id) { login_event.id }

    let!(:past_events) do
      (1..6).map do |i|
        create(:login_event, person: person, city: "city_#{i}", country: "country_#{i}")
      end
    end

    before do
      allow(SessionsMailer).to receive(:suspicious_login_mailer).and_return(double("mailer", deliver_now: true))
    end

    after do
      SuspiciousLoginService.new(login_event_id).investigate
    end

    context "invalid login event" do
      it "should do nothing" do
        login_event.destroy
        expect(SessionsMailer).to_not receive(:suspicious_login_mailer)
      end
    end

    context "login event has missing info" do
      it "should do nothing" do
        login_event.update(city: nil)
        expect(SessionsMailer).to_not receive(:suspicious_login_mailer)
      end
    end

    context "user has active two factor auth" do
      it "should do nothing" do
        create(:two_factor_auth, person: person)
        create(:login_event, person: person)

        expect(SessionsMailer).to_not receive(:suspicious_login_mailer)
      end
    end

    context "person has suspicious email sent recently" do
      it "should do nothing" do
        allow_any_instance_of(Person).to receive(:suspicious_email_sent_recently?).and_return(true)
        expect(SessionsMailer).to_not receive(:suspicious_login_mailer)
      end
    end

    context "user does not have enough past events" do
      it "should do nothing" do
        person.login_events.where.not(id: login_event.id).delete_all
        create(:login_event, person: person)
        expect(SessionsMailer).to_not receive(:suspicious_login_mailer)
      end
    end

    context "user has login event from the same city in the past" do
      it "should do nothing" do
        past_events.last.update(city: login_event.city, country: login_event.country)
        expect(SessionsMailer).to_not receive(:suspicious_login_mailer)
      end
    end

    context "user has login events from the same location but beyond past events threshold" do
      it "should send suspicious email" do
        past_events.first.update(city: login_event.city, country: login_event.country)
        expect(SessionsMailer).to receive(:suspicious_login_mailer)
      end
    end

    context "user has no login event from the location in the recent threshold time" do
      it "should send suspicious email" do
        expect(SessionsMailer).to receive(:suspicious_login_mailer)
      end
    end
  end
end
