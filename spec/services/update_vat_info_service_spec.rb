require "rails_helper"

describe "Remove vat info on country change" do
  before do
    @person = create(:person, country: 'Austria', customer_type: 'business')
    @person.build_vat_information({ company_name: 'company', vat_registration_number: 'U57477929' }).save
  end

  it "should remove vat info" do
    expect(@person.vat_information&.company_name).to eq 'company'
    allow(FeatureFlipper).to receive(:show_feature?).with(:vat_tax, @person).and_return(true)
    result = UpdateVatInfoService.new(person: @person, country_code: "US").process
    @person.reload
    expect(@person.vat_information&.company_name).to eq ""
    expect(result[:show_vat_warning]).to eq true
  end

  it "should not remove vat info" do
    expect(@person.vat_information&.company_name).to eq 'company'
    UpdateVatInfoService.new(person: @person, country_code: 'US').process
    expect(@person.vat_information&.company_name).to eq 'company'
  end
end
