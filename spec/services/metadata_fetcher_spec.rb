require "rails_helper"

describe MetadataFetcher do
  let(:distro) do
    create(
      :distribution,
      :with_salepoint,
      converter_class: "MusicStores::SevenDigital::Converter",
      delivery_type: "full_delivery"
    )
  end
  let(:metadata_key) { "metadata_key" }
  let(:response_url) { "response_url" }

  describe ".fetch_metadata" do
    it "calls the aws s3 wrapper with the petri archive bucket, and its metadata_key" do
      allow_any_instance_of(MetadataFetcher).to receive(:metadata_key)
        .and_return(metadata_key)

      expect(AwsWrapper::S3).to receive(:read_url).with(
        bucket: PETRI_ARCHIVE_BUCKET,
        key: metadata_key
      ).and_return(response_url)

      expect(MetadataFetcher.fetch_metadata(distro)).to eq(response_url)
    end
  end

  describe ".metadata_key" do
    context "All other stores" do
      it "correctly formats the metadata key" do
        distro.converter_class = "MusicStores::DDEX::Converter"
        object_key = "#{distro.petri_bundle.album.upc}/#{distro.store.short_name}/#{distro.sqs_message_id}.xml"
        expect(MetadataFetcher.metadata_key(distro)).to eq(object_key)
      end
    end

    context "AmazonOD" do
      it "correctly formats the metadata key" do
        distro.converter_class = "MusicStores::Amazonod::Converter"
        amazonod_object_key = "#{distro.petri_bundle.album.id}/#{distro.store.short_name}/#{distro.sqs_message_id}.xml"
        expect(MetadataFetcher.metadata_key(distro)).to eq(amazonod_object_key)
      end
    end
  end

  describe ".archived_metadata_url" do
    context "distro has not been delivered" do
      it "is nil" do
        allow(distro).to receive(:state).and_return("not_delivered")
        expect(MetadataFetcher.archived_metadata_url(distro)).to eq(nil)
      end
    end

    context "distro has been delivered" do
      it "calls the aws s3 wrapper with the petri archive bucket, its metadata key, and a ten minute expires at" do
        allow_any_instance_of(MetadataFetcher).to receive(:metadata_key)
          .and_return(metadata_key)
        allow(distro).to receive(:state).and_return("delivered")

        expect(AwsWrapper::S3).to receive(:read_url).with(
          bucket: PETRI_ARCHIVE_BUCKET,
          key: metadata_key,
          options: {
            expires: 10.minutes
          }
        ).and_return(response_url)

        expect(MetadataFetcher.archived_metadata_url(distro)).to eq(response_url)
      end
    end
  end
end
