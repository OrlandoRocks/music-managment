require "rails_helper"

describe StoreCreator do
  it "raises error if store already exists" do
    store_info = { id: 36, name: "iTunes", abbrev: "ww", short_name: "iTunesWW", position: 10 }
    expect{StoreCreator.create(store_info)}.to raise_error(StoreCreator::StoreExistsError)
  end

  it "creates a new store with defaults" do
    store_info = { name: "Dummy Store", abbrev: "dummy", short_name: "Dummy", position: 1000 }
    StoreCreator.create(store_info)
    expect(Store.exists?(abbrev: "dummy")).to eq true

    expect(SalepointableStore.joins(:store)
      .where(Store.arel_table[:abbrev].eq("dummy")
      .and(SalepointableStore.arel_table[:salepointable_type].eq('Album'))).any?).to eq true

    expect(SalepointableStore.joins(:store)
      .where(Store.arel_table[:abbrev].eq("dummy")
      .and(SalepointableStore.arel_table[:salepointable_type].eq('Single'))).any?).to eq true
  end
end
