require "rails_helper"

describe AssetsUploader::Transcoder do
  let(:song) { create(:song) }
  let(:source_bucket) { double(:source_bucket) }
  let(:destination_bucket) { double(:destination_bucket) }

  let(:s3_resource) { double(:s3_resource) }
  let(:s3_bucket) { double(:s3_bucket) }
  let(:s3_object) { double(:s3_object) }

  let(:media_metadata_json) {
    {
      "media"=>{
        "@ref"=> "foobar.com",
        "track"=>[
          {
            "@type"=>"General",
          },
          {
            "@type"=>"Audio",
          }
        ]
      }
    }
  }

  before(:each) do
    allow_any_instance_of(AssetsUploader::Transcoder).to receive(:media_metadata).and_return(media_metadata_json)
  end

  describe "#upload" do  
    context "uploaded asset is mp3" do
      before do
        allow_any_instance_of(described_class).to receive(:ffprobe_metadata).and_return(
          JSON.parse(
            {
              streams: [
                {
                  sample_rate: 44_100,
                  sample_fmt: "s16",
                  codec_name: "mp3",
                  duration: 123.2
                }
              ],
              format: {
                bit_rate: 44_000
              }
            }.to_json
          )
        )
        expect(SecureRandom).to receive(:uuid).and_return("uuid")
        allow(Aws::S3::Bucket).to receive(:new).with(BIGBOX_BUCKET_NAME).and_return(destination_bucket)
        allow(Aws::S3::Bucket).to receive(:new).with("s3_bucket").and_return(source_bucket)
        allow(Time).to receive(:current).and_return(DateTime.parse("Fri, 17 Sep 2021 13:19:23 +0530"))

        expect(Aws::S3::Resource).to receive(:new).and_return(s3_resource)
        expect(s3_resource).to receive(:bucket).with(BIGBOX_BUCKET_NAME).and_return(s3_bucket)
      end

      it "does not copy orig asset(bucket is already same), copy mp3 asset to required location and return s3_streaming_asset" do
        asset_url = "s3://#{BIGBOX_BUCKET_NAME}/s3_key.mp3"
        source_object = double(:source_object)
        destination_object = double(:destination_object)
        mp3_source_object = double(:mp3_source_object)
        mp3_destination_object = double(:mp3_destination_object)

        expect(destination_bucket).to receive(:object).with("#{song.person.id}/s3_key.mp3.mp3").and_return(mp3_destination_object)
        expect(destination_bucket).to receive(:object).with("s3_key.mp3").and_return(mp3_source_object)

        expect(mp3_destination_object).to receive(:copy_from).with(mp3_source_object)

        expect(s3_bucket).to receive(:object).with("s3_key.mp3").and_return(s3_object)
        expect(s3_object).to receive(:presigned_url).with(:get).and_return("streaming_url")

        expect(described_class.new(asset_url: asset_url, song: song, orig_filename: "s3_key.mp3").upload).to eq(
          {
            uuid: "uuid",
            bucket: BIGBOX_BUCKET_NAME,
            key: "#{song.person.id}/s3_key.mp3",
            orig_filename: "s3_key.mp3",
            bit_rate: 44_000,
            duration: 123_200,
            status: 0,
            created_at: 1_631_864_963_000,
            streaming_url: "streaming_url"
          }
        )

        song.reload
        expect(song.s3_streaming_asset.key).to eq("#{song.person.id}/s3_key.mp3.mp3")
        expect(song.s3_streaming_asset.bucket).to eq(BIGBOX_BUCKET_NAME)
      end
    end

    context "uploaded asset is not mp3" do
      before do
        allow_any_instance_of(described_class).to receive(:ffprobe_metadata).and_return(
          JSON.parse(
            {
              streams: [
                {
                  sample_rate: 44_100,
                  sample_fmt: "s16",
                  codec_name: "flac",
                  duration: 123.2
                }
              ],
              format: {
                bit_rate: 44_000
              }
            }.to_json
          )
        )
        expect(SecureRandom).to receive(:uuid).and_return("uuid")
        allow(Aws::S3::Bucket).to receive(:new).with(BIGBOX_BUCKET_NAME).and_return(destination_bucket)
        allow(Aws::S3::Bucket).to receive(:new).with("s3_bucket").and_return(source_bucket)
        allow(Time).to receive(:current).and_return(DateTime.parse("Fri, 17 Sep 2021 13:19:23 +0530"))

        expect(Aws::S3::Resource).to receive(:new).and_return(s3_resource)
        expect(s3_resource).to receive(:bucket).with(BIGBOX_BUCKET_NAME).and_return(s3_bucket)
      end

      it "copies original asset to required location and calls MigrateNonFlac to generate streaming asset" do
        asset_url = "s3://s3_bucket/s3_key.flac"
        source_object = double(:source_object)
        destination_object = double(:destination_object)
        migrate_non_flac = double(:migrate_non_flac)
        s3_orig_asset = double(:s3_orig_asset)

        expect(source_bucket).to receive(:object).with("s3_key.flac").and_return(source_object)
        expect(destination_bucket).to receive(:object).with("#{song.person.id}/1631864963000-s3_key.flac").and_return(destination_object)
        expect(S3Asset).to receive(:new).with(
          key: "#{song.person.id}/1631864963000-s3_key.flac",
          bucket: BIGBOX_BUCKET_NAME
        ).and_return(s3_orig_asset)
        expect(destination_object).to receive(:copy_from).with(source_object)

        expect(s3_bucket).to receive(:object).with("s3_key.flac").and_return(s3_object)
        expect(s3_object).to receive(:presigned_url).with(:get).and_return("streaming_url")

        expect(AssetsBackfill::MigrateNonFlac).to receive(:new).with(
          {
            song: song,
            asset: s3_orig_asset,
            destination_asset_key: "#{song.person.id}/1631864963000-s3_key.flac.mp3",
            output_format: {
              extension: "mp3"
            },
            callback_url: "https://#{CALLBACK_API_URLS['US']}/api/transcoder/songs/set_streaming_asset"
          }
        ).and_return(migrate_non_flac)

        expect(migrate_non_flac).to receive(:run)

        expect(described_class.new(asset_url: asset_url, song: song).upload).to eq(
          {
            uuid: "uuid",
            bucket: BIGBOX_BUCKET_NAME,
            key: "#{song.person.id}/1631864963000-s3_key.flac",
            orig_filename: "1631864963000-s3_key.flac",
            bit_rate: 44_000,
            status: 0,
            created_at: 1_631_864_963_000,
            streaming_url: "streaming_url",
            duration: 123_200
          }
        )

        song.reload
        expect(song.s3_streaming_asset).to be nil
      end
    end
  end
  describe "#validate_asset" do
    let(:asset_url) { "s3://s3_bucket/s3_key.flac" }
    subject(:transcoder) { described_class.new(asset_url: asset_url, song: song) }
    
    it "returns asset details when durations are valid" do
      asset_url = "s3://s3_bucket/s3_key.flac"
      asset_details = double(:asset_details)

      allow(transcoder).to receive(:duration).and_return(5000)
      allow(transcoder).to receive(:asset_details).and_return(asset_details)
      expect(transcoder.validate_asset).to eq(asset_details)
    end

    context "with immersive audio" do
      before do
        create(:spatial_audio, song: song, s3_asset: create(:s3_asset))
        service = instance_double(ImmersiveAudio::ValidationService)
        allow(ImmersiveAudio::ValidationService).to receive(:new).and_return(service)
        allow(service).to receive(:call).and_return({ errors: { validate_duration: :immersive_duration_error } })
        allow(transcoder).to receive(:duration).and_return(5000)
      end

      it "returns an error if an existing immesrive audio file does not match duration" do
        asset_url = "s3://s3_bucket/s3_key.flac"
        expect(transcoder.validate_asset).to eq(
          errors: { 
            validate_duration: :immersive_duration_error 
            } 
          )
      end
    end

    it "returns error when duration is less than 2 seconds" do
      asset_url = "s3://s3_bucket/s3_key.flac"
      asset_details = double(:asset_details)

      allow(transcoder).to receive(:duration).and_return(1000)
      expect(transcoder.validate_asset).to eq(
        errors: {
          duration: "Duration is 2 seconds or less."
        }
      )
    end

    context "when other tracks have no duration" do
      it "returns asset_details " do
        create(:song, duration_in_seconds: nil, album: song.album)
        asset_url = "s3://s3_bucket/s3_key.flac"
        asset_details = double(:asset_details)

        allow(transcoder).to receive(:duration).and_return(10000)
        allow(transcoder).to receive(:asset_details).and_return(asset_details)
        expect(transcoder.validate_asset).to eq(asset_details)
      end
    end

    context "when validating file format" do
      let(:transcoder_args) {
        { asset_url: asset_url, song: song, file_type_validation_enabled: true }
      }

      subject(:transcoder) { described_class.new(transcoder_args) }

      context "when file has the appropriate stereo file format" do
        let(:media_metadata_json) {
          {
            "media"=>{
              "@ref"=> "foobar.com",
              "track"=>[
                {
                  "@type"=>"General",
                },
                {
                  "@type"=>"Audio",
                }
              ]
            }
          }
        }

        before(:each) do
          allow_any_instance_of(AssetsUploader::Transcoder).to receive(:media_metadata).and_return(media_metadata_json)
        end

        it "returns asset_details" do
          asset_url = "s3://s3_bucket/s3_key.flac"
          asset_details = double(:asset_details)

          allow(transcoder).to receive(:duration).and_return(5000)
          allow(transcoder).to receive(:asset_details).and_return(asset_details)

          expect(transcoder.validate_asset).to eq(asset_details)
        end
      end

      context "when file has an atmos file format" do
        let(:media_metadata_json) {
          {
            "media"=>{
              "@ref"=> "foobar.com",
              "track"=>[
                {
                  "@type"=>"General",
                },
                {
                  "@type"=>"Audio",
                  "extra"=>
                  {
                    "AdmProfile_Format"=>"Dolby Atmos Master"
                  }
                }
              ]
            }
          }
        }

        before(:each) do
          allow_any_instance_of(AssetsUploader::Transcoder).to receive(:media_metadata).and_return(media_metadata_json)
        end

        it "returns an error" do
          asset_url = "s3://s3_bucket/s3_key.flac"
          asset_details = double(:asset_details)

          allow(transcoder).to receive(:duration).and_return(5000)

          expect(transcoder.validate_asset).to eq(
            errors: {
              validate_file_type: :stereo_file_type_error
            }
          )
        end
      end
    end
  end
end
