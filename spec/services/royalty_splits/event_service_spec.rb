require "rails_helper"

describe RoyaltySplits::EventService do
  describe "recipient event hooks" do
    subject { described_class }
    context "when changing split percents" do
      let!(:split) { royalty_split_with_recipients 2 }
      let(:recipient1) { split.recipients.first }
      let(:recipient2) { split.recipients.second }
      before do
        allow(subject).to receive(:recipient_changed_percent)
        allow(subject).to receive(:recipient_created)
        allow(subject).to receive(:recipient_destroyed)
        recipient1.percent = 20
        recipient2.percent = 80
        split.recipients = [recipient1, recipient2]
        split.save!
      end
      it { should have_received(:recipient_changed_percent).with(recipient1, from: 50) }
      it { should have_received(:recipient_changed_percent).with(recipient2, from: 50) }
      it { should have_received(:recipient_changed_percent).twice }
      it { should_not have_received(:recipient_created) }
      it { should_not have_received(:recipient_destroyed) }
    end
    context "when creating a split" do
      let(:split) { build_royalty_split_with_recipients 3 }
      before do
        allow(subject).to receive(:split_created)
        allow(subject).to receive(:recipient_changed_percent)
        allow(subject).to receive(:recipient_created)
        allow(subject).to receive(:recipient_destroyed)
        split.save!
      end
      it "should call recipient_created" do
        aggregate_failures "once for each recipient" do
          split.recipients.each do |recipient|
            should have_received(:recipient_created).with(recipient).once
          end
        end
      end
      it { should have_received(:recipient_created).exactly(3).times }
      it { should have_received(:split_created).once }
      it { should_not have_received(:recipient_changed_percent) }
      it { should_not have_received(:recipient_destroyed) }
    end
    context "when destroying a split" do
      let(:owner) { create :person }
      let!(:split) do
        royalty_split_with_recipients 3, owner: owner do |split|
          split.title = "Hi there"
        end
      end
      let!(:recipients) { split.recipients }
      before do
        allow(subject).to receive(:split_destroyed)
        allow(subject).to receive(:recipient_changed_percent)
        allow(subject).to receive(:recipient_created)
        allow(subject).to receive(:recipient_destroyed)
        split.destroy!
      end
      it "should have received recipient destroyed three times" do
        aggregate_failures "once for each recipient" do
          recipients.each do |recipient|
            should have_received(:recipient_destroyed).with(recipient).once
          end
        end
      end
      it { should have_received(:recipient_destroyed).exactly(3).times }
      it { should have_received(:split_destroyed).once }
      it { should_not have_received(:recipient_changed_percent) }
      it { should_not have_received(:recipient_created) }
    end
  end

  describe ".recipient_changed_percent" do
    let!(:recipient) { create :royalty_split_recipient }
    it "should send a notification" do
      expect { described_class.recipient_changed_percent recipient, from: 2 }.to change { Notification.count }.by(1)
    end
  end

  describe ".recipient_created" do
    let!(:recipient) { create :royalty_split_recipient }
    it "should send a notification" do
      expect { described_class.recipient_created recipient }.to change { Notification.count }.by(1)
    end
  end

  describe ".recipient_destroyed" do
    let!(:recipient) { create :royalty_split_recipient }
    it "should send a notification" do
      expect { described_class.recipient_destroyed recipient }.to change { Notification.count }.by(1)
    end
  end
end
