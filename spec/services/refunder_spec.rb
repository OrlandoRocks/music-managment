require "rails_helper"

describe Refunder do
  let(:person)                { create(:person, :admin) }
  let(:stored_credit_card)    { create(:stored_credit_card) }
  let(:invoice)               { create(:invoice) }
  let(:braintree_transaction) { create(:braintree_transaction,
                                  invoice_id: invoice.id,
                                  stored_credit_card_id: stored_credit_card.id)
                              }
  let(:refunded_transaction)  { double({valid?: true, success?: true}).as_null_object }
  let(:invoice2)              { create(:invoice) }
  let(:person_transaction)    { create(:person_transaction, target_id: invoice2.id, target_type: "Invoice" )}
  let(:balance_adjustment)    { double({valid?: true}).as_null_object }

  describe ".refund_double_charges" do
    context "for real" do
      before(:each) do
        allow_any_instance_of(Refunder).to receive(:double_credit_card_transactions).and_return([braintree_transaction])
        allow_any_instance_of(Refunder).to receive(:double_balance_transaction_invoices).and_return([invoice2])
        allow_any_instance_of(Refunder).to receive(:current_server_ip).and_return("10.0.0.175")
      end

      it "processes cc refunds for all double credit card transactions in the given period" do
        expect(BraintreeTransaction).to receive(:process_refund).with(
          braintree_transaction.id,
          braintree_transaction.amount,
          {
            refund_reason: "person was charged twice",
            refund_category: "other",
            refunded_by_id: person.id,
            ip_address: "10.0.0.175"
          }
        ).and_return(refunded_transaction)
        Refunder.refund_double_charges({start_date: "2018-03-01", admin_id: person.id, for_real: true})
      end

      it "creates balances adjustment credits for all person transactions in the period" do
        expect(BraintreeTransaction).to receive(:process_refund).with(
          braintree_transaction.id,
          braintree_transaction.amount,
          {
            refund_reason: "person was charged twice",
            refund_category: "other",
            refunded_by_id: person.id,
            ip_address: "10.0.0.175"
          }
        ).and_return(refunded_transaction)
        expect(BalanceAdjustment).to receive(:create).with(
          credit_amount: invoice2.final_settlement_amount_cents.to_i/100.0,
          person_id: invoice2.person_id,
          category: "Refund - Other",
          admin_note: "Redfunding double charge",
          posted_by_id: person.id,
          posted_by_name: person.name,
          customer_note: "customer was charged twice"
        ).and_return(balance_adjustment)
        Refunder.refund_double_charges({start_date: "2018-03-01", admin_id: person.id, for_real: true})
      end
    end

    context "dry run" do
      it "only performs a dry run unless explicitly told to process refunds for real" do
        allow_any_instance_of(Refunder).to receive(:double_credit_card_transactions).and_return(double(count: 1, pluck: [braintree_transaction.id] ))
        allow_any_instance_of(Refunder).to receive(:double_balance_transaction_invoices).and_return(double(count: 1, pluck: [invoice2.id]))
        expect(BalanceAdjustment).not_to receive(:create)
        Refunder.refund_double_charges({start_date: "2018-03-01", admin_id: person.id})
      end
    end
  end
end

