require "rails_helper"

RSpec.describe CurrencyMinorUnitConverter, type: :service do
  describe "#adjust_minor_unit_for" do
    it "returns amount in minor unit for target currency" do
      subject1 = described_class.new("GBP", "BHD")
      expect(subject1.adjust_minor_unit_for(1)).to eq(10.to_d)

      subject2 = described_class.new("BHD", "GBP")
      expect(subject2.adjust_minor_unit_for(10)).to eq(1.to_d)
    end
  end
end
