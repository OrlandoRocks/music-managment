require "rails_helper"

describe LifetimeEarningsFetcher do
  describe ".fetch" do
    context "when the asset type is not an Album or a Song" do
      it "raises an error" do
        error_msg = "You can only fetch lifetime earnings for an Album or Song."

        expect {
          LifetimeEarningsFetcher.fetch(1, "Single")
        }.to raise_error(error_msg)
      end
    end

    context "when a song is not found" do
      it "raises an error" do
        expect {
          LifetimeEarningsFetcher.fetch(nil, "Song")
        }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
