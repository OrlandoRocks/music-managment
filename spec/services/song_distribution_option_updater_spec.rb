require "rails_helper"

describe SongDistributionOptionUpdater, type: :model do
  let(:args) do
    { option_name: option_name, option_value: option_value, upc_codes: upc_codes }
  end

  describe '#initialize' do
    it { is_expected.to validate_presence_of(:option_name) }
    it { is_expected.to validate_presence_of(:option_value) }
    it { is_expected.to validate_presence_of(:upc_codes) }
    it { is_expected.to validate_inclusion_of(:option_name).in_array(['available_for_streaming', 'free_song', 'album_only']) }
    it { is_expected.to validate_inclusion_of(:option_value).in_array(['true', 'false']) }
  end

  describe '.update' do
    let(:song_dist_opt_updater) { described_class.update(args) }

    context 'when the option name is invalid' do
      let(:option_name)  { 'foo' }
      let(:option_value) { 'true' }
      let(:upc_codes)    { [1] }

      it 'should create an invlaid option name error message' do
        expect(song_dist_opt_updater.errors.messages[:option_name]).to include "is not included in the list"
      end
    end

    context 'when the option value is invalid' do
      let(:option_name)  { 'available_for_streaming' }
      let(:option_value) { 'bar' }
      let(:upc_codes)    { [1] }

      it 'should create an invalid option value error message' do
        expect(song_dist_opt_updater.errors.messages[:option_value]).to include "is not included in the list"
      end
    end

    context 'when the correct values are provided' do
      let(:option_name)    { 'available_for_streaming' }
      let(:option_value)   { 'false' }
      let(:album)          { create(:album, :with_uploaded_song_distribution_option) }
      let(:valid_upc_code) { album.upcs.first.number }

      context 'when there are invalid UPC codes' do
        let(:upc_codes) { [12345, 54321, valid_upc_code] }

        it 'should store the invalid upc codes' do
          expect(song_dist_opt_updater.rejected_upc_codes).to eq [12345, 54321]
        end
      end

      context 'when there are valid UPC codes' do
        let(:upc_codes)         { [valid_upc_code]}
        let(:updated_opt_value) { album.songs.first.song_distribution_options.first.option_value }

        it 'should update a songs distribution option with the option value' do
          expect(song_dist_opt_updater.rejected_upc_codes).to eq []
          expect(updated_opt_value).to eq false
        end
      end
    end
  end
end
