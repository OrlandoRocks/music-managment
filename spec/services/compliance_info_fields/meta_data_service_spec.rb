require 'rails_helper'

describe ComplianceInfoFields::MetaDataService do
  let(:us_person) { create(:person, :with_address, country: "US") }

  let(:payout_provider_contact_individual) do
    {
      "first_name" => Faker::Name.first_name,
      "last_name" => Faker::Name.last_name,
      "email" => Faker::Internet.unique.email,
      "mobile" => Faker::PhoneNumber.unique.phone_number,
      "phone" => Faker::PhoneNumber.unique.phone_number
    }
  end

  let!(:payee_extended_detail_response_individual) do
    Payoneer::Responses::PayeeExtendedDetail.new(
      double(
        "response",
        body: JSON.dump(
          contact: payout_provider_contact_individual,
          type: "INDIVIDUAL"
        )
      )
    )
  end

  def call_verify_payee_compliance_info(person)
    PayoutProvider::VerifyPayeeComplianceInfo.new(
      person,
      provider_payee_details: payee_extended_detail_response_individual
    ).call
  end

  describe ".display" do
    context "when address and compliance_fields are updated for US person" do
      before do
        call_verify_payee_compliance_info(us_person)
        us_person.reload
      end

      it "displays persons first name" do
        expect(us_person.compliance_first_name).to eq(described_class.display(us_person).dig(:first_name, :field_value))
      end

      it "displays person for first_name as locked" do
        expect(described_class.display(us_person)[:first_name][:locked]).to eq(true)
      end

      it "displays persons last name" do
        expect(us_person.compliance_last_name).to eq(described_class.display(us_person).dig(:last_name, :field_value))
      end

      it "displays person for last_name as locked" do
        expect(described_class.display(us_person)[:first_name][:locked]).to eq(true)
      end

      it "displays persons for company_name as not locked" do
        expect(described_class.display(us_person)[:company_name][:locked]).to eq(false)
      end

      it "displays persons company_name as nil" do
        expect(us_person.compliance_company_name).to eq(nil)
        expect(described_class.display(us_person).dig(:company_name, :field_value)).to eq(nil)
      end

      it "displays persons compliance_info_field_id for company_name as nil" do
        expect(described_class.display(us_person)[:company_name][:compliance_info_field_id]).to eq(nil)
      end

      it "displays persons for company_name as not locked" do
        expect(described_class.display(us_person)[:company_name][:locked]).to eq(false)
      end
    end
  end
end