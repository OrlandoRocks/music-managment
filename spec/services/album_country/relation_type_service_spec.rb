require "rails_helper"

describe AlbumCountry::RelationTypeService do
  describe ".update" do
    let(:album) { create(:album, :live, :with_salepoints) }

    it "sets the countries based on passed in iso_codes" do
      countries = Country.non_eu_gb_only

      service = AlbumCountry::RelationTypeService.update(album, countries)

      expect(service.included).to eq Country.non_eu_gb_only
      expect(album.countries).to eq Country.non_eu_gb_only
    end

    it "excluded all countries if passed a blank array" do
      service = AlbumCountry::RelationTypeService.update(album, [""])
      expect(service.included).to be_empty
      expect(album.countries).to be_empty

      service = AlbumCountry::RelationTypeService.update(album, [nil])
      expect(service.included).to be_empty
      expect(album.countries).to be_empty

      service = AlbumCountry::RelationTypeService.update(album, [])
      expect(service.included).to be_empty
      expect(album.countries).to be_empty
    end
  end

  describe "#included" do
    let(:album) { create(:album) }
    let(:sanctioned_country) { Country.unscoped.find_by(is_sanctioned: true) }
    let(:countries) { Country.non_eu_gb_only }
    let(:album_country) {
      create(:album_country, album: album, country: sanctioned_country)
    }

    it "returns a list of countries included for distribution" do
      countries.each do |country|
        create(:album_country, album: album, country: country, relation_type: "include" )
      end
      expect(AlbumCountry::RelationTypeService.included(album)).to eq countries
      expect(album.countries).to eq countries
    end

    it "does not return an excluded sanctioned country" do
      album_country.update(relation_type: "exclude")
      expect(AlbumCountry::RelationTypeService.included(album)).not_to include(sanctioned_country)
      expect(album.countries).not_to include(sanctioned_country)
    end

    it "does not return an included sanctioned country" do
      album_country.update(relation_type: "include")
      expect(AlbumCountry::RelationTypeService.included(album)).not_to include(sanctioned_country)
      expect(album.countries).not_to include(sanctioned_country)
    end
  end

  describe "#excluded" do
    let(:album) { create(:album) }
    context "when an album does not have excluded or included countries" do
      it "should return an empty array" do
        expect(AlbumCountry::RelationTypeService.excluded(album)).to eq([])
        expect(album.excluded_countries).to eq([])
      end
    end

    context "when an album has excluded countries" do
      it "should return an array of excluded countries" do
        excluded_country = Country.find_by(iso_code: "CA")
        create(:album_country, relation_type: "exclude", album: album, country: excluded_country)
        expect(AlbumCountry::RelationTypeService.excluded(album)).to eq([excluded_country])
        expect(album.excluded_countries).to eq([excluded_country])
      end
    end

    context "when an album has included countries" do
      it "should return an array of excluded countries" do
        included_country = Country.find_by(iso_code: "US")
        create(:album_country, relation_type: "include", album: album, country: included_country)
        expect(AlbumCountry::RelationTypeService.excluded(album)).to eq(Country.unsanctioned - [included_country])
        expect(album.excluded_countries).to eq(Country.unsanctioned - [included_country])
      end
    end
  end
end
