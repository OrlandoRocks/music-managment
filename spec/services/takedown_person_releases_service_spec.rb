require "rails_helper"

describe TakedownPersonReleasesService do
  describe ".takedown_releases" do
    it "invokes the MassTakedownForm with the proper arguments" do
      person = create(:person, :with_live_album)
      admin = create(:person, :admin)
      ip_address = "127.0.0.1"

      options = {
        album_ids: person.live_releases.map(&:id).map(&:to_s).join(","),
        track_level_stores: Store::TRACK_MONETIZATION_STORES,
        album_level_stores: "all",
        send_email: "0",
        ip_address: ip_address,
        admin: admin
      }

      expect(MassTakedownForm).to receive(:new).with(options).and_call_original

      TakedownPersonReleasesService.takedown_releases(
        person.id,
        admin.id,
        ip_address
      )
    end
  end
end
