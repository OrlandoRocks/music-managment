require "rails_helper"

describe UserDetectionService do
  let(:user_agent_string) { "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36" }
  let(:request) { double("Request", user_agent: user_agent_string, remote_ip: "100.37.214.82")}

  before(:each) do
    @service = UserDetectionService.new(request)
  end

  describe ".browser_name" do
    it "returns the browser name" do
      expect(@service.browser_name).to eq("Chrome")
    end
  end

  describe ".location" do
    it "returns user's location with a valid ip" do
      allow(Geocoder).to receive(:search).and_return([double("result", city: "Topeka", state: "KS", country: "USA")])

      expect(@service.location).to eq("Topeka, KS USA")
    end

    it "returns nil without any geocoder results" do
      allow(Geocoder).to receive(:search).and_return([double("result", city: nil, state: nil, country: nil)])

      expect(@service.location).to eq(nil)
    end
  end
end
