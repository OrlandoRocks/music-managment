require "rails_helper"

describe TaxWithholdings::Reporting::IRSPayablesReportCsvService, type: :service do
  before(:all) { IRSTransaction.destroy_all }

  let!(:current_tax_withheld_user) do
    create(:person, country: "US")
  end

  let!(:current_taxable_person_transactions) do
    10.times.map do
      create(
        :person_transaction,
        :taxable_balance_adjustment,
        :with_irs_tax_withholding,
        person: current_tax_withheld_user,
        credit: 100.00,
        debit: 0.00
      )
    end
  end

  let!(:current_irs_transaction) { TaxWithholdings::Reporting::WithholdingsSummarizationService.new.summarize! }
  let!(:report_file)             { "taxwithholding_test_report_#{Time.now.to_i}.csv" }
  let!(:generated_report_data)   { subject.generate_csv!(current_irs_transaction, report_file) }

  describe "#generate_csv!" do
    it "returns accurate report stats" do
      _csv_report, report_stats = generated_report_data

      expect(report_stats).to include(
        irs_transaction_id: current_irs_transaction.id,
        currency_wise_withheld_amount: {
          "USD" => current_taxable_person_transactions.map(&:irs_tax_withholding).map(&:withheld_amount).sum
        },
        total_withheld_accounts: 1,
        withholding_init_date: DateTime.current.beginning_of_year.strftime("%m/%d/%Y"),
        withholding_end_date: current_irs_transaction.created_at.strftime("%m/%d/%Y")
      )
    end

    it "returns csv with accurate withheld_amount" do
      csv_report, _report_stats = generated_report_data
      csv_withheld_amount_total = CSV.parse(File.read(csv_report), headers: true).entries
        .sum { |csv_record| BigDecimal(csv_record["total_tax_withholding_amount"]) }

      expect(csv_withheld_amount_total)
        .to eql(TaxWithholdings::IRSWithholdingsService::IRS_PERCENTAGE * current_taxable_person_transactions.sum(&:credit))
    end

    it "returns csv with accurate record count" do
      csv_report, _report_stats = generated_report_data
      withheld_people_count = CSV.parse(File.read(csv_report)).entries.slice(1..).count
      expect(withheld_people_count).to eql(1)
    end
  end
end
