require "rails_helper"

describe TaxWithholdings::TransactionRangeFinderService, type: :service do

  include ActiveSupport::Testing::TimeHelpers

  let(:tax_year) { 2022 }
  let(:prev_jan_02) { "#{tax_year - 1}-01-02".to_datetime }
  let(:prev_april_02) { "#{tax_year - 1}-04-02".to_datetime }
  let(:jan_02) { "#{tax_year}-01-02".to_datetime }
  let(:jan_15) { "#{tax_year}-01-15".to_datetime }
  let(:feb_02) { "#{tax_year}-02-02".to_datetime }
  let(:feb_14) { "#{tax_year}-02-14".to_datetime }
  let(:mar_02) { "#{tax_year}-03-02".to_datetime }

  let(:fr_user_with_past_us_audits) do
    person = create(:person)
    self_identified = create(:country_audit_source)
    payoneer_kyc = create(:country_audit_source, name: "Payoneer KYC")
    create(:country_audit, person: person, country: Country.find_by(iso_code: "US"), country_audit_source: payoneer_kyc, created_at: prev_jan_02)
    create(:country_audit, person: person, country: Country.find_by(iso_code: "US"), country_audit_source: self_identified, created_at: jan_02)
    create(:country_audit, person: person, country: Country.find_by(iso_code: "US"), country_audit_source: payoneer_kyc, created_at: jan_15)
    create(:country_audit, person: person, country: Country.find_by(iso_code: "FR"), country_audit_source: self_identified, created_at: feb_02)
    create(:country_audit, person: person, country: Country.find_by(iso_code: "US"), country_audit_source: payoneer_kyc, created_at: feb_14)
    create(:country_audit, person: person, country: Country.find_by(iso_code: "FR"), country_audit_source: payoneer_kyc, created_at: mar_02)

    person
  end

  let(:us_hopping_user) do
    person = create(:person)
    self_identified = create(:country_audit_source)
    payoneer_kyc = create(:country_audit_source, name: "Payoneer KYC")
    create(:country_audit, person: person, country: Country.find_by(iso_code: "US"), country_audit_source: self_identified, created_at: DateTime.parse("14-01-2020"))
    create(:country_audit, person: person, country: Country.find_by(iso_code: "US"), country_audit_source: payoneer_kyc, created_at: DateTime.parse("14-08-2022"))

    person
  end

  let(:us_territory_user) do
    person = create(:person)
    self_identified = create(:country_audit_source)
    payoneer_kyc = create(:country_audit_source, name: "Payoneer KYC")
    create(:country_audit, person: person, country: Country.find_by(iso_code: "US"), country_audit_source: payoneer_kyc, created_at: prev_jan_02)
    create(:country_audit, person: person, country: Country.find_by(iso_code: "AS"), country_audit_source: self_identified, created_at: jan_02)
    create(:country_audit, person: person, country: Country.find_by(iso_code: "GU"), country_audit_source: payoneer_kyc, created_at: jan_15)
    create(:country_audit, person: person, country: Country.find_by(iso_code: "PR"), country_audit_source: self_identified, created_at: feb_02)
    create(:country_audit, person: person, country: Country.find_by(iso_code: "UM"), country_audit_source: payoneer_kyc, created_at: feb_14)
    create(:country_audit, person: person, country: Country.find_by(iso_code: "VI"), country_audit_source: payoneer_kyc, created_at: mar_02)

    person
  end

  let(:us_user_with_past_us_french_and_german_audits) do
    person = create(:person)
    self_identified = create(:country_audit_source)
    payoneer_kyc = create(:country_audit_source, name: "Payoneer KYC")
    create(:country_audit, person: person, country: Country.find_by(iso_code: "US"), country_audit_source: payoneer_kyc, created_at: prev_jan_02)
    create(:country_audit, person: person, country: Country.find_by(iso_code: "FR"), country_audit_source: self_identified, created_at: jan_02)
    create(:country_audit, person: person, country: Country.find_by(iso_code: "DE"), country_audit_source: payoneer_kyc, created_at: jan_15)
    create(:country_audit, person: person, country: Country.find_by(iso_code: "DE"), country_audit_source: self_identified, created_at: feb_02)
    create(:country_audit, person: person, country: Country.find_by(iso_code: "FR"), country_audit_source: payoneer_kyc, created_at: feb_14)
    create(:country_audit, person: person, country: Country.find_by(iso_code: "US"), country_audit_source: payoneer_kyc, created_at: mar_02)

    person
  end

  let(:old_us_user) do
    person = create(:person)
  end


  describe "#call" do
    it "returns correct time ranges for french user that was a us user in previous year and part of current year" do
      travel_to(Time.parse("#{tax_year - 1 }-11-22")) do
        fr_user_with_past_us_audits.country_audits
      end

      travel_to(Time.parse("#{tax_year}-11-22")) do
        result = TaxWithholdings::TransactionRangeFinderService.new(fr_user_with_past_us_audits.id, tax_year).call
        expect(result.to_s).to eq("[{:range=>2022-01-01 00:00:00 UTC..2022-02-02 00:00:00 UTC, :country=>\"US\"}, {:range=>2022-02-02 00:00:00 UTC..2022-02-14 00:00:00 UTC, :country=>\"FR\"}, {:range=>2022-02-14 00:00:00 UTC..2022-03-02 00:00:00 UTC, :country=>\"US\"}, {:range=>2022-03-02 00:00:00 UTC..2022-12-31 23:59:59 UTC, :country=>\"FR\"}]")
      end
    end

    it "returns correct time ranges for us user that hopped around US territories only" do
      travel_to(Time.parse("#{tax_year - 1 }-11-22")) do
        us_territory_user.country_audits
      end

      travel_to(Time.parse("#{tax_year}-11-22")) do
        result = TaxWithholdings::TransactionRangeFinderService.new(us_territory_user.id, tax_year).call
        expect(result.to_s).to eq("[{:range=>2022-01-01 00:00:00 UTC..2022-12-31 23:59:59 UTC, :country=>\"US\"}]")
      end
    end

    it "returns correct time ranges for us user that hopped around europe" do
      travel_to(Time.parse("#{tax_year - 1 }-11-22")) do
        us_user_with_past_us_french_and_german_audits.country_audits
      end

      travel_to(Time.parse("#{tax_year}-11-22")) do
        result = TaxWithholdings::TransactionRangeFinderService.new(us_user_with_past_us_french_and_german_audits.id, tax_year).call
        expect(result.to_s).to eq("[{:range=>2022-01-01 00:00:00 UTC..2022-01-02 00:00:00 UTC, :country=>\"US\"}, {:range=>2022-01-02 00:00:00 UTC..2022-02-02 00:00:00 UTC, :country=>\"FR\"}, {:range=>2022-01-15 00:00:00 UTC..2022-02-14 00:00:00 UTC, :country=>\"DE\"}, {:range=>2022-02-14 00:00:00 UTC..2022-03-02 00:00:00 UTC, :country=>\"FR\"}, {:range=>2022-03-02 00:00:00 UTC..2022-12-31 23:59:59 UTC, :country=>\"US\"}]")
      end
    end

    it "returns correct time ranges for us user that joined two years prior" do
      travel_to(Time.parse("#{tax_year - 2 }-11-20")) do
        old_us_user.country_audits
      end

      travel_to(Time.parse("#{tax_year}-11-22")) do
        result = TaxWithholdings::TransactionRangeFinderService.new(old_us_user.id, tax_year).call
        expect(result.to_s).to eq("[{:range=>2022-01-01 00:00:00 UTC..2022-12-31 23:59:59 UTC, :country=>\"US\"}]")
      end
    end

    it "return correct time ranges if the person updated the same country again in 2022 without updates in the previous year" do
      result = TaxWithholdings::TransactionRangeFinderService.new(us_hopping_user.id, 2022).call
      expect(result.to_s).to eq("[{:range=>2022-01-01 00:00:00 UTC..2022-12-31 23:59:59 UTC, :country=>\"US\"}]")
    end
  end
end
