require "rails_helper"

describe StoreLaunchEmail do
  let(:store)    { FactoryBot.create(:store, launched_at: Date.today+1.week) }
  let(:person)   { FactoryBot.create(:person) }
  let(:album)    { FactoryBot.create(:album, person: person) }
  let(:releases) { [album.id] }
  let(:release_data) {
    [{
        artist_name: album.artist_name,
        album_type: album.album_type,
        album_name: album.name,
        upc: album.upc.number
    }]
  }

  before(:each) do
    FactoryBot.create(:salepoint_subscription, album: album, finalized_at: Time.now, effective: Time.now)
    releases_by_person = {person.id=>[album.id]}
    $redis.set("StoreEmails:#{store.short_name}", releases_by_person.to_json)
  end
  describe ".send_store_launch_email" do
    it "sends the email" do
      allow(Store).to receive(:find).with(store.id).and_return(store)
      allow(Person).to receive(:find).with(person.id).and_return(person)
      allow(AutomatorMailer).to receive(:store_launch).with(store, person, release_data).and_return(double({deliver: true}))
      expect(AutomatorMailer).to receive(:store_launch).with(store, person, release_data)
      StoreLaunchEmail.send_store_launch_email(store.id, person.id, releases)
    end

    it "clears the person_id key out of Redis" do
      StoreLaunchEmail.send_store_launch_email(store.id, person.id, releases)
      expect(JSON.parse($redis.get("StoreEmails:#{store.short_name}"))).to eq({})
    end
  end
end
