require "rails_helper"

describe ProductDisplay do
  describe "#display_price" do
    let(:person)  { build(:person) }
    let(:product) do
      build(:product,
        display_name: "awesome_product",
        country_website_id: person.country_website_id,
        price: 7.99
      )
    end
    let(:targeted_product) do
      build(:targeted_product,
        product: product,
        price_adjustment_type: "percentage_off",
        price_adjustment: 20
      )
    end

    subject { ProductDisplay.new(person, product).display_price }

    it "should display the price in the product's currency" do
      uk_id = CountryWebsite.find_by(country: 'UK').id

      product = build(:product,
            display_name: "awesome_product",
            country_website_id: uk_id,
            currency: 'GBP',
            price: 7.99
      )
      dis_price = ProductDisplay.new(person, product).display_price

      expect(dis_price.currency).to eq('GBP')
    end

    context "when a person does not have a targeted_product for the given product" do
      it "returns the original price" do
        expect(subject).to eq Money.new(7_99)
      end
    end

    context "when a person has a targeted_product for the given product" do
      before do
        allow(TargetedProduct)
          .to receive(:targeted_product_for_active_offer)
          .and_return(targeted_product)
      end

      it "calculates the discounted price" do
        expect(subject).to eq Money.new(6_40)
      end
    end

    context "when a targeted_product is free" do
      let(:targeted_product) do
        build(:targeted_product,
          product: product,
          price_adjustment_type: "override",
          price_adjustment: 0,
          show_discount_price: false
        )
      end

      before do
        allow(TargetedProduct)
          .to receive(:targeted_product_for_active_offer)
          .and_return(targeted_product)
      end

      it "sets the price to the products base price" do
        expect(subject).to eq Money.new(7_99)
      end
    end

    context "when a targeted_product is free" do
      let(:targeted_product) do
        build(:targeted_product,
          product: product,
          price_adjustment_type: "override",
          price_adjustment: 0,
          show_discount_price: false
        )
      end

      before do
        allow(TargetedProduct)
          .to receive(:targeted_product_for_active_offer)
          .and_return(targeted_product)
      end

      it "should set the price to the products base price" do
        expect(subject).to eq Money.new(7_99)
      end
    end

    context "when a targeted_product is free" do
      let(:targeted_product) do
        build(:targeted_product, product: product, price_adjustment_type: "override", price_adjustment: 0, flag_text: "Flag Text", show_discount_price: false)
      end

      it "should set the price to the products base price" do
        allow(TargetedProduct)
          .to receive(:targeted_product_for_active_offer)
          .and_return(targeted_product)

        expect(subject).to eq Money.new(7_99)
      end
    end
  end

  describe "#price_adj_description" do
    let(:person)  { build(:person) }
    let(:product) do
      build(:product,
        display_name: "awesome_product",
        country_website_id: person.country_website_id,
        price: 7.99
      )
    end
    let(:targeted_product) do
      build(:targeted_product,
        product: product,
        price_adjustment_type: "override",
        price_adjustment: 0,
        flag_text: "Flag Text",
        price_adjustment_description: "Awesome Deal",
        show_discount_price: false
      )
    end

    subject { ProductDisplay.new(person, product).price_adj_description }

    before do
      allow(TargetedProduct)
        .to receive(:targeted_product_for_active_offer)
        .and_return(targeted_product)
    end

    context "when a targeted_product has discount descriptions" do
      it "sets the price_adj_description" do
        expect(subject).to eq "Awesome Deal"
      end
    end
  end

  describe "#flag_text" do
    let(:person)  { build(:person) }
    let(:product) do
      build(:product,
        display_name: "awesome_product",
        country_website_id: person.country_website_id,
        price: 7.99
      )
    end
    let(:targeted_product) do
      build(:targeted_product,
        product: product,
        price_adjustment_type: "override",
        price_adjustment: 0,
        flag_text: "Flag Text",
        show_discount_price: false
      )
    end

    before do
      allow(TargetedProduct)
        .to receive(:targeted_product_for_active_offer)
        .and_return(targeted_product)
    end

    subject { ProductDisplay.new(person, product).flag_text }

    context "when a targeted_product has discount descriptions" do
      it "sets the flag_text" do
        expect(subject).to eq "Flag Text"
      end
    end
  end

  describe "#targeted_product" do
    let(:person)  { build(:person) }
    let(:product) do
      build(:product,
        display_name: "awesome_product",
        country_website_id: person.country_website_id,
        price: 7.99
      )
    end
    let(:targeted_product) do
      build(:targeted_product,
        product: product,
        price_adjustment_type: "override",
        price_adjustment: 0,
        flag_text: "Flag Text",
        show_discount_price: false
      )
    end

    subject { ProductDisplay.new(person, product).targeted_product }

    before do
      allow(TargetedProduct)
        .to receive(:targeted_product_for_active_offer)
        .and_return(targeted_product)
    end

    it "finds the targeted_product" do
      expect(subject).to eq targeted_product
    end
  end

  describe "#to_hash" do
    let(:person)  { build(:person) }
    let(:product) do
      build(:product,
        display_name: "awesome_product",
        country_website_id: person.country_website_id,
        price: 7.99
      )
    end
    let(:expected_hash) do
      {
        full_price: "7.99",
        discount_price: "6.40",
        show_discount_price: true,
        flag_text: "Flag Text",
        price_adj_description: "Best Deal"
      }
    end
    let(:targeted_product) do
      build(:targeted_product,
        product: product,
        price_adjustment_type: "percentage_off",
        price_adjustment: 20,
        flag_text: "Flag Text",
        show_discount_price: true
      )
    end

    before do
      allow(TargetedProduct)
        .to receive(:targeted_product_for_active_offer)
        .and_return(targeted_product)
    end

    subject { ProductDisplay.new(person, product).to_hash }

    it "returns a hash of the price and discount descriptions" do
      expect(subject).to eq expected_hash
    end
  end
end
