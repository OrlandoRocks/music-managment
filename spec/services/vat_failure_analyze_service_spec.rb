require "rails_helper"

RSpec.describe VatFailureAnalyzeService, type: :service do
  let!(:succeeded) { create_list(:purchase_tax_information, 10, :with_vat) }
  let!(:failed) { create_list(:purchase_tax_information, 2, :failed) }
  let!(:failure_handled) { create_list(:purchase_tax_information, 5, :failure_handled) }

  let(:subject) { VatFailureAnalyzeService.new(Time.zone.today, Time.zone.today) }

  describe "#succeeded_calls_count" do
    it "returns succeeded calls" do
      expect(subject.succeeded_calls_count).to eq(succeeded.count)
    end
  end

  describe "#failure_handled_calls_count" do
    it "returns failure handled calls count" do
      expect(subject.failure_handled_calls_count).to eq(failure_handled.count)
    end
  end

  describe "#total_calls_count" do
    it "returns total calls count" do
      expect(subject.total_calls_count).to eq(PurchaseTaxInformation.count)
    end
  end

  describe "#failed_calls_count" do
    it "returns failed calls count" do
      expect(subject.failed_calls_count).to eq(failed.count)
    end
  end

  describe "#failure_handled_calls_ratio" do
    it "returns  ratio of failure handled calls count to total calls count" do
      expect(subject.failure_handled_calls_ratio)
        .to eq(failure_handled.count.fdiv(PurchaseTaxInformation.count).round(4))
    end
  end

  describe "#failed_calls_ratio" do
    it "returns  ratio of failed calls count to total calls count" do
      expect(subject.failed_calls_ratio)
        .to eq(failed.count.fdiv(PurchaseTaxInformation.count).round(4))
    end
  end
end
