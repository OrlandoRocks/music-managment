require 'rails_helper'

describe Currencylayer::ExchangeService do
  describe "#store_exchange_rates" do
    it "should store the exchange rates in the foreign exchange rates table" do
      timestamp = 1608618126
      rate = 1.57414
      exchange_json = {
        "success" => "true",
        "timestamp" => timestamp,
        "quotes" => {"EURCAD" => rate}
      }
      expect_any_instance_of(Currencylayer::ApiClient)
        .to receive(:get_forex)
        .with("EUR","CAD")
        .and_return(exchange_json)

      expect {
        service = Currencylayer::ExchangeService.new
        service.store_exchange_rates("EUR", "CAD")
      }.to change(ForeignExchangeRate, :count).by(1)
      
      new_foreign_exchange_rate = ForeignExchangeRate.last
      expect(new_foreign_exchange_rate.valid_from).to eq(Time.at(timestamp))
      expect(new_foreign_exchange_rate.source_currency).to eq("EUR")
      expect(new_foreign_exchange_rate.target_currency).to eq("CAD")
      expect(new_foreign_exchange_rate.exchange_rate).to eq(rate)
    end

    it "should store all the exchange rates" do
      timestamp = 1608618126
      cad_rate = 1.57414
      gbp_rate = 0.91122
      exchange_json = {
        "success" => true,
        "timestamp" => timestamp,
        "quotes" => {"EURCAD" => cad_rate, "EURGBP" => gbp_rate}
      }
      allow_any_instance_of(Currencylayer::ApiClient)
        .to receive(:get_forex)
        .with("EUR", "CAD,GBP")
        .and_return(exchange_json)

      expect {
        service = Currencylayer::ExchangeService.new
        service.store_exchange_rates("EUR", "CAD,GBP")
      }.to change(ForeignExchangeRate, :count).by(2)
  
      new_foreign_exchange_rates = ForeignExchangeRate.last(2)
      expect(new_foreign_exchange_rates[0].valid_from).to eq(Time.at(timestamp))
      expect(new_foreign_exchange_rates[0].source_currency).to eq("EUR")
      expect(new_foreign_exchange_rates[0].target_currency).to eq("CAD")
      expect(new_foreign_exchange_rates[0].exchange_rate).to eq(cad_rate)
      expect(new_foreign_exchange_rates[1].valid_from).to eq(Time.at(timestamp))
      expect(new_foreign_exchange_rates[1].source_currency).to eq("EUR")
      expect(new_foreign_exchange_rates[1].target_currency).to eq("GBP")
      expect(new_foreign_exchange_rates[1].exchange_rate).to eq(gbp_rate)
    end

    it "should raise airbrake error when the response is not success" do
      exchange_json = {
        "success" => false
      }
      allow_any_instance_of(Currencylayer::ApiClient)
        .to receive(:get_forex)
        .and_return(exchange_json)
      expect(Tunecore::Airbrake).to receive(:notify)
      expect {
        service = Currencylayer::ExchangeService.new
        service.store_exchange_rates("EUR", ["CAD","GBP"])
      }.to change(ForeignExchangeRate, :count).by(0)
    end
  end
end
