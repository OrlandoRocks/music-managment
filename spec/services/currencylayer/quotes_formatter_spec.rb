require "rails_helper"

RSpec.describe Currencylayer::QuotesFormatter, type: :service do
  describe ".call" do
    it "formats quotes returned by currencylayer API" do
      source_currency = "USD"
      target_currencies = ["AED", "AFN", "ALL"]
      quotes = {
        "USDAED" => 3.67315,
        "USDAFN" => 60.790001,
        "USDALL" => 126.194504,
      }

      subject = described_class.call!(quotes, source_currency, target_currencies)

      expected_result = [
        { source_currency: "USD", target_currency: "AED", exchange_rate: 3.67315 },
        { source_currency: "USD", target_currency: "AFN", exchange_rate: 60.790001 },
        { source_currency: "USD", target_currency: "ALL", exchange_rate: 126.194504 },
      ]

      expect(subject).to eq(expected_result)
    end

    it "formates quotes for single target currency" do
      source_currency = "USD"
      target_currencies = "AED"
      quotes = {
        "USDAED" => 3.67315
      }

      subject = described_class.call!(quotes, source_currency, target_currencies)

      expected_result = [
        { source_currency: "USD", target_currency: "AED", exchange_rate: 3.67315 }
      ]

      expect(subject).to eq(expected_result)
    end

    it "raises error when there is discripancy in quotes and given currency" do
      source_currency = "USD"
      target_currencies = ["INR", "AED"]
      quotes = {
        "USDINS" => 83.67315,
        "USDAED" => 3.67315
      }

      expect { described_class.new(quotes, source_currency, target_currencies) }
        .to raise_error Currencylayer::UnknownQuotesFormatError
    end
  end

  describe ".new" do
    it "raises error when there is discripancy in quotes and given currency" do
      source_currency = "USD"
      target_currencies = ["INR", "AED"]
      quotes = {
        "USDINS" => 83.67315,
        "USDAED" => 3.67315
      }

      expect { described_class.new(quotes, source_currency, target_currencies) }
        .to raise_error Currencylayer::UnknownQuotesFormatError
    end
  end

  describe "#exchange_rates" do
    it "formats quotes returned by currencylayer API" do
      source_currency = "USD"
      target_currencies = ["AED", "AFN", "ALL"]
      quotes = {
        "USDAED" => 3.67315,
        "USDAFN" => 60.790001,
        "USDALL" => 126.194504,
      }

      subject = described_class
                .new(quotes, source_currency, target_currencies)
                .exchange_rates

      expected_result = [
        { source_currency: "USD", target_currency: "AED", exchange_rate: 3.67315 },
        { source_currency: "USD", target_currency: "AFN", exchange_rate: 60.790001 },
        { source_currency: "USD", target_currency: "ALL", exchange_rate: 126.194504 },
      ]

      expect(subject).to eq(expected_result)
    end

    it "formates quotes for single target currency" do
      source_currency = "USD"
      target_currencies = "AED"
      quotes = {
        "USDAED" => 3.67315
      }

      subject = described_class
                .new(quotes, source_currency, target_currencies)
                .exchange_rates

      expected_result = [
        { source_currency: "USD", target_currency: "AED", exchange_rate: 3.67315 }
      ]

      expect(subject).to eq(expected_result)
    end
  end
end
