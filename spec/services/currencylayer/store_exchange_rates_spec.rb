require "rails_helper"

RSpec.describe Currencylayer::StoreExchangeRates, type: :service do
  describe ".call!" do
    it "stores exchangerates from currencylayer response to database" do
      timestamp = 1646677800
      date = Time.at(timestamp).to_datetime

      instance = instance_double(
        Currencylayer::Responses::Base,
        quotes: { "GBPUSD": 1.76},
        timestamp: timestamp
      )
      expect(Currencylayer::HistoricRateFetcher)
        .to receive(:fetch!) { instance }

      described_class.call!(
        Currencylayer::HistoricRateFetcher,
        source_currency: "GBP",
        target_currencies: "USD",
        date: date
        )

      expect(
        ForeignExchangeRate.for_date(source: "GBP", target: "USD", date: date)
      ).to_not be_nil
    end

    it "raises InvalidRate Error" do
      timestamp = 1646677800
      date = Time.at(timestamp).to_datetime

      instance = instance_double(
        Currencylayer::Responses::Base,
        quotes: { "GBPINR": 1.76},
        timestamp: timestamp
      )
      expect(Currencylayer::HistoricRateFetcher)
        .to receive(:fetch!) { instance }

      expect{ described_class.call!(
        Currencylayer::HistoricRateFetcher,
        source_currency: "GBP",
        target_currencies: "USD",
        date: date
        )
      }.to raise_error(Currencylayer::StoreExchangeRates::InvalidRateError)
    end
  end
end
