require "rails_helper"

RSpec.describe Currencylayer::HistoricRateFetcher, type: :service do
  describe "#response" do
    context "success response from currencylayer" do
      it "returns response" do
        options = {
          date: Time.zone.today,
          source_currency: "USD",
          target_currencies: ["GBP"]
        }

        response = double(
          "Faraday response", status: 200, body: {
            success: true,
            source: "USD",
            timestamp: Time.now.getutc,
            quotes: {
              USDGBP: 0.76
            },
            date: Time.zone.today,
            historical: true
          }.to_json
        )

        expected_response = Currencylayer::Responses::Historical.new(response)

        allow_any_instance_of(Currencylayer::ApiClient)
          .to receive(:send_request)
          .with(:historical, options)
          .and_return(expected_response)

        subject = described_class.new(options)

        expect(subject.response).to eq(expected_response)
      end
    end

    context "failed response from currencylayer" do
      it "raises CallFailedError" do
        options = {
          date: Time.zone.today,
          source_currency: "USD",
          target_currencies: ["GBP"]
        }

        response = double(
          "Faraday response", status: 200, body: {
            success: false,
            error: {
              code: 201,
              info: "Error"
            }
          }.to_json
        )

        expected_response = Currencylayer::Responses::Historical.new(response)

        allow_any_instance_of(Currencylayer::ApiClient)
          .to receive(:send_request)
          .with(:historical, options)
          .and_return(expected_response)

        subject = described_class.new(options)

        expect { subject.response }
          .to raise_error(
            Currencylayer::CallFailedError,
            Currencylayer::CallFailedError::DEFAULT_ERR_MSG
          )
      end
    end
  end
end
