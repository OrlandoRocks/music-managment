require "rails_helper"

describe SuspiciousTransferDeterminationService do
  let(:paypal_transfer)    {build(:paypal_transfer)}
  describe ".transfer_suspicious?" do
    context "rules engine indicates suspicion" do
      before do
        allow(BlacklistedPaypalPayee::SuspiciousTransferRulesService).to receive(:suspicious?).and_return(true)
      end
      it "returns true" do
        expect(SuspiciousTransferDeterminationService.transfer_suspicious?(paypal_transfer)).to eq true
      end
    end
    context "transfer's payee is blacklisted" do
      before do
        create(:blacklisted_paypal_payee, payee: paypal_transfer.paypal_address)
      end
      it "returns true" do
        expect(SuspiciousTransferDeterminationService.transfer_suspicious?(paypal_transfer)).to eq true
      end
    end
    context "it is not suspicious" do
      before do
        allow(BlacklistedPaypalPayee::SuspiciousTransferRulesService).to receive(:suspicious?).and_return(false)
      end
      it "returns false" do
        expect(SuspiciousTransferDeterminationService.transfer_suspicious?(paypal_transfer)).to eq false
      end
    end
  end
end
