require "rails_helper"

describe InvoiceGenerator::UploadInboundInvoicePdf do
  describe 'upload invoices in bulk to s3' do
    before(:each) do
      @person = TCFactory.build_person(
        name: 'john smith',
        email: 'john@example.net',
        country: 'US'
      )
      album = TCFactory.build_album(person: @person)
      purchase = Product.add_to_cart(album.person, album)
      @invoice = Invoice.factory(@person, purchase)
      @invoice.update(settled_at: Time.now)
    end

    it 'should create and upload pdf to s3' do
      allow(FeatureFlipper).to receive(:show_feature?).with(:vat_tax, @person).and_return(true)
      upload_invoice_service = InvoiceGenerator::UploadInboundInvoicePdf.new(@invoice)
      allow(upload_invoice_service).to receive(:upload_invoice).and_return(true)
      expect(upload_invoice_service.process).to eq(true)
    end
  end
end
