require "rails_helper"

PAYOUT_TRANSFER = 'PayoutTransfer'.freeze

describe InvoiceGenerator::UploadOutboundInvoicePdf do
  let(:person) { FactoryBot.create(:person, :with_tc_social, country: "Luxembourg") }
  let(:payout_transfer) { FactoryBot.create(:payout_transfer, person: person) }
  let(:vat_tax_adjustment) do
    FactoryBot.create(:vat_tax_adjustment,
                      person: person,
                      amount: payout_transfer.amount_cents * 17 / 100,
                      related_id: payout_transfer.id,
                      related_type: PAYOUT_TRANSFER)
  end
  let(:invoice) do
    OutboundInvoice.create(related: payout_transfer,
                           person: person,
                           invoice_sequence: person.last_generated_outbound_sequence + 1,
                           user_invoice_prefix: person.outbound_invoice_prefix,
                           invoice_date: Date.today,
                           vat_tax_adjustment: vat_tax_adjustment)
  end

  it 'should create and upload pdf to s3' do
    allow(FeatureFlipper).to receive(:show_feature?).with(:vat_tax, person).and_return(true)
    allow(FeatureFlipper).to receive(:show_feature?).with(:auto_approved_withdrawals, person.id).and_return(false)
    upload_invoice_service = InvoiceGenerator::UploadOutboundInvoicePdf.new(invoice)
    allow(upload_invoice_service).to receive(:upload_invoice).and_return(true)
    expect(upload_invoice_service.process).to eq(true)
  end
end
