require "rails_helper"

PAYOUT_TRANSFER = 'PayoutTransfer'.freeze

describe InvoiceGenerator::GenerateOutboundData do
  let(:person) { FactoryBot.create(:person, :with_tc_social, country: "Luxembourg") }
  let(:payout_transfer) { FactoryBot.create(:payout_transfer, person: person) }
  let(:vat_tax_adjustment) do
    FactoryBot.create(:vat_tax_adjustment,
                      person: person,
                      amount: payout_transfer.amount_cents * 17 / 100,
                      related_id: payout_transfer.id,
                      related_type: PAYOUT_TRANSFER)
  end
  let(:invoice) do
    OutboundInvoice.create(related: payout_transfer,
                           person: person,
                           invoice_sequence: person.last_generated_outbound_sequence + 1,
                           user_invoice_prefix: person.outbound_invoice_prefix,
                           invoice_date: Date.today,
                           vat_tax_adjustment: vat_tax_adjustment)
  end

  let(:response) { InvoiceGenerator::GenerateOutboundData.new(invoice).invoice_data }

  it 'return hash of outbound data' do
    expect(response[:currency]).to eq(payout_transfer.currency)
    expect(response[:date]).to eq(invoice.invoice_date)
  end

  it 'return person details' do
    expect(response[:person_data][:name]).to eq(person.name)
    expect(response[:person_data][:address1]).to eq(person.address1)
    expect(response[:person_data][:address2]).to eq(person.address2)
    expect(response[:person_data][:city]).to eq(person.city)
    expect(response[:person_data][:state]).to eq(person.state)
    expect(response[:person_data][:customer_type]).to eq('individual')
    expect(response[:person_data][:vat_registration_number]).to eq(vat_tax_adjustment.vat_registration_number)
    expect(response[:person_data][:country]).to eq(person.country)
  end

  it 'return purchase details' do
    expect(response[:items].first[:id]).to eq(payout_transfer.id)
    expect(response[:items].first[:item_name]).to eq(custom_t('invoices.royalty_invoices.invoice_item_bi'))
    expect(response[:items].first[:currency]).to eq(payout_transfer.currency)
    expect(response[:items].first[:tax_rate]).to eq(vat_tax_adjustment.tax_rate)
    expect(response[:items].first[:place_of_supply]).to eq(vat_tax_adjustment.place_of_supply)
    expect(response[:items].first[:tax_type]).to eq(vat_tax_adjustment.tax_type)
  end
end
