require "rails_helper"

describe InvoiceGenerator::EmailNotification do
  describe 'Invoice email notifification for Tunecore Customer' do
    before(:each) do
      us_person = TCFactory.build_person(
        name: 'john smith',
        email: 'john@example.net',
        country: 'US'
      )
      us_album = TCFactory.build_album(person: us_person)
      us_purchase = Product.add_to_cart(us_album.person, us_album)
      @us_invoice = Invoice.factory(us_person, us_purchase)
      allow(FeatureFlipper).to receive(:show_feature?).with(:vat_tax, us_person).and_return(true)
      @us_invoice.update(settled_at: Time.zone.now)
    end

    it 'should update invoice sent at after the process' do
      InvoiceGenerator::EmailNotification.new(@us_invoice).perform!
      @us_invoice.reload
      expect(@us_invoice.invoice_sent_at).to be_present
    end

    it 'should not update the invoice number' do
      invoice_number = @us_invoice.invoice_number
      InvoiceGenerator::EmailNotification.new(@us_invoice).perform!
      @us_invoice.reload
      expect(@us_invoice.invoice_number).to eq(invoice_number)
    end
  end

  describe 'Invoice email notifification for  Believe International Customer' do
    before(:each) do
      bi_person = TCFactory.build_person(
        name: 'john smith',
        email: 'john@example.net',
        country: 'Austria'
      )
      bi_album = TCFactory.build_album(person: bi_person)
      bi_purchase = Product.add_to_cart(album.bi_person, album)
      @bi_invoice = Invoice.factory(bi_person, purchase)
      allow(FeatureFlipper).to receive(:show_feature?).with(:vat_tax, bi_person).and_return(true)
      @bi_invoice.update(settled_at: Time.zone.now)

      it 'should update invoice sent at after the process' do
        InvoiceGenerator::EmailNotification.new(@bi_invoice).perform!
        @bi_invoice.reload

        expect(@bi_invoice.invoice_sent_at).to be_present
        expect(@bi_invoice.invoice_number).to be_present
      end

      it 'should not update the invoice number' do
        invoice_number = @bi_invoice.invoice_number
        InvoiceGenerator::EmailNotification.new(@bi_invoice).perform!
        @bi_invoice.reload
        expect(@bi_invoice.invoice_number).to eq(invoice_number)
      end
    end
  end
end
