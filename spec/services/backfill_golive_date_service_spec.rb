require 'rails_helper'

describe BackfillGoliveDateService do
  describe '#' do
    let(:album) {
      FactoryBot.create(
        :album,
        :finalized,
        golive_date: nil,
      )
    }

    it '.run' do
      subject.run_on_album(album)
      expect(album.golive_date).not_to be_nil
    end
  end
end
