require "rails_helper"

describe RejectionEmailService do
  describe "#release_rejected" do
    let(:review_audit) { build(:review_audit, :rejected) }
    let(:review_reason) { review_audit.review_reasons.first }

    context "when the review audit does not have an email template" do
      it "should raise an Airbrake" do
        review_reason.update(template_type: nil)
        review_audit.review_reasons << review_reason

        error_msg = "error delivering release rejected email for "\
          "album_id=#{review_audit.album.id}. No template type was found"
        expect(Airbrake).to receive(:notify).with(error_msg, album: review_audit.album)

        RejectionEmailService.new(review_audit).notify
      end
    end

    context "when the review audit has review reasons from more than one template" do
      it "should raise an Airbrake" do
        album = review_audit.album
        review_reason.update(template_type: "changes_requested")

        review_reason2 = create(:review_reason, template_type: "rights_verification")
        review_audit.review_reasons << review_reason2
        review_audit.save(validate: false)

        error_msg = "Unable to send rejection email for Album ID: #{album}\n" \
          "Review Audit contains Review Reasons from more than one template"
        expect(Airbrake).to receive(:notify).with(error_msg, album: review_audit.album)

        RejectionEmailService.new(review_audit).notify
      end
    end

    context "when the review audit has a changes requested template_type" do
      it "should call the RejectionEmailMailer with the changes_requested template" do
        review_reason.update(
          email_template_path: "denied_translations",
          template_type: "changes_requested"
        )
        rejection_email_dbl = double(RejectionEmailMailer, deliver: true)

        expect(RejectionEmailMailer)
          .to receive(:send).with(:changes_requested, review_audit)
          .and_return(rejection_email_dbl)

        RejectionEmailService.new(review_audit).notify
      end
    end

    context "when the review audit has a rights verification template_type" do
      it "should call the RejectionEmailMailer with the rights_verification template" do
        review_reason.update(
          email_template_path: "denied_album_unauthorized_remix",
          template_type: "rights_verification"
        )
        rejection_email_dbl = double(RejectionEmailMailer, deliver: true)

        expect(RejectionEmailMailer)
          .to receive(:send).with(:rights_verification, review_audit)
          .and_return(rejection_email_dbl)

        RejectionEmailService.new(review_audit).notify
      end
    end

    context "when the review audit has a will_not_distribute template_type" do
      it "should call the RejectionEmailMailer with the email template path" do
        review_reason.update(
          email_template_path: "denied_streaming_fraud",
          template_type: "will_not_distribute"
        )
        rejection_email_dbl = double(RejectionEmailMailer, deliver: true)

        expect(RejectionEmailMailer)
          .to receive(:send).with(:denied_streaming_fraud, review_audit)
          .and_return(rejection_email_dbl)

        RejectionEmailService.new(review_audit).notify
      end
    end

    context "when the review audit has multiple review reasons" do
      it "should call the RejectionEmailMailer with the email_template_path" do
        review_reason.update(
          template_type: "changes_requested"
        )
        review_audit.review_reasons << create(:review_reason, template_type: "changes_requested")
        rejection_email_dbl = double(RejectionEmailMailer, deliver: true)

        expect(RejectionEmailMailer)
          .to receive(:send).with(:changes_requested, review_audit)
          .and_return(rejection_email_dbl)

        RejectionEmailService.new(review_audit).notify
      end
    end

    context "when the review audit has multiple review reasons and " \
      "has a template type of 'will_not_distribute'" do
      it "should call the RejectionEmailMailer mulitple times" do
        review_reason.update(
          template_type: "will_not_distribute",
          email_template_path: "denied_album_unsupported_language"
        )
        review_audit.review_reasons << create(:review_reason, {
          template_type: "will_not_distribute",
          email_template_path: "denied_ringtone_submitted_textone"
        })
        rejection_email_dbl = double(RejectionEmailMailer, deliver: true)

        review_audit.review_reasons.each do |review_reason|
          expect(RejectionEmailMailer)
            .to receive(:send).with(review_reason.email_template_path.to_sym, review_audit)
            .and_return(rejection_email_dbl)
        end

        RejectionEmailService.new(review_audit).notify
      end
    end
  end
end
