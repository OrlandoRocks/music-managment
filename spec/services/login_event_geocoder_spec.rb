require "rails_helper"

describe LoginEventGeocoder do
  describe ".geocode" do
    let (:person) { create(:person) }

    it "geocodes a login event" do
      login_event = create(:login_event, person: person, ip_address: "100.37.214.12")
      response = {
        city:  "Brooklyn",
        subdivision: "New York",
        country: "United States",
        latitude: 40.6501,
        longitude: -73.9496
      }

      allow(IpAddressGeocoder).to receive(:geocoded_attributes).and_return(response)
      LoginEventGeocoder.geocode(login_event.id)
      expect(login_event.reload.city).to eq("Brooklyn")
    end
  end
end
