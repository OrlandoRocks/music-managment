# frozen_string_literal: true

require "rails_helper"

describe Admin::PaypalMasspayService do
  describe "#call" do
    let!(:admin) {Person.joins(:roles).find_by(roles: {name: "Admin"})}
    let!(:canadian_person) {create(:person, country: 'CA')}
    let!(:canadian_transfer) {create(:paypal_transfer, person: canadian_person)}
    let!(:canadian_country_website) {CountryWebsite.find(CountryWebsite::CANADA)}

    before(:each) do
      allow_any_instance_of(PayPalBusiness::API)
        .to receive(:mass_pay)
        .and_return(double(:response, ack: "Success", correlationID: "1234", timestamp: Time.now.utc.iso8601))
    end

    context "without any processing transactions" do
      context "only invalid transactions" do
        it "returns false with no valid payments message" do
          allow(canadian_transfer).to receive(:valid?).and_return(false)
          messages = []
          messages << "Warning: transfer ##{canadian_transfer.id} is not valid! Skipping for....... Country: #{canadian_country_website.country}. Corporate Entity Id: #{canadian_person.corporate_entity_id}"
          messages << Admin::PaypalMasspayService::NO_VALID_PAYMENTS
          result = Admin::PaypalMasspayService.call([canadian_transfer], canadian_country_website, admin.id)

          expect(result).to eq([false, messages])
        end
      end
      context "only not-pending transactions" do
        it "returns false with no valid payments message" do
          canadian_transfer.tap do |transfer|
            transfer.mark_as_processing!
            transfer.mark_as_completed!
            transfer.reload
          end
          
          messages = []
          messages << "Transfer ##{canadian_transfer.id} is no longer 'pending', cannot change to 'processing' Country: #{canadian_country_website.country}. Corporate Entity Id: #{canadian_person.corporate_entity_id}"
          messages << Admin::PaypalMasspayService::NO_VALID_PAYMENTS
          result = Admin::PaypalMasspayService.call([canadian_transfer], canadian_country_website, admin.id)

          expect(result).to eq([false, messages])
        end
      end
    end
    context "with processing transactions" do
      it "processes the transactions" do
        expect(Admin::PaypalMasspayService.call([canadian_transfer], canadian_country_website, admin.id))
          .to eq([true, []])
        expect(canadian_transfer.reload.transfer_status).to eq(PaypalTransfer::COMPLETED_STATUS)
      end

      it "should create outbound invoice for vat applicable users'" do
        allow_any_instance_of(TcVat::OutboundVatCalculator).to receive(:fetch_vat_info).and_return({ tax_rate: '17.0' })
        allow_any_instance_of(Person).to receive(:vat_applicable?).and_return(true)

        Admin::PaypalMasspayService.call([canadian_transfer], canadian_country_website, admin.id)
        expect(canadian_transfer.outbound_invoice).to be_present
        expect(canadian_transfer.vat_tax_adjustment).to be_present
        expect(canadian_transfer.vat_tax_adjustment.person_transactions.count).to eql(2)
      end
    end
  end

  describe "#paypal_payments_groups" do
    let!(:admin) {Person.joins(:roles).find_by(roles: {name: "Admin"})}
    let!(:person) {create(:person, country: 'CA')}
    let!(:canadian_person) {create(:person, country: 'CA')}
    let!(:canadian_transfer) {create(:paypal_transfer, person: canadian_person)}
    let!(:canadian_country_website) {CountryWebsite.find(CountryWebsite::CANADA)}
    context "no flags" do
      it "creates groups by corporate_entity_id" do
        person.update(corporate_entity: CorporateEntity.find_by_name(CorporateEntity::TUNECORE_US_NAME))
        american_transfer = create(:paypal_transfer, person: person)

        service = Admin::PaypalMasspayService
                    .new([canadian_transfer, american_transfer], canadian_country_website, admin.id)

        expected_result = {
          1 => [
            {
              email: american_transfer.paypal_address,
              amount: american_transfer.payment_amount,
              currency: american_transfer.currency,
              id: american_transfer.id
            }
          ],
          2 => [
            {
              email: canadian_transfer.paypal_address,
              amount: canadian_transfer.payment_amount,
              currency: canadian_transfer.currency,
              id: canadian_transfer.id
            }
          ]
        }
        expect(service.paypal_payments_groups).to eq(expected_result)
      end
    end
  end
end
