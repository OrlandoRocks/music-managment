require 'rails_helper'

describe Admin::PaypalMassPayHandler do
  describe "#call" do
    let!(:admin) {Person.joins(:roles).find_by(roles: {name: "Admin"})}
    let!(:canadian_person) { create(:person, country: 'CA') }
    let!(:canadian_transfer_one) { create(:paypal_transfer, person: canadian_person)}
    let!(:canadian_transfer_two) { create(:paypal_transfer, person: canadian_person)}
    let!(:american_person) do 
      create(
        :person, 
        country: 'CA', 
        corporate_entity: CorporateEntity.find_by(name: CorporateEntity::TUNECORE_US_NAME)
      )
    end

    let!(:american_transfer_one) { create(:paypal_transfer, person: american_person) }
    let!(:american_transfer_two) { create(:paypal_transfer, person: american_person) }

    let!(:all_transfers) do
      [canadian_transfer_one, canadian_transfer_two, american_transfer_one, american_transfer_two]
    end

    before(:each) do
      allow_any_instance_of(PayPalBusiness::API)
        .to receive(:mass_pay)
        .and_return(double(:response, ack: "Success", correlationID: "1234", timestamp: Time.now.utc.iso8601))
    end

    context "everything goes as expected" do
      it "processes all transfers sucessfully and marks them `completed`" do
        Sidekiq::Testing.inline! do
          expect(Admin::PaypalMassPayHandler.call(admin, all_transfers)).to eq([])
        end
        statuses = all_transfers.map(&:reload).map(&:transfer_status).uniq

        expect(statuses.size).to eq(1)
        expect(statuses.first).to eq("completed")
      end
    end
  end
end
