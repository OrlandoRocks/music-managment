# frozen_string_literal: true
require 'rails_helper'
describe Admin::PaypalPayoutService do
  describe "#call" do
    let!(:admin) {Person.joins(:roles).find_by(roles: {name: "Admin"})}
    let!(:canadian_person) {create(:person, country: 'CA')}
    let!(:canadian_transfer) {create(:paypal_transfer, person: canadian_person, transfer_status: :processing)}
    let!(:canadian_country_website) {CountryWebsite.find(CountryWebsite::CANADA)}

    before(:each) do
      allow_any_instance_of(PayPalBusiness::API)
        .to receive(:mass_pay)
        .and_return(double(:response, ack: "Success", correlationID: "1234", timestamp: Time.now.utc.iso8601))
    end

    context "without any processing transactions" do
      context "only invalid transactions" do
        it "returns :transfer_in_wrong_transfer_status with no valid payments message" do
          canadian_transfer.update(transfer_status: :pending)
          result = Admin::PaypalPayoutService.call(admin, canadian_transfer)

          expect(result).to eq(:transfer_in_wrong_transfer_status)
        end
      end
      context "only not-pending transactions" do
        it "returns :transfer_in_wrong_transfer_status with no valid payments message" do
          canadian_transfer.tap do |transfer|
            transfer.mark_as_completed!
            transfer.reload
          end
          result = Admin::PaypalPayoutService.call(admin, canadian_transfer)

          expect(result).to eq(:transfer_in_wrong_transfer_status)
        end
      end
    end
    context "with processing transactions" do
      it "processes the transactions" do
        Admin::PaypalPayoutService.call(admin, canadian_transfer)
        expect(canadian_transfer.reload.transfer_status).to eq(PaypalTransfer::COMPLETED_STATUS)
      end

      it "should create outbound invoice for vat applicable users'" do
        allow_any_instance_of(TcVat::OutboundVatCalculator).to receive(:fetch_vat_info).and_return({ tax_rate: '17.0' })
        allow_any_instance_of(Person).to receive(:vat_applicable?).and_return(true)

        Admin::PaypalPayoutService.call(admin, canadian_transfer)
        canadian_transfer.reload
        expect(canadian_transfer.outbound_invoice).to be_present
      end
    end
  end
end
