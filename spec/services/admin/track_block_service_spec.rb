# frozen_string_literal: true
require 'rails_helper'
describe Admin::TrackBlockService do 
  context "with approved album" do 
    let(:person) { create(:person) }
    let!(:album)  { create(:album, 
      :paid, 
      :with_uploaded_songs, 
      :with_salepoints, 
      :with_petri_bundle,
      number_of_songs: 2,
      person: person,
      legal_review_state: "APPROVED") 
    }

    let(:song) { album.songs.first }

    let(:store) { Store.find_by(short_name: 'SCblock')}

    let(:params) { {
      person: "Admin Person",
      song_id: song.id,
      store_id: store.id
     }
    }
    
    subject(:sc_service) { described_class.new(params) }

    context "when no track monetization exists" do 
      it "creates a track monetization for soundcloud" do 
        expect { subject.create_track_monetization }.to change { TrackMonetization.count }.by(1)
      end

      it "delivers a track monetization for sound cloud" do 

        expect_any_instance_of(TrackMonetization).to receive(:start).with({actor: "Admin Person", message: "Starting Delivery To #{store.name}"})
        subject.create_track_monetization
      end 
    end 

    context "when Sound Cloud track monetization exists" do 
      before do 
        create(:track_monetization, 
          song_id: song.id,
           store_id: Store.find_by(short_name: "SCblock").id, 
           person_id: person.id,
           eligibility_status: "Eligible"
          )
      end 
      it 'enqueues the track monetization' do
        expect_any_instance_of(TrackMonetization).to receive(:start).with({actor: "Admin Person", message: "Starting Delivery To #{store.name}"})
        subject.create_track_monetization
      end
    end 
  end 
end
