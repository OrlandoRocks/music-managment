# frozen_string_literal: true

require "rails_helper"

describe Admin::PaymentService do
  describe "#fetch_transfers" do
    let!(:person) { create(:person, :with_balance, amount: 50_000) }
    let!(:low_value_transfer) { create(:paypal_transfer, person: person, payment_cents: 500) }
    let!(:high_value_transfer) { create(:paypal_transfer, person: person, payment_cents: 10_000) }

    context "filter 'type' is 'paypal'" do
      context "when 'threshold_status' filter is set to 'over'" do
        it "fetches transfers with amount greater than the threshold value" do
          allow(PaypalTransfer).to receive(:threshold_in_cents).and_return(5_000)
          response = Admin::PaymentService.fetch_transfers({ type: "paypal", threshold_status: "over" })
          expect(response[:transfers]).to include high_value_transfer
          expect(response[:transfers]).not_to include low_value_transfer
        end
      end

      context "when threshold_status filter is set to 'under'" do
        it "fetches transfers with amount less than the threshold value" do
          allow(PaypalTransfer).to receive(:threshold_in_cents).and_return(5_000)
          response = Admin::PaymentService.fetch_transfers({ type: "paypal", threshold_status: "under" })
          expect(response[:transfers]).to include low_value_transfer
          expect(response[:transfers]).not_to include high_value_transfer
        end
      end
    end

    context "filter 'type' is 'check'" do
      let!(:pending_check_transfer) { create(:manual_check_transfer, transfer_status: "pending") }
      let!(:processing_check_transfer) { create(:manual_check_transfer, transfer_status: "processing") }
      let!(:completed_check_transfer) { create(:manual_check_transfer, transfer_status: "completed") }
      let!(:cancelled_check_transfer) { create(:manual_check_transfer, transfer_status: "cancelled") }

      it "fetches check transfers filtered by status" do
        statuses = ["pending", "processing", "completed", "cancelled"]
        statuses.each do |status|
          response = Admin::PaymentService.fetch_transfers({ type: "check", status: status })
          expect(response[:transfers]).to include public_send("#{status}_check_transfer")

          statuses.reject { |s| s == status }.each do |st|
            expect(response[:transfers]).not_to include public_send("#{st}_check_transfer")
          end
        end
      end
    end

    it "returns error for invalid 'type'" do
      response = Admin::PaymentService.fetch_transfers({ type: "cash" })
      expect(response[:error]).to eq("There is no type of cash")
    end
  end
end
