require "rails_helper"

describe Admin::CopyrightStatusUpdaterService do
  describe '.update' do
    let(:admin) { create(:person, :admin) }

    it "creates a copyright state transition with the correct states" do
      copyright_status = create(:copyright_status, status: "pending")
      params = { status: "approved", note: { note: 'My note' } }

      expect {
        Admin::CopyrightStatusUpdaterService.update(copyright_status, admin, params)
      }.to change {
        copyright_status.copyright_state_transitions.count
      }.by(1)

      copyright_state_transition = copyright_status.copyright_state_transitions.last
      expect(copyright_state_transition.prev_state).to eq "pending"
      expect(copyright_state_transition.state).to eq "approved"
    end

    it "creates a note" do
      copyright_status = create(:copyright_status, status: "pending")
      params = { status: "approved", note: { note: 'My note' } }

      expect {
        Admin::CopyrightStatusUpdaterService.update(copyright_status, admin, params)
      }.to change {
        copyright_status.related.notes.count
      }.by(1)

      note = copyright_status.related.notes.last
      expect(note.subject).to eq "Legal Review Status Updated to 'approved'"
      expect(note.note_created_by_id).to eq admin.id
      expect(note.note).to eq params[:note][:note]
    end

    it "updates the copyright status" do
      copyright_status = create(:copyright_status, status: "pending")
      params = { status: "approved", note: { note: 'My note' } }

      expect {
        Admin::CopyrightStatusUpdaterService.update(copyright_status, admin, params)
      }.to change {
        copyright_status.status
      }.from("pending").to("approved")
    end

    it "adds an error when creating an invalid record" do
      copyright_status = create(:copyright_status, status: "pending")
      params = { status: "not a status", note: { note: '' } }

      copyright_status_updater = Admin::CopyrightStatusUpdaterService
        .update(copyright_status, admin, params)

      expect(copyright_status_updater.error).to be_present
    end

    it "adds an error when there's no note" do
      copyright_status = create(:copyright_status, status: "pending")
      params = { status: "approved", note: { note: '' } }

      copyright_status_updater = Admin::CopyrightStatusUpdaterService
        .update(copyright_status, admin, params)

      expect(copyright_status_updater.error).to be_present
    end

  end
end
