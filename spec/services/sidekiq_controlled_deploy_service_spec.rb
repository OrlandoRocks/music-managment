require "rails_helper"

describe SidekiqControlledDeployService do
  it "pauses the queues" do
    dbl = double(Sidekiq::Queue, pause!: true)
    allow(Sidekiq::Queue).to receive(:new).and_return(dbl)
    expect(dbl).to receive(:pause!)
    SidekiqControlledDeployService.pause!
  end

  it "writes the inflight workers to a file" do
    worker_args = [
      { queue: "delivery-default", args: ["Distribution", 1] },
      { queue: "delivery-critical", args: ["Distribution", 2] }
    ].map(&:with_indifferent_access)
    sidekiq_workers = double(Sidekiq::Workers, map: worker_args)
    allow(Sidekiq::Workers).to receive(:new).and_return(sidekiq_workers)
    SidekiqControlledDeployService.write_inflight_workers
    expect(Marshal.load(File.read(SidekiqControlledDeployService::PAUSE_WORKER_FILE_PATH)).map(&:with_indifferent_access)).to eq(worker_args)
  end

  it "reenqueues the inflight workers that were written to the file" do
    worker_args = [
      { queue: "delivery-default", args: ["Distribution", 1] },
      { queue: "delivery-critical", args: ["Distribution", 2] }
    ].map(&:with_indifferent_access)
    sidekiq_workers = double(Sidekiq::Workers, map: worker_args)
    allow(Sidekiq::Workers).to receive(:new).and_return(sidekiq_workers)
    worker = double(Delivery::DistributionWorker, perform_async: true)
    allow(Delivery::DistributionWorker).to receive(:set).and_return(worker)
    expect(worker).to receive(:perform_async)
    
    SidekiqControlledDeployService.write_inflight_workers
    SidekiqControlledDeployService.reenqueue_inflight_workers
  end

  it "unpauses the queues" do
    dbl = double(Sidekiq::Queue, unpause!: true)
    allow(Sidekiq::Queue).to receive(:new).and_return(dbl)
    expect(dbl).to receive(:unpause!)
    SidekiqControlledDeployService.unpause!
  end
end
