require "rails_helper"

describe BlacklistedPaypalPayee::SuspiciousTransferRulesService do
  let(:paypal_transfer) {create(:paypal_transfer)}
  describe ".suspicious" do
     before(:each) do
      PersonBalance.where(person_id: paypal_transfer.person_id).first.update(balance: "200.00")
    end
    context "has previously approved check transfers and no previously approved paypal" do
      before do
        check = build(:check_transfer, person_id: paypal_transfer.person_id, transfer_status: "completed")
        allow(check).to receive(:verify_password).and_return(true)
        check.save
      end
      it "returns true" do
        expect(BlacklistedPaypalPayee::SuspiciousTransferRulesService.suspicious?(paypal_transfer)).to eq(true)
      end
    end

    context "has previously approved EFT transfers and no previously approved paypal" do
      before do
        person    = paypal_transfer.person
        bank_acct = create(:stored_bank_account, person_id: person.id)
        eft       = EftBatchTransaction.new(stored_bank_account_id: bank_acct.id, amount: "100.00")
        allow(eft).to receive(:verify_password).and_return(true)
        eft.save
        eft.update(status: "success")
      end
      it "returns true" do
        expect(BlacklistedPaypalPayee::SuspiciousTransferRulesService.suspicious?(paypal_transfer)).to eq(true)
      end
    end

    context "has previously approved check transfers and previously approved paypal" do
      before do
        check  = build(:check_transfer, person_id: paypal_transfer.person_id, transfer_status: "completed")
        allow(check).to receive(:verify_password).and_return(true)
        check.save
        paypal = build(:paypal_transfer, person_id: paypal_transfer.person_id, transfer_status: "completed")
        paypal.save(validate: false)
      end
      it "returns false" do
        expect(BlacklistedPaypalPayee::SuspiciousTransferRulesService.suspicious?(paypal_transfer)).to be false
      end
    end

    context "has neither perviosly approved check nor EFT transfers" do
      it "returns false" do
        expect(BlacklistedPaypalPayee::SuspiciousTransferRulesService.suspicious?(paypal_transfer)).to be false
      end
    end
  end
end
