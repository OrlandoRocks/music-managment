require "rails_helper"

describe BlacklistedPaypalPayee::SearchService do
  before(:each) do
    create_list(:blacklisted_paypal_payee, 10)
  end
  describe ".search" do
    let(:blacklisted_paypal_payee) { BlacklistedPaypalPayee.first }
    let(:payee_address)            { blacklisted_paypal_payee.payee }
    context "by payee" do
      it "returns the correct payee" do
        results = BlacklistedPaypalPayee::SearchService.search(payee_address, nil, nil)
        expect(results.first.payee).to eq(payee_address)
      end
    end

    context "with pagination" do
      it "returns the correct results" do
        results = BlacklistedPaypalPayee::SearchService.search(nil, 2, 2)
        expect(results.length).to eq(2)
      end
    end
  end
end
