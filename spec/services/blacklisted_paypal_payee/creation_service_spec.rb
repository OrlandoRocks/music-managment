require "rails_helper"

describe BlacklistedPaypalPayee::CreationService do
  describe ".create" do
    context "comma separated" do
      it "creates the new payees" do
        expect{
          BlacklistedPaypalPayee::CreationService.create("one@paypal.com, two@paypal.com, three@paypal.com")
        }.to change{BlacklistedPaypalPayee.count}.by(3)
      end
    end

    context "whitespace separated" do
      it "creates the new payees" do
        expect{
          BlacklistedPaypalPayee::CreationService.create("one@paypal.com two@paypal.com three@paypal.com")
        }.to change{BlacklistedPaypalPayee.count}.by(3)
      end
    end

    context "newline separated" do
      it "creates the new payees" do
        expect{
          BlacklistedPaypalPayee::CreationService.create("one@paypal.com\ntwo@paypal.com\nthree@paypal.com")
        }.to change{BlacklistedPaypalPayee.count}.by(3)
      end
    end
  end
end
