require "rails_helper"

describe DeliveryErrorNotifierService do

  describe ".notify" do
    context "with specified from_date and to_date" do
      it "sends one email for each release" do
        distribution1 = create(:distribution, :with_salepoint)
        distribution2 = create(:distribution, :with_salepoint)
        [distribution1, distribution2].each { |d| d.update(state: "error", updated_at: "2018-06-05") }

        expect(DeliveryErrorSummaryMailer).to receive(:delivery_errors_email).exactly(2).times.and_return(double({deliver: true}))
        DeliveryErrorNotifierService.notify("2018-06-05", "2018-06-06")
      end
    end

    context "without any from_date or to_date" do
      it "sends an email for each errored release, including all of its distribution errors" do
        distribution = create(:distribution, :with_salepoint)
        distribution.update(state: "error", updated_at: Date.current - 3.days)

        expect(DeliveryErrorSummaryMailer).to receive(:delivery_errors_email).exactly(1).times.and_return(double({deliver: true}))
        DeliveryErrorNotifierService.notify
      end
    end
  end
end
