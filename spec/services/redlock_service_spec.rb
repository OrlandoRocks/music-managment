require "rails_helper"


describe RedlockService do
     # provides a mutual exclusion lock, referenced by user defined key, and prevents another operation from aquiring the same lock until it is released when the first operation completes

    describe "#self.with_lock," do

        let(:the_block) { -> {:success}}
        let(:result) { :failure }

        it "will raise an error when no key is given" do
            expect do
                RedlockService.with_lock(nil, &the_block)
            end.to raise_error RedlockService::MissingKeyError
        end

        it "will raise an error when no block is given" do
            expect do
                RedlockService.with_lock("key_a")
            end.to raise_error RedlockService::MissingBlockError
        end

        it "executes block and returns a :success status and the result when a lock can be established" do
            result = RedlockService.with_lock("key_a", &the_block)
            expect(result).to eq([:success, :success])
        end

        it "executes a second block and returns a :success status and the result when a different lock is established" do
            RedlockService.with_lock("key_a", &the_block)
            result = RedlockService.with_lock("key_b", &the_block)
            expect(result).to eq([:success, :success])
        end

        it "executes a block and returns a success status and the result when the same lock is acquired after it has been released by a previous operation" do
            RedlockService.with_lock "key_a", &the_block
            result = RedlockService.with_lock "key_a", &the_block
            expect(result).to eq([:success, :success])
        end

        it "times out and returns a :failure status and nil result when trying to aquire the same lock before the previous operation has completed and lock has been released" do
            def try_twice_same_key
                RedlockService.with_lock("key_a") { RedlockService.with_lock "key_a", &the_block }
            end

            expect { try_twice_same_key }.to_not raise_error
            expect(try_twice_same_key).to eq([:success,[:failure,nil]])
        end

        it "executes block and returns a success status and the result when trying to access before lock is released but using different key" do
            def try_twice_different_key
                RedlockService.with_lock("key_a") do
                        RedlockService.with_lock "key_b", &the_block
                end
            end
            
            expect { try_twice_different_key }.to_not raise_error
            expect(try_twice_different_key).to eq([:success, [:success,:success]])
        end

    end
end
