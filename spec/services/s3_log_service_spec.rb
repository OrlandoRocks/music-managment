require "rails_helper"
describe S3LogService do
  let!(:bucket)         { "payment-gateway-tx-tunecore.com" }
  let!(:bucket_path)    { "staging" }
  let!(:data)    { "raw response goes here" }
  let!(:file_name)      { "timestamp_braintree_invoice_id1" }


  describe "#initialize" do
    context "good inputs" do
      it "doesn't raise error" do
        options = {
            file_name:   file_name,
            data:        data,
            bucket_name: bucket,
            bucket_path: bucket_path
        }

        expect{S3LogService.new(options)}.not_to raise_error
      end
    end
    context "without file_name" do
      it "raises error" do
        options = {
            data:        data,
            bucket_name: bucket,
            bucket_path: bucket_path
        }
        expect{S3LogService.new(options)}.to raise_error(S3LogService::NoFileNameProvidedError)
      end
    end
    context "without data" do
      it "raises error" do
        options = {
            file_name:   file_name,
            bucket_name: bucket,
            bucket_path: bucket_path
        }
        expect{S3LogService.new(options)}.to raise_error(S3LogService::NoDataProvidedError)
      end
    end
    context "without bucket_name" do
      it "raises error" do
        options = {
            file_name:   file_name,
            data:        data,
            bucket_path: bucket_path
        }
        expect{S3LogService.new(options)}.to raise_error(S3LogService::NoBucketFoundError)
      end
    end
    context "without bucket_path" do
      it "raises error" do
        options = {
            file_name:   file_name,
            data:        data,
            bucket_name: bucket
        }
        expect{S3LogService.new(options)}.to raise_error(S3LogService::NoBucketPathError)
      end
    end
  end
  context "s3" do
    let!(:options) do 
      {
        file_name:   file_name,
        data:        data,
        bucket_name: bucket,
        bucket_path: bucket_path
      }
    end
    let!(:service) { S3LogService.new(options) }
    describe "#write_and_upload" do
      it "streams to s3" do
        expect_any_instance_of(Aws::S3::Object).to receive(:upload_file)
        service.write_and_upload
      end
    end
    describe "#upload_only" do
      it "raises error without a valid file" do
        expect{service.upload_only}.to raise_error(S3LogService::InvalidFileError)
      end
    end

  end
  context "uploading files directly" do
    let!(:file) do 
      file = Tempfile.new
      file.write("foo")
      file.close
      file
    end
    let!(:options) do 
      {
        file_name:   file_name,
        data:        file,
        bucket_name: bucket,
        bucket_path: bucket_path
      }
    end
    let!(:service) { S3LogService.new(options) }
    after(:each) do
      file.unlink
    end
    describe "#upload_only" do
      it "streams to s3" do
        expect_any_instance_of(Aws::S3::Object).to receive(:upload_file)
        service.upload_only
      end
      it "streams to s3" do
        expect_any_instance_of(Aws::S3::Object).to receive(:upload_file)
        service.upload_only
      end
    end
  end

end
