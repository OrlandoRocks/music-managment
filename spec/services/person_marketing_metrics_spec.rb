require "rails_helper"

describe 'PersonMarketingMetrics' do
  let(:person) { FactoryBot.create(:person) }
  subject { PersonMarketingMetrics.new(person) }

  describe '.any_credits_available?' do
    it "returns true when a user has no available credits" do
      expect(CreditUsage).to receive(:all_available).and_return(['a_fake_credit'])
      expect(subject.any_credits_available?).to eq true
    end

    it "returns false when a user has available credits" do
      expect(CreditUsage).to receive(:all_available).and_return([])
      expect(subject.any_credits_available?).to eq false
    end
  end

  describe '.release_in_progress?' do
    it "returns true when a user has a relase in progress" do
      album = FactoryBot.create(:album, person: person)
      expect(album.payment_applied?).to eq false
      expect(subject.release_in_progress?).to eq true
    end

    it "returns false when a user has no releases in progress" do
      album = FactoryBot.create(:album, :paid, person: person)
      expect(album.payment_applied?).to eq true
      expect(subject.release_in_progress?).to eq false
    end
  end

  describe '.credit_in_last_invoice?' do
    before do
      invoice = FactoryBot.create(:invoice, person: person)
      invoice.purchases << FactoryBot.create(:purchase, person: person)
    end

    it "returns true when a user's last invoice did include a credit" do
      expect_any_instance_of(Product).to receive(:is_a_credit_product?).and_return(true)
      expect(subject.credit_in_last_invoice?).to eq true
    end

    it "returns false when a user's last invoice did not include a credit" do
      expect(subject.credit_in_last_invoice?).to eq false
    end
  end

end
