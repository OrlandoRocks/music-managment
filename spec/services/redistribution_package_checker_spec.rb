require "rails_helper"

describe RedistributionPackageChecker do
  before(:each) do
    @person      = FactoryBot.create(:person)
    @artist      = FactoryBot.create(:artist)
    album        = FactoryBot.create(:album)
    creative     = FactoryBot.create(:creative, artist_id: @artist.id, creativeable_id: album.id, creativeable_type: 'Album').update(person_id: @person.id)
    petri_bundle = FactoryBot.build(:petri_bundle, album_id: album.id)
    petri_bundle.save
    @distro1 = FactoryBot.create(:distribution, converter_class: "MusicStores::Itunes::Converter", petri_bundle_id: petri_bundle.id)
    @distro2 = FactoryBot.create(:distribution, converter_class: "MusicStores::Itunes::Converter", petri_bundle_id: petri_bundle.id)
    @package = RedistributionPackage.create(person_id: @person.id, artist_id: @artist.id, converter_class: "MusicStores::Itunes::Converter")
  end

  describe ".check_redistribution_statuses" do
    context "all distributions are delivered" do
      it "sets the package's state to 'redelivered'" do
        @distro1.update(state: "delivered")
        @distro2.update(state: "delivered")
        RedistributionPackageChecker.check_redistribution_statuses
        expect(RedistributionPackage.first.state).to eq("redelivered")
      end
    end

    context "some distributions errored but others are still enqueued" do
      it "keeps the package's state 'pending'" do
        @distro1.update(state: "error")
        @distro2.update(state: "enqueued")
        RedistributionPackageChecker.check_redistribution_statuses
        expect(RedistributionPackage.first.state).to eq("pending")
      end
    end

    context "all distributions are errored" do
      it "sets the package's state to errored" do
        @distro1.update(state: "error")
        @distro2.update(state: "error")
        RedistributionPackageChecker.check_redistribution_statuses
        expect(RedistributionPackage.first.state).to eq("error")
      end
    end

    context "some distributions are errored, none are still enqueued" do
      it "sets the pcakage's state to errored" do
        @distro1.update(state: "error")
        @distro2.update(state: "delivered")
        RedistributionPackageChecker.check_redistribution_statuses
        expect(RedistributionPackage.first.state).to eq("error")
      end
    end
  end
end
