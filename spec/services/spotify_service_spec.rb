require "rails_helper"
describe SpotifyService do
  include SpotifyApiHelper

  before :each do
    @faraday = fake_spotify_client
    allow(Faraday).to receive(:new).and_return(@faraday)
    allow(DistributorAPI::Album).to receive(:delivered_albums).and_return([double(http_body: { 'albums' => albums })])
  end

  let(:albums) { [] }

  context "querying the db" do
    context 'without Spotify URI' do
      let(:albums) { [create(:distribution, :delivered_to_spotify).petri_bundle.album] }

      it "finds UPCS for Spotify distributions" do
        distros = SpotifyService.new.find_unlinked_spotify_distributions
        expect(distros.count).to eq(1)
      end
    end

    context 'with albums that have a Spotify URI assigned to them' do
      let(:albums) { [create(:external_service_id, service_name: "Spotify").linkable] }

      it "ignores them" do
        distros = SpotifyService.new.find_unlinked_spotify_distributions
        expect(distros.include?(albums.first)).to be false
      end
    end

    context 'when there are multiple distributions' do
      before { old_dist.update(updated_at: 2.weeks.ago) }

      let(:albums) { [old_dist.album, new_dist.album] }
      let(:new_dist) { create(:distribution, :delivered_to_spotify) }
      let(:old_dist) { create(:distribution, :delivered_to_spotify) }

      it "defaults to distributions from the last week" do
        distros = SpotifyService.new.find_unlinked_spotify_distributions
        expect(distros.count).to eq(1)
        expect(distros.first).to eq(new_dist.petri_bundle.album)
      end
    end

    context 'when delivered via distributor' do
      let(:albums) { [create(:distribution, :delivered_to_spotify_via_distributor).petri_bundle.album] }

      it "finds UPCS for Spotify" do
        distros = SpotifyService.new.find_unlinked_spotify_distributions
        expect(distros.count).to eq(1)
      end
    end
  end

  context "without Spotify album URIs" do
    it "obtains a fresh bearer token on initialization" do
      expect_any_instance_of(Spotify::ApiClient).to receive(:get_token)
      SpotifyService.new
    end

    context 'with multiple albums' do
      before do
        allow(DistributorAPI::Album).to receive(:delivered_albums)
                                          .and_return([double(http_body: { 'albums' => albums })])
      end

      let(:albums) { create_list(:distribution, 3, :delivered_to_spotify).map(&:album) }

      it "queries Spotify by album UPC for each album" do
        svc = SpotifyService.new
        albs = svc.find_unlinked_spotify_distributions
        expect_any_instance_of(Spotify::ApiClient).to receive(:find_by_upc).at_least(3).times
        svc.search_by_upc(albs)
      end
    end

    it "does not set an external id for new albums without Spotify URIs" do
      distro = create(:distribution, :delivered_to_spotify).petri_bundle.album
      allow_any_instance_of(Spotify::ApiClient).to receive(:find_by_upc).and_return([])
      expect {
        SpotifyService.new.search_by_upc([distro])
      }.to_not change(ExternalServiceId, :count)
    end

    it "marks spotify uris as unavailable for older distributions" do
      distro = create(:distribution, :delivered_to_spotify)
      distro.update(updated_at: 1.month.ago)
      album = distro.petri_bundle.album
      allow_any_instance_of(Spotify::ApiClient).to receive(:find_by_upc).and_return([])
      expect {
        SpotifyService.new.search_by_upc([album])
      }.to change(ExternalServiceId, :count).by(1)
      expect(album.external_id_for("Spotify")).to eq("unavailable")
    end

    it "creates a new ExternalServiceId for the album" do
      distro          = create(:distribution, :delivered_to_spotify)
      unlinked_album  = distro.petri_bundle.album
      allow_any_instance_of(Spotify::ApiClient).to receive(:find_by_upc).and_return({ "id" => "spotify_id" })

      expect {
        SpotifyService.new.search_by_upc([unlinked_album])
      }.to change(ExternalServiceId, :count).by(1)
      expect(unlinked_album.reload.external_id_for("Spotify")).to_not be nil
    end
  end

  context "#get_tracks_for_album" do
    before :each do
      @album = create(:album, :with_uploaded_songs, number_of_songs: 5)
      @album.set_external_id_for("Spotify", "1234")
      @alb_uri = @album.external_id_for("Spotify")
      @isrcs   = @album.songs.map(&:isrc)
      @resp    = JSON.parse(album_track_objects(@isrcs))["tracks"]
    end

    describe "#extract_track_info" do
      it "turns a spotify tracks array into a map of isrcs to uris" do
        svc     = SpotifyService.new
        extract = svc.extract_track_info(@resp)
        expect(extract).to be_a(Hash)
        expect(extract.keys).to eq(@isrcs)
      end
    end

    describe "#unlinked_song_ids" do
      before :each do
        @album = create(:album, :with_uploaded_songs, number_of_songs: 5)
        @album.set_external_id_for("Spotify", "1234")
        @alb_uri = @album.external_id_for("Spotify")
        @isrcs   = @album.songs.map(&:isrc)
        @resp    = JSON.parse(album_track_objects(@isrcs))["tracks"]
      end

      it "maps the isrcs returned from Spotify to TC song ids" do
        svc     = SpotifyService.new
        extract = svc.extract_track_info(@resp)
        ids     = svc.unlinked_song_ids(extract.keys)

        expect(ids).to be_a(ActiveRecord::Relation)
        expect(ids.map { |i| i["id"] }).to eq(@album.songs.map(&:id))
      end

      it "only returns songs without spotify external_service_ids" do
        svc = SpotifyService.new
        allow(svc.spot_client).to receive(:get_album_tracks).and_return(@resp)
        @album.songs.each do |song|
          song.set_external_id_for("spotify", song.name.to_s)
        end
        extract = svc.extract_track_info(@resp)
        ids = svc.unlinked_song_ids(extract.keys)

        expect(ids.empty?).to be true
        expect(svc).not_to receive(:format_and_insert)
        svc.get_tracks_for_album(@album.external_id_for("spotify"))
      end

      it "adds a spotify esi for songs with esis for other services" do
        svc = SpotifyService.new
        allow(svc.spot_client).to receive(:get_album_tracks).and_return(@resp)
        @album.songs.first.set_external_id_for("apple", Time.now.to_i.to_s)
        extract = svc.extract_track_info(@resp)
        ids = svc.unlinked_song_ids(extract.keys)

        expect(ids.count(:all)).to eq(@album.songs.count)
        expect(svc).to receive(:format_and_insert).with(extract, ids)

        svc.get_tracks_for_album(@album.external_id_for("spotify"))
      end

      it "return if album uri is unavailable for spotify" do
        svc = SpotifyService.new
        expect(svc.get_tracks_for_album("unavailable")).to eq(nil)
      end
    end

    it "only creates records for songs that don't already have an ESI" do
      album = create(:album, :with_uploaded_songs, number_of_songs: 2)

      create(:external_service_id, linkable: album.songs.first, service_name: "spotify")

      track_hash = {
        album.songs.first.isrc => "1738",
        album.songs.last.isrc => "lkj98"
      }

      svc = SpotifyService.new
      allow(svc.spot_client).to receive(:get_album_tracks).and_return({ tracks: "12345" })
      allow(svc).to receive(:extract_track_info).and_return(track_hash)

      expect {
        svc.get_tracks_for_album("12345")
      }.to change(ExternalServiceId, :count).by(1)
    end

    it "does not create any records when all songs already have ESIs" do
      svc = SpotifyService.new

      create(
        :external_service_id, linkable: create(:song), identifier: "abc123",
                              service_name: "spotify"
      )
      create(
        :external_service_id, linkable: create(:song), identifier: "efg456",
                              service_name: "spotify"
      )
      track_hash = {
        "isrc1" => "abc123",
        "isrc2" => "efg456"
      }

      allow(svc.spot_client).to receive(:get_album_tracks).and_return(nil)
      allow(svc).to receive(:extract_track_info).and_return(track_hash)

      expect {
        svc.get_tracks_for_album("12345")
      }.to change(ExternalServiceId, :count).by(0)
    end

    it "does not raise an error when a song already has an ESI" do
      create(
        :external_service_id, linkable: create(:song), identifier: "abc123",
                              service_name: "spotify"
      )

      track_hash = { "isrc1" => "abc123" }

      svc = SpotifyService.new
      allow(svc.spot_client).to receive(:get_album_tracks).and_return(nil)
      allow(svc).to receive(:extract_track_info).and_return(track_hash)

      expect {
        svc.get_tracks_for_album("12345")
      }.to_not raise_error
    end

    describe "#format_and_insert" do
      it "sets up and executes a batch insertion of ExternalServiceIds" do
        svc = SpotifyService.new
        track_hash = {
          "isrc1" => "1738",
          "isrc2" => "lkj98"
        }

        tc_ids = [
          double(id: 1, resolved_isrc: "isrc1"),
          double(id: 2, resolved_isrc: "isrc2")
        ]

        expect {
          svc.format_and_insert(track_hash, tc_ids)
        }.to change(ExternalServiceId, :count).by(2)
      end

      it "creates a new ExternalServiceId for each album song" do
        resp = full_tracks(@isrcs).to_json
        allow_any_instance_of(Spotify::ApiClient)
          .to receive(:get_album_tracks).and_return(JSON.parse(resp))
        svc = SpotifyService.new
        expect {
          svc.get_tracks_for_album(@album.external_id_for("Spotify"))
        }.to change(ExternalServiceId, :count).by(5)
      end
    end
  end

  describe "#find_unlinked_artists_albums" do
    let!(:album) {
      create(
        :album,
        name: "new_spotify_album",
        finalized_at: Time.now
      )
    }

    let!(:salepoint) { 
      create(:salepoint,
      salepointable: album,
      store_id: Store::SPOTIFY_STORE_ID,
      finalized_at: Time.now
      )
    }

    context "when spotify esi record is 'new_artist'" do
      let!(:new_artist_esi) {
        ExternalServiceId.create!(
          linkable: album.creatives.first,
          linkable_type: "Creative",
          identifier: nil,
          service_name: "spotify",
          state: "new_artist"
        )
      }

      context "when album has a unavailable record" do
        let!(:album_esi) {
          ExternalServiceId.create!(
            linkable: album,
            linkable_type: "Album",
            identifier: "unavailable",
            service_name: "spotify",
            state: "matched"
          )
        }

        it "will not return the album" do
          expect(SpotifyService.new.find_unlinked_artists_albums).to eq([])
        end
      end

      context "when album has valid esi record" do
        let!(:album_esi) {
          ExternalServiceId.create!(
            linkable: album,
            linkable_type: "Album",
            identifier: "valid_spotify_id",
            service_name: "spotify",
            state: "matched"
          )
        }

        it "will return the album" do
          expect(SpotifyService.new.find_unlinked_artists_albums).to eq([album])
        end
      end
    end

    context "when there is no artist esi record" do
      context "when album has a unavailable record" do
        let!(:album_esi) {
          ExternalServiceId.create!(
            linkable: album,
            linkable_type: "Album",
            identifier: "unavailable",
            service_name: "spotify",
            state: "matched"
          )
        }

        it "will return the album" do
          expect(SpotifyService.new.find_unlinked_artists_albums).to eq([])
        end
      end

      context "when album has valid esi record" do
        let!(:album_esi) {
          ExternalServiceId.create!(
            linkable: album,
            linkable_type: "Album",
            identifier: "valid_spotify_id",
            service_name: "spotify",
            state: "matched"
          )
        }

        it "will return the album" do
          expect(SpotifyService.new.find_unlinked_artists_albums).to eq([album])
        end
      end
    end

    context "when the artist esi record has nil state and value" do
      let!(:new_artist_esi) {
        ExternalServiceId.create!(
          linkable: album.creatives.first,
          linkable_type: "Creative",
          identifier: nil,
          service_name: "spotify",
          state: nil
        )
      }
      context "when album has a unavailable record" do
        let!(:album_esi) {
          ExternalServiceId.create!(
            linkable: album,
            linkable_type: "Album",
            identifier: "unavailable",
            service_name: "spotify",
            state: "matched"
          )
        }

        it "will return the album" do
          expect(SpotifyService.new.find_unlinked_artists_albums).to eq([])
        end
      end

      context "when album has valid esi record" do
        let!(:album_esi) {
          ExternalServiceId.create!(
            linkable: album,
            linkable_type: "Album",
            identifier: "valid_spotify_id",
            service_name: "spotify",
            state: "matched"
          )
        }

        it "will return the album" do
          expect(SpotifyService.new.find_unlinked_artists_albums).to eq([album])
        end
      end
    end
  end

  describe "#update_artist_ids" do
    let(:person) { create(:person) }
    let(:primary_artist) { create(:artist, name: "Spoabc") }
    let(:featuring_artist) { create(:artist, name: "Spoefg") }
    let(:remixer_artist) { create(:artist, name: "Spohij") }
    let(:invalid_artist) { create(:artist, name: "Lil do sumpin' else") }
    let(:album_uri) { Faker::Lorem.characters[0..22] }
    let(:spotify_album_tracks) {
      {
        primary_artist.name.downcase => Faker::Lorem.characters[0..22],
        featuring_artist.name.downcase => Faker::Lorem.characters[0..22],
        remixer_artist.name.downcase => Faker::Lorem.characters[0..22],
      }
    }
    let(:album) {
      create(
        :album,
        :with_uploaded_songs,
        :with_spotify_album_esi,
        :with_creative_song_roles,
        :with_creatives,
        {
          alb_identifier: album_uri,
          person: person,
          cr_artist_roles: [
            { artist: remixer_artist, role: "remixer" },
            { artist: invalid_artist, role: "flugelhorn" }
          ],
          artist_roles: [
            { artist: primary_artist, role: "primary_artist" },
            { artist: featuring_artist, role: "featuring" }
          ]
        }
      )
    }

    let(:svc) { SpotifyService.new }

    it "should insert spotify artist id for the album with primary, featuring and remixer artists" do
      allow_any_instance_of(Spotify::ApiClient).to receive(:find_artists_by_album_uri).and_return(spotify_album_tracks)
      svc.update_artist_ids([album])
      spotify_album_tracks.each do |_key, val|
        esi = ExternalServiceId.find_by(service_name: "spotify", linkable_type: "Creative", identifier: val)
        expect(esi.identifier).to eq(val)
      end
    end

    it "should not insert new record if an album already has creatives in external service ids table" do
      allow_any_instance_of(Spotify::ApiClient).to receive(:find_artists_by_album_uri).and_return(spotify_album_tracks)
      svc.update_artist_ids([album]) # creates orginal spotify esis
      expect { svc.update_artist_ids([album]) }.to change {
                                                     ExternalServiceId.where(
                                                       service_name: "spotify",
                                                       linkable_type: "Creative"
                                                     ).count
                                                   }.by(0)
    end

    context "when the artist is on multiple releases across the same account" do
      let(:album1) {
        create(
          :album,
          :with_uploaded_songs,
          :with_spotify_album_esi,
          :with_creative_song_roles,
          :with_creatives,
          {
            alb_identifier: album_uri,
            person: person,
            cr_artist_roles: [
              { artist: remixer_artist, role: "remixer" },
              { artist: invalid_artist, role: "flugelhorn" }
            ],
            artist_roles: [
              { artist: primary_artist, role: "primary_artist" },
              { artist: featuring_artist, role: "featuring" }
            ]
          }
        )
      }
      it "updates the esi records across all creative instances for each artist" do
        allow_any_instance_of(Spotify::ApiClient).to receive(:find_artists_by_album_uri)
          .and_return(spotify_album_tracks)

        svc.update_artist_ids([album1])
        esis = ExternalServiceId
               .by_artist_name_and_person_id([
                                               primary_artist.name,
                                               featuring_artist.name,
                                               remixer_artist.name
                                             ], album.person_id)
               .by_service("Spotify")
               .pluck(:identifier).uniq

        expect(esis).to match_array(spotify_album_tracks.values)
      end
    end

    context "when there is an artist with a non spotify valid role on album" do
      let(:spotify_album_tracks) {
        {
          primary_artist.name.downcase => Faker::Lorem.characters[0..22],
          featuring_artist.name.downcase => Faker::Lorem.characters[0..22],
          remixer_artist.name.downcase => Faker::Lorem.characters[0..22],
          invalid_artist.name.downcase => Faker::Lorem.characters[0..22]
        }
      }

      it "should not insert spotify artist id for artist with non spotify roles" do
        allow_any_instance_of(Spotify::ApiClient).to receive(:find_artists_by_album_uri)
          .and_return(spotify_album_tracks)

        svc.update_artist_ids([album])
        id = spotify_album_tracks[invalid_artist.name.downcase]
        esi = ExternalServiceId.find_by(service_name: "spotify", linkable_type: "Creative", identifier: id)
        expect(esi).to be nil
      end
    end
  end

  describe "#get_uri_by_albums" do
    let(:albums) { [] }
    before do
      3.times { albums << create(:album) }
    end
    it "calls search_by_upc with the albums_array" do
      expect_any_instance_of(SpotifyService).to receive(:search_by_upc).once.with(albums)
      described_class.get_uri_by_albums(albums)
    end
  end

  describe '#date_delivered' do
    subject { described_class.new.date_delivered(album) }

    let(:time) { Time.now }
    let(:albums) { [album] }

    context 'when album is delivered' do
      let(:album) { create(:album, :with_spotify_distribution, updated_at: time) }

      it { is_expected.to be_within(1.second).of time }
    end
  end

  describe '#find_unlinked_spotify_distributions' do
    subject { described_class.new.find_unlinked_spotify_distributions }

    context 'when album is delivered' do
      let(:album) { create(:album, :with_spotify_distribution) }
      let(:albums) { [album] }

      it { is_expected.to eq([album]) }
    end

    context 'when album is delivered via distributor' do
      let(:album) { create(:album, :with_spotify_distribution, via_distributor: true) }
      let(:albums) { [album] }

      it { is_expected.to eq([album]) }
    end

    context 'with both distributions' do
      let(:album1) { create(:album, :with_spotify_distribution) }
      let(:album2) { create(:album, :with_spotify_distribution, via_distributor: true) }
      let(:albums) { [album1, album2] }

      it { is_expected.to eq([album1, album2]) }
    end
  end
end
