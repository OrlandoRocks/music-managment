require "rails_helper"

class ReferrerServiceDummy
  include ReferrerService
end

describe ReferrerService do
  describe "#record_referrer_event" do

    it "should only record friend referrals for exact code matches" do
      # Create various Friend Referral Campaigns where there are similar
      # referral codes

      code1 = create(:friend_referral_campaign, :campaign_code => "aBc")
      code2 = create(:friend_referral_campaign, :campaign_code => "ABc")
      code3 = create(:friend_referral_campaign, :campaign_code => "AbC")

      person = build(:person,
        referral_campaign: code3.campaign_id,
        referral_type: "raf",
        referral_token: code3.campaign_code
      )
      invoice = build(:invoice)

      expect(FriendReferral).to receive(:record_event).once

      referrer_service = ReferrerServiceDummy.new
      referrer_service.record_referrer_event(invoice, person)

      person.referral_token = code2.campaign_code
      expect(FriendReferral).to receive(:record_event).once

      referrer_service.record_referrer_event(invoice, person)
    end
  end
end
