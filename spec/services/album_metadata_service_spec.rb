require "rails_helper"

describe AlbumMetadataService do
  describe "qobuz" do
    let(:album) { create(:album, :with_songs, number_of_songs: 3) }
    let(:song1) { album.songs.first }
    let(:song2) { album.songs.second }
    let(:song3) { album.songs.third }

    context "when there is a lofi song" do
      it "returns an array including the lofi song" do
        allow_any_instance_of(SongS3DetailService).to receive(:lo_fi).and_return([song1])

        qobuz_store = Store.find(Store::QOBUZ_STORE_ID)
        create(:salepoint, store: qobuz_store, salepointable: album)

        expected = AlbumMetadataService.new(album)

        expect(expected.show_remove_qobuz_confirmation?).to be false
        expect(expected.visible_lo_fi_songs.length).to eq(1)
      end

      context "and another salepoint" do
        it "returns an empty array" do
          allow_any_instance_of(SongS3DetailService).to receive(:lo_fi).and_return([song1])

          qobuz_store = Store.find(Store::QOBUZ_STORE_ID)
          create(:salepoint, store: qobuz_store, salepointable: album)
          google_store = Store.find(Store::GOOGLE_STORE_ID)
          create(:salepoint, store: google_store, salepointable: album)

          expected = AlbumMetadataService.new(album)

          expect(expected.show_remove_qobuz_confirmation?).to be true
          expect(expected.visible_lo_fi_songs.length).to eq(0)
        end
      end
    end

    context "when there are no lofi songs" do
      it "returns an empty array" do
        allow_any_instance_of(SongS3DetailService).to receive(:lo_fi).and_return([])

        qobuz_store = Store.find(Store::QOBUZ_STORE_ID)
        create(:salepoint, store: qobuz_store, salepointable: album)

        expected = AlbumMetadataService.new(album)

        expect(expected.visible_lo_fi_songs.length).to eq(0)
      end
    end
  end
end
