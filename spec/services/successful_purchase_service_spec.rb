require "rails_helper"

describe SuccessfulPurchaseService do
  describe "#process" do
    let(:person) { create(:person) }

    context ".successful_purchse" do
      before(:each) do
        mailer = double("Mailer")
        expect(PersonNotifier).to receive(:payment_thank_you).and_return(mailer)
        expect(mailer).to receive(:deliver)
      end

      it "should send a soundout report purchase event" do
        invoice = create(:invoice, person: person)
        service = SuccessfulPurchaseService.new(invoice)
        allow(invoice).to receive(:person).and_return(person)
        allow(invoice).to receive(:final_settlement_amount_cents).and_return(10000)
        allow(invoice).to receive(:update_person_analytics).and_return(nil)
        allow(invoice).to receive(:check_and_set_first_time_distribution_or_credit).and_return(true)
        soundout_report = create(:soundout_report, :with_track, person: person)
        purchase = double(Purchase)
        product = double(Product)

        allow(purchase).to receive(:related_type).and_return(SoundoutReport.name)
        allow(purchase).to receive(:product).and_return(product)
        allow(product).to receive(:tcs_subscription?).and_return(false)
        allow(purchase).to receive(:related).and_return(soundout_report)
        allow(purchase).to receive_message_chain(:product, :name).and_return("")
        allow(invoice).to receive(:purchases).and_return([purchase])
        expect(service).to receive(:record_referrer_event)

        service.process
      end

      it "kicks off Delivery::AlbumWorker with related_type of Album" do
        album  = create(:album, person: person, legal_review_state: "NEEDS REVIEW")
        invoice  = create(:invoice, :with_purchase, related: album, person: person)
        allow(invoice).to receive(:final_settlement_amount_cents).and_return(10000)
        allow(invoice).to receive(:update_person_analytics).and_return(nil)
        allow(invoice).to receive(:check_and_set_first_time_distribution_or_credit).and_return(true)
        service = SuccessfulPurchaseService.new(invoice)
        expect(Delivery::AlbumWorker).to receive(:perform_async).with(album.id)

        service.process
      end

      it "kicks off Delivery::AlbumWorker with related_type of CreditUsage" do
        allow(CreditUsage).to receive(:credit_available_for?).with(person, Album.name).and_return(true)
        album        = create(:album, :finalized, person: person, legal_review_state: "NEEDS REVIEW")
        credit_usage = create(:credit_usage, related: album, person: person, applies_to_type: Album.name)
        invoice      = create(:invoice, :with_purchase, related: credit_usage, person: person)
        expect(Delivery::AlbumWorker).to receive(:perform_async).with(album.id)
        service = SuccessfulPurchaseService.new(invoice)

        service.process
      end

      it "sends a refer a friend purchase notification if the user was referred by a friend" do
        person = create(:person, :with_friend_referral)
        album   = create(:album, person: person, legal_review_state: "NEEDS REVIEW")
        invoice = create(:invoice, :with_purchase, related: person.albums.last, person: person)
        expect(invoice).to receive(:send_refer_friend_purchase_notification).with(false)
        service = SuccessfulPurchaseService.new(invoice)

        service.process
      end

      it "does not send a refer a friend purchase notification if the user was not referred by a friend" do
        person = create(:person)
        album   = create(:album, person: person, legal_review_state: "NEEDS REVIEW")
        invoice = create(:invoice, :with_purchase, related: person.albums.last, person: person)
        expect(invoice).not_to receive(:send_refer_friend_purchase_notification).with(false)
        service = SuccessfulPurchaseService.new(invoice)

        service.process
      end

      it "sets the Youtube Monetization effective date"  do
        person               = create(:person, :with_ytsr_purchase)
        ytsr_purchase        = person.purchases.where(related_type: "YoutubeMonetization").first
        youtube_monetization = ytsr_purchase.related
        invoice              = create(:invoice, person: person)
        invoice.purchases << ytsr_purchase

        expect(youtube_monetization.effective_date).to be_nil
        service = SuccessfulPurchaseService.new(invoice)

        service.process
        expect(youtube_monetization.effective_date).not_to be_nil
      end
    end

    context ".after_purchase" do
      it "should rescue if mailer throws an error" do
        album  = create(:album, person: person, legal_review_state: "NEEDS REVIEW")
        invoice  = create(:invoice, :with_purchase, related: album, person: person)
        allow(invoice).to receive(:final_settlement_amount_cents).and_return(10000)
        allow(invoice).to receive(:update_person_analytics).and_return(nil)
        allow(invoice).to receive(:check_and_set_first_time_distribution_or_credit).and_return(true)
        allow(PersonNotifier).to receive(:payment_thank_you).and_raise(StandardError)
        expect(InvoiceLog.count).to eq(0)
        service = SuccessfulPurchaseService.new(invoice)
        expect(Airbrake).to receive(:notify)

        service.process
        expect(InvoiceLog.count).to eq(1)
      end
    end
  end
end
