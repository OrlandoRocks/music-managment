require "rails_helper"
include SalepointQuery

describe SalepointQuery do

  describe "#salepoint?" do
    it "returns true when present" do
      sp = create(:salepoint)
      store_id = sp.store_id
      expect(salepoint?([sp], store_id)).to be true
    end

    it "returns false when missing" do
      sp = create(:salepoint)
      store_id = sp.store_id + 1
      expect(salepoint?([sp], store_id)).to be false
    end
  end

  describe "#only_salepoint?" do
    it "returns true when only salepoint" do
      sp = create(:salepoint)
      store_id = sp.store_id
      expect(only_salepoint?([sp], store_id)).to be true
    end

    it "returns false when missing" do
      sp = create(:salepoint)
      store_id = sp.store_id
      expect(only_salepoint?([sp], store_id + 1)).to be false
    end

    it "returns false when an additional salepoint is present" do
      sp = create(:salepoint, store: Store.find(Store::GOOGLE_STORE_ID))
      sp2 = create(:salepoint, store: Store.find(Store::FB_REELS_STORE_ID))
      store_id = sp.store_id
      expect(only_salepoint?([sp, sp2], store_id)).to be false
    end
  end

  describe "#other_salepoints?" do
    it "returns true when other salepoints exist" do
      sp = create(:salepoint, store: Store.find(Store::GOOGLE_STORE_ID))
      sp2 = create(:salepoint, store: Store.find(Store::FB_REELS_STORE_ID))
      store_id = sp.store_id
      expect(other_salepoints?([sp, sp2], store_id)).to be true
    end
    it "returns false when missing" do
      sp = create(:salepoint, store: Store.find(Store::GOOGLE_STORE_ID))
      store_id = sp.store_id
      expect(other_salepoints?([sp], store_id + 1)).to be false
    end

    it "returns false when only salepoint" do
      sp = create(:salepoint, store: Store.find(Store::GOOGLE_STORE_ID))
      store_id = sp.store_id
      expect(other_salepoints?([sp], store_id)).to be false
    end
  end
end
