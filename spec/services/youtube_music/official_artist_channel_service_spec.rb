require 'rails_helper'
describe YoutubeMusic::OfficialArtistChannelService do
  describe '#authorization_redirect_url' do
    subject(:service) { described_class.authorization_redirect_url }
    let(:expected_url) { "https://accounts.google.com/o/oauth2/v2/auth" }
    it "builds the redirect url for youtube artist channel auth" do
      expect(subject.split("?").first).to eq(expected_url)
    end
  end
  context 'with logged in user' do
    let(:artist) { create(:artist, name: "Big Snax") }

    before do
      create(:album, person: person, artist: artist)
      create(:album, person: person, artist: artist)
      create(:album, person: person, artist: artist)
      person.albums.each do |album|
        create(:creative, creativeable: album, person: person, artist: artist, creativeable_type: "Album")
      end
    end

    let(:person) { create(:person, email: "BigSnax@hangry.com", name: "Big Snax") }

    describe '#create_esi_entries' do
      let(:esi_type) { "youtube_authorization" }
      let(:identifier) { "token" }
      let(:params) { {
          code: "authorize_snax!",
          scope: YoutubeMusic::OfficialArtistChannelService::SCOPE_URLS,
          person: person,
          artist_name: person.name
        }
      }

      subject { described_class.new(params).create_esi_entries(esi_type, identifier) }

      context "when user artist has no youtube esis" do
        it "creates records in external_service_ids table" do
          expect{ subject }.to change{ ExternalServiceId.count }.by(3)
        end

        it "saves the authorization code and service name to all external_service_ids for person artist" do
          subject
          codes = ExternalServiceId.joins(ExternalServiceId.creative_join)
                                   .where(creatives: { person_id: person.id})
                                   .pluck(:identifier, :service_name).uniq
          expect(codes).to eq([["token", "youtube_authorization"]])
        end
      end

      context "When user artist has exsisting youtube esis" do
        before do
          artist.creatives.each do |creative|
            ExternalServiceId.create!(
              linkable: creative,
              identifier: 'old_token',
              service_name: esi_type
            )
          end
        end

        it "overwrites with new identifier" do
          subject
          codes = ExternalServiceId.joins(ExternalServiceId.creative_join)
                                   .where(creatives: { person_id: person.id})
                                   .pluck(:identifier, :service_name).uniq
          expect(codes).to eq([["token", "youtube_authorization"]])
        end
      end
    end

    describe "#fetch_and_store_channel" do
      let(:person) { create(:person, email: "BigSnax@hangry.com", name: "Big Snax") }
      let(:esi_type_channel) { "youtube_channel_id" }
      let(:esi_type_auth) { "youtube_authorization" }
      let(:channel_id) { "big_snax_channel_id" }
      let(:token) { "refresh_token"}
      let(:code) { "authorize_snax!" }
      let(:params) { {
          code: code,
          scope: YoutubeMusic::OfficialArtistChannelService::SCOPE_URLS,
          person: person,
          artist_name: person.name
        }
      }

      before do
       allow_any_instance_of(Youtube::ApiClient).to receive(:fetch_refresh_token).and_return(token)
       allow_any_instance_of(Youtube::ApiClient).to receive(:fetch_channel_id).and_return(channel_id)
      end

      subject { described_class.new(params).fetch_and_store_channel }

      it "calls Youtube::ApiClient to fetch token and channel id" do
        expect(Youtube::ApiClient).to receive(:new).with({authorization: code}).and_call_original
        expect_any_instance_of(Youtube::ApiClient).to receive(:fetch_refresh_token).once
        expect_any_instance_of(Youtube::ApiClient).to receive(:fetch_channel_id).once
        subject
      end

      it "stores the auth token in the external_service_ids table" do
        subject
        channel_ids = ExternalServiceId.joins(ExternalServiceId.creative_join)
                                 .where(service_name: esi_type_auth)
                                 .where(creatives: { person_id: person.id})
                                 .pluck(:identifier, :service_name).uniq
        expect(channel_ids).to eq([[token, "youtube_authorization"]])
      end

      it "stores channel_id in external_service_ids table" do
        subject
        channel_ids = ExternalServiceId.joins(ExternalServiceId.creative_join)
                                 .where(service_name: esi_type_channel)
                                 .where(creatives: { person_id: person.id})
                                 .pluck(:identifier, :service_name).uniq
        expect(channel_ids).to eq([[channel_id, "youtube_channel_id"]])
      end
    end

    context "with created channels" do

      let(:channel_id) { "big_snax_channel_id" }
      let(:token) { "auth_token"}
      subject { described_class.channel_id_active_status(person.albums.first) }

      before do
        creatives = Creative.person_by_artist_name(person.id, person.name)
        auth_entries = creatives.each_with_object([]) do |c,array|
          entry = {
                linkable_id: c.id,
                linkable_type: "Creative",
                service_name: ExternalServiceId::YOUTUBE_AUTH,
                identifier: token
                }
          array << entry
        end
        channel_entries = creatives.each_with_object([]) do |c,array|
          entry = {
                linkable_id: c.id,
                linkable_type: "Creative",
                service_name: ExternalServiceId::YOUTUBE_CHANNEL,
                identifier: channel_id
                }
          array << entry
        end
        esi_entries = channel_entries + auth_entries
        ExternalServiceId.create!(esi_entries)
      end

      describe "#channel_id_active_status" do

        context "when channel still exists" do

          before { allow_any_instance_of(Youtube::ApiClient).to receive(:fetch_channel_id).and_return(channel_id) }

          it 'calls the youtube api client' do
            expect_any_instance_of(Youtube::ApiClient).to receive(:fetch_channel_id)
            subject
          end

          it 'returns true' do
            expect(subject).to eq(true)
          end
        end

        context "When channel has been removed" do

          before { allow_any_instance_of(Youtube::ApiClient).to receive(:fetch_channel_id).and_return(nil) }

          it 'calls the youtube api client' do
            expect_any_instance_of(Youtube::ApiClient).to receive(:fetch_channel_id)
            subject
          end

          it "sets the external_service_ids identifier to 'deleted'" do
            subject
            channel_ids = ExternalServiceId.joins(ExternalServiceId.creative_join)
                                   .where(service_name: ExternalServiceId::YOUTUBE_CHANNEL)
                                   .where(creatives: { person_id: person.id})
                                   .pluck(:identifier).uniq
            expect(channel_ids).to eq(["deleted"])
          end

          it 'returns false' do
            expect(subject).to eq(false)
          end
        end

        context "when API returns an error" do

          def raise_api_response(error_class)
            allow_any_instance_of(Youtube::ApiClient).to receive(:fetch_channel_id).and_raise(error_class)
          end 

          context "when youtube api returns an ApiError" do 

            it "notifies airbrake and returns true" do 
              error_class = Youtube::ApiClient::ApiError
              raise_api_response(error_class)
              expect(Airbrake).to receive(:notify).with("Youtube API Error: #{error_class}")
              expect(subject).to eq(true)
            end 
          end 

          context "when youtube api returns ServerError" do 
            it "notifies Airbrake and a ServerError" do 
              error_class = Youtube::ApiClient::ServerError
              raise_api_response(error_class)
              expect(Airbrake).to receive(:notify).with("Youtube API Server Error: #{error_class}")
              expect { subject }.to raise_error(error_class)
            end 
          end 

          context "when youtube api returns ApiQuotaError" do 
            it "notifies Airbrake and raises an ApiQuotaError" do 
              error_class = Youtube::ApiClient::ApiQuotaError
              raise_api_response(error_class)
              expect(Airbrake).to receive(:notify).with("Youtube API Quota Reached: #{error_class}")
              expect { subject }.to raise_error(error_class)
            end 
          end 

          context "when youtube api returns an AccountError" do 
            it  "notifies Airbrake and returns false" do 
              error_class = Youtube::ApiClient::AccountError
              raise_api_response(error_class)
              expect(Airbrake).to receive(:notify).with("Youtube Account Error: #{error_class}")
              expect(subject).to eq false 
            end 
          end 
        end 
      end

      describe '#enqueue_distribution!' do

        subject { described_class.new(person: person).enqueue_distribution! }

        context "with distributions" do
          before do
            person.albums.each do |album|
              petri = create(:petri_bundle, album: album)
              sp = create(:salepoint, salepointable: album, store_id: Store::GOOGLE_STORE_ID, finalized_at: Time.now)
              create(:distribution, petri_bundle: petri, salepoints: [sp])
              album.distributions.update(state: "delivered")
            end
            allow_any_instance_of(described_class).to receive(:creatives).and_return(person.creatives)
          end

          it "eneuques one distribution for artist" do
            expect_any_instance_of(Distribution).to receive(:retry).once
            subject
          end
        end
      end
    end
  end
end
