require 'rails_helper'
describe YoutubeMusic::YoutubeChannelVerificationService do
  let(:creative_person) { create(:person, email: "genius@creative.com", name: "Creative Genius") }
  let(:stable_person) { create(:person, email: "genius@stable.com", name: "Stable Genius") }
  let(:artist_with_valid_channel) { create(:artist, name: "Read Terms and Conditions", person: creative_person) }
  let(:artist_with_expired_channel) { create(:artist, name: "I Violated Terms and Conditions", person: stable_person) }
  let(:expired_channel_id) { "ExPiReDcHaNeLiD" }
  let(:expired_refresh_token) { "ExPiReDtOkEn" }
  let(:valid_channel_id) { "VaLiDcHaNnEliD" }
  let(:valid_refresh_token) { "VaLiDtOkeN" }
  before do
    3.times do
      create(:album, person: creative_person, artist: artist_with_valid_channel)
      create(:album, person: stable_person, artist: artist_with_expired_channel)
    end

    artist_with_valid_channel.creatives.update(person: creative_person)
    artist_with_expired_channel.creatives.update(person: stable_person)

    artist_with_valid_channel.creatives.each do |creative|
      ExternalServiceId.create!(
        linkable: creative,
        identifier: valid_channel_id,
        service_name: ExternalServiceId::YOUTUBE_CHANNEL
      )
      ExternalServiceId.create!(
        linkable: creative,
        identifier: valid_refresh_token,
        service_name: ExternalServiceId::YOUTUBE_AUTH
      )
    end


     artist_with_expired_channel.creatives.each do |creative|
      ExternalServiceId.create!(
        linkable: creative,
        identifier: expired_channel_id,
        service_name:ExternalServiceId::YOUTUBE_CHANNEL
      )
      ExternalServiceId.create!(
        linkable: creative,
        identifier: expired_refresh_token,
        service_name: ExternalServiceId::YOUTUBE_AUTH
      )
    end
  end



  describe "#verify_channels" do
    let(:batch_data) { fetch_batch_dataset }
    subject(:verification_service) { YoutubeMusic::YoutubeChannelVerificationService.new(batch_data) }

    before do
      allow_any_instance_of(Youtube::ApiClient).to receive(:fetch_channel_id) do |model|
        model.refresh_token == valid_refresh_token ? valid_channel_id : 'this_is_expired'
      end
    end

    it "updates verified_at for all channels that are still active" do
      subject.verify_channels
      channel_ids = external_service_idetifier_for(ExternalServiceId::YOUTUBE_CHANNEL, artist_with_valid_channel)
      expect(channel_ids.where.not(verified_at: nil).count).to eq(channel_ids.count)
    end


    it "deletes external_service_id entries for channels that are no longer valid" do
      subject.verify_channels
      channel_ids = external_service_idetifier_for([ExternalServiceId::YOUTUBE_CHANNEL, ExternalServiceId::YOUTUBE_AUTH], artist_with_expired_channel)
      expect(channel_ids).to be_empty
    end

    it "deletes external_service_ids when account error is encountered" do
      allow_any_instance_of(Youtube::ApiClient).to receive(:fetch_channel_id).and_raise(Youtube::ApiClient::AccountError)
      subject.verify_channels
      channel_ids = external_service_idetifier_for([ExternalServiceId::YOUTUBE_CHANNEL, ExternalServiceId::YOUTUBE_AUTH], artist_with_expired_channel)
      expect(channel_ids).to be_empty
    end

    it "does NOT delete external_service_ids when API error is encountered" do
      allow_any_instance_of(Youtube::ApiClient).to receive(:fetch_channel_id).and_raise(Youtube::ApiClient::ApiError)
      subject.verify_channels
      channel_ids = external_service_idetifier_for([ExternalServiceId::YOUTUBE_CHANNEL, ExternalServiceId::YOUTUBE_AUTH], artist_with_expired_channel)
      expect(channel_ids.where(verified_at: nil).count).to eq(channel_ids.count)
    end

    it "Logs the channel in the external_service_id_deletes table" do
      epected_deleted_attributes = {
        "artist_name" => artist_with_expired_channel.name,
        "person_id" => stable_person.id,
        "identifier" => expired_channel_id,
        "service_name" => ExternalServiceId::YOUTUBE_CHANNEL
      }
      subject.verify_channels
      deleted_record_hash = ExternalServiceIdDelete
                              .find_by(service_name: ExternalServiceId::YOUTUBE_CHANNEL)
                              .attributes
      expect(deleted_record_hash).to include(epected_deleted_attributes)
    end


    it "Logs the channel in the external_service_id_deletes table" do
      epected_deleted_attributes = {
        "artist_name" => artist_with_expired_channel.name,
        "person_id" => stable_person.id,
        "identifier" => expired_refresh_token,
        "service_name" => ExternalServiceId::YOUTUBE_AUTH
      }
      subject.verify_channels
      deleted_record_hash = ExternalServiceIdDelete
                              .find_by(service_name: ExternalServiceId::YOUTUBE_AUTH)
                              .attributes
      expect(deleted_record_hash).to include(epected_deleted_attributes)
    end
  end

  def external_service_idetifier_for(service_name, artist)
    ExternalServiceId.joins(ExternalServiceId.creative_join)
                       .where(service_name: service_name)
                       .where(creatives: { artist_id: artist.id})
  end

  def fetch_batch_dataset
    query = "SELECT
              a.name AS artist_name, p.id person_id, e1.identifier AS channel, e2.identifier AS token
            FROM
              creatives c
            JOIN
              artists a ON a.id = c.artist_id
            JOIN
              people p ON c.person_id = p.id
            JOIN
              external_service_ids e1 ON c.id = e1.linkable_id
                AND e1.linkable_type = 'Creative'
                AND e1.service_name = 'youtube_channel_id'
                AND (e1.verified_at < CURDATE() - INTERVAL 30 DAY OR e1.verified_at is NULL)
            JOIN
              external_service_ids e2 ON c.id = e2.linkable_id
                AND e2.linkable_type = 'Creative'
                AND e2.service_name = 'youtube_authorization'
            GROUP BY
              e1.identifier"

    results = ActiveRecord::Base.connection.exec_query(query)
  end
end
