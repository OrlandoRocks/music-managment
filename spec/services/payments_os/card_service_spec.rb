require "rails_helper"

describe PaymentsOS::CardService do
  let(:person) { create(:person) }
  it "creates a new stored_credit_card" do
    create(:payments_os_customer, person: person)
    allow_any_instance_of(PaymentsOS::ApiClient).to receive(:send_request).and_return(PaymentsOS::Responses::NewPaymentMethod.new(
      OpenStruct.new(
        body: JSON.dump({
          state: 'created',
          expiration_date: "05/19",
          last_4_digits: "5439",
          token: SecureRandom.uuid,
          vendor: "Visa"
        })
      )
    ))
    state = CountryState.first
    city = state.country_state_cities.first
    stored_credit_card_params = {
        payments_os_token: "99a1864d-b7ac-4c50-9d17-b87818a55269",
        state_id: state.id,
        state_city_name: city.name
    }
    is_success, _ = PaymentsOS::CardService.new(person).store_payment_method(stored_credit_card_params)
    expect(is_success).to be :ok
  end
end
