require "rails_helper"

describe PaymentsOS::PaymentsService do

  describe "#pay" do

    before(:each) do
      invoice = create(:invoice, final_settlement_amount_cents: 12000)
      person = create(:person)
      stored_credit_card = create(:stored_credit_card, person: person)
      cvv = 123
      @service = PaymentsOS::PaymentsService.new(invoice, stored_credit_card, cvv)
    end

    it "should make a successful payment by making payment and charge api calls" do
      payment_response = double(:payment, id: 1, status: PaymentsOSTransaction::CREDITED, response_json: "{}", "success?" => true)
      allow_any_instance_of(PaymentsOS::ApiClient).to receive(:send_request).with(:payment, anything).and_return(payment_response)
      redirect_url = "htp://test.in"
      charge_response = double(:charge, id: 2,
                               status: PaymentsOSTransaction::SUCCEEDED,
                               response_json: "{}",
                               reconciliation_id: "123",
                               redirect_url: redirect_url,
                               "success?" => true)
      allow_any_instance_of(PaymentsOS::ApiClient).to receive(:send_request).with(:charge, anything).and_return(charge_response)

      transaction = @service.pay
      expect(transaction.otp_redirect_url).to eq(redirect_url)
    end

    it "should not return redirect_url when payment endpoint fails" do
      payment_response = double(:payment, "success?" => false, response_json: '{"sample" : "true"}')
      allow_any_instance_of(PaymentsOS::ApiClient).to receive(:send_request).with(:payment, anything).and_return(payment_response)
      transaction = @service.pay
      expect(transaction.otp_redirect_url).to be_nil
      expect(transaction.payment_status).to eq(PaymentsOSTransaction::FAILED)
    end

    it "should not return redirect_url when charge endpoint fails" do
      payment_response = double(:payment, id: 1, status: PaymentsOSTransaction::CREDITED, response_json: "{}", "success?" => true)
      allow_any_instance_of(PaymentsOS::ApiClient).to receive(:send_request).with(:payment, anything).and_return(payment_response)
      charge_response = double(:charge, "success?" => false, response_json: "{}")
      allow_any_instance_of(PaymentsOS::ApiClient).to receive(:send_request).with(:charge, anything).and_return(charge_response)

      transaction = @service.pay
      expect(transaction.otp_redirect_url).to be_nil
      expect(transaction.charge_status).to eq(PaymentsOSTransaction::FAILED)
    end

    it "should make a successful recurring payment without redirect" do
      payment_response = double(:payment, id: 1, status: PaymentsOSTransaction::CREDITED, response_json: "{}", "success?" => true)
      allow_any_instance_of(PaymentsOS::ApiClient).to receive(:send_request).with(:payment, anything).and_return(payment_response)
      charge_response = OpenStruct.new(
          {
              status: 200,
              body: {
                  result: {
                      status: 'Succeed'
                  }
              }.to_json
          })
      charge = PaymentsOS::Responses::Charge.new(charge_response)
      allow_any_instance_of(PaymentsOS::ApiClient).to receive(:send_request).with(:recurring_charge, anything).and_return(charge)

      transaction = @service.pay(true)
      expect(transaction.otp_redirect_url).to be_nil
      expect(transaction.charge_status).to eq(PaymentsOSTransaction::SUCCEEDED)
    end

  end

end
