require "rails_helper"

describe PaymentsOS::RefundService do
  describe "#refund" do
    let(:payos_transaction) { create(:payments_os_transaction) }
    it "initiates a refund process for a valid transaction" do
      allow_any_instance_of(PaymentsOS::RefundService).to receive(:refund).and_return(
        PaymentsOS::Responses::Refund.new(
          Faraday::Response.new(
            body: JSON.dump(
              "id": "821da472-4c5e-4bb1-a455-30a527a1cde6",
              "created": "1585408311625",
              "result": {
                "status": "Pending"
              },
              "provider_data": {
                "provider_name": "PayUIndia",
                "description": "Refund Request Queued",
                "raw_response": JSON.dump(
                  "paymentId" => "403993715520769041",
                  "amount" => "69.69",
                  "referenceId" => "8d9b2d2a9dc51d6e0f80da",
                  "status" => "PENDING",
                  "refundId" => "130469491",
                  "currency" => "INR",
                  "message" => "Refund Request Queued"
                ),
                "external_id": "130469491"},
                "amount": 1000,
                "provider_configuration": {
                  "id": "9faf8706-8d5d-4e9d-8c76-01b56439b13f",
                  "name": "Payu india",
                  "created": "1581403578500",
                  "modified": "1581489662865",
                  "account_id": "7ccba9b8-573b-4d40-93c3-32e7ad7f2ac3",
                  "provider_id": "cca51d5b-6bc9-4bb3-844a-70850d687a1b",
                  "type": "cc_processor",
                  "href": "https://api.paymentsos.com/accounts/7ccba9b8-573b-4d40-93c3-32e7ad7f2ac3/provider-configurations/9faf8706-8d5d-4e9d-8c76-01b56439b13f"
                }
            ),
            status: 200
          )
        )
      )

      refund_response = PaymentsOS::RefundService.new(payos_transaction).refund(
        1000,
        "Test",
        "The release did not go live"
      )
      expect(refund_response.success?).to be_truthy
    end
  end
end
