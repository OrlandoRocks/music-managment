require "rails_helper"

describe PaymentsOS::CustomerService do
  let!(:person) { create(:person) }

  describe "#find_or_create_customer" do
    it "creates a new payments_os_customer if the person doesn't have one" do
      allow_any_instance_of(PaymentsOS::ApiClient).to receive(:send_request).
        and_return(PaymentsOS::Responses::Customer.new(
          OpenStruct.new(
            body: JSON.dump({
              customer_reference: "a7e5712f-d087-4e47-80fe-efc45f16c61a",
              id: "28d823f5-d3a8-45df-a779-52e8b002abd4",
              created: "1584506387164",
              payment_methods: []
            }),
            status: 200
          )
      ))

      customer, _ = PaymentsOS::CustomerService.new(person).find_or_create_customer  
      expect(customer).to be_instance_of(PaymentsOSCustomer).and have_attributes(person_id: person.id)
    end

    it "fetches existing customer if the person already has one" do
      payos_customer =  create(:payments_os_customer, person: person)
      allow_any_instance_of(PaymentsOS::ApiClient).to receive(:send_request).
        and_return(PaymentsOS::Responses::Customer.new(
          OpenStruct.new(
            body: JSON.dump({
              customer_reference: payos_customer.customer_reference,
              id: payos_customer.customer_reference,
              created: "1584506387164",
              payment_methods: []
            }),
            status: 200
          )
      ))

      customer, _ = PaymentsOS::CustomerService.new(person).find_or_create_customer
      expect(customer).to be_instance_of(PaymentsOSCustomer).and have_attributes(
        person_id: person.id,
        customer_reference: payos_customer.customer_reference
      )
    end

    it "returns nil and error response if there's an API failure" do
      allow_any_instance_of(PaymentsOS::ApiClient).to receive(:send_request).and_return(
        PaymentsOS::Responses::Customer.new(OpenStruct.new(
          body: JSON.dump(more_info: "Unable to Create Customer")
        ))
      )
      customer, response = PaymentsOS::CustomerService.new(person).find_or_create_customer  
      expect(customer).to be_nil
      expect(response.errors).to eq("Unable to Create Customer")
    end
  end
end
