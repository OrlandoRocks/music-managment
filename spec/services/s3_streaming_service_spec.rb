require "rails_helper"
describe S3StreamingService do
  let!(:bucket)         { "lyric-reports" }
  let!(:bucket_path)    { "test_runs" }

  let!(:headers)        { ["header"] }
  let!(:header_data)    { [["data"]] }
  let!(:no_header_data) { ["data"] }

  let!(:file_name)      { "bro.csv" }


  describe "#initialize" do
    context "good inputs" do
      it "doesn't raise error" do
        options = {
            file_name:   file_name,
            headers:     headers,
            data:        header_data,
            bucket_name: bucket,
            bucket_path: bucket_path
        }


        expect{S3StreamingService.new(options)}.not_to raise_error
      end
    end
    context "without headers" do
      it "sets headers to nil" do
        options = {
            file_name:   file_name,
            data:        no_header_data,
            bucket_name: bucket,
            bucket_path: bucket_path
        }
        subject = S3StreamingService.new(options)

        expect(subject.headers).to eq(:no_headers_provided)
      end
    end
    context "without file_name" do
      it "raises error" do
        options = {
            data:        no_header_data,
            bucket_name: bucket,
            bucket_path: bucket_path
        }
        expect{S3StreamingService.new(options)}.to raise_error(S3StreamingService::NoFileNameProvidedError)
      end
    end
    context "without data" do
      it "raises error" do
        options = {
            file_name:   file_name,
            bucket_name: bucket,
            bucket_path: bucket_path
        }
        expect{S3StreamingService.new(options)}.to raise_error(S3StreamingService::NoDataProvidedError)
      end
    end
    context "without bucket_name" do
      it "raises error" do
        options = {
            file_name:   file_name,
            data:        no_header_data,
            bucket_path: bucket_path
        }
        expect{S3StreamingService.new(options)}.to raise_error(S3StreamingService::NoBucketFoundError)
      end
    end
    context "without bucket_path" do
      it "raises error" do
        options = {
            file_name:   file_name,
            data:        no_header_data,
            bucket_name: bucket
        }
        expect{S3StreamingService.new(options)}.to raise_error(S3StreamingService::NoBucketPathError)
      end
    end
  end
  context "streaming" do
    let!(:no_header_options)  do
      {
        file_name:   file_name,
        data:        no_header_data,
        bucket_name: bucket,
        bucket_path: bucket_path
      }
    end
    let!(:header_options)  do
      {
        file_name:   file_name,
        headers:     headers,
        data:        header_data,
        bucket_name: bucket,
        bucket_path: bucket_path
      }
    end
    let!(:no_header_subject) { S3StreamingService.new(no_header_options) }
    let!(:header_subject) { S3StreamingService.new(header_options) }
    let!(:no_header_csv) { "data\n" }
    let!(:header_csv) { "header\ndata\n" }
    describe "#write_and_upload" do
      it "streams to s3" do
        expect_any_instance_of(Aws::S3::Object).to receive(:upload_stream)
        no_header_subject.write_and_upload
      end
      context "without headers" do
        it "doesn't write headers to s3" do
          expect(CSV).to receive(:generate_line).once
          no_header_subject.write_and_upload
        end
      end
      context "with headers" do
        it "writes headers to s3" do
          expect(CSV).to receive(:generate_line).twice
          header_subject.write_and_upload
        end
      end
    end
  end
end
