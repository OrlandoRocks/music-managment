require 'rails_helper'

RSpec.describe VatAssesmentService, type: :service do
  describe  '#acceptable?' do
    it "returns true if vat number is not taken" do
      person = create(:person)
      vat_number = '12345'
      allow_any_instance_of(VatAssesmentService)
        .to receive(:vat_already_taken?)
        .and_return(false)

      expect(VatAssesmentService
        .new(person, vat_number)
        .acceptable?)
        .to be true
    end

    it "returns true if there exist another user with same vat number and address" do
      person1 = create(:person, name: "name1")
      vat_info = create(:vat_information, person: person1)
      person2 = create(:person, name: "name2")

      expect(VatAssesmentService
        .new(person2, vat_info.vat_registration_number)
        .acceptable?)
        .to be true
    end

    it "returns false if there exist another user with same vat number and different address" do
      person1 = create(:person, name: "name1")
      vat_info = create(:vat_information, person: person1)
      person2 = create(:person, name: "name2", address2: "random address")

      expect(VatAssesmentService
        .new(person2, vat_info.vat_registration_number)
        .acceptable?)
        .to be false
    end

    context "transient address given" do
      let(:transient_address) do
        {
          address1: "address1",
          address2: "",
          city: "",
          corporate_entity_id: "2",
          country: "LU",
          customer_type: "Individual",
          state: "",
          postal_code: "12345",
          compliance_info: {
            first_name: "",
            last_name: "",
            company_name: ""
          }
        }
      end

      it "returns true if there exist another user with same vat number and address" do
        person1 = create(:person,
                         name: "name1",
                        country: "LU",
                        address1: "address1",
                        postal_code: "12345"
                        )
        vat_info = create(:vat_information, person: person1)
        person2 = create(:person, name: "name2", address1: "address1")

        expect(VatAssesmentService
          .new(person2, vat_info.vat_registration_number, transient_address)
          .acceptable?)
          .to be true
      end

      it "returns false if there exist another user with same vat number and different address" do
        person1 = create(:person, name: "name1", address1: "address1")
        vat_info = create(:vat_information, person: person1)
        person2 = create(:person, name: "name2")

        expect(VatAssesmentService
          .new(person2, vat_info.vat_registration_number, transient_address)
          .acceptable?)
          .to be false
      end
    end
  end
end
