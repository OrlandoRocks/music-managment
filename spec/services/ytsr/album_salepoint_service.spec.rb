require "rails_helper"

describe Ytsr::AlbumSalepointService do
  describe 'class.self' do
    it 'should have YT_STORE_NAMES constants' do
      expect(Ytsr::AlbumSalepointService::YT_STORE_NAMES.sort).to eq([
        "YouTube Music / YouTube Shorts",
        "YouTube Content ID Monetization"
      ].sort)
    end

    it 'should have YT_STORE_SHORT_NAMES constants' do
      expect(Ytsr::AlbumSalepointService::YT_STORE_SHORT_NAMES.sort).to eq([
        "YTMusic",
        "YoutubeSR"
      ].sort)
    end
  end

  describe '#call' do
    context 'with person with ytsr in cart' do
      let(:person) { create(:person, :with_ytsr) }
      let(:album) { person.albums.first }

      context "with 'remove from cart' params" do
        let(:remove_params) { { salepoints: 'remove_salepoints', album_id: album.id } }

        it 'should remove salepoint' do
          expect(album.salepoints.size).to eq 1

          Ytsr::AlbumSalepointService.new(
            person: person,
            params: remove_params
          ).call

          expect(album.salepoints.size).to eq 0
        end
      end
    end

    context 'with person with empty cart' do
      let(:person) { create(:person, :with_album_in_cart) }
      let(:album) { person.albums.first }

      context "with 'add_salepoints' params" do
        let(:add_to_cart_params) { { salepoints: 'add_salepoints', album_id: album.id } }
        let(:youtube_store_names) { Ytsr::AlbumSalepointService::YT_STORE_NAMES.sort }

        it 'should add salepoint' do
          expect(album.salepoints.size).to eq 0

          Ytsr::AlbumSalepointService.new(
            person: person,
            params: add_to_cart_params
          ).call

          album.reload
          created_salepoint_store_names = album.salepoints.map { |salepoint| salepoint.store.name }.sort

          expect(album.salepoints.size).to eq 2
          expect(created_salepoint_store_names).to eq(youtube_store_names)
        end
      end

      context "with invalid params" do
        let(:invalid_params_1) { {} }
        let(:invalid_params_2) { { salepoints: 'im a bad imput' } }
        let(:invalid_params_3) { { salepoints: 'add_salepoints', album_id: nil } }

        it "should return ':invalid_params'" do
          call_1 = Ytsr::AlbumSalepointService.new(
            person: person,
            params: invalid_params_1
          ).call

          call_2 = Ytsr::AlbumSalepointService.new(
            person: person,
            params: invalid_params_2
          ).call

          call_3 = Ytsr::AlbumSalepointService.new(
            person: person,
            params: invalid_params_3
          ).call

          expect(call_1).to eq(:invalid_params)
          expect(call_2).to eq(:invalid_params)
          expect(call_3).to eq(:invalid_params)
        end
      end
    end
  end
end
