require "rails_helper"

describe Ytsr::Track do
  describe ".create" do
    context "when an album is not approved for distribution" do
      let(:person) { create(:person, :with_live_album, :with_ytsr) }
      let(:album)  { person.albums.first }

      it "should return false" do
        album.update(legal_review_state: "NEEDS REVIEW")
        expect(Ytsr::Track.create(album.id)).to eq false
      end
    end

    context "when a person has not purchased youtube monetization" do
      let(:person) { create(:person, :with_live_album) }
      let(:album)  { person.albums.first }

      it "should return false" do
        album.update(legal_review_state: "APPROVED")
        allow(person).to receive(:has_purchased_ytm?) { false }
        expect(Ytsr::Track.create(album.id)).to eq false
      end
    end

    context "when an album is approved and the customer has ytsr" do
      it "should update the status of the youtube_salepoint to complete" do
        person = create(:person, :with_ytsr)
        album  = person.albums.first
        Ytsr::Track.create(album.id)
        youtube_salepoint = album.salepoints.first
        expect(youtube_salepoint.status).to eq "complete"
      end

      context "when an album has a ytsr salepoint" do
        let(:person) { create(:person, :with_ytsr) }
        let(:album)  { person.albums.first }

        it "should not create a ytsr salepoint" do
          expect(Salepoint).not_to receive(:add_youtubesr_store)
          Ytsr::Track.create(album.id)
        end
      end

      context "when an album does not have a ytsr salepoint" do
        let(:person)            { create(:person, :with_ytsr_purchase) }
        let(:album)             { person.albums.first }
        let(:ytm_purchase)      { person.youtube_monetization.purchase }
        let(:youtube_salepoint) { double(:youtube_salepoint) }

        it "should create a ytsr salepoint" do
          album.salepoints.destroy_all
          allow(youtube_salepoint).to receive(:update) { true }
          person.youtube_monetization.update_attribute(:effective_date, "2017-12-01")
          expect(Salepoint).to receive(:add_youtubesr_store).with(album, ytm_purchase) { youtube_salepoint }
          Ytsr::Track.create(album.id)
        end
      end

      context "when an albums track has" do
        let(:person)           { create(:person, :with_ytsr) }
        let(:album)            { person.albums.first }
        let(:ineligible_genre) { create(:genre, you_tube_eligible: false) }

        before do
          album.songs << create(:song, name: "Celluloid Heroes")
        end

        context "an invalid primary genre" do
          it "should create rejected salepoints for releases with an invalid primary genre" do
            album.update(primary_genre: ineligible_genre)
            Ytsr::Track.create(album.id)
            salepoint_songs_state = album.songs.flat_map(&:salepoint_songs).map(&:state).uniq
            expect(salepoint_songs_state).to eq ["rejected"]
          end
        end

        context "an invalid secondary genre" do
          it "should create rejected salepoints for releases with an invalid secondary genre" do
            album.update(primary_genre: create(:genre, you_tube_eligible: true), secondary_genre: ineligible_genre)
            Ytsr::Track.create(album.id)
            salepoint_songs_state = album.songs.flat_map(&:salepoint_songs).map(&:state).uniq
            expect(salepoint_songs_state).to eq ["rejected"]
          end
        end

        context "an invalid album name" do
          it "should create rejected salepoints for releases with an invalid album name" do
            album.update(name: "I Am Nature Sounds")
            Ytsr::Track.create(album.id)
            salepoint_songs_state = album.songs.flat_map(&:salepoint_songs).map(&:state).uniq
            expect(salepoint_songs_state).to eq ["rejected"]
          end
        end

        context "an invalid track name" do
          it "should create rejected salepoints for releases with an invalid song name" do
            album.songs.first.update(name: "This is Video Game")
            Ytsr::Track.create(album.id)
            salepoint_songs_state = album.songs.flat_map(&:salepoint_songs).map(&:state).uniq
            expect(salepoint_songs_state).to eq ["rejected"]
          end
        end
      end
    end
  end
end
