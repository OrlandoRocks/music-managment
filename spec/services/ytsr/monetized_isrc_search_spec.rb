require "rails_helper"

describe Ytsr::MonetizedIsrcSearch do
  let(:person1) { create(:person) }
  let(:person2) { create(:person) }
  let(:album1)  { create(:album, person: person1) }
  let(:album2)  { create(:album, person: person2) }
  let(:song1)   { create(:song, album: album1) }
  let(:song2)   { create(:song, album: album2) }

  describe ".search" do
    context "when given an isrc string of two songs from two different users" do

      before(:each) do
        @isrc_string = "#{song1.isrc}, #{song2.isrc}"
      end

      subject { Ytsr::MonetizedIsrcSearch.search(@isrc_string) }

      it "returns a hash with keys of the person_ids" do
        expect(subject.keys).to eq [person1.id, person2.id]
      end

      it "each person_id key's value is a hash with :person, :monetized_songs, :non_monetized_songs" do
        expect(subject[person1.id].keys).to eq [:person, :monetized_songs, :non_monetized_songs]
        expect(subject[person2.id].keys).to eq [:person, :monetized_songs, :non_monetized_songs]
      end

      it "the :person key's value is a person object" do
        expect(subject[person1.id][:person].class).to eq Person
        expect(subject[person2.id][:person].class).to eq Person
      end

      context "when a song has been monetized for youtube" do
        it "sets the :monetized_songs key's value to an array of those songs" do
          album1.update_columns(payment_applied: 1)
          album2.update_columns(payment_applied: 1)

          store = Store.find_by(id: Store::YTSR_STORE_ID)

          track_monetization1 = create(
            :track_monetization,
            song: song1,
            store: store,
            eligibility_status: 'eligible'
          )

          track_monetization2 = create(
            :track_monetization,
            song: song2,
            store: store,
            eligibility_status: 'eligible'
          )

          monetized_songs1 = subject[person1.id][:monetized_songs]

          expect(monetized_songs1.class).to eq(Array)
          expect(monetized_songs1).to match_array([song1])

          monetized_songs2 = subject[person2.id][:monetized_songs]

          expect(monetized_songs2.class).to eq(Array)
          expect(monetized_songs2).to match_array([song2])
        end

        context "when a song has an optional_isrc number" do
          it "sets the :monetized_song key's value to include that song" do
            album1.update_columns(payment_applied: 1)

            song1.update_columns(optional_isrc: "QMEU31607760")

            store = Store.find_by(id: Store::YTSR_STORE_ID)

            track_monetization1 = create(
              :track_monetization,
              song: song1,
              store: store,
              eligibility_status: 'eligible'
            )

            monetized_songs = subject[person1.id][:monetized_songs]
            non_monetized_songs = subject[person1.id][:non_monetized_songs]

            expect(monetized_songs).to include(song1)
            expect(non_monetized_songs).not_to include(song1)
          end
        end
      end

      context "when a song's isrc is NOT found in a user's monetized songs for youtube" do
        it "sets the :non_monetized_songs key's value to an array of those songs" do
          album1.update_columns(payment_applied: 1)
          album2.update_columns(payment_applied: 1)

          non_monetized_songs1 = subject[person1.id][:non_monetized_songs]

          expect(non_monetized_songs1.class).to eq(Array)
          expect(non_monetized_songs1).to match_array([song1])

          non_monetized_songs2 = subject[person2.id][:non_monetized_songs]

          expect(non_monetized_songs2.class).to eq(Array)
          expect(non_monetized_songs2).to match_array([song2])
        end
      end
    end
  end
end
