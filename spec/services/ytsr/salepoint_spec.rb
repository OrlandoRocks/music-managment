require "rails_helper"

describe Ytsr::Salepoint do
  describe ".create" do
    context "when a person does not have a paid for album" do
      let(:person) { create(:person, :with_live_album) }
      let(:album)  { person.albums.first }

      it "should invoke Ytsr::Track" do
        album.update(payment_applied: 0)
        expect(Ytsr::Track).not_to receive(:create)
        expect(Ytsr::Salepoint.create(person.id)).to eq false
      end
    end

    context "when a person has a paid for Ringtone album" do
      let(:person) { create(:person, :with_live_album) }
      let(:album)  { person.albums.first }

      it "should not invoke Ytsr::Track" do
        album.update_attribute("album_type", "Ringtone")
        expect(Ytsr::Track).not_to receive(:create)
        expect(Ytsr::Salepoint.create(person.id)).to eq false
      end
    end

    context "when a person has a paid for taken down album" do
      let(:person) { create(:person, :with_taken_down_album) }

      it "should not invoke Ytsr::Track" do
        expect(Ytsr::Track).not_to receive(:create)
        expect(Ytsr::Salepoint.create(person.id)).to eq false
      end
    end

    context "when a person has a paid for deleted album" do
      let(:person) { create(:person, :with_live_album) }
      let(:album)  { person.albums.first }

      it "should not invoke Ytsr::Track" do
        album.update_attribute("is_deleted", true)
        expect(Ytsr::Track).not_to receive(:create)
        expect(Ytsr::Salepoint.create(person.id)).to eq false
      end
    end

    context "when a person has eligible albums for a ytsr salepoint" do
      let(:person)     { create(:person, :with_live_album) }
      let(:album)      { person.albums.first }
      let(:ytsr_store) { Store.find_by(abbrev: "ytsr") }

      context "with an existing ytsr salepoint" do
        it "invokes Ytsr::Track" do
          album.salepoints << create(:salepoint, salepointable: album, store: ytsr_store, status: "new")
          expect(Ytsr::Track).to receive(:create).with(album.id)
          Ytsr::Salepoint.create(person.id)
        end
      end

      context "without an existing ytsr salepoint" do
        it "invokes Ytsr::Track" do
          expect(Ytsr::Track).to receive(:create).with(album.id)
          Ytsr::Salepoint.create(person.id)
        end
      end
    end
  end
end
