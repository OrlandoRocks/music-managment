require "rails_helper"

describe Ytsr::MonetizationService do
  describe "#call" do
    context 'with add to cart params' do
      let(:add_to_cart_params) { { monetization: "monetize" } }

      context 'person has active youtube monetization' do
        let(:person) { create(:person, :with_active_ytm) }

        it 'returns a symbol says use has ytm' do
          expect(Ytsr::MonetizationService.new(person: person, params: add_to_cart_params).call).to eq :has_ytm
        end
      end

      context 'person does not have active youtube monetization' do
        let(:person) { create(:person) }

        it 'adds to cart' do
          expect(person.ytm_in_cart?).to eq false
          Ytsr::MonetizationService.new(person: person, params: add_to_cart_params).call
          expect(person.ytm_in_cart?).to eq true
        end
      end
    end

    context 'with remove from cart params' do
      let(:remove_from_cart_params) { { monetization: "opt_out" } }
      let(:add_to_cart_params) { { monetization: "monetize" } }

      context 'person has active youtube monetization' do
        let(:person) { create(:person, :with_active_ytm) }

        it 'returns a symbol says use has ytm' do
          expect(Ytsr::MonetizationService.new(person: person, params: remove_from_cart_params).call).to eq :has_ytm
        end
      end

      context 'person does not have active youtube monetization' do
        let(:person) { create(:person) }

        it 'adds then removes from the cart' do
          expect(person.ytm_in_cart?).to eq false
          Ytsr::MonetizationService.new(person: person, params: add_to_cart_params).call
          expect(person.ytm_in_cart?).to eq true
          Ytsr::MonetizationService.new(person: person, params: remove_from_cart_params).call
          expect(person.ytm_in_cart?).to eq false
        end
      end
    end

    context "with bad params" do
      let(:person) { create(:person, :with_active_ytm) }
      let(:bad_params_string_1) { { monetize: "bad params" } }
      let(:bad_params_string_2) { { monetize: 1 } }
      let(:bad_params_empty_hash) { {} }

      it "should return a symbol stating the params are invalid" do
        expect(Ytsr::MonetizationService.new(person: person, params: bad_params_string_1).call).to eq :invalid_params
        expect(Ytsr::MonetizationService.new(person: person, params: bad_params_string_2).call).to eq :invalid_params
        expect(Ytsr::MonetizationService.new(person: person, params: bad_params_empty_hash).call).to eq :invalid_params
      end
    end
  end
end
