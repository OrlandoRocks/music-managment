require "rails_helper"

describe Takedown::BatchService do
  describe "#process_batch_takedown" do
    subject { described_class.new(album_ids: album_ids, send_email: true, opts: opts).run }
    let(:opts) { 
      {
        subject: "BatchTakedown Spec", 
        note: "Release was taken down via takedown:expired_albums task", 
        user_id: 0 
      } 
    }
    let(:album_ids) { 3.times.map { Random.rand(10) } }
    
    it "enqueues Sidekiq Workers for each album_id" do
      expect(Takedown::ExpiredRenewalWorker).to receive(:perform_async).exactly(3).times.with(an_instance_of(Integer), true, opts)
      subject
    end 
  end
end
