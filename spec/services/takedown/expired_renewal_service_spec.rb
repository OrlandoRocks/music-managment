require "rails_helper"

describe Takedown::ExpiredRenewalService do
  describe "#process_renewal_takedown" do
    let(:expired_album) { create(:album, :with_one_year_renewal_expired, :with_petri_bundle) }
    let(:live_album) { create(:album, :with_one_year_renewal, :with_petri_bundle)}
    let(:opts) { 
      {
        subject: "BatchTakedown Spec", 
        note: "Release was taken down via takedown:expired_albums task", 
        user_id: 0 
      } 
    }

    it "takes down albums" do
      expect(BatchNotifier).to_not receive(:takedown)
      Takedown::ExpiredRenewalService.new(expired_album.id, false, opts).run
      expect(expired_album.reload.takedown_at).to_not eq(nil)
    end 

    it "sends notification email" do
      expect(BatchNotifier).to receive(:takedown).and_call_original
      Takedown::ExpiredRenewalService.new(expired_album.id, true, opts).run 
      expect(expired_album.reload.takedown_at).to_not eq(nil)
    end
    
    it "notifies Airbrakes when takedown fails" do
      allow_any_instance_of(Album).to receive(:takedown!).and_return(false)
      expect(BatchNotifier).to_not receive(:takedown)
      expect(Airbrake).to receive(:notify).once
      Takedown::ExpiredRenewalService.new(expired_album.id, true, opts).run
    end 

    it "does not take down albums with plans" do
      create(:person_plan, person: expired_album.person)
      Takedown::ExpiredRenewalService.new(expired_album.id, false, opts).run
      expect(expired_album.reload.takedown_at).to eq(nil)
    end 

    it "does not take down albums that are not expired" do
      expect_any_instance_of(Album).to_not receive(:takedown!) 
      Takedown::ExpiredRenewalService.new(live_album.id, false, opts).run
    end 
  end 
end 