require "rails_helper"

describe "vat_posting" do
  before do
    @person = create(:person)
    @intake_records = create_intake_records(@person)
  end
  context "sip vat posting" do
    it "return vat is not applicable for the person" do
      mock_vat_applicable_countries_api
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
      sales_record_master_ids = @intake_records[:sales_record_master_id]
      you_tube_royalty_record_ids = @intake_records[:you_tube_royalty_record_ids]
      expect(VatTaxAdjustment.exists?).to eq false
      vat_posting_processor = SipVatProcessor.new(
        @person.id,
        sales_record_master_ids,
        you_tube_royalty_record_ids
      )
      vat_posting_processor.process!
      expect(VatTaxAdjustment.exists?).to eq false
      expect(vat_posting_processor.message).to eq "VAT is not applicable for the person"
    end

    it "return vat is applicable for the person" do
      @person.update(country: "Luxembourg")
      mock_vat_applicable_countries_api
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
      sales_record_master_ids = @intake_records[:sales_record_master_id]
      you_tube_royalty_record_ids = @intake_records[:you_tube_royalty_record_ids]
      expect(VatTaxAdjustment.exists?).to eq false
      vat_posting_processor = SipVatProcessor.new(
        @person.id,
        sales_record_master_ids,
        you_tube_royalty_record_ids
      )
      vat_posting_processor.process!
      expect(VatTaxAdjustment.exists?).to eq true
      expect(vat_posting_processor.message).to eq "Successfully completed VAT posting"
    end

    it "return vat tax feature is disabled" do
      sales_record_master_ids = @intake_records[:sales_record_master_id]
      you_tube_royalty_record_ids = @intake_records[:you_tube_royalty_record_ids]
      vat_posting_processor = SipVatProcessor.new(
        @person.id,
        sales_record_master_ids,
        you_tube_royalty_record_ids
      )
      vat_posting_processor.process!
      expect(VatTaxAdjustment.exists?).to eq false
      expect(vat_posting_processor.message).to include 'VAT Tax feature is currently disabled'
    end
  end
end

def create_intake_records(person)
  you_tube_royalty_intake = create(:you_tube_royalty_intake, person: person)
  you_tube_royalty_record_ids = you_tube_royalty_intake.you_tube_royalty_records.pluck(:id)
  sip_store = create(:sip_store)
  sales_record_master = create(:sales_record_master, sip_store: sip_store)
  person_intake = create(:person_intake, person: person)
  create(:person_intake_sales_record_master, person_intake: person_intake, sales_record_master: sales_record_master)
  { sales_record_master_id: [sales_record_master.id], you_tube_royalty_record_ids: you_tube_royalty_record_ids }
end

def mock_vat_applicable_countries_api
  allow_any_instance_of(TcVat::Base)
    .to receive(:vat_applicable_countries)
    .and_return(
      'business' => [{
        'country' => 'Luxembourg',
        'tax_rate' => 10
      }],
      'individual' => []
    )
end
