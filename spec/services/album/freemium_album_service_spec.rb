require "rails_helper"

describe Album::FreemiumAlbumService do
    let(:album) { create(:album) }
    subject { Album::FreemiumAlbumService.call(album) }
  
    context "facebook" do

        context "feature flag enabled" do

            context "active offer" do

                context "strictly facebook release" do

                    it "returns true" do
                        allow(Store).to receive(:discovery_platforms).and_return([])
                        expect_any_instance_of(Album::FreemiumAlbumService).to receive(:tiktok_freemium?).and_return(false)
                        expect_any_instance_of(Album::FreemiumAlbumService).to receive(:general_discovery?).and_return(false)
                        expect(FeatureFlipper).to receive(:show_feature?).with(:freemium_flow, album.person).at_least(:once).and_return(true)
                        expect(TargetedProductStore).to receive(:active_offer?).with(album.person.country_website_id, :facebook).and_return(true)
                        expect(album).to receive(:strictly_facebook_release?).and_return(true)
                        expect(subject).to be(true)
                    end

                end

                context "not strictly facebook release" do

                    it "returns false" do
                        allow(Store).to receive(:discovery_platforms).and_return([])
                        expect_any_instance_of(Album::FreemiumAlbumService).to receive(:tiktok_freemium?).and_return(false)
                        expect_any_instance_of(Album::FreemiumAlbumService).to receive(:general_discovery?).and_return(false)
                        expect(FeatureFlipper).to receive(:show_feature?).with(:freemium_flow, album.person).at_least(:once).and_return(true)
                        expect(TargetedProductStore).to receive(:active_offer?).with(album.person.country_website_id, :facebook).and_return(true)
                        expect(album).to receive(:strictly_facebook_release?).and_return(false)
                        expect(subject).to be(false)
                    end

                end

            end

            context "no active offer" do
                
                it "returns false" do
                    allow(Store).to receive(:discovery_platforms).and_return([])
                    expect_any_instance_of(Album::FreemiumAlbumService).to receive(:tiktok_freemium?).and_return(false)
                    expect_any_instance_of(Album::FreemiumAlbumService).to receive(:general_discovery?).and_return(false)
                    expect(FeatureFlipper).to receive(:show_feature?).with(:freemium_flow, album.person).at_least(:once).and_return(true)
                    expect(TargetedProductStore).to receive(:active_offer?).with(album.person.country_website_id, :facebook).and_return(false)
                    expect(album).to_not receive(:strictly_facebook_release?)
                    expect(subject).to be(false)
                end

            end
        end

        context "feature flag disabled" do

            it "returns false" do
                allow(Store).to receive(:discovery_platforms).and_return([])
                expect_any_instance_of(Album::FreemiumAlbumService).to receive(:tiktok_freemium?).and_return(false)
                expect_any_instance_of(Album::FreemiumAlbumService).to receive(:general_discovery?).and_return(false)
                expect(FeatureFlipper).to receive(:show_feature?).with(:freemium_flow, album.person).at_least(:once).and_return(false)
                expect(TargetedProductStore).to_not receive(:active_offer?).with(album.person.country_website_id, :facebook)
                expect(album).to_not receive(:strictly_facebook_release?)
                expect(subject).to be(false)
            end

        end
    end

    context "general_discovery" do

        context "feature flag enabled" do

            context "active offer" do

                context "album is general discovery release" do

                    it "returns true" do
                        allow(Store).to receive(:discovery_platforms).and_return([])
                        expect_any_instance_of(Album::FreemiumAlbumService).to receive(:tiktok_freemium?).and_return(false)
                        expect(FeatureFlipper).to receive(:show_feature?).with(:discovery_platforms, album.person).at_least(:once).and_return(true)
                        expect(TargetedProductStore).to receive(:active_offer?).with(album.person.country_website_id, :discovery_platforms).and_return(true)
                        expect(album).to receive(:is_general_discovery_release?).and_return(true)
                        expect(subject).to be(true)
                    end

                end

                context "album is not general discovery release" do

                    it "returns false" do
                        allow(Store).to receive(:discovery_platforms).and_return([])
                        expect_any_instance_of(Album::FreemiumAlbumService).to receive(:tiktok_freemium?).and_return(false)
                        expect_any_instance_of(Album::FreemiumAlbumService).to receive(:facebook_freemium?).and_return(false)
                        expect(FeatureFlipper).to receive(:show_feature?).with(:discovery_platforms, album.person).at_least(:once).and_return(true)
                        expect(TargetedProductStore).to receive(:active_offer?).with(album.person.country_website_id, :discovery_platforms).and_return(true)
                        expect(album).to receive(:is_general_discovery_release?).and_return(false)
                        expect(subject).to be(false)
                    end

                end

            end

            context "no active offer" do
                
                it "returns false" do
                    allow(Store).to receive(:discovery_platforms).and_return([])
                    expect_any_instance_of(Album::FreemiumAlbumService).to receive(:tiktok_freemium?).and_return(false)
                    expect_any_instance_of(Album::FreemiumAlbumService).to receive(:facebook_freemium?).and_return(false)
                    expect(FeatureFlipper).to receive(:show_feature?).with(:discovery_platforms, album.person).at_least(:once).and_return(true)
                    expect(TargetedProductStore).to receive(:active_offer?).with(album.person.country_website_id, :discovery_platforms).and_return(false)
                    expect(album).to_not receive(:is_general_discovery_release?)
                    expect(subject).to be(false)
                end

            end
        end

        context "feature flag disabled" do

            it "returns false" do
                allow(Store).to receive(:discovery_platforms).and_return([])
                expect_any_instance_of(Album::FreemiumAlbumService).to receive(:tiktok_freemium?).and_return(false)
                expect_any_instance_of(Album::FreemiumAlbumService).to receive(:facebook_freemium?).and_return(false)
                expect(FeatureFlipper).to receive(:show_feature?).with(:discovery_platforms, album.person).at_least(:once).and_return(false)
                expect(TargetedProductStore).to_not receive(:active_offer?).with(album.person.country_website_id, :discovery_platforms)
                expect(album).to_not receive(:is_general_discovery_release?)
                expect(subject).to be(false)
            end

        end

    end

    context "tiktok" do

        context "feature flag enabled" do

            context "active offer" do

                context "album is tiktok release" do

                    # impossible condition

                end

                context "album is not tiktok release (album can never be tiktok release because is_tiktok_release? method does not exist" do

                    it "returns false" do
                        allow(Store).to receive(:discovery_platforms).and_return([])
                        expect_any_instance_of(Album::FreemiumAlbumService).to receive(:general_discovery?).and_return(false)
                        expect_any_instance_of(Album::FreemiumAlbumService).to receive(:facebook_freemium?).and_return(false)
                        expect(FeatureFlipper).to receive(:show_feature?).with(:tiktok_releases, album.person).at_least(:once).and_return(true)
                        expect(TargetedProductStore).to receive(:active_offer?).with(album.person.country_website_id, :tiktok).and_return(true)
                        expect(subject).to be(false)
                    end

                end

            end

            context "no active offer" do
                
                it "returns false" do
                    allow(Store).to receive(:discovery_platforms).and_return([])
                    expect_any_instance_of(Album::FreemiumAlbumService).to receive(:general_discovery?).and_return(false)
                    expect_any_instance_of(Album::FreemiumAlbumService).to receive(:facebook_freemium?).and_return(false)
                    expect(FeatureFlipper).to receive(:show_feature?).with(:tiktok_releases, album.person).at_least(:once).and_return(true)
                    expect(TargetedProductStore).to receive(:active_offer?).with(album.person.country_website_id, :tiktok).and_return(false)
                    expect(subject).to be(false)
                end

            end
        end

        context "feature flag disabled" do

            it "returns false" do
                allow(Store).to receive(:discovery_platforms).and_return([])
                expect_any_instance_of(Album::FreemiumAlbumService).to receive(:general_discovery?).and_return(false)
                expect_any_instance_of(Album::FreemiumAlbumService).to receive(:facebook_freemium?).and_return(false)
                expect(FeatureFlipper).to receive(:show_feature?).with(:tiktok_releases, album.person).at_least(:once).and_return(false)
                expect(FeatureFlipper).to receive(:show_feature?).with(:tiktok_freemium, album.person).at_least(:once).and_return(false)
                expect(TargetedProductStore).to_not receive(:active_offer?).with(album.person.country_website_id, :tiktok)
                expect(subject).to be(false)
            end

        end

    end

    context "none" do
        it "returns false" do
            expect_any_instance_of(Album::FreemiumAlbumService).to receive(:general_discovery?).and_return(false)
            expect_any_instance_of(Album::FreemiumAlbumService).to receive(:facebook_freemium?).and_return(false)
            expect_any_instance_of(Album::FreemiumAlbumService).to receive(:tiktok_freemium?).and_return(false)
            expect(subject).to be(false)
        end
    end

end
