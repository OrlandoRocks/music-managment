require "rails_helper"

describe Album::DolbyAtmosCartService do
  let!(:person) { create(:person) }
  let!(:album) { create(:album, person: person) }
  let!(:song) { create(:song, :with_s3_asset, album: album, name: "Dobby Atlas Train") }
  subject { described_class.call(person, album) }
  context "the album has dolby atmos tracks" do
    before(:each) do
      create(:spatial_audio, song: song)
    end
    it "returns :success" do
      expect(subject).to be(:success)
    end
    it "adds dolby atmos track purchases to the cart" do
      subject
      expect(song.spatial_audio.purchase.present?).to be(true)
    end
    context "add_to_cart rollsback" do
      before(:each) do
        allow(Product).to receive(:add_to_cart).and_raise(ActiveRecord::Rollback)
      end
      it "returns :failure" do
        expect(subject).to be(:failure)
      end
    end
  end
  context "the album does not have dolby atmos tracks" do
    it "returns :no_dolby_tracks" do
      expect(subject).to be(:no_dolby_tracks)
    end
  end
end
