require "rails_helper"

describe Album::AutomatorService do
  describe ".update_automator" do
    context "when deliver_automator is set to true" do
      it "should call update_automator" do
        album = create(:album)

        expect(album).to receive(:add_automator)

        Album::AutomatorService.update_automator(album, true)
      end
    end

    context "when deliver_automator is set to false" do
      it "should delete the automator subscription" do
        album = create(:album)
        album.create_salepoint_subscription(is_active:  true)

        Album::AutomatorService.update_automator(album, false)

        expect(album.reload.salepoint_subscription).to be_nil
      end

      it "should not delete the automator subscription when finalized" do
        album = create(:album)
        album.create_salepoint_subscription(is_active:  true, finalized_at: Date.current)

        Album::AutomatorService.update_automator(album, false)

        expect(album.reload.salepoint_subscription).to be_present
      end
    end
  end

  describe ".add_and_finalize_automator" do
    it "should add and finalize automator" do
      album = create(:album)
      Album::AutomatorService.add_and_finalize_automator(album)
      salepoint_subscription = album.reload.salepoint_subscription

      expect(salepoint_subscription).to be_present
      expect(salepoint_subscription.finalized_at).to_not be_nil
    end

    it "should not delete automator if already present and finalized" do
      album = create(:album)
      album.create_salepoint_subscription(is_active:  true, finalized_at: Date.current)

      Album::AutomatorService.add_and_finalize_automator(album)
      salepoint_subscription = album.reload.salepoint_subscription

      expect(salepoint_subscription).to be_present
      expect(salepoint_subscription.finalized_at).to_not be_nil
    end
  end
end
