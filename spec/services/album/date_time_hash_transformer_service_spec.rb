require "rails_helper"

describe Album::DateTimeHashTransformerService do
  describe "::translate_date_time_hash" do
    context "when a date_time_hash is passed in" do
      context "when the date_time_hash is valid" do
        context "when the date_time_hash has stringified keys" do
          it "returns a valid Time object in local time" do
            date_time_hash = {
              "month"=>"6",
              "day"=>"6",
              "year"=>"2019",
              "hour"=>"12",
              "min"=>"00",
              "meridian"=>"AM"
            }

            result = Album::DateTimeHashTransformerService.datetime_hash_to_iso(date_time_hash)

            expect(result).to eq(Time.new(2019, 6, 5, 23, 0, 0, "-05:00").getlocal.iso8601)
          end
        end

        context "when the date_time_hash has symbolized keys" do
          it "returns a valid Time object in local time" do
            date_time_hash = {
              month: "6",
              day: "6",
              year: "2019",
              hour: "12",
              min: "00",
              meridian: "AM"
            }

            result = Album::DateTimeHashTransformerService.datetime_hash_to_iso(date_time_hash)

            expect(result).to eq(Time.new(2019, 6, 5, 23, 0, 0, "-05:00").getlocal.iso8601)
          end
        end
      end

      context "when the date_time_hash is invalid" do
        it "raises an ArgumentError" do
          date_time_hash = {
            month: nil,
            day: "6",
            year: "2019",
            hour: "12",
            min: "00",
            meridian: "AM"
          }

          expect {
            Album::DateTimeHashTransformerService.datetime_hash_to_iso(date_time_hash)
          }.to raise_error(ArgumentError)
        end
      end
    end

    context "when a date_time_hash is not passed in" do
      it "returns nil" do
        date_time_hash = nil

        result = Album::DateTimeHashTransformerService.datetime_hash_to_iso(date_time_hash)

        expect(result).to eq(nil)
      end
    end
  end
end
