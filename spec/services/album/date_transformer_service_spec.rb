require "rails_helper"

describe Album::DateTransformerService do
  describe '#translate_release_date_time' do
    context 'album has a valid golive_date' do
      it 'returns a valid Time object in local time' do
        album = build(
          :album,
          golive_date: DateTime.parse('2019-07-02-11:22'),
          timed_release_timing_scenario: 1
        )

        album.golive_date = Album::DateTransformerService
                              .translate_release_date_time(album)

        expect(album.golive_date).to eq(
          Time.new(2019, 7, 2, 11, 22, 0, "-04:00").getlocal.iso8601
        )
      end
    end

    context 'album has an invalid golive_date' do
      it 'raises an ArgumentError' do
        bogus_album = double(
          'album',
          {
            golive_date: {
              year: 2019,
              month: 14,   # this is the invalid part
              day: 2,
              hour: 11,
              min: 22,
              meridian: 'AM'
            },
            timed_release_timing_scenario: 1
          }
        )

        expect {
          Album::DateTransformerService.translate_release_date_time(bogus_album)
        }.to raise_error(ArgumentError)
      end
    end

    context 'album is missing part of its golive_date' do
      it 'raises an ArgumentError' do
        bogus_album = double(
          'album',
          {
            golive_date: {
              year: 2019,
              month: 12,
              day: nil,    # this is the invalid part
              hour: 11,
              min: 22,
              meridian: 'AM'
            },
            timed_release_timing_scenario: 1
          }
        )

        expect {
          Album::DateTransformerService.translate_release_date_time(bogus_album)
        }.to raise_error(ArgumentError)
      end
    end
  end
end
