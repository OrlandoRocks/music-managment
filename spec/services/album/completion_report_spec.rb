require "rails_helper"

describe Album::CompletionReport do
  let(:person)       { create(:person) }
  let(:album)        { create(:album, person_id: person.id) }

  describe "#songs_complete?" do
    context "with songs" do
      let(:songwriter_id)     { SongRole.where(role_type: "songwriter").first.id}
      let(:song)              { create(:song, album_id: album.id) }
      let(:song_data)         { SongDataBuilder.build(person.id, song) }
      let(:song_data_reports) { [SongData::CompletionReport.new(song_data)] }

      let(:song_artists)      { [double(associated_to: "Album", credit: "primary_artist", role_ids: "#{songwriter_id}", name: "artist name")] }

      context "with complete songs" do
        before do
          allow(song_data).to receive(:name?).and_return(true)
          allow(song_data).to receive(:asset_url?).and_return(true)
          allow(song_data).to receive(:artists).and_return(song_artists)
        end
        it "is true" do
          report = Album::CompletionReport.new(person.id, album.id, song_data_reports)
          expect(report.songs_complete?).to be true
        end
      end

      context "with incomplete songs" do
        it "is false" do
          report = Album::CompletionReport.new(person.id, album.id, song_data_reports)
          expect(report.songs_complete?).to be false
        end
      end
    end

    context "without songs" do
      it "is false" do
        report = Album::CompletionReport.new(person.id, album.id, [])
        expect(report.songs_complete?).to be false
      end
    end
  end
end
