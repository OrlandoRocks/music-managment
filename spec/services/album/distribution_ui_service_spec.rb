require "rails_helper"

describe Album::DistributionUIService do
    let(:album) { create(:album) }
    subject { Album::DistributionUIService.call(album) }
  
    context "Album is paid" do

        it "returns a template hash for a :distributed state" do
            expect(album).to receive(:payment_applied?).and_return(true)
            expect(Salepoint).to receive(:salepoints_to_add_to_cart).and_call_original
            expect(album).to receive(:salepoint_subscription).at_least(:once).and_call_original
            expect(Product).to receive(:price).at_least(:once).and_call_original
            expect_any_instance_of(Album::DistributionUIService).to receive(:show_automator_addon_if_finalized?).and_return(false)
            expect_any_instance_of(Album::DistributionUIService).to receive(:automator_price_with_discount).with(album, album.person).and_call_original
            expect_any_instance_of(Album::DistributionUIService).to receive(:get_automator_product).with(album.person).and_call_original

            result = subject

            expect(result[:display_state]).to be(:distributed)
            expect(result[:partial]).to eq(Album::DistributionUIService::PARTIALS[:distributed])
            expect(result[:addons]).to be(Album::DistributionUIService::ATMOS_AND_AUTOMATOR_ADDONS)
            expect(result[:locals]).to include(:price, :salepoints, :album, :store_names, :automator, :automator_product, :automator_price, show_automator: false)
            expect(result[:addon_locals]).to include(:album, :automator_product, :automator_price, :show_automator)
        end

    end

    context "Album is not paid" do

        context "Album is ready" do

            context "credit is available" do
                it "returns a template hash for a :ready_credit state" do
                    expect(album).to receive(:should_finalize?).and_return(true)
                    expect_any_instance_of(Album::DistributionUIService).to receive(:album_songs_status).with(album, album.person).and_return(true)
                    expect_any_instance_of(Album::DistributionUIService).to receive(:inventory?).and_return(true)
                    expect_any_instance_of(Album::DistributionUIService).to receive(:show_automator_addon?).with(album).and_return(false)
                    expect(album).to receive(:salepoint_subscription).at_least(:once).and_call_original
                    expect(Album::PreorderPricingService).to receive(:call).with(album).and_call_original
                    expect_any_instance_of(Album::DistributionUIService).to receive(:automator_price_with_discount).with(album, album.person).and_call_original
                    expect_any_instance_of(Album::DistributionUIService).to receive(:get_automator_product).with(album.person).and_call_original
                    expect_any_instance_of(Album::DistributionUIService).to receive(:filtered_distribution_products).with(album, album.person).and_call_original

                    result = subject

                    expect(result[:display_state]).to be(:ready_credit)
                    expect(result[:partial]).to eq(Album::DistributionUIService::PARTIALS[:ready_plan])
                    expect(result[:addons]).to be(Album::DistributionUIService::ALL_ADDONS)
                    expect(result[:locals]).to include(:album, :distribution_products, :automator, :automator_product, :automator_price, :preorder_price, submit_tag_key: ".distribute_with_credit", credit_partial: true, with_credit: true, show_automator: false)
                    expect(result[:addon_locals]).to include(:album, :automator, :automator_product, :automator_price, :preorder_price, :show_automator)
                end
            end

            context "user has a plan" do
                it "returns a template hash for a :ready_plan state" do
                    expect(album).to receive(:should_finalize?).and_return(true)
                    expect_any_instance_of(Album::DistributionUIService).to receive(:album_songs_status).with(album, album.person).and_return(true)
                    expect(album.person).to receive(:owns_or_carted_plan?).at_least(:once).and_return(true)
                    expect_any_instance_of(Album::DistributionUIService).to receive(:show_automator_addon?).with(album).and_return(false)
                    expect(album).to receive(:salepoint_subscription).at_least(:once).and_call_original
                    expect(Album::PreorderPricingService).to receive(:call).with(album).and_call_original
                    expect_any_instance_of(Album::DistributionUIService).to receive(:automator_price_with_discount).with(album, album.person).and_call_original
                    expect_any_instance_of(Album::DistributionUIService).to receive(:get_automator_product).with(album.person).and_call_original

                    result = subject

                    expect(result[:display_state]).to be(:ready_plan)
                    expect(result[:partial]).to eq(Album::DistributionUIService::PARTIALS[:ready_plan])
                    expect(result[:addons]).to be(Album::DistributionUIService::ALL_ADDONS)
                    expect(result[:locals]).to include(:album, :distribution_products, :automator, :automator_product, :automator_price, :preorder_price, submit_tag_key: ".distribute_with_plan", credit_partial: false, with_credit: true, show_automator: false)
                    expect(result[:addon_locals]).to include(:album, :automator, :automator_product, :automator_price, :preorder_price, :show_automator)
                end
            end

            context "credit is not available" do
                it "returns a template hash for a :ready_no_credit state" do
                    expect(album).to receive(:should_finalize?).and_return(true)
                    expect(Album::FreemiumAlbumService).to receive(:call).and_return(false)
                    expect_any_instance_of(Album::DistributionUIService).to receive(:album_songs_status).with(album, album.person).and_return(true)
                    expect_any_instance_of(Album::DistributionUIService).to receive(:show_automator_addon?).with(album).and_return(false)
                    expect(album).to receive(:salepoint_subscription).at_least(:once).and_call_original
                    expect(Album::PreorderPricingService).to receive(:call).with(album).and_call_original
                    expect_any_instance_of(Album::DistributionUIService).to receive(:automator_price_with_discount).with(album, album.person).and_call_original
                    expect_any_instance_of(Album::DistributionUIService).to receive(:get_automator_product).with(album.person).and_call_original
                    expect_any_instance_of(Album::DistributionUIService).to receive(:filtered_distribution_products).with(album, album.person).and_call_original

                    result = subject

                    expect(result[:display_state]).to be(:ready_no_credit)
                    expect(result[:partial]).to eq(Album::DistributionUIService::PARTIALS[:ready_no_credit])
                    expect(result[:addons]).to be(Album::DistributionUIService::ALL_ADDONS)
                    expect(result[:locals]).to include(:album, :products, :automator, :automator_product, :automator_price, :preorder_price, with_credit: false, show_automator: false)
                    expect(result[:addon_locals]).to include(:album, :automator, :automator_product, :automator_price, :preorder_price, :show_automator)
                end
            end

            context "album is freemium" do
                it "returns a template hash for a :ready_no_credit state" do
                    expect(album).to receive(:should_finalize?).and_return(true)
                    expect_any_instance_of(Album::DistributionUIService).to receive(:album_songs_status).with(album, album.person).and_return(true)
                    expect(Album::FreemiumAlbumService).to receive(:call).and_return(true)
                    expect_any_instance_of(Album::DistributionUIService).to receive(:show_automator_addon?).with(album).and_return(false)
                    expect(album).to receive(:salepoint_subscription).at_least(:once).and_call_original
                    expect(Album::PreorderPricingService).to receive(:call).with(album).and_call_original
                    expect_any_instance_of(Album::DistributionUIService).to receive(:automator_price_with_discount).with(album, album.person).and_call_original
                    expect_any_instance_of(Album::DistributionUIService).to receive(:get_automator_product).with(album.person).and_call_original
                    expect_any_instance_of(Album::DistributionUIService).to receive(:filtered_distribution_products).with(album, album.person).and_call_original

                    result = subject

                    expect(result[:display_state]).to be(:ready_no_credit)
                    expect(result[:partial]).to eq(Album::DistributionUIService::PARTIALS[:ready_no_credit])
                    expect(result[:addons]).to be(Album::DistributionUIService::ALL_ADDONS)
                    expect(result[:locals]).to include(:album, :products, :automator, :automator_product, :automator_price, :preorder_price, with_credit: false)
                    expect(result[:addon_locals]).to include(:album, :automator, :automator_product, :automator_price, :preorder_price)
                end
            end

            context "credit has been applied" do
                it "returns a template hash for a :ready_already_credited state" do
                    expect(album).to receive(:should_finalize?).and_return(true)
                    expect_any_instance_of(Album::DistributionUIService).to receive(:album_songs_status).with(album, album.person).and_return(true)
                    expect(CreditUsage).to receive(:credit_usage_for).with(album.person, album).and_return(true)

                    result = subject
                    
                    expect(result[:display_state]).to be(:ready_already_credited)
                    expect(result[:partial]).to eq(Album::DistributionUIService::PARTIALS[:ready_already_credited])
                    expect(result[:addons]).to be(Album::DistributionUIService::ATMOS_ADDONS)
                    expect(result[:locals].count).to eq(2)
                    expect(result[:locals]).to include(:album, :show_automator)
                    expect(result[:addon_locals].count).to eq(2)
                    expect(result[:addon_locals]).to include(:album, :show_automator)
                end
            end     

        end

        context "album is not ready" do
            context "credit is available" do
                it "returns a template hash for a :not_ready_credit state" do
                    expect_any_instance_of(Album::DistributionUIService).to receive(:inventory?).and_return(true)
                    expect(CreditUsage).to receive(:number_credits_available).with(album.person, album.class.name).and_call_original
                    result = subject
                    expect(result[:display_state]).to be(:not_ready_credit)
                    expect(result[:partial]).to eq(Album::DistributionUIService::PARTIALS[:not_ready_credit])
                    expect(result[:addons]).to be(Album::DistributionUIService::ATMOS_ADDONS)
                    expect(result[:locals]).to include(:album, :inventory_count, :show_automator)
                    expect(result[:locals].count).to eq(3)
                    expect(result[:addon_locals]).to include(:album, :show_automator)
                    expect(result[:addon_locals].count).to eq(2)
                end
            end

            context "credit is not available" do
                it "returns a template hash for a :ready_no_credit state" do
                    result = subject
                    expect(result[:display_state]).to be(:not_ready_no_credit)
                    expect(result[:partial]).to be(nil)
                    expect(result[:addons]).to be(nil)
                    expect(result[:locals]).to be(nil)
                    expect(result[:addon_locals]).to be(nil)
                end
            end

        end
        

    end

end
