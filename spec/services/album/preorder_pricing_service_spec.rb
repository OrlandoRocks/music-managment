require "rails_helper"

describe Album::PreorderPricingService do
    let(:person) { create(:person) }
    let(:album) { create(:album) }
    let(:product) { create(:product) }
    let(:original_price) { 10 }
    let(:count) { nil }
    let(:service) { Album::PreorderPricingService.new(album, count, person) }
    let(:targeted_product) { nil }

    describe "#preorder_with_discount" do

        subject { service.preorder_with_discount }

        before do
            allow(service).to receive(:product).and_return(product)
            allow(service).to receive(:original_price).and_return(original_price)
            allow(TargetedProduct).to receive(:targeted_product_for_active_offer).and_return(targeted_product)
        end

        context "no targeted offer" do

            it "should return original price" do
                expect(subject).to eq(ActionController::Base.helpers.number_to_currency(original_price))
            end

        end

        context "has targeted offer" do
            let(:targeted_product) { TargetedProduct.new }

            it "should pass on original and discounted price to money with discount" do
                adjusted_price = 5

                allow(targeted_product)
                .to receive(:adjust_price)
                .with(original_price)
                .and_return(adjusted_price)

                allow(service)
                .to receive(:money_with_discount)
                .with(original_price, adjusted_price, product.currency)
                .and_return("discounted")

                expect(subject).to eq("discounted")
            end
        end
    end
    
    describe "#call" do

        subject { service.call }

        context "count is nil" do

            context "no preorder purchases" do
                
                it "returns 0" do
                    expect(subject).to eq(0)
                end

            end

            context "with preorder purchase" do

                before { allow(album).to receive(:preorder_purchase).and_return(create(:purchase))}

                it "calls #preorder_with_discount" do
                    expect(service).to receive(:preorder_with_discount)
                    subject
                end

            end

        end

        context "count is present" do
            let(:count){ 1 }

            it "calls #preorder_with_discount" do
                expect(service).to receive(:preorder_with_discount)
                subject
            end

        end

    end

end
