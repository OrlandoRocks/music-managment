require "rails_helper"

describe ArtistMapping::ArtistIdCreator do
  before(:each) do
    @person   = FactoryBot.create(:person)
    @artist   = FactoryBot.create(:artist)
    album     = FactoryBot.create(:album)
    @creative = FactoryBot.create(:creative, artist_id: @artist.id, creativeable_id: album.id, creativeable_type: 'Album')
    @creative.update(person_id: @person.id)

  end

  let(:without_identifier) {
      {
        "person_id" => @person.id,
        "artist_id" => @artist.id,
        "service_name" => "apple"
      }
    }

  describe ".create_artist_id" do
    before(:each) do
      allow(Apple::ArtistIdCreator).to receive(:create_artist_id)
    end
    it "calls on the correct store's artist ID creator" do
      expect(Apple::ArtistIdCreator).to receive(:create_artist_id).with(@person.id, @artist.id, nil)
      ArtistMapping::ArtistIdCreator.create_artist_id(without_identifier)
    end

    it "updates the associated external service ID recrods to the 'requested' state" do
      ArtistMapping::ArtistIdCreator.create_artist_id(without_identifier)
      ExternalServiceId.artist_ids_for(@person.id, @artist.id, "apple").each do |es|
        expect(es.state).to eq("requested")
      end
    end

    context "fails to send to store" do
      it "updates the external service IDs to a state of 'error'" do
        ExternalServiceId.create(linkable_type: "Creative", linkable_id: @creative.id, service_name: "apple")
        allow(Apple::ArtistIdCreator).to receive(:create_artist_id).and_raise("some error")
        ArtistMapping::ArtistIdCreator.create_artist_id(without_identifier)
        ExternalServiceId.artist_ids_for(@person.id, @artist.id, "apple").each do |es|
          expect(es.state).to eq("error")
        end
      end
    end
  end
end
