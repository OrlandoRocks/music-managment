require "rails_helper"

describe ArtistMapping::ArtistIdUpdater do
  describe ".update_artist_id" do
    before(:each) do
      @person   = FactoryBot.create(:person)
      @artist   = FactoryBot.create(:artist)
      album    = FactoryBot.create(:album)
      FactoryBot.create(:creative, artist_id: @artist.id, creativeable_id: album.id, creativeable_type: 'Album').update(person_id: @person.id)
    end

    let(:with_identifier) {
        {
          "person_id" => @person.id,
          "artist_id" => @artist.id,
          "service_name" => "apple",
          "identifier" => "12345"
        }
      }
    it "calls on the given store's artist ID updater" do
      allow(Apple::ArtistIdUpdater).to receive(:update_artist_id)
      expect(Apple::ArtistIdUpdater).to receive(:update_artist_id).with(with_identifier)
      ArtistMapping::ArtistIdUpdater.update_artist_id(with_identifier)
    end
  end
end
