require "rails_helper"

describe ArtistMapping::AlbumRedistributor do
  before(:each) do
    @person      = FactoryBot.create(:person)
    @artist      = FactoryBot.create(:artist)
    album        = FactoryBot.create(:album)
    salepoint    = FactoryBot.create(:salepoint, salepointable: album)
    creative     = FactoryBot.create(:creative, artist_id: @artist.id, creativeable_id: album.id, creativeable_type: 'Album').update(person_id: @person.id)
    petri_bundle = FactoryBot.build(:petri_bundle, album_id: album.id)
    petri_bundle.save
    @distro = FactoryBot.create(:distribution, converter_class: "MusicStores::Itunes::Converter", petri_bundle_id: petri_bundle.id, salepoints: [salepoint] )
  end

  let(:with_identifier) {
      {
        "person_id" => @person.id,
        "artist_id" => @artist.id,
        "service_name" => "apple",
        "identifier" => "12345"
      }
    }
  describe ".redistribute" do
    it "creates or updates the Redistribution Package" do
      ArtistMapping::AlbumRedistributor.redistribute(with_identifier)
      expect(RedistributionPackage.pending.length).to eq(1)
      expect(RedistributionPackage.pending.first.distributions.length).to eq(1)
    end
    it "retries each associated distribution" do
      allow(Delivery::Retry).to receive(:retry)
      expect(Delivery::Retry).to receive(:retry).exactly(1).times
      ArtistMapping::AlbumRedistributor.redistribute(with_identifier)
    end
  end
end
