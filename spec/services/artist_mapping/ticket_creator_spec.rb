require 'rails_helper'

describe ArtistMapping::TicketCreator do
  describe ".create_ticket" do
    before(:each) do
      @person   = create(:person)
      @artist   = create(:artist)
      album    = create(:album)
      create(:creative, artist_id: @artist.id, creativeable_id: album.id, creativeable_type: 'Album').update(person_id: @person.id)
    end

    let(:with_identifier) {
      {
          "person_id" => @person.id,
          "artist_id" => @artist.id,
          "service_name" => "apple",
          "identifier" => "12345"
      }
    }
    it "calls on the given store's artist ID updater" do
      allow(Apple::TicketCreator).to receive(:create_ticket)
      expect(Apple::TicketCreator).to receive(:create_ticket).with(with_identifier)
      ArtistMapping::TicketCreator.create_ticket(with_identifier)
    end
  end
end
