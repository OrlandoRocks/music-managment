require "rails_helper"

describe PayoutTransferReportService do
  describe ".generate_report_csv" do
    context "when provider is Payoneer" do
      let!(:payout_transfer1) { create(:payout_transfer, amount_cents: 9_500) }
      let!(:payout_transfer2) { create(:payout_transfer, amount_cents: 9_600) }
      let!(:payout_transfer3) { create(:payout_transfer, amount_cents: 10_000) }

      it "returns a CSV file with headers defined in PayoutTransferReportService::PAYONEER_REPORT_HEADERS" do
        form_params = { provider_status: PayoutTransfer::COMPLETED }
        file = PayoutTransferReportService.generate_report_csv(PayoutProvider::PAYONEER, form_params)
        content = CSV.parse(file.read, headers: true)

        expect(content.headers).to eq(PayoutTransferReportService::PAYONEER_REPORT_HEADERS)
      end

      it "includes transfers matching the given filters" do
        form_params = { filter_amount: "95.00", filter_type: ">" }
        file = PayoutTransferReportService.generate_report_csv(PayoutProvider::PAYONEER, form_params)
        content = CSV.parse(file.read, headers: true)

        expect(content.size).to eq(2)
        expect(content.map { |row| row["Payout Transfer ID"] })
          .to include(payout_transfer2.id.to_s, payout_transfer3.id.to_s)
      end
    end

    context "when provider is PayPal" do
      let(:person) { create(:person, :with_balance, amount: 500) }
      let!(:low_value_transfer) {
        create(:paypal_transfer, person: person, payment_cents: 100_00, current_balance_cents: 500_00)
      }
      let!(:high_value_transfer) {
        create(:paypal_transfer, person: person, payment_cents: 200_00, current_balance_cents: 500_00)
      }

      it "returns a CSV file with headers defined in PayoutTransferReportService::PAYPAL_REPORT_HEADERS" do
        form_params = { type: "paypal", threshold_status: "under" }
        file = PayoutTransferReportService.generate_report_csv("paypal", form_params)
        content = CSV.parse(file.read, headers: true)

        expect(content.headers).to eq(PayoutTransferReportService::PAYPAL_REPORT_HEADERS)
      end

      it "includes transfers matching the given filters" do
        allow(PaypalTransfer).to receive(:threshold_in_cents).and_return(150_00)
        form_params = { type: "paypal", threshold_status: "over" }
        file = PayoutTransferReportService.generate_report_csv("paypal", form_params)
        content = CSV.parse(file.read, headers: true)

        expect(content.size).to eq(1)
        expect(content[0]["Withdrawal Amount"]).to include("$200.00")
      end
    end
  end
end
