require "rails_helper"

RSpec.describe DownloadChannel, type: :channel do
  let!(:person) { create(:person) }

  before do
    CableAuthService.refresh_token(person)
  end

  describe "subscription" do
    it "successfully subscribes when valid channel and token are passed" do
      subscribe channel: "DownloadChannel", cable_auth_token: person.cable_auth_token.token
      expect(subscription).to be_confirmed
    end

    it "streams from a person-specific channel" do
      subscribe channel: "DownloadChannel", cable_auth_token: person.cable_auth_token.token
      expect(subscription).to have_stream_from("downloads-channel-#{person.cable_auth_token.token}")
    end
  end

  describe "fetch_report" do
    let!(:data) {
      {
        action: "fetch_report",
        params: { payout_provider: "payoneer" },
        cable_auth_token: person.cable_auth_token.token
      }.with_indifferent_access
    }
    let!(:sample_file) { Tempfile.new }

    before do
      subscribe channel: "DownloadChannel", cable_auth_token: person.cable_auth_token.token
      allow(PayoutTransferReportService).to receive(:generate_report_csv).and_return(sample_file)
    end

    it "generates report" do
      expect(subscription).to have_stream_from("downloads-channel-#{person.cable_auth_token.token}")
      expect {
        subscription.fetch_report(data)
      }.to have_broadcasted_to("downloads-channel-#{person.cable_auth_token.token}")
    end

    it "broadcasts to the streaming channel" do
      subscribe channel: "DownloadChannel", cable_auth_token: person.cable_auth_token.token
      expect(subscription).to have_stream_from("downloads-channel-#{person.cable_auth_token.token}")
      expect {
        subscription.fetch_report(data)
      }.to have_broadcasted_to("downloads-channel-#{person.cable_auth_token.token}")
    end
  end
end
