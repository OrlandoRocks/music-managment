require "rails_helper"

RSpec.describe ApplicationCable::Connection, type: :channel do
  let!(:person1) { create(:person) }
  let!(:person2) { create(:person) }

  before do
    CableAuthService.refresh_token(person1)
    CableAuthService.refresh_token(person2)
  end

  it "successfully connects" do
    connect "/cable?email=#{person1.email}&cable_auth_token=#{person1.cable_auth_token.token}"
    expect(connection.cable_auth_token).to eq person1.cable_auth_token.token
  end

  it "rejects connection when authentication params are not passed" do
    expect { connect "/cable" }.to have_rejected_connection
  end

  it "rejects connection when the person is not found" do
    expect {
      connect "/cable?email=incorrect@email.com&cable_auth_token=#{person1.cable_auth_token.token}"
    }.to have_rejected_connection
  end

  it "rejects connection for invalid token" do
    expect {
      connect "/cable?email=#{person1.email}&cable_auth_token=#{person2.cable_auth_token.token}"
    }.to have_rejected_connection
  end
end
