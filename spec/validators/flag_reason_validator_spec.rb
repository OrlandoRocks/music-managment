require "rails_helper"

RSpec.describe FlagReasonValidator, type: :validator do
  before do
    stub_const("Validatable", Class.new).class_eval do
      include ActiveModel::Validations
      attr_accessor :flag, :flag_reason
      validates_with FlagReasonValidator
    end
  end

  subject { Validatable.new }

  context "flag_reason presence" do
    context "flags with mandatory flag reason" do
      let(:flag) { Flag.with_mandatory_flag_reason.take }

      it "should be valid with flag and flag reason" do
        flag_reason = FlagReason.find_by(flag: flag)
        subject.flag = flag
        subject.flag_reason = flag_reason

        expect(subject).to be_valid
      end

      it "should not be valid without flag_reason" do
        subject.flag = flag
        subject.flag_reason = nil

        expect(subject).to_not be_valid
      end
    end

    context "flags without mandatory flag_reason" do
      let(:flag) { create(:flag) }

      it "should be valid without flag_reason" do
        subject.flag = flag
        subject.flag_reason = nil

        expect(subject).to be_valid
      end
    end
  end

  context "flag_reason belongs to flag" do
    it "should not be valid without proper flag reason" do
      flag = Flag.find_by(Flag::ADDRESS_LOCKED)
      flag_reason = FlagReason.where.not(flag: flag).take
      subject.flag = flag
      subject.flag_reason = flag_reason

      expect(subject).to_not be_valid
    end
  end
end
