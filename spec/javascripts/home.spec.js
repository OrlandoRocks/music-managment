//var $ = require('jasmine-jquery'),
var Browser = require('zombie');
    Browser.site = process.env.url || 'http://test.tunecore.com:3000';

describe('Home Page', function(){

  it('should load', function(done){
    Browser.visit('/', function(){
     done();
    });
  });

  describe('Slider', function(){

  });

  describe('Video Gallery', function(){

    it('should swap video screenshot and activate a new thumbnail upon clicking a thumbnail', function(done){
      Browser.visit('/', function(e, browser){
        browser.dump();
        browser.clickLink('.thumbs a[href="costello"]', function(){
          expect(browser.query('.thumbs .active h3').html()).toBe('Katie Costello');
          expect(browser.query('.showing #costello')).toBeTruthy();
          done();
        });
      });
    });

    it('should load a youtube video when screenshot is clicked and return to screenshot when thumbnail is clicked', function(done){
      Browser.visit('/', function(e, browser){
        browser.clickLink('.showing > a', function(){
          expect(browser.query('.showing > iframe')).toBeTruthy();
          browser.clickLink('.thumbs .active', function(){
            expect(browser.query('.showing > a')).toBeTruthy();
            done();
          });
        });
      });
    });

  });

});
