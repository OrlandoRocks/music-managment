require "rails_helper"

describe TaxWithholdings::IRSWithholdingsService, type: :service do
  it_behaves_like "ruleable"

  include ActiveSupport::Testing::TimeHelpers

  before do
    PersonTransaction.delete_all
  end

  subject(:service_class) do
    TaxWithholdings::IRSWithholdingsService
  end

  describe "when doesn't qualify for taxwithholding" do
    let(:debit_transaction) { create(:person_transaction, debit: 666.0, credit: 0)}
    let!(:withholding_exempt_user_transaction) {
      person = create(:person)
      create(:withholding_exemption, person: person, withholding_exemption_reason: WithholdingExemptionReason.find_by(name: :other))
      create(:person_transaction, credit: 70.0, debit: 0, person: person, target_type: "PersonIntake")
    }

    let(:irs_withholding) {
      create(
        :irs_tax_withholding,
        withheld_from_person_transaction: create(:person_transaction, credit: 6.0, debit: 0, person: create(:person), target_type: "PersonIntake")
      )
    }

    let(:not_us_transaction) do
      create(:person_transaction, credit: 666.0, debit: 0, person: create(:person, :with_india_country))
    end

    let(:us_transaction_with_tax_form) do
      person = create(:person)
      create(:tax_form, person: person, tax_form_type_id: 2, payout_provider_config_id: 1)
      create(:person_transaction, credit: 666.0, debit: 0, person: person, target_type: "PersonIntake")
    end

    let(:us_transaction_under_tax_threshold) do
      create(:person_transaction, credit: 4.0, debit: 0, person: create(:person), target_type: "PersonIntake")
    end

    let(:person_no_tax_forms) do
      create(:person)
    end

    let(:us_transaction_over_tax_threshold_a) do
      create(:person_transaction, credit: 5.0, debit: 0, person: person_no_tax_forms, target_type: "PersonIntake")
    end

    let(:us_transaction_over_tax_threshold_b) do
      create(:person_transaction, credit: 6.0, debit: 0, person: person_no_tax_forms, target_type: "PersonIntake")
    end

    let(:us_tax_transaction_with_no_credit) do
      create(:person_transaction, credit: 0, debit: 0, person: person_no_tax_forms, target_type: "PersonIntake")
    end

    let!(:us_person_with_no_current_w9) do
      create(:person)
    end

    let!(:us_person_with_no_current_w9_transaction) do
      create(:person_transaction, person: us_person_with_no_current_w9, credit: 50.0, debit: 0, target_type: "PersonIntake")
    end

    let!(:us_person_with_no_current_w9_forms) do
      create(
        :tax_form,
        tax_form_type_id: 2,
        person: us_person_with_no_current_w9,
        payout_provider_config_id: 1,
        submitted_at: DateTime.now.ago(10.days).to_date
      )
    end

    let!(:us_person_with_no_current_w8_forms) do
      create(
        :tax_form,
        tax_form_type_id: 1,
        payout_provider_config_id: 1,
        person: us_person_with_no_current_w9,
        submitted_at: DateTime.now.ago(5.days).to_date
      )
    end
    let!(:foreign_exchange_rate) { create(:foreign_exchange_rate, :eur) }

    let(:tax_year) { 2022 }
    let(:mar_03) { "#{tax_year}-03-03".to_datetime }
    let(:jan_15) { "#{tax_year}-01-15".to_datetime }
    let(:jan_03) { "#{tax_year}-01-03".to_datetime }
    let(:jan_02) { "#{tax_year}-01-02".to_datetime }
    let(:jan_01) { "#{tax_year}-01-01".to_datetime }
    let(:previous_dec_15) { "#{tax_year - 1}-12-15".to_datetime }

    let(:person_with_all_earning_types) do
      person = create(:person, :with_balance, amount: 100)
      ba1 = create(:balance_adjustment, person: person, category: "Songwriter Royalty", posted_by: person, posted_by_name: person.name)
      ba2 = create(:balance_adjustment, person: person, category: "Songwriter Royalty", posted_by: person, posted_by_name: person.name)

      create(:person_earnings_tax_metadatum, tax_blocked: true, tax_year: tax_year, person: person, total_publishing_earnings: 100.00, total_distribution_earnings: 100.00)
      create(:person_transaction, target_type: "PersonIntake", person: person, credit: 50, debit: 0, created_at: previous_dec_15)
      create(:person_transaction, target: ba1, person: person, credit: 50, debit: 0, created_at: previous_dec_15)

      create(:person_transaction, target_type: "PersonIntake", person: person, credit: 50, debit: 0, created_at: jan_01)
      create(:person_transaction, target: ba2, person: person, credit: 50, debit: 0, created_at: jan_01)
      create(:person_transaction, target_type: "PersonIntake", person: person, credit: 50, debit: 0, created_at: jan_02)
      create(:person_transaction, target_type: "YouTubeRoyaltyIntake", person: person, credit: 50, debit: 0, created_at: jan_03)
      person
    end

    let(:person_with_all_earning_types_and_diff_country_audits) do
      person = create(:person, :with_balance, amount: 100, country: "FR")

      self_identified = create(:country_audit_source)
      payoneer_kyc = create(:country_audit_source, name: "Payoneer KYC")

      ba1 = create(:balance_adjustment, person: person, category: "Songwriter Royalty", posted_by: person, posted_by_name: person.name)
      ba2 = create(:balance_adjustment, person: person, category: "Songwriter Royalty", posted_by: person, posted_by_name: person.name)

      create(:country_audit, person: person, country: Country.find_by(iso_code: "FR"), country_audit_source: self_identified, created_at: previous_dec_15)

      create(:person_earnings_tax_metadatum, tax_blocked: true, tax_year: tax_year, person: person, total_publishing_earnings: 100.00, total_distribution_earnings: 100.00)
      create(:person_transaction, target_type: "PersonIntake", person: person, credit: 50, debit: 0, created_at: previous_dec_15)
      create(:person_transaction, target: ba1, person: person, credit: 50, debit: 0, created_at: previous_dec_15)
      create(:country_audit, person: person, country: Country.find_by(iso_code: "FR"), country_audit_source: payoneer_kyc, created_at: jan_01)

      create(:person_transaction, target_type: "PersonIntake", person: person, credit: 50, debit: 0, created_at: jan_01)
      create(:person_transaction, target: ba2, person: person, credit: 50, debit: 0, created_at: jan_01)

      create(:country_audit, person: person, country: Country.find_by(iso_code: "US"), country_audit_source: payoneer_kyc, created_at: jan_15)


      create(:person_transaction, target_type: "PersonIntake", person: person, credit: 50, debit: 0, created_at: mar_03)
      create(:person_transaction, target_type: "YouTubeRoyaltyIntake", person: person, credit: 50, debit: 0, created_at: mar_03)
      person
    end

    let(:us_transaction_previous_year) do
      create(:person_transaction, credit: 6.0, debit: 0, person: create(:person), target_type: "PersonIntake", created_at: previous_dec_15)
    end

    let(:rollover_txn) do
      create(:person_transaction, credit: 6.0, debit: 0, person: person_no_tax_forms, target_type: "TaxableEarningsRollover")
    end

    it "returns :amount_irs_withheld if already withheld" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:progressive_withholding_exemption).and_return(true)
      expect(subject.new(irs_withholding.withheld_from_person_transaction).call).to be(:amount_irs_withheld)
    end

    it "returns :transaction_debit if the transaction is a debit" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:progressive_withholding_exemption).and_return(true)
      expect(subject.new(debit_transaction).call).to be(:transaction_debit)
    end

    it "returns :user_exempt when transaction does not belong to US user" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:progressive_withholding_exemption).and_return(false)
      expect(subject.new(not_us_transaction).call).to be(:user_exempt)
    end

    it "returns :tax_form_submitted when the current tax form is a w-9" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking).and_return(false)
      allow(FeatureFlipper).to receive(:show_feature?).with(:block_taxform_sandbox_payoneer_api).and_return(false)
      allow(FeatureFlipper).to receive(:show_feature?).with(:progressive_withholding_exemption).and_return(true)

      expect(subject.new(us_transaction_with_tax_form).call).to be(:tax_form_submitted)
    end

    it "returns :under_tax_threshold when transaction when US user earnings are under tax threshold specificed in engine rules" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:block_taxform_sandbox_payoneer_api).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:progressive_withholding_exemption).and_return(true)

      expect(subject.new(us_transaction_under_tax_threshold).call).to be(:under_tax_threshold)
    end

    it "creates IRSTaxWithholdings and Person trasactions of the irs_withholding type whose debit equals the amount to be withheld" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking).and_return(false)
      allow(FeatureFlipper).to receive(:show_feature?).with(:block_taxform_sandbox_payoneer_api).and_return(false)
      allow(FeatureFlipper).to receive(:show_feature?).with(:progressive_withholding_exemption).and_return(true)

      withheld_pt_ids = [us_transaction_over_tax_threshold_b, us_transaction_over_tax_threshold_a, us_tax_transaction_with_no_credit].map(&:id)
      subject.new(us_transaction_over_tax_threshold_a).call

      irs_withheld_debits      = PersonTransaction.where(target: IRSTaxWithholding.where(withheld_from_person_transaction_id: withheld_pt_ids)).sum(&:debit)
      expected_withheld_debits = ([
        us_transaction_over_tax_threshold_b,
        us_transaction_over_tax_threshold_a,
        us_tax_transaction_with_no_credit
      ].sum(&:credit) * subject::IRS_PERCENTAGE)

      expect(expected_withheld_debits).to eq(irs_withheld_debits)
    end

    context "when the person doesn't have an active W9 taxform" do
      it "makes an API call to fetch latest tax-forms" do
        allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking).and_return(false)
        allow(FeatureFlipper).to receive(:show_feature?).with(:block_taxform_sandbox_payoneer_api).and_return(false)
        allow(FeatureFlipper).to receive(:show_feature?).with(:progressive_withholding_exemption).and_return(true)

        create(:tax_form, person: person_no_tax_forms, tax_form_type_id: TaxFormType.find_by(kind: "W8BEN").id, payout_provider_config_id: 1)
        expect(TaxFormCheckService).to receive(:check_api?).with(person_no_tax_forms.id)
        subject.new(us_transaction_over_tax_threshold_a).call
      end
    end

    context "when the person has an active W9 taxform" do
      it "doesn't make an API call to fetch latest tax-forms" do
        allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking).and_return(false)
        allow(FeatureFlipper).to receive(:show_feature?).with(:block_taxform_sandbox_payoneer_api).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).with(:progressive_withholding_exemption).and_return(true)

        create(:tax_form, person: person_no_tax_forms, tax_form_type_id: TaxFormType.find_by(kind: "W9 - Individual").id, payout_provider_config_id: 1)
        expect(TaxFormCheckService).not_to receive(:check_api?)
        subject.new(us_transaction_over_tax_threshold_a).call
      end
    end

    it "creates withholding transaction for transactions in the current year" do
      travel_to(Time.parse("2022-01-01")) do

        allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking).and_return(false)
        allow(FeatureFlipper).to receive(:show_feature?).with(:block_taxform_sandbox_payoneer_api).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).with(:progressive_withholding_exemption).and_return(true)

        transaction = person_with_all_earning_types.person_transactions.last
        subject.new(transaction).call

        year = person_with_all_earning_types.person_transactions.where(target_type: "IRSTaxWithholding")
                                            .map(&:created_at)
                                            .map(&:year)
                                            .uniq
        expect(year).to eq([tax_year])
      end
    end

    it "creates the correct number of withholding transaction for transactions for the current year" do
      travel_to(Time.parse("2022-01-01")) do

        allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking).and_return(false)
        allow(FeatureFlipper).to receive(:show_feature?).with(:block_taxform_sandbox_payoneer_api).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).with(:progressive_withholding_exemption).and_return(true)

        transaction = person_with_all_earning_types.person_transactions.last
        subject.new(transaction).call

        withholding_transaction_count = person_with_all_earning_types.person_transactions.where(target_type: "IRSTaxWithholding").count

        expect(withholding_transaction_count).to eq(4)
      end
    end

    it "only operates on a transaction in the current year" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking).and_return(false)
      allow(FeatureFlipper).to receive(:show_feature?).with(:block_taxform_sandbox_payoneer_api).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:progressive_withholding_exemption).and_return(true)

      travel_to(Time.parse("2022-01-01")) do
        expect(subject.new(us_transaction_previous_year).call).to be(:wrong_year)
      end
    end

    it "creates the correct number of withholding transaction for transactions done, for the current year ,while user \"in\" US or territories" do

      travel_to(Time.parse("2021-01-01")) { person_with_all_earning_types_and_diff_country_audits}
      travel_to(Time.parse("2022-11-01")) do

        allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking).and_return(false)
        allow(person_with_all_earning_types_and_diff_country_audits).to receive(:from_united_states_and_territories?).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).with(:block_taxform_sandbox_payoneer_api).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).with(:progressive_withholding_exemption).and_return(true)

        transaction = person_with_all_earning_types_and_diff_country_audits.person_transactions.last
        subject.new(transaction).call

        ranges = TaxWithholdings::TransactionRangeFinderService.new(person_with_all_earning_types_and_diff_country_audits.id, tax_year).call

        withholding_transaction_count = person_with_all_earning_types_and_diff_country_audits.person_transactions.where(target_type: "IRSTaxWithholding").count

        expect(withholding_transaction_count).to eq(2)
      end
    end

    context "when the person doesn't have an active W9 taxform" do
      it "makes an API call to fetch latest tax-forms" do
        allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking).and_return(false)
        allow(FeatureFlipper).to receive(:show_feature?).with(:block_taxform_sandbox_payoneer_api).and_return(false)
        allow(FeatureFlipper).to receive(:show_feature?).with(:progressive_withholding_exemption).and_return(true)

        create(:tax_form, person: person_no_tax_forms, tax_form_type_id: TaxFormType.find_by(kind: "W8BEN").id, payout_provider_config_id: 1)
        expect(TaxFormCheckService).to receive(:check_api?).with(person_no_tax_forms.id)
        subject.new(us_transaction_over_tax_threshold_a).call
      end
    end

    context "when the person has an active W9 taxform" do
      it "doesn't make an API call to fetch latest tax-forms" do
        allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking).and_return(false)
        allow(FeatureFlipper).to receive(:show_feature?).with(:block_taxform_sandbox_payoneer_api).and_return(false)
        allow(FeatureFlipper).to receive(:show_feature?).with(:progressive_withholding_exemption).and_return(true)

        create(:tax_form, person: person_no_tax_forms, tax_form_type_id: TaxFormType.find_by(kind: "W9 - Individual").id, payout_provider_config_id: 1)
        expect(TaxFormCheckService).not_to receive(:check_api?)
        subject.new(us_transaction_over_tax_threshold_a).call
      end
    end

    describe "withholding transaction comment" do
      before do
        allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking).and_return(false)
        allow(FeatureFlipper).to receive(:show_feature?).with(:block_taxform_sandbox_payoneer_api).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).with(:progressive_withholding_exemption).and_return(true)
      end

      context "for rollover withholding transactions" do
        it "says 'IRS Backup Withholding for 2021'" do
          subject.new(rollover_txn).call
          withholding = IRSTaxWithholding.find_by(withheld_from_person_transaction_id: rollover_txn.id)
          withholding_txn = PersonTransaction.find_by(target: withholding)
          expect(withholding_txn.comment).to eq("IRS Backup Withholding for 2021")
        end
      end

      context "when the W-9 isn't the most recent taxform for a user" do
        before do
          allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(true)
          allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking).and_return(false)
          allow(FeatureFlipper).to receive(:show_feature?).with(:block_taxform_sandbox_payoneer_api).and_return(true)
          allow(TaxFormCheckService).to receive(:check_api?).with(us_person_with_no_current_w9.id).and_return(nil)
        end

      it "creates withholdings when the withholding service is called" do
        subject.new(us_person_with_no_current_w9_transaction).call
        withholding_transaction_count = us_person_with_no_current_w9.person_transactions.where(target_type: "IRSTaxWithholding").count
        expect(withholding_transaction_count).to eq(1)
      end
    end

      context "for non-rollover withholding transactions" do
        it "includes posting date" do
          subject.new(us_transaction_over_tax_threshold_a).call
          withholding = IRSTaxWithholding.find_by(withheld_from_person_transaction_id: us_transaction_over_tax_threshold_a.id)
          withholding_txn = PersonTransaction.find_by(target: withholding)
          posting_date = us_transaction_over_tax_threshold_a.created_at.strftime("%m/%d/%Y")
          expect(withholding_txn.comment).to eq("IRS Backup Withholding for Posting #{posting_date}")
        end
      end

      context "for transactions where users are exempt from withholding" do
        it "does not create withholdings" do
          subject.new(withholding_exempt_user_transaction).call
          withholding_transaction_count = us_person_with_no_current_w9.person_transactions.where(target_type: "IRSTaxWithholding").count
          expect(withholding_transaction_count).to eq(0)
        end
      end
    end
  end
end
