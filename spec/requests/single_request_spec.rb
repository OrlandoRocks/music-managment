require "rails_helper"

describe SinglesController, type: :request do
  describe "POST #create" do
    context "when the single is a cyrillic language" do
      it "creates a single for the user and renders the cyrillic release template" do
        person = create(:person)
        set_current_user(person)

        params = {
          single: {
            name: "Single Name",
            creatives: [{"id": "", "role": "primary_artist", "name": "Single Creative"}],
            language_code_legacy_support: "ru",
            primary_genre_id: "1",
            sale_date: "April 24, 2019",
            orig_release_year: ""
          }
        }

        post "/singles", params: params

        new_single = person.albums.find_by(name: "Single Name")
        expect(new_single.artists.first.name).to eq "Single Creative"
        expect(response).to render_template :cyrillic_release
        expect(response.body).to include "Подождите!"
      end
    end

    context "when the single saves successfully" do
      it "creates a single for the user and redirects to the single show page" do
        person = create(:person)
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)

        params = {
          single: {
            name: "Single Name",
            creatives: [{"id": "", "role": "primary_artist", "name": "Single Creative"}],
            language_code_legacy_support: "en",
            primary_genre_id: "1",
            timed_release_timing_scenario: "relative_time",
            orig_release_year: "",
            golive_date: {"month"=>"6", "day"=>"6", "year"=>"2019", "hour"=>"12", "min"=>"00", "meridian"=>"AM"}
          }
        }

        post "/singles", params: params

        new_single = person.albums.find_by(name: "Single Name")
        expect(new_single.artists.first.name).to eq "Single Creative"
        expect(new_single).to be_songwriter
        expect(response).to redirect_to new_single
        expect(response.body).to include(
          "<html><body>You are being <a href=\"http://www.example.com/singles/#{new_single.id}\">redirected</a>.</body></html>"
        )
      end

      it "creates an 'Single created' note" do
        person = create(:person)
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)

        params = {
          single: {
            name: "Single Name",
            creatives: [{"id": "", "role": "primary_artist", "name": "Single Creative"}],
            language_code_legacy_support: "en",
            primary_genre_id: "1",
            timed_release_timing_scenario: "relative_time",
            orig_release_year: "",
            golive_date: {"month"=>"6", "day"=>"6", "year"=>"2019", "hour"=>"12", "min"=>"00", "meridian"=>"AM"}
          }
        }

        expect(Note).to receive(:create)

        post "/singles", params: params
      end

      it "creates external service ids for the creative", run_sidekiq_worker: true do
        allow($believe_api_client).to receive(:post).and_return(nil)
        allow(Person::AfterCommitCallbacks).to receive(:send_to_zendesk).and_return(true)
        person = create(:person)
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)
        allow(Apple::ArtistIdValidator).to receive_message_chain(:new, :valid?).and_return(true)

        params = {
          single: {
            name: "Single Name",
            creatives: [{"id": "", "role": "primary_artist", "name": "Single Creative"}],
            language_code_legacy_support: "en",
            primary_genre_id: "1",
            timed_release_timing_scenario: "relative_time",
            golive_date: {"month"=>"6", "day"=>"6", "year"=>"2019", "hour"=>"12", "min"=>"00", "meridian"=>"AM"},
            orig_release_year: ""
          },
          artist_urls: [{ apple: "https://itunes.apple.com/us/artist/ryan-moe/id12345" }]
        }

        post "/singles", params: params

        new_creative = person.singles.find_by(name: "Single Name").creatives.last
        external_service_id = new_creative.external_service_ids.find_by(identifier: "12345")
        expect(external_service_id).to be_present
      end

      it "adds store automator", run_sidekiq_worker: true do
        allow($believe_api_client).to receive(:post).and_return(nil)
        allow(Person::AfterCommitCallbacks).to receive(:send_to_zendesk).and_return(true)
        person = create(:person)
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)
        allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
        allow(FeatureFlipper).to receive(:show_feature?).with(:salepoints_redesign, person).and_return(true)
        allow(Apple::ArtistIdValidator).to receive_message_chain(:new, :valid?).and_return(true)

        expect(Album::AutomatorService).to receive(:update_automator)

        params = {
          single: {
            name: "Single Name",
            creatives: [{"id": "", "role": "primary_artist", "name": "Single Creative"}],
            language_code_legacy_support: "en",
            primary_genre_id: "1",
            sale_date: "April 24, 2019",
            orig_release_year: "",
          }
        }

        post "/singles", params: params
      end
    end

    context "when single creation is unsuccessful" do
      it "renders the edit template " do
        allow(Person::AfterCommitCallbacks).to receive(:send_to_zendesk).and_return(true)
        person = create(:person)
        set_current_user(person)

        params = {
          single: {
            name: "Single Name",
            language_code_legacy_support: "en",
          }
        }

        post "/singles", params: params
        expect(response).to render_template :edit
        expect(response.body).to include "Edit Single"
      end
    end
  end

  describe "PUT #update" do
    context "when the single successfully updates" do
      it "renders the single show page" do
        single = create(:single, name: "Original Single Name")
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(single.person)

        params = {
          single: {
            name: "New Single Name",
            creatives: [{"id": "", "role": "primary_artist", "name": "Single Creative"}],
            language_code_legacy_support: "en",
            primary_genre_id: "1",
            sale_date: "April 24, 2019",
            orig_release_year: ""
          }
        }

        put "/singles/#{single.id}", params: params

        expect(single.reload.album_name).to eq "New Single Name"
        expect(single).to be_songwriter
        expect(response).to redirect_to single
        expect(response.body).to include(
          "<html><body>You are being <a href=\"http://www.example.com/singles/#{single.id}\">redirected</a>.</body></html>"
        )
      end
    end

    context "when the single does not update successfully" do
      it "renders the edit template" do
        allow(Person::AfterCommitCallbacks).to receive(:send_to_zendesk).and_return(true)
        single = create(:single, name: "Original Single Name")
        set_current_user(single.person)

        put "/singles/#{single.id}", params: { single: { name: nil } }

        expect(single.album_name).to eq "Original Single Name"
        expect(response).to render_template :edit
        expect(response.body).to include "Edit Single"
      end
    end

    context "when the single has not been paid for and the preorder date expired" do
      it "updates the salepoint preorder data start date to nil if the sale date does not support preorder" do
        single = create(:single, payment_applied: false, salepoints: [create(:salepoint, :itunes)])
        preorder_purchase = create(:preorder_purchase, album: single)
        salepoint_preorder_data = build(:salepoint_preorder_data, salepoint: single.salepoints.first)
        preorder_purchase.salepoint_preorder_data << salepoint_preorder_data
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(single.person)

        put "/singles/#{single.id}", params: { single: { name: "New Album Name" } }

        expect(salepoint_preorder_data.reload.start_date).to be_nil
        expect(salepoint_preorder_data.enabled).to be_falsy
      end

      it "updates the salepoint preorder data start date if the sale date supports preorder" do
        single = create(:single, payment_applied: false, salepoints: [create(:salepoint, :itunes)], sale_date: Date.today + 12.days, golive_date: Date.today + 12.days)
        preorder_purchase = create(:preorder_purchase, album: single)
        salepoint_preorder_data = build(:salepoint_preorder_data, salepoint: single.salepoints.first)
        preorder_purchase.salepoint_preorder_data << salepoint_preorder_data
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(single.person)

        put "/singles/#{single.id}", params: { single: { name: "New Album Name" } }

        expect(salepoint_preorder_data.reload.start_date).to eq single.sale_date - 1.day
      end
    end
  end
end
