require "rails_helper"

RSpec.describe "v2 login", type: :request do
  include V2::JwtHelper
  include Rails.application.routes.url_helpers

  JWT_DURATION = 60 # seconds

  it "authenticates a user with a valid JWT and nonce" do
    person = create(:person)
    jwt = generate_jwt_for(person)
    nonce = generate_nonce
    save_nonce(nonce, JWT_DURATION, person.id)
    cookies["v2_login"] = "true"
    enable_feature_flipper

    get(
      dashboard_path,
      params: { id: person.id, nonce: nonce },
      headers: {
        authorization: jwt,
        referer: valid_referer
      }
    )

    expect(response.status).to eql(302)
    redirect_location = response.headers["Location"]
    expect(URI(redirect_location).path).to eql(dashboard_path)
  end

  it "after login, redirects the user to their default landing page" do
    person = create(:person)
    jwt = generate_jwt_for(person)
    nonce = generate_nonce
    save_nonce(nonce, JWT_DURATION, person.id)
    cookies["v2_login"] = "true"
    enable_feature_flipper

    get(
      person_preferences_path, # path doesn't matter; user will be redirected
      params: { id: person.id, nonce: nonce },
      headers: {
        authorization: jwt,
        referer: valid_referer
      }
    )

    expect(response.status).to eql(302)
    redirect_location = response.headers["Location"]
    expect(URI(redirect_location).path).to eql(dashboard_path)
  end

  it "after login, redirects the user to the passed return_to param" do
    person = create(:person)
    jwt = generate_jwt_for(person)
    nonce = generate_nonce
    save_nonce(nonce, 60, person.id)
    cookies["v2_login"] = "true"
    enable_feature_flipper
    return_to = person_preferences_path

    get(
      home_path, # any path is fine
      params: { id: person.id, nonce: nonce, return_to: return_to },
      headers: {
        authorization: jwt,
        referer: valid_referer
      }
    )

    expect(response.status).to eql(302)
    redirect_location = response.headers["Location"]
    expect(URI(redirect_location).path).to eql(return_to)
  end

  def generate_jwt_for(person)
    generate_jwt(
      duration: JWT_DURATION,
      payload: {
        id: person.id,
        email: person.email,
        name: person.name,
        roles: person.roles.pluck(:name)
      }
    )
  end

  def enable_feature_flipper
    allow(FeatureFlipper).to receive(:show_feature?).with(any_args).and_call_original
    allow(FeatureFlipper).to receive(:show_feature?).with(:v2_login).and_return(true)
  end

  def valid_referer
    V2::Authentication::SharedLoginService::BASE_URL
  end
end
