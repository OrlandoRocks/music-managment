require "rails_helper"

describe RingtonesController, type: :request do
  describe "POST #create" do
    context "when the ringtone is a cyrillic language" do
      it "creates a ringtone for the user and renders the cyrillic release template" do
        person = create(:person)
        set_current_user(person)

        params = {
          ringtone: {
            name: "Ringtone Name",
            creatives: [{"id": "", "role": "primary_artist", "name": "Ringtone Creative"}],
            language_code_legacy_support: "ru",
            primary_genre_id: "1",
            sale_date: "April 24, 2019",
            orig_release_year: ""
          }
        }

        post "/ringtones", params: params

        new_ringtone = person.ringtones.find_by(name: "Ringtone Name")
        expect(new_ringtone.artists.first.name).to eq "Ringtone Creative"
        expect(response).to render_template :cyrillic_release
        expect(response.body).to include "Подождите!"
      end
    end

    context "when the ringtone saves successfully" do
      it "creates a ringtone and redirects to the ringtone show page" do
        person = create(:person)
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)

        params = {
          ringtone: {
            name: "Ringtone Name",
            creatives: [{"id": "", "role": "primary_artist", "name": "Ringtone Creative"}],
            language_code_legacy_support: "en",
            primary_genre_id: "1",
            sale_date: "April 24, 2019",
            orig_release_year: ""
          }
        }

        post "/ringtones", params: params

        new_ringtone = person.ringtones.find_by(name: "Ringtone Name")
        expect(new_ringtone.artists.first.name).to eq "Ringtone Creative"
        expect(response).to redirect_to new_ringtone
        expect(response.body).to include(
          "<html><body>You are being <a href=\"http://www.example.com/ringtones/#{new_ringtone.id}\">redirected</a>.</body></html>"
        )
      end

      it "creates an 'Ringtone created' note" do
        person = create(:person)
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)

        params = {
          ringtone: {
            name: "Ringtone Name",
            creatives: [{"id": "", "role": "primary_artist", "name": "Ringtone Creative"}],
            language_code_legacy_support: "en",
            primary_genre_id: "1",
            sale_date: "April 24, 2019",
            orig_release_year: "",
          }
        }

        expect(Note).to receive(:create)

        post "/ringtones", params: params
      end

      it "creates external service ids for the creative", run_sidekiq_worker: true do
        allow($believe_api_client).to receive(:post).and_return(nil)
        allow(Person::AfterCommitCallbacks).to receive(:send_to_zendesk).and_return(true)
        person = create(:person)
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)
        allow(Apple::ArtistIdValidator).to receive_message_chain(:new, :valid?).and_return(true)

        params = {
          ringtone: {
            name: "Ringtone Name",
            creatives: [{"id": "", "role": "primary_artist", "name": "Ringtone Creative"}],
            language_code_legacy_support: "en",
            primary_genre_id: "1",
            sale_date: "April 24, 2019",
            orig_release_year: ""
          },
          artist_urls: [{ apple: "https://itunes.apple.com/us/artist/ryan-moe/id12345" }]
        }

        post "/ringtones", params: params

        new_creative = person.ringtones.find_by(name: "Ringtone Name").creatives.last
        external_service_id = new_creative.external_service_ids.find_by(identifier: "12345")
        expect(external_service_id).to be_present
      end
    end

    context "when ringtone creation is unsuccessful" do
      it "renders the edit template " do
        person = create(:person)
        set_current_user(person)

        params = {
          ringtone: {
            name: "Ringtone Name",
            language_code_legacy_support: "en",
          }
        }

        post "/ringtones", params: params
        expect(response).to render_template :new
        expect(response.body).to include "Create A Ringtone"
      end
    end
  end

  describe "PUT #update" do
    context "when the ringtone successfully updates" do
      it "renders the ringtone show page" do
        ringtone = create(:ringtone, name: "Original Ringtone Name")
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(ringtone.person)
        allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)

        params = {
          ringtone: {
            name: "New Ringtone Name",
            creatives: [{"id": "", "role": "primary_artist", "name": "Ringtone Creative"}],
            language_code_legacy_support: "en",
            primary_genre_id: "1",
            sale_date: "April 24, 2019",
            orig_release_year: ""
          }
        }

        put "/ringtones/#{ringtone.id}", params: params

        expect(ringtone.reload.name).to eq "New Ringtone Name"
        expect(response).to redirect_to ringtone
        expect(response.body).to include(
          "<html><body>You are being <a href=\"http://www.example.com/ringtones/#{ringtone.id}\">redirected</a>.</body></html>"
        )
      end
    end

    context "when the ringtone does not update successfully" do
      it "renders the edit template" do
        ringtone = create(:ringtone, name: "Original Ringtone Name")
        set_current_user(ringtone.person)

        put "/ringtones/#{ringtone.id}", params: { ringtone: { name: nil } }

        expect(ringtone.name).to eq "Original Ringtone Name"
        expect(response).to render_template :edit
        expect(response.body).to include "Edit Ringtone"
      end
    end
  end
end
