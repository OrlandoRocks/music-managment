require "rails_helper"

describe TaxInfosController, type: :request do
  describe 'post #create' do
    context "when a publishing_composer does not have tax info" do
      it "will create a new object with valid params" do
        publishing_composer = create(:publishing_composer)
        person = publishing_composer.person
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)
        tax_form_params = {
          form_type: "W9 - Individual",
          submitted_at: Date.today,
          expires_at: 2.years.from_now,
          person_id: person.id
        }
        tax_info_params = {
          composer_id: publishing_composer.id,
          name: "Mr. Thurman Wilderman",
          address_1: "16709 Cormier Rest",
          city: "North Weldonbury",
          state: "GA",
          zip: 62_111,
          country: "US",
          nosocial: false,
          salt: "NqygLpvxAJzuHZTuUiJ57w==",
          encrypted_tax_id: "xVhHgLbM9v4QI6/Kkxez6A==\n",
          is_entity: false,
          dob: 25.years.ago,
          classification: "individual",
          exempt_payee: false,
          agreed_to_w9: true
        }

        expect { post "/composers/#{publishing_composer.id}/tax_info", params: { composer_id: publishing_composer.id, tax_info: tax_info_params, tax_form: tax_form_params } }.to change { TaxInfo.count }.by(1)
        expect(response.body).to include "<html><body>You are being <a href=\"http://www.example.com/composers/#{publishing_composer.id}\">redirected</a>.</body></html>"
        expect(response).to redirect_to(composer_path(publishing_composer))
      end

      it "will not create a new object with invalid params" do
        publishing_composer = create(:publishing_composer)
        person = publishing_composer.person
        create(:account, person: person)
        set_current_user(person)
        tax_info_params = {
          composer_id: publishing_composer.id,
          name: "Mr. Thurman Wilderman",
          address_1: "16709 Cormier Rest",
          city: "North Weldonbury",
          state: nil,
          zip: 62_111,
          country: "US",
          nosocial: false,
          salt: "NqygLpvxAJzuHZTuUiJ57w==",
          encrypted_tax_id: "xVhHgLbM9v4QI6/Kkxez6A==\n",
          is_entity: false,
          dob: 25.years.ago,
          classification: "individual",
          exempt_payee: false,
          agreed_to_w9: true
        }

        expect { post "/composers/#{publishing_composer.id}/tax_info", params: { tax_form: "w9", tax_info: tax_info_params } }.to_not change(TaxInfo, :count)
        expect(response).to render_template('tax_infos/new')
      end
    end

    context "when a publishing_composer has existing tax info" do
      it "will update tax info if a publishing_composer switches between w9 and w8 forms" do
        publishing_composer = create(:publishing_composer)
        person = publishing_composer.person
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)
        tax_form_params = {
          form_type: "W9 - Individual",
          submitted_at: Date.today,
          expires_at: 2.years.from_now,
          person_id: person.id
        }
        tax_info = create(:tax_info, tax_id: '908881234', publishing_composer: publishing_composer, city: "Queens", agreed_to_w9: true)
        tax_info_params = {
          composer_id: publishing_composer.id,
          name: "Mr. Thurman Wilderman",
          address_1: "16709 Cormier Rest",
          city: "Brooklyn",
          state: "GA",
          zip: 62_111,
          country: "US",
          nosocial: false,
          salt: "NqygLpvxAJzuHZTuUiJ57w==",
          encrypted_tax_id: "xVhHgLbM9v4QI6/Kkxez6A==\n",
          is_entity: false,
          dob: 25.years.ago,
          classification: "individual",
          exempt_payee: false,
          agreed_to_w8ben: true,
          country_of_corp: person.country_website_id,
          owner_type: "individual"
        }

        expect { post "/composers/#{publishing_composer.id}/tax_info", params: { composer_id: publishing_composer.id, tax_form: tax_form_params, tax_info: tax_info_params } }.to_not change(TaxInfo, :count)
        expect(tax_info.reload.city).to eq("Brooklyn")
        expect(tax_info.reload.agreed_to_w8ben_at).not_to be(nil)
        expect(tax_info.reload.agreed_to_w9_at).to be(nil)
        expect(response).to redirect_to(composer_path(publishing_composer))
      end
    end
  end
end
