require "rails_helper"

describe "POST /people" do
  context "creates a new person" do
    before(:each) do
      allow_any_instance_of(RecaptchaVerifiable).to receive(:recaptcha).and_return(true)
      allow(IpAddressGeocoder).to receive(:geocoded_attributes).and_return({
      city:        "Brooklyn",
      subdivision: "New York",
      country:     "United States",
      latitude:    40.6501,
      longitude:   -73.9496
      }.with_indifferent_access)
    end

    it "redirects to /dashboard/create_account" do
      params = {
        person: {
          accepted_terms_and_conditions: 1,
          country: "US",
          email: "blah@tunecore.com",
          name: "Test Person",
          password: "Testpass123!",
          password_confirmation: "Testpass123!",
        },
      }

      post("/people", params: params)

      expect(response.status).to eql(302)
      expect(response.headers['location']).
        to include(dashboard_create_account_path)
    end

    it "creates a TwoFactorAuthPrompt for the new user" do
      params = {
        person: {
          accepted_terms_and_conditions: 1,
          country: "US",
          email: "blah@tunecore.com",
          name: "Test Person",
          password: "Testpass123!",
          password_confirmation: "Testpass123!",
        },
      }

      post("/people", params: params)

      person = Person.find_by!(email: params[:person][:email])
      expect(person.two_factor_auth_prompt).to be_present
    end

    it "marks users signing up with facebook as verified" do
      params = {
        person: {
          accepted_terms_and_conditions: 1,
          country: "US",
          email: "test@tunecore.com",
          is_opted_in: 0,
          name: "Test Man",
          password: "Testpass123!",
          password_confirmation: "Testpass123!",
          postal_code: "11215"
        },
        external_services_person: {
          account_id: "123",
          external_service_id: ExternalService.facebook.id
        }
      }

      expect do
        expect { post "/people", params: params }.to change(ExternalServicesPerson, :count).by(1)
      end.to change(Person, :count).by(1)
      expect(response.body).to include(
        "<html><body>You are being <a href=\"http://test.tunecore.com/dashboard/create_account?fb=true\">redirected</a>.</body></html>"
      )
      new_person = Person.find_by(name: "Test Man")
      expect(new_person.email).to eq("test@tunecore.com")
      expect(new_person.is_verified).to eq(1)
      expect(new_person.external_services_people.first.account_id).to eq("123")
      expect(new_person.external_services.first.name).to eq(ExternalService.facebook.name)
    end

    it "should redirect facebook signup users when email already in use to login page" do
      person = create(:person, email: "mytestingemail@testing.com")
      params = {
        person: {
          accepted_terms_and_conditions: 1,
          country: "US",
          email: person.email,
          is_opted_in: 0,
          name: "Test Man",
          password: "Testpass123!",
          password_confirmation: "Testpass123!",
          postal_code: "11215"
        },
        external_services_person: {
          account_id: "123",
          external_service_id: ExternalService.facebook.id
        }
      }

      expect do
        expect { post "/people", params: params }.not_to change(ExternalServicesPerson, :count)
      end.not_to change(Person, :count)

      assert_redirected_to login_path(email: person.email, ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
    end

    it "should only create external services person record if external service id is valid" do
      params = {
        person: {
          accepted_terms_and_conditions: 1,
          country: "US",
          email: "test@tunecore.com",
          is_opted_in: 0,
          name: "Test Man",
          password: "Testpass123!",
          password_confirmation: "Testpass123!",
          postal_code: "11215"
        },
        external_services_person: {
          account_id: "123",
          external_service_id: ExternalService.last.id + 1
        }
      }

      expect do
        expect { post "/people", params: params }.not_to change(ExternalServicesPerson, :count)
      end.to change(Person, :count).by(1)

      person = Person.last
      expect(person.external_services_people).to eq([])
      expect(person.external_services).to eq([])
    end
  end
end

describe "PUT /people" do
  context "updates a person" do
    it "updates attributes on the user based on the params" do
      person = create(:person, postal_code: "11215", email: "oldemail@tunecore.com")
      allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)

      params = {
        person: {
          postal_code: "11217",
          email: "newemail@tunecore.com",
        }
      }

      patch "/people/#{person.id}", params: params

      expect(person.postal_code).to eq "11217"
      expect(person.email).to eq "newemail@tunecore.com"
      expect(response.body).to include(
        "<html><body>You are being <a href=\"http://www.example.com/dashboard\">redirected</a>.</body></html>"
      )
    end
  end
end

describe "NEW /people" do
  it "redirects to the signup page" do
    get "/people/new"

    expect(response).to redirect_to("/signup")
  end

  it "renders the signup page" do
    get "/signup"

    expect(response.body).to include("Signup Page")
  end
end

describe "EDIT /people" do
  it "edits a person" do
    person = create(:person, email: "oldemail@tunecore.com", postal_code: "11217")
    set_current_user(person)

    get "/people/#{person.id}/edit"

    expect(response.body).to include("Account Settings")
  end
end

describe "UNVERIFIED /people" do
  context "when user is verified" do
    it "redirects to the dashboard" do
      person = create(:person)
      allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)

      get "/people/#{person.id}/unverified"

      assert_redirected_to dashboard_path
    end
  end
end

describe "COMPLETE_VERIFY /people" do
  context "when user is not logged in" do
    it "redirects back to login path" do
      person = create(:person, is_verified: false)
      person.generate_invite_code

      get "/people/#{person.id}/complete_verify", params: { id: person.id, key: person.invite_code }

      assert_redirected_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
      expect(person.reload.is_verified).to eq(1)
    end
  end

  context "when user is logged in" do
    it "should send another verification key email if key is expired" do
      person = create(:person, is_verified: false)
      person.generate_invite_code
      allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)
      allow($believe_api_client).to receive(:post).and_return(nil)
      person.invite_expiry = Date.today - 1.day
      person.save(validate: false)

      expect(PersonNotifier).to receive_message_chain(:verify, :deliver)

      get "/people/#{person.id}/complete_verify", params: { id: person.id, key: person.invite_code }

      assert_redirected_to unverified_person_path(person)
    end

    it "should redirect back to unverified page if key is blank or key is invalid" do
      person = create(:person, is_verified: false)
      person.generate_invite_code
      allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)
      allow($believe_api_client).to receive(:post).and_return(nil)

      get "/people/#{person.id}/complete_verify", params: { id: person.id, key: nil }

      assert_redirected_to unverified_person_path(person)
    end
  end
end
