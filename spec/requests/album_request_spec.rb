require "rails_helper"

describe AlbumController, type: :request do
  describe "GET #new" do
    it "should default the album language to English for a global user" do
      person = create(:person)
      set_current_user(person)

      get new_album_path

      expect(assigns(:album).language_code).to eq "en"
    end

    it "should default the album language to German for a DE user" do
      person = create(:person, country: "DE")
      set_current_user(person)

      get new_album_path

      expect(assigns(:album).language_code).to eq "de"
    end

    it "should load the album_app page if the album_app feature is enabled" do
      person = create(:person)
      set_current_user(person)
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

      get new_album_path

      expect(response).to render_template "album/album_app"
      expect(response.body).to include "Create a New Album"
    end
  end

  describe "GET #stop_cancellation" do
    it "should remove a cancellation" do
      album   = create(:album)
      renewal = double(:renewal)
      allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(album.person)

      expect(Renewal).to receive(:renewal_for).with(album).and_return(renewal)
      expect(renewal).to receive(:keep!)
      expect { get "/albums/#{album.id}/stop_cancellation", params: { id: album.id } }
        .to change(Note, :count).by(1)
    end

    it "should work as a transaction" do
      album   = create(:album)
      renewal = double(:renewal)
      allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(album.person)

      expect(Renewal).to receive(:renewal_for).and_return(renewal)
      allow(renewal).to receive(:keep!).and_raise(StandardError)
      expect { get "/albums/#{album.id}/stop_cancellation", params: { id: album.id } }
        .not_to change(Note, :count)
    end
  end

  describe "POST #create" do
    context "when the album uses a cyrillic language" do
      it "creates an album for the user and renders the cyrillic release template" do
        person = create(:person)
        set_current_user(person)

        params = {
          album: {
            name: "Album Name",
            creatives: [{"id": "", "role": "primary_artist", "name": "Artist Name"}],
            language_code_legacy_support: "ru",
            primary_genre_id: "1",
            sale_date: "April 24, 2019",
            orig_release_year: ""
          }
        }

        post "/albums", params: params

        new_album = person.albums.find_by(name: "Album Name")
        expect(new_album.artists.first.name).to eq "Artist Name"
        expect(response).to render_template :cyrillic_release
        expect(response.body).to include "Подождите!"
      end
    end

    context "when the album saves successfully" do
      it "creates an album for the user and redirects to the album show page" do
        person = create(:person)
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)

        params = {
          album: {
            name: "Album Name",
            creatives: [{"id": "", "role": "primary_artist", "name": "Artist Name"}],
            language_code_legacy_support: "en",
            primary_genre_id: "1",
            orig_release_year: "",
            timed_release_timing_scenario: "relative_time",
            golive_date: {"month"=>"6", "day"=>"6", "year"=>"2019", "hour"=>"12", "min"=>"00", "meridian"=>"AM"}
          }
        }

        post "/albums", params: params

        new_album = person.albums.find_by(name: "Album Name")
        expect(new_album.artists.first.name).to eq "Artist Name"
        expect(new_album.primary_genre_id).to eq 1
        expect(new_album).to be_songwriter
        expect(response).to redirect_to new_album
        expect(response.body).to include(
          "<html><body>You are being <a href=\"http://www.example.com/albums/#{new_album.id}\">redirected</a>.</body></html>"
        )
      end

      it "creates an 'Album created' note" do
        person = create(:person)
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)

        expect(Note).to receive(:create)

        post "/albums", params: timed_release_params
      end

      it "creates external service ids for the creative", run_sidekiq_worker: true do
        allow($believe_api_client).to receive(:post).and_return(nil)
        allow(Person::AfterCommitCallbacks).to receive(:send_to_zendesk).and_return(true)
        person = create(:person)
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)
        allow(Apple::ArtistIdValidator).to receive_message_chain(:new, :valid?).and_return(true)

        post "/albums", params: timed_release_params.merge(
          artist_urls: [{ apple: "https://itunes.apple.com/us/artist/ryan-moe/id12345" }]
        )

        new_creative = person.albums.find_by(name: "Album Name").creatives.last
        external_service_id = new_creative.external_service_ids.find_by(identifier: "12345")
        expect(external_service_id).to be_present
      end

      it "adds store automator", run_sidekiq_worker: true do
        allow($believe_api_client).to receive(:post).and_return(nil)
        allow(Person::AfterCommitCallbacks).to receive(:send_to_zendesk).and_return(true)
        person = create(:person)
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)
        allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
        allow(FeatureFlipper).to receive(:show_feature?).with(:salepoints_redesign, person).and_return(true)
        allow(Apple::ArtistIdValidator).to receive_message_chain(:new, :valid?).and_return(true)

        expect(Album::AutomatorService).to receive(:update_automator)

        post "/albums", params: timed_release_params
      end
    end

    context "when album creation is unsuccessful" do
      it "renders the new template" do
        person = create(:person)
        set_current_user(person)

        params = {
          album: {
            name: "Album Name",
            language_code_legacy_support: "en",
          }
        }

        post "/albums", params: params
        expect(response).to render_template :new
        expect(response.body).to include "Create An Album"
      end
    end
  end

  describe "PUT #update" do
    context "when the album updates successfully" do
      it "redirects to the album show page" do
        album = create(:album)
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(album.person)

        params = {
          album: {
            name: "New Album Name",
            label_name: "Label Name",
            language_code_legacy_support: "en",
            golive_date: {"month"=>"6", "day"=>"7", "year"=>"2019", "hour"=>"11", "min"=>"00", "meridian"=>"AM"}
          }
        }

        put "/albums/#{album.id}", params: params
        album.reload

        expect(album.name).to eq "New Album Name"
        expect(album.sale_date).to eq "2019-06-07".to_date
        expect(album).to be_songwriter
        expect(response).to redirect_to album
        expect(response.body).to include(
          "<html><body>You are being <a href=\"http://www.example.com/albums/#{album.id}\">redirected</a>.</body></html>"
        )
      end
    end

    context "when the album does not update successfully" do
      it "renders to the edit action" do
        album = create(:album)
        set_current_user(album.person)

        put "/albums/#{album.id}", params: { album: { name: nil } }

        expect(response).to render_template :edit
        expect(response.body).to include "Edit"
      end
    end
  end
end

def timed_release_params
  {
    album: {
      name: "Album Name",
      creatives: [{"id": "", "role": "primary_artist", "name": "Artist Name"}],
      language_code_legacy_support: "en",
      primary_genre_id: "1",
      orig_release_year: "",
      timed_release_timing_scenario: "relative_time",
      golive_date: {"month"=>"6", "day"=>"6", "year"=>"2019", "hour"=>"12", "min"=>"00", "meridian"=>"AM"}
    }
  }
end
