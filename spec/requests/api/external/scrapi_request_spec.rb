require 'rails_helper'

module Api::External
  describe ScrapiController do
    describe 'GET #callback' do
      it 'should enqueue GetJobWorker to update the Scrapi Job' do
        params = {
          api_key: API_CONFIG['API_KEY'],
          jobId: 'jobId',
          analysisId: 'analysisId',
        }

        get '/api/external/scrapi/callback', params: params
        expect(response.status).to eq(200)
      end

      it 'should respond with 404 if missing jobId' do
        params = {
          api_key: API_CONFIG['API_KEY'],
          analysisId: 'analysisId',
        }

        get '/api/external/scrapi/callback', params: params
        expect(response.status).to eq(404)
      end

      it 'should respond with 404 if empty jobId' do
        params = {
          api_key: API_CONFIG['API_KEY'],
          jobId: '',
          analysisId: 'analysisId',
        }

        get '/api/external/scrapi/callback', params: params
        expect(response.status).to eq(404)
      end
    end
  end
end
