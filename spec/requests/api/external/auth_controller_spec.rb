require 'rails_helper'

module Api::External
  describe AuthController do
    describe '#get_token' do
      let (:person) { create(:person) }

      before { $redis.set('session:valid_session', nil) }

      context 'with invalid session token' do
        it 'should respond with unauthorized' do
          get '/api/external/auth/get_token', params: {
            tunecore_id: 'invalid'
          }

          expect(response.status).to eq(401)
          expect(response.body).to include('invalid session')
        end
      end

      context 'with valid user session' do
        it 'should respond with a valid generated JWT token' do
          marshal_dump = Marshal.dump({person: person.id})
          $redis.set('session:valid_session', marshal_dump)

          params = {
            tunecore_id: 'valid_session'
          }

          get '/api/external/auth/get_token', params: params

          data = JSON.parse(response.body)

          expect(response.status).to eq(200)
          expect(response.body).to include('token')
          expect(data['person_id']).to eq(person.id)
          expect(data['country_website']).to eq(person.country_website.country)
        end
      end
    end
  end
end
