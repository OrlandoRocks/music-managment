require "rails_helper"

describe Api::Backstage::AlbumsController, type: :request do
  describe "PATCH #update" do
    context "when the album updates successfully" do
      it "renders JSON with status: ok" do
        album = create(:album, :with_multiple_uploaded_songs)
        songs = album.songs
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(album.person)

        album_params = {
          album: { song_order: songs.map{ |song| song.id } }
        }

        put "/api/backstage/albums/#{album.id}", params: album_params
        parsed_body = JSON.parse(response.body)
        ordered_songs = songs.map{ |song| song.id }

        expect(parsed_body["song_order"]).to match_array ordered_songs
        expect(response.status).to eq(200)
      end
    end
  end
end
