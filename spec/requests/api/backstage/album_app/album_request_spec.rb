require "rails_helper"

describe Api::Backstage::AlbumApp::AlbumsController, type: :request do
  describe "POST #create" do
    context "when the params are valid" do
      it "creates a new album" do
        album_count = Album.count
        person = create(:person)
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)
        params  = {
          album: {
            name:               "Too Many Cooks",
            creatives:          [{ "name" => Faker::Name.name, "role" => "primary_artist" }],
            album_type:         "Album",
            golive_date:        {hour: "01", min: '01', month: '12', day: '12', year: (DateTime.now + 1.year).strftime('%Y'), meridian: 'am'},
            timed_release_timing_scenario: 'relative_time',
            is_various:         true,
            label_name:         '',
            language_code:      "en",
            person:             person,
            primary_genre_id:   Genre.active.first.id,
            sale_date:          Date.today,
            secondary_genre_id: nil
          }
        }

        post "/api/backstage/album_app/albums", params: params

        expect(Album.count).to eq(album_count + 1)

        expect(person.reload.albums.where(name: "Too Many Cooks")).to exist
      end

      it "creates a new upc" do
        album_count = Album.count
        person = create(:person)
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)
        optional_upc_number = "751416144528"
        params  = {
          album: {
            name:                "Too Many Cooks",
            creatives:           [{ "name" => Faker::Name.name, "role" => "primary_artist" }],
            album_type:          "Album",
            golive_date:         {hour: "01", min: '01', month: '12', day: '12', year: (DateTime.now + 1.year).strftime('%Y'), meridian: 'am'},
            timed_release_timing_scenario: 'relative_time',
            is_various:          true,
            label_name:          '',
            language_code:       "en",
            person:              person,
            primary_genre_id:    Genre.active.first.id,
            sale_date:           Date.today,
            secondary_genre_id:  nil,
            optional_upc_number: optional_upc_number
          }
        }

        post "/api/backstage/album_app/albums", params: params

        expect(Album.count).to eq(album_count + 1)

        expect(
          person.reload.albums.first.upcs.where(upcable_type: "Album", number: optional_upc_number, upc_type: "optional")
        ).to exist
      end

      it "includes the album show page url in the response" do
        album_count = Album.count
        person = create(:person)
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)
        params = {
          album: {
            name:                "Too Many Cooks",
            creatives:           [{ "name" => Faker::Name.name, "role" => "primary_artist" }],
            album_type:          "Album",
            golive_date:         {hour: "01", min: '01', month: '12', day: '12', year: (DateTime.now + 1.year).strftime('%Y'), meridian: 'am'},
            timed_release_timing_scenario: 'relative_time',
            is_various:          true,
            label_name:          '',
            language_code:       "en",
            person:              person,
            primary_genre_id:    Genre.active.first.id,
            sale_date:           Date.today,
            secondary_genre_id:  nil,
          }
        }

        post "/api/backstage/album_app/albums", params: params

        expect(Album.count).to eq(album_count + 1)

        json_response = JSON.parse(response.body)
        expect(json_response["album_show_url"]).to include "http://test.tunecore.com/albums/"
      end

      it "creates creatives and associated artists" do
        album_count = Album.count
        person = create(:person)
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)
        params = {
          album: {
            name:               "Too Many Cooks",
            creatives: [{
              "name" => "Creative Name",
              "role" => "primary_artist",
              "apple" => { create_page: false },
              "spotify" => { create_page: false },
            }],
            album_type:         "Album",
            golive_date:        {hour: "01", min: '01', month: '12', day: '12', year: (DateTime.now + 1.year).strftime('%Y'), meridian: 'am'},
            timed_release_timing_scenario: 'relative_time',
            is_various:         false,
            label_name:         "",
            language_code:      "en",
            person:             person,
            primary_genre_id:   Genre.active.first.id,
            sale_date:          Date.today,
            secondary_genre_id: nil
          }
        }

        post "/api/backstage/album_app/albums", params: params

        expect(Album.count).to eq(album_count + 1)

        creative = person.albums.first.creatives.first
        expect(creative.role).to eq "primary_artist"
        expect(creative.artist.name).to eq "Creative Name"
      end

      it "creates external service ids for the creative" do
        esid_count = ExternalServiceId.count
        person = create(:person)
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)
        params = {
          album: {
            name:               "Too Many Cooks",
            creatives: [{
                          "name" => "Creative Name",
                          "role" => "primary_artist",
                          "apple" => { create_page: true },
                          "spotify" => { create_page: true },
                        }],
            album_type:         "Album",
            golive_date:        {hour: "01", min: '01', month: '12', day: '12', year: (DateTime.now + 1.year).strftime('%Y'), meridian: 'am'},
            timed_release_timing_scenario: 'relative_time',
            is_various:         false,
            label_name:         "",
            language_code:      "en",
            person:             person,
            primary_genre_id:   Genre.active.first.id,
            sale_date:          Date.today,
            secondary_genre_id: nil
          }
        }

        post "/api/backstage/album_app/albums", params: params

        expect(ExternalServiceId.count).to eq(esid_count + 2)
      end

      it "marks the Album 'is_various' when it's a various artist album" do
        album_count = Album.count
        person = create(:person)
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)
        params = {
          album: {
            name:               "Too Many Cooks",
            creatives:          [{ "name" => Faker::Name.name, "role" => "primary_artist" }],
            album_type:         "Album",
            golive_date:        {hour: "01", min: '01', month: '12', day: '12', year: (DateTime.now + 1.year).strftime('%Y'), meridian: 'am'},
            timed_release_timing_scenario: 'relative_time',
            is_various:         true,
            label_name:         "",
            language_code:      "en",
            person:             person,
            primary_genre_id:   Genre.active.first.id,
            sale_date:          Date.today,
            secondary_genre_id: nil
          }
        }

        post "/api/backstage/album_app/albums", params: params

        expect(Album.count).to eq(album_count + 1)

        expect(person.reload.albums.first.is_various).to be_truthy
      end

      context "when the album is a Single" do
        it "creates a song" do
          single_count = Single.count
          song_count = Song.count
          person = create(:person)
          allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)
          params = {
            album: {
              name: "Too Many Cooks",
              creatives: [{
                "name" => Faker::Name.name,
                "role" => "primary_artist",
                "apple" => { create_page: false },
                "spotify" => { create_page: false },
              }],
              album_type: "Single",
              golive_date: {hour: "01", min: '01', month: '12', day: '12', year: (DateTime.now + 1.year).strftime('%Y'), meridian: 'am'},
              timed_release_timing_scenario: 'relative_time',
              is_various: false,
              label_name: "",
              language_code: "en",
              person: person,
              primary_genre_id: Genre.active.first.id,
              sale_date: Date.today,
              secondary_genre_id: nil
            }
          }

          post "/api/backstage/album_app/albums", params: params

          expect(Single.count).to eq(single_count + 1)
          expect(Song.count).to eq(song_count + 1)

          song = person.singles.first.song
          expect(song.name).to eq "Too Many Cooks"
        end
      end
    end

    context "when data is invalid" do
      it "does not create an album and returns a JSON error" do
        album_count = Album.count
        person = create(:person)
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)
        params = {
          album: {
            name:                nil,
            album_type:          "Album",
            golive_date:         {hour: "01", min: '01', month: '12', day: '12', year: (DateTime.now + 1.year).strftime('%Y'), meridian: 'am'},
            timed_release_timing_scenario: 'relative_time',
            is_various:          true,
            language_code:       "en",
            person:              person,
            primary_genre_id:    nil,
            sale_date:           Date.today,
            secondary_genre_id:  nil,
          }
        }
        post "/api/backstage/album_app/albums", params: params

        expect(Album.count).to eq(album_count)

        expect(JSON.parse(response.body)["errors"].present?).to be_truthy
      end
    end
  end
end
