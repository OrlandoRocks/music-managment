require "rails_helper"

describe "POST /api/backstage/song_data" do
  it "returns song data without a root" do
    person = create(:person, :with_live_album)
    song_name = "Song Name"
    album_id = person.albums.first.id
    song_role = SongRole.find_by(role_type: "songwriter")
    params = {
      name: song_name,
      album_id: album_id,
      explicit: true,
      artists: [
        {
          role_ids: [song_role.id]
        }
      ],
      song_copyright_claims: {
        has_claimant: nil
      }
    }
    allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)

    post "/api/backstage/song_data", params: { song: params }

    response_body = JSON.parse(response.body)
    expect(response_body["data"]["name"]).to eq song_name
    expect(response_body["data"]["album_id"]).to eq album_id
  end
end

describe "PUT /api/backstage/song_data" do
  it "returns updated song data without a root" do
    person = create(:person, :with_live_album)
    song = create(:song, album: person.albums.first)
    song_role = SongRole.find_by(role_type: "songwriter")
    params = {
      id: song.id,
      name: song.name,
      album_id: song.album.id,
      explicit: true,
      artists: [
        {
          role_ids: [song_role.id]
        }
      ],
      song_copyright_claims: {
        has_claimant: nil
      }
    }
    allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)

    put "/api/backstage/song_data/#{song.id}", params: { song: params }

    response_body = JSON.parse(response.body)
    expect(response_body["data"]["id"]).to eq song.id
  end
end

describe "DELETE /api/backstage/song_data" do
  context "when the song is the current user's track" do
    it "deletes the track" do
      person = create(:person, :with_live_album)
      song = create(:song, album: person.albums.first)
      song.album.update(legal_review_state: "NEEDS REVIEW", finalized_at: nil, payment_applied: false)
      allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)

      delete "/api/backstage/song_data/#{song.id}", params: { id: song.id }

      expect(person.reload.songs).not_to exist
      expect(Note.last.related.id).to eq song.album.id
      expect(response.body).to be_empty
    end
  end

  context "when the song is not the current user's track" do
    it "does not delete the track" do
      person = create(:person, :with_live_album)
      song = create(:song, album: person.albums.first)
      another_person = create(:person)
      allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(another_person)

      expect { delete "/api/backstage/song_data/#{song.id}", params: { id: song.id } }
        .to raise_error(ActiveRecord::RecordNotFound)
    end
  end
end
