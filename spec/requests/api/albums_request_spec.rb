require "rails_helper"

describe Api::AlbumsController do
  describe "POST #create" do
    context "when album params contain an attribute that is NOT whitelisted" do
      it "should NOT create an album" do
        person = create(:person)
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)

        params = {
          album: {
            invalid_attribute: "Songs of Love"
          },
          person_id: person.id,
          api_key: API_CONFIG["albums"]["API_KEY"]
        }

        expect { post "/api/albums", params: params }.not_to change(person.albums, :count)
        expect(response.status).to eq 422
      end
    end

    context "when album params are whitelisted" do
      it "creates an album" do
        person = create(:person)
        album_name = "Songs of Love"
        artist_name = "Fake Band Name"
        primary_genre_id = 23
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)

        params = {
          album: {
            name: album_name,
            creatives: [{ role: "primary_artist", name: artist_name }],
            golive_date: Time.now + 1.days,
            is_various: "0",
            label_name: "The Medicine of Music",
            language_code_legacy_support: "en",
            optional_upc_number: "884088560058",
            orig_release_year: "",
            previously_released: false,
            primary_genre_id: primary_genre_id,
            recording_location: "Bushwick, NYC",
            sale_date: "July 1, 2015",
            secondary_genre_id: "",
            timed_release_timing_scenario: "absolute_time"
          },
          person_id: person.id,
          api_key: API_CONFIG["albums"]["API_KEY"]
        }

        post "/api/albums", params: params

        new_album = person.albums.find_by(name: album_name)
        new_artist_name = new_album.creatives.first.artist.name

        expect(new_album.name).to eq album_name
        expect(new_artist_name).to eq artist_name
        expect(new_album.primary_genre.id).to eq primary_genre_id
      end

      it "creates an album creation note" do
        person = create(:person)
        allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)

        params = {
          album: {
            name: "Songs of Love",
            creatives: [{ role: "primary_artist", name: "Artist Name" }],
            orig_release_year: "",
            primary_genre_id: 23,
            sale_date: "July 1, 2015"
          },
          person_id: person.id,
          api_key: API_CONFIG["albums"]["API_KEY"]
        }

        post "/api/albums", params: params
        new_album = person.albums.find_by(name: "Songs of Love")

        expect(new_album.notes).to exist
      end
    end
  end
end
