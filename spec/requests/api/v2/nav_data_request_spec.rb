require "rails_helper"

describe "GET /api/v2/nav_data", type: :request do
  let!(:locale) { I18n.locale }

  after { I18n.locale = locale }

  context "with bad token" do
    it "returns 401" do
      get api_v2_nav_data_path

      expect(response.status).to eq(401)
    end
  end

  context "with good token" do
    let!(:person) { create(:person) }
    let!(:token) { Api::V2::BaseController.new.send(:generate_jwt, person) }

    it "returns nav data" do
      get api_v2_nav_data_path,
          headers: { "HTTP_AUTHORIZATION" => "Bearer #{token}" }

      expect(response.status).to eq(200)

      parsed = JSON.parse(response.body)
      expect(parsed.keys)
        .to eq(%w[
                admin
                cart_items_count
                country_website
                current_lang
                current_locale_lang_abbrv
                language_selector_options
                plan
                publishing_url
                show
                translations
                unseen_notifications_count
                user_id
            ])
      expect(parsed["user_id"]).to eq person.id
    end

    context "locale" do
      it "uses tc_lang" do
        get api_v2_nav_data_path,
            headers: { "HTTP_AUTHORIZATION" => "Bearer #{token}" },
            params: { "tc_lang" => "fr" }

        expect(I18n.locale).to eq(:"fr")
      end

      it "uses tld default" do
        get api_v2_nav_data_path,
            headers: { "HTTP_AUTHORIZATION" => "Bearer #{token}" },
            params: { "tld" => "fr" }

        expect(I18n.locale).to eq(:"fr")
      end

      it "uses tld when tc_lang is invalid" do
        get api_v2_nav_data_path,
            headers: { "HTTP_AUTHORIZATION" => "Bearer #{token}" },
            params: { "tc_lang" => "foo", "tld" => "fr" }

        expect(I18n.locale).to eq(:"fr")
      end

      it "tc_lang preferred to tld" do
        get api_v2_nav_data_path,
            headers: { "HTTP_AUTHORIZATION" => "Bearer #{token}" },
            params: { "tc_lang" => "en-ca", "tld" => "fr" }

        expect(I18n.locale).to eq(:"en-ca")
      end

      it "bad tld falls back to en-us" do
        get api_v2_nav_data_path,
            headers: { "HTTP_AUTHORIZATION" => "Bearer #{token}" },
            params: { "tld" => "foo" }

        expect(I18n.locale).to eq(:"en-us")
      end

      it "prefers user preference over tld" do
        person.update(country_website_language_id: 9)
        get api_v2_nav_data_path,
            headers: { "HTTP_AUTHORIZATION" => "Bearer #{token}" },
            params: { "tld" => "foo" }

        expect(I18n.locale).to eq(:"pt-us")
      end
    end
  end
end
