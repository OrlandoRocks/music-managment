require "rails_helper"

describe "POST /api/v2/web/refresh_tokens", type: :request do
  include V2::JwtHelper

  let!(:payload) do
    {
      id: 123,
      email: "jwt@t.co",
      name: "JWT Person",
      roles: ["user"]
    }
  end
  let!(:valid_token) { generate_jwt(payload: payload) }
  let!(:duration) { V2::JwtHelper::EXPIRY_DURATION }

  before do
    save_token_digest(valid_token, duration, payload[:id])
  end

  after do
    digested = digest(valid_token)
    delete_digest(digest_key(digested))
  end

  context "without header token" do
    it "returns 401" do
      post api_v2_web_refresh_tokens_path

      expect(response.status).to eq(401)
    end
  end

  context "with unknown header token" do
    it "returns 401" do
      headers = { "HTTP_AUTHORIZATION" => "foo" }
      post api_v2_web_refresh_tokens_path, headers: headers

      expect(response.status).to eq(401)
    end
  end

  context "with known header token" do
    it "returns new token" do
      headers = { "HTTP_AUTHORIZATION" => valid_token }
      post api_v2_web_refresh_tokens_path, headers: headers

      expect(response.status).to eq(200)

      nonce = JSON.parse(response.body)["nonce"]
      delete_nonce(nonce)
      expect(nonce).to be_truthy

      token = JSON.parse(response.body)["token"]
      digested = digest(token)
      delete_digest(digest_key(digested))

      expect(token).to be_truthy
    end
  end
end
