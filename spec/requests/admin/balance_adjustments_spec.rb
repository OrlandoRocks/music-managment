require "rails_helper"

describe "POST /admin/people/:id/balance_adjustments" do
  it "creates a new balance adjustment" do
    person = create(:person, :with_balance, amount: 50_000)
    admin = create(:person, :admin)
    admin.roles << Role.new(name: "Refunds")
    set_current_user(admin)
    params = {
      category: "Refund - Renewal",
      customer_note: "fs",
      admin_note: "fds",
      credit_from_form: 10.0,
      debit_from_form: 0,
      disclaimer: 1
    }

    post "/admin/people/#{person.id}/balance_adjustments", params: { balance_adjustment: params, person_id: person.id }

    expect(BalanceAdjustment.last.person).to eq person
  end
end
