require "rails_helper"

describe "PATCH /admin/payout_service_fees/:id" do
  it "updates payout fee amount" do
    person = create(:person)
    admin = create(:person, :admin)
    payout_service_fee =  create(:payout_service_fee)
    set_current_user(admin)

    allow_any_instance_of(Admin::PayoutServiceFeesController).to receive(:redirect_restrict_access).and_return(true)

    params = {
      payout_service_fee: {
        amount: 30
      },
      id: payout_service_fee.id
    }

    put "/admin/payout_service_fees/#{payout_service_fee.id}", params: params

    expect(response).to redirect_to admin_payout_service_fees_path
    expect(payout_service_fee.reload.amount.to_i).to eq(30)
  end
end