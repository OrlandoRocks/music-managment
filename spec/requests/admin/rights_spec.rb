require "rails_helper"

describe "PATCH /admin/rights/:id" do
  it "updates a user's profile" do
    person = create(:person)
    admin = create(:person, :admin)
    role = create(:role)
    set_current_user(admin)
    params = {
      id: person.id,
      roles: [role.id]
    }

    patch "/admin/rights/#{person.id}", params: params

    expect(person.roles).to include(role)
  end
end
