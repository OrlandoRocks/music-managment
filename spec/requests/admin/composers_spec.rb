require "rails_helper"

describe "GET /admin/composers/list" do
  it "renders a list view" do
    person = create(:person, :with_balance, amount: 50_000)
    admin = create(:person, :admin)
    admin.roles << Role.new(name: "Publishing Viewer")
    admin.roles << Role.new(name: "Publishing Manager")
    set_current_user(admin)

    get "/admin/composers/list"

    expect(response).to be_successful
    expect(response).to render_template('list')
  end
end

describe "GET /admin/composers/download_client_import" do
  context "when passed in a proper date param" do
    it "returns a success response" do
      person = create(:person, :with_balance, amount: 50_000)
      admin = create(:person, :admin)
      admin.roles << Role.new(name: "Publishing Viewer")
      admin.roles << Role.new(name: "Publishing Manager")
      set_current_user(admin)

      from_date = Date.today.strftime("%m/%d/%Y")

      params = {
        d: {
          from_date: from_date
        }
      }

      get "/admin/composers/download_client_import", params: params

      expect(response).to be_successful
    end
  end
end
