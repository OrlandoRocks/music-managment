require "rails_helper"

describe "POST /admin/targeted_offers/:targeted_offer_id/targeted_products" do
  it "creates a new targeted product" do
    admin = create(:person, :admin)
    targeted_offer = create(:targeted_offer, created_by_id: admin.id)
    product = create(:product)
    set_current_user(admin)
    params = {
      product_id: product.id,
      display_name: "10% off discount",
      flag_text: "test",
      price_adjustment_type: "percentage_off",
      price_adjustment: 10.0,
      price_adjustment_description: "description",
      show_expiration_date: true,
      sort_order: 1,
      targeted_offer_id: targeted_offer.id,
      renewals_past_due: true,
      renewals_due: true,
      renewals_upcomming: true
    }

    post "/admin/targeted_offers/#{targeted_offer.id}/targeted_products", params: { targeted_product: params, targeted_offer_id: targeted_offer.id }

    expect(response).to have_http_status :ok
    expect(response.body).to include "Product Added to Offer."
    expect(targeted_offer.targeted_products.first.display_name).to eq "10% off discount"
  end

  it "creates all products into targeted products" do
    admin = create(:person, :admin)
    targeted_offer = create(:targeted_offer, created_by_id: admin.id)

    all_product_count = Product.where(country_website_id: targeted_offer.country_website_id).count

    set_current_user(admin)
    params = {
      product_id: :all_products,
      display_name: "10% off discount",
      flag_text: "test",
      price_adjustment_type: "percentage_off",
      price_adjustment: 10.0,
      price_adjustment_description: "description",
      show_expiration_date: true,
      sort_order: 1,
      targeted_offer_id: targeted_offer.id,
      renewals_past_due: true,
      renewals_due: true,
      renewals_upcomming: true
    }

    post "/admin/targeted_offers/#{targeted_offer.id}/targeted_products", params: { targeted_product: params, targeted_offer_id: targeted_offer.id }

    expect(response).to have_http_status :ok
    expect(response.body).to include "Product Added to Offer."
    expect(targeted_offer.targeted_products.count).to eq(all_product_count)
  end
end

describe "PATCH /admin/targeted_offers/:targeted_offer_id/targeted_products/:id" do
  it "updates an existing targeted product" do
    admin = create(:person, :admin)
    targeted_offer = create(:targeted_offer, created_by_id: admin.id)
    product = create(:product)
    targeted_product = create(:targeted_product, display_name: "Holiday Special", product: product, targeted_offer: targeted_offer)
    set_current_user(admin)
    params = {
      display_name: "New user offer"
    }

    patch "/admin/targeted_offers/#{targeted_offer.id}/targeted_products/#{targeted_product.id}", params: { targeted_product: params, targeted_offer_id: targeted_offer.id, id: targeted_product.id }

    expect(response.status).to eq 200
    expect(response.body).to include "Product saved"
    expect(targeted_product.reload.display_name).to eq "New user offer"
  end
end
