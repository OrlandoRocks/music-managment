require "rails_helper"

describe "POST /admin/cert_batches" do
  it "creates a new cert batches" do
    admin = create(:person, :admin)
    set_current_user(admin)

    params = {"cert_batch":
                {
                  "promotion"=>"laksdmf",
                  "description"=>"lkmasdf",
                  "cert_engine"=>"FreeStores",
                  "engine_params"=>"asdf",
                  "spawning_code"=>"asdf",
                  "brand_code"=>"ffffff",
                  "expiry_date"=>"12/25/2019"
                }
             }

    expect {
      post "/admin/cert_batches", params: params
    }.to change(CertBatch, :count).by(1)
  end
end

describe "PATCH /admin/cert_batches/:id" do
  it "updates cert batches" do
    admin = create(:person, :admin)
    set_current_user(admin)
    cert_batch = create(:cert_batch)

    params = {
      "cert_batch" => {
        "promotion"=>"99999",
        "description"=>"asdf",
        "cert_engine"=>"FreeStores",
        "engine_params"=>"fffvvvv",
        "spawning_code"=>"rrrrr",
        "brand_code"=>"fffsss",
        "expiry_date"=>"12/25/2019"
      },
      "id" => cert_batch.id
    }

    patch "/admin/cert_batches/#{cert_batch.id}", params: params

    expect(cert_batch.reload.description).to eq params["cert_batch"]["description"]
    expect(cert_batch.reload.expiry_date).to eq(Time.new(2019, 12, 25))
  end
end

describe "POST /admin/cert_batches/:id/exp" do
  it "updates the cert batch's expiry date" do
    admin = create(:person, :admin)
    set_current_user(admin)
    cert_batch = create(:cert_batch, expiry_date: Time.current)
    expiry_date = Time.current.yesterday

    params = {
      cert: {
        expiry_date: expiry_date.strftime("%m/%d/%Y")
      },
      id: cert_batch.id
    }

    post "/admin/cert_batches/#{cert_batch.id}/exp", params: params

    expect(cert_batch.reload.expiry_date.to_date).to eq expiry_date.to_date
  end
end

describe "POST /admin/cert_batches/:id/admin_only" do
  it "updates the cert batch's admin only attribute" do
    admin = create(:person, :admin)
    set_current_user(admin)
    cert_batch = create(:cert_batch, admin_only: false)

    params = {
      cert: {
        admin_only: true
      },
      id: cert_batch.id
    }

    post "/admin/cert_batches/#{cert_batch.id}/admin_only", params: params

    expect(cert_batch.reload.admin_only).to eq(true)
  end
end
