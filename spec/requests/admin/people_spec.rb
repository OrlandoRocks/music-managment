require "rails_helper"

describe "PATCH /admin/people/:id/update_profile" do
  it "updates a user's profile" do
    person = create(:person)
    admin = create(:person, :admin)
    set_current_user(admin)
    params = {
      id: person.id,
      email: "newemail@tunecore.com"
    }

    patch "/admin/people/#{person.id}/update_profile", params: { person: params }

    expect(person.reload.email).to eq "newemail@tunecore.com"
  end
end
