require "rails_helper"

describe "POST /admin/review_reasons" do
  it "creates a review reason" do
    person = create(:person)
    admin = create(:person, :admin)

    set_current_user(admin)

    allow_any_instance_of(Admin::ReviewReasonsController).to receive(:content_manager_role_required).and_return(true)

    params = { review_reason: { role_ids: [],
                                reason_type: "FLAGGED",
                                reason: "asdfasdf",
                                email_template_path: "album_blocked_from_sales_cover_song_formatting",
                                should_unfinalize: "false"
                               },
               id: 1
             }

    expect {
      post "/admin/review_reasons", params: params
    }.to change(ReviewReason, :count).by(1)
  end
end

describe "PATCH /admin/review_reasons/:id" do
  it "creates a review reason" do
    person = create(:person)
    admin = create(:person, :admin)
    review_reason = create(:review_reason)
    set_current_user(admin)

    allow_any_instance_of(Admin::ReviewReasonsController).to receive(:content_manager_role_required).and_return(true)

    params = { review_reason: { role_ids: [],
                                reason_type: 'NOT SURE',
                                reason: "No reason",
                                email_template_path: "album_blocked_from_sales_cover_song_formatting",
                                should_unfinalize: "false"
    },
               id: 1
    }

    patch "/admin/review_reasons/#{review_reason.id}", params: params

    expect(review_reason.reload.reason).to eq('No reason')
    expect(review_reason.reload.reason_type).to eq('NOT SURE')
  end
end


