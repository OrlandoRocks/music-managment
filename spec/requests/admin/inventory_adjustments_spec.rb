require "rails_helper"

describe "POST /admin/people/:person_id/credits/:inventory_id/inventory_adjustments" do
  it "creates a new inventory adjustment" do
    admin = create(:person, :admin)
    admin.roles << Role.new(name: "Refunds")
    set_current_user(admin)
    inventory = create(:album_credit_inventory)
    person = inventory.person
    params = {
      rollback: 0,
      amount: 10,
      admin_note: "fds",
      category: "Other",
      inventory_id: inventory.id
    }

    post "/admin/people/#{person.id}/credits/#{inventory.id}/inventory_adjustments", params: { inventory_adjustment: params, person_id: person.id }

    expect(InventoryAdjustment.last.inventory).to eq inventory
  end
end
