require "rails_helper"

describe "POST /band_photos", skip: true do
  context "when band photo is valid" do
    it "creates a new band photo" do
      person = create(:person)
      set_current_user(person)
      params = {
        width: 768,
        size: 1024,
        height: 1024,
        bit_depth: 32,
        mime_type: "image/jpeg",
        description: "Band photo taken in Paris",
        file: fixture_file_upload('background_cover_art.jpg')
      }

      post "/band_photos", params: { band_photo: params }

      expect(BandPhoto.last.person).to eq person
      expect(response).to redirect_to band_photos_path
    end
  end

  context "when band photo is a valid .gif" do
    it "creates a new band photo" do
      person = create(:person)
      set_current_user(person)
      params = {
        width: 768,
        size: 1024,
        height: 1024,
        bit_depth: 32,
        mime_type: "image/jpeg",
        description: "Band photo taken in Paris",
        file: fixture_file_upload('ajax-loader.gif')
      }

      post "/band_photos", params: { band_photo: params }

      expect(BandPhoto.last.person).to eq person
      expect(response).to redirect_to band_photos_path
    end
  end

  context "when band photo is not valid" do
    it "does not create a new band photo" do
      person = create(:person)
      set_current_user(person)
      params = { description: "Band photo taken in Paris" }

      post "/band_photos", params: { band_photo: params }

      expect(response.body).to include "Band Photo"
    end
  end
end

describe "PUT /band_photos", skip: true do
  it "updates an existing band photo" do
    band_photo = create(:band_photo)
    person = band_photo.person
    set_current_user(person)
    description = "This is my new band photo description"
    params = {
      description: description,
      file: fixture_file_upload('background_cover_art.jpg')
    }

    put "/band_photos/#{band_photo.id}", params: { band_photo: params }

    expect(band_photo.reload.description).to eq description
    expect(response).to redirect_to band_photos_path
  end
end

describe "DELETE /band_photos", skip: true do
  it "deletes an existing band photo" do
    band_photo = create(:band_photo)
    person = band_photo.person
    set_current_user(person)
    params = {
      id: band_photo.id
    }

    delete "/band_photos/#{band_photo.id}", params: params

    expect { band_photo.reload }.to raise_error(ActiveRecord::RecordNotFound)
    expect(response).to redirect_to band_photos_path
  end
end
