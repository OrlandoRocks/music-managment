require "rails_helper"

describe ResetPasswordsController, type: :request do
  describe "PUT #update" do
    context "when the user is updated successfully" do
      it "resets the users password" do
        person = create(:person, password: "DefaultTestpass123!", password_confirmation: "DefaultTestpass123!")
        new_password = "Testpass123!"
        allow(Person).to receive(:find).and_return(person)
        allow(person).to receive(:valid_invite?).and_return(true)

        params = {
          person_id: person.id,
          key: 12345,
          person: {
            password: new_password,
            password_confirmation: new_password,
            new_password: "true"
          }
        }

        put "/reset_password", params: params

        expect(person.is_password_correct?("Testpass123!")).to be_truthy
        expect(response).to redirect_to dashboard_path
      end

      it "sends the user an email" do
        person = create(:person, password: "DefaultTestpass123!", password_confirmation: "DefaultTestpass123!")
        new_password = "Testpass123!"
        allow(Person).to receive(:find).and_return(person)
        allow(person).to receive(:valid_invite?).and_return(true)

        params = {
          person_id: person.id,
          key: 12345,
          person: {
            password: new_password,
            password_confirmation: new_password,
            new_password: "true"
          }
        }

        person_notifier_dbl = double(:person_notifier_dbl)
        expect(PersonNotifier).to receive(:change_password).with(person, "Testpass123!").and_return(person_notifier_dbl)
        expect(person_notifier_dbl).to receive(:deliver)

        put "/reset_password", params: params
      end

      it "creates a logged in event" do
        person = create(:person, password: "DefaultTestpass123!", password_confirmation: "DefaultTestpass123!")
        new_password = "Testpass123!"
        allow(Person).to receive(:find).and_return(person)
        allow(person).to receive(:valid_invite?).and_return(true)

        params = {
          person_id: person.id,
          key: 12345,
          person: {
            password: new_password,
            password_confirmation: new_password,
            new_password: "true"
          }
        }

        expect(person).to receive(:create_logged_in_event)

        put "/reset_password", params: params
      end
    end

    context "when the user is not updated" do
      it "renders the show page" do
        person = create(:person, password: "DefaultTestpass123!", password_confirmation: "DefaultTestpass123!")
        allow(Person).to receive(:find).and_return(person)
        allow(person).to receive(:valid_invite?).and_return(true)

        params = {
          person_id: person.id,
          key: 12345,
          person: {
            password: "Testpass123!",
            password_confirmation: "Testpass",
            new_password: "true"
          }
        }

        put "/reset_password", params: params

        expect(response).to render_template :show
      end
    end
  end
end
