require 'rails_helper'

RSpec.describe FeatureSpecDbProxy::RemoteActiveRecordBaseProxiesController, type: :request do
  describe "DELETE /__active-record__" do
    it "delegates .delete_all to the specified model" do
      args = { "model" => "Person" }

      allow(Person).to receive(:delete_all)

      delete "/__active-record__", params: args

      expect(Person).to have_received(:delete_all)
    end

    it "returns 200 OK" do
      args = { "model" => "Person" }

      allow(Person).to receive(:delete_all)

      delete "/__active-record__", params: args

      expect(response.status).to eq(200)
    end
  end
end
