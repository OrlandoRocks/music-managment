require "rails_helper"

describe "POST /stored_credit_cards" do
  context "when successful" do
    it "creates a stored credit card with braintree" do
      person = create(:person, :with_address)
      allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)
      params = {
        customer_id: person.braintree_customer_id,
        braintree_payment_nonce: 'fake-valid-nonce',
        address1: person.address1,
        address2: person.address2,
        city: person.city,
        state: person.state,
        zip: person.zip,
        country: "US",
        firstname: person.name,
        lastname: person.name,
        tcs_return_msg: "true"
      }

      post "/stored_credit_cards", params: params

      expect(response.body).to include "payment_preferences"
      expect(StoredCreditCard.last.person_id).to eq person.id
      expect(Note.last.note_created_by_id).to eq person.id
      expect(Note.last.note).to include "added"
    end
  end
end

describe "DELETE /stored_credit_cards" do
  context "when deleting an existing non-preferred credit card" do
    it "creates a note for the user documenting the deleted card" do
      person = create(:person)
      allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)
      stored_credit_card = create(:stored_credit_card, person: person)
      stored_credit_card_id = stored_credit_card.id
      params = {
        id: stored_credit_card_id
      }

      delete "/stored_credit_cards/#{stored_credit_card_id}", params: params

      expect(Note.last.note).to eq "Credit Card ************#{stored_credit_card.last_four} Deleted"
      expect(Note.last.note_created_by_id).to eq person.id
      expect(response.body).to include "payment_preferences"
    end
  end

  context "when deleting an existing preferred credit card" do
    it "fails to delete the credit card" do
      person = create(:person)

      allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(person)
      stored_credit_card = create(:stored_credit_card, person: person)
      stored_credit_card_id = stored_credit_card.id
      create(
        :person_preference,
        person: person,
        preferred_credit_card_id: stored_credit_card_id,
        preferred_payment_type: "CreditCard",
        do_not_autorenew: 1
      )
      params = {
        id: stored_credit_card_id
      }

      delete "/stored_credit_cards/#{stored_credit_card_id}", params: params

      expect(Note.count).to eq 0
      expect(response.body).to include "payment_preferences"
    end
  end
end
