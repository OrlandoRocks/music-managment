require 'rails_helper'

RSpec.describe FeatureSpecDbProxy::RemoteFactoryProxiesController, type: :request do
  describe "POST /__factory-bot__" do
    it "delegates creation to the specified FactoryBot factory" do
      args = ["person", {"email"=>"test@example.com"}]

      allow(FactoryBot).to receive(:create)

      post "/__factory-bot__", params: { _json: args, format: :json }

      expect(FactoryBot)
        .to have_received(:create)
        .with("person", "email" => "test@example.com")
    end

    it "returns a json serialization of the object" do
      expected_json = { is_foobar: true }.to_json
      created_object = double('some_object', { as_json: expected_json })
      args = ["person", {"email"=>"test@example.com"}]

      allow(FactoryBot)
        .to receive(:create)
        .and_return(created_object)

      post "/__factory-bot__", params: { _json: args, format: :json }

      expect(response.body).to eq expected_json
    end
  end

  describe "PATCH /__factory-bot__" do
    context "when the object exists" do
      it "updates the object" do
        existing_person = create(:person)

        attributes = { name: "New name" }
        params = {
          model: "Person",
          id: existing_person.id,
          attributes: attributes
        }

        patch "/__factory-bot__", params: { **params, format: :json }

        expect(existing_person.reload.name).to eq "New name"
      end

      it "returns a json serialization of the object" do
        existing_person = create(:person)
        expected_json = { is_foobar: true }.to_json
        attributes = { name: "New name" }
        params = {
          model: "Person",
          id: existing_person.id,
          attributes: attributes
        }

        allow(Person)
          .to receive(:find_by)
          .and_return(existing_person)

        allow(existing_person)
          .to receive(:as_json)
          .and_return(expected_json)

        patch "/__factory-bot__", params: { **params, format: :json}

        expect(response.body).to eq expected_json
      end
    end

    context "when the object does not exist" do
      it "returns a 404" do
        params = { model: "Person", id: nil }

        patch "/__factory-bot__", params: { **params, format: :json }

        expect(response.status).to eq 404
      end
    end
  end
end
