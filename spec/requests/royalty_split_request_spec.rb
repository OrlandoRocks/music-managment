require "rails_helper"

describe RoyaltySplitsController, type: :request do
  context "when logged in with free account" do
    let(:logged_in_user) { create :person, plan: Plan.first }
    let!(:logged_in_user_w9) { create :tax_form, :current_active_individual_w9, person: logged_in_user }
    before do
      allow(FeatureFlipper).to receive(:show_feature?).and_call_original
      allow(FeatureFlipper).to receive(:show_feature?).with(:royalty_splits, logged_in_user).and_return(true)
      set_current_user(logged_in_user)
    end
    describe "GET albums_royalty_splits_path" do
      let(:album) { create :album, :with_uploaded_songs, person: logged_in_user }
      before do
        get albums_royalty_splits_path(album_ids: [album.id])
      end
      it { expect(response).to have_http_status(:forbidden) }
    end
    describe "GET royalty_splits_path" do
      before do
        get royalty_splits_path
      end
      it { expect(response).to have_http_status(:forbidden) }
    end
    describe "POST add_songs_to_royalty_splits_path" do
      before do
        post add_songs_to_royalty_splits_path
      end
      it { expect(response).to have_http_status(:forbidden) }
    end
    describe "GET royalty_split_path" do
      let!(:royalty_split) { royalty_split_with_recipients owner: logged_in_user }
      before do
        get royalty_split_path(royalty_split)
      end
      it { expect(response).to have_http_status(:forbidden) }
    end
    describe "GET invited_splits_path" do
      before do
        get invited_royalty_splits_path
      end
      subject { request }
      it { should render_template(:invited_splits) }
    end
  end

  context "when logged in with paid account" do
    let(:password) { "Password123!" }
    let(:logged_in_user) { create :person, password: password, plan: Plan.last }
    let!(:logged_in_user_w9) { create :tax_form, :current_active_individual_w9, person: logged_in_user }
    before do
      allow(FeatureFlipper).to receive(:show_feature?).and_call_original
      allow(FeatureFlipper).to receive(:show_feature?).with(:royalty_splits, logged_in_user).and_return(true)
      set_current_user(logged_in_user)
    end

    describe "GET albums_royalty_splits_path" do
      context "when marked suspicious" do
        before do
          logged_in_user.update(status: "Suspicious")
          get albums_royalty_splits_path(album_ids: [create(:album).id])
        end
        it { expect(response).to have_http_status(:forbidden) }
      end

      context "with valid album" do
        let(:album) { create :album, :with_uploaded_songs, person: logged_in_user }
        before do
          get albums_royalty_splits_path(album_ids: [album.id])
        end
        subject { request }
        it { should render_template(:albums) }
      end

      context "with bad album_id" do
        before do
          get albums_royalty_splits_path(album_ids: [-1])
        end
        subject { request }
        it { expect(flash[:notice]).to eql(t("royalty_splits.albums_not_found_for_current_user")) }
        it { should redirect_to(:discography) }
      end
    end

    describe "GET royalty_splits_path" do
      context "when marked suspicious" do
        before do
          logged_in_user.update(status: "Suspicious")
          get royalty_splits_path
        end
        it { expect(response).to have_http_status(:forbidden) }
      end

      let!(:royalty_splits) { create_list :royalty_split, 3, owner: logged_in_user }
      let!(:album) { create :album, :with_uploaded_songs, person: logged_in_user }
      let!(:royalty_split_song) { create :royalty_split_song, song: album.songs.first }
      before do
        get royalty_splits_path
      end

      subject { request }
      it { should render_template(:index) }
    end

    describe "POST add_songs_to_royalty_splits_path" do
      context "when marked suspicious" do
        before do
          logged_in_user.update(status: "Suspicious")
          post add_songs_to_royalty_splits_path
        end
        it { expect(response).to have_http_status(:forbidden) }
      end

      context "with no parameters" do
        before do
          post add_songs_to_royalty_splits_path
        end

        subject { request }
        it { expect(response).to redirect_to(discography_path) }
        it { expect(flash[:notice]).to eql(t("royalty_splits.album_missing_from_url")) }
      end

      context "without unowned album" do
        let(:royalty_split) { create :royalty_split, owner: logged_in_user }
        let(:album) { create :album, :with_uploaded_songs }
        before do
          post add_songs_to_royalty_splits_path,
               params: { chosen_royalty_split_id: royalty_split.id, album_ids: [album.id] }
        end
        subject { request }
        it { expect(response).to redirect_to(discography_path) }
        it { expect(flash[:notice]).to eql(t("royalty_splits.albums_not_found_for_current_user")) }
      end

      context "without song_ids" do
        let(:royalty_split) { create :royalty_split, owner: logged_in_user }
        let(:album) { create :album, :with_uploaded_songs, person: logged_in_user }
        before do
          post add_songs_to_royalty_splits_path,
               params: { chosen_royalty_split_id: royalty_split.id, album_ids: [album.id] }
        end
        subject { request }
        it { expect(response).to have_http_status(:not_found) }
        it { expect(response.body).to include(t("royalty_splits.no_valid_songs_selected")) }
      end

      context "with valid, owned song_ids and no existing split selected or royalty_split params" do
        let(:albums) { create_list :album, 2, :with_uploaded_songs, person: logged_in_user }
        let(:songs) { create_list :song, 3, album: albums.sample }
        before do
          post add_songs_to_royalty_splits_path, params: { song_ids: songs.map(&:id), album_ids: albums.map(&:id) }
        end
        subject { request }
        it { expect(response).to have_http_status(:bad_request) }
        it { should_not render_template(:albums) }
      end

      context "with valid chosen_royalty_split_id and songs_ids" do
        let(:royalty_split) { royalty_split_with_recipients 3, owner: logged_in_user }
        let(:albums) { create_list :album, 2, :with_uploaded_songs, person: logged_in_user }
        let(:songs) { create_list :song, 3, album: albums.sample }
        before do
          allow(RoyaltySplits::NotificationService).to receive(:songs_added_to_split).and_call_original
          post add_songs_to_royalty_splits_path,
               params: {
                 chosen_royalty_split_id: royalty_split.id,
                 song_ids: songs.map(&:id),
                 album_ids: albums.map(&:id)
               }
          royalty_split.reload
        end
        subject { request }
        it { expect(flash[:notice]).to eql(t("royalty_splits.success")) }
        it { should redirect_to albums_royalty_splits_path(params: { album_ids: albums.map(&:id) }) }
        it { expect(royalty_split.songs).to include(*songs) }
        it "should call the notification service" do
          expect(RoyaltySplits::NotificationService).to have_received(:songs_added_to_split).with(
            split: royalty_split, new_songs: songs
          )
        end
      end

      context "with unowned chosen_royalty_split_id and valid songs_ids" do
        let(:royalty_split) { royalty_split_with_recipients }
        let(:albums) { create_list :album, 2, :with_uploaded_songs, person: logged_in_user }
        let(:songs) { create_list :song, 3, album: albums.sample }
        before do
          post add_songs_to_royalty_splits_path,
               params: {
                 chosen_royalty_split_id: royalty_split.id,
                 song_ids: songs.map(&:id),
                 album_ids: albums.map(&:id)
               }
        end
        subject { request }
        it { expect(flash[:alert]).to eql(t("royalty_splits.split_config_not_found_for_user")) }
        it { should redirect_to albums_royalty_splits_path(params: { album_ids: albums.map(&:id) }) }
      end

      context "with valid chosen_royalty_split_id, albums, and unowned songs_ids" do
        let(:royalty_split) { royalty_split_with_recipients owner: logged_in_user }
        let!(:albums) { create_list :album, 2, :with_uploaded_songs, person: logged_in_user }
        let(:songs) { create_list :song, 3 }
        before do
          post add_songs_to_royalty_splits_path,
               params: {
                 chosen_royalty_split_id: royalty_split.id,
                 song_ids: songs.map(&:id),
                 album_ids: albums.map(&:id)
               }
        end
        subject { request }
        it { should render_template(:albums) }
        it { expect(response.body).to include(t("royalty_splits.no_valid_songs_selected")) }
      end

      context "with valid chosen_royalty_split_id, song_ids, and unowned album_ids" do
        let(:royalty_split) { royalty_split_with_recipients owner: logged_in_user }
        let(:album) { create :album, :with_uploaded_songs, person: logged_in_user }
        let(:songs) { create_list :song, 3, album: album }
        let!(:unowned_albums) { create_list :album, 2, :with_uploaded_songs }
        before do
          post add_songs_to_royalty_splits_path,
               params: {
                 chosen_royalty_split_id: royalty_split.id,
                 song_ids: songs.map(&:id),
                 album_ids: unowned_albums.map(&:id)
               }
        end
        subject { request }
        it { should redirect_to discography_path }
        it { expect(flash[:notice]).to eql(t("royalty_splits.albums_not_found_for_current_user")) }
      end

      context "with valid royalty_split params for creation" do
        let(:royalty_split) { attributes_for :royalty_split, owner: logged_in_user }
        let(:owner_attributes) { attributes_for :royalty_split_recipient, person_id: logged_in_user.id, percent: 10 }
        let(:non_owner_attributes) { attributes_for :royalty_split_recipient, email: "text@example.com", percent: 90 }
        let(:album) { create :album, :with_uploaded_songs, person: logged_in_user }
        let(:songs) { create_list :song, 3, album: album }
        before do
          allow(RoyaltySplits::NotificationService).to receive(:songs_added_to_split).and_call_original
          post add_songs_to_royalty_splits_path,
               params: {
                 song_ids: songs.map(&:id),
                 album_ids: [album.id],
                 royalty_split: {
                   title: "Band test",
                   recipients_attributes: [owner_attributes, non_owner_attributes]
                 }
               }
        end
        subject { request }
        it { should redirect_to albums_royalty_splits_path(params: { album_ids: [album.id] }) }
        it { expect(flash[:notice]).to eql(t("royalty_splits.success")) }
        it "should not send a new song notification" do
          expect(RoyaltySplits::NotificationService).not_to have_received(:songs_added_to_split)
        end
      end
    end

    describe "GET royalty_split_path" do
      context "when marked suspicious" do
        before do
          logged_in_user.update(status: "Suspicious")
          get royalty_split_path(create(:royalty_split))
        end
        it { expect(response).to have_http_status(:forbidden) }
      end

      context "with valid royalty split" do
        let!(:royalty_split) { royalty_split_with_recipients owner: logged_in_user }
        before do
          get royalty_split_path(royalty_split)
        end

        subject { request }
        it { should redirect_to(edit_royalty_split_path royalty_split) }
      end

      context "with unowned royalty split" do
        let!(:royalty_split) { royalty_split_with_recipients }
        before do
          get royalty_split_path(royalty_split)
        end
        it { should redirect_to royalty_splits_path }
        it { expect(flash[:alert]).to eql(t("royalty_splits.split_config_not_found_for_user")) }
      end
    end

    describe "PATCH royalty_split_path" do
      context "when marked suspicious" do
        before do
          logged_in_user.update(status: "Suspicious")
          patch royalty_split_path(create(:royalty_split))
        end
        it { expect(response).to have_http_status(:forbidden) }
      end

      context "with valid royalty split params" do
        let!(:royalty_split) { create :royalty_split, owner: logged_in_user }
        let(:owner_recipient_attrs) do
          attributes_for :royalty_split_recipient,
                         id: royalty_split.owner_recipient.id,
                         percent: 51
        end
        let(:another_recipient_attrs) {
          attributes_for :royalty_split_recipient, email: "example@email.com", percent: 49
        }

        before do
          patch royalty_split_path(
            royalty_split, params: {
              royalty_split: {
                title: "new split title",
                recipients_attributes: [owner_recipient_attrs, another_recipient_attrs]
              }
            }
          )
          royalty_split.reload
        end

        it { expect(response).to redirect_to(edit_royalty_split_path royalty_split) }
        it { expect(royalty_split).to have_attributes(title: "new split title") }
        it { expect(royalty_split.recipients).to include(have_attributes(email: "example@email.com")) }
        it { expect(royalty_split.recipients).to include(have_attributes(person: logged_in_user)) }
      end

      context "with existing owner recipient split 0%" do
        let!(:royalty_split) { create :royalty_split, owner: logged_in_user }
        let(:owner_recipient) {
          attributes_for :royalty_split_recipient, id: royalty_split.owner_recipient.id, percent: 0
        }
        let(:first_recipient) {
          attributes_for :royalty_split_recipient, :with_email, email: "person1@example.com", percent: 51
        }
        let(:second_recipient) {
          attributes_for :royalty_split_recipient, :with_email, email: "person2@example.com", percent: 49
        }
        before do
          patch royalty_split_path(
            royalty_split, params: {
              royalty_split: {
                title: "new split title123",
                recipients_attributes: [first_recipient, second_recipient, owner_recipient]
              }
            }
          )
          royalty_split.reload
        end
        it { expect(response).to redirect_to(edit_royalty_split_path royalty_split) }
        it { expect(royalty_split).to have_attributes(title: "new split title123") }
        it { expect(royalty_split.recipients).to include(have_attributes(email: "person1@example.com")) }
        it { expect(royalty_split.recipients).to include(have_attributes(email: "person2@example.com")) }
        it { expect(royalty_split.recipients).to include(have_attributes(person_id: logged_in_user.id)) }
      end

      context "with destroyed royalty split recipient" do
        let!(:royalty_split) { create :royalty_split, owner: logged_in_user }
        let!(:recipient) { create :royalty_split_recipient, royalty_split: royalty_split }
        it "should delete recipients" do
          expect do
            patch royalty_split_path(
              royalty_split, params: {
                royalty_split: {
                  recipients_attributes: [
                    { id: recipient.id, _destroy: true }
                  ]
                }
              }
            )
          end.to change { RoyaltySplitRecipient.count }.by(-1)
          expect(response).to redirect_to(edit_royalty_split_path royalty_split)
        end
      end

      context "with unowned royalty split" do
        let!(:royalty_split) { create :royalty_split }
        before do
          patch royalty_split_path(royalty_split, params: { royalty_split: { title: "unused title" } })
        end
        it { should redirect_to royalty_splits_path }
        it { expect(flash[:alert]).to eql(t("royalty_splits.split_config_not_found_for_user")) }
      end
    end

    describe "DELETE royalty_split_path" do
      context "when marked suspicious" do
        before do
          logged_in_user.update(status: "Suspicious")
          delete royalty_split_path(create(:royalty_split))
        end
        it { expect(response).to have_http_status(:forbidden) }
      end

      context "with valid, empty, owned, royalty_split" do
        let!(:royalty_split) { create :royalty_split, owner: logged_in_user }
        it do
          expect do
            delete royalty_split_path(royalty_split)
          end.to change { RoyaltySplit.count }.by(-1)
        end
      end
    end

    describe "DELETE royalty_split_song_path" do
      context "when marked suspicious" do
        before do
          logged_in_user.update(status: "Suspicious")
          delete destroy_royalty_split_song_path(create(:royalty_split_song))
        end
        it { expect(response).to have_http_status(:forbidden) }
      end

      context "with valid, owned, royalty_split_song" do
        let(:royalty_split) { create :royalty_split, owner: logged_in_user }
        let(:song) { create :song, person: logged_in_user }
        let!(:royalty_split_song) { create :royalty_split_song, royalty_split: royalty_split, song: song }
        it do
          expect do
            delete destroy_royalty_split_song_path(royalty_split_song)
          end.to change { RoyaltySplitSong.count }.by(-1)
          expect(response.parsed_body["royalty_split_song"]).to include("id" => royalty_split_song.id)
        end
      end

      context "with valid, unowned, royalty_split_song" do
        let!(:royalty_split_song) { create :royalty_split_song }
        it do
          expect do
            delete destroy_royalty_split_song_path(royalty_split_song)
          end.to change { RoyaltySplitSong.count }.by(0)
          expect(response).to have_http_status(:forbidden)
          expect(response.body).to include(t("royalty_splits.forbidden"))
        end
      end

      context "with invalid id" do
        before do
          delete destroy_royalty_split_song_path("invalid-id")
        end
        subject { response }
        it { should have_http_status(:not_found) }
        it {
          expect(response.body).to include(t("royalty_splits.songs_split_config_not_found"))
        }
      end
    end
  end
end
