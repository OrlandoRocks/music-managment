require "rails_helper"

describe "POST /artwork" do
  context "with valid params" do
    it "creates artwork for the album" do
      person = create(:person)
      album = create(:album, person: person)
      set_current_user(person)
      params = {
        artwork: fixture_file_upload('album_artwork.tiff')
      }

      post "/albums/#{album.id}/artwork", params: params

      expect(status).to eq 200
      expect(album.artwork.last_errors).to be_nil
    end
  end

  context "without valid params" do
    it "creates artwork with an error message" do
      person = create(:person)
      album = create(:album, person: person)
      set_current_user(person)

      post "/albums/#{album.id}/artwork"

      expect(status).to eq 200
      expect(album.artwork.last_errors).to eq(
        I18n.t("controllers.artwork.try_again")
      )
    end
  end
end
