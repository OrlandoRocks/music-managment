require 'rails_helper'

shared_examples_for 'ruleable' do
  let(:resource) { described_class }

  describe '.engine' do
    it 'has a Rules Engine' do
      expect(described_class.engine.class).to eq RulesEngine::RuleRegistry
    end

    it 'has the expression `list_of` in its registry' do
      expect(described_class.engine.registry.keys).to include(:list_of)
    end
  end

  describe '#define_rules' do
    it 'can add an aribtrary rule' do
      described_class.define_rules do
        rule :add, ->(a, b) { a + b }
      end
      expect(described_class.engine.registry.keys).to include(:add)
    end
  end

  describe '#evaluate_rule' do
    let(:random_number) { rand(1_000_000) }

    it 'will evaluate a single rule if found in the DB' do
      described_class.define_rules do
        rule :add_5, ->(config, test_number) { config[:value] + test_number }
      end

      expect(
        described_class.evaluate_rule(enabled: true, name: :add_5, category: :test, against: random_number)
      ).to be(random_number + 5)
    end
  end

  describe '#evaluate_rules' do
    let(:random_number) { rand(1_000_000) }

    it 'will raise an `RuleableError` if no rules found in DB' do
      described_class.define_rules do
        rule :add, ->(a, b) { a + b }
      end
      expect {
        described_class.evaluate_rule(name: :foo, enabled: false, category: :none, against: :none)
      }.to raise_error(Ruleable::ClassMethods::RuleableError)
    end

    it 'will evaluate all enabled rules if found in the DB' do
      described_class.define_rules do
        rule :add_5, ->(config, test_number) { config[:value] + test_number }
        rule :div_5, ->(config, test_number) { config[:value] / test_number }
        rule :mult_5, ->(config, test_number) { config[:value] * test_number }
      end
      expect(
        described_class.evaluate_rules(category: :test, against: random_number)
      ).to eq([random_number + 5, random_number * 5])
    end

  end

  describe '#evaluate_rules' do
    let(:random_number) { rand(1_000_000) }

    it "will show the evaluation strategy for a rule set" do
      described_class.define_rules do
        rule :add_5, ->(config, test_number) { config[:value] + test_number }
        rule :div_5, ->(config, test_number) { config[:value] / test_number }
        rule :mult_5, ->(config, test_number) { config[:value] * test_number }
      end

      expect(
        described_class.evaluation_strategy(random_number, :test)
      ).to eq([
          ["add_5", {"value"=>5}, random_number],
          ["mult_5", {"value"=>5}, random_number]
        ])
    end
  end

  describe '#enabled_and_registered_rule_names' do
    let(:random_number) { rand(1_000_000) }
    it "includes the enabled rule names from the DB" do
      described_class.define_rules do
        rule :add_5, ->(config, test_number) { config[:value] + test_number }
        rule :div_5, ->(config, test_number) { config[:value] / test_number }
        rule :mult_5, ->(config, test_number) { config[:value] * test_number }
      end

      expect(
        described_class.new(random_number).enabled_and_registered_rule_names(random_number, :test)
      ).to eq([:add_5, :mult_5])
    end
  end
end