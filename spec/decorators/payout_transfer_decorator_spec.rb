require 'rails_helper'

describe PayoutTransferDecorator do
  let(:payout_transfer) { create(:payout_transfer) }

  describe "#status" do
    it "shows the payout transfer status" do
      expect(PayoutTransferDecorator.new(payout_transfer).status).to eq(custom_t(:submitted))
    end

    it "returns completed if provider_status is completed" do
      payout_transfer.provider_status = PayoutTransfer::COMPLETED
      payout_transfer.save
      expect(PayoutTransferDecorator.new(payout_transfer).status).to eq(custom_t(:completed))
    end

    it "returns denied if provider_status is denied" do
      payout_transfer.provider_status = PayoutTransfer::DECLINED
      payout_transfer.save
      expect(PayoutTransferDecorator.new(payout_transfer).status).to eq(custom_t(:declined))
    end
  end

  describe "#to_money" do
    it "converts a payout transfer to money" do
      money = PayoutTransferDecorator.new(payout_transfer).to_money
      expect(money).to be_a(Money)
    end
  end
end
