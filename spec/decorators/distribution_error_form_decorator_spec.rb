require "rails_helper"

describe DistributionErrorFormDecorator do
  describe "#table_title" do
    context "when the filter type is 'store'" do
      let(:distro_error_form)           { Distribution::ErrorForm.new({}).save }
      let(:decorated_distro_error_form) { DistributionErrorFormDecorator.new(distro_error_form) }
      let(:spotify_store_id)            { Store.find_by(abbrev: "sp").id }

      it "returns the store name" do
        expect(decorated_distro_error_form.table_title(spotify_store_id)).to eq "Spotify"
      end
    end

    context "when the filter type is not 'store'" do
      let(:distro_error_form)           { Distribution::ErrorForm.new(filter_type: "release").save }
      let(:decorated_distro_error_form) { DistributionErrorFormDecorator.new(distro_error_form) }

      it "returns the title" do
        expect(decorated_distro_error_form.table_title("My Album")).to eq "My Album"
      end
    end

    describe "#by_store?" do
      let(:distro_error_form)           { Distribution::ErrorForm.new({}).save }
      let(:decorated_distro_error_form) { DistributionErrorFormDecorator.new(distro_error_form) }

      it "returns true when the filter type is store" do
        expect(decorated_distro_error_form.by_store?).to eq true
        expect(decorated_distro_error_form.by_error?).to eq false
      end
    end

    describe "#by_error?" do
      let(:distro_error_form)           { Distribution::ErrorForm.new(filter_type: "error").save }
      let(:decorated_distro_error_form) { DistributionErrorFormDecorator.new(distro_error_form) }

      it "returns true when the filter type is error" do
        expect(decorated_distro_error_form.by_error?).to eq true
        expect(decorated_distro_error_form.by_store?).to eq false
      end
    end

    describe "#by_release?" do
      let(:distro_error_form)           { Distribution::ErrorForm.new(filter_type: "release").save }
      let(:decorated_distro_error_form) { DistributionErrorFormDecorator.new(distro_error_form) }

      it "returns true when the filter type is release" do
        expect(decorated_distro_error_form.by_release?).to eq true
        expect(decorated_distro_error_form.by_store?).to eq false
      end
    end
  end
end
