require "rails_helper"

describe PaypalTransferDecorator do
  let(:paypal_transfer)    {build(:paypal_transfer)}
  let(:decorated_transfer) {PaypalTransferDecorator.new(paypal_transfer)}

  before(:each) do
    paypal_transfer.save(validate: false)
  end
  describe "#suspicious_class" do
    before(:each) do
      PersonBalance.where(person_id: paypal_transfer.person_id).first.update(balance: "200.00")
    end
    context "has previously approved check transfers and no previously approved paypal" do
      before do
        check = build(:check_transfer, person_id: paypal_transfer.person_id, transfer_status: "completed")
        allow(check).to receive(:verify_password).and_return(true)
        check.save
      end
      it "returns 'suspicious'" do
        expect(decorated_transfer.suspicious_class).to eq("suspicious")
      end
    end

    context "has previously approved EFT transfers and no previously approved paypal" do
      before do
        person    = paypal_transfer.person
        bank_acct = create(:stored_bank_account, person_id: person.id)
        eft       = EftBatchTransaction.new(stored_bank_account_id: bank_acct.id, amount: "100.00")
        allow(eft).to receive(:verify_password).and_return(true)
        eft.save
        eft.update(status: "success")
      end
      it "returns 'suspicious'" do
        expect(decorated_transfer.suspicious_class).to eq("suspicious")
      end
    end

    context "payee is blacklisted" do
      before do
        create(:blacklisted_paypal_payee, payee: decorated_transfer.paypal_address)
      end
      it "returns 'suspicious'" do
        expect(decorated_transfer.suspicious_class).to eq("suspicious")
      end
    end

    context "has previously approved check transfers and previously approved paypal" do
      before do
        check  = build(:check_transfer, person_id: paypal_transfer.person_id, transfer_status: "completed")
        allow(check).to receive(:verify_password).and_return(true)
        check.save
        paypal = build(:paypal_transfer, person_id: paypal_transfer.person_id, transfer_status: "completed")
        paypal.save(validate: false)
      end
      it "returns nil" do
        expect(decorated_transfer.suspicious_class).to be nil
      end
    end

    context "has neither perviosly approved check nor EFT transfers" do
      it "returns nil" do
        expect(decorated_transfer.suspicious_class).to be nil
      end
    end
  end
end
