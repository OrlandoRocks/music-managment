require "rails_helper"

describe CreditUsage do
  let!(:person) { create(:person) }
  let!(:no_credit_person) { create(:person) }
  let!(:plan_person) { create(:person) }

  describe "#for" do
    let!(:album) { create(:album, person: person) }
    let!(:freemium_album) { create(:album, :freemium_release, person: person) }
    let!(:product) { TCFactory.build_album_credit_product(5) }
    let!(:purchase) { create(:purchase, person_id: person.id, product_id: product.id, related: product, paid_at: Time.now()) }
    let!(:inventory) do
      create(:inventory,
             person: person,
             inventory_type: 'Album',
             product_item: product.product_items.first,
             purchase: purchase,
             quantity: 5)
    end

    it "should create credit usage when available" do
      credit = CreditUsage.for(person, album)

      expect(credit.valid?).to eq(true)
      expect(credit.new_record?).to eq(false)
      expect(credit.person_id).to eq(person.id)
      expect(credit.related_id).to eq(album.id)
    end

    it "should not call #plan_credit_for when user has no plan" do
      expect(CreditUsage).not_to receive(:plan_credit_for).with(person, album)
      credit = CreditUsage.for(person, album)
    end

    it "should call #plan_credit_for when user has a plan" do
      create(:person_plan, person: plan_person, plan: Plan.first)

      expect(CreditUsage).to receive(:plan_credit_for).with(plan_person, album)
      credit = CreditUsage.for(plan_person, album)
    end

    it "should create credit usage when user has a plan" do
      create(:person_plan, person: plan_person, plan: Plan.first)

      credit = CreditUsage.for(plan_person, album)
      expect(credit.valid?).to eq(true)
      expect(credit.new_record?).to eq(false)
      expect(credit.person_id).to eq(plan_person.id)
      expect(credit.related_id).to eq(album.id)
    end

    it "should create credit usage when user has a plan and distributes freemium product" do
      create(:person_plan, person: plan_person, plan: Plan.first)

      credit = CreditUsage.for(plan_person, freemium_album)
      expect(credit.valid?).to eq(true)
      expect(credit.new_record?).to eq(false)
      expect(credit.person_id).to eq(plan_person.id)
      expect(credit.related_id).to eq(freemium_album.id)
    end

    it "returns nil when it is a free release" do
      allow(Product).to receive(:free_release?).and_return true
      credit = CreditUsage.for(person, album)
      expect(credit).to eq(nil)
    end

    it "should return nil if no credits available" do
      credit = CreditUsage.for(no_credit_person, album)
      expect(credit.blank?).to eq(true)
    end

    it "should return nil if item already used" do
      allow(CreditUsage).to receive(:credit_available_for?).and_return(true)
      allow(Inventory).to receive(:used?).and_return(true)
      credit = CreditUsage.for(person, album)
      expect(credit.blank?).to eq(true)
    end

    it "should return existing credit usage if present and not finalized" do
      allow(CreditUsage).to receive(:credit_available_for?).and_return(true)
      credit = CreditUsage.for(person, album)
      credit2 = CreditUsage.for(person, album)

      expect(credit.id).to eq(credit2.id)

      credit.finalized_at = Time.now()
      credit.save!

      credit2 = CreditUsage.for(person, album)
      expect(credit2.blank?).to eq(true)
    end
  end

  describe "#credit_available_for?" do
    it "should determine if credits are available" do
      product = TCFactory.build_album_credit_product(5)
      purchase = TCFactory.create(:purchase, person_id: person.id, product_id: product.id, related: product, paid_at: Time.now())
      inventory = TCFactory.create(:inventory,
                                   person: person,
                                   inventory_type: 'Album',
                                   product_item: product.product_items.first,
                                   purchase: purchase,
                                   quantity: 5)

      expect(inventory.valid?).to eq(true)
      expect(CreditUsage.credit_available_for?(person, 'Album')).to eq(true)

      album = TCFactory.create(:album, person: person)
      usage = TCFactory.create(:credit_usage, person: person, related: album)

      expect(CreditUsage.credit_available_for?(person, 'Album')).to eq(true)

      inventory.quantity_used = 4
      inventory.save!

      expect(CreditUsage.credit_available_for?(person, 'Album')).to eq(false)
    end
  end

  describe "#number_credits_available" do
    it "should return number of available credits" do
      product = TCFactory.build_album_credit_product(5)
      purchase = TCFactory.create(:purchase, person_id: person.id, product_id: product.id, related: product, paid_at: Time.now())
      inventory = TCFactory.create(:inventory,
                                   person: person,
                                   inventory_type: 'Album',
                                   product_item: product.product_items.first,
                                   purchase: purchase,
                                   quantity: 5)

      expect(CreditUsage.number_credits_available(person, 'Album')).to eq(5)

      album = TCFactory.create(:album, person: person)
      usage = TCFactory.create(:credit_usage, person: person, related: album)

      expect(CreditUsage.number_credits_available(person, 'Album')).to eq(4)

      inventory.quantity_used = 4
      inventory.save!

      expect(CreditUsage.number_credits_available(person, 'Album')).to eq(0)
    end
  end

  describe "#creditable?" do
    it "should return if a class type is creditable" do
      expect(CreditUsage.creditable?('Album')).to eq(true)
      expect(CreditUsage.creditable?('Single')).to eq(true)
      expect(CreditUsage.creditable?('Ringtone')).to eq(true)
      expect(CreditUsage.creditable?('Salepoint')).to eq(false)
    end
  end

  describe "#iventory_use_options" do
    it "should return a hash of objects to use in Inventory.use!" do
      expect(CreditUsage).to receive(:credit_available_for?).and_return(true)
      album = TCFactory.create(:album, person: person)
      usage = TCFactory.create(:credit_usage, person: person, related: album)

      options = usage.inventory_use_options
      expect(options.size).to eq(1)
      expect(options.has_key?(:item_to_use)).to eq(true)
      expect(options[:item_to_use].id).to eq(album.id)
      expect(options[:item_to_use].class.name).to eq(album.class.name)
    end
  end

  describe "#remove_from_cart_pre_proc" do
    it "should call remove_from_cart_pre_proc on related" do
      allow(CreditUsage).to receive(:credit_available_for?).and_return(true)
      album = TCFactory.create(:album, person: person)
      credit = TCFactory.create(:credit_usage, person: person, related: album)
      expect(credit.related).to receive :remove_from_cart_pre_proc
      credit.remove_from_cart_pre_proc
    end
  end

  describe "#add_to_cart_pre_proc" do
    it "should call add_to_cart_pre_proc on related" do
      allow(CreditUsage).to receive(:credit_available_for?).and_return(true)
      album = TCFactory.create(:album, person: person)
      credit = TCFactory.create(:credit_usage, person: person, related: album)
      expect(credit.related).to receive :add_to_cart_pre_proc
      credit.add_to_cart_pre_proc
    end
  end

  describe "#validate_applies_to_type" do
    it "should enforce related class name and applies to type are the same" do
      album = TCFactory.build(:album, person: person)
      credit = TCFactory.build(:credit_usage, person: person, related: album, applies_to_type: album.class.name)

      credit.send(:validate_applies_to_type)
      expect(credit.errors[:applies_to_type].blank?).to eq(true)

      credit.applies_to_type = "Single"
      credit.send(:validate_applies_to_type)
      expect(credit.errors[:applies_to_type].blank?).to eq(false)
    end

    it "should use related class name instead of related type" do
      single = TCFactory.build(:single, person: person)
      credit = TCFactory.build(:credit_usage, person: person, related: single, applies_to_type: single.class.name)

      expect(credit.related_type == credit.applies_to_type).to eq(false)

      credit.send(:validate_applies_to_type)
      expect(credit.errors[:applies_to_type].blank?).to eq(true)
    end
  end

  describe "#credit_usage_for" do

    it "should return credit usage for given item" do
      allow(CreditUsage).to receive(:credit_available_for?).and_return(true)
      album = TCFactory.create(:album, person: person)
      credit = TCFactory.create(:credit_usage, person: person, related: album)

      usage = CreditUsage.credit_usage_for(person, album)
      expect(usage.blank?).to eq(false)
      expect(usage.id).to eq(credit.id)
    end

    it "should return nil if no credit usage is currently applied to item" do
      album = TCFactory.create(:album, person: person)

      usage = CreditUsage.credit_usage_for(person, album)
      expect(usage.blank?).to eq(true)
    end
  end

  describe "#finalized?" do
    before(:each) do
      @album = TCFactory.create(:album, person: person)
      allow(CreditUsage).to receive(:credit_available_for?).and_return(true)
      @credit = CreditUsage.for(person, @album)
    end

    it "should return true if credit usage is finalized" do
      expect(@credit.finalized?).to eq(false)
    end

    it "should return false if credit usage is not finalized" do
      @credit.finalized_at = Time.now()
      expect(@credit.finalized?).to eq(true)
    end
  end

  describe "#paid_post_proc" do
    it "should update the related payment_applied, steps_required and finalized_at" do
      expect(CreditUsage).to receive(:credit_available_for?).and_return(true)
      album = create(:album)
      credit = create(:credit_usage, person: album.person, related: album, applies_to_type: album.class.name)
      allow(album).to receive(:should_finalize?).and_return(true)

      expect(album.finalized_at).to eq(nil)
      expect(album.payment_applied).to eq(false)
      expect(album.steps_required).not_to eq(0)

      credit.paid_post_proc
      album = album.reload

      expect(album.finalized?).to eq true
      expect(album.payment_applied).to eq(true)
      expect(album.steps_required).to eq(0)
    end
  end
end
