require "rails_helper"

describe Composer do
  before(:each) do
    allow(FeatureFlipper).to receive(:show_feature?).and_return(false)
    allow_any_instance_of(PublishingAdministrationHelper).to receive(:rights_app_enabled?).and_return(false)
  end

  describe "selected_splits?" do
    before :each do
      @composer = create(:composer)
    end

    it "should return true when there are splits" do
      @splits = create(:publishing_split, composer: @composer)
      expect(@composer.selected_splits?).to be_truthy
    end

    it "should return false when there are no splits" do
      expect(@composer.selected_splits?).to be_falsey
    end
  end

  describe "create 'Unallocated Sum' composition" do
    before :each do
      @composer = create(:composer)
      @composer_with_publisher = create(:composer,
        publisher: create(:publisher,
        name: 'Composer with Publisher'),
        person_id: create(:person).id
      )
    end

    it "should create unallocated sum song after composer is created" do
      compositions = @composer.compositions.find_all{|c| c.is_unallocated_sum? }
      expect(compositions.count).to eq(1)
      expect(compositions.first.publishing_splits.first.percent).to eq(100)
    end

    it "should name the unallocated sum appropriately" do
      @composer.compositions.detect{|c| c.is_unallocated_sum? }.name == @composer.name
      @composer_with_publisher.compositions.detect{|c| c.is_unallocated_sum? }.name == @composer_with_publisher.publisher.name
    end
  end

  describe ".with_no_unallocated_sum_composition" do
    before :each do
      @composer = create(:composer)
      @composer2 = create(:composer, person_id: create(:person).id)
      create(:publishing_split, composer: @composer2)
    end

    it "should return composer with no unallocated_sum_composition" do
      unallocated_sum_composition = @composer2.compositions.detect { |c| c.is_unallocated_sum? }
      expect(unallocated_sum_composition).not_to be_nil
      unallocated_sum_composition.destroy
      expect(Composer.with_no_unallocated_sum_composition).to include(@composer2)
      expect(Composer.with_no_unallocated_sum_composition).not_to include(@composer)
    end
  end

  describe "#process_paid" do
    before :each do
      @person       = create(:person)
      @account      = create(:account, person: @person)
      @composer     = create(
        :composer,
        :active,
        :with_pub_admin_purchase,
        person:   @person,
        account:  @account,
        email:    Faker::Internet.email
      )
    end

    context "BMI reminder email" do
      it "should send BMI reminder email if not affiliated with PRO as songwriter" do
        @composer.update(performing_rights_organization_id: nil)
        expect(MailerWorker).to receive(:perform_async)
        @composer.process_paid
      end

      it "should not send BMI reminder email if affiliated with a PRO as songwriter" do
        expect(MailerWorker).not_to receive(:perform_async)
        @composer.process_paid
      end
    end

    context "has rights app" do
      before :each do
        allow(FeatureFlipper).to receive(:show_feature?) { true }
        allow_any_instance_of(PublishingAdministrationHelper).to receive(:rights_app_enabled?).and_return(true)
      end

      it "should invoke ArtistAccountWriterWorker if active composer's account does not have provider_account_id" do
        expect(PublishingAdministration::ArtistAccountWriterWorker)
          .to receive(:perform_async).with(@composer.id)
        @composer.process_paid
      end

      it "should invoke the WriterCreationWorker if active composer's account has provider_account_id" do
        @account.update(provider_account_id: "DMC123")
        expect(PublishingAdministration::WriterCreationWorker)
          .to receive(:perform_async).with(@composer.id)
        @composer.process_paid
      end

      it "should not invoke ArtistAccountWriterWorker or WriterCreationWorker if composer is not active" do
        inactive_composer = create(:composer, account: @account, person: @person)

        expect(PublishingAdministration::ArtistAccountWriterWorker)
          .not_to receive(:perform_async).with(inactive_composer.id)
        expect(PublishingAdministration::WriterCreationWorker)
          .not_to receive(:perform_async).with(inactive_composer.id)

        inactive_composer.process_paid
      end

      it "should invoke the CompositionCreationWorker regardless of composer status" do
        expect(PublishingAdministration::CompositionCreationWorker)
          .to receive(:perform_async).with(@composer.id)
        @composer.process_paid
      end
    end
  end

  describe "#send_bmi_registration_reminder" do
    it "should log it in the notes" do
      composer = create(:composer)
      composer.send(:send_bmi_registration_reminder)
      expect(Note.find_by(subject: "Publishing Email").note)
        .to match(/#{composer.name}.*#{composer.publishing_administrator.email}/)
    end
  end

  describe "::missing_splits?" do
    before(:each) do
      @account      = create(:account)
      @composer     = create(:composer, account: @account)
      @album        = create(:album, person: @composer.person)
      @composition  = create(:composition)
      @song         = create(:song, album: @album)
      @purchase     = create(:purchase,
                        person: @composer.publishing_administrator,
                        related_id: @composer.id,
                        related_type: "Composer")
    end

    context "user has composition" do
      before(:each) do
        @song.update(composition: @composition)
      end

      it "returns true for a user that has a composition with a new state for any his songs" do
        @composition.update_attribute(:state, 'new')
        expect(Composer.missing_splits?(@composer.person)).to be true
      end

      it "returns true for a user that has a composition with a nil state for any his songs" do
        expect(Composer.missing_splits?(@composer.person)).to be true
      end

      it "returns false for a user that has a composition of different state" do
        @composition.update_attribute(:state, 'pending_distribution')
        expect(Composer.missing_splits?(@composer.person)).to be false
      end
    end

    it "returns false for a user without a composition for any of his songs" do
      expect(Composer.missing_splits?(@composer.person)).to eq true
    end
  end

  describe "agreed_to_terms validation" do
    context "when rights app is enabled" do
      it "returns true and bypasses validation" do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
        allow_any_instance_of(PublishingAdministrationHelper).to receive(:rights_app_enabled?).and_return(true)
        composer = build(:composer, agreed_to_terms: "")
        expect(composer.valid?).to be_truthy
      end
    end
  end

  def sample_esignature_response
    {'status' => 'sent', 'guid' => 'ABCDEFG1234567'}
  end

  describe "Publishing Administration Status" do
    before :each do
      @active_composer     = create(:composer,
                                    account: nil,
                                    performing_rights_organization_id: 1,
                                    cae: "123456789")
      @terminated_composer = create(:composer,
                                    :terminated,
                                    account: nil,
                                    performing_rights_organization_id: 1,
                                    cae: "123456789")
    end

    describe "is_active?" do
      before :each do
        create(
          :purchase,
          related_type: "Composer",
          related_id: @active_composer.id,
          product: Product.find(65),
          person_id: @active_composer.person_id
        )
      end

      xit "returns true unless a composer is terminated" do
        expect(@active_composer.is_active?).to be(true)
        expect(@terminated_composer.is_active?).to be(false)
      end
    end

    describe "#terminate_pub_admin" do
      it "records the date on which the composer's publishing admin was terminated" do
        @active_composer.terminate_pub_admin
        expect(@active_composer.terminated_at).not_to be(nil)
      end
    end

    describe "reactivate_pub_admin" do
      before :each do
        create(
          :purchase,
          related_type: "Composer",
          related_id: @terminated_composer.id,
          product: Product.find(65),
          person_id: @terminated_composer.person_id
        )
      end
      xit "deletes the termination date" do
        @terminated_composer.reactivate_pub_admin
        expect(@terminated_composer.terminated_at).to be(nil)
        expect(@terminated_composer.is_active?).to be(true)
      end
    end
  end

  describe "terminated composers" do
    context "fully terminated composers" do
      let(:composer) { build_stubbed(:composer, :terminated) }
      let!(:terminated_composer) { build_stubbed(:terminated_composer, :fully, composer: composer) }

      describe ".does_not_have_fully_terminated_composers" do
        it "checks if the composer is fully terminated" do
          expect(composer.does_not_have_fully_terminated_composers).to eq(["cannot edit terminated composers"])
        end
      end

      describe ".has_fully_terminated_composers?" do
        it "returns true for fully terminated composers" do
          expect(composer.has_fully_terminated_composers?).to eq(true)
        end
      end
    end

    context "partially terminated composers" do
      before :each do
        @composer = create(:composer)
        @terminated_composer_composer = create(:terminated_composer, :partially, composer: @composer)
      end

      describe ".does_not_have_fully_terminated_composers" do
        it "returns nil if the composer is partially terminated" do
          expect(@composer.does_not_have_fully_terminated_composers).to eq(nil)
        end
      end

      describe ".has_fully_terminated_composers?" do
        it "returns false for partially terminated composers" do
          expect(@composer.has_fully_terminated_composers?).to eq(false)
        end
      end
    end
  end

  describe "#assign_publishing_role" do
    it "sets the publishing role after creating a composer" do
      composer = build(:composer)
      expect(composer.publishing_role).to be_nil
      composer.save
      expect(composer.publishing_role).not_to be_nil
    end
  end

  describe "#letter_of_direction" do
    it "should return the letter of direction document of the composer" do
      composer = create(:composer)
      lod = create(:legal_document, name: DocumentTemplate::LOD, subject_id: composer.id, subject_type: 'composer')
      expect(composer.letter_of_direction).to eq(lod)
    end

    it "should return nil if there is no letter of direction" do
      composer = create(:composer)
      expect(composer.letter_of_direction).to be_nil
    end
  end

  describe "valid_letter_of_direction" do
    it "should return true if letter of direction is not at all needed for the composer" do
      composer = build_stubbed(:composer)
      expect(composer).to receive(:letter_of_direction).and_return(nil)
      expect(composer.valid_letter_of_direction?).to be_truthy
    end

    it "should return true if the letter of direction is present with valid signed_at date" do
      composer = build_stubbed(:composer)
      lod = create(:legal_document, signed_at: Date.today())
      expect(composer).to receive(:letter_of_direction).and_return(lod)
      expect(composer.valid_letter_of_direction?).to be_truthy

    end

    it "should return false if the letter of direction is not yet signed" do
      composer = build_stubbed(:composer)
      lod = create(:legal_document, signed_at: nil)
      expect(composer).to receive(:letter_of_direction).and_return(lod)
      expect(composer.valid_letter_of_direction?).to be_falsey
    end
  end

  describe "#create_or_update_publisher" do
    context "when a composer does NOT have a publisher" do
      it "should update the publisher" do
        composer = create(:composer)

        publisher_params = {
          name: "Pub Name",
          cae: "123456789",
          performing_rights_organization_id: 56
        }

        expect(composer.publisher).to be_nil
        composer.create_or_update_publisher(publisher_params)

        publisher = composer.reload.publisher
        expect(publisher.name).to eq publisher_params[:name]
        expect(publisher.cae).to eq publisher_params[:cae]
      end
    end

    context "when a composer has a publisher" do
      it "should create a new publisher" do
        composer = create(:composer, :with_publisher)

        publisher_params = {
          name: "Pub Name",
          cae: "123456789",
          performing_rights_organization_id: 56
        }

        composer.create_or_update_publisher(publisher_params)

        publisher = composer.reload.publisher
        expect(publisher.name).to eq publisher_params[:name]
        expect(publisher.cae).to eq publisher_params[:cae]
      end
    end
  end

  describe 'four byte char validation' do
    let(:composer) { build(:composer, first_name: "💿💿💿💿💿", last_name: "💿💿💿💿💿") }

    it 'validates first_name and last_name for four byte chars' do
      composer.valid?
      expect(composer.errors.messages[:first_name].size).to be 1
      expect(composer.errors.messages[:last_name].size).to be 1
    end
  end
end
