require 'rails_helper'

describe TwoFactorAuthPrompt do
  let(:person) { create(:person) }

  describe "#active?" do
    context "is active" do
      it "returns true" do
        prompt = create(:two_factor_auth_prompt, person: person)

        expect(prompt.active?).to be_truthy
      end
    end

    context "is inactive" do
      it "returns false" do
        person.create_two_factor_auth(activated_at: Time.now)
        prompt = create(:two_factor_auth_prompt, person: person, prompt_at: 15.days.from_now)

        expect(prompt.active?).to be_falsey
      end
    end
  end

  describe "#dismiss" do
    it "dismisses the prompt" do
      prompt = create(:two_factor_auth_prompt, person: person)

      expect{prompt.dismiss}.to change{ prompt.dismissed_count}.by(1)
    end
  end
end
