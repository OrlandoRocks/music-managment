require 'rails_helper'

describe Account do
  describe "#catalogue_code" do
    context "account manages more than one publishing_composer with more than one unique PRO" do
      it "returns the default catalogue code (BMI)" do
        default_code  = RA_CATALOGUE_CODES[:BMI]
        account       = create(:account)
        pro           = create(:performing_rights_organization, name: "ASCAP")
        publishing_composer1     = create(:publishing_composer, account: account, performing_rights_organization: pro, cae: "123456789")
        publishing_composer2     = create(:publishing_composer, :active, account: account)

        expect(account.catalogue_code).to eq default_code
      end
    end

    context "account manages one publishing_composer" do
      context "publishing_composer's PRO is ASCAP" do
        it "returns the ASCAP catalogue code" do
          ASCAP_code  = RA_CATALOGUE_CODES[:ASCAP]
          pro         = create(:performing_rights_organization, name: "ASCAP")
          publishing_composer    = create(:publishing_composer, performing_rights_organization: pro, cae: "123456789")
          account     = publishing_composer.account

          expect(account.catalogue_code).to eq ASCAP_code
        end
      end

      context "publishing_composer's PRO is SESAC or SESAC, Inc" do
        it "returns the SESAC catalogue code" do
          SESAC_code  = RA_CATALOGUE_CODES[:SESAC]
          pro         = create(:performing_rights_organization, name: "SESAC, Inc.")
          publishing_composer    = create(:publishing_composer, performing_rights_organization: pro, cae: "123456789")
          account     = publishing_composer.account

          expect(account.catalogue_code).to eq SESAC_code
        end
      end

      context "publishing_composer's PRO is BMI" do
        it "returns the BMI catalogue code" do
          BMI_code    = RA_CATALOGUE_CODES[:BMI]
          pro         = create(:performing_rights_organization, name: "BMI")
          publishing_composer    = create(:publishing_composer, performing_rights_organization: pro, cae: "123456789")
          account     = publishing_composer.account

          expect(account.catalogue_code).to eq BMI_code
        end
      end

      context "publishing_composer's PRO is nil OR not included in CATALOGUE_CODES" do
        it "returns the default catalogue code (BMI)" do
          default_code          = RA_CATALOGUE_CODES[:BMI]
          publishing_composer_without_pro  = create(:publishing_composer)
          publishing_composer_with_acam    = create(:publishing_composer, :active)

          expect(publishing_composer_without_pro.account.catalogue_code).to eq default_code
          expect(publishing_composer_with_acam.account.catalogue_code).to eq default_code
        end
      end
    end
  end

  describe "#import_from_rights_app" do
    context "when the account does not have any paid publishing_composers" do
      it "should not import data from rights app" do
        account = create(:account)

        expect(PublishingAdministration::PublishingWriterImporterWorker).not_to receive(:perform_async)

        account.import_from_rights_app
      end
    end

    context "when the account has paid for non-legacy publishing_composers" do
      it "imports data from rights app" do
        account = create(:account)
        create(:publishing_composer, account: account)
        paid_publishing_composer = create(:publishing_composer, :with_pub_admin_purchase, account: account)

        expect(PublishingAdministration::PublishingWriterImporterWorker).to receive(:perform_async).with(paid_publishing_composer.id)
        expect(PublishingAdministration::PublishingWorkReconciliationWorker).to receive(:perform_async).with(account.id, true)

        account.import_from_rights_app
      end
    end
  end
end
