require "rails_helper"

describe DistributionSalepoint do
  let(:album)        { FactoryBot.create(:album) }
  let(:petri_bundle) { FactoryBot.create(:petri_bundle, album: album) }
  let(:distro)       do
    distribution = create(:distribution,
                          petri_bundle: petri_bundle,
                          converter_class: "MusicStores::SevenDigital::Converter",
                          delivery_type: nil)

    distribution.salepoints << create(:salepoint, salepointable: album, store: Store.find_by_short_name("7Digital"))

    distribution
  end

  it "should prevent duplicate distributions salepoints" do
    salepoint = distro.salepoints.first

    distro.salepoints << salepoint rescue nil

    expect(distro.valid?).to eq(false)
    expect(distro.errors.messages).to include(:distributions_salepoints)
  end
end
