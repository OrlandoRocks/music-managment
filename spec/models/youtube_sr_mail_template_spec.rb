require "rails_helper"

describe YoutubeSrMailTemplate do
  let(:user) { FactoryBot.create(:person, email: "testuser1@test.com") }
  let(:another_user) { FactoryBot.create(:another_person) }
  let(:album) { FactoryBot.create(:album, :with_uploaded_songs, person: user) }
  let(:another_album) { FactoryBot.create(:album, :with_uploaded_songs, person: another_user) }

  describe '.types_for_select' do
    it "returns an array of available templates for select dropdown" do
      expected_result =
        [
          ["Removed from Monetization",                   :removed_monetization],
          ["Rights to Instrumental",                      :instrumental_rights],
          ["Disputing a Claim on Your YouTube Channel",   :disputing_a_claim],
          ["YouTube Ownership Conflict",                  :ownership_conflict],
          ["Authorization for Use in Video",              :authorization],
          ["Appealed Claim",                              :disputed_claim],
          ["Creative Commons",                            :creative_commons_content],
          ["Art Track",                                   :art_track]
        ]

      expect(YoutubeSrMailTemplate.types_for_select).to eq(expected_result)
    end

    it "returns the 'remove monetization' template record" do
      template_name = 'removed_monetization'
      expected_template = YoutubeSrMailTemplate.where(template_name: template_name).first
      expect(YoutubeSrMailTemplate.return_template(template_name)).to eq(expected_template)
    end

    it "returns the 'authorization' template record" do
      template_name = 'authorization'
      expected_template = YoutubeSrMailTemplate.where(template_name: template_name).first
      expect(YoutubeSrMailTemplate.return_template(template_name)).to eq(expected_template)
    end
  end

  describe "#evaluate_attribute" do
    let(:youtube_sr_template) { YoutubeSrMailTemplate.return_template('removed_monetization') }
    let(:songs) { [ build_stubbed(:song), build_stubbed(:song) ] }
    let(:data) { [
        {
            song_id: songs[0].id,
            custom_fields: {
                third_party: "Scott",
            },
            song: songs[0]
        },
        {
            song_id: songs[1].id,
            custom_fields: {
                third_party: "Troy",
            },
            song: songs[1]
        }
    ] }

    it "returns an evaluated body based on passed in params" do
      expect(youtube_sr_template.evaluate_attribute(:subject, data)).to eq("Multiple ISRCs: Removed from Monetization")
    end

    it "evaluates for the header" do
      expect(youtube_sr_template.evaluate_attribute(:header, data)).to eq("<p>Hello #{songs.first.person_name},</p><p>We have been contacted by third parties stating that they have the rights to your content:</p>")
    end

    it "returns empty when given a bogus attribute" do
      expect(youtube_sr_template.evaluate_attribute(:fake_body, data)).to be_empty
    end
  end
end
