require "rails_helper"

describe MumaSong do

  describe "self.insert_itunes_sales_by_year_for_new_muma_songs" do

    before(:each) do 
      person = TCFactory.create(:person)
      muma_song = TCFactory.create(:muma_song)
      srm = TCFactory.create(:sales_record_master)
      sr  = TCFactory.create(:sales_record, :person_id=>person.id, :sales_record_master=>srm, :related_id=>muma_song.tc_song_and_album.id, :related_type=>"Song")
    end

    it "insert new muma_songs into sales_by_year table" do
      result = ActiveRecord::Base.connection.execute("select count(*) from sales_by_year")
      result = result.first[0] if result

      expect(result).not_to be_blank
      expect(result.to_i).to eq(0)

      MumaSong.insert_itunes_sales_by_year_for_new_muma_songs
      
      result = ActiveRecord::Base.connection.execute("select count(*) from sales_by_year")
      result = result.first[0] if result

      expect(result).not_to be_blank
      expect(result.to_i).to eq(1)
    end

  end

end
