require "rails_helper"

describe AlbumCountry do
  let(:country) { Country.unscoped.find(60) }
  let(:album_country) { build(:album_country, country: country) }
  context "#valid?" do
    it "allows 'exclude' as relation_type" do
      album_country.relation_type = "exclude"
      expect(album_country).to be_valid
    end

    it "allows 'include' as relation_type" do
      album_country.relation_type = "include"
      expect(album_country).to be_valid
    end

    it "doesn't allow other values as relation_type" do
      album_country.relation_type = "notcorrect"
      expect(album_country).not_to be_valid
    end

    it "doesn't allow sanctioned countries with an 'include' relation_type" do
      country = Country.unscoped.sanctioned.first
      album = create(:album)
      album.album_countries.create(country_id: country.id, relation_type: "include")
      expect(album.album_countries.last.errors.messages[:country]).not_to be_empty
    end
  end
end
