require 'rails_helper'

RSpec.describe SelfBillingStatus, type: :model do
  let(:person) { FactoryBot.create(:person, :with_tc_social, country: 'Austria') }

  describe 'Associations' do
    it { should belong_to(:person) }
  end

  describe 'Validations' do
    it 'is not valid without status' do
      vat = SelfBillingStatus.create(person: person)
      expect(vat).to_not be_valid
    end
  end
end
