require 'rails_helper'

describe PayinProviderConfig, type: :model do

  let(:password) { "passwordio" }
  let(:private_key) { "pkey_pcool" }
  let(:signature) { "signature.and.some123.numbers.12312" }
  let(:ppc) { create(:payin_provider_config, password: password, private_key: private_key, signature: signature) }

  it 'should encrypt the password on the cipher_datum object' do
    expect(ppc.decrypted_password).to eq(password)
  end

  it 'should encrypt the private_key on the cipher_datum object' do
    expect(ppc.decrypted_private_key).to eq(private_key)
  end

  it 'should encrypt the signature on the cipher_datum object' do
    expect(ppc.decrypted_signature).to eq(signature)
  end

  it 'should add a cipher datum for the secrets decryption' do
    expect do
      create(:payin_provider_config, password: password, private_key: private_key, signature: signature)
    end.to change {
      CipherDatum.count
    }.by(1)
  end
end
