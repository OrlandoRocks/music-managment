require "rails_helper"

include Tunecore::FriendReferral

describe FriendReferral do
  before(:each) do
    @referee = TCFactory.build_person(email: 'referee@tunecore.com')
    product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
    @invoice = TCFactory.build_invoice
    @purchase = TCFactory.create(:purchase, product: product, person: @referee, related: product, invoice: @invoice)
    @referrer = TCFactory.build_person(email: 'referrer@tunecore.com')
    @transaction = TCFactory.build_friend_referral(commission: 25, referrer: @referrer, referee: @referee, invoice: @invoice, status: 'purchase')
    @kickback = double('Kickback', referrer_commission: 5, referrer_balance: 10, referrer_email: @referrer.email, referrer_id: @referrer.id, referrer_unique_referrals: 2)
    @campaign_hash = { campaign_id: 411, campaign_code: 'dt' }
    @campaign = TCFactory.build_friend_referral_campaign(person: @referrer, campaign_code: 'dt')
  end

  describe ".record_event" do
    it "should save new event" do
      expect(Kickback).to receive(:record_event).and_return(@kickback)
      expect { FriendReferral.record_event(@invoice, @referee, @campaign_hash) }.to change { FriendReferral.count }.by(1)
    end

    context "when referrer is invalid" do
      it "should mark status as error" do
        allow(@kickback).to receive_messages(referrer_id: 9999)
        expect(Kickback).to receive(:record_event).and_return(@kickback)
        kt = FriendReferral.record_event(@invoice, @referee, @campaign_hash)
        expect(kt.status).to eq('error')
      end
    end

    context "when purchasing non-qualifying product" do
      it "should log message when campaign_code is not in the system" do
        expect(FriendReferral).to receive('is_qualifying_purchase?').and_return(false)

        @campaign_hash[:campaign_code] = 'wf'
        fr = FriendReferral.record_event(@invoice, @referee, @campaign_hash)
        expect(fr.error_message).to match(/campaign_code.*not found/)
        expect(fr.status).to eq('invalid')
      end
    end
  end

  describe "#post_commission!" do
    before :each do
      @transaction.update_attribute(:status, 'approved')
    end

    it "should post balance adjustment to referrer" do
      @transaction.post_commission!
      expect(@referrer.person_transactions.first.credit).to eq(25)
      expect(@referrer.person_transactions.first.target).to be_kind_of(FriendReferral)
    end

    it "should log failure" do
      allow(@transaction).to receive(:post_adjustment).and_return([nil, 'Failed to post adjustment'])
      @transaction.post_commission!
      expect(@transaction.status).to eq('posting_failed')
      expect(@transaction.error_message).to eq('Failed to post adjustment')
    end

    it "should log success" do
      @transaction.post_commission!
      expect(@transaction.posting).to be_kind_of(PersonTransaction)
      expect(@transaction.status).to eq('posted')
      expect(@transaction.error_message).to be_nil
    end
  end
end
