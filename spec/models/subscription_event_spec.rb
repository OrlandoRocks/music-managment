require "rails_helper"
include ActiveSupport::Testing::TimeHelpers

describe SubscriptionEvent do
  let(:person) { create(:person) }

  describe "#as_json" do
    it "has an additional attribute :price" do
      social_purchase = create(:subscription_purchase, :social_us, person: person)
      status   = create(:person_subscription_status, subscription_type: "Social", person: person)
      sub_event = create(:subscription_event, :social, person_subscription_status: status, subscription_purchase: social_purchase)

      expect(sub_event.as_json(root: false)["price"].present?).to eq true
    end
  end

  describe "::create_purchase_event" do
    around(:each) do |example|
      Timecop.freeze do
        @sub_purchase = create(:subscription_purchase, :social_us,
          person:           person,
          termination_date: Time.now + 6.months
        )

        example.run
      end
    end

    context "when a person_subscription_status does not exist for the user" do
      before do
        travel_to Time.local(2020)
      end

      after do
        travel_back
      end

      it "should create the person_subscription_status record and set the effective_date, termination_date, person and subscription_type" do
        expect {
          SubscriptionEvent.create_purchase_event(@sub_purchase)
        }.to change(PersonSubscriptionStatus, :count).by(1)

        p = PersonSubscriptionStatus.last
        expect(p.effective_date.to_s).to eq(Time.now.to_s)
        expect(p.termination_date.to_s).to eq((Time.now + 1.month).to_s)
        expect(p.subscription_type).to eq("Social")
        expect(p.person).to eq(@sub_purchase.person)
      end

      it "creates a SubscriptionEvent record with the correct event_type, subscription_purchase and subscription_status" do
        expect {
          SubscriptionEvent.create_purchase_event(@sub_purchase)
        }.to change(SubscriptionEvent, :count).by(1)

        d = SubscriptionEvent.last
        expect(d.event_type).to eq("Purchase")
        expect(d.subscription_purchase).to eq(@sub_purchase)
        expect(d.person_subscription_status).to eq(PersonSubscriptionStatus.last)
      end

      it "updates the SubscriptionPurchase record" do
        format = "%m/%d/%y %I:%M:%S"
        SubscriptionEvent.create_purchase_event(@sub_purchase)
        expect(@sub_purchase.reload.effective_date.strftime(format)).to eq(Time.now.strftime(format))
        expect(@sub_purchase.reload.termination_date.strftime(format)).to eq((Time.now + 1.month).strftime(format))
      end

      it "sets subscription_type" do
        SubscriptionEvent.create_purchase_event(@sub_purchase)
        expect(SubscriptionEvent.last.subscription_type).to eq("Social")
      end
    end

    context "when a person_subscription_status already exists for the user" do
      before(:each) do
        @person_subscription_status = create(:person_subscription_status,
          subscription_type: "Social",
          person: @sub_purchase.person,
          effective_date: Time.now,
          termination_date: Time.now,
          canceled_at: Time.now
        )
      end

      it "sets the termination_date based on next_purchase_effective_start_date for the related person_subscription_status" do
        expect_any_instance_of(PersonSubscriptionStatus).to receive(:next_purchase_effective_start_date).and_return(Time.now + 5.days)
        SubscriptionEvent.create_purchase_event(@sub_purchase)
        expect(@person_subscription_status.reload.termination_date.to_s).to eq((Time.now + 5.days + 1.month).to_s)
      end

      it "blanks out the canceled_at date" do
        SubscriptionEvent.create_purchase_event(@sub_purchase)
        expect(@person_subscription_status.reload.canceled_at.nil?).to be true
      end

      it "updates the subscription_purchase effective_date and termination date based on the person_subscription_status" do
        expect_any_instance_of(PersonSubscriptionStatus).to receive(:next_purchase_effective_start_date).and_return(Time.now + 5.days)
        SubscriptionEvent.create_purchase_event(@sub_purchase)
        expect(@sub_purchase.reload.effective_date.to_s).to eq((Time.now + 5.days).to_s)
        expect(@sub_purchase.reload.termination_date).to eq(@person_subscription_status.reload.termination_date)
      end

      it "creates a SubscriptionEvent record with the event_type based on the subscription_status" do
        expect_any_instance_of(PersonSubscriptionStatus).to receive(:next_purchase_event_type).and_return("Fake event type")

        expect {
          SubscriptionEvent.create_purchase_event(@sub_purchase)
        }.to change(SubscriptionEvent, :count).by(1)

        d = SubscriptionEvent.last
        expect(d.event_type).to eq("Fake event type")
        expect(d.subscription_purchase).to eq(@sub_purchase)
        expect(d.person_subscription_status).to eq(@person_subscription_status.reload)
      end
    end

    context "when a subscription_purchase does not expire" do
      it "creates a person_subscription_status with a nil termination_date" do
        fb_sub_purchase = create(:subscription_purchase, :fb_tracks)

        SubscriptionEvent.create_purchase_event(fb_sub_purchase.reload)
        pss = PersonSubscriptionStatus.last

        expect(pss.termination_date.nil?).to be true
        expect(pss.expires?).to be false
      end
    end
  end
end
