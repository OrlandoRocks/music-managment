require "rails_helper"
require_relative "blank_artist_shared"
require_relative "petri_interface_shared"

describe Ringtone, "implementing an interface to PETRI" do
  before(:each) do
    @distro = Ringtone.new
  end

  it_should_behave_like "PETRI interface"
end

describe Ringtone do
  before(:each) do
    @ringtone = build(:ringtone)
  end

  it "is_a? looks through class hierarchy" do
    expect(@ringtone.is_a?(Album)).to eq(true)
  end

  it "instance_of? does not look through heirarchy" do
    expect(@ringtone.instance_of?(Album)).to eq(false)
  end

  it "Should have the type ringtone" do
    expect(@ringtone.album_type).to eq("Ringtone")
  end

  it "Should be found by album" do
    #
    # Be sure to set the inheritance_column for
    # ActiveRecord's STI inheritance on both the
    # Album and the Single model
    #
    @ringtone.save
    assert ringtone = Album.find(@ringtone.id)
    assert ringtone.is_a?(Ringtone)
  end

  it "should not set a golive_date or timed_release_timing_scenario, and be valid" do
    ringtone = FactoryBot.create(:ringtone)
    expect(ringtone.golive_date).to eq(nil)
    expect(ringtone.timed_release_timing_scenario).to eq(nil)
    expect(ringtone.valid?).to eq(true)
  end
end

describe Ringtone do
  before(:each) do
    @ringtone = build(:ringtone)
  end

  it "should be valid from factory" do
    expect(@ringtone.valid?).to eq(true)
  end

  it "should only allow one song" do
    2.times do
      @ringtone.songs << build(:song)
    end

    expect(@ringtone.valid?).to eq(false)
  end

  it "must have one song to be valid" do
    @ringtone.songs.clear
    expect(@ringtone.valid?).to eq(false)
  end

  it "should take an isrc for its song" do
    @ringtone.respond_to?(:optional_isrc=)
  end

  it "should take a value for explicit lyrics/parental advisory for its song" do
    @ringtone.respond_to?(:parental_advisory=)
  end
end

describe Ringtone, "with artists" do
  it 'should not titleize the artist' do
    ringtone = create(:ringtone, allow_different_format: true, name: "My Cool Album")
    creative_name = "OMG michelle"
    ringtone.creatives << Creative.new(name: creative_name, role: "primary_artist")
    ringtone.save

    creative = ringtone.creatives.includes(:artist).find_by(artists: { name: creative_name })

    expect(creative).not_to be_nil
    expect(creative.name).to eq creative_name
  end
end

describe Ringtone, "when it has duplicate featuring artists" do
  before(:each) do
    @product = create(:ringtone,
                      allow_different_format: false,
                      creatives: [{ "name" => "Philip J. Fry", "role" => "primary_artist" }, { "name" => "Turanga Leela", "role" => "featuring" }, { "name" => "Turanga Leela", "role" => "featuring" }],
                      name: 'Orphan of the Stars')
    @song = @product.song
  end

  it "should not create the song with duplicate artists" do
    expect(@product.reload.creatives.size).to eq(2)
    expect(@product.creatives[1].name).to eq("Turanga Leela")
    expect(@product.creatives[2]).to eq(nil)
    expect(@product.name).to eq('Orphan of the Stars')
  end

  it "should not add duplicate artists to a song" do
    @song.add_featured_artist('Turanga Leela')
    @song.save
    expect(@product.reload.creatives.size).to eq(2)
    expect(@product.creatives[1].name).to eq("Turanga Leela")
    expect(@product.creatives[2]).to eq(nil)
    expect(@product.name).to eq('Orphan of the Stars')
  end
end

describe Ringtone do
  before(:each) do
    @product = create(:ringtone,
                      allow_different_format: false,
                      name: 'My Cool Ringtone')
  end

  it_should_behave_like "testing blank artists"
end

describe Ringtone do
  before(:each) do
    @product = create(:ringtone,
                      allow_different_format: false,
                      name: 'My Cool Ringtone')
    @product.creatives << Creative.new(name: "Robin", role: "primary_artist")
    @product.creatives << Creative.new(name: "Bob", role: "featuring")
    @product.save!
  end

  it_should_behave_like "testing updating with blank artists"
end

describe Ringtone do
  describe "ensure_takedown_genre" do
    before(:each) do
      @pop    = Genre.where("name = 'Pop'").first
      @folk   = Genre.where("name = 'Folk'").first
      @alt    = Genre.where("name = 'Alternative'").first

      @ringtone = create(:ringtone)
    end

    it "should update the genre to an available genre" do
      @ringtone.primary_genre       = @folk
      @ringtone.secondary_genre     = @folk
      @ringtone.save

      @ringtone.ensure_takedown_genre

      @ringtone.reload
      expect(@ringtone.primary_genre_id).to eq(@pop.id)
      expect(@ringtone.secondary_genre_id).to eq(@pop.id)
    end

    it "should not change genre if not needed" do
      @ringtone.primary_genre_id    = @alt.id
      @ringtone.secondary_genre_id  = @alt.id
      @ringtone.save

      @ringtone.ensure_takedown_genre

      @ringtone.reload
      expect(@ringtone.primary_genre_id).to eq(@alt.id)
      expect(@ringtone.secondary_genre_id).to eq(@alt.id)
    end
  end
end

describe Ringtone do
  describe "#paid_post_proc" do
    before(:each) do
      @ringtone = create(:ringtone, :with_artwork, created_with: nil)
      store     = Store.find_by(abbrev: "ww")
      create(:salepointable_store, store: store)
      salepoint             = create(:salepoint, salepointable: @ringtone, store: store, variable_price_id: 39)
      @ringtone.salepoints  = [salepoint]
      upload                = create(:upload)
      @ringtone.song.upload = upload
    end

    it "should set the finalized_at" do
      expect(@ringtone.finalized?).to eq false
      @ringtone.paid_post_proc
      expect(@ringtone.reload.finalized?).to eq true
    end

    it "should set the payment_applied to true" do
      expect(@ringtone.payment_applied).to eq(false)
      @ringtone.paid_post_proc
      expect(@ringtone.reload.payment_applied).to eq(true)
    end
  end

  describe "#creatives=" do
    it "creates both primary and featured artists" do
      ringtone = create(:ringtone)

      ringtone.creatives = [
        { id: nil, role: "primary_artist", name: "Charlie" },
        { id: nil, role: "featuring", name: "Frank" }
      ].map(&:with_indifferent_access)

      ringtone.save

      ringtone.creatives = [
        { id: nil, role: "primary_artist", name: "Charlie" },
        { id: nil, role: "featuring", name: "Pearl" }
      ].map(&:with_indifferent_access)

      ringtone.save

      ringtone.reload

      expect(ringtone.creatives.size).to eq(2)
    end
  end
end
