require "rails_helper"

describe YoutubeMonetization do
  before(:each) do
    @person = FactoryBot.create(:person, email: "g@gmail.com")
    @other_person = FactoryBot.create(:person, email: "1234@internet.com")

    @album1 = FactoryBot.create(:album, :approved, person: @person)
    @album2 = FactoryBot.create(:album, :approved, person: @person, name: "The Gilded Palace of Sin")
    @album3 = FactoryBot.create(:album, :approved, person: @person, name: "Roadblock to Heck")
    @others_album = FactoryBot.create(:album, :approved, person: @other_person)

    @song1 = FactoryBot.create(:song, album: @album1)
    @song2 = FactoryBot.create(:song, album: @album2)
    @song3 = create(:song, album: @album3)
    @others_song = FactoryBot.create(:song, album: @others_album)

    @yt_store = Store.find_by(abbrev: "ytsr")

    @salepointable_store = FactoryBot.create(:salepointable_store, store: @yt_store)

    @salepoint1 = FactoryBot.create(:salepoint, salepointable: @album1, store: @yt_store, status: "complete")
    @salepoint2 = FactoryBot.create(:salepoint, salepointable: @album2, store: @yt_store, status: "complete")
    @others_salepoint = FactoryBot.create(:salepoint, salepointable: @others_album, store: @yt_store)

    @youtube_monetization = FactoryBot.create(:youtube_monetization, person: @person)
    @ytm_tracks = YtmTracks.new(@person)
    @others_youtube_monetization = FactoryBot.create(:youtube_monetization, person: @other_person)

    @ytm_ineligible_song1 = FactoryBot.create(:ytm_ineligible_song, song: @song1)
    @ytm_blocked_song3 = create(:ytm_blocked_song, song: @song3)

    @artist1 = FactoryBot.create(:artist)
    @artist2 = FactoryBot.create(:artist, name: "The Flying Burrito Brothers")
    @other_artist = FactoryBot.create(:artist, name: "Somebody Else!!!!!")

    @creative1 = FactoryBot.create(:creative, artist: @artist1, creativeable: @album1)
    @creative2 = FactoryBot.create(:creative, artist: @artist2, creativeable: @album2)
    @others_creative = FactoryBot.create(:creative, artist: @other_artist, creativeable: @others_album)

    @salepoint_song1 = FactoryBot.create(:salepoint_song, salepoint: @salepoint1, song: @song1)
    @salepoint_song2 = FactoryBot.create(:salepoint_song, salepoint: @salepoint2, song: @song2)
    @others_salepoint_song = FactoryBot.create(:salepoint_song, salepoint: @others_salepoint, song: @others_song)
  end

  it "knows its own product name" do
    expect(@youtube_monetization.name).to eq "YouTube Sound Recording Revenue"
  end

  describe "#mark_as_ineligible" do
    it "should not permit marking of songs the user does not own" do
      expect { @youtube_monetization.mark_as_ineligible([@others_song.id]) }.to raise_error(RuntimeError, "You cannot mark songs you don't own")
    end

    it "should return the ids of songs successfully and unsuccessfully marked" do
      song_ids = [@song1.id, @song2.id]

      results = @youtube_monetization.mark_as_ineligible(song_ids)
      expect(results[false].first.song_id).to eq @song1.id
      expect(results[true].count).to eq 1
    end
  end

  describe "#mark_as_eligible" do
    it "should not permit marking of songs the user does not own" do
      expect { @youtube_monetization.mark_as_eligible([@others_song.id]) }.to raise_error(RuntimeError, "You cannot mark songs you don't own")
    end

    it "should destroy YtmIneligibleSong objects for each passed Song id" do
      @youtube_monetization.mark_as_eligible([@song1.id])
      expect(@ytm_tracks.ineligible_songs).to eq([])
    end
  end

  describe "#monetize_or_mark_ineligible" do
    it "should sort songs properly and call the right methods" do
      expect(@youtube_monetization).to receive(:monetize_songs).with(["1"])
      expect(@youtube_monetization).to receive(:mark_as_ineligible).with(["2"])
      @youtube_monetization.monetize_or_mark_ineligible({"1" => "monetize", "2" => "ineligible", "3" => "exterminate"})
    end

    it "should not call methods that are not needed for the passed songs" do
      expect(@youtube_monetization).not_to receive(:monetize_songs)
      expect(@youtube_monetization).not_to receive(:mark_as_ineligible)
      @youtube_monetization.monetize_or_mark_ineligible({})
    end
  end

  describe "#block_songs" do
    it "should not permit blocking of songs the user does not own" do
      expect { @youtube_monetization.block_songs([@others_song.id]) }.to raise_error(RuntimeError, "You cannot mark songs you don't own")
    end

    it "should return the ids of songs successfully and unsuccessfully marked" do
      song_ids = [@song2.id, @song3.id]

      results = @youtube_monetization.block_songs(song_ids)
      expect(results[false].first.song_id).to eq(@song3.id)
      expect(results[true].count).to eq 1
    end

    it "should create a ytm_blocked_song record" do
      song_ids = [@song2.id, @song3.id]

      ytm_blocked_song_count = YtmBlockedSong.count

      @youtube_monetization.block_songs(song_ids)

      expect(@song2.ytm_blocked_song).to be_an_instance_of(YtmBlockedSong)
      expect(YtmBlockedSong.count).to eq(ytm_blocked_song_count + 1)
    end

    context "when a song has already been delivered" do
      it "takes down the associated salepoint_song" do
        create(:distribution_song, salepoint_song: @salepoint_song2)

        expect(@salepoint_song2.reload.takedown_at).to eq(nil)

        song_ids = [@song2.id]

        @youtube_monetization.block_songs(song_ids)

        expect(@salepoint_song2.reload.takedown_at).to_not eq(nil)
      end
    end
  end

  describe "#unblock_songs" do
    it "should not permit marking of songs the user does not own" do
      expect { @youtube_monetization.unblock_songs([@others_song.id]) }.to raise_error(RuntimeError, "You cannot mark songs you don't own")
    end

    it "should destroy YtmIneligibleSong objects for each passed Song id" do
      @youtube_monetization.unblock_songs([@song3.id])
      expect(@ytm_tracks.blocked_songs).to eq([])
    end

    context "when a song has already been delivered" do
      it "removes the takedown of the associated salepoint_song" do
        create(:distribution_song, salepoint_song: @salepoint_song2)

        YtmBlockedSong.create(song: @song2)

        expect(@salepoint_song2.reload.takedown_at).to_not eq(nil)

        song_ids = [@song2.id]

        @youtube_monetization.unblock_songs(song_ids)

        expect(@salepoint_song2.reload.takedown_at).to eq(nil)
      end
    end
  end

  describe "#search" do
    it "should not return other users' songs" do
      expect(@ytm_tracks.search.first.include?(@others_song)).to be_falsey
    end

    it "should return all of user's songs with album-level YouTubeSR salepoints if not passed options" do
      expect(@ytm_tracks.search.first).to match_array([@song1, @song2])
    end

    it "should return [songs, true] if any songs don't have album-level YouTubeSR salepoints" do
      @salepoint1.destroy
      expect(@ytm_tracks.search[1]).to be_truthy
    end

    it "should return [songs, false] if all songs have album-level YouTubeSR salepoints" do
      salepoint3 = create(:salepoint, salepointable: @album3, store: @yt_store, status: "complete")

      expect(@ytm_tracks.search[1]).to be_falsey
    end

    it "should return [empty array, true] if no songs have album-level YouTubeSR salepoints" do
      @salepoint1.destroy
      @salepoint2.destroy
      results = @ytm_tracks.search
      expect(results.first).to eq([])
      expect(results[1]).to be_truthy
    end

    it "should filter by artist" do
      expect(@ytm_tracks.search(artists: @artist1.id).first).to eq([@song1])
    end

    it "should filter by release (album/single)" do
      expect(@ytm_tracks.search(releases: @album1.id).first).to eq([@song1])
    end

    it "should filter by salepoint_song statuses" do
      expect(@ytm_tracks.search(status: "processing").first).to match_array([@song1, @song2])
    end

    it "should filter by ineligible status" do
      expect(@ytm_tracks.search(status: "marked_ineligible").first).to eq([@song1])
    end

    it "should filter by taken down status" do
      @salepoint_song2.takedown_at = Time.now
      @salepoint_song2.save
      expect(@ytm_tracks.search(status: "taken_down").first).to eq([@song2])
    end

    it "should return a song if it has a salepoint_song even if the album is takendown" do
      @album1.takedown!
      expect(@ytm_tracks.search.first).to match_array([@song1, @song2])
    end
  end

  describe "#albums" do
    it "should return all eligible albums" do
      expect(@ytm_tracks.albums).to match_array([@album1, @album2, @album3])
    end
  end
end

describe YoutubeMonetization do
  describe "#create_youtube_purchase" do
    it "should call Product::add_to_cart" do
      ytm = FactoryBot.create(:youtube_monetization)
      product = Product.find(Product::US_YTM)
      expect(Product).to receive(:add_to_cart)

      ytm.create_youtube_purchase(product)
    end
  end

  describe "#after_successful_purchase" do
    let(:ytm) { build(:youtube_monetization) }

    it "sets the effective_date" do
      expect(ytm.effective_date).to be_nil
      ytm.after_successful_purchase
      expect(ytm.reload.effective_date).not_to be_nil
    end

    it "enqueues a job within the TrackMonetization::SubscriptionWorker" do
      expect(TrackMonetization::SubscriptionWorker).to receive(:perform_async)
        .with(person_id: ytm.person.id, service: "YTTracks")

      ytm.after_successful_purchase
    end
  end

  describe "#monetize_songs" do
    before(:each) do
      person = FactoryBot.create(:person)
      album = FactoryBot.create(:album, person: person)
      store = Store.find_by(abbrev: "ytsr")
      FactoryBot.create(:salepointable_store, store: store)
      @salepoint = FactoryBot.create(:salepoint, salepointable: album, store: store)
      @song1 = FactoryBot.create(:song, album: album)
      @song2 = FactoryBot.create(:song, album: album)

      @ytm = FactoryBot.create(:youtube_monetization, person: person)
    end

    it "should call SalepointSong::create on all songs without salepoint_songs" do
      song_ids = [@song1.id, @song2.id]

      expect(SalepointSong).to receive(:create!).with(state: "approved", salepoint_id: @salepoint.id, song_id: @song1.id)
      expect(SalepointSong).to receive(:create!).with(state: "approved", salepoint_id: @salepoint.id, song_id: @song2.id)
      @ytm.monetize_songs(song_ids)
    end

    context "song1 already has salepoint_song for ytsr" do
      it "should only call create on song that do not have " do
        FactoryBot.create(:salepoint_song, salepoint: @salepoint, song: @song1)

        expect(SalepointSong).not_to receive(:create!).with(state: "approved", salepoint_id: @salepoint.id, song_id: @song1.id)
        expect(SalepointSong).to receive(:create!).with(state: "approved", salepoint_id: @salepoint.id, song_id: @song2.id)

        @ytm.monetize_songs([@song1.id, @song2.id])
      end
    end
  end
end
