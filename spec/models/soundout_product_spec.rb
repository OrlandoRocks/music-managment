require "rails_helper"

describe SoundoutProduct do
  describe "#order_report" do
    it "should return the provider itentifer and process the order" do
      report_purchase = TCFactory.build(:soundout_report)
      report          = report_purchase.soundout_product
      allow(report_purchase).to receive(:paid?).and_return(true)

      expect(report_purchase).to receive(:process_order)
      soundout_id = "1234"
      expect(Soundout).to receive(:order_report).and_return(soundout_id)
      expect(report.order_report(report_purchase)).to eq(soundout_id)
    end
  end

  describe "#fetch_report" do
    it "should return the status and the report_data and process the fetch" do
      report_purchase = TCFactory.build(:soundout_report)
      report          = report_purchase.soundout_product

      report_data = "Data"
      expect(report_purchase).to receive(:process_report_fetch).with(true, report_data)
      expect(Soundout).to receive(:fetch_report).and_return([true, report_data])
      expect(report.fetch_report(report_purchase)).to eq([true, report_data])
    end
  end

  describe "#purchase" do
    before(:each) do
      @person = Person.first
      @report = SoundoutProduct.find_for_purchase(SoundoutProduct::STARTER, @person.country_website)
      @report_purchase = TCFactory.build(:soundout_report)
      purchase = Purchase.new
      expect(@report_purchase).to receive(:valid?).and_return(true)
      expect(Product).to receive(:add_to_cart).and_return(purchase)
      expect(purchase).to receive(:valid?).and_return(true)
    end

    it "should create song_library_upload_id param based on asset type" do
      assets = {}
      purchases = {}
      purchases["1"] = SoundoutProduct::STARTER
      assets["1"] = "song_library"
      expect(SoundoutReport).to receive(:create).with(
        soundout_product_id: @report.id,
        person_id: @person.id,
        status: "new",
        track_id: "1",
        track_type: "SongLibraryUpload"
      ).and_return(@report_purchase)

      expect(SoundoutProduct.purchase(@person, purchases, assets)).to eq(error_type: false)
    end

    it "should create song_id param based on asset type" do
      assets = {}
      purchases = {}
      purchases["1"] = SoundoutProduct::STARTER
      assets["1"] = "song"
      expect(SoundoutReport).to receive(:create).with(
        soundout_product_id: @report.id,
        person_id: @person.id,
        status: "new",
        track_id: "1",
        track_type: "Song"
      ).and_return(@report_purchase)

      expect(SoundoutProduct.purchase(@person, purchases, assets)).to eq(error_type: false)
    end
  end

  describe "::soundout_products_detail" do
    it "returns the proper soundout_products for supplied currency" do
      country_website = create(:country_website)
      product = create(:product, country_website_id: country_website.id)
      soundout_products = create(:soundout_product, product: product)

      result = SoundoutProduct.soundout_products_detail(country_website.id)

      expect(result).to eq(
        [soundout_products.as_json(only: [:id, :report_type, :display_name, :price, :currency], root: false)]
      )
    end
  end

  describe "#as_json" do
    it "returns the expected json" do
      soundout_product = create(:soundout_product)

      result = soundout_product.as_json

      expect(result).to eq expected_soundout_product_json(soundout_product)
    end

    def expected_soundout_product_json(soundout_product)
      {
        soundout_product: {
          "available_in_days" => soundout_product.available_in_days,
          "created_at" => soundout_product.created_at.as_json,
          "currency" => soundout_product.product.currency,
          "display_name" => soundout_product.display_name,
          "id" => soundout_product.id,
          "number_of_reviews" => soundout_product.number_of_reviews,
          "price" => soundout_product.product.price.as_json,
          "product_id" => soundout_product.product_id,
          "report_type" => soundout_product.report_type,
          "updated_at" => soundout_product.updated_at.as_json
        }
      }
    end
  end
end
