require 'rails_helper'

RSpec.describe CompanyInformation, type: :model do
  describe "Validations" do
    [:enterprise_number, :company_registry_data, :place_of_legal_seat].each do |field|
       it { should allow_value("test () . / _ - @ # , *").for(field) }
       it { should_not allow_value("not <> allowed").for(field) }
    end

    it "validates customer_type is business" do
      business_user = create(:person, customer_type: 'business', country: "LU")
      businsess_user_company_info = business_user.build_company_information

      individual_user = create(:person, customer_type: 'individual', country: "LU")
      individual_user_company_info = individual_user.build_company_information

      expect(businsess_user_company_info.valid?).to be true
      expect(individual_user_company_info.valid?).to be false
    end
  end
end
