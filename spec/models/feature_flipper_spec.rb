require "rails_helper"

describe FeatureFlipper do
  before (:each) do
    $redis.flushdb
    FeatureFlipper.add_feature("Nuclear Holocaust")
    FeatureFlipper.add_feature("Zombie Apocalypse")
    FeatureFlipper.add_feature("Asteroid Collision")
  end

  it "correctly creates the feature name" do
    expect(FeatureFlipper.all_features[2].name).to eq :asteroid_collision
  end

  it "creates features in a deactivated state" do
    expect(FeatureFlipper.all_features[1].percentage).to eq 0
    expect(FeatureFlipper.all_features[1].name).to eq :zombie_apocalypse
  end

  it "updates a specific feature" do
    user1 = FactoryBot.create(:person)
    user2 = FactoryBot.create(:person, email: "y@example.com")
    user3 = FactoryBot.create(:person, email: "z@example.com")

    FeatureFlipper.update_feature :nuclear_holocaust, 50, "#{user1.id}, #{user2.id}"
    expect(FeatureFlipper.all_features[0].percentage).to eq 50
    expect(FeatureFlipper.all_features[0].users).to include user1.id.to_s
    expect(FeatureFlipper.all_features[0].users).to include user2.id.to_s
    expect(FeatureFlipper.all_features[0].users).to_not include user3.id.to_s
  end

  context "tells whether a specific user can access a feature" do
    let(:user){ FactoryBot.create(:person) }

    context "when the user does not have access" do
      it "denies access to the feature" do
        expect(
          FeatureFlipper.show_feature?(:nuclear_holocaust, user)
        ).to be false

      end
    end

    context "when the user does have access by ID" do
      it "allows access to the feature" do
        FeatureFlipper.update_feature :nuclear_holocaust, 0, user.id.to_s

        expect(
          FeatureFlipper.show_feature?(:nuclear_holocaust, user)
        ).to be true
      end
    end

    context "when the user has access but not by ID" do
      it "allows access to the feature" do
        FeatureFlipper.update_feature :nuclear_holocaust, 100, ""

        expect(
          FeatureFlipper.show_feature?(:nuclear_holocaust, user)
        ).to be true
      end
    end

    context "when a user is NOT passed in to .show_feature?" do

      context "and the percent on is 100%" do
        it "allows access" do
          FeatureFlipper.update_feature :nuclear_holocaust, 100, ""

          expect(FeatureFlipper.show_feature?(:nuclear_holocaust, nil)).to be true
        end
      end

      context "and the percent on is less than 100%" do
        it "does NOT allow access" do
          FeatureFlipper.update_feature :nuclear_holocaust, 99, ""

          expect(FeatureFlipper.show_feature?(:nuclear_holocaust, nil)).to be false
        end
      end
    end

    context "when Redis is unavailable" do
      it "rescues the error and defaults all features to off" do
        FeatureFlipper.update_feature :asteroid_collision, 99, ""
        FeatureFlipper.update_feature :nuclear_holocaust, 99, ""

        error = StandardError.new("Error connecting to Redis - (ECONNREFUSED)")
        allow($rollout).to receive(:active?).and_raise(error)

        expect(FeatureFlipper.show_feature?(:asteroid_collision, nil)).to be false
        expect(FeatureFlipper.show_feature?(:nuclear_holocaust, nil)).to be false
      end
    end
  end

  describe ".get_users" do
    let(:user_ids) { ["1", "2"] }

    before do
      FeatureFlipper.add_by_user_ids(:mass_balance_adjustment_email, user_ids)
    end

    it "should return an array of user ids who have the feature flag" do
      user_ids = FeatureFlipper.get_users(:mass_balance_adjustment_email)
      expect(user_ids).to match user_ids
    end
  end

  describe ".add_by_user_ids" do
    context "when an array of user ids is not passed in" do
      it "should return" do
        result = FeatureFlipper.add_by_user_ids(:mass_balance_adjustment_email, 1)
        expect(result).to be_nil
      end
    end

    context "when an array of user ids is passed in" do
      it "should add the user ids to the feature flag" do
        user_ids = ["1", "2"]
        existing_users = FeatureFlipper.get_users(:mass_balance_adjustment_email)
        expect(existing_users).not_to include(user_ids)

        FeatureFlipper.add_by_user_ids(:mass_balance_adjustment_email, user_ids)

        new_users = FeatureFlipper.get_users(:mass_balance_adjustment_email)
        expect(new_users).to match(user_ids)
      end
    end
  end
end
