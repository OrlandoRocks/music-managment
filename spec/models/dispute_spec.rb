require "rails_helper"

RSpec.describe Dispute, type: :model do
  context "Associations" do
    it { should belong_to(:invoice) }
    it { should belong_to(:refund) }
    it { should belong_to(:processed_by) }
    it { should belong_to(:dispute_status) }
    it { should belong_to(:person) }
  end

  context "Validations" do
    it { should validate_presence_of(:source) }
    it { should validate_presence_of(:person) }
    it { should validate_presence_of(:invoice) }
    it { should validate_presence_of(:dispute_identifier) }
    it { should validate_presence_of(:transaction_identifier) }
    it { should validate_presence_of(:dispute_status) }
    it { should validate_presence_of(:currency) }
    it { should validate_presence_of(:amount_cents) }
    it { should validate_presence_of(:reason) }
  end

  describe "#maximum_chargeback_amount_cents" do
    it "returns full transaction amount when there are no refunds/disputes" do
      dispute = create(:dispute)

      expect(dispute.maximum_chargeback_amount_cents)
        .to eq(dispute.source.amount * 100)
    end

    it "returns maximum chargeback amount" do
      dispute = create(:dispute)
      invoice = dispute.invoice
      refund = create(
        :refund,
        :with_braintree_settlement,
        invoice: invoice
      )

      dispute_source_amount = dispute.source.amount * 100
      refunded_amount = refund.refund_settlements.sum(:settlement_amount_cents)

      expect(dispute.maximum_chargeback_amount_cents)
        .to eq(dispute_source_amount - refunded_amount)
    end
  end
end
