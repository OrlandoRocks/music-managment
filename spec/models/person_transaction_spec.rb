require "rails_helper"

describe PersonTransaction do
  context "#date_range_by_user" do
    let(:person)       { create(:person) }
    let(:other_person) { create(:person) }

    it "only selects the created_at field" do
      txn = create(:person_transaction, person: person)
      response = PersonTransaction.date_range_by_user(person)
      response_txn = response.first

      expect(response_txn.created_at).to be_within(1.second).of(txn.created_at)
      expect(response_txn.id).to be_nil
    end

    it "loads all transactions in a given range" do
      months_range = (3...0).to_a
      start_of_month = Date.parse('1-9-2018')
      months_range.each do |number_of_months|
        Timecop.freeze(start_of_month - number_of_months.months) do
          create(:person_transaction, person: person)
        end
      end

      response = PersonTransaction.date_range_by_user(person)

      expect(response.length).to eq months_range.length
    end

    it "groups transactions by month" do
      months_range = (3...0).to_a
      start_of_month =  Date.parse('1-9-2018')
      months_range.each do |number_of_months|
        Timecop.freeze(start_of_month - number_of_months.months) do
          5.times do
            create(:person_transaction, person: person)
          end
        end
      end

      response = PersonTransaction.date_range_by_user(person)
      expect(response.length).to eq months_range.length
    end

    it "should only load dates for that user" do
      create(:person_transaction, person: person)
      start_of_month = Date.today.at_beginning_of_month

      Timecop.freeze(start_of_month - 3.months) do
        create(:person_transaction, person: other_person)
      end

      response = PersonTransaction.date_range_by_user(person)
      expect(response.length).to eq 1
    end
  end

  context "debit and credit" do
    let(:person) { create(:person, :with_balance, amount: 100) }
    let(:balance) { person.person_balance }

    it "should debit the person balance if a person transaction has a debit" do
      _txn = create(
        :person_transaction,
        person: person,
        target_id: 1,
        target_type: 'BalanceAdjustment',
        debit: 1.00,
        credit: 0.00,
        previous_balance: balance.balance
      )

      expect(balance.reload.balance).to eq(99.00)
    end

    it "should credit the person balance if a person transaction is created with a credit" do
      _txn = create(
        :person_transaction,
        person: person,
        target_id: 1,
        target_type: 'BalanceAdjustment',
        previous_balance: balance.balance,
        credit: 1.00,
        debit: 0.00
      )

      expect(balance.reload.balance).to eq(101.00)
    end

    it "should not be valid if the currency does not match person_balance currency" do
      txn = create(
        :person_transaction,
        person: person,
        target_id: 1,
        target_type: 'BalanceAdjustment',
        previous_balance: balance.balance,
        credit: 1.00
      )

      balance.update_attribute(:currency, "CAD")
      expect(txn).not_to be_valid
    end
  end

  context "country-specific currency" do
    let(:american_person)   { create(:person, country: 'US') }
    let(:canadian_person)   { create(:person, country: 'CA') }
    let(:italian_person)    { create(:person, country: 'IT') }
    let(:german_person)     { create(:person, country: 'DE') }
    let(:french_person)     { create(:person, country: 'FR') }
    let(:australian_person) { create(:person, country: 'AU') }
    let(:british_person)    { create(:person, country: 'GB') }

    it "should set the balance currency to USD if they are a US customer" do
      balance = american_person.person_balance
      txn = create(
        :person_transaction,
        person: american_person,
        target_id: 1,
        target_type: 'BalanceAdjustment',
        previous_balance: balance.balance,
        credit: 1.00
      )

      expect(balance.reload.currency).to eq(txn.currency)
      expect(txn.currency).to eq("USD")
    end

    it "should set the balance currency to CAD if they are a CA customer" do
      balance = canadian_person.person_balance
      txn = create(
        :person_transaction,
        person: canadian_person,
        target_id: 1,
        target_type: 'BalanceAdjustment',
        previous_balance: balance.balance,
        credit: 1.00
      )

      expect(balance.reload.currency).to eq(txn.currency)
      expect(txn.currency).to eq("CAD")
    end

    it "should set the balance currency to AUD if they are an AU customer" do
      balance = australian_person.person_balance
      txn = create(
        :person_transaction,
        person: australian_person,
        target_id: 1,
        target_type: 'BalanceAdjustment',
        previous_balance: balance.balance,
        credit: 1.00
      )

      expect(balance.reload.currency).to eq(txn.currency)
      expect(txn.currency).to eq("AUD")
    end

    it "should set the balance currency to GBP if they are a UK customer" do
      balance = british_person.person_balance
      txn = create(
        :person_transaction,
        person: british_person,
        target_id: 1,
        target_type: 'BalanceAdjustment',
        previous_balance: balance.balance,
        credit: 1.00
      )

      expect(balance.reload.currency).to eq(txn.currency)
      expect(txn.currency).to eq("GBP")
    end

    it "should set the balance currency to EUR if they are an EU customer" do
      txns = [italian_person, french_person, german_person].map do |european|
        create(
          :person_transaction,
          person: european,
          target_id: 1,
          target_type: 'BalanceAdjustment',
          previous_balance: european.person_balance,
          credit: 1.00
        )
      end

      balances = [italian_person, french_person, german_person].map(&:person_balance)

      expect(balances.map(&:currency)).to all(eq('EUR'))
      expect(txns.map(&:currency)).to all(eq('EUR'))
    end
  end

  describe ".transaction_types_by_user" do
    let(:person)                  { build(:person) }
    let(:payout_transaction_type) { PersonTransaction::PAYOUT_TRANSACTION_TYPE.first }

    context "when the payoneer_payout feature flag is disabled" do
      it "does not return the Payout Transaction transaction type" do
        allow(Payoneer::FeatureService).to receive(:payoneer_payout_enabled?).and_return(false)
        transaction_types = PersonTransaction.transaction_types_by_user(person)
        expect(transaction_types).not_to include payout_transaction_type
      end
    end

    context "when the payoneer_payout feature flag is enabled" do
      it "returns the Payout Transaction transaction type" do
        allow(Payoneer::FeatureService).to receive(:payoneer_payout_enabled?).and_return(true)
        transaction_types = PersonTransaction.transaction_types_by_user(person)
        expect(transaction_types).to include payout_transaction_type
      end
    end
  end
end

describe PersonTransaction, "scopes" do
  context ".irs_tax_withholdings" do
    let(:person) { create(:person, :with_balance, amount: 100) }
    let(:txn1) { create(:person_transaction, person: person, target_type: "IRSTaxWithholding") }
    let(:txn2) { create(:person_transaction, person: person, target_type: "BalanceAdjustment") }

    it "returns transactions with 'IRSTaxWithholding' target_type" do
      expect(PersonTransaction.irs_tax_withholding_transactions).to include(txn1)
      expect(PersonTransaction.irs_tax_withholding_transactions).not_to include(txn2)
    end
  end

  context ".non_rollover_transactions" do
    let(:person) { create(:person, :with_balance, amount: 100) }
    let(:txn) { create(:person_transaction, person: person, target_type: "TaxableEarningsRollover") }

    it "excludes transactions with target_type 'TaxableEarningsRollover'" do
      expect(PersonTransaction.all).to include(txn)
      expect(PersonTransaction.non_rollover_transactions).not_to include(txn)
    end
  end

  context ".for_us_taxable_balance_adjustment_target" do
    let!(:person) { create(:person, :with_balance, amount: 100) }
    let!(:balance_adjustment1) {
      create(
        :balance_adjustment, person: person, category: "Songwriter Royalty", credit_amount: "12.45", posted_by: person,
                             posted_by_name: person.name
      )
    }
    let!(:balance_adjustment2) {
      create(
        :balance_adjustment, person: person, category: "Tidal DAP", credit_amount: "12.45", posted_by: person,
                             posted_by_name: person.name
      )
    }
    let!(:balance_adjustment3) {
      create(
        :balance_adjustment, person: person, category: "YouTube MCN Royalty", credit_amount: "12.45", posted_by: person,
                             posted_by_name: person.name
      )
    }

    let!(:txn1) {
      create(:person_transaction, person: person, target_id: balance_adjustment1.id, target_type: "BalanceAdjustment")
    }
    let!(:txn2) {
      create(:person_transaction, person: person, target_id: balance_adjustment2.id, target_type: "BalanceAdjustment")
    }
    let!(:txn3) {
      create(:person_transaction, person: person, target_id: balance_adjustment3.id, target_type: "BalanceAdjustment")
    }

    it "includes taxable balance adjustment targets" do
      expect(PersonTransaction.for_us_taxable_balance_adjustment_target).to include(txn1, txn2)
      expect(PersonTransaction.for_us_taxable_balance_adjustment_target).not_to include(txn3)
    end
  end

  context ".with_splits_information" do

    it "returns split information for the transaction" do
      person = create(:person, :with_balance, amount: 100)
      song = create(:song)
      split = create(:royalty_split)
      intake = create(:royalty_split_intake, person_id: person.id)
      split_detail =
        create(
          :royalty_split_detail,
          person_id: person.id,
          owner_id: person.id,
          song_id: song.id,
          royalty_split_id: split.id,
          royalty_split_title: split.title,
          royalty_split_intake_id: intake.id)
      txn = create(:person_transaction, person: person, target: intake)

      txn_with_splits = PersonTransaction.with_splits_information(person.id, PersonTransaction.all).first

      expect(txn_with_splits.transaction_id).to eq(txn.id)
      expect(txn_with_splits.amount).to eq(-10)
      expect(txn_with_splits.currency).to eq("USD")
      expect(txn_with_splits.song_id).to eq(song.id)
      expect(txn_with_splits.person_id).to eq(person.id)
      expect(txn_with_splits.royalty_split_id).to eq(split_detail.royalty_split_id)
      expect(txn_with_splits.royalty_split_title).to eq(split_detail.royalty_split_title)
    end
  end
end
