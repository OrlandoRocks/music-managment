require "rails_helper"

describe BulkReleaseSubscriptionForm do
  let(:album)  { FactoryBot.create(:album, :with_one_year_renewal) }
  describe "#process" do
    context "subscription_actions is mapped to a cancel" do
      it "should call cancel on ReleaseSubscription" do
        expect_any_instance_of(ReleaseSubscription).to receive(:process_cancel).once
        BulkReleaseSubscriptionForm.new({subscription_actions: {"#{album.id}" => 'cancel'}}).process
      end

      it "should send cancel email for album" do
        expect_any_instance_of(ReleaseSubscription).to receive(:process_cancel).and_return(true)
        expect_any_instance_of(PersonNotifier).to receive(:album_renewal_change_cancel_status).with(album).once
        BulkReleaseSubscriptionForm.new({subscription_actions: {"#{album.id}" => 'cancel'}}).process
      end      
    end

    context "subscription_actions is mapped to a takedown" do
      it "should call takedown on ReleaseSubscription" do
        expect_any_instance_of(ReleaseSubscription).to receive(:process_takedown).once
        BulkReleaseSubscriptionForm.new({subscription_actions: {"#{album.id}" => 'takedown'}}).process
      end

      it "should send takedown email for album" do
        expect_any_instance_of(ReleaseSubscription).to receive(:process_takedown).and_return(true)
        expect_any_instance_of(BatchNotifier).to receive(:takedown).with(album.person, [album.renewal], album.takedown_at).once
        BulkReleaseSubscriptionForm.new({subscription_actions: {"#{album.id}" => 'takedown'}}).process
      end
    end

    context "subscription_actions is invalid" do
      it "should not call takedown or cancel on ReleaseSubscription" do
        expect_any_instance_of(ReleaseSubscription).not_to receive(:process_takedown)
        expect_any_instance_of(ReleaseSubscription).not_to receive(:process_cancel)
        expect(BulkReleaseSubscriptionForm.new({subscription_actions: {"#{album.id}" => 'foobar'}}).process).to be(false)
      end
    end
  end
end
