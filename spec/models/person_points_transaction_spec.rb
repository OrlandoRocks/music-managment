require 'rails_helper'

RSpec.describe PersonPointsTransaction, type: :model do
  describe "relationships" do
    subject { build(:person_points_transaction) }

    it { should belong_to(:person) }
  end

  describe "validations" do
    let(:subject) { build(:person_points_transaction) }

    it { should validate_presence_of(:person) }
    it { should validate_presence_of(:target) }

    describe "validate_credit_or_debit" do
      it "should add error if both credit & debit points are present" do
        subject.credit_points = subject.debit_points = 10

        subject.validate

        expect(subject.errors[:transaction].join).to include("cannot have both credit & debit")
      end

      it "should add error if neithr credit nor debit points are present" do
        subject.credit_points = subject.debit_points = 0

        subject.validate
        expect(subject.errors[:transaction].join).to include("cannot be empty on both credit & debit")
      end
    end

    describe "validate target_credit_debit" do
      context "debit transaction" do
        before do
          subject.debit_points = 10
          subject.credit_points = 0
        end

        it "should add error if not in allowed debit source" do
          subject.target = create(:achievement)
          subject.validate

          expect(subject.errors[:target].join).to include("outside of allowed debit classes")
        end
      end

      context "credit transaction" do
        before do
          subject.credit_points = 10
          subject.debit_points = 0
        end

        it "should add error if not in allowed credit source" do
          subject.target = create(:reward)
          subject.validate

          expect(subject.errors[:target].join).to include("outside of allowed credit classes")
        end
      end
    end

    it "should not allow to update points after created" do
      subject.save

      subject.assign_attributes({credit_points: 10})
      subject.validate

      expect(subject.errors[:transaction].join).to include("cannot update credit or debit points!")
    end
  end

  describe "callbacks" do
    describe "#update_balance" do
      let(:person) { create(:person) }
      let(:transaction) { build(:person_points_transaction, person: person, credit_points: 100) }

      it "should update balance of user appropriate" do
        points = person.available_points
        transaction.save

        expect(person.available_points).to eq(points + transaction.credit_points)
      end
    end
  end

  describe "#is_credit?" do
    it "should return true for credit transactions" do
      transaction = create(:person_points_transaction)

      expect(transaction.is_credit?).to be_truthy
    end

    it "should return false for credit transactions" do
      transaction = create(:person_points_transaction, :debit_transaction)

      expect(transaction.is_credit?).to be_falsey
    end
  end

  describe "#is_debit?" do
    it "should return true for credit transactions" do
      transaction = create(:person_points_transaction)

      expect(transaction.is_debit?).to be_falsey
    end

    it "should return false for credit transactions" do
      transaction = create(:person_points_transaction, :debit_transaction)

      expect(transaction.is_debit?).to be_truthy
    end
  end

  describe "#update_balance" do
    it "should update balance of the difference between credit & debit" do
      transaction = create(:person_points_transaction, :debit_transaction, debit_points: 10)
      person = transaction.person
      person.points_balance.update(balance: 0)

      transaction.update_balance
      expect(person.points_balance.reload.balance).to eq(-10)
    end
  end
end
