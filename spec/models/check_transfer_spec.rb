require "rails_helper"

describe CheckTransfer do

  describe ".generate" do
    before(:each) do
      @person = create(:person, :with_balance, amount: 50_000, password: "Testpass123!")
      @params = {
        payee:       "Little Bobby Tables",
        city:        "Brooklyn",
        state:       "NY",
        postal_code: "11201",
        country:     "US",
        address1:    "45 Main Street",
        payment_amount: 1000,
        password_entered: "Testpass123!"
      }
    end

    it "creates a new check_transfer" do
      expect {
        CheckTransfer.generate(@person, @params)
      }.to change { CheckTransfer.count }.by(1)
    end

    it "sends an email on successful check_transfer creation" do
      expect{CheckTransfer.generate(@person, @params)}.to change{ActionMailer::Base.deliveries.count}.by(1)
    end
  end

  describe "#cancel" do

    before(:each) do
      @check_transfer = TCFactory.create(:check_transfer)
    end

    it "should rollback the fee and the payment in seperate transactions" do
      expect {
        @check_transfer.cancel
      }.to change(PersonTransaction, :count).by(2)
    end

    it "should return false if cannot rollback" do
      @check_transfer.update_attribute(:transfer_status,"completed")
      expect(@check_transfer.cancel).to eq(false)
    end

    it "should return true if rollback occurs" do
      expect(@check_transfer.cancel).to eq(true)
    end

    it "should update the status to 'cancelled'" do
      @check_transfer.cancel
      expect(@check_transfer.reload.transfer_status).to eq("cancelled")
    end

    it "should work in a transaction" do
      expect {
        expect(@check_transfer).to receive(:transfer_status=).and_raise(StandardError)
        expect(@check_transfer.cancel).to eq(false)
      }.not_to change(PersonTransaction,:count)
    end
  end

  # flag the first occurrence of a Payee
  describe "#set_suspicious_flags" do
    let(:customer) do
      FactoryBot.create(:person, :with_balance, amount: 50_000,
        email: 'check_withdraw@tunecore.com',
        password: "Testpass123!"
      )
    end
    let(:test_params) do
      { payment_amount: 101.00, postal_code: "11201", state: "NY", currency: "USD", country: "United States", city: "Brooklyn", address1: "123 Main St", payee: "Good Guy", password_entered: "Testpass123!" }
    end

    it " does not flag a first check withdrawal" do
      check_transfer = CheckTransfer.generate(customer, test_params)
      expect(check_transfer.suspicious_flags["payee"]).to eq false
    end

    it " does not flag a check withdrawal when no completed withdrawals exist" do
      check_transfer = CheckTransfer.generate(customer, test_params)
      expect(check_transfer.suspicious_flags["payee"]).to eq false
    end

    it " does not flag a check withdrawal when a completed withdrawal exists with same payee" do
      check_transfer = CheckTransfer.generate(customer, test_params)
      expect(check_transfer.suspicious_flags["payee"]).to eq false
    end

    it " does flag a check withdrawal when a completed withdrawal exists with a different payee" do
      check_transfer = CheckTransfer.generate(customer, test_params)
      check_transfer.transfer_status = "completed"
      check_transfer.save

      new_transfer = CheckTransfer.generate(customer, test_params.merge!(payee: 'Bad Guy'))
      expect(new_transfer.suspicious_flags["payee"]).to eq true
    end

    it " does not flag a check withdrawal when a completed withdrawal exists with a payee in different case" do
      check_transfer = CheckTransfer.generate(customer, test_params)
      check_transfer.transfer_status = "completed"
      check_transfer.save

      new_transfer = CheckTransfer.generate(customer, test_params.merge!(payee: 'good guy'))
      expect(new_transfer.suspicious_flags["payee"]).to eq false
    end

    it " check withdrawals with mixed qoutes match existing completed withdrawals correctly" do
      check_transfer = CheckTransfer.generate(customer, test_params.merge!(payee: 'Men\'s choir"s'))
      check_transfer.transfer_status = "completed"
      check_transfer.save

      new_transfer = CheckTransfer.generate(customer, test_params.merge!(payee: 'Men\'s choir"s'))
      expect(new_transfer.suspicious_flags["payee"]).to eq false
    end
  end
end
