require "rails_helper"

describe CountryPhoneCode do

  describe ".codes_with_country_names" do
    before(:each) do
      @countries = Country.where("iso_code in (?)", ["US", "CA", "FR", "IT"])
    end

    before(:each) do
      @countries.map { |c| create(:country_phone_code, country: c) }
    end

    it "returns an array of country names and phone codes" do
      result = CountryPhoneCode.codes_with_country_names

      expect(result[0].name).to eq "Afghanistan"
      expect(result[0].code).to eq "93"

      expect(result[1].name).to eq "Albania"
      expect(result[1].code).to eq "355"

      expect(result[2].name).to eq "Algeria"
      expect(result[2].code).to eq "213"
    end
  end
end
