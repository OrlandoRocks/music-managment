require "rails_helper"

describe ReferralDatum do
  context "#person" do
    let(:person) { build_stubbed(:person) }
    let(:referral_datum) { build_stubbed(:referral_datum, person: person) }
    it "should return a person" do
      expect(referral_datum.person).to eq person
    end
  end
end
