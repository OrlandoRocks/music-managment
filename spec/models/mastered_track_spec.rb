require "rails_helper"

describe MasteredTrack do
  describe "::find_claims" do
    it "should return all the claim id for all MasteredTrack related purchases for an invoice" do
      person = FactoryBot.create(:person)
      invoice = FactoryBot.create(:invoice, person: person)
      mastered_track1 = FactoryBot.create(:mastered_track, person: person, claim_id: "yyyy-yyyy")
      mastered_track2 = FactoryBot.create(:mastered_track, person: person, claim_id: "xxxx-xxxx")
      FactoryBot.create(:purchase, related: mastered_track1, person: person, invoice: invoice)
      FactoryBot.create(:purchase, related: mastered_track2, person: person, invoice: invoice)

      claim_ids = MasteredTrack.find_claims(invoice.id)
      expect(claim_ids.include?(mastered_track1.claim_id)).to eq(true)
      expect(claim_ids.include?(mastered_track2.claim_id)).to eq(true)
      expect(claim_ids.length).to eq(2)
    end
  end

  describe "::retrieve_download_link" do
    it "should set the download_link for the mastered track found by person_id and landr_track_id" do
      person = FactoryBot.create(:person)
      mastered_track = FactoryBot.create(:mastered_track, person: person, landr_track_id: "123123")
      FactoryBot.create(:purchase, related: mastered_track, person: person)
      link = "link.url"

      allow(MasteredTrack).to receive(:invoice_downloads_complete?).and_return(false)
      MasteredTrack.retrieve_download_link(person.id, "landr_track_id", link, mastered_track.job_id, "")
      expect(mastered_track.reload.download_link).to eq(link)
    end
  end

  describe "::invoice_downloads_complete?" do
    describe "when there are 2 mastered_tracks in an invoice" do
      before(:each) do
        @person = FactoryBot.create(:person)
        @invoice = FactoryBot.create(:invoice, person: @person)
        @mastered_track1 = FactoryBot.create(:mastered_track, person: @person, job_id: "abc123")
        @mastered_track2 = FactoryBot.create(:mastered_track, person: @person, job_id: "321cba")
        FactoryBot.create(:purchase, related: @mastered_track1, person: @person, invoice: @invoice)
        FactoryBot.create(:purchase, related: @mastered_track2, person: @person, invoice: @invoice)
      end

      context "only one track has a download_link" do
        it "should return false" do
          @mastered_track1.update_attribute(:download_link, "link.url")
          expect(MasteredTrack.invoice_downloads_complete?(@invoice.id)).to eq(false)
        end
      end

      context "when both tracks have a download_link" do
        it "should return true" do
          @mastered_track1.update_attribute(:download_link, "link.url")
          @mastered_track2.update_attribute(:download_link, "link.url2")
          expect(MasteredTrack.invoice_downloads_complete?(@invoice.id)).to eq(true)
        end
      end
    end
  end

  describe "::add_items" do
    before(:each) do
      @person = FactoryBot.create(:person)
    end

    it "should create mastered tracks for each set of attribute hashes passed in and correctly set attributes" do
      attr1 = {'claim_id'=> "xxxx-xxxx", 'landr_purchase_date'=> "2014-12-02T21=>12=>09.2323178Z", 'name'=> "Track1", 'job_id'=> "abc123"}
      attr2 = {'claim_id'=> "yyyy-yyyy", 'landr_purchase_date'=> "2014-12-02T21=>12=>09.2323178Z", 'name'=> "Track2", 'job_id'=> "321cba"}

      mastered_tracks_param = [attr1, attr2]
      expect{
        MasteredTrack.add_items(@person, mastered_tracks_param)
      }.to change(MasteredTrack, :count).by(2)
    end
  end

  describe "::remove_items_by_claim_ids" do
    context "when there are 3 mastered tracksfor user1" do
      before(:each) do
        @person = FactoryBot.create(:person)
        @person2 = FactoryBot.create(:person, email: "test@email.com")
        @claim_id1 = "xxxx-xxxx-xxxx"
        @claim_id2 = "yyyy-yyyy-yyyy"
        @mastered_track1 = FactoryBot.create(:mastered_track, person: @person, claim_id: @claim_id2)
        @mastered_track2 = FactoryBot.create(:mastered_track, person: @person, claim_id: @claim_id1)
        @mastered_track3 = FactoryBot.create(:mastered_track, person: @person, claim_id: @claim_id1)
        @mastered_track4 = FactoryBot.create(:mastered_track, person: @person2, claim_id: @claim_id2)
      end

      it "should destroy all mastered tracks that match the claim_ids passed in for a user1" do
        destroyed_tracks = nil

        expect{
          destroyed_tracks = MasteredTrack.remove_items_by_claim_ids(@person, [@claim_id1, @claim_id2])
        }.to change(MasteredTrack, :count).by(-3)

        expect(destroyed_tracks.length).to eq(3)
        expect(destroyed_tracks.include?(@mastered_track1)).to eq(true)
        expect(destroyed_tracks.include?(@mastered_track2)).to eq(true)
        expect(destroyed_tracks.include?(@mastered_track3)).to eq(true)
        expect(destroyed_tracks.include?(@mastered_track4)).to eq(false)
      end

      it "should not destroy a mastered track that has been paid for" do
        @mastered_track4.update_attribute(:paid_at, Time.now)

        expect{
          MasteredTrack.remove_items_by_claim_ids(@person2, [@claim_id2])
        }.to change(MasteredTrack, :count).by(0)
      end
    end
  end

  describe "::purchase_track" do
    it "should call #purchase_track with the correct product for all mastered_track ids passed in for a user" do
      person = FactoryBot.create(:person)
      mastered_track1 = FactoryBot.create(:mastered_track, person: person)
      mastered_track2 = FactoryBot.create(:mastered_track, person: person)

      expect(Product).to receive(:find_products_for_country).with(person.country_domain, :mastered_track).and_return(true)
      expect(MasteredTrack).to receive(:where).and_return([mastered_track1, mastered_track2])

      expect(mastered_track1).to receive(:purchase_track)
      expect(mastered_track2).to receive(:purchase_track)

      MasteredTrack.purchase_track(person, [mastered_track1.id, mastered_track2.id])
    end
  end

  describe "#purchase_track" do
    before(:each) do
      @mastered_track = FactoryBot.create(:mastered_track)
      @product = Product.first
      @purchase = double(Purchase)
      allow(@purchase).to receive(:valid?).and_return(true)
    end

    it "should call Product::add_to_cart" do
      expect(Product).to receive(:add_to_cart).with(@mastered_track.person, @mastered_track, @product).and_return(@purchase)
      @mastered_track.purchase_track(@product)
    end

  end

  describe "#paid_post_proc" do
    before(:each) do
      @mastered_track = FactoryBot.create(:mastered_track)
      @paid_at = Time.now
      purchase = FactoryBot.create(:purchase, related: @mastered_track, person: @mastered_track.person, paid_at: @paid_at)
    end

    it "should set the paid_at to the paid_at of the associated purchase" do
      @mastered_track.paid_post_proc
      expect(@mastered_track.paid_at.to_s).to eq(@paid_at.to_s)
    end

  end

  describe "#destroy" do
    it "should not destroy the mastered_track record if it marked as paid_at" do
      mastered_track = FactoryBot.create(:mastered_track, paid_at: Time.now)
      mastered_track.destroy

      expect(MasteredTrack.where(id: mastered_track.id).present?).to eq(true)
    end

    it "should destroy the mastered_track record if it is not marked as paid" do
      mastered_track = FactoryBot.create(:mastered_track)
      mastered_track.destroy

      expect(MasteredTrack.where(id: mastered_track.id).present?).to eq(false)
    end
  end
end
