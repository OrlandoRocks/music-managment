require "rails_helper"

describe AlbumItunesStatus do
  describe "#update_failure" do
    before(:each) do
      @album_itunes_status = FactoryBot.create(:album_itunes_status, content_review_status: "asd", itunes_connect_status: "asd",stores_list: "asd", itunes_created_at: "asd", content_state_status: "asd")
    end

    it "sets the content_review_status to Failed to retrieve state" do
      @album_itunes_status.update_failure
      expect(@album_itunes_status.content_review_status).to eq('Failed to retrieve state')
    end

    it "sets all other fields to nil" do
      @album_itunes_status.update_failure
      expect(@album_itunes_status.itunes_connect_status).to eq(nil)
      expect(@album_itunes_status.stores_list).to eq(nil)
      expect(@album_itunes_status.itunes_created_at).to eq(nil)
      expect(@album_itunes_status.content_state_status).to eq(nil)
    end
  end

  describe "#update_success_xml" do
    before(:each) do
      @doc_xml = double(Object)

      @itunes_connect_status = "value1"
      @content_review_status = "value2"
      @on_store = "value3"
      @created = Date.today
      @status = "value4"

      content_status_info_node = double(attributes: {
        "content_review_status" => double(value: @content_review_status),
        "itunes_connect_status" => double(value: @itunes_connect_status)
      })

      upload_status_info_node = double(attributes: {
        "created" => double(value: @created),
        "status" => double(value: @status)
      })

      store_status_node = double(attributes: {
        "on_store" => double(value: @on_store)
      })

      allow(@doc_xml).to receive(:xpath).with("//content_status_info").and_return([content_status_info_node])
      allow(@doc_xml).to receive(:xpath).with("//upload_status_info").and_return([upload_status_info_node])
      allow(@doc_xml).to receive(:xpath).with("//store_status").and_return([store_status_node])

      @album_itunes_status = FactoryBot.create(:album_itunes_status)
    end


    it "sets the values of album_itunes_status" do
      @album_itunes_status.update_success_xml(@doc_xml)
      expect(@album_itunes_status.itunes_connect_status).to eq(@itunes_connect_status)
      expect(@album_itunes_status.content_review_status).to eq(@content_review_status)
      expect(@album_itunes_status.stores_list).to eq(@on_store)
      expect(@album_itunes_status.itunes_created_at).to eq(@created)
      expect(@album_itunes_status.content_state_status).to eq(@status)
    end

    scenario "should update stores_list=nil when stores_status tag is not present" do
      allow(@doc_xml).to receive(:xpath).with("//store_status").and_return([])
      expect(@album_itunes_status.update_success_xml(@doc_xml)).to be true
      expect(@album_itunes_status.stores_list).to eq(nil)
    end
  end
end
