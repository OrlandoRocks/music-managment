require 'rails_helper'

describe PayoutTransfer do
  describe ".by_tunecore_status" do
    it "only returns tunecore transfers with status" do
      pending_payout_transfer = create(
        :payout_transfer,
        tunecore_status: PayoutTransfer::SUBMITTED,
      )

      rejected_payout_transfer = create(
        :payout_transfer,
        tunecore_status: PayoutTransfer::REJECTED
      )

      expect(
        PayoutTransfer.by_tunecore_status(PayoutTransfer::SUBMITTED)
      ).to include(pending_payout_transfer)

      expect(
        PayoutTransfer.by_tunecore_status(PayoutTransfer::SUBMITTED)
      ).not_to include(rejected_payout_transfer)
    end
  end

  describe ".by_provider_status" do
    it "only returns provider transfers with status" do
      pending_payout_transfer = create(
        :payout_transfer, provider_status: PayoutTransfer::PENDING
      )

      completed_payout_transfer = create(
        :payout_transfer,   provider_status: PayoutTransfer::COMPLETED
      )

      expect(
        PayoutTransfer.by_provider_status(PayoutTransfer::PENDING)
      ).to include(pending_payout_transfer)

      expect(
        PayoutTransfer.by_provider_status(PayoutTransfer::PENDING)
      ).not_to include(completed_payout_transfer)
    end
  end

  describe ".rolled_back" do
    let!(:regular_transfer) { create(:payout_transfer, rolled_back: false) }
    let!(:old_transfer) { create(:payout_transfer, rolled_back: nil) }
    let!(:rolled_back_transfer) { create(:payout_transfer, rolled_back: true) }

    it "only returns payout transfers that have been rolled back" do
      rolled_back = PayoutTransfer.rolled_back
      expect(rolled_back.size).to eq(1)
      expect(rolled_back.first).to eq(rolled_back_transfer)
    end
  end

  describe ".recently_approved_transactions" do
    let!(:person_1) { create(:person, country: "Luxembourg") }
    let!(:person_2) { create(:person, country: "Luxembourg") }

    let!(:incomplete_transfer_by_person_1) { create(:payout_transfer, person_id: person_1.id) }

    let!(:old_transfer_by_person_1) {
      create(
        :payout_transfer,
        provider_status: :completed,
        tunecore_status: :approved,
        person_id: person_1.id,
        created_at: DateTime.now.ago(11.days),
        updated_at: DateTime.now.ago(11.days)
      )
    }

    let!(:recent_transfer_by_person_1) {
      create(
        :payout_transfer,
        provider_status: :completed,
        tunecore_status: :approved,
        person_id: person_1.id,
        created_at: DateTime.now.ago(9.days),
        updated_at: DateTime.now.ago(9.days)
      )
    }

    let!(:recent_transfer_by_person_2) {
      create(
        :payout_transfer,
        provider_status: :completed,
        tunecore_status: :approved,
        person_id: person_2.id,
        created_at: DateTime.now.ago(6.days),
        updated_at: DateTime.now.ago(6.days)
      )
    }

    it "includes approved transfers within a given number of days" do
      expect(PayoutTransfer.recently_approved_transactions(person_1.id, 10)).to include(recent_transfer_by_person_1)
      expect(PayoutTransfer.recently_approved_transactions(person_1.id, 10)).not_to include(old_transfer_by_person_1)
    end

    it "doesn't include incomplete transfers" do
      expect(PayoutTransfer.recently_approved_transactions(person_1.id, 10)).not_to include(incomplete_transfer_by_person_1)
    end

    it "includes completed transfers of the given person only" do
      expect(PayoutTransfer.recently_approved_transactions(person_2.id, 10)).to include(recent_transfer_by_person_2)
      expect(PayoutTransfer.recently_approved_transactions(person_2.id, 10)).not_to include(recent_transfer_by_person_1)
    end
  end

  context "the tunecore approval lifecyle" do
    describe "#submitted?" do
      it "is true if a transfer has been submitted to tunecore" do
        transfer = create(
          :payout_transfer, tunecore_status: PayoutTransfer::SUBMITTED
        )
        expect(transfer.submitted?).to eq true
      end

      it "is false if not submitted" do
        [
          create(:payout_transfer, tunecore_status: PayoutTransfer::APPROVED),
          create(:payout_transfer, tunecore_status: PayoutTransfer::REJECTED)
        ].each do |transfer|
          expect(transfer.submitted?).to eq false
        end
      end
    end

    describe "#rejected?" do
      it "is true if a tunecore admin has rejected a transaction" do
        transfer = create(
          :payout_transfer, tunecore_status: PayoutTransfer::REJECTED
        )
        expect(transfer.rejected?).to eq true
      end


      it "is false if it's been approved or submitted" do
        [
          create(:payout_transfer, tunecore_status: PayoutTransfer::APPROVED),
          create(:payout_transfer, tunecore_status: PayoutTransfer::SUBMITTED)
        ].each do |transfer|
          expect(transfer.rejected?).to eq false
        end
      end
    end

    describe "#approved?" do
      it "is false if not approved" do
        transfer = create(
          :payout_transfer, tunecore_status: PayoutTransfer::SUBMITTED
        )
        expect(transfer.approved?).to eq false
      end

      it "is true if approved" do
        transfer = create(
          :payout_transfer, tunecore_status: PayoutTransfer::APPROVED
        )
        expect(transfer.approved?).to eq true
      end
    end
  end

  context "the provider lifecyle" do
    describe "#completed?" do
      it "is false if not approved" do
        transfer = create(
          :payout_transfer,
          provider_status:  PayoutTransfer::PENDING,
          tunecore_status: PayoutTransfer::APPROVED
        )
        expect(transfer.completed?).to eq false
      end

      it "is true if approved" do
        transfer = create(
          :payout_transfer,
          provider_status:  PayoutTransfer::COMPLETED,
          tunecore_status: PayoutTransfer::APPROVED
        )
        expect(transfer.completed?).to eq true
      end
    end
  end

  describe "#mark_provider_as" do
    it "doesnt update provider_status if not in PROVIDER_STATUSES" do
      transfer = create(:payout_transfer)

      provider_status = transfer.provider_status
      expect(transfer.mark_provider_as("notgood")).to eq false
      expect(transfer.provider_status).to eq provider_status
    end

    it "updates provider_status" do
      transfer = create(:payout_transfer)

      expect(transfer.mark_provider_as(PayoutTransfer::DECLINED)).to eq true
      expect(transfer.provider_status).to eq PayoutTransfer::DECLINED
    end
  end

  describe "#dollar_amount" do
    it "should return the correct dollar amount including cents" do
      transfer = create(:payout_transfer, amount_cents: 9725)
      expect(transfer.dollar_amount).to eq Money.new(97_25)
    end

    it "should work for less than dollar amount" do
      transfer = create(:payout_transfer, amount_cents: 66)
      expect(transfer.dollar_amount).to eq Money.new(66)
    end
  end

  describe "#unsuccessful?" do
    it "should return true if either the tunecore status is rejected or the payoneer status is declined" do
      transfer = create(:payout_transfer, amount_cents: 9725, tunecore_status: PayoutTransfer::REJECTED, provider_status: PayoutTransfer::NONE)
      expect(transfer.unsuccessful?).to eq(true)
    end

    it "should return false if the tunecore status or payoneer status is not in a rejected/declined state" do
      transfer = create(:payout_transfer, amount_cents: 9725, tunecore_status: PayoutTransfer::APPROVED, provider_status: PayoutTransfer::COMPLETED)
      expect(transfer.unsuccessful?).to eq(false)
    end
  end

  describe "#amount_to_transfer" do
    it "should return the amount including fees for BANK ACCOUNT" do
      payout_transfer = create(:payout_transfer, amount_cents: 9725, fee_cents: 100, withdraw_method: "ACCOUNT")
      expect(payout_transfer.amount_to_transfer).to eq Money.new(98_25)
    end

    it "should return the amount including fees for CHECK" do
      payout_transfer = create(:payout_transfer, amount_cents: 9725, fee_cents: 100, withdraw_method: "PAPER_CHECK")
      expect(payout_transfer.amount_to_transfer).to eq Money.new(98_25)
    end

    it "should return the amount without fees ONLY for PAYPAL" do
      payout_transfer = create(:payout_transfer, amount_cents: 9725, fee_cents: 100, withdraw_method: "PAYPAL")
      expect(payout_transfer.amount_to_transfer).to eq Money.new(97_25)
    end
  end

  describe "#check_if_withdraw_method_changed" do
    let(:person) { create(:person) }
    let(:payout_provider) { create(:payout_provider, person: person) }

    it "Returns true if a payout transfer has a different withdrawal method than the last payout transfer" do
      existing_payout_transfer = create(:payout_transfer, payout_provider: payout_provider, amount_cents: 5000, withdraw_method: PayoutTransfer::ACCOUNT)

      expect(payout_provider.payout_transfers.size).to eq(1)

      new_payout_transfer = PayoutTransfer.new(
        payout_provider: payout_provider,
        amount_cents: 6500,
        tunecore_status: PayoutTransfer::SUBMITTED,
        provider_status: PayoutTransfer::NONE,
        transaction_type: "debit",
        currency: person.currency,
        withdraw_method: PayoutTransfer::PAYPAL,
      )
      payout_provider.payout_transfers << new_payout_transfer
      new_payout_transfer.save

      expect(payout_provider.payout_transfers.size).to eq(2)
      expect(new_payout_transfer.withdraw_method_changed).to eq(true)
    end

    it "returns false if a payout transfer has the same withdrawal method that the last payout transfer has" do
      existing_payout_transfer = create(:payout_transfer, payout_provider: payout_provider, amount_cents: 5000, withdraw_method: PayoutTransfer::ACCOUNT)

      expect(payout_provider.payout_transfers.size).to eq(1)

      new_payout_transfer = PayoutTransfer.new(
        payout_provider: payout_provider,
        amount_cents: 6500,
        tunecore_status: PayoutTransfer::SUBMITTED,
        provider_status: PayoutTransfer::NONE,
        transaction_type: "debit",
        currency: person.currency,
        withdraw_method: PayoutTransfer::ACCOUNT
      )
      payout_provider.payout_transfers << new_payout_transfer
      new_payout_transfer.save

      expect(payout_provider.payout_transfers.size).to eq(2)
      expect(new_payout_transfer.withdraw_method_changed).to eq(false)
    end
  end

  context "with auto-approval eligibility" do
    let!(:eligible_transfer) { create(:auto_approval_eligible_payout_transfer) }
    let!(:ineligible_transfer) { create(:payout_transfer) }

    describe ".with_auto_approval_eligible_transactions" do
      it "only returns payout transfers are eligible for auto-approval" do
        auto_approval_eligible_transfers = PayoutTransfer.with_auto_approval_eligible_transactions
        expect(auto_approval_eligible_transfers.size).to eq(1)
        expect(auto_approval_eligible_transfers.first).to eq(eligible_transfer)
      end
    end

    describe ".without_auto_approval_eligible_transactions" do
      it "only returns payout transfers are not eligible for auto-approval" do
        auto_approval_ineligible_transfers = PayoutTransfer.without_auto_approval_eligible_transactions
        expect(auto_approval_ineligible_transfers.size).to eq(1)
        expect(auto_approval_ineligible_transfers.first).to eq(ineligible_transfer)
      end
    end
  end

  describe "flagged?" do
    let!(:customer) { create(:person, :with_balance, amount: 150) }
    let(:payout_provider) { create(:payout_provider, person: customer) }
    let(:transfer) { create(:payout_transfer, payout_provider: payout_provider, amount_cents: 5000) }

    it "returns false when the person's acount is active" do
      expect(transfer.flagged?).to eq(false)
    end

    it "returns true when the person's acount is marked suspicious" do
      customer.mark_as_suspicious!(create(:note), "Credit Card Fraud")
      expect(transfer.flagged?).to eq(true)
    end

    it "returns true when the withdraw method is changed from last transfer" do
      allow(transfer).to receive(:withdraw_method_changed).and_return(true)
      expect(transfer.flagged?).to eq(true)
    end
  end
end
