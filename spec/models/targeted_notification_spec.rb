require "rails_helper"

describe TargetedNotification do
  describe "#new_global_notifications" do
    it "returns global admin-created notifications that DO NOT have an associated notification" do
      admin_notification_with_notification = create(:targeted_notification)
      notification = create(:notification, targeted_notification: admin_notification_with_notification )
      person = notification.person

      admin_notification_without_notification = create(:targeted_notification)

      expect(TargetedNotification.new_global_notifications(person))
        .to include(admin_notification_without_notification)

      expect(TargetedNotification.new_global_notifications(person))
        .not_to include(admin_notification_with_notification)

      expect(TargetedNotification.new_global_notifications(person).count)
        .to equal(1)
    end
  end

  describe "#add_people_from_csv" do
    before do
      @people = []
      4.times do
        @people << create(:person)
      end

      @csv = @people.collect(&:id).join("\r\n")

      @file = double("file")
      allow(@file).to receive(:read).and_return(@csv)

      @notification = create(:targeted_notification, global: false)
    end

    it "creates notifications for people in the csv" do
      expect {
        @notification.add_people_from_csv(@file)
      }.to change(Notification, :count).by(4)
    end

    it "does not create notifications for people that have allready been added to the notification" do
      notification = Notification.build_from_targeted_notification(@notification, @people.first.id)
      notification.save!

      expect {
        @notification.add_people_from_csv(@file)
      }.to change(Notification, :count).by(3)
    end

    it "returns a list of new notifications" do
      expect {
        @notifications = @notification.add_people_from_csv(@file)
      }.to change(Notification, :count).by(4)
      expect(@notifications.count).to eq(4)
    end

    it "returns false if the targeted notification is global" do
      @notification.global = true
      @notification.save

      expect(@notification.add_people_from_csv(@file)).to be_falsey
    end

    it "does not add invalid person ids" do
      @csv << "\r\n"
      @csv << "123456"

      expect {
        @notification.add_people_from_csv(@file)
      }.to change(Notification, :count).by(4)
    end
  end

  it "destroys all notifications associated on destroy" do
    @tn = create(:targeted_notification)
    @n = create(:notification, targeted_notification: @tn)

    expect {
      @tn.destroy
    }.to change(Notification, :count).by(-1)
  end
end
