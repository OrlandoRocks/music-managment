# frozen_string_literal: true

require "rails_helper"

describe BatchBalanceAdjustment do
  before(:each) do
    # copy csv to the upload path
    @ingestion_filename = 'songwriter_payable_client_listing.csv'
    @payment_held_filename = 'songwriter_payment_held_clients.csv'
    @bad_payment_held_filename = 'bad_songwriter_payment_held_clients.csv'
    @bad_ingestion_filename = 'bad_songwriter_payable_client_listing.csv'

    dest = BATCH_BALANCE_ADJUSTMENTS_UPLOAD_PATH
    file_source = File.expand_path(File.dirname(__FILE__)) + '/../files/'
    FileUtils.cp file_source + @ingestion_filename, dest
    FileUtils.cp file_source + @payment_held_filename, dest
    FileUtils.cp file_source + @bad_ingestion_filename, dest
    FileUtils.cp file_source + @bad_payment_held_filename, dest
  end

  describe ".ingest_file" do
    before :each do
      expect(BatchBalanceAdjustment.count).to eq(0)
    end

    it "should create batch adjustment details" do
      expect(BatchBalanceAdjustment.count).to eq(0)
      batch_adjustment = BatchBalanceAdjustment.ingest_file(@ingestion_filename)
      details = batch_adjustment.details
      expect(details.count).to eq(3)

      detail1 = details.detect { |d| d.person_id == 1 }
      detail2 = details.detect { |d| d.person_id == 2 }
      detail3 = details.detect { |d| d.person_id == 481_355 }

      expect(detail1.current_balance).to eq(114.18)
      expect(detail2.current_balance).to eq(0.73)
      expect(detail3.current_balance).to eq(29.47)
    end

    it "should return a BatchBalanceAdjustment object" do
      expect(BatchBalanceAdjustment.ingest_file(@ingestion_filename)).to be_kind_of(BatchBalanceAdjustment)
    end

    it "should rollback when any entry in the ingestion failed to import" do
      expect { BatchBalanceAdjustment.ingest_file(@bad_ingestion_filename) }.to raise_error
      expect(BatchBalanceAdjustment.find_by(ingested_filename: @bad_ingestion_filename)).to be_nil
    end

    it "should not allow duplicate ingestion of the same file" do
      BatchBalanceAdjustment.ingest_file(@ingestion_filename)
      expect { BatchBalanceAdjustment.ingest_file(@ingestion_filename) }.to raise_error
      expect(BatchBalanceAdjustment.where(ingested_filename: @ingestion_filename).count).to eq(1)
    end

    it "should capture hold payment and hold type" do
      batch_adjustment = BatchBalanceAdjustment.ingest_file(@payment_held_filename)
      details = batch_adjustment.details
      expect(details.collect(&:hold_payment)).to eq(%w(Yes Yes Yes))
      expect(details.collect(&:hold_type)).to eq(['Negative Balance', 'Missing Tax', 'Negative Balance'])
    end

    it "should fail when hold type is invalid" do
      expect { BatchBalanceAdjustment.ingest_file(@bad_payment_held_filename) }.to raise_error
    end
  end

  describe "callbacks" do
    let!(:batch_adjustment) { BatchBalanceAdjustment.ingest_file(@ingestion_filename) }

    before do
      BatchBalanceAdjustment.delete_all
      allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(true)
    end

    it "schedules tax withholding task when earnings batch status transitions to posted" do
      batch_adjustment = BatchBalanceAdjustment.ingest_file(@ingestion_filename)
      expect(TaxWithholdings::MassAdjustmentsBatchWorker)
        .to receive(:perform_async)
      batch_adjustment.update(status: "posted")
    end

    it "does not trigger tax batch tax withholding job when for status transitions to a non 'posted state" do
      batch_adjustment = BatchBalanceAdjustment.ingest_file(@ingestion_filename)
      expect(TaxWithholdings::MassAdjustmentsBatchWorker)
        .not_to receive(:perform_async)
      batch_adjustment.update(status: "incomplete")
    end

    xit "summarizes earnings when the batch is marked posted" do
      batch_adjustment = BatchBalanceAdjustment.ingest_file(@ingestion_filename)
      expect(TaxBlocking::BatchPersonEarningsSummarizationWorker)
        .to receive(:perform_async).with(batch_balance_adjustment_ids: batch_adjustment.id)
      batch_adjustment.update(status: "posted")
    end

    xit "doesn't summarize earnings when the batch is marked incomplete" do
      batch_adjustment = BatchBalanceAdjustment.ingest_file(@ingestion_filename)
      expect(TaxBlocking::BatchPersonEarningsSummarizationWorker).not_to receive(:perform_async)
      batch_adjustment.update(status: "incomplte")
    end

    xit "doesn't summarize earnings when the feature flag 'advanced_tax_blocking' is not set" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking).and_return(false)
      batch_adjustment = BatchBalanceAdjustment.ingest_file(@ingestion_filename)
      expect(TaxBlocking::BatchPersonEarningsSummarizationWorker).not_to receive(:perform_async)
      batch_adjustment.update(status: "posted")
    end
  end
end
