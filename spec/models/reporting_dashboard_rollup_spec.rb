require "rails_helper"

describe ReportingDashboardRollup do
  describe ".calculate_daily_values" do
    it "creates a rollup for every country website" do
      ReportingDashboardRollup.calculate_daily_values(Date.today)

      expect(ReportingDashboardRollup.count).to eq CountryWebsite.count
    end

    it "updates the attributes for each created rollup" do
      ReportingDashboardRollup.calculate_daily_values(Date.today)
      country_website_id = CountryWebsite.first.id

      rollup = ReportingDashboardRollup.where(country_website_id: country_website_id).last

      expect(rollup.new_dist_first_purchase).to eq 0.00
      expect(rollup.new_dist_not_first_purchase).to eq 0.00
      expect(rollup.returning_dist).to eq 0.00
      expect(rollup.renewals).to eq 0.00
      expect(rollup.pub_first_purchase).to eq 0.00
      expect(rollup.pub_not_first_purchase).to eq 0.00
    end
  end
end
