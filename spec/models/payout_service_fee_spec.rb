require "rails_helper"

describe PayoutServiceFee do
  it "should have an amount greater than 0" do
    payout_service_fee = PayoutServiceFee.new(:fee_type => PayoutServiceFee::TYPES.first, :amount => -1.00)
    expect(payout_service_fee.valid?).to be_falsey
    expect(payout_service_fee.errors.messages[:amount].size).to eq 1
  end
  
  it "should have a vaild fee type" do
    payout_service_fee = PayoutServiceFee.new(:fee_type => 'not_a_real_fee_type', :amount => 1.00)
    expect(payout_service_fee.valid?).to be_falsey
    expect(payout_service_fee.errors.messages[:fee_type].size).to eq 1
  end
 
  context "creating a new fee" do 
    before(:each) do
      @payout_service_fee = TCFactory.create(:payout_service_fee, {:fee_type => "eft_service", :amount => 2.00})
      expect(@payout_service_fee.archived_at).to be_nil
    end

    it "should archive a fee if a new one of the same type is added" do
      new_payout_service_fee = TCFactory.create(:payout_service_fee, {:fee_type => "eft_service", :amount => 1.25})
      expect(@payout_service_fee.reload.archived_at).not_to be_nil
      expect(new_payout_service_fee.archived_at).to be_nil
    end

    it "should retrieve the lastest non-archived service fee" do
      TCFactory.create(:payout_service_fee, {:fee_type => "eft_service", :amount => 1.19})
      TCFactory.create(:payout_service_fee, {:fee_type => "eft_service", :amount => 2.98})
      expect(PayoutServiceFee.current("eft_service").amount).to eq(2.98)
    end
  end
end 
