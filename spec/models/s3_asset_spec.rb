require "rails_helper"

describe S3Asset do
  describe ".set_download_filename" do
    it "sends the bucket, key, and options to the aws sdk wrapper" do
      bucket_name = "bucket_name"
      key = "key"
      filename = "test.xml"
      options = {
        content_disposition: "attachment; filename=\"#{filename}\""
      }

      expect(AwsWrapper::S3).to receive(:update_options).with(
        bucket: bucket_name,
        key: key,
        options: options
      )

      S3Asset.set_download_filename(bucket_name, key, filename)
    end
  end

  describe "#get_put_presigned_url" do
    it "returns presigned url and key/bucket for the asset" do
      s3 = double(:s3)
      s3_bucket = double(:s3_bucket)
      s3_object = double(:s3_object)

      expect(Aws::S3::Resource).to receive(:new).and_return(s3)
      expect(s3).to receive(:bucket).with("bucket_name").and_return(s3_bucket)
      expect(s3_bucket).to receive(:object).and_return(s3_object)
      expect(s3_object).to receive(:presigned_url).with(:put, expires_in: 3600).and_return("presigned_url")

      allow(Time).to receive(:now).and_return(Time.new(2020, 6, 11, 0, 0, 0, '+00:00'))

      resp = S3Asset.get_put_presigned_url("bucket_name", "file_name", 10)

      expect(resp[:key_name]).to eq("10/1591833600000-file_name")
      expect(resp[:bucket_name]).to eq("bucket_name")
      expect(resp[:presigned_url]).to eq("presigned_url")
    end
  end
end
