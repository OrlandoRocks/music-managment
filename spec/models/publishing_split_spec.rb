require "rails_helper"

describe PublishingSplit do
  describe "when saving" do
    before :each do
      person = TCFactory.build_person
      @composer = TCFactory.build_composer(:person => person)
      album = TCFactory.build_album(:person => person, :payment_applied => true, :finalized_at => Time.now)
      song = TCFactory.build_song(:album => album)
      composition = TCFactory.build_composition(:song => song)
      @split = TCFactory.create(:publishing_split, :composition => composition, :percent => 23.15)
    end

    it "should save percent in two decimals" do
      @split.reload
      expect(@split.percent).to eq(23.15)
      @split.percent = 100.00
      @split.save!
      @split.reload
      expect(@split.percent).to eq(100.00)
    end

    it "should set composition status" do
      expect(@split.composition.state).to eq('split_submitted')
    end

    it "should return error when total percentage is more than 100%" do
      split = TCFactory.build(:publishing_split, :percent => 200)
      expect(split.valid?).to be_falsey
      expect(split.errors).not_to be_empty
    end

    it "should set updated_by" do
      PaperTrail.request.whodunnit = '12345'
      split = TCFactory.create(:publishing_split, :percent => 50)
      expect(split.updated_by).to eq('12345')
    end
  end

  describe "::admin_query" do
    before :each do
      @person = TCFactory.build_person
      @album = TCFactory.build_album(person: @person, finalized_at: Time.now, is_deleted: false)
      @composer = TCFactory.build_composer(person: @person)
      @song = TCFactory.build_song(name: "Test", album: @album)
      @composition = TCFactory.build_composition(song: @song)
      @publishing_split = TCFactory.build_publishing_split(composition: @composition, composer: @composer)
      @upc = TCFactory.build_upc(:number=>"12875281", :upcable=>@album)
    end

    it "should return correct split when searching by Account ID" do
      @searched_items = PublishingSplit.admin_query(query: "Account ID", search: @person.id, per_page: 10, order: nil, page: nil)
      expect(@searched_items).to include(@publishing_split)
    end

    it "should return correct split when searching by Account Email" do
      @searched_items = PublishingSplit.admin_query(query: "Account Email", search: @person.email, per_page: 10, order: nil, page: nil)
      expect(@searched_items).to include(@publishing_split)
    end

    it "should return correct split when searching by Account Name" do
      @searched_items = PublishingSplit.admin_query(query: "Account Name", search: @person.name, per_page: 10, order: nil, page: nil)
      expect(@searched_items).to include(@publishing_split)
    end

    it "should return correct split when searching by Composition ID" do
      @searched_items = PublishingSplit.admin_query(query: "Composition ID", search: @composition.id, per_page: 10, order: nil, page: nil)
      expect(@searched_items).to include(@publishing_split)
    end

    it "should return correct split when searching by Composition Name" do
      @searched_items = PublishingSplit.admin_query(query: "Composition Name", search: @composition.name, per_page: 10, order: nil, page: nil)
      expect(@searched_items).to include(@publishing_split)
    end

    it "should return correct split when searching by Release ID" do
      @searched_items = PublishingSplit.admin_query(query: "Release ID", search: @album.id, per_page: 10, order: nil, page: nil)
      expect(@searched_items).to include(@publishing_split)
    end

    it "should return correct split when searching by Release UPC" do
      @searched_items = PublishingSplit.admin_query(query: "Release UPC", search: @upc.number, per_page: 10, order: nil, page: nil)
      expect(@searched_items).to include(@publishing_split)
    end

    it "should return correct split when searching by Release Name" do
      @searched_items = PublishingSplit.admin_query(query: "Release Name", search: @album.name, per_page: 10, order: nil, page: nil)
      expect(@searched_items).to include(@publishing_split)
    end

  end

  describe "#for_unknown_coriter?" do
    let(:publishing_split) { create(:publishing_split) }

    subject { publishing_split.for_unknown_cowriter? }

    context "when the writer is not an unknown cowriter" do
      before { allow(publishing_split).to receive(:for_unknown_cowriter?).and_return(false) }

      it { should be_falsey }
    end

    context "when the writer is an unknown cowriter" do
      before { allow(publishing_split).to receive(:for_unknown_cowriter?).and_return(true) }

      it { should be_truthy }
    end
  end
end
