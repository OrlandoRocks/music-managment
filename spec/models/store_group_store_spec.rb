require "rails_helper"

describe StoreGroupStore do

  describe "relationships" do

    describe "store" do
      it "should not raise error" do
        store_group_store = TCFactory.create(:store_group_store)
        expect {
          store_group_store.store
        }.not_to raise_error
      end
    end

    describe "store_group" do
      it "should not raise error" do
        store_group_store = TCFactory.create(:store_group_store)
        expect {
          store_group_store.store_group
        }.not_to raise_error
      end
    end

  end

end
