require "rails_helper"

describe TaxInfo do
  describe "#tax_id=" do
    context "when there is no existing tax_id" do
      before(:each) do
        @tax_id = '908881234'
        @composer = build_stubbed(:composer)
        @tax_info = build(:tax_info, composer: @composer)
      end

      it "assigns encrypted_tax_id and salt" do
        @tax_info.tax_id = @tax_id

        expect(@tax_info.encrypted_tax_id).to be_present
        expect(@tax_info.salt).to be_present
        expect(@tax_info.encrypted_tax_id).not_to eq(@tax_id)
      end
    end

    context "with an existing tax_id" do
      before(:each) do
        @tax_id = '908881234'
        @composer = build_stubbed(:composer)
        @tax_info = build(:tax_info, tax_id: @tax_id, composer: @composer)
      end

      it "updates encrypted_tax_id and salt" do
        new_tax_id = '123456789'
        old_salt = @tax_info.salt
        old_encrypted_tax_id = @tax_info.encrypted_tax_id

        @tax_info.tax_id = new_tax_id

        expect(@tax_info.encrypted_tax_id).to be_present
        expect(@tax_info.encrypted_tax_id).not_to eq(old_encrypted_tax_id)
        expect(@tax_info.salt).to be_present
        expect(@tax_info.salt).not_to eq(old_salt)
        expect(@tax_info.tax_id).to eq(new_tax_id)
      end

      it "creates a new version" do
        with_versioning do
          expect { @tax_info.update(tax_id: "987654321") }.to change { @tax_info.versions.count }.by(1)
        end
      end
    end
  end

  describe "#tax_id" do
    before(:each) do
      @tax_id = '908881234'
      @composer = build_stubbed(:composer)
      @tax_info = build(:tax_info, composer: @composer)
    end

    context "when there is an encrypted_tax_id" do
      it "returns the decrypted encrypted_tax_id" do
        @tax_info.tax_id = @tax_id

        expect(@tax_info.tax_id).to eq(@tax_id)
      end
    end

    context "when there is no encrypted_tax_id" do
      it "returns nil" do
        @tax_info.tax_id = nil

        expect(@tax_info.tax_id).to be_nil
      end
    end
  end
end
