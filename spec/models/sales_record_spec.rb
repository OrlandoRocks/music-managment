require "rails_helper"

describe SalesRecord do
  describe '.get_album_release_details' do
    let(:person)        { create(:person) }
    let(:genre)         { create(:genre, name: "Instrumental") }
    let(:artist)        { create(:artist) }
    let(:album) do
      create(:album, person: person, primary_genre_id: genre.id, creatives: [
        { "role" => "primary_artist", "name" => artist.name }
      ])
    end
    let(:song)         { create(:song, album: album) }

    let(:single) do
      create(:single, person: person, primary_genre_id: genre.id, creatives: [
        { "role" => "primary_artist", "name" => artist.name }
      ])
    end

    let(:ringtone) do
      create(:single, person: person, creatives: [
        { "role" => "primary_artist", "name" => artist.name }
      ])
    end

    context 'when the release is album' do
      it 'should return release details of a album' do
        release_details = SalesRecord.get_album_release_details(person, album, {related_type: 'Album'})
        expect(release_details.album_name).to eq(album.name)
        expect(release_details.release_type).to eq(album.album_type)
        expect(release_details.genre).to eq(genre.name)
        expect(release_details.artist_name).to eq(artist.name)
        expect(release_details.upc_number).to eq(album.upc.number)
        expect(release_details.optional_upc_number).to eq(nil)
      end

      it 'should not return release details of a album' do
        person1 = create(:person)
        release_details = SalesRecord.get_album_release_details(person1, album, {related_type: 'Album'})
        expect(release_details).to eq(nil)
      end
    end

    context 'when the release is single' do
      it 'should return release details of a single release' do
        srm = create(:sales_record_master, period_sort: Date.today.beginning_of_month - 4.months)
        sales_record = create(:sales_record, sales_record_master: srm, related: single.song, person: person, quantity: 1)
        release_details = SalesRecord.get_album_release_details(person, single, {related_type: 'Single'})
        expect(release_details.album_name).to eq(single.name)
        expect(release_details.release_type).to eq(single.album_type)
        expect(release_details.genre).to eq(genre.name)
        expect(release_details.artist_name).to eq(artist.name)
        expect(release_details.upc_number).to eq(single.upc.number)
        expect(release_details.optional_upc_number).to eq(nil)
      end
    end

    context 'when the release is ringtone' do
      it 'should return release details of a ringtone' do
        srm = create(:sales_record_master, period_sort: Date.today.beginning_of_month - 4.months)
        sales_record = create(:sales_record, sales_record_master: srm, related: ringtone.song, person: person, quantity: 1)
        release_details = SalesRecord.get_album_release_details(person, ringtone, {related_type: 'Ringtone'})
        expect(release_details.name).to eq(ringtone.name)
        expect(release_details.release_type).to eq(ringtone.album_type)
        expect(release_details.upc_number).to eq(ringtone.upc.number)
      end
    end
  end
end