require "rails_helper"

describe YoutubeRoyaltiesNotification do
  describe "validations" do
    it "should set fields" do
      person = build(:person)
      intake_record = build(:you_tube_royalty_intake, person: person)
      notification = YoutubeRoyaltiesNotification.new(person: person, notification_item: intake_record)

      expect(notification).to be_valid
      expect(notification.url).not_to be_blank
      expect(notification.title).not_to be_blank
      expect(notification.link_text).not_to be_blank
      expect(notification.person).to eq(person)
    end
  end

  describe ".create_notifications" do
    context "there is one intake_record with a notification and 2 without" do
      it "should create 2 notifications for intake_record2 and intake_record3" do
        person = create(:person)
        intake_record1 = create(:you_tube_royalty_intake, person: person)
        _notification = create(:youtube_royalties_notification, notification_item: intake_record1, person: person)
        _intake_record2 = create(:you_tube_royalty_intake, person: person)
        _intake_record3 = create(:you_tube_royalty_intake, person: person)

        expect {
          YoutubeRoyaltiesNotification.create_notifications
        }.to change(YoutubeRoyaltiesNotification, :count).by(2)
      end
    end
  end
end
