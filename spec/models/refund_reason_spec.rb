require 'rails_helper'

RSpec.describe RefundReason, type: :model do
  context "Validations" do
    it { should validate_presence_of(:reason) }
  end
end
