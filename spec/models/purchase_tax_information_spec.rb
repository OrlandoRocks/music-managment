require 'rails_helper'

RSpec.describe PurchaseTaxInformation, type: :model do
  before(:each) do
    @product = TCFactory.build_ad_hoc_album_product
    @album = Album.first
    @person = @album.person
    FeatureFlipper.update_feature(:vat_tax, 100, '')
  end

  it "should create entries on purchase recalculate" do
    purchase = Purchase.recalculate_or_create(@person, @product, @album)
    expect(purchase.purchase_tax_information).to be_present
  end

  it "should not create purchase tax information if purchase is not saved" do
    purchase = Purchase.new(product: @product, person: @person)
    expect(purchase.valid?).to eq(false)
    expect(purchase.purchase_tax_information).to eq(nil)
  end
end
