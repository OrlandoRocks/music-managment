require 'rails_helper'

RSpec.describe TierPerson, type: :model do
  describe "relationships" do
    subject { build(:tier_person) }

    it { should belong_to(:tier) }
    it { should belong_to(:person) }
  end

  describe "validations" do
    let(:subject) { build(:tier_person) }

    it { should validate_presence_of(:status) }

    context "when status is active" do
      before { subject.status = 'active' }

      it { should validate_uniqueness_of(:tier_id).scoped_to(:person_id) }
    end
  end
end
