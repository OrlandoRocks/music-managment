require "rails_helper"

describe Copyright do
  let!(:copyright) { build(:copyright) }

  context "#valid?" do
    it "should be valid with all relevant fields" do
      expect(copyright).to be_valid
    end
  end

  context "invalid" do
    context "year" do
      it "should return invalid if composition is false" do
        copyright.composition = false
        expect(copyright).to_not be_valid
      end

      it "should return invalid if recording is false" do
        copyright.recording = false
        expect(copyright).to_not be_valid
      end
    end

    context "copyrightable" do
      it "should return invalid if associated copyrightable is missing" do
        copyright.copyrightable = nil

        expect(copyright).to_not be_valid
      end
    end
  end
end
