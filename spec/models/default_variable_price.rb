require "rails_helper"

describe DefaultVariablePrice do

  describe "creating a new object" do

    it "should build fine from the factory" do
      expect {
        object = TCFactory.create(:default_variable_price)
      }.not_to raise_error
    end

    it "should throw error if does not have a variable_price_store" do
      expect {
        default_variable_price = TCFactory.create(:default_variable_price,
          :variable_price_store => nil
        )
      }.to raise_error
    end

    it "should throw error if does not have a store " do
      expect {
        default_variable_price = TCFactory.create(:default_variable_price,
          :store => nil
        )
      }.to raise_error
    end

    it "should throw error if the variable_price_store id is not valid for the current store" do
      expect {
        default_variable_price = TCFactory.create(:default_variable_price,
          :variable_price_store => 1000000
        )
      }.to raise_error
    end

  end

end

