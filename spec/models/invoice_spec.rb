require "rails_helper"

describe Invoice, "factory() receiving locked relateds" do
  let!(:person) {
    create(:person, name: "trevor", email: "trevor@protocool.net")
  }
  let!(:album) { create(:album, person: person) }
  let!(:purchase) { Product.add_to_cart(album.person, album) }

  it "should not create a new invoice record" do
    expect { Invoice.factory(nil, purchase) rescue nil }.not_to change(Invoice, :count)
  end
end

describe ".factory" do
  before(:each) do
    TCFactory.build_ad_hoc_album_product
  end

  let!(:person) {
    create(:person, name: "trevor", email: "trevor@protocool.net")
  }
  let!(:album) { create(:album, person: person) }
  let!(:purchase) { Product.add_to_cart(album.person, album) }

  context "with incorrect parameters" do
    it "without purchase should not change invoice count" do
      expect { Invoice.factory(person, nil) rescue nil }.not_to change(Invoice, :count)
    end
  end

  context "with already attached invoice" do
    it "should not change invoice count" do
      invoice = Invoice.factory(person, purchase)
      expect { Invoice.factory(person, purchase) rescue nil }.not_to change(Invoice, :count)
    end

    it "should cause an error" do
      expect(Airbrake).to receive(:notify)
      invoice = Invoice.factory(person, purchase)
      Invoice.factory(person, purchase)
    end
  end
end

describe ".factory" do
  before(:each) do
    TCFactory.build_ad_hoc_album_product
    @person = TCFactory.build_person(
      name: "trevor", email: "trevor@protocool.net",
    )
    @album = TCFactory.build_album(person: @person)
    @purchase = Product.add_to_cart(@album.person, @album)
  end

  context "with incorrect parameters" do
    it "without person should rescue and not change invoice count" do
      expect(Airbrake).to receive(:notify)
      expect { Invoice.factory(nil, @purchase) }.not_to change(Invoice, :count)
    end

    it "without purchase should not change invoice count" do
      expect { Invoice.factory(@person, nil) rescue nil }.not_to change(Invoice, :count)
    end
  end

  context "with already attached invoice" do
    it "should not change invoice count" do
      invoice = Invoice.factory(@person, @purchase)
      initial_invoice_qty = Invoice.count
      allow(@purchase).to receive(:attach_to_invoice).and_raise(StandardError)
      expect(Invoice.count).to eq(initial_invoice_qty)
    end

    it "should notify airbrake" do
      expect(Airbrake).to receive(:notify)
      invoice = Invoice.factory(@person, @purchase)
      Invoice.factory(@person, @purchase)
    end
  end
end

describe Invoice, "factory() receiving acceptable purchases" do
  before(:each) do
    TCFactory.build_ad_hoc_album_product
    @person = TCFactory.build_person(
      name: "trevor", email: "trevor@protocool.net",
    )
    @album = TCFactory.build_album(person: @person)
    @video = TCFactory.build_video(person: @person)
    @purchase = Product.add_to_cart(@album.person, @album)
  end

  it "should create a new invoice record" do
    expect do
      Invoice.factory(@person, @purchase)
    end.to change(Invoice, :count).by(1)
  end

  it "should notify purchases that they are attached to the invoice" do
    expect(@purchase).to receive(:attach_to_invoice).and_return(true)
    Invoice.factory(@person, @purchase)
  end

  it "should should note attached purchases in purchases association" do
    expect(Invoice.factory(@person, @purchase).purchases).to include(@purchase)
  end

  it "should not be settled" do
    expect(Invoice.factory(@person, @purchase)).not_to be_settled
  end

  it "should have a USD currency" do
    TCFactory.build_ad_hoc_album_product
    @person = TCFactory.build_person
    @album = TCFactory.build_album(person: @person)
    @video = TCFactory.build_video(person: @person)
    @purchase = Product.add_to_cart(@album.person, @album)
    @invoice = Invoice.factory(@person, @purchase)
    expect(@invoice.currency).to eq("USD")
  end

  context "from country with pegged rate" do
    it "should have a pegged_rate_id" do
      allow_any_instance_of(ApiLogger::InitLoggerService).to receive(:call).and_return(true)
      @indian_person = create(:person, name: "rodney", email: "rodney@india.net", country: "IN", dormant: true)
      @indian_album = create(:album, person: @indian_person)
      @indian_purchase = Product.add_to_cart(@indian_album.person, @indian_album)

      invoice = Invoice.factory(@indian_person, @indian_purchase)
      expect(invoice.foreign_exchange_pegged_rate).to_not be(nil)
    end
    it "should have current pegged rate as pegged_rate_id" do
      @indian_person = create(:person, name: "rodney", email: "rodney@india.net", country: "IN", dormant: true)
      @indian_album = create(:album, person: @indian_person)
      @indian_purchase = Product.add_to_cart(@indian_album.person, @indian_album)

      invoice = Invoice.factory(@indian_person, @indian_purchase)
      current_rate = Country.find_by(iso_code: "IN").current_pegged_rate
      expect(invoice.foreign_exchange_pegged_rate).to eq(current_rate)
    end
  end
  context "from country without pegged rate" do
    it "should not have pegged_rate_id" do
      invoice = Invoice.factory(@person, @purchase)
      expect(invoice.foreign_exchange_pegged_rate).to be(nil)
    end
  end
end

describe Invoice, "#set_person_analytic_if_first_time_distro" do
  it "should return a person_analytic record for first distro for all countries" do
    CountryWebsite.all.each do |_cw|
      person = create(:person, country_website: CountryWebsite.first)
      # First distro
      create_distribution_invoice(person)
      # Second Distro
      create_distribution_invoice(person)

      person.reload
      expect(person.invoices.size).to eq(2)

      first_invoice = person.invoices.first
      last_invoice = person.invoices.last

      first_invoice.update(settled_at: Time.now)
      last_invoice.update(settled_at: Time.now + 1.second)

      expect(first_invoice.set_person_analytic_if_first_time_distro.class.name).to eq("PersonAnalytic")
      expect(last_invoice.set_person_analytic_if_first_time_distro).to be(false)
      expect(person.analytic_by_metric("first_distribution_invoice_id").metric_value).to be(first_invoice.id)
    end
  end

  def create_distribution_invoice(person)
    country_code = person.country_domain
    distribution_product_id = Product::PRODUCT_COUNTRY_MAP[country_code][:album_distribution].first
    album = create(:album, person: person)
    purchase = create(:purchase, person: person, product_id: distribution_product_id, related: album)
    create(:invoice, purchases: [purchase], person: person)
  end

  def create_plan_invoice(person, plan_name)
    plan = Plan.find_by(name: plan_name)
    product = plan.product_by_person(person)
    purchase = create(:purchase, person_id: person.id, related: product, product: product)
    create(:invoice, purchases: [purchase], person: person)
  end

  it "should return a person_analytic record for first paid Plan purchase for all countries" do
    CountryWebsite.all.each do |_cw|
      person = create(:person, country_website: CountryWebsite.first)
      # First distro rising_artist
      create_plan_invoice(person, "rising_artist")
      # Second Distro breakout_artist
      create_plan_invoice(person, "breakout_artist")

      person.reload
      expect(person.invoices.size).to eq(2)

      first_invoice = person.invoices.first
      last_invoice = person.invoices.last

      first_invoice.update(settled_at: Time.now)
      last_invoice.update(settled_at: Time.now + 1.second)

      expect(first_invoice.set_person_analytic_if_first_time_distro.class.name).to eq("PersonAnalytic")
      expect(last_invoice.set_person_analytic_if_first_time_distro).to be(false)
      expect(person.analytic_by_metric("first_distribution_invoice_id").metric_value).to be(first_invoice.id)
    end
  end

  it "should not return a person_analytic record for first free Plan purchase for all countries" do
    CountryWebsite.all.each do |_cw|
      person = create(:person, country_website: CountryWebsite.first)
      # First distro new_artist
      create_plan_invoice(person, "new_artist")

      person.reload
      expect(person.invoices.size).to eq(1)
      person.invoices.first.update(settled_at: Time.now)

      expect(person.invoices.first.set_person_analytic_if_first_time_distro).to be false
    end
  end

  it "should return a person_analytic record for first paid Plan purchase if a user upgrade from free for all countries" do
    CountryWebsite.all.each do |_cw|
      person = create(:person, country_website: CountryWebsite.first)
      # First distro new_artist
      create_plan_invoice(person, "new_artist")
      # Second Distro rising_artist
      create_plan_invoice(person, "rising_artist")
      # Third Distro breakout_artist
      create_plan_invoice(person, "breakout_artist")

      person.reload
      expect(person.invoices.size).to eq(3)

      first_invoice = person.invoices.first
      second_invoice = person.invoices.second
      last_invoice = person.invoices.last

      first_invoice.update(settled_at: Time.now)
      second_invoice.update(settled_at: Time.now)
      last_invoice.update(settled_at: Time.now + 1.second)

      expect(first_invoice.set_person_analytic_if_first_time_distro).to be false
      expect(second_invoice.set_person_analytic_if_first_time_distro.class.name).to eq("PersonAnalytic")
      expect(last_invoice.set_person_analytic_if_first_time_distro).to be false
      expect(person.analytic_by_metric("first_distribution_invoice_id").metric_value).to be(second_invoice.id)
    end
  end
end

describe Invoice, "#contains_distribution_product?" do
  it "should return true if any purchases are related to an album" do
    person = create(:person)

    # First distro
    TCFactory.generate_album_purchase(person)
    expect(person.invoices.first.contains_distribution_product?).to be_truthy
  end
end

describe Invoice, "receiving a partial settlement" do
  before(:each) do
    TCFactory.build_ad_hoc_album_product
    @person = TCFactory.build_person(
      name: "trevor", email: "trevor@protocool.net",
    )
    @album = TCFactory.build_album(person: @person)
    @purchase = Product.add_to_cart(@album.person, @album)
    @invoice = Invoice.factory(@person, @purchase)
  end

  it "should not notify the purchases" do
    allow(@invoice).to receive(:purchases).and_return([@purchase])
    @invoice.settlement_received(@person, 1)
  end

  it "should store the received amount as a new InvoiceSettlement" do
    expect { @invoice.settlement_received(@person, 1) }.to change(InvoiceSettlement, :count).by(1)
  end

  it "should put the newly created InvoiceSettlement into the invoice_settlements assocation" do
    @invoice.invoice_settlements.reload
    expect { @invoice.settlement_received(@person, 1) }.to change(@invoice.invoice_settlements, :length).by(1)
  end

  it "should still be can_settle?" do
    @invoice.settlement_received(@person, 1)
    expect(@invoice).not_to be_can_settle
  end
end

describe Invoice, "receiving a settlement with a duplicate source argument" do
  before(:each) do
    TCFactory.build_ad_hoc_album_product
    @person = TCFactory.build_person(
      name: "trevor", email: "trevor@protocool.net",
    )
    @album = TCFactory.build_album(person: @person)
    @purchase = Product.add_to_cart(@album.person, @album)
    @invoice = Invoice.factory(@person, @purchase)
  end

  it "should not create a new InvoiceSettlement record for the subsequent settlement_received()" do
    @invoice.settlement_received(@person, 1)
    expect { @invoice.settlement_received(@person, 1) }.not_to change(InvoiceSettlement, :count)
  end

  it "should not put the invalid record into invoice_settlements on subsequent settlement_received()" do
    @invoice.invoice_settlements.reload
    @invoice.settlement_received(@person, 1)
    expect { @invoice.settlement_received(@person, 1) }.not_to change { @invoice.invoice_settlements.length }
  end
end

describe Invoice, "receiving a complete settlement" do
  before(:each) do
    TCFactory.build_ad_hoc_album_product
    @person = TCFactory.build_person(
      name: "trevor", email: "trevor@protocool.net",
    )
    @album = TCFactory.build_album(person: @person)
    @purchase = Product.add_to_cart(@album.person, @album)
    @invoice = Invoice.factory(@person, @purchase)
  end

  it "should store the received amount" do
    expect { @invoice.settlement_received(@person, 998) }.to change(InvoiceSettlement, :count).by(1)
  end

  it "should not call settled!" do
    expect(@invoice).not_to receive(:settled!)
    expect { @invoice.settlement_received(@person, 998) }.to change(InvoiceSettlement, :count).by(1)
  end

  it "should be can_settle?" do
    allow(@invoice).to receive(:settled!) # we just want to make sure the record is created
    @invoice.settlement_received(@person, @invoice.invoice_amount_cents)
    expect(@invoice).to be_can_settle
  end

  it "should not be settled" do
    expect(@invoice).not_to be_settled
  end
end

describe Invoice, "update invoice gst config payment os settlements" do
  let!(:non_igst_gst) { GstConfig.find_by(igst: nil) }
  let!(:igst_gst) { GstConfig.find_by(cgst: nil) }
  let!(:non_igst_state) { CountryState.find_by(gst_config: non_igst_gst) }
  let!(:igst_state) { CountryState.find_by(gst_config: igst_gst) }

  context "indian person" do
    let!(:indian_person) { create(:person, state: non_igst_state.name, country: "IN") }
    let!(:non_igst_cc) { create(:stored_credit_card, :payos_card, person: indian_person, state_id: non_igst_state.id) }
    let!(:igst_cc) { create(:stored_credit_card, :payos_card, person: indian_person, state_id: igst_state.id) }

    before :each do
      allow(FeatureFlipper).to receive(:show_feature?).with(:bi_transfer, indian_person).and_return(false)
      allow(FeatureFlipper).to receive(:show_feature?).with(:enable_gst, indian_person).and_return(true)
    end

    context "cc settlement" do
      let!(:payments_os_transaction) do
        create(
          :payments_os_transaction,
          person: indian_person,
          stored_credit_card: igst_cc
        )
      end
      let!(:invoice) { create(:invoice, person: indian_person) }

      it "should update gst with cc gst" do
        invoice.settlement_received(payments_os_transaction, 100)
        expect(invoice.gst_config).to eq(payments_os_transaction.gst_config)
      end
    end
    context "balance settlement" do
      let!(:person_transaction) { create(:person_transaction, person: indian_person) }

      context "gst already set" do
        let!(:gst_invoice) { create(:invoice, person: indian_person, gst_config: igst_gst) }

        it "should not update gst" do
          gst_invoice.settlement_received(person_transaction, 100)
          expect(gst_invoice.gst_config).to eq(igst_gst)
        end
      end
      context "gst not yet set" do
        let!(:non_gst_invoice) { create(:invoice, person: indian_person) }

        context "person has a state" do
          it "should update gst with person state" do
            non_gst_invoice.settlement_received(person_transaction, 100)
            expect(non_gst_invoice.gst_config).to eq(non_igst_gst)
          end
        end
        context "person does not have a state" do
          it "should update gst with person's most recent credit card state" do
            indian_person.update(state: nil)
            indian_person.reload
            non_gst_invoice.settlement_received(person_transaction, 100)
            expect(non_gst_invoice.gst_config).to eq(igst_gst)
          end
        end
      end
    end
  end
  context "non-indian user" do
    let!(:non_indian_person) { create(:person) }
    let!(:invoice) { create(:invoice, person: non_indian_person) }
    let!(:person_transaction) { create(:person_transaction, person: non_indian_person) }
    before :each do
      allow(FeatureFlipper).to receive(:show_feature?).with(:bi_transfer, non_indian_person).and_return(false)
      allow(FeatureFlipper).to receive(:show_feature?).with(:enable_gst, non_indian_person).and_return(true)
    end

    it "should not update gst" do
      invoice.settlement_received(person_transaction, 100)
      expect(invoice.gst_config).to be(nil)
    end
  end
end

describe Invoice, "when receiving settled!() while can_settle? is false" do
  before(:each) do
    TCFactory.build_ad_hoc_album_product
    @person = TCFactory.build_person(
      name: "trevor", email: "trevor@protocool.net",
    )
    @album = TCFactory.build_album(person: @person)
    @purchase = Product.add_to_cart(@album.person, @album)
    @invoice = Invoice.factory(@person, @purchase)
  end

  it "should start out as not can_settle?" do
    expect(@invoice).not_to be_can_settle
  end

  it "should raise an error" do
    expect { @invoice.settled! }.to raise_error(ArgumentError, /settlements do not match costs/)
  end

  it "should not change final_settlement_amount" do
    @invoice.final_settlement_amount_cents = nil
    expect { @invoice.settled! rescue nil }.not_to change(@invoice, :final_settlement_amount_cents)
  end

  it "should not change final_settlement_amount" do
    @invoice.final_settlement_amount_cents = nil
    expect { @invoice.settled!(Time.now) rescue nil }.not_to change(@invoice, :settled_at)
  end

  it "should not be settled when finished" do
    @invoice.settled! rescue nil
    expect(@invoice).not_to be_settled
  end
end

describe Invoice, "when receiving settled!() while can_settle? is true" do
  before(:each) do
    TCFactory.build_ad_hoc_album_product
    @person = TCFactory.build_person(
      name: "trevor", email: "trevor@protocool.net",
    )
    TCFactory.generate_album_purchase(@person, false)
    @purchase = @person.purchases.first
    @invoice = @person.invoices.first
    @invoice.settlement_received(@person, @purchase.cost_cents)
  end

  it "should note the final_settlement_amount" do
    expect { @invoice.settled! }.to change(@invoice, :final_settlement_amount_cents).to(@purchase.cost_cents)
  end

  it "should note the settled_at time" do
    explicit_time = Time.now

    @invoice.settled!(explicit_time)

    expect(@invoice.settled_at).to be_within(1.second).of(explicit_time)
  end

  it "should be can_settle" do
    expect(@invoice).to be_can_settle
  end

  it "should start out as not settled?()" do
    expect(@invoice).not_to be_settled
  end

  it "should notify the purchases" do
    expect(@purchase).not_to be_paid
    @invoice.settled!
    expect(@invoice.purchases.first.reload).to be_paid
  end

  it "should be settled when finished" do
    @invoice.settled!
    expect(@invoice).to be_settled
  end
end

describe Invoice, "when being settled!() more than once" do
  before(:each) do
    TCFactory.build_ad_hoc_album_product
    @person = TCFactory.build_person(
      name: "trevor", email: "trevor@protocool.net",
    )
    @album = TCFactory.build_album(person: @person)
    @purchase = Product.add_to_cart(@person, @album)
    @invoice = Invoice.factory(@person, @purchase)
    @invoice.settlement_received(@person, @invoice.invoice_amount_cents)
    @invoice.settled!
  end

  it "should raise an error" do
    expect { @invoice.settled! }.to raise_error(ArgumentError, /already settled/)
  end

  it "should not change final_settlement_amount" do
    @invoice.final_settlement_amount_cents = nil
    expect { @invoice.settled! rescue nil }.not_to change(@invoice, :final_settlement_amount_cents)
  end

  it "should not change final_settlement_amount" do
    @invoice.final_settlement_amount_cents = nil
    expect { @invoice.settled!(Time.now) rescue nil }.not_to change(@invoice, :settled_at)
  end
end

describe Invoice, "being destroyed before settlements" do
  before(:each) do
    TCFactory.build_ad_hoc_album_product
    @person = TCFactory.build_person(
      name: "trevor", email: "trevor@protocool.net",
    )
    @album = TCFactory.build_album(person: @person)
    @purchase = Product.add_to_cart(@person, @album)
    @invoice = Invoice.factory(@person, @purchase)
  end

  it "should delete the invoice record" do
    expect { @invoice.destroy }.to change(Invoice, :count).by(-1)
  end

  it "should notify the purchases" do
    allow(@invoice).to receive(:purchases).and_return([@purchase])
    expect(Purchase).to receive(:detach_purchases).with(@invoice)
    @invoice.destroy
  end

  it "should create an invoice_log" do
    initial_invoice_log_count = InvoiceLog.count
    @invoice.destroy
    expect(initial_invoice_log_count + 1).to eq(InvoiceLog.count)
  end

  it "should not be 'destroyable' if not visible_to_customer" do
    @invoice.batch_status = "waiting_for_batch"
    expect(@invoice.can_destroy?(false)).to eq(false)
  end

  it "should be 'destroyable' if visible_to_customer" do
    @invoice.batch_status = "visible_to_customer"
    expect(@invoice.can_destroy?(false)).to eq(true)
  end
end

describe Invoice, "being destroyed after settlements received" do
  before(:each) do
    TCFactory.build_ad_hoc_album_product
    @person = TCFactory.build_person(
      name: "trevor", email: "trevor@protocool.net",
    )
    @album = TCFactory.build_album(person: @person)
    @purchase = Product.add_to_cart(@person, @album)
    @invoice = Invoice.factory(@person, @purchase)
    @invoice.settlement_received(@person, 1)
  end

  it "should refuse to be destroyed" do
    expect { @invoice.destroy }.to raise_error(RuntimeError, /cannot destroy/)
  end

  it "should not notify purchases" do
    expect(@invoice).not_to receive(:purchases)
    @invoice.destroy rescue nil
  end
end

describe Invoice, "when checking if a customer had previously paid" do
  it "should return true when the customer has one or more paid for invoices" do
    TCFactory.build_ad_hoc_album_product
    person = TCFactory.build_person(
      name: "trevor", email: "trevor@protocool.net",
    )
    TCFactory.generate_album_purchase(person, false)
    purchase = person.purchases.first
    invoice = person.invoices.first
    invoice.settlement_received(person, purchase.cost_cents)
    invoice.settled!
    expect(Invoice.previous_customer?(person)).to eq(true)
  end

  it "should return false when the customer does not have any paid invoices" do
    person = TCFactory.build_person(
      name: "trevor", email: "trevor@protocool.net",
    )
    expect(Invoice.previous_customer?(person)).to eq(false)
  end

  context "Canadian customer scenarios" do
    it "should have a CAD currency" do
      TCFactory.build_ad_hoc_album_product
      @person = TCFactory.build_person(country: "CA", postal_code: "E4Y2E4")
      @album = TCFactory.build_album(person: @person)
      @video = TCFactory.build_video(person: @person)
      @purchase = Product.add_to_cart(@album.person, @album)
      @invoice = Invoice.factory(@person, @purchase)
      expect(@invoice.currency).to eq("CAD")
    end
  end
end

describe Invoice, "when checking if customer is first time distribution customer" do
  before(:each) do
    @person = TCFactory.build_person
    TCFactory.generate_songwriter_product
    TCFactory.build_ad_hoc_album_product
  end

  context "With no previous purchases" do
    it "should return a truthy value when the current invoice contains non songwriter product" do
      allow(Product).to receive(:distribution_product_ids).and_return(Product.all.map { |p| p.id })
      expect(@person.invoices).to be_empty
      invoice = buy_both_products
      boolean = !!invoice.reload.set_person_analytic_if_first_time_distro
      expect(boolean).to be_truthy
    end

    it "should return a falsey value when current invoice contains only songwriter product" do
      expect(@person.invoices).to be_empty
      invoice = buy_songwriter_product
      expect(invoice.set_person_analytic_if_first_time_distro).to be_falsey
    end
  end

  def buy_songwriter_product
    composer = TCFactory.build_composer(person_id: @person.id, agreed_to_terms: "1")

    purchase = Product.add_to_cart(@person, composer)
    invoice = Invoice.factory(@person, purchase)

    invoice.settlement_received(composer, purchase.cost_cents)
    invoice.settled!
    invoice
  end

  def buy_both_products
    album = TCFactory.build_album(person: @person)
    purchase1 = Product.add_to_cart(@person, album)
    composer = TCFactory.build_composer(person_id: @person.id, agreed_to_terms: "1")
    purchase2 = Product.add_to_cart(@person, composer)
    invoice = Invoice.factory(@person, [purchase1, purchase2])

    invoice.settlement_received(composer, invoice.invoice_amount_cents)
    invoice.settled!
    invoice
  end
end

describe Invoice, "when adding renewal purchases to invoices" do
  before(:each) do
    @person = TCFactory.create(:person)
    TCFactory.create(:person_transaction, debit: 0, credit: 35, person: @person)
    expect(@person.person_balance.reload.balance).to eq(35)

    TCFactory.generate_album_product

    store = Store.first
    # Create an album
    @album1 = TCFactory.create(
      :album,
      person: @person,
      known_live_on: nil,
      finalized_at: nil
    )

    salepoint = TCFactory.create(
      :salepoint,
      salepointable: @album1,
      store: store,
      has_rights_assignment: true
    )

    song1 = TCFactory.create(:song, album: @album1)
    song2 = TCFactory.create(:song, album: @album1)
    TCFactory.create(:upload, song: song1)
    TCFactory.create(:upload, song: song2)

    # Create another album
    @album2 = TCFactory.create(
      :album,
      person: @person,
      known_live_on: nil,
      finalized_at: nil
    )

    salepoint = TCFactory.create(
      :salepoint,
      salepointable: @album2,
      store: store,
      has_rights_assignment: true
    )

    song1 = TCFactory.create(:song, album: @album2)
    song2 = TCFactory.create(:song, album: @album2)
    TCFactory.create(:upload, song: song1)
    TCFactory.create(:upload, song: song2)

    Product.add_to_cart(@person, @album1)
    Product.add_to_cart(@person, @album2)
  end

  it "should create a balance only invoice according to priority of purchase and add the rest to its own invoice" do
    allow(Purchase).to receive(:prioritize_purchases).and_return([@person.purchases[1], @person.purchases[0]])

    invoices = Invoice.add_renewal_purchases_to_invoices(@person, @person.purchases)

    expect(invoices.size).to eq(2)

    expect(invoices[0].purchases.size).to       eq(1)
    expect(invoices[0].purchases[0].related).to eq(@album2)

    expect(invoices[1].purchases.size).to eq(1)
    expect(invoices[1].purchases[0].related).to eq(@album1)
  end

  context "it errors out" do
    it "should create an invoice log" do
      initial_invoice_log_count = InvoiceLog.count
      allow(Invoice).to receive(:factory).and_raise(StandardError)

      Invoice.add_renewal_purchases_to_invoices(@person, @person.purchases)

      expect(initial_invoice_log_count + 1).to eq(InvoiceLog.count)
    end

    it "should notify airbrake" do
      expect(Airbrake).to receive(:notify)
      allow(Invoice).to receive(:factory).and_raise(StandardError)
      Invoice.add_renewal_purchases_to_invoices(@person, @person.purchases)
    end
  end
end

describe Invoice, "#first_purchase?" do
  it "returns true for the first invoice of a person" do
    current_time = Time.now
    @person = create(:person)
    @invoice1 = create(:invoice, person: @person, settled_at: current_time)
    @invoice2 = create(:invoice, person: @person, settled_at: current_time + 1.second)

    expect(@invoice1.first_purchase?).to be true
    expect(@invoice2.first_purchase?).to be false
  end
end

describe Invoice, "#includes_distribution_to_store?" do
  let!(:invoice) { create(:invoice) }
  let!(:person) { invoice.person }
  let!(:store_name) { Store::SPOTIFY }
  subject { invoice.includes_distribution_to_store?(store_name) }

  def active_album(store_short_name, album_traits)
    album = create(:album, *album_traits, person: person)
    store = Store.find_by(short_name: store_short_name)
    create(:petri_bundle, album: album)
    create(:salepoint, :payment_applied, :finalized, salepointable: album, store: store)
    album
  end

  context "includes album purchase" do
    context "with store salepoints" do
      context "for specified store" do
        let!(:album) { active_album(Store::SPOTIFY, [:paid]) }
        before(:each) do
          purch = create(:purchase, product: Product.find(36), person: person, related: album)
          invoice.purchases << purch
        end

        context "with specific store name arguments" do
          it "returns true" do
            expect(subject).to be(true)
          end
        end

        context "with :any argument" do
          let!(:store_name) { :any }
          it "returns true" do
            expect(subject).to be(true)
          end
        end

      end

      context "for a different store" do
        let!(:album) { active_album("Google", [:paid]) }
        before(:each) do
          purch = create(:purchase, product: Product.find(36), person: person, related: album)
          invoice.purchases << purch
        end
        
        context "with specific store name arguments" do
          it "returns false" do
            expect(subject).to be(false)
          end
        end

        context "with :any argument" do
          let!(:store_name) { :any }
          it "returns true" do
            expect(subject).to be(true)
          end
        end

      end
    end
    context "without store salepoints" do
      let!(:album) { create(:album, person: person) }
      before(:each) do
        purch = create(:purchase, product: Product.find(36), person: person, related: album)
        invoice.purchases << purch
      end
      
      context "with specific store name arguments" do
          it "returns false" do
            expect(subject).to be(false)
          end
      end

      context "with :any argument" do
        let(:store_name) { :any }
        it "returns false" do
          expect(subject).to be(false)
        end
      end

    end

    context "album is paid via credit and includes distribution for specified store" do
      let!(:album) { active_album(Store::SPOTIFY, [:paid]) }
      before(:each) do
        purch = create(:purchase, product: Product.find(36), person: person, related: album)
        invoice.purchases << purch
      end
      it "returns true" do
        expect(subject).to be(true)
      end
    end

    context "album is paid via credit and includes distribution for specified store" do
      let!(:album) { active_album(Store::SPOTIFY, [:paid]) }
      before(:each) do
        create(:person_plan, person: person, plan: Plan.find(2))
        cred = CreditUsage.for(person, album)
        purch = create(:purchase, product: Product.find(107), person: person, related: cred)
        invoice.purchases << purch
      end
      it "returns true" do
        expect(subject).to be(true)
      end
    end

    context "album is paid via credit and includes distribution for specified store" do
      let!(:album) { active_album("Google", [:paid]) }
      before(:each) do
        create(:person_plan, person: person, plan: Plan.find(2))
        cred = CreditUsage.for(person, album)
        purch = create(:purchase, product: Product.find(107), person: person, related: cred)
        invoice.purchases << purch
      end
      it "returns false" do
        expect(subject).to be(false)
      end
    end

    context "includes direct salepoint purchase for specified store" do
      let!(:album) { create(:album, person: person) }
      before(:each) do
        salepoint = create(:salepoint, album: album, store: Store.find_by(short_name: Store::SPOTIFY))
        purch = create(:purchase, product: Product.find(9), person: person, related: salepoint)
        invoice.purchases << purch
      end
      it "returns true" do
        expect(subject).to be(true)
      end
    end

    context "includes direct salepoint purchase for a different store" do
      let!(:album) { create(:album, person: person) }
      before(:each) do
        salepoint = create(:salepoint, album: album, store: Store.find_by(short_name: "Google"))
        purch = create(:purchase, product: Product.find(9), person: person, related: salepoint)
        invoice.purchases << purch
      end
      it "returns false" do
        expect(subject).to be(false)
      end
    end
  end

  context "does not include album purchase" do
    before(:each) do
      purch = create(:purchase, person: person)
      invoice.purchases << purch
    end
    it "returns false" do
      expect(subject).to be(false)
    end
  end
end

describe Invoice, ".analytics_by_date" do
  it "returns invoices within a given date range" do
    start_date = Date.today
    end_date = start_date + 2.days
    invoice = create(:invoice, settled_at: 1.day.from_now)
    future_invoice = create(:invoice, settled_at: 5.days.from_now)

    result = Invoice.analytics_by_date(start_date, end_date)

    expect(result).to include(invoice)
    expect(result).to_not include(future_invoice)
  end
end

describe Invoice do
  describe "#check_and_set_first_time_distribution_or_credit" do
    before(:each) do
      @person = create(:person)
      @invoice = create(:invoice, person: @person)
      @invoice2 = create(:invoice, person: @person)
    end

    context "user has an invoice and person_analytics with matching invoice ids" do
      it "returns true" do
        create(:person_analytic, person: @person, metric_value: @invoice.id)
        expect(@invoice.reload.check_and_set_first_time_distribution_or_credit).to be true
      end
    end

    context "user has an invoice and person_analytics with invoice ids that do not match" do
      it "returns false" do
        create(:person_analytic, person: @person, metric_value: @invoice2.id)
        expect(@invoice.reload.check_and_set_first_time_distribution_or_credit).to be false
      end
    end

    context "user does not have a person_analytic for first_time distro invoice id" do
      it "calls set_person_analytic_if_first_time_distro" do
        expect(@invoice).to receive(:set_person_analytic_if_first_time_distro).and_return(nil)
        @invoice.check_and_set_first_time_distribution_or_credit
      end
    end
  end

  describe "#credit_purchases?" do
    before(:each) do
      @dist_product = mock_model(
        Product,
        id: 36,
        price: 9.99,
        product_type: "album",
        country_website_id: 1,
        name: "Foo Bar",
        marketing_event_triggers: MarketingEventTrigger.all
      )
      @dist_person = create(:person, email: "customer1@example.com")
      @dist_purchase = create(
        :purchase, person: @dist_person, related: @dist_product, paid_at: Time.now,
                   product: @dist_product
      )
      @dist_invoice = create(:invoice, person: @dist_person)
      @dist_invoice.purchases << @dist_purchase
      create(:person_analytic, person: @dist_person, metric_value: @dist_invoice.id)

      @credit_product = mock_model(
        Product,
        id: 13,
        price: 29.99,
        product_type: "credit",
        country_website_id: 1,
        name: "Credit",
        marketing_event_triggers: MarketingEventTrigger.all
      )
      @credit_person = create(:person, email: "customer2@example.com")
      @credit_purchase = create(
        :purchase, person: @credit_person, related: @credit_product,
                   paid_at: Time.now, product: @credit_product
      )
      @credit_invoice = create(:invoice, person: @credit_person)
      @credit_invoice.purchases << @credit_purchase
      create(:person_analytic, person: @credit_person, metric_value: @credit_invoice.id)
    end

    context "user has purchased a distribution release for the first time" do
      it "returns false" do
        expect(@dist_invoice.reload).not_to be_credit_purchases
      end
    end

    context "user has purchased a credit product for the first time" do
      it "returns false" do
        expect(@credit_invoice.reload).to be_credit_purchases
      end
    end
  end

  describe "#send_refer_friend_purchase_notification" do
    it "calls send_notification on ReferAFriend::PurchaseHandler" do
      invoice = build_stubbed(:invoice)
      allow(ReferAFriendNotificationWorker).to receive(:perform_async).with(invoice.id, true)
      expect(ReferAFriendNotificationWorker).to receive(:perform_async).with(invoice.id, true)
      invoice.send_refer_friend_purchase_notification(true)
    end
  end

  describe "#purchased_publishing_before?" do
    let(:person) { create(:person) }
    let(:first_invoice) { create(:invoice, person: person, settled_at: Time.current - 1.day) }
    let(:second_invoice) { create(:invoice, person: person, settled_at: Time.current) }

    before(:each) do
      product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
      composer = create(:composer, person: person, account: create(:account, person: person))

      purchase = create(
        :purchase,
        person: person,
        product: product,
        invoice: first_invoice,
        related: composer,
        paid_at: Time.current
      )
    end

    it "returns true for an invoice settled_at after an invoice with a Composer purchase" do
      expect(second_invoice.purchased_publishing_before?).to be_truthy
    end

    it "returns false for an invoice settled_at without an previous invoice with a Composer purchase" do
      expect(first_invoice.purchased_publishing_before?).to be_falsey
    end

    it "returns false for an unsettled invoice" do
      second_invoice.update(settled_at: nil)

      expect(second_invoice.purchased_publishing_before?).to be_falsey
    end
  end

  describe "#purchased_credits_before?" do
    let(:person) { create(:person) }
    let(:first_invoice) { create(:invoice, person: person, settled_at: Time.current - 1.day) }
    let(:second_invoice) { create(:invoice, person: person, settled_at: Time.current) }

    before(:each) do
      product = Product.find(Product::US_ONE_ALBUM_CREDIT_PRODUCT_ID)

      purchase = create(
        :purchase,
        person: person,
        product: product,
        invoice: first_invoice,
        related: product,
        paid_at: Time.current
      )
    end

    it "returns true for an invoice settled_at after an invoice with a Composer purchase" do
      expect(second_invoice.purchased_credits_before?).to be_truthy
    end

    it "returns false for an invoice settled_at without an previous invoice with a Composer purchase" do
      expect(first_invoice.purchased_credits_before?).to be_falsey
    end

    it "returns true for an unsettled invoice if any past invoices has item" do
      second_invoice.update(settled_at: nil)

      expect(second_invoice.purchased_credits_before?).to be_truthy
    end
  end

  describe "#album_or_credit_purchase?" do
    before(:each) do
      product_id = Product.distribution_product_ids.first
      person = create(:person)
      album = create(:album, person: person)
      @invoice = create(:invoice, person: person)
      @purchase = create(:purchase, person: person, product_id: product_id, invoice: @invoice, related: album)
    end

    it "returns true for an invoice with a purchase with the product ids in Product::distribution_product_ids" do
      expect(@invoice.album_or_credit_purchase?).to be true
    end

    it "returns false for an invoice without a purchase with the product ids in Product::distribution_product_ids" do
      @purchase.update_attribute(:product_id, 0)
      expect(@invoice.album_or_credit_purchase?).to be false
    end
  end

  describe "#recently_purchased_first_distro_and_publishing?" do
    let(:invoice) { build_stubbed(:invoice, settled_at: Time.now) }

    it "calls publishing_purchase? if songwriter_product_purchase is not passed in" do
      expect(invoice).to receive(:publishing_purchase?).and_return(false)
      invoice.recently_purchased_first_distro_and_publishing?(true)
    end

    it "calls check_and_set_first_time_distribution_or_credit if is_first_time_dist_customer is not passed in" do
      expect(invoice).to receive(:check_and_set_first_time_distribution_or_credit).and_return(false)
      invoice.recently_purchased_first_distro_and_publishing?(nil, false)
    end

    it "returns true if is_first_time_dist_customer and songwriter_product_purchase are both true" do
      expect(invoice.recently_purchased_first_distro_and_publishing?(true, true)).to eq(true)
    end

    it "returns false if is_first_time_dist_customer and songwriter_product_purchase are both false" do
      expect(invoice.recently_purchased_first_distro_and_publishing?(false, false)).to eq(false)
    end

    it "calls and returns the value of purchased_publishing_recently if is_first_time_dist_customer is true and songwriter_product_purchase is false" do
      expect(invoice).to receive(:purchased_publishing_recently?).and_return(:test_return)
      expect(invoice.recently_purchased_first_distro_and_publishing?(true, false)).to eq(:test_return)
    end

    it "calls and returns the value of purchased_first_distro_recently? if is_first_time_dist_customer is false and songwriter_product_purchase is true" do
      expect(invoice).to receive(:purchased_first_distro_recently?).and_return(:test_return2)
      expect(invoice.recently_purchased_first_distro_and_publishing?(false, true)).to eq(:test_return2)
    end
  end

  describe "#purchased_publishing_recently?" do
    before(:each) do
      @time = Time.now
      @person = create(:person)
      @invoice1 = create(:invoice, person: @person, settled_at: @time - 1.second)
      @invoice2 = create(:invoice, person: @person, settled_at: @time)
      @product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
      @account = create(:account, person: @person)
      @composer = create(:composer, person: @person, account: @account)
      @purchase = create(
        :purchase, person: @person, product: @product, invoice: @invoice1, related: @composer,
                   paid_at: Time.now
      )
    end

    it "returns true for an invoice if the person has another invoice within the passed in timeframe that had a publishing purchase" do
      expect(@invoice2.purchased_publishing_recently?(3.seconds)).to be true
    end

    it "returns false for an invoice if the person has another invoice with the publishing purchase after this invoice" do
      @invoice1.update_attribute(:settled_at, @time + 1.second)
      expect(@invoice2.purchased_publishing_recently?(1.day)).to be false
    end

    it "returns false for an invoice if the person has another invoice with the publishing purchase before the timeframe passed in" do
      @invoice1.update_attribute(:settled_at, @time - 5.seconds)
      expect(@invoice2.purchased_publishing_recently?(4.seconds)).to be false
    end
  end

  describe "#purchased_first_distro_recently?" do
    before(:each) do
      @person = create(:person)
      @time = Time.now
      @invoice1 = create(:invoice, person: @person, settled_at: @time)
      @invoice2 = create(:invoice, person: @person, settled_at: @time)
    end

    it "returns false if a person_analytic for the invoice's person does not exist for the metric first_distribution_invoice_id" do
      expect(@invoice1.purchased_first_distro_recently?(1.year)).to be false
    end

    context "person has a person_analytic" do
      before(:each) do
        @person_analytic = create(:person_analytic, person: @person, metric_value: @invoice1.id)
      end

      it "returns false for an invoice that was purchased after the time restriction" do
        @invoice1.update_attribute(:settled_at, @time - 5.days)
        expect(@invoice2.purchased_first_distro_recently?(1.day)).to be false
      end

      it "returns false for an invoice that was purchased before the invoice with the first_distribution_invoice_id" do
        @invoice2.update_attribute(:settled_at, @time - 1.second)
        expect(@invoice2.purchased_first_distro_recently?(1.day)).to be false
      end

      it "returns true for an invoice that was purchased after the invoice with the first_distribution_invoice_id and before the time_restriction" do
        @invoice2.update_attribute(:settled_at, @time + 1.second)
        expect(@invoice2.purchased_first_distro_recently?(1.day)).to be true
      end
    end
  end

  describe ".sales_amounts" do
    it "returns invoices with their sales amounts" do
      date = Date.yesterday
      invoice = create(:invoice, final_settlement_amount_cents: 1000, settled_at: date)

      result = Invoice.sales_amounts(date)

      invoice_result = result.first
      expect(invoice_result["country_website_id"]).to eq invoice.person.country_website_id
      expect(invoice_result["total_sales"].to_f).to eq 10.0
      expect(invoice_result["average_sales"].to_f).to eq 10.0
    end
  end

  describe ".renewal_customers" do
    it "returns number of customers with renewal product" do
      invoice = create(:invoice, settled_at: Date.today)
      product = create(:product, product_family: "Renewal")
      create(:purchase, invoice: invoice, product: product)

      result = Invoice.renewal_customers(Date.today)

      invoice_result = result.first
      expect(invoice_result["country_website_id"]).to eq invoice.person.country_website_id
      expect(invoice_result["num_customers"]).to eq 1
    end
  end

  describe ".returning_dist" do
    it "returns number of returning customers with distribution product" do
      date = Date.today
      invoice = create(:invoice, settled_at: date)
      another_invoice = create(:invoice, settled_at: date - 2.days)
      product = create(:product, product_family: "Distribution")
      create(:purchase, invoice: invoice, product: product, person: invoice.person)
      create(
        :person_analytic,
        person: invoice.person,
        metric_name: "first_distribution_invoice_id",
        metric_value: another_invoice.id
      )

      result = Invoice.returning_dist(date)

      expect(result.first["country_website_id"]).to eq invoice.person.country_website_id
      expect(result.first["num_customers"]).to eq 1
    end
  end

  describe ".product_sales" do
    it "returns product information from invoices" do
      date = Date.yesterday
      invoice = create(:invoice, settled_at: date)
      product = create(:product, product_family: "Renewal")
      purchase = create(:purchase, invoice: invoice, product: product)

      result = Invoice.product_sales(date)

      expect(result.first["id"]).to eq product.id
      expect(result.first["country_website_id"]).to eq product.country_website_id
      expect(result.first["name"]).to eq product.name
      expect(result.first["product_family"]).to eq product.product_family
      expect(result.first["product_count"]).to eq 1
      expect(result.first["product_amount"]).to eq purchase.cost_cents / 100.to_f
    end
  end

  describe "#associated_product_types" do
    let(:person) { create(:person) }

    it "should return product types from the purchases" do
      product = create(:product)
      purchase = create(:purchase, person: person, related: product, paid_at: Time.now, product: product)
      invoice = create(:invoice, person: create(:person))
      invoice.purchases << purchase

      expect(invoice.associated_product_types).to eq([product.applies_to_product])
    end

    it "should return product types from credit based purchases" do
      allow(CreditUsage).to receive(:credit_available_for?).and_return(true)
      album = create(:album, person: person)
      product = create(:product)
      credit_usage = create(:credit_usage, person: person, related: album, applies_to_type: "Album")
      purchase = create(:purchase, person: person, related: credit_usage, paid_at: Time.now, product: product)
      invoice = create(:invoice, person: create(:person))
      invoice.purchases << purchase

      expect(invoice.associated_product_types).to include(credit_usage.applies_to_type)
    end
  end

  describe "#associated_album_ids" do
    let(:person) { create(:person) }
    let(:invoice) { create(:invoice, person: person) }

    it "should return associated album ids" do
      album = create(:album, person: person)
      product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)

      purchase = create(:purchase, person: person, related: album, paid_at: Time.now, product: product)
      invoice.purchases << purchase

      expect(invoice.associated_album_ids).to include(album.id)
    end

    it "should return credit used album ids" do
      allow(CreditUsage).to receive(:credit_available_for?).and_return(true)
      album = create(:album, person: person)
      product = create(:product)
      credit_usage = create(:credit_usage, person: person, related: album, applies_to_type: "Album")
      purchase = create(:purchase, person: person, related: credit_usage, paid_at: Time.now, product: product)
      invoice.purchases << purchase

      expect(invoice.associated_album_ids).to include(album.id)
    end
  end

  describe "#distributed_item_before?" do
    let(:person) { create(:person) }

    let(:invoice) { build(:invoice, person: person) }

    it "should return true for album" do
      create(:album, :finalized, person: person)
      expect(invoice.distributed_item_before?(Album, [])).to be_truthy
    end

    it "should return true for single" do
      create(:single, :finalized, album_type: "Single", person: person)
      expect(invoice.distributed_item_before?(Single, [])).to be_truthy
    end

    it "should return true for ringtone" do
      create(:ringtone, :finalized, album_type: "Ringtone", person: person)
      expect(invoice.distributed_item_before?(Ringtone, [])).to be_truthy
    end

    it "should exclude ids from exclusion list" do
      album = create(:album, :finalized, person: person)
      expect(invoice.distributed_item_before?(Album, [album.id])).to be_falsey
    end
  end

  describe "#outstanding_amount_in_local_currency" do
    let(:ind_person) { create(:person, :with_india_country_website) }
    let(:us_person) { create(:person) }
    let(:ind_current_pegged_rate) { create(:foreign_exchange_pegged_rate, pegged_rate: 73.12) }

    it "returns the payable amount after conversion with pegged rate" do
      ind_invoice = create(
        :invoice,
        :with_purchase_indian_product,
        person: ind_person,
        foreign_exchange_pegged_rate: ind_current_pegged_rate
      )
      ind_invoice.purchases[0].cost_cents = 234
      rounded_expected_amount = 17_110 # pegged_rate * invoice outstanding amount (73.12 * 234)
      expect(ind_invoice.outstanding_amount_in_local_currency).to eq(rounded_expected_amount)
    end

    it "returns the payable amount in USD if invoice does not contain pegged rate" do
      album = create(:album, person: us_person)
      invoice_without_pegged_rate = create(
        :invoice,
        :with_purchase,
        person: us_person,
        related: album
      )
      expect(
        invoice_without_pegged_rate.outstanding_amount_in_local_currency
      ).to eq(invoice_without_pegged_rate.outstanding_amount_cents)
    end
  end

  describe "#gst_tax_amount" do
    !let(:ind_person) { create(:person, :with_india_country_website) }
    before :each do
      allow(FeatureFlipper).to receive(:show_feature?).with(:bi_transfer, ind_person).and_return(false)
      allow(FeatureFlipper).to receive(:show_feature?).with(:enable_gst, ind_person).and_return(true)
    end

    it "should return nil if there is not gst config" do
      invoice = create(:invoice, gst_config_id: nil)
      expect(invoice.gst_tax_amount).to be_nil
    end

    it "should return the igst tax amount if igst is configured" do
      gst_config = create(:gst_config, igst: 18)
      invoice = create(:invoice, final_settlement_amount_cents: 400, gst_config_id: gst_config.id)
      expect(invoice.gst_tax_amount.igst.to_f).to eq(0.72)
      expect(invoice.gst_tax_amount.cgst).to be_nil
      expect(invoice.gst_tax_amount.sgst).to be_nil
    end

    it "should return the cgst and sgst tax amount if igst is null" do
      gst_config = create(:gst_config, cgst: 9, sgst: 8, igst: nil)
      invoice = create(:invoice, final_settlement_amount_cents: 400, gst_config_id: gst_config.id)
      expect(invoice.gst_tax_amount.cgst.to_f).to eq(0.36)
      expect(invoice.gst_tax_amount.sgst.to_f).to eq(0.32)
      expect(invoice.gst_tax_amount.igst).to be_nil
    end
  end
end

describe "Invoice static content should strip emojis" do
  before(:each) do
    person = TCFactory.build_person(
      name: "john smith",
      email: "john@example.net",
      country: "US"
    )
    us_album = TCFactory.build_album(person: person)
    us_purchase = Product.add_to_cart(us_album.person, us_album)
    @us_invoice = Invoice.factory(person, us_purchase)
  end

  it "ensure emojis are stripped when creating invoice static content" do
    @us_invoice.person.address1 = "address1-stripped\u{1F601}\u{1F512}"
    @us_invoice.person.address2 = "\u{1F601}\u{1F512}address2-stripped"
    stripped_address_hash = @us_invoice.send(:customer_address_attributes)
    stripped_address_hash["address1"].should eq("address1-stripped")
    stripped_address_hash["address2"].should eq("address2-stripped")
  end
end

describe "Invoice static content should include name from compliance info fields" do
  before(:each) do
    person = TCFactory.build_person(
      name: "john smith",
      email: "john@example.net",
      country: "US"
    )
    us_album = TCFactory.build_album(person: person)
    us_purchase = Product.add_to_cart(us_album.person, us_album)
    @invoice = Invoice.factory(person, us_purchase)
    person.compliance_info_fields.create!(
      [
        { field_name: :first_name, field_value: "Comp first name" },
        { field_name: :last_name, field_value: "Comp last name" },
        { field_name: :company_name, field_value: "Compliance Company Name" },
      ]
    )
  end

  it "should include the first name last name combination" do
    @invoice.create_static_content

    expect(@invoice.invoice_static_customer_info.name).to eq("Comp first name Comp last name")
  end

  it "should include company name if the the trasanction is a business trasanction" do
    allow(@invoice.person).to receive(:business?).and_return(true)
    @invoice.create_static_content
    expect(@invoice.invoice_static_customer_info.name).to eq("Compliance Company Name")
  end
end

describe "Partial invoice refunds" do
  before(:each) do
    stub_vat_api_and_redis
    allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

    @person = create_person
    @invoice = create_invoice
    add_balance_settlement
    make_the_invoice_partial!
  end

  it "should return true for the can destory invoice method" do
    expect(@invoice.can_destroy?).to be_truthy
  end

  it "should destory invoice settlements" do
    settlement_ids = @invoice.invoice_settlements.pluck(:id)
    @invoice.credit_and_remove_settlements!

    expect(InvoiceSettlement.where(id: settlement_ids)).not_to exist
  end

  it "should revert the vat posting" do
    vat_tax_adjustment_id = @invoice.vat_tax_adjustment

    @invoice.credit_and_remove_settlements!
    expect(VatTaxAdjustment.where(id: vat_tax_adjustment_id)).not_to exist
    expect(@person.person_transactions.where(comment: VatTaxAdjustment::VAT_REVERTED_COMMENT)).to exist
  end

  it "the person balance should be same as before the payment" do
    @invoice.credit_and_remove_settlements!

    expect(@person.person_balance.balance).to eq(@initial_balance)
  end

  it "should destroy the outbound invoice" do
    outbound_invoice_id = @invoice.outbound_invoice.id

    @invoice.credit_and_remove_settlements!
    expect(OutboundInvoice.where(id: outbound_invoice_id)).not_to exist
  end

  private

  def create_person
    @initial_balance = 1000
    person = TCFactory.build_person(
      name: "trevor",
      email: "trevor@protocool.net",
      country: "LU"
    )
    vat_info = person.build_vat_information(vat_registration_number: "0411905847", vat_registration_status: "valid")
    vat_info.save(validate: false)

    person.person_balance.update(balance: @initial_balance)
    person
  end

  def create_invoice
    TCFactory.build_ad_hoc_album_product
    album = TCFactory.build_album(person: @person)
    purchase = Product.add_to_cart(album.person, album)
    Invoice.factory(@person, purchase)
  end

  def add_balance_settlement
    balance = Cart::Payment::Strategies::Balance.new
    manager = Cart::Payment::Manager.new(@person, @invoice.purchases, "an ip address")
    allow(manager).to receive(:invoice).and_return(@invoice)
    balance.process(manager)
  end

  def stub_vat_api_and_redis
    allow_any_instance_of(TcVat::ApiClient)
      .to receive(:send_request!)
      .and_return(
        FakeVatApiHelper.generate do |response|
          response.valid           = true
          response.vat_number      = "0411905847"
          response.name            = "test"
          response.country         = "Luxembourg"
          response.place_of_supply = "Luxembourg"
          response.tax_rate        = 17
          response.tax_type        = "VAT"
          response.error_message   = nil
        end
      )

    data = {
      business: [
        { place_of_supply: "Luxembourg", tax_rate: "17.0", country: "Luxembourg" }
      ],
      individual: [
        { place_of_supply: "Luxembourg", tax_rate: "17.0", country: "Luxembourg" }
      ]
    }
    allow($redis).to receive(:get).and_return(data.to_json)
  end

  def make_the_invoice_partial!
    @invoice.update(settled_at: nil)
  end
end

describe "BatchTransactionScopes" do
  before(:each) do
    @invoice = create(:invoice, :with_batch_transaction_eligible_batch_transaction)
  end

  context "unsettled_visible_to_customer.batch_transaction_eligible" do
    it "should return an invoice" do
      expect(Invoice.unsettled_visible_to_customer.batch_transaction_eligible(Time.now + 30.days).count).to eq(1)
    end
  end

  context "unsettled_visible_to_customer" do
    it "should return an invoice" do
      expect(Invoice.unsettled_visible_to_customer.count).to eq(2)
    end
  end

  context "batch_transaction_eligible" do
    it "should not return an invoice from 19 days ago" do
      expect(Invoice.batch_transaction_eligible(Time.now + 19.days).count).to eq(0)
    end
    it "should return an invoice from 20 days ago" do
      expect(Invoice.batch_transaction_eligible(Time.now + 20.days).count).to eq(1)
    end
    it "should return an invoice from 21 days ago" do
      expect(Invoice.batch_transaction_eligible(Time.now + 21.days).count).to eq(1)
    end
    it "should not return an invoice from 41 days ago" do
      expect(Invoice.batch_transaction_eligible(Time.now + 41.days).count).to eq(1)
    end
    it "should return an invoice from 43 days ago" do
      expect(Invoice.batch_transaction_eligible(Time.now + 43.days).count).to eq(0)
    end
  end
end
