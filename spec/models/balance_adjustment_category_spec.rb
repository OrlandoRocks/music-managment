require "rails_helper"

RSpec.describe BalanceAdjustmentCategory, type: :model do
  it "includes all existing balance adjustment categories" do
    expect(
      BalanceAdjustmentCategory.select(:name).pluck(:name)
    ).to contain_exactly(
      "Refund - Renewal",
      "Refund - Other",
      "Service Adjustment",
      "Songwriter Royalty",
      "Other",
      "YouTube MCN Royalty",
      "Facebook",
      "Tidal DAP"
    )
  end

  it { should validate_uniqueness_of(:name) }

  describe "scopes" do
    describe ".us_taxable" do
      it "includes taxable categories" do
        expect(BalanceAdjustmentCategory.us_taxable.pluck(:name)).to contain_exactly("Songwriter Royalty", "Tidal DAP", "Facebook")
      end
    end
  end
end
