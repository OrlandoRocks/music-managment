require "rails_helper"

describe YoutubeSrMailTemplate do
  describe 'class methods' do
    let(:user) { FactoryBot.create(:person, email: "testuser1@test.com") }
    let(:another_user) { FactoryBot.create(:another_person) }
    let(:album) { FactoryBot.create(:album, :with_uploaded_songs, person: user) }
    let(:another_album) { FactoryBot.create(:album, :with_uploaded_songs, person: another_user) }

    describe '#types_for_select' do
      it "returns an array of available templates for select dropdown" do
        expected_result =
          [
            ["Removed from Monetization",                   :removed_monetization],
            ["Rights to Instrumental",                      :instrumental_rights],
            ["Disputing a Claim on Your YouTube Channel",   :disputing_a_claim],
            ["YouTube Ownership Conflict",                  :ownership_conflict],
            ["Authorization for Use in Video",              :authorization],
            ["Appealed Claim",                              :disputed_claim],
            ["Creative Commons",                            :creative_commons_content],
            ["Art Track",                                   :art_track]
          ]

        expect(YoutubeSrMailTemplate.types_for_select).to eq(expected_result)
      end
    end

    describe "#types_for_select" do
      it "returns the 'remove monetization' template record" do
        template_name = 'removed_monetization'
        expected_template = YoutubeSrMailTemplate.where(template_name: template_name).first
        expect(YoutubeSrMailTemplate.return_template(template_name)).to eq(expected_template)
      end

      it "returns the 'authorization' template record" do
        template_name = 'authorization'
        expected_template = YoutubeSrMailTemplate.where(template_name: template_name).first
        expect(YoutubeSrMailTemplate.return_template(template_name)).to eq(expected_template)
      end
    end
  end
end
