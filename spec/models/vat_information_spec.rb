require 'rails_helper'

RSpec.describe VatInformation, type: :model do
  let(:person) { FactoryBot.create(:person, :with_tc_social, country: "AT") }

  describe "Validations" do
    it "is not valid with invalid vat registration format" do
      vat = VatInformation.create(company_name: "abc", person: person,
                                  vat_registration_number: "122312")
      expect(vat).to_not be_valid
    end

    it "is not valid without vat_registration_number" do
      vat = VatInformation.create(company_name: "abc", person: person)
      expect(vat.vat_registration_status).to eq('invalid')
    end

    it "is not valid if there exist another user with same vat number and different address" do
      vat_info = create(:vat_information, person: person, vat_registration_number: "U37675002")
      another_person = create(:person, country: "AT", address1: "different address")
      new_vat_info = build(:vat_information, person: another_person, vat_registration_number: "U37675002")

      expect(new_vat_info).to be_invalid
      expect(new_vat_info.errors[:base]).to include("vat.errors.unavailable")
    end
  end
end
