require "rails_helper"

describe CountryState do
  
  describe "#get_gst_config" do
    before(:each) do
      gst = create(:gst_config, igst: 10)
      create(:country_state, name: "Fake STATE", gst_config: gst)
    end

    it "should return the correct gst config for the given state ignoring case after stripping spaces" do
      gst_config = CountryState.get_gst_config(" FAKE state ")
      expect(gst_config.igst).to eq(10)
    end

    it "should return nil if the passed state name does not match with any records" do      
      gst_config = CountryState.get_gst_config("FAKE statezzzz")
      expect(gst_config).to be_nil
    end

    it "should return nil for blank state" do
      gst_config = CountryState.get_gst_config(nil)
      expect(gst_config).to be_nil
    end
  end
end
