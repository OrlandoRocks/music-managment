require "rails_helper"

describe Distribution::MassRetryForm do
  describe "#save" do
    let(:person)          { build_stubbed(:person, :with_live_distributions) }
    let(:mass_retry_form) { Distribution::MassRetryForm.new(mass_retry_params) }

    let(:mass_priority_retry_form) {
      mass_retry_params[:priority] = 1
      Distribution::MassRetryForm.new(mass_retry_params) 
    }
    let(:job_name)        { "test job" }
    let(:album_ids_or_upcs_params) { ["1", "2", "3", "4"] }
    let(:mass_retry_params) do
      { person_id:         person.id,
        job_name:          job_name,
        album_ids_or_upcs: album_ids_or_upcs_params,
        store_ids:         [3],
        metadata_only:     nil,
        remote_ip:         "127.0.0.1" }
    end
    let(:expected_retry_params) do
      { person_id:         person.id,
        job_name:          job_name,
        album_ids_or_upcs: album_ids_or_upcs,
        store_ids:         [3],
        metadata_only:     nil,
        remote_ip:         "127.0.0.1",
        retry_in_batches:  false,
        use_sns_only:      nil,
        daily_retry_limit: nil
       }
    end

    context "when invalid" do
      it "should not call the Distribution::MassRetryWorker" do
        mass_retry_form.person_id = nil
        expect(Distribution::MassRetryWorker).not_to receive(:perform_async)
        expect(mass_retry_form.save).to eq false
      end
    end

    context "when valid" do
      context "when retrying all distributions" do
        let(:album_ids_or_upcs) { ["1", "2", "3", "4"] }

        before do
          expected_retry_params[:use_sns_only] = false
        end

        it "should enqueue a job with all album_ids_or_upcs" do
          expect(Distribution::MassRetryWorker).to receive(:perform_in).once.with(0.hours, expected_retry_params)
          expect(mass_retry_form.save).to eq true
        end
      end
  
      context "when the number of albums is greater than MAX_RETRY_ALBUMS" do 
        let(:expected_no_of_workers) { (album_ids_or_upcs_params.count / Distribution::MassRetryForm::MAX_RETRY_ALBUMS.to_f).ceil }
        
        context "when no of albums is evenly divided by MAX_RETRY_ALBUMS" do
          let(:album_ids_or_upcs_params) { (1..20).to_a.map(&:to_s) }
          
          it "enqueues multiple MassRetryWorker" do 
            stub_const("Distribution::MassRetryForm::MAX_RETRY_ALBUMS", 5)
  
            expect(Distribution::MassRetryWorker).to receive(:perform_in)
            .exactly(expected_no_of_workers).times
            .with(instance_of(ActiveSupport::Duration), an_instance_of(Hash))
            
            mass_retry_form.save
          end
        end
        
        context "when no of albums is evenly divided by MAX_RETRY_ALBUMS" do
          let(:album_ids_or_upcs_params) { (1..23).to_a.map(&:to_s) }
          
          it "enqueues multiple MassRetryWorker" do 
            stub_const("Distribution::MassRetryForm::MAX_RETRY_ALBUMS", 5)
            
            expect(Distribution::MassRetryWorker).to receive(:perform_in)
            .exactly(expected_no_of_workers).times
            .with(instance_of(ActiveSupport::Duration), an_instance_of(Hash))
  
            mass_retry_form.save
          end
        end 
      end 

      context "when retrying distributions in batches" do
        before do
          allow(SecureRandom).to receive(:uuid) { 1 }
          mass_retry_params[:retry_in_batches] = "true"
          expected_retry_params[:retry_in_batches] = true
          mass_retry_params[:daily_retry_limit] = "2000"
          expected_retry_params[:daily_retry_limit] = 2000
        end

        context "without a daily_retry_limit" do
          let(:album_ids_or_upcs) { ["1", "2", "3", "4"] }

          it "should not call the Distribution::MassRetryWorker" do
            mass_retry_params[:daily_retry_limit] = ""
            expect(Distribution::MassRetryWorker).not_to receive(:perform_in)
            expect(mass_retry_form.save).to eq false
          end
        end

        context "with a daily_retry_limit" do
          let(:album_ids_or_upcs) { ["1", "2"] }

          before do
            mass_retry_params[:daily_retry_limit] = "2"
            expected_retry_params[:daily_retry_limit] = "2"
            expected_retry_params[:use_sns_only] = false
          end

          it "should enqueue a job with a limited amount of album_ids_or_upcs" do
            mass_retry_params[:retry_in_batches] = "false"
            mass_retry_params[:album_ids_or_upcs] = album_ids_or_upcs
            expected_retry_params[:retry_in_batches] = false
            expect(Distribution::MassRetryWorker).to receive(:perform_in).with(0.hours, expected_retry_params)
            mass_retry_form.save
          end

          it "should store the retry batches album_ids_or_upcs as a List in redis" do
            mass_retry_params[:retry_in_batches] = "true"
            mass_retry_params[:daily_retry_limit] = "2000"
            expected_retry_params[:retry_in_batches] = true
            expected_retry_params[:daily_retry_limit] = "2000"

            expect($redis).to receive(:rpush).with("distribution:batch-retry-albums:1", mass_retry_params[:album_ids_or_upcs])
            mass_retry_form.save
          end

          it "should store the retry batches data as a hash in redis" do
            mass_retry_params[:retry_in_batches] = "true"
            mass_retry_params[:daily_retry_limit] = "2000"
            expected_retry_params[:retry_in_batches] = true
            expected_retry_params[:daily_retry_limit] = "2000"

            expect($redis).to receive(:hmset)
            mass_retry_form.save
          end
        end
      end
    end


    context "when valid with priority" do
      context "when retrying all distributions" do
        let(:album_ids_or_upcs) { ["1", "2", "3", "4"] }

        before do
          expected_retry_params[:use_sns_only] = false
        end

        it "should enqueue a job with all album_ids_or_upcs" do
          expect(
            Distribution::MassPriorityRetryWorker
          ).to receive(:perform_in)
          .once
          .with(0.hours, expected_retry_params)
          expect(mass_priority_retry_form.save).to eq true
        end
      end

      context "when the number of albums is greater than MAX_RETRY_ALBUMS" do
        let(:expected_no_of_workers) {
          (album_ids_or_upcs_params.count / Distribution::MassRetryForm::MAX_RETRY_ALBUMS.to_f).ceil
        }

        context "when no of albums is evenly divided by MAX_RETRY_ALBUMS" do
          let(:album_ids_or_upcs_params) { (1..20).to_a.map(&:to_s) }

          it "enqueues multiple MassPriorityRetryWorker" do
            stub_const("Distribution::MassRetryForm::MAX_RETRY_ALBUMS", 5)

            expect(Distribution::MassPriorityRetryWorker).to receive(:perform_in)
            .exactly(expected_no_of_workers).times
            .with(instance_of(ActiveSupport::Duration), an_instance_of(Hash))

            mass_priority_retry_form.save
          end
        end

        context "when no of albums is evenly divided by MAX_RETRY_ALBUMS" do
          let(:album_ids_or_upcs_params) { (1..23).to_a.map(&:to_s) }

          it "enqueues multiple MassPriorityRetryWorker" do 
            stub_const("Distribution::MassRetryForm::MAX_RETRY_ALBUMS", 5)
            
            expect(Distribution::MassPriorityRetryWorker).to receive(:perform_in)
            .exactly(expected_no_of_workers).times
            .with(instance_of(ActiveSupport::Duration), an_instance_of(Hash))
  
            mass_priority_retry_form.save
          end
        end 
      end 

      context "when retrying distributions in batches" do
        before do
          allow(SecureRandom).to receive(:uuid) { 1 }
          mass_retry_params[:retry_in_batches] = "true"
          expected_retry_params[:retry_in_batches] = true
          mass_retry_params[:daily_retry_limit] = "2000"
          expected_retry_params[:daily_retry_limit] = 2000
        end

        context "without a daily_retry_limit" do
          let(:album_ids_or_upcs) { ["1", "2", "3", "4"] }

          it "should not call the Distribution::MassPriorityRetryWorker" do
            mass_retry_params[:daily_retry_limit] = ""
            expect(Distribution::MassPriorityRetryWorker).not_to receive(:perform_in)
            expect(mass_priority_retry_form.save).to eq false
          end
        end

        context "with a daily_retry_limit" do
          let(:album_ids_or_upcs) { ["1", "2"] }

          before do
            mass_retry_params[:daily_retry_limit] = "2"
            expected_retry_params[:daily_retry_limit] = "2"
            expected_retry_params[:use_sns_only] = false
          end

          it "should enqueue a job with a limited amount of album_ids_or_upcs" do
            mass_retry_params[:retry_in_batches] = "false"
            mass_retry_params[:album_ids_or_upcs] = album_ids_or_upcs
            expected_retry_params[:retry_in_batches] = false
            expect(Distribution::MassPriorityRetryWorker).to receive(:perform_in).with(0.hours, expected_retry_params)
            mass_priority_retry_form.save
          end

          it "should store the retry batches album_ids_or_upcs as a List in redis" do
            mass_retry_params[:retry_in_batches] = "true"
            mass_retry_params[:daily_retry_limit] = "2000"
            expected_retry_params[:retry_in_batches] = true
            expected_retry_params[:daily_retry_limit] = "2000"

            expect($redis).to receive(:rpush).with("distribution:batch-retry-albums:1", mass_retry_params[:album_ids_or_upcs])
            mass_priority_retry_form.save
          end

          it "should store the retry batches data as a hash in redis" do
            mass_retry_params[:retry_in_batches] = "true"
            mass_retry_params[:daily_retry_limit] = "2000"
            expected_retry_params[:retry_in_batches] = true
            expected_retry_params[:daily_retry_limit] = "2000"

            expect($redis).to receive(:hmset)
            mass_priority_retry_form.save
          end
        end
      end
    end
  end

  describe '#send_sns_notification?' do 
    let(:store_ids)         { nil }
    let(:metadata_only)     { nil }
    let(:daily_retry_limit) { nil }
    let(:retry_in_batches_bool)  { false }
    let(:use_sns_only)      { false }
    let(:use_sns_only_bool) { false }
    let(:job_name)          { "" }
  
    let(:mass_retry_params) do
      { person_id:         99,
        album_ids_or_upcs: ["1","2","3"],
        store_ids:         store_ids,
        metadata_only:     metadata_only,
        retry_in_batches:  retry_in_batches_bool,
        daily_retry_limit: daily_retry_limit,
        remote_ip:         '127.0.0.1',
        job_name:          job_name,
        use_sns_only:      use_sns_only_bool
       }
    end
  
    it 'send_sns_notification?' do
      mass_retry_params_sns_notification =
        mass_retry_params.merge(use_sns_only: true, retry_in_batches: true)
      mass_retry_params_not_sns_only_sns_notification =
        mass_retry_params.merge(use_sns_only: false, retry_in_batches: true)
      mass_retry_params_not_batches_sns_notification =
        mass_retry_params.merge(use_sns_only: true, retry_in_batches: false)
      mass_retry_params_not_sns_only_not_batches_sns_notification =
        mass_retry_params.merge(use_sns_only: false, retry_in_batches: false)
  
      expect(Distribution::MassRetryForm.new(mass_retry_params_sns_notification).send('send_sns_notification?')).to eql(true)
      expect(Distribution::MassRetryForm.new(mass_retry_params_not_sns_only_sns_notification).send('send_sns_notification?')).to eql(false)
      expect(Distribution::MassRetryForm.new(mass_retry_params_not_batches_sns_notification ).send('send_sns_notification?')).to eql(true)
      expect(Distribution::MassRetryForm.new(mass_retry_params_not_sns_only_not_batches_sns_notification).send('send_sns_notification?')).to eql(true)
    end
  
    it 'use_sns_only?' do
      mass_retry_params_sns_only =
        mass_retry_params.merge(use_sns_only: true)
      mass_retry_params_not_sns_only =
        mass_retry_params.merge(use_sns_only: false)
      expect(Distribution::MassRetryForm.new(mass_retry_params_sns_only).send('use_sns_only?')).to eql(true)
      expect(Distribution::MassRetryForm.new(mass_retry_params_not_sns_only).send('use_sns_only?')).to eql(false)
    end
  
    it 'retry_in_batches?' do
      mass_retry_params_retry_in_batches =
        mass_retry_params.merge(retry_in_batches: true)
      mass_retry_params_not_retry_in_batches =
        mass_retry_params.merge(retry_in_batches: false)
      expect(Distribution::MassRetryForm.new(mass_retry_params_retry_in_batches).send('retry_in_batches?')).to eql(true)
      expect(Distribution::MassRetryForm.new(mass_retry_params_not_retry_in_batches).send('retry_in_batches?')).to eql(false)
    end
  end
end
