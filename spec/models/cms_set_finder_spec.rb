require "rails_helper"
describe CmsSetFinder do
  before :each do
    CmsSet.all.each { |set| set.update_attribute(:start_tmsp, nil) }

    @frozen_time = Timecop.freeze(Time.local(2021, 1, 1))

    # had to randomize due to the MAX() function in
    # MySQL returning multiple results if time
    # is in same for a set of results
    states = {
      active:      Time.local(2020, 12, 01),
      old:         Time.local(2019, 12, 30),
      future:      Time.local(2022, 1, 2),
      unscheduled: nil,
    }

    { us: [1, 1], de: [5, 6] }.each do |country_code, country_website|
      ["login", "interstitial", "dashboard"].each do |callout_type|
        states.each do |callout_state, time_offset|
          time_offset += rand(1..1000) if time_offset
          instance_variable_set(
            "@#{callout_state}_#{country_code}_#{callout_type}_set",
            FactoryBot.create(
              "#{callout_type}_set".to_sym,
              :no_date_check,
              callout_name: "#{callout_state} #{country_code} #{callout_type}",
              country_website_id: country_website[0],
              country_website_language_id: country_website[1],
              start_tmsp: time_offset,
            )
          )
        end
      end
    end
  end

  after do
    Timecop.return
  end

  describe ".upcoming_sets" do
    it "should return all scheduled upcoming sets" do
      expect(CmsSetFinder.upcoming_sets).to include(
        @future_us_login_set,
        @future_us_dashboard_set,
        @future_us_interstitial_set,
        @future_de_login_set,
        @future_de_dashboard_set,
        @future_de_interstitial_set,
      )
    end
  end

  describe ".unscheduled_and_archived_sets" do
    it "should return all unscheduled and archived sets" do
      expect(CmsSetFinder.unscheduled_and_archived_sets).to include(
        @unscheduled_de_login_set,
        @unscheduled_us_login_set,
        @unscheduled_us_dashboard_set,
        @unscheduled_de_dashboard_set,
        @unscheduled_us_interstitial_set,
        @unscheduled_de_interstitial_set,
        @old_us_login_set,
        @old_de_login_set,
        @old_us_dashboard_set,
        @old_de_dashboard_set,
        @old_us_interstitial_set,
        @old_de_interstitial_set,
      )
    end
  end

  describe ".active_set" do
    it "should return only the active set of the correct type" do
      expect(CmsSetFinder.active_set("login", :us, 'United States')).to eq @active_us_login_set
      expect(CmsSetFinder.active_set("interstitial", :us, 'United States')).to eq @active_us_interstitial_set
      expect(CmsSetFinder.active_set("dashboard", :us, 'United States')).to eq @active_us_dashboard_set

      expect(CmsSetFinder.active_set("login", :de, 'Deutschland')).to eq @active_de_login_set
      expect(CmsSetFinder.active_set("interstitial", :de, 'Deutschland')).to eq @active_de_interstitial_set
      expect(CmsSetFinder.active_set("dashboard", :de, 'Deutschland')).to eq @active_de_dashboard_set
    end
  end

  describe ".active_sets" do
    it "should return the active cms sets of each type per country" do
      expect(CmsSetFinder.active_sets).to include(
        @active_us_login_set,
        @active_us_interstitial_set,
        @active_us_dashboard_set,
        @active_de_login_set,
        @active_de_interstitial_set,
        @active_de_dashboard_set,
      )
    end

    it "should not return sets scheduled for the future" do
      expect(CmsSetFinder.active_sets).to_not include(
        @future_us_login_set,
        @future_de_login_set,
        @future_us_dashboard_set,
        @future_de_dashboard_set,
        @future_us_interstitial_set,
        @future_de_interstitial_set,
      )
    end

    it "should only return a set of each type if one is active" do
      @active_us_login_set.destroy
      @active_de_login_set.destroy
      @old_us_login_set.destroy
      @old_de_login_set.destroy

      expect(CmsSetFinder.active_sets).to_not include(
        @active_us_login_set,
        @active_de_login_set,
      )
      expect(CmsSetFinder.active_sets).to_not include(
        @old_us_login_set,
        @old_de_login_set,
      )

      expect(CmsSetFinder.active_sets).to include(
        @active_us_interstitial_set,
        @active_de_interstitial_set,
        @active_us_dashboard_set,
        @active_de_dashboard_set,
      )
    end
  end
end
