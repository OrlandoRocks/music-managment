require 'rails_helper'

describe CountryAudit do
  let!(:self_identified) {create(:country_audit_source)}
  let!(:payoneer_kyc) {create(:country_audit_source, name: "Payoneer KYC")}
  let!(:person) {create(:person)}
  let!(:first_audit) do
    create(:country_audit, person: person, country: Country.first, country_audit_source: self_identified)
  end
  let!(:second_audit) do
    create(:country_audit, person: person, country: Country.first, country_audit_source: payoneer_kyc)
  end
  context "destroy" do
    it "disallows destruction" do
      expect{first_audit.destroy}.to raise_error(CountryAudit::ImmutableCountryAuditError)
    end
  end
  context "update" do
    context "country"do
      it "disallows updation" do
        expect{first_audit.update(country: Country.second)}.to raise_error(CountryAudit::ImmutableCountryAuditError)
      end
    end
    context "source" do
      it "disallows updation" do
        expect{first_audit.update(country_audit_source: payoneer_kyc)}.to raise_error(CountryAudit::ImmutableCountryAuditError)
      end
    end
  end
  context "create" do
    context "identical audit" do
      it "disallows creation of identical audit" do
        failed_audit = build(:country_audit, person: second_audit.person, country: second_audit.country,
                              country_audit_source: second_audit.country_audit_source)

        expect(failed_audit.valid?).to be(false)
        expect(failed_audit.errors.full_messages).to eq(["Identical audit Cannot add audit with identical attributes"])
      end
    end
    context "new audit" do
      it "allows creation of new audit" do
        third_audit = create(:country_audit, person: second_audit.person, country: second_audit.country,
                             country_audit_source: first_audit.country_audit_source)

        expect(third_audit.present?).to be(true)
        expect(third_audit.valid?).to be(true)
        expect(third_audit.errors.blank?).to be(true)
      end
    end
  end
end
