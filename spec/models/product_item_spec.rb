require "rails_helper"

describe ProductItem, "determining has_price?" do

  it "should return false for has_price? if price is set to 0.00" do
    product_item = ProductItem.new(:price => 0.00)
    expect(product_item.has_price?).to eq(false)
  end

  it "should return false for has_price? if price is set to nil" do
    product_item = ProductItem.new(:price => nil)
    expect(product_item.has_price?).to eq(false)
  end

  it "should return true for has_price? if price is set to a float greater than zero" do
    product_item = ProductItem.new(:price => 1.11)
    expect(product_item.has_price?).to eq(true)
  end

end

describe ProductItem, "determining an item's price by using the top_level_rules" do

  it "should calculate price for multiple product item rules " do
    one = ProductItemRule.new
    allow(one).to receive(:calculate_price).and_return(2.00)

    two = ProductItemRule.new
    allow(two).to receive(:calculate_price).and_return(3.00)

    expect(ProductItemRule).to receive(:top_level_rules).and_return([one, two])

    item = ProductItem.new
    expect(item.top_level_rules_price(Album.new)).to eq(5.00)
  end

  it "should calculate total price when price is set" do 
    product_item = ProductItem.new(:price => 1.11)
    expect(product_item.total_price(Album.new)).to eq(1.11)
  end

end
