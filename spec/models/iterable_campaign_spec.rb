require "rails_helper"

describe IterableCampaign do
  context "validates :campaign_id" do
    context "when the campaign_id is present" do
      it "allows creation" do
        expect { create(:iterable_campaign) }.not_to(raise_error)
      end
    end

    context "when the campaign_id is not present" do
      it "prevents creation" do
        expect { create(:iterable_campaign, campaign_id: nil) }.to(raise_error)
      end
    end
  end

  context "validates :environment" do
    context "when the environment is present" do
      context "when the environment is in the allowed set" do
        it "allows creation" do
          expect { create(:iterable_campaign) }.not_to(raise_error)
        end
      end

      context "when the environment is not in the allowed set" do
        it "prevents creation" do
          expect { create(:iterable_campaign, environment: "FAKE_ENVIRONMENT") }.to(raise_error)
        end
      end
    end

    context "when the environment is not present" do
      it "prevents creation" do
        expect { create(:iterable_campaign, campaign_id: nil) }.to(raise_error)
      end
    end
  end

  context "validates :name" do
    context "when the name is present" do
      context "when the name is in the allowed set" do
        it "allows creation" do
          expect { create(:iterable_campaign) }.not_to(raise_error)
        end
      end

      context "when the name is not in the allowed set" do
        it "prevents creation" do
          expect { create(:iterable_campaign, name: "FAKE_NAME") }.to(raise_error)
        end
      end
    end

    context "when the name is not present" do
      it "prevents creation" do
        expect { create(:iterable_campaign, name: nil) }.to(raise_error)
      end
    end
  end

  context "when there exists an IterableCampaign with the same name and environment" do
    it "prevents creation" do
      campaign = create(:iterable_campaign)
      expect {
        create(:iterable_campaign, environment: campaign.environment, name: campaign.name)
      }.to(raise_error)
    end
  end
end
