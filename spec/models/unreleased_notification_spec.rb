# encoding: UTF-8
require "rails_helper"

describe UnreleasedNotification do
  context "when album is missing artwork, tracks, and stores" do
    let(:album) { create(:album) }
    let(:unreleased_notification) { TCFactory.create(:unreleased_notification, person: album.person, notification_item: album) }

    it "creates every thirty days" do
      expect {
        UnreleasedNotification.create_notifications(run_date: album.created_on+31.days)
      }.not_to change(UnreleasedNotification, :count)
    end

    it "does not create past 120 days" do
      expect {
        UnreleasedNotification.create_notifications(run_date: album.created_on+120.days)
      }.to change(UnreleasedNotification, :count)
    end

    it "does not create past 150 days" do
      expect {
        UnreleasedNotification.create_notifications(run_date: album.created_on+150.days)
      }.not_to change(UnreleasedNotification, :count)
    end

    it "sets system_message_type to UnreleasedNotification" do
      expect(unreleased_notification.type).to eq(UnreleasedNotification.name)
    end

    it "creates text based on what the release is missing" do
      expect(unreleased_notification.text).not_to be_blank
    end

    it "sets a url" do
      expect(unreleased_notification.url).to eq("/albums/#{unreleased_notification.notification_item_id}")
    end

    it "sets a title" do
      expect(unreleased_notification.title).to eq("You have an unreleased album")
    end

    it "sets link_text" do
      expect(unreleased_notification.link_text).to eq("Click here to finish")
    end

    it "sets an image_url" do
      expect(unreleased_notification.image_url).not_to be_blank
    end
  end

  context "when album is not missing items" do
    let(:album) { create(:album, :with_uploaded_songs_and_artwork, :with_salepoints, :with_songwriter, number_of_songs: 5) }

    it "sets text" do
      unreleased_notification = TCFactory.create(:unreleased_notification, person: album.person, notification_item: album)
      expect(unreleased_notification.text).to eq("Your album is unreleased. Release your album today!")
    end

    it "creates an unreleased notification for people with unfinished albums" do
      expect {
        UnreleasedNotification.create_notifications(run_date: album.created_on+30.days)
      }.to change(UnreleasedNotification, :count)
    end
  end

  context "when user is German" do
    let(:german_person) { create(:person, country: "DE", country_website_id: 5) }
    let(:album) { create(:album, person: german_person) }
    let(:unreleased_notification) { TCFactory.create(:unreleased_notification, person: album.person, notification_item: album) }

    it "sets a title in German" do
      expect(unreleased_notification.title).to eq("Du hast ein unveröffentlichtes album")
    end

    it "sets text in German" do
      expect(unreleased_notification.text).to eq("Bei deinem album fehlen noch artwork, tracks, and stores.")
    end

    it "sets link_text in German" do
      expect(unreleased_notification.link_text).to eq("Klicke hier um den Vorgang abzuschließen")
    end
  end
end
