require "rails_helper"

describe CertBatch do
  before(:each) do
    @person = TCFactory.create(:person)
    product = Product.find(Product::US_YTM)
    @purchase =  Product.add_to_cart(@person, product)
    Kernel::silence_warnings { Cert.const_set( :INVALID_CA_PRODUCTS, [] ) }
  end

  describe "When generating certs" do
    it "should include brand_code when one is entered" do
      cert_batch = create(:cert_batch, brand_code: "BRAND")
      cert_batch.create_certs!(1)
      expect(cert_batch.certs.first.cert).to match(/BRAND/)
    end

    it "should match cert with brand_code when entered with case-insensitive" do
       cert_batch = create(:cert_batch, brand_code: "brand")
       cert_batch.create_certs!(1)
       code = cert_batch.certs.first.cert.sub(/brand/,"BRAND")
       test_usable(code)
    end

    it "should not break the generating of certs when brand_code is empty string" do
      cert_batch = create(:cert_batch, brand_code: "")
      cert_batch.create_certs!(1)
      expect(cert_batch.finalized).to be_truthy
      test_usable(cert_batch.certs.first.cert)
    end

    it "should not break the generating of certs when brand_code is nil" do
      cert_batch = create(:cert_batch, brand_code: nil)
      cert_batch.create_certs!(1)
      expect(cert_batch.finalized).to be_truthy
      test_usable(cert_batch.certs.first.cert)
    end
  end

  describe "validation" do
    context "when the cert_batch has a spawning engine" do
      let(:cert_batch) { build(:cert_batch, cert_engine: "SpawningPercent", spawning_code: "SPAWNINGCODE") }

      context "when the spawning_code is unique" do
        it "returns true" do
          expect(cert_batch.valid?).to be_truthy
        end
      end

      context "when the spawning_code is not unique" do
        before(:each) do
          create(:cert_batch, cert_engine: "SpawningPercent", spawning_code: "SPAWNINGCODE")
        end

        it "returns false" do
          expect(cert_batch.valid?).to be_falsy
        end

        it "returns a uniqueness error" do
          cert_batch.valid?

          expect(cert_batch.errors.messages[:spawning_code]).to be_present
          expect(cert_batch.errors.messages[:spawning_code].join(" ")).to include("has already been taken")
        end
      end
    end

    context "when the cert_batch has a non-spawning engine" do
      context "when there is no spawning_code" do
        let(:cert_batch) { build(:cert_batch, cert_engine: "DefaultPercent", spawning_code: nil) }

        it "returns true" do
          expect(cert_batch.valid?).to be_truthy
        end
      end

      context "when there is a spawning_code" do
        let(:cert_batch) { build(:cert_batch, cert_engine: "DefaultPercent", spawning_code: "SPAWNINGCODE") }

        context "when the spawning_code is unique" do
          it "returns true" do
            expect(cert_batch.valid?).to be_truthy
          end
        end

        context "when the spawning_code is not unique" do
          before(:each) do
            create(:cert_batch, cert_engine: "DefaultPercent", spawning_code: "SPAWNINGCODE")
          end

          it "returns true" do
            expect(cert_batch.valid?).to be_truthy
          end
        end
      end
    end
  end

  def test_usable(code)
    verified = Cert.verify verify_opts(:entered_code => code)
    expect(verified).to be_a(Cert)
    expect(verified.purchase).to eq(@purchase)
  end

  def verify_opts(options={})
    { :entered_code => "0001",
      :purchase => @purchase,
      :current_user => @person
    }.merge(options)
  end
end
