require "rails_helper"

describe Faq do
  before(:each) do
    allow(Faq).to receive(:faq_json).and_return(File.open("#{Rails.root}/spec/support/faqs.json"))
  end

  it "returns the YTM FAQs" do
    faqs = Faq.get_faqs(:youtube_money)
    expect(faqs.count).to be > 0
  end

  it "return the YTM FAQs for Pub Admin" do
    faqs = Faq.get_faqs(:youtube_money_pubadmin)
    expect(faqs.count).to be > 0
  end
end
