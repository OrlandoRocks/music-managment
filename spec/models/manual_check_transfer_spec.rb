require "rails_helper"

VALID_PARAMS = {
  payment_amount: 200,
  payee: "Withdrawing person",
  address1: "123 Test Road",
  city: "New York",
  state: 'NY',
  postal_code: "10009",
  country: 'US',
  password_entered: 'Testpass123!'
}.freeze

describe ManualCheckTransfer do
  describe "::send_scope" do
    context "when a scope name is passed in" do
      context "when the scope name is valid" do
        it "sends that scope" do
          wanted_manual_check_transfer = create(:manual_check_transfer, transfer_status: "processing")
          unwanted_manual_check_transfer = create(:manual_check_transfer, transfer_status: "cancelled")

          result = ManualCheckTransfer.send_scope("processing")

          expect(result).to include(wanted_manual_check_transfer)
          expect(result).to_not include(unwanted_manual_check_transfer)
        end
      end

      context "when the scope name is invalid" do
        it "raises a NoMethodError" do
          create(:manual_check_transfer, transfer_status: "pending")

          expect{
            ManualCheckTransfer.send_scope("delete_all")
          }.to raise_error
        end
      end
    end

    context "when a scope name is not passed in" do
      it "defaults to sending the 'pending' scope" do
        wanted_manual_check_transfer = create(:manual_check_transfer, transfer_status: "pending")
        unwanted_manual_check_transfer = create(:manual_check_transfer, transfer_status: "completed")

        result = ManualCheckTransfer.send_scope

        expect(result).to include(wanted_manual_check_transfer)
        expect(result).to_not include(unwanted_manual_check_transfer)
      end
    end
  end

  it "saves with valid params" do
    person = create(:person)
    balance = 1000
    person.person_balance.update(balance: balance)

    check_transfer = ManualCheckTransfer.generate(person, VALID_PARAMS)
    withdrawal_and_fee = (check_transfer.payment_cents + check_transfer.admin_charge_cents) / 100

    expect(check_transfer).to be_valid
    expect(check_transfer.payment_cents).to eq(20000)
    expect(check_transfer.admin_charge_cents).to eq(300)
    expect(check_transfer.debit_transaction_id).not_to be_nil
    expect(person.person_balance.reload.balance).to eq(balance - withdrawal_and_fee)
  end

  it "should require a state for US/Canada" do
    person = create(:person)
    balance = 1000
    person.person_balance.update(balance: balance)

    check_transfer = ManualCheckTransfer.generate(person, VALID_PARAMS.merge(country: 'US', state: nil))

    expect(check_transfer.errors.messages[:state].size).to eq 1
    expect(person.person_balance.reload.balance).to eq(balance)
  end

  it "should not require a state for non US/Canada, such as Germany" do
    person = create(:person)
    balance = 1000
    person.person_balance.update(balance: balance)

    check_transfer = ManualCheckTransfer.generate(person, VALID_PARAMS.merge(country: 'DE', state: nil))
    withdrawal_and_fee = (check_transfer.payment_cents + check_transfer.admin_charge_cents) / 100

    expect(check_transfer).to be_valid
    expect(person.person_balance.reload.balance).to eq(balance - withdrawal_and_fee)
  end

  it "should require a country" do
    person = create(:person)
    balance = 1000
    person.person_balance.update(balance: balance)

    check_transfer = ManualCheckTransfer.generate(person, VALID_PARAMS.merge(country: nil))

    expect(check_transfer.errors.messages[:country].size).to eq 1
    expect(person.person_balance.reload.balance).to eq(balance)
  end

  it "should require an address1" do
    person = create(:person)
    balance = 1000
    person.person_balance.update(balance: balance)

    check_transfer = ManualCheckTransfer.generate(person, VALID_PARAMS.merge(address1: nil))

    expect(check_transfer.errors.messages[:address1].size).to eq 1
    expect(person.person_balance.reload.balance).to eq(balance)
  end

  it "should have amount to be above the minimum 100" do
    person = create(:person)
    balance = 1000
    person.person_balance.update(balance: balance)

    check_transfer = ManualCheckTransfer.generate(person, VALID_PARAMS.merge(payment_amount: 99.99))

    expect(check_transfer.errors.messages[:payment_amount].size).to eq 1
    expect(person.person_balance.reload.balance).to eq(balance)
  end

  it "should associate the withdrawal with the customers last login event" do
    person = create(:person)
    balance = 1000
    person.person_balance.update(balance: balance)

    login_event    = LoginEvent.create(person_id: person.id)
    check_transfer = ManualCheckTransfer.generate(person, VALID_PARAMS)

    allow(person).to receive(:last_login_event) { login_event }

    expect(check_transfer.login_tracks.first.trackable).to eq check_transfer
    expect(check_transfer.login_tracks.first.login_event).to eq login_event
  end
end
