require "rails_helper"

describe Blacklist do
  before(:each) do
    @blacklist = Blacklist.new
  end

  it "should be valid" do
    expect(@blacklist).to be_valid
  end
end


describe Blacklist, 'should not allow words in the Blacklist model/table' do
  fixtures :blacklists

  it do
    expect(Blacklist.blacklisted?('fuckers')).to be_truthy
  end
end


describe Blacklist, 'should not allow words that appear in the TC rules of the blacklist model' do
  fixtures :blacklists

  it 'tune' do
    expect(Blacklist.blacklisted?('tune')).to be_falsey
  end
  
  it 'tunecore' do
    expect(Blacklist.blacklisted?('tunecore')).to be_truthy
  end
  
  it 'mytune' do
    expect(Blacklist.blacklisted?('mytune')).to be_falsey
  end
  
  it "core" do
    expect(Blacklist.blacklisted?('core')).to be_falsey
  end
  
  it "starts with www" do
    expect(Blacklist.blacklisted?('www1')).to be_truthy
    expect(Blacklist.blacklisted?('www')).to be_truthy
    expect(Blacklist.blacklisted?('wwwasfadfasdfas')).to be_truthy
  end
  
  it "should allow www later in the string" do
    expect(Blacklist.blacklisted?('mywww')).to be_falsey
  end
  
  it "starts with web" do
     expect(Blacklist.blacklisted?('web1')).to be_truthy
     expect(Blacklist.blacklisted?('web')).to be_truthy
     expect(Blacklist.blacklisted?('webasfadfasdfas')).to be_truthy
   end
   
   
   it "should allow web later in the string" do
     expect(Blacklist.blacklisted?('myweb')).to be_falsey
   end
   
   
   it "studio" do
      expect(Blacklist.blacklisted?('studio')).to be_truthy
    end
end
