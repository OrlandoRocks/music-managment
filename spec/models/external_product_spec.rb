require "rails_helper"

describe ExternalProduct do
  describe "when validating" do
    let(:ext_prod) { create(:external_product) }

    it "should be valid with all required attrs present" do
      expect(ext_prod.valid?).to eq(true)
    end

    it "should be invalid without a name" do
      ext_prod.name = nil
      expect(ext_prod.valid?).to eq(false)
    end

    it "should be invalid without an external_service_id" do
      ext_prod.external_service_id = nil
      expect(ext_prod.valid?).to eq(false)
    end

    it "should be invalid without a person" do
      ext_prod.person = nil
      expect(ext_prod.valid?).to eq(false)
    end

    it "should be invalid without a status" do
      ext_prod.status = nil
      expect(ext_prod.valid?).to eq(false)
    end

    it "should be invalid without an external_service_id" do
      ext_prod.external_service_id = nil
      expect(ext_prod.valid?).to eq(false)
    end
  end
end
