require "rails_helper"

describe StoredCreditCard, "when being created" do
  it "should require a customer_vault_id" do
    expect { TCFactory.create(:stored_credit_card, {:customer_vault_id => nil}) }.to raise_error(ActiveRecord::RecordInvalid)
  end

  it "should belong to a person" do
    expect { TCFactory.create(:stored_credit_card, {:person_id => nil}) }.to raise_error(ActiveRecord::RecordInvalid)
  end
end

describe StoredCreditCard do
  describe "#destroy" do
    context "when the stored_credit_card has a bt_token" do
      before(:each) do
        @person = FactoryBot.create(:person)
        @stored_credit_card = create(:person, :with_braintree_blue_stored_credit_card).stored_credit_cards.first
        expect_any_instance_of(Braintree::PaymentMethodGateway).to receive(:delete).with(@stored_credit_card.bt_token).and_return(true)
      end

      it "calls the braintree api method to delete the card" do
        @stored_credit_card.destroy("127.0.0.1", @person)
      end

      it "creates a BraintreeVaultTransaction record" do
        expect {
          @stored_credit_card.destroy("127.0.0.1", @person)
        }.to change(BraintreeVaultTransaction, :count).by(1)
      end
    end

    describe "#days_until_credit_card_expires" do
      it "returns the number of days the card has until expiration" do
        stored_credit_card = build_stubbed(:bb_stored_credit_card, expiration_month: 5, expiration_year: 2020)

        date = DateTime.new(2020, 4, 28)
        Timecop.travel(date)

        expect(stored_credit_card.days_until_credit_card_expires).to eq(33)
        Timecop.return
      end

      it "returns nil when the card is already expired" do
        stored_credit_card = build_stubbed(:bb_stored_credit_card, expiration_month: 5, expiration_year: 2020)

        date = DateTime.new(2020, 8, 28)
        Timecop.travel(date)

        expect(stored_credit_card.days_until_credit_card_expires).to be_nil
        Timecop.return
      end

      it "returns 0 on the last day of the month" do
        stored_credit_card = build_stubbed(:bb_stored_credit_card, expiration_month: 5, expiration_year: 2020)

        date = DateTime.new(2020, 5, 31)
        Timecop.travel(date)

        expect(stored_credit_card.days_until_credit_card_expires).to eq(0)
        Timecop.return
      end
    end
  end

  describe "::create_with_payments_os" do
    before(:each) do
      @person = create(:person)
      @payos_customer = create(:payments_os_customer, person: @person)
    end
    it "returns creates a new stored credit card" do
      card_token = "99a1864d-b7ac-4c50-9d17-b87818a55269"
      allow_any_instance_of(PaymentsOS::CustomerService).to receive(:find_or_create_customer).and_return(
        true,
        PaymentsOS::Responses::Customer.new(
          Faraday::Response.new(body: JSON.dump({
            "id": @payos_customer.customer_id,
            "created": "1582781980977",
            "modified": "1582781980977",
            "customer_reference": @payos_customer.customer_reference,
            "first_name": @person.first_name,
            "last_name": @person.last_name,
            "email": @person.email
          }), status: 200))
      )
      allow_any_instance_of(PaymentsOS::CardService).to receive(:store_payment_method).and_return(
        true,
        PaymentsOS::Responses::NewPaymentMethod.new(Faraday::Response.new(
          body: JSON.dump({
            "token": card_token,
            "token_type": "credit_card",
            "state": "created",
            "holder_name": "John Mark",
            "expiration_date": "10/2029",
            "last_4_digits": "1111",
            "pass_luhn_validation": true,
            "vendor": "VISA",
            "card_type": "CREDIT",
            "country_code": "USA",
            "billing_address": {
              "country": "USA",
              "state": "NY",
              "city": "NYC",
              "line1": "fifth avenue 10th"
            },
          }), status: 200)))
        is_success, result = StoredCreditCard.create_with_payments_os(
          @person,
          payments_os_token: card_token,
          first_name: @person.first_name,
          last_name: @person.last_name,
          phone_number: @person.phone_number,
          address1: @person.address1,
          address2: @person.address2,
          city: @person.city,
          state: @person.state,
          country: @person.country_domain,
          zip: @person.zip
        )
        expect(is_success).to be true
    end
  end

  describe "::create_with_braintree" do
    before(:each) do
      @person = FactoryBot.create(:person)
    end

    context "person has a braintree_customer_id in current gateway per corporate entity" do
      before(:each) do
        expect_any_instance_of(Braintree::PaymentMethodGateway).to receive(:create).and_return(nil)
        expect(StoredCreditCard).to receive(:handle_braintree_result).and_return(nil)
        allow_any_instance_of(Braintree::ConfigGatewayService).to receive(:find_customer).and_return(true)
        @person.braintree_customer_id = 129837
      end

      it "calls Braintree::PaymentMethod.create and StoredCreditCard.handle_braintree_result" do
        StoredCreditCard.create_with_braintree(@person)
      end

      it "does not create a new customer" do
        customer_id = "128372"
        braintree_customer_success = double(Object)
        braintree_customer_object = double(Object)
        allow(braintree_customer_success).to receive(:customer).and_return(braintree_customer_object)
        allow(braintree_customer_object).to receive(:id).and_return(customer_id)

        expect_any_instance_of(Braintree::ConfigGatewayService).not_to receive(:create_customer)

        StoredCreditCard.create_with_braintree(@person)
      end
    end

    context "person does not have a braintree_customer_id" do
      it "calls Braintree::Customer.create, person#update_attributes, Braintree::PaymentMethod.create and StoredCreditCard.handle_braintree_result" do
        customer_id = "128372"
        braintree_customer_success = double(Object)
        braintree_customer_object = double(Object)
        allow(braintree_customer_success).to receive(:customer).and_return(braintree_customer_object)
        allow(braintree_customer_object).to receive(:id).and_return(customer_id)

        expect_any_instance_of(Braintree::ConfigGatewayService).to receive(:create_payment_method).and_return(nil)
        expect(StoredCreditCard).to receive(:handle_braintree_result).and_return(nil)
        expect_any_instance_of(Braintree::ConfigGatewayService).to receive(:create_customer).and_return(braintree_customer_success)

        StoredCreditCard.create_with_braintree(@person)

        expect(@person.reload.braintree_customer_id).to eq(customer_id)
      end
    end
  end

  describe "#update_with_braintree" do
    it "calls Braintree::PaymentMethod::update and StoredCreditCard::handle_braintree_result" do
      s = create(:person, :with_braintree_blue_stored_credit_card).stored_credit_cards.first
      expect_any_instance_of(Braintree::PaymentMethodGateway).to receive(:update).and_return(nil)
      expect(StoredCreditCard).to receive(:handle_braintree_result).and_return(nil)
      s.update_with_braintree
    end
  end

  describe "::handle_braintree_result" do
    before(:each) do
      @stored_credit_card = FactoryBot.create(:bb_stored_credit_card)
      @person = FactoryBot.create(:person)
    end

    it "when the result is successful" do
      result = Braintree::SuccessfulResult.new
      allow(result).to receive(:payment_method).and_return(double(Object))
      expect(StoredCreditCard).to receive(:create_or_update_card).and_return(@stored_credit_card)
      expect(BraintreeVaultTransaction).to receive(:create).and_return(nil)
      expect(Note).to receive(:create).and_return(nil)

      StoredCreditCard.handle_braintree_result(result, @person, @stored_credit_card)
    end

    it "when the result is errored" do
      result = double(Object)
      expect(BraintreeVaultTransaction).to receive(:create).and_return(nil)
      expect(result).to receive(:success?)
      StoredCreditCard.handle_braintree_result(result, @person, @stored_credit_card)
    end
  end

  describe "::create_or_update_card" do
    let(:person) { create(:person) }
    it "updates the params of a previously existing card" do
      card = create(:stored_credit_card, last_four: "1111", cc_type: "Visa")
      payment_method = instance_double(
        Braintree::CreditCard,
        token: "123456",
        card_type: "Mastercard",
        expiration_month: 4,
        expiration_year: 2025,
        last_4: "1234"
      )

      options = {
        firstname: "Matty",
        lastname: "Ice",
        address1: "1234 test street",
        address2: "apt 2C",
        city: "New York",
        state: "NY",
        zip: "11238"
      }

      StoredCreditCard.create_or_update_card(payment_method, person, card, options)

      expect(card.last_four).to eq("1234")
      expect(card.cc_type).to eq("Mastercard")
      expect(card.expiration_month).to eq(4)
      expect(card.expiration_year).to eq(2025)
      expect(card.first_name).to eq("Matty")
      expect(card.last_name).to eq("Ice")
      expect(card.address1).to eq("1234 test street")
      expect(card.address2).to eq("apt 2C")
      expect(card.city).to eq("New York")
      expect(card.state).to eq("NY")
      expect(card.zip).to eq("11238")
    end

    describe "mark_as_failed?" do
      before(:each) do
        create(:person)
        @card = create(:stored_credit_card, last_four: "1111", cc_type: "Visa")
        @payment_method = instance_double(
          Braintree::CreditCard,
          token: "123456",
          card_type: "Mastercard",
          expiration_month: 4,
          expiration_year: 2025,
          last_4: "1234"
        )
        @options = {
          firstname: "Matty",
          lastname: "Ice",
          address1: "1234 test street",
          address2: "apt 2C",
          city: "New York",
          state: "NY",
          zip: "11238"
        }
      end

      context "for no braintree transactions" do
        it "should return false" do 
          scc = StoredCreditCard.create_or_update_card(@payment_method, @person, @card, @options)
          create(:braintree_transaction,  stored_credit_card_id: scc.id )
          expect(scc.mark_as_failed?).to eq(false)
        end
      end

      context "for braintree braintractions with a success status" do
        it "should return true" do
          @scc = StoredCreditCard.create_or_update_card(@payment_method, @person, @card, @options)
          create(:braintree_transaction, :succeeded, stored_credit_card_id: @scc.id)
          expect(@scc.mark_as_failed?).to eq(false)
        end
      end

      context "for braintree braintractions with a failed status" do
        before(:each) do 
          @scc = StoredCreditCard.create_or_update_card(@payment_method, @person, @card, @options)
          @btt = create(:braintree_transaction, :declined, stored_credit_card_id: @scc.id)
        end
        it "and was updated should return false" do
          @scc.updated_at = @btt.created_at + 1.days
          @scc.save
          expect(@scc.mark_as_failed?).to eq(false)
        end
        it "and was not updated should return true" do
          expect(@scc.mark_as_failed?).to eq(true)
        end

      end
    end

    it "creates a new card if one does not currently exist" do
      payment_method = instance_double(
        Braintree::CreditCard,
        token: "123456",
        card_type: "Mastercard",
        expiration_month: 4,
        expiration_year: 2025,
        last_4: "1234"
      )

      options = {
        firstname: "Matty",
        lastname: "Ice",
        address1: "1234 test street",
        address2: "apt 2C",
        city: "New York",
        state: "NY",
        zip: "11238"
      }

      card = StoredCreditCard.create_or_update_card(payment_method, person, nil, options)

      expect(card.last_four).to eq("1234")
      expect(card.cc_type).to eq("Mastercard")
      expect(card.expiration_month).to eq(4)
      expect(card.expiration_year).to eq(2025)
      expect(card.first_name).to eq("Matty")
      expect(card.last_name).to eq("Ice")
      expect(card.address1).to eq("1234 test street")
      expect(card.address2).to eq("apt 2C")
      expect(card.city).to eq("New York")
      expect(card.state).to eq("NY")
      expect(card.zip).to eq("11238")
    end
  end

  describe "#tunecore_bt_payin_provider_config" do
    it "returns tunecore config according to user's currency" do
      american = create(:person)
      american_cc = create(:stored_credit_card, person: american)
      canadian = create(:person, :with_canada_country_website)
      canadian_cc = create(:stored_credit_card, person: canadian)
      tc_corporate_entity = CorporateEntity.find_by(name: CorporateEntity::TUNECORE_US_NAME)
      bi_corporate_entity = CorporateEntity.find_by(name: CorporateEntity::TUNECORE_US_NAME)
      usd_tc_config = PayinProviderConfig.find_by(name: PayinProviderConfig::BRAINTREE_NAME,
                                                  corporate_entity: tc_corporate_entity,
                                                  currency: "USD")
      cad_tc_config = PayinProviderConfig.find_by(name: PayinProviderConfig::BRAINTREE_NAME,
                                                  corporate_entity: bi_corporate_entity,
                                                  currency: "CAD")
      expect(american_cc.tunecore_bt_payin_provider_config).to eq(usd_tc_config)
      expect(canadian_cc.tunecore_bt_payin_provider_config).to eq(cad_tc_config)
    end
  end

  describe "#billing_address" do
    it "returns billing address of stored credit card" do
      person = create(:person)
      stored_credit_card = create(:stored_credit_card, person: person)
      billing_address = stored_credit_card.billing_address

      expect(billing_address[:street_address]).to eq(stored_credit_card.address1)
      expect(billing_address[:extended_address]).to eq(stored_credit_card.address2)
      expect(billing_address[:locality]).to eq(stored_credit_card.city)
      expect(billing_address[:region]).to eq(stored_credit_card.state)
      expect(billing_address[:postal_code]).to eq(stored_credit_card.zip)
      expect(billing_address[:country_code_alpha2]).to eq(stored_credit_card.country)
      expect(billing_address[:first_name]).to eq(person.first_name)
      expect(billing_address[:last_name]).to eq(person.last_name)
    end
  end

  describe "#located_in_eea?" do
    it "returns true if stored credit card inside EEA" do
      stored_credit_card = create(:stored_credit_card, :lux_card)

      expect(stored_credit_card.located_in_eea?).to eq(true)
    end

    it "returns false if stored credit card outside EEA" do
      stored_credit_card = create(:stored_credit_card)

      expect(stored_credit_card.located_in_eea?).to eq(false)
    end
  end
end

def default_return_data(cc_number = nil)
  {"customer_vault" => {"customer" => {"first_name"=>"Test", "last_name"=>"Testerson", "address_1"=>"123 Main Street", "address_2"=>"APT 1", "state" => "NJ",
                                       "city"=>"Hoboken", "postal_code"=>"07030", "country"=>"US", "cc_number"=>cc_number.nil? ? "4xxxxxxxxxxx1111" : cc_number, "cc_exp" => "1010", "customer_vault_id"=>"123456"}}}
end

def create_credit_card_with_braintree_response(braintree_response)
  @stored_credit_card = StoredCreditCard.new({:customer_vault_id => 12345, :person=>TCFactory.create(:person), :status => "new"})
  mock_query_gateway = double('query_gw')
  allow(Tunecore::Braintree::Query).to receive(:new).and_return(mock_query_gateway)
  expect(mock_query_gateway).to receive(:execute).with({"customer_vault_id"=>@stored_credit_card.customer_vault_id, "report_type"=>"customer_vault"}).and_return(braintree_response)
  @stored_credit_card.save
end
