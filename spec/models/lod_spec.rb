require "rails_helper"

describe Lod do
  before(:each) do
    @lod = TCFactory.create(:lod, :last_status => nil)
  end

  def sample_signed_contract_response(opt = {})
    {'state' => opt[:state], 'guid' => opt[:guid],
      'recipients'=>[
        {'completed_at'=>'', 'name'=>'Gary Cheong', 'is_sender'=>true,
          'must_sign'=>'false', 'document_role_id'=>'cc_A', 'viewed_at'=>'',
          'role_id'=>'cc_A', 'state'=>'pending', 'email'=>'gcheong@tunecore.com'},
        {'completed_at'=>opt[:completed_at], 'name'=>'Lotta Lana Sr', 'is_sender'=>false,
          'must_sign'=>'true', 'document_role_id'=>'signer_A', 'viewed_at'=>'',
          'role_id'=>'signer_A', 'state'=>'signed', 'email'=>'signer_A@tunecore.com'}
      ]
    }
  end

  describe "when creating lod" do
    it "should not create lod_history when status is nil" do
      expect(@lod.last_status).to be_nil
      expect(@lod.history.size).to eq(0)
    end
  end

  describe "when updating lod status" do
    before :each do
      @scheduled_date = Time.now - 4.days
      @sent_date = Time.now - 3.days
      @signed_date = Time.now - 2.days
      @expired_date = Time.now - 1.day
      @lod.mark_as_scheduled_to_send @scheduled_date
      @lod.mark_as_sent_to_customer @sent_date
    end

    context "for signed document" do
      it "should update lod_history" do
        @lod.mark_as_signed_by_customer @signed_date
        expect(@lod.history.reload.collect(&:status)).to eq(['scheduled_to_send', 'sent_to_customer', 'signed_by_customer'])
        expect(@lod.history.collect{|h| h.status_at.to_s }).to eq([@scheduled_date.to_s, @sent_date.to_s, @signed_date.to_s])
      end
    end

    context "for expired document" do
      it "should update lod_history" do
        @lod.mark_as_expired @expired_date
        expect(@lod.history.reload.collect(&:status)).to eq(['scheduled_to_send', 'sent_to_customer', 'expired'])
        expect(@lod.history.collect{|h| h.status_at.to_s }).to eq([@scheduled_date.to_s, @sent_date.to_s, @expired_date.to_s])
      end
    end
  end

  describe "#update_document_status" do
    before :each do
      @lod = TCFactory.build_lod(:last_status => 'sent_to_customer', :document_guid => 'SENTTOCUSTOMER1')
    end

    it "should update an lod status to 'signed_by_customer' when passed a RS source for a signed document" do
      success = @lod.update_document_status(sample_signed_contract_response(:state => 'signed', :guid => @lod.document_guid))
      expect(@lod.reload.last_status).to eq('signed_by_customer')
      expect(success).to be_truthy
    end
  end
end
