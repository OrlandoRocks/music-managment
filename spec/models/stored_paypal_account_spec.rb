require "rails_helper"

describe StoredPaypalAccount, "on creation" do
  before(:each) do
    @customer = create(:person)
  end

  it "should require a country_website" do
    stored_paypal_account = build(:stored_paypal_account)
    expect(stored_paypal_account).not_to be_valid
  end

end
describe StoredPaypalAccount, "when being created" do
  before(:each) do
    @customer = create(:person)
    @stored_paypal_account = create(:stored_paypal_account, person: @customer)
  end

  it "should be valid" do
    expect(@stored_paypal_account).to be_valid
  end

  it "should mark a current stored paypal account as archived when adding a new one" do
    new_stored_paypal_account = create(:stored_paypal_account, person: @customer)
    @stored_paypal_account.reload
    expect(@stored_paypal_account.archived_at).not_to be_nil
  end

  it "should update the users person preferences to use their new stored paypal account" do
    new_stored_paypal_account = create(:stored_paypal_account, person: @customer)
    expect(new_stored_paypal_account.person.person_preference.preferred_paypal_account_id).to eq(new_stored_paypal_account.id)
  end

  it "should be able to pay an invoice with a StoredPaypalAccount as reference transaction" do
    @invoice = create(:invoice, settled_at: nil, final_settlement_amount_cents: nil, person: @customer)
    expect(@invoice.settled?).to eq(false)
    #mock out the PayPalAPI.reference_transaction call
    transaction = double('pp_response')
    allow(PayPalAPI).to receive(:reference_transaction).with(@customer.paypal_payin_provider_config, any_args).and_return(transaction)
    allow(transaction).to receive(:response).and_return(sale_return_data)

    @paypal_transaction = @stored_paypal_account.pay(@invoice) #this will return a paypal transaction
    expect(@invoice.settled?).to eq(true)
  end
end

describe StoredPaypalAccount, "when #pay(invoice)" do
  before(:each) do
    @customer = create(:person)
    @paypal_transaction = create(:paypal_transaction, person: @customer)
    @stored_paypal_account = create(:stored_paypal_account, person: @customer)
    @invoice = create(:invoice, :settled_at => nil, :final_settlement_amount_cents => nil, :person=>@customer)
    expect(@invoice.settled?).to be_falsey

    @transaction = double('pp_response')
    allow(PayPalAPI).to receive(:reference_transaction).with(@customer.paypal_payin_provider_config, any_args).and_return(@transaction)
  end

  context "with successful paypal transaction" do
    before(:each) do
      allow(@transaction).to receive(:response).and_return(sale_return_data)
    end

    it "should store the referenced PayPal account" do
      @paypal_transaction = @stored_paypal_account.pay(@invoice)
      expect(@paypal_transaction.referenced_paypal_account).to eq(@stored_paypal_account)
    end

    it "should associate paypal_transaction with the stored_paypal_account" do
      @paypal_transaction = @stored_paypal_account.pay(@invoice)
      expect(@stored_paypal_account.paypal_transactions).to include(@paypal_transaction)
    end

    it "should associate the same payin config" do
      @paypal_transaction = @stored_paypal_account.pay(@invoice)
      expect(@paypal_transaction.payin_provider_config).to eq(@stored_paypal_account.payin_provider_config)
    end

    it "should return paypal_transaction" do
      expect(@stored_paypal_account.pay(@invoice)).to be_a_kind_of(PaypalTransaction)
    end
  end

  context "with a failed paypal transaction" do
    it "should not be able to pay an invoice" do
      allow(@transaction).to receive(:response).and_return(sale_return_data('Completed', {'ACK' => 'Failure'}))
      @stored_paypal_account.pay(@invoice)
      expect(@invoice.settled?).to be_falsey
    end
  end

  context "with north american currencies" do
    before(:each) do
      allow(@transaction).to receive(:response).and_return(sale_return_data)
    end

    it "should return us credentials for us website" do
      expect(StoredPaypalAccount.checkout_credentials(CountryWebsite.find_by(country: "US"))).to eq({"USER"=>"dan_1303924341_biz_api1.tunecore.com", "SIGNATURE"=>"AiPC9BjkCyDFQXbSkoZcgqH3hpacALmKZwEGSugwVD3ZwshtnIwLGLFz", "PWD"=>"1303924353"})
    end

    it "should return ca credentials for ca website" do
      expect(StoredPaypalAccount.checkout_credentials(CountryWebsite.find_by(country: "CA"))).to eq({"USER"=>"causd_1317758079_biz_api1.tunecore.com", "SIGNATURE"=>"AE8Z1e9pCI.by00Q2BDitFIgdkYJAvHnkuaUwdFaFCAa06MV.N0Zbswb", "PWD"=>"1317758112"})
    end

  end

  context "with non north american country websites" do
    it "should return UK credentials for UK website" do
      expect(StoredPaypalAccount.checkout_credentials(CountryWebsite.find_by(country: "UK"))).to eq({"USER"=>"sandbox-uk_api1.tunecore.com", "SIGNATURE"=>"AiPC9BjkCyDFQXbSkoZcgqH3hpacAVYow.HE-wat9ynUnCozVQK2z3PV", "PWD"=>'7E53CEWCUD6S5NVL'})
    end

    it "should return DE credentials for DE website" do
      expect(StoredPaypalAccount.checkout_credentials(CountryWebsite.find_by(country: "DE"))).to eq({"USER"=>"sandbox-uk_api1.tunecore.com", "SIGNATURE"=>"AiPC9BjkCyDFQXbSkoZcgqH3hpacAVYow.HE-wat9ynUnCozVQK2z3PV", "PWD"=>'7E53CEWCUD6S5NVL'})
    end
  end

end

def sale_return_data(paymentstatus = 'Completed', options_overwrite={})
  {'ACK' => "Success", 'PAYMENTSTATUS' => paymentstatus, 'AMT' => "9.99", 'BILLINGAGREEMENTID' => "B-0RK57377ME0665506", 'TRANSACTIONID' => "9CB082202F284692D", 'TRANSACTIONTYPE' => "expresscheckout", 'FEEAMT' => "0.59",
   'PENDINGREASON' => "None", 'REASONCODE' => "None", 'CURRENCYCODE' => "USD"}.merge(options_overwrite)
end
