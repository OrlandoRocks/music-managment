require "rails_helper"

describe PlansPlanFeature do
  let!(:plan)  { create(:plan) }
  let!(:plan_feature) { create(:plan_feature) }

  context "When a PlansPlanFeature is created, " do
    it "log a historical record with change_type of 'add'" do
      expect{ PlansPlanFeature.create!(
        plan: plan,
        plan_feature: plan_feature
      )}.to change{ PlansPlanFeatureHistory.count }.by(1)

      feature = PlansPlanFeature.last
      feature_history = PlansPlanFeatureHistory.last

      expect( feature_history.plan_id ).to eq( feature.plan_id )
      expect( feature_history.plan_feature_id ).to eq( feature.plan_feature_id )
      expect( feature_history.change_type ).to eq( PlansPlanFeatureHistory.change_types[:add] )
    end
  end

  context "When a PlansPlanFeature is deleted, " do
    it "log a historical record with change_type of 'remove'" do
      PlansPlanFeature.create!(
        plan: plan,
        plan_feature: plan_feature
      )
      feature = PlansPlanFeature.last

      expect{ PlansPlanFeature.last.destroy! }.to change{ PlansPlanFeatureHistory.count }.by(1)

      feature_history = PlansPlanFeatureHistory.last
      expect( feature_history.plan_id ).to eq( feature.plan_id )
      expect( feature_history.plan_feature_id ).to eq( feature.plan_feature_id )
      expect( feature_history.change_type ).to eq( PlansPlanFeatureHistory.change_types[:remove] )
    end
  end
end
