require "rails_helper"

describe RenewalItem do

  it "should not be a subscription if its related item is an Album type" do
    single = Single.new
    renewal_item = RenewalItem.new(:related => single)
    expect(renewal_item.is_subscription?).to eq(false)
  end

  it "should not be a subscription if its related item is a Video type" do
    video = Video.new
    renewal_item = RenewalItem.new(:related => video)
    expect(renewal_item.is_subscription?).to eq(false)
  end


  it "should be a subscription if its related item is not a Distributable type" do
    product = Product.new
    renewal_item = RenewalItem.new(:related => product)
    expect(renewal_item.is_subscription?).to eq(true)
  end

end
