require "rails_helper"

describe SalepointPreorderData, "when validating" do
  it "should have a validation error if salepoint_preorder_data is created for a ringtone's salepoint" do
    store = Store.find(36)
    ringtone = FactoryBot.create(:ringtone)
    ringtone_salepoint = TCFactory.create(:salepoint, :store => store, :has_rights_assignment => true,
                                          :salepointable_type => ringtone.class.name, :salepointable_id => ringtone.id)
    salepoint_preorder_data = SalepointPreorderData.new(salepoint: ringtone_salepoint)
    salepoint_preorder_data.valid?
    expect(salepoint_preorder_data.errors[:salepoint].first).to eq I18n.t("model.salepoint_preorder_data.not_supported_type")
  end
end

describe SalepointPreorderData do
  before(:each) do
    @person = create(:person)
    @album = create(:album, person: @person, sale_date: Date.today + 12.days)
    @song1 = create(:song, album: @album)
    @song2 = create(:song, album: @album)
    @song3 = create(:song, album: @album)
    @song4 = create(:song, album: @album)
    salepoint = TCFactory.create(:salepoint, :salepointable_type => "Album", :salepointable_id => @album.id)
    salepoint.update_attribute(:store_id, 36)
    @preorder_purchase = TCFactory.create(:preorder_purchase, paid_at: Time.now)
    @spd = TCFactory.create(:salepoint_preorder_data, salepoint: salepoint, preorder_purchase: @preorder_purchase)
  end

  describe "#update_grat_tracks" do
    it "should add preorder_instant_grat_songs for all ids passed in" do
      expect {
        @spd.update_grat_tracks([@song1.id, @song2.id])
      }.to change(PreorderInstantGratSong, :count).by(2)
    end

    it "should remove preorder_instant_grat_songs that exist, but not in the array of ids" do
      TCFactory.create(:preorder_instant_grat_song, salepoint_preorder_data: @spd, song: @song1)
      expect {
        @spd.update_grat_tracks([])
      }.to change(PreorderInstantGratSong, :count).by(-1)
    end

    it "should not change the rows in the PreorderInstantGratSong model if grat songs match ids passed in" do
      TCFactory.create(:preorder_instant_grat_song, salepoint_preorder_data: @spd, song: @song1)
      expect {
        @spd.update_grat_tracks([@song1.id])
      }.to change(PreorderInstantGratSong, :count).by(0)
    end

    it "should add and remove songs as necessary based on the ids passed in" do
      pigs = TCFactory.create(:preorder_instant_grat_song, salepoint_preorder_data: @spd, song: @song1)
      expect {
        @spd.update_grat_tracks([@song2.id, @song3.id, @song4.id])
      }.to change(PreorderInstantGratSong, :count).by(2)

      expect { pigs.reload }.to raise_error
    end
  end

  describe "#set_track_price" do
    it "should set the correct track level price tier on the associated salepoint" do
      @spd.set_track_price(".69")
      expect(@spd.salepoint.track_variable_price.price.to_f).to eq(0.69)
    end
  end

  describe "#set_salepoint_album_tier" do
    it "should set the correct album tier for preorder with grat tracks (less than 11 tracks)" do
      TCFactory.create(:preorder_instant_grat_song, salepoint_preorder_data: @spd, song: @song1)
      @spd.set_track_price(".99")
      @spd.set_salepoint_album_tier
      expect(@spd.salepoint.variable_price.price.to_f).to eq(2.99)
    end

    it "should set the correct album tier for preorder without grat tracks (less than 11 tracks)" do
      @spd.set_track_price(".99")
      @spd.set_salepoint_album_tier
      expect(@spd.salepoint.variable_price.price.to_f).to eq(3.99)
    end

    it "should set the correct album tier for album with more than 10 tracks with preorder_price greater than 9.99" do
      allow(@spd).to receive(:album_songs_count).and_return(12)
      allow(@spd).to receive(:track_price).and_return(0.99)
      allow(@spd).to receive(:preorder_price).and_return(11.88)
      @spd.set_salepoint_album_tier
      expect(@spd.salepoint.variable_price.price.to_f).to eq(11.99)
    end

    it "should set the correct album tier for album with more than 10 tracks with preorder_price less than 9.99" do
      allow(@spd).to receive(:album_songs_count).and_return(12)
      allow(@spd).to receive(:track_price).and_return(0.99)
      allow(@spd).to receive(:preorder_price).and_return(6.99)
      @spd.set_salepoint_album_tier
      expect(@spd.salepoint.variable_price.price.to_f).to eq(9.99)
    end

    it "should set the price tier for the realse for a single to the Mid/Front tier (price code: 3)" do
      single = FactoryBot.create(:single, person: @person, sale_date: Date.today + 10.days)
      store = Store.find_by(abbrev: "ww")
      salepoint = TCFactory.create(:salepoint, :salepointable_type => "Album", :salepointable_id => single.id, store: store)
      @spd = TCFactory.create(:salepoint_preorder_data, salepoint: salepoint)

      @spd.set_salepoint_album_tier
      expect(@spd.salepoint.variable_price.price_code).to eq("3")
    end
  end

  describe "#validate_grat_track_size" do
    it "should return false if there is an instant grat track when the album song count is 2" do
      @song4.destroy
      @song3.destroy
      TCFactory.create(:preorder_instant_grat_song, salepoint_preorder_data: @spd, song: @song1)
      expect(@spd.validate_grat_track_size).to be(false)
    end

    it "should return true if there are less than (or equal to) half as many instant grat tracks as album songs" do
      TCFactory.create(:preorder_instant_grat_song, salepoint_preorder_data: @spd, song: @song1)
      TCFactory.create(:preorder_instant_grat_song, salepoint_preorder_data: @spd, song: @song2)
      @spd.set_track_price(".99")
      expect(@spd.validate_grat_track_size).to be(true)
    end

    it "should return true if there is one instant grat track and album song count is 3" do
      TCFactory.create(:preorder_instant_grat_song, salepoint_preorder_data: @spd, song: @song1)
      TCFactory.create(:preorder_instant_grat_song, salepoint_preorder_data: @spd, song: @song2)
      TCFactory.create(:preorder_instant_grat_song, salepoint_preorder_data: @spd, song: @song3)
      expect(@spd.validate_grat_track_size).to be(false)
    end

    it "should return true if there are no instant grat tracks" do
      expect(@spd.validate_grat_track_size).to be(true)
    end
  end

  describe "#validate_preorder_start_date" do
    it "should return false if the preorder start_date is nil" do
      @spd.update_attribute(:start_date, nil)
      expect(@spd.validate_preorder_start_date).to be(false)
    end

    it "should return false if the preorder start_date is within 10 days" do
      @spd.update_attribute(:start_date, Date.today + 5.days)
      expect(@spd.validate_preorder_start_date).to be(false)
    end

    it "should return false if the preorder start_date is the same day as the album sale_date" do
      @spd.update_attribute(:start_date, Date.today + 12.days)
      expect(@spd.validate_preorder_start_date).to be(false)
    end

    it "should return true if the preorder start_date at least day before the album sale_date and 10 days after today" do
      @spd.update_attribute(:start_date, Date.today + 11.days)
      expect(@spd.validate_preorder_start_date).to be(true)
    end
  end

  describe "#validate_preorder_pricing with grat tracks" do
    before(:each) do
      #4 tracks at 1.29 = max price 5.16
      @spd.set_track_price("1.29")
      TCFactory.create(:preorder_instant_grat_song, salepoint_preorder_data: @spd, song: @song1)
    end

    it "should return false if the preorder price is nil" do
      @spd.update_attribute(:variable_price_id, nil)
      expect(@spd.validate_preorder_pricing).to be(false)
    end

    it "should return false if the price is greater than number of tracks times track price" do
      @spd.set_preorder_price_tier(5.99)
      expect(@spd.validate_preorder_pricing).to be(false)
    end

    it "should return false if the price is equal than number of tracks times track price" do
      @spd.set_preorder_price_tier(5.16)
      expect(@spd.validate_preorder_pricing).to be(false)
    end

    it "should return false if the price is less than the number of grat tracks times track price + .99" do
      @spd.set_preorder_price_tier(1.99)
      expect(@spd.validate_preorder_pricing).to be(false)
    end

    it "should return true if the price is between the the number of grat tracks times track price + .99 and the number of tracks times track price" do
      @spd.set_preorder_price_tier(3.99)
      expect(@spd.validate_preorder_pricing).to be(true)
    end
  end

  describe "#validate_preorder_pricing without grat tracks" do
    before(:each) do
      @spd.set_track_price("1.29")
    end

    it "should return false if the preorder price exceeds number of tracks times track price" do
      @spd.set_preorder_price_tier(6.17)
      expect(@spd.validate_preorder_pricing).to be(false)
    end

    it "should return false if the preorder price is less than the minimum price by track size" do
      @spd.set_preorder_price_tier(0.99)
      expect(@spd.validate_preorder_pricing).to be(false)
    end

    it "should return true if the preorder price is between the minimum price by track size and less than the number of tracks times the track price" do
      @spd.set_preorder_price_tier(5.16)
      expect(@spd.validate_preorder_pricing).to be(true)
    end

    it "should return true if the album_type is a Single" do
      single = FactoryBot.create(:single, person: @person, sale_date: Date.today + 10.days)
      store = Store.find_by(abbrev: "ww")
      salepoint = TCFactory.create(:salepoint, :salepointable_type => "Album", :salepointable_id => single.id, store: store)
      @spd = TCFactory.create(:salepoint_preorder_data, salepoint: salepoint)
      @song = TCFactory.create(:song, album: single)

      @spd.set_preorder_price_tier(1.29)
      @spd.set_salepoint_album_tier
      @spd.set_track_price("1.29")

      expect(@spd.validate_preorder_pricing).to be(true)
    end
  end

  describe "#preorder_data_errors" do
    before(:each) do
      @spd.set_track_price("1.29")
    end
    it "should return a blank array if preorder_data is valid" do
      @spd.set_preorder_price_tier(3.99)
      expect(@spd.preorder_data_errors.present?).to be(false)
    end

    it "should return an error message if the preorder price is invalid" do
      @spd.set_preorder_price_tier(0.99)
      expect(@spd.preorder_data_errors.present?).to be(true)
    end

    it "should return an error message if the number of grat track is invalid" do
      @spd.update_grat_tracks([@song1.id, @song2.id, @song3.id, @song4.id])
      expect(@spd.preorder_data_errors.present?).to eq(true)
    end

    it "should return an error message if the preorder start date is invalid" do
      @spd.update(start_date: Date.today)
      expect(@spd.preorder_data_errors.present?).to eq(true)
    end

    context "when validating a preorder_data for google it should not validate the pricing" do
      it "should return no error message if an itunes only field is invalid" do
        @spd.salepoint.update_attribute(:store, Store.find_by(short_name: "Google"))
        @spd.update_grat_tracks([@song1.id, @song2.id, @song3.id, @song4.id])
        expect(@spd.preorder_data_errors.present?).to eq(false)
      end

      it "should reutrn an error message if the start date is invalid" do
        @spd.update(start_date: Date.today)
        expect(@spd.preorder_data_errors.present?).to eq(true)
      end
    end
  end

  describe "#preorder_data" do
    it "should return a hash of the preorder data" do
      @spd.update(preview_songs: true, paid_at: Time.now, variable_price_id: 31)
      pigs = TCFactory.create(:preorder_instant_grat_song, salepoint_preorder_data: @spd, song: @song1)

      preorder_data = @spd.preorder_data
      expect(preorder_data[:wholesale_price_tier]).to be(@spd.variable_price.price_code)
      expect(preorder_data[:preorder_start_date]).to be(@spd.start_date)
      expect(preorder_data[:preview_songs]).to be(@spd.preview_songs)
      expect(preorder_data[:instant_grat_tracks]).to eq([pigs.song_id])
    end

    it "should return nil if the preorder is not paid for" do
      @preorder_purchase.update(paid_at: nil)
      expect(@spd.preorder_data).to be(nil)
    end
  end

  describe "#set_preorder_price_tier" do
    it "should set the varaible_price_id of the closest (greater than or equal to) variable_price to the preorder_price" do
      @spd.set_preorder_price_tier(3.99)
      expect(@spd.variable_price.price).to eq(3.99)
    end

    it "should set the price code for the preorder_price_tier for singles to code 3" do
      single = FactoryBot.create(:single, person: @person, sale_date: Date.today + 10.days)
      store = Store.find_by(abbrev: "ww")
      salepoint = TCFactory.create(:salepoint, :salepointable_type => "Album", :salepointable_id => single.id, store: store)
      @spd = TCFactory.create(:salepoint_preorder_data, salepoint: salepoint)
      @spd.set_preorder_price_tier(1.29)
      expect(@spd.variable_price.price_code).to eq("3")
      expect(@spd.variable_price.price).to eq(9.99)
    end
  end

  describe "#preorder_price" do
    #max price for @spd 5.16
    before(:each) do
      @spd.set_track_price("1.29")
    end

    it "should return the max price that the preorder album price would be set to if the tier is 1 tier above the max price" do
      @spd.set_preorder_price_tier(5.16)
      expect(@spd.preorder_price).to eq(5.16)
    end

    it "should return the tier price if the price is less than the max price" do
      @spd.set_preorder_price_tier(3.99)
      expect(@spd.preorder_price).to eq(3.99)
    end

    it "should return the tier price if the price is more than 1 tier above the max price" do
      @spd.set_preorder_price_tier(6.99)
      expect(@spd.preorder_price).to eq(6.99)
    end
  end
end

describe SalepointPreorderData do
  describe "#paid_post_proc" do
    before(:each) do
      album = TCFactory.create(:album)
      store = Store.find_by(abbrev: "ww")
      salepoint = TCFactory.create(:salepoint, :salepointable_type => "Album", :salepointable_id => album.id, store: store)
      @spd = TCFactory.create(:salepoint_preorder_data, salepoint: salepoint)
    end

    it "should update the paid_at value and record a Hubspot event", run_sidekiq_worker: true do
      expect(@spd.paid_at).to eq(nil)

      @spd.paid_post_proc
      expect(@spd.reload.paid_at.nil?).to eq(false)
    end
  end

  describe "#is_enabled?" do
    before(:each) do
      @album = FactoryBot.create(:album)
      @preorder_purchase = FactoryBot.create(:preorder_purchase, album: @album, itunes_enabled: false, google_enabled: false)
    end

    context "salepoint_preorder_data for itunes" do
      before(:each) do
        itunes_store = Store.find_by(short_name: "iTunesWW")
        FactoryBot.create(:salepointable_store, store: itunes_store)
        @itunes_salepoint = FactoryBot.create(:salepoint, salepointable: @album, store: itunes_store, variable_price: itunes_store.variable_prices.first)
        @itunes_spd = FactoryBot.create(:salepoint_preorder_data, salepoint: @itunes_salepoint, preorder_purchase: @preorder_purchase)
      end

      context "when the related preorder_purchase is set to itunes_enabled = false" do
        it "should return false" do
          expect(@itunes_spd.is_enabled?).to eq(false)
        end
      end

      context "when the related preorder_purchase is set to itunes_enabled = true" do
        before(:each) do
          @preorder_purchase.update(itunes_enabled: true)
        end

        it "should return true" do
          expect(@itunes_spd.is_enabled?).to eq(true)
        end
      end
    end

    context "salepoint_preorder_data for google" do
      before(:each) do
        google_store = Store.find_by(short_name: "Google")
        FactoryBot.create(:salepointable_store, store: google_store)
        @google_salepoint = FactoryBot.create(:salepoint, salepointable: @album, store: google_store)
        @google_spd = FactoryBot.create(:salepoint_preorder_data, salepoint: @google_salepoint, preorder_purchase: @preorder_purchase)
      end

      context "when the related preorder_purchase is set to google_enabled = false" do
        it "should return false" do
          expect(@google_spd.is_enabled?).to eq(false)
        end
      end

      context "when the related preorder_purchase is set to google_enabled = true" do
        before(:each) do
          @preorder_purchase.update(google_enabled: true)
        end

        it "should return true" do
          expect(@google_spd.is_enabled?).to eq(true)
        end
      end
    end
  end

  describe "#valid_preorder_data?" do
    before(:each) do
      @album = FactoryBot.create(:album, sale_date: Date.today + 10.days)
      @preorder_purchase = FactoryBot.create(:preorder_purchase, album: @album)
    end

    context "salepoint_preorder_data for itunes" do
      before(:each) do
        itunes_store = Store.find_by(short_name: "iTunesWW")
        FactoryBot.create(:salepointable_store, store: itunes_store)
        @itunes_salepoint = FactoryBot.create(:salepoint, salepointable: @album, store: itunes_store, variable_price: itunes_store.variable_prices.first)
        @itunes_spd = FactoryBot.create(:salepoint_preorder_data, salepoint: @itunes_salepoint, preorder_purchase: @preorder_purchase)
      end

      it "should call #validate_start_date, #validate_preorder_pricing, and #validate_grat_track_size" do
        expect(@itunes_spd).to receive(:validate_preorder_start_date).and_return(true)
        expect(@itunes_spd).to receive(:validate_preorder_pricing).and_return(true)
        expect(@itunes_spd).to receive(:validate_grat_track_size).and_return(true)
        @itunes_spd.valid_preorder_data?
      end
    end

    context "salepoint_preorder_data for google" do
      before(:each) do
        google_store = Store.find_by(short_name: "Google")
        FactoryBot.create(:salepointable_store, store: google_store)
        @google_salepoint = FactoryBot.create(:salepoint, salepointable: @album, store: google_store)
        @google_spd = FactoryBot.create(:salepoint_preorder_data, salepoint: @google_salepoint, preorder_purchase: @preorder_purchase)
      end

      it "should only call #validate_start_date" do
        expect(@google_spd).to receive(:validate_preorder_start_date).and_return(true)
        expect(@google_spd).not_to receive(:validate_preorder_pricing)
        expect(@google_spd).not_to receive(:validate_grat_track_size)
        @google_spd.valid_preorder_data?
      end
    end
  end

  describe ".by_store" do
    it "returns the salepoint preorder data associated to the store short name" do
      salepoint = create(:salepoint, :itunes)
      salepoint_preorder_data = create(:salepoint_preorder_data, salepoint: salepoint)

      result = SalepointPreorderData.by_store("iTunesWW").first

      expect(result).to eq salepoint_preorder_data
    end

    it "returns an empty array if there is no salepoint preorder data associated to the store short name" do
      result = SalepointPreorderData.by_store("Google")

      expect(result).to be_empty
    end
  end
end
