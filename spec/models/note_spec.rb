require "rails_helper"

describe Note do
  before(:each) do
    @person = Person.find(43)
    @valid_attributes = {:subject => "Test", :note => "Test", :related => @person, :note_created_by_id => 42}
  end

  it "should create a new instance given valid attributes" do
    Note.create!(@valid_attributes)
  end
  
  it "should update 'note_created_by' if note is created for user while taken over" do
    @person.takeover_admin_id = 1
    note = Note.create(subject: "Test", note: "Test", related: @person, note_created_by: @person)
    expect(note.note_created_by_id).to be(1)
  end
end
