require "rails_helper"

describe TerminatedComposer do
  describe "#account" do
    it "returns the composer's account" do
      account = build(:account)
      composer = build(:composer, account: account)
      terminated_composer = build(:terminated_composer, :fully, composer: composer)
      expect(terminated_composer.account).to eq account
    end
  end

  describe "#fully_terminated?" do
    before :each do
      @fully_terminated     = build(:terminated_composer, :fully)
      @partially_terminated = build(:terminated_composer, :partially)
    end

    it "returns a boolean for whether the terminated composer is fully terminated or not" do
      expect(@fully_terminated.fully_terminated?).to be true
      expect(@partially_terminated.fully_terminated?).to be false
    end
  end

  describe "#partially_terminated?" do
    before :each do
      @fully_terminated     = build(:terminated_composer, :fully)
      @partially_terminated = build(:terminated_composer, :partially)
    end

    it "returns a boolean for whether the terminated composer is partially terminated or not" do
      expect(@fully_terminated.partially_terminated?).to be false
      expect(@partially_terminated.partially_terminated?).to be true
    end
  end

  describe "#create_alt_provider_acct_id" do
    it "invokes the PublishingAdministration::TerminatedAccountWorker if partially terminated" do
      expect(PublishingAdministration::TerminatedAccountWorker)
        .to receive(:perform_async)
        .and_return true

      create(:terminated_composer, :partially)
    end
  end

  describe "#reactivate_composer_pub_admin_status" do
    it "nullifies the composer's terminated_at" do
      composer = create(:composer, terminated_at: Date.today)
      terminated_composer = create(:terminated_composer, :fully, composer: composer)

      terminated_composer.destroy

      expect(composer.reload.terminated_at).to eq nil
    end
  end

  describe "update_composer_pub_admin_status" do
    it "updates the composer's terminated_at" do
      composer = create(:composer)
      create(:terminated_composer, :fully, composer: composer)

      expect(composer.reload.terminated_at.present?).to be true
    end
  end
end
