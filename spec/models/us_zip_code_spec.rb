require "rails_helper"

describe UsZipCode, "basic functionality" do

  it "should provide the city and state, separated by a comma" do
    code = UsZipCode.new(:city => "Sedalia", :state => "MO")
    expect(code.city_and_state).to eq("Sedalia, MO")
  end

end
