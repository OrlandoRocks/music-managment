require 'rails_helper'

RSpec.describe CopyrightDocument, type: :model do
  describe ".upload_document" do
    context "successful upload of document" do
      it "should upload and save copyright document" do
        pdf_file = Rack::Test::UploadedFile.new("spec/assets/booklet.pdf", 'application/pdf')
        expect(CopyrightDocument.count).to eq(0)
        album = create(:album)
        document = CopyrightDocument.upload_document(album, pdf_file)
        expect(document).to eq(true)
        expect(CopyrightDocument.count).to eq(1)
      end
    end

    context "unsuccessful upload of document" do
      it "should upload and save copyright document" do
        pdf_file = Rack::Test::UploadedFile.new("spec/assets/booklet.pdf", 'application/pdf')
        expect(CopyrightDocument.count).to eq(0)
        allow_any_instance_of(S3Asset).to receive(:put!).and_raise(StandardError)
        album = create(:album)
        document = CopyrightDocument.upload_document(album, pdf_file)
        expect(document).to eq(false)
        expect(CopyrightDocument.count).to eq(0)
      end
    end
  end

  describe ".s3_key"do
    it "should return path based on environment" do
      album = create(:album)
      copyright_document = create(:copyright_document, related: album)
      expect(copyright_document.s3_key('document1')).to eq("indiapublishing/test/#{album.person.id}/#{album.id}/document1")
    end
  end
end
