require "rails_helper"

describe Lyric do
  describe "#before_save" do
    let(:song)  { create(:song, lyric: Lyric.create) }
    let(:lyric) { song.lyric }

    context "when a lyrics content is provided" do
      let(:expected_content) { "First line\nSecond line\nThird line" }

      it "capitalizes the first letter of each new line" do
        lyric.update_attribute(:content, "first line\nsecond line\nthird line")
        expect(lyric.content).to eq(expected_content)
      end
    end
  end

  describe 'four byte char validation' do
    let(:lyric) { build(:lyric, content: "💿💿💿💿💿") }

    it 'validates first_name and last_name for four byte chars' do
      lyric.valid?
      expect(lyric.errors.messages[:content].size).to be 1
    end
  end
end
