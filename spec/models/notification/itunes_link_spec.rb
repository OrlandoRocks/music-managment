require "rails_helper"

describe ItunesLinkNotification do
  describe ".create_notifications" do
    let(:album) do
      create(
        :album,
        apple_identifier: "123456789",
        orig_release_year: Date.today - 1.year
      )
    end

    it "creates an itunes notification for albums that were just marked live and whose sale_date has passed" do
      album.known_live_on = Date.today
      album.sale_date = Date.today - 1.day
      album.save!

      expect {
        ItunesLinkNotification.create_notifications
      }.to change(ItunesLinkNotification, :count).by(1)
    end

    it "creates an itunes notification for albums already known live on their sale_date" do
      album.known_live_on = Date.today - 1.day
      album.sale_date = Date.today
      album.save!

      expect {
        ItunesLinkNotification.create_notifications
      }.to change(ItunesLinkNotification, :count).by(1)
    end

    it "does not create an itunes notification for albums that that are known live today but not past the sale date" do
      album.known_live_on = Date.today
      album.sale_date = Date.today + 1.day
      album.save!

      expect {
        ItunesLinkNotification.create_notifications
      }.not_to change(ItunesLinkNotification, :count)
    end

    it "only creates notifications for albums with an apple_identifier" do
      album.known_live_on = Date.today
      album.sale_date = Date.today - 1.day
      album.apple_identifier = nil
      album.save!

      expect {
        ItunesLinkNotification.create_notifications
      }.not_to change(ItunesLinkNotification, :count)
    end

    it "does not allow a notification for the same album more than once" do
      album.known_live_on = Date.today
      album.sale_date = Date.today - 1.day
      album.save!

      expect {
        ItunesLinkNotification.create_notifications
      }.to change(ItunesLinkNotification, :count).by(1)

      expect {
        ItunesLinkNotification.create_notifications
      }.not_to change(ItunesLinkNotification, :count)
    end

    it "does not create notifications for ringtones" do
      album.known_live_on = Date.today
      album.sale_date = Date.today - 1.day
      album.update_attribute(:album_type, "Ringtone")
      album.save!

      expect {
        ItunesLinkNotification.create_notifications
      }.not_to change(ItunesLinkNotification, :count)
    end
  end

  describe '#setup_system_notification' do
    let(:notification) do
      ItunesLinkNotification.create(
        person: album.person,
        notification_item: album
      )
    end
    let(:album) do
      create(
        :album,
        apple_identifier: "123456789",
        orig_release_year: Date.today - 1.year
      )
    end

    before do
      create(:artwork, album: album)
    end

    it "sets system_message_type to ItunesLinkNotification" do
      expect(notification.type)
        .to eq(ItunesLinkNotification.name)
    end

    it "creates text based on what the release is missing" do
      expect(notification.text)
        .not_to be_blank
    end

    it "sets a url" do
      expect(notification.url)
        .to eq("/albums/#{album.id}")
    end

    it "sets a title" do
      expect(notification.title)
        .to eq("iTunes link available")
    end

    it "sets link_text" do
      expect(notification.link_text)
        .to eq("Click here to see your iTunes link")
    end

    it "sets an image_url" do
      expect(notification.image_url)
        .not_to be_blank
    end
  end
end
