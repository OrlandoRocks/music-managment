require "rails_helper"

describe YtmBlockedSong do
  describe "#create" do
    context "when its song has a salepoint_song" do
      it "rejects the salepoint_song" do
        album = create(:album, :with_songs)
        song = create(:song, album: album)
        store = Store.find_by(id: Store::YTSR_STORE_ID)
        salepoint = create(:salepoint, salepointable: album, store: store)
        salepoint_song = create(:salepoint_song, salepoint: salepoint, song: song)

        expect_any_instance_of(YtmBlockedSong).to receive(:reject_salepoint_song).and_return(true)

        YtmBlockedSong.create(song: song)
      end

      context "when its salepoint_song has a distribution_song" do
        it "takes down that salepoint_song" do
          album = create(:album, :with_songs)
          song = create(:song, album: album)
          store = Store.find_by(id: Store::YTSR_STORE_ID)
          salepoint = create(:salepoint, salepointable: album, store: store)
          salepoint_song = create(:salepoint_song, salepoint: salepoint, song: song)
          distribution_song = create(:distribution_song, salepoint_song: salepoint_song)
          
          expect_any_instance_of(YtmBlockedSong).to receive(:takedown_salepoint_song).and_return(true)

          YtmBlockedSong.create(song: song)
        end
      end

      context "when its salepoint_song has no distribution_song" do
        it "does not take down its salepoint_song" do
          album = create(:album, :with_songs)
          song = create(:song, album: album)
          store = Store.find_by(id: Store::YTSR_STORE_ID)
          salepoint = create(:salepoint, salepointable: album, store: store)
          salepoint_song = create(:salepoint_song, salepoint: salepoint, song: song)

          expect_any_instance_of(YtmBlockedSong).to_not receive(:takedown_salepoint_song)

          YtmBlockedSong.create(song: song)
        end
      end
    end

    context "when its song has no salepoint_song" do
      it "does not initiate its rejection callback" do
        album = create(:album, :with_songs)
        song = create(:song, album: album)
        store = Store.find_by(id: Store::YTSR_STORE_ID)
        salepoint = create(:salepoint, salepointable: album, store: store)

        expect_any_instance_of(YtmBlockedSong).to_not receive(:reject_salepoint_song)

        YtmBlockedSong.create(song: song)
      end

      it "does not initiate its takedown callback" do
        album = create(:album, :with_songs)
        song = create(:song, album: album)
        store = Store.find_by(id: Store::YTSR_STORE_ID)
        salepoint = create(:salepoint, salepointable: album, store: store)
        
        expect_any_instance_of(YtmBlockedSong).to_not receive(:takedown_salepoint_song)

        YtmBlockedSong.create(song: song)
      end
    end
  end

  describe "#destroy" do
    context "when its song has a salepoint_song" do
      context "when its salepoint_song has a distribution_song" do
        it "initiates a salepoint_song takedown" do
          album = create(:album, :with_songs)
          song = create(:song, album: album)
          store = Store.find_by(id: Store::YTSR_STORE_ID)
          salepoint = create(:salepoint, salepointable: album, store: store)
          salepoint_song = create(:salepoint_song, salepoint: salepoint, song: song)
          distribution_song = create(:distribution_song, salepoint_song: salepoint_song)

          YtmBlockedSong.create(song: song)

          song.reload

          expect_any_instance_of(YtmBlockedSong).to receive(:remove_salepoint_song_takedown).and_return(true)

          song.ytm_blocked_song.destroy
        end
      end

      context "when its salepoint_song has no distribution_song" do
        it "does not initiate a salepoint_song takedown" do
          album = create(:album, :with_songs)
          song = create(:song, album: album)
          store = Store.find_by(id: Store::YTSR_STORE_ID)
          salepoint = create(:salepoint, salepointable: album, store: store)
          salepoint_song = create(:salepoint_song, salepoint: salepoint, song: song)

          YtmBlockedSong.create(song: song)

          song.reload

          expect_any_instance_of(YtmBlockedSong).to_not receive(:remove_salepoint_song_takedown)

          song.ytm_blocked_song.destroy
        end
      end
    end

    context "when its song has no salepoint_song" do
      it "does not initiate a salepoint_song takedown" do
        album = create(:album, :with_songs)
        song = create(:song, album: album)
        store = Store.find_by(id: Store::YTSR_STORE_ID)
        salepoint = create(:salepoint, salepointable: album, store: store)

        YtmBlockedSong.create(song: song)

        song.reload

        expect_any_instance_of(YtmBlockedSong).to_not receive(:remove_salepoint_song_takedown)

        song.ytm_blocked_song.destroy
      end
    end
  end
end
