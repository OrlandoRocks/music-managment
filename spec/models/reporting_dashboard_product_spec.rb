require "rails_helper"

describe ReportingDashboardProduct do
  describe ".calculate_daily_values" do
    it "creates a `reporting_dashboard_product` for every product" do
      ReportingDashboardProduct.calculate_daily_values(Date.today)

      expect(ReportingDashboardProduct.count).to eq Product.count
    end

    it "updates the attributes for each created 'reportingdashboardproduct'" do
      product = create(:product, product_family: "Renewal")

      ReportingDashboardProduct.calculate_daily_values(Date.today)

      reporting_dashboard_product = ReportingDashboardProduct.where(product_id: product.id).first
      expect(reporting_dashboard_product.country_website_id).to eq product.country_website_id
      expect(reporting_dashboard_product.product_id).to eq product.id
      expect(reporting_dashboard_product.product_name).to eq product.name
      expect(reporting_dashboard_product.product_family).to eq product.product_family
      expect(reporting_dashboard_product.product_count).to eq 0
      expect(reporting_dashboard_product.product_amount).to eq 0.00
    end
  end
end
