require "rails_helper"

describe Genre do
  let(:parent_genre) { create(:genre) }
  let(:subgenre) { create(:genre, parent_id: parent_genre.id) }

  describe "#parent_genre" do
    it "should return the parent genre for a given subgenre" do
      expect(subgenre.parent_genre).to eq(parent_genre)
    end
  end

  describe "#subgenres" do
    it "should return all subgenres for a parent genre" do
      expect(subgenre.parent_genre).to eq(parent_genre)
    end
  end

  describe "global" do
    it "should not return country-specific genres" do
      india_genre = Genre.find_by(name: 'Indian')
      global_gs = Genre.global

      global_gs.each do |gre|
        expect(gre.name).not_to eq('Indian')
        expect(gre.parent_id).not_to eq(india_genre.id)
      end
    end
  end
end
