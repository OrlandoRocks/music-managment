require "rails_helper"

describe RenewalHistory do

  before(:each) do
    @renewal_history = RenewalHistory.new
    @renewal = Renewal.new
    @renewal_history.renewal = @renewal
  end

  it "should be false with history" do
    allow(@renewal).to receive(:has_history?).and_return(true)
    allow(@renewal).to receive(:has_first_renewal_settings?).and_return(true)
    expect(@renewal_history.send(:should_use_first_renewal_interval?)).to eq(false)
  end

  it "should be false without first renewal settings" do
    allow(@renewal).to receive(:has_history?).and_return(false)
    allow(@renewal).to receive(:has_first_renewal_settings?).and_return(false)
    expect(@renewal_history.send(:should_use_first_renewal_interval?)).to eq(false)
  end

  it "should be true without history and first renewal settings" do
    allow(@renewal).to receive(:has_history?).and_return(false)
    allow(@renewal).to receive(:has_first_renewal_settings?).and_return(true)
    expect(@renewal_history.send(:should_use_first_renewal_interval?)).to eq(true)
  end

end

describe RenewalHistory, "Calculating expire dates" do
  before do
    travel_to Time.local(2020)
  end

  after do
    travel_back
  end

    let!(:product) { Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID) }
    let!(:renewal_product_item) { TCFactory.create(:product_item,  :product=>product, :first_renewal_interval=>"year", :first_renewal_duration =>1, :renewal_interval => "year", :renewal_duration =>1) }
    let!(:person) { TCFactory.create(:person) }
    let!(:album) { TCFactory.create(:album, :person => person) }
    let!(:purchase) { TCFactory.create(:purchase, :person_id=>person.id, :related=>album, :product=>product) }
    let!(:renewal) { Renewal.create(:person=> person,:item_to_renew=>renewal_product_item, :purchase => purchase) }

  it "should create a history line item with a start_date of monday close to today and an expiration a year from now" do
    renewal_history = renewal.history.last
    expect(renewal_history).not_to be_nil
    expect(renewal_history.starts_at).to eq(Time.now.at_midnight)
    expect(renewal.expires_at).to eq(Time.now.at_midnight + 1.year)
  end

  context "Renewal purchase " do
    it "should create a history line item with a start_date of monday close to today and an expiration a year from now" do
      purchase = TCFactory.create(:purchase, :person_id=>person.id, :related=>renewal, :product=>product)
      renewal.renew!(purchase)
      renewal_history_first = renewal.history.first
      renewal_history = renewal.reload.history.last
      expect(renewal_history.starts_at).to eq(renewal_history_first.expires_at)
      expect(renewal.expires_at).to eq(renewal_history_first.expires_at + 1.year)
    end

    it "should create a history line item with a start_date of monday close to today and an expiration a year from now" do
      #update renewal to have an old expiration date
      renewal_history_first =  renewal.history.first
      renewal_history_first.update(:expires_at=>Time.parse("2010-01-01"),
      :starts_at=>Time.parse("2009-01-01"))
      expect(renewal.expires_at).to eq(Time.parse("2010-01-01"))

      #purchase a renewal
      purchase = TCFactory.create(:purchase, :person_id=>person.id, :related=>renewal, :product=>product)
      renewal.renew!(purchase)
      renewal_history = renewal.history.last
      expect(renewal_history.starts_at).to  eq(renewal_history_first.expires_at)
      expect(renewal.reload.expires_at).to eq(Time.now.at_midnight + 1.year)
    end
  end

end
