require "rails_helper"
require_relative "./petri_interface_shared"
require_relative "./blank_artist_shared"

describe Single, "implementing an interface to PETRI" do
  before(:each) do
    @distro = FactoryBot.create(:single)
  end

  it_should_behave_like "PETRI interface"
end

describe Single, "Using single table inheritance" do
  before(:each) do
    @single = FactoryBot.create(:single)
  end

  it "Should have the type single" do
    expect(@single.album_type).to eq("Single")
  end

  it "Should be found by album" do
    #
    # Be sure to set the inheritance_column for
    # ActiveRecord's STI inheritance on both the
    # Album and the Single model
    #
    assert single = Album.find(@single.id)
    assert single.is_a?(Single)
  end

  describe "#new" do
    it "sets the language code of the song if language_code option passed in" do
      lc = LanguageCode.find_by(code: "it")
      single = Single.new(language_code: lc.code)
      expect(single.song.metadata_language_code).to eq lc
    end

    it "sets the language code of the song if language_code_legacy_support option passed in" do
      lc = LanguageCode.find_by(code: "it")
      single = Single.new(language_code_legacy_support: lc.code)
      expect(single.song.metadata_language_code).to eq lc
    end

    it "doesn't set the language code of the song if no option passed" do
      single = Single.new
      expect(single.song.metadata_language_code).to be_nil
    end
  end
end

describe Single do
  before(:each) do
    @single = create(:single, creatives: [{ "name" => "artist", "role" => "primary_artist" }, { "name" => "", "role" => "featuring" }])
    @single.song = create(:song, album: @single)
  end

  it "does not allow a compliation single" do
    @single.is_various = true
    expect(@single.valid?).to be_falsey
  end

  it "should only allow one song" do
    2.times do
      @single.songs << build(:song, album: @single)
    end

    expect(@single.valid?).to eq(false)
  end

  it "must have one song to be valid" do
    @single.songs.clear
    expect(@single.valid?).to eq(false)
  end

  it "should be valid from factory" do
    expect(@single.valid?).to eq(true)
  end

  it "should take an isrc for its song" do
    @single.respond_to?(:optional_isrc=)
  end

  it "should not be valid if the optional_isrc is not correctly formatted" do
    @single.optional_isrc = 'not-valid'
    expect(@single.valid?).to eq(false)
    expect(@single.errors.messages[:optional_isrc].size).to eq 1
  end

  it "should take a value for explicit lyrics/parental advisory for its song" do
    @single.respond_to?(:parental_advisory=)
  end
end

describe Single, "when creatives are the same at song and album levels for singles" do
  before(:each) do
    @single = FactoryBot.create(
      :single,
      creatives: [{ "name" => "artist", "role" => "primary_artist" }],
      name: 'Idol Dalliance'
    )
    @single.song.creatives << Creative.new(name: "artist", role: 'primary_artist')
    @single.save
    @single.reload
  end

  it 'should have 1 songs' do
    expect(@single.songs.size).to eq(1)
  end

  it 'should have 1 creative at the album level and 0 creatives at the song level' do
    expect(@single.creatives.size).to eq(1)
    expect(@single.songs.first.creatives.size).to eq(0)
  end
end

describe Single, " when it is a single" do
  before(:each) do
    @person = TCFactory.build_person(name: 'trevor', email: 'trevor@protocool.net')
    @single = FactoryBot.create(:single,
                                 person: @person,
                                 name: 'My Cool Album',
                                 creatives: [{ "name" => "My Selected Artist", "role" => "primary_artist" }])

    @song = @single.song
  end

  it 'should create a song with the extact same title' do
    assert @single.valid?
    assert @single.save!
    assert @single.songs.first.valid?
    assert @single.songs.first.save!

    assert @single.reload

    expect(@single.songs.size).to eq(1)
    expect(@song.name).to eq('My Cool Album')
  end

  it "should create a song with the same title when the title contains (feat)" do
    single = create(:single, name: "My Single (feat. Sophie)")
    expect(single.song.name).to eq("My Single (feat. Sophie)")
  end

  it "should be able to find songs when reloaded" do
    song = @single.songs.first
    song.valid?
    assert song.valid?

    @single.reload
    assert !@single.songs.empty?
  end

  it 'should update the title of the song if the title of the album is changed' do
    @single.name = "Hello There"
    expect(@single.name).to eq('Hello There')
    !@single.songs.empty?

    assert @single.valid?
    assert @single.save!
    assert @single.songs.first.valid?
    assert @single.songs.first.save!

    assert @single.reload

    assert @single.is_a?(Single)
    assert expect(@single.name).to eq('Hello There')
    assert !@single.songs.empty?

    assert !@song.reload.nil?, "Song is nil and should not be"
    expect(@song.name).to eq('Hello There')
  end
end

describe Single, "when setting an artist" do
  it 'should return an artist object' do
    @single = FactoryBot.create(:single,
                                 name: 'My Cool Album')

    @single.creatives << Creative.new(name: "OMG Michelle", role: "primary_artist")
    expect(@single.valid?).to be_truthy

    creative = @single.creatives.joins(:artist).find_by(artists: { name: "OMG Michelle" })
    expect(creative).not_to be_nil
    expect(creative.artist.class).to eq(Artist)
  end

  it 'should not titleize the artist' do
    @single = FactoryBot.create(:single,
                                 allow_different_format: true,
                                 name: 'My Cool Album')

    @single.creatives << Creative.new(name: "OMG michelle", role: "primary_artist")
    @single.save

    creative = @single.creatives.joins(:artist).find_by(artists: { name: "OMG michelle" })
    expect(creative).not_to be_nil
    expect(creative.name).to eq('OMG michelle')
  end
end

describe Single, "when it has duplicate featuring artists" do
  before(:each) do
    @single = FactoryBot.create(:single,
                                 creatives: [{ "name" => "Philip J. Fry", "role" => "primary_artist" }, { "name" => "Turanga Leela", "role" => "primary_artist" }, { "name" => "Turanga Leela", "role" => "primary_artist" }],
                                 name: 'Orphan of the Stars')
    @song = @single.song
  end

  it "should not create the song with duplicate artists" do
    expect(@single.reload.creatives.size).to eq(2)
    expect(@single.creatives[1].name).to eq("Turanga Leela")
    expect(@single.creatives[2]).to eq(nil)
    expect(@single.name).to eq('Orphan of the Stars')
  end

  it "should not add duplicate artists to a song" do
    @song.add_featured_artist('Turanga Leela')
    @song.save
    expect(@single.reload.creatives.size).to eq(2)
    expect(@single.creatives[1].name).to eq("Turanga Leela")
    expect(@single.creatives[2]).to eq(nil)
    expect(@single.name).to eq('Orphan of the Stars')
  end
end

describe Single, "when setting featured artists on a single album" do
  before(:each) do
    @single = FactoryBot.create(:single,
                                 creatives: [{ "name" => "artist", "role" => "primary_artist" }],
                                 name: 'Idol Dalliance')

    @single.song = TCFactory.build(:song)
    @single.save

    @single.add_featured_artist('Bobo Baggins')
    artist = Artist.find_or_create_by(name: "Artist Name")
    @single.creatives << Creative.new(role: 'primary_artist', artist: artist)
    @song = @single.song
  end

  it 'does NOT automatically set the title of the album and song to include (feat. name)' do
    assert @single.is_a?(Single)
    assert @single.save == true
    expect(@single.reload.name).to eq('Idol Dalliance')
    expect(@song.reload.name).to eq('Idol Dalliance')
  end

  it 'should not change the title of the single song and its own title to include (feat. name) when album#name is changed' do
    @single.name = "Hello Drawer!"
    @single.save
    expect(@single.reload.name).to eq('Hello Drawer!')
    @single.valid?
    expect(@song.reload.name).to eq('Hello Drawer!')
  end

  it 'should change the name of the song or the album if allow_different_format is selected' do
    @single.allow_different_format = true
    @single.save # We save it here so that the song will also be allow_different_format before we set the name
    @single.reload # otherwise the song will capitalize the name before it knows not to do so.
    @song.reload
    @single.name = "Am I capitalized?"
    @single.save
    @single.reload
    @song.reload
    expect(@single.name).to eq('Am I capitalized?')
    expect(@song.name).to eq('Am I capitalized?')
  end
end

describe Single do
  before(:each) do
    @product = FactoryBot.create(:single,
                                  allow_different_format: false,
                                  name: 'My Cool Single')
  end

  it_should_behave_like "testing blank artists"
end

describe Single do
  before(:each) do
    @product = FactoryBot.create(:single,
                                  allow_different_format: false,
                                  name: 'My Cool Single')
    @product.creatives << Creative.new(name: "Robin", role: "primary_artist")
    @product.creatives << Creative.new(name: "Bob", role: "featuring")
    @product.save!
  end

  it_should_behave_like "testing updating with blank artists"
end

describe Single do
  before(:each) do
    @single = create(:single, :with_salepoints, :with_artwork, :with_songwriter)
    upload = create(:upload, song: @single.song)
    @single.song.upload = upload
  end

  describe "#paid_post_proc" do
    it "should set the finalized_at" do
      expect(@single.finalized?).to eq false
      @single.paid_post_proc
      expect(@single.reload.finalized?).to eq true
    end

    it "should set the payment_applied to true" do
      expect(@single.payment_applied).to eq false
      @single.paid_post_proc
      expect(@single.reload.payment_applied).to eq true
    end
  end

  describe "#creatives=" do
    let(:person) { create(:person) }
    let(:single) { create(:single, person_id: person.id) }

    it "only allows a primary artist to be added" do
      single.creatives = [
        { role: "primary_artist", name: "Charlie" }.with_indifferent_access,
        { role: "featuring", name: "Frank" }.with_indifferent_access
      ]

      single.save

      single.reload
      expect(single.creatives.size).to eq 1
    end
  end

  describe "#golive_date" do
    context "in a timed_release context" do
      context "invalid golive_date's" do
        context "when saving a single" do
          it "should indicate it's invalid" do
            @album = build(:single, golive_date: 123123123)
            @album.save

            expect(@album.errors.empty?).to be(false)
          end
        end
      end

      context "valid golive_dates's" do
        context "when saving a single" do
          it "should indicate it's valid" do
            @album = build(:single, golive_date: Time.now + 1.days)
            allow(FeatureFlipper).to receive(:show_feature?).with(:crt_cache, nil).and_return(true)
            @album.save

            expect(@album.errors.empty?).to be(true)
          end
        end
      end

      context "without a golive date, but with a sale_date" do
        it "should add the golive_date to be whatever the sale_date is and set the timed release timing scenario" do
          this_sale_date = Date.new(2019, 6, 1)

          single = build(:album, sale_date: this_sale_date, golive_date: nil, timed_release_timing_scenario: nil)
          allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
          single.save
          single.name = single.name + ': a timed release album'

          single.save

          expect(single.golive_date.in_time_zone('Eastern Time (US & Canada)').to_date).to eq(this_sale_date)
          expect(single.timed_release_timing_scenario).to eq('relative_time')
        end
      end
    end

    context "not in a timed_release context" do
      context "invalid golive_date's" do
        context "when saving a single" do
          it "should indicate it's valid" do
            @album = build(:single, golive_date: 123123123)
            @album.save

            expect(@album.errors.empty?).to be(false)
          end
        end
      end
    end
  end

  describe '#parental_advisory_enabled?' do
    let(:single) { create(:single) }

    subject { single.parental_advisory_enabled? }

    context "no song present" do
      before { allow(single).to receive(:song).and_return(nil) }

      it { should be_falsey }
    end

    context "song present" do
      it "should return advisory value set on the song" do
        song = create(:song, album: single)

        expect(subject).to eq(song.parental_advisory)
      end
    end
  end

end
