require "rails_helper"

describe MassAdjustmentEntry do
  let!(:person) { create(:person, :with_balance, amount: 50_000) }
  let!(:batch) { create(:mass_adjustment_batch) }
  let!(:positive_entry) { create(:mass_adjustment_entry, person: person, mass_adjustment_batch: batch, status: MassAdjustmentEntry::SUCCESS, amount: 5000) }
  let!(:negative_entry) { create(:mass_adjustment_entry, person: person, mass_adjustment_batch: batch, status: MassAdjustmentEntry::SUCCESS, amount: -100000) }
  let!(:unprocessed_entry) { create(:mass_adjustment_entry, person: person, mass_adjustment_batch: batch, status: nil) }
  let!(:errored_entry) { create(:mass_adjustment_entry, person: person, mass_adjustment_batch: batch, status: MassAdjustmentEntry::ERROR) }
  let!(:cancelled_entry) { create(:mass_adjustment_entry, person: person, mass_adjustment_batch: batch, status: MassAdjustmentEntry::CANCELLED) }


  describe "scopes" do
    context "unprocessed" do
      it "returns entries with a nil status" do
        expect(MassAdjustmentEntry.unprocessed).to eq([unprocessed_entry])
      end
    end

    context "successful_entries" do
      it "returns entries with a success status" do
        expect(MassAdjustmentEntry.successful_entries).to eq([positive_entry, negative_entry])
      end
    end
    context "errored_entries" do
      it "returns entries with an errored status" do
        expect(MassAdjustmentEntry.errored_entries).to eq([errored_entry])
      end
    end

    context "cancelled" do
      it "returns entries with a cancelled status" do
        expect(MassAdjustmentEntry.cancelled_entries).to eq([cancelled_entry])
      end
    end
  end

  context "#account_goes_negative?" do
    it "returns true if the account's balance will go negative if the entry is processed" do
      expect(negative_entry.account_goes_negative?).to be_truthy
    end

    it "returns false if the account's balance will stay positive if the entry is processed" do
      expect(positive_entry.account_goes_negative?).to be_falsey
    end
  end
end
