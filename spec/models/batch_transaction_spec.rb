require "rails_helper"

describe BatchTransaction do
  describe "#process" do
    before(:each) do
      allow(FeatureFlipper).to receive(:show_feature?).with(any_args).and_call_original
      expect(FeatureFlipper).to receive(:show_feature?).with(:try_multiple_payment_methods, any_args).and_return(false)
      @invoices      = []
      @payment_batch = TCFactory.create(:payment_batch)
      @payment_batch.initialize_batch_counts
      @invoices << Invoice.create(person: TCFactory.create(:person), batch_status: "waiting_for_batch")
      product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
      album   = TCFactory.create(:album, person: @invoices.first.person)
      @purchase      = TCFactory.create(:purchase, invoice: @invoices.first, person: @invoices.first.person, related: album, product: product)
      @person        = @invoices.first.person
    end

    it "should process with balance if person accepts balance and has enough" do
      @person.person_balance.update balance: @purchase.cost_cents / 100 + 1
      @transactions = @payment_batch.create_transactions_for_invoices(@invoices)

      expect(@transactions.first).to receive(:process_with_balance)
      expect(@transactions.first.build_payment_options).to eq([[:balance]])

      @transactions.first.process
    end

    it "should process with braintree cc if person does not accept balance but has credit card" do
      mock_braintree_query
      @card = create(:stored_credit_card, person_id: @person.id, expiration_year: Date.today.year + 1)
      @person.build_person_preference(pay_with_balance: 0, preferred_payment_type: "CreditCard", preferred_credit_card_id: @card.id)
      @transactions = @payment_batch.create_transactions_for_invoices(@invoices)

      expect(@transactions.first).to receive(:process_with_braintree_credit_card)
      expect(@transactions.first.build_payment_options).to eq([[:card, @card]])

      @transactions.first.process
    end

    it "should process with braintree cc if person does not have enough balance but has credit card" do
      mock_braintree_query

      @card = create(:stored_credit_card, person_id: @person.id)
      @person.build_person_preference(pay_with_balance: 1, preferred_payment_type: "CreditCard", preferred_credit_card_id: @card.id)
      @transactions = @payment_batch.create_transactions_for_invoices(@invoices)

      @person.person_balance.balance = @purchase.cost_cents / 100 - 1
      expect(@transactions.first).to receive(:process_with_braintree_credit_card)
      expect(@transactions.first.build_payment_options).to eq([[:card, @card]])

      @transactions.first.process
    end

    it "should process with payments os if the person does not have enough balance but has payments os credit card" do
      state = create(:country_state)
      city = create(:country_state_city)
      @card = create(:stored_credit_card, person_id: @person.id, payments_os_token: "something123",
                     state_id: state.id, state_city_id: city.id)
      @person.build_person_preference(pay_with_balance: 0, preferred_payment_type: "CreditCard", preferred_credit_card_id: @card.id)
      @transaction = create(:batch_transaction, invoice: @invoices.first, payment_batch: @payment_batch, stored_credit_card: @card)

      expect(@transaction).to receive(:process_with_payments_os)
      expect(@transaction.build_payment_options).to eq([[:card, @card]])

      @transaction.process
    end

    it "should process with paypal if person does not accept balance and does renew with cc and has pp" do
      @account = TCFactory.create(:stored_paypal_account, person_id: @person.id)
      @person.person_preference.pay_with_balance = 0
      @person.person_preference.preferred_payment_type = "PayPal"
      @person.person_preference.preferred_paypal_account_id = @account.id
      @person.person_preference.save!
      @transactions = @payment_batch.create_transactions_for_invoices(@invoices)

      expect(@transactions.first).to receive(:process_with_paypal)
      expect(@transactions.first.build_payment_options).to eq([[:paypal, @account]])

      @transactions.first.process
    end

    it "should keep track of transactions not able to be processed" do
      @person.build_person_preference(pay_with_balance: 0, preferred_payment_type: "PayPal")
      @transactions = @payment_batch.create_transactions_for_invoices(@invoices)

      expect(@transactions.first).not_to receive(:process_with_balance)
      expect(@transactions.first).not_to receive(:process_with_paypal)
      expect(@transactions.first).not_to receive(:process_with_braintree_credit_card)
      expect(@transactions.first.build_payment_options).to eq([[:failure_marker]])

      @transactions.first.process
      @transactions.first.process
      expect(@payment_batch.transactions_not_processed).not_to be_empty
      expect(@transactions.first.reload.processed).to eq('cannot_process')
    end
    # INVOICE LOG SPECS CAN EASILY BREAK WHEN LOGS ARE ADDED IN OTHER DEPENDENCIES - I.E. IF THIS METHOD CALLS A METHOD
    # WITH AN INVOICE LOG THAT DOESN'T EXIST WHEN THIS SPEC EXISTS, LOG VARIABLES WILL BE OFF AND NEED
    # TO BE ADJUSTED
    it "should create InvoiceLog for transactions not able to be processed" do
      @person.build_person_preference(pay_with_balance: 0, preferred_payment_type: "PayPal")
      transaction = @payment_batch.create_transactions_for_invoices(@invoices).first

      transaction.process

      log = InvoiceLog.all[-1]
      invoice = transaction.invoice
      purchase = invoice.purchases.first
      expected_options = {
        person_id: invoice.person.id,
        batch_transaction_id: transaction.id,
        payment_batch_id: @payment_batch.id,
        braintree_transaction_id: nil,
        paypal_transaction_id: nil,
        stored_paypal_account_id: nil,
        renewal_id: nil,
        current_method_name: "process",
        message: "Add not processed transaction",
        invoice_id: invoice.id,
        purchase_id: purchase.id,
        invoice_created_at: invoice.created_at,
        invoice_final_settlement_amount_cents: invoice.final_settlement_amount_cents,
        invoice_currency: invoice.currency,
        invoice_batch_status: invoice.batch_status,
        invoice_settled_at: invoice.settled_at,
        invoice_vat_amount_cents: invoice.vat_amount_cents,
        invoice_foreign_exchange_pegged_rate_id: invoice.foreign_exchange_pegged_rate_id,
        purchase_created_at: purchase.created_at,
        purchase_product_id: purchase.product_id,
        purchase_cost_cents: purchase.cost_cents,
        purchase_discount_cents: purchase.discount_cents,
        purchase_currency: purchase.currency,
        purchase_paid_at: purchase.paid_at,
        purchase_salepoint_id: purchase.salepoint_id,
        purchase_no_purchase_album_id: purchase.no_purchase_album_id,
        purchase_related_id: purchase.related_id,
        purchase_related_type: purchase.related_type,
        purchase_targeted_product_id: purchase.targeted_product_id,
        purchase_price_adjustment_histories_count: purchase.price_adjustment_histories_count,
        purchase_vat_amount_cents: purchase.vat_amount_cents,
        purchase_status: purchase.status
      }
      actual_options = {
        person_id: log.person_id,
        batch_transaction_id: log.batch_transaction_id,
        payment_batch_id: log.payment_batch_id,
        braintree_transaction_id: log.braintree_transaction_id,
        paypal_transaction_id: log.paypal_transaction_id,
        stored_paypal_account_id: log.stored_paypal_account_id,
        renewal_id: log.renewal_id,
        current_method_name: log.current_method_name,
        message: log.message,
        invoice_id: log.invoice_id,
        purchase_id: log.purchase_id,
        invoice_created_at: log.invoice_created_at,
        invoice_final_settlement_amount_cents: log.invoice_final_settlement_amount_cents,
        invoice_currency: log.invoice_currency,
        invoice_batch_status: log.invoice_batch_status,
        invoice_settled_at: log.invoice_settled_at,
        invoice_vat_amount_cents: log.invoice_vat_amount_cents,
        invoice_foreign_exchange_pegged_rate_id: log.invoice_foreign_exchange_pegged_rate_id,
        purchase_created_at: log.purchase_created_at,
        purchase_product_id: log.purchase_product_id,
        purchase_cost_cents: log.purchase_cost_cents,
        purchase_discount_cents: log.purchase_discount_cents,
        purchase_currency: log.purchase_currency,
        purchase_paid_at: log.purchase_paid_at,
        purchase_salepoint_id: log.purchase_salepoint_id,
        purchase_no_purchase_album_id: log.purchase_no_purchase_album_id,
        purchase_related_id: log.purchase_related_id,
        purchase_related_type: log.purchase_related_type,
        purchase_targeted_product_id: log.purchase_targeted_product_id,
        purchase_price_adjustment_histories_count: log.purchase_price_adjustment_histories_count,
          purchase_vat_amount_cents: log.purchase_vat_amount_cents,
          purchase_status: log.purchase_status
      }
      expect(expected_options).to eq(actual_options)
    end

    it "should not add duplicate transactions to not processed attribute" do
      # having no preferred preferred_paypal_account_id will cause no process
      @person.build_person_preference(pay_with_balance: 0, preferred_payment_type: "PayPal", preferred_paypal_account_id: nil)
      @transactions = @payment_batch.create_transactions_for_invoices(@invoices)
      @transactions.first.process
      expect(@payment_batch.transactions_not_processed.size).to eq(1)
      expect(@payment_batch.transactions_not_processed.first).to eq(@transactions.first)

      @transactions.first.process
      expect(@payment_batch.transactions_not_processed.size).to eq(1)
    end

    it "should retrieve updated balance after processing another transaction with balance" do
      @person.person_balance.update balance: @purchase.cost_cents / 100 + 1

      # Create another purchase/invoice
      @invoices << Invoice.create(person: @person, batch_status: "waiting_for_batch")
      album         = TCFactory.create(:album, person: @invoices.first.person)
      @purchase2    = TCFactory.create(:purchase, invoice: @invoices[1], person: @person, related: album, product: @purchase.product)

      @transactions = @payment_batch.create_transactions_for_invoices(@invoices)
      expect(@transactions.first).to receive(:process_with_balance)
      expect(@transactions[1]).not_to receive(:process_with_balance)

      @transactions[0].process
    end
  end

  describe "#process (without rspec setup)" do
    it "should not process payment if invoice is already settled" do
      settled_invoice = TCFactory.create(:invoice, settled_at: Time.now, batch_status: "waiting_for_batch")
      person_txn = TCFactory.create(:person_transaction)

      invoice_settlement = TCFactory.create(:invoice_settlement, invoice: settled_invoice, source: person_txn)
      transaction = TCFactory.create(:batch_transaction, invoice: settled_invoice, processed: "pending")

      transaction.process

      expect(transaction.processed).to eq("processed")
      expect(transaction.matching_type).to eq("PersonTransaction")
    end
  end

  describe "#build_payment_options" do
    before(:each) do
      @person = create(:person)
      @renewal_product = Product.find(1)
      create(:album, :with_one_year_renewal, :purchaseable, person: @person)
      @purchase = create(:purchase, person: @person, related: @person.renewals.first, product: @renewal_product)
      @invoices = [Invoice.factory(@person, [@purchase])]
      @invoices.first.update(batch_status: "waiting_for_batch")
      @person.invoices.first.settlement_received(@purchase, @purchase.cost_cents)
      @payment_batch = TCFactory.create(:payment_batch)
      @payment_batch.initialize_batch_counts
      @transactions = @payment_batch.create_transactions_for_invoices(@invoices)
      @transaction = @transactions.first
      allow(FeatureFlipper).to receive(:show_feature?).with(any_args).and_call_original
      expect(FeatureFlipper).to receive(:show_feature?).at_least(:once).with(:try_multiple_payment_methods, @person.id).and_return(true)
    end

    it "should have no options if none exist" do
      expect(@transaction.build_payment_options).to eq([[:failure_marker]])
      expect(@transaction).to receive(:mark_failure).with(@person, @invoices.last)
      @transaction.process
    end

    it "should add the option of using the balance" do
      @person.person_balance.update_balance(2 * @transaction.amount)
      expect(@person).to receive(:renew_with_balance?).at_least(:once).and_return(true)
      expect(@transaction.build_payment_options).to eq([[:balance], [:failure_marker]])

      @card1 = FactoryBot.create(:bb_stored_credit_card, person: @person)
      expect(@transaction.build_payment_options).to eq([[:balance], [:card, @card1], [:failure_marker]])

      @paypal = TCFactory.create(:stored_paypal_account, person: @person)
      expect(@transaction.build_payment_options).to eq([[:balance], [:paypal, @paypal], [:card, @card1], [:failure_marker]])

      expect(@transaction).to receive(:process_with_balance) do
        @transaction.update(processed: 'processed')
      end
      expect(@transaction).not_to receive(:process_with_card)
      expect(@transaction).not_to receive(:process_with_paypal)
      expect(@transaction).not_to receive(:mark_failure)

      @transaction.process
    end

    it "should add both preferred and stored cards" do
      @card1 = FactoryBot.create(:bb_stored_credit_card, person: @person)
      @card2 = FactoryBot.create(:bb_stored_credit_card, person: @person)
      @card3 = FactoryBot.create(:bb_stored_credit_card, person: @person)
      @person.build_person_preference(pay_with_balance: 0, preferred_payment_type: "CreditCard", preferred_credit_card_id: @card2.id).save!

      expect(@transaction.reload.build_payment_options.length).to eq(4)
      expect(@transaction.reload.build_payment_options).to eq([[:card, @card2], [:card, @card3], [:card, @card1], [:failure_marker]])

      expect(@transaction).to receive(:process_with_card).ordered.with(@card2) {} # do nothing
      expect(@transaction).to receive(:process_with_card).ordered.with(@card3) {} # do nothing
      expect(@transaction).to receive(:process_with_card).ordered.with(@card1) do
        @transaction.update(processed: 'processed')
      end
      expect(@transaction).not_to receive(:mark_failure)
      expect(@transaction).not_to receive(:process_with_balance)
      @transaction.process
    end

    it "should add paypal account" do
      @account = TCFactory.create(:stored_paypal_account, person: @person)
      expect(@transaction.reload.build_payment_options).to eq([[:paypal, @account], [:failure_marker]])

      expect(@transaction).to receive(:process_with_paypal).ordered.with(@account) do
        @transaction.update(processed: 'processed')
      end
      @transaction.process
    end

    it "should correctly arrange both paypal and credit card methods" do
      @card1 = FactoryBot.create(:bb_stored_credit_card, person: @person)
      @card2 = FactoryBot.create(:bb_stored_credit_card, person: @person)
      @person.build_person_preference(pay_with_balance: 0, preferred_payment_type: "CreditCard", preferred_credit_card_id: @card2.id).save!
      @paypal = TCFactory.create(:stored_paypal_account, person: @person)

      expect(@transaction.reload.build_payment_options).to eq([[:card, @card2], [:paypal, @paypal], [:card, @card1], [:failure_marker]])

      expect(@transaction).to receive(:process_with_card).ordered.with(@card2) {} # do nothing
      expect(@transaction).to receive(:process_with_paypal).ordered.with(@paypal) do
        @transaction.update(processed: 'processed')
      end
      @transaction.process
    end

    context "albums taken down" do
      let!(:person) { @person }
      let!(:plan_products_ids) { PlansProduct.where(plan_id: 4).pluck(:product_id) }
      let!(:plan_product) do
        Product.find_by(id: plan_products_ids, country_website_id: person.country_website_id)
      end
      let!(:product_item) { ProductItem.find_by(product_id: plan_product.id) }
      let!(:person_plan) { create(:person_plan, person: person, plan: Plan.find(Plan::PROFESSIONAL)) }
      let!(:renewal) do
        create(:renewal,
               person: person,
               item_to_renew: product_item)
      end
      let!(:renewal_item) {create(:renewal_item, renewal: renewal, related: person_plan)}
      let!(:product) { renewal.item_to_renew.renewal_product }
      let!(:creative1) { {name: "Star Laser Beam", role: Creative::PRIMARY_ARTIST_ROLE}.with_indifferent_access }
      let!(:creative2) { {name: "World Sized Drill", role: Creative::PRIMARY_ARTIST_ROLE}.with_indifferent_access }
      let!(:creative3) { {name: "Drill Sized Star", role: Creative::PRIMARY_ARTIST_ROLE}.with_indifferent_access }
      let!(:album1) { create(:album, :paid, :with_salepoints, creatives: [creative1], person: person) }
      let!(:album2) { create(:album, :paid, :with_salepoints, creatives: [creative2], person: person) }
      let!(:album3) { create(:album, :paid, :with_salepoints, creatives: [creative3], person: person) }
      let!(:addtl_artist_1) { create(:plan_addon, :additional_artist_addon, :paid, person: person) }
      let!(:addtl_artist_2) { create(:plan_addon, :additional_artist_addon, :paid, person: person) }

      before(:each) do
        create(:petri_bundle, album: album3)
        album3.takedown!
      end

      it "should trigger an additional artist plan_addons adjustment when necessary" do
        purchases = Plans::RenewAdditionalArtistsService.plan_and_additional_artist_purchases!(person, renewal, product)
        invoice = create(:invoice, purchases: purchases, person: person)

        person.person_balance.update(balance: 100)
        @transaction.invoice = invoice

        expect_any_instance_of(Plans::CancelAdditionalArtistService).to receive(:cancel_unrenewed_artists).and_return(true)
        @transaction.process
      end

      it "should NOT trigger an additional artist plan_addons adjustment if unnecessary" do
        expect_any_instance_of(Plans::CancelAdditionalArtistService).to receive(:cancel_unrenewed_artists).and_return(false)
        @transaction.process
      end
    end
  end

  describe "#process functions" do
    before(:each) do
      @person = create(:person)
      @renewal_product = Product.find(1)
      create(:album, :with_one_year_renewal, :purchaseable, person: @person)
      @purchase = create(:purchase, person: @person, related: @person.renewals.first, product: @renewal_product)
      @invoices = [Invoice.factory(@person, [@purchase])]
      @invoices.first.update(batch_status: "waiting_for_batch")
      @person.invoices.first.settlement_received(@purchase, @purchase.cost_cents)
      @person.invoices.first.settled!
      @payment_batch = TCFactory.create(:payment_batch)
      @payment_batch.initialize_batch_counts

      @transactions = @payment_batch.create_transactions_for_invoices(@invoices)
    end

    describe "#process_with_balance" do

      it "should create a person transaction of the right amount" do
        @person.person_balance.update balance: @transactions.first.amount + 1
        expect do
          @transactions.first.send(:process_with_balance)
        end.to change(PersonTransaction, :count).by(1)

        pt = PersonTransaction.find_by(target: @transactions.first)
        expect(pt.debit).to eq(@transactions.first.amount)
      end

      it "should do nothing if called with not enough balance" do
        @person.person_balance.update balance: @transactions.first.amount / 2

        expect do
          @transactions.first.send(:process_with_balance)
        end.to change(PersonTransaction, :count).by(0)

        expect(PersonTransaction.find_by(target: @transactions.first)).to be_nil
      end

      it "should try to settle the invoice and keep track of success" do
        @person.person_balance.update balance: @transactions.first.amount + 1
        @transactions.first.send(:process_with_balance)

        expect(@invoices.first.settled?).to eq(true)
        expect(@transactions.first.reload.matching_type).to eq("PersonTransaction")
        expect(@transactions.first.reload.processed).to eq("processed")
        expect(@transactions.first.reload.processed?).to be_truthy
      end

      it "should log a failed transaction" do
        @person.person_balance.update balance: @transactions.first.amount + 1

        allow(@invoices.first).to receive(:settled?).and_return(false)
        expect(@transactions.first).to receive(:log).exactly(2).times
        @transactions.first.send(:process_with_balance)

        expect(@transactions.first.reload.processed?).to be_falsey
      end

      it "should notify airbrake for a failed transaction" do
        @person.person_balance.update balance: @transactions.first.amount + 1
        expect(Airbrake).to receive(:notify)
        allow(@invoices.first).to receive(:settled?).and_return(false)
        @transactions.first.send(:process_with_balance)
      end

      it "should create two InvoiceLogs on a failed transaction" do
        # stubbing settled! out this way because we don't want to update final_settlement_amount_cents and settled_at
        allow(@invoices.first).to receive(:settled!).and_return(false)
        allow(@invoices.first).to receive(:settled?).and_return(false)
        allow_any_instance_of(PersonTransaction).to receive(:id).and_return(1)
        transaction = @transactions.first
        @person.person_balance.update balance: transaction.amount + 1

        transaction.send(:process_with_balance)
        log_1 = InvoiceLog.all[-2]
        log_2 = InvoiceLog.all[-1]

        invoice = transaction.invoice
        purchase = invoice.purchases.first
        comment = "Pay with balance"
        message = "comment: #{comment}, person_transaction_id: 1, amount: #{transaction.amount}"

        expected_options_process = {
            person_id: invoice.person.id,
            batch_transaction_id: transaction.id,
            payment_batch_id: @payment_batch.id,
            braintree_transaction_id: nil,
            paypal_transaction_id: nil,
            stored_paypal_account_id: nil,
            renewal_id: nil,
            current_method_name: "process_with_balance",
            message: message,
            invoice_id: invoice.id,
            purchase_id: purchase.id,
            invoice_created_at: invoice.created_at,
            invoice_final_settlement_amount_cents: invoice.final_settlement_amount_cents,
            invoice_currency: invoice.currency,
            invoice_batch_status: invoice.batch_status,
            invoice_settled_at: invoice.settled_at,
            invoice_vat_amount_cents: invoice.vat_amount_cents,
            invoice_foreign_exchange_pegged_rate_id: invoice.foreign_exchange_pegged_rate_id,
            purchase_created_at: purchase.created_at,
            purchase_product_id: purchase.product_id,
            purchase_cost_cents: purchase.cost_cents,
            purchase_discount_cents: purchase.discount_cents,
            purchase_currency: purchase.currency,
            purchase_paid_at: purchase.paid_at,
            purchase_salepoint_id: purchase.salepoint_id,
            purchase_no_purchase_album_id: purchase.no_purchase_album_id,
            purchase_related_id: purchase.related_id,
            purchase_related_type: purchase.related_type,
            purchase_targeted_product_id: purchase.targeted_product_id,
            purchase_price_adjustment_histories_count: purchase.price_adjustment_histories_count,
            purchase_vat_amount_cents: purchase.vat_amount_cents,
            purchase_status: purchase.status
        }
        actual_options_process = {
            person_id: log_1.person_id,
            batch_transaction_id: log_1.batch_transaction_id,
            payment_batch_id: log_1.payment_batch_id,
            braintree_transaction_id: log_1.braintree_transaction_id,
            paypal_transaction_id: log_1.paypal_transaction_id,
            stored_paypal_account_id: log_1.stored_paypal_account_id,
            renewal_id: log_1.renewal_id,
            current_method_name: log_1.current_method_name,
            message: log_1.message,
            invoice_id: log_1.invoice_id,
            purchase_id: log_1.purchase_id,
            invoice_created_at: log_1.invoice_created_at,
            invoice_final_settlement_amount_cents: log_1.invoice_final_settlement_amount_cents,
            invoice_currency: log_1.invoice_currency,
            invoice_batch_status: log_1.invoice_batch_status,
            invoice_settled_at: log_1.invoice_settled_at,
            invoice_vat_amount_cents: log_1.invoice_vat_amount_cents,
            invoice_foreign_exchange_pegged_rate_id: log_1.invoice_foreign_exchange_pegged_rate_id,
            purchase_created_at: log_1.purchase_created_at,
            purchase_product_id: log_1.purchase_product_id,
            purchase_cost_cents: log_1.purchase_cost_cents,
            purchase_discount_cents: log_1.purchase_discount_cents,
            purchase_currency: log_1.purchase_currency,
            purchase_paid_at: log_1.purchase_paid_at,
            purchase_salepoint_id: log_1.purchase_salepoint_id,
            purchase_no_purchase_album_id: log_1.purchase_no_purchase_album_id,
            purchase_related_id: log_1.purchase_related_id,
            purchase_related_type: log_1.purchase_related_type,
            purchase_targeted_product_id: log_1.purchase_targeted_product_id,
            purchase_price_adjustment_histories_count: log_1.purchase_price_adjustment_histories_count,
            purchase_vat_amount_cents: log_1.purchase_vat_amount_cents,
            purchase_status: log_1.purchase_status
        }

        comment = "Balance transaction failed"
        message = "comment: #{comment}, person_transaction_id: 1, amount: #{transaction.amount}"
        expected_options_failure = {
            person_id: invoice.person.id,
            batch_transaction_id: transaction.id,
            payment_batch_id: @payment_batch.id,
            braintree_transaction_id: nil,
            paypal_transaction_id: nil,
            stored_paypal_account_id: nil,
            renewal_id: nil,
            current_method_name: "process_with_balance",
            message: message,
            invoice_id: invoice.id,
            purchase_id: purchase.id,
            invoice_created_at: invoice.created_at,
            invoice_final_settlement_amount_cents: invoice.final_settlement_amount_cents,
            invoice_currency: invoice.currency,
            invoice_batch_status: invoice.batch_status,
            invoice_settled_at: invoice.settled_at,
            invoice_vat_amount_cents: invoice.vat_amount_cents,
            invoice_foreign_exchange_pegged_rate_id: invoice.foreign_exchange_pegged_rate_id,
            purchase_created_at: purchase.created_at,
            purchase_product_id: purchase.product_id,
            purchase_cost_cents: purchase.cost_cents,
            purchase_discount_cents: purchase.discount_cents,
            purchase_currency: purchase.currency,
            purchase_paid_at: purchase.paid_at,
            purchase_salepoint_id: purchase.salepoint_id,
            purchase_no_purchase_album_id: purchase.no_purchase_album_id,
            purchase_related_id: purchase.related_id,
            purchase_related_type: purchase.related_type,
            purchase_targeted_product_id: purchase.targeted_product_id,
            purchase_price_adjustment_histories_count: purchase.price_adjustment_histories_count,
            purchase_vat_amount_cents: purchase.vat_amount_cents,
            purchase_status: purchase.status
        }
        actual_options_failure = {
            person_id: log_2.person_id,
            batch_transaction_id: log_2.batch_transaction_id,
            payment_batch_id: log_2.payment_batch_id,
            braintree_transaction_id: log_2.braintree_transaction_id,
            paypal_transaction_id: log_2.paypal_transaction_id,
            stored_paypal_account_id: log_2.stored_paypal_account_id,
            renewal_id: log_2.renewal_id,
            current_method_name: log_2.current_method_name,
            message: log_2.message,
            invoice_id: log_2.invoice_id,
            purchase_id: log_2.purchase_id,
            invoice_created_at: log_2.invoice_created_at,
            invoice_final_settlement_amount_cents: log_2.invoice_final_settlement_amount_cents,
            invoice_currency: log_2.invoice_currency,
            invoice_batch_status: log_2.invoice_batch_status,
            invoice_settled_at: log_2.invoice_settled_at,
            invoice_vat_amount_cents: log_2.invoice_vat_amount_cents,
            invoice_foreign_exchange_pegged_rate_id: log_2.invoice_foreign_exchange_pegged_rate_id,
            purchase_created_at: log_2.purchase_created_at,
            purchase_product_id: log_2.purchase_product_id,
            purchase_cost_cents: log_2.purchase_cost_cents,
            purchase_discount_cents: log_2.purchase_discount_cents,
            purchase_currency: log_2.purchase_currency,
            purchase_paid_at: log_2.purchase_paid_at,
            purchase_salepoint_id: log_2.purchase_salepoint_id,
            purchase_no_purchase_album_id: log_2.purchase_no_purchase_album_id,
            purchase_related_id: log_2.purchase_related_id,
            purchase_related_type: log_2.purchase_related_type,
            purchase_targeted_product_id: log_2.purchase_targeted_product_id,
            purchase_price_adjustment_histories_count: log_2.purchase_price_adjustment_histories_count,
            purchase_vat_amount_cents: log_2.purchase_vat_amount_cents,
            purchase_status: log_2.purchase_status
        }

        expect(expected_options_process).to eq(actual_options_process)
        expect(expected_options_failure).to eq(actual_options_failure)
      end
    end

    describe "#process_with_paypal" do
      before(:each) do
        @account = TCFactory.create(:stored_paypal_account, person: @person)
        mock_paypal_transaction("Completed", 'AMT' => @invoices.first.amount)
      end

      it "should create a paypal transaction of the right amount" do
        expect do
          @transactions.first.send(:process_with_paypal, @account)
        end.to change(PaypalTransaction, :count).by(1)

        pt = PaypalTransaction.last
        expect(pt.amount).to eq(@transactions.first.amount)
      end

      it "should try to settle the invoice and keep track of success" do
        @transactions.first.send(:process_with_paypal, @account)

        expect(@invoices.first.settled?).to eq(true)
        expect(@transactions.first.reload.matching_type).to eq("PaypalTransaction")
        expect(@transactions.first.reload.processed).to eq("processed")
        expect(@transactions.first.reload.processed?).to be_truthy
      end

      it "should log a failed transaction" do
        allow(@invoices.first).to receive(:settled?).and_return(false)
        expect(@transactions.first).to receive(:log).exactly(3).times

        @transactions.first.send(:process_with_paypal, @account)

        expect(@invoices.first.reload.batch_status).to eq "visible_to_customer"
        expect(@transactions.first.reload.processed?).to be_falsey
      end

      it "should create two invoice logs on a failed transaction" do
        invoice = @invoices.first
        batch_transaction = @transactions.first
        allow(invoice).to receive(:settled?).and_return(false)

        batch_transaction.send(:process_with_paypal, @account)

        person = invoice.person
        invoice.reload
        paypal_transaction = invoice.paypal_transactions.first

        log_1 = InvoiceLog.all[-2]
        log_2 = InvoiceLog.all[-1]
        expected_log_1 = {
            message: "comment: Pay with Paypal",
            person_id: person.id,
            invoice_id: invoice.id,
            payment_batch_id: @payment_batch.id,
            paypal_transaction_id: paypal_transaction.id,
            batch_transaction_id: batch_transaction.id,
            stored_paypal_account_id: invoice.person.preferred_paypal_account.id
        }
        expected_log_2 = {
            truncated_message: "comment: Paypal transaction failed",
            person_id: person.id,
            invoice_id: invoice.id,
            payment_batch_id: @payment_batch.id,
            paypal_transaction_id: paypal_transaction.id,
            batch_transaction_id: batch_transaction.id,
            stored_paypal_account_id: invoice.person.preferred_paypal_account.id
        }

        actual_log_1 = {
            message: log_1.message,
            person_id: log_1.person_id,
            invoice_id: log_1.invoice_id,
            payment_batch_id: log_1.payment_batch_id,
            paypal_transaction_id: log_1.paypal_transaction_id,
            batch_transaction_id: log_1.batch_transaction_id,
            stored_paypal_account_id: log_1.stored_paypal_account_id
        }
        actual_log_2 = {
            truncated_message: log_2.message[0,34],
            person_id: log_2.person_id,
            invoice_id: log_2.invoice_id,
            payment_batch_id: log_2.payment_batch_id,
            paypal_transaction_id: log_2.paypal_transaction_id,
            batch_transaction_id: log_2.batch_transaction_id,
            stored_paypal_account_id: log_2.stored_paypal_account_id
        }
        expect(expected_log_1).to eq(actual_log_1)
        expect(expected_log_2).to eq(actual_log_2)
      end

      context "when paypal throws an exception" do
        it "should set the batch transaction to pending" do
          allow(PaypalTransaction).to receive(:process_reference_transaction).and_raise(StandardError)
          @transactions.first.send(:process_with_paypal, @account)
          expect(@transactions.first.processed).to eq("pending")
        end
        it "should create one invoice log" do
          allow(PaypalTransaction).to receive(:process_reference_transaction).and_raise(StandardError)
          batch_transaction = @transactions.first
          invoice = @invoices.first
          batch_transaction.send(:process_with_paypal, @account)

          log = InvoiceLog.all[-1]
          person = invoice.person
          expected_log = {
              truncated_message: "comment: Paypal Exception",
              person_id: person.id,
              invoice_id: invoice.id,
              payment_batch_id: @payment_batch.id,
              paypal_transaction_id: nil,
              batch_transaction_id: batch_transaction.id,
              stored_paypal_account_id: invoice.person.preferred_paypal_account.id
          }

          actual_log = {
              truncated_message: log.message[0, 25],
              person_id: log.person_id,
              invoice_id: log.invoice_id,
              payment_batch_id: log.payment_batch_id,
              paypal_transaction_id: log.paypal_transaction_id,
              batch_transaction_id: log.batch_transaction_id,
              stored_paypal_account_id: log.stored_paypal_account_id
          }
          expect(expected_log).to eq(actual_log)
        end

        it "should notify airbrake" do
          expect(Airbrake).to receive(:notify)
          allow(PaypalTransaction).to receive(:process_reference_transaction).and_raise(StandardError)
          batch_transaction = @transactions.first
          invoice = @invoices.first
          batch_transaction.send(:process_with_paypal, @payment_batch)
        end
      end
    end

    describe "#process_with_braintree_credit_card" do
      before(:each) do
        @payment_batch = FactoryBot.create(:payment_batch)
        person = FactoryBot.create(:person)
        @stored_credit_card = FactoryBot.create(:bb_stored_credit_card, person: person)
        @amount = 19.99
        @batch_transaction = FactoryBot.create(:batch_transaction, payment_batch: @payment_batch, stored_credit_card: @stored_credit_card, invoice: FactoryBot.create(:invoice, person: person), amount: @amount)
        @braintree_transaction = FactoryBot.create(:braintree_transaction, person: person)
        allow(@batch_transaction).to receive(:process_braintree_blue_payment).and_return(@braintree_transaction)
      end

      it "calls process_successful_credit_card and marks as processed if the related invoice is settled" do
        allow(@braintree_transaction).to receive_message_chain(:invoice, :settled?).and_return(true)
        expect(@batch_transaction).to receive(:process_successful_credit_card)

        @batch_transaction.send(:process_with_braintree_credit_card, @stored_credit_card)

        expect(@batch_transaction.matching).to eq(@braintree_transaction)
        expect(@batch_transaction.processed).to eq("processed")
        expect(@batch_transaction.processed?).to be_truthy
      end

      it "it does not call process_successful_credit_card or mark as processed if the related invoice is not settled" do
        allow(@braintree_transaction).to receive_message_chain(:invoice, :settled?).and_return(false)
        expect(@batch_transaction).not_to receive(:process_successful_credit_card)
        @batch_transaction.send(:process_with_braintree_credit_card, @stored_credit_card)
        expect(@batch_transaction.processed?).to be_falsey
      end

      it "sets the processed value to 'pending' if an exception is raised in the creation of the braintree_transaction" do
        allow(@batch_transaction).to receive(:process_braintree_blue_payment).and_raise("Error")
        @batch_transaction.send(:process_with_braintree_credit_card, @stored_credit_card)
        expect(@batch_transaction.processed).to eq("pending")
      end

      it "adds two invoice logs if the related invoice is not settled" do
        allow(@braintree_transaction).to receive_message_chain(:invoice, :settled?).and_return(false)
        @batch_transaction.send(:process_with_braintree_credit_card, @stored_credit_card)
        invoice = @batch_transaction.invoice
        log_1 = InvoiceLog.all[-2]
        log_2 = InvoiceLog.all[-1]
        expected_log_1 = {
            truncated_message: "comment: Braintree Transaction",
            person_id: invoice.person.id,
            invoice_id: invoice.id,
            payment_batch_id: @payment_batch.id,
            braintree_transaction_id: @braintree_transaction.id,
            batch_transaction_id: @batch_transaction.id
        }
        expected_log_2 = {
            truncated_message: "comment: Braintree transaction failed",
            person_id: invoice.person.id,
            invoice_id: invoice.id,
            payment_batch_id: @payment_batch.id,
            braintree_transaction_id: @braintree_transaction.id,
            batch_transaction_id: @batch_transaction.id
        }

        actual_log_1 = {
            truncated_message: log_1.message[0,30],
            person_id: log_1.person_id,
            invoice_id: log_1.invoice_id,
            payment_batch_id: log_1.payment_batch_id,
            braintree_transaction_id: log_1.braintree_transaction_id,
            batch_transaction_id: log_1.batch_transaction_id
        }
        actual_log_2 = {
            truncated_message: log_2.message[0,37],
            person_id: log_2.person_id,
            invoice_id: log_2.invoice_id,
            payment_batch_id: log_2.payment_batch_id,
            braintree_transaction_id: log_2.braintree_transaction_id,
            batch_transaction_id: log_2.batch_transaction_id
        }
        expect(expected_log_1).to eq(actual_log_1)
        expect(expected_log_2).to eq(actual_log_2)
      end

      it "adds one invoice log if an exception is raised in the creation of the braintree_transaction" do
        allow(@batch_transaction).to receive(:process_braintree_blue_payment).and_raise("Error")
        @batch_transaction.send(:process_with_braintree_credit_card, @stored_credit_card)
        invoice = @batch_transaction.invoice
        log = InvoiceLog.all[-1]
        expected_log = {
          truncated_message: "comment: Braintree Exception",
          person_id: invoice.person.id,
          invoice_id: invoice.id,
          payment_batch_id: @payment_batch.id,
          braintree_transaction_id: nil,
          batch_transaction_id: @batch_transaction.id
        }
        actual_log = {
          truncated_message: log.message[0,28],
          person_id: log.person_id,
          invoice_id: log.invoice_id,
          payment_batch_id: log.payment_batch_id,
          braintree_transaction_id: log.braintree_transaction_id,
          batch_transaction_id: log.batch_transaction_id
        }
        expect(expected_log).to eq(actual_log)
      end
      it "notifies airbrake if an exception is raised in the creation of the braintree_transaction" do
        expect(Airbrake).to receive(:notify)
        allow(@batch_transaction).to receive(:process_braintree_blue_payment).and_raise("Error")
        @batch_transaction.send(:process_with_braintree_credit_card, @payment_batch)
      end
    end

    describe "#process_successful_credit_card" do
      before(:each) do
        @payment_batch = FactoryBot.create(:payment_batch)
        person = FactoryBot.create(:person)
        @amount = 19.99
        @batch_transaction = FactoryBot.create(:batch_transaction, payment_batch: @payment_batch, stored_credit_card: @stored_credit_card, invoice: FactoryBot.create(:invoice, person: person), amount: @amount)
        @braintree_transaction = FactoryBot.create(:braintree_transaction, person: person, amount: @amount)
      end

      it "sends a renewal thank you email" do
        mailer = double("mailer")
        expect(BatchNotifier).to receive(:renewal_payment_thank_you).and_return(mailer)
        expect(mailer).to receive(:deliver).and_return(true)
        @batch_transaction.send(:process_successful_credit_card, @braintree_transaction)
      end

      it "adds to the payment_batches braintree_amount_successful and braintree_count_sucessful" do
        allow(BatchNotifier).to receive_message_chain(:renewal_payment_thank_you, :deliver).and_return(true)
        @batch_transaction.send(:process_successful_credit_card, @braintree_transaction)
      end
    end
  end

  describe "#process_braintree_blue_payment" do
    before(:each) do
      @braintree_successful_result = Braintree::SuccessfulResult.new
      @transaction = create(:braintree_transaction)
      allow(FeatureFlipper).to receive(:show_feature?).with(any_args).and_call_original
      expect(FeatureFlipper).to receive(:show_feature?).with(:send_recurring_indicator).and_return(true)
      expect_any_instance_of(Braintree::ConfigGatewayService).to receive(:sale).with(hash_including(transaction_source: 'recurring')).and_return(@braintree_successful_result)
      allow(@braintree_successful_result).to receive(:transaction).and_return(@transaction)
      @transaction_id = "1askjdy2"
      person = FactoryBot.create(:person)
      @stored_credit_card = FactoryBot.create(:bb_stored_credit_card, person: person)
      @batch_transaction = FactoryBot.create(:batch_transaction, stored_credit_card: @stored_credit_card, invoice: FactoryBot.create(:invoice, person: person))
    end

    it "creates a braintree_transaction" do
      expect do
        @batch_transaction.send :process_braintree_blue_payment, @stored_credit_card
      end.to change(BraintreeTransaction, :count).by(1)
    end

    it "calls settle_invoice on the created braintree_transaction" do
      expect_any_instance_of(BraintreeTransaction).to receive(:settle_invoice).and_return(nil)
      @batch_transaction.send :process_braintree_blue_payment, @stored_credit_card
    end

    it "calls BraintreeTransactionLogWorker" do
      expect(BraintreeTransactionLogWorker).to receive_message_chain(:new, :async, :write_to_bucket)
      @batch_transaction.send :process_braintree_blue_payment, @stored_credit_card
    end

    it "calls TransactionResponse" do
      expect(TransactionResponse).to receive(:process_transaction_params)
      @batch_transaction.send :process_braintree_blue_payment, @stored_credit_card
    end
  end

  describe "#braintree_payment_splits_config" do
    let(:person) { create(:person, :with_lux_country) }
    let(:invoice) { create(:invoice, person: person) }
    let(:stored_credit_card) { create(:bb_stored_credit_card, person: person) }
    let(:batch_transaction) { create(:batch_transaction, stored_credit_card: stored_credit_card, invoice: invoice) }
    options = { is_renewal: true }

    it "should return TC merchant if none of the payment splits feature flags are enabled" do
      payin_provider_config = batch_transaction.send :braintree_payment_splits_config, stored_credit_card.person, options
      expect(payin_provider_config.corporate_entity.name).to eq("TuneCore US")
    end

    it "should return TC merchant if payment_splits_renewal is disabled and money_in_bi_transfer is enabled" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:money_in_bi_transfer, person).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:payment_splits_renewal, person).and_return(false)
      payin_provider_config = batch_transaction.send :braintree_payment_splits_config, stored_credit_card.person, options
      expect(payin_provider_config.corporate_entity.name).to eq("TuneCore US")
    end

    it "should return TC merchant if payment_splits_renewal is enabled and money_in_bi_transfer is disabled" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:money_in_bi_transfer, person).and_return(false)
      allow(FeatureFlipper).to receive(:show_feature?).with(:payment_splits_renewal, person).and_return(true)
      payin_provider_config = batch_transaction.send :braintree_payment_splits_config, stored_credit_card.person, options
      expect(payin_provider_config.corporate_entity.name).to eq("TuneCore US")
    end

    it "should return BI merchant if both payment splits feature flags are enabled" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:money_in_bi_transfer, person).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:payment_splits_renewal, person).and_return(true)

      payin_provider_config = batch_transaction.send :braintree_payment_splits_config, stored_credit_card.person, options
      expect(payin_provider_config.corporate_entity.name).to eq("BI Luxembourg")
    end
  end
end
