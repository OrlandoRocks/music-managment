require "rails_helper"

describe Upload do

  def create_upload(file_name="test.mp3")
    upload = Upload.new
    file = double("file")
    allow(file).to receive(:original_filename).and_return(file_name)
    upload.file =  file

    upload
  end

  it "must be most be MP3 or AAC" do
    upload = create_upload("test.wav")
    expect(upload.valid?).to be_falsey
  end

end
