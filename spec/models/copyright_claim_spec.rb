require 'rails_helper'

RSpec.describe CopyrightClaim, type: :model do
  let!(:copyright_claim) { create(:copyright_claim) }

  describe ".search_by_internal_claim_id" do
    it "returns claim with internal claim id matching the search term" do
      album = create(:album, :finalized, name: "Wherever you go")
      claim = create(:copyright_claim, asset: album, internal_claim_id: 'TX123')

      results = CopyrightClaim.search_by_internal_claim_id("TX123")

      expect(results).to include(claim)
    end
  end
  describe ".search_by_account" do
    it "returns claim with internal claim id matching the search term" do
      person = create(:person, name: "The Only user")
      claim = create(:copyright_claim, person: person)

      results = CopyrightClaim.search_by_account("The Only user")

      expect(results).to include(claim)
    end
  end
  describe ".search_by_admin" do
    it "returns claim with internal claim id matching the search term" do
      admin = create(:person, :admin, :with_role, role_name: "Content Review Manager", name: "admin user")
      claim = create(:copyright_claim, admin: admin)

      results = CopyrightClaim.search_by_admin("admin user")

      expect(results).to include(claim)
    end
  end
  describe ".search_by_store" do
    it "returns claim with internal claim id matching the search term" do
      store = create(:store, name:"Test store")
      claim = create(:copyright_claim, store: store)

      results = CopyrightClaim.search_by_store("Test store")

      expect(results).to include(claim)
    end
  end
  context "When creating CopyrightClaim" do
    it "should have hold_amount when compute_hold_amount is true" do
      expect(copyright_claim.hold_amount).to_not be_nil
    end

    it "should not have hold_amount when compute_hold_amount is false" do
      copyright_claim_wo_hold = create(:copyright_claim, compute_hold_amount:false)
      expect(copyright_claim_wo_hold.hold_amount).to be_nil
    end

    it "should deactivate other related claims when a asset has duplicate claims" do
      new_copyright_claim = create(:copyright_claim, asset: copyright_claim.asset)
      expect(CopyrightClaim.active.where(asset: copyright_claim.asset).count).to eq(1)
    end

    it "should validate asset if asset is song" do
      asset = copyright_claim.asset.songs.first
      new_copyright_claim = build(:copyright_claim, asset: asset)
      new_copyright_claim.save
      expect(new_copyright_claim.errors.full_messages).to include("Claim cannot be processed - Already held at release")
    end

    it "should update hold_amount when updated with compute_hold_amount true" do
      copyright_claim_wo_hold = create(:copyright_claim, compute_hold_amount:false)
      expect(copyright_claim_wo_hold.hold_amount).to be_nil
      allow_any_instance_of(LifetimeEarningsFetcher).to receive(:fetch).and_return([{lifetime_earnings: 45}])
      copyright_claim_wo_hold.compute_hold_amount = true
      copyright_claim_wo_hold.save
      expect(copyright_claim_wo_hold.hold_amount).to eq(45)
    end
  end
end
