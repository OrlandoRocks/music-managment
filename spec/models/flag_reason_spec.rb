require "rails_helper"

RSpec.describe FlagReason, type: :model do
  describe ".payoneer_kyc_completed_address_lock" do
    it "returns flag_reason for payoneer_kyc_completed address lock" do
      flag = Flag.find_by(Flag::ADDRESS_LOCKED)
      flag_reason = FlagReason.find_by(
        flag: flag,
        reason: FlagReason::PAYONEER_KYC_COMPLETED
      )

      expect(FlagReason.payoneer_kyc_completed_address_lock)
        .to eq(flag_reason)
    end
  end
end
