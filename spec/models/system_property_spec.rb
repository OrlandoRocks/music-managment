require "rails_helper"

describe SystemProperty do
  describe ".set" do
    context "when a system property exists" do
      it "finds the system property and resets its attributes" do
        name = "SP"
        value = "value"
        description = "description"
        system_property = create(:system_property, name: name)

        result = SystemProperty.set(name, value, description)

        expect(system_property.reload.value).to eq value
        expect(system_property.reload.description).to eq description
        expect(result).to eq system_property.value
      end
    end
  end

  context "when a system property exists" do
    it "creates a system property with attributes" do
      name = "SP"
      value = "value"
      description = "description"

      result = SystemProperty.set(name, value, description)

      system_property = SystemProperty.find_by(name: name)
      expect(system_property.value).to eq value
      expect(system_property.description).to eq description
      expect(result).to eq system_property.value
    end
  end

  describe ".get" do
    context "when a system property exists" do
      it "returns the system property value" do
        name = "SP"
        system_property = create(:system_property, name: name)

        result = SystemProperty.get(name)

        expect(result).to eq system_property.value
      end
    end

    context "when a system property does not exist" do
      it "returns nil" do
        name = "SP"

        result = SystemProperty.get(name)

        expect(result).to be_nil
      end
    end
  end
end
