require "rails_helper"

describe PayoneerAchFee do

  context "#get_fee_cents" do
    it "should return the fee for target country for any country website" do
      person = create(:person)
      payee_details = OpenStruct.new({withdrawal_currency: "USD"})
      fee = PayoneerAchFee.get_fee_cents(person.country_domain, person, payee_details)
      expect(fee).to eq(0.1e3)
    end
  end
end
