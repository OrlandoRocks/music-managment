require "rails_helper"

describe Album, "when dealing with #takedown! and ensuring data consistency" do
  before(:each) do
    @person = TCFactory.build_person
    TCFactory.generate_album_purchase(@person)
    @album = @person.albums.first
  end

  it 'should be able to call #takedown!' do
    expect(@album.takedown!).to be_truthy
  end

  it 'should set known_live_on if not set if the takedown_at time is set' do
    expect(@album.takedown!).to be_truthy
    expect(@album.known_live_on).not_to be_blank
  end

  it 'should set renewal_due_on if the takedown_at time is set' do
    expect(@album.takedown!).to be_truthy
    expect(@album.renewal_due_on).not_to be_blank
  end

  it "should remove the takedown_at date when remove_takedown is set" do
    @album.takedown_at = Time.now
    @album.known_live_on = Time.now
    @album.renewal_due_on = Time.now
    @album.save!

    expect { @album.remove_takedown }.to change{@album.takedown_at}.to(nil)
  end

  it "should create notes when album is taken down" do
    expect(@album.reload.notes).not_to be_present
    @album.takedown!({user_id: @album.person.id, note: 'Album was taken down', subject: 'Takedown'})
    expect(@album.reload.notes).to be_present
  end

  it "should clear the takedown_at on the renewal object" do
    Renewal.adjust_expiration_date_for(@album, Time.now + 1.week)
    @album.takedown!

    expect(@album.remove_takedown).to be_truthy
    @album.reload
    expect(@album.has_unpaid_renewal?).to be_truthy
  end

  it "should set the takedown_at" do
    renewal = @person.renewals.with_renewal_information.first

    expect(renewal.takedown_at).to be_nil
    expect(@album.takedown!).to be_truthy
    renewal.reload
    expect(renewal.takedown_at).not_to be_nil
  end

  it "should not takedown albums processing in the batch" do
    renewal = Renewal.renewal_for(@album)
    purchase = Product.add_to_cart(@person, renewal, Product.find_by(country_website: @person.country_website))
    expect(purchase).not_to be_nil
    invoice = Invoice.factory(@person, [purchase], 'processing_in_batch')
    expect(invoice).not_to be_blank

    expect(@album.takedown!).to eq(false)
  end

  it "should takedown albums that fail validation" do
    @album.name  = nil
    renewal      = @person.renewals.with_renewal_information.first

    expect(renewal.takedown_at).to be_nil
    expect(@album.takedown!).to be_truthy
    renewal.reload
    expect(renewal.takedown_at).not_to be_nil
  end

  context "album has 2 salepoints, with one salepoint for the YoutubeSR store" do
    it "should not call takedown! on a YoutubeSR salepoint" do
      ytsr_store     = Store.find_by(abbrev: "ytsr")
      other_store    = Store.find_by(abbrev: "ww")
      ytsr_salepoint = create(:salepoint, salepointable: @album, store: ytsr_store)
      ww_salepoint   = create(:salepoint, salepointable: @album, store: other_store, variable_price_id: 17)

      expect(ytsr_salepoint).not_to receive(:takedown!)
      expect(ww_salepoint).not_to receive(:takedown!)

      @album.takedown!
    end
  end

  context "album has song that has a track monetization" do
    it "should not takedown the track monetization" do
      song = @album.songs.first
      track_monetization = create(:track_monetization, :eligible, :delivered, song: song)

      @album.takedown!

      expect(@album.takedown_at.present?).to be true
      expect(track_monetization.takedown_at.nil?).to be true
    end
  end

  it 'should send sns notification for takedown' do
    allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

    expect(Sns::Notifier).to receive(:perform).with(
      topic_arn: ENV.fetch('SNS_RELEASE_TAKEDOWN_TOPIC'),
      album_ids_or_upcs: [@album.id],
      store_ids: @album.salepoints.pluck(:store_id),
      delivery_type: 'takedown',
      person_id: nil
    )
    @album.takedown!
  end

  it 'should send sns notification for takedown with high priority and person' do
    allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

    expect(Sns::Notifier).to receive(:perform).with(
      topic_arn: ENV.fetch('SNS_RELEASE_TAKEDOWN_URGENT_TOPIC'),
      album_ids_or_upcs: [@album.id],
      store_ids: @album.salepoints.pluck(:store_id),
      delivery_type: 'takedown',
      person_id: 10
    )
    @album.takedown!({
      priority: 1,
      user_id: 10
    })
  end
end

describe Album, "when removing takedown" do
  before(:each) do
    @person = TCFactory.build_person
    TCFactory.generate_album_purchase(@person)
    @album = @person.albums.first
    @album.takedown!
    expect(@album.takedown_at).not_to be_blank
  end

  it "should work in a transaction" do
    #make the last part fail and asser that the first parts were not saved
    expect(Renewal).to receive(:renewal_for).with(@album).and_raise(StandardError)

    expect(@album.remove_takedown).to be_falsey
    expect(@album.reload.takedown_at).not_to be_blank
  end

  it "should allow date change when not previously released" do
    @album.previously_released = "false"
    @album.update_attribute(:orig_release_year, "2012-01-2")
    @album.update_attribute(:sale_date, "2012-01-1")

    expect(@album.valid?).to be_truthy
    expect(@album.remove_takedown).to be_truthy
  end

  it "should remove takedown for album, salepoints, and renewal" do
    @album.remove_takedown

    expect(@album.takedown_at).to be_blank
    @album.salepoints.each do |salepoint|
      expect(salepoint.takedown_at).to be_blank
    end

    expect(Renewal.renewal_for(@album).takedown_at).to be_blank
  end

  it 'should send sns notification for untakedown' do
    allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

    expect(Sns::Notifier).to receive(:perform).with(
      topic_arn: ENV.fetch('SNS_RELEASE_UNTAKEDOWN_TOPIC'),
      album_ids_or_upcs: [@album.id],
      store_ids: @album.salepoints.pluck(:store_id),
      delivery_type: 'untakedown',
      person_id: nil
    )
    @album.remove_takedown
  end

  it 'should send sns notification for untakedown with high priority and person' do
    allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

    expect(Sns::Notifier).to receive(:perform).with(
      topic_arn: ENV.fetch('SNS_RELEASE_UNTAKEDOWN_URGENT_TOPIC'),
      album_ids_or_upcs: [@album.id],
      store_ids: @album.salepoints.pluck(:store_id),
      delivery_type: 'untakedown',
      person_id: 10
    )
    @album.remove_takedown({
      priority: 1,
      user_id: 10
    })
  end
end
