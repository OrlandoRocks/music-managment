require "rails_helper"

describe Delivery::Retry do
  let(:admin) { create(:person, :admin) } # Admin
  let(:album)  { FactoryBot.create(:album) }
  let!(:salepoint) { create(:salepoint, salepointable: album)}
  let(:petri_bundle) { FactoryBot.create(:petri_bundle, album: album) }
  let!(:distribution) { FactoryBot.create(:distribution, petri_bundle: petri_bundle, converter_class: "MusicStores::Facebook::Converter", salepoints: [salepoint]) }
  let!(:believe_distribution) { FactoryBot.create(:distribution, petri_bundle: petri_bundle, converter_class: "MusicStores::Believe::Converter") }
  let!(:distribution_retry) { Delivery::Retry.new({ delivery: distribution, delivery_type: "metadata_only", priority: 1, person: admin}) }
  let(:distribution_retry_form) { DistributionRetryForm.new(retriable_distributions: [distribution_retry], album: album, ip_address: "1.1.1.1") }

  it "is valid with all attributes set" do
    expect(distribution_retry_form.valid?).to eq true
  end

  it "is invalid with not all attributes set" do
    distribution_retry_form.album = nil
    expect(distribution_retry_form.valid?).to eq false
  end

  describe "#save" do
    it "returns false if distribution_retry_form is invalid" do
      distribution_retry_form.album = nil
      expect(distribution_retry_form.save).to eq false
    end

    it "enqueues the distribution retry job for each retriable_distribution" do
      allow_any_instance_of(Delivery::Retry).to receive(:send_sns_notification)
      distribution_retry_form.retriable_distributions.each do |_|
        expect(Delivery::DistributionWorker).to receive_message_chain(:set, :perform_async)
      end
      distribution_retry_form.save
    end
  end
end
