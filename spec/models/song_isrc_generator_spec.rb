require "rails_helper"

describe Tunecore::SongIsrcGenerator do

  before(:each) do
    @year = Time.now.strftime("%y")
  end

  it "should provide isrc numbers at 0" do
    generator = Tunecore::SongIsrcGenerator.new(0)
    expect(generator.isrc).to eq("TCAAA#{@year}00000")
  end

  it "should provide isrc numbers at 1" do
    generator = Tunecore::SongIsrcGenerator.new(1)
    expect(generator.isrc).to eq("TCAAA#{@year}00001")
  end

  it "should provide isrc numbers with the sequence place holder incrementing to 2" do
    generator = Tunecore::SongIsrcGenerator.new(100000)
    expect(generator.isrc).to eq("TCAAB#{@year}00000")
  end

  it "should provide isrc numbers with the sequence place holder incrementing to 3" do
    generator = Tunecore::SongIsrcGenerator.new(200000)
    expect(generator.isrc).to eq("TCAAC#{@year}00000")
  end

  it "should roll back over to one on id 3500000" do
    generator = Tunecore::SongIsrcGenerator.new(3500000)
    expect(generator.isrc).to eq("TCABK#{@year}00000")
  end

  it "should be on Z for 3499999" do
    generator = Tunecore::SongIsrcGenerator.new(3499999)
    expect(generator.isrc).to eq("TCABJ#{@year}99999")
  end

  it "should skip JP for 600000000" do
    generator = Tunecore::SongIsrcGenerator.new(600000000)
    expect(generator.isrc).to eq("TCJQA#{@year}00000")
  end

  it "should always be a 12 digit number" do
    generator = Tunecore::SongIsrcGenerator.new(1000000000000)
    expect(generator.isrc.size).to eq(12)
  end

  it "should always be a 12 digit number with 0 as the id" do
    generator = Tunecore::SongIsrcGenerator.new(0)
    expect(generator.isrc.size).to eq(12)
  end

  it "should only allow numbers 00001 - 99999 in the NNNNN spot" do
    # CC - XXX - YY - NNNNN
    expect(Tunecore::SongIsrcGenerator::ID_LIMIT).to eq(99999)
  end

  #
  #  Tests to prove that the production dataset will work
  #  when we go live
  #
  it "should not have a Z or 0-9 in the placeholder spot" do
    generator = Tunecore::SongIsrcGenerator.new(1085936)
    expect(generator.isrc).not_to match(/TCAAK([0-9]|Z)\d{7}/)
  end

  it "should have A in the placeholder spot" do
    generator = Tunecore::SongIsrcGenerator.new(1085936)
    expect(generator.isrc).to match(/TCAAK\d{7}/)
  end

end

#
#
#  Testing Years.
#
#
describe Tunecore::SongIsrcGenerator, "when calculating ISRCs with different years" do
  it "should use the last two digits of the current year (1)" do
    expect(Date).to receive(:today).and_return('2010-01-01'.to_date)
    generator = Tunecore::SongIsrcGenerator.new(1085936)
    expect(generator.isrc).to eq("TCAAK1085936")
  end

  it "should use the last two digits of the current year (2)" do
    expect(Date).to receive(:today).and_return('2012-01-01'.to_date)
    generator = Tunecore::SongIsrcGenerator.new(1085936)
    expect(generator.isrc).to eq("TCAAK1285936")
  end

  it "should use the last two digits of the current year (3)" do
    expect(Date).to receive(:today).and_return('2000-01-01'.to_date)
    generator = Tunecore::SongIsrcGenerator.new(1085936)
    expect(generator.isrc).to eq("TCAAK0085936")
  end
end

describe Tunecore::SongIsrcGenerator, "when calculating ISRCs in the spectrum of our OPTION_GROUP map" do
  before(:each) do
    expect(Date).to receive(:today).and_return('2011-08-07'.to_date)
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(1)
    expect(generator.isrc).to eq("TCAAA1100001")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(99999)
    expect(generator.isrc).to eq("TCAAA1199999")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(100000)
    expect(generator.isrc).to eq("TCAAB1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(100001)
    expect(generator.isrc).to eq("TCAAB1100001")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(199999)
    expect(generator.isrc).to eq("TCAAB1199999")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(200002)
    expect(generator.isrc).to eq("TCAAC1100002")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(250002)
    expect(generator.isrc).to eq("TCAAC1150002")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(300000)
    expect(generator.isrc).to eq("TCAAD1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(400000)
    expect(generator.isrc).to eq("TCAAE1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(500000)
    expect(generator.isrc).to eq("TCAAF1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(600000)
    expect(generator.isrc).to eq("TCAAG1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(700000)
    expect(generator.isrc).to eq("TCAAH1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(800000)
    expect(generator.isrc).to eq("TCAAI1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(900000)
    expect(generator.isrc).to eq("TCAAJ1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(1000000)
    expect(generator.isrc).to eq("TCAAK1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(1100000)
    expect(generator.isrc).to eq("TCAAL1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(1200000)
    expect(generator.isrc).to eq("TCAAM1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(1300000)
    expect(generator.isrc).to eq("TCAAN1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(1400000)
    expect(generator.isrc).to eq("TCAAO1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(1500000)
    expect(generator.isrc).to eq("TCAAP1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(1600000)
    expect(generator.isrc).to eq("TCAAQ1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(1700000)
    expect(generator.isrc).to eq("TCAAR1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(1800000)
    expect(generator.isrc).to eq("TCAAS1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(1900000)
    expect(generator.isrc).to eq("TCAAT1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(1999998)
    expect(generator.isrc).to eq("TCAAT1199998")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(2000000)
    expect(generator.isrc).to eq("TCAAU1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(2100067)
    expect(generator.isrc).to eq("TCAAW1100067")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(2200000)
    expect(generator.isrc).to eq("TCAAX1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(2300000)
    expect(generator.isrc).to eq("TCAAY1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(2400000)
    expect(generator.isrc).to eq("TCAAZ1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(2500000)
    expect(generator.isrc).to eq("TCABA1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(2600000)
    expect(generator.isrc).to eq("TCABB1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(2700000)
    expect(generator.isrc).to eq("TCABC1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(2800000)
    expect(generator.isrc).to eq("TCABD1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(2900000)
    expect(generator.isrc).to eq("TCABE1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(3000000)
    expect(generator.isrc).to eq("TCABF1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(3100000)
    expect(generator.isrc).to eq("TCABG1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(3200000)
    expect(generator.isrc).to eq("TCABH1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(3300000)
    expect(generator.isrc).to eq("TCABI1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(3400000)
    expect(generator.isrc).to eq("TCABJ1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(3499997)
    expect(generator.isrc).to eq("TCABJ1199997")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(3499999)
    expect(generator.isrc).to eq("TCABJ1199999")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(3500000)
    expect(generator.isrc).to eq("TCABK1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(6999999)
    expect(generator.isrc).to eq("TCACT1199999")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(7000000)
    expect(generator.isrc).to eq("TCACU1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(14000000)
    expect(generator.isrc).to eq("TCAFP1100000")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(20999999)
    expect(generator.isrc).to eq("TCAIJ1199999")
  end

  it do
    generator = Tunecore::SongIsrcGenerator.new(21000000)
    expect(generator.isrc).to eq("TCAIK1100000")
  end
end

describe Tunecore::SongIsrcGenerator, "when calculating ISRCs with different company identifiers" do
  it "should increment alphabet when the identifier is used" do
    album = create(:album)
    song = create(:song, name: "Song name", album: album)
    generator = Tunecore::SongIsrcGenerator.new(song.id)
    expect(generator.isrc[2,3]).to eq(song.tunecore_isrc[2,3].next!)
  end

  it "increments the alphabet when the identifier is already in use" do
    current_decade = Time.now.year.to_s.last(2)

    album = create(:album)
    song1 = create(:song, name: "Song name", album: album)
    song1.update(tunecore_isrc: "TCAAA#{current_decade}00066")
    song2 = create(:song, name: "Song name", album: album)
    song2.update(tunecore_isrc: "TCAAA#{current_decade}00066")
    generator = Tunecore::SongIsrcGenerator.new(66)
    expect(generator.isrc[2,3]).to eq(song2.tunecore_isrc[2,3].next!)
  end
end
