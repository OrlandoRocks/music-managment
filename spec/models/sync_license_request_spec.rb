require "rails_helper"

describe SyncLicenseRequest do
  it "duration should propagate properly" do
    sr = TCFactory.build(:sync_license_request)
    expect(sr.valid?).to eq(true)

    sr.duration_min = 3
    sr.duration_sec = 40

    sr.save!
    expect(sr.duration).to eq(3 * 60 + 40)

    sr.duration_min = "2"

    expect(sr.duration).to eq(2 * 60 + 40)

    sr.duration_sec = "23"
    sr.save!

    expect(sr.duration).to eq(2 * 60 + 23)

    expect(sr.duration_min).to eq(2)
    expect(sr.duration_sec).to eq(23)
  end

  it "should set default status on create" do
    sr = TCFactory.build(:sync_license_request)
    expect(sr.status).to eq(nil)

    expect(sr.valid?).to eq(true)
    expect(sr.status).to eq("new")

    sr = TCFactory.build(:sync_license_request, status: "revised")
    expect(sr.status).to eq("revised")

    expect(sr.valid?).to eq(true)
    expect(sr.status).to eq("revised")

    sr.status = "test"
    expect(sr.valid?).to eq(false)
  end

  it "should validate presence of muma_song" do
    sr = TCFactory.create(:sync_license_request)
    expect(sr.valid?).to eq(true)

    sr.muma_song.destroy
    sr.reload
    expect(sr.valid?).to eq(false)
  end

  it "should not allow invalid duration values" do
    sr = TCFactory.create(:sync_license_request)
    expect(sr.valid?).to eq(true)

    sr.duration_min = 2
    sr.duration_sec = 40
    expect(sr.valid?).to eq(true)

    sr.duration_min = 4
    sr.duration_sec = 60
    expect(sr.valid?).to eq(false)

    sr.duration_sec = 59
    expect(sr.valid?).to eq(true)
    expect(sr.duration).to eq(4 * 60 + 59)
  end

  it "should save a ticketbooth url if changing status to licensed" do
    composition = create(:composition)
    create(:muma_song, composition: composition)
    s3_asset = create(:s3_asset)
    song = create(:song, s3_asset: s3_asset, composition: composition)
    sync_license_request = create(:sync_license_request, song: song)

    expect_any_instance_of(S3Asset).to(
      receive(:create_restricted_url).with(SyncLicenseRequest::NUMBER_ASSET_DOWNLOADS)
      .and_return("www.restricted_url.com")
    )

    sync_license_request.status = SyncLicenseRequest::STATUSES[:licensed]
    sync_license_request.save

    expect(sync_license_request.ticketbooth_url).to eq("www.restricted_url.com")
  end

  it "should update statuses properly" do
    sr = TCFactory.create(:sync_license_request)
    expect(sr.status).to eq("new")
    expect(sr.new?).to eq(true)
    expect(sr.valid?).to eq(true)

    sr.quote_sent
    expect(sr.status).to eq("quote sent")
    expect(sr.quote_sent?).to eq(true)
    expect(sr.valid?).to eq(true)

    sr.revised
    expect(sr.status).to eq("revised")
    expect(sr.revised?).to eq(true)
    expect(sr.valid?).to eq(true)

    sr.licensed
    expect(sr.status).to eq("licensed")
    expect(sr.licensed?).to eq(true)
    expect(sr.valid?).to eq(true)

    sr.expired
    expect(sr.status).to eq("expired")
    expect(sr.expired?).to eq(true)
    expect(sr.valid?).to eq(true)
  end
end
