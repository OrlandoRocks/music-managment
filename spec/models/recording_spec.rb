require 'rails_helper'

describe Recording do
  it "should validate for the presence of a composition or publishing_composition" do
    song = create(:song)
    composition = create(:composition)
    publishing_composition = create(:publishing_composition)

    recording1 = build(:recording, recordable: song, composition: composition, publishing_composition: nil)
    recording2 = build(:recording, recordable: song, composition: nil, publishing_composition: publishing_composition)
    recording3 = build(:recording, recordable: song, composition: nil, publishing_composition: nil)

    expect(recording1.valid?).to be_truthy
    expect(recording2.valid?).to be_truthy
    expect(recording3.valid?).to be_falsy
  end
end
