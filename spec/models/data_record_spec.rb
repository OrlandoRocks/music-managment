require "rails_helper"

describe DataRecord do
  before(:each) do
    @data_record = DataRecord.new
  end

  it "should never let multiple records with the same data resolution_identifier, data_report_id, measure, and description be created" do
    options = {
      :data_report_id => 1, 
      :resolution_identifier => :D1_2003.to_s
    }
    TCFactory.build_data_record(options)
    expect{
      TCFactory.build_data_record(options)
    }.to raise_error
  end
  
  it "should allow records to have the same data_report_id and resolution_identifier" do
    options = {
      :data_report_id => 1, 
      :resolution_identifier => :D1_2003.to_s, 
      :measure => 'album', 
    }
    TCFactory.build_data_record(
      options.merge(:description => 'completed')
    )

    expect{
      TCFactory.build_data_record(
        options.merge(:description => 'errored')
      )
    }.not_to raise_error
  end
  
end
