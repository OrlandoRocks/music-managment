require "rails_helper"

describe 'PersonPlan' do
  include ActiveSupport::Testing::TimeHelpers
  include PersonPlanSpecHelper
  let(:person) { create(:person) }

  describe "#has_plan?" do
    context 'plan activeness' do
      context "when user does not have a plan" do
        it "returns false" do
          expect(person.has_plan?).to eq(false)
        end
      end

      context "when user has a plan" do
        before do
          create_person_plan!(person, Plan.first, PersonPlanHistory::INITIAL_PURCHASE)
        end

        it "returns true" do
          expect(person.has_plan?).to eq(true)
        end
      end
    end
  end

  describe "current_plan?" do
    let(:plan_id) { Plan::NEW_ARTIST }
    let(:plan) { Plan.find(plan_id) }
    context "When user doesn't have a plan" do
      it "should return false" do
        expect(person.current_plan?(plan_id)).to be false
      end
    end

    context "When user has new artist plan" do
      before do
        create_person_plan!(person, plan, PersonPlanHistory::INITIAL_PURCHASE)
      end

      it "should return true only for new artist plan id" do
        expect(person.current_plan?(Plan::RISING_ARTIST)).to be false
        expect(person.current_plan?(plan_id)).to be true
      end
    end
  end

  describe "#active_plan_on_datetime" do
    context 'active plan during release' do
      before do
        Timecop.freeze(Time.local(2020))
      end

      after do
        Timecop.return
      end

      it 'should return active plan during specified date time' do
        create_person_plan!(person, Plan.second, PersonPlanHistory::INITIAL_PURCHASE)
        travel 1.year
        create_person_plan!(person, Plan.third, PersonPlanHistory::USER_UPGRADE)
        travel 1.year
        create_person_plan!(person, Plan.first, PersonPlanHistory::FORCED_DOWNGRADE)
        travel_back

        expect(person.active_plan_on_datetime(Timecop.freeze(Time.local(2020)))).to eq(Plan.second)
        expect(person.active_plan_on_datetime(Timecop.freeze(Time.local(2021)))).to eq(Plan.third)
        expect(person.active_plan_on_datetime(Timecop.freeze(Time.local(2022)))).to eq(Plan.first)
      end
    end
  end

  describe "#renew!" do
    context "user is upgrading" do
      let!(:person) { create(:person, :with_plan_renewal, plan: Plan.find(Plan::BREAKOUT_ARTIST)) }
      let!(:breakout_purchase) do
        plan_product = person.plan.products.find_by(country_website_id: person.country_website_id)
        create(:purchase, product: plan_product, person: person, related: person.renewals.last)
      end
      let!(:professional_purchase) do
        plan = Plan.find(Plan::PROFESSIONAL)
        plan_product = plan.products.find_by(country_website_id: person.country_website_id)
        create(:purchase, person: person, product: plan_product, related: plan_product)
      end
      it "creates a new renewal and cancels all other renewals" do
        person.person_plan.renew!(professional_purchase)
        renewals = person.renewals

        expect(renewals.length).to be(2)
        expect(renewals.count { |renewal| renewal.canceled? }).to be(1)
      end
    end
    context "user is renewing their plan" do
      let!(:person) { create(:person, :with_plan_renewal) }
      let!(:purchase) do
        plan_product = person.plan.products.find_by(country_website_id: person.country_website_id)
        create(:purchase, product: plan_product, person: person, related: person.renewals.last)
      end
      it "does not create a new renewal" do
        starting_renewals = person.renewals
        person.person_plan.renew!(purchase)
        ending_renewals = person.reload.renewals
        expect(starting_renewals).to eq(ending_renewals)
      end
    end
  end
end
