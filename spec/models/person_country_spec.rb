require 'rails_helper'

RSpec.describe PersonCountry, type: :model do
  let(:person) { create(:person) }

  it do
    should define_enum_for(:source)
      .with(payoneer: "payoneer")
      .backed_by_column_of_type(:string)
  end

  describe "association" do
    it { should belong_to(:person) }
    it { should belong_to(:country) }
  end

  describe "validations" do
    let(:subject) { build(:person_country) }

    it { should validate_presence_of(:person_id) }
    it { should validate_presence_of(:country_id) }
    it { should validate_presence_of(:source) }

    it "should validate country being recorded is not latest from the source" do
      previous = create(:person_country)

      subject = previous.dup

      expect(subject).to be_invalid
      expect(subject.errors.messages[:country]).to include('is already the latest from this source')
    end
  end

  describe ".last_country_recorded" do
    it "should return last record country id from the source" do
      previous = create(:person_country, person: person)

      country_id = PersonCountry.last_country_recorded(previous.source, person.id)

      expect(country_id).to eq(previous.country_id)
    end

    context "no recorded country from the source" do
      it "should return nil" do
        country_id = PersonCountry.last_country_recorded(:payoneer, person.id)

        expect(country_id).to be_nil
      end
    end
  end

  describe ".record_country" do
    context "last recorded country from the source is different" do
      it "should create the record" do
        allow(PersonCountry).to receive(:last_country_recorded).and_return(nil)

        expect {
          PersonCountry.record_country(:payoneer, person.id, 1)
        }.to change(PersonCountry, :count).by(1)
      end
    end

    context "last recorded country is the same" do
      it "should not create the record" do
        allow(PersonCountry).to receive(:last_country_recorded).and_return(1)

        expect {
          PersonCountry.record_country(:payoneer, person.id, 1)
        }.to_not change(PersonCountry, :count)
      end
    end
  end
end
