require "rails_helper"

describe PublishingReport do
  let(:composer){ create(:composer, cae: "9045751234")}
  let(:report_lines){ [] }
  let(:report_date){ Time.now }

  it "creates summary report lines" do
    report_lines << PublishingReport.generate_catalog_summary_report_line(composer.person_id, report_date)
    expect(report_lines.count).to eq(1)
  end

  it "writes the report to the correct file location" do
    report_lines << PublishingReport.generate_catalog_summary_report_line(composer.person_id, report_date)

    summary_report = PublishingReport.write_catalog_summary_report(report_lines, report_date)
    expect(File).to exist(summary_report)

    File.delete(summary_report)
  end

  describe '#client_import' do
    let(:paid_at_date) { Date.today }

    let(:options) { { start_date: paid_at_date.strftime("%m/%d/%Y") } }

    let(:composer_with_pub_admin_purchase) { create(:composer, :with_pub_admin_purchase) }

    describe 'when a composer has a person' do
      it 'returns a valid generated file path' do
        composer_with_pub_admin_purchase

        client_import_file_path = PublishingReport.client_import(options)

        file_exists = File.file?(client_import_file_path)

        expect(file_exists).to be_truthy
      end
    end

    describe 'when a composer does not have an account or a person' do
      it 'returns a valid generated file path regardless' do
        composer = composer_with_pub_admin_purchase
        composer.update_columns(person_id: nil, account_id: nil)

        client_import_file_path = PublishingReport.client_import(options)

        file_exists = File.file?(client_import_file_path)

        expect(file_exists).to be_truthy
      end
    end
  end

  describe '#client_import_csv' do
    let(:paid_at_date) { Date.today }

    let(:options) { { start_date: paid_at_date.strftime("%m/%d/%Y") } }

    let(:composer_with_pub_admin_purchase) { create(:composer, :with_pub_admin_purchase) }

    describe 'when a composer has a person' do
      it 'returns a valid generated file path' do
        composer_with_pub_admin_purchase

        client_import_folder_path, filename = PublishingReport.client_import_csv(options)

        file_exists = File.exist?(File.join(client_import_folder_path, "/New Tax Info.csv"))
        expect(file_exists).to be_truthy
        FileUtils.remove_dir(client_import_folder_path)
      end
    end

    describe 'when a composer does not have an account or a person' do
      it 'returns a valid generated file path regardless' do
        composer = composer_with_pub_admin_purchase
        composer.update_columns(person_id: nil, account_id: nil)

        client_import_folder_path, filename = PublishingReport.client_import_csv(options)

        file_exists = File.exist?(File.join(client_import_folder_path, "/New Tax Info.csv"))

        expect(file_exists).to be_truthy
        FileUtils.remove_dir(client_import_folder_path)
      end
    end
  end
end

