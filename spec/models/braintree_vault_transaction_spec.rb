require "rails_helper"

describe BraintreeVaultTransaction do
  def successful_response
    'response=1&responsetext=Customer%20Update%20Successful&authcode=&transactionid=0&avsresponse=&cvvresponse=&orderid=&type=&response_code=100&customer_vault_id=1460284452&username=1118461&time=20090825212928&amount=&hash=12e39a45a7f57dcc3aff409e29d06213&merchant_defined_field_5=1&merchant_defined_field_10=1'
  end

  def invalid_card_response
    'response=1&responsetext=Customer%20Update%20Successful&authcode=&transactionid=0&avsresponse=&cvvresponse=&orderid=&type=&response_code=100&customer_vault_id=1460284452&username=1118461&time=20090825212928&amount=&hash=12e39a45a7f67dcc3aff409e29d06213&merchant_defined_field_5=0&merchant_defined_field_10=1'
  end

  before do
    ActiveRecord::Base.connection.execute "SET FOREIGN_KEY_CHECKS = 0";
  end

  after do
    ActiveRecord::Base.connection.execute "SET FOREIGN_KEY_CHECKS = 1";
  end

  context "the braintree response is not valid" do
    subject { BraintreeVaultTransaction.process("StoredCreditCard", invalid_card_response, 1, '127.0.0.1') }

    it "does not process the response" do
      response = double('bt_response')
      allow(ActiveMerchant::Billing::Integrations::Braintree::Return).to receive(:new).and_return(response)
      expect(response).to receive(:valid?).and_return(false)

      subject

      expect(BraintreeVaultTransaction.count).to eq(0)
    end
  end

  context "when the response code received from BrainTree is 100" do
    subject { BraintreeVaultTransaction.process("StoredCreditCard",successful_response, 1, '127.0.0.1') }

    let(:response) { double('bt_response') }
    let(:stored_card) { double('stored_cc') }

    before do
      allow(ActiveMerchant::Billing::Integrations::Braintree::Return).to receive(:new).and_return(response)
      allow(response).to receive(:valid?).and_return(true)
      allow(response).to receive(:mark_as_preferred?).and_return(false)
      allow(response).to receive(:response_code).and_return(100)
      allow(response).to receive(:customer_id).and_return('23453465')
      allow(response).to receive(:message).and_return("Customer Added")
      allow(stored_card).to receive(:id).and_return(1)
    end

    it "creates a new stored credit card" do
      expect(StoredCreditCard).to receive(:find_by).exactly(2).times.and_return(nil)
      expect(StoredCreditCard).to receive(:create).and_return(stored_card)

      subject
    end

    it "returns a response of '100' " do
      processed_transaction = subject

      expect(processed_transaction.response_code).to eq "100"
    end
  end

  context "'mark as preferred' is set" do
    subject { BraintreeVaultTransaction.process("StoredCreditCard",successful_response, 1, '127.0.0.1') }

    it "creates a person preference for the customer" do
      response = double('bt_response')
      allow(ActiveMerchant::Billing::Integrations::Braintree::Return).to receive(:new).and_return(response)
      allow(response).to receive(:valid?).and_return(true)
      allow(response).to receive(:mark_as_preferred?).and_return(true)
      allow(response).to receive(:renew_with_balance?).and_return(true)
      allow(response).to receive(:response_code).and_return(100)
      allow(response).to receive(:customer_id).and_return('23453465')
      allow(response).to receive(:message).and_return("Customer Added")
      allow(PersonPreference).to receive(:where).and_return([])

      stored_card = double('stored_cc')
      allow(stored_card).to receive(:id).and_return(1)

      expect(StoredCreditCard).to receive(:find_by).exactly(2).times.and_return(nil)
      expect(StoredCreditCard).to receive(:create).and_return(stored_card)
      expect(PersonPreference).to receive(:create)

      subject
    end
  end

  context "when the response code received from BrainTree is 300" do
    subject { BraintreeVaultTransaction.process("StoredCreditCard",successful_response, 1, '127.0.0.1') }

    it "does not create a new stored credit card" do
      response = double('bt_response')
      allow(ActiveMerchant::Billing::Integrations::Braintree::Return).to receive(:new).and_return(response)
      allow(response).to receive(:valid?).and_return(false)
      stored_card = double('stored_cc')

      expect(StoredCreditCard).not_to receive(:create)

      subject
    end

    it "returns a response of '300'" do
      processed_transaction = subject

      expect(processed_transaction.response_code).to eq "300"
    end
  end

  context "when the card is already in our db" do
    subject { BraintreeVaultTransaction.process("StoredCreditCard",successful_response, 1, '127.0.0.1') }

    it "updates the stored credit card" do
      response = double('bt_response')
      allow(ActiveMerchant::Billing::Integrations::Braintree::Return).to receive(:new).and_return(response)
      allow(response).to receive(:valid?).and_return(true)
      allow(response).to receive(:mark_as_preferred?).and_return(false)
      allow(response).to receive(:response_code).and_return(100)
      allow(response).to receive(:customer_id).and_return('23453465')
      allow(response).to receive(:message).and_return("Card Updated")
      stored_card = double('stored_cc')
      allow(stored_card).to receive(:id).and_return(1)

      expect(StoredCreditCard).to receive(:find_by).and_return(stored_card)
      expect(BraintreeVaultTransaction).to receive(:process_related_stored_item).and_return(stored_card)

      subject
    end
  end

  context "Checkbox permutations"  do
    before do
      @response = double('bt_response')
      allow(ActiveMerchant::Billing::Integrations::Braintree::Return).to receive(:new).and_return(@response)
      allow(@response).to receive(:valid?).and_return(true)
      allow(@response).to receive(:response_code).and_return(100)
      allow(@response).to receive(:customer_id).and_return('23453465')
      allow(@response).to receive(:message).and_return("Customer Added")
    end

    it "should create a person preference if 'mark as preferred' is set" do
      allow(@response).to receive(:mark_as_preferred?).and_return(true)
      allow(@response).to receive(:renew_with_balance?).and_return(true)

      allow(PersonPreference).to receive(:where).and_return([])
      expect(PersonPreference).to receive(:create)

      mock_braintree_query
      BraintreeVaultTransaction.process("StoredCreditCard", successful_response, 1, '127.0.0.1')
    end

    it "should NOT create a person preference if 'mark as preferred' is not set" do
      allow(@response).to receive(:mark_as_preferred?).and_return(false)
      allow(@response).to receive(:renew_with_balance?).and_return(true)

      expect(PersonPreference).not_to receive(:first)
      expect(PersonPreference).not_to receive(:create)

      mock_braintree_query
      BraintreeVaultTransaction.process("StoredCreditCard", successful_response, 1, '127.0.0.1')
    end

    it "should update the customer's preferences if 'mark as preferred' is set and the customer has preferences set already" do
      allow(@response).to receive(:mark_as_preferred?).and_return(true)
      allow(@response).to receive(:renew_with_balance?).and_return(true)

      prefs = double('person_preferences', id: 1)

      allow(PersonPreference).to receive(:where).and_return([prefs])

      expect(prefs).to receive(:update)
      expect(PersonPreference).not_to receive(:create)

      mock_braintree_query
      BraintreeVaultTransaction.process("StoredCreditCard", successful_response, 1, '127.0.0.1')
    end
  end
end
