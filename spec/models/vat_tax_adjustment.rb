require 'rails_helper'

RSpec.describe VatTaxAdjustment, type: :model do
  let(:person) { FactoryBot.create(:person, :with_tc_social, country: "Luxembourg") }
  let(:person_intake) { FactoryBot.create(:person_intake, person: person) }
  describe "Associations" do
    it { should belong_to(:person) }
  end

  describe "Validations" do
    it "related_id should be unique for a particular related type" do
      VatTaxAdjustment.create(related: person_intake, person: person)
      vat = VatTaxAdjustment.create(related: person_intake, person: person)
      expect(vat).to_not be_valid
    end
  end
end
