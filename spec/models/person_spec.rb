require "rails_helper"
include ActiveSupport::Testing::TimeHelpers

describe Person, "scopes" do
  describe "associations" do
    it { should have_many(:royalty_split_recipients) }
    it { should have_many(:owned_royalty_splits).with_foreign_key(:owner_id).class_name('RoyaltySplit') }

    describe "current_tax_forms_across_programs" do
      it "returns the latest active tax-forms across programs" do
        person = create(:person)
        tax_form1 = create(:tax_form, person: person, created_at: 1.minute.ago, payout_provider_config: PayoutProviderConfig.by_env.first)
        tax_form2 = create(:tax_form, person: person, created_at: Time.now, payout_provider_config: PayoutProviderConfig.by_env.first)
        tax_form3 = create(:tax_form, person: person, created_at: Time.now - 1.second, payout_provider_config: PayoutProviderConfig.by_env.last)
        tax_form4 = create(:tax_form, person: person, created_at: Time.now, payout_provider_config: PayoutProviderConfig.by_env.last)
        expect(person.current_tax_forms_across_programs).not_to include [tax_form1, tax_form3]
        expect(person.current_tax_forms_across_programs).to match_array [tax_form2, tax_form4]
      end
    end
  end

  context "for_person named scope" do
    it "returns a single person" do
      person = create(:person)

      result = Person.for_person(person.id)

      expect(result).to include(person)
    end
  end

  context "with_no_taxform_associations named scope" do
    it "returns users with no tax form associations" do
      3.times { create(:person, :with_tax_form) }
      people_without_taxforms = 4.times.map { create(:person) }

      result = Person.with_no_taxform_associations.pluck(:id)
      expect(result).to_not include(people_without_taxforms.pluck(:id))
    end
  end

  context "with_no_lifetime_earning named scope" do
    it "returns users with no tax form associations" do
      4.times { create(:person) }
      people_with_lifetime_earnings    = 3.times.map { create(:person, :with_lifetime_earnings) }

      result = Person.with_no_lifetime_earning.pluck(:id)
      expect(result).to_not include(people_with_lifetime_earnings.pluck(:id))
    end
  end

  context "with_no_valid_taxforms_or_associations named scope" do
    it "returns users with invalid taxforms" do
      people_with_invalid_taxforms = 4.times.map { create(:person, :with_invalid_tax_form) }
      expect(Person.with_no_valid_taxforms_or_associations.pluck(:id))
        .to include(*people_with_invalid_taxforms.pluck(:id))
    end

    it "returns users with no taxforms" do
      people_without_taxform_associations = 2.times.map { create(:person) }
      expect(Person.with_no_valid_taxforms_or_associations.pluck(:id))
        .to include(*people_without_taxform_associations.pluck(:id))
    end

    it "does not return users with valid(w9) taxform" do
      people_with_valid_taxform = 3.times.map { create(:person, :with_tax_form) }
      expect(Person.with_no_valid_taxforms_or_associations.pluck(:id))
        .to_not include(people_with_valid_taxform.pluck(:id))
    end
  end

  context "with_total_albums named scope" do
    it "returns a person with the requested number of albums" do
      person_with_albums = create(:person, :with_live_albums)
      person_without_albums = create(:person)

      result = Person.with_total_albums(person_with_albums.albums.count)

      expect(result).to include(person_with_albums)
      expect(result).to_not include(person_without_albums)
    end
  end

  context "with_renewals_for_day named scope" do
    it "returns a person with renewals for a given day" do
      person = create(:person, :with_stored_credit_card)
      person_without_renewal = create(:person)
      create(
        :person_preference,
        person: person,
        preferred_credit_card: person.stored_credit_cards.first
      )
      product = create(
        :product,
        created_by: person,
        expires_at: 1.month.from_now
      )
      purchase = create(
        :purchase,
        :unpaid,
        person: person,
        product: product
      )
      product_item = create(
        :product_item,
        :with_duration,
        :yearly,
        created_at: 6.months.ago,
        product: product
      )
      renewal_without_takedown = create(
        :renewal,
        :no_takedown_at,
        item_to_renew: product_item,
        person: person,
        purchase: purchase
      )

      result = Person.with_renewals_for_day(
        false,
        renewal_without_takedown.renewal_history.first.expires_at
      )

      expect(result).to include(person)
      expect(result).to_not include(person_without_renewal)
    end
  end

  context "with_positive_balance scope" do
    let!(:rich_person) { create(:person, :with_balance, amount: 50_000) }
    let!(:poor_person) { create(:person, :with_balance, amount: 100) }
    let!(:broke_person) { create(:person) }
    let!(:those_with_dollars) { Person.with_positive_balance }
    let!(:adam_sandler_in_uncut_gems) { create(:person, :with_balance, amount: -100_000) }

    it "returns people with positive balance" do
      expect(those_with_dollars).to include(rich_person, poor_person)
    end

    it "does not return people with 0 balance" do
      expect(those_with_dollars).to_not include(broke_person)
    end

    it "does not return people with negative balance" do
      expect(those_with_dollars).to_not include(adam_sandler_in_uncut_gems)
    end
  end

  context "needs_marked_dormant scope" do
    let!(:rich_person) { create(:person, :with_balance, amount: 50_000) }
    let!(:poor_person) { create(:person, :with_balance, amount: 100) }
    let!(:broke_person) { create(:person) }
    let!(:dormant_person) do  create(:person, :with_balance, amount: 100, dormant: true)
    end
    let!(:adam_sandler_in_uncut_gems) { create(:person, :with_balance, amount: -100_000) }
    let!(:new_person) { create(:person, :with_balance, amount: 5000) }

    #before each is necessary because the created_on is getting updated by a callback in the person model
    # they were too cool for timestamps cause who needs beautiful built in rails methods or to know when
    # YOUR PERSON MODEL IS UPDATED, so we get to do this.
    before(:each) do
      old_people = [rich_person,poor_person,broke_person,dormant_person,adam_sandler_in_uncut_gems]
      old_people.each do |p|
        p.update(created_on: Time.current - 4.years)
      end
      new_person.update(created_on: Time.current - 1.year)
    end

    it "returns people with positive balance" do
      needs_marked_dormant = Person.needs_marked_dormant
      expect(needs_marked_dormant).to include(rich_person, poor_person)
    end

    it "does not return people with 0 balance" do
      needs_marked_dormant = Person.needs_marked_dormant
      expect(needs_marked_dormant).to_not include(broke_person)
    end

    it "does not return people with negative balance" do
      needs_marked_dormant = Person.needs_marked_dormant
      expect(needs_marked_dormant).to_not include(adam_sandler_in_uncut_gems)
    end

    it "does not return people that are already marked dormant" do
      needs_marked_dormant = Person.needs_marked_dormant
      expect(needs_marked_dormant).to_not include(dormant_person)
    end

    it "does not return people whose accounts were created in the past 3 years" do
      needs_marked_dormant = Person.needs_marked_dormant
      expect(needs_marked_dormant).to_not include(new_person)
    end

    it "returns only people that have not logged in the past 3 years" do
      rich_person.update(recent_login: Time.current)
      needs_marked_dormant = Person.needs_marked_dormant

      expect(needs_marked_dormant).to include(poor_person)
      expect(needs_marked_dormant).to_not include(rich_person)
    end
  end

  context "with flags" do
    let(:flag_1) { Flag.find_by(Flag::WITHDRAWAL) }
    let(:flag_2) { Flag.find_by(Flag::FIRST_NAME_LOCKED) }
    let!(:person_without_flags) { create(:person) }
    let!(:person_with_correct_flag) { create(:person) }
    let!(:person_with_wrong_flag) { create(:person) }

    before do
      person_with_correct_flag.flags << flag_1
      person_with_wrong_flag.flags << flag_2
    end

    describe "with_flag named scope" do
      it "returns only people with the specified flag" do
        people = Person.with_flag(Flag::WITHDRAWAL)

        expect(people).to include(person_with_correct_flag)
        expect(people).not_to include(person_without_flags, person_with_wrong_flag)
      end
    end

    describe "without_flag named scope" do
      it "returns only people without the specified flag" do
        people = Person.without_flag(flag_1)

        expect(people).not_to include(person_with_correct_flag)
        expect(people).to include(person_without_flags, person_with_wrong_flag)
      end
    end
  end

  context "with payout_providers" do
    let!(:person_without_payout_providers) { create(:person) }
    let!(:person_with_inactive_payout_provider) { create(:person, :with_inactive_payout_provider) }
    let!(:person_with_active_payout_provider) { create(:person, :with_payout_provider) }

    describe "with_active_payout_providers named scope" do
      it "returns only people with an active payout provider" do
        results = described_class.with_active_payout_providers
        expect(results).to include(person_with_active_payout_provider)
        expect(results).not_to include(
          person_without_payout_providers, person_with_inactive_payout_provider
        )
      end
    end

    describe "without_active_payout_providers named scope" do
      it "returns only people without active payout providers" do
        results = described_class.without_active_payout_providers
        expect(results).to include(
          person_without_payout_providers, person_with_inactive_payout_provider
        )
        expect(results).not_to include(person_with_active_payout_provider)
      end
    end
  end

  context "with lifetime_earnings" do
    let!(:person_without_lifetime_earnings) { create(:person) }
    let!(:person_with_zero_lifetime_earning_record) { create(:person, :with_zero_lifetime_earnings) }
    let!(:person_with_nonzero_lifetime_earning_record) { create(:person, :with_lifetime_earnings) }

    describe "with_lifetime_earnings named scope" do
      it "returns people who have a lifetime_earning record with a non-zero person_intake or youtube_intake amount" do
        results = described_class.with_lifetime_earnings
        expect(results).to include(
          person_with_nonzero_lifetime_earning_record
        )
        expect(results).not_to include(
          person_without_lifetime_earnings,
          person_with_zero_lifetime_earning_record
        )
      end
    end
  end
end

describe Person do
  describe "#valid?" do
    context "when no email is passed" do
      it "should be invalid" do
        person = build(:person, country: nil)
        expect(person.valid?).to be_falsey
        expect(person.save).to be_falsey
        expect(person.errors.full_messages.size).to eq(1)
        expect(person.errors.full_messages.first).to eq("Country can't be blank")
      end
    end
  end

  describe ".india_user?" do
    context "for user with india country website" do
      it "returns true" do
        person = create(:person, country: "IN")
        expect(person.india_user?).to be_truthy
      end
    end

    context "for user a non india country website" do
      it "returns false" do
        person = create(:person, country_website_id: 1)
        expect(person.india_user?).to be_falsey
      end
    end
  end

  describe "#latest_gst_info?" do
    it "should return the latest gst_info record of this person" do
      person = create(:person, country: "IN")
      create(:gst_info, person: person, gstin: "1234567890ABCDE", created_at: Time.now() - 10.days)
      latest_gst_info = create(:gst_info, person: person, gstin: "1234567890FGHIJ", created_at: Time.now())
      expect(person.latest_gst_info).to eq(latest_gst_info)
    end

    it "should return nil if there are not gst_info records for this person" do
      person = create(:person, country: "IN")
      expect(person.latest_gst_info).to be_nil
    end
  end

  describe ".valid_phone_number?" do
    context "phone number is nil" do
      context "for india site users" do
        it "should not save and return false" do
          indian_user = build(:person, country: "IN", country_website_id: CountryWebsite::INDIA, phone_number: nil)

          expect(indian_user.save).to be_falsey
          expect(indian_user.errors.full_messages.size).to eq(2)
          expect(indian_user.errors.full_messages.first).to eq("Mobile number or phone number must be present")
        end
      end

      context "for non india site users" do
        it "should save" do
          person = build(:person, phone_number: nil)

          expect(person.save).to be_truthy
        end
      end
    end
  end

  context ".country" do
    context "when the country is an iso-2 code" do
      it "returns a full country name" do
        person = FactoryBot.build(:person, country: "US")
        expect(person.country).to eq "United States"
      end
    end

    context "when the country is a full country name" do
      it "returns a full country name" do
        person = FactoryBot.build(:person, country: "US")
        expect(person.country).to eq "United States"
      end
    end
  end

  context "when checking YouTube Money enrollment" do
    it "correctly reports when not enrolled" do
      person = FactoryBot.create(:person)
      expect(person.has_youtube_money?).to eq false
    end

    it "correctly reports when enrolled" do
      person = FactoryBot.create(:person)
      FactoryBot.create(:youtube_monetization, person_id: person.id)
      expect(person.has_youtube_money?).to eq true
    end
  end


  describe "#youtube_monetization" do
    it "should return the person's latest YoutubeMonetization record" do
      person = create(:person)
      create(:youtube_monetization, person_id: person.id)
      ytmtwo = create(:youtube_monetization, person_id: person.id)

      expect(person.youtube_monetization).to eq(ytmtwo)
    end
  end

  describe "#has_active_youtube_monetization" do
    it "should return false if a user has a terminated youtube monetization" do
      person = create(:person)
      ytm = create(:youtube_monetization, person_id: person.id)
      ytm.update(termination_date: DateTime.current - 1.days)

      expect(person.has_active_youtube_monetization?).to eq(false)
    end

    it "should return true if a user has an active youtube monetization" do
      person = create(:person)
      ytm = create(:youtube_monetization, person_id: person.id)

      expect(person.has_active_youtube_monetization?).to eq(true)
    end
  end

  context "when determining if any stores on sale are missing from distributions" do
    let(:person) { TCFactory.create(:person) }
    let(:store)  { TCFactory.create(:store, name: "Ray's Music Exchange", launched_at: Time.now, on_sale: true) }
    let!(:album_1) { TCFactory.create(:album, person: person, finalized_at: Time.now) }
    let!(:album_2) { TCFactory.create(:album, person: person, finalized_at: Time.now) }
    let(:stores_on_sale) { Store.stores_on_sale }

    it "returns no stores when the person has no albums" do
      expect(person.missing_on_sale_stores(stores_on_sale)).to be_empty
    end

    context "when the person has albums" do
      context "not all of which some have been distributed to a store" do
        it "returns 'on sale' stores that have NOT been distrbuted to by all albums" do
          SalepointableStore.create(store: store, salepointable_type: "Album")
          TCFactory.create(:salepoint, salepointable: album_1, store_id: store.id)

          expect(person.missing_on_sale_stores?(stores_on_sale)).to eq true
        end
      end

      context "which have all been distributed" do
        it "does not return 'on sale' stores that have been distributed to" do
          SalepointableStore.create(store: store, salepointable_type: "Album")
          TCFactory.create(:salepoint, salepointable: album_1, store_id: store.id)
          TCFactory.create(:salepoint, salepointable: album_2, store_id: store.id)

          expect(person.missing_on_sale_stores(stores_on_sale)).to be_empty
          expect(person.missing_on_sale_stores?(stores_on_sale)).to eq false
        end

        it "returns 'on sale' stores that have NOT been distributed to" do
          store_distributed_to = store
          SalepointableStore.create(store: store_distributed_to, salepointable_type: "Album")
          TCFactory.create(:salepoint, salepointable: album_1, store_id: store_distributed_to.id)
          TCFactory.create(:salepoint, salepointable: album_2, store_id: store_distributed_to.id)

          store_not_distributed_to = TCFactory.create(:store, name: "Not Distributed To", launched_at: Time.now, on_sale: true)
          SalepointableStore.create(store: store_not_distributed_to, salepointable_type: "Album")

          stores_on_sale = Store.stores_on_sale

          expect(person.missing_on_sale_stores(stores_on_sale)).to     include(store_not_distributed_to)
          expect(person.missing_on_sale_stores(stores_on_sale)).not_to include(store_distributed_to)
          expect(person.missing_on_sale_stores?(stores_on_sale)).to eq true
        end
      end
    end
  end

  context "when creating a new person" do
    let(:person) { TCFactory.build_person }

    it "does not have an apple ID" do
      expect(person.apple_id).to eq nil
    end

    it "sets the person's balance to $0" do
      expect(person.person_balance.balance).to eq 0
    end

    it "sets the person's active statue to true" do
      expect(person.status).to eq 'Active'
      expect(person.active?).to eq true
    end

    context "when created with a person from the US" do
      it "sets the person's currency to USD" do
        expect(person.currency).to eq "USD"
      end

      it "sets the person's balance to USD" do
        expect(person.person_balance.currency).to eq "USD"
      end
    end
  end

  context "when creating a new record it should process targeted offers" do
    before(:each) do
      @person = TCFactory.build(:person)
    end

    it "should call add_to_targeted_offer" do
      expect(@person).to receive(:add_to_targeted_offer)
      @person.save
    end

    it "should use a valid join token from the form submission" do
      @person.referral_data.build(key: "jt", value: "abc")
      expect(TargetedOffer).to receive(:add_person_with_join_token).with(@person, "abc")
      @person.save
    end
  end

  context "when updating an existing person's email" do
    let(:person) { create(:person) }

    it "should call create_profile_modification_note" do
      expect(person).to receive(:create_profile_modification_note)
      person.email = 'test@gmail.com'
      person.save(validate: false)
    end

    it "should notify airbrake when email is changed" do
      allow(Person).to receive(:current_user_from_paper_trail).and_return(person)
      expect(Airbrake).to receive(:notify)
      person.email = 'test@gmail.com'
      person.save(validate: false)
    end
  end

  describe ".update_zendesk" do
    context "certian attributes are changed" do
      let(:person) { create(:person, is_verified: false) }

      it "should call send_to_zendesk when email is changed and person is verified" do
        expect(Zendesk::Person).to receive(:create_apilogger)
        person.email = 'test@gmail.com'
        person.is_verified = true
        person.save(validate: false)
      end

      it "should call send_to_zendesk when name is changed and person is verified" do
        expect(Zendesk::Person).to receive(:create_apilogger)
        person.email = 'Zendesk'
        person.is_verified = true
        person.save(validate: false)
      end
    end
  end

  context "when determining person's balance" do
    let(:person) { create(:person, :with_balance, amount: 100) }

    it "should not add any decimal places if balance is a whole number" do
      expect(person.balance).to eq 100
    end

    it "should round to the hundreth if balance is not a whole number" do
      person.person_balance.balance = 123.456
      expect(person.balance).to eq 123.45
    end

    context "when determining balance_cents" do
      it "balance's decimal should move two places (multiply by 100) and keep its rounded value" do
        person.person_balance.balance = 123.456
        expect(person.balance_cents).to eq 12_345

        person.person_balance.balance = 123
        expect(person.balance_cents).to eq 12_300
      end
    end
  end

  context "when determining a person's available balance" do
    let(:person) { create(:person, :with_balance, amount: 100) }

    context "when the person does not have any copyright claims" do
      it "should return the person balance" do
        expect(person.available_balance).to eq person.person_balance.balance
      end
    end

    context "when the person has copyright claims" do
      let(:copyright_claim1) { create(:copyright_claim, person: person) }
      let(:copyright_claim2) { create(:copyright_claim, person: person) }

      before do
        person.copyright_claims << copyright_claim1
        person.copyright_claims << copyright_claim2
      end

      let(:hold_amount1) {copyright_claim1.hold_amount}
      let(:hold_amount2) {copyright_claim2.hold_amount}

      it "should return the person balance subtracted by the sum of the hold amounts" do
        expected_balance = person.balance - hold_amount1 - hold_amount2
        expect(person.available_balance).to eq expected_balance
      end

      it "should round to the hundreth if available_balance is not a whole number" do
        person.person_balance.balance = 123.456

        expect(person.available_balance).to eq 123.45 - (hold_amount1 + hold_amount2)
      end

      context "when determining available_balance_cents" do
        it "balance's decimal should move two places (multiply by 100) and keep its rounded value" do
          person.person_balance.balance = 123.456
          expect(person.available_balance_cents).to eq 12_345  - hold_amount1 - hold_amount2
        end
      end
    end
  end

  context "when determining a person's hold amount" do
    let(:person) { create(:person, :with_balance, amount: 100) }
    let(:copyright_claim1) { create(:copyright_claim, person: person) }
    let(:copyright_claim2) { create(:copyright_claim, person: person) }

    before do
      person.copyright_claims << copyright_claim1
      person.copyright_claims << copyright_claim2
    end

    let(:hold_amount1) {copyright_claim1.hold_amount}
    let(:hold_amount2) {copyright_claim2.hold_amount}

    it "should return the sum of all the copyright claim holds amounts" do
      expect(person.hold_amount).to eq hold_amount1+hold_amount2
    end

    it "should round to the hundreth if hold_amount is not a whole number" do
      copyright_claim1.update(hold_amount: 123.456)
      expect(person.hold_amount).to eq 123.45 + hold_amount2
    end

    context "when determining hold_amount_cents" do
      it "hold_amount's decimal should move two places (multiply by 100) and keep its rounded value" do
        copyright_claim1.update(hold_amount: 123.456)
        copyright_claim2.update(hold_amount: 12.34)
        expect(person.hold_amount_cents).to eq 12_345 + 1_234

        copyright_claim1.update(hold_amount: 123)
        copyright_claim2.update(hold_amount: 12)
        expect(person.hold_amount_cents).to eq 12_300 + 1_200
      end
    end

    context "when a person has an inactive copyright claim" do
      it "the inactive copyright claim is discarded when calculating the hold amount" do
        copyright_claim1.update(hold_amount: 100)
        copyright_claim2.update(hold_amount: 50, is_active: false)
        expect(person.hold_amount_cents).to eq 10_000
      end
    end
  end

  context "when updating an email address" do
    it "creates a note regarding the changes" do
      new_email = "updated@newemail.com"
      admin = create(:person, :admin)

      allow(PaperTrail).to receive_message_chain(:request, :whodunnit).and_return(admin.id)
      allow(PaperTrail).to receive_message_chain(:request, :controller_info, :[]).and_return("0.0.0.0")

      person = create(:person)
      old_email = person.email

      person.update(email: new_email)

      expect(person.reload.notes.first.note)
        .to match(/Changed field "email" from.*#{old_email}.*#{new_email}/)
    end
  end

  describe "#account_held?" do
    it "must have at least one active copyright claim, or the account must be locked, or the account must be marked suspicious" do
      person_with_copyright_claim = create(:person, :with_active_copyright_claim)
      expect(person_with_copyright_claim.account_held?).to be true

      person_with_locked_account = create(:person, :with_locked_account)
      expect(person_with_locked_account.account_held?).to be true

      person_with_suspicious_account = create(:person, :with_suspicious_account)
      expect(person_with_suspicious_account.account_held?).to be true
    end
  end
end

describe Person do
  context "when creating a new record" do
    describe "royalty split recipient" do
      let(:email) { "testemail123@domain.fake" }
      let!(:recipient) { create :royalty_split_recipient, :with_email, email: email }
      before do
        @person = FactoryBot.create(:person, email: email)
        recipient.reload
      end
      it { expect(recipient.email).to be_nil }
      it { expect(recipient.person).to eql(@person) }
    end
  end
end

describe Person, "when validating the email confirmation" do
  it "should validate the confirmation of email on an existing record when changing the original" do
    person_defaults = TCFactory.person_defaults()
    person = Person.create(person_defaults)
    person.email_confirmation = "something_new@test.com"
    person.original_password = "Testpass123!"
    assert person.invalid?, "Expected a valid person:  #{errors(person)}"
  end

  it "should not fail validation confirmation of email if the new email matches the existing email" do
    person_defaults = TCFactory.person_defaults()
    person = Person.create(person_defaults)
    person.name = "Joe Schmoe "
    person.original_password = "Testpass123!"
    person.email_confirmation = person.email
    assert person.valid?, "Expected valid person:  #{errors(person)}"
  end
end

describe Person, "when adding an address" do
  it "should allow setting of postal code" do
    person_defaults = TCFactory.person_defaults(country: "AU", postal_code: "65201")
    person = Person.new(person_defaults)
    assert person.save, "Expected person to be valid: #{errors(person)}"
    assert person.reload.postal_code, "Expected postal code to have a value"
  end
end

describe Person, "when editing name" do
  before(:each) do
    @person_defaults = TCFactory.person_defaults
    @person = Person.new(@person_defaults)
  end

  # GC - We removed separating names into first and last name, so I adjusted this test
  it "should not be able to enter first and last name seperately" do
    @person.update(first_name: "John", last_name: "Smith")
    expect(@person.name).not_to eq("John Smith")
  end

  it "should not overwrite the name if first and last name are not set" do
    @person.update(name: "John Smith")
    expect(@person.name).to eq("John Smith")
  end

  it "should be able to separate full name into first and last name" do
    @person.name = "John Smith"
    expect(@person.first_name).to eq("John")
    expect(@person.last_name).to eq("Smith")
  end

  it "should validate first and last name are set" do
    @person.name = nil
    @person.save
    expect(@person.errors.size).not_to eq 0
    expect(@person.errors.size).not_to eq 0
  end
end

describe Person, "when creating a password for a new record" do
  it "should not be valid if the password fields are blank" do
    person = Person.create(TCFactory.person_defaults(password: '', password_confirmation: ''))
    expect(person.errors[:password]).to include(I18n.t("errors.messages.blank"))
  end

  it "should not be valid if the password confirmation field is blank" do
    person = Person.create(TCFactory.person_defaults(password: 'Testpass123!', password_confirmation: ''))
    expect(person.errors[:password_confirmation]).to include(I18n.t("errors.messages.blank"))
  end

  it "should not be valid if the passwords do not match" do
    person = Person.create(TCFactory.person_defaults(password: 'Testpass123!', password_confirmation: 'passtest'))
    expect(person.errors[:password_confirmation]).to include(I18n.t("models.person.passwords_do_not_match"))
  end

  it "should pass if the passwords match" do
    person = Person.create(TCFactory.person_defaults(password: 'Testpass123!', password_confirmation: 'Testpass123!'))
    expect(person.errors.size).to eq 0
  end

  # This test is in response to an odd state where password verification fails (even if the passwords match on the front end)
  # on a post back of other errors that have been corrected because the password has been salted and doesn't match the confirmation
  it "should pass validation if the record was invalid on an initial save attempt, had the password salted, which causes password confirmation to not match on a re submit" do
    person = Person.create(TCFactory.person_defaults(name: nil, password: 'Testpass123!', password_confirmation: 'Testpass123!'))

    expect(person.errors[:name].size).to be > 0
    person.name = "A good name"
    person.password = "Testpass123!"
    person.password_confirmation = "Testpass123!"

    expect(person).to be_valid
  end

  it "fulfills the password requirements" do
    person = Person.create(TCFactory.person_defaults(password: 'Testpass123!', password_confirmation: 'Testpass123!'))
    expect(person).to be_valid
  end

  it "does not fulfill password requirements" do
    person = Person.create(TCFactory.person_defaults(password: 'Testpass123!', password_confirmation: 'Testpass123!'))
    person.update(password: "testpass123")
    expect(person.valid?).to eq(false)
  end
end

describe Person, "when changing a password on an existing record" do
  before(:each) do
    @person = Person.create(TCFactory.person_defaults)
  end

  it "should be valid (not change the password) if the password fields are blank" do
    @person.update(password: '', password_confirmation: '')
    expect(@person).to be_valid
  end

  it "should not be valid if the password confirmation field is blank" do
    @person.update(password: 'Testpass123!', password_confirmation: '')
    expect(@person).not_to be_valid
    expect(@person.errors[:password_confirmation]).to include(I18n.t("errors.messages.blank"))
  end

  it "should not be valid if the passwords do not match" do
    @person.update(password: 'Testpass123!', password_confirmation: 'passtest')
    expect(@person).not_to be_valid
    expect(@person.errors[:password_confirmation]).to include(I18n.t("models.person.passwords_do_not_match"))
  end

  it "should pass if the passwords match" do
    @person.update(password: 'Testpass123!', password_confirmation: 'Testpass123!', original_password: 'testpass')
    expect(@person).to be_valid
    expect(@person.errors.size).to eq 0
  end
end

describe Person, "authentication" do
  it 'should be able to authenticate a user and return the appropriate record' do
    customer = create(:person, :with_live_album)
    customer.save

    options = { email: customer.email, password: "Testpass123!" }
    person = Person.authenticate(options)

    expect(person).to eq(customer)
  end

  it "should increment failed logins" do
    options = { email: "test@tunecore.com", password: "Testpass123!" }

    person = Person.create!(TCFactory.person_defaults(options))
    options[:password] = "SOMETHING-INCORRECT"
    expect { Person.authenticate(options) }.to change { person.reload.login_attempts }.by(1)
  end

  it "should check for correct passwords - should be false" do
    options = { password: "Testpass123!" }
    person = Person.create!(TCFactory.person_defaults(options))
    expect(person.is_password_correct?("SOMETHING")).to eq(false)
  end

  it "should check for correct passwords - should be false" do
    options = { password: "Testpass123!" }
    person = Person.create!(TCFactory.person_defaults(options))
    expect(person.is_password_correct?("Testpass123!")).to eq(true)
  end

  it "should use a random salt" do
    options = { password: "Testpass123!" }
    allow(Person).to receive(:random_salt).and_return('incredibly_random_salt')
    person = Person.create!(TCFactory.person_defaults(options))

    expect(person.is_password_correct?("Testpass123!")).to eq(true)
    expect(person.salt).to eq 'incredibly_random_salt'
  end

  it "should use 32 bytes of randomness for the salt" do
    allow(SecureRandom).to receive(:hex).with(32).and_return('so_much_randomness')
    expect(Person.random_salt).to eq 'so_much_randomness'
  end

  it 'should not authenticate a user and return the appropriate record when the user is Locked' do
    person = TCFactory.build_person(status: "Locked", password: "Testpass123!", password_confirmation: "Testpass123!")
    expect(Person.authenticate(email: person.email, password: "Testpass123!")).to eq(nil)
  end

  it 'should authenticate a user and return the appropriate record when the user is Suspicious' do
    # customer = people(:tunecore_suspicious_customer)
    person = TCFactory.build_person(status: "Suspicious", password: "Testpass123!", password_confirmation: "Testpass123!")
    expect(Person.authenticate(email: person.email, password: "Testpass123!")).to eq(person)
  end

  it "should set first_login if the user has never logged in" do
    person = TCFactory.create(:person, password: "Testpass123!", password_confirmation: "Testpass123!")
    expect(person.recent_login).to be_blank

    logged_in = Person.authenticate(email: person.email, password: "Testpass123!")

    expect(logged_in.first_login).to eq true
    expect(logged_in.reload.recent_login).not_to be_blank
  end
end

describe Person, "accepting terms and conditions" do
  before(:each) do
    @person = Person.new(TCFactory.person_defaults)
  end

  it "should be able to set terms and conditions using a boolean" do
    @person.accepted_terms_and_conditions = true
    expect(@person.accepted_terms_and_conditions).not_to be_nil
  end

  it "should have ? method for accepted terms and conditions" do
    @person.accepted_terms_and_conditions = true
    expect(@person.accepted_terms_and_conditions?).to eq(true)
  end

  it "should require terms and conditions" do
    @person.accepted_terms_and_conditions = false
    assert ! @person.valid?
    @person.accepted_terms_and_conditions = true
    assert @person.valid?
  end

  it "should set 0 to nil when setting terms and conditions boolean" do
    @person.accepted_terms_and_conditions = "0"
    assert ! @person.valid?
    expect(@person.accepted_terms_and_conditions_on).to eq(nil)
  end

  it "should set nil to nil when setting terms and conditions boolean" do
    @person.accepted_terms_and_conditions = nil
    assert ! @person.valid?
    expect(@person.accepted_terms_and_conditions_on).to eq(nil)
  end

  it "should add error message to accepted_terms_and_conditions if nil" do
    @person.accepted_terms_and_conditions_on = nil
    assert ! @person.valid?
    expect(@person.errors.size).not_to eq 0
  end
end

describe Person, "when changing the status of a person" do
  before(:each) do
    @person = Person.find(43)
  end

  it "should fail if the user is a customer when mark_as_deleted is called" do
    person = create(:person, :with_live_albums)
    create(:invoice, person: person, settled_at: 1.day.ago)

    person.mark_as_deleted("Other")
    expect(person.status).to eq("Active")
    expect(person.active?).to eq(true)
    expect(person.email.last(8)).not_to eq(".DELETED")
    expect(person.deleted).to eq(0)
  end

  it "should set the status to 'Locked' when lock! is called" do
    @person.lock!("Credit Card Fraud", false)
    expect(@person.status).to eq('Locked')
    expect(@person.active?).to eq(false)
  end

  it "should set the status to 'Suspicious' when mark_as_suspicious is called" do
    note = double("person_note")
    allow(note).to receive(:note).and_return("Test Note")
    allow(note).to receive(:note_created_by_id).and_return(0)
    allow(note).to receive(:created_at).and_return(Time.now)
    allow(note).to receive(:note_created_by_name).and_return("System")
    @person.mark_as_suspicious!(note)
    expect(@person.status).to eq('Suspicious')
    expect(@person.active?).to eq(true)
  end

  context "when removing suspicion" do
    let (:person) { create(:person, :with_suspicious_account, :with_blocked_from_distribution_flag ) }

    it "should set the status to 'Active'" do
      person.remove_suspicion!
      expect(person.status).to eq('Active')
      expect(person.active?).to eq(true)
    end

    it "should remove the blocked_from_distribution flag" do
      person.remove_suspicion!
      expect(person.blocked_from_distribution_flag).to be_nil
    end
  end
end

describe Person, "when deleting/undeleting a person" do
  before(:each) do
    @person = Person.find(8)
  end

  it "should set the status to 'Locked', alter the email address, and set the 'deleted' flag when mark_as_deleted is called" do
    @person.mark_as_deleted("Other")
    expect(@person.status).to eq("Locked")
    expect(@person.active?).to eq(false)
    expect(@person.email.last(8)).to eq(".DELETED")
    expect(@person.deleted).to eq(1)
  end

  it "should remove deleted conditions when remove_deletion! is called" do
    @person.remove_deletion!
    expect(@person.status).to eq("Active")
    expect(@person.active?).to eq(true)
    expect(@person.email.last(8)).not_to eq(".DELETED")
    expect(@person.deleted).to eq(0)
  end
end

describe Person, "when checking rights for and admin" do
  it "should return false if the user does not have the right role" do
    person = Person.find(43)  # load tunecore customer
    expect(person.has_role?("Admin")).to eq(false)
  end

  it "should return true if the user has the right role" do
    person = Person.find(42)  # load tunecore admin
    expect(person.has_role?("Admin")).to eq(true)
  end

  it "should return true if the user has the correct controller and action rights" do
    person = Person.find(33) # load super admin
    expect(person.is_permitted_to?("admin/rights", "edit")).to eq(true)
  end

  it "should return false if the user does not have the right controller and action rights" do
    person = Person.find(42) # load admin
    expect(person.is_permitted_to?("admin/rights", "edit")).to eq(false)
  end

  it "should return true if the user has the right permission even if they do not have the admin role" do
    person = Person.find(43)
    expect(person.is_permitted_to?("admin/rights", "edit", false)).to eq(true)
  end

  it "should return false if the user has the right permission but does not have the admin role" do
    person = Person.find(43)
    expect(person.is_permitted_to?("admin/rights", "edit")).to eq(false)
  end

  it "should return has_role result for Reward Admin role" do
    person = create(:person)

    expect(person).to receive(:has_role?).with('Reward Admin').and_return(true)
    expect(person.is_reward_admin?).to be_truthy
  end
end

describe Person, "when retrieving artists for tour date creation" do
  before(:each) do
    @person = Person.find(43)
    allow_any_instance_of(Creative).to receive(:artist).and_return(build(:artist))
  end

  it "should be a list of artist objects" do
    expect(@person.artists_for_tour_date.is_a?(Array)).to eq(true)
    expect(@person.artists_for_tour_date.first.is_a?(Artist)).to eq(true)
  end

  it "should be a list of unique artists" do
    expect(@person.artists_for_tour_date.uniq.size == @person.artists_for_tour_date.size).to eq(true)
  end
end

describe Person, "when changing auto-renewal of a person" do
  before(:each) do
    @person = Person.find(43)
    @person.update(corporate_entity: CorporateEntity.first)
    @note = { note: 'Changing auto-renewal setting', ip_address: 'xx:xx:xx', note_created_by: @person }
    allow(Braintree::PaymentMethod).to receive(:delete).and_return(true)
  end

  it "should create person preferences when auto-renewal is set" do
    expect(@person.person_preference).to be_nil
    @person.enable_auto_renewal(@note)
    @person.reload
    expect(@person.person_preference).not_to be_nil
  end

  it "should set do_not_autorenew to false when auto-renewal is enabled" do
    @person.enable_auto_renewal(@note)
    expect(@person.person_preference.do_not_autorenew).to eq(0)
  end

  it "should set do_not_autorenew to true when auto-renewal is disabled" do
    @person.disable_auto_renewal(@note)
    expect(@person.person_preference.do_not_autorenew).to eq(1)
  end

  it "should archive stored payment methods when disabling auto renewal" do
    cc = TCFactory.create(:stored_credit_card, person_id: @person.id)
    pp = TCFactory.create(:stored_paypal_account, person: @person)

    @person.person_preference.reload.preferred_credit_card_id = cc.id
    @person.person_preference.preferred_paypal_account_id    = pp.id
    @person.person_preference.save(validate: false)

    expect(@person.autorenew?).to eq(true)
    expect(@person.stored_credit_cards.active.count).to be    > 0
    expect(@person.stored_paypal_accounts.active.count).to be > 0

    @person.disable_auto_renewal(@note)
    expect(@person).to be_valid

    expect(@person.stored_credit_cards.active.count).to    eq(0)
    expect(@person.stored_paypal_accounts.active.count).to eq(0)

    expect(@person.person_preference.reload.preferred_paypal_account_id).to be_blank
    expect(@person.person_preference.reload.preferred_credit_card_id).to be_blank
  end

  # for people who were disabled prior to the addition of the archive payment forms code
  it "should archive stored payment methods when enabling auto-renewal" do
    cc = TCFactory.create(:stored_credit_card, person_id: @person.id)
    pp = TCFactory.create(:stored_paypal_account, person: @person)

    @person.person_preference.reload.do_not_autorenew = 1
    @person.person_preference.preferred_credit_card_id    = cc.id
    @person.person_preference.preferred_paypal_account_id = pp.id
    @person.person_preference.save(validate: false)

    expect(@person.person_preference.reload.do_not_autorenew).to eq(1)

    @person.reload
    expect(@person.preferences_set?).to eq(true)

    expect(@person.autorenew?).to eq(false)
    expect(@person.stored_credit_cards.active.count).to be > 0
    expect(@person.stored_paypal_accounts.active.count).to be > 0

    @person.enable_auto_renewal(@note)
    expect(@person).to be_valid

    expect(@person.stored_credit_cards.active.count).to    eq(0)
    expect(@person.stored_paypal_accounts.active.count).to eq(0)
  end

  it "should return validation error when attempting to set auto-renewal without a note" do
    @note[:note] = nil
    @person.enable_auto_renewal(@note)
    expect(@person.errors.size).to be > 0
  end

  it "should not change auto-renewal without a note" do
    @note[:note] = nil
    expect(@person.autorenew?).to eq true
    @person.disable_auto_renewal(@note)
    @person.reload
    expect(@person.autorenew?).to eq true
  end

  it "should create a note" do
    expect(@person.notes.count).to eq(0)
    @person.disable_auto_renewal(@note)
    expect(@person.notes.count).to eq(1)
  end

  context "when failed to save to person preference" do
    before(:each) do
      @person_preference = @person.build_person_preference
      allow(@person.person_preference).to receive(:save).and_raise(ActiveRecord::RecordInvalid.new(@person_preference))
    end

    it "should return errors" do
      @person.enable_auto_renewal(@note)
      expect(@person.errors.size).to be > 0
    end

    it "should not disable auto-renewal" do
      expect(@person.autorenew?).to eq true
      @person.disable_auto_renewal(@note)
      @person.reload
      expect(@person.autorenew?).to eq true
    end
  end

  describe "autorenew?" do
    it "should return true when auto-renewal is enabled" do
      @person.create_person_preference(do_not_autorenew: false)
      expect(@person.autorenew?).to eq(true)
    end

    it "should return false when auto-renewal is disabled" do
      @person.create_person_preference(do_not_autorenew: true)
      expect(@person.autorenew?).to eq(false)
    end
  end
end

describe "converting US users to CA users" do
  before(:each) do
    @person = create(:person, country: "US")
    @person.person_balance.update_attribute(:balance, 0.00)
    mock_braintree_query
  end

  it "should not convert a person with no currency items" do
    test_conversion(true)
  end

  it "should not convert a person with a paid purchase" do
    product = FactoryBot.create(:purchase, person: @person, paid_at: Time.now, related_id: 1, related_type: 'Product')
    test_conversion(false)
  end

  it "should convert a person with only an unpaid purchase" do
    Purchase.create(person: @person, product_id: Product.count + 1, paid_at: nil, related_id: Product.count + 1, related_type: 'Product')
    test_conversion(true)
    expect(@person.accepted_terms_and_conditions_on).to be nil
  end

  it "should not convert a person with a paid invoice" do
    invoice = TCFactory.create(:invoice, person: @person, settled_at: Time.now)
    invoice_settlement = TCFactory.create(:invoice_settlement, invoice: invoice)
    test_conversion(false)
  end

  it "should convert a person with only an unpaid invoice" do
    TCFactory.create(:invoice, person: @person, settled_at: nil)
    test_conversion(true)
    expect(@person.accepted_terms_and_conditions_on).to be nil
  end

  it "should not convert a person with a used targeted_offer" do
    targeted_offer = create(:targeted_offer, status: "Inactive")
    create(:targeted_product, product: Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID), targeted_offer: targeted_offer)
    targeted_offer.update_attribute("status", "Active")
    create(:targeted_person, person: @person, usage_count: 1, targeted_offer: targeted_offer)
    test_conversion(false)
  end

  it "should convert a person with an unused targeted_offer" do
    targeted_offer = create(:targeted_offer, status: "Inactive")
    create(:targeted_product, product: Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID), targeted_offer: targeted_offer)
    targeted_offer.update_attribute("status", "Active")
    create(:targeted_person, person: @person, usage_count: 0, targeted_offer: targeted_offer)
    test_conversion(true)
    expect(@person.accepted_terms_and_conditions_on).to be nil
  end

  it "should not convert a person with a stored bank account" do
    TCFactory.create(:stored_bank_account, person: @person)
    test_conversion(false)
  end

  it "should convert a person with a stored_credit_card" do
    TCFactory.create(:stored_credit_card, person: @person)
    test_conversion(true)
  end

  it "should not convert a person with a braintree_transaction" do
    TCFactory.create(:braintree_transaction, person: @person)
    test_conversion(false)
  end

  it "should not convert a person with a paypal transaction" do
    TCFactory.create(:paypal_transaction, person: @person)
    test_conversion(false)
  end

  it "should not convert a person with a balance transaction" do
    TCFactory.create(:person_transaction, person: @person)
    test_conversion(false)
  end

  it "should not convert a person with a balance != 0" do
    @person.person_balance.update_attribute(:balance, 1.00)
    test_conversion(false)
  end
end

describe "Canadian scenarios" do
  context "CA User that comes through www.tunecore.com" do
    it "should set all CA attributes (as of 9/21/2011)" do
      person = TCFactory.build_person(country: "Canada", postal_code: 'E4Y2E4')
      person = TCFactory.build_person(country: "CA", postal_code: 'E4Y2E4')
      expect(person.currency).to eq("CAD")
      expect(person.domain).to eq("test.tunecore.ca")
      expect(person.person_balance.currency).to eq("CAD")
      expect(person.country_website_id).to eq(2)
    end
  end

  context "CA User that comes through ca.tunecore.com" do
    let(:ca_person) do
      create(:person,
        email: "doug@gwn.ca",
        country: "CA",
        postal_code: 'E4Y2E4',
        password: 'Awesome123!',
        password_confirmation: 'Awesome123!',
        name: 'Ralphy McRalpherson',
        accepted_terms_and_conditions_on: Date.today
      )
    end

    it "sets all CA attributes" do
      expect(ca_person.currency).to eq("CAD")
      expect(ca_person.domain).to eq("test.tunecore.ca")
      expect(ca_person.person_balance.currency).to eq("CAD")
      expect(ca_person.country_website_id).to eq(2)
    end

    context "and the country_website CANNOT be changed" do
      before do
        policy_with_failure_errors = double('policy')
        allow(policy_with_failure_errors)
          .to receive_message_chain(:errors, :full_messages)
          .and_return(['some error'])

        allow(policy_with_failure_errors)
          .to receive_message_chain(:errors, :any?)
          .and_return(true)

        allow(CountryWebsiteUpdater::Policy)
          .to receive(:can_update?)
          .and_return(policy_with_failure_errors)
      end

      it "does not allow a country_website to be changed" do
        us_country = CountryWebsite.find(1)
        ca_country = CountryWebsite.find(2)

        ca_person.update(country_website_id: us_country.id)
        ca_person.reload

        expect(ca_person.country_website_id).to eq ca_country.id
        expect(ca_person.accepted_terms_and_conditions_on).not_to eq nil
      end
    end

    context "and the country_website CAN be changed" do
      before do
        policy_with_no_errors = double('policy')
        allow(policy_with_no_errors)
          .to receive_message_chain(:errors, :full_messages)
          .and_return([])

        allow(policy_with_no_errors)
        .to receive_message_chain(:errors, :any?)
          .and_return(false)

        allow(CountryWebsiteUpdater::Policy)
          .to receive(:can_update?)
          .and_return(policy_with_no_errors)
      end

      it "allows a country_website to be changed" do
        us_country = CountryWebsite.find(1)
        ca_country = CountryWebsite.find(2)

        ca_person.update(country_website_id: us_country.id)
        ca_person.reload

        expect(ca_person.country_website_id).to eq us_country.id
        expect(ca_person.accepted_terms_and_conditions_on).to eq nil
      end
    end
  end
end

describe Person, "selected_splits?" do
  before(:each) do
    @person = TCFactory.build_person
  end

  it "should return true when splits are selected" do
    @composer = TCFactory.create(:composer, person: @person)
    @splits = TCFactory.create(:publishing_split, composer: @composer)
    expect(@person.selected_splits?).to eq true
  end

  it "should return false when no splits are selected" do
    @composer = TCFactory.create(:composer, person: @person)
    expect(@person.selected_splits?).to eq false
  end

  it "should return false when no composer" do
    expect(@person.selected_splits?).to eq false
  end
end

describe Person do
  before(:each) do
    @person = TCFactory.create(:person)
  end

  describe "domain" do
    it "should use current rails_env and country website for domain" do
      @person.country_website_id = CountryWebsite::UNITED_STATES
      expect(@person.domain).to eq("test.tunecore.com")

      @person.country_website = CountryWebsite.find(CountryWebsite::CANADA)
      expect(@person.domain).to eq("test.tunecore.ca")
    end
  end

  describe "migrate_to_bigbox" do
    before(:each) do
      @album  = mock_model(Album)
      @bigbox = mock_model(Song)
      @legacy = mock_model(Song)

      allow(@bigbox).to receive(:s3_asset).and_return(true)
      allow(@bigbox).to receive(:s3_orig_asset).and_return(nil)

      allow(@legacy).to receive(:s3_asset).and_return(nil)
      allow(@legacy).to receive(:s3_orig_asset).and_return(true)

      allow(@album).to receive(:songs).and_return([@bigbox, @legacy])
      allow(@person).to receive(:albums).and_return([@album])
      allow(@person).to receive(:migrated_to_bigbox?).and_return(false)
    end

    it "should send legacy songs to bigbox" do
      expect(@legacy).to receive(:send_to_bigbox)
      @person.migrate_to_bigbox
    end

    it "should not send bigbox asset songs to bigbox" do
      expect(@legacy).to receive(:send_to_bigbox)
      expect(@bigbox).not_to receive(:send_to_bigbox)
      @person.migrate_to_bigbox
    end

    it "should return the number of songs sent to bigbox" do
      expect(@legacy).to receive(:send_to_bigbox)
      expect(@bigbox).not_to receive(:send_to_bigbox)
      expect(@person.migrate_to_bigbox).to eq(1)
    end
  end
end

describe Person, "custom email uniquess check" do
  before(:each) do
    @person = TCFactory.create(:person)
    expect(@person.valid?).to eq(true)
  end

  it "should properly set validation flag" do
    expect(@person.email_uniqueness_validated?).to eq(true)

    person2 = TCFactory.build(:person, email: @person.email)
    expect(person2.invalid?).to eq(true)
    expect(person2.email_uniqueness_validated?).to eq(false)

    person2.email = "mytestemail@testing.com"
    expect(person2.valid?).to eq(true)
    expect(person2.email_uniqueness_validated?).to eq(true)

    person2.save!
  end
end

describe Person do
  describe "::validate_user" do
    before(:each) do
      @person = FactoryBot.create(:person, email: "customer@example.com", is_verified: true)
    end

    it "should return true if the person found by the id and the email matches and the person is_verified" do
      expect(Person.validate_user(@person.id, @person.email)).to eq(true)
    end

    it "should return false if the person found by the id and is not verified" do
      @person.update_attribute(:is_verified, false)
      expect(Person.validate_user(@person.id, @person.email)).to eq(false)
    end

    it "should raise ActiveRecord::RecordNotFound if the person found by the id does not match the email of the user" do
      expect { Person.validate_user(@person.id, "other email") }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  describe "#analytic_by_metric" do
    before(:each) do
      @person = FactoryBot.create(:person)
      @person_analytic = FactoryBot.create(:person_analytic, person: @person, metric_name: PersonAnalytic::METRIC_NAMES.first)
    end

    it "returns the person_analytic record that matches the person and metric_name" do
      expect(@person.analytic_by_metric(PersonAnalytic::METRIC_NAMES.first)).to eq(@person_analytic)
    end

    it "returns nil if the record does not exist" do
      expect(@person.analytic_by_metric("random_metric")).to eq(nil)
    end
  end

  describe "#create_analytic_by_metric_name_and_value" do
    it "calls person_analytics.create with the passed in metric_name and value" do
      person = build_stubbed(:person)
      metric_name = "METRIC_NAME"
      metric_value = 0
      mock_pa = double(PersonAnalytic)
      expect(person).to receive(:person_analytics).and_return(mock_pa)
      expect(mock_pa).to receive(:create).with(metric_name: metric_name, metric_value: metric_value)

      person.create_analytic_by_metric_name_and_value(metric_name, metric_value)
    end
  end

  describe "#mark_as_verified" do
    it "should verify the person" do
      person = build(:person, is_verified: false)
      expect(person.is_verified).to be 0

      person.mark_as_verified
      expect(person.is_verified).to be 1
    end
  end

  describe "#label_name" do
    context "when the label_name attribute is present" do
      it "returns the label_name attribute" do
        person = build_stubbed(:person)

        allow(person)
          .to receive(:label)
          .and_return(
            double('label', { name: 'bogus'})
          )
        allow(person)
          .to receive(:attributes)
          .and_return(
            { 'label_name' => 'cool'}
          )

        expect(person.label_name).to eq 'cool'
      end
    end

    context "when only the label is present" do
      it "returns the label" do
        person = build_stubbed(:person)
        allow(person)
          .to receive(:label)
          .and_return(
            double('label', { name: 'awesome'})
          )

        expect(person.label_name).to eq 'awesome'
      end
    end

    context "when neither the label_name nor label is present" do
      it "returns nil" do
        person = build_stubbed(:person)

        expect(person.label_name).to be nil
      end
    end
  end

  describe "#first_release?" do
    context "when the person does not have any albums" do
      it "returns false" do
        person = create(:person)
        expect(person.first_release?).to be false
      end
    end

    context "when the person has more than one finalized album" do
      it "returns false" do
        person = create(:person, :with_live_albums)
        expect(person.first_release?).to be false
      end
    end

    context "when the person has one finalized album" do
      it "returns true" do
        person = create(:person, :with_live_album)
        expect(person.first_release?).to be true
      end
    end
  end

  describe "#distributed_albums?" do
    context "when the person has taken down album" do
      it "returns false" do
        person = create(:person, :with_taken_down_album)
        expect(person.distributed_albums?).to be false
      end
    end

    context "when the person with delivered distributions" do
      it "returns true" do
        person = create(:person, :with_delivered_distributions)
        expect(person.distributed_albums?).to be true
      end
    end
  end
end

describe Person do
  describe "#create_logged_in_event" do
    it "creates a LoginEvent" do
      @person = create(:person)

      expect do
        @person.create_logged_in_event("127.0.0.1", "fake_user_agent")
      end.to change(LoginEvent, :count).by(1)
    end

    it "should skip creating LoginEvent when marked for skipping" do
      person = create(:person)
      ENV['SKIP_LOGIN_EVENT_LOGGING_FOR_USERS'] = person.email

      expect do
        person.create_logged_in_event("127.0.0.1", "fake_user_agent")
      end.to_not change(LoginEvent, :count)

      ENV.delete('SKIP_LOGIN_EVENT_LOGGING_FOR_USERS')
    end
  end

  describe "#record_login" do
    before do
      travel_to Time.local(2020)
    end

    after do
      travel_back
    end
    it "should register ip and time" do
      person = create(:person)
      ip_address = '12.8.6.1'
      time = Time.current

      Timecop.freeze(time) do
        person.record_login(ip_address)

        expect(person.last_logged_in_ip).to eq(ip_address)
        expect(person.recent_login.utc.to_s).to eq(time.utc.to_s)
      end
    end
  end

  describe "#assign_country_website_on_create" do
    context "user selects country as United Kingdom" do
      it "sets country_website_id to 3 if the feature is active" do
        @person = FactoryBot.create(:person, country: "GB")
        expect(@person.country_website_id).to eq(3)
      end
    end

    context "user selects country as Australia" do
      it "sets country_website_id to 4 if the feature is active" do
        @person = FactoryBot.create(:person, country: "AU")
        expect(@person.country_website_id).to eq(4)
      end
    end

    context "user selects country as New Zealand" do
      it "sets country_website_id to 4 if the feature is active" do
        @person = FactoryBot.create(:person, country: "NZ")
        expect(@person.country_website_id).to eq(4)
      end
    end

    context "user selects country as Germany with feature active" do
      before(:each) do
        @person = FactoryBot.create(:person, country: "DE")
      end

      it "sets country_website_id to 5 if the feature is active" do
        expect(@person.country_website_id).to eq(5)
      end

      it "sets the currency to EUR" do
        expect(@person.currency).to eq "EUR"
      end

      it "sets the domain to de" do
        expect(@person.domain).to eq "test.tunecore.de"
      end

      it "sets the person_balance currency to EUR" do
        expect(@person.person_balance.currency).to eq "EUR"
      end
    end
  end

  describe "validates_presence_of apple_id" do
    before(:each) do
      @person = FactoryBot.build(:person)
    end

    context "apple_role is not blank" do
      before(:each) do
        @person.apple_role = "Artist Manager"
      end

      it "is valid with an apple_id" do
        @person.apple_id = "valid@email.com"
        expect(@person.valid?).to be true
      end

      it "is invalid without an apple_id" do
        expect(@person.invalid?).to be true
      end
    end

    it "is valid without both an apple_id or apple_role" do
      expect(@person.valid?).to be true
    end
  end

  describe "validates_presence_of apple_role" do
    before(:each) do
      @person = FactoryBot.build(:person)
    end

    context "apple_id is present" do
      before(:each) do
        @person.apple_id = "valid@email.com"
      end

      it "is valid with an apple_role" do
        @person.apple_role = "Artist Manager"
        expect(@person.valid?).to be true
      end

      it "is invalid without an apple_role" do
        expect(@person.invalid?).to be true
      end
    end
  end

  describe "#country_not_japan" do
    it "is invalid if the person's country is japan" do
      @person = FactoryBot.build(:person, country: "JP")
      expect(@person.valid?).to be false
    end

    it "is valid if the person's country is not" do
      @person = FactoryBot.build(:person, country: "US")
      expect(@person.valid?).to be true
    end
  end

  describe "#referred_by" do
    let(:person) { FactoryBot.build(:person) }

    it "true if referral_type on person is raf" do
      person.referral_type = "raf"
      referral_string = "raf"
      expect(person.referred_by(referral_string)).to eq true
    end
    it "false if referral_type on person is not raf" do
      person.referral_type = "something_else"
      referral_string = "raf"
      expect(person.referred_by(referral_string)).to eq false
    end

    it "true if referral_datum exists with key == to ref and value == raf" do
      person.referral_type = nil
      referral_string = "applemusicartist"
      person.referral_data.build(key: "ref", value: referral_string)
      expect(person.referred_by(referral_string)).to eq true
    end

    it "true if referral_datum exists with key == to ref and value == raf (with referral_type set)" do
      person.referral_type = "raf"
      referral_string = "applemusicartist"
      person.referral_data.build(key: "ref", value: referral_string)
      expect(person.referred_by(referral_string)).to eq true
    end

    it "false if no referral_datum found with key == to ref and value == raf" do
      person.referral_type = nil
      referral_string = "applemusicartist"
      person.referral_data.build(key: "ref", value: "something_else")
      expect(person.referred_by(referral_string)).to eq false
    end
  end

  describe "#join_token" do
    let(:person) { build_stubbed(:person) }
    it "returns latest referral_datum with key == jt" do
      person.referral_data.build(key: "jt", value: "JoinNow")
      expect(person.join_token).to eq "JoinNow"
    end

    it "returns nil if no referral_datum with key == jt" do
      expect(person.join_token).to eq nil
    end
  end

  describe "#has_join_token?" do
    let(:person) { build_stubbed(:person) }
    it "returns true referral_datum with key == jt" do
      person.referral_data.build(key: "jt", value: "JoinNow")
      expect(person.has_join_token?).to eq true
    end

    it "returns false if no referral_datum with key == jt" do
      expect(person.has_join_token?).to eq false
    end
  end

  describe "#has_tc_social?" do
    it "is true if person has oauth tokens for tc_social client app" do
      person = FactoryBot.create(:person, :with_tc_social)

      expect(person.has_tc_social?).to eq true
    end

    it "is false if person doesnt have oauth tokens for tc_social client app" do
      person = FactoryBot.create(:person)

      expect(person.has_tc_social?).to eq false
    end

    it "is true if person has active subscription to Social" do
      person = create(:person, :with_active_social_subscription_status)
      expect(person.has_tc_social?).to eq true
    end
  end

  describe "#tc_social_token" do
    it "returns token value if exists" do
      person = FactoryBot.create(:person, :with_tc_social)
      token = OauthToken.where(user_id: person.id, type: "Oauth2Token").first.token

      expect(person.tc_social_token).to eq token
    end

    it "returns nil if diesnt exists" do
      person = FactoryBot.create(:person)
      expect(person.tc_social_token).to eq nil
    end
  end

  describe "#pop_album_for_review" do
    before { allow_any_instance_of(ReviewAudit).to receive(:send_sns_notification) }

    before(:each) do
      @person = FactoryBot.create(:person)
      @album = FactoryBot.create(:album, :with_uploaded_songs, :with_petri_bundle, number_of_songs: 2, person: @person)
      @album.petri_bundle.distributions << FactoryBot.create(:distribution, petri_bundle_id: @album.petri_bundle.id)
      allow_any_instance_of(Distribution).to receive(:start)
    end

    context "unfinalized album" do
      it "should not return an album finalized" do
        expect(@person.pop_album_for_review).to be_nil
      end

      it "should not return an album after one was approved" do
        @album.finalized_at = Time.now
        @album.mark_as_needs_review
        @album.save
        @person.pop_album_for_review.start_review(person_id: @person.id)
        review_audit = @album.review_audits.last
        review_audit.event = "APPROVED"
        review_audit.save
        review_audit.update_album_review_state
        @album_2 = FactoryBot.create(:album, :with_uploaded_songs, number_of_songs: 2, person: @person)
        @album_2.mark_as_needs_review
        expect(@person.pop_album_for_review).to be_nil
      end
    end

    context "finalized album" do
      before(:each) do
        @album.finalized_at = Time.now
        @album.mark_as_needs_review
        @album.save
      end

      it "returns albums for review" do
        album_1 = @person.pop_album_for_review.start_review(person_id: @person.id)
        expect(album_1).to eq(@album)

        review_audit = album_1.review_audits.last
        review_audit.event = 'APPROVED'
        review_audit.save
        review_audit.update_album_review_state

        album_2 = FactoryBot.build(:album, :with_uploaded_songs,
                                    number_of_songs: 2,
                                    person: @person,
                                    finalized_at: Time.now,
                                    legal_review_state: "NEEDS REVIEW",
                                    takedown_at: nil)

        album_2.mark_as_needs_review

        expect(@person.pop_album_for_review).to eq(album_2)
      end

      it "does not return an album for review that has been taken down" do
        @person.pop_album_for_review.start_review(person_id: @person.id)
        review_audit = @album.review_audits.last
        review_audit.event = 'APPROVED'
        review_audit.save
        review_audit.update_album_review_state

        album_2 = FactoryBot.build(:album, :with_uploaded_songs,
                                    number_of_songs: 2,
                                    person: @person,
                                    finalized_at: Time.now,
                                    legal_review_state: "NEEDS REVIEW",
                                    takedown_at: Time.now)
        album_2.mark_as_needs_review

        expect(@person.pop_album_for_review).to be_nil
      end
    end
  end

  describe '#albums_left_to_review' do
    before { allow_any_instance_of(ReviewAudit).to receive(:send_sns_notification) }

    before(:each) do
      @person = FactoryBot.create(:person)
      @album = FactoryBot.create(:album, :with_uploaded_songs, :with_petri_bundle, number_of_songs: 2, person: @person)
      @album.petri_bundle.distributions << FactoryBot.create(:distribution, petri_bundle_id: @album.petri_bundle.id)
      allow_any_instance_of(Distribution).to receive(:start)
    end

    context "unfinalized album" do
      it "should return a count of 0" do
        expect(@person.albums_left_to_review).to eq(0)
      end

      it "should return a count of 0 after an album was approved" do
        @album.finalized_at = Time.now
        @album.mark_as_needs_review
        @album.save
        @person.pop_album_for_review.start_review(person_id: @person.id)
        review_audit = @album.review_audits.last
        review_audit.event = "APPROVED"
        review_audit.save
        review_audit.update_album_review_state
        @album_2 = FactoryBot.create(:album, :with_uploaded_songs, number_of_songs: 2, person: @person)
        expect(@person.albums_left_to_review).to eq(0)
      end
    end

    context "finalized album" do
      before(:each) do
        @album.finalized_at = Time.now
        @album.mark_as_needs_review
        @album.save
      end

      it "should return a count of 1" do
        expect(@person.albums_left_to_review).to eq(1)
      end

      it "should return a count of 1 after a review has been approved" do
        @person.pop_album_for_review.start_review(person_id: @person.id)
        review_audit = @album.review_audits.last
        review_audit.event = "APPROVED"
        review_audit.save
        review_audit.update_album_review_state
        @album_2 = FactoryBot.create(:album, :with_uploaded_songs, number_of_songs: 2, person: @person)
        @album_2.finalized_at = Time.now
        @album_2.mark_as_needs_review
        @album_2.save
        expect(@person.albums_left_to_review).to eq(1)
      end

      it "should return a count of 2" do
        @album_2 = FactoryBot.create(:album, :with_uploaded_songs, number_of_songs: 2, person: @person)
        @album_2.finalized_at = Time.now
        @album_2.mark_as_needs_review
        @album_2.save
        expect(@person.albums_left_to_review).to eq(2)
      end
    end
  end

  describe '#total_albums_left_to_review_for_agent' do
    before { allow_any_instance_of(ReviewAudit).to receive(:send_sns_notification) }

    before(:each) do
      @person = FactoryBot.create(:person)
      @album = FactoryBot.create(:album, :with_uploaded_songs, :with_petri_bundle, number_of_songs: 2, person: @person)
      @album.petri_bundle.distributions << FactoryBot.create(:distribution, petri_bundle_id: @album.petri_bundle.id)
      allow_any_instance_of(Distribution).to receive(:start)
    end

    context "unfinalized album" do
      it "should return a count of 0" do
        expect(@person.total_albums_left_to_review_for_agent).to eq(0)
      end

      it "should return a count of 0 after an album was approved" do
        @album.finalized_at = Time.now
        @album.mark_as_needs_review
        @album.save
        @person.pop_album_for_review.start_review(person_id: @person.id)
        review_audit = @album.review_audits.last
        review_audit.event = "APPROVED"
        review_audit.save
        review_audit.update_album_review_state
        @album_2 = FactoryBot.create(:album, :with_uploaded_songs, number_of_songs: 2, person: @person)
        @album_2.mark_as_needs_review
        expect(@person.total_albums_left_to_review_for_agent).to eq(0)
      end
    end

    context "finalized album" do
      before(:each) do
        @album.finalized_at = Time.now
        @album.mark_as_needs_review
        @album.save
      end

      it "should return a count of 1" do
        expect(@person.total_albums_left_to_review_for_agent).to eq(1)
      end

      it "should return a count of 1 after a review has been approved" do
        @person.pop_album_for_review.start_review(person_id: @person.id)
        review_audit = @album.review_audits.last
        review_audit.event = "APPROVED"
        review_audit.save
        review_audit.update_album_review_state
        @album_2 = FactoryBot.create(:album, :with_uploaded_songs, number_of_songs: 2, person: @person)
        @album_2.finalized_at = Time.now
        @album_2.mark_as_needs_review
        @album_2.save
        expect(@person.total_albums_left_to_review_for_agent).to eq(1)
      end

      it "should return a count of 2" do
        @album_2 = FactoryBot.create(:album, :with_uploaded_songs, number_of_songs: 2, person: @person)
        @album_2.finalized_at = Time.now
        @album_2.mark_as_needs_review
        @album_2.save
        expect(@person.total_albums_left_to_review_for_agent).to eq(2)
      end
    end
  end

  describe "#has_distribution_credits_available?" do
    it "returns true when a person has album credits" do
      person = FactoryBot.create(:person, :with_distribution_credits, inventory_type: "Album")
      expect(person.has_distribution_credits_available?).to be_truthy
    end

    it "returns true when a person has song credits" do
      person = FactoryBot.create(:person, :with_distribution_credits, inventory_type: "Single")
      expect(person.has_distribution_credits_available?).to be_truthy
    end

    it "returns true when a person has ringtone credits" do
      person = FactoryBot.create(:person, :with_distribution_credits, inventory_type: "Ringtone")
      expect(person.has_distribution_credits_available?).to be_truthy
    end

    it "returns false when a person has no credits" do
      person = FactoryBot.create(:person)
      expect(person.has_distribution_credits_available?).to be_falsey
    end
  end

  describe "#composer_paid_for_on" do
    let(:person) { build(:person) }

    context "when a person has paid for publishing admin service" do
      it "returns a timestamp of when it was paid for" do
        paid_at_time = Time.now
        pub_admin_product = create(:product, display_name: "pub_admin")
        create(:purchase, person: person, product_id: pub_admin_product.id, paid_at: paid_at_time)
        # conversion to string necessary to get equality check to return true
        expect(person.paid_for_composer_on.to_s).to eq paid_at_time.to_s
      end
    end

    context "when a person has NOT paid for publishing admin service" do
      it "returns nil" do
        expect(person.paid_for_composer_on).to eq nil
      end
    end
  end

  describe "#paid_for_composer?" do
    context "when the rights app feature is disabled" do
      it "returns false if the person doesn't have any active composers" do
        composer = build(:composer)
        person   = composer.person
        expect(person.paid_for_composer?).to be_falsey
      end

      it "returns true if the person has an active composer and doesn't have an account" do
        composer = create(:composer, account: nil)
        person   = composer.person
        pub_admin_purchase = create(:purchase,
                                    related_type: "Composer",
                                    related_id:   composer.id,
                                    person_id:    person.id,
                                    product_id:   Product.find_products_for_country(composer.person.country_domain, :songwriter_service).first)

        composer.related_purchases << pub_admin_purchase
        expect(person.paid_for_composer?).to be_truthy
        expect(person.paid_for_composer?).to be_truthy
      end
    end

    context "when the rights app feature is enabled" do
      before(:each) do
        allow(FeatureFlipper).to receive(:show_feature?) { true }
      end

      it "returns false if the person doesn't have any active composers" do
        composer = build(:composer)
        person   = composer.person
        expect(person.paid_for_composer?).to be_falsey
      end

      it "returns true if the person has an active composer" do
        person         = create(:person)
        person.account = create(:account)
        create(:composer, :with_pub_admin_purchase, person: person, account: person.account)
        expect(person.paid_for_composer?).to be_truthy
      end
    end
  end

  describe "#paid_for_publishing_composer?" do
    context "when the rights app feature is disabled" do
      it "returns false if the person doesn't have any active composers" do
        publishing_composer = build(:publishing_composer)
        person   = publishing_composer.person
        expect(person.paid_for_publishing_composer?).to be_falsey
      end

      it "returns true if the person has an active publishing_composer and doesn't have an account" do
        publishing_composer = create(:publishing_composer, account: nil)
        person   = publishing_composer.person
        pub_admin_purchase = create(:purchase,
                                    related_type: "PublishingComposer",
                                    related_id:   publishing_composer.id,
                                    person_id:    person.id,
                                    product_id:   Product.find_products_for_country(publishing_composer.person.country_domain, :songwriter_service).first)

        publishing_composer.related_purchases << pub_admin_purchase
        expect(person.paid_for_publishing_composer?).to be_truthy
        expect(person.paid_for_publishing_composer?).to be_truthy
      end
    end

    context "when the rights app feature is enabled" do
      before(:each) do
        allow(FeatureFlipper).to receive(:show_feature?) { true }
      end

      it "returns false if the person doesn't have any active publishing_composers" do
        publishing_composer = build(:publishing_composer)
        person   = publishing_composer.person
        expect(person.paid_for_publishing_composer?).to be_falsey
      end

      it "returns true if the person has an active publishing_composer" do
        person         = create(:person)
        person.account = create(:account)
        create(:publishing_composer, :with_pub_admin_purchase, person: person, account: person.account)
        expect(person.paid_for_publishing_composer?).to be_truthy
      end
    end
  end

  describe "country helper methods" do
    context "with a US person" do
      let (:person) { build(:person, country_website_id: 1) }

      it "should return true for is_us?" do
        expect(person.is_us?).to eq true
      end

      it "should return false for other countries" do
        expect(person.is_ca?).to eq false
        expect(person.is_uk?).to eq false
        expect(person.is_de?).to eq false
      end
    end

    context "with a CA person" do
      let (:person) { build(:person, country_website_id: 2) }

      it "should return true for is_ca?" do
        expect(person.is_ca?).to eq true
      end

      it "should return false for other countries" do
        expect(person.is_us?).to eq false
        expect(person.is_uk?).to eq false
        expect(person.is_de?).to eq false
      end
    end

    context "with a UK person" do
      let (:person) { build(:person, country_website_id: 3) }

      it "should return true for is_uk?" do
        expect(person.is_uk?).to eq true
      end

      it "should return false for other countries" do
        expect(person.is_ca?).to eq false
        expect(person.is_fr?).to eq false
        expect(person.is_de?).to eq false
      end
    end
  end

  describe "#releases_eligible_for_yt_artwork" do
    let(:person) { create(:person, :with_live_album) }
    let(:album)  { person.albums.first }

    context "when a person has eligible releases for youtube artwork" do
      it "returns eligible releases" do
        album.salepoints << create(:salepoint, salepointable: album)
        expect(person.releases_eligible_for_youtube_art_tracks_store?).to eq true
      end
    end

    context "when a person does NOT have eligible releases for youtube artwork" do
      it "returns an empty array" do
        youtube_art_tracks_store_id = Store.find_by(short_name: "Google").id
        album.salepoints << create(:salepoint, store_id: youtube_art_tracks_store_id, salepointable: album)
        expect(person.releases_eligible_for_youtube_art_tracks_store?).to eq false
      end
    end
  end

  describe "#can_withdraw" do
    let(:person) { create(:person) }

    it "is true when they have balance" do
      person.person_balance.balance = 50.00
      expect(person.can_withdraw?).to eq true
    end

    it "is false when they do not have balance" do
      person.person_balance.balance = 0.00
      expect(person.can_withdraw?).to eq false
    end
  end

  describe "#legacy_payout_enabled?" do
    let(:person) { create(:person) }
    let(:payout_provider) { create(:payout_provider, person: person) }

    it "is true when they have balance" do
      payout_provider.update(tunecore_status: PayoutProvider::ACTIVE)
      expect(person.legacy_payout_enabled?).to eq false
    end

    it "is false when they do not have balance" do
      payout_provider.update(tunecore_status: PayoutProvider::DISABLED)
      expect(person.legacy_payout_enabled?).to eq true
    end
  end

  context "track monetizations" do
    describe "#potentially_monetizable_songs" do
      before(:each) do
        @subscriber = create(:person)
        @album1     = create(
          :album, :with_uploaded_songs, :paid,
          person: @subscriber,
          legal_review_state: "APPROVED"
        )
        @potential_mons = @subscriber.potentially_monetizable_songs
      end

      it "returns all songs from a user's live albums" do
        expect(@potential_mons.map(&:id)).to eq(@album1.songs.map(&:id))
      end

      it "does not return songs that have track_monetizations already" do
        TrackMonetization.create(
          person_id: @subscriber.id,
          song_id:   @album1.songs.first.id,
          store_id:  83
        )
        potential_mons = @subscriber.potentially_monetizable_songs
        expect(potential_mons).not_to include(@album1.songs.first)
      end

      it "does not return songs that have not been paid for and finalized" do
        album2 = create(:album, :with_uploaded_songs, person: @subscriber)
        potential_mons = @subscriber.potentially_monetizable_songs
        expect(potential_mons).not_to include(album2.songs)
      end
    end
  end

  describe ".new_dist_customers" do
    it "returns number of customers with specific person analytic values" do
      person = create(:person)
      invoice = create(:invoice, person: person, settled_at: Date.today)
      create(:person_analytic, person: person, metric_name: "first_distribution_invoice_id", metric_value: invoice.id)
      create(:person_analytic, person: person, metric_name: "first_invoice_id", metric_value: invoice.id)
      create(:person_analytic, person: person, metric_name: "publishing_invoice_id", metric_value: invoice.id)

      result = Person.new_dist_customers(Date.today)

      expect(result.first["country_website_id"]).to eq person.country_website_id
      expect(result.first["purchase_type"]).to eq "first_dist_and_new"
      expect(result.first["num_customers"]).to eq 1
    end
  end

  describe ".pub_customers" do
    it "returns number of customers with publishing person analytics" do
      person = create(:person)
      invoice = create(:invoice, person: person, settled_at: Date.today)
      create(:person_analytic, person: person, metric_name: "first_distribution_invoice_id", metric_value: invoice.id)
      create(:person_analytic, person: person, metric_name: "publishing_invoice_id", metric_value: invoice.id)

      result = Person.pub_customers(Date.today)

      expect(result.first["country_website_id"]).to eq person.country_website_id
      expect(result.first["purchase_type"]).to eq "pub_and_new"
      expect(result.first["num_customers"]).to eq 1
    end
  end

  describe "#imported_from_stem?" do
    let(:person) { build_stubbed(:person) }

    subject { person.imported_from_stem? }

    context "user referral is stem" do
      before { person.referral = 'stem' }

      it { should be_truthy }
    end

    context "user referral is not stem" do
      before { person.referral = nil }

      it { should be_falsey }
    end
  end

  describe "#no_payment_on_file?" do
    let(:person) { create(:person) }

    subject { person.no_payment_on_file? }

    context "user has saved credit card" do
      before { person.stored_credit_cards << StoredCreditCard.new }

      it { should be_falsey }
    end

    context "user has saved paypal" do
      before { person.stored_paypal_accounts << StoredPaypalAccount.new }

      it { should be_falsey }
    end

    context "user has no credit card and paypal" do
      it { should be_truthy }
    end
  end

  describe "#stem_user_without_payment?" do
    let(:person) { build_stubbed(:person) }

    subject { person.stem_user_without_payment? }

    context "user imported from stem" do
      before { allow(person).to receive(:imported_from_stem?).and_return true }

      context "user has no payment on file" do
        before { allow(person).to receive(:no_payment_on_file?).and_return true }

        it { should be_truthy }
      end

      context "user has payment on file" do
        before { allow(person).to receive(:no_payment_on_file?).and_return false }

        it { should be_falsey }
      end
    end

    context "user not imported from stem" do
      before { allow(person).to receive(:imported_from_stem?).and_return false }

      context "user has no payment on file" do
        before { allow(person).to receive(:no_payment_on_file?).and_return true }

        it { should be_falsey }
      end

      context "user has payment on file" do
        before { allow(person).to receive(:no_payment_on_file?).and_return false }

        it { should be_falsey }
      end
    end
  end

  describe "#blocked_for_stem?" do
    let(:person) { build_stubbed(:person) }

    subject { person.blocked_for_stem? }

    context "user imported from stem" do
      before { allow(person).to receive(:stem_user_without_payment?).and_return true }

      context "user does not have disable_stem_modal feature" do
        it { should be_truthy }
      end

      context "user has disable_stem_modal feature" do
        before { allow(person).to receive(:feature_enabled?).with(:disable_stem_modal).and_return true }

        it { should be_falsey }
      end
    end

    context "user not imported from stem" do
      before { allow(person).to receive(:stem_user_without_payment?).and_return false }

      context "user does not have disable_stem_modal feature" do
        it { should be_falsey }
      end

      context "user has disable_stem_modal feature" do
        before { allow(person).to receive(:feature_enabled?).with(:disable_stem_modal).and_return true }

        it { should be_falsey }
      end
    end
  end

  describe "#has_store_salepoints?" do
    context "when the user has salepoints from that store" do
      it "should return true" do
        person = create(:person, :with_album_in_cart)
        store = Store.find_by(short_name: 'YTMusic')
        album = person.albums.first
        album.salepoints << create(:salepoint, salepointable: album, store: store)

        expect(person.has_store_salepoints?('YTMusic')).to eq(true)
      end
    end

    context "when the user doesn't have salepoints from that store" do
      it "should return false" do
        person = create(:person, :with_album_in_cart)
        store = Store.find_by(short_name: 'YTMusic')

        expect(person.has_store_salepoints?('YTMusic')).to eq(false)
      end
    end
  end

  def active_album(store_short_name, album_traits)
    album = create(:album, *album_traits, person: person)
    store = Store.find_by(short_name: store_short_name)
    create(:petri_bundle, album: album)
    create(:salepoint, :payment_applied, :finalized, salepointable: album, store: store)
    album
  end

  describe "#active_store_salepoints?" do
    let!(:person) { create(:person) }
    subject { person.active_store_salepoints?(Store::SPOTIFY) }
    context "when user has an album in that store" do
      context "when album is paid" do
        let!(:album) { active_album(Store::SPOTIFY, [:paid, :approved]) }
        context "when album is taken down" do
          before(:each) do
            album.takedown!
          end
          it "returns false" do
            expect(subject).to be(false)
          end
        end
        context "when salepoint is taken down" do
          before(:each) do
            salepoint = album.salepoints.first
            salepoint.takedown!
          end
          it "returns false" do
            expect(subject).to be(false)
          end
        end
      end
      context "when album is not paid" do
        let!(:album) { active_album(Store::SPOTIFY, []) }
        it "returns false" do
          expect(subject).to be(false)
        end
      end
      context "when the release is active" do
        context "when album is approved" do
          let!(:album) { active_album(Store::SPOTIFY, [:paid, :approved]) }
          it "returns true" do
            expect(subject).to be(true)
          end
        end
        context "when album is pending review" do
          let!(:album) { active_album(Store::SPOTIFY, [:paid, :pending_approval]) }
          it "returns true" do
            expect(subject).to be(true)
          end
        end
      end
    end
    context "when user doesn't have an album in that store" do
      let!(:album) { active_album("Google".freeze, [:paid, :approved]) }
      it "returns false" do
        expect(subject).to be(false)
      end
    end
  end

  describe "#paid_unfinalized_store_salepoints?" do
    let!(:person) { create(:person) }
    subject { person.paid_unfinalized_store_salepoints?(Store::SPOTIFY) }
    context "when user has an album in that store" do
      context "when album is paid" do
        let!(:album) { active_album(Store::SPOTIFY, [:paid, :approved]) }
        context "when album is taken down" do
          before(:each) do
            album.takedown!
          end
          it "returns false" do
            expect(subject).to be(false)
          end
        end
        context "when salepoint is taken down" do
          before(:each) do
            salepoint = album.salepoints.first
            salepoint.takedown!
          end
          it "returns false" do
            expect(subject).to be(false)
          end
        end
      end
      context "when album is not paid" do
        let!(:album) { active_album(Store::SPOTIFY, []) }
        it "returns false" do
          expect(subject).to be(false)
        end
      end
      context "when the release is active" do
        let!(:album) { active_album(Store::SPOTIFY, [:paid]) }
        context "there are inventory usages for the store salepoint" do
          before(:each) do
            purch = create(:purchase, product: Product.find(36), person: person, related: album)
            Product.process_purchase(person, [purch])
          end

          it "returns true" do
            expect(subject).to be(true)
          end

        end

        context "there are no inventory usages for the salepoints" do

          it "returns false" do
            expect(subject).to be(false)
          end

        end

      end
    end
    context "when user doesn't have an album in that store" do
      let!(:album) { active_album("Google".freeze, [:paid, :approved]) }
      context "there are inventory usages for other store salepoints" do
        before(:each) do
          purch = create(:purchase, product: Product.find(36), person: person, related: album)
          Product.process_purchase(person, [purch])
        end

        it "returns true" do
          expect(subject).to be(false)
        end
      end

      context "there are no inventory usages" do
        it "returns false" do
          expect(subject).to be(false)
        end
      end
    end

  end

  describe "#has_store_distributions?" do
    context "when the user has distributions for that store (1 distribution)" do
      it "should return true" do
        person = create(:person, :with_delivered_distributions)
        expect(person.has_store_distributions?('BelieveLiv')).to eq(true)
      end

      it "should return false for having 3 distributions for that store" do
        person = create(:person, :with_delivered_distributions)
        expect(person.has_store_distributions?('BelieveLiv', 3)).to eq(false)
      end
    end

    context "when the user doesn't have distributions for that store" do
      it "should return false" do
        person = create(:person, :with_live_albums)

        expect(person.has_store_distributions?('BelieveLiv')).to eq(false)
      end
    end

    context "when the user has 3 distributions for that store" do
      it "should return true if you look for 3 distributions" do
        person = create(:person, :with_three_delivered_distros)

        expect(person.has_store_distributions?('BelieveLiv', 3)).to eq(true)
      end
    end
  end

  describe "#stem_free_trial_days_left" do
    let(:person) { build_stubbed(:person) }

    context "user is not imported from stem" do
      it "should return 0" do
        allow(person).to receive(:imported_from_stem?).and_return false

        expect(person.stem_free_trial_days_left).to be_zero
      end
    end

    context "user is imported from stem" do
      it "should return trial days left from Stem module" do
        allow(person).to receive(:imported_from_stem?).and_return true
        allow(MassIngestion::Stem).to receive(:free_trial_days_left).and_return(10)

        expect(person.stem_free_trial_days_left).to eq(10)
      end
    end
  end

  describe "#check_credit_card_expiration_date" do
    let(:person_without_distribution) { create(:person) }
    let(:person_with_distribution) { create(:person, :with_live_album) }
    let(:credit_card) { create(:bb_stored_credit_card, expiration_month: 10, expiration_year: 2020) }

    context "conditions are not met to return values" do
      it "returns nil when card not within 45 days of expiring" do
        date = DateTime.new(2020, 5, 28)
        Timecop.travel(date)

        person_with_distribution.stored_credit_cards << credit_card
        person_with_distribution.person_preference = create(:person_preference,
                                                            person: person_with_distribution,
                                                            preferred_credit_card: person_with_distribution.stored_credit_cards.last)

        expect(person_with_distribution.check_credit_card_expiration_date).to be_nil
        Timecop.return
      end

      it "returns nil when card is expired" do
        date = DateTime.new(2020, 5, 28)
        Timecop.travel(date)
        person_with_distribution.stored_credit_cards << credit_card
        person_with_distribution.person_preference = create(:person_preference,
                                                            person: person_with_distribution,
                                                            preferred_credit_card: person_with_distribution.stored_credit_cards.last)
        person_with_distribution.preferred_credit_card.update(expiration_month: 2)

        expect(person_with_distribution.check_credit_card_expiration_date).to be_nil
        Timecop.return
      end

      it "returns nil when user does not have active distributions" do
        date = DateTime.new(2020, 5, 28)
        Timecop.travel(date)
        person_without_distribution.stored_credit_cards << credit_card
        person_without_distribution.person_preference = create(:person_preference,
                                                            person: person_without_distribution,
                                                            preferred_credit_card: person_without_distribution.stored_credit_cards.last)
        expect(person_without_distribution.check_credit_card_expiration_date).to be_nil
        Timecop.return
      end

      it "returns the nil when a paypal account has been uploaded and has not been deleted" do
        paypal_account = create(:stored_paypal_account, person: person_with_distribution)
        date = Date.new(2020, 5, 28)
        Timecop.travel(date)
        person_with_distribution.stored_credit_cards << credit_card
        person_with_distribution.person_preference = create(
            :person_preference,
            person: person_with_distribution,
            preferred_credit_card: person_with_distribution.stored_credit_cards.last,
            preferred_paypal_account: paypal_account,
            preferred_payment_type: "PayPal"
        )

        person_with_distribution.preferred_credit_card.update(expiration_month: 5)
        expect(person_with_distribution.check_credit_card_expiration_date).to eq(nil)
        Timecop.return
      end
    end

    context "conditions to return values are met" do
      xit "returns the number of days until the card expires" do
        date = Date.new(2020, 5, 28)
        Timecop.travel(date)
        person_with_distribution.stored_credit_cards << credit_card
        person_with_distribution.person_preference = create(:person_preference,
                                                            person: person_with_distribution,
                                                            preferred_credit_card: person_with_distribution.stored_credit_cards.last)
        person_with_distribution.preferred_credit_card.update(expiration_month: 5)

        expect(person_with_distribution.check_credit_card_expiration_date).to eq(3)
        Timecop.return
      end

      it "returns the number of days until the card expires even when a previous paypal account has been uploaded and deleted" do
        paypal_account = create(:stored_paypal_account, person: person_with_distribution)
        date = Date.new(2020, 5, 28)
        Timecop.travel(date)
        person_with_distribution.stored_credit_cards << credit_card
        person_with_distribution.person_preference = create(
            :person_preference,
            person: person_with_distribution,
            preferred_credit_card: person_with_distribution.stored_credit_cards.last,
            preferred_paypal_account: paypal_account
        )

        person_with_distribution.preferred_credit_card.update(expiration_month: 5)
        person_with_distribution.person_preference.update(preferred_paypal_account: nil)

        expect(person_with_distribution.check_credit_card_expiration_date).to eq(3)
        Timecop.return
      end
    end
  end

  describe "#post_update_password" do
    let(:person) { create(:person) }

    context "user password changed post actions success" do
      it "should return true" do
        remote_ip = "127.0.0.1"
        user_agent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36"
        is_updated = person.post_update_password(person.password,remote_ip,user_agent)
        expect(is_updated).to eq(true)
        expect(person.account_locked_until).to eq(nil)
        expect(person.login_attempts).to eq(0)
        expect(person.invite_code).to eq(nil)
        expect(person.invite_expiry).to eq(nil)
        expect { person.post_update_password(person.password,remote_ip,user_agent) }
          .to change(LoginEvent, :count).by(1)
        expect { person.post_update_password(person.password,remote_ip,user_agent) }
          .to change { ActionMailer::Base.deliveries.count }.by(1)
      end
    end
  end

  describe "#recalculate_lifetime_earning" do
    let(:person) { create(:person) }

    context "earning record not found" do
      it "should create a new earning record" do
        expect(person.lifetime_earning).to be_nil
        person.recalculate_lifetime_earning

        expect(person.lifetime_earning).to_not be_nil
      end
    end

    it "should recalculate using the earning method" do
      earning = person.create_lifetime_earning

      expect(earning).to receive(:recalculate!)
      person.recalculate_lifetime_earning
    end
  end

  describe "#lifetime_earning_amount" do
    let(:person) { create(:person) }

    context "lifetime earning amount present" do
      it "should recalculate and return total intake" do
        expect(person.lifetime_earning).to be_nil

        result = person.lifetime_earning_amount

        expect(person.lifetime_earning).to_not be_nil
        expect(result).to eq(person.lifetime_earning.total_intake)
      end
    end

    context "lifetime earning not present" do
      it "should return total intake" do
        person.create_lifetime_earning.recalculate!
        result = person.lifetime_earning_amount

        expect(result).to eq(person.lifetime_earning.total_intake)
      end
    end
  end

  describe "#reward_eligible_releases" do
    let(:person) { create(:person) }

    it "should return number of active, distributed releases" do
      create(:album, :approved, person: person)
      create(:album, :live, person: person)

      expect(person.reward_eligible_releases.count).to eq(1)
    end
  end
end

describe Person, "#first_distribution??" do
  context "when the release passed in is the person's first distribuction" do
    it "returns true" do
      person = create(:person)
      album = create(:album, :finalized, person: person)

      result = person.first_distribution?(album)

      expect(result).to eq true
    end
  end

  context "when the release passed in is not the person's first distribuction" do
    it "returns false" do
      person = create(:person, :with_live_album)
      person.albums.first.update!(finalized_at: 5.days.ago)
      album = create(:album, :finalized, person: person)

      result = person.first_distribution?(album)

      expect(result).to eq false
    end
  end
end

describe Person do
  context "customer type is business" do
    it "update is_business true" do
      person = create(:person, customer_type: "business")
      expect(person.is_business).to eq true
    end
  end

  describe "Validations" do
    it "standardizes legacy country codes received from payout provider" do
      LEGACY_NEW_COUNTRY_CODE_MAP.each do |key, value|
        person = create(:person)
        person.update(country: key)
        expect(person[:country]).to eq value
      end
    end
  end
end

describe Person, "#countries_from_sources" do
  context "data source is available in stored credit card" do
    it "includes the country from credit card in the return value" do
      person = create(:person)
      stored_cc = create(:stored_credit_card, person: person)
      expect(person.countries_from_sources.pluck(:iso_code)).to include(stored_cc.country)
    end
  end

  context "data source is available from user self identified country" do
    it "includes the country from self identified country"  do
      person = create(:person, :with_india_country_website)
      expect(person.countries_from_sources.pluck(:iso_code)).to include("IN")
    end
  end

  context "data source is available from the previous logged in ips" do
    it "includes country from pervious login events" do
      person = create(:person)
      create(
        :login_event,
        person_id: person.id,
        ip_address: Faker::Internet.ip_v4_address,
        country: "CN"
      )
      expect(person.countries_from_sources.pluck(:iso_code)).to include("CN")
    end
  end

  context "data source is available from two factor authentication country code" do
    it "includes country from 2FA authentication country code" do
      person = create(:person)
      create(:two_factor_auth, country_code: "971", person: person)
      expect(person.countries_from_sources.pluck(:iso_code)).to include("AE")
    end
  end

  context '#has_multiple_countries?' do
    let(:person) { create(:person) }

    it 'should be false if all sources are from US Territories' do
      expect(person.has_multiple_countries?).to be false
    end

    it 'should be true if country sources are both from and outside of US and Territories' do
      create(:login_event, person: person, country: 'FR')
      expect(person.has_multiple_countries?).to be true
    end
  end
end


describe 'four byte char validation' do
  let(:person) { build(:person, name: "💿💿💿💿💿") }

  it 'validates first_name and last_name for four byte chars' do
    person.valid?
    expect(person.errors.messages[:name].size).to be 1
  end
end

describe Person, "#required_address_info?" do
  context "customer requires address info based on their country" do
    it "returns true when customer has valid info" do
      person = create(:person, address1: "63 Pearl Street", city: "Brooklyn", foreign_postal_code: 112011, country: "IN")
      expect(person.required_address_info?).to eq true
    end
    it "returns false when customer is missing address1" do
      person = create(:person, city: "Brooklyn", foreign_postal_code: 112011, country: "IN")
      expect(person.required_address_info?).to eq false
    end
    it "returns false when customer is missing city" do
      person = create(:person, address1: "63 Pearl Street", foreign_postal_code: 112011, country: "IN")
      expect(person.required_address_info?).to eq false
    end
    it "returns false when customer is missing foreign_postal_code" do
      person = create(:person, address1: "63 Pearl Street", city: "Brooklyn", country: "IN")
      expect(person.required_address_info?).to eq false
    end
  end

  context "customer does not require address info based on their country (US & territories)" do
    it "returns true when user is from US or territory" do
      person = create(:person, country: "US")
      samoan_person = create(:person, country: "AS")
      expect(person.required_address_info?).to eq true
      expect(samoan_person.required_address_info?).to eq true
    end
  end
end

describe Person, "address validations" do
  let!(:person) do
    p = create(:person)
    p.require_address = true
    p
  end
  before(:each) do
    allow(FeatureFlipper).to receive(:show_feature?).with(:bi_transfer, an_instance_of(Person)).and_return(true)
  end
  context "missing parameters" do

    context "country" do
      it "does not save" do
        updates = valid_address_fields_except(:country).merge(country:"")
        expect(person.update(updates)).to be(false)
      end
    end

    context "american user" do
      context "address" do
        it "saves" do
          updates = valid_address_fields_except(:address1, :address2)
          expect(person.update(updates)).to be(true)
        end
      end
      context "city" do
        it "saves" do
          updates = valid_address_fields_except(:city)
          expect(person.update(updates)).to be(true)
        end
      end
      context "state" do
        it "saves" do
          updates = valid_address_fields_except(:state)
          expect(person.update(updates)).to be(true)
        end
      end
      context "postal code" do
        it "saves" do
          updates = valid_address_fields_except(:postal_code)
          expect(person.update(updates)).to be(true)
        end
      end
    end

    context "non-american user" do
      context "address" do
        it "does not save" do
          updates = valid_address_fields_except(:address1, :address2, :country).merge(country: Country::INDIA_ISO)
          expect(person.update(updates)).to be(false)
        end
      end
      context "city" do
        it "does not save" do

          updates = valid_address_fields_except(:city, :country).merge(country: Country::INDIA_ISO)
          expect(person.update(updates)).to be(false)
        end
      end
      context "state" do
        it "saves" do
          updates = valid_address_fields_except(:state, :country).merge(country: Country::INDIA_ISO)
          expect(person.update(updates)).to be(true)
        end
      end
      context "postal code" do
        it "does not save" do
          updates = valid_address_fields_except(:postal_code, :country).merge(country: Country::INDIA_ISO)
          expect(person.update(updates)).to be(false)
        end
      end
    end

  end

  context "no missing parameters" do
    context "invalid parameters" do

      context "country" do
        context "malformed" do
          it "does not save" do
            updates = valid_address_fields_except(:country)
                        .merge(invalid_address_fields_except(:address1, :address2, :city, :state, :postal_code))
            expect(person.update(updates)).to be(false)
          end
        end
        context "country does not exist" do
          it "does not save" do
            updates = valid_address_fields_except(:country).merge(country:"ZZ")
            expect(person.update(updates)).to be(false)
          end
        end
      end

      context "american user" do
        context "address" do
          it "does not save" do
            updates = valid_address_fields_except(:address1, :address2)
                        .merge(invalid_address_fields_except(:city, :state, :postal_code, :country))
            expect(person.update(updates)).to be(false)
          end
        end
        context "city" do
          it "does not save" do
            updates = valid_address_fields_except(:city)
                        .merge(invalid_address_fields_except(:address1, :address2, :state, :postal_code, :country))
            expect(person.update(updates)).to be(false)
          end
        end
        context "state" do
          context "malformatted" do
            it "does not save" do
              updates = valid_address_fields_except(:state)
                          .merge(invalid_address_fields_except(:address1, :address2, :city, :postal_code, :country))
              expect(person.update(updates)).to be(false)
            end
          end
          context "not us state" do
            it "does not save" do
              updates = valid_address_fields_except(:state, :country)
                          .merge(state: "AN")
              expect(person.update(updates)).to be(false)
            end
          end
        end
        context "postal code" do
          it "does not save" do
            updates = valid_address_fields_except(:postal_code)
                        .merge(invalid_address_fields_except(:address1, :address2, :city, :state, :country))
            expect(person.update(updates)).to be(false)
          end
        end
      end

      context "non american user" do
        context "address" do
          it "does not save" do
            updates = valid_address_fields_except(:address1, :address2, :country)
                        .merge(invalid_address_fields_except(:city, :state, :postal_code, :country))
                        .merge(country: Country::INDIA_ISO)
            expect(person.update(updates)).to be(false)
          end
        end
        context "city" do
          it "does not save" do
            updates = valid_address_fields_except(:city, :country)
                        .merge(invalid_address_fields_except(:address1, :address2, :state, :postal_code, :country))
                        .merge(country: Country::INDIA_ISO)
            expect(person.update(updates)).to be(false)
          end
        end
        context "state" do
          context "malformatted" do
            it "does not save" do
              updates = valid_address_fields_except(:state, :country)
                          .merge(invalid_address_fields_except(:address1, :address2, :city, :postal_code, :country))
                          .merge(country: Country::INDIA_ISO)
              expect(person.update(updates)).to be(false)
            end
          end
          context "not us state" do
            it "saves" do
              updates = valid_address_fields_except(:state, :country)
                          .merge(state: "AN")
                          .merge(country: Country::INDIA_ISO)
              expect(person.update(updates)).to be(true)
            end
          end
        end
        context "postal code" do
          it "does not save" do
            updates = valid_address_fields_except(:postal_code, :country)
                        .merge(invalid_address_fields_except(:address1, :address2, :city, :state, :country))
                        .merge(country: Country::INDIA_ISO)
            expect(person.update(updates)).to be(false)
          end
        end
      end

    end
    context "no invalid parameters" do

      context "american user" do
        context "with state name" do
          it "reformats to abbreviation" do
            updates = valid_address_fields_except(:state).merge(state: "New York")
            expect(person.update(updates)).to be(true)
            expect(person.state).to eq("NY")
          end
        end
        context "with state abbreviation" do
          it "saves" do
            updates = valid_address_fields_except(:none)
            expect(person.update(updates)).to be(true)
            expect(person.state).to eq("NY")
          end
        end
      end

      context "non american user" do
        context "no invalid parameters" do
          it "saves" do
            updates = valid_address_fields_except(:none)
            expect(person.update(updates)).to be(true)
          end
        end
      end

    end
  end

  context "address_locked" do
    before(:each) do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
      person.lock_address!(reason: FlagReason.payoneer_kyc_completed_address_lock)
    end

    context "when attempting to update address fields" do
      it "raises error" do
        expect{person.update(valid_address_fields_except(:none))}
          .to raise_error(Tunecore::People::AddressValidatable::AddressLockedError)
      end
    end
  end
end

describe Person do
  describe "#linked_tunecore_accounts" do
    context "when payout provider is nil" do
      let(:person) { create(:person) }

      it "returns an empty array" do
        expect(person.linked_tunecore_accounts).to eq([])
      end
    end

    context "with payout provider" do
      let(:people) { create_list(:person, 3, :with_payout_provider) }

      before do
        PayoutProvider.where(person_id: people.map(&:id)).update_all(provider_identifier: "1234")
        people.each { |person| person.payout_provider.reload }
      end

      it "returns other people with same payout provider identifer" do
        linked_accounts = people.first.linked_tunecore_accounts

        expect(linked_accounts.size).to eq(2)
        expect(linked_accounts.map(&:id)).not_to include(people.first.id)
      end
    end
  end
end

def valid_address_fields_except(*exception)
  {
    address1: "123 Address St",
    address2: "Apt 1",
    postal_code: "12345-1234",
    city: "New York",
    state: "NY",
    country: "US"
  }.except(*exception)
end

def invalid_address_fields_except(*exception)
  {
    address1: "123 $$$ St",
    address2: "Apt $1",
    postal_code: "_/12345-1234",
    city: "1New York",
    state: "1NY",
    country: "1US"
  }.except(*exception)
end

describe Person, 'country audits' do
  context 'create' do
    it 'should create an country audit on sign up' do
      expect {
        person = build(:person)
        person.country_audit_source = CountryAuditSource::SELF_IDENTIFIED_NAME
        person.save
      }.to change {
        CountryAudit.count
      }.by(1)
    end
  end

  context 'update' do
    let! (:person) {
      person = build(:person)
      person.country_audit_source = CountryAuditSource::SELF_IDENTIFIED_NAME
      person.save
      person.country_audit_source = nil
      person
    }

    it 'should create an country audit if country has changed' do
      expect {
        person.country_audit_source = CountryAuditSource::SELF_IDENTIFIED_NAME
        person.country = 'GB'
        person.save
      }.to change {
        CountryAudit.count
      }.by(1)
    end

    it 'should create a country audit if the audit_source has changed but the country is the same' do
      expect {
        person.country_audit_source = CountryAuditSource::PAYONEER_KYC_NAME
        person.save
      }.to change {
        CountryAudit.count
      }.by(1)
    end

    it 'does not create a new country audit (and does not raise an error) when country and audit_source are the same' do
      expect {
        person.country_audit_source = CountryAuditSource::PAYONEER_KYC_NAME
        person.country = 'GB'
        person.save
        person.country_audit_source = CountryAuditSource::PAYONEER_KYC_NAME
        person.country = 'GB'
        person.address1 = "New Address 1"
        person.save
      }.to change {
        CountryAudit.count
      }.by(1)
    end

    it 'should not create country audit if country has not changed' do
      expect {
        person.country_audit_source = CountryAuditSource::SELF_IDENTIFIED_NAME
        person.save
      }.to_not change {
        CountryAudit.count
      }
    end

    it 'should raise exception if country_audit_source is missing' do
      expect {
        person.country = Country::INDIA_ISO
        person.save
      }.to raise_exception('missing country_audit_source')
    end
  end
end

describe Person, 'India site user with India country' do
  let!(:gujarat) { CountryState.find_by(name: "Gujarat") }
  let!(:punjab) { CountryState.find_by(name: "Punjab") }
  let!(:jalandhar) { CountryStateCity.find_by(name:"Jalandhar", state_id: CountryState.find_by(name: "Punjab").id) }
  let!(:person) { create(:person, :with_india_country_website) }

  let!(:us_person_on_in) do
    us_person_on_in = create(:person, :with_india_country_website)
    us_person_on_in.update(country: "US")
    us_person_on_in
  end

  context 'updating state' do
    it 'updates state and country_state when given a valid Indian state' do
      expect(person.country_state).to be nil
      person.update(state: "Gujarat")
      expect(person.state).to eq("Gujarat")
      expect(person.country_state).to eq(gujarat)
    end

    it 'updates state but does not update country_state when given an invalid Indian state' do
      expect(person.country_state).to be nil
      person.update(state: "Georgia")
      expect(person.state).to eq("Georgia")
      expect(person.country_state).to be nil
    end

    it 'does not update country_state for users with non-India country' do
      expect(us_person_on_in.country_state).to be nil
      us_person_on_in.update(state: "Gujarat")
      expect(us_person_on_in.state).to eq("Gujarat")
      expect(us_person_on_in.country_state).to be nil
    end
  end

  context 'updating city' do
    it 'updates city and country_state_city when a user has a valid state and city' do
      expect(person.country_state_city).to be nil
      person.update(state: "Punjab", city: "Jalandhar")
      expect(person.city).to eq("Jalandhar")
      expect(person.country_state_city).to eq(jalandhar)
    end

    it 'updates city but not country_state city when user has invalid state or city' do
      expect(person.country_state_city).to be nil
      person.update(state: "Tennessee", city: "Jalandhar")
      expect(person.city).to eq("Jalandhar")
      expect(person.country_state_city).to be nil
    end

    it 'updates city but not country_state city for users with non-India country' do
      expect(us_person_on_in.country_state).to be nil
      us_person_on_in.update(state: "Punjab", city: "Jalandhar")
      expect(us_person_on_in.city).to eq("Jalandhar")
      expect(us_person_on_in.country_state_city).to be nil
    end
  end

  context 'manually updating country_state' do
    it 'updates country_state AND state for India users' do
      person.update(country_state: gujarat)
      expect(person.country_state).to eq(gujarat)
      expect(person.state).to eq('Gujarat')
    end

    it 'does not update country_state or state for users in another country' do
      us_person_on_in.update(country_state: gujarat)
      expect(us_person_on_in.country_state).to be nil
      expect(us_person_on_in.state).to be nil
    end
  end

  context 'manually updating country_state_city' do
    it 'updates country_state_city AND city for India users' do
      person.update(country_state: punjab, country_state_city: jalandhar)
      expect(person.country_state_city).to eq(jalandhar)
      expect(person.city).to eq('Jalandhar')
    end

    it 'does not update country_state_city or city for users in another country' do
      us_person_on_in.update(country_state: punjab, country_state_city: jalandhar)
      expect(us_person_on_in.country_state_city).to be nil
      expect(us_person_on_in.city).to be nil
    end
  end

  context 'changing from India to another country' do
    it 'clears existing country_state and country_state_city' do
      person.update(state: "Punjab", city: "Jalandhar")
      expect(person.country_state).to eq(punjab)
      expect(person.country_state_city).to eq(jalandhar)
      person.country_audit_source = CountryAuditSource::SELF_IDENTIFIED_NAME
      person.update(country: "US")
      expect(person.country_state).to be nil
      expect(person.country_state_city).to be nil
    end
  end

  context 'changing from another country to India' do
    it 'adds country_state and country_state_city info if possible' do
      us_person_on_in.update(state: "Punjab", city: "Jalandhar")
      expect(us_person_on_in.country_state_city).to be nil
      expect(us_person_on_in.country_state).to be nil
      us_person_on_in.country_audit_source = CountryAuditSource::SELF_IDENTIFIED_NAME
      us_person_on_in.update(country: "IN")
      expect(us_person_on_in.country_state).to eq(punjab)
      expect(us_person_on_in.country_state_city).to eq(jalandhar)
    end
  end
end

describe Person, 'payin config' do
  let!(:person) { create(:person) }
  it "should use the US corporate entity when feature flipper is off" do
    person.update!(corporate_entity: CorporateEntity.find_by(name: CorporateEntity::BI_LUXEMBOURG_NAME))
    expect(FeatureFlipper).to receive(:show_feature?).with(:money_in_bi_transfer, person.id).and_return(false)
    expect(person.payin_corporate_entity).to eq(CorporateEntity.find_by(name: CorporateEntity::TUNECORE_US_NAME))
  end

  it "should use the person's entity when the feature flipper is on" do
    person.update!(corporate_entity: CorporateEntity.find_by(name: CorporateEntity::BI_LUXEMBOURG_NAME))
    expect(FeatureFlipper).to receive(:show_feature?).with(:money_in_bi_transfer, person.id).and_return(true)
    expect(person.payin_corporate_entity).to eq(CorporateEntity.find_by(name: CorporateEntity::BI_LUXEMBOURG_NAME))
  end

  it "should build the payin config for paypal" do
    expect(FeatureFlipper).to receive(:show_feature?).with(:money_in_bi_transfer, person.id).and_return(true)
    expected_payin_config = PayinProviderConfig.find_by!(name: PayinProviderConfig::PAYPAL_NAME, corporate_entity: person.corporate_entity, currency: person.country_website.currency)
    expect(person.paypal_payin_provider_config).to eq(expected_payin_config)
  end
end

describe Person, "#tax_withheld?" do
  let!(:person1) { create(:person) }
  let!(:person2) { create(:person) }
  let!(:txn) { create(:person_transaction, person: person1, target_type: "IRSTaxWithholding") }

  it "returns true when the person has atleast one IRSTaxWithholding type transaction" do
    expect(person1.tax_withheld?).to be_truthy
  end

  it "returns false when the person doesn't have atleast one IRSTaxWithholding type transaction" do
    expect(person2.tax_withheld?).to be_falsey
  end
end

describe Person, "Plan Cart Methods" do
  let!(:purchased_plan_person) { create(:person) }
  let!(:person_plan) { create(:person_plan, person: purchased_plan_person)}
  let!(:carted_plan_person) do
    person = create(:person)
    plan = Plan.last
    product = plan.products.find_by!(country_website: person.country_website)
    Product.add_to_cart(person, product, product)
    person
  end
  let!(:legacy_person) { create(:person) }

  describe "#owns_or_carted_plan?" do
    it "returns true when the person has a person_plan" do
      expect(purchased_plan_person.owns_or_carted_plan?).to be true
    end

    it "returns true when the person has a carted plan" do
      expect(carted_plan_person.owns_or_carted_plan?).to be true
    end

    it "returns false when the person is on legacy pricing" do
      expect(legacy_person.owns_or_carted_plan?).to be false
    end
  end

  describe "#plan_in_cart?" do
    it "returns true when the person has a carted plan" do
      expect(carted_plan_person.plan_in_cart?).to be true
    end

    it "returns false when the person does not have a carted plan" do
      expect(purchased_plan_person.plan_in_cart?).to be false
      expect(legacy_person.plan_in_cart?).to be false
    end
  end

  describe "#clear_cart!" do
    let!(:person) { create(:person) }
    let!(:album_1) { create(:album, person: person) }
    let!(:album_2) { create(:album, person: person) }
    let!(:product) { Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID) }
    let!(:in_cart_purchase) { create(:purchase, :unpaid, person: person, product: product, related: album_1) }
    let!(:paid_purchase) { create(:purchase, :in_invoice, person: person, product: product, related: album_2) }

    subject { person.clear_cart! }
    it "destroys unpaid purchases, doesn't touch paid purchases" do
      subject
      expect{ in_cart_purchase.reload }.to raise_error(ActiveRecord::RecordNotFound)
      expect(paid_purchase.reload.present?).to be(true)
    end
  end
end

describe Person, "Tax Forms" do
  let(:person) { create(:person, :with_payout_provider) }
  let!(:primary_tax_form) { create(:tax_form, :w9_individual, person: person) }
  let!(:secondary_tax_form) do
    create(
      :tax_form, :w9_entity,
      person: person, payout_provider_config: PayoutProviderConfig.secondary_tax_form_config
    )
  end
  let(:tax_form_mappings) do
    {
      distribution: "primary",
      publishing: "secondary"
    }
  end

  describe "#primary_tax_form" do
    it "returns the tax-form submitted under the user's default payoneer program" do
      expect(person.primary_tax_form).to eq(primary_tax_form)
    end
  end

  describe "#secondary_tax_form" do
    it "returns the tax-form submitted under the user's secondary-tax-form payoneer program" do
      expect(person.secondary_tax_form).to eq(secondary_tax_form)
    end
  end

  describe "#user_mapped_distribution_tax_form" do
    it "returns the tax-form mapped by the user to the distribution stream" do
      expect(person.user_mapped_distribution_tax_form).to eq(nil)

      PayoutProvider::TaxFormRevenueStreamMapperForm
        .new(person: person, mappings: tax_form_mappings)
        .save

      expect(person.user_mapped_distribution_tax_form).to eq(primary_tax_form)
    end
  end

  describe "#user_mapped_publishing_tax_form" do
    it "returns the tax-form mapped by the user to the publishing stream" do
      expect(person.user_mapped_publishing_tax_form).to eq(nil)

      PayoutProvider::TaxFormRevenueStreamMapperForm
        .new(person: person, mappings: tax_form_mappings)
        .save

      expect(person.user_mapped_publishing_tax_form).to eq(secondary_tax_form)
    end
  end
end

# helper method for conversions
def test_conversion(convert)
  country_id_before = 1
  country_id_after = convert ? 2 : 1
  currency_after = convert ? 'CAD' : 'USD'

  expect(@person.person_balance.currency).to eq('USD')
  expect(@person.country_website_id).to eq(1)
  expect(@person.convert_to_canadian).to eq convert

  @person.reload

  expect(@person.country_website_id).to eq(country_id_after)
  expect(@person.person_balance.currency).to eq(currency_after)
end
