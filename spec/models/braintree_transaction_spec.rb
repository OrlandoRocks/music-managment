require "rails_helper"

def successful_response
'response=1&responsetext=Customer%20Update%20Successful&authcode=&transactionid=0&avsresponse=&cvvresponse=&orderid=&type=&response_code=100&customer_vault_id=1460284452&username=1118461&time=20090825212928&amount=&hash=12e39a45a7f57dcc3aff409e29d06213'
end

def invalid_response
'response=1&responsetext=Customer%20Update%20Successful&authcode=&transactionid=0&avsresponse=&cvvresponse=&orderid=&type=&response_code=100&customer_vault_id=1460284452&username=1118461&time=20090825212928&amount=&hash=12e39a45a7f67dcc3aff409e29d06213'
end

describe BraintreeTransaction do
  let(:mock_gateway_response) { double('gateway_response') }
  let(:braintree_transaction) { build_stubbed(:braintree_transaction) }

  it 'should set status to success if response code is 100', fdoc: :consider do
    braintree_transaction.response_code = "100"
    braintree_transaction.set_status(successful_response)
    expect(braintree_transaction.status).to eq('success')
  end

  it "should set status to fraud if response code is 250" do
    allow(AdminNotifier).to receive(:suspicious_activity)
    braintree_transaction.response_code = "250"
    braintree_transaction.set_status(invalid_response)
    expect(braintree_transaction.status).to eq('fraud')
  end

  it "should set status to fraud if response code is 253" do
    allow(AdminNotifier).to receive(:suspicious_activity)
    braintree_transaction.response_code = "253"
    braintree_transaction.set_status(invalid_response)
    expect(braintree_transaction.status).to eq('fraud')
  end

  it 'should set status to decline if response code is between 200-299 (excluding 250-253)', fdoc: :consider do
    braintree_transaction.response_code = "200"
    braintree_transaction.set_status(invalid_response)
    expect(braintree_transaction.status).to eq('declined')

    braintree_transaction.response_code = "299"
    braintree_transaction.set_status(invalid_response)
    expect(braintree_transaction.status).to eq('declined')
  end

  it 'should set status to error if response code is = 300 and AVS or CVV rejection was not reason for error', fdoc: :consider do
    braintree_transaction.response_code = "300"
    braintree_transaction.raw_response = "response=3&responsetext=Invalid%20Credit%20Card%20Number%20REFID:102270542&authcode=&transactionid=0&avsresponse=&cvvresponse=&orderid=194188&type=sale&response_code=300&username=1155833&time=20091019170816&amount=34.83&hash=4b9ef2a83eb259d97a66ced9895a0fe6"
    braintree_transaction.set_status(braintree_transaction.raw_response)
    expect(braintree_transaction.status).to eq('error')
  end

  it 'should set status to declined if response code is = 300 and CVV rejection was reason for error', fdoc: :consider do
    braintree_transaction.response_code = "300"
    braintree_transaction.raw_response = "response=2&responsetext=CVV%20REJECTED&authcode=052962&transactionid=1099996931&avsresponse=Y&cvvresponse=N&orderid=194191&type=sale&response_code=300&username=1155833&time=20091019171510&amount=41.94&hash=4ceaf61462dbfd1cc0d26185b0c6f969"
    braintree_transaction.set_status(braintree_transaction.raw_response)
    expect(braintree_transaction.status).to eq('declined')
  end

  it 'should set status to declined if response code is = 300 and AVS rejection was reason for error', fdoc: :consider do
    braintree_transaction.response_code = "300"
    braintree_transaction.raw_response = "response=2&responsetext=AVS%20REJECTED&authcode=052962&transactionid=1099996931&avsresponse=Y&cvvresponse=N&orderid=194191&type=sale&response_code=300&username=1155833&time=20091019171510&amount=41.94&hash=4ceaf61462dbfd1cc0d26185b0c6f969"
    braintree_transaction.set_status(braintree_transaction.raw_response)
    expect(braintree_transaction.status).to eq('declined')
  end

  it 'should set the status to duplicate if response code = 400', fdoc: :consider do
    braintree_transaction.response_code = "400"
    braintree_transaction.raw_response = "response=2&responsetext=DUPLICATE%5B06-12066%5D&authcode=&transactionid=1197647385&avsresponse=Y&cvvresponse=M&orderid=262506&type=sale&response_code=400&username=1155833&time=20100303191738&amount=9.99&hash=339bdc78be22f8419510cf424913a8c4"
    braintree_transaction.set_status(braintree_transaction.raw_response)
    expect(braintree_transaction.status).to eq('duplicate')
  end

  it 'should set the status to declined if the response code = 400 and the response text = NO+CHECKING+ACCT', fdoc: :consider do
    braintree_transaction.response_code = "400"
    braintree_transaction.raw_response = "response=2&responsetext=NO+CHECKING+ACCT+++++++++++++++++%5B06-12066%5D&authcode=&transactionid=1197647385&avsresponse=Y&cvvresponse=M&orderid=262506&type=sale&response_code=400&username=1155833&time=20100303191738&amount=9.99&hash=339bdc78be22f8419510cf424913a8c4"
    braintree_transaction.set_status(braintree_transaction.raw_response)
    expect(braintree_transaction.status).to eq('declined')
  end

  it "should set status to error if validation failed" do
    allow(mock_gateway_response).to receive(:response_code).and_return(100)
    allow(mock_gateway_response).to receive(:store?).and_return(false)
    bt = BraintreeTransaction.create(:transaction_id => 1, :gateway_response => mock_gateway_response)
    bt.transaction_id = 1
    expect(bt).not_to be_valid
    bt.set_status(invalid_response)
    expect(bt.status).to eq('error')
  end

  it "should flag transaction as duplicate if invoice has already been paid" do
    braintree_transaction = create(:braintree_transaction, response_code: "100", ip_address: "127.0.0.1", amount: 1.00, action: "sale")
    braintree_transaction.set_status(invalid_response)
    braintree_transaction.detect_duplicates
    expect(braintree_transaction.status).to eq('duplicate')
  end
end

describe BraintreeTransaction, "when making a purchase" do
  let(:mock_gateway_response) { double('gateway_response') }

  before(:each) do
    ActiveRecord::Base.connection.execute "SET FOREIGN_KEY_CHECKS = 0";
  end

  after(:each) do
    ActiveRecord::Base.connection.execute "SET FOREIGN_KEY_CHECKS = 1";
  end

  context "when the response is invalid" do
    it "marks the transaction as invalid" do
      person = create(:person)
      invoice = create(:invoice, person: person)

      allow(ActiveMerchant::Billing::Integrations::Braintree::Return).to receive(:new).and_return(mock_gateway_response)
      default_stubs_for_mock_response
      allow(mock_gateway_response).to receive(:valid?).and_return(false)

      transaction = BraintreeTransaction.process("successful_response", person.id, '127.0.0.1')

      expect(transaction.status).to eq('invalid')
    end
  end

  it "should store stored credit card if the user requested storage" do
    person = create(:person)
    invoice = create(:invoice, person: person)
    allow(ActiveMerchant::Billing::Integrations::Braintree::Return).to receive(:new).and_return(mock_gateway_response)
    default_stubs_for_mock_response
    allow(mock_gateway_response).to receive(:transaction_id).and_return("100")
    allow(mock_gateway_response).to receive(:order_id).and_return(invoice.id)
    allow(mock_gateway_response).to receive(:customer_id).and_return(person.id)
    allow(mock_gateway_response).to receive(:store?).and_return(true)
    allow(mock_gateway_response).to receive(:mark_as_preferred?).and_return(false)
    allow(StoredCreditCard).to receive_message_chain(:where, :first).and_return(nil)

    expect(StoredCreditCard).to receive(:create)
    BraintreeTransaction.process("successful_response", person.id, '127.0.0.1')
  end

  it "should store transaction_id as a string" do
    person = create(:person)
    invoice = create(:invoice, person: person)
    allow(ActiveMerchant::Billing::Integrations::Braintree::Return).to receive(:new).and_return(mock_gateway_response)
    default_stubs_for_mock_response
    allow(mock_gateway_response).to receive(:transaction_id).and_return("100")
    allow(mock_gateway_response).to receive(:order_id).and_return(invoice.id)
    allow(mock_gateway_response).to receive(:customer_id).and_return(person.id)
    allow(mock_gateway_response).to receive(:transaction_id).and_return("123456")
    bt = BraintreeTransaction.process("successful_response", person.id, '127.0.0.1')

    expect(bt.transaction_id).to eq("123456")
  end

  it "should NOT store stored credit card if the user requested storage" do
    person = create(:person)
    invoice = create(:invoice, person: person)
    allow(ActiveMerchant::Billing::Integrations::Braintree::Return).to receive(:new).and_return(mock_gateway_response)
    default_stubs_for_mock_response
    allow(mock_gateway_response).to receive(:transaction_id).and_return("100")
    allow(mock_gateway_response).to receive(:order_id).and_return(invoice.id)
    allow(mock_gateway_response).to receive(:customer_id).and_return(person.id)
    allow(mock_gateway_response).to receive(:store?).and_return(false)

    expect(StoredCreditCard).not_to receive(:find)
    expect(StoredCreditCard).not_to receive(:create)
    BraintreeTransaction.process("successful_response", person.id, '127.0.0.1')
  end

  context "when no person preference exists" do
    it "should store the credit card as the preferred credit card if 'Store CC' IS checked and 'Mark as Preferred' IS checked" do
      person = create(:person)
      invoice = create(:invoice, person: person)
      allow(ActiveMerchant::Billing::Integrations::Braintree::Return).to receive(:new).and_return(mock_gateway_response)
      default_stubs_for_mock_response
      allow(mock_gateway_response).to receive(:transaction_id).and_return("100")
      allow(mock_gateway_response).to receive(:order_id).and_return(invoice.id)
      allow(mock_gateway_response).to receive(:customer_id).and_return(person.id)
      allow(mock_gateway_response).to receive(:store?).and_return(true)
      allow(mock_gateway_response).to receive(:mark_as_preferred?).and_return(true)

      person_preference = double("person_preference", "nil?" => true, preferred_credit_card_id: nil)
      allow(PersonPreference).to receive(:where).with({ person_id: person.id }).and_return([person_preference])
      expect(PersonPreference).to receive(:create)

      mock_braintree_query
      BraintreeTransaction.process("successful_response", person.id, '127.0.0.1')
    end
  end

  context "when a person preference exists" do
    it "should store the credit card as the preferred credit card if 'Store CC' IS checked and 'Mark as Preferred' IS checked" do
      person = create(:person)
      invoice = create(:invoice, person: person)
      allow(ActiveMerchant::Billing::Integrations::Braintree::Return).to receive(:new).and_return(mock_gateway_response)
      default_stubs_for_mock_response
      allow(mock_gateway_response).to receive(:transaction_id).and_return("100")
      allow(mock_gateway_response).to receive(:order_id).and_return(invoice.id)
      allow(mock_gateway_response).to receive(:customer_id).and_return(person.id)
      allow(mock_gateway_response).to receive(:store?).and_return(true)
      allow(mock_gateway_response).to receive(:mark_as_preferred?).and_return(true)

      person_preference = double("person_preference", "nil?" => false, preferred_credit_card_id: nil)
      allow(PersonPreference).to receive(:where).with({ person_id: person.id }).and_return([person_preference])
      expect(person_preference).to receive(:update_attribute)

      mock_braintree_query
      BraintreeTransaction.process("successful_response", person.id, '127.0.0.1')
    end
  end

  it "should NOT store the credit card as the preferred credit card if 'Store CC' IS NOT checked and 'Mark as Preferred' IS checked" do
    person = create(:person)
    invoice = create(:invoice, person: person)
    allow(ActiveMerchant::Billing::Integrations::Braintree::Return).to receive(:new).and_return(mock_gateway_response)
    default_stubs_for_mock_response
    allow(mock_gateway_response).to receive(:transaction_id).and_return("100")
    allow(mock_gateway_response).to receive(:order_id).and_return(invoice.id)
    allow(mock_gateway_response).to receive(:customer_id).and_return(person.id)
    allow(mock_gateway_response).to receive(:store?).and_return(false)
    allow(mock_gateway_response).to receive(:mark_as_preferred?).and_return(true)

    expect(PersonPreference).not_to receive(:find)
    expect(PersonPreference).not_to receive(:create)
    BraintreeTransaction.process("successful_response", person.id, '127.0.0.1')
  end

  it "should NOT store the credit card as the preferred credit card if 'Store CC' IS NOT checked and 'Mark as Preferred' IS NOT checked" do
    person = create(:person)
    invoice = create(:invoice, person: person)
    allow(ActiveMerchant::Billing::Integrations::Braintree::Return).to receive(:new).and_return(mock_gateway_response)
    default_stubs_for_mock_response
    allow(mock_gateway_response).to receive(:transaction_id).and_return("100")
    allow(mock_gateway_response).to receive(:order_id).and_return(invoice.id)
    allow(mock_gateway_response).to receive(:customer_id).and_return(person.id)
    allow(mock_gateway_response).to receive(:store?).and_return(false)
    allow(mock_gateway_response).to receive(:mark_as_preferred?).and_return(false)

    expect(PersonPreference).not_to receive(:find)
    expect(PersonPreference).not_to receive(:create)
    BraintreeTransaction.process("successful_response", person.id, '127.0.0.1')
  end

  it "should settle the invoice when a successful sale is made" do
    bt = build(:braintree_transaction, status: "success", action: "sale")
    invoice = double("invoice")

    allow(bt).to receive(:invoice).and_return(invoice)
    allow(bt).to receive(:payment_amount_cents).and_return(100)
    allow(invoice).to receive(:id).and_return(1)
    allow(invoice).to receive(:can_settle?).and_return(true)
    allow(invoice).to receive(:settled?).and_return(false)
    expect(invoice).to receive(:settlement_received)
    expect(invoice).to receive(:settled!)
    bt.settle_invoice
  end

  it "should send a thank you when the invoice is settled" do
    bt = build(:braintree_transaction, status: "success", action: "sale")
    invoice = double("invoice")

    allow(bt).to receive(:invoice).and_return(invoice)
    allow(bt).to receive(:payment_amount_cents).and_return(100)
    allow(invoice).to receive(:id).and_return(1)
    allow(invoice).to receive(:can_settle?).and_return(true)
    allow(invoice).to receive(:settled?).and_return(true)
    bt.settle_invoice
  end

  it "should NOT settle the invoice when an unsuccessful sale is made" do
    bt = build(:braintree_transaction, status: "declined", action: "sale")
    invoice = double("invoice")

    allow(bt).to receive(:invoice).and_return(invoice)
    allow(bt).to receive(:payment_amount_cents).and_return(100)
    expect(invoice).not_to receive(:settlement_received)
    expect(invoice).not_to receive(:settled!)
    bt.settle_invoice
  end

  it "should NOT settle the invoice when the transaction is not a sale" do
    bt = build(:braintree_transaction, status: "success", action: "refund")
    invoice = double("invoice")

    allow(bt).to receive(:invoice).and_return(invoice)
    allow(bt).to receive(:payment_amount_cents).and_return(100)
    expect(invoice).not_to receive(:settlement_received)
    expect(invoice).not_to receive(:settled!)
    bt.settle_invoice
  end

  describe "::process_purchase_with_stored_card" do
    context "when using a braintree_blue credit card" do
      it "should call #process_braintree_blue_transaction" do
        person = create(:person)
        invoice = create(:invoice, person: person)
        stored_credit_card = FactoryBot.create(:bb_stored_credit_card, person: person)
        stub_braintree_blue_methods
        BraintreeTransaction.process_purchase_with_stored_card(invoice, stored_credit_card, payment_amount: 1000)
      end

      it "should set #braintree_transaction to currency of country_website of stored card" do
        person = create(:person, country_website: CountryWebsite.find(2))
        invoice = create(:invoice, person: person)
        stored_credit_card = FactoryBot.create(:bb_stored_credit_card, person: person)
        stub_braintree_blue_methods
        bt_txn = BraintreeTransaction.process_purchase_with_stored_card(invoice, stored_credit_card, payment_amount: 1000)
        expect(bt_txn.currency).to eq(stored_credit_card.person.country_website.currency)
      end
    end
  end

  describe "#process_braintree_blue_transaction" do

    before(:each) do
      transaction = double(
        "braintree blue transaction",
        id: 99999,
        processor_response_code: "10",
        processor_response_text: "processor_response_text",
        processor_response_type: "processor_response_type",
        additional_processor_response: "additional_processor_response",
        network_response_text: "network_response_text",
        avs_error_response_code: "avs_response_code",
        avs_postal_code_response_code: "avs_postal_response",
        avs_street_address_response_code: "avs_street_address_response",
        cvv_response_code: "cvv_response_code",
      )

      @transaction_id = SecureRandom.hex
      allow(transaction).to receive(:currency_iso_code).and_return("USD")
      allow(transaction).to receive(:processor_response_code).and_return("1000")
      allow(transaction).to receive(:id).and_return(@transaction_id)
      @braintree_successful_result = Braintree::SuccessfulResult.new
      allow(@braintree_successful_result).to receive(:transaction).and_return(transaction)
    end

    it "should call gateway_3ds_sale_transaction if CC and merchant belongs to EEA, three_d_secure_nonce and featureflag are present" do
      person = create(:person, :with_lux_stored_credit_card)
      stored_credit_card = person.stored_credit_cards.first
      bt = build(:braintree_transaction, stored_credit_card: stored_credit_card)
      bt.invoice = create(:invoice, person: bt.person)

      expect_any_instance_of(BraintreeTransaction).to receive(:create_3ds_sale_transaction).and_return(@braintree_successful_result)
      allow(FeatureFlipper).to receive(:show_feature?).with(:money_in_bi_transfer, person).and_return(true)
      bt.process_braintree_blue_transaction(10.00, { three_d_secure_nonce: SecureRandom.hex })

      expect(bt.transaction_id).to eq(@transaction_id)
      expect(bt.status).to eq("success")
      expect(bt.response_code).to eq("1000")
      expect(bt.currency).to eq(person.currency)
    end

    it "should call gateway_sale_transaction if money_in_bi_transfer featureflag not enabled" do
      person = create(:person, :with_lux_stored_credit_card)
      stored_credit_card = person.stored_credit_cards.first
      bt = build(:braintree_transaction, stored_credit_card: stored_credit_card)
      bt.invoice = create(:invoice, person: bt.person)

      expect_any_instance_of(BraintreeTransaction).to receive(:create_sale_transaction).and_return(@braintree_successful_result)
      bt.process_braintree_blue_transaction(10.00, { three_d_secure_nonce: SecureRandom.hex })

      expect(bt.transaction_id).to eq(@transaction_id)
      expect(bt.status).to eq("success")
      expect(bt.response_code).to eq("1000")
      expect(bt.currency).to eq(person.currency)
    end

    it "should call create_sale_transaction if transaction is renewal. CC and merchant belongs to EEA, three_d_secure_nonce and featureflag are present" do
      person = create(:person, :with_lux_stored_credit_card)
      stored_credit_card = person.stored_credit_cards.first
      bt = build(:braintree_transaction, stored_credit_card: stored_credit_card, ip_address: "renewal")
      bt.invoice = create(:invoice, person: bt.person)

      expect_any_instance_of(BraintreeTransaction).to receive(:create_sale_transaction).and_return(@braintree_successful_result)
      allow(FeatureFlipper).to receive(:show_feature?).with(:money_in_bi_transfer, person).and_return(true)

      bt.process_braintree_blue_transaction(10.00, { three_d_secure_nonce: SecureRandom.hex })

      expect(bt.transaction_id).to eq(@transaction_id)
      expect(bt.status).to eq("success")
      expect(bt.response_code).to eq("1000")
      expect(bt.currency).to eq(person.currency)
    end

    it "should call gateway_sale_transaction if CC and merchant not in EEA" do
      person = create(:person, :with_braintree_blue_stored_credit_card)
      stored_credit_card = person.stored_credit_cards.first
      bt = build(:braintree_transaction, stored_credit_card: stored_credit_card)
      bt.invoice = create(:invoice, person: bt.person)

      expect_any_instance_of(BraintreeTransaction).to receive(:create_sale_transaction).and_return(@braintree_successful_result)
      allow(FeatureFlipper).to receive(:show_feature?).with(:money_in_bi_transfer, person).and_return(true)
      bt.process_braintree_blue_transaction(10.00, { three_d_secure_nonce: SecureRandom.hex })

      expect(bt.transaction_id).to eq(@transaction_id)
      expect(bt.status).to eq("success")
      expect(bt.response_code).to eq("1000")
      expect(bt.currency).to eq(person.currency)
    end

    it "correctly sets the attributes on the braintree_transaction" do
      stored_credit_card = create(:person, :with_braintree_blue_stored_credit_card).stored_credit_cards.first
      bt = build(:braintree_transaction, stored_credit_card: stored_credit_card)
      bt.invoice = create(:invoice, person: bt.person)

      expect_any_instance_of(Braintree::TransactionGateway).to receive(:sale).and_return(@braintree_successful_result)
      bt.process_braintree_blue_transaction(10.00)

      expect(bt.transaction_id).to eq(@transaction_id)
      expect(bt.status).to eq("success")
      expect(bt.response_code).to eq("1000")
      expect(bt.currency).to eq("USD")
    end

    it "should call the #BraintreeTransactionLogWorker" do
      expect(BraintreeTransactionLogWorker).to receive_message_chain(:new, :async, :write_to_bucket)
      stored_credit_card = FactoryBot.create(:bb_stored_credit_card)
      bt = build(:braintree_transaction, stored_credit_card: stored_credit_card)
      braintree_successful_result = Braintree::SuccessfulResult.new
      bt.invoice = create(:invoice, person: bt.person)
      bt.process_braintree_blue_transaction(10.00)
    end
  end

  describe "#process_transaction_params" do
    it "correctly sets the attributes on the braintree_transaction" do
      stored_credit_card = create(:bb_stored_credit_card)
      bt = build(:braintree_transaction, stored_credit_card: stored_credit_card)
      braintree_successful_result = Braintree::SuccessfulResult.new
      bt.invoice = create(:invoice, person: bt.person)
      stored_credit_card.person = bt.person
      stored_credit_card.save
      transaction = double(
        "braintree blue transaction",
        id: 99999,
        processor_response_code: "10",
        processor_response_text: "processor_response_text",
        processor_response_type: "processor_response_type",
        additional_processor_response: "additional_processor_response",
        network_response_text: "network_response_text",
        avs_error_response_code: "X",
        avs_postal_code_response_code: "Y",
        avs_street_address_response_code: "D",
        cvv_response_code: "M",
      )
      expect_any_instance_of(Braintree::ConfigGatewayService).to receive(:sale).and_return(braintree_successful_result)
      allow(braintree_successful_result).to receive(:transaction).and_return(transaction)
      transaction_id = "1askjdy2"
      allow(transaction).to receive(:id).and_return(transaction_id)
      allow(transaction).to receive(:currency_iso_code).and_return("USD")
      allow(transaction).to receive(:processor_response_code).and_return("1000")
      bt.process_braintree_blue_transaction(10.00)

      expect(bt.processor_response_text).to eq("processor_response_text")
      expect(bt.processor_response_type).to eq("processor_response_type")
      expect(bt.additional_processor_response).to eq("additional_processor_response")
      expect(bt.network_response_text).to eq("network_response_text")

      expect(bt.avs_response_code).to eq("X")
      expect(bt.avs_postal_response).to eq("Y")
      expect(bt.avs_street_address_response).to eq("D")
      expect(bt.cvv_response_code).to eq("M")

      expect(bt.avs_message).to eq("Exact match, 9-character ZIP")
      expect(bt.avs_postal_message).to eq("Exact match, 5-character numeric ZIP")
      expect(bt.avs_street_address_message).to eq("Exact match, 5-character numeric ZIP")
      expect(bt.cvv_message).to eq("CVV2/CVC2 Match")
    end
  end

  describe "#process_transaction_params" do
    it "correctly sets the attributes on the braintree_transaction" do
      stored_credit_card = create(:bb_stored_credit_card)
      bt = build(:braintree_transaction, stored_credit_card: stored_credit_card)
      braintree_successful_result = Braintree::SuccessfulResult.new
      bt.invoice = create(:invoice, person: bt.person)
      stored_credit_card.person = bt.person
      stored_credit_card.save
      transaction = double(
        "braintree blue transaction",
        id: 99999,
        processor_response_code: "10",
        processor_response_text: "processor_response_text",
        processor_response_type: "processor_response_type",
        additional_processor_response: "additional_processor_response",
        network_response_text: "network_response_text",
        avs_error_response_code: "X",
        avs_postal_code_response_code: "Y",
        avs_street_address_response_code: "D",
        cvv_response_code: "M",
      )
      expect_any_instance_of(Braintree::ConfigGatewayService).to receive(:sale).and_return(braintree_successful_result)
      allow(braintree_successful_result).to receive(:transaction).and_return(transaction)
      transaction_id = "1askjdy2"
      allow(transaction).to receive(:id).and_return(transaction_id)
      allow(transaction).to receive(:currency_iso_code).and_return("USD")
      allow(transaction).to receive(:processor_response_code).and_return("1000")
      bt.process_braintree_blue_transaction(10.00)

      expect(bt.processor_response_text).to eq("processor_response_text")
      expect(bt.processor_response_type).to eq("processor_response_type")
      expect(bt.additional_processor_response).to eq("additional_processor_response")
      expect(bt.network_response_text).to eq("network_response_text")

      expect(bt.avs_response_code).to eq("X")
      expect(bt.avs_postal_response).to eq("Y")
      expect(bt.avs_street_address_response).to eq("D")
      expect(bt.cvv_response_code).to eq("M")

      expect(bt.avs_message).to eq("Exact match, 9-character ZIP")
      expect(bt.avs_postal_message).to eq("Exact match, 5-character numeric ZIP")
      expect(bt.avs_street_address_message).to eq("Exact match, 5-character numeric ZIP")
      expect(bt.cvv_message).to eq("CVV2/CVC2 Match")
    end

    it "should notify airbrake if it errors" do
      stored_credit_card = FactoryBot.create(:bb_stored_credit_card)
      bt = build(:braintree_transaction, stored_credit_card: stored_credit_card)
      braintree_successful_result = Braintree::SuccessfulResult.new
      bt.invoice = create(:invoice, person: bt.person)

      expect(Airbrake).to receive(:notify).at_least(:once)
      allow(TransactionResponse).to receive(:process_transaction_params).and_raise("error here")
      bt.process_braintree_blue_transaction(10.00)
    end

  end

  describe "process transactions with merchants associated to persons corporate entity when money_in_bi_transfer is enabled" do
    let(:person) { create(:person, :with_lux_country) }
    let(:invoice) { create(:invoice, person: person) }
    let(:stored_credit_card) { create(:bb_stored_credit_card, person: person) }

    it "should process transactions to TC merchant when money_in_bi_transfer is disabled" do
      stub_braintree_blue_methods
      bt_txn = BraintreeTransaction.process_purchase_with_stored_card(invoice, stored_credit_card, payment_amount: 1000)
      tc_config = PayinProviderConfig.find_by(corporate_entity_id: 1, name: "Braintree", currency: person.currency)

      expect(bt_txn.payin_provider_config.corporate_entity.name).to eq("TuneCore US")
      expect(bt_txn.payin_provider_config).to eq(tc_config)
    end

    it "should process transactions to merchant associated with persons corporate_entity when money_in_bi_transfer is enabled" do
      stub_braintree_blue_methods
      allow(FeatureFlipper).to receive(:show_feature?).with(:money_in_bi_transfer, person).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:payment_splits_renewal, person).and_return(true)
      bt_txn = BraintreeTransaction.process_purchase_with_stored_card(invoice, stored_credit_card, payment_amount: 1000)
      bi_config = PayinProviderConfig.find_by(corporate_entity_id: 2, name: "Braintree", currency: person.currency)

      expect(bt_txn.payin_provider_config.corporate_entity.name).to eq("BI Luxembourg")
      expect(bt_txn.payin_provider_config).to eq(bi_config)
    end
  end

  # this just sets up some initial stubs for the mock
  # you can overwrite any of them by restubbing them in the test
  def default_stubs_for_mock_response
    allow(mock_gateway_response).to receive(:valid?).and_return(true)
    allow(mock_gateway_response).to receive(:response_code).and_return(100)
    allow(mock_gateway_response).to receive(:order_id).and_return(1)
    allow(mock_gateway_response).to receive(:action).and_return("sale")
    allow(mock_gateway_response).to receive(:amount).and_return(99.99)
    allow(mock_gateway_response).to receive(:customer_id).and_return(999)
    allow(mock_gateway_response).to receive(:transaction_id).and_return("1")
    allow(mock_gateway_response).to receive(:store?).and_return(false)
    allow(mock_gateway_response).to receive(:mark_as_preferred?).and_return(false)
    allow(mock_gateway_response).to receive(:avs_result).and_return({})
    allow(mock_gateway_response).to receive(:cvv_result).and_return({})
    allow(mock_gateway_response).to receive(:currency).and_return("USD")
    allow(mock_gateway_response).to receive(:processor_id).and_return("ccprocessora")
  end

  # all methods related to #process_purchase_with_stored_card
  def stub_braintree_blue_methods
    expect_any_instance_of(BraintreeTransaction).to receive(:process_braintree_blue_transaction).and_return(nil)
    expect_any_instance_of(BraintreeTransaction).to receive(:save).and_return(nil)
    expect_any_instance_of(BraintreeTransaction).to receive(:detect_duplicates).and_return(nil)
    expect_any_instance_of(BraintreeTransaction).to receive(:log_transaction).and_return(nil)
    expect_any_instance_of(BraintreeTransaction).to receive(:settle_invoice).and_return(nil)
  end

  def default_stubs_for_direct_post_response
    allow(mock_gateway_response).to receive(:params).and_return({ "transactionid" => "1", "response_code" => "100" })
    allow(mock_gateway_response).to receive(:avs_result).and_return({ "code" => "" })
    allow(mock_gateway_response).to receive(:cvv_result).and_return({ "code" => "" })
  end
end

describe BraintreeTransaction, "when processing a row from the csv" do
  before(:each) do
    @person = create(:person)

    mock_braintree_query
    @stored_credit_card = create(:stored_credit_card, person: @person)

    @invoice = create(:invoice, person: @person)
    @batch_transaction = create(:batch_transaction, :invoice => @invoice, :stored_credit_card => @stored_credit_card)
    # call the processor method with correct variables
    @transaction = BraintreeTransaction.process_batch_response_row('1234', 100, "Z", "", @batch_transaction.amount, @batch_transaction.id, @batch_transaction.invoice_id, @batch_transaction.invoice.person_id, 'ccprocessora')
  end

  it "should assign the correct variables to the correct fields" do
    expect(@transaction.country_website_id).to eq(1)
    expect(@transaction.currency).to eq("USD")
    expect(@transaction.person_id).to eq(@person.id)
    expect(@transaction.status).to eq("success")
    expect(@transaction.action).to eq("sale")
    expect(@transaction.ip_address).to eq("batch")
    expect(@transaction.stored_credit_card_id).to eq(@stored_credit_card.id)
    expect(@transaction.response_code).to eq("100")
    expect(@transaction.invoice_id).to eq(@invoice.id)
    expect(@transaction.transaction_id).to eq('1234')
  end

  describe "#braintree_payment_splits_config for renewal transactions" do
    let(:person) { create(:person, :with_lux_country) }
    let(:invoice) { create(:invoice, person: person) }
    let(:stored_credit_card) { create(:bb_stored_credit_card, person: person) }
    let(:bt) { create(:braintree_transaction, person: person, ip_address: "renewal") }
    options = { is_renewal: true }

    it "should return TC merchant if none of the payment splits feature flags are enabled" do
      payin_provider_config = bt.send :braintree_payment_splits_config, stored_credit_card.person, options
      expect(payin_provider_config.corporate_entity.name).to eq("TuneCore US")
    end

    it "should return TC merchant if money_in_bi_transfer is disabled and payment_splits_renewal is enabled" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:money_in_bi_transfer, person).and_return(false)
      allow(FeatureFlipper).to receive(:show_feature?).with(:payment_splits_renewal, person).and_return(true)

      payin_provider_config = bt.send :braintree_payment_splits_config, stored_credit_card.person, options
      expect(payin_provider_config.corporate_entity.name).to eq("TuneCore US")
    end

    it "should return TC merchant if money_in_bi_transfer is enabled and payment_splits_renewal is disabled" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:money_in_bi_transfer, person).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:payment_splits_renewal, person).and_return(false)
      payin_provider_config = bt.send :braintree_payment_splits_config, stored_credit_card.person, options
      expect(payin_provider_config.corporate_entity.name).to eq("TuneCore US")
    end

    it "should return BI merchant if both payment splits feature flags are enabled" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:money_in_bi_transfer, person).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:payment_splits_renewal, person).and_return(true)

      payin_provider_config = bt.send :braintree_payment_splits_config, stored_credit_card.person, options
      expect(payin_provider_config.corporate_entity.name).to eq("BI Luxembourg")
    end
  end

  describe "#braintree_payment_splits_config for non-renewal transactions" do
    let(:person) { create(:person, :with_lux_country) }
    let(:invoice) { create(:invoice, person: person) }
    let(:stored_credit_card) { create(:bb_stored_credit_card, person: person) }
    let(:bt) { create(:braintree_transaction, person: person) }
    options = { is_renewal: false }

    it "should return TC merchant if none of the payment splits feature flags are enabled" do
      payin_provider_config = bt.send :braintree_payment_splits_config, stored_credit_card.person, options
      expect(payin_provider_config.corporate_entity.name).to eq("TuneCore US")
    end

    it "should return TC merchant if money_in_bi_transfer is disabled and payment_splits_renewal is enabled" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:money_in_bi_transfer, person).and_return(false)
      allow(FeatureFlipper).to receive(:show_feature?).with(:payment_splits_renewal, person).and_return(true)

      payin_provider_config = bt.send :braintree_payment_splits_config, stored_credit_card.person, options
      expect(payin_provider_config.corporate_entity.name).to eq("TuneCore US")
    end

    it "should return TC merchant if money_in_bi_transfer is enabled and payment_splits_renewal is disabled" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:money_in_bi_transfer, person).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:payment_splits_renewal, person).and_return(false)
      payin_provider_config = bt.send :braintree_payment_splits_config, stored_credit_card.person, options
      expect(payin_provider_config.corporate_entity.name).to eq("BI Luxembourg")
    end

    it "should return BI merchant if both payment splits feature flags are enabled" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:money_in_bi_transfer, person).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:payment_splits_renewal, person).and_return(true)

      payin_provider_config = bt.send :braintree_payment_splits_config, stored_credit_card.person, options
      expect(payin_provider_config.corporate_entity.name).to eq("BI Luxembourg")
    end
  end
end
