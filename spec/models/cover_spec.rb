require "rails_helper"

describe Cover do
  let(:cover) { build_stubbed(:cover) }

  it 'should be able to set a custom render size' do
    cover.render_size = 300
    expect(cover.render_size).to eq(300)
  end

  it 'should have dimensions which are a square of the render size' do
    cover.render_size = 300
    expect(cover.dimensions).to eq('300x300')
  end

  it 'should be able to return <id>.png' do
    expect(cover.id_dot_png).to eq("#{cover.id}.png")
  end

  it 'should generate rendered_background_filename' do
    expect(cover.rendered_background_filename).to eq("#{Cover::BACKGROUND_IMAGES_DIR}/#{cover.id}.png")
  end

  it 'should generate rendered_title_filename' do
    expect(cover.rendered_title_filename).to eq("#{Cover::TITLE_IMAGES_DIR}/#{cover.id}.png")
  end

  it 'should generate rendered_artist_filename' do
    expect(cover.rendered_artist_filename).to eq("#{Cover::ARTIST_IMAGES_DIR}/#{cover.id}.png")
  end

  it 'should generate rendered_cover_filename' do
    expect(cover.rendered_cover_filename).to eq("#{Cover::COVER_IMAGES_DIR}/#{cover.render_size}_#{cover.id}.png")
  end

  it 'should generate rendered_cover_path' do
    expect(cover.local_cover_path).to eq("/#{Cover::COVER_ART_TMP_DIR}/covers/#{cover.render_size}_#{cover.id}.png")
  end

  it 'should be able to generate rendered_cover_descriptive_filename' do
    expect(cover.rendered_cover_descriptive_filename).to eq("#{Cover::COVER_IMAGES_DIR}/#{cover.id}None.png")
  end

  it "should return that background's url" do
    new_cover = create(:cover)
    expect(new_cover.background.url).to eq cover.background_url
  end

  describe "render_cover" do
    let(:artist_fixture) {Rails.root.join("spec", "fixtures", "artist.jpg")}
    let(:background_fixture) {Rails.root.join("spec", "fixtures", "background_cover_art.jpg")}
    let(:title_fixture) {Rails.root.join("spec", "fixtures", "title.jpg")}

    before(:each) do
      allow(cover).to receive(:rendered_artist_filename).and_return(artist_fixture)
      allow(cover).to receive(:rendered_background_filename).and_return(background_fixture)
      allow(cover).to receive(:rendered_title_filename).and_return(title_fixture)
    end

    it 'should create rendered_cover_filename' do
      cover.render_cover
      expect(File.exists?(cover.rendered_cover_filename)).to be true
    end
  end
end
