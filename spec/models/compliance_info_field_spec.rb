require "rails_helper"

describe ComplianceInfoField do
  it "doesn't allow adding fields that aren't on the whitelist" do
    person = create(:person)
    complaince_info_field = build(:compliance_info_field, person: person, field_name: "greggy", field_value: "greggy")
    expect(complaince_info_field.valid?).to be(false)
  end
end
