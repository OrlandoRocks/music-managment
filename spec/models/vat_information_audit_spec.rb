require "rails_helper"

RSpec.describe VatInformationAudit, type: :model do
  let(:person) do
    create(
      :person,
      country: "BE",
      corporate_entity: CorporateEntity.find_by(name: CorporateEntity::BI_LUXEMBOURG_NAME)
    )
  end

  describe "vat information create" do
    it "should not audit if the vat_information is created" do
      person.build_vat_information(vat_registration_number: "0897123456", company_name: "test").save

      expect(person.vat_information_audits).to be_blank
    end
  end

  describe "vat information update" do
    before(:each) do
      person.build_vat_information(vat_registration_number: "0897123456", company_name: "test").save
    end

    it "should audit when invalidate" do
      vat_info = person.vat_information
      old_values = vat_info.attributes.slice("vat_registration_status", "vat_registration_number", "company_name")

      vat_info.invalidate!

      audit = person.vat_information_audits.last
      attributes = ["vat_registration_status", "vat_registration_number", "company_name"]
      expect(audit.attributes.slice(*attributes)).to eq old_values
    end

    it "should audit when vat reg number gets updated" do
      vat_info = person.vat_information
      old_vat = vat_info.vat_registration_number

      vat_info.update(vat_registration_number: "0812345678")

      audit = person.vat_information_audits.last
      expect(audit.vat_registration_number).to eq old_vat
    end

    it "should add audits on vat information delete" do
      person.vat_information.destroy
      expect(person.vat_information_audits).to be_present
    end
  end

  describe "user country change" do
    before(:each) do
      person.build_vat_information(vat_registration_number: "0897123456", company_name: "test").save
    end

    it "should audit on country change" do
      vat_info = person.vat_information

      person.update(country: "LU")
      person.vat_information.update(
        vat_registration_number: "17389679",
        company_name: "test company"
      )

      expect(vat_info.reload).to be_present
      expect(person.vat_information_audits.count).to eq 2
      expect(person.vat_information_audits.where(vat_registration_number: "0897123456")).to be_present
    end
  end
end
