require 'rails_helper'

describe PayoutProvider do
  let(:person) { create(:person) }
  let(:unapproved_payout_provider) { create(:payout_provider, :skip_validation, tunecore_status: 'active', provider_status: 'pending') }
  let(:approved_payout_provider) { create(:payout_provider, :skip_validation, provider_status: 'approved', tunecore_status: 'active') }
  let(:onboarding_payout_provider) { create(:payout_provider, :skip_validation, provider_status: 'onboarding', tunecore_status: 'active') }
  let(:inactive_payout_provider) { create(:payout_provider, tunecore_status: 'disabled', provider_status: 'approved', active_provider: false) }
  let(:declined_payout_provider) { create(:payout_provider, provider_status: 'declined', tunecore_status: 'active', active_provider: false) }

  describe "validations" do
    context "when an active payout provider already exists for a user" do
      it "disallows multiple active providers" do
        person.payout_providers << declined_payout_provider
        person.payout_providers <<  approved_payout_provider

        invalid_provider = PayoutProvider.new(person_id: person.id, provider_status: 'approved', tunecore_status: 'active', active_provider: true)
        expect(invalid_provider.valid?).to eq(false)
        expect(invalid_provider.errors.full_messages.size).to eq(1)
        expect(invalid_provider.errors.full_messages.first).to eq("Another active provider already exists")

        declined_payout_provider.update(active_provider: true)
        expect(declined_payout_provider.errors.full_messages.size).to eq(1)
        expect(declined_payout_provider.errors.full_messages.first).to eq("Another active provider already exists")
      end
    end

    context "when an inactive payout provider already exists for a user" do
      it "does save" do
        person.payout_providers << declined_payout_provider
        invalid_payout_provider = PayoutProvider.new(tunecore_status: "active", provider_status: "onboarding", person_id: person.id, active_provider: true)
        expect(invalid_payout_provider.save).to be_truthy
      end
    end


    it "successfully saves payout provider with a valid provider status" do
      payout_provider = PayoutProvider.new(person_id: person.id, tunecore_status: "active", provider_status: "onboarding", active_provider: true)

      expect(payout_provider.save).to be_truthy
    end

    it "does not save with an invalid payout provider" do
      payout_provider = PayoutProvider.new(person_id: person.id, tunecore_status: "active")

      expect(payout_provider.save).to be_falsey
    end
  end

  context "with the name 'Payoneer' and tunecore_status as 'active'" do
    let(:provider_id) { "12345" }
    let!(:payout_providers) do
      [
        create(
          :payout_provider,
          :approved,
          :skip_validation,
          person: person,
          provider_identifier: provider_id,
          active_provider: true
        ),
        create(
          :payout_provider,
          :declined,
          :skip_validation,
          person: person,
          provider_identifier: provider_id,
          active_provider: true
        ),
        create(
          :payout_provider,
          :approved,
          :skip_validation,
          person: create(:person),
          provider_identifier: provider_id,
          active_provider: true
        ),
        create(
          :payout_provider,
          :approved,
          :skip_validation,
          person: person,
          provider_identifier: provider_id,
          active_provider: false
        ),
        create(
          :payout_provider,
          :approved,
          :skip_validation,
          person: person,
          provider_identifier: "98765",
          active_provider: true
        )
      ]
    end

    before(:all) do
      described_class.destroy_all
    end

    describe ".active_by" do
      it "returns active payout providers with a given provider ID" do
        expect(described_class.active_by(provider_identifier: provider_id))
          .to contain_exactly(*payout_providers[0..2])
      end
    end

    describe ".active_providers_for_people_by" do
      it "returns providers for people with active providers using the same provider identifier" do
        expect(described_class.active_providers_for_people_by(provider_identifier: provider_id))
          .to contain_exactly(payout_providers[0], payout_providers[2])
      end
    end

    describe ".count_of_active_providers_for_people_by :provider_identifier" do
      it "returns the number of providers for people with active providers using the same provider identifier" do
        expect(
          described_class.count_of_active_providers_for_people_by(
            provider_identifier: provider_id
          )
        ).to eq(2)
      end
    end
  end

  describe "#can_use_payoneer?" do
    it "only returns true if both active and approved are true" do
      expect(approved_payout_provider.can_use_payoneer?).to be true
      expect(unapproved_payout_provider.can_use_payoneer?).to be false
      expect(inactive_payout_provider.can_use_payoneer?).to be false
    end
  end

  describe "provider status #onboarding?, #approved?, #declined?" do
    it "is true if provider_status matches" do
      expect(approved_payout_provider.approved?).to eq true
      expect(onboarding_payout_provider.onboarding?).to eq true
      expect(declined_payout_provider.declined?).to eq true
    end

    it "is false if provider_status doesn't match" do
      expect(onboarding_payout_provider.approved?).to eq false
      expect(approved_payout_provider.onboarding?).to eq false
      expect(approved_payout_provider.declined?).to eq false
    end
  end

  describe "tunecore status #active, #disabled" do
    it "is true when tunecore status matches" do
      active_payout_provider   = create(:payout_provider, :skip_validation, tunecore_status: "active")
      disabled_payout_provider = create(:payout_provider, :skip_validation, tunecore_status: "disabled")
      expect(active_payout_provider.active?).to eq true
      expect(disabled_payout_provider.disabled?).to eq true
    end

    it "is false when tunecore status doesn't match" do
      active_payout_provider   = create(:payout_provider, :skip_validation, tunecore_status: "active")
      disabled_payout_provider = create(:payout_provider, :skip_validation, tunecore_status: "disabled")
      expect(active_payout_provider.disabled?).to eq false
      expect(disabled_payout_provider.active?).to eq false
    end
  end

  describe "#other_linked_tunecore_accounts" do
    it "returns all the other linked tunecore accounts for the same provider" do
      create(:payout_provider,
        :skip_validation,
        provider_status: PayoutProvider::APPROVED,
        tunecore_status: PayoutProvider::ACTIVE,
        provider_identifier: "123456"
      )
      create(:payout_provider,
        :skip_validation,
        provider_status: PayoutProvider::APPROVED,
        tunecore_status: PayoutProvider::ACTIVE,
        provider_identifier: "123456"
      )
      provider = create(:payout_provider,
        :skip_validation,
        provider_status: PayoutProvider::APPROVED,
        tunecore_status: PayoutProvider::ACTIVE,
        provider_identifier: "123456"
      )

      expect(provider.other_linked_tunecore_accounts.length).to eq(2)
    end
  end

  describe "#has_made_successful_transfer?" do
    it "should return true if the person had approved transfers" do
      payout_provider = create(:payout_provider)
      create(:approved_payout_transfer, payout_provider: payout_provider)
      expect(payout_provider.has_made_successful_transfer?).to be_truthy
    end

    it "should return false if the person does not have approved transfers" do
      payout_provider = create(:payout_provider)
      create(:submitted_payout_transfer, payout_provider: payout_provider)
      expect(payout_provider.has_made_successful_transfer?).to be_falsey
    end
  end

  describe "#disassociate_account" do
    it "successfully disassociates a payoneer account from a tunecore account" do
      expect(approved_payout_provider.provider_status).to eq(PayoutProvider::APPROVED)
      expect(approved_payout_provider.active_provider).to be(true)
      approved_payout_provider.disassociate_account
      expect(approved_payout_provider.reload.provider_status).to eq(PayoutProvider::DECLINED)
      expect(approved_payout_provider.reload.active_provider).to be(false)
    end
  end
end
