require "rails_helper"

describe Album::AfterCommitCallbacks do

  before(:each) do
    allow_any_instance_of(Believe::ApiClient).to receive(:post).and_return(true)
  end

  describe ".after_commit" do
    context "finalized album needs review" do
      before(:each) do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(false)
        @album = create(:album, :finalized, legal_review_state: "NEEDS REVIEW")
      end
    end
  end

  describe "caching calls" do
    before(:each) do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
    end

    context "an album is not finalized" do
      it "should not make any cache calls" do
        album = create(:album)
        expect(CrtCacheWorker).not_to receive(:perform_async)
        album.save
      end
    end

    context "an album is finalized" do
      it "should write to cache" do
        album = create(:album)
        album.finalized_at = Time.now
        allow(CrtCacheWorker).to receive(:perform_async).with(album.id)
        expect(CrtCacheWorker).to receive(:perform_async).with(album.id)
        album.save
      end
    end
  end

  describe ".process_renewal_status" do
    before(:each) do
      allow(FeatureFlipper).to receive(:show_feature?)
        .with(:crt_cache, nil)
        .and_return(false)

      @album = create(:album, :finalized, legal_review_state: "NEEDS REVIEW")
    end

    it "should call cancel renewal when release rejected" do
      allow(@album).to receive(:cancel_renewal).and_return(:true)
      expect(@album).to receive(:cancel_renewal)
      @album.legal_review_state = "REJECTED"
      @album.save
    end

    it "should call reset renewal when release denied=>approval" do
      allow(@album).to receive(:cancel_renewal).and_return(:true)
      @album.legal_review_state = "REJECTED"
      @album.save
      allow(@album).to receive(:reset_renewal).and_return(:true)
      expect(@album).to receive(:reset_renewal)
      @album.legal_review_state = "APPROVED"
      @album.save
    end
  end

  describe '.create_song_fingerprints' do
    context 'for finalized album' do
      let(:album) {
        create(
          :album,
          :finalized,
          :with_artwork,
          :with_salepoints,
          :with_uploaded_songs,
          :with_songwriter,
          number_of_songs: 2
        )
      }

      let(:person) { create(:person) }

      it 'should create scrapi jobs for songs' do
        ENV["SCRAPI_JOB_ENABLED"] = 'true'
        # Change to ensure create_song_fingerprints does it's job
        Timecop.freeze

        album.update_columns(finalized_at: Time.now - 1.month)

        album.finalized_at = Time.now
        song_ids = album.songs.collect(&:id)
        song_ids.each do |sid|
          expect(Scrapi::CreateJobWorker).to receive(:perform_async).with(sid)
        end
        album.save!

        Timecop.return
      end

      it "should not create scrapi jobs if album has already been finalized " do
        expect(Scrapi::CreateJobWorker).to_not receive(:perform_async)
        album.save!
      end

      it "should create scrapi jobs if any album songs asset changed after album unfinalized " do
        ENV["SCRAPI_JOB_ENABLED"] = 'true'
        Timecop.freeze
        old_finalized_at = album.finalized_at
        album.update_columns(finalized_at: nil)
        album.notes.create(subject: "Unfinalized", note: "Unfinalized", note_created_by: person, created_at: old_finalized_at + 1.day)
        expect(album.songs.count).to eq(2)
        song = create(:song, :with_upload, album: album, s3_asset: create(:s3_asset, created_at: Time.now + 2.day, updated_at: Time.now+2.day))
        album.songs << song
        album.reload
        album.finalized_at = Time.now + 3.day
        expect(Scrapi::CreateJobWorker).to receive(:perform_async).with(song.id)
        album.save!
        expect(album.songs.count).to eq(3)
        Timecop.return
      end
    end

    context 'for non finaliezd album' do
      let(:album) { create(:album, :with_songs, number_of_songs: 2) }

      it 'should not create scrapi jobs' do
        expect(Scrapi::CreateJobWorker).not_to receive(:perform_async)
        album.save!
      end
    end
  end
end
