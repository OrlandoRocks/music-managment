require "rails_helper"

require "rails_helper"

# This class is used to test methods that have to be overriden from DataReport.
class TestReport < DataReport
  
  def record_day(date)
     build_records(:day, date)
  end

  def build_records(resolution, date)
    create_record(resolution, date, [[15, 'value 1'], [30, 'value 2'], [45, 'value 3']])
  end
  
  def data(days)
    super(["value 1", 'value 2', 'value 3'], days)
  end

end


describe DataRow, "when being created" do
  
  it "should raise an error if it created with something other than an array" do
    expect{ DataRow.new("string!")}.to raise_error(RuntimeError, /You must pass in an array/)
    expect{ DataRow.new(42)}.to raise_error(RuntimeError, /You must pass in an array/)
  end
  
  it "should raise an error if it is created with an empty array" do
    expect{ DataRow.new([])}.to raise_error(RuntimeError, /Your array cannot be empty/)
  end
  
  it "should raise an error if it is created with an array holding values other than a data record" do
    expect{ DataRow.new(["string!"])}.to raise_error(RuntimeError, /Your array must contain at least one data record/)
  end
  
  it "should not raise an error if it is created with an array that has a data record" do
    expect{ DataRow.new([TCFactory.build(:data_record)]) }.not_to raise_error
  end
  
  it "should raise an error if all of the resolution identifiers are not the same (we need to make sure we're always comparing the same time frame)" do 
    record_array = []
    record_array << TCFactory.build(:data_record, {:description => "value 1", :resolution_identifier => :M3_2009})
    record_array << TCFactory.build(:data_record, {:description => "value 2", :resolution_identifier => :M4_2008})
    
    expect{ DataRow.new(record_array)}.to raise_error(RuntimeError, /All resolution identifiers must be the same/)
  end
  
  it "should not raise an error if all of the resolution identifiers are the same" do
    record_array = []
    record_array << TCFactory.build(:data_record, {:description => 'value 1', :resolution_identifier => :M3_2009})
    record_array << TCFactory.build(:data_record, {:description => 'value 2', :resolution_identifier => :M3_2009})
    
    lambda{ expect(DataRow.new(record_array)).not_to raise_error}
  end
  
end

describe DataRow, "when calling .get_item('key')" do
  before(:each) do
    record_array = []
    record_array << TCFactory.build(:data_record, {:description => 'key 1', :measure => 1.0})
    record_array << TCFactory.build(:data_record, {:description => 'key 2', :measure => 6095})
    @data_row = DataRow.new(record_array)
  end
  
  it "should return the value of 'key 1'" do 
    expect(@data_row.get_item('key 1')).to eq(1.0)
  end
  
  it "should return the value of 'key 2'" do
    expect(@data_row.get_item('key 2')).to eq(6095)
  end
  
  it "should return nil for non-existant key" do
    expect(@data_row.get_item('not a key')).to be_nil
  end
end


describe DataRow, "when calling ['key']" do 
  before(:each) do
    record_array = []
    record_array << TCFactory.build(:data_record, {:description => 'key 1', :measure => 1.0})
    record_array << TCFactory.build(:data_record, {:description => 'key 2', :measure => 6095})
    @data_row = DataRow.new(record_array)
  end
  
  it "should return the value of ['key 1']" do
    expect(@data_row['key 1']).to eq(1.0)
  end
  
  it "should return the value of ['key 2']" do
    expect(@data_row['key 2']).to eq(6095)
  end
  
  it "should return nil for non-existant key" do
    expect(@data_row['not a key']).to be_nil
  end
end

describe DataRow, "when calling each_pair" do
  before(:each) do
    record_array = []
    record_array << TCFactory.build(:data_record, {:description => 'key 1', :measure => 1.0})
    record_array << TCFactory.build(:data_record, {:description => 'key 2', :measure => 6095})
    @data_row = DataRow.new(record_array)
  end
  
  it "should return the key/value pairs in the correct order" do
    test_string = ""
    @data_row.each_pair do |key, value|
      test_string += "key:#{key},value:#{value}|"
    end
    expect(test_string).to eq("key:key 1,value:1.0|key:key 2,value:6095.0|")
  end
end
