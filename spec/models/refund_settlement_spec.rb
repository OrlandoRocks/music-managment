require "rails_helper"

RSpec.describe RefundSettlement, type: :model do
  context "Associations" do
    it { should belong_to(:refund) }
    it { should belong_to(:source) }
  end

  context "Validations" do
    it "should be invalid if refund or source is missing" do
      settlement = RefundSettlement.new
      settlement.valid?

      expect(settlement.errors.keys).to eq %i[refund source_id source_type]
    end
  end
end
