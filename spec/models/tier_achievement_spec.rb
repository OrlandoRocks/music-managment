require 'rails_helper'

RSpec.describe TierAchievement, type: :model do
  describe "relationships" do
    subject { build(:tier_achievement) }

    it { should belong_to(:tier) }
    it { should belong_to(:achievement) }
  end

  describe "validations" do
    let(:subject) { build(:tier_achievement) }

    it { should validate_uniqueness_of(:tier_id).scoped_to(:achievement_id) }
  end
end
