require "rails_helper"

describe DistributionSong do
  before :each do
    @album          = TCFactory.build_album
    @petri_bundle   = TCFactory.build_petri_bundle(album: @album)
    @distro         = FactoryBot.create(:distribution, :with_salepoint)
    @salepoint_song = TCFactory.build_salepoint_song
    @distro_song    = TCFactory.build_distribution_song(distribution: @distro, salepoint_song: @salepoint_song)
  end

  describe "#has_been_delivered?" do
    it "should return false if the distro_song was never delivered" do
      expect(@distro_song.has_been_delivered?).to eq(false)
    end
  end

  describe "#store" do
    it "should return the store for salepoint" do
      expect(@distro_song.store).to eq(@salepoint_song.salepoint.store)
    end
  end

  describe "#taken_down?" do
    it "is false if takedown_at is null" do
      expect(@distro_song.taken_down?).to eq false
    end

    it "is true if takedown_at is not null" do
      @salepoint_song.takedown_at = Time.now
      expect(@distro_song.taken_down?).to eq true
    end
  end
end
