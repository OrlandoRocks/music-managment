require "rails_helper"

describe Album, "implementing an interface to PETRI" do
  before(:each) do
    @distro = Album.new
  end

  describe "#parental_advisory" do
    it "should respond to parental_advisory" do
      expect(@distro.respond_to?(:parental_advisory)).to eq(true)
    end
  end

  it_should_behave_like "PETRI interface"  
end
