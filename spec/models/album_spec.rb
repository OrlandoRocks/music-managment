require "rails_helper"
include ActiveSupport::Testing::TimeHelpers

describe Album do
  describe "associations" do
    subject { build :album }
    it { should have_many(:songs).inverse_of(:album) }
    it { should have_many(:royalty_splits).inverse_of(:albums) }
  end
end

describe Album, "scopes" do
  context ".for_takedown" do
    it "returns albums in the Amazon On Demand with a takedown_at date within a given date range" do
      finish = Date.today
      start = finish - 14.days
      album = create(:album)
      album_outside_date_range = create(:album)
      salepoint  = create(:salepoint, :aod_store, salepointable_id: album.id, takedown_at: finish - 5.days, salepointable_type: 'Album')
      salepoint_outside_date_range = create(:salepoint, :aod_store, salepointable_id: album_outside_date_range.id, takedown_at: finish - 1.month, salepointable_type: 'Album')
      tunecore_upc = create(:tunecore_upc, upcable_type: 'Album', upcable_id: album.id)
      optional_upc = create(:optional_upc, upcable_type: 'Album', upcable_id: album.id)

      results = Album.for_amazon_takedown(start, finish)

      expect(results).to include(album)
      expect(results).to_not include(album_outside_date_range)
    end
  end

  describe ".crt_list_view_query" do
    before { travel_to Time.local(2020) }
    after { travel_back }

    it "should get albums that need review and are within the last year" do
      album_finalized_one_week_ago = create(:album, {
        finalized_at: 1.week.ago,
        legal_review_state: "NEEDS REVIEW"
      })
      album_finalized_two_years_ago = create(:album, {
        finalized_at: 2.years.ago,
        legal_review_state: "NEEDS REVIEW"
      })
      album_finalized_today = create(:album, {
        finalized_at: Time.current - 1.minute,
        legal_review_state: "NEEDS REVIEW"
      })

      results = Album.crt_list_view_query("finalized_at", "asc", "NEEDS REVIEW")

      expect(results).to include(album_finalized_one_week_ago)
      expect(results).to include(album_finalized_today)
      expect(results).not_to include(album_finalized_two_years_ago)
    end
  end

  describe ".search_by_album_name" do
    it "returns albums with names matching the search term" do
      album = create(:album, :finalized, name: "My Beautiful Dark Twisted Fantasy")

      results = Album.search_by_album_name("twisted")

      expect(results).to include(album)
    end
  end

  describe ".search_by_artist_name" do
    it "returns albums with artists that have names matching the search term" do
      artist = create(:artist, name: "Yeezus Christ")
      album = create(:album, :finalized, {
        legal_review_state: "NEEDS REVIEW"
      })
      creative = create(:creative, artist: artist, creativeable: album)
      album.creatives << creative

      results = Album.search_by_artist_name("christ")

      expect(results).to include(album)
    end
  end

  describe ".search_by_person_name" do
    it "returns albums with people that have names matching the search term" do
      person = create(:person, name: "Yeezus Christ")
      album = create(:album, :finalized, person: person)

      results = Album.search_by_person_name("yeez")

      expect(results).to include(album)
    end
  end

  describe ".search_by_country_website_id" do
    it "returns albums where the person country_website_id matches the id passed in" do
      country_website = CountryWebsite.first
      person = create(:person)
      person.update(country: country_website.country, country_website_id: country_website.id)
      album = create(:album, person: person)

      results = Album.search_by_country_website_id(country_website.id)

      expect(results).to include(album)
    end
  end

  describe ".search_by_paid_at_date" do
    it "returns albums that have purchases between the date ranges" do
      start_date = Time.current - 10.days
      end_date = Time.current

      album = create(:album, :with_one_year_renewal)
      album.purchases.first.update(paid_at: Time.current - 5.days)

      album_without_purchase_in_range = create(:album, :with_one_year_renewal)
      album_without_purchase_in_range.purchases.first.update(paid_at: Time.current - 11.days)

      results = Album.search_by_paid_at_date(start_date: start_date, end_date: end_date)

      expect(results).to include(album)
      expect(results).not_to include(album_without_purchase_in_range)
    end
  end

  describe ".search_by_last_review_date" do
    it "returns albums where the last review date is between the date ranges" do
      crt_agent = create(:person, :admin, :with_crt_manager_role)
      start_date = Time.current - 10.days
      end_date = Time.current

      allow(PetriBundle).to receive_message_chain(:for, :distributions) { [] }

      review_audit = create(:review_audit, {
        person: crt_agent,
        created_at: Time.current - 5.days,
        event: "APPROVED",
      })
      album = create(:album, :approved, review_audits: [review_audit])

      review_audit2 = create(:review_audit, {
        person: crt_agent,
        created_at: Time.current - 11.days,
        event: "APPROVED",
      })
      album_without_ra_in_range = create(:album, :approved, review_audits: [review_audit2])

      results = Album
        .search_by_last_review_date(start_date: start_date, end_date: end_date)

      expect(results).to include(album)
      expect(results).not_to include(album_without_ra_in_range)
    end
  end

  describe ".search_by_release_date" do
    it "returns albums were releases within the date ranges" do
      start_date = Time.current - 10.days
      end_date = Time.current

      album = create(:album, golive_date: Time.current - 5.days)

      album_created_out_of_range = create(:album, golive_date: Time.current - 11.days)

      results = Album.search_by_release_date(start_date: start_date, end_date: end_date)

      expect(results).to include(album)
      expect(results).not_to include(album_created_out_of_range)
    end
  end
end

describe Album, "#dismissed?" do
  it "calls the aws wrapper with the bigbox domain and upc" do
    album = create(:album)
    create(:tunecore_upc, upcable: album)

    expect(AwsWrapper::SimpleDb).to receive(:get_attributes).with(
      domain: BIGBOX_ASSETS_SDB_DOMAIN,
      item: album.upc.to_s
    ).and_return({})

    album.dismissed?
  end

  context "when there are no attributes for the upc" do
    it "returns false" do
      album = create(:album)
      create(:tunecore_upc, upcable: album)

      allow(AwsWrapper::SimpleDb).to receive(:get_attributes).with(
        domain: BIGBOX_ASSETS_SDB_DOMAIN,
        item: album.upc.to_s
      ).and_return({})

      expect(album.dismissed?).to eq(false)
    end
  end

  context "when there is no 'state' attribute with value 'dismissed' for the upc" do
    it "returns false" do
      album = create(:album)
      create(:tunecore_upc, upcable: album)

      allow(AwsWrapper::SimpleDb).to receive(:get_attributes).with(
        domain: BIGBOX_ASSETS_SDB_DOMAIN,
        item: album.upc.to_s
      ).and_return("state" => ["not_dismissed"])

      expect(album.dismissed?).to eq(false)
    end
  end

  context "when there is a 'state' attribute with value 'dismissed' for the upc" do
    it "returns true" do
      album = create(:album)
      create(:tunecore_upc, upcable: album)

      allow(AwsWrapper::SimpleDb).to receive(:get_attributes).with(
        domain: BIGBOX_ASSETS_SDB_DOMAIN,
        item: album.upc.to_s
      ).and_return("state" => ["dismissed"])

      expect(album.dismissed?).to eq(true)
    end
  end
end

describe Album do
  describe 'creatives=' do
    let(:album) { create(:album) }
    context 'no main artist' do
      it 'should add error' do
        album.creatives = []

        expect(album.errors.full_messages).to eq(["Creatives You must have a Main Artist!"])
        expect(album.instance_variable_get(:@creatives_missing)).to eq(true)
      end
    end

    context 'no name for artists' do
      it 'should add error and assign creatives_missing' do
        album.creatives = [{role: 'primary_artist', name: '', }]

        expect(album.errors.full_messages).to eq(["Creatives You must have a Main Artist!"])
        expect(album.instance_variable_get(:@creatives_missing)).to eq(true)
      end
    end
  end

  context "album name" do
    context "album create" do
      context "allow_different_format is true" do
        it 'does not titleize' do
          album = build_stubbed(:album, name: "whatever whatever feat. whoever", allow_different_format: true)
          expect(album.name).to eq("whatever whatever feat. whoever")
        end
      end

      context "allow different format is false" do
        context "with (feat). in the title" do
          it 'does save (feat). in the name' do
            album = build_stubbed(:album, name: "My Song (feat. Sophie)")
            expect(album.name).to eq("My Song (feat. Sophie)")
          end
        end
      end
    end
  end
  context 'during creation' do
    it 'titelizes names for albums without non-formatting language codes' do
      album = create(:album, name: 'tic tac toe', language_code: 'en')
      expect(album.name).to eq('Tic Tac Toe')
    end

    it 'does not titlize names for albums with non-formatting language codes' do
      album = create(:album, name: 'tic tac toe', language_code: 'fr')
      expect(album.name).to eq('tic tac toe')
    end

    it "strips spaces when names are passed in to #name" do
      album = create(:album, name: "Bob ")
      expect(album.name).to eq('Bob')
    end

    context "when there is only a primary artist" do
      let(:album) { create(:album) }

      it "creates a creative that maps to the person for the primary artist" do
        creative = album.creatives.first
        expect(creative.person_id).to eq(album.person_id)
      end
    end
  end

  describe "#convert_to_single?" do
    it "should return true for an album with one song" do
      album = create(:album, :with_uploaded_songs, number_of_songs: 1)
      expect(album.convert_to_single?).to eq(true)
    end

    it "should return false for an album with multiple songs" do
      album = create(:album, :with_uploaded_songs, number_of_songs: 2)
      expect(album.convert_to_single?).to eq(false)
    end

    it "should return false for an album of type single" do
      album = create(:album, album_type: "Single")
      expect(album.convert_to_single?).to eq(false)
    end

    it "should return false for an album of type ringtone" do
      album = create(:album, album_type: "Ringtone")
      expect(album.convert_to_single?).to eq(false)
    end
  end

  describe "#get_finalized_store_ids" do
    before(:each) do
      @album = create(:album, :paid)
    end

    it "should return only finalized sale_points" do
      sp_store_1 = create(:salepointable_store, store: Store.find_by(short_name: "Akazoo"))
      sp_store_2 = create(:salepointable_store, store: Store.find_by(short_name: "KKBox"))
      finalized_sp = create(:salepoint, salepointable: @album, store: sp_store_1.store, finalized_at: Time.now)
      unfinalized_sp = create(:salepoint, salepointable: @album, store: sp_store_2.store, finalized_at: nil)
      @album.salepoints = [finalized_sp, unfinalized_sp]
      expect(@album.get_finalized_store_ids).to eq([finalized_sp.store_id])
    end
    it "should return an empty_array on no matches" do
      expect(@album.get_finalized_store_ids).to eq([])
    end
  end
  describe "#sku" do
    it 'should return a properly formatted 11 digit SKU based on the album.id' do
      album = build_stubbed(:album)
      expect(album).to receive(:id).once.and_return(88888)
      expect(album.sku).to eq('A0000088888')
    end
  end

  describe "#start_review" do
    before(:each) do
      @album = create(:album, :with_uploaded_songs, number_of_songs: 2)
      @crt_reviewer = Person.find_by_name!("Super Admin")
    end

    context "when it did not generate a review within the last 30 minutes" do
      it "creates a new review audit with STARTED REVIEW event" do
        @album.start_review(person_id: @crt_reviewer.id)
        expect(@album.review_audits.count).to eq(1)
        expect(@album.review_audits.last.event).to eq("STARTED REVIEW")
      end
    end

    context "when it did generate a review within the last 30 minutes" do
      it "should not create new review audit" do
        @album.start_review(person_id: @crt_reviewer.id)
        @album.start_review(person_id: @crt_reviewer.id)
        expect(@album.review_audits.count).to_not eq(2)
      end
    end
  end

  describe "#tunecore_upc" do
    it "should assign a tunecore upc automatically" do
      @album = create(:album)
      expect(@album.tunecore_upc).not_to be_blank
    end
  end

  describe "#optional_upc_number" do
    it "validates the upc" do
      @album = build(:album, optional_upc_number: "adfssad")
      @album.valid?
      expect(@album.errors.messages[:optional_upc_number].size).to be > 0
    end
  end

  describe "#add_to_cart_pre_proc" do
    before(:each) do
      @album = build(:album)
    end

    it "should try to change single track albums" do
      expect(@album).to receive :change_single_track_album_to_single
      @album.add_to_cart_pre_proc
    end

    it "should try to add related items to cart" do
      expect(@album).to receive :add_related_to_cart
      @album.add_to_cart_pre_proc
    end
  end

  describe "#distribute!" do
    before(:each) do
      @album    = create(:album)
      @song1    = create(:song, album: @album)
      @upload1  = create(:upload, song: @song1)
      @album.songs << @song1
    end

    it "should not recreate ircs or upcs if called more than once" do
      @album.distribute!

      song_isrcs = []

      # Save original isrcs
      @album.songs.each do |song|
        song_isrcs << song.tunecore_isrc
      end

      # Save original upc
      album_upc = @album.tunecore_upc
      @album.distribute!

      #Ensure they haven't changed
      @album.songs.each_with_index do |song,i|
        expect(song_isrcs[i]).to eq(song.tunecore_isrc)
      end

      expect(@album.tunecore_upc).to eq(album_upc)
    end

    it "should remove booklets which are not finalized" do
      booklet = Booklet.new(:album => @album)
      booklet.save!

      expect { @album.distribute! }.to change(Booklet, :count).by(-1)

      expect(@album.booklet.blank?).to eq(true)
    end

    it "should not remove booklets which are finalized" do
      booklet = Booklet.new(:album => @album, :paid_at => Time.now)
      booklet.save!

      expect { @album.distribute! }.to_not change(Booklet, :count)

      expect(@album.booklet.blank?).to eq(false)
    end

    it "should set the payment_applied flag for all saleoints that have associated inventory usages" do
      purchase = create(
        :purchase,
        product: Product.find_by(country_website: @album.person.country_website),
        person: @album.person,
        related: @album
      )
      inventory = create(:salepoint_inventory, person: @album.person, purchase: purchase)
      Store.where("is_active = true and name not like '%iTunes%' and abbrev not in ('aod_us', 'az')").all.each do |s|
        create(:salepointable_store, store: s)
        sp = create(:salepoint, store: s, salepointable: @album, has_rights_assignment: true)
        create(:inventory_usage, inventory: inventory, related: sp)
      end
      create(:petri_bundle, album_id: @album.id)
      @album.distribute!
    end

    it "should reset the variable price for itunes if the salepoint_preorder_data for itunes exists and the preorder was not paid for" do
      preorder_purchase = create(:preorder_purchase, album: @album)
      store = Store.find_by(abbrev: "ww")
      create(:salepointable_store, store: store)
      salepoint = create(:salepoint, salepointable: @album, store: store, variable_price: store.variable_prices.first)
      create(:salepoint_preorder_data, salepoint: salepoint, preorder_purchase: preorder_purchase)

      allow(@album).to receive(:itunes_ww_salepoint).and_return(salepoint)
      expect(salepoint.salepoint_preorder_data).to receive(:set_track_price).with(0.99).and_return(true)
      expect(salepoint).to receive(:set_variable_price).with(9.99).and_return(true)

      @album.distribute!
    end

    it "should not reset the varaible price for itunes if the spd for itunes was paid for" do
      preorder_purchase = create(:preorder_purchase, album: @album, paid_at: Time.now)
      store = Store.find_by(abbrev: "ww")
      create(:salepointable_store, store: store)
      salepoint = create(:salepoint, salepointable: @album, store: store, variable_price: store.variable_prices.first)
      create(:salepoint_preorder_data, salepoint: salepoint, preorder_purchase: preorder_purchase)

      allow(@album).to receive(:itunes_ww_salepoint).and_return(salepoint)
      expect(salepoint.salepoint_preorder_data).not_to receive(:set_track_price)
      expect(salepoint).not_to receive(:set_variable_price)

      salepoint_ids =  @album.reload.salepoints.pluck(:id)
      @album.distribute!
    end
  end

  describe "#for_takedown_by_day" do
    context "yearly album is canceled" do
      it "should not takedown album until expired ( no grace period )" do
        person = create(:person)
        album = create(:album, :purchaseable, :with_one_year_renewal, person: person)
        renewal = person.renewals.first
        renewal.update(takedown_at: nil, canceled_at: Time.now)
        renewal_history = renewal.renewal_history.first

        expire_date = Date.today
        renewal_history.expires_at = expire_date
        renewal_history.starts_at  = expire_date - 1.year
        renewal_history.save!

        renewal_history.update(expires_at: expire_date + 1.day)
        expect(Album.for_takedown_by_day(expire_date - 2, nil)).not_to include(album)

        renewal_history.update(expires_at: expire_date)
        expect(Album.for_takedown_by_day(expire_date - 2, nil)).not_to include(album)

        renewal_history.update(expires_at: expire_date - 1.day)
        expect(Album.for_takedown_by_day(expire_date - 2, nil)).to include(album)
      end
    end

    context "yearly album is not canceled" do
      it "should not takedown until after grace period" do
        person = create(:person)
        album = create(:album, :purchaseable, :with_one_year_renewal, person: person)
        renewal = person.renewals.first
        renewal.update(takedown_at: nil)
        renewal_history = renewal.renewal_history.first

        expire_date = Date.today
        renewal_history.expires_at = expire_date
        renewal_history.starts_at  = expire_date - 1.year
        renewal_history.save!

        expect(expire_date).to eq(Date.parse(renewal_history.expires_at.to_s))

        expect(Album.for_takedown_by_day(expire_date, nil)).to include(album)
        expect(Album.for_takedown_by_day(expire_date - 1.day, nil)).not_to include(album)
        expect(Album.for_takedown_by_day(expire_date + 1.day, nil)).to include(album)
      end
    end
    context "plan person has expired album" do
      it "should exclude plan person's album" do
       person = create(:person, :with_person_plan)
       album = create(:album, :purchaseable, :with_one_year_renewal, person: person)
       renewal = person.renewals.first
       renewal.update(takedown_at: nil)
       renewal_history = renewal.renewal_history.first

       expire_date = Date.today
       renewal_history.expires_at = expire_date
       renewal_history.starts_at  = expire_date - 1.year
       renewal_history.save!

       expect(expire_date).to eq(Date.parse(renewal_history.expires_at.to_s))

       expect(Album.for_takedown_by_day(expire_date, nil)).not_to include(album)
       expect(Album.for_takedown_by_day(expire_date + 1.day, nil)).not_to include(album)
      end
    end
  end

  describe "#change_single_track_album_to_single" do
    context "when calling with a single track album" do
      before(:each) do
        @album = create(:album, :creatives => [{ "name" => "default artist", "role" => "primary_artist" }])
        @s3_asset = create(:s3_asset)
        @song1 = create(:song, :album => @album, :s3_asset => @s3_asset)
        @upload1 = create(:upload, :song => @song1)
        @album.songs << @song1
      end

      context "when track is < 10 minutes" do
        before(:each) do
          # Duration expected in ms
          @song1.big_box_meta_data = { "duration" => [(10.minutes - 1.second) * 1000] }
        end

        it "changes to a single" do
          @album.save
          @album = Album.find(@album.id)

          allow_any_instance_of(Song).to receive(:big_box_meta_data).and_return({"duration" => [55900]})
          @album.change_single_track_album_to_single

          expect(@album.single?).to eq true
          expect(@album.album_type).to eq("Single")
        end

        it "should change the album's name to the name of the single track" do
          expect(@album.change_single_track_album_to_single).to be_truthy
          @album.save
          @album = Album.find(@album.id)

          expect(@album.name).to eq("#{@song1.name}")
        end

        it "changes a compilation album to a non compilation single" do
          @album.is_various = 1
          expect(@album.save).to be_truthy
          expect(@album.change_single_track_album_to_single).to be_truthy

          @album = Album.find(@album.id)
          expect(@album.is_various).to be_falsey
        end

        it "moves all creatives from a compilation album to a single" do
          artist = Artist.find_or_create_by(name: "Main Artist")
          @song1.creatives << Creative.new(:role => "primary_artist", :artist => artist)
          artist = Artist.find_or_create_by(name: "featured artist")
          @song1.creatives << Creative.new(:role => "featuring", :artist => artist)

          @album.is_various = 1
          expect(@album.save).to be_truthy
          expect(@album.change_single_track_album_to_single).to be_truthy

          @single = Album.find(@album.id)
          expect(@single.creatives.size).to eq(3)
          expect(@single.songs.first.creatives.size).to eq(0)
        end
      end

      context "when track is >= 10 minutes" do
        before(:each) do
          # Duration expected in MS and in an array
          @song1.big_box_meta_data = { "duration" => [10.minutes * 1000] }
        end

        it "should not change a multi-track album to a single" do
          expect(@album.change_single_track_album_to_single).to be_nil
          expect(@album.class).to be Album
          expect(@album.album_type).to eq("Album")
        end

        it "should not change the name of the song" do
          expect(@album.name).not_to eq(@song1.name)
        end
      end
    end

    context "when calling with a multi track album" do
      before(:each) do
        @album = create(:album, :with_uploaded_songs, number_of_songs: 2)
        @song1, @song2 = @album.songs
      end

      it "should not change a multi-track album to a single" do
        expect(@album.change_single_track_album_to_single).to be_nil
        expect(@album.class).to be Album
        expect(@album.album_type).to eq("Album")
      end

      it "should not change the name of the song" do
        expect(@album.name).not_to eq(@song1.name)
        expect(@album.name).not_to eq(@song2.name)
      end
    end

    it 'should do nothing to singles and ringtones' do
      single = build_stubbed(:single)
      expect(single.change_single_track_album_to_single).to be_nil
      expect(single.album_type).to eq("Single")

      ringtone = build_stubbed(:ringtone, person: single.person)
      expect(ringtone.change_single_track_album_to_single).to be_nil
      expect(ringtone.album_type).to eq("Ringtone")
    end
  end

  describe "#finalize" do
    it "marks finalized with Time.now" do
      time = Time.local(2015, 10, 21, 15, 30, 0)
      Timecop.freeze(time) do
        album = create(:album, :payment_applied, :purchaseable)

        expect(album.finalized?).to eq false
        album.salepoints.each { |sp| expect(sp).to receive(:finalize) }
        expect(Note).to receive(:create)

        album.finalize

        expect(album.finalized?).to eq true
        expect(album.finalized_at).to eq time
      end
    end

    it "wont finalize an album without necessary items (artwork, songs, etc)" do
      album = create(:album)
      expect(album.finalized?).to eq false
      expect(Note).not_to receive(:create)
      album.finalize
      expect(album.finalized?).to eq false
    end

    it "wont finalize if not paid" do
      album = create(:album, :with_salepoints, :with_uploaded_song_and_artwork)
      expect(album.finalized?).to eq false
      expect(Note).not_to receive(:create)
      album.finalize
      expect(album.finalized?).to eq false
    end

    it "encodes album assets if album is finalized" do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
      album = create(:album, :payment_applied, :purchaseable)
      expect_any_instance_of(AssetsBackfill::Base).to receive(:run).once
      album.finalize
    end

    it "wont encode assets if unable to finzlie" do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
      album = create(:album, :with_salepoints, :with_uploaded_song_and_artwork)
      expect_any_instance_of(AssetsBackfill::Base).not_to receive(:run)
      album.finalize
    end

    it "should add salepoints missed while unfinalized and automator was effective with process_missing_automator_salepoints" do
      album = create(:album, :payment_applied, :purchaseable)
      effective_date = Store.find_by(short_name: "KKBox").launched_at - 1.days
      create(:salepoint_subscription, album: album, effective: effective_date, finalized_at: effective_date)

      allow(Inventory).to receive(:use!).and_return([])
      allow(album).to receive(:process_missing_automator_salepoints)

      album.finalize

      expect(album).to have_received(:process_missing_automator_salepoints)
    end
  end

  describe "#unfinalize" do
    it "marks finalized_at as nil if able to finalize" do
      album = create(:album, :paid)
      album.salepoints.each { |sp| expect(sp).to receive(:unfinalize) }
      expect(Note).to receive(:create)
      expect(album.finalized?).to eq true
      album.unfinalize
      expect(album.finalized?).to eq false
    end

    it "updates created_with_songwriter and created_with" do
      album = create(:album, :paid, :without_songwriter)
      expect(album.finalized?).to eq true
      expect(album.created_with).nil?
      expect(album.created_with_songwriter).to eq false
      album.unfinalize
      expect(album.created_with).to eq 'songwriter'
      expect(album.created_with_songwriter).to eq true
    end

    it "doesn't update created_with_songwriter and created_with when Ringtone" do
      album = create(:album, :paid, :ringtone)
      expect(album.finalized?).to eq true
      album.unfinalize
      expect(album.created_with).nil?
      expect(album.created_with_songwriter).to be false
    end
  end

  describe "#finalized?" do
    it "should return false to #finalized? if finalized_at is not set" do
      @album = build(:album)
      @album.finalized_at = nil
      expect(@album.finalized?).to be_falsey
    end

    it "should return true to #finalize? if finalized_at is set" do
      @album = build(:album)
      @album.finalized_at = Time.now
      expect(@album.finalized?).to be_truthy
    end
  end

  describe "#all_duplicate_content" do
    before(:each) do
      @album = TCFactory.build_album(:creatives => [{ "name" => "Artist Name", "role" => "primary_artist" }])
      @album.songs << Song.new(:name => "idol dalliance", content_fingerprint: Digest::MD5.hexdigest("This is a music file"))
      @first_single = @album.songs.first

      @first_album = TCFactory.build_album(:creatives => [{ "name" => "Artist Name", "role" => "primary_artist" }])
      @first_album.songs << Song.new(:name => "first song", content_fingerprint: Digest::MD5.hexdigest("This is the first music file"))
      @first_song = @first_album.songs.first

      @second_album = TCFactory.build_album(:creatives => [{ "name" => "Artist Name", "role" => "primary_artist" }])
      @second_album.songs << Song.new(:name => "second song", content_fingerprint: Digest::MD5.hexdigest("This is the second music file"))
      @second_song = @second_album.songs.first
    end

    context "albums with a single song" do
      it "should return an empty array when there are no matches" do
        expect(@album.all_duplicate_content).to match_array([])
      end
      it "should return an array of a single pair if there is only one match" do
        @first_song.content_fingerprint = Digest::MD5.hexdigest("This is a music file")
        @first_song.save!

        expected_result = [{
          flagged_song_id: @first_single.id,
          matched_song_ids: [@first_song.id]
        }]

        expect(@album.all_duplicate_content).to match_array(expected_result)
      end

      it "should return an array of all matched song pairs" do
        @first_song.content_fingerprint = Digest::MD5.hexdigest("This is a music file")
        @first_song.save!

        @second_song.content_fingerprint = Digest::MD5.hexdigest("This is a music file")
        @second_song.save!

        expected_result = [{
          flagged_song_id: @first_single.id,
          matched_song_ids: [@first_song.id, @second_song.id]
        }]

        expect(@album.all_duplicate_content).to match_array(expected_result)
      end
    end

    context "albums with multiple songs" do
      before(:each) do
        @album.songs << Song.new(:name => "Second dalliance", content_fingerprint: Digest::MD5.hexdigest("This is a second music file"))
        @second_single = @album.songs.second

        @third_album = TCFactory.build_album(:creatives => [{ "name" => "Artist Name", "role" => "primary_artist" }])
        @third_album.songs << Song.new(:name => "idol dalliance", content_fingerprint: Digest::MD5.hexdigest("This is the third music file"))
        @third_song = @third_album.songs.first
      end

      it "should return an empty array when there are no matches" do
        expect(@album.all_duplicate_content).to match_array([])
      end

      it "should return an array of a single pair if there is only one match" do
        @first_song.content_fingerprint = Digest::MD5.hexdigest("This is a music file")
        @first_song.save!

        @second_song.content_fingerprint = Digest::MD5.hexdigest("This is a second music file")
        @second_song.save!

        expected_result = [
          {
            flagged_song_id: @first_single.id,
            matched_song_ids: [@first_song.id]
          },
          {
            flagged_song_id: @second_single.id,
            matched_song_ids: [@second_song.id]
          }
        ]

        expect(@album.all_duplicate_content).to match_array(expected_result)
      end

      it "should return an array of all matched song pairs" do
        @first_song.content_fingerprint = Digest::MD5.hexdigest("This is a music file")
        @first_song.save!

        @second_song.content_fingerprint = Digest::MD5.hexdigest("This is a music file")
        @second_song.save!

        @third_song.content_fingerprint = Digest::MD5.hexdigest("This is a second music file")
        @third_song.save!

        expected_result = [
          {
            flagged_song_id: @first_single.id,
            matched_song_ids: [@first_song.id, @second_song.id]
          },
          {
            flagged_song_id: @second_single.id,
            matched_song_ids: [@third_song.id]
          }
        ]

        expect(@album.all_duplicate_content).to match_array(expected_result)
      end
    end
  end

  describe "#flag_duplicate_content" do
    before(:each) do
      @album = TCFactory.build_album(:creatives => [{ "name" => "Artist Name", "role" => "primary_artist" }])
      @album.songs << Song.new(:name => "idol dalliance", content_fingerprint: Digest::MD5.hexdigest("This is a music file"))
      @first_single = @album.songs.first

      @first_album = TCFactory.build_album(:creatives => [{ "name" => "Artist Name", "role" => "primary_artist" }])
      @first_album.songs << Song.new(:name => "idol dalliance", content_fingerprint: Digest::MD5.hexdigest("This is the first music file"))
      @first_album.finalized_at = Time.now
      @first_song = @first_album.songs.first
      @first_song.album.finalized_at = Time.now
      @first_song.album.save
    end

    context "when album is not finalized" do
      it "should not flag any albums" do
        @first_song.content_fingerprint = Digest::MD5.hexdigest("This is a music file")
        @first_song.save!
        @album.flag_duplicate_content
        expect(AlbumFlaggedContent.count).to eq 0
      end
    end

    context "when album is finalized" do
      it "should not flag albums when no matches were found" do
        @album.finalized_at = Time.now
        @first_single.album.finalized_at = Time.now
        @album.flag_duplicate_content
        expect(AlbumFlaggedContent.count).to eq 0
      end
      it "should flag a the album if one match was found" do
        @album.finalized_at = Time.now
        @first_single.album.finalized_at = Time.now

        @first_song.content_fingerprint = Digest::MD5.hexdigest("This is a music file")
        @first_song.save!
        @album.flag_duplicate_content
        expect(AlbumFlaggedContent.count).to eq 1
      end
    end
  end

  describe "#sale_date" do
    before(:each) do
      @album = build(:album)
      @album.sale_date = nil
    end

    it "should parse dates" do
      date = "September 21, 2009"
      @album.sale_date = date
      assert @album.sale_date == Date.parse(date)
    end

    it "should not parse incorrectly formatted dates" do
      date = "dasfasdfafd"
      @album.sale_date = date
      assert @album.sale_date == nil, "-Expected dates to fail parsing"
      @album.valid?
      assert !@album.errors.empty?, "-Expected album to be invalid"
    end

    it "should provide sale date to string" do
      date = Date.today
      @album.sale_date = date
      assert !@album.sale_date_to_s.nil?
      assert @album.sale_date_to_s == date.strftime(Tunecore::StringDateParser::DATE_FORMAT)
    end
  end

  describe "#orig_release_year" do
    before :each do
      @album = build(:album)
    end

    it "should parse dates" do
      date = "September 21, 2009"
      @album.orig_release_year = date
      assert @album.orig_release_year == Date.parse(date)
    end

    it "should save the original release year as the current sale date when not provided" do
      @album.sale_date = Date.today
      @album.save!
      @album.update(:orig_release_year => nil)
      @album.valid?
      assert @album.orig_release_year == @album.sale_date, "Should be the same date, before validation setting when not provided."
    end

    it "should not parse incorrectly formatted dates" do
      date = "dasfasdfafd"
      @album.orig_release_year = date
      expect(@album.orig_release_year).to be_nil
    end

    it "should not consider original release date to be invalid if sale date is not set" do
      @album.orig_release_year = nil
      @album.valid?
      assert @album.errors_on(:orig_release_date).empty?
    end

    it "should say the original release date is invalid if it's after sale date" do
      @album.golive_date = Time.now.utc
      @album.not_previously_released = false
      @album.orig_release_year = Date.today + 1

      expect { @album.save! }.to raise_error
      expect(@album.errors.messages[:orig_release_date]).to be_empty
    end

    it "should provide original release date to string" do
      date = Date.today
      @album.orig_release_year = date
      assert !@album.orig_release_year_to_s.nil?
      assert @album.orig_release_year_to_s == date.strftime(Tunecore::StringDateParser::DATE_FORMAT)
    end
  end

  describe "#golive_date" do
    context "in a timed_release context" do
      context "invalid golive_date's" do
        context "when saving an album" do
          it "should indicate it's invalid" do
            album = build(:album, golive_date: 123123123)
            album.save

            expect(album).not_to be_valid
            expect(album.errors[:golive_date]).to include "Please enter a valid date/time."
          end
        end
      end

      context "valid golive_dates's" do
        context "when saving an album" do
          it "should indicate it's valid" do
            album = build(:album, golive_date: Time.now + 1.days)
            allow(FeatureFlipper).to receive(:show_feature?).with(:crt_cache, nil).and_return(true)
            album.save

            expect(album).to be_valid
          end
        end
      end
    end

    context "not in a timed_release context" do
      context "invalid golive_date's" do
        context "when saving an album" do
          it "should indicate it's valid" do
            album = build(:album, golive_date: 123123123)
            album.save

            expect(album).not_to be_valid
          end
        end
      end
    end

    context "in a timed release context" do
      context "without a golive date, but with a sale_date" do
        it "should add the golive_date to be whatever the sale_date is and set the timed release timing scenario" do
          this_sale_date = Date.new(2019, 6, 1)

          @album = build(:album, sale_date: this_sale_date, golive_date: nil, timed_release_timing_scenario: nil)
          allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
          @album.save

          @album.name = @album.name + ': a timed release album'

          @album.save

          expect(@album.golive_date.in_time_zone('Eastern Time (US & Canada)').to_date).to eq(this_sale_date)
          expect(@album.timed_release_timing_scenario).to eq('relative_time')
        end
      end
    end
  end

  describe "#parental_advisory?" do
    it "should return true if any of the songs have their .parental_advisory set to true" do
      @album = Album.new
      @song1 = mock_model(Song, {:parental_advisory => true})
      @song2 = mock_model(Song, {:parental_advisory => false})
      expect(@album).to receive(:songs).once.and_return([@song1, @song2])
      expect(@album.parental_advisory?).to be_truthy
    end

    it "should return false if all of the songs hae their .parental_advisory set to false" do
      @album = Album.new
      @song2 = mock_model(Song, {:parental_advisory => false})
      expect(@album).to receive(:songs).once.and_return([@song2])
      expect(@album.parental_advisory?).to be_falsey
    end
  end

  describe "#mark_as_deleted" do
    it "should change .is_deleted? from false to true" do
      @album = build(:album)
      expect { @album.mark_as_deleted }.to change { @album.is_deleted? }.from(false).to(true)
    end

    it "should change set the .deleted_date to set it to an instance of Time" do
      @album = build(:album)
      expect { @album.mark_as_deleted }.to change { @album.deleted_date }
      expect(@album.deleted_date).to be_instance_of(Time)
    end

    it "should call remove_optional_upcs and remove_physical_upc" do
      @album = build(:album)
      expect(@album).to receive(:remove_optional_upcs).once
      expect(@album).to receive(:remove_physical_upc).once
      @album.mark_as_deleted
      expect(@album.optional_upc).to be_nil
      expect(@album.physical_upc).to be_nil
    end

    it "should cancel any renewal" do
      person = create(:person)
      album = create(:album, :purchaseable, :with_one_year_renewal, finalized_at: nil, person: person)

      expect(album.renewal.canceled_at).to be_nil

      album.mark_as_deleted

      expect(album.renewal.canceled_at).not_to be_nil
    end

    it "should throw exception when not album.is_deletable?" do
      album = TCFactory.create(:album, :finalized_at => nil, :payment_applied => true)
      expect(album.is_deletable?).to be_falsey
      expect { album.mark_as_deleted }.to raise_error(Exception, "You cannot delete this album without a takedown request")
    end

    it "should throw exception when any part of the process fail" do
      exception = Exception.new("Failed to update attributes")
      album = create(:album)
      allow(album).to receive(:update_attribute).and_raise(exception)
      expect { album.mark_as_deleted }.to raise_error(Exception)
    end
  end

  describe "#remove_optional_upcs" do
    before(:each) do
      @album = create(:album)
      @album.optional_upcs.create!(number: "640859000723", upc_type: "optional")
      @album.optional_upcs.create!(number: "783707931722", upc_type: "optional")
    end

    it "should delete the 2 optional upcs from the database if the album hasn't been finalized" do
      expect(@album).to receive(:finalized?).once.and_return(false)
      expect { @album.remove_optional_upcs }.to change{ Upc.count }.by(-2)
    end

    it "should not delete the 2 optional upcs from the database if the album has been finalized" do
      expect(@album).to receive(:finalized?).once.and_return(true)
      expect { @album.remove_optional_upcs }.not_to change{ Upc.count }
    end
  end

  describe "#upc" do
    it "should return the .optional_upc if there is one" do
      @album = Album.new
      expect(@album).to receive(:optional_upc).once.and_return('Optional UPC')
      expect(@album).not_to receive(:tunecore_upc)
      expect(@album.upc).to eq('Optional UPC')
    end

    it "should return the .tunecore_upc if there is no .optional_upc" do
      @album = Album.new
      expect(@album).to receive(:optional_upc).once.and_return(nil)
      expect(@album).to receive(:tunecore_upc).once.and_return('TC UPC')
      expect(@album.upc).to eq('TC UPC')
    end
  end

  describe "#assign_new_tunecore_upc!" do
    it "should create a new tunecore_upc when .assign_new_tunecore_upc! is called" do
      album = create(:album)
      original_upc = album.upc
      expect(Upc.where(upcable_id: album.id).count).to eq 1
      album.assign_new_tunecore_upc!
      expect(Upc.where(upcable_id: album.id).count).to eq 2
      expect(album.upc).to_not eq original_upc
    end
  end


  describe "#type_supports_subscription?" do
    it "should ensure album type supports subscription" do
      album = build(:album)

      album.album_type = "Album"
      expect(album.supports_subscriptions).to eq(true)

      album.album_type = "Single"
      expect(album.supports_subscriptions).to eq(true)

      album.album_type = "Ringtone"
      expect(album.supports_subscriptions).to eq(false)
    end
  end

  describe "#supports_subscriptions" do
    it 'should properly determine if the album supports salepoint subscriptions' do
      album = build_stubbed(:album)

      album.album_type = "Album"
      expect(album.supports_subscriptions).to eq(true)

      album.album_type = "Single"
      expect(album.supports_subscriptions).to eq(true)

      album.album_type = "Ringtone"
      expect(album.supports_subscriptions).to eq(false)
    end

    it "should return false if taken down" do
      album = build(:album)
      album.album_type = 'Album'
      expect(album.supports_subscriptions).to eq(true)

      album.takedown_at = Time.now()
      expect(album.supports_subscriptions).to eq(false)
    end
  end

  describe "#type_supports_preorder?" do
    it 'should ensure album type supports preorders' do
      album = build_stubbed(:album)

      album.album_type = "Album"
      expect(album.type_supports_preorder?).to eq(true)

      album.album_type = "Single"
      expect(album.type_supports_preorder?).to eq(true)

      album.album_type = "Ringtone"
      expect(album.type_supports_preorder?).to eq(false)
    end
  end

  describe "#supports_preorder?" do
    it "should ensure album is not finalized" do
      album = create(:album)
      store = Store.first
      salepoint = TCFactory.create(:salepoint, salepointable: album, store_id: store.id)
      salepoint.update_attribute(:store_id, 36)
      album.update_attribute(:payment_applied, false)
      expect(album.reload.supports_preorder?).to eq(true)

      album.update_attribute(:payment_applied, true)
      expect(album.supports_preorder?).to eq(false)
    end

    it "should ensure album has iTunes salepoint" do
      album = create(:album)
      store = Store.first
      TCFactory.create(:salepoint, salepointable: album, store_id: store.id)

      expect(album.supports_preorder?).to eq(false)
    end
  end

  describe '#has_non_formattable_language_code?' do
    it 'returns true if album has language code of fr, it or sv' do
      fr_album = build_stubbed(:album, language_code: 'fr')
      it_album = build_stubbed(:album, language_code: 'it')
      sv_album = build_stubbed(:album, language_code: 'sv')
      expect(fr_album.has_non_formattable_language_code?).to be_truthy
      expect(it_album.has_non_formattable_language_code?).to be_truthy
      expect(sv_album.has_non_formattable_language_code?).to be_truthy
    end

    it 'returns false for albums without language code fr, it or sv' do
      album = build_stubbed(:album)
      expect(album.has_non_formattable_language_code?).to be_falsey
    end
  end

  describe "#missing_salepoint" do
    it "should return all albums which are missing a salepoint for given store" do
      person    = create(:person)
      album     = create(:album, :person => person, :finalized_at => Date.today, :album_type => "Album")
      single    = create(:album, :person => person, :finalized_at => Date.today, :album_type => "Single")
      ringtone  = create(:album, :person => person, :finalized_at => Date.today, :album_type => "Ringtone")

      spotify = Store.find(26) # spotify
      SalepointableStore.create!(:store => spotify, :salepointable_type => "Album")
      SalepointableStore.create!(:store => spotify, :salepointable_type => "Single")

      itunes = Store.find(36) # iTunesWW

      SalepointableStore.create!(:store => itunes, :salepointable_type => "Album")
      SalepointableStore.create!(:store => itunes, :salepointable_type => "Single")
      SalepointableStore.create!(:store => itunes, :salepointable_type => "Ringtone")

      missing = person.albums.missing_salepoint(spotify).map {|a| a.id}
      expect(missing.size).to eq(2)
      expect(missing.include?(album.id)).to eq(true)
      expect(missing.include?(single.id)).to eq(true)

      missing = person.albums.missing_salepoint(itunes).map {|a| a.id}
      expect(missing.size).to eq(3)
      expect(missing.include?(album.id)).to eq(true)
      expect(missing.include?(single.id)).to eq(true)
      expect(missing.include?(ringtone.id)).to eq(true)

      TCFactory.create(:salepoint,
                      :salepointable_id => album.id,
                      :salepointable_type => album.class.name,
                      :store => itunes)

      missing = person.albums.missing_salepoint(itunes).map {|a| a.id}
      expect(missing.size).to eq(2)
      expect(missing.include?(single.id)).to eq(true)
      expect(missing.include?(ringtone.id)).to eq(true)
    end
  end

  describe "#facebook_tracks" do
    before :each do
      @album = create(:album, :with_uploaded_songs)
      @album.songs.each{|s| create(:track_monetization, song: s, person: @album.person) }
    end

    it "returns track monetizations for the FBTracks store" do
      expect(@album.reload.facebook_tracks.count).to eq(@album.songs.count)
    end

    it "ignores tracks monetized through other stores" do
      @album.songs.each{|s| create(:track_monetization, song: s, person: @album.person, store: Store.first) }
      fbtracks   = @album.reload.facebook_tracks
      all_tracks = @album.track_monetizations

      expect(all_tracks.count).not_to eq(fbtracks.count)
    end
  end

  describe "#created_with_songwriter?" do
    it "returns true if the album's created_with field is 'songwriter'" do
      album = build_stubbed(:album, created_with: "songwriter")

      expect(album).to be_created_with_songwriter
    end

    it "returns true if the album's created_with field is 'songwriter_relaxed'" do
      album = build_stubbed(:album, created_with: "songwriter_relaxed")

      expect(album).to be_created_with_songwriter
    end

    it "returns false if the album's created_with field is nil" do
      album = build_stubbed(:album, created_with: nil)

      expect(album).not_to be_created_with_songwriter
    end
  end

  describe "#skip_songwriter_validations?" do
    it "returns true if the album was NOT created with songwriter" do
      album = build_stubbed(:album, created_with: nil)

      expect(album).to be_skip_songwriter_validations
    end

    it "returns false if the album was created with songwriter" do
      album = build_stubbed(:album, created_with: "songwriter")

      expect(album).not_to be_skip_songwriter_validations
    end
  end

  describe ".same_language_codes_on_songs" do
    context "when it's an album" do
      it "updates the songs language codes to the album's language code" do
        album = create(:album, :with_uploaded_songs)
        album.songs.each { |s| create(:track_monetization, song: s, person: album.person) }
        album.update(language_code: "hu")
        album.same_language_codes_on_songs
        same_language_code_as_album = album.songs.all? { |s| s.metadata_language_code.code == "hu" }
        expect(same_language_code_as_album).to eq(true)
      end
    end

    context "when it's a Single" do
      it "updates the song's language codes to the single's language code" do
        single = create(:single, song: create(:song, :with_language_code))
        single.update(language_code: "hu")
        single.same_language_codes_on_songs
        same_language_code_as_album = single.songs.all? { |s| s.metadata_language_code.code == "hu" }
        expect(same_language_code_as_album).to eq(true)
      end
    end

     context "when it's a Ringtone" do
      it "updates the song's language codes to the ringtone's language code" do
        ringtone = create(:ringtone, song: create(:song, :with_language_code))
        ringtone.update(language_code: "hu")
        ringtone.same_language_codes_on_songs
        same_language_code_as_album = ringtone.songs.all? { |s| s.metadata_language_code.code == "hu" }
        expect(same_language_code_as_album).to eq(true)
      end
    end
  end

  describe ".ready_to_renew" do
    context "with an album that has no petri bundle" do
      it "returns albums that are ready to renew" do
        album_with_renewal_item = create(:album, :rejected)
        product = create(:product)
        product_item = create(
          :product_item,
          product: product,
          renewal_duration: 11,
          renewal_interval: "year"
        )
        renewal = create(:renewal, :no_takedown_at, item_to_renew: product_item)
        create(:renewal_item, renewal: renewal, related: album_with_renewal_item)
        end_date = renewal.renewal_history.first.expires_at

        result = Album.ready_to_renew(end_date)

        expect(result).to include(album_with_renewal_item)
      end
    end

    context "with an album that has a petri bundle" do
      it "does not returns albums that are ready to renew" do
        album_with_petri_bundle = create(:album, :rejected)
        petri_bundle = create(:petri_bundle, album: album_with_petri_bundle)
        create(:distribution, :enqueued, petri_bundle: petri_bundle)
        product = create(:product)
        product_item = create(
          :product_item,
          product: product,
          renewal_duration: 11,
          renewal_interval: "year"
        )
        renewal = create(:renewal, :no_takedown_at, item_to_renew: product_item)
        create(:renewal_item, renewal: renewal, related: album_with_petri_bundle)
        end_date = renewal.renewal_history.first.expires_at

        result = Album.ready_to_renew(end_date)

        expect(result).not_to include(album_with_petri_bundle)
      end
    end
  end

  describe ".timed_release_absolute_time?" do
    context 'when a timed release timing scenario is absolute timing' do
      it 'should return true' do
        @album = build_stubbed(:album)
        @album.timed_release_timing_scenario = 'absolute_time'

        expect(@album.timed_release_absolute_time?).to be(true)
      end
    end

    context 'when a timed release timing scenario is not absolute timing' do
      it 'should return false' do
        @album = build_stubbed(:album)
        @album.timed_release_timing_scenario = 'relative_time'

        expect(@album.timed_release_absolute_time?).to be(false)
      end
    end
  end

  describe ".timed_release_same_time?" do
    context 'when a timed release timing scenario is same timing' do
      it 'should return true' do
        @album = build_stubbed(:album)
        @album.timed_release_timing_scenario = 'relative_time'

        expect(@album.timed_release_relative_time?).to be(true)
      end
    end

    context 'when a timed release timing scenario is not same timing' do
      it 'should return false' do
        @album = build_stubbed(:album)
        @album.timed_release_timing_scenario = 'absolute_time'

        expect(@album.timed_release_relative_time?).to be(false)
      end
    end

    context "user tries to update an album when a timed release date is in the past" do
      before { allow_any_instance_of(ReviewAudit).to receive(:send_sns_notification) }

      it 'should save succesfully' do
        Timecop.freeze
        album = create(
          :album,
          :purchaseable,
          :payment_applied,
          :with_petri_bundle,
          golive_date: Time.now
        )
        album.finalize

        Timecop.travel(Time.now + 1.day)
        person = create(:person, :admin)
        content_review_role = Role.where(name: 'Content Review Manager')
        person.roles << content_review_role

        review_reason = create(:review_reason, roles: content_review_role, should_unfinalize: true)
        review_audit = create(:review_audit, album: album, event: "REJECTED", person: person, review_reasons: [review_reason])

        expect(album.reload.finalized_at).to eq(nil)
        expect(album.update(name: 'TestName')).to be(true)

        album.finalize
        review_audit = create(:review_audit, album: album.reload, event: "APPROVED", person: person)
        expect(album.reload.finalized_at).not_to eq(nil)
        Timecop.return
      end
    end
  end

  describe "#as_json" do
    it "returns the expected json" do
      album = create(:album, :with_artwork)

      result = album.as_json(only: [:id, :name, :artist_name, :artwork_url])

      expect(result).to eq expected_album_json(album)
    end

    def expected_album_json(album)
      {
        album: {
          "artist_name" => album.artist_name,
          "artwork_url" => {
            "small_url" => album.artwork.url(:small),
            "medium_url" => album.artwork.url(:medium),
            "large_url" => album.artwork.url(:large)
          },
          "id" => album.id,
          "name" => album.name
        }
      }
    end
  end

  context "#golive_date_for_display" do
    context "when sale_date is greater than golive_date" do
      it 'returns the sale_date' do
        current = 2.weeks.from_now
        previous = 2.weeks.ago
        album = build_stubbed(:album, golive_date: previous, sale_date: current)

        result = album.golive_date_for_display

        expect(result).to eq album.sale_date.to_time
      end
    end

    context "when sale_date is not greater than golive date" do
      it 'returns the golive date' do
        current = 2.weeks.from_now
        previous = 2.weeks.ago
        album = build_stubbed(:album, golive_date: current, sale_date: previous)

        result = album.golive_date_for_display

        expect(result).to eq album.golive_date
      end
    end
  end

  context "Lockable" do
    it "should test_release_album" do
      admin = create(:person, :admin)
      album = create(:album, locked_by: admin)

      album.release_lock

      expect(album.locked_by).to be_nil
    end

    it 'should test_should_not_release_album' do
      admin = build_stubbed(:person, :admin)
      allow(admin).to receive(:okay_to_release?).and_return(false)
      album = build_stubbed(:album, locked_by: admin)

      album.release_lock

      expect(album.locked_by).not_to be_nil
    end

    it 'should test_locked_and_editable' do
      admin = build_stubbed(:person, :admin)
      person = build_stubbed(:person)
      album = build_stubbed(:album, locked_by: admin)

      expect(album.editable_by?(person)).to eq false
      expect(album.editable_by?(admin)).to eq true
    end

    it 'should test_locked_and_not_editable' do
      admin = build_stubbed(:person, :admin)
      album = build_stubbed(:album, locked_by: admin)

      expect(album.locked?).to eq true
    end
  end

  describe "#has_aod" do
    it "should return true if an album has Amazon-on-Demand as a listed store" do
      album = create(:album)
      create(:salepoint, :aod_store, salepointable_id: album.id, salepointable_type: 'Album')

      expect(album.has_aod).to be(true)
    end

    it "should return false if an album doesn't have Amazon-on-Demand as a listed store" do
      album = create(:album)

      expect(album.has_aod).to be(false)
    end
  end

  describe "#display_added_aod" do
    it "should return false if an album has Amazon-on-Demand as a listed store but didn't add the artwork template" do
      album = create(:album)
      create(:salepoint, :aod_store, salepointable_id: album.id, salepointable_type: 'Album')

      expect(album.display_added_aod).to be_falsey
    end

    it "should return false if an album has Amazon-on-Demand as a listed store and added the artwork template" do
      album = create(:album)
      salepoint = create(:salepoint, :aod_store, salepointable_id: album.id, salepointable_type: 'Album')
      salepoint.template_id = 1
      salepoint.save

      expect(album.display_added_aod).to be_truthy
    end
  end

  describe "#salepoints_with_inventory_check" do
    context "when deleting a salepoint that has not been used or paid for" do
      it "removes the salepoint" do
        album = create(:album)
        salepoint = create(:salepoint, :aod_store, salepointable_id: album.id, salepointable_type: 'Album')

        album.salepoints = []

        expect(album.reload.salepoints).not_to include(salepoint)
      end
    end

    context "when deleting a salepoint that has been used" do
      it "does not remove the salepoint" do
        album = create(:album)
        salepoint = create(:salepoint, :aod_store, salepointable_id: album.id, salepointable_type: 'Album')
        allow(Inventory).to receive(:used?).with(salepoint).and_return(true)

        album.salepoints = []

        expect(album.reload.salepoints).to include(salepoint)
      end
    end

    context "when deleting a salepoint that has not been used but has been paid for" do
      it "does not remove the salepoint" do
        album = create(:album)
        salepoint = create(:salepoint, :aod_store, salepointable_id: album.id, salepointable_type: 'Album', payment_applied: true)

        album.salepoints = []

        expect(album.reload.salepoints).to include(salepoint)
      end
    end
  end

  describe "#check_for_duplicate_with_songs" do
    it "should prevent duplicate artists between primary and featuring artists" do
      album = create(:album, :with_songs)
      song = album.songs.first
      artist1 = create(:artist, name: "Charlie")

      song.creatives << build(:creative, creativeable: song, artist: artist1, role: "featuring")
      song.save

      new_creatives = [{id: nil, name: "Johnny", role: "primary_artist"}, {id: nil, name: artist1.name, role: "primary_artist"}].map(&:with_indifferent_access)
      album.assign_attributes({creatives: new_creatives})

      expect(album.errors).to include(:creatives)
    end
  end

  describe "#creatives=" do
    context "when the name of a creative is the same as another, but has trailing whitespace" do
      it "only keeps the distinct name stripped of trailing whitespace" do
        album = build(:album)
        creatives_data = [{id: nil, name: "Johnny", role: "primary_artist"}, {id: nil, name: "Johnny   ", role: "primary_artist"}].map(&:with_indifferent_access)
        album.creatives = creatives_data
        album.save
        album.reload

        expect(album.creatives.joins(:artist).where(artists: { name: "Johnny" }).count).to eq(1)
      end
    end
  end

  describe "genres" do
    describe "#primary_genre" do
      it "should not accept the Indian parent genre" do
        album = build(:album, primary_genre: Genre.india_parent_genre)

        album.save

        expect(album.errors).to include(:primary_genre_id)
      end
    end

    describe "#secondary" do
      it "should not accept the Indian parent genre" do
        album = build(:album, secondary_genre: Genre.india_parent_genre)

        album.save

        expect(album.errors).to include(:secondary_genre_id)
      end
    end
  end

  describe "#create_copyright_status" do
    context "when the album is created by a non-indian user" do
      it "should not create a copyright status record" do
        album = create(:album)

        expect(album.copyright_status).to be nil
      end
    end

    context "when the album is created by an indian user" do
      it "should create a copyright status record with a status of 'pending'" do
        person = create(:person, :with_india_country_website)
        album = create(:album, person: person)

        copyright_status = album.copyright_status
        expect(copyright_status.status).to eq "pending"
      end
    end
  end

  describe "four byte char validation" do
    let(:album) { build(:album, name: "💿💿💿💿💿") }

    it "validates name for four-byte chars" do
      album.valid?
      expect(album.errors.messages[:name].size).to be > 0
    end
  end

  describe "#process_missing_automator_salepoints" do
    it "should add missing salepoints that should have been added with automator" do
      album = create(:single,  :payment_applied, :with_uploaded_song_and_artwork, :with_songwriter, album_type: "Single")
      effective_date = Store.find_by(short_name: "KKBox").launched_at - 1.days
      create(:salepoint_subscription, album: album, effective: effective_date, finalized_at: effective_date)
      sp_store_1 = create(:salepointable_store, store: Store.find_by(short_name: "Akazoo"))
      sp_store_2 = create(:salepointable_store, store: Store.find_by(short_name: "KKBox"))
      create(:salepoint, salepointable: album, store: sp_store_1.store, finalized_at: Time.now)

      allow(Inventory).to receive(:use!).and_return([])

      album.process_missing_automator_salepoints

      expect(album.salepoints.where(store: sp_store_2.store).count).to eq(1)
    end
  end
end
