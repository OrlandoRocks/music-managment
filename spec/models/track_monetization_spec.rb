require "rails_helper"

describe TrackMonetization do
  describe "#deliver" do
  end

  context "TC-Distributor" do

    let!(:admin) { create(:person, :admin) }
    let!(:album) { create(:album, :purchaseable, :paid, :with_salepoint_song, :approved, :taken_down) }
    let!(:song1) { album.songs.first }
    let!(:song2) { album.songs.last }

    let!(:store) { create(:store, :tc_distributor) }
    let!(:store_delivery_config) { create(:store_delivery_config, store_id: store.id, distributable_class: "TrackMonetization") }
    let!(:track_monetization1) {
      create(:track_monetization, :eligible,
                                  :delivered_via_tc_distributor,
                                  store_id: store.id,
                                  song_id: song1.id) }
    let!(:track_monetization2) {
      create(:track_monetization, :eligible,
                                  :delivered_via_tc_distributor,
                                  store_id: store.id,
                                  song_id: song2.id) }

    let!(:track_monetizations_by_album_store_delivery_types) {
      {
        { album_id: album.id, delivery_type: "full_delivery", store_id: store.id } => [track_monetization1, track_monetization2]
      }
    }

    it "#for_tc_distributor_delivery" do
      expect(described_class.for_tc_distributor_delivery).to eql(track_monetizations_by_album_store_delivery_types)

      album2 = create(:album, :purchaseable, :paid, :with_salepoint_song, :approved, :taken_down)
      song3 = album2.songs.first
      song4 = album2.songs.last

      store2 = create(:store, :tc_distributor)
      store_delivery_config2 = create(:store_delivery_config, store_id: store2.id, distributable_class: "TrackMonetization")
      track_monetization3 =
        create(:track_monetization, :eligible,
                                    :delivered_via_tc_distributor,
                                    store_id: store2.id,
                                    song_id: song3.id)
      track_monetization4 =
        create(:track_monetization, :eligible,
                                    :delivered_via_tc_distributor,
                                    store_id: store2.id,
                                    song_id: song4.id)

      track_monetizations_by_album_store_delivery_types2 =
        {
          { album_id: album.id, delivery_type: "full_delivery", store_id: store.id } => [track_monetization1, track_monetization2],
          { album_id: album2.id, delivery_type: "full_delivery", store_id: store2.id } => [track_monetization3, track_monetization4]
        }

      expect(described_class.for_tc_distributor_delivery).to eql(track_monetizations_by_album_store_delivery_types2)
    end

  end

  describe "#taken_down?" do
    it "is false if takedown_at is null" do
      track_monetization = build(:track_monetization)
      expect(track_monetization.taken_down?).to eq false
    end

    it "is true if takedown_at is not null" do
      track_monetization = build(:track_monetization, takedown_at: Time.now)
      expect(track_monetization.taken_down?).to eq true
    end
  end

  describe "#approved_for_distribution?" do
    it "is false if not eligible" do
      track_monetization = build(:track_monetization, eligibility_status: "pending")
      expect(track_monetization.approved_for_distribution?).to eq false
    end

    it "is true if eligible" do
      track_monetization = build(:track_monetization, eligibility_status: "eligible")
      expect(track_monetization.approved_for_distribution?).to eq true
    end
  end

  describe "#block_and_disqualify" do
    it "sets its state to 'blocked'" do
      track_monetization = create(:track_monetization, eligibility_status: "eligible")
      track_monetization.block_and_disqualify({})

      expect(track_monetization.reload.state).to eq("blocked")
    end

    it "sets its eligibility_status to 'ineligible'" do
      track_monetization = create(:track_monetization, eligibility_status: "eligible")
      track_monetization.block_and_disqualify({})

      expect(track_monetization.reload.eligibility_status).to eq("ineligible")
    end

    context "when in 'delivered' state" do
      let(:track_monetization) {
        create(:track_monetization, :eligible, :delivered)
      }

      it "sets its state to 'blocked'" do
        track_monetization.block_and_disqualify({})
        expect(track_monetization.reload.state).to eq("blocked")
      end

      it "sets its eligibility_status to 'ineligible'" do
        track_monetization.block_and_disqualify({})
        expect(track_monetization.reload.eligibility_status).to eq("ineligible")
      end

      it "perform monetization takedown" do
        expect(TrackMonetization::TakedownService).to receive(:takedown)
        track_monetization.block_and_disqualify({})
      end

      context "delivery via tc-distributor" do
        let(:track_monetization) {
          create(:track_monetization, :eligible, :delivered_via_tc_distributor)
        }

        it "sets its state to 'blocked'" do
          track_monetization.block_and_disqualify({})
          expect(track_monetization.reload.state).to eq("blocked")
        end

        it "sets its eligibility_status to 'ineligible'" do
          track_monetization.block_and_disqualify({})
          expect(track_monetization.reload.eligibility_status).to eq("ineligible")
        end

        it "perform monetization takedown" do
          track_monetization.update(tc_distributor_state: "delivered")
          expect(TrackMonetization::TakedownService).to receive(:takedown)
          track_monetization.block_and_disqualify({})
        end
      end
    end
  end

  describe "#unblock_and_qualify" do
    context "when the track_monetization is already blocked" do
      it "sets its state to 'new'" do
        track_monetization = create(:track_monetization, :blocked)
        track_monetization.unblock_and_qualify({})

        expect(track_monetization.reload.state).to eq("new")
      end

      it "sets its eligibility_status to 'eligible'" do
        track_monetization = create(:track_monetization, :blocked)
        track_monetization.unblock_and_qualify({})

        expect(track_monetization.reload.eligibility_status).to eq("eligible")
      end

      context "with existing takedown" do
        let!(:store_delivery_config) {
          create(:store_delivery_config, store_id: store.id, distributable_class: "TrackMonetization")
        }
        let(:track_monetization) {
          create(:track_monetization, :blocked, takedown_at: 2.days.ago, store: store)
        }

        before { track_monetization.unblock_and_qualify({}) }

        context "legacy tunecore delivery" do
          let(:store) { create(:store, :tunecore) }

          it "sets its state to 'new'" do
            expect(track_monetization.reload.state).to eq("new")
          end

          it "sets its eligibility_status to 'eligible'" do
            expect(track_monetization.reload.eligibility_status).to eq("eligible")
          end
        end

        context "delivery via tc-distributor" do
          let(:store) { create(:store, :tc_distributor) }

          it "sets its state to 'new'" do
            expect(track_monetization.reload.state).to eq("new")
          end

          it "sets its eligibility_status to 'eligible'" do
            expect(track_monetization.reload.eligibility_status).to eq("eligible")
          end
        end
      end
    end
  end

  describe "#pend" do
    it "sets its state to 'pending_approval'" do
      track_monetization = create(:track_monetization, eligibility_status: "eligible")
      track_monetization.pend({})

      expect(track_monetization.reload.state).to eq("pending_approval")
    end

    it "sets its eligibility_status to 'pending'" do
      track_monetization = create(:track_monetization, eligibility_status: "eligible")
      track_monetization.pend({})

      expect(track_monetization.reload.eligibility_status).to eq("pending")
    end
  end

  describe "#check_eligibility" do
    let(:current_user) { Person.first }
    it "runs the track_monetization through the eligibility engine" do
      track_monetization = create(:track_monetization)

      expect_any_instance_of(TrackMonetization::EligibilityEngineService).to receive(:validate!)

      track_monetization.check_eligibility
    end

    it "results in a changed eligibility status" do
      track_monetization = create(:track_monetization)
      first_state = track_monetization.eligibility_status

      track_monetization.check_eligibility

      expect(track_monetization.reload.eligibility_status).not_to eq(first_state)
    end

    it "do not change eligibility status for track_monetization having manual_approval " do
      track_monetization = create(:track_monetization, :with_monetization_blocked_song)

      expect(track_monetization.track_monetization_blocker).not_to be_nil

      track_monetization.track_monetization_blocker.destroy

      expect(track_monetization.reload.track_monetization_blocker).to be_nil
      expect(track_monetization.reload.manual_approval).to be_nil

      track_monetization.create_manual_approval(current_user)

      expect(track_monetization.reload.manual_approval).not_to be_nil

      # run auto-vetting
      track_monetization.check_eligibility

      expect(track_monetization.reload.blocked?).not_to be_truthy
      expect(track_monetization.reload.track_monetization_blocker).to be_nil
      expect(track_monetization.reload.manual_approval).not_to be_nil
    end
  end

  describe "#after_create" do
    describe "#block_track_monetization" do
      context "when there is a track_monetization_blocker attached to its song" do
        it "sets its state to blocked" do
          song = create(:song)
          store = Store.find(83)
          track_monetization_blocker = create(:track_monetization_blocker, store: store, song: song)
          track_monetization = create(:track_monetization, song: song, store: store)
          expect(track_monetization.reload.state).to eq("blocked")
        end

        it "sets its eligibility_status to ineligible" do
          song = create(:song)
          store = Store.find(83)
          track_monetization_blocker = create(:track_monetization_blocker, store: store, song: song)
          track_monetization = create(:track_monetization, song: song, store: store)
          expect(track_monetization.reload.eligibility_status).to eq("ineligible")
        end
      end
    end
  end

  describe "#retry" do
    context "when its state is not enqueued" do
      it "triggers a distribution" do
        track_monetization = create(:track_monetization, :errored)

        expect(track_monetization).to receive(:enqueue_distribution)

        track_monetization.retry({})
      end
    end

    context "when its state is enqueued" do
      it "triggers a distribution" do
        track_monetization = create(:track_monetization, :with_enqueued_state)

        expect(track_monetization).to receive(:enqueue_distribution)

        track_monetization.retry({})
      end
    end

    context "when its state is packaged" do
      it "triggers a distribution" do
        track_monetization = create(:track_monetization, state: "packaged")

        expect(track_monetization).to receive(:enqueue_distribution)

        track_monetization.retry({})
      end
    end
  end

  describe "#create_manual_approval" do
    let(:current_user) { Person.first }
    context "create manaul approval" do
      it "for an eligible track_monetization" do
        track_monetization = create(:track_monetization, eligibility_status: "eligible")

        expect(track_monetization.manual_approval).to be_nil

        track_monetization.create_manual_approval(current_user)

        expect(track_monetization.reload.manual_approval).not_to be_nil
      end
    end

    context "don't create manaul approval" do
      it "for a track_monetization having track_monetization_blocker association" do
        track_monetization = create(:track_monetization, :with_monetization_blocked_song)

        expect(track_monetization.track_monetization_blocker).not_to be_nil

        track_monetization.create_manual_approval(current_user)

        expect(track_monetization.reload.manual_approval).to be_nil
      end
    end
  end
end
