require "rails_helper"

describe ReleaseSubscription do
  let(:album)  { FactoryBot.create(:album) }

  describe "#process_action" do
    context "subscription_action is cancel" do
      it "should call cancel on album" do
        expect_any_instance_of(Album).to receive(:cancel_renewal).once
        ReleaseSubscription.new(album_id: album.id, subscription_action: 'cancel').process_action
      end
    end

    context "subscription_action is takedown" do
      it "should call takedown! on album" do
        expect_any_instance_of(Album).to receive(:takedown!).once
        ReleaseSubscription.new(album_id: album.id, subscription_action: 'takedown').process_action
      end
    end

    context "subscription_action is invalid" do
      it "should not call any action on album" do
        expect_any_instance_of(Album).not_to receive(:takedown!)
        expect_any_instance_of(Album).not_to receive(:cancel_renewal)
        expect(ReleaseSubscription.new(album_id: album.id, subscription_action: 'foobar').process_action).to be(false)
      end
    end
  end
end
