require "rails_helper"

describe "DirectAdvance::EligibleUsersCollector" do

  describe ".collect" do
    before(:each) do
      end_date   = "2017-01-01"
      @collector = DirectAdvance::EligibleUsersCollector.new(end_date)
      allow(@collector).to receive(:starting_users).and_return([[1,"US"],[2,"US"],[3,"CA"],[4,"UK"]])
      allow(@collector).to receive(:user_earnings).with(1).and_return("fake_HIGH_earnings_report")
      allow(@collector).to receive(:user_earnings).with(2).and_return("fake_LOW_earnings_report")
      allow(@collector).to receive(:user_earnings).with(3).and_return("fake_HIGH_earnings_report")
      allow(@collector).to receive(:user_earnings).with(4).and_return("fake_LOW_earnings_report")

      eligible_calc   = double("eligible_calc", user_is_eligible: true)
      ineligible_calc = double("ineligible_calc", user_is_eligible: false)

      allow(DirectAdvance::EarningsReportCalculator).to receive(:calculate).with("fake_HIGH_earnings_report", end_date).and_return(eligible_calc)
      allow(DirectAdvance::EarningsReportCalculator).to receive(:calculate).with("fake_LOW_earnings_report", end_date).and_return(ineligible_calc)

      @collector.collect
    end

    it "adds eligible users with country_website = US and country = US to @us_eligible_users" do
      expect(@collector.us_eligible_users).to eq [1]
    end

    it "adds eligible users with country_website = US and country != US to @non_us_eligible_users" do
      expect(@collector.non_us_eligible_users).to eq [3]
    end
  end

  context "test_run" do
    it "limits user query to 25 results when test_run is true" do
      allow_any_instance_of(DirectAdvance::EligibleUsersCollector).to receive(:find_latest_run_date).and_return("2017-01-01")
      collector = DirectAdvance::EligibleUsersCollector.new("2017-01-01", {test_run: true})
      expect(collector.initial_users_query).to include("limit 25")
    end

    it "does NOT limit user query to 25 results when test_run is false" do
      collector = DirectAdvance::EligibleUsersCollector.new("2017-01-01", {test_run: false})
      expect(collector.initial_users_query).to_not include("limit 25")
    end

    it "does NOT limit user query to 25 results when no options are given" do
      collector = DirectAdvance::EligibleUsersCollector.new("2017-01-01")
      expect(collector.initial_users_query).to_not include("limit 25")
    end
  end
  describe "user_earnings" do
    before(:each) do
      end_date    = Date.current
      test_amount = 500.00
      @collector  = DirectAdvance::EligibleUsersCollector.new(end_date)
      @person1    = create(:person)
      @person2    = create(:person)
      @person3    = create(:person)
      # not including person3 because we want to check someone with no earnings
      people      = [@person1, @person2]
      people.each_with_index do |person, i|
        if i == 0
          # we want one user with all 12 months
          12.times do |time|
            actual_time = time + 1
            create(:person_intake, person: person, amount: test_amount, currency: "EUR",
                   created_at: Date.current - (actual_time).month)
          end
        else
          # one month that shouldn't be counted
          create(:person_intake, person: person, amount: test_amount - 100, currency: "EUR",
                 created_at: Date.current)
          create(:person_intake, person: person, amount: test_amount, currency: "EUR",
                 created_at: Date.current - 1.month)
          create(:person_intake, person: person, amount: test_amount, currency: "EUR",
                 created_at: Date.current - 3.month)
          create(:person_intake, person: person, amount: test_amount, currency: "EUR",
                 created_at: Date.current - 4.month)
          create(:person_intake, person: person, amount: test_amount, currency: "EUR",
                 created_at: Date.current - 9.month)
          # one month that should only be in the last element of the array
          create(:person_intake, person: person, amount: test_amount + 100, currency: "EUR",
                 created_at: Date.current - 14.month)
        end
      end
      @person_1_earnings = @collector.user_earnings(@person1.id)
      @person_2_earnings = @collector.user_earnings(@person2.id)
    end
    context "less than twelve months of continuous earnings" do
      it "creates an array of 14 items" do
        expect(@person_1_earnings.length).to eq(14)
      end
    end
    context "twelve months of continuous earnings" do
      it "creates an array of 14 items" do
        expect(@person_2_earnings.length).to eq(14)
      end
    end
    context "0 earnings" do
      it "creates an array of 14 items" do
        person_3_earnings = @collector.user_earnings(@person3.id)
        expect(person_3_earnings.length).to eq(14)
      end
    end
    it "the first element is the pid" do
      expect(@person_1_earnings.first).to eq(@person1.id)
      expect(@person_2_earnings.first).to eq(@person2.id)
    end
    it "the last element is the sum of all previous earnings" do
      person_2_total    = @collector.total_prev_earnings(@person2.id)
      # expecting 0 because we didn't give them any earnings > 12 months ago
      expect(@person_1_earnings.last).to eq(0.0)
      expect(@person_2_earnings.last).to eq(person_2_total)
    end
    it "the middle 12 elements are the monthly earnings in order of most recent to least" do
      person_1_monthly    = @collector.monthly_earnings(@person1.id).map(&:amount)
      person_2_monthly    = @collector.monthly_earnings(@person2.id).map(&:amount)
      # 'earnings[1..12]' is how DirectAdvance::EarningsReportCalculator finds the monthly earnings
      expect(@person_1_earnings[1..12]).to eq(person_1_monthly)
      # selecting because DirectAdvance::EarningsReportCalculator expects 0s and we want to compare the values here
      # we compare the full output below
      above_zero_earnings = @person_2_earnings[1..12].select {|month| month > 0.0}
      expect(above_zero_earnings).to eq(person_2_monthly)
    end
    it "has 0s for months with no earnings" do
      expect(@person_1_earnings[1..12].any?{|item| item <= 0.0}).to be(false)
      expect(@person_2_earnings[1..12].any?{|item| item <= 0.0}).to be(true)
    end
    it "doesn't have the current month" do
      expect(@person_2_earnings.any?{|item| item == 400}).to be(false)
    end
    it "looks has the values and format that calculate expects" do
      person_1_expectation = [@person1.id, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 0.0]
      person_2_expectation = [@person2.id, 500, 0.0, 500, 500, 0.0, 0.0, 0.0, 0.0, 500, 0.0, 0.0, 0.0, 600]
      expect(@person_1_earnings).to eq(person_1_expectation)
      expect(@person_2_earnings).to eq(person_2_expectation)
    end
  end
end
