require "rails_helper"

describe "DirectAdvance::DatabaseCleaner" do

  describe ".clean" do

    before(:each) do
      create(:direct_advance_earnings_period_record, report_run_on: 6.months.ago.to_s)
      create(:direct_advance_earnings_period_record, report_run_on: Date.yesterday.to_s)
    end

    context "when there are NO earnings_reports over a year old" do
      it "does nothing" do
        expect {
          DirectAdvance::DatabaseCleaner.clean
        }.to_not change(DirectAdvance::EarningsPeriodRecord, :count)
      end
    end

    context "when there are earnings_reports over a year old" do
      it "deletes them" do
        create(:direct_advance_earnings_period_record, report_run_on: 13.months.ago.to_s)
        expect {
          DirectAdvance::DatabaseCleaner.clean
        }.to change(DirectAdvance::EarningsPeriodRecord, :count).by(-1)
      end
    end
  end
end
