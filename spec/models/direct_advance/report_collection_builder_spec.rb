require "rails_helper"

describe "DirectAdvance::ReportCollectionSearch" do

  describe "determines which field to search" do
    context "when an '@' is in search terms" do
      it "sets search_field to user_email" do
        search = DirectAdvance::ReportCollectionSearch.search("test@example.com")
        expect(search.search_field).to eq 'user_email'
      end
    end

    context "when an '@' is NOT in search terms" do
      it "sets search_field to user_id" do
        search = DirectAdvance::ReportCollectionSearch.search("1234567")
        expect(search.search_field).to eq 'user_id'
      end
    end
  end

  describe "search by user id" do
    it "returns a person on an exact match" do
      person = create(:person)
      search = DirectAdvance::ReportCollectionSearch.search(person.id)
      expect(search.person).to eq person
    end

    it "returns nil when no match" do
      search = DirectAdvance::ReportCollectionSearch.search(999999)
      expect(search.person).to eq nil
    end
  end

  describe "search by user email" do
    it "returns a person on an exact match" do
      person = create(:person, email: "test@tunecore.com")
      search = DirectAdvance::ReportCollectionSearch.search(person.email)
      expect(search.person).to eq person
    end

    it "returns nil when no match" do
      search = DirectAdvance::ReportCollectionSearch.search("bad-email@example.com")
      expect(search.person).to eq nil
    end
  end

  describe "report contents" do

    before(:each) do
      @date1 = Date.today
      @date2 = @date1 - 1.week
      @date3 = @date1 - 2.weeks
      @person = create(:person)
      create(:direct_advance_eligibility_record, :ineligible, person: @person, report_run_on: @date1)
      create(:direct_advance_eligibility_record, :eligible, person: @person, report_run_on: @date2)
      create(:direct_advance_eligibility_record, :eligible, person: @person, report_run_on: @date3)
      create(:direct_advance_earnings_period_record, :ineligible_collection, person: @person, report_run_on: @date1)
      create(:direct_advance_earnings_period_record, :eligible_collection, person: @person, report_run_on: @date2)
      create(:direct_advance_earnings_period_record, :eligible_collection, person: @person, report_run_on: @date3)
    end

    subject { DirectAdvance::ReportCollectionSearch.search(@person.id) }

    it "places the most recent run in @latest_report" do
      expect(subject.latest_report[:report_run_on]).to eq @date1
    end

    it "places all but the most recent run in @historical_reports" do
      dates = subject.historical_reports.map { |r| r[:report_run_on] }
      expect(dates).to include(@date2, @date3)
    end

    describe "#check_eligibility" do
      it "sets eligible to false if the account is suspicious" do
        allow(@person).to receive(:suspicious?).and_return(true)
        report = DirectAdvance::ReportCollectionSearch.search(@person.id)
        expect(report.eligible).to be false
      end

      it "sets eligible to false if the latest_report says ineligible" do
        expect(subject.eligible).to be false
      end

      it "sets elibigle to false if there is no latest_report" do
        person2 = create(:person)
        report = DirectAdvance::ReportCollectionSearch.search(person2.id)
        expect(report.eligible).to be false
      end

      it "sets eligible to true if account is not suspicious and the latest_report is eligible" do
        person2 = create(:person)
        create(:direct_advance_eligibility_record, :eligible, person: person2, report_run_on: @date1)
        report = DirectAdvance::ReportCollectionSearch.search(person2.id)
        expect(report.eligible).to be true
      end
    end
  end

  describe "report contents with no historical_reports" do

    before(:each) do
      @date1 = Date.today
      @person = create(:person)
      create(:direct_advance_eligibility_record, :ineligible, person: @person, report_run_on: @date1)
      create(:direct_advance_earnings_period_record, :ineligible_collection, person: @person, report_run_on: @date1)
    end

    subject { DirectAdvance::ReportCollectionSearch.search(@person.id) }

    it "places the only report run in @latest_report, none in @historical_reports" do
      expect(subject.latest_report[:report_run_on]).to eq @date1
      expect(subject.historical_reports).to eq []
    end
  end
end
