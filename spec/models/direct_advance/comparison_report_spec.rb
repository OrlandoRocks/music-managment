require "rails_helper"

describe "DirectAdvance::EarningsReportCalculator" do

  before(:each) do
    allow(File).to receive(:delete)
    file    = double("file", write: nil)
    objects = double("objects", with_prefix: nil)
    bucket  = double("bucket", objects: objects)
    s3      = double("s3", buckets: {"lyric-reports" => bucket})

    allow(objects).to receive(:[]).and_return(file)
    stub_const("S3_CLIENT", s3)

    allow_any_instance_of(DirectAdvance::S3Helper).to receive(:write_and_upload)
    allow_any_instance_of(DirectAdvance::S3Helper).to receive(:previous_report_for_segment_and_date).and_return([10,11,12])
  end

  describe ".run_for" do
    it "writes and uploads comparison reports" do
      end_date = "2017-01-01"
      us_eligible_users = [1,2,3]
      expect_any_instance_of(DirectAdvance::S3Helper).to receive(:upload_comparison_report).with("dropped_accounts", "US", [10, 11, 12], "2017-01-01")
      expect_any_instance_of(DirectAdvance::S3Helper).to receive(:upload_comparison_report).with("added_accounts", "US", [1, 2, 3], "2017-01-01")

      DirectAdvance::ComparisonReport.run_for("US", us_eligible_users, end_date, false)
    end
  end
end
