require "rails_helper"

describe "DirectAdvance::Manager" do

  before(:each) do
    file    = double("file", write: nil)
    objects = double("objects", with_prefix: nil)
    bucket  = double("bucket", objects: objects)
    s3      = double("s3", buckets: {"lyric-reports" => bucket})

    allow(objects).to receive(:[]).and_return(file)
    stub_const("S3_CLIENT", s3)
  end

  describe ".manage" do

    context "arguments" do

      context "options hash" do
        it "defaults to all true" do
          manager = DirectAdvance::Manager.new("2017-01-01")
          expect(manager.options[:create_eligibility_reports]).to eq true
          expect(manager.options[:create_sales_reports]).to       eq true
          expect(manager.options[:create_comparison_reports]).to  eq true
          expect(manager.options[:set_feature_flags]).to          eq true
        end

        it "sets create_eligibility_reports to false" do
          manager = DirectAdvance::Manager.new("2017-01-01", {create_eligibility_reports: false})
          expect(manager.options[:create_eligibility_reports]).to eq false
          expect(manager.options[:create_sales_reports]).to       eq true
          expect(manager.options[:create_comparison_reports]).to     eq true
          expect(manager.options[:set_feature_flags]).to          eq true
        end

        it "sets create_eligibility_reports to false" do
          manager = DirectAdvance::Manager.new("2017-01-01", {create_sales_reports: false})
          expect(manager.options[:create_eligibility_reports]).to eq true
          expect(manager.options[:create_sales_reports]).to       eq false
          expect(manager.options[:create_comparison_reports]).to     eq true
          expect(manager.options[:set_feature_flags]).to          eq true
        end

        it "sets create_eligibility_reports to false" do
          manager = DirectAdvance::Manager.new("2017-01-01", {create_comparison_reports: false})
          expect(manager.options[:create_eligibility_reports]).to eq true
          expect(manager.options[:create_sales_reports]).to       eq true
          expect(manager.options[:create_comparison_reports]).to     eq false
          expect(manager.options[:set_feature_flags]).to          eq true
        end

        it "sets create_eligibility_reports to false" do
          manager = DirectAdvance::Manager.new("2017-01-01", {set_feature_flags: false})
          expect(manager.options[:create_eligibility_reports]).to eq true
          expect(manager.options[:create_sales_reports]).to       eq true
          expect(manager.options[:create_comparison_reports]).to     eq true
          expect(manager.options[:set_feature_flags]).to          eq false
        end
      end

      context "with an invalid end_date argument" do
        it "returns an error message" do
          expect {
            DirectAdvance::Manager.manage("31")
          }.to raise_error "DirectAdvance Manager Log Message: TCDA: Must supply valid end date in format: (YYYY-MM-DD)"
        end
      end

      context "with no end_date argument" do
        it "returns an error message" do
          expect {
            DirectAdvance::Manager.manage
          }.to raise_error ArgumentError
        end
      end

      context "with a valid end_date argument" do
        it "does not raise an error" do
          allow_any_instance_of(DirectAdvance::Manager).to receive(:manage).and_return("OK")
          expect {
            DirectAdvance::Manager.manage("2017-01-01")
          }.to_not raise_error
        end
      end
    end

    context "successful run with default options" do

      before(:each) do
        @end_date = "2017-01-01"
        @manager  = DirectAdvance::Manager.new(@end_date)
        collector = double("Collector", us_eligible_users: [1], non_us_eligible_users: [2])
        allow(collector).to receive(:eligible_users).and_return([1,2])
        allow(DirectAdvance::EligibleUsersCollector).to receive(:collect).and_return(collector)
        allow(DirectAdvance::SalesReportGenerator).to receive(:generate)
                    .and_return(double(file_name: "user_id_string.csv", headers: ["header"], data: [["data"]]))
        allow(FeatureFlipper).to receive(:update_feature).and_return(true)

        us_report     = double("eligible_US_users_report")
        allow(us_report).to receive(:read).and_return("1")
        non_us_report = double("eligible_non_US_users_report")
        allow(non_us_report).to receive(:read).and_return("2")

        allow_any_instance_of(DirectAdvance::S3Helper).to receive(:get_eligible_users_report).with("US", "2017-01-01").and_return(us_report)
        allow_any_instance_of(DirectAdvance::S3Helper).to receive(:get_eligible_users_report).with("non_US", "2017-01-01").and_return(non_us_report)
      end

      it "calls EligibleUsersCollector.collect to get users by eligibility and by US/non_US" do
        expect(DirectAdvance::EligibleUsersCollector).to receive(:collect)
                                                           .with(@end_date, DirectAdvance::Manager::DEFAULT_OPTIONS)
        @manager.manage
      end

      it "it calls SalesReportGenerator.generate to create sales reports" do
        expect(DirectAdvance::SalesReportGenerator).to receive(:generate)
        @manager.manage
      end

      it "it calls update on FeatureFlipper with user_ids" do
        expect(FeatureFlipper).to receive(:update_feature).with("lyric_advance", 0, "1, 2")
        @manager.manage
      end

      context "report generation" do
        it "writes and uploads all three eligible user reports" do
          suffix = @end_date + ".csv"
          expect(@manager.s3_helper).to receive(:write_and_upload).with("eligible_users_ALL_" + suffix, [1,2])
          expect(@manager.s3_helper).to receive(:write_and_upload).with("eligible_US_users_" + suffix, [1])
          expect(@manager.s3_helper).to receive(:write_and_upload).with("eligible_non_US_users_" + suffix, [2])
          @manager.manage
        end

        it "writes and uploads comparison reports" do

        end
      end
    end
  end
end
