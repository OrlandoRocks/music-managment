require "rails_helper"

describe "DirectAdvance::EarningsReportCalculator" do

  describe ".calculate" do
    let(:user_id)  { 123000 }
    let(:end_date) { Date.today.to_s }
    let(:sufficient_earnings) {
      Array.new(12) { BigDecimal.new(500) } + [ BigDecimal.new(5_000) ]
    }
    let(:insufficient_earnings) {
      Array.new(12) { BigDecimal.new(15) } + [ BigDecimal.new(200) ]
    }
    let(:passing_report) { [user_id] + sufficient_earnings   }
    let(:failing_report) { [user_id] + insufficient_earnings }


    it "returns an object with an attribute `user_is_eligible`" do
      calc = DirectAdvance::EarningsReportCalculator.calculate(passing_report, end_date)
      expect(calc).to respond_to(:user_is_eligible)
    end

    it "the `user_is_eligible` attr is a boolean" do
      calc = DirectAdvance::EarningsReportCalculator.calculate(passing_report, end_date)
      expect(calc.user_is_eligible).to be_in ([true, false])
    end

    context "sets user_is_eligible to false when any one of the checks fail" do
      subject { DirectAdvance::EarningsReportCalculator.new(passing_report, end_date) }

      it "sets it false when no_zero_earnings_months? fails" do
        allow(subject).to receive(:no_zero_earnings_months?).and_return(false)
        subject.calculate
        expect(subject.user_is_eligible).to eq false
      end

      it "sets it false when min_annual_earnings_met? fails" do
        allow(subject).to receive(:min_annual_earnings_met?).and_return(false)
        subject.calculate
        expect(subject.user_is_eligible).to eq false
      end

      it "sets it false when avg_monthly_earnings_met? fails" do
        allow(subject).to receive(:avg_monthly_earnings_met?).and_return(false)
        subject.calculate
        expect(subject.user_is_eligible).to eq false
      end
    end

    context "no_zero_earnings_months? check" do
      it "returns false if any month's earning are 0" do
        report = passing_report
        report[5] = BigDecimal.new(0)

        calc = DirectAdvance::EarningsReportCalculator.calculate(report, end_date)
        expect(calc.no_zero_earnings_months?).to eq false
      end
      it "returns false if any month's earning are less than 0" do
        report = passing_report
        report[5] = BigDecimal.new(-1)

        calc = DirectAdvance::EarningsReportCalculator.calculate(report, end_date)
        expect(calc.no_zero_earnings_months?).to eq false
      end

      it "returns true if NO month's earning are 0" do
        calc = DirectAdvance::EarningsReportCalculator.calculate(passing_report, end_date)
        expect(calc.no_zero_earnings_months?).to eq true
      end
    end


    context "min_annual_earnings_met? check" do
      it "returns false if sum of last 12 month's earnings is less than 870" do
        calc = DirectAdvance::EarningsReportCalculator.calculate(failing_report, end_date)
        expect(calc.min_annual_earnings_met?).to eq false
      end

      it "returns true if sum of last 12 month's earnings is greater than 870" do
        calc = DirectAdvance::EarningsReportCalculator.calculate(passing_report, end_date)
        expect(calc.min_annual_earnings_met?).to eq true
      end
    end

    context "avg_monthly_earnings_met? check" do
      it "returns false when adjusted average monthly average earnings is less than $92" do
        calc = DirectAdvance::EarningsReportCalculator.calculate(failing_report, end_date)
        expect(calc.avg_monthly_earnings_met?).to eq false
      end

      it "returns true when adjusted average monthly average earnings is greater than $92" do
        calc = DirectAdvance::EarningsReportCalculator.calculate(passing_report, end_date)
        expect(calc.avg_monthly_earnings_met?).to eq true
      end
    end

    context "saving earning reports" do
      it "saves 13 records for a single person's report (one for each period)" do
        expect {
          DirectAdvance::EarningsReportCalculator.calculate(passing_report, end_date)
        }.to change(DirectAdvance::EarningsPeriodRecord, :count).by(13)
      end

      it "creates a record for each period and names it accordingly" do
        expect(DirectAdvance::EarningsPeriodRecord.count).to eq 0
        DirectAdvance::EarningsReportCalculator.calculate(passing_report, end_date)
        expected_periods = ["month_1", "month_2", "month_3", "month_4", "month_5",
                            "month_6", "month_7", "month_8", "month_9", "month_10",
                            "month_11", "month_12", "all_prev_months"]
        actual_periods = DirectAdvance::EarningsPeriodRecord.pluck(:period)
        expect(actual_periods).to eq expected_periods
      end
    end

    context "saving eligibility report" do
      it "saves a report" do
        expect {
          DirectAdvance::EarningsReportCalculator.calculate(passing_report, end_date)
        }.to change(DirectAdvance::EligibilityRecord, :count).by(1)
      end

      it "correctly saves an eligible report" do
        DirectAdvance::EarningsReportCalculator.calculate(passing_report, end_date)
        expect(DirectAdvance::EligibilityRecord.last.eligible).to eq true
      end

      it "correctly saves an ineligible report" do
        DirectAdvance::EarningsReportCalculator.calculate(failing_report, end_date)
        expect(DirectAdvance::EligibilityRecord.last.eligible).to eq false
      end
    end
  end
end
