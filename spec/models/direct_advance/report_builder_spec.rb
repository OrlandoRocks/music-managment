require "rails_helper"

describe "DirectAdvance::ReportBuilder" do

  describe ".build" do
    let(:person)           { create(:person) }
    let(:report_run_date)  { Date.today.to_s}

    context "given an eligible person and a report_run_date" do

      before(:each) do
        create(:direct_advance_earnings_period_record, :eligible_collection, person: person, report_run_on: report_run_date)
        create(:direct_advance_eligibility_record, :eligible, person: person, report_run_on: report_run_date)
      end

      subject { DirectAdvance::ReportBuilder.build(person, report_run_date) }

      it "returns a report with earnings for 14 periods" do
        expect(subject[:earnings].count).to eq 14
      end

      it "returns a report with eligibility set to true" do
        expect(subject[:eligible]).to eq true
      end
    end

    context "given an INeligible person and a report_run_date" do

      before(:each) do
        create(:direct_advance_earnings_period_record, :ineligible_collection, person: person, report_run_on: report_run_date)
        create(:direct_advance_eligibility_record, :ineligible, person: person, report_run_on: report_run_date)
      end

      subject { DirectAdvance::ReportBuilder.build(person, report_run_date) }

      it "returns a report with earnings for 14 periods" do
        expect(subject[:earnings].count).to eq 14
      end

      it "returns a report with eligibility set to true" do
        expect(subject[:eligible]).to eq false
      end
    end

    context "given a person with no DirectAdvance records and a report_run_date" do

      subject { DirectAdvance::ReportBuilder.build(person, report_run_date) }

      it "returns a report with no earnings" do
        expect(subject[:earnings].count).to eq 0
      end

      it "returns a report with eligibility not set" do
        expect(subject[:eligibile]).to eq nil
      end
    end

  end
end
