require "rails_helper"

describe "DirectAdvance::SalesReportGenerator" do
  describe ".generate" do
    it "initializes instance and invoke generate" do
      expect(DirectAdvance::SalesReportGenerator)
        .to receive(:new)
        .with(1)
        .and_return(DirectAdvance::SalesReportGenerator.new(1))

      expect_any_instance_of(DirectAdvance::SalesReportGenerator).to receive(:generate)

      DirectAdvance::SalesReportGenerator.generate(1)
    end
  end

  describe "#generate" do
    let!(:person) { create(:person) }

    let!(:sip_store) { create(:streaming_sip_store) }
    let!(:srm) { create(:sales_record_master, sip_store: sip_store) }

    it "should set appropriate headers" do
      expected_headers = %w(
        sales_period
        posted_date
        store
        country
        artists
        label
        album_type
        album_id
        album_name
        upc
        release_date
        song_id
        song_name
        track_num
        tunecore_isrc
        optional_isrc
        distribution_type
        units_sold
        rev_per_unit
        amount_earned
      )

      subject = DirectAdvance::SalesReportGenerator.generate(person.id)
      headers = subject.headers

      expect(headers).to match_array(expected_headers)
    end

    it "should return sales records and archives from last 12 month" do
      oldish_srm = create(:sales_record_master, sip_store: sip_store, created_at: 11.months.ago)
      too_old_srm = create(:sales_record_master, sip_store: sip_store, created_at: 13.months.ago)

      sales_record_1 = create(:sales_record_summary, person: person, sales_record_master: srm)
      sales_record_2 = create(:sales_record_summary, person: person, sales_record_master: oldish_srm)
      create(:sales_record_summary, person: person, sales_record_master: too_old_srm)#won't be included

      subject = DirectAdvance::SalesReportGenerator.generate(person.id)
      result = subject.data

      expect(result.length).to eq(2)
      expect(result.first.any?(sales_record_1.related_id)).to be(true)
      expect(result.second.any?(sales_record_2.related_id)).to be(true)
    end

    it "should not return sales records and archives older than 12 month" do
      older_sales_record_master = create(:sales_record_master, sip_store: sip_store, created_at: 13.months.ago)
      create(:sales_record_summary, person: person, sales_record_master: older_sales_record_master)

      subject = DirectAdvance::SalesReportGenerator.generate(person.id)
      result = subject.data

      expect(result.length).to eq(0)
    end

    describe "add_youtube_to_lyric_report feature is turned on" do
      before(:each) do
        allow(FeatureFlipper).to receive(:show_feature?).with(:crt_cache, nil).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).with(:add_youtube_to_lyric_report).and_return(true)
      end

      it "should return you tube records" do
        sales_period_start = "2014-11-01"
        album1 = create(:album, person: person)
        song1 = create(:song, album: album1)
        record_1 = create(:you_tube_royalty_record, person: person, song: song1, sales_period_start: sales_period_start)
        record_2 = create(:you_tube_royalty_record, person: person, song: song1, sales_period_start: sales_period_start)
        album2 = create(:album, person: person)
        song2 = create(:song, album: album2)
        record_3 = create(:you_tube_royalty_record, person: person, song: song2, sales_period_start: sales_period_start)

        subject = DirectAdvance::SalesReportGenerator.generate(person.id)
        result = subject.data

        expect(result.length).to eq(3)
        expect(result.first.any?(record_1.song_id)).to be(true)
        expect(result.second.any?(record_2.song_id)).to be(true)
        expect(result.third.any?(record_3.song_id)).to be(true)
      end
    end

    describe "add_youtube_to_lyric_report feature is turned off" do
      before(:each) do
        allow(FeatureFlipper).to receive(:show_feature?).with(:crt_cache, nil).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).with(:add_youtube_to_lyric_report).and_return(false)
      end

      it "should not return you tube records" do
        sales_record = create(:sales_record_summary, person: person, sales_record_master: srm)
        sales_period_start = "2014-11-01"
        album1 = create(:album, person: person)
        song1 = create(:song, album: album1)
        create(:you_tube_royalty_record, person: person, song: song1, sales_period_start: sales_period_start)
        create(:you_tube_royalty_record, person: person, song: song1, sales_period_start: sales_period_start)
        album2 = create(:album, person: person)
        song2 = create(:song, album: album2)
        create(:you_tube_royalty_record, person: person, song: song2, sales_period_start: sales_period_start)

        subject = DirectAdvance::SalesReportGenerator.generate(person.id)
        result = subject.data

        expect(result.length).to eq(1)
        expect(result.first.any?(sales_record.related_id)).to be(true)
      end
    end

  end
end
