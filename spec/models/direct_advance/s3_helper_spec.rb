require "rails_helper"

describe "DirectAdvance::S3Helper" do

  before(:each) do
    objects    = double("objects")
    allow(objects).to receive(:with_prefix).and_return([@us_report_1, @us_report_2, @us_report_3])
    bucket     = double("bucket", objects: objects)
    s3         = double("s3", buckets: {"lyric-reports" => bucket})
    stub_const("S3_CLIENT", s3)
    @s3_helper = DirectAdvance::S3Helper.new()
  end

  describe ".initialize" do
    describe "s3 bucket path" do
      context "with the s3 bucket rename feature flag on" do
        subject { DirectAdvance::S3Helper.new().path }

        context "production environment" do
          before {
            allow(Rails).to receive(:env).and_return('production')
            allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
          }
          it { is_expected.to match /production/ }
        end

        context "qa environment" do
          before {
            allow(Rails).to receive(:env).and_return('qa')
            allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
          }
          it { is_expected.to match /qa/ }
        end

        context "staging environment" do
          before {
            allow(Rails).to receive(:env).and_return('staging')
            allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
          }
          it { is_expected.to match /staging/ }
        end
      end

      context "with the s3 bucket rename feature flag off" do
        subject { DirectAdvance::S3Helper.new().path }

        context "production environment" do
          before {
            Rails.env.stub(:production? => true)
            allow(FeatureFlipper).to receive(:show_feature?).and_return(false)
          }

          context "test_run is false" do
            it { is_expected.to match /automated_runs/ }
          end

          context "test_run is true" do
            subject { DirectAdvance::S3Helper.new(true).path }

            it { is_expected.to match /test_runs/ }
          end
        end

        context "non production environment" do
          before { allow(FeatureFlipper).to receive(:show_feature?).and_return(false) }
          it { is_expected.to match /test_runs/ }
        end
      end
    end
  end

  describe "#upload_eligible_user_reports" do

    it "writes and uploads user eligibility reports to s3" do
      collector = double("EligibleUsersCollector", us_eligible_users: [1,2,3], non_us_eligible_users: [4,5,6])
      allow(collector).to receive(:eligible_users).and_return([1,2,3,4,5,6])
      end_date = "2017-01-01"
      allow(@s3_helper).to receive(:upload_to_s3)

      expect(@s3_helper).to receive(:write_and_upload).with("eligible_US_users_2017-01-01.csv", [1,2,3])
      expect(@s3_helper).to receive(:write_and_upload).with("eligible_non_US_users_2017-01-01.csv", [4,5,6])
      expect(@s3_helper).to receive(:write_and_upload).with("eligible_users_ALL_2017-01-01.csv", [1,2,3,4,5,6])

      @s3_helper.upload_eligible_user_reports(collector, end_date)
    end
  end

  describe "#upload_comparison_report" do
    it "writes and uploads comparison report for a user segment" do
      allow(@s3_helper).to receive(:upload_to_s3)

      expect(@s3_helper).to receive(:write_and_upload).with("dropped_accounts_US_2017-01-01.csv", [1,2,3,4,5])

      @s3_helper.upload_comparison_report("dropped_accounts", "US", [1,2,3,4,5], "2017-01-01")
    end
  end

  context "selecting files on S3" do

    describe "#most_recent_eligible_users_report" do
      before(:each) do
        @all_report_1 = double("ALL_eligible_users_report_1", key: "eligible_users_ALL_2017-01-01.csv")
        @all_report_2 = double("ALL_eligible_users_report_2", key: "eligible_users_ALL_2017-01-08.csv")
        @all_report_3 = double("ALL_eligible_users_report_3", key: "eligible_users_ALL_2017-01-15.csv")
        objects       = double("objects")
        allow(objects).to receive(:with_prefix).and_return([@all_report_1, @all_report_2, @all_report_3])
        bucket        = double("bucket", objects: objects)
        s3            = double("s3", buckets: {"lyric-reports" => bucket})
        stub_const("S3_CLIENT", s3)

        @s3_helper = DirectAdvance::S3Helper.new()
      end

      it "gets the most recent dated report with prefix 'eligible_users_ALL_'" do
        allow(@all_report_3).to receive(:read).and_return("1,2,3")
        expect(@s3_helper).to receive(:extract_user_ids_from_report).with(@all_report_3)
        @s3_helper.most_recent_eligible_users_report
      end
    end

    context "eligible user report for comparison by segment (US/non_US)" do
      before(:each) do
        @us_report_1      = double("US_eligible_users_report_1", key: "eligible_US_users_2017-01-01.csv")
        @us_report_2      = double("US_eligible_users_report_2", key: "eligible_US_users_2017-01-08.csv")
        @us_report_3      = double("US_eligible_users_report_3", key: "eligible_US_users_2017-01-15.csv")
        @us_report_4      = double("US_eligible_users_report_4", key: "eligible_US_users_2017-01-22.csv")
        objects    = double("objects")
        allow(objects).to receive(:with_prefix).and_return([@us_report_1, @us_report_2, @us_report_3, @us_report_4])
        bucket     = double("bucket", objects: objects)
        s3         = double("s3", buckets: {"lyric-reports" => bucket})
        stub_const("S3_CLIENT", s3)
        @s3_helper = DirectAdvance::S3Helper.new()
      end

      describe "#previous_report_for_segment_and_date" do
        it "gets the latest report run for segment prior to given date" do
          allow(@us_report_2).to receive(:read).and_return("1,2,3")
          expect(@s3_helper).to receive(:extract_user_ids_from_report).with(@us_report_2)
          @s3_helper.previous_report_for_segment_and_date("US", "2017-01-15")
        end
      end
    end
  end
end
