require "rails_helper"

describe DirectAdvance::EligibilityRecord do
  let(:report) { create(:direct_advance_eligibility_record) }

  describe "required attributes" do

    it "person_id" do
      report.person_id = nil
      expect(report.valid?).to eq false
    end

    it "report_run_on" do
      report.report_run_on = nil
      expect(report.valid?).to eq false
    end

    it "eligible" do
      report.eligible = nil
      expect(report.valid?).to eq false
    end
  end

  describe "required attribute types" do

    it "requires report_run_on to be a Date" do
      report.report_run_on = 1
      expect(report.valid?).to eq false
    end
  end

  describe "uniqueness for a person_id and report_run_on" do

    before(:each) do
      @run_date = Date.today
      @person   = create(:person)
      create(:direct_advance_eligibility_record, person: @person, report_run_on: @run_date)
    end

    it "does NOT allow a new record with the same person_id and run date" do
      record = build(:direct_advance_eligibility_record, person: @person, report_run_on: @run_date)
      expect(record).to_not be_valid
    end

    it "does allow a new record with a different person_id and same run date" do
      new_person = create(:person)
      record = build(:direct_advance_eligibility_record, person: new_person, report_run_on: @run_date)
      expect(record).to be_valid
    end

    it "does allow a new record with the same person_id and a different run date" do
      new_date = Date.today + 1.day
      record = build(:direct_advance_eligibility_record, person: @person, report_run_on: new_date)
      expect(record).to be_valid
    end
  end
end
