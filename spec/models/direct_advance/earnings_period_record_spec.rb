require "rails_helper"

describe "DirectAdvance::EarningsPeriodRecord" do
  let(:report) { create(:direct_advance_earnings_period_record) }

  describe "required attributes" do

    it "person_id" do
      report.person_id = nil
      expect(report.valid?).to eq false
    end

    it "amount" do
      report.amount = nil
      expect(report.valid?).to eq false
    end

    it "period" do
      report.period = nil
      expect(report.valid?).to eq false
    end

    it "report_run_on" do
      report.report_run_on = nil
      expect(report.valid?).to eq false
    end
  end

  describe "required attribute types" do

    it "requires report_run_on to be a Date" do
      report.report_run_on = 1
      expect(report.valid?).to eq false
    end
  end

  describe "uniqueness for a person_id, report_run_on and period" do

    before(:each) do
      @run_date = Date.today
      @person   = create(:person)
      @period   = "month_1"
      create(:direct_advance_earnings_period_record, person: @person, report_run_on: @run_date, period: @period)
    end

    it "does NOT allow a new record with the same person_id, run date and period" do
      record = build(:direct_advance_earnings_period_record, person: @person, report_run_on: @run_date, period: @period)
      expect(record).to_not be_valid
    end

    it "does allow a new record with all the same attrs except a different person_id" do
      new_person = create(:person)
      record = build(:direct_advance_earnings_period_record, person: new_person, report_run_on: @run_date, period: @period)
      expect(record).to be_valid
    end

    it "does allow a new record with all the same attrs except a different run_date" do
      new_date = Date.today + 1.day
      record = build(:direct_advance_earnings_period_record, person: @person, report_run_on: new_date, period: @period)
      expect(record).to be_valid
    end

    it "does allow a new record with all the same attrs except a different period" do
      new_period = "month_2"
      record = build(:direct_advance_earnings_period_record, person: @person, report_run_on: @run_date, period: new_period)
      expect(record).to be_valid
    end

  end
end
