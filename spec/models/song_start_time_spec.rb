require "rails_helper"

describe SongStartTime do
  let!(:song_start_time) { build(:song_start_time) }

  context "#valid?" do
    it "should be valid with all relevant fields" do
      expect(song_start_time).to be_valid
    end
  end

  context "invalid" do
    context "song" do
      it "should return invalid if song is nil" do
        song_start_time.song = nil

        expect(song_start_time).to_not be_valid
      end
    end

    context "store" do
      it "should return invalid if store is nil" do
        song_start_time.store = nil

        expect(song_start_time).to_not be_valid
      end
    end

    context "song_id and store_id" do
      it "should return invalid if the song_id and store_id are not unique" do
        dup_song_start_time = create(:song_start_time, store: Store.first, song: Song.first)

        song_start_time.song = dup_song_start_time.song
        song_start_time.store = dup_song_start_time.store

        expect(song_start_time).to_not be_valid
      end
    end

    context "start_time" do
      it "should return invalid if start_time is nil" do
        song_start_time.start_time = nil

        expect(song_start_time).to_not be_valid
      end
    end
  end
end
