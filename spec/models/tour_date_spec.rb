require "rails_helper"

describe TourDate, "when trying to create a new tour date with blank fields" do
  it { is_expected.to validate_presence_of(:person) }
  it { is_expected.to validate_presence_of(:artist) }
  it { is_expected.to validate_presence_of(:country) }
  it { is_expected.to validate_presence_of(:city) }
  it { is_expected.to validate_presence_of(:venue) }
  it { is_expected.to validate_presence_of(:event_date_at) }

  it "should require us state if country selected is 'USA'" do
    td = FactoryBot.build(:tour_date, us_state: nil)
    expect(td).not_to be_valid
  end

  it "should not require us state if country selected is any other than USA" do
    td = FactoryBot.build(:tour_date, country: Country.find_by(name: "Canada"))
    expect(td).to be_valid
  end
end

describe TourDate, "testing event date string entry" do
  it "should allow setting of event date with date like September 3, 2009" do
    td = build(:tour_date)
    td.event_date_at = nil
    td.event_date_at = Time.now.strftime("%B %d, %Y")
    td.valid?
    assert td.valid?
  end

  it "should have an error for bad string date." do
    td = build(:tour_date)
    td.event_date_at = nil
    td.event_date_at = "asdfadsfasd"
    expect(td).not_to be_valid
  end
end

describe TourDate, "test field limitation" do
  it { is_expected.to validate_length_of(:city).is_at_most(50).with_message("exceeded 50 characters") }
  it { is_expected.to validate_length_of(:venue).is_at_most(50).with_message("exceeded 50 characters") }
  it { is_expected.to validate_length_of(:information).is_at_most(500).with_message("exceeded 500 characters") }
end

describe TourDate, "When I've already created 50 tour dates" do
  it "should not create a new one" do
    person = mock_model(Person, :tour_dates =>double(Array, :size => 50))
    td = FactoryBot.build(:tour_date, person: person)
    expect(td).not_to be_valid
  end
end

describe TourDate, "When I enter an old date" do
  it "should not save" do
    td = FactoryBot.build(:tour_date, :event_date_at=>Time.now - 1.day)
    expect(td).not_to be_valid
  end
end

describe TourDate, "When all validations passes" do
  it "should set us state to other if country is any other than USA" do
    other = TCFactory.build_us_state(:name => "OTHER")
    canada = Country.find_by(name: "Canada")
    td = FactoryBot.create(:tour_date, country: canada)
    expect(td).to receive(:set_us_state_to_other)
    td.save
    expect(td).to be_valid
  end
end
