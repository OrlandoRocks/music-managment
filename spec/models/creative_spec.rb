require "rails_helper"

describe Creative do
  before(:each) do
    @album = TCFactory.build_album(:creatives => [{"name"=>"Artist Name", "role"=>"primary_artist"}])
    @creative = @album.creatives.first
  end

  it "should be valid" do
    expect(@creative).to be_valid
  end
end

describe Creative, "when calling create_role" do
  it 'should create only create 1 new artist record even if the artist has multiple roles' do
    album = TCFactory.build_album
    expect { Creative.create_role(album, 'new artist name', 'primary_artist')
             Creative.create_role(album, 'new artist name', 'composer')
           }.to change(Artist, :count).by(1)
  end

  it 'should create one new Creative record for each role' do
    album = TCFactory.build_album
    expect { Creative.create_role(album, 'new artist name', 'primary_artist')
             Creative.create_role(album, 'new artist name', 'composer')
           }.to change(Creative, :count).by(2)
  end

  it 'should create creatives that are attached to the creativeable that was passed in' do
    album = TCFactory.build_album
    expect { Creative.create_role(album, 'new artist name', 'primary_artist')
             Creative.create_role(album, 'new artist name', 'composer')
           }.to change(album.creatives, :count).by(2)
  end

  it 'should only create one new Creative record if the creative role has been previous been created' do
    album = TCFactory.build_album
    expect { Creative.create_role(album, 'new artist name', 'composer')
             Creative.create_role(album, 'new artist name', 'composer')
           }.to change(Creative, :count).by(1)
  end
end

describe Creative, "when being validated" do
  before(:each) do
    @album = TCFactory.build_album(:creatives => [{"name"=>"Artist Name", "role"=>"primary_artist"}])
  end

  it "should make sure that the role field doesn't accept garbage data" do
    expect{Creative.create_role(@album, 'new artist name', 'not a role')}.not_to change{Creative.count}
  end

  it "should raise an error if the role is changed to garbage data after it has been succesfully created" do
    x = @album.creatives.first #there should only be one automatically created creative
    x.role = 'not a role'
    expect(x.save).to be_falsey
    expect(x.errors.messages[:role].size).to eq 1
  end
end

describe Creative, "when :selected_artist_name is used on an album" do
  before(:each) do
    @album = TCFactory.build_album(:creatives => [{"name"=>"Artist Name", "role"=>"primary_artist"}])
  end

  it 'prevents duplicate instances of a creative_role / artist_id' do
    expect(@album.creatives.size).to eq(1)
    expect(@album.creatives.first.artist.id).to eq(@album.artist.id)

    creative_params = @album.creatives.map do |creative|
      {
        id: creative[:id],
        role: creative[:role],
        name: creative.artist.name
      }.with_indifferent_access
    end

    creative_params << {
                         id: @album.creatives.first[:id],
                         role: "primary_artist", name: "Artist Name"
                       }.with_indifferent_access

    expect(creative_params.size).to eq(2)

    @album.creatives = creative_params
    @album.save

    expect(@album.creatives.size).to eq(1)
    expect(@album.creatives.first.artist.id).to eq(@album.artist.id)
  end
end

describe Creative, "when a song is asked about album level creatives (normal album)" do
  before(:each) do
    @album = TCFactory.build_album(:creatives => [{"name"=>"Primary Artist", "role"=>"primary_artist"}])
    @song = Song.create!(:name => 'I Bet You Look Good On The Dancefloor', :album => @album)
  end

  it 'should have the same artist_name for songs and albums' do
    expect(@song.artist_name).to eq(@album.artist_name)
    expect(@song.artist).to eq(@album.artist)
  end
end

describe Creative, "when the module 'creativeable' deals with adding and returning various creatives" do
  before(:each) do
    @album = TCFactory.build_album(:creatives => [{"name"=>"Primary Artist", "role"=>"primary_artist"}])
    @song = Song.create!(:name => 'I Bet You Look Good On The Dancefloor', :album => @album)
  end

  it "should allow #add_primary_artist to add a new creative role" do
    expect{ @album.add_primary_artist('Cool New Artist') }.to change{@album.creatives.size}.by(1)
    expect(@album.primary_artists.size).to eq(2)
    expect(@album.primary_artists.collect{|x| x.name}.join(', ')).to eq('Primary Artist, Cool New Artist')
  end

  it "should allow #primary_artists to return an array of Creatives" do
    @album.add_primary_artist('Cool New Artist')
    @album.primary_artists.each{|x| expect(x).to be_instance_of(Creative)}
  end

  it "should allow #add_featured_artist to add a new featured artist" do
    expect{ @album.add_featured_artist('Cool New Artist') }.not_to raise_error
  end

  it "should only return the featured artist" do
    expect{ @album.add_featured_artist('Cool New Artist') }.to change{@album.creatives.size}.by(1)
    expect(@album.creatives.size).to eq(2)
    expect(@album.featured_artists.size).to eq(1)
    expect(@album.featured_artists.first.name).to eq('Cool New Artist')
  end

  it "should allow #featured_artists to return an array of featured artists" do
    @album.add_featured_artist('Cool New Artist')
    expect(@album.featured_artists).to be_instance_of(Array)
  end

  it "should return an empty array if no featured artists are set" do
    expect(@album.featured_artists).to be_instance_of(Array)
    expect(@album.featured_artists.size).to eq(0)
  end

  it "should not create duplicate creative roles" do
    expect{ @album.add_featured_artist('Cool New Artist');
            @album.add_featured_artist('Cool New Artist')}.to change{@album.featured_artists.size}.by(1)
  end

  it "should break off _artist when calling role_for_petri" do
    expect(@album.primary_artists.first.role_for_petri).to eq('primary')
  end

  it "should return the string composer for items that don't have _artist attached" do
    @album.add_composer('marcus')
    expect(@album.composers.first.role_for_petri).to eq('composer')
  end

  describe ".get_apple_artist_id" do
    context "artist puts apple ID in release 1, but not other releases" do
      it "returns the artist's apple id in release 1" do
        album1 = create(:album)
        creative1 = create(:creative, creativeable: album1)
        artist = creative1.artist
        esi = create(:external_service_id, linkable_id: creative1.id, linkable_type: "Creative", service_name: "apple", identifier: "12345")

        album2 = create(:album)
        creative2 = create(:creative, creativeable: album2, artist: artist)

        expect(creative1.get_apple_artist_id).to eq("12345")
        expect(creative2.get_apple_artist_id).to be nil
      end
    end

    context "artist puts apple ID in release 1 and a different apple ID in other releases" do
      it "returns the artist's apple id in latest release" do
        album1 = create(:album)
        creative1 = create(:creative, creativeable: album1)
        artist = creative1.artist
        esi = create(:external_service_id, linkable_id: creative1.id, linkable_type: "Creative", service_name: "apple", identifier: "12345")

        album2 = create(:album)
        creative2 = create(:creative, creativeable: album2, artist: artist)
        esi = create(:external_service_id, linkable_id: creative2.id, linkable_type: "Creative", service_name: "apple", identifier: "123456")

        expect(creative1.get_apple_artist_id).to eq("12345")
        expect(creative2.get_apple_artist_id).to eq("123456")
      end
    end

    context "artist does not have an apple artist ID" do
      it "returns nothing" do
        album1 = create(:album)
        creative1 = create(:creative, creativeable: album1)
        artist = creative1.artist

        album2 = create(:album)
        creative2 = create(:creative, creativeable: album2, artist: artist)

        expect(creative1.get_apple_artist_id).to eq(nil)
        expect(creative2.get_apple_artist_id).to eq(nil)
      end
    end
  end
end

describe Creative do
  describe "#external_service_id_for" do
    it "returns the identifier for the given service" do
      album = create(:album)
      creative = create(:creative, creativeable: album)
      esi = create(:external_service_id, linkable_id: creative.id, linkable_type: "Creative", service_name: "apple", identifier: "12345")

      expect(creative.external_id_for("apple")).to eq("12345")
    end
  end

  describe "without_youtube_oac" do
    it "filters out creatives with a youtube oac" do
      album1 = create(:album)

      creative1 = create(:creative, creativeable: album1)
      create(:external_service_id, linkable_id: creative1.id, linkable_type: "Creative", service_name: "youtube_channel_id", identifier: "12345")

      creative2 = create(:creative, creativeable: album1)
      create(:external_service_id, linkable_id: creative2.id, linkable_type: "Creative", service_name: "apple", identifier: "123456")

      expect(Creative.without_youtube_oac.map(&:id)).not_to include creative1.id
      expect(Creative.without_youtube_oac.map(&:id)).to include creative2.id
    end
  end

  describe "#existing_external_ids_on_acct" do
    it "returns any external ids on the person with the same artist name" do
      eid = create(:external_service_id, :apple_artist_id)

      creative = create(:creative, creativeable: eid.linkable.album, person: eid.linkable.person, artist: eid.linkable.artist)

      expect(creative.existing_external_id_on_acct('apple')).to eq(eid.identifier)
    end
  end
end
