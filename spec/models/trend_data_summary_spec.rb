require "rails_helper"

describe TrendDataSummary, "retrieving all trend data" do
  fixtures :albums

  before(:each) do
    @album = Album.first
    @zip = Zipcode.first

    tdd1 = create(:trend_data_detail, {:zip_code => @zip.zipcode, :country_code => "US", :sale_date => Date.today, :album => @album})
    tds1 = create(:trend_data_summary, {:trend_data_detail => tdd1})

    tdd2 = create(:trend_data_detail, {:zip_code => @zip.zipcode, :sale_date => Date.today-2.days, :album=>@album})
    tds2 = create(:trend_data_summary, {:trend_data_detail => tdd2})

    tdd3 = create(:trend_data_detail, {:zip_code => @zip.zipcode, :sale_date => Date.today-4.days, :album=>@album})
    tds3 = create(:trend_data_summary, {:trend_data_detail => tdd3})
  end

  it "should return only data in date range" do
    results, count = TrendDataSummary.all_trend_data(@album.person, [Provider.first.id], Date.today - 1.day, Date.today + 1.day)
    expect(results).not_to be_empty

    expect(results.first.album_name).to eq(@album.name)
    expect(results.first.state).to eq(@zip.state)
    expect(results.first.city).to  eq(@zip.city_mixed_case)
    expect(results.first.country_name).to eq("United States")
    expect(results.first.sale_date).to eq(Date.today)

    results,count = TrendDataSummary.all_trend_data(@album.person, [Provider.first.id], Date.today - 3.day, Date.today - 1.day)
    expect(results).not_to be_empty
    expect(results.size).to eq(1)
    expect(count).to eq(1)
    expect(results.first.sale_date).to eq(Date.today - 2.days)

    results,count = TrendDataSummary.all_trend_data(@album.person, [Provider.first.id], Date.today - 5.day, Date.today - 3.day)
    expect(results).not_to be_empty
    expect(results.size).to eq(1)
    expect(count).to eq(1)
    expect(results.first.sale_date).to eq(Date.today - 4.days)
  end

  it "should return all data for a given release in date range" do
    various_album = albums(:various_album)
    expect(@album).not_to eq(various_album)
    tdd1 = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => various_album})
    tds1 = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd1})
    tdd1.trend_data_summary_id = tds1.id
    tdd1.save!

    results,count = TrendDataSummary.all_trend_data(@album.person, [Provider.first.id], Date.today - 1.day, Date.today + 1.day, :album=>@album )
    expect(results.size).to eq(1)
    expect(count).to eq(1)
    expect(results.first.album_name).to eq(@album.name)

    results,count = TrendDataSummary.all_trend_data(various_album.person, [Provider.first.id], Date.today - 1.day, Date.today + 1.day, :album=>various_album )
    expect(results.size).to eq(1)
    expect(count).to eq(1)
    expect(results.first.album_name).to eq(various_album.name)
  end

  it "return all data for a given song in a date range" do
    #Get an album with songs
    @album = Album.select("albums.*, count(songs.id) as count").joins("inner join songs on songs.album_id = albums.id").group("albums.id HAVING count >= 2").first
    @song1 = @album.songs.first
    @song2 = @album.songs.last

    tdd1 = create(:trend_data_detail, {:sale_date => Date.today, :album => @album, :song=>@song1})
    tds1 = create(:trend_data_summary, {:trend_data_detail => tdd1})

    tdd2 = create(:trend_data_detail, {:sale_date => Date.today, :album => @album, :song=>@song2})
    tds2 = create(:trend_data_summary, {:trend_data_detail => tdd2})

    results,count = TrendDataSummary.all_trend_data(@album.person, [Provider.first.id], Date.today - 1.day, Date.today + 1.day, :album=>@album, :song=>@song1 )
    expect(results.size).to eq(1)
    expect(count).to eq(1)
    expect(results.first.song_name).to eq(@song1.name)

    results,count = TrendDataSummary.all_trend_data(@album.person, [Provider.first.id], Date.today - 1.day, Date.today + 1.day, :album=>@album, :song=>@song2 )
    expect(results.size).to eq(1)
    expect(count).to eq(1)
    expect(results.first.song_name).to eq(@song2.name)
  end
end

describe TrendDataSummary, "retrieving top" do
  fixtures :albums

  before(:each) do
    @album1 = Album.all[0]
    @album2 = Album.all[2]
    @album3 = Album.all[3]

    zipcodes = []
    zipcodes << create(:zipcode, :zipcode=>"11215", :city=>"Brooklyn", :state=>"NY", :cbsa_name=>"New York-Northern New Jersey-Long Island NY-NJ-PA", :cbsa_short_name=>"New York NY", :cbsa=>1)
    zipcodes << create(:zipcode, :zipcode=>"11766", :city=>"Mt Sinai", :state=>"NY", :cbsa_name=>"New York-Northern New Jersey-Long Island NY-NJ-PA", :cbsa_short_name=>"New York NY", :cbsa=>1)
    zipcodes << create(:zipcode, :zipcode=>"00001", :city=>"Philadelphia", :state=>"PA", :cbsa_name=>"Philadelphia", :cbsa_short_name=>"Philadelphia", :cbsa=>2)

    tdd1 = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => @album1, :country_code => 'MY', :zip_code => "abcde"})
    tds1 = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd1})
    tdd1.trend_data_summary_id = tds1.id
    tdd1.save!
    TCFactory.create(:geo, {:date => tdd1.sale_date, :trend_data_summary_id => tds1.id, :country_code => tdd1.country_code})

    tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => @album2, :country_code => 'CA', :zip_code => "edcba", :qty => 2})
    tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd})
    tdd.trend_data_summary_id = tds.id
    tdd.save!
    TCFactory.create(:geo, {:date => tdd.sale_date, :trend_data_summary_id => tds.id, :country_code => tdd.country_code, :qty => 2})

    tdds = []
    zipcodes.each do |zipcode|
      tdds << TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => @album3, :country_code => 'US', :zip_code => zipcode.zipcode})
    end

    tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdds[0], :qty => 3})

    tdds.each do |tdd|
      tdd.trend_data_summary_id = tds.id
      tdd.save!
    end

    TCFactory.create(:geo, {:date => tdd.sale_date, :trend_data_summary_id => tds.id, :country_code => 'US', :cbsa => zipcodes[0].cbsa, :qty => 2})
    TCFactory.create(:geo, {:date => tdd.sale_date, :trend_data_summary_id => tds.id, :country_code => 'US', :cbsa => zipcodes[2].cbsa, :qty => 1})
  end

  describe "releases" do
    it "should return top selling releases" do
      top_releases = TrendDataSummary.top_downloaded_releases(@album1.person, [Provider.first.id], Date.today - 3.days, Date.today)

      expect(top_releases.size).to eq(3)
      expect(top_releases[0][:name]).to eq(@album3.name)
      expect(top_releases[0][:album_qty].to_i).to eq(3)

      expect(top_releases[1][:name]).to eq(@album2.name)
      expect(top_releases[1][:album_qty].to_i).to eq(2)

      expect(top_releases[2][:name]).to eq(@album1.name)
      expect(top_releases[2][:album_qty].to_i).to eq(1)
    end
  end

  describe "countries" do
    it "should return top selling countries" do
      top_countries = TrendDataSummary.top_downloaded_countries(@album1.person, [Provider.first.id], Date.today - 3.days, Date.today)

      expect(top_countries.size).to eq(3)
      expect(top_countries[0][:iso_code]).to eq("US")
      expect(top_countries[0][:qty].to_i).to eq(3)

      expect(top_countries[1][:iso_code]).to eq("CA")
      expect(top_countries[1][:qty].to_i).to eq(2)

      expect(top_countries[2][:iso_code]).to eq("MY")
      expect(top_countries[2][:qty].to_i).to eq(1)
    end

    it "should return top selling countries for an album" do
      top_countries = TrendDataSummary.top_downloaded_countries(@album2.person, [Provider.first.id], Date.today - 3.days, Date.today, :album=>@album2)
      expect(top_countries.size).to eq(1)
      expect(top_countries[0][:iso_code]).to eq("CA")
      expect(top_countries[0][:qty].to_i).to eq(2)
    end

    it "should return top selling countries for a song" do
      #Get an album with songs
      @album = Album.select("albums.*, count(songs.id) as count").joins("inner join songs on songs.album_id = albums.id").group("albums.id HAVING count >= 2").first
      @song1 = @album.songs.first
      @song2 = @album.songs.last

      tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => @album, :song => @song1, :country_code => 'US', :zip_code => "11238"})
      tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd})
      TCFactory.create(:geo, {:date => tdd.sale_date, :trend_data_summary_id => tds.id, :country_code => tdd.country_code})

      tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => @album, :song => @song2, :country_code => 'CA', :zip_code => "abcde"})
      tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd})
      TCFactory.create(:geo, {:date => tdd.sale_date, :trend_data_summary_id => tds.id, :country_code => tdd.country_code})

      top_countries = TrendDataSummary.top_downloaded_countries(@album2.person, [Provider.first.id], Date.today - 3.days, Date.today, :album=>@album, :song=>@song1)
      expect(top_countries.size).to eq(1)
      expect(top_countries[0][:iso_code]).to eq("US")
      expect(top_countries[0][:qty].to_i).to eq(1)

      top_countries = TrendDataSummary.top_downloaded_countries(@album2.person, [Provider.first.id], Date.today - 3.days, Date.today, :album=>@album, :song=>@song2)
      expect(top_countries.size).to eq(1)
      expect(top_countries[0][:iso_code]).to eq("CA")
      expect(top_countries[0][:qty].to_i).to eq(1)
    end

  end

  describe "us markets" do
    it "should return top selling us markets" do
      top_markets = TrendDataSummary.top_us_markets(@album1.person, Date.today - 3.days, Date.today)

      expect(top_markets.size).to eq(2)
      expect(top_markets[0][:cbsa_name]).to eq("New York-Northern New Jersey-Long Island NY-NJ-PA")
      expect(top_markets[1][:cbsa_name]).to eq("Philadelphia")
    end

    it "should return top selling us markets for an album" do
      top_markets = TrendDataSummary.top_us_markets(@album3.person, Date.today - 3.days, Date.today, :album => @album3)
      expect(top_markets.size).to eq(2)
      expect(top_markets[0][:cbsa_name]).to eq("New York-Northern New Jersey-Long Island NY-NJ-PA")
      expect(top_markets[0][:qty].to_i).to eq(2)
    end

    it "should return top selling us markets for a song" do
      #Get an album with songs
      @album = Album.select("albums.*, count(songs.id) as count").joins("inner join songs on songs.album_id = albums.id").group("albums.id HAVING count >= 2").first
      @song1 = @album.songs.first
      @song2 = @album.songs.last

      tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => @album, :song => @song1, :country_code => 'US', :zip_code => "11238"})
      tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd})
      TCFactory.create(:geo, {:date => tdd.sale_date, :trend_data_summary_id => tds.id, :country_code => tdd.country_code})

      tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => @album, :song => @song2, :country_code => 'US', :zip_code => "00001"})
      tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd})
      TCFactory.create(:geo, {:date => tdd.sale_date, :trend_data_summary_id => tds.id, :country_code => tdd.country_code, :cbsa => '2'})

      top_markets = TrendDataSummary.top_us_markets(@album.person, Date.today - 3.days, Date.today, :album=>@album, :song=>@song1)
      expect(top_markets.size).to eq(1)
      expect(top_markets[0][:cbsa_name]).to eq("New York-Northern New Jersey-Long Island NY-NJ-PA")

      top_markets = TrendDataSummary.top_us_markets(@album.person, Date.today - 3.days, Date.today, :album=>@album, :song=>@song2)
      expect(top_markets.size).to eq(1)
      expect(top_markets[0][:cbsa_name]).to eq("Philadelphia")
    end

  end

  describe "songs" do
    before(:each) do
      #Get an album with songs
      @album = Album.find(1062)
      @song1 = @album.songs.first
      @song2 = @album.songs.last

      tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => @album, :song => @song1, :qty => 2, :trans_type_id => TransType.song_download.id})
      tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd, :qty => tdd.qty})
      tdd.trend_data_summary_id = tds.id
      tdd.save!

      tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => @album, :song => @song2, :trans_type_id => TransType.song_download.id})
      tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd})
      tdd.trend_data_summary_id = tds.id
      tdd.save!

      @album2 = Album.select("albums.*, count(songs.id) as count").joins("inner join songs on songs.album_id = albums.id").where("albums.id != #{@album.id}").group("albums.id HAVING count >= 2").first
      @song3 = @album2.songs.first

      tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => @album2, :song => @song3, :trans_type_id => TransType.song_download.id})
      tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd})
      tdd.trend_data_summary_id = tds.id
      tdd.save!

    end

    it "should return top selling songs" do
      top_songs = TrendDataSummary.top_downloaded_songs(@album.person, [Provider.first.id], Date.today - 3.days, Date.today)

      expect(top_songs.size).to eq(3)
      expect(top_songs[0][:id].to_i).to eq(@song1.id)
      expect(top_songs[0][:qty].to_i).to eq(2)
      expect(top_songs[0][:album_id].to_i).to eq(@song1.album_id)
      expect(top_songs[0][:album_name]).to eq(@song1.album.name)
    end

    it "should return top selling songs for an album" do
      top_songs = TrendDataSummary.top_downloaded_songs(@album.person, [Provider.first.id], Date.today - 3.days, Date.today, :album=>@album)

      expect(top_songs.size).to eq(2)
      expect(top_songs[0][:id].to_i).to eq(@song1.id)
      expect(top_songs[0][:qty].to_i).to eq(2)

      top_songs = TrendDataSummary.top_downloaded_songs(@album2.person, [Provider.first.id], Date.today - 3.days, Date.today, :album=>@album2)

      expect(top_songs.size).to eq(1)
      expect(top_songs[0][:id].to_i).to eq(@song3.id)
      expect(top_songs[0][:qty].to_i).to eq(1)
    end
  end

  describe "top_ringtones" do
    it "should return top selling ringtones" do
      @ringtone1 = albums(:ringtone)
      @ringtone2 = albums(:ringtone2)

      tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => @ringtone1, :song => @ringtone1, :trans_type_id => TransType.ringtone.id, :qty => 2})
      tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd, :qty => tdd.qty})
      tdd.trend_data_summary_id = tds.id
      tdd.save!

      tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => @ringtone2, :song => @ringtone2, :trans_type_id => TransType.ringtone.id})
      tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd})
      tdd.trend_data_summary_id = tds.id
      tdd.save!

      top_ringtones = TrendDataSummary.top_ringtones(@ringtone1.person, [Provider.first.id], Date.today - 2.months, Date.today )

      expect(top_ringtones.size).to eq(2)
      expect(top_ringtones.first[:name]).to eq(@ringtone1.name)
      expect(top_ringtones.first[:ringtone_qty].to_i).to eq(2)

      expect(top_ringtones.last[:name]).to  eq(@ringtone2.name)
      expect(top_ringtones.last[:ringtone_qty].to_i).to eq(1)
    end
  end
end

describe TrendDataSummary, "retrieving streaming top" do
  fixtures :albums

  before(:each) do
    @album1 = Album.all[0]
    @album2 = Album.all[2]
    @album3 = Album.all[3]

    zipcodes = []
    zipcodes << create(:zipcode, :zipcode=>"11215", :city=>"Brooklyn", :state=>"NY", :cbsa_name=>"New York-Northern New Jersey-Long Island NY-NJ-PA", :cbsa_short_name=>"New York NY", :cbsa=>1)
    zipcodes << create(:zipcode, :zipcode=>"11766", :city=>"Mt Sinai", :state=>"NY", :cbsa_name=>"New York-Northern New Jersey-Long Island NY-NJ-PA", :cbsa_short_name=>"New York NY", :cbsa=>1)
    zipcodes << create(:zipcode, :zipcode=>"00001", :city=>"Philadelphia", :state=>"PA", :cbsa_name=>"Philadelphia", :cbsa_short_name=>"Philadelphia", :cbsa=>2)

    tdd1 = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => @album1, :country_code => 'MY', :zip_code => "abcde", :trans_type_id => TransType.stream.id})
    tds1 = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd1})
    tdd1.trend_data_summary_id = tds1.id
    tdd1.save!
    TCFactory.create(:geo, {:date => tdd1.sale_date, :trend_data_summary_id => tds1.id, :country_code => tdd1.country_code})

    tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => @album2, :country_code => 'CA', :zip_code => "edcba", :qty => 2, :trans_type_id => TransType.stream.id})
    tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd})
    tdd.trend_data_summary_id = tds.id
    tdd.save!
    TCFactory.create(:geo, {:date => tdd.sale_date, :trend_data_summary_id => tds.id, :country_code => tdd.country_code, :qty => 2})

    tdds = []
    (0...3).each do |i|
      tdds << TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => @album3, :country_code => 'US', :zip_code => zipcodes[i].zipcode, :trans_type_id => TransType.stream.id})
    end

    tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdds[0], :qty => 3})

    (0...3).each do |i|
      tdds[i].trend_data_summary_id = tds.id
      tdds[i].save!
    end

    TCFactory.create(:geo, {:date => tdd.sale_date, :trend_data_summary_id => tds.id, :country_code => 'US', :cbsa => zipcodes[0].cbsa, :qty => 2})
    TCFactory.create(:geo, {:date => tdd.sale_date, :trend_data_summary_id => tds.id, :country_code => 'US', :cbsa => zipcodes[2].cbsa, :qty => 1})
  end

  describe "releases" do
    it "should return top streamed releases" do
      top_releases = TrendDataSummary.top_streamed_releases(@album1.person, [Provider.first.id], Date.today - 3.days, Date.today)

      expect(top_releases.size).to eq(3)
      expect(top_releases[0][:name]).to eq(@album3.name)
      expect(top_releases[0][:stream_qty].to_i).to eq(3)

      expect(top_releases[1][:name]).to eq(@album2.name)
      expect(top_releases[1][:stream_qty].to_i).to eq(2)

      expect(top_releases[2][:name]).to eq(@album1.name)
      expect(top_releases[2][:stream_qty].to_i).to eq(1)
    end
  end

  describe "countries" do
    it "should return top selling countries" do
      top_countries = TrendDataSummary.top_streamed_countries(@album1.person, [Provider.first.id], Date.today - 3.days, Date.today)

      expect(top_countries.size).to eq(3)
      expect(top_countries[0][:iso_code]).to eq("US")
      expect(top_countries[0][:qty].to_i).to eq(3)

      expect(top_countries[1][:iso_code]).to eq("CA")
      expect(top_countries[1][:qty].to_i).to eq(2)

      expect(top_countries[2][:iso_code]).to eq("MY")
      expect(top_countries[2][:qty].to_i).to eq(1)
    end

    it "should return top selling countries for an album" do
      top_countries = TrendDataSummary.top_streamed_countries(@album2.person, [Provider.first.id], Date.today - 3.days, Date.today, :album=>@album2)
      expect(top_countries.size).to eq(1)
      expect(top_countries[0][:iso_code]).to eq("CA")
      expect(top_countries[0][:qty].to_i).to eq(2)
    end

    it "should return top selling countries for a song" do
      #Get an album with songs
      @album = Album.select("albums.*, count(songs.id) as count").joins("inner join songs on songs.album_id = albums.id").group("albums.id HAVING count >= 2").first
      @song1 = @album.songs.first
      @song2 = @album.songs.last

      tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => @album, :song => @song1, :country_code => 'US', :zip_code => "11238", :trans_type_id => TransType.stream.id})
      tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd})
      TCFactory.create(:geo, {:date => tdd.sale_date, :trend_data_summary_id => tds.id, :country_code => tdd.country_code})

      tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => @album, :song => @song2, :country_code => 'CA', :zip_code => "abcde", :trans_type_id => TransType.stream.id})
      tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd})
      TCFactory.create(:geo, {:date => tdd.sale_date, :trend_data_summary_id => tds.id, :country_code => tdd.country_code})

      top_countries = TrendDataSummary.top_streamed_countries(@album2.person, [Provider.first.id], Date.today - 3.days, Date.today, :album=>@album, :song=>@song1)
      expect(top_countries.size).to eq(1)
      expect(top_countries[0][:iso_code]).to eq("US")
      expect(top_countries[0][:qty].to_i).to eq(1)

      top_countries = TrendDataSummary.top_streamed_countries(@album2.person, [Provider.first.id], Date.today - 3.days, Date.today, :album=>@album, :song=>@song2)
      expect(top_countries.size).to eq(1)
      expect(top_countries[0][:iso_code]).to eq("CA")
      expect(top_countries[0][:qty].to_i).to eq(1)
    end

  end

  describe "songs" do
    before(:each) do
      #Get an album with songs
      @album = Album.find(1062)
      @song1 = @album.songs.first
      @song2 = @album.songs.last

      tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => @album, :song => @song1, :qty => 2, :trans_type_id => TransType.stream.id})
      tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd, :qty => tdd.qty})
      tdd.trend_data_summary_id = tds.id
      tdd.save!

      tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => @album, :song => @song2, :trans_type_id => TransType.stream.id})
      tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd})
      tdd.trend_data_summary_id = tds.id
      tdd.save!

      @album2 = Album.select("albums.*, count(songs.id) as count").joins("inner join songs on songs.album_id = albums.id").where("albums.id != #{@album.id}").group("albums.id HAVING count >= 2").first
      @song3 = @album2.songs.first

      tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => @album2, :song => @song3, :trans_type_id => TransType.stream.id})
      tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd})
      tdd.trend_data_summary_id = tds.id
      tdd.save!
    end

    it "should return top streamed songs" do
      top_songs = TrendDataSummary.top_streamed_songs(@album.person, [Provider.first.id], Date.today - 3.days, Date.today)

      expect(top_songs.size).to eq(4)
      expect(top_songs[1][:id].to_i).to eq(@song1.id)
      expect(top_songs[1][:qty].to_i).to eq(2)
      expect(top_songs[1][:album_id].to_i).to eq(@song1.album_id)
      expect(top_songs[1][:album_name]).to eq(@song1.album.name)
    end

    it "should return top streamed songs for an album" do
      top_songs = TrendDataSummary.top_streamed_songs(@album.person, [Provider.first.id], Date.today - 3.days, Date.today, :album=>@album)

      expect(top_songs.size).to eq(3)
      expect(top_songs[1][:id].to_i).to eq(@song1.id)
      expect(top_songs[1][:qty].to_i).to eq(2)

      top_songs = TrendDataSummary.top_streamed_songs(@album2.person, [Provider.first.id], Date.today - 3.days, Date.today, :album=>@album2)

      expect(top_songs.size).to eq(1)
      expect(top_songs[0][:id].to_i).to eq(@song3.id)
      expect(top_songs[0][:qty].to_i).to eq(1)
    end
  end
end

describe TrendDataSummary, "retrieving trend data counts" do
  fixtures :albums
  before(:each) do
    @album = Album.first

    tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album=>@album})
    tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd})

    tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today - 1.months, :album=>@album, :qty => 2})
    tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd, :qty => tdd.qty})

    tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today - 4.months, :album=>@album})
    tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd})
  end

  it "should return trend data counts by day" do
    #Last week should only return one
    results = TrendDataSummary.trend_data_counts_by_day(@album.person, [Provider.first.id], Date.today-1.week, Date.today)
    expect(results.size).to eq(1)
    expect(results[0]["sale_date"]).to eq(Date.today)
    expect(results[0]["quantity"].to_i).to eq(1)

    #Last three months should return 2
    results = TrendDataSummary.trend_data_counts_by_day(@album.person, [Provider.first.id], Date.today-3.months, Date.today)
    expect(results.size).to eq(2)
    expect(results[1]["sale_date"]).to eq(Date.today)
    expect(results[1]["quantity"].to_i).to eq(1)
    expect(results[0]["sale_date"]).to eq(Date.today - 1.months)
    expect(results[0]["quantity"].to_i).to eq(2)

    #Last 6 months should return 3
    results = TrendDataSummary.trend_data_counts_by_day(@album.person, [Provider.first.id], Date.today-6.months, Date.today)
    expect(results.size).to eq(3)
    expect(results[2]["sale_date"]).to eq(Date.today)
    expect(results[2]["quantity"].to_i).to eq(1)
    expect(results[1]["sale_date"]).to eq(Date.today - 1.months)
    expect(results[1]["quantity"].to_i).to eq(2)
    expect(results[0]["sale_date"]).to eq(Date.today - 4.months)
    expect(results[0]["quantity"].to_i).to eq(1)
  end

  it "should return trend data counts for an album by day" do
    various_album = albums(:various_album)
    expect(@album).not_to eq(various_album)

    tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => various_album})
    tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd})

    results = TrendDataSummary.trend_data_counts_by_day(@album.person, [Provider.first.id], Date.today-1.week, Date.today, :album=>@album )
    expect(results.size).to eq(1)

    results = TrendDataSummary.trend_data_counts_by_day(various_album.person, [Provider.first.id], Date.today-1.week, Date.today, :album=>various_album )
    expect(results.size).to eq(1)

    results = TrendDataSummary.trend_data_counts_by_day(various_album.person, [Provider.first.id], Date.today-1.month, Date.today-1.week, :album=>various_album )
    expect(results.size).to eq(0)
  end

  it "should return trend data counts for an album and a song by date" do
    #Get an album with songs
    @album = Album.select("albums.*, count(songs.id) as count").joins("inner join songs on songs.album_id = albums.id").group("albums.id HAVING count >= 2").first
    @song1 = @album.songs.first
    @song2 = @album.songs.last

    tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => @album, :song => @song1, :qty => 2})
    tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd, :qty => tdd.qty})

    tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :album => @album, :song => @song2})
    tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd})

    results = TrendDataSummary.trend_data_counts_by_day(@album.person, [Provider.first.id], Date.today-1.week, Date.today, :album=>@album, :song=>@song1 )
    expect(results.size).to eq(1)
    expect(results.first[:quantity].to_i).to eq(2)

    results = TrendDataSummary.trend_data_counts_by_day(@album.person, [Provider.first.id], Date.today-1.week, Date.today, :album=>@album, :song=>@song2 )
    expect(results.size).to eq(1)
    expect(results.first[:quantity].to_i).to eq(1)
  end

end

describe TrendDataSummary do
  fixtures :albums

  describe "#ringtones_sold?" do
    it "should return true if there is ringtone trend data in the date period" do
      tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today - 3.days})
      tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd})

      expect(TrendDataSummary.ringtones_sold?(tdd.album.person, [Provider.first.id], Date.today - 1.month, Date.today + 1.week)).to be_falsey

      tdd = TCFactory.create(:trend_data_detail, {:sale_date => Date.today, :trans_type_id => TransType.ringtone.id})
      tds = TCFactory.create(:trend_data_summary, {:trend_data_detail => tdd})

      expect(TrendDataSummary.ringtones_sold?(tdd.album.person, [Provider.first.id], Date.today - 1.month, Date.today + 1.week)).to be_truthy
    end
  end
end
