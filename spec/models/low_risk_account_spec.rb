require 'rails_helper'

RSpec.describe LowRiskAccount do
  describe "#save" do
    it "cannot add the same person twice" do
      person = create(:person)

      LowRiskAccount.create!(person_id: person.id)

      expect do
        LowRiskAccount.create!(person_id: person.id)
      end.to raise_error(ActiveRecord::RecordNotUnique)
    end
  end
end
