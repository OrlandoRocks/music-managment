require 'rails_helper'

RSpec.describe WithholdingExemption, type: :model do

  context " when is active?" do

    before (:each) do
      WithholdingExemption.destroy_all
    end

    let!(:active_exemption) do 
      create(:withholding_exemption, withholding_exemption_reason: WithholdingExemptionReason.last) 
    end

    it "ended? should be false" do
      expect(active_exemption.ended?).to be(false)
    end

    it "revoked? should be false" do
      expect(active_exemption.revoked?).to be(false)
    end

    it "readonly? should be false" do
      expect(active_exemption.readonly?).to be(false)
    end

    describe "when trying to revoke" do
      it "ended_at and revoked_at should be the same" do
        active_exemption.revoke!
        expect(active_exemption.ended_at.to_datetime).to eq(active_exemption.revoked_at)
      end
    end
  end

  context "when is ended?" do
    let!(:ended_exemption) do 
      create(:withholding_exemption, ended_at: Time.now, withholding_exemption_reason: WithholdingExemptionReason.last) 
    end
    it "ended? should be true" do
      expect(ended_exemption.ended?).to be(true)
    end

    it "revoked? should be false" do
      expect(ended_exemption.revoked?).to be(false)
    end

    it "readonly? should be false" do
      expect(ended_exemption.readonly?).to be(false)
    end

    it "active? should be false" do
      expect(ended_exemption.active?).to be(false)
    end
  end
  

  context "when is revoked?" do
    let!(:revoked_exemption) do 
      exemption = create(:withholding_exemption, ended_at: Time.now, withholding_exemption_reason: WithholdingExemptionReason.last)
      exemption.revoke!
      exemption
    end
    it "ended? should be true" do
      expect(revoked_exemption.ended?).to be(true)
    end

    it "revoked? should be true" do
      expect(revoked_exemption.revoked?).to be(true)
    end

    it "readonly? should be true" do
      expect(revoked_exemption.readonly?).to be(true)
    end

    it "active? should be false" do
      expect(revoked_exemption.active?).to be(false)
    end
  end

  context "when is ready_only" do
    let!(:readonly_exemption) do 
      exemption = create(:withholding_exemption, ended_at: Time.now, withholding_exemption_reason: WithholdingExemptionReason.last)
      exemption.revoke!
      exemption
    end

    it "throws an exception when trying to update the record" do
      assert_raises ActiveRecord::ReadOnlyRecord  do 
        readonly_exemption.update(id: readonly_exemption.id + 100000) 
      end
    end

  end
end
