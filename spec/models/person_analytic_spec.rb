require "rails_helper"

describe PersonAnalytic do
  describe "validates presence of person" do
    context "when a person_analytic has an metric name" do
      before(:each) do
        @person = FactoryBot.create(:person)
        @person_analytic = PersonAnalytic.new(metric_name: "first_distribution_invoice_id")
      end

      it "is invalid without a person" do
        expect(@person_analytic.invalid?).to be true
      end

      it "is valid with a person" do
        @person_analytic.person = @person
        expect(@person_analytic.valid?).to be true
      end
    end
  end

  describe "validates presence of a metric name" do
    context "when a person_analytic has a person" do
      before(:each) do
        @person = FactoryBot.create(:person)
        @person_analytic = PersonAnalytic.new(person: @person)
      end

      it "is invalid without a metric name" do
        expect(@person_analytic.invalid?).to be true
      end

      it "is valid witha a metric name" do
        @person_analytic.metric_name = "first_distribution_invoice_id"
        expect(@person_analytic.valid?).to be true
      end
    end
  end

  describe "validates the inclusion of the metric_name in the constant METRIC_NAMES" do
    context "when a person_analytic has a person" do
      before(:each) do
        @person = FactoryBot.create(:person)
        @person_analytic = PersonAnalytic.new(person: @person)
      end

      it "is valid if the metric_name is in the constant" do
        @person_analytic.metric_name = PersonAnalytic::METRIC_NAMES.first
        expect(@person_analytic.valid?).to be true
      end

      it "is invalid if the metric_name is not in the constant" do
        @person_analytic.metric_name = "not_a_metric_name"
        expect(@person_analytic.invalid?).to be true
      end
    end
  end

  describe ".update_analytics_for_invoice" do
    let(:invoice) { create(:invoice) }

    it "should call calculate_metric_if_empty with each metric name and person ID" do
      PersonAnalytic::METRIC_NAMES.each do |metric_name|
        expect(PersonAnalytic).to receive(:calculate_metric_if_empty).with(metric_name, invoice.person_id)
      end

      PersonAnalytic.update_analytics_for_invoice(invoice.id)
    end
  end
end

