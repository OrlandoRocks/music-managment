require "rails_helper"

describe Artist do
  before :each do
    @artist = Artist.create(name: "Artist 1")
  end

  context "when saving" do
    context "when 'name' is <= 120 characters" do
      it "should save" do
        name = "A" * 120
        artist = Artist.create(name: name)
        expect(artist.name).to(eq(name))
      end
    end

    context "when 'name' is > 120 characters" do
      it "should truncate 'name' to 120 characters and save" do
        name = "A" * 121
        artist = Artist.create(name: name)
        expect(artist.name).not_to(eq(name))
        expect(artist.name).to(eq("A" * 120))
      end
    end
  end

  describe "#blacklisted?" do
    it "returns true for a blacklisted artist" do
      BlacklistedArtist.create(artist: @artist)

      expect(@artist.blacklisted?).to be true
    end

    it "returns false for an artist without a blacklisted_artist object" do
      BlacklistedArtist.destroy_all

      expect(@artist.blacklisted?).to be false
      expect(BlacklistedArtist.count).to be 0
    end

    it "returns false for an artist removed from blacklist" do
      BlacklistedArtist.create(artist: @artist, active: false)

      expect(@artist.blacklisted?).to be false
    end

  end

  describe "#blacklist!" do
    it "creates a blacklisted_artist object for that artist if one does not exist" do
      expect{
        @artist.blacklist!
      }.to change(BlacklistedArtist, :count).by(1)
    end

    it "reactivates a blacklisted_artist object if one exists" do
      @artist.blacklist!
      blacklist_count = BlacklistedArtist.count
      @artist.remove_from_blacklist
      @artist.blacklist!

      expect(BlacklistedArtist.count).to eq(blacklist_count)
      expect(@artist.blacklisted?).to be true
    end
  end

  describe "#remove_from_blacklist" do
    it "returns true if Artist is not blacklisted" do
      BlacklistedArtist.destroy_all

      expect(@artist.remove_from_blacklist).to be true
    end

    it "sets the blacklisted_artist to inactive" do
      @artist.blacklist!
      blacklist = @artist.blacklisted_artist
      @artist.remove_from_blacklist

      expect(@artist.blacklisted?).to be false
    end

    it "does not delete the blacklisted_artist object" do
      @artist.blacklist!

      expect{
        @artist.remove_from_blacklist
      }.not_to change(BlacklistedArtist, :count)
    end
  end

  describe "bulk_find_or_create" do
    before :each do
      @artist = create(:artist)
    end

    it "returns an array of artist objects" do
      find_or_create = Artist.bulk_find_or_create([@artist.name]).first
      expect(find_or_create).to eq @artist
    end

    it "creates new artists for those whose names do not exist" do
      artist = build(:artist)
      Artist.bulk_find_or_create([artist.name])

      expect(Artist.last.name).to eq(artist.name)
    end

    it "does not create duplicate artists" do
      2.times{ create(:artist) }
      new_artist = "Some Artist"
      test_set   = [new_artist] + Artist.limit(2).pluck(:name)

      expect{
        Artist.bulk_find_or_create(test_set)
      }.to change(Artist, :count).by(1)
    end

    it "creates nothing if there are no new names" do
      existing_names = Artist.pluck(:name)

      expect(Artist).not_to receive(:import)
      Artist.bulk_find_or_create(existing_names)
    end

    it "returns persisted artist objects of mixed arrays of new & existing names" do
      newbie  = build(:artist)
      existing = Artist.pluck(:name)

      Artist.bulk_find_or_create(existing << newbie.name)
      expect(Artist.exists?(name: newbie.name)).to be true
    end
  end

  describe ".scrubbed_name" do
    context "update based on rules set" do
      it "should downcase the scrubbed_name" do
        artist = create(:artist, name: 'Test Scrubbed Name')
        expect(artist.scrubbed_name).to eq('test scrubbed name')
      end

      it "should remove trailing spaces" do
        artist = create(:artist, name: '   Test Scrubbed Name    ')
        expect(artist.scrubbed_name).to eq('test scrubbed name')
      end

      it "should remove special characters" do
        artist = create(:artist, name: '? # % Test/ S@c~r#u$b%b*e(,)d N\'a\"m^e!')
        expect(artist.scrubbed_name).to eq('test scrubbed name')
      end

      it "should replace & with and" do
        artist = create(:artist, name: 'Scrubbed & Name')
        expect(artist.scrubbed_name).to eq('scrubbed and name')
      end
    end
  end
end
