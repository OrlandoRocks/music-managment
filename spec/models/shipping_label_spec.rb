require "rails_helper"

describe ShippingLabel, " when processing state and zip code options from the user" do
  it "should fail when the user puts a blank character in the state abbreviation" do
    label = ShippingLabel.new TCFactory.shipping_label_defaults(:state => ' M')
    label.save
    expect(label.errors.messages[:state].size).to eq 1
  end

  it "should fail when the user doesn't supply a 5 digit zip code (1)" do
    label = ShippingLabel.new TCFactory.shipping_label_defaults(:zip => ' 1234')
    label.save
    expect(label.errors.messages[:zip].size).to eq 1
  end

  it "should fail when the user doesn't supply a 5 digit zip code (2)" do
    label = ShippingLabel.new TCFactory.shipping_label_defaults(:zip => '12 34')
    label.save
    expect(label.errors.messages[:zip].size).to eq 1
  end
end


describe ShippingLabel, " when global_flag has been set" do
  it "should allow the user to not include address information" do
    label = ShippingLabel.new TCFactory.shipping_label_defaults(:address1 => '', :zip => '', :global_flag => true)
    label.save
    expect(label.errors.messages.size).to eq 0
    expect(label.errors.messages.size).to eq 0
    expect(label.errors.messages.size).to eq 0
    expect(label.errors.messages.size).to eq 0
  end
end
