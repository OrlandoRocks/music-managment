require "rails_helper"

describe TargetedProduct, "when calculating the new price based on price adjustment rules" do
  it "should return the original price when the price_adjustment_type is set to 'none'" do
    targeted_product = build(:targeted_product, price_adjustment_type: 'none')
    expect(targeted_product.adjust_price(BigDecimal.new("57.99"))).to eq(BigDecimal.new("57.99"))
  end

  it "should return 47.99 (original price 57.99) when the price_adjustment_type = 'dollars_off' and price_adjustment is 10.00" do
    targeted_product = build(:targeted_product, price_adjustment_type: 'dollars_off', price_adjustment: 10.00)
    expect(targeted_product.adjust_price(BigDecimal.new("57.99"))).to eq(BigDecimal.new("47.99"))
  end

  it "should return 52.191 (original price 57.99) when the price_adjustment_type = 'percentage_off' and price_adjustment is 10.00" do
    targeted_product = build(:targeted_product, price_adjustment_type: 'percentage_off', price_adjustment: 10.00)
    expect(targeted_product.adjust_price(BigDecimal.new("57.99"))).to eq(BigDecimal.new("52.191"))
  end

  it "should return 50.00 (original price 57.99) when the price_adjustment_type = 'override' and price_adjustment is 50.00" do
    targeted_product = build(:targeted_product, price_adjustment_type: 'override', price_adjustment: 50.00)
    expect(targeted_product.adjust_price(BigDecimal.new("57.99"))).to eq(BigDecimal.new("50.00"))
  end
end

describe TargetedProduct, "when create a new targeted product" do

  before(:each) do
    @targeted_offer     = create(:targeted_offer, status: "Active")
    @us_one_year_album  = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
  end

  it "should not let you target a product with a different country website than the offer" do

    #Create a targeted product with a product that has the wrong country id
    @targeted_product = build(:targeted_product, product: @us_one_year_album, targeted_offer_id: @targeted_offer.id)
    expect(@targeted_product).to be_valid

    #Set to a wrong country id
    wrong_country_id  = ( @targeted_offer.country_website_id.to_i + 1 ).to_s
    @targeted_product.product.country_website_id = wrong_country_id

    #Make sure its not valid
    expect(@targeted_product).not_to be_valid
    expect(@targeted_product).to have(1).error_on(:product)
  end

  it  "should require the targeted offer to be inactive" do
    expect(@targeted_offer.targeted_products.create(product: @us_one_year_album).new_record?)
      .to be_truthy

    @targeted_offer.update(status: "Inactive")
    @tp = @targeted_offer.targeted_products.create(product: @us_one_year_album)
    expect(@tp.new_record?).to be_falsey

    @tp.targeted_offer.update(status: "Active")

    @tp.destroy
    expect(TargetedProduct.find(@tp.id)).not_to be_nil

    @tp.targeted_offer.update(status: "Inactive")

    @tp.destroy
    expect(TargetedProduct.where(id: @tp.id)).to be_empty
  end

end


describe "targeting products" do

  before(:each) do
    @to = create(:targeted_offer, status: "Inactive")
    @product_in   = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
    @product_out  = Product.find(Product::US_TWO_YEAR_ALBUM_PRODUCT_ID)

    #Create a dummy offer
    @dummy_offer  = create(:targeted_offer)
    @dummy_prod   = Product.find(Product::US_FIVE_YEAR_ALBUM_PRODUCT_ID)
    @dummy_offer.targeted_products.create(product: @dummy_prod)


    #Add Product in into offer
    @tp = @to.targeted_products.create(product: @product_in)
    @to.targeted_products.create(product: @dummy_prod)

    #activate offer
    @to.update(status: "Active")

    @person_in  = create(:person)
    @person_out = create(:person)

    @to.targeted_people.create(person: @person_in)

    #Add both into dummy offer
    @dummy_offer.targeted_people.create(person: @person_in)
    @dummy_offer.targeted_people.create(person: @person_out)
  end

  it "should return tp when person and product in offer" do
    expect(TargetedProduct.targeted_product_for_active_offer(@person_in, @product_in).id).to eq(@tp.id)
  end

  it "should not return tp when person in and product out" do
    expect(TargetedProduct.targeted_product_for_active_offer(@person_in, @product_out)).to be_nil
  end

  it "should not return tp when person out and product in" do
    expect(TargetedProduct.targeted_product_for_active_offer(@person_out, @product_in)).to be_nil
  end

  context "when targeted products are created with targeted stores" do

    let(:targeted_offer) { create(:targeted_offer, 
      start_date: Time.now - 40.days, 
      expiration_date: Time.now + 10.days, 
      offer_type: "existing", 
      status: "New") }

    let(:product) { Product.find(Product::US_ONE_YEAR_SINGLE_PRODUCT_ID) }

    let(:targeted_product) { create(:targeted_product, product: product, targeted_offer: targeted_offer, price_adjustment: 10.00, price_adjustment_type: "dollars_off") }

    let(:single) { create(:single, person: @person_in) }

    let(:store) { Store.discovery_platforms.first }

    let(:salepoint) { create(:salepoint, salepointable: single, finalized_at: DateTime.now, store: store) }
    
    let!(:targeted_product_store) { create(:targeted_product_store, targeted_product: targeted_product, store: salepoint.store) }

    context "when salepoint stores are within targeted product stores" do 
     
      before do 
        targeted_offer.update(status: "Active")
      end 
      
      it "should return targeted product when person and product in offer" do
        expect(TargetedProduct.targeted_product_for_active_offer(@person_in, product, single).id).to eq(targeted_product.id)
      end
    end
    
    context "when any salepoint stores are not included in targeted product stores" do 

      before do 
        create(:salepoint, salepointable: single, finalized_at: DateTime.now ) 
      end 

      it "should not apply the targeted offer" do 
        expect(TargetedProduct.targeted_product_for_active_offer(@person_in, product, single)).to eq(nil)
      end 
    end 

    it "should not return tp when album instance is Album" do
      album = create(:album, person: @person_in)
      salepoint = create(:salepoint, salepointable: album, finalized_at: DateTime.now)
      create(:targeted_product_store, targeted_product: targeted_product, store: salepoint.store)
      expect(TargetedProduct.targeted_product_for_active_offer(@person_in, product, album)).to eq(nil)
    end
  end
end

describe "targeting renewals" do

  before(:each) do
    @person_past_due       = create(:person, country: "Afghanistan")
    @person_past_due_lt_30 = create(:person, country: "Afghanistan")
    @person_due_now        = create(:person, country: "Afghanistan")
    @person_due_in_future  = create(:person, country: "Afghanistan")

    #Generate album purchases for the peeps
    product = TCFactory.generate_album_product(49.99)
    TCFactory.generate_album_purchase(@person_past_due, true, product)
    TCFactory.generate_album_purchase(@person_past_due_lt_30, true, product)
    TCFactory.generate_album_purchase(@person_due_now, true, product)
    TCFactory.generate_album_purchase(@person_due_in_future, true, product)

    #Set the renewal dates to be the important dates
    renewal = @person_past_due.albums.first.renewal.renewal_history.last
    renewal.update_attribute(:expires_at, Date.today - 31.day)

    renewal = @person_past_due_lt_30.albums.first.renewal.renewal_history.last
    renewal.update_attribute(:expires_at, Date.today - 1.day)

    renewal = @person_due_now.albums.first.renewal.renewal_history.last
    renewal.update_attribute(:expires_at, Date.today + 4.weeks - 1.day)

    renewal = @person_due_in_future.albums.first.renewal.renewal_history.last
    renewal.update_attribute(:expires_at, Date.today + 5.weeks)

    #Create a targeted product targeting the album renewal
    @to = create(:targeted_offer, status: "Inactive", country: "Afghanistan")
    product = Product.where(product_type: "Renewal", applies_to_product: "Album").first
    @tp = @to.targeted_products.create(product: product)
    @to.update({status: "Active"})

    @to.targeted_people.create(person: @person_past_due)
    @to.targeted_people.create(person: @person_past_due_lt_30)
    @to.targeted_people.create(person: @person_due_now)
    @to.targeted_people.create(person: @person_due_in_future)

    @to.set_population_count_from_criteria
    expect(@to.reload.population_criteria_count).to eq(4)
  end

  it "should not be active for past due renewals when flag set to false" do
    @tp.update_attribute(:renewals_past_due, false)

    expect(TargetedProduct.targeted_product_for_active_offer(@person_past_due,       @tp.product, @person_past_due.renewals.first )).to be_nil
    expect(TargetedProduct.targeted_product_for_active_offer(@person_past_due_lt_30, @tp.product, @person_past_due_lt_30.renewals.first )).not_to be_nil
    expect(TargetedProduct.targeted_product_for_active_offer(@person_due_now,        @tp.product, @person_due_now.renewals.first  )).not_to be_nil
    expect(TargetedProduct.targeted_product_for_active_offer(@person_due_in_future,  @tp.product, @person_due_in_future.renewals.first )).not_to be_nil
  end

  it "should not include people with renewals due now when flag set to false" do
    @tp.update_attribute(:renewals_due, false)

    expect(TargetedProduct.targeted_product_for_active_offer(@person_past_due,       @tp.product, @person_past_due.renewals.first )).not_to be_nil
    expect(TargetedProduct.targeted_product_for_active_offer(@person_past_due_lt_30, @tp.product, @person_past_due_lt_30.renewals.first )).to be_nil
    expect(TargetedProduct.targeted_product_for_active_offer(@person_due_now,        @tp.product, @person_due_now.renewals.first  )).to be_nil
    expect(TargetedProduct.targeted_product_for_active_offer(@person_due_in_future,  @tp.product, @person_due_in_future.renewals.first )).not_to be_nil
  end

  it "should not include people with renewals due now when flag set to false" do
    @tp.update_attribute(:renewals_upcomming, false)

    expect(TargetedProduct.targeted_product_for_active_offer(@person_past_due,       @tp.product, @person_past_due.renewals.first )).not_to be_nil
    expect(TargetedProduct.targeted_product_for_active_offer(@person_past_due_lt_30, @tp.product, @person_past_due_lt_30.renewals.first )).not_to be_nil
    expect(TargetedProduct.targeted_product_for_active_offer(@person_due_now,        @tp.product, @person_due_now.renewals.first  )).not_to be_nil
    expect(TargetedProduct.targeted_product_for_active_offer(@person_due_in_future,  @tp.product, @person_due_in_future.renewals.first )).to be_nil
  end

  it "should get all if all are set" do
    expect(TargetedProduct.targeted_product_for_active_offer(@person_past_due,       @tp.product, @person_past_due.renewals.first )).not_to be_nil
    expect(TargetedProduct.targeted_product_for_active_offer(@person_past_due_lt_30, @tp.product, @person_past_due_lt_30.renewals.first )).not_to be_nil
    expect(TargetedProduct.targeted_product_for_active_offer(@person_due_now,        @tp.product, @person_due_now.renewals.first  )).not_to be_nil
    expect(TargetedProduct.targeted_product_for_active_offer(@person_due_in_future,  @tp.product, @person_due_in_future.renewals.first )).not_to be_nil
  end
end

describe TargetedProduct do

  describe "#discount_amount_cents" do

    before :each do
      @targeted_product = create(:targeted_product)
    end

    it "should calculate percentage discounts correctly" do
      @targeted_product.update(price_adjustment_type: "percentage_off", price_adjustment: 50)
      expect(@targeted_product.discount_amount_cents(10000)).to eq 5000
    end

    it "should calculate override discounts correctly" do
      @targeted_product.update(price_adjustment_type: "override", price_adjustment: 75)
      expect(@targeted_product.discount_amount_cents(10000)).to eq 2500
    end

    it "should calculate dollars off discounts correctly" do
      expect(@targeted_product.discount_amount_cents(10000)).to eq 1000
    end

    it "should return the full price if the discount is greater than it" do
      @targeted_product.update(price_adjustment_type: "dollars_off", price_adjustment: 150)
      expect(@targeted_product.discount_amount_cents(10000)).to eq 10000
    end

    it "should return 0 if original_price - the_discount is negative" do
      @targeted_product.update(price_adjustment_type: "override", price_adjustment: 75)
      expect(@targeted_product.discount_amount_cents(0)).to eq 0
    end
  end

end

describe TargetedProduct do
  describe ".targeted_product_for_active_offer" do

    context "for a TC Social subscription (or any other product)" do

      before(:each) do
        @person   = create(:person)
        album     = create(:album, person: @person)
        @product  = Product.where(display_name: "tc_social_monthly", country_website_id: @person.country_website_id).first
        @related  = create(:subscription_purchase, :social_us, person: @person)

        @targeted_offer   = create(:targeted_offer)
        @targeted_product  = create(:targeted_product, targeted_offer: @targeted_offer, product: @product)
        @targeted_person  = create(:targeted_person, person: @person, targeted_offer: @targeted_offer)
        @targeted_offer.update_attribute(:status, "Active")
      end

      context "when the targeted_offer is unlimited use" do
        it "always returns the targeted_product regardless of how many times it has been used" do
          5.times do
            targeted_product = TargetedProduct.targeted_product_for_active_offer(@person, @product, @related)
            expect(targeted_product).to eq @targeted_product
            @targeted_person.use!
          end
        end
      end

      context "when the targeted_offer is limited to one use" do
        before(:each) do
          @targeted_offer.update_attribute("usage_limit", 1)
        end

        it "returns the targeted_product before it is used, but not after" do
          before_use = TargetedProduct.targeted_product_for_active_offer(@person, @product, @related)
          expect(before_use).to eq @targeted_product
          @targeted_person.use!
          after_use = TargetedProduct.targeted_product_for_active_offer(@person, @product, @related)
          expect(after_use).to be nil
        end
      end

      context "when the targeted_offer is limited to two uses" do
        before(:each) do
          @targeted_offer.update_attribute("usage_limit", 2)
        end

        it "returns the targeted_product before it is used twice, but not after" do
          2.times do
            before_use = TargetedProduct.targeted_product_for_active_offer(@person, @product, @related)
            expect(before_use).to eq @targeted_product
            @targeted_person.use!
          end
          after_use = TargetedProduct.targeted_product_for_active_offer(@person, @product, @related)
          expect(after_use).to be nil
        end
      end
    end
  end

  describe ".add_all_products" do
    let(:targeted_offer) { create(:targeted_offer) }

    let(:params) do
      {
        price_adjustment_type: "percentage_off",
        price_adjustment: 10.0,
        targeted_offer_id: targeted_offer.id
      }
    end

    it "should add all products from the country website" do
      all_product_count = Product.where(country_website_id: targeted_offer.country_website_id).count

      TargetedProduct.add_all_products(params)

      expect(targeted_offer.targeted_products.count).to eq(all_product_count)
    end
  end
end
