require 'rails_helper'

RSpec.describe TierCertification, type: :model do
  describe "relationships" do
    subject { build(:tier_certification) }

    it { should belong_to(:tier) }
    it { should belong_to(:certification) }
  end

  describe "validations" do
    let(:subject) { build(:tier_certification) }

    it { should validate_uniqueness_of(:tier_id).scoped_to(:certification_id) }
  end
end
