require "rails_helper"
include ActiveSupport::Testing::TimeHelpers

describe EftBatch, "scopes" do
  describe ".uploaded" do
    it "returns eft batches with a status of 'uploaded'" do
      uploaded_eft_batch = create(:eft_batch, :uploaded)
      eft_batch = create(:eft_batch)

      results = EftBatch.uploaded

      expect(results).to include(uploaded_eft_batch)
      expect(results).not_to include(eft_batch)
    end
  end

  describe ".unprocessed" do
    it "returns eft batches with a response_processed_at value of nil" do
      unprocessed_eft_batch = create(:eft_batch, :unprocessed)
      eft_batch = create(:eft_batch, :processed)

      results = EftBatch.unprocessed

      expect(results).to include(unprocessed_eft_batch)
      expect(results).not_to include(eft_batch)
    end
  end

  describe ".sent_last_30_days" do
    it "returns eft batches with a batch_sent_at value in the last 30 days" do
      today = Date.today

      eft_batch_unsent = create(:eft_batch, batch_sent_at: nil)
      eft_batch_sent_today = create(:eft_batch, batch_sent_at: today)
      eft_batch_sent_before_30_days = create(:eft_batch, batch_sent_at: today - 35.days)
      eft_batch_sent_in_last_30_days = create(:eft_batch, batch_sent_at: today - 20.days)

      results = EftBatch.sent_last_30_days

      expect(results).to include(eft_batch_sent_today)
      expect(results).to include(eft_batch_sent_in_last_30_days)
      expect(results).not_to include(eft_batch_unsent)
      expect(results).not_to include(eft_batch_sent_before_30_days)
    end
  end
end

describe EftBatch do
  describe '.upload_confirmed_batch_dates' do
    context 'when there are no eft batches with a status of upload confirmed' do
      it 'should return an empty array'  do
        expect(EftBatch.upload_confirmed_batch_dates).to eq []
      end
    end

    context 'when there are eft batches with a status of upload confirmed' do
      before do
        travel_to Time.local(2020)
        create(:eft_batch, :upload_confirmed)
        create(:eft_batch, :upload_confirmed, batch_sent_at: nil)
        create_list(:eft_batch, 2, :upload_confirmed, batch_sent_at: 1.day.ago)
      end

      after do
        travel_back
      end

      it 'should return the uniq batch sent at dates in date format' do
        expect(EftBatch.upload_confirmed_batch_dates).to eq [Date.today.to_date, 1.day.ago.to_date]
      end
    end
  end

  describe "#completed?" do
    context "when the Eft Batch does NOT have any Eft Batch Transactions with a processing status" do
      it "returns true" do
        eft_batch = create(:eft_batch)
        eft_batch_transaction = create(:eft_batch_transaction, :with_person_balance, eft_batch: eft_batch)
        eft_batch_transaction.update(status: "success")

        expect(eft_batch.completed?).to be_truthy
      end
    end

    context "when the Eft Batch has Eft Batch Transactions with a processing status" do
      it "returns false" do
        eft_batch = create(:eft_batch)
        eft_batch_transaction = create(:eft_batch_transaction, :with_person_balance, eft_batch: eft_batch)
        eft_batch_transaction.update(status: "pending_approval")

        expect(eft_batch.completed?).to be_falsy
      end
    end
  end
end
