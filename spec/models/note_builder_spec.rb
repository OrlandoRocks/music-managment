require "rails_helper"
describe  SongDistributionOption::NoteBuilder do
  let(:person) do
    FactoryBot.create(:person,
                       email: "homer@springfield.net",
                      )
  end
  let(:song) { FactoryBot.create(:song) }
  let(:note_builder) do
    SongDistributionOption::NoteBuilder.new(
      song: song,
      option: :available_for_streaming,
      user: person,
      user_input: "DOH"
    )
  end

  describe "#note" do
    it "returns an instance of Note" do
      expect(note_builder.note).to be_a(Note)
    end

    it "returns a note with a subject" do
      expect(note_builder.note.subject).to_not be_blank
    end

    it "returns a note with note text" do
      expect(note_builder.note.note).to_not be_blank
    end

    it "returns a note with user input appended to the end of note text" do
      expect(note_builder.note.note).to match(/DOH/)
    end
  end
end
