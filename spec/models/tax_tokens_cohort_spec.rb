require "rails_helper"

describe TaxTokensCohort do
  describe "#is_blocking?" do
    context "end_date has not passed" do
      it "returns false" do
        cohort = TaxTokensCohort.create(start_date: 2.days.ago, end_date: 2.days.from_now)
        expect(cohort.is_blocking?).to eq(false)
      end
    end

    context "end_date has passed" do
      it "returns true" do
        cohort = TaxTokensCohort.create(start_date: 2.days.ago, end_date: 1.day.ago)
        expect(cohort.is_blocking?).to eq(true)
      end
    end
  end

  describe ".remove_all_tax_tokens" do
    before(:each) do
      @tax_tokens_cohort = create(:tax_tokens_cohort)
      @tax_tokens = create_list(:tax_token, 5, tax_tokens_cohort: @tax_tokens_cohort)
    end

    it "sets is_visible to false for all the tax_tokens in that cohort" do
      @tax_tokens_cohort.remove_all_tax_tokens
      tax_tokens_visibility = @tax_tokens.all? { |token| token.is_visible? }
      expect(tax_tokens_visibility).to eq(false)
    end
  end

  describe "#active" do
    before(:each) do
      @tax_tokens_cohort = create(:tax_tokens_cohort)
      @tax_tokens_visible = create_list(:tax_token, 5, tax_tokens_cohort: @tax_tokens_cohort, is_visible: true)
      @tax_tokens_not_visible = create_list(:tax_token, 5, tax_tokens_cohort: @tax_tokens_cohort, is_visible: false)
    end

    it "returns tax tokens cohorts with any visible tax tokens only" do
      expect(TaxTokensCohort.active.first).to eq(@tax_tokens_cohort)
    end

    it "does not return anything if the tax tokens cohort does not have any visible tax tokens" do
      @tax_tokens_cohort.tax_tokens.map { |token|token.update(is_visible:false) }
      expect(TaxTokensCohort.active.count).to eq(0)
    end
  end
end
