require 'rails_helper'

RSpec.describe TierThreshold, type: :model do
  describe "relationships" do
    subject { build(:tier_threshold) }

    it { should belong_to(:tier) }
  end
end
