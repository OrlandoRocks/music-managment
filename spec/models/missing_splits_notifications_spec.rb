require "rails_helper"

describe MissingSplitsNotification do
  describe "setup missing notification" do
    before(:each) do
      @album = TCFactory.create(:album)
      @missing_splits_notification = TCFactory.create(:missing_splits_notification, person: @album.person, notification_item: nil)
    end
  
    it "sets system_message_type to MissingSplitsNotification" do
      expect(@missing_splits_notification.type).to eq(MissingSplitsNotification.name)
    end

    it "sets text for missing notifications" do
      expect(@missing_splits_notification.text).to eq("We can't register your compositions and collect your royalties until you tell us your split percentages (i.e., the percentage of the composition that you own or control).")
    end

    it "sets a url" do
      expect(@missing_splits_notification.url).to eq("/publishing_administration/composers")
    end

    it "sets a title" do
      expect(@missing_splits_notification.title).to eq("Missing Splits: Action Required")
    end

    it "sets link_text" do
      expect(@missing_splits_notification.link_text).to eq("Set missing splits now")
    end

    it "sets an image_url" do
      expect(@missing_splits_notification.image_url).not_to be_blank
    end
  end
  
  describe "#create_notifications" do
    before(:each) do
      @album = TCFactory.create(:album)
      @album.songs << TCFactory.create(:song)
      @composer = TCFactory.create(:composer, person: @album.person)
      @purchase = TCFactory.create(:purchase, :person=>@album.person, :related=>@composer, :product_id=>65, :paid_at=>Date.today)
    end
    
    
    it "creates an missing splits notification for composers with missing compositions" do
      expect {
        MissingSplitsNotification.create_notifications
      }.to change(MissingSplitsNotification, :count).by(1)
    end
  end
end
