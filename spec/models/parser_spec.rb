require "rails_helper"

describe ReferralDatum::Parser do
  before(:each) do
    @cookie_data = {}
    ReferralDatum.allowed_parameters.each do |allowed_parameter|
      @cookie_data[allowed_parameter] = "test"
    end
    @cookie_data[:tc_timestamp] = 1458752702
    @timestamp = DateTime.strptime(1458752702.to_s, "%s")
  end

  context "#timestamp for json" do
    it "returns timestamp from tc_timestamp key in cookie" do
      parser = ReferralDatum::Parser.new(@cookie_data.to_json)
      expect(parser.timestamp).to be_an_instance_of(Time)
      expect(parser.timestamp).to eq @timestamp
    end

    it "doesn't blow up on bad timestamp" do
      @cookie_data[:tc_timestamp] = "BAAAD"
      parser = ReferralDatum::Parser.new(@cookie_data.to_json)
      expect(parser.timestamp).to be_an_instance_of(Time)
    end
  end

  context "#parsed for json" do
    it "returns hash with allowed parameters" do
      parser = ReferralDatum::Parser.new(@cookie_data.to_json)

      ReferralDatum.allowed_parameters.each do |allowed_parameter|
        expect(parser.parsed).to have_key(allowed_parameter)
      end
    end

    it "doesnt have tc_timestamp key" do
      parser = ReferralDatum::Parser.new(@cookie_data.to_json)
      expect(parser.parsed).not_to have_key(:tc_timestamp)
    end

    it "only returns cookies with valid json" do
      parser = ReferralDatum::Parser.new("Just a String")

      expect(parser.parsed).not_to have_key(:bad)
    end
  end

  context "#timestamp for hash" do
    it "has a timestamp" do
      params = {
        ref: "test",
        utm_source: "test",
      }
      parser = ReferralDatum::Parser.new(params)
      expect(parser.timestamp).to be_an_instance_of(Time)
    end
  end

  context "#parsed for hash" do
    it "only returns params from whitelist" do
      parser = ReferralDatum::Parser.new(@cookie_data)
      expect(parser.parsed).to have_key(:ref)
      expect(parser.parsed).to have_key(:utm_source)
    end

    it "filters out non-whitelist params" do
      params = {
        bad:   "test",
        worse: "test",
      }
      parser = ReferralDatum::Parser.new(params)
      expect(parser.parsed).not_to have_key(:bad)
      expect(parser.parsed).not_to have_key(:worse)
    end
  end
end
