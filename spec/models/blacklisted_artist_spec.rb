require "rails_helper"

describe BlacklistedArtist do
  describe ".includes_scrubbed_artist_name?" do
    let(:blacklisted_artist) { create(:artist, name: "DMX") }

    before do
      create(:blacklisted_artist, artist: blacklisted_artist)
    end

    context "when a scrubbed artist name matches a blacklisted artist name" do
      it "returns true" do
        expect(
          described_class.includes_scrubbed_artist_name?(blacklisted_artist.scrubbed_name)
        ).to be true
      end
    end

    context "when a scrubbed artist name does not match a blacklisted artist name" do
      it "returns false" do
        artist = create(:artist)

        expect(
          described_class.includes_scrubbed_artist_name?(artist.scrubbed_name)
        ).to be false
      end
    end
  end
end
