require "rails_helper"

describe LegalDocument do
  let(:document)    { create(:legal_document) }
  let(:sample_uuid) { "aaaa-1111-bbbb-2222" }

  it "gets a UUID on create" do
    allow(SecureRandom).to receive(:uuid).and_return(sample_uuid)
    expect(document.external_uuid).to eq(sample_uuid)
  end
end
