require "rails_helper"

describe PaypalTransaction, "when being created" do
  before(:each) do
    @customer = TCFactory.create(:person)
    @paypal_config = @customer.paypal_payin_provider_config
  end

  it "should be valid" do
    paypal_transaction = TCFactory.create(:paypal_transaction)
    expect(paypal_transaction).to be_valid
  end

  it "should be require a country_website" do
    paypal_transaction = TCFactory.build(:paypal_transaction, :country_website_id=>nil)
    paypal_transaction.save
    expect(paypal_transaction.errors.messages[:country_website_id].size).to eq 1
  end

  it "should be require a person_id" do
    paypal_transaction = TCFactory.build(:paypal_transaction, :person=>nil)
    paypal_transaction.save
    expect(paypal_transaction.errors.messages[:person_id].size).to eq 1
  end

  context "when transaction ack is 'Failure'" do
    before(:each) do
      #mock out the PayPalAPI.reference_transaction and process calls
      transaction = double('pp_response')
      allow(PayPalAPI).to receive(:process).with(@paypal_config, any_args).and_return(transaction)
      allow(PayPalAPI).to receive(:reference_transaction).with(@paypal_config, any_args).and_return(transaction)
      allow(transaction).to receive(:response).and_return(returned_transaction_data('Failure', '', :"L_ERRORCODE0"=>10417))
      prepare_transaction_data
    end

    it "should record status as 'Failed' when calling process_reference_transaction" do
      paypal_transaction = PaypalTransaction.process_reference_transaction(TCFactory.create(:stored_paypal_account, :person => @customer), @invoice)
      expect(paypal_transaction.ack).to eq('Failure')
      expect(paypal_transaction.status).to eq('Failed')
    end
  end

  context "when transaction ack is 'Success'" do
    before(:each) do
      #mock out the PayPalAPI.reference_transaction and process calls
      transaction = double('pp_response')
      allow(PayPalAPI).to receive(:process).with(@paypal_config, any_args).and_return(transaction)
      allow(PayPalAPI).to receive(:reference_transaction).with(@paypal_config, any_args).and_return(transaction)
      allow(transaction).to receive(:response).and_return(returned_transaction_data)

      voided_transaction = double('pp_response')
      allow(PayPalAPI).to receive(:void).with(@paypal_config, any_args).and_return(voided_transaction)
      allow(voided_transaction).to receive(:response).and_return(returned_voided_transaction_data)

      prepare_transaction_data
    end
  end

  context "when transaction ack is 'Success' for reference transactions" do
    before(:each) do
      #mock out the PayPalAPI.reference_transaction and process calls
      transaction = double('pp_response')
      allow(PayPalAPI).to receive(:reference_transaction).with(@paypal_config, any_args).and_return(transaction)
      allow(transaction).to receive(:response).and_return(returned_reference_transaction_data)
      prepare_transaction_data
    end

    it "should record status as returned when calling process_reference_transaction" do
      paypal_transaction = PaypalTransaction.process_reference_transaction(TCFactory.create(:stored_paypal_account, :person => @customer), @invoice)
      expect(paypal_transaction.status).to eq('Completed')
      expect(paypal_transaction.currency).to eq('USD')
      expect(paypal_transaction.payin_provider_config).to eq(@paypal_config)
    end
  end
end

describe PaypalTransaction, "#set_error_message" do
  before(:each) do
    @transaction = TCFactory.create(:paypal_transaction)
  end
  it "should not add error if errorc_code_message returns nil" do
    expect(@transaction).to receive(:error_code_message).and_return(nil)
    expect(@transaction.errors).not_to receive(:add)

    @transaction.send(:set_error_message)
  end

  it "should add error if error_code_message returns value" do
    expect(@transaction).to receive(:error_code_message).twice.and_return("test")
    expect(@transaction.errors).to receive(:add).with(:base, "test")

    @transaction.send(:set_error_message)
  end
end

describe PaypalTransaction, "#error_code_message" do
  it "should return message for 10201" do
    transaction = TCFactory.build(:paypal_transaction, :raw_response => "L_ERRORCODE0=10201")
    expect(transaction.error_code_message.eql?("Whoops! Looks like your saved PayPal account is no longer valid with us.")).to eq(true)
  end

  it "should return message for 10417" do
    transaction = TCFactory.build(:paypal_transaction, :raw_response => "L_ERRORCODE0=10417")
    expect(transaction.error_code_message.eql?('PayPal has returned the following message: PayPal could not complete the transaction, please instruct the customer to use an alternative payment method.')).to eq(true)
  end

  it "should otherwise return nil" do
    transaction = TCFactory.build(:paypal_transaction, :raw_response => "L_ERRORCODE0=2222")
    expect(transaction.error_code_message).to eq(nil)
  end
end

def prepare_transaction_data
  @invoice = TCFactory.create(:invoice, :person => @customer)
  @email = @customer.email
  @ip = 'xx.xx.xx.xx'
end

def returned_transaction_data(ack = 'Success', paymentstatus = 'Completed', options_overwrite={})
  {'ACK' => ack, 'PAYMENTSTATUS' => paymentstatus, 'PAYMENTINFO_0_PAYMENTSTATUS' => paymentstatus, 'AMT' => "9.99", 'BILLINGAGREEMENTID' => "B-0RK57377ME0665506", 'PAYMENTINFO_0_TRANSACTIONID' => "9CB082202F284692D", 'TRANSACTIONTYPE' => "expresscheckout", 'FEEAMT' => "0.59",
   'PENDINGREASON' => "None", 'REASONCODE' => "None", 'PAYMENTINFO_0_CURRENCYCODE' => "USD"}.merge(options_overwrite)
end

def returned_reference_transaction_data(ack = 'Success', paymentstatus = 'Completed', options_overwrite={})
  {'ACK' => ack, 'PAYMENTSTATUS' => paymentstatus, 'AMT' => "9.99", 'BILLINGAGREEMENTID' => "B-0RK57377ME0665506", 'TRANSACTIONID' => "9CB082202F284692D", 'TRANSACTIONTYPE' => "merchtpmt", 'FEEAMT' => "0.59",
   'PENDINGREASON' => "None", 'REASONCODE' => "None", 'CURRENCYCODE' => "USD"}.merge(options_overwrite)
end

def returned_voided_transaction_data(ack = 'Success', options_overwrite={})
  {"AUTHORIZATIONID"=>"9CB082202F284692D", "BUILD"=>"1882144", "TIMESTAMP"=>"2011-06-15T19:31:28Z", "VERSION"=>"65.1", "CORRELATIONID"=>"c8d55b4161665", "ACK"=> ack}.merge(options_overwrite)
end
