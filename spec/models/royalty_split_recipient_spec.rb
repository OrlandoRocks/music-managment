# frozen_string_literal: true

require "rails_helper"

RSpec.describe RoyaltySplitRecipient, type: :model do
  describe "validations and associations" do
    subject { build :royalty_split_recipient }
    it { should belong_to(:person) }
    it { should belong_to(:royalty_split) }

    it { should validate_presence_of(:percent) }
    it { should validate_numericality_of(:percent).is_greater_than_or_equal_to(0.0).is_less_than_or_equal_to(100.0) }
    it { should allow_value(100.0, 0.1, 0.01).for(:percent) }
    it { should validate_presence_of(:email).allow_nil }
    it { should validate_uniqueness_of(:email).case_insensitive.scoped_to(:royalty_split_id).allow_nil }
    it { should validate_presence_of(:person).allow_nil }
    it { should validate_uniqueness_of(:person_id).scoped_to(:royalty_split_id).allow_nil }
    it { should validate_presence_of(:royalty_split) }
  end

  describe "not owner" do
    subject { build :royalty_split_recipient, :not_owner }
    it { should be_valid }
    it { should have_attributes(owner?: false) }
  end

  describe "owner" do
    subject { build :royalty_split_recipient, :owner }
    it { should be_valid }
    it { should have_attributes(owner?: true) }
  end

  describe "cannot create two owners" do
    let(:owner) { create :person }
    let(:royalty_split_recipient) { royalty_split.not_owner_recipients.first }
    let!(:royalty_split) { royalty_split_with_recipients(2, owner: owner) }
    it { expect(royalty_split.recipients.count).to eql(2) }
    it { expect(royalty_split).to be_valid }
    it { expect(royalty_split_recipient).to be_valid }
    it { expect(royalty_split.owner_recipient).to be_valid }
    it { expect(royalty_split.owner_recipient).not_to eql(royalty_split_recipient) }
    it "should not create two owners" do
      expect {
        create :royalty_split_recipient, royalty_split: royalty_split, email: owner.email.upcase
      }.to raise_error(ActiveRecord::RecordInvalid).and change(RoyaltySplitRecipient, :count).by(0)
    end
  end

  describe ".disallow_both_email_and_person_id" do
    context "with both person and email" do
      let(:person) { create :person }
      subject { build :royalty_split_recipient, email: "asdf@qwer.xyz", person: person }
      it { should be_invalid }
      it { should have(1).error_on(:email) }
    end

    context "with neither person nor email" do
      let(:person) { create :person }
      subject { build :royalty_split_recipient, email: nil, person: nil }
      it { should be_invalid }
      it { should have(1).error_on(:email) }
    end
  end

  describe ".only_owner_allows_zero_percent" do
    context "when owner" do
      subject { build :royalty_split_recipient, :owner }
      it { expect(subject.owner?).to eql(true) }
      it { should allow_value(0.0).for(:percent) }
    end

    context "when not owner" do
      subject { build :royalty_split_recipient, :not_owner }
      it { expect(subject.owner?).to eql(false) }
      it { should_not allow_value(0.0).for(:percent) }
    end
  end

  describe "email" do
    subject { create :royalty_split_recipient, email: "UPPERCASE@EMAIL.COM", person: nil }
    before do
      subject.valid?
    end
    it "should downcase email in validation" do
      expect(subject.email).to eq("uppercase@email.com")
    end
    context "when a person record has a matching email" do
      let!(:person) { create :person, email: "UpperCase@email.coM" }
      before do
        subject.valid?
      end
      it "should switch to using corresponding person record" do
        expect(subject.person).to eq(person)
      end
      it "show correct email with .display_email" do
        expect(subject.display_email).to eq("uppercase@email.com")
      end
      it { expect(subject.email).to be_nil }
    end
  end

  describe ".display_email" do
    describe "recipient with email" do
      subject { create :royalty_split_recipient, email: "test123@example.com", person: nil }
      before do
        subject.valid?
      end
      it { should be_valid }
      it { expect(subject.display_email).to eq("test123@example.com") }
    end
    describe "recipient with person_id" do
      let(:person) { create :person, email: "testemail321@example.com" }
      subject { create :royalty_split_recipient, person: person, email: nil }
      before do
        subject.valid?
      end
      it { should be_valid }
      it { expect(subject.display_email).to eq("testemail321@example.com") }
    end
  end

  describe ".check_cannot_destroy_split_owner before_destroy" do
    before do
      @royalty_split = royalty_split_with_recipients(2)
      @not_owner_recipient = @royalty_split.not_owner_recipients.first
      @owner_recipient = @royalty_split.owner_recipient
    end
    it "should destroy non-owner recipient" do
      expect { @not_owner_recipient.destroy! }.to change(RoyaltySplitRecipient, :count).by(-1)
    end
    it "should not destroy owner recipients" do
      expect {
        @owner_recipient.destroy!
      }.to raise_error(ActiveRecord::RecordNotDestroyed).and change(RoyaltySplitRecipient, :count).by(0)
    end
    it "should destroy all recipients (even owners) when parent split is destroyed" do
      expect { @royalty_split.destroy! }.to change(RoyaltySplitRecipient, :count).by(-2)
    end
  end

  describe "check for is_active - not deactivated" do
    it "should be active if deactivated_at is not set and accepted_at is set" do
      recipient = create(:royalty_split_recipient, accepted_at: Time.now + 3)

      expect(recipient.active?).to be true
    end
  end

  describe "check for is_active - deactivated" do
    it "should not be active if deactivated at is set" do
      recipient = create(:royalty_split_recipient, deactivated_at: Time.now - 2)

      expect(recipient.active?).to be false
    end

    it "should not be active if accepted_at is not set" do
      recipient = create(:royalty_split_recipient)

      expect(recipient.active?).to be false
    end
  end

  describe ".automatically_accept_owner_recipients!" do
    context "when owner" do
      let(:split) { create :royalty_split }
      subject { split.owner_recipient }
      it { expect(subject.accepted_at).to be_present }
    end
    context "when not owner" do
      subject { create :royalty_split_recipient, :not_owner }
      it { expect(subject.accepted_at).to be_nil }
    end
  end
end
