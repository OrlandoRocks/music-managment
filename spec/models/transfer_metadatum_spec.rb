require 'rails_helper'

RSpec.describe TransferMetadatum, type: :model do
  describe "#autoapprove!" do
    let(:person) { create(:person, :with_balance, amount: 1_000_00) }
    let(:transfer) { create(:payout_transfer, person: person, amount_cents: 99_00) }

    it "updates the transfer_metadatum autoapproved to true" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:auto_approved_withdrawals, person.id).and_return(false)
      transfer_metadatum = create(:transfer_metadatum, auto_approved: false, trackable: transfer)
      transfer_metadatum.auto_approve!

      expect(transfer_metadatum.auto_approved).to eq(true)
    end
  end
end
