require 'rails_helper'

describe PaymentsOSTransaction, type: :model do
  describe "scopes" do
    context "user_transactions" do
      let(:person) { create(:person) }
      let(:stored_credit_card) { create(:stored_credit_card, :payos_card) }
      it "returns the selected users transactions only" do
        transactions = Array.new(5).map do
          create(:payments_os_transaction, person: person, stored_credit_card: stored_credit_card)
        end
        expect(
          PaymentsOSTransaction.user_transactions(person).pluck(:payment_id)
        ).to match_array(transactions.pluck(:payment_id))
      end
    end

    context "#action_filter" do
      it "filters transactions by action" do
        sale_transactions = create_list(:payments_os_transaction, 5)
        refund_transactions = create_list(:payments_os_transaction, 5, :successful_refund)
        filtered_transactions = PaymentsOSTransaction.action_filter(action: PaymentsOSTransaction::SALE)
        expect(filtered_transactions.pluck(:id)).to contain_exactly(*sale_transactions.pluck(:id))
      end
    end

    context "#min_amount_filter" do
      it "filters transactions by minimum amount" do
        ten_rupee_transactions = create_list(:payments_os_transaction, 5, amount: 1000)
        hundred_rupee_transactions = create_list(:payments_os_transaction, 5, amount: 10000)
        filtered_transactions = PaymentsOSTransaction.min_amount_filter(mina: 2000)
        expect(filtered_transactions.pluck(:id)).to contain_exactly(*hundred_rupee_transactions.pluck(:id))
      end
    end

    context "#max_amount_filter" do
      it "filters transactions by minimum amount" do
        ten_rupee_transactions = create_list(:payments_os_transaction, 5, amount: 1000)
        hundred_rupee_transactions = create_list(:payments_os_transaction, 5, amount: 10000)
        filtered_transactions = PaymentsOSTransaction.max_amount_filter(maxa: 2000)
        expect(filtered_transactions.pluck(:id)).to contain_exactly(*ten_rupee_transactions.pluck(:id))
      end
    end

    context "#transaction_id_filter" do
      it "filters transaction by id" do
        transactions = create_list(:payments_os_transaction, 5)
        filter_txn_id = transactions.sample.payment_id
        filtered_transactions = PaymentsOSTransaction.transaction_id_filter(transaction_id: filter_txn_id.to_s)
        expect(filtered_transactions.pluck(:payment_id)).to contain_exactly(*filter_txn_id)
      end
    end

    context "#from_date_filter" do
      it "filters transaction by from_date" do
        one_week_ago = DateTime.now.ago(1.week)
        two_weeks_ago = DateTime.now.ago(2.weeks)

        transactions_1_week_ago = create_list(:payments_os_transaction, 5, created_at: one_week_ago)
        transactions_2_weeks_ago = create_list(:payments_os_transaction, 5, created_at: two_weeks_ago)
        filtered_transactions = PaymentsOSTransaction.from_date_filter(from_date: one_week_ago)
        expect(filtered_transactions.pluck(:id)).to contain_exactly(*transactions_1_week_ago.pluck(:id))
      end
    end

    context "#to_date_filter" do
      it "filters transactions until date" do
        one_week_ago = DateTime.now.ago(1.week)
        two_weeks_ago = DateTime.now.ago(2.weeks)

        transactions_1_week_ago = create_list(:payments_os_transaction, 5, created_at: one_week_ago)
        transactions_2_weeks_ago = create_list(:payments_os_transaction, 5, created_at: two_weeks_ago)
        filtered_transactions = PaymentsOSTransaction.to_date_filter(to_date: two_weeks_ago)
        expect(filtered_transactions.pluck(:id)).to contain_exactly(*transactions_2_weeks_ago.pluck(:id))
      end
    end

    context "#status_filter" do
      it "filters transactions by status" do
        initialized_transactions = create_list(:payments_os_transaction, 5, payment_status: PaymentsOSTransaction::INITIALIZED)
        credited_transactions = create_list(:payments_os_transaction, 5, payment_status: PaymentsOSTransaction::CREDITED)
        filtered_transactions = PaymentsOSTransaction.status_filter(payments_os_status: PaymentsOSTransaction::CREDITED)

        expect(filtered_transactions.pluck(:id)).to contain_exactly(*credited_transactions.pluck(:id))
      end
    end

    context "#person_filter" do
      it "filters transaction based on the person to whom the transaction belongs" do
        person = create(:person)
        random_person_transactions = create_list(:payments_os_transaction, 5)
        selected_user_transactions = create_list(:payments_os_transaction, 5, person: person)

        filtered_transactions = PaymentsOSTransaction.person_filter(person_search: person.email)
        expect(filtered_transactions.pluck(:id)).to contain_exactly(*selected_user_transactions.pluck(:id))
      end
    end


    context "successful_refunds" do
      let(:person) { create(:person) }
      let(:stored_credit_card) { create(:stored_credit_card, :payos_card) }
      it "returns a collection of successful refund transactions" do
        Array.new(2).map do
          create(:payments_os_transaction, :failed_refund, person: person, stored_credit_card: stored_credit_card)
        end

        successful_transactions = Array.new(3).map do
          create(:payments_os_transaction, :successful_refund, person: person, stored_credit_card: stored_credit_card)
        end

        expect(
          PaymentsOSTransaction.successful_refunds.pluck(:refund_id)
        ).to match_array(successful_transactions.pluck(:refund_id))
      end
    end
  end
end
