require "rails_helper"

# = Description
# In refactoring the album models we realized that a key, unknown dependency was
# the interface to the PETRI delivery system.  This test is to ensure that all
# of the models that decend from the Album class respond to the interface.

shared_examples_for "PETRI interface" do

  it "should respond to label_name" do
    expect(@distro).to respond_to(:label_name)
  end

  it "should respond to is_various" do
    expect(@distro).to respond_to(:is_various)
  end

  it "should respond to takedown?" do
    expect(@distro).to respond_to(:takedown?)
  end

  it "should respond to orig_release_date" do
    expect(@distro).to respond_to(:orig_release_date)
  end

  it "should respond to sku do" do
    expect(@distro).to respond_to(:sku)
  end

  it "should respond to genres" do
    expect(@distro).to respond_to(:genres)
  end

  it "should return an array when genre is called" do
    expect(@distro.genres).to be_an_instance_of(Array)
  end

  it "should respond to copyright_name" do
    expect(@distro).to respond_to(:copyright_name)
  end

  it "should respond to id" do
    expect(@distro).to respond_to(:id)
  end

  it "should respond to label" do
    expect(@distro).to respond_to(:label)
  end

  it "should respond sale_date" do
    expect(@distro).to respond_to(:sale_date)
  end

  it "should respond to myspace_id" do
    expect(@distro).to respond_to(:myspace_id)
  end

  it "should respond to booklet" do
    expect(@distro).to respond_to(:booklet)
  end

  it "should respond to artist_name" do
    expect(@distro).to respond_to(:artist_name)
  end

  it "should respond to creatives" do
    expect(@distro).to respond_to(:creatives)
  end

  it "should return an array when creatives is called" do
    expect(@distro.creatives).to respond_to(:each)
  end

  it "should respond to liner_notes" do
    expect(@distro).to respond_to(:liner_notes)
  end
  it "should respond to artwork" do
    expect(@distro).to respond_to(:artwork)
  end

  it "should respond to parental_advisory" do
    expect(@distro).to respond_to(:parental_advisory)
  end

  it "should respond to name" do
    expect(@distro).to respond_to(:name)
  end

  it "should respond to upc" do
    expect(@distro).to respond_to(:upc)
  end

  it "should respond to songs" do
    expect(@distro).to respond_to(:songs)
  end

  it "should return an array when songs is called" do
    expect(@distro.songs).to respond_to(:each)
  end
end
