require "rails_helper"

describe TrackMonetizationBlocker do
  describe "#create" do
    context "when its song has a track_monetization" do
      it "initiates a track_monetization block if monetization is not already blocked" do
        song = create(:song)
        store = Store.find(Store::FBTRACKS_STORE_ID)
        track_monetization = create(:track_monetization, song: song, store: store)

        TrackMonetizationBlocker.create(song: song, store: store)

        expect(track_monetization.reload.eligibility_status).to eq("ineligible")
        expect(track_monetization.reload.state).to eq("blocked")
      end

      it "sets that track_monetization's state to blocked" do
        song = create(:song)
        store = Store.find(Store::FBTRACKS_STORE_ID)
        track_monetization = create(:track_monetization, song: song, store: store)

        TrackMonetizationBlocker.create(song: song, store: store)

        expect(track_monetization.reload.state).to eq("blocked")
      end

      it "sets that track_monetization's eligibility_status to ineligible" do
        song = create(:song)
        store = Store.find(Store::FBTRACKS_STORE_ID)
        track_monetization = create(:track_monetization, song: song, store: store)

        TrackMonetizationBlocker.create(song: song, store: store)

        expect(track_monetization.reload.eligibility_status).to eq("ineligible")
      end

      context "when its track_monetization has been delivered" do
        it "initiates a track_monetization takedown" do
          song = create(:song)
          store = Store.find(Store::FBTRACKS_STORE_ID)
          track_monetization = create(:track_monetization, :delivered, song: song, store: store)

          expect_any_instance_of(TrackMonetization).to receive(:perform_monetization_takedown)

          TrackMonetizationBlocker.create(song: song, store: store)
        end
      end

      context "when its track_monetization has not been delivered" do
        it "does not initiate a track_monetization takedown" do
          song = create(:song)
          store = Store.find(Store::FBTRACKS_STORE_ID)
          track_monetization = create(:track_monetization, song: song, store: store)

          expect_any_instance_of(TrackMonetization).to_not receive(:perform_monetization_takedown)

          TrackMonetizationBlocker.create(song: song, store: store)
        end
      end
    end

    context "when its song has no track_monetization" do
      it "does not initiate a track_monetization block" do
        song = create(:song)
        store = Store.find(Store::FBTRACKS_STORE_ID)

        expect_any_instance_of(TrackMonetization).to_not receive(:block_and_disqualify)

        TrackMonetizationBlocker.create(song: song, store: store)
      end

      it "does not initiate a track_monetization takedown" do
        song = create(:song)
        store = Store.find(Store::FBTRACKS_STORE_ID)

        expect_any_instance_of(TrackMonetization).to_not receive(:perform_monetization_takedown)

        TrackMonetizationBlocker.create(song: song, store: store)
      end
    end
  end

  describe "#destroy" do
    context "when its song has a track_monetization" do
      it "ensure an ineligibile track is unblocked and eligible" do
        song = create(:song)
        store = Store.find(Store::FBTRACKS_STORE_ID)
        track_monetization = create(:track_monetization, song: song, store: store, eligibility_status: "ineligible")
        track_monetization_blocker = create(:track_monetization_blocker, song: song, store: store)

        track_monetization_blocker.destroy

        expect(track_monetization.reload.eligibility_status).to eq("eligible")
        expect(track_monetization.reload.state).to eq("new")
      end

      it "sets the state of its track_monetization to new" do
        song = create(:song)
        store = Store.find(Store::FBTRACKS_STORE_ID)
        track_monetization = create(:track_monetization, song: song, store: store)
        track_monetization_blocker = create(:track_monetization_blocker, song: song, store: store)

        track_monetization_blocker.destroy

        expect(track_monetization.reload.state).to eq("new")
      end

      context "when its track_monetization has been taken down" do
        it "initiates a track_monetization takedown undo" do
          song = create(:song)
          store = Store.find(Store::FBTRACKS_STORE_ID)
          track_monetization = create(:track_monetization, :taken_down, song: song, store: store)
          track_monetization_blocker = create(:track_monetization_blocker, song: song, store: store)

          expect_any_instance_of(TrackMonetization).to receive(:undo_monetization_takedown)

          track_monetization_blocker.destroy
        end
      end

      context "when its track_monetization has not been taken down" do
        it "does not initiate a track_monetization takedown undo" do
          song = create(:song)
          store = Store.find(Store::FBTRACKS_STORE_ID)
          track_monetization = create(:track_monetization, song: song, store: store)
          track_monetization_blocker = create(:track_monetization_blocker, song: song, store: store)

          expect_any_instance_of(TrackMonetization).to_not receive(:undo_monetization_takedown)

          track_monetization_blocker.destroy
        end
      end
    end

  end
end
