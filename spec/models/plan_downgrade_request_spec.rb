require "rails_helper"

describe PlanDowngradeRequest do
    subject { PlanDowngradeRequest.new(
        person_id: person&.id,
        status: status,
        reason_id: reason&.id,
        requested_plan_id: requested_plan&.id
    )}
    let!(:person) { create(:person, :with_person_plan)}
    let!(:reason) { DowngradeCategoryReason.first }
    let!(:requested_plan) { Plan.first }
    let!(:status) { PlanDowngradeRequest::ACTIVE_STATUS }

    describe "validations" do

        context "status" do

            context "supported status" do
                it "passses validation" do
                    result = subject
                    expect(result.save).to be_truthy
                    expect(result.errors.full_messages).to be_empty
                end
            end

            context "non-supported status" do
                let!(:status) {"Some unsupported status"}
                it "fails validation" do
                    result = subject
                    expect(result.save).to be_falsey
                    expect(result.errors.full_messages).to include("Status is not included in the list")
                end
            end

            context "nil status" do
                let!(:status) { nil }
                it "fails validation" do
                    result = subject
                    expect(result.save).to be_falsey
                    expect(result.errors.full_messages).to include("Status is not included in the list")
                end
            end

        end

        context "nil reason" do
            let!(:reason) { nil }
            it "fails validation" do
                result = subject
                expect(result.save).to be_falsey
                expect(result.errors.full_messages).to include("Reason can't be blank")
            end
        end

        context "nil requested plan" do
            let!(:requested_plan) { nil }
            it "fails validation" do
                result = subject
                expect(result.save).to be_falsey
                expect(result.errors.full_messages).to include("Requested plan can't be blank")
            end
        end

        context "user is not a plan user" do
            let(:person) { create(:person)}
            it "adds errors and does not save properly" do
                result = subject
                expect(result.save).to be_falsey
                expect(result.errors.full_messages).to include(I18n.t("no_person_plan"))
            end
        end

        context "creating a new 'Open' request for user" do
            
            context "with an existing 'Open' request" do
                before { PlanDowngradeRequest.create(
                    person_id: person&.id,
                    status: status,
                    reason_id: reason&.id,
                    requested_plan_id: requested_plan&.id
                ) }
                it "fails validation" do
                    result = subject
                    expect(result.save).to be_falsey
                    expect(result.errors.full_messages).to include("Person has already been taken")
                end
            end

            context "with an existing non-'Open' request" do
                before { PlanDowngradeRequest.create(
                    person_id: person&.id,
                    status: PlanDowngradeRequest::PROCESSED_STATUS,
                    reason_id: reason&.id,
                    requested_plan_id: requested_plan&.id
                ) }
                it "passes validation" do
                    result = subject
                    expect(result.save).to be_truthy
                    expect(result.errors.full_messages).to be_empty
                end
            end

            context "first request" do
                it "passes validation" do
                    result = subject
                    expect(result.save).to be_truthy
                    expect(result.errors.full_messages).to be_empty
                end
            end
        end
    end

    describe "#eligible_plan_for_downgrade" do
        context "with eligible plan" do
            context "with 'Active' status" do
                it "does not add errors and saves properly" do
                    result = subject
                    expect(result.save).to be_truthy
                    expect(result.errors.full_messages).to be_empty
                end
            end

            context "with non-'Active' status" do
                let!(:status) { PlanDowngradeRequest::PROCESSED_STATUS }
                it "does not add errors and saves properly" do
                    result = subject
                    expect(result.save).to be_truthy
                    expect(result.errors.full_messages).to be_empty
                end
            end
        end

        context "with ineligible plan" do
            before do
                person.plan = Plan.first
            end
            context "with 'Active' status" do
                it "adds errors and does not save properly" do
                    result = subject
                    expect(result.save).to be_falsey
                    expect(result.errors.full_messages).to include("You are not eligible to downgrade to this plan")
                end
            end

            context "with non-'Active' status" do
                let!(:status) { PlanDowngradeRequest::PROCESSED_STATUS }
                it "does not add errors and saves properly" do
                    result = subject
                    expect(result.save).to be_truthy
                    expect(result.errors.full_messages).to be_empty
                end
            end
        end
    end

    describe "#cancel" do
        
        context "Cancelling an active plan" do
            it "updates the status to 'Canceled'" do
                downgrade_request = subject
                expect(downgrade_request.cancel).to be(true)
                expect(downgrade_request.status).to eq(PlanDowngradeRequest::CANCELED_STATUS)
                expect(downgrade_request.errors.full_messages).to be_empty
            end
        end

        context "Cancelling an already inactive plan" do
            let!(:status) { PlanDowngradeRequest::PROCESSED_STATUS }
            it "adds errors to model and does not update status" do
                downgrade_request = subject
                expect(downgrade_request.cancel).to be(false)
                expect(downgrade_request.status).to_not eq(PlanDowngradeRequest::CANCELED_STATUS)
                expect(downgrade_request.errors.full_messages).to include("Not an active request")
            end
        end

    end
end
