require 'rails_helper'

describe CorporateEntity do
  describe "validations" do
    let(:subject) { build(:corporate_entity) }

    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:name).case_insensitive }
  end
end
