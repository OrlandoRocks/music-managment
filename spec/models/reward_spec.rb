require 'rails_helper'

RSpec.describe Reward, type: :model do
  describe "relationships" do
    subject { build(:reward) }

    it { should have_many(:tier_rewards) }
    it { should have_many(:tiers).through(:tier_rewards) }
  end

  describe "validations" do
    let(:subject) { build(:reward) }

    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:content_type) }
    it { should validate_presence_of(:category) }
    it { should validate_presence_of(:points) }
  end

  describe "#redeem!" do
    let(:person) { create(:person) }
    let(:reward) { create(:reward) }

    context "User does not have access to reward" do
      it "should raise not allowed error" do
        expect {
          reward.redeem!(person)
        }.to raise_error("Reward not allowed")
      end
    end

    context "user does not have enough balance" do
      it "should insufficient balance error" do
        allow(person).to receive(:rewards).and_return(Reward.all)

        expect {
          reward.redeem!(person)
        }.to raise_error("Insufficient balance")
      end
    end

    it "should create debit transaction" do
      allow(person).to receive(:rewards).and_return(Reward.all)
      person.points_balance.update(balance: reward.points)

      expect {
        reward.redeem!(person)
      }.to change(PersonPointsTransaction, :count).by(1)

      transaction = PersonPointsTransaction.last
      expect(transaction.debit_points).to eq(reward.points)
      expect(transaction.target.id).to eq(reward.id)
    end
  end
end
