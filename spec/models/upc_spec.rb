require "rails_helper"

describe Upc, "validations" do

  describe ".optional" do
    it "returns a collection of upcs with upc_type set to 'optional'" do
      optional_upc = create(:optional_upc)
      expect(Upc.optional).to eq [optional_upc]
      expect(Upc.optional).not_to include(optional_upc.upcable.tunecore_upc)
    end
  end

  describe ".tunecore" do
    it "returns a collection of upcs with upc_type set to 'optional'" do
      album        = create(:album)
      optional_upc = create(:optional_upc)
      tc_upc       = optional_upc.upcable.upcs.where(upc_type: "tunecore").last
      expect(Upc.tunecore).to include(album.upc, tc_upc)
      expect(Upc.tunecore).not_to include(optional_upc)
    end
  end

  describe ".physical" do
    it "returns a collection of upcs with upc_type set to 'physical'" do
      album        = create(:album)
      physical_upc = create(:physical_upc)
      expect(Upc.physical).to eq [physical_upc]
      expect(Upc.physical).not_to include(physical_upc.upcable.tunecore_upc)
    end
  end

  let(:album) { create(:album) }

  context "when it is a tunecore upc" do
    it "should not require a number to be valid if it is a new tunecore UPC" do
      upc = build(:tunecore_upc)
      expect(upc).to be_valid
    end
  end

  context "when it is an optional upc" do
    it "should be invalid if number of  an optional upc is blank" do
      upc = build(:optional_upc, number: "")
      expect(upc).not_to be_valid
      expect(upc).to have(3).errors_on(:number)
    end

    it "should be invalid if the number of an optional upc has spaces in it" do
      upc = build(:optional_upc, number: "850700 084304")
      expect(upc).not_to be_valid
      expect(upc).to have(2).error_on(:number)
      expect(upc.errors[:number]).to eq(["is not a number", "is not valid"])
    end

    it "should be invalid if the number of an optional upc is too short" do
      upc = build(:optional_upc, number: "1234567890")
      expect(upc).not_to be_valid
      expect(upc).to have(1).error_on(:number)
      expect(upc.errors[:number].first).to eq("is too short (minimum is 12 characters)")
    end

    it "should be invalid if the number of an optional upc is too long" do
      upc = build(:optional_upc, number: "12345678901234")
      expect(upc).not_to be_valid
      expect(upc).to have(1).error_on(:number)
      expect(upc.errors[:number].first).to eq("is too long (maximum is 13 characters)")
    end

    it "should be invalid if the number of an optional upc contains non-integers" do
      upc = build(:optional_upc, number: "12jkd6789012")
      expect(upc).not_to be_valid
      expect(upc).to have(2).error_on(:number)
      expect(upc.errors[:number]).to eq(["is not a number", "is not valid"])
    end

    it "should be invalid if the number of an optional UPC starts with the Tunecore UPC Prefix" do
      upc = build(:optional_upc, number: "859700000533")
      expect(upc).not_to be_valid
      expect(upc).to have(1).error_on(:number)
      expect(upc.errors[:number].first).to eq I18n.t("models.media.cannot_start_with") + " 8597"
    end

    it "should be valid if the number is valid and is an optional upc" do
      upc = create(:optional_upc, number: "123456789012")
      expect(upc).to be_valid
    end

    it "should be valid if 13 digit EAN is entered" do
      upc = create(:optional_upc, number: "5412705000639")
      expect(upc).to be_valid
    end

    it "should be valid if 13 digit EAN is entered" do
      upc = create(:optional_upc, number: "5033319000084")
      expect(upc).to be_valid
    end

    it "should be valid if 13 digit EAN is entered" do
      upc = create(:optional_upc, number: "5033319000183")
      expect(upc).to be_valid
    end

    it "should be valid if 13 digit EAN is entered" do
      upc = create(:optional_upc, number: "5033319000107")
      expect(upc).to be_valid
    end

    it "should be valid if 13 digit EAN is entered" do
      upc = create(:optional_upc, number: "5033319000114")
      expect(upc).to be_valid
    end

    it "should be valid if 13 digit EAN is entered" do
      upc = create(:optional_upc, number: "7320470134641")
      expect(upc).to be_valid
    end
  end

  context "when it is a physical upc" do
    it "should be invalid if it is not 12 digits long" do
      physical_upc = build(:physical_upc, number: "1234567891011")
      expect(physical_upc).to be_invalid
    end

    it "should be valid if it is 12 digits long" do
      physical_upc = build(:physical_upc, number: "123456789012")
      expect(physical_upc).to be_valid
    end

    it "should be invalid if the upc number is not unique" do
      physical_upc_1 = create(:physical_upc, number: "123456789012")
      physical_upc_2 = build(:physical_upc, number: "123456789012")

      expect(physical_upc_2).to be_invalid
    end

    it "should be invalid if the upc number contains non-numeric characters" do
      physical_upc_1 = build(:physical_upc, number: "12345678901$")
      physical_upc_2 = build(:physical_upc, number: "123456AZ78B9")
      expect(physical_upc_1).to be_invalid
      expect(physical_upc_2).to be_invalid
    end
  end
end

describe Upc, "saving and callbacks" do
  let(:album) do
    Album.new( :creatives => [{"name"=>"default artist", "role"=>"primary_artist"}],
                        :label_name => "default label",
                        :sale_date => Date.today,
                        :orig_release_year => Date.today,
                        :name => "default album",
                        :is_various => false,
                        :language_code => TCFactory.create(:language_code).code,
                        :primary_genre_id => Genre.first.id,
                        :person => TCFactory.build_person({:name => 'Jake Howerton', :email => 'howerton@tunecore.com'}),
                        :golive_date => Time.now + 1.days,
                        :timed_release_timing_scenario => 'absolute_time'
               )
  end

  it "should set number automatically for tunecore UPC" do
    expect(album.tunecore_upc).to be_nil
    album.save
    album.reload
    expect(album.tunecore_upc).not_to be_nil
    expect(album.tunecore_upc.number).to match(/^#{Upc::TUNECORE_PREFIX}#{"%07d" % album.tunecore_upc.id}/)
  end

  it "should not be valid if the UPC is already in the database" do
    album.optional_upc_number = "123456789012"
    album.save
    upc_count = album.upcs.length
    upc = album.optional_upcs.create(number: "123456789012", upc_type: "optional")
    expect(upc).not_to be_valid
    expect(album.upcs.length).to eq(upc_count)
  end

  it "should deactivate old Tunecore UPCs when a new one is added" do
    album.save
    first_upc = album.tunecore_upc
    expect(first_upc).not_to be_inactive
    new_upc = album.tunecore_upcs.create(upc_type: "tunecore")
    expect(new_upc).not_to be_inactive
    first_upc.reload
    expect(first_upc).to be_inactive
  end

  it "should deactivate old Optional UPCs when a new one is added" do
    album.optional_upc_number = "123456789012"
    album.save
    first_upc = album.optional_upc
    expect(first_upc).not_to be_inactive
    new_upc = album.optional_upcs.create(number: "123456789111", upc_type: "optional")
    expect(new_upc).not_to be_inactive
    first_upc.reload
    expect(first_upc).to be_inactive
  end
end

describe Upc do
  context "checksum validation" do
    let(:album)           { create(:album) }
    # for valid and invalid upcs - https://www.gs1.org/check-digit-calculator
    let(:valid_upc_1)     { "123456789012" }
    let(:valid_upc_2)     { "789798667670" }
    let(:valid_upc_3)     { "629461936729" }
    let(:invalid_upc_1)   { "123456789019" }
    let(:invalid_upc_2)   { "789798667673" }
    let(:invalid_upc_3)   { "629461936720" }

    context "invalid checksums" do
      it "rejects it" do
        upc = build(:optional_upc, number: invalid_upc_1)
        expect(upc).to be_invalid
      end

      it "rejects it" do
        upc = build(:optional_upc, number: invalid_upc_2)
        expect(upc).to be_invalid
      end

      it "rejects it" do
        upc = build(:optional_upc, number: invalid_upc_3)
        expect(upc).to be_invalid
      end
    end

    context "valid checksums" do
      it "accepts it" do
        upc = build(:optional_upc, number: valid_upc_1, upcable: album)
        expect(upc).to be_valid
      end

      it "accepts it" do
        upc = build(:optional_upc, number: valid_upc_2, upcable: album)
        expect(upc).to be_valid
      end

      it "accepts it" do
        upc = build(:optional_upc, number: valid_upc_3, upcable: album)
        expect(upc).to be_valid
      end
    end
  end
end

describe 'four byte char validation' do
  let(:upload) { build(:upload, original_filename: "💿💿💿💿💿") }

  it 'validates original_filename for four byte chars' do
    upload.valid?
    expect(upload.errors.messages[:original_filename].size).to be 1
  end
end
