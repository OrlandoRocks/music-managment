# frozen_string_literal: true

require "rails_helper"

RSpec.describe RoyaltySplitSong, type: :model do
  describe "validations and associations" do
    subject { build :royalty_split_song }
    it { should belong_to(:song) }
    it { should belong_to(:royalty_split) }

    it { should validate_presence_of(:song) }
    it { should validate_presence_of(:royalty_split) }

    it { should validate_uniqueness_of(:song) }
  end
  describe "valid record" do
    subject { create :royalty_split_song }
    it { should be_valid }
  end
end
