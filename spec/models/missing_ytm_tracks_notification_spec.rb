require "rails_helper"

describe MissingYtmTracksNotification do
  it "should set fields" do
    song = create(:song)

    notification = MissingYtmTracksNotification.new(notification_item: song)

    expect(notification).to be_valid
    expect(notification.url).not_to be_blank
    expect(notification.title).not_to be_blank
    expect(notification.person).to eq(song.album.person)
  end
end
