require "rails_helper"

describe PriceAdjustmentHistory, "when creating a new history" do
  
  it "should not save invalid histories" do
    
    price_adjustment = PriceAdjustmentHistory.new
    expect(price_adjustment.save).to eq(false)
    
    expect(price_adjustment.errors.messages[:admin].size).to eq 1
    expect(price_adjustment.errors.messages[:purchase].size).to eq 1
    expect(price_adjustment.errors.messages[:original_cost_cents].size).to eq 1
    expect(price_adjustment.errors.messages[:original_discount_cents].size).to eq 1
    expect(price_adjustment.errors.messages[:new_cost_cents].size).to eq 1
    expect(price_adjustment.errors.messages[:adjustment_cents].size).to eq 1
    expect(price_adjustment.errors.messages[:note].size).to eq 1
    
  end
  
  it "should save valid histories and update the count in purchases" do
    
    price_adjustment = PriceAdjustmentHistory.new
    expect(price_adjustment.save).to eq(false)
    
    person   = TCFactory.create(:person)
    purchase = TCFactory.create(:purchase, :person_id=>person.id, :product=>Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID), :related=>TCFactory.create(:album, :person=>person))
    expect(purchase.person).not_to be_nil
    
    price_adjustment.admin                     = TCFactory.create(:person)
    price_adjustment.purchase                  = purchase
    price_adjustment.original_cost_cents       = 4999
    price_adjustment.original_discount_cents   = 10
    price_adjustment.new_cost_cents            = 6000
    price_adjustment.note                      = "This customer sucks Pay more!!!!!!"
    
    expect(purchase.price_adjustment_histories_count).to eq(0)
    expect(price_adjustment.save).to eq(true)
    expect(purchase.reload.price_adjustment_histories_count).to eq(1)
    
  end
  
  it "should calculate the adjustment cents" do
    
    person   = TCFactory.create(:person)
    purchase = TCFactory.create(:purchase, :person_id=>person.id, :product=>Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID), :related=>TCFactory.create(:album, :person=>person))    
    
    price_adjustment                           = PriceAdjustmentHistory.new
    price_adjustment.purchase                  = purchase
    price_adjustment.original_cost_cents       = 4999
    price_adjustment.original_discount_cents   = 10
    price_adjustment.new_cost_cents            = 6000
    price_adjustment.note                      = "This customer sucks Pay more!!!!!!"
    price_adjustment.admin                     = TCFactory.create(:person)
    
    price_adjustment.save!
    expect(price_adjustment.adjustment_cents).to eq(6000 - ( 4999 - 10 ))
    
  end
  
end
