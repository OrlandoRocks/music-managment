require 'rails_helper'

RSpec.describe IRSTaxWithholding, type: :model do
  describe "validations" do
    it "is invalid with withheld_amount not greater than 0" do
      withheld_from_person_transaction_record = create(:person_transaction)
      irs_withholding_record = build(
        :irs_tax_withholding,
        withheld_from_person_transaction: withheld_from_person_transaction_record,
        irs_transaction_id: 0,
        withheld_amount: 0
      )
      expect(irs_withholding_record).to_not be_valid
    end

    it "is valid with withheld_amount greater than 0" do
      withheld_from_person_transaction_record = create(:person_transaction)
      irs_withholding_record = build(
        :irs_tax_withholding,
        withheld_from_person_transaction: withheld_from_person_transaction_record,
        irs_transaction_id: 0,
        withheld_amount: 20
      )
      expect(irs_withholding_record).to be_valid
    end
  end
end
