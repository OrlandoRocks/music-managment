require "rails_helper"

describe MassAdjustmentBatch do
  describe "Associations" do
    it { should belong_to(:balance_adjustment_category) }
  end

  describe :latest do
    it "should return nil if there are no records" do
      expect(MassAdjustmentBatch.latest).to be_nil
    end

    it "should return the last record" do
      batch1 = create(:mass_adjustment_batch)
      create(:mass_adjustment_batch, created_at: Time.now - 2.days)
      expect(MassAdjustmentBatch.latest).to eq(batch1)
    end
  end

  describe :batch_in_flight? do
    it "should return false if there are no batches" do
      expect(MassAdjustmentBatch.batch_in_flight?).to be_falsey
    end

    it "should return false for latest batch in declined state" do
      create(:mass_adjustment_batch, status: MassAdjustmentBatch::DECLINED)
      expect(MassAdjustmentBatch.batch_in_flight?).to be_falsey
    end

    it "should return false for latest batch in cancelled state" do
      create(:mass_adjustment_batch, status: MassAdjustmentBatch::CANCELLED)
      expect(MassAdjustmentBatch.batch_in_flight?).to be_falsey
    end

    it "should return false if latest batch entries are all processed" do
      allow_any_instance_of(MassAdjustmentBatch).to receive(:processing_completed?).and_return(true)
      create(:mass_adjustment_batch, status: MassAdjustmentBatch::SCHEDULED)
      expect(MassAdjustmentBatch.batch_in_flight?).to be_falsey
    end

    [MassAdjustmentBatch::NEW, MassAdjustmentBatch::APPROVED, MassAdjustmentBatch::SCHEDULED].each do |state|
      it "should return true for latest batch in #{state} state" do
        batch = create(:mass_adjustment_batch, status: state)
        create(:mass_adjustment_entry, mass_adjustment_batch: batch)
        expect(MassAdjustmentBatch.batch_in_flight?).to be_truthy
      end
    end
  end

  describe :process_batches_as_completed do
    it "should change the status to completed if all entries have a status of success" do
      batch = create(:mass_adjustment_batch, status: MassAdjustmentBatch::SCHEDULED)

      create(:mass_adjustment_entry, mass_adjustment_batch: batch, status: MassAdjustmentEntry::SUCCESS)
      create(:mass_adjustment_entry, mass_adjustment_batch: batch, status: MassAdjustmentEntry::SUCCESS)

      MassAdjustmentBatch.process_batches_as_completed

      expect(batch.reload.status).to eq(MassAdjustmentBatch::COMPLETED)
    end

    it "should change the status to completed_with_error if all entries are processed" \
      "but entries with a status of error are present" do
      batch = create(:mass_adjustment_batch, status: MassAdjustmentBatch::SCHEDULED)

      create(:mass_adjustment_entry, mass_adjustment_batch: batch, status: MassAdjustmentEntry::SUCCESS)
      create(:mass_adjustment_entry, mass_adjustment_batch: batch, status: MassAdjustmentEntry::ERROR)

      MassAdjustmentBatch.process_batches_as_completed

      expect(batch.reload.status).to eq(MassAdjustmentBatch::COMPLETED_WITH_ERROR)
    end
  end

  describe :processing_completed? do
    it "should return true if all the entries are processed" do
      batch = create(:mass_adjustment_batch)
      create(:mass_adjustment_entry, mass_adjustment_batch: batch, status: MassAdjustmentEntry::SUCCESS)
      create(:mass_adjustment_entry, mass_adjustment_batch: batch, status: MassAdjustmentEntry::ERROR)
      expect(batch.processing_completed?).to be_truthy
    end

    it "should return false if some entries are not yet processed" do
      batch = create(:mass_adjustment_batch)
      create(:mass_adjustment_entry, mass_adjustment_batch: batch, status: nil)
      create(:mass_adjustment_entry, mass_adjustment_batch: batch, status: MassAdjustmentEntry::ERROR)
      expect(batch.processing_completed?).to be_falsey
    end
  end

  describe :needs_negative_values_processed? do
    let(:batch) { create(:mass_adjustment_batch) }
    it "should return true if only positive values have been processed" do
      entry1 = create(:mass_adjustment_entry, mass_adjustment_batch: batch, status: nil, balance_turns_negative: true)
      entry2 = create(:mass_adjustment_entry, mass_adjustment_batch: batch, status: MassAdjustmentEntry::SUCCESS, balance_turns_negative: false)

      expect(batch.needs_negative_values_processed?).to eq(true)
    end

    it "should return false if the batch only contains negative values that have been processed" do
      entry1 = create(:mass_adjustment_entry, mass_adjustment_batch: batch, status: MassAdjustmentEntry::SUCCESS, balance_turns_negative: true)
      entry2 = create(:mass_adjustment_entry, mass_adjustment_batch: batch, status: MassAdjustmentEntry::SUCCESS, balance_turns_negative: true)

      expect(batch.needs_negative_values_processed?).to eq(false)
    end

    it "should return false if all values have been processed" do
      entry1 = create(:mass_adjustment_entry, mass_adjustment_batch: batch, status: MassAdjustmentEntry::SUCCESS, balance_turns_negative: true)
      entry2 = create(:mass_adjustment_entry, mass_adjustment_batch: batch, status: MassAdjustmentEntry::SUCCESS, balance_turns_negative: false)

      expect(batch.needs_negative_values_processed?).to eq(false)
    end
  end

  describe "::incomplete" do
    it "returns mass_adjustment_batches without a completed status" do
      batch1 = create(:mass_adjustment_batch, status: MassAdjustmentBatch::COMPLETED)
      batch2 = create(:mass_adjustment_batch, status: MassAdjustmentBatch::DECLINED)

      create(:mass_adjustment_entry, mass_adjustment_batch: batch1)
      create(:mass_adjustment_entry, mass_adjustment_batch: batch2)

      expect(MassAdjustmentBatch.incomplete.length).to eq(1)
      expect(MassAdjustmentBatch.incomplete.first.id).to eq(batch2.id)
    end

    context "when there are batches with multiple entries" do
      it "returns destinct mass_adjustment_batches" do
        batch = create(:mass_adjustment_batch, status: MassAdjustmentBatch::DECLINED)

        5.times { create(:mass_adjustment_entry, mass_adjustment_batch: batch) }

        expect(MassAdjustmentBatch.incomplete.length).to eq(1)
        expect(MassAdjustmentBatch.incomplete.first.id).to eq(batch.id)
      end
    end
  end

  describe "#errored_entriies" do
    context "when there are errored entries" do
      it "should return the errored mass adjustment entries" do
        batch = create(:mass_adjustment_batch, status: MassAdjustmentBatch::COMPLETED)

        entry1 = create(:mass_adjustment_entry, mass_adjustment_batch: batch)
        entry2 = create(:mass_adjustment_entry, mass_adjustment_batch: batch, status: MassAdjustmentEntry::ERROR)

        expect(batch.errored_entries).to include entry2
        expect(batch.errored_entries).not_to include entry1
      end
    end
  end

  describe "callbacks" do
    let!(:batch) { create(:mass_adjustment_batch, status: MassAdjustmentBatch::SCHEDULED) }
    let!(:entry1) { create(:mass_adjustment_entry, mass_adjustment_batch: batch, status: MassAdjustmentEntry::SUCCESS) }
    let!(:entry2) { create(:mass_adjustment_entry, mass_adjustment_batch: batch, status: MassAdjustmentEntry::SUCCESS) }

    it "schedules tax withholding job when earnings batch status transitions to 'completed'" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(true)
      expect(TaxWithholdings::MassAdjustmentsBatchWorker).to receive(:perform_async)
      MassAdjustmentBatch.process_batches_as_completed
    end

    it "does not trigger batch tax withholding job when us_tax_withholding flag is unset" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(false)
      expect(TaxWithholdings::MassAdjustmentsBatchWorker).not_to receive(:perform_async)
      MassAdjustmentBatch.process_batches_as_completed
    end

    it "does not trigger batch tax withholding job when for status is not 'completed'" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(true)
      expect(TaxWithholdings::MassAdjustmentsBatchWorker).not_to receive(:perform_async)
    end
  end
end
