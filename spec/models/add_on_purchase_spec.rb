require 'rails_helper'

RSpec.describe AddOnPurchase, type: :model do
  let(:person) {create(:person)}
  let(:product) {create(:product)}

  describe "validates presence of person" do
    it "is invalid without a person" do
      purchase = build(:add_on_purchase, person: nil, product: product)
      expect(purchase).not_to be_valid
    end

    it "is valid witha a person" do
      purchase = build(:add_on_purchase, person: person, product: product)
      expect(purchase).to be_valid
    end
  end
end
