require "rails_helper"

describe StoreGroup do

  describe "relationships" do

    describe "#stores" do
      it "should allow adding of stores, and persist" do
        store_group = TCFactory.create(:store_group)
        store = TCFactory.create(:store) # , :name => "Some New Store2", :abbrev => "test", :short_name => "test")
        store_group.stores << store
        store_group.reload

        found = StoreGroup.find(store_group.id)
        expect(found.stores.include?(store)).to eq(true)
      end

      it "should allow adding of store_group_store" do
        store_group = TCFactory.create(:store_group)
        store = TCFactory.create(:store) #, :name => "Some New Store2", :abbrev => "test", :short_name => "test")
        store_group_store = StoreGroupStore.new(:store => store, :store_group => store_group)
        store_group.store_group_stores << store_group_store

        store_group = StoreGroup.find(store_group.id)
        expect(store_group.store_group_stores.include?(store_group_store)).to eq(true)
      end
    end

  end

end
