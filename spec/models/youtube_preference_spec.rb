require "rails_helper"

describe YoutubePreference do

  describe "validations" do
    before(:each) do
      @person = TCFactory.create(:person)
    end

    it "should not allow mcn to be turned off after set" do
      yt = TCFactory.create(:youtube_preference, :whitelist => false, :mcn => true, :person => @person)

      expect(yt.valid?).to eq(true)

      yt.mcn = false
      expect(yt.valid?).to eq(false)
    end

    it "should force mcn_agreement if mcn opted" do
      yt = TCFactory.build(:youtube_preference, :whitelist => true, :mcn => false, :person => @person)
      yt.mcn_agreement = Time.now()
      yt.save!

      expect(yt.valid?).to eq(true)

      yt.mcn = true
      yt.mcn_agreement = nil

      expect(yt.valid?).to eq(false)
    end

    it "should require channel id to be present" do
      yt = TCFactory.build(:youtube_preference, :whitelist => false, :mcn => true, :channel_id => nil, :person => @person)
      expect(yt.channel_id.blank?).to eq(true)
      expect(yt.valid?).to eq(false)
      expect(yt.errors[:channel_id].any?).to eq(true)
    end
  end

  describe "mcn=" do
    before(:each) do 
      @person = TCFactory.create(:person)
    end

    it "should set the agreement when opting in to mcn" do
      yt = TCFactory.create(:youtube_preference, :whitelist => false, :mcn => false, :person => @person)
      expect(yt.valid?).to eq(true)
      expect(yt.mcn_agreement.blank?).to eq(true)

      yt.mcn = true
      expect(yt.mcn_agreement.blank?).to eq(false)
      expect(yt.valid?).to eq(true)
    end
  end
  
  describe "#reset_preferences" do
    before(:each) do
      @person = TCFactory.create(:person)
      @yt_preference = TCFactory.create(:youtube_preference, person: @person, mcn: true, channel_id: "Test Channel", whitelist: true, mcn_agreement: Time.now)
    end
    
    it "should reset all attributes for the yt_preference" do
      @yt_preference.reset_preferences
      expect(@yt_preference.mcn).to eq(false)
      expect(@yt_preference.channel_id).to eq("")
      expect(@yt_preference.whitelist).to eq(false)
      expect(@yt_preference.mcn_agreement).to eq(nil)
    end
  end

end
