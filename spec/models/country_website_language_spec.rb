require "rails_helper"

describe CountryWebsiteLanguage do
  describe 'validations' do
    it 'should return valid if the record is valid' do
      country_website_lang = create(:country_website_language)
      expect(country_website_lang.valid?).to eq(true)
    end

    it "should not be valid if it's missing a a country_website" do
      country_website_lang = build(:country_website_language)
      country_website_lang.country_website = nil

      expect(country_website_lang.valid?).to eq(false)
    end

    it "should not be valid if it's missing a a selector_language" do
      country_website_lang = build(:country_website_language)
      country_website_lang.selector_language = nil

      expect(country_website_lang.valid?).to eq(false)
    end

    it "should not be valid if it's missing a a selector_country" do
      country_website_lang = build(:country_website_language)
      country_website_lang.selector_country = nil

      expect(country_website_lang.valid?).to eq(false)
    end

    it "should not be valid if it's missing a a redirect_country_websites" do
      country_website_lang = build(:country_website_language)
      country_website_lang.redirect_country_website_id = nil

      expect(country_website_lang.valid?).to eq(false)
    end
  end

  describe '.excluded_locales' do
    let(:user) { create(:person) }

    subject { CountryWebsiteLanguage.excluded_locales(user, "US") }

    before { allow(FeatureFlipper).to receive(:show_feature?) }

    context "russian language feature flipper is on" do
      it "should not include russian language in exclusion" do
        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .with(:russian_language_option, user)
          .and_return(true)

        expect(subject).to_not include(CountryWebsiteLanguage::RU_US)
      end
    end

    context "russian language feature flipper is off" do
      it "should include russian language in exclusion" do
        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .with(:russian_language_option, user)
          .and_return(false)

        expect(subject).to include(CountryWebsiteLanguage::RU_US)
      end
    end

    context "indonesian language feature flipper is on" do
      it "should not include indonesian language in exclusion" do
        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .with(:indonesian_language_option, user)
          .and_return(true)

        expect(subject).to_not include(CountryWebsiteLanguage::ID_US)
      end
    end

    context "indonesian language feature flipper is off" do
      it "should include indonesian language in exclusion" do
        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .with(:indonesian_language_option, user)
          .and_return(false)

        expect(subject).to include(CountryWebsiteLanguage::ID_US)
      end
    end

    context "romanian language feature flipper is on" do
      it "should not include romanian language in exclusion" do
        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .with(:romanian_language_option, user)
          .and_return(true)

        expect(subject).to_not include(CountryWebsiteLanguage::RO_US)
      end
    end

    context "romanian language feature flipper is off" do
      it "should include romanian language in exclusion" do
        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .with(:romanian_language_option, user)
          .and_return(false)

        expect(subject).to include(CountryWebsiteLanguage::RO_US)
      end
    end

    context "thai language feature flipper is off" do
      it "should include thai language in exclusion" do
        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .with(:thai_language_option, user)
          .and_return(false)

        expect(subject).to include(CountryWebsiteLanguage::TH_US)
      end
    end

    context "hungarian language feature flipper is off" do
      it "should include hungarian language in exclusion" do
        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .with(:hungarian_language_option, user)
          .and_return(false)

        expect(subject).to include(CountryWebsiteLanguage::HU_US)
      end
    end

    context "dutch language feature flipper is off" do
      it "should include dutch language in exclusion" do
        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .with(:dutch_language_option, user)
          .and_return(false)

        expect(subject).to include(CountryWebsiteLanguage::NL_US)
      end
    end

    context "czech language feature flipper is off" do
      it "should include czech language in exclusion" do
        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .with(:czech_language_option, user)
          .and_return(false)

        expect(subject).to include(CountryWebsiteLanguage::CS_US)
      end
    end

    context "turkish language feature flipper is off" do
      it "should include turkish language in exclusion" do
        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .with(:turkish_language_option, user)
          .and_return(false)

        expect(subject).to include(CountryWebsiteLanguage::TR_US)
      end
    end

    context "polish language feature flipper is off" do
      it "should include polish language in exclusion" do
        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .with(:polish_language_option, user)
          .and_return(false)

        expect(subject).to include(CountryWebsiteLanguage::PL_US)
      end
    end

    context "french canadian language feature flipper is off" do
      it "should include french canadian language in exclusion" do
        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .with(:fr_ca_language_option, user)
          .and_return(false)

        expect(subject).to include(CountryWebsiteLanguage::FR_CA)
      end
    end

    context "simplified chinese language feature flipper is off" do
      it "should include simplified chinese language in exclusion" do
        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .with(:sc_language_option, user)
          .and_return(false)

        expect(subject).to include(CountryWebsiteLanguage::SC_US)
      end
    end

    context "traditional chinese language feature flipper is off" do
      it "should include traditional chinese language in exclusion" do
        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .with(:tc_language_option, user)
          .and_return(false)

        expect(subject).to include(CountryWebsiteLanguage::TC_US)
      end
    end
  end

  describe '.filter_locales' do
    let(:user) { create(:person) }

    it "should exclude locales from the excluded locales list" do
      allow(CountryWebsiteLanguage)
        .to receive(:excluded_locales)
        .with(user, "US")
        .and_return([CountryWebsiteLanguage::RU_US])

      languages = CountryWebsiteLanguage.filter_locales(user, "US")
      expect(languages.pluck(:yml_languages)).to_not include(CountryWebsiteLanguage::RU_US)
    end
  end

  describe "mapping to LOCALE_TO_COUNTRY_MAP" do
    it "should return a valid country_website country value for all present country_website_language yml_languages" do
      yml_languages = CountryWebsiteLanguage.where.not(yml_languages: nil).map(&:yml_languages)

      yml_languages.each do |yml_language|
        mapped_country_value = LOCALE_TO_COUNTRY_MAP[yml_language.to_s]

        expect(mapped_country_value).to be_truthy

        expect(CountryWebsite.find_by(country: mapped_country_value)).to be_truthy
      end
    end
  end
end
