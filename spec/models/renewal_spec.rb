require "rails_helper"

describe Renewal, "creating a new renewal" do
  it "should return the starts_at date matching the minimum renewal_history and the expires_at date matching the maximum renewal_history" do
    person = create(:person)
    product = create(:product)
    product_item = create(
      :product_item,
      product: product,
      renewal_duration: 1,
      renewal_interval: "year"
    )
    renewal = create(:renewal, person: person, item_to_renew: product_item)
    renewal_history = renewal.history.first

    expect(renewal.starts_at).to eq(renewal_history.starts_at)
    expect(renewal.expires_at).to eq(renewal_history.expires_at)
  end
end

describe Renewal do
  it "should ask renewal its first renewal dates are set" do
    person = create(:person)
    product = create(:product)
    item = create(
      :product_item,
      product: product,
      renewal_duration: 1,
      renewal_interval: nil
    )
    renewal = create(:renewal, person: person, item_to_renew: item)

    expect(renewal.has_first_renewal_settings?).to eq(false)
  end

  it "should set the first newal settings for a given product item" do
    person = create(:person)
    product = create(:product)
    item = create(
      :product_item,
      product: product,
      renewal_duration: 1,
      renewal_interval: "year",
      first_renewal_interval: "year",
      first_renewal_duration: 1
    )
    renewal = create(:renewal, person: person, item_to_renew: item)

    expect(renewal.has_first_renewal_settings?).to eq(true)
  end
end

describe Renewal, "for subscriptions and renewals" do
  it "should consider any distributable item not a subscription, but a renewal" do
    person = create(:person)
    product = create(:product)
    item = create(
      :product_item,
      product: product,
      renewal_duration: 1,
      renewal_interval: "year"
    )
    renewal = create(:renewal, person: person, item_to_renew: item)
    renewal_item = create(:renewal_item, renewal: renewal, related: product)

    expect(renewal.is_subscription?).to eq(true)
  end

  it "should consider any distributable item a renewal" do
    person = create(:person)
    product = create(:product)
    item = create(
      :product_item,
      product: product,
      renewal_duration: 1,
      renewal_interval: "year"
    )
    renewal = create(:renewal, person: person, item_to_renew: item)
    album = create(:album)
    renewal_item = create(:renewal_item, renewal: renewal, related: album)

    expect(renewal.is_subscription?).to eq(false)
  end
end

describe Renewal, "is an unpaid/expired renewal?" do
  it "is not unpaid if there is not a renewal" do
    album = build(:album)

    expect(Renewal).to receive(:renewal_for).and_return(nil)
    expect(Renewal.unpaid_renewal?(album)).to eq(false)
  end

  it "is an unpaid renwal if the renewal object from renewal_for's expires_within? returns true" do
    person = create(:person)
    product = create(:product)
    item = create(
      :product_item,
      product: product,
      renewal_duration: 1,
      renewal_interval: "year"
    )
    renewal = create(:renewal, person: person, item_to_renew: item)
    album = create(:album)

    expect(renewal).to receive(:expires_within?).and_return(true)
    expect(Renewal).to receive(:renewal_for).and_return(renewal)
    expect(Renewal.unpaid_renewal?(album)).to eq(true)
  end

  it "is not an unpaid renewal if the renewal object from renewal_for's expires_within? returns false and expired? returns false" do
    person = create(:person)
    product = create(:product)
    item = create(
      :product_item,
      product: product,
      renewal_duration: 1,
      renewal_interval: "year"
    )
    renewal = create(:renewal, person: person, item_to_renew: item)
    album = create(:album)

    expect(renewal).to receive(:expires_within?).and_return(false)
    expect(Renewal).to receive(:renewal_for).and_return(renewal)
    expect(Renewal.unpaid_renewal?(album)).to eq(false)
  end

  it "should be expired if takedown_at and canceled_at are nil and expires_at occurs before the current time" do
    person = create(:person)
    product = create(:product)
    item = create(
      :product_item,
      product: product,
      renewal_duration: 1,
      renewal_interval: "year"
    )
    renewal = create(:renewal, :no_takedown_at, :expired, person: person, item_to_renew: item, canceled_at: nil)

    expect(renewal.expired?).to eq(true)
  end

  it "shouldn't be expired if takedown_at is not nil" do
    person = create(:person)
    product = create(:product)
    item = create(
      :product_item,
      product: product,
      renewal_duration: 1,
      renewal_interval: "year"
    )
    renewal = create(:renewal, person: person, item_to_renew: item)

    expect(renewal.expired?).to eq(false)
  end

  it "shouldn't be expired if canceled_at is not nil" do
    person = create(:person)
    product = create(:product)
    item = create(
      :product_item,
      product: product,
      renewal_duration: 1,
      renewal_interval: "year"
    )
    renewal = create(:renewal, person: person, item_to_renew: item)
    allow(renewal).to receive(:takedown_at).and_return(nil)
    allow(renewal).to receive(:canceled_at).and_return(Time.now)

    expect(renewal.expired?).to eq(false)
  end

  it "should return correct takedown_date if not expired or canceled" do
    product = create(:product)
    renewal_product_item = create(:product_item, product: product,
                                                 first_renewal_interval: "year", first_renewal_duration: 1, renewal_interval: "year", renewal_duration: 1)
    person   = create(:person)
    album    = create(:album, person: person)
    purchase = create(:purchase, person: person, related: album, product: product)
    renewal = create(:renewal, :no_takedown_at, person: person, item_to_renew: renewal_product_item, purchase: purchase, canceled_at: nil)
    renewal.renew!(purchase)

    # Yearly
    expect(renewal.takedown_date).to eq(renewal.expires_at + Renewal::GRACE_DAYS_BEFORE_TAKEDOWN.days)

    # Monthly
    renewal.item_to_renew.renewal_interval = "month"
    renewal.renewal_history.build(expires_at: Date.today + 1.month)

    expect(renewal.takedown_date).to eq(renewal.expires_at + Renewal::GRACE_DAYS_BEFORE_TAKEDOWN_MONTHLY.days)
  end
end

describe Renewal, "when building invoices with people due for renewal" do
  before(:each) do
    @person  = create(:person)
    @person2 = create(:person)

    product = TCFactory.generate_album_product(4999)
    TCFactory.generate_album_purchase(@person, true, product: product)
    TCFactory.generate_album_purchase(@person2, true, product: product)
    @expire_date = @person.renewals.first.renewal_history.first.expires_at
    @product = create(:product, :album_renewal)
  end

  it "should use a targeted product for only people that have targeted products" do
    @targeted_offer   = TCFactory.create(:targeted_offer)
    @targeted_product = TargetedProduct.create(product: @product, targeted_offer: @targeted_offer)
    @targeted_person  = TargetedPerson.create(person: @person, targeted_offer: @targeted_offer)

    # Create purchases for renewals due
    Renewal.create_renewal_purchases(@expire_date)

    # Make sure the only person1 who is in the offer gets the targeted product price
    expect(@person.purchases.last.targeted_product_id).to eq(@targeted_product.id)
    expect(@person2.purchases.last.targeted_product_id).to be_nil
  end

  it "should yield by person" do
    yield_count = 0
    # Create purchases for renewals due
    allow_any_instance_of(Album).to receive(:has_ever_been_approved?).and_return(true)
    Renewal.create_renewal_purchases(@expire_date) do |person, purchases|
      yield_count += 1
      expect(purchases).not_to be_empty
      purchases.each do |purchase|
        expect(purchase.person).to eq(person)
      end
    end
    expect(yield_count).to eq(2)
  end

  it "should not yield people with disabled auto-renew" do
    yield_count = 0
    # Create purchases for renewals due
    @person2.disable_auto_renewal({})
    @person2.person_preference.save!(validate: false)
    @person2.save!
    allow_any_instance_of(Album).to receive(:has_ever_been_approved?).and_return(true)
    expect(@person2.person_preference&.do_not_autorenew?).to be_truthy
    people = []
    Renewal.create_renewal_purchases(@expire_date) do |person, purchases|
      yield_count += 1
      expect(purchases).not_to be_empty
      people << person
      purchases.each do |purchase|
        expect(purchase.person).to eq(person)
      end
    end
    expect(people.size).to eq(1)
    expect(people).to include(@person)
    expect(people).not_to include(@person2)
    expect(yield_count).to eq(1)
  end

  it "does NOT trigger BlockedRenewalsMailer" do
    allow_any_instance_of(Album).to receive(:has_ever_been_approved?).and_return(true)

    allow(BlockedRenewalsMailer).to receive(:blocked_renewals)
    allow(Renewal).to receive(:renewals_blocked_for_person?)

    Renewal.create_renewal_purchases(@expire_date)

    expect(Renewal).to have_received(:renewals_blocked_for_person?).exactly(2).times
    expect(BlockedRenewalsMailer).not_to have_received(:blocked_renewals)
  end

  it "should not yield for person with sanctioned country" do
    yield_count = 0
    @person2.update(country: Country.sanctioned.first.iso_code)
    allow_any_instance_of(Album).to receive(:has_ever_been_approved?).and_return(true)

    expect(BlockedRenewalsMailer).to receive(:blocked_renewals)
                                     .exactly(1)
                                     .times
                                     .with(person: @person2, renewals: @person2.renewals.active)
                                     .and_return(double("Mail::Message", deliver: double))

    people = []
    Renewal.create_renewal_purchases(@expire_date) do |person, purchases|
      yield_count += 1
      expect(purchases).not_to be_empty
      people << person
      purchases.each { |purchase| expect(purchase.person).to eq(person) }
    end

    expect(people.size).to eq(1)
    expect(people).to      include(@person)
    expect(people).not_to  include(@person2)
    expect(yield_count).to eq(1)
  end

  it "should exclude releases that were never approved" do
    expect(Renewal).to receive(:skip_related_album?).exactly(2).times.and_return(true)
    Renewal.create_renewal_purchases(@expire_date) do |_person, purchases|
      expect(purchases).to be_empty
    end
  end

  it "should exclude releases for people with plans" do
    create(:person_plan, person: @person)
    create(:person_plan, person: @person2)

    expect(Renewal).to receive(:skip_related_plan?).exactly(2).times.and_return(true)
    Renewal.create_renewal_purchases(@expire_date) do |_person, purchases|
      expect(purchases).to be_empty
    end
  end
end

describe Renewal, "::add_additional_artists?" do
  let!(:non_plan_person) { create(:person) }
  let!(:plan_person) { create(:person) }
  let!(:plan_person_with_artists) { create(:person) }
  let!(:plan_person_person_plan) { create(:person_plan, person: plan_person) }
  let!(:artists_person_plan) { create(:person_plan, person: plan_person_with_artists) }
  let!(:album) { create(:album, person: non_plan_person) }
  before(:each) do
    creative1 = {name: "Star Laser Beam", role: Creative::PRIMARY_ARTIST_ROLE}.with_indifferent_access
    creative2 = {name: "World Sized Drill", role: Creative::PRIMARY_ARTIST_ROLE}.with_indifferent_access
    creative3 = {name: "Drill Sized Star", role: Creative::PRIMARY_ARTIST_ROLE}.with_indifferent_access
    create(:album, :paid, :with_salepoints, creatives: [creative1], person: plan_person_with_artists)
    create(:album, :paid, :with_salepoints, creatives: [creative2], person: plan_person_with_artists)
    create(:album, :paid, :with_salepoints, creatives: [creative3], person: plan_person_with_artists)
  end
  context "related is PersonPlan, user has plan, user has additional artists" do
    it "returns true" do
      expect(Renewal.add_additional_artists?(artists_person_plan, plan_person_with_artists)).to be(true)
    end
  end
  context "user doesn't have additional artists" do
    it "returns false" do
      expect(Renewal.add_additional_artists?(plan_person_person_plan, plan_person)).to be(false)
    end
  end
  context "user doesn't have a plan" do
    it "returns false" do
      expect(Renewal.add_additional_artists?(album, non_plan_person)).to be(false)
    end
  end
end

describe Renewal, "requested downgrades flow (::handle_plan_downgrade_condition" do
  subject { Renewal.handle_plan_downgrade_condition(person, renewals) }
  let!(:person) { create(:person, :with_plan_renewal) }
  let!(:requested_plan) { Plan.first }
  let!(:renewals) { person.renewals.to_a }
  reason = DowngradeCategoryReason.first
  context "with active downgrade request" do
    let!(:downgrade_request) { PlanDowngradeRequest.create(
      person_id: person&.id,
      status: PlanDowngradeRequest::ACTIVE_STATUS,
      reason_id: reason&.id,
      requested_plan_id: requested_plan&.id
    )}

    
    it "calls Plans::RequestedDowngradeService" do
      expect(Plans::RequestedDowngradeService).to receive(:call).and_call_original
      expect(subject).to_not be(renewals)
    end

  end

  context "with inactive downgrade request" do
    let!(:downgrade_request) { PlanDowngradeRequest.create(
      person_id: person&.id,
      status: PlanDowngradeRequest::CANCELED_STATUS,
      reason_id: reason&.id,
      requested_plan_id: requested_plan&.id
    )}
    it "does not call Plans::RequestedDowngradeService and returns renewals unchanged" do
      expect(Plans::RequestedDowngradeService).to_not receive(:call)
      expect(subject).to be(renewals)
    end
    
  end

  context "with no downgrade request" do

    it "does not call Plans::RequestedDowngradeService and returns renewals unchanged" do
      expect(Plans::RequestedDowngradeService).to_not receive(:call)
      expect(subject).to be(renewals)
    end
    
  end
end

describe Renewal, "#plan_renewal?" do
  subject { renewal.plan_renewal? }
  let!(:person) { create(:person) }
  let!(:product) { Product.find_by(
    country_website_id: person.country_website_id,
    name: "1 Year Album"
  ) }
  let!(:renewal_product_item) { product.product_items.first }
  let!(:related) { create(:album, person: person) }
  let!(:purchase) { create(:purchase, person: person, related: related, product: product) }
  let!(:renewal) { create(:renewal, person: person, item_to_renew: renewal_product_item, purchase: purchase) }
  context "item_to_renew is a ProductItem" do
    
    context "item_to_renew_id is in PLAN_PRODUCT_ITEM_IDS" do
      let!(:person) { create(:person, :with_person_plan)}
      let!(:product) { person.plan.products.find_by(country_website_id: person.country_website_id)}
      let!(:related) { product }
      it "returns true" do
        expect(subject).to be(true)
      end
    end

    context "item_to_renew_id is NOT in PLAN_PRODUCT_ITEM_IDS" do
      it "returns false" do
        expect(subject).to be(false)
      end
    end
  end

  context "item_to_renew is not a ProductItem" do
    let!(:renewal) { create(:renewal, person: person, item_to_renew: product, purchase: purchase) }
    it "returns false" do
      expect(subject).to be(false)
    end
  end
end

describe Renewal, "when pricing the renewal" do
  before(:each) do
    # Generate album purchases
    @person = create(:person)
    TCFactory.generate_album_purchase(@person)

    # Create a targeted product targeting the album renewal
    @to = create(:targeted_offer, status: "Inactive", country: "United Kingdom")
    @product = Product.where( product_type: "Renewal", applies_to_product: "Album" ).first
    @tp = @to.targeted_products.create(product: @product, price_adjustment: 10)
    @to.update(status: "Active")

    @to.targeted_people.create(person: @person)
  end

  context "and renewal is targeted with renewal targeted product flags" do
    before(:each) do
      @tp.renewals_past_due  = true
      @tp.renewals_due       = false
      @tp.renewals_upcomming = false
      expect(@tp.save).to eq(true)
    end

    it "should not use the targeted product price if it not within targeted product renewal window" do
      expect(@person.renewals.first.price_to_renew).to eq(@product.price)
    end

    it "should use the targeted product price if it is within the targeted product renewal window" do
      renewal = @person.albums.first.renewal.renewal_history.last
      renewal.update_attribute(:expires_at, Date.today - 31.day)

      expect(@person.renewals.first.reload.price_to_renew).to be < @product.price.to_f
    end
  end

  context "renewal is targeted without renewal flags" do
    it "should use the targeted product price" do
      expect(@person.renewals.first.reload.price_to_renew).to be < @product.price.to_f
    end
  end
end

describe Renewal, "when pricing the 'opposite' product" do
  before(:each) do
    # Generate album purchases
    @person = create(:person)

    # Generate a yearly album product
    TCFactory.generate_album_product

    # Create a targeted product targeting the album renewal's opposite product
    @to = TCFactory.create(:targeted_offer, status: "Inactive", country: "United Kingdom")
    @product = Product.where( product_type: "Renewal", applies_to_product: "Album" ).first
    @tp = @to.targeted_products.create(product: @product, price_adjustment: 10)
    @to.update(status: "Active")

    @to.targeted_people.create(person: @person)

    # Purchase a monthly album
    TCFactory.generate_album_purchase(@person, true, renewal_interval: "month")
  end

  context "renewal is targeted with renewal targeted product flags" do
    before(:each) do
      @tp.renewals_past_due  = true
      @tp.renewals_due       = false
      @tp.renewals_upcomming = false
      expect(@tp.save).to eq(true)
    end

    it "should not use the targeted product price if it not within targeted product renewal window" do
      expect(@person.renewals.first.opposite_price_to_renew).to eq(@product.price)
    end

    it "should use the targeted product price if it is within the targeted product renewal window" do
      renewal = @person.albums.first.renewal.renewal_history.last
      renewal.update_attribute(:expires_at, Date.today - 31.day)

      expect(@person.renewals.first.opposite_price_to_renew).to be < @product.price
    end
  end

  context "renewal is targeted without renewal flags" do
    it "should use the targeted product price" do
      expect(@person.renewals.first.opposite_price_to_renew).to be < @product.price
    end
  end
end

describe Renewal, "takendown named scope" do
  before(:each) do
    allow_any_instance_of(Renewal).to receive(:create_renewal_history)
    allow_any_instance_of(Renewal).to receive(:create_renewal_items)
    album = create(:album)
    purchase = create(:purchase, related: album, person: album.person)

    @renewal1 = create(:renewal, item_to_renew: ProductItem.first, takedown_at: nil, person_id: album.person_id, purchase: purchase)
    @renewal2 = create(:renewal, item_to_renew: ProductItem.first, takedown_at: Time.now, person_id: album.person_id, purchase: purchase)
  end

  it "should return renewals according to takedown true/false" do
    expect(Renewal.takendown(true).count).to be > 0
    Renewal.takendown(true).each do |r|
      expect(r.takedown_at).not_to be_nil
    end

    expect(Renewal.takendown(false).count).to be > 0
    Renewal.takendown(false).each do |r|
      expect(r.takedown_at).to be_nil
    end
  end
end

describe Renewal, "canceled named scope" do
  before(:each) do
    allow_any_instance_of(Renewal).to receive(:create_renewal_history)
    allow_any_instance_of(Renewal).to receive(:create_renewal_items)
    album = create(:album)
    purchase = create(:purchase, related: album, person: album.person)

    @renewal1 = create(:renewal, item_to_renew: ProductItem.first, canceled_at: nil, person_id: album.person_id, purchase: purchase)
    @renewal2 = create(:renewal, item_to_renew: ProductItem.first, canceled_at: Time.now, person_id: album.person_id, purchase: purchase)
  end

  it "should return renewals according to takedown true/false" do
    expect(Renewal.canceled(true).count).to be > 0
    Renewal.canceled(true).each do |r|
      expect(r.canceled_at).not_to be_nil
    end

    expect(Renewal.canceled(false).count).to be > 0
    Renewal.canceled(false).each do |r|
      expect(r.canceled_at).to be_nil
    end
  end
end

describe Renewal, "expires named scopes" do
  before(:each) do
    @date = Date.today

    allow_any_instance_of(Renewal).to receive(:create_renewal_history)
    allow_any_instance_of(Renewal).to receive(:create_renewal_items)
    album = create(:album)
    purchase = create(:purchase, related: album, person: album.person)

    # Renewal that does not expire on the date
    @renewal1 = create(:renewal, item_to_renew: ProductItem.first, person_id: album.person_id, purchase: purchase)
    @renewal1.renewal_history.new(expires_at: @date.to_time - 1.week, purchase_id: 1).save(validate: false)

    # Renewal whose only expiration is the date
    @renewal2 = create(:renewal, item_to_renew: ProductItem.first, person_id: album.person_id, purchase: purchase)
    @renewal2.renewal_history.new(expires_at: @date.to_time, purchase_id: 1).save(validate: false)

    # Renewal who has a newer expiration but previously expired on the date
    @renewal3 = create(:renewal, item_to_renew: ProductItem.first, person_id: album.person_id, purchase: purchase)
    @renewal3.renewal_history.new(expires_at: @date.to_time, purchase_id: 1).save(validate: false)
    @renewal3.renewal_history.new(expires_at: @date.to_time + 1.week, purchase_id: 1).save(validate: false)
  end

  describe "expires_between" do
    it "should return renewals who expires or once expired between the dates" do
      expect(Renewal.expires_between(@date - 2.weeks, @date).count).to eq(3)
      expect(Renewal.expires_between(@date - 2.days, @date + 1.day).count).to   eq(2)
      expect(Renewal.expires_between(@date + 2.days, @date + 2.weeks).count).to eq(1)
    end
  end
end

describe Renewal, "with deactivated product" do
  it "should renew" do
    person = people(:tunecore_admin)
    TCFactory.generate_album_purchase(person)

    # Deactivate the product
    purchase = person.purchases.first
    product  = purchase.product
    product.update_attribute(:status, "Inactive")
    assert product.reload.status == "Inactive"

    # Get the renewal
    album = purchase.related
    renewal = album.renewal

    # Get renewal product
    renewal_product = product.product_items.first.renewal_product

    # Assert we can add to cart and generate a valid purchase
    purchase = Product.add_to_cart(person, renewal, renewal_product)
    assert purchase.valid?
    assert purchase.cost_cents == renewal_product.price * 100
    product.update_attribute(:status, "Active")
  end
end

describe Renewal, "change_album_renwal_length" do
  it "should allow change to a product item" do
    person = create(:person, country_website_id: 1)
    renewal = Renewal.new(person: person)

    renewal_product = Product.find(Product::US_TWO_YEAR_ALBUM_PRODUCT_ID)
    product_item = renewal_product.product_items.first
    product_change = double("renewal_product_change")
    expect(RenewalProductChange).to receive(:create).with(renewal: renewal, new_item_to_renew: product_item).and_return(product_change)
    expect(product_change).to receive(:valid?)

    renewal.change_album_renewal_length('Album', 'year', 2)
  end
end

describe Renewal, "creation using virtual purchase attribute" do
  before(:each) do
    allow_any_instance_of(Renewal).to receive(:create_renewal_history)
    allow_any_instance_of(Renewal).to receive(:create_renewal_items)
    @album = create(:album)
  end

  it "missing purchase causes validation error" do
    renewal = Renewal.new(item_to_renew: @album, person_id: @album.person_id, purchase: nil)
    expect(renewal).not_to be_valid
    expect(renewal.errors.size).to eq(1)
    expect(renewal.errors.include?(:purchase)).to eq(true)
  end

  it "purchase included causes no errors" do
    purchase = create(:purchase, related: @album, person: @album.person)
    renewal = Renewal.new(item_to_renew: ProductItem.first, person_id: @album.person_id, purchase: purchase)
    expect(renewal).to be_valid
  end
end

describe Renewal do
  before(:each) do
    album    = create(:album, :with_one_year_renewal)
    @renewal = album.renewal
  end

  describe "#expired_live_and_not_canceled?" do
    before(:each) do
      allow(@renewal).to receive(:taken_down?).and_return(false)
      allow(@renewal).to receive(:canceled?).and_return(false)
      allow(@renewal).to receive(:expired?).and_return(true)
    end

    context "when expired, not taken down and not canceled" do
      it "returns true" do
        expect(@renewal.expired_live_and_not_canceled?).to eq true
      end
    end

    context "when not expired" do
      it "returns false" do
        allow(@renewal).to receive(:expired?).and_return(false)
        expect(@renewal.expired_live_and_not_canceled?).to eq false
      end
    end

    context "when taken down" do
      it "returns false" do
        allow(@renewal).to receive(:taken_down?).and_return(true)
        expect(@renewal.expired_live_and_not_canceled?).to eq false
      end
    end

    context "when canceled" do
      it "returns false" do
        allow(@renewal).to receive(:canceled?).and_return(true)
        expect(@renewal.expired_live_and_not_canceled?).to eq false
      end
    end
  end

  describe "#expired?" do
    context "when expires_at is before today" do
      it "returns true" do
        allow(@renewal).to receive(:expires_at).and_return(Date.yesterday)
        expect(@renewal.expired?).to eq true
      end
    end

    context "when expires_at is today" do
      it "returns true" do
        allow(@renewal).to receive(:expires_at).and_return(Time.zone.today)
        expect(@renewal.expired?).to eq true
      end
    end

    context "when expires_at is after today" do
      it "returns false" do
        allow(@renewal).to receive(:expires_at).and_return(Date.tomorrow)
        expect(@renewal.expired?).to eq false
      end
    end
  end

  describe "#canceled?" do
    context "when canceled_at is nil" do
      it "returns false" do
        allow(@renewal).to receive(:canceled_at?).and_return(false)
        expect(@renewal.canceled?).to eq false
      end
    end

    context "when canceled_at is NOT nil" do
      it "returns true" do
        allow(@renewal).to receive(:canceled_at?).and_return(true)
        expect(@renewal.canceled?).to eq true
      end
    end
  end

  describe "#taken_down?" do
    context "when takedown_at is nil" do
      it "returns false" do
        allow(@renewal).to receive(:takedown_at?).and_return(false)
        expect(@renewal.taken_down?).to eq false
      end
    end

    context "when takedown_at is NOT nil" do
      it "returns true" do
        allow(@renewal).to receive(:takedown_at?).and_return(true)
        expect(@renewal.taken_down?).to eq true
      end
    end
  end

  describe "expires_at" do
    it "returns the expires_at value of the associated renewal_history with the greatest expires_at date" do
      @date_in_past = 1.year.ago
      @renewal.renewal_history.first.update_attribute("expires_at", @date_in_past)
      @renewal.send(:write_attribute, "expires_at", nil) # to clear out the cached attribute and allow for query with updated renewal_history
      expect(@renewal.expires_at.utc.to_date).to eq @date_in_past.utc.to_date
    end
  end

  describe "current_term_starts_at" do

    let!(:renewal) do
      album    = create(:album, :with_one_year_renewal)
      album.renewal
    end

    context "with 1 renewal history" do
      
      let(:date_further_in_past) { 2.year.ago }
      
      it "returns the starts_at value of the associated renewal_history" do
        renewal.renewal_history.first.update_attribute("starts_at", date_further_in_past)
        expect(renewal.current_term_starts_at.utc.to_date).to eq date_further_in_past.utc.to_date
      end
    end

    context "with multiple renewal histories" do
      
      let(:date_in_ten_years) { Date.today + 10.years }
      let(:date_in_nine_years) { Date.today + 9.years }
      let(:date_further_in_past) { 2.year.ago }
      let!(:another_renewal_history) { RenewalHistory.new(expires_at: date_in_ten_years, starts_at:date_in_nine_years, purchase_id:1, renewal:renewal) }

      it "returns the starts_at value of the associated renewal_history with the greatest expires_at date" do
        another_renewal_history.save(validate: false)
        expect(renewal.current_term_starts_at.utc.to_date).to eq another_renewal_history.starts_at
      end

    end
    
  end

  describe ".expires_on_date" do
    it "returns renewals expiring on the passed in date" do
      product = create(:product)
      product_item = create(
        :product_item,
        :with_duration,
        :yearly,
        product: product
      )
      renewal_without_takedown = create(
        :renewal,
        :no_takedown_at,
        item_to_renew: product_item
      )

      result = Renewal.expires_on_date(
        false,
        renewal_without_takedown.renewal_history.first.expires_at
      )

      expect(result).to include(renewal_without_takedown)
    end
  end

  describe ".expires_within_dates" do
    context "when one date is passed in" do
      it "returns renewals that expire between date and 5 weeks from date" do
        product = create(:product)
        product_item = create(
          :product_item,
          :with_duration,
          :yearly,
          product: product
        )
        renewal = create(
          :renewal,
          :no_takedown_at,
          item_to_renew: product_item
        )
        expiration_date = renewal.renewal_history.first.expires_at

        result = Renewal.expires_within_dates(expiration_date - 5.weeks)

        expect(result).to include(renewal)

        result = Renewal.expires_within_dates(expiration_date - 7.weeks)

        expect(result).not_to include(renewal)
      end
    end

    context "when two dates are passed in" do
      it "returns renewals that expire between those two dates" do
        product = create(:product)
        product_item = create(
          :product_item,
          :with_duration,
          :yearly,
          product: product
        )
        renewal = create(
          :renewal,
          :no_takedown_at,
          item_to_renew: product_item
        )
        expiration_date = renewal.renewal_history.first.expires_at

        result = Renewal.expires_within_dates(
          expiration_date - 2.days, expiration_date + 3.days
        )

        expect(result).to include(renewal)

        result = Renewal.expires_within_dates(
          expiration_date + 2.days, expiration_date + 5.days
        )

        expect(result).not_to include(renewal)
      end
    end
  end
  describe "#mark_as_takedown!" do
    before(:each) do
      @person = create(:person)
      product = create(:product)
      item = create(
          :product_item,
          product: product,
          renewal_duration: 1,
          renewal_interval: "year"
      )
      @renewal = create(:renewal, person: @person, item_to_renew: item)
      @purchase = create(:purchase, person: @person, related: @renewal, product: product, paid_at: nil)
    end
    it "creates an InvoiceLog" do
      initial_log_count = InvoiceLog.count

      @renewal.mark_as_takedown!

      log = InvoiceLog.all[-1]
      expected_options = {
          person_id: @person.id,
          current_method_name: "mark_as_takedown!",
          message: "",
          renewal_id: @renewal.id,
          purchase_id: @purchase.id,
          purchase_created_at: @purchase.created_at,
          purchase_product_id: @purchase.product_id,
          purchase_cost_cents: @purchase.cost_cents,
          purchase_discount_cents: @purchase.discount_cents,
          purchase_currency: @purchase.currency,
          purchase_paid_at: @purchase.paid_at,
          purchase_salepoint_id: @purchase.salepoint_id,
          purchase_no_purchase_album_id: @purchase.no_purchase_album_id,
          purchase_related_id: @purchase.related_id,
          purchase_related_type: @purchase.related_type,
          purchase_targeted_product_id: @purchase.targeted_product_id,
          purchase_price_adjustment_histories_count: @purchase.price_adjustment_histories_count,
          purchase_vat_amount_cents: @purchase.vat_amount_cents,
          purchase_status: @purchase.status
      }
      actual_options = {
          person_id: log.person_id,
          current_method_name: log.current_method_name,
          message: log.message,
          renewal_id: log.renewal_id,
          purchase_id: log.purchase_id,
          purchase_created_at: log.purchase_created_at,
          purchase_product_id: log.purchase_product_id,
          purchase_cost_cents: log.purchase_cost_cents,
          purchase_discount_cents: log.purchase_discount_cents,
          purchase_currency: log.purchase_currency,
          purchase_paid_at: log.purchase_paid_at,
          purchase_salepoint_id: log.purchase_salepoint_id,
          purchase_no_purchase_album_id: log.purchase_no_purchase_album_id,
          purchase_related_id: log.purchase_related_id,
          purchase_related_type: log.purchase_related_type,
          purchase_targeted_product_id: log.purchase_targeted_product_id,
          purchase_price_adjustment_histories_count: log.purchase_price_adjustment_histories_count,
          purchase_vat_amount_cents: log.purchase_vat_amount_cents,
          purchase_status: log.purchase_status
      }

      expect(expected_options).to eq(actual_options)
      expect(initial_log_count + 1).to eq(InvoiceLog.count)
    end
    it "sets takedown_at" do
      @renewal.mark_as_takedown!
      expect(@renewal.takedown_at).to_not be(nil)
    end
    it "destroys purchase renewal is related to" do
      @renewal.mark_as_takedown!
      purchase_after_takedown = Purchase.find_by(id: @purchase.id)
      expect(purchase_after_takedown).to be(nil)
    end
  end

  describe "#self.make_expires_at_cache_for_albums" do
    it "should build a cache of album ids to their latest expiration dates" do
      renewed_album = create(:album, :with_one_year_renewal)
      renewal_history = renewed_album.renewal_histories.first

      albums = Album.where(id: renewed_album)
      cache = Renewal.make_expires_at_cache_for_albums(albums, Single.none, Ringtone.none)

      expected_cache = {}
      expected_cache[renewed_album.id] = renewal_history.expires_at

      expect(cache).to eq(expected_cache)
    end
  end
end
