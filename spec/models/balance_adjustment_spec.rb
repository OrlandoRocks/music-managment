require "rails_helper"

describe BalanceAdjustment, "Testing credit scenarios" do
  before(:each) do
		PersonTransaction.delete_all
		BalanceAdjustment.delete_all
		PersonBalance.delete_all
    PersonIntake.delete_all
		Person.delete_all
    @admin = TCFactory.build_person(:force_is_administrator => true)
    @person = TCFactory.build_person(:email=>"balance_adjustment_test@tunecore.com")
    @initial_balance = @person.person_balance.reload.balance
    @balance_adjustment = TCFactory.create(:balance_adjustment, :person=>@person, :posted_by => @admin,:posted_by_name=>@admin.name, :credit_amount=>100.0, :debit_amount=>0.00)
    @balance_after_adjustment = @person.person_balance.reload.balance
    @balance_adjustment_rollback = TCFactory.create(:balance_adjustment, :person=>@person, :posted_by => @admin,:posted_by_name=>@admin.name, :rollback=>1, :related_id=>@balance_adjustment.id)
    @balance_after_rollback = @person.person_balance.reload.balance
  end

  it "should have the original credit amount == the rollback debit amount" do

    expect(PersonTransaction.where(target_id: @balance_adjustment.id, target_type: "BalanceAdjustment").first.credit).to eq(@balance_adjustment_rollback.debit_amount)
  end

  it "should have the original debit amount == the rollback credit amount" do
    expect(PersonTransaction.where(target_id: @balance_adjustment.id, target_type: "BalanceAdjustment").first.debit).to eq(@balance_adjustment_rollback.credit_amount)
  end

  it "should populate the transaction type with 'BalanceAdjustment'" do
    expect(PersonTransaction.where(target_id: @balance_adjustment.id, target_type: "BalanceAdjustment").length).to eq(1)
  end

  it "should populate the rollback transaction type with 'BalanceAdjustment'" do
    expect(PersonTransaction.where(target_id: @balance_adjustment_rollback.id, target_type: "BalanceAdjustment").length).to eq(1)
  end

  it "should have the person_balance = 0 before any transactions" do
    expect(@initial_balance).to eq(0)
  end

  it "should match the person_balance and the balance adjustment credit amount after the transaction has taken place" do
    expect(@balance_after_adjustment).to eq(@balance_adjustment.credit_amount)
  end

  it "should have the same balance before any action and after a transaction/rollback" do
    expect(@initial_balance).to eq(@balance_after_rollback)
  end

  it "is a USD currency if the person is from the US" do
    expect(@balance_adjustment.currency).to eq("USD")
  end
end

describe BalanceAdjustment, "Testing debit scenarios" do
  before(:each) do
    @admin = TCFactory.build_person(:force_is_administrator => true)
    @person = TCFactory.build_person(:email=>"balance_adjustment_test@tunecore.com")
    @initial_balance = @person.person_balance.reload.balance
    @balance_adjustment_first = TCFactory.create(:balance_adjustment, :person=>@person, :posted_by => @admin,:posted_by_name=>@admin.name, :credit_amount=>250.0, :debit_amount=>0.00)
    @balance_before_debit = @person.person_balance.reload.balance
    @balance_adjustment = TCFactory.create(:balance_adjustment, :person=>@person, :posted_by => @admin,:posted_by_name=>@admin.name, :credit_amount=>0.00, :debit_amount=>100.00)
    @balance_after_debit = @person.person_balance.reload.balance
    @balance_adjustment_rollback = TCFactory.create(:balance_adjustment, :person=>@person, :posted_by => @admin,:posted_by_name=>@admin.name, :rollback=>1, :related_id=>@balance_adjustment.id)
    @balance_after_rollback = @person.person_balance.reload.balance
  end

  it "should have the original credit amount == the rollback debit amount" do
    expect(PersonTransaction.where(target_id: @balance_adjustment.id, target_type: "BalanceAdjustment").first.credit).to eq(@balance_adjustment_rollback.debit_amount)
  end

  it "should have the original debit amount == the rollback credit amount" do
    expect(PersonTransaction.where(target_id: @balance_adjustment.id, target_type: "BalanceAdjustment").first.debit).to eq(@balance_adjustment_rollback.credit_amount)
  end

  it "should populate the transaction type with 'BalanceAdjustment'" do
    expect(PersonTransaction.where(target_id: @balance_adjustment.id, target_type: "BalanceAdjustment").length).to eq(1)
  end

  it "should populate the transaction type with 'BalanceAdjustment'" do
    expect(PersonTransaction.where(target_id: @balance_adjustment_rollback.id, target_type: "BalanceAdjustment").length).to eq(1)
  end

  it "should have the person_balance = 0 before any transactions" do
    expect(@initial_balance).to eq(0)
  end

  it "should match the person_balance with the intial balance - debit adjustment" do
    expect(@balance_after_debit).to eq(@balance_before_debit - @balance_adjustment.debit_amount)
  end

  it "should have the same balance before any action and after a transaction/rollback" do
    expect(@balance_before_debit).to eq(@balance_after_rollback)
  end

  it "should not overdraft the account" do
    allow_any_instance_of(CurrencyHelper).to receive(:balance_to_currency).and_return(1)
    @balance_adjustment_first = TCFactory.create(:balance_adjustment, :person=>@person, :posted_by => @admin,:posted_by_name=>@admin.name, :credit_amount=>250.0, :debit_amount=>0.00)
    @balance_adjustment = TCFactory.build(:balance_adjustment, :person=>@person, :posted_by => @admin,:posted_by_name=>@admin.name, :credit_amount=>0.00, :debit_amount=>300.00)
    @balance_adjustment.save
    expect(@balance_adjustment.errors.messages[:debit_amount].size).to eq 1
  end

  it "is a USD currency if the person is from the US" do
    expect(@balance_adjustment.currency).to eq("USD")
  end

  context "Negative balance scenarios" do
    before(:each) do
      @person.person_balance.update_attribute(:balance, -20.00)
      @initial_balance = @person.person_balance.reload.balance
    end

    it "should allow a credit of an account with a negative balance" do
      TCFactory.create(:balance_adjustment, :person=>@person, :posted_by => @admin,:posted_by_name=>@admin.name, :credit_amount=>30.0, :debit_amount=>0.00)
      expect(@person.person_balance.reload.balance).to eq(10.0)
    end

    it "should NOT allow a debit of an account with a negative balance" do
      debit = TCFactory.build(:balance_adjustment, :person=>@person, :posted_by => @admin,:posted_by_name=>@admin.name, :credit_amount=>0.0, :debit_amount=>20.00)
      debit.save
      expect(debit.errors.messages[:base].size).to eq 1
      expect(@person.person_balance.reload.balance).to eq(-20.0)
    end

    it "should NOT allow rollback which creates a negative balance" do
      allow_any_instance_of(CurrencyHelper).to receive(:balance_to_currency).and_return(1)
      credit = TCFactory.create(:balance_adjustment, :person=>@person, :posted_by => @admin,:posted_by_name=>@admin.name, :credit_amount=>30.0, :debit_amount=>0.00)
      expect(@person.person_balance.reload.balance).to eq(10.0)
      rollback = TCFactory.build(:balance_adjustment, :person=>@person, :posted_by => @admin,:posted_by_name=>@admin.name, :rollback=>1, :related_id=>credit.id)
      rollback.save
      expect(rollback.errors.messages[:debit_amount].size).to eq 1
      expect(@person.person_balance.reload.balance).to eq(10.0)
    end
  end

  context "Canadian customer scenarios" do
    before(:each) do
      @admin = TCFactory.build_person(:force_is_administrator => true)
      @person = TCFactory.build_person(:email=>"ca_test@tunecore.com", :country => "CA", :postal_code=>'E4Y2E4')
      @initial_balance = @person.person_balance.reload.balance
      @balance_adjustment_first = TCFactory.create(:balance_adjustment, :person=>@person, :posted_by => @admin,:posted_by_name=>@admin.name, :credit_amount=>250.0, :debit_amount=>0.00)
      @balance_before_debit = @person.person_balance.reload.balance
      @balance_adjustment = TCFactory.create(:balance_adjustment, :person=>@person, :posted_by => @admin,:posted_by_name=>@admin.name, :credit_amount=>0.00, :debit_amount=>100.00)
      @balance_after_debit = @person.person_balance.reload.balance
      @balance_adjustment_rollback = TCFactory.create(:balance_adjustment, :person=>@person, :posted_by => @admin,:posted_by_name=>@admin.name, :rollback=>1, :related_id=>@balance_adjustment.id)
      @balance_after_rollback = @person.person_balance.reload.balance
    end

    it "is a CAD currency" do
      expect(@balance_adjustment.currency).to eq("CAD")
    end
  end
end
