require "rails_helper"

describe Entitlement do
  before(:each) do
    @entitlement = Entitlement.new
  end

  describe "#canceled?" do
    it "should be canceled when 'canceled_at' returns nil" do
      allow(@entitlement).to receive(:canceled_at).and_return(nil)
      expect(@entitlement.canceled?).to eq(false)
    end

    it "should not be canceled when 'canceled_at' returns a date" do
      allow(@entitlement).to receive(:canceled_at).and_return(Date.today)
      expect(@entitlement.canceled?).to eq(true)
    end
  end

  describe "#active?" do
    it "should be active when 'expires_at' occurs after today" do
      allow(@entitlement).to receive(:expires_at).and_return(Date.today + 2.days)
      expect(@entitlement.active?).to eq(true)
    end

    it "should be inactive when 'expires_at' before after today" do
      allow(@entitlement).to receive(:expires_at).and_return(Date.today - 2.days)
      expect(@entitlement.active?).to eq(false)
    end
  end

  describe "#person_has_active_access_rights?" do
    it "should return true when a person has active access rights" do
      allow(Entitlement).to receive_message_chain(:select, :joins, :where, :group)
        .and_return([Entitlement.new])
      expect(Entitlement.person_has_active_access_rights?(Person.new, "login", "index")).to eq(true)
    end

    it "should return false when a person's rights are expired" do
      allow(Entitlement).to receive_message_chain(:select, :joins, :where, :group)
        .and_return([])
      expect(Entitlement.person_has_active_access_rights?(Person.new, "login", "index")).to eq(false)
    end
  end

  describe "#history" do
    it "should be able to get a historical record of entitlements" do
      expect(@entitlement.history).to eq([])
    end
  end

  describe "#starts_at" do
    it "should grab starts_at out of the piggybacking attributes, but empty if not piggybacked" do
      expect(@entitlement.starts_at).to eq(nil)
    end

    it "should grab canceled_at out of the piggybacking attributes, but empty if not piggybacked" do
      expect(@entitlement.starts_at).to eq(nil)
    end

    it "should grab expires_at out of the piggybacking attributes, but empty if not piggybacked" do
      expect(@entitlement.starts_at).to eq(nil)
    end
  end
end
