require 'rails_helper'

describe DowngradeCategoryReason do
  let(:downgrade_category) { create(:downgrade_category) }
  let(:downgrade_category_reason) { create(:downgrade_category_reason, downgrade_category: downgrade_category) }

  describe "#downgrade_category_reasons" do
    it "should return the category of the given downgrade reason" do
      expect(downgrade_category_reason.downgrade_category_id).to eq (downgrade_category.id)
    end
  end
end
