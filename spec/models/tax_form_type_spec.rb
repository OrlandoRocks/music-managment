require "rails_helper"

RSpec.describe TaxFormType, type: :model do
  describe "Scopes" do
    describe "w9_types" do
      it "includes all form types of W9 type" do
        expect(TaxFormType.w9_types.pluck(:kind)).to contain_exactly("W9 - Individual", "W9 - Entity")
      end
    end
  end
end
