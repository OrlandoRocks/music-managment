require "rails_helper"

describe PreorderPurchase do
  describe "#update_enabled_states" do
    let(:album) { FactoryBot.create(:album) }
    subject { FactoryBot.create(:preorder_purchase, album: album) }

    shared_examples "update enabled" do |property, google, itunes|
      it "should be enabled" do
        stores.each do |s|
          FactoryBot.create(:salepointable_store, store: s)
          FactoryBot.create(:salepoint, store: s, salepointable_type: album.class.name, salepointable_id: album.id, variable_price_id: s.short_name == "iTunesWW" ? s.variable_prices.first.id : nil)
        end
        subject.update_enabled_states(property)
        expect(subject.google_enabled).to eq(google)
        expect(subject.itunes_enabled).to eq(itunes)
      end
    end

    context "update to all enabled" do
      let(:stores) { [Store.find_by(short_name: "iTunesWW"), Store.find_by(short_name: "Google")] }
      it_behaves_like "update enabled", "all", true, true
    end

    context "update to google enabled" do
      let(:stores) { [Store.find_by(short_name: "Google")] }
      it_behaves_like "update enabled", "google", true, false
    end

    context "update to itunes enabled" do
      let(:stores) { [Store.find_by(short_name: "iTunesWW")] }
      it_behaves_like "update enabled", "itunes", false, true
    end

    context "update to none enabled from all enabled" do
      let(:stores) { [Store.find_by(short_name: "iTunesWW"), Store.find_by(short_name: "Google")] }
      it_behaves_like "update enabled", "all", true, true
      it_behaves_like "update enabled", "none", false, false
    end

    it "when updating to empty or unknown string" do
      expect { subject.update_enabled_states("") }.to raise_error(RuntimeError, "Invalid store")
    end
  end

  describe "#enabled_preorder_data_count" do
    let(:album) { FactoryBot.create(:album) }
    subject { FactoryBot.create(:preorder_purchase, album: album) }

    it "should show a count of 2 when all enabled" do
      stores = [Store.find_by(short_name: "iTunesWW"), Store.find_by(short_name: "Google")]
      stores.each do |s|
        FactoryBot.create(:salepointable_store, store: s)
        FactoryBot.create(:salepoint, store: s, salepointable: album, variable_price_id: s.short_name == "iTunesWW" ? s.variable_prices.first.id : nil)
      end
      subject.update_enabled_states("all")
      expect(subject.enabled_preorder_data_count).to eq(2)
    end

    it "should show a count of 1 when iTunes is enabled" do
      store = Store.find_by(short_name: "iTunesWW")
      FactoryBot.create(:salepointable_store, store: store)
      FactoryBot.create(:salepoint, store: store, salepointable: album, variable_price_id: store.variable_prices.first.id)
      subject.update_enabled_states("itunes")
      expect(subject.enabled_preorder_data_count).to eq(1)
    end

    it "should show a count of 1 when Google is enabled" do
      store = Store.find_by(short_name: "Google")
      FactoryBot.create(:salepointable_store, store: store)
      FactoryBot.create(:salepoint, store: store, salepointable: album)
      subject.update_enabled_states("google")
      expect(subject.enabled_preorder_data_count).to eq(1)
    end

    it "should update to none enabled from all enabled" do
      stores = [Store.find_by(short_name: "iTunesWW"), Store.find_by(short_name: "Google")]
      stores.each do |s|
        FactoryBot.create(:salepointable_store, store: s)
        FactoryBot.create(:salepoint, store: s, salepointable: album, variable_price_id: s.short_name == "iTunesWW" ? s.variable_prices.first.id : nil)
      end
      subject.update(itunes_enabled: true, google_enabled: true)
      subject.update_enabled_states("none")
      expect(subject.enabled_preorder_data_count).to eq(0)
    end
  end

  describe "#enabled?" do
    let(:album) { FactoryBot.create(:album) }
    subject { FactoryBot.create(:preorder_purchase, album: album) }

    it "should be disabled" do
      expect(subject.enabled?).to eq(false)
    end

    it "should be enabled when iTunes is enabled" do
      store = Store.find_by(short_name: "iTunesWW")
      FactoryBot.create(:salepointable_store, store: store)
      FactoryBot.create(:salepoint, store: store, salepointable: album, variable_price_id: store.variable_prices.first.id)
      subject.update_enabled_states("itunes")
      expect(subject.enabled?).to eq(true)
    end

    it "should be enabled when Google is enabled" do
      store = Store.find_by(short_name: "Google")
      FactoryBot.create(:salepointable_store, store: store)
      FactoryBot.create(:salepoint, store: store, salepointable: album)
      subject.update_enabled_states("google")
      expect(subject.enabled?).to eq(true)
    end
  end

  describe "#valid_preorder_data?" do
    it "should call the validate_preorder_data? method on salepoint preorder data" do
      album = FactoryBot.create(:album)
      preorder_purchase = FactoryBot.create(:preorder_purchase, album: album)
      stores = [Store.find_by(short_name: "iTunesWW"), Store.find_by(short_name: "Google")]
      spd = ""
      stores.each do |s|
        FactoryBot.create(:salepointable_store, store: s)
        salepoint = FactoryBot.create(:salepoint, store: s, salepointable: album, variable_price_id: s.short_name == "iTunesWW" ? s.variable_prices.first.id : nil)
        spd = FactoryBot.create(:salepoint_preorder_data, salepoint: salepoint, preorder_purchase: preorder_purchase)
      end
      allow(preorder_purchase).to receive(:salepoint_preorder_data).and_return([spd])

      expect(spd).to receive(:valid_preorder_data?)
      preorder_purchase.valid_preorder_data?
    end
  end

  describe "#preorder_data_errors" do
    it "should call the preorder_data_errors method on salepoint preorder data" do
      album = FactoryBot.create(:album)
      preorder_purchase = FactoryBot.create(:preorder_purchase, album: album)
      store = Store.find_by(short_name: "iTunesWW")
      FactoryBot.create(:salepointable_store, store: store)
      salepoint = FactoryBot.create(:salepoint, store: store, salepointable: album, variable_price_id: store.variable_prices.first.id)
      spd = FactoryBot.create(:salepoint_preorder_data, salepoint: salepoint, preorder_purchase: preorder_purchase)
      preorder_purchase.update_enabled_states("itunes")
      allow(preorder_purchase).to receive(:salepoint_preorder_data).and_return([spd])
      expect(spd).to receive(:preorder_data_errors).and_return([])
      preorder_purchase.preorder_data_errors
    end
  end

  describe "#name" do
    let(:album) { FactoryBot.create(:album) }
    subject { FactoryBot.create(:preorder_purchase, album: album) }

    it "should show iTunes and Google when both iTunes and Google are enabled" do
      stores = [Store.find_by(short_name: "iTunesWW"), Store.find_by(short_name: "Google")]
      stores.each do |s|
        FactoryBot.create(:salepointable_store, store: s)
        FactoryBot.create(:salepoint, store: s, salepointable: album, variable_price_id: s.short_name == "iTunesWW" ? s.variable_prices.first.id : nil)
      end
      subject.update_enabled_states("all")
      expect(subject.name).to eq("iTunes and Google Preorder")
    end

    it "should show iTunes when iTunes is enabled" do
      store = Store.find_by(short_name: "iTunesWW")
      FactoryBot.create(:salepointable_store, store: store)
      FactoryBot.create(:salepoint, store: store, salepointable: album, variable_price_id: store.variable_prices.first.id)
      subject.update_enabled_states("itunes")
      expect(subject.name).to eq("iTunes Preorder")
    end

    it "should show Google when Google is enabled" do
      store = Store.find_by(short_name: "Google")
      FactoryBot.create(:salepointable_store, store: store)
      FactoryBot.create(:salepoint, store: store, salepointable: album)
      subject.update_enabled_states("google")
      expect(subject.name).to eq("Google Preorder")
    end
  end

  describe "#update_preorder_data" do
    let(:album) { FactoryBot.create(:album, sale_date: Date.today + 10.days) }
    subject { FactoryBot.create(:preorder_purchase, album: album) }

    context "salepoint_preorder_data for itunes only" do
      before(:each) do
        itunes_store = Store.find_by(short_name: "iTunesWW")
        FactoryBot.create(:salepointable_store, store: itunes_store)
        @itunes_salepoint = FactoryBot.create(:salepoint, salepointable: album, store: itunes_store, variable_price: itunes_store.variable_prices.first)
        @itunes_spd = FactoryBot.create(:salepoint_preorder_data, salepoint: @itunes_salepoint, preorder_purchase: subject)
      end

      it "should call #update_attributes, #update_grat_tracks, #set_preorder_price_tier, set_salepoint_album_tier and #set_track_price" do
        allow(subject).to receive_message_chain(:album, :itunes_ww_salepoint).and_return(@itunes_salepoint)
        allow(@itunes_salepoint).to receive(:salepoint_preorder_data).and_return(@itunes_spd)
        allow(subject).to receive(:salepoint_preorder_data).and_return([@itunes_spd])

        expect(@itunes_spd).to receive(:update_grat_tracks).and_return(true)
        expect(@itunes_spd).to receive(:set_track_price).and_return(true)
        expect(@itunes_spd).to receive(:set_preorder_price_tier).and_return(true)
        expect(@itunes_spd).to receive(:set_salepoint_album_tier).and_return(true)
        expect(@itunes_spd).to receive(:update).with(start_date: nil, preview_songs: nil).and_return(true)

        subject.update_preorder_data({track_price: true, album_price: true}, {})
      end
    end

    context "salepoint_preorder_data for google" do
      before(:each) do
        google_store = Store.find_by(short_name: "Google")
        FactoryBot.create(:salepointable_store, store: google_store)
        @google_salepoint = FactoryBot.create(:salepoint, salepointable: album, store: google_store)
        @google_spd = FactoryBot.create(:salepoint_preorder_data, salepoint: @google_salepoint, preorder_purchase: subject)
      end

      it "should only call #validate_start_date" do
        allow(subject).to receive(:salepoint_preorder_data).and_return([@google_spd])
        expect(@google_spd).not_to receive(:update_grat_tracks)
        expect(@google_spd).not_to receive(:set_track_price)
        expect(@google_spd).not_to receive(:set_preorder_price_tier)
        expect(@google_spd).not_to receive(:set_salepoint_album_tier)
        expect(@google_spd).to receive(:update).with(start_date: nil, preview_songs: nil).and_return(true)

        subject.update_preorder_data({}, {})
      end
    end

    context "date trasnlations for foreign countries" do
      before(:each) do
        itunes_store = Store.find_by(short_name: "iTunesWW")
        FactoryBot.create(:salepointable_store, store: itunes_store)
        @itunes_salepoint = FactoryBot.create(:salepoint, salepointable: album, store: itunes_store, variable_price: itunes_store.variable_prices.first)
        @itunes_spd = FactoryBot.create(:salepoint_preorder_data, salepoint: @itunes_salepoint, preorder_purchase: subject)
        @expected_start_date = DateTime.parse("2016-10-25")
        allow(subject).to receive_message_chain(:album, :itunes_ww_salepoint).and_return(@itunes_salepoint)
        allow(@itunes_salepoint).to receive(:salepoint_preorder_data).and_return(@itunes_spd)
        allow(subject).to receive(:salepoint_preorder_data).and_return([@itunes_spd])

        expect(@itunes_spd).to receive(:update_grat_tracks).and_return(true)
        expect(@itunes_spd).to receive(:set_track_price).and_return(true)
        expect(@itunes_spd).to receive(:set_preorder_price_tier).and_return(true)
        expect(@itunes_spd).to receive(:set_salepoint_album_tier).and_return(true)
      end

      it "parses the 'en' country date format successfully" do
        I18n.locale = "en"
        expect(@itunes_spd).to receive(:update).with(start_date: @expected_start_date, preview_songs: nil).and_return(true)

        subject.update_preorder_data({track_price: true, album_price: true}, { start_date: "10/25/2016" })
      end

      it "parses the 'fr' country date format successfully" do
        I18n.locale = "fr"
        expect(@itunes_spd).to receive(:update).with(start_date: @expected_start_date, preview_songs: nil).and_return(true)

        subject.update_preorder_data({track_price: true, album_price: true}, { start_date: "25.10.2016" })
      end

      it "parses the 'de' country date format successfully" do
        I18n.locale = "de"
        expect(@itunes_spd).to receive(:update).with(start_date: @expected_start_date, preview_songs: nil).and_return(true)

        subject.update_preorder_data({track_price: true, album_price: true}, { start_date: "25.10.2016" })
      end

      it "parses the 'it' country date format successfully" do
        I18n.locale = "it"
        expect(@itunes_spd).to receive(:update).with(start_date: @expected_start_date, preview_songs: nil).and_return(true)

        subject.update_preorder_data({track_price: true, album_price: true}, { start_date: "25/10/2016" })
      end
    end
  end

  describe "#paid_post_proc" do
    it "should update the paid_at" do
      preorder_purchase = FactoryBot.create(:preorder_purchase)

      expect(preorder_purchase.paid_at).to eq(nil)
      preorder_purchase.paid_post_proc

      expect(preorder_purchase.paid_at.present?).to eq(true)
    end
  end

  describe "#type_of_album" do
    context "album_type is Album" do
      it "should be valid" do
        album = FactoryBot.create(:album)
        preorder_purchase = FactoryBot.build(:preorder_purchase, album: album)
        expect(preorder_purchase.valid?).to eq(true)
      end
    end

    context "album_type is Single" do
      it "should be valid" do
        single = FactoryBot.create(:single)
        preorder_purchase = FactoryBot.build(:preorder_purchase, album: single)
        expect(preorder_purchase.valid?).to eq(true)
      end
    end

    context "album_type is Ringtone" do
      it "should not be valid" do
        ringtone = FactoryBot.create(:ringtone)
        preorder_purchase = FactoryBot.build(:preorder_purchase, album: ringtone)
        expect(preorder_purchase.valid?).to eq(false)
      end
    end
  end

  describe "#validate_not_paid_for" do
    before(:each) do
      @album = FactoryBot.create(:album)
      store = Store.find_by(short_name: "iTunesWW")
      FactoryBot.create(:salepointable_store, store: store)
      FactoryBot.create(:salepoint, store: store, salepointable: @album, variable_price: store.variable_prices.first)
      @preorder_purchase = FactoryBot.create(:preorder_purchase, album: @album)
    end

    context "when the paid_at is set" do
      it "should be a valid update" do
        expect(@preorder_purchase.update(paid_at: Time.now, itunes_enabled: true)).to eq(true)
      end
    end

    context "when the paid_at has already been set" do
      it "should not be a valid update" do
        @preorder_purchase.update(paid_at: Time.now)
        expect(@preorder_purchase.update(itunes_enabled: true)).to eq(false)
      end
    end
  end

  describe "#create_enable_salepoint_preorder_data" do
    before(:each) do
      @album = FactoryBot.create(:album)
      @preorder_purchase = FactoryBot.create(:preorder_purchase, album: @album)
    end

    context "album has both itunes and google salepoints" do
      before(:each) do
        store = Store.find_by(short_name: "iTunesWW")
        FactoryBot.create(:salepointable_store, store: store)
        FactoryBot.create(:salepoint, store: store, salepointable: @album, variable_price: store.variable_prices.first)
        store = Store.find_by(short_name: "Google")
        FactoryBot.create(:salepointable_store, store: store)
        FactoryBot.create(:salepoint, store: store, salepointable: @album)
      end

      it "should create the google and itunes salepoint_preorder_data" do
        expect {
          @preorder_purchase.update(google_enabled: true, itunes_enabled: true)
        }.to change(SalepointPreorderData, :count).by(2)
      end
    end

    context "album does not have itunes salepoint" do
      it "should raise an error when trying to add the itunes salpoint" do
        expect{@preorder_purchase.update(itunes_enabled: true)}.to raise_error(ActiveRecord::RecordInvalid)
      end
    end
  end

  describe "#recalculate_purchase_price" do
    context "when the preorder_purchase has an associated unpaid pruchase" do
      before(:each) do
        @album = FactoryBot.create(:album)
        @preorder_purchase = FactoryBot.create(:preorder_purchase, album: @album)
        product = TCFactory.build_ad_hoc_preorder_album_product(50.00)
        @purchase = FactoryBot.create(:purchase, related: @preorder_purchase, product: product, person: @album.person, paid_at: nil)
        store = Store.find_by(short_name: "iTunesWW")
        FactoryBot.create(:salepointable_store, store: store)
        FactoryBot.create(:salepoint, store: store, salepointable: @album, variable_price: store.variable_prices.first)
        store = Store.find_by(short_name: "Google")
        FactoryBot.create(:salepointable_store, store: store)
        FactoryBot.create(:salepoint, store: store, salepointable: @album)
        @preorder_purchase.reload
      end

      context "when google_enabled and itunes_enabled are both false" do
        it "should destroy the purchase" do
          expect(@preorder_purchase.purchase).to receive(:destroy)
          @preorder_purchase.send(:recalculate_purchase_price)
        end
      end

      context "when google_enabled or itunes_enabled is changed" do
        it "should recalculate the purchase" do
          @preorder_purchase.update(itunes_enabled: true, google_enabled: true)
          allow(@preorder_purchase).to receive(:google_enabled_changed?).and_return(true)
          expect(@preorder_purchase.purchase).to receive(:recalculate)
          @preorder_purchase.send(:recalculate_purchase_price)
        end
      end
    end
  end
end
