require "rails_helper"

describe PaymentBatch do

  context "when creating transactions for invoices" do

    before(:each) do
      @invoices      = []
      @payment_batch = TCFactory.create(:payment_batch)
      @invoices     << TCFactory.create(:invoice, settled_at: nil)
      @invoices     << TCFactory.create(:invoice, settled_at: nil)
      product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
      album   = TCFactory.create(:album, :person=>@invoices.first.person)
      @purchase      = TCFactory.create(:purchase, :invoice_id=>@invoices.first.id, :person_id=>@invoices.first.person.id, :related=>album, :product=>product)
    end

    it "should update invoice status to 'processing in batch'" do
      @transactions = @payment_batch.create_transactions_for_invoices(@invoices)

      @invoices.each do |invoice|
        expect(invoice.reload.batch_status).to eq("processing_in_batch")
      end

    end

    it "should create transactions successfully" do
      @transactions = @payment_batch.create_transactions_for_invoices(@invoices)

      expect(@transactions.size).to eq(2)
      @transactions.each do |transaction|
        expect(transaction).to be_valid
      end

    end

    it "should only create transactions for unsettled invoices" do
      settled_invoice = TCFactory.create(:invoice)
      invoices = @invoices
      invoices << settled_invoice
      transactions = @payment_batch.create_transactions_for_invoices(invoices)

      expect(transactions.size).to eq(2)
      transactions.each do |transaction|
        expect(transaction).to be_valid
      end
    end

    it "should update the batch total" do
      @transactions = @payment_batch.create_transactions_for_invoices(@invoices)

      expect(@payment_batch.reload.total_amount).to eq(@invoices.sum{|invoice| invoice.amount})
      expect(@payment_batch.filter_sum("Album", "pending")).to eq(@purchase.cost_cents)
    end

    it "should create an invoice_log for each invoice" do
      # only testing that a log is created here as they are less valuable when successful
      initial_invoice_log_count = InvoiceLog.count
      @transactions = @payment_batch.create_transactions_for_invoices(@invoices)
      expect(initial_invoice_log_count + @invoices.length).to eq(InvoiceLog.count)
    end

    context "when batch country_website conflicts with invoice country_website" do
      it "should create an invoice_log" do
        initial_invoice_log_count = InvoiceLog.count
        invoice = @invoices.first
        purchase = invoice.purchases.first
        allow_any_instance_of(Invoice).to receive(:country_website_id).and_return(8)


        @transactions = @payment_batch.create_transactions_for_invoices([invoice])

        log = InvoiceLog.last
        expected_log = {
            truncated_message: "comment: Error - Invoice being processed in wrong country payment_batch",
            person_id: invoice.person.id,
            invoice_id: invoice.id,
            purchase_id: purchase.id,
            payment_batch_id: @payment_batch.id
        }
        actual_log = {
            truncated_message: log.message[0,71],
            person_id: log.person_id,
            invoice_id: log.invoice_id,
            purchase_id: log.purchase_id,
            payment_batch_id: log.payment_batch_id
        }

        expect(expected_log).to eq(actual_log)
        expect(initial_invoice_log_count + 1).to eq(InvoiceLog.count)
      end

      it "should notify airbrake" do
        expect(Airbrake).to receive(:notify)

        initial_invoice_log_count = InvoiceLog.count
        invoice = @invoices.first
        purchase = invoice.purchases.first
        allow_any_instance_of(Invoice).to receive(:country_website_id).and_return(8)
        @transactions = @payment_batch.create_transactions_for_invoices([invoice])
      end

    end

    context "when an invoice has been processed too much" do

      before(:each) do
        allow(@invoices.first).to receive(:batch_attempts).and_return(BRAINTREE_BATCH[:run_limit])
        allow(BatchNotifier).to receive_message_chain(:batch_failure_notice, :deliver).and_return(true)
      end


      it "should make it visible to customer" do
        @transactions = @payment_batch.create_transactions_for_invoices(@invoices)
        expect(@invoices.first.batch_status).to eq("visible_to_customer")
      end

      it "should create an invoice log" do
        initial_invoice_log_count = InvoiceLog.count
        invoice = @invoices.first
        @transactions = @payment_batch.create_transactions_for_invoices([invoice])

        log = InvoiceLog.last
        expected_log = {
            person_id: invoice.person.id,
            invoice_id: invoice.id,
            payment_batch_id: @payment_batch.id,
            purchase_id: @purchase.id
        }
        actual_log = {
            person_id: log.person_id,
            invoice_id: log.invoice_id,
            payment_batch_id: log.payment_batch_id,
            purchase_id: log.purchase_id
        }

        expect(expected_log).to eq(actual_log)
        expect(initial_invoice_log_count + 1).to eq(InvoiceLog.count)
      end

    end

  end

end

describe PaymentBatch do
  context "aggregates" do
    before(:each) do
      @payment_batch = TCFactory.create(:payment_batch)
      @invoices = (1..10).map { TCFactory.create(:invoice, settled_at: nil) }
      @transactions = @payment_batch.create_transactions_for_invoices(@invoices)
    end

    it "should have initial counts" do
      expect(@payment_batch.batch_transactions.count).to eq(10)
      expect(@payment_batch.pending_batch_transactions.count).to eq(10)
      expect(@payment_batch.unprocessed_batch_transactions.count).to eq(10)

      expect(@payment_batch.balance_amount_successful).to eq(0)
      expect(@payment_batch.payments_os_count_total).to eq(0)
      expect(@payment_batch.paypal_count_total).to eq(0)
      expect(@payment_batch.braintree_count_successful).to eq(0)
    end

    it "should count specific payment types" do
      @transactions[0].update(processed: "processed", matching_type: PersonTransaction.to_s)
      expect(@payment_batch.pending_batch_transactions.count).to eq(9)
      expect(@payment_batch.balance_count_total).to eq(1)
      expect(@payment_batch.balance_amount_successful).to eq(@transactions[0].amount)

      @transactions[1].update(processed: "processed", matching_type: PaypalTransaction.to_s)
      @transactions[2].update(processed: "processed", matching_type: PaypalTransaction.to_s)
      expect(@payment_batch.sent_back_count).to eq(7)
      expect(@payment_batch.paypal_count_total).to eq(2)
      expect(@payment_batch.paypal_amount_total).to eq(@transactions[1].amount + @transactions[2].amount)

      @transactions[3].update(processed: "processed", matching_type: BraintreeTransaction.to_s)
      @transactions[4].update(processed: "processed", matching_type: BraintreeTransaction.to_s)
      @transactions[5].update(processed: "processed", matching_type: PaymentsOSTransaction.to_s)
      expect(@payment_batch.sent_back_count).to eq(4)
      expect(@payment_batch.credit_card_batch_transactions.count).to eq(3)
      expect(@payment_batch.braintree_count).to eq(2)
      expect(@payment_batch.payments_os_count).to eq(1)
      expect(@payment_batch.braintree_amount_total).to eq(@transactions[3].amount + @transactions[4].amount)
    end
  end
end

describe PaymentBatch, "Batch Processing" do

  before(:each) do
    @invoices      = []
    @payment_batch = TCFactory.create(:payment_batch,
      :total_amount                => 0,
      :balance_amount              => 0,
      :cannot_process_amount       => 0,
      :braintree_amount_total      => 0,
      :braintree_amount_successful => 0,
      :adyen_amount_total          => 0,
      :adyen_amount_successful     => 0,
      :payments_os_amount_total    => 0,
      :payments_os_amount_successful => 0
    )
  end

  context "when the batch is empty" do

    it "should return false" do
      id = @payment_batch.id
      expect(@payment_batch).to receive(:batch_transactions).and_return([])
      expect(@payment_batch.process_transactions).to eq(false)
    end

  end

  context "when the batch is not empty" do

    before(:each) do
      #Create a person and an ad hoc product to buy
      @person           = TCFactory.create(:person)
      product           = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)

      renewal_product   = Product.find(Product::US_TWO_YEAR_RENEWAL_ID)

      #Create and buy an album
      album    = TCFactory.create(:album, :person=>@person)
      purchase = Product.add_to_cart(@person, album)
      Invoice.factory(@person, [purchase])
      @person.invoices.first.settlement_received(purchase,purchase.cost_cents)
      @person.invoices.first.settled!

      @payment_batch.initialize_batch_counts

      #Add a new invoice to the batch
      purchase = Product.add_to_cart(@person, @person.renewals.first, renewal_product)
      @invoices << Invoice.factory(@person,[purchase])
      @invoices.first.update_attributes(:batch_status=>"waiting_for_batch")

      @transactions  = @payment_batch.create_transactions_for_invoices(@invoices)

    end

    it "should call process on the transaction and return true" do
      @transactions.each do |transaction|
        expect(transaction).to receive(:process)
      end

      expect(@payment_batch.process_transactions(@transactions)).to eq(true)
    end

    it "should create the batch summary correctly" do
      expect(@payment_batch.filter_sum('Album', 'pending')).to eq(@invoices.first.purchases.sum(:cost_cents))
      expect(@payment_batch.filter_sum('Album', 'cannot_process')).to eq(0)
      expect(@payment_batch.filter_sum('Album', nil)).to eq(@invoices.first.purchases.sum(:cost_cents))
      expect(@payment_batch.filter_sum(nil, nil)).to eq(@invoices.first.purchases.sum(:cost_cents))
      expect(@payment_batch.filter_sum('Ringtone', nil)).to eq(0)
    end

    it "should create an invoice log when exception is raised" do
      initial_invoice_log_count = InvoiceLog.count

      allow_any_instance_of(BatchTransaction).to receive(:process).and_raise(StandardError)

      @payment_batch.process_transactions(@transactions)

      log = InvoiceLog.last
      invoice = @invoices.first
      batch_transaction = @transactions.first
      expected_log = {
          person_id: invoice.person.id,
          invoice_id: invoice.id,
          batch_transaction_id: batch_transaction.id,
          payment_batch_id: @payment_batch.id
      }
      actual_log = {
          person_id: log.person_id,
          invoice_id: log.invoice_id,
          batch_transaction_id: log.batch_transaction_id,
          payment_batch_id: log.payment_batch_id
      }

      expect(expected_log).to eq(actual_log)
      expect(initial_invoice_log_count + 1).to eq(InvoiceLog.count)
    end

   it "should create an airbrake notification when exception is raised" do
     expect(Airbrake).to receive(:notify)
     allow_any_instance_of(BatchTransaction).to receive(:process).and_raise(StandardError)
     @payment_batch.process_transactions(@transactions)
     log = InvoiceLog.all[-1]
     invoice = @invoices.first
     batch_transaction = @transactions.first
   end

  end
end

describe PaymentBatch, "complete_batch" do

  before(:each) do
    @batch       = PaymentBatch.new
    expect(@batch).to receive(:update).with(:balance_amount=> @batch.balance_amount,
                                                       :paypal_amount_total=> @batch.paypal_amount_total,
                                                       :paypal_amount_successful=>@batch.paypal_amount_successful,
                                                       braintree_amount_total: @batch.braintree_amount_total,
                                                       braintree_amount_successful: @batch.braintree_amount_successful,
                                                       adyen_amount_total: @batch.adyen_amount_total,
                                                       adyen_amount_successful: @batch.adyen_amount_successful,
                                                       payments_os_amount_total: @batch.payments_os_amount_total,
                                                       payments_os_amount_successful: @batch.payments_os_amount_successful,
                                                       status: 'complete')
    mailer = double("mailer")
    allow(BatchSummaryNotifier).to receive(:cc_failure_summary).and_return(mailer)
    allow(BatchSummaryNotifier).to receive(:batch_summary).and_return(mailer)
    allow(mailer).to receive(:deliver)
  end

  it "should upload to braintree if there are transactions for braintree" do

    transaction = double("transaction")
    transactions = [transaction]

    allow(@batch).to receive(:transactions_not_processed).and_return([])
    allow(@batch).to receive(:total_amount).and_return(100)

    @batch.complete_batch
  end

  it "should complete the batch if there is nothing to wait on from braintree" do

    allow(@batch).to receive(:transactions_not_processed).and_return([])
    allow(@batch).to receive(:id).and_return(1)
    allow(@batch).to receive(:braintree_amount_total).and_return(0)
    allow(@batch).to receive(:total_amount).and_return(200)

    allow(BatchSummaryNotifier).to receive_message_chain(:batch_summary, :deliver)

    @batch.complete_batch
  end

  it "should delete an empty batch" do

    allow(@batch).to receive(:transactions_not_processed).and_return([])
    allow(@batch).to receive(:id).and_return(1)
    allow(@batch).to receive(:braintree_amount_total).and_return(0)
    allow(@batch).to receive(:total_amount).and_return(0)

    expect(@batch).to receive(:destroy)

    @batch.complete_batch

  end

  it "should process transactions not able to be processed" do

    transaction  = double("transaction")
    invoice      = double("invoice")
    allow(invoice).to receive(:person_id).and_return(1)
    allow(transaction).to receive(:invoice).and_return(invoice)
    allow(transaction).to receive(:invoice_id).and_return(1)
    allow(transaction).to receive(:process_with_balance).and_return(false)
    allow(transaction).to receive(:process_with_paypal).and_return(false)
    allow(transaction).to receive(:process_with_braintree_credit_card).and_return(false)
    allow(transaction).to receive(:amount).and_return(100)

    transactions = [transaction]

    allow(@batch).to receive(:transactions_not_processed).and_return(transactions)

    allow(BatchSummaryNotifier).to receive_message_chain(:not_processed_summary,:deliver)
    allow(BatchNotifier).to receive_message_chain(:batch_failure_notice, :deliver)

    expect(transaction.invoice).not_to receive(:update).with(:batch_status=>'visible_to_customer')
    expect(transaction).not_to receive(:update).with(:processed=>'cannot_process')

    @batch.complete_batch

  end

  it "log if there are failed balance transactions" do
    allow(@batch).to receive(:transactions_not_processed).and_return([])
    allow(@batch).to receive(:id).and_return(1)

    allow(@batch).to receive(:balance_count).and_return(1)
    allow(@batch).to receive(:balance_count_successful).and_return(2)

    @batch.complete_batch
  end

end

describe PaymentBatch, "search" do
  let!(:payment_batch1) {create(:payment_batch, batch_date: '13-01-2020', country_website_id: 1, currency: 'USD')}
  let!(:payment_batch2) {create(:payment_batch, batch_date: '10-01-2020', country_website_id: 1, currency: 'USD')}

  context "search with params" do
    it "should return results based on id search" do
      search_result = PaymentBatch.search({id: payment_batch1.id})
      expect(search_result.relation.map(&:id)).to include(payment_batch1.id)
    end

    it "should return results based on batch_date_min" do
      search_result = PaymentBatch.search({batch_date_min: '01/11/2020'})
      expect(search_result.relation.map(&:id)).to include(payment_batch1.id)
    end

    it "should return results based on batch_date_max" do
      search_result = PaymentBatch.search({batch_date_max: '01/11/2020'})
      expect(search_result.relation.map(&:id)).to include(payment_batch2.id)
    end

    it "should return results based on batch_date_max" do
      PaymentBatch.create(batch_date: '16-01-2020', country_website_id: 1, currency: 'USD')
      search_result = PaymentBatch.search({batch_date_min: '01/10/2020', batch_date_max: '01/14/2020'})
      expect(search_result.relation.map(&:id)).to include(payment_batch2.id, payment_batch1.id)
    end
  end
end
