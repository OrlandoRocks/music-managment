require "rails_helper"

describe PersonCountryMethods do
  describe "#person_country_phone_code" do
    context "when the country name is nil" do
      let(:person) { build(:person, country: nil) }
      
      it "should return nil" do
        expect(person.person_country_phone_code).to(be_nil)
      end
    end
    
    context "when the country name is blank" do
      let(:person) { build(:person, country: "") }
      
      it "should return nil" do
        expect(person.person_country_phone_code).to(be_nil)
      end
    end
    
    context "when the country name is valid" do
      context "when the country is nil" do
        let(:person) { build(:person, country: "Fake Country") }
        
        it "should return nil" do
          expect(person.person_country_phone_code).to(be_nil)
        end
      end
      
      context "when the country is not nil" do
        let(:country) { Country.find_by(name: "Canada")}
        let(:person) { build(:person, country: "Canada") }
        
        context "when the country does not have phone codes" do
          it "should return nil" do
            allow_any_instance_of(Country).to(receive(:country_phone_codes)).and_return([])
            expect(person.person_country_phone_code).to(be_nil)
          end
        end
        
        
        context "when the country has phone codes" do
          it "should return the country phone code" do
            expect(person.person_country_phone_code).to(eq("1"))
          end
        end
      end
    end
  end
end