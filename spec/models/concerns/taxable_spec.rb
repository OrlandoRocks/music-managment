require "rails_helper"
include ActiveSupport::Testing::TimeHelpers

describe Taxable do
  before do
    allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking, any_args).and_return(false)
    allow(FeatureFlipper).to receive(:show_feature?).with(:progressive_withholding_exemption, any_args).and_return(true)

    # Disable automatic creation of tax form mappings on tax_forms
    allow(TaxReports::TaxFormMappingService).to receive(:call!)
  end
  
  shared_context "valid tax form" do
    before do
      create(:tax_form, :with_revenue_streams, submitted_at: Time.current.beginning_of_year, payout_provider_config: us_payout_provider_config, person: person)
    end
  end

  shared_context "valid tax form for pub and dist revenue streams" do
    before do
      create(:tax_form, :with_revenue_streams, submitted_at: Time.current.beginning_of_year, person: person, payout_provider_config: us_payout_provider_config, revenue_stream_codes: ["publishing", "distribution"])
    end
  end

  shared_context "valid pub and dist tax forms" do
    before do
      tax_form_1 = create(:tax_form, :with_revenue_streams, submitted_at: Time.current.beginning_of_year, person: person, payout_provider_config: us_payout_provider_config, revenue_stream_codes: ["distribution"])
      tax_form_2 = create(:tax_form, :with_revenue_streams, submitted_at: Time.current.beginning_of_year, person: person, payout_provider_config: tax_prog_payout_provider_config, revenue_stream_codes: ["publishing"])
    end
  end

  before do
    travel_to(Time.current.end_of_year)

    person.tax_forms.destroy_all
    person.tax_form_revenue_streams.destroy_all
  end

  after do
    travel_back
  end

  subject(:person) { create(:person, :with_login_events, country: "US") }
  let(:us_payout_provider_config) { PayoutProviderConfig.find_by_program_id(PayoutProviderConfig::TC_US_PROGRAM_ID) }
  let(:tax_prog_payout_provider_config) { PayoutProviderConfig.find_by_program_id(PayoutProviderConfig::SECONDARY_TAX_FORM_PROGRAM_ID) }

  describe "#met_taxable_threshold?" do
    before { stub_const("TaxFormCheckService::TAXABLE_THRESHOLD", 5.0) }

    shared_examples "person who met taxable threshold" do |with_default|
      it { expect(person.met_taxable_threshold?).to be true } if with_default
      it { expect(person.met_taxable_threshold?(revenue_streams_args)).to be true }
    end

    shared_examples "person who did not meet taxable threshold" do |with_default|
      it { expect(person.met_taxable_threshold?).to be false } if with_default
      it { expect(person.met_taxable_threshold?(revenue_streams_args)).to be false }
    end

    context "with argument(s) specifying all revenue stream types" do
      let(:revenue_streams_args) { { revenue_streams: [:publishing, :distribution] } }

      context "with transactions for both revenue streams" do
        let!(:distribution_person_transaction) { create(:person_transaction_for_person_intake, person: person) }
        let!(:publishing_person_transaction) { create(:person_transaction, :taxable_balance_adjustment, person: person) }

        context "when taxable threshold is met in both revenue streams" do
          it_behaves_like "person who met taxable threshold", true
        end

        context "when taxable threshold is not met in both revenue streams" do
          before do
            publishing_person_transaction.update(credit: BigDecimal("1.0"))
          end

          it_behaves_like "person who did not meet taxable threshold", true
        end

        context "when taxable threshold is not met in any of the revenue streams" do
          before do
            publishing_person_transaction.update(credit: BigDecimal("1.0"))
            distribution_person_transaction.update(credit: BigDecimal("1.0"))
          end

          it_behaves_like "person who did not meet taxable threshold", true
        end
      end

      context "with transactions in one revenue stream and not the other" do
        let!(:publishing_person_transaction) { create(:person_transaction_for_person_intake, person: person) }

        context "when taxable threshold is met" do
          it_behaves_like "person who did not meet taxable threshold", true
        end

        context "when taxable threshold is not met" do
          before do
            publishing_person_transaction.update(credit: BigDecimal("1.0"))
          end

          it_behaves_like "person who did not meet taxable threshold", true
        end
      end

      context "with no transactions" do
        it_behaves_like "person who did not meet taxable threshold", true
      end
    end

    context "with an argument specifying one of the revenue stream types" do
      let(:revenue_streams_args) { { revenue_streams: :publishing } }
 
      context "with transactions for both revenue streams" do
        let!(:distribution_person_transaction) { create(:person_transaction_for_person_intake, person: person) }
        let!(:publishing_person_transaction) { create(:person_transaction, :taxable_balance_adjustment, person: person) }

        context "when taxable threshold is met in both revenue streams" do
          it_behaves_like "person who met taxable threshold"
        end

        context "when taxable threshold is met in only the specified revenue stream" do
          before do
            distribution_person_transaction.update(credit: BigDecimal("1.0"))
          end

          it_behaves_like "person who met taxable threshold"
        end

        context "when taxable threshold is met in only the other revenue stream(s)" do
          before do
            publishing_person_transaction.update(credit: BigDecimal("1.0"))
          end

          it_behaves_like "person who did not meet taxable threshold"
        end

        context "when taxable threshold is not met in any of the revenue streams" do
          before do
            publishing_person_transaction.update(credit: BigDecimal("1.0"))
            distribution_person_transaction.update(credit: BigDecimal("1.0"))
          end

          it_behaves_like "person who did not meet taxable threshold"
        end
      end

      context "with no transactions" do
        it_behaves_like "person who did not meet taxable threshold"
      end
    end
  end

  describe "#valid_tax_forms?" do
    context "with pub or dist tax forms" do
      include_context "valid tax form"

      it { expect(person.valid_tax_forms?).to be true }
    end

    context "without pub or dist tax forms" do
      it { expect(person.valid_tax_forms?).to be false }
    end

    context "with both publishing and distribution tax forms" do
      include_context "valid tax form for pub and dist revenue streams"

      it { expect(person.valid_tax_forms?).to be true }
    end

    context "with separate tax forms for publishing and distribution" do
      include_context "valid pub and dist tax forms"

      it { expect(person.valid_tax_forms?).to be true }
    end
  end

  describe "#last_dist_or_pub_tax_form_submitted_at" do
    context "with pub or dist tax forms" do
      include_context "valid tax form"

      it "returns the time that the last tax form was submitted" do
        person.reload
        expect(person.last_dist_or_pub_tax_form_submitted_at).to eq person.tax_forms.last.submitted_at
      end
    end

    context "without pub or dist tax forms" do
      it { expect(person.last_dist_or_pub_tax_form_submitted_at).to be_nil }
    end
  end

  describe "#withdrawals_since_last_tax_form_submitted?" do
    before do
      allow(FeatureFlipper).to receive(:show_feature?).with(:auto_approved_withdrawals, person.id).and_return(true)
      person.payout_transfers.destroy_all
    end

    context "with pub or dist tax forms" do
      include_context "valid tax form"

      context "with payout transfers" do
        context "with completed withdrawals created after the last tax form was submitted" do
          before { create(:approved_payout_transfer, person: person) }

          it { expect(person.withdrawals_since_last_tax_form_submitted?).to be true }
        end

        context "with completed withdrawals only created before the last tax form was submitted" do
          before { create(:payout_transfer, person: person, created_at: 1.year.ago) }

          it { expect(person.withdrawals_since_last_tax_form_submitted?).to be false }
        end

        context "with withdrawals that have not been completed" do
          before { create(:payout_transfer, person: person) }
        
          it { expect(person.withdrawals_since_last_tax_form_submitted?).to be false }
        end
      end

      context "with paypal transfers" do
        context "with completed withdrawals created after the last tax form was submitted" do
          before { create(:paypal_transfer, person: person, transfer_status: "completed") }

          it { expect(person.withdrawals_since_last_tax_form_submitted?).to be true }
        end

        context "with completed withdrawals only created before the last tax form was submitted" do
          before { create(:paypal_transfer, person: person, transfer_status: "completed", created_at: 1.year.ago) }

          it { expect(person.withdrawals_since_last_tax_form_submitted?).to be false }
        end

        context "with withdrawals that have not been completed" do
          before { create(:paypal_transfer, person: person, transfer_status: "pending") }

          it { expect(person.withdrawals_since_last_tax_form_submitted?).to be false }
        end
      end
    end

    context "without pub or dist tax forms" do
      before do
        create(:approved_payout_transfer, person: person)
        create(:approved_payout_transfer, person: person, created_at: 1.year.ago)
      end

      it { expect(person.withdrawals_since_last_tax_form_submitted?).to be false }
    end
  end
end
