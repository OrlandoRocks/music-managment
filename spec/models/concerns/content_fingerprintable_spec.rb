require "rails_helper"
describe ContentFingerprintable do
  describe "#create_and_store_content_fingerprint!" do
    let(:s3_asset) { create(:s3_asset) }
    let(:song) { create(:song, s3_asset: s3_asset) }
    let(:expected_fingerpint) { "d41d8cd98f00b204e9800998ecf8427e" }
    let(:s3_client) { AWS::S3::Client.new }
    let(:s3) { AWS::S3.new }
    let(:s3_bucket) { AWS::S3::Bucket.new(s3_asset.bucket) }
    let!(:s3_object) { Aws::S3::Object.new(s3_asset.bucket, s3_asset.key, client: s3_client) }
    let(:s3_objects) { { s3_asset.key => s3_object } }
    let(:buckets) {
      {
        s3_bucket.name => s3_bucket
      }
    }

    before do
      stub_const("S3_CLIENT", s3)
      allow(s3).to receive(:buckets).and_return(buckets)
      allow(s3_bucket).to receive(:objects).and_return(s3_objects)
      allow(s3_object).to receive(:etag).and_return(expected_fingerpint.to_json)
    end

    it "creates fetches ETAG from S3, saves it to the content_fingerprint column for songs" do
      expect(s3).to receive(:buckets).once
      expect(s3_bucket).to receive(:objects).once
      expect(s3_object).to receive(:etag).once

      song.create_and_store_content_fingerprint!
      expect(song.reload.content_fingerprint).to eq(expected_fingerpint)
    end

    it "creates fetches ETAG from S3 and saves it to the content_fingerprint column for spatial audio" do
      expect(s3).to receive(:buckets).once
      expect(s3_bucket).to receive(:objects).once
      expect(s3_object).to receive(:etag).once

      #create_and_store_content_fingerprint! runs on after_create hook
      spatial_audio = create(:spatial_audio, s3_asset: s3_asset)
      expect(spatial_audio.reload.content_fingerprint).to eq(expected_fingerpint)
    end

    it "notifies Airbrake if unsuccessful" do
      expect(s3).to receive(:buckets).once
      expect(s3_bucket).to receive(:objects).once
      allow(s3_bucket).to receive(:objects).and_raise(Aws::S3::Errors::ServiceError.new({},""))
      expect(Airbrake).to receive(:notify).with(
        "Content Fingerprint Generation Error for SpatialAudio", Aws::S3::Errors::ServiceError
      )
      #create_and_store_content_fingerprint! runs on after_create hook
      spatial_audio = create(:spatial_audio, s3_asset: s3_asset)
    end

    it "notifies Airbrake if unsuccessful" do
      expect(s3).to receive(:buckets).once
      expect(s3_bucket).to receive(:objects).once
      expect(s3_object).to receive(:etag).once
      allow(song).to receive(:save!).and_raise(ActiveRecord::RecordInvalid)
      expect(Airbrake).to receive(:notify).with(
        "Content Fingerprint Generation Error for Song", ActiveRecord::RecordInvalid
      )
      song.create_and_store_content_fingerprint!
    end
  end
end
