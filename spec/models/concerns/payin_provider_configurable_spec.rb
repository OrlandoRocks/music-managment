require "rails_helper"

describe PayinProviderConfigurable do
  describe "#braintree_config_by_corporate_entity" do
    it "returns config according to user's currency and corporate entity" do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
      american = create(:person)
      canadian = create(:person, :with_canada_country_website)
      tc_corporate_entity = CorporateEntity.find_by(name: CorporateEntity::TUNECORE_US_NAME)
      bi_corporate_entity = CorporateEntity.find_by(name: CorporateEntity::BI_LUXEMBOURG_NAME)
      usd_tc_config = PayinProviderConfig.find_by(name: PayinProviderConfig::BRAINTREE_NAME,
                                                  corporate_entity: tc_corporate_entity,
                                                  currency: "USD")
      cad_tc_config = PayinProviderConfig.find_by(name: PayinProviderConfig::BRAINTREE_NAME,
                                                  corporate_entity: bi_corporate_entity,
                                                  currency: "CAD")
      expect(american.braintree_config_by_corporate_entity).to eq(usd_tc_config)
      expect(canadian.braintree_config_by_corporate_entity).to eq(cad_tc_config)
    end
  end

  describe "#config_gateway_service_by_country" do
    it "returns TC config if TC country is passed" do
      person = create(:person)
      tc_corporate_entity = CorporateEntity.find_by(name: CorporateEntity::TUNECORE_US_NAME)
      tc_config = PayinProviderConfig.find_by(name: PayinProviderConfig::BRAINTREE_NAME,
                                                  corporate_entity: tc_corporate_entity,
                                                  currency: "USD")
      expect(person.config_gateway_service_by_country('US').config).to eq(tc_config)
    end

    it "returns BE config if BE country is passed" do
      person = create(:person)
      bi_corporate_entity = CorporateEntity.find_by(name: CorporateEntity::BI_LUXEMBOURG_NAME)
      be_config = PayinProviderConfig.find_by(name: PayinProviderConfig::BRAINTREE_NAME,
                                                  corporate_entity: bi_corporate_entity,
                                                  currency: "USD")
      expect(person.config_gateway_service_by_country('LU').config).to eq(be_config)
    end
  end
end
