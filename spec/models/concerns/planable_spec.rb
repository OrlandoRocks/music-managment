require "rails_helper"

describe Planable do
  let(:person) { create(:person) }

  describe "#can_do" do
    context "without passing 'include_carted_plan' param" do
      it "should allow member with plan higher than rising to add additional artists" do
        create(:person_plan, person: person, plan: Plan.find(Plan::PROFESSIONAL))
        expect(person.can_do?(:buy_additional_artists)).to eq(true)
      end

      it "should allow a member with plan higher than Emerging artist to add store automator" do
        create(:person_plan, person: person, plan: Plan.find(Plan::BREAKOUT_ARTIST))
        expect(person.can_do?(:store_automator)).to eq(true)
      end

      it "should not allow a member with a plan lower than Rising artist to add store automator" do
        create(:person_plan, person: person, plan: Plan.find(Plan::NEW_ARTIST))
        expect(person.can_do?(:store_automator)).to eq(false)
      end

      it "should not allow a member with a plan lower than breakout artist to add additional artists" do
        create(:person_plan, person: person, plan: Plan.find(Plan::RISING_ARTIST))
        expect(person.can_do?(:buy_additional_artists)).to eq(false)
      end

      it "should ignore eligible plan in cart" do
        allow_any_instance_of(Person).to receive(:cart_plan).and_return(Plan.find(Plan::BREAKOUT_ARTIST))
        create(:person_plan, person: person, plan: Plan.find(Plan::RISING_ARTIST))
        expect(person.can_do?(:buy_additional_artists)).to eq(false)
      end
    end
  end

  describe "#can_do?(:store_automator)" do
    context "when a user has a plan" do
      context "when the plan does not allow the automator" do
        it "should return false" do
          person1 = create(:person)
          plan1 = Plan.find(Plan::NEW_ARTIST)

          create(:person_plan, person: person1, plan: plan1)

          expect(person1.can_do?(:store_automator)).to be_falsey
        end
      end

      context "when the plan allows the automator" do
        it "should return true" do
          plan = Plan.find(Plan::PROFESSIONAL)

          create(:person_plan, person: person, plan: plan)

          expect(person.can_do?(:store_automator)).to be_truthy
        end
      end
    end

    context "when a user has no plan" do
      it "should return true" do
        person.plan = nil
        person.save

        expect(person.can_do?(:store_automator)).to be_truthy
      end
    end
  end

  describe "#feature_gating_can_do?" do
    context "when user does not have access to trends and sales feature" do
      let(:person) { create(:person) }

      before do
        allow(person).to receive(:can_do?).and_return(false)
      end

      context "when feature gating is not live" do
        it "returns true" do
          expect(person.feature_gating_can_do?(:premium_sales_report)).to eq(true)
        end
      end

      context "when feature gating is live" do
        before do
          allow(FeatureFlipper).to receive(:show_feature?).with(:plans_pricing, person).and_return(true)
        end

        context "when feature is not an admin accessible feature" do
          it "invokes the .can_do? method" do
            expect(person).to receive(:can_do?).with(:store_automator, { include_carted_plan: false })
            person.feature_gating_can_do?(:store_automator)
          end
        end

        context "when feature is an admin accessible feature" do
          context "when user is not an admin" do
            it "invokes .can_do? method" do
              expect(person).to receive(:can_do?).with(:trend_reports, { include_carted_plan: false })
              person.feature_gating_can_do?(:trend_reports)
            end
          end

          context "when user is taken over by an admin" do
            let(:admin) { create(:person, :admin) }

            before do
              allow(person).to receive(:takeover_admin_id).and_return(admin.id)
            end

            context "when admin does not have feature and sales reporting role" do
              it "invokes .can_do? method" do
                expect(person).to receive(:can_do?).with(:premium_sales_report, { include_carted_plan: false })
                person.feature_gating_can_do?(:premium_sales_report)
              end
            end

            context "when admin has features and sales reporting role" do
              before do
                admin.roles << Role.find_by(name: "Sales And Trends")
              end

              it "returns true and does not invoke .can_do? method" do
                [:trend_reports, :premium_sales_report].each do |feature|
                  expect(person.feature_gating_can_do?(feature)).to eq(true)
                end

                expect(person).not_to receive(:can_do?).with(anything)
              end
            end
          end
        end
      end
    end
  end

  describe "#can_use_store_expander?" do
    context "legacy user" do
      context "when pricing is NOT enabled" do
        before do
          allow(FeatureFlipper).to receive(:show_feature?).with(:plans_pricing, person).and_return false
        end

        it "returns true" do
          expect(person.can_use_store_expander?).to eq true
        end
      end

      context "when pricing is enabled" do
        before do
          allow(FeatureFlipper).to receive(:show_feature?).with(:plans_pricing, person).and_return true
        end

        it "returns false" do
          expect(person.can_use_store_expander?).to eq false
        end
      end
    end

    context "plans user" do
      context "when pricing is NOT enabled" do
        let!(:person_plan) { create(:person_plan, person: person) }

        before do
          allow(FeatureFlipper).to receive(:show_feature?).with(:plans_pricing, person).and_return false
        end

        it "returns true" do
          expect(person.can_use_store_expander?).to eq true
        end
      end

      context "when pricing is enabled" do
        let!(:person_plan) { create(:person_plan, person: person) }

        before do
          allow(FeatureFlipper).to receive(:show_feature?).with(:plans_pricing, person).and_return true
        end

        it "returns true" do
          expect(person.can_use_store_expander?).to eq true
        end
      end
    end
  end

  describe "#user_or_cart_plan?" do
    it "returns true if plan_id matches plan in cart" do
      allow_any_instance_of(Person).to receive(:cart_plan).and_return(Plan.find(Plan::NEW_ARTIST))
      expect(person.user_or_cart_plan?(Plan::NEW_ARTIST)).to be(true)
    end
    it "returns true if plan_id matches user's plan" do
      create(:person_plan, person: person, plan: Plan.find(Plan::NEW_ARTIST))
      expect(person.user_or_cart_plan?(Plan::NEW_ARTIST)).to be(true)
    end
    it "returns false if plan_id does not match plan in cart or user's plan" do
      expect(person.user_or_cart_plan?(Plan::NEW_ARTIST)).to be(false)
    end
  end

  describe "#active_splits_collaborator?" do
    it "returns true if the user has a paid and not canceled splits collaborator plan_addon" do
      create(:plan_addon, :splits_collaborator, :paid, person: person)
      expect(person.active_splits_collaborator?).to be(true)
    end
    it "returns false if the user has no splits collaborator plan_addon" do
      expect(person.active_splits_collaborator?).to be(false)
    end
    it "returns false if the user has only a canceled splits collaborator addon" do
      create(:plan_addon, :splits_collaborator, :paid, :canceled, person: person)
      expect(person.active_splits_collaborator?).to be(false)
    end
    it "returns false if the user only has an unpaid splits collaborator addon" do
      create(:plan_addon, :splits_collaborator, person: person)
      expect(person.active_splits_collaborator?).to be(false)
    end
  end

  describe "#can_accept_splits?" do
    let!(:w9) { create :tax_form, :current_active_individual_w9, person: person }

    before do
      allow(FeatureFlipper).to receive(:show_feature?).and_call_original
      allow(FeatureFlipper).to receive(:show_feature?).with(:royalty_splits, person).and_return(true)
    end
    it "returns true if user has active splits collaborator" do
      allow(person).to receive(:active_splits_collaborator?).and_return(true)
      expect(person.can_accept_splits?).to be(true)
    end
    it "returns true if the user has plan with splits_collaborator feature" do
      create(:person_plan, person: person, plan: Plan.find(Plan::RISING_ARTIST))
      expect(person.can_accept_splits?).to be(true)
    end
    it "returns false if user does not have active splits collaborator or plan with splits_collaborator feature" do
      expect(person.can_accept_splits?).to be(false)
    end

    context "when only royalty_splits_beta flag is on, with splits" do
      before do
        allow(FeatureFlipper).to receive(:show_feature?).with(:royalty_splits, person).and_return(false)
        allow(FeatureFlipper).to receive(:show_feature?).with(:royalty_splits_beta, person).and_return(true)
        create(:royalty_split_recipient, :not_owner, person: person)
      end

      it "false if user is new artist" do
        create(:person_plan, person: person, plan: Plan.find(Plan::NEW_ARTIST))
        expect(person.can_accept_splits?).to be(false)
      end

      it "true if user is new artist, and collaborator addon" do
        create(:person_plan, person: person, plan: Plan.find(Plan::NEW_ARTIST))
        create(:plan_addon, :splits_collaborator, :paid, person: person)
        expect(person.can_accept_splits?).to be(true)
      end

      it "true if user is rising artist" do
        create(:person_plan, person: person, plan: Plan.find(Plan::RISING_ARTIST))
        expect(person.can_accept_splits?).to be(true)
      end

      it "true if user is professional artist" do
        create(:person_plan, person: person, plan: Plan.find(Plan::PROFESSIONAL))
        expect(person.can_accept_splits?).to be(true)
      end
    end
  end

  describe "#can_create_splits?" do
    context "when only royalty_splits_beta flag is on" do
      before do
        allow(FeatureFlipper).to receive(:show_feature?).and_call_original
        allow(FeatureFlipper).to receive(:show_feature?).with(:royalty_splits, person).and_return(false)
        allow(FeatureFlipper).to receive(:show_feature?).with(:royalty_splits_beta, person).and_return(true)
      end

      it "true if user is a US professional, with a w9" do
        create(:person_plan, person: person, plan: Plan.find(Plan::PROFESSIONAL))
        create(:tax_form, :current_active_individual_w9, person: person)
        person.stub(:from_united_states_and_territories?) { true }
        expect(person.can_create_splits?).to be(true)
      end

      it "true if user is a US professional, without a w9" do
        create(:person_plan, person: person, plan: Plan.find(Plan::PROFESSIONAL))
        person.stub(:from_united_states_and_territories?) { true }
        expect(person.can_create_splits?).to be(false)
      end

      it "true if user is a non-US professional" do
        create(:person_plan, person: person, plan: Plan.find(Plan::PROFESSIONAL))
        person.stub(:from_united_states_and_territories?) { false }
        expect(person.can_create_splits?).to be(true)
      end

      it "false if user is rising artist" do
        create(:person_plan, person: person, plan: Plan.find(Plan::RISING_ARTIST))
        expect(person.can_create_splits?).to be(false)
      end

      it "false if user has no plan" do
        person.update(plan: nil)
        expect(person.can_create_splits?).to be(false)
      end
    end
  end

  describe "#has_unaccepted_split_invites?" do
    subject { person.has_unaccepted_split_invites? }
    let!(:split) { create(:royalty_split) }
    context "with pending splits invite" do
      before(:each) do
        create(:royalty_split_recipient, person: person, royalty_split: split)
      end
      it "should return true" do
        expect(subject).to be(true)
      end
    end
    context "with no pending splits invite" do
      it "should return false" do
        expect(subject).to be(false)
      end
    end
    context "with only accepted splits invites" do
      before(:each) do
        create(:royalty_split_recipient, :accepted, person: person, royalty_split: split)
      end
    end
  end

  describe "#add_splits_collaborator_to_cart!" do
    subject { person.add_splits_collaborator_to_cart! }

    it "finds or creates unpaid SplitsCollaborator for user" do
      subject
      expect(SplitsCollaboratorAddon.active.unpaid.exists?(person: person)).to be(true)
    end
    it "adds SplitsCollaborator to cart" do
      subject
      expect(person.purchases.unpaid.not_in_invoice.any?(&:splits_collaborator?)).to be(true)
    end
  end

  describe "#show_invited_splits_based_on_feature_flag?" do
    before do
      allow(FeatureFlipper).to receive(:show_feature?).and_call_original
    end

    context "when royalty_split feature is on" do
      before do
        allow(FeatureFlipper).to receive(:show_feature?).with(:royalty_splits, person).and_return(true)
      end

      it "should be true" do
        expect(person.show_invited_splits_based_on_feature_flag?).to be(true)
      end
    end

    context "when royalty_split_beta feature is on" do
      before do
        allow(FeatureFlipper).to receive(:show_feature?).with(:royalty_splits, person).and_return(false)
        allow(FeatureFlipper).to receive(:show_feature?).with(:royalty_splits_beta, person).and_return(true)
      end

      it "should be false for legacy users" do
        person.update!(plan: nil)
        expect(person.show_invited_splits_based_on_feature_flag?).to be(false)
      end

      it "should be false for rising artist plans" do
        person.update!(plan: Plan.find(Plan::RISING_ARTIST))
        expect(person.show_invited_splits_based_on_feature_flag?).to be(false)
      end

      it "should be true for professional plans" do
        person.update!(plan: Plan.find(Plan::PROFESSIONAL))
        expect(person.show_invited_splits_based_on_feature_flag?).to be(true)
      end

      it "should be true for legacy users with splits" do
        person.update!(plan: nil)
        recipient = create(:royalty_split_recipient, person: person)
        expect(recipient.royalty_split.owner).not_to eql(person)
        expect(person.show_invited_splits_based_on_feature_flag?).to be(true)
      end
    end
  end

  describe "#show_splits_based_on_feature_flag?" do
    before do
      allow(FeatureFlipper).to receive(:show_feature?).and_call_original
    end

    context "when royalty_split feature is on" do
      it "should be true" do
        allow(FeatureFlipper).to receive(:show_feature?).with(:royalty_splits, person).and_return(true)
        expect(person.show_splits_based_on_feature_flag?).to be(true)
      end
    end

    context "when royalty_split_beta feature is on" do
      before do
        allow(FeatureFlipper).to receive(:show_feature?).with(:royalty_splits, person).and_return(false)
        allow(FeatureFlipper).to receive(:show_feature?).with(:royalty_splits_beta, person).and_return(true)
      end

      it "should be false for legacy users" do
        person.update!(plan: nil)

        expect(person.show_splits_based_on_feature_flag?).to be(false)
      end

      it "should be false for rising artist plans" do
        person.update!(plan: Plan.find(Plan::RISING_ARTIST))

        expect(person.show_splits_based_on_feature_flag?).to be(false)
      end

      it "should be true for professional plan users outside US" do
        person.stub(:from_united_states_and_territories?) { false }
        person.update!(plan: Plan.find(Plan::PROFESSIONAL))

        expect(person.show_splits_based_on_feature_flag?).to be(true)
      end

      it "should be false for professional plan users inside US without w9" do
        person.stub(:from_united_states_and_territories?) { true }
        person.update!(plan: Plan.find(Plan::PROFESSIONAL))

        expect(person.show_splits_based_on_feature_flag?).to be(false)
      end

      it "should be true for professional plan users inside US with w9" do
        create(:tax_form, :current_active_individual_w9, person: person)
        person.update!(plan: Plan.find(Plan::PROFESSIONAL))
        person.stub(:from_united_states_and_territories?) { true }

        expect(person.show_splits_based_on_feature_flag?).to be(true)
      end

      it "should be false for legacy users with splits" do
        person.update!(plan: nil)
        recipient = create(:royalty_split_recipient, person: person)
        expect(recipient.royalty_split.owner).not_to eql(person)
        expect(person.show_splits_based_on_feature_flag?).to be(false)
      end
    end
  end
end
