require "rails_helper"

describe S3MetadataFetchable do
  describe "#fetch_asset_metadata" do
    let(:mock_resp_body) {
      {"format" => {"format_name" => "flac", "size" => "42"}}
    }
    let(:s3_asset) { FactoryBot.create(:s3_asset) }
    it "fetches metadata" do
      resp = instance_double(HTTParty::Response, body: mock_resp_body.to_json, code: 200)
      expect(HTTParty).to receive(:post).with(S3MetadataFetchable::FFPROBE_URI, body: {source: {bucket: s3_asset.bucket, key: s3_asset.key}}.to_json, headers: {"Content-Type": "application/json"}).and_return(resp)
      expect(s3_asset.fetch_asset_metadata).to eq(mock_resp_body)
    end
    it "saves metadata" do
      expect(s3_asset).to receive(:fetch_asset_metadata).and_return(mock_resp_body)
      s3_asset.set_asset_metadata!
      s3d = S3Detail.where(s3_asset_id: s3_asset.id).first
      expect(s3d.metadata_json).to eq(mock_resp_body)
      expect(s3d.file_size).to eq(42)
      expect(s3d.file_type).to eq("flac")
    end
  end
end
