require "rails_helper"

describe Linkable do
  describe ".linkable_type" do
    it "returns 'Album' if linkable is a Single" do
      single = build(:single)
      expect(single.linkable_type).to eq("Album")
    end

    it "returns 'Album' if linkable is a Ringtone" do
      ringtone = build(:ringtone)
      expect(ringtone.linkable_type).to eq("Album")
    end

    it "returns 'Album' if linkable is an Album" do
      album = build(:album)
      expect(album.linkable_type).to eq("Album")
    end

    it "returns 'Creative' if linkable is a Creative" do
      album = build(:album)
      expect(album.creatives.first.linkable_type).to eq("Creative")
    end

    it "returns 'Song' if linkable is a Creative" do
      song = build(:song)
      expect(song.linkable_type).to eq("Song")
    end
  end

  describe ".set_external_id_for" do
    it "updates the external service ID if it exists with the identifier" do
      album = create(:album)
      create(:external_service_id,
              linkable_id: album.creatives.first.id,
              linkable_type: "Creative",
              service_name: "apple",
              identifier: "12345",
              state: "did_not_match")
      album.set_external_id_for("apple", "123456")
      identifier = album.external_service_ids.first.identifier
      expect(identifier).to eq("123456")
    end

    it "creates an external service ID if it does not exist" do
      album = create(:album)
      album.set_external_id_for("apple", "123456")
      expect(album.external_service_ids.present?).to eq(true)
    end
  end

  describe ".update_or_create_external_service_id" do
    let(:album) { create(:album) }

    before do
      @external_service_id = FactoryBot.create(:external_service_id,
                                                  linkable_id: album.creatives.first.id,
                                                  linkable_type: "Creative",
                                                  service_name: "apple",
                                                  identifier: "12345",
                                                  state: "did_not_match")
    end

    context "linkable has creatives with apple external_service_ids with a state of did_not_match" do
      it "updates the identifier to the apple artist ID and the state to 'matched'" do
        album.update_or_create_external_service_id("apple", "68613517", album.creatives)
        expect(album.creatives.first.external_service_id.first.identifier).to eq("68613517")
        expect(album.creatives.first.external_service_id.first.state).to eq("matched")
      end
    end

    context "linkable does NOT have creatives with apple external_service_ids" do
      it "creates an external_service_id with the creative as the linkable." do
        @external_service_id.update(identifier: "987654321")
        album.update_or_create_external_service_id("apple", "68613517", album.creatives)
        expect(album.creatives.first.external_service_ids.count).to eq(1)
      end
    end

    context "linkable has creatives with apple external_service_ids with a state other than 'did_not_match'" do
      it "does not create or update any external_service_id" do
        album.creatives.first.external_service_ids.first.update(state: "processing")
        album.update_or_create_external_service_id("apple", "68613517", album.creatives)

        expect(album.creatives.first.external_service_ids.count).to eq(1)
        expect(album.creatives.first.external_service_ids.first.state).to eq("matched")
      end
    end
  end
end
