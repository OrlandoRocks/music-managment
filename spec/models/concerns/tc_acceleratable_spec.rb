require "rails_helper"

describe TcAcceleratable do
  let!(:person) { create(:person) }
  describe "#tc_accelerator_opted_in?" do
    subject { person.tc_accelerator_opted_in? }
    context "user has distributed a release" do
      before(:each) do
        allow(person).to receive(:has_never_distributed?).and_return(false)
      end
      context "user has opted out flag" do
        before(:each) do
          TcAcceleratorService.opt!(person, :out)
        end
        it "returns false" do
          expect(subject).to be(false)
        end
      end
      context "user does not have opted out flag" do
        it "returns true" do
          expect(subject).to be(true)
        end
      end
    end
    context "user has never distributed" do
      before(:each) do
        allow(person).to receive(:has_never_distributed?).and_return(true)
      end
      context "user has opted out flag" do
        before(:each) do
          TcAcceleratorService.opt!(person, :out)
        end
        it "returns false" do
          expect(subject).to be(false)
        end
      end
      context "user does not have opted out flag" do
        it "returns false" do
          expect(subject).to be(false)
        end
      end
    end
  end
  describe "#tc_accelerator_opted_out?" do
    subject { person.tc_accelerator_opted_out? }
    context "user has opted out flag" do
      before(:each) do
        TcAcceleratorService.opt!(person, :out)
      end
      it "returns true" do
        expect(subject).to be(true)
      end
    end
    context "user does not have opted out flag" do
      it "returns false" do
        expect(subject).to be(false)
      end
    end
  end
  describe "#tc_accelerator_do_not_notify?" do
    subject { person.tc_accelerator_do_not_notify? }
    context "user has do not notify out flag" do
      before(:each) do
        TcAcceleratorService.do_not_notify!(person, nil)
      end
      it "returns true" do
        expect(subject).to be(true)
      end
    end
    context "user does not have do not notify flag" do
      it "returns false" do
        expect(subject).to be(false)
      end
    end
  end
  
  describe "#show_accelerator_cta?" do
    subject { person.show_accelerator_cta? }
    context "with feature flag on" do
      before(:each) do
        allow(FeatureFlipper).to receive(:show_feature?).with(:tc_accelerator, person).and_return(true)
      end
      context "user has distributed" do
        before(:each) do
          allow(person).to receive(:has_never_distributed?).and_return(false)
        end
        it "returns true" do
          expect(subject).to be(true)
        end
      end
      context "user has never distributed" do
        before(:each) do
          allow(person).to receive(:has_never_distributed?).and_return(true)
        end
        it "returns false" do
          expect(subject).to be(false)
        end
      end
    end
    context "with feature flag off" do
      before(:each) do
        allow(FeatureFlipper).to receive(:show_feature?).with(:tc_accelerator, person).and_return(false)
      end
      context "user has distributed" do
        before(:each) do
          allow(person).to receive(:has_never_distributed?).and_return(false)
        end
        it "returns false" do
          expect(subject).to be(false)
        end
      end
      context "user has not distributed" do
        before(:each) do
          allow(person).to receive(:has_never_distributed?).and_return(true)
        end
        it "returns false" do
          expect(subject).to be(false)
        end
      end
    end
  end
  describe "#show_accelerator_modal?" do
    let(:invoice) {create(:invoice, person: person)}
    subject { person.show_accelerator_modal?(invoice) }

    context "with feature flag on" do
      before(:each) do
        allow(FeatureFlipper).to receive(:show_feature?).with(:tc_accelerator, person).and_return(true)
      end
      context "user has distributed" do
        before(:each) do
          allow(person).to receive(:has_never_distributed?).and_return(false)
        end
        context "user has asked not to be notified again" do
          before(:each) do
            allow(person).to receive(:tc_accelerator_do_not_notify?).and_return(true)
          end
          it "returns true" do
            expect(invoice).to_not receive(:includes_distribution_to_store?)
            expect(subject).to be(false)
          end
        end
        context "user has NOT asked not to be notified again" do
          before(:each) do
            allow(person).to receive(:tc_accelerator_do_not_notify?).and_return(false)
          end

          context "no invoice" do
            let(:invoice) { nil }
            it "raises error" do
              expect {subject}.to raise_error(NoMethodError)
            end
          end
          
          context "with invoice" do
            it "returns true" do
              expect(invoice).to receive(:includes_distribution_to_store?).and_return(true)
              expect(subject).to be(true)
            end
          end
        end
      end
      context "user has not yet distributed" do
        before(:each) do
          allow(person).to receive(:has_never_distributed?).and_return(true)
        end
        it "returns false" do
          expect(invoice).to receive(:includes_distribution_to_store?).and_return(false)
          expect(subject).to be(false)
        end
      end
    end
    context "with feature flag off" do
      before(:each) do
        allow(FeatureFlipper).to receive(:show_feature?).with(:tc_accelerator, person).and_return(false)
      end
      it "returns false" do
        expect(subject).to be(false)
      end
    end
  end
end
