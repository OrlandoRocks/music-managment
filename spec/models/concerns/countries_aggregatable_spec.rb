require "rails_helper"

RSpec.describe CountriesAggregatable do
  describe "#countries_from_non_self_sources" do
    let!(:person) { create(:person) }

    context "address_locked flag enabled" do
      context "with payoneer kyc completed reason" do
        it "returns KYC country" do
          flag_reason = FlagReason.payoneer_kyc_completed_address_lock
          person.lock_address!(reason: flag_reason)
          subject = person.countries_from_non_self_sources

          expect(subject).to eq(person.kyc_country)
        end
      end

      context "with self_identfied_addres_locked reason" do
        it "returns self identified country" do
          flag_reason = FlagReason
                        .address_lock_flag_reasons
                        .find_by(reason: FlagReason::SELF_IDENTIFIED_ADDRESS_LOCKED)
          person.lock_address!(reason: flag_reason)
          subject = person.countries_from_non_self_sources

          expect(subject).to eq(person.self_identified_country)
        end
      end
    end

    context "without address_lock flag" do
      it "includes the country from credit card in the return value" do
        create(:stored_credit_card, person: person)
        subject = person.countries_from_non_self_sources

        expect(subject).to include(*person.credit_card_countries)
      end

      it "includes country from pervious login events" do
        create(
          :login_event,
          person_id: person.id,
          ip_address: Faker::Internet.ip_v4_address,
          country: "CN"
        )
        subject = person.countries_from_non_self_sources

        expect(subject).to include(*person.logged_in_ip_countries)
      end

      it "includes country from 2FA authentication country code" do
        create(:two_factor_auth, country_code: "971", person: person)
        subject = person.countries_from_non_self_sources

        expect(subject).to include(*person.two_factor_auth_country)
      end
    end
  end
end
