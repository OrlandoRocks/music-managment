require "rails_helper"

shared_examples_for "testing blank artists" do
  it "should allow artists" do
    @product.creatives << Creative.new(:name => "Robin", :role => "primary_artist")
    expect(@product.save).to be_truthy
  end

  it "should not allow blank artists" do
    expect {@product.creatives << Creative.new(:name => "", :role => "primary_artist")}.to raise_error
  end
end

shared_examples_for "testing updating with blank artists" do
  it "should allow updating if there are artists" do
    @product.update({"label_name"=>"Test User",
     "optional_upc_number"=>"",
     "creatives"=>
      [{"name"=>"Jimmy", "role"=>"primary_artist"},
       {"name"=>"", "role"=>"featuring"}],
     "secondary_genre_id"=>"",
     "optional_isrc"=>"",
     "album_name"=>"Ringtone",
     "parental_advisory"=>"0",
     "sale_date"=>"September 10, 2010",
     "orig_release_year"=>"September 10, 2010",
     "primary_genre_id" => Genre.active.last.id.to_s,
     "song_id"=>"9"})
    @product.save
    expect(@product.errors.messages).to be_empty
    @product.reload
    expect(@product.creatives.detect {|creative| creative['role'] == 'primary_artist'}).not_to be_blank
  end

  it "should not allow updating if there are blank artists" do
    @product.update({"label_name"=>"Test User",
     "optional_upc_number"=>"",
     "creatives"=>
      [{"name"=>"", "role"=>"primary_artist"},
       {"name"=>"Bob", "role"=>"featuring"}],
     "secondary_genre_id"=>"",
     "optional_isrc"=>"",
     "album_name"=>"Ringtone",
     "parental_advisory"=>"0",
     "sale_date"=>"September 10, 2010",
     "orig_release_year"=>"September 10, 2010",
     "primary_genre_id" => Genre.active.last.id.to_s,
     "song_id"=>"9"})
    @product.save
    expect(@product.errors.messages).not_to be_empty
    @product.reload
    expect(@product.creatives.detect {|creative| creative['role'] == 'primary_artist'}).not_to be_blank
  end
end
