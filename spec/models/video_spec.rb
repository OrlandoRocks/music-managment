require "rails_helper"

describe Video, "Test non-required accessors" do
  before(:each) do
    @person = create(:person, name: 'trevor', email: 'trevor@protocool.net')
    @video = build(:music_video, person: @person, name: 'first video')
  end

  it "should be valid if created from FactoryBot" do
    expect(@video).to be_valid
  end
end

describe Video, "Test Basic Required Fields" do
  it "should set an artist id before validating" do
    vid = MusicVideo.create! TCFactory.music_video_defaults(:selected_artist_name => "Bobo Baggins")
    expect(vid.artist).not_to be_nil
    expect(vid.artist).not_to be_nil
    expect(vid.artist.name).to eql("Bobo Baggins")
  end

  it "should be valid" do
    video = create(:video, length_min: "4", selected_artist_name: "default artist", video_type: "MusicVideo", accepted_format: true)

    expect(video).to be_valid
  end
end

describe Video, "test creation failures without required fields" do
  it "should not fail without a sale_date because it's no longer required" do
    vid = MusicVideo.new TCFactory.music_video_defaults.except(:sale_date)
    expect(vid.errors.messages.size).to eq 0
  end

  it "should not fail without a orig_release_year because it's no longer required" do
    vid = MusicVideo.new TCFactory.music_video_defaults.except(:orig_release_year)
    expect(vid.errors.messages.size).to eq 0
  end

  it "should not fail without genres because it's no longer required" do
    vid = MusicVideo.new TCFactory.music_video_defaults.except(:selected_genre_ids)
    expect(vid.errors.messages.size).to eq 0
  end
end


describe Video, " titleizes video and name" do
  it "should titelize the video name" do
    video = TCFactory.build_music_video(:name => 'first video')
    expect(video.name).to eq("First Video")
  end

  it "should allow an override for video name" do
    video = TCFactory.build_music_video(:allow_different_format => true)
    video.name = "first video"
    video.save
    expect(video.name).to eq("first video")
  end

  it "should create an artist from the selected artist string" do
    video = TCFactory.build_music_video(selected_artist_name: "gogo Bobo")
    expect(video.artist.name).to eq("gogo Bobo")
  end
end

describe Video, "correct handling of likely user inputs for time fields" do
  it "should handle the likely inputs from the UI of min and sec (1)" do
    vid = MusicVideo.create! TCFactory.music_video_defaults(:length_sec => '33', :length_min => 'min')
    expect(vid.errors.messages.size).to eq 0
    expect(vid.length).to eq(33)
  end


  it "should handle the likely inputs from the UI of min and sec (2)" do
    vid = MusicVideo.create! TCFactory.music_video_defaults(:length_min => '2')
    expect(vid.errors.messages.size).to eq 0
    expect(vid.length).to eq(120)
  end

  it "should handle just input of minutes" do
    vid = MusicVideo.create! TCFactory.music_video_defaults(:length_sec => '', :length_min => '2')
    expect(vid.length).to eq(120)
  end

  it "should handle just the input of seconds" do
    vid = MusicVideo.create! TCFactory.music_video_defaults(:length_sec => '300', :length_min => 'min')
    expect(vid.length).to eq(300)
  end

  it "should return minutes and seconds" do
    vid = MusicVideo.create! TCFactory.music_video_defaults(:length_sec => 123, :length_min => 0)
    vid.reload
    expect(vid.length_sec).to eq(3)
    expect(vid.length_min).to eq(2)
  end
end


describe Video, "correct handling of UPC assignment and reassignment" do
  before(:each) do
    @person = FactoryBot.create(:person, :name => 'trevor', :email => 'trevor@protocool.net')
    @video = TCFactory.build_video(:person => @person, :name => 'test video')
  end

  it "should have a Tunecore UPC" do
    expect(@video.upc).not_to be_nil
    expect(@video.upc).to eql(@video.tunecore_upc)
  end

  it "should be able to have a new Tunecore UPC assigned" do
    old_upc = @video.tunecore_upc
    @video.assign_new_tunecore_upc!
    expect(@video.upc).not_to eql(old_upc)
  end

  it "should be findable by the first Tunecore UPC even if it was assigned a new TC UPC" do
    old_upc = @video.tunecore_upc
    @video.assign_new_tunecore_upc!
    @video.reload

    expect(old_upc).not_to eql(@video.upc)
    expect(Video.find_upc(old_upc.number)).to be === @video
  end

  it "should leave an original TuneCore UPC and set it to inactive" do
    @video.assign_new_tunecore_upc!
    expect(Upc.where("inactive = true and upcable_id = ?", @video.id).size).to eq(1)
  end
end



describe Video, "being locked by an administrator" do
  before(:each) do
    @person = FactoryBot.create(:person, name: "Regular User", email: "reg@person.gov")
    @admin = FactoryBot.create(
      :person,
      :name => 'trevor_admin',
      :email => 'trevor_admin@protocool.net',
    )
    @video = TCFactory.build_video(:person => @person, :name => 'test video')
  end

  it "should not start out locked" do
    expect(@video).not_to be_locked
  end

  it "should change locked_by_id when being locked" do
    expect {@video.obtain_lock(@admin)}.to change(@video, :locked_by_id).from(nil).to(@admin.id)
  end

  it "should change locked_by_type when being locked" do
    expect {@video.obtain_lock(@admin)}.to change(@video, :locked_by_type).from(nil).to('Person')
  end

  it "should change locked_by when being locked" do
    expect {@video.obtain_lock(@admin)}.to change(@video, :locked_by).from(nil).to(@admin)
  end

  it "should change locked? when being locked" do
    expect {@video.obtain_lock(@admin)}.to change(@video, :locked?).from(false).to(true)
  end

  it "should not be editable by admin when not locked" do
    expect(@video.editable_by?(@admin)).to eql(false)
  end

  it "should not be editable by owner when admin has the lock" do
    @video.obtain_lock(@admin)
    expect(@video.editable_by?(@person)).to eql(false)
  end

  it "should be editable by admin when locked" do
    @video.obtain_lock(@admin)
    expect(@video.editable_by?(@admin)).to eql(true)
  end

  it "should release the lock" do
    # setup
    video = MusicVideo.new
    person = Person.new
    video.locked_by = person

    # expectations
    expect(person).to receive(:okay_to_release?).and_return(true)
    expect(video).to receive(:update_attribute).and_return(true)

    # run
    video.release_lock
  end
end
