require "rails_helper"

describe Salepoint do

  it "requires a vriable price specific to the store" do
    sp = TCFactory.build(:salepoint, store: Store.where("short_name = ?", "Amazon").first)
    sp.variable_price=Store.where("short_name=?", "AmazonOD").first.variable_prices.first

    sp.valid?
    expect(sp.errors[:variable_price]).to_not be_blank
  end

  it "sets an implicit variable price when there is only one variable price" do
    store = TCFactory.create(:store)
    variable_price_store = TCFactory.create(:variable_prices_store, store: store)
    sp = TCFactory.build(:salepoint, variable_price_id: nil, store: store)
    expect(sp.variable_price).to be_blank

    sp.valid?

    expect(sp.variable_price).to eq(variable_price_store.variable_price)
  end

  describe "#has_delivered_distribution?" do
    it "is true if the latest distribution has a delivered transition" do
      album        = create(:album)
      salepoint    = create(:salepoint, salepointable: album)
      distribution = create(:distribution)
      create(:transition, state_machine: distribution, new_state: "delivered")
      distribution.salepoints << salepoint
      expect(salepoint.has_delivered_distribution?).to eq true
    end

    it "is false if the latest distribution doesn't have a delivered transition" do
      album        = create(:album)
      salepoint    = create(:salepoint, salepointable: album)
      distribution = create(:distribution)
      create(:transition, state_machine: distribution, new_state: "error")
      distribution.salepoints << salepoint
      expect(salepoint.has_delivered_distribution?).to eq false
    end
  end

  describe "#send_metadata_update" do
    it "returns false never been delivered" do
      album        = create(:album)
      salepoint    = create(:salepoint, salepointable: album, finalized_at: DateTime.now)
      distribution = create(:distribution)
      distribution.salepoints << salepoint

      expect(salepoint.send_metadata_update).to eq false
      expect(distribution).not_to receive(:retry)
    end

    it "returns false if distribution doesn't support takedown" do
      album        = create(:album)
      salepoint    = create(:salepoint, salepointable: album, finalized_at: DateTime.now)
      distribution = create(:distribution)
      distribution.salepoints << salepoint
      create(:transition, state_machine: distribution, new_state: "delivered")

      expect_any_instance_of(Distribution).to receive(:supports_takedown?).and_return(false)
      expect(salepoint.send_metadata_update).to eq false
      expect(distribution).not_to receive(:retry)
    end

    it "returns true if retry is called successfully" do
      album        = create(:album)
      salepoint    = create(:salepoint, salepointable: album, finalized_at: DateTime.now)
      distribution = create(:distribution)
      distribution.salepoints << salepoint
      create(:transition, state_machine: distribution, new_state: "delivered")

      expect_any_instance_of(Distribution).to receive(:supports_takedown?).and_return(true)
      expect(salepoint.send_metadata_update).to eq true
      expect(salepoint.distributions.latest.delivery_type).to eq "metadata_only"
      expect(salepoint.distributions.latest.reload.state).to eq "enqueued"
      expect(distribution).not_to receive(:retry)
    end
  end

  describe "#finalize" do
    let!(:album) { create(:album) }

    context "when itunes store" do
      before do
        allow(FeatureFlipper).to receive(:show_feature?).with(:curated_artists_flag, album.person)
                                                        .and_return(true)
      end

      it "calls itunes methods" do
        salepoint = described_class.new(salepointable: album,
                                        store_id: Store::ITUNES_STORE_IDS.first,
                                        payment_applied: true)
        salepoint.save(validate: false)

        expect(salepoint).to receive(:create_apple_artists)
        expect(salepoint).to receive(:verify_apple_curated_artist)
        salepoint.finalize
      end
    end

    context "when not itunes store" do
      it "doesn't call itunes methods" do
        salepoint = create(:salepoint, salepointable: album, payment_applied: true)

        expect(salepoint).not_to receive(:create_apple_artists)
        expect(salepoint).not_to receive(:verify_apple_curated_artist)
        salepoint.finalize
      end
    end

    context "when the salepoint is a YT Proxy Salepoint" do
      let(:salepoint) { create(:salepoint, payment_applied: false, store_id: Store::YTSR_PROXY_ID, salepointable: album, payment_applied: true) }

      context "when salepoint is added post album purchase" do

        before do
          #album has been previously purchased and approved
          allow_any_instance_of(Album).to receive(:approved?).and_return(true)
          allow_any_instance_of(Album).to receive(:finalized?).and_return(true)
        end

        it "creates track Monetizations" do
          expect(TrackMonetization::YoutubePostApprovalWorker)
            .to receive(:perform_async)
            .with(salepoint.salepointable_id, Store::YTSR_STORE_ID)

          salepoint.finalize
        end
      end

      context "when salepoint is added pre album purchase" do
        before do
          #album hasn not been previously purchased and approved
          allow_any_instance_of(Album).to receive(:approved_for_distribution?).and_return(false)
          allow_any_instance_of(Album).to receive(:finalized?).and_return(false)
        end

        it "does not create track Monetizations" do
          expect(salepoint).to_not receive(:monetize_yt_tracks)
          expect(salepoint).to receive(:post_purchase_ytsr_proxy_salepoint?).and_return(false)

          salepoint.finalize
        end
      end
    end


    context "when the salepoint is a FB Salepoint" do
      let(:salepoint) { create(:salepoint, payment_applied: false, store_id: Store::FB_REELS_STORE_ID, salepointable: album, payment_applied: true) }

      context "when salepoint is added post album purchase" do

        before do
          #album has been previously purchased and approved
          allow_any_instance_of(Album).to receive(:approved?).and_return(true)
          allow_any_instance_of(Album).to receive(:finalized?).and_return(true)
        end

        it "creates track Monetizations" do
          expect(TrackMonetization::FbPostApprovalWorker)
            .to receive(:perform_async)
            .with(salepoint.salepointable_id, Store::FBTRACKS_STORE_ID)

          salepoint.finalize
        end
      end

      context "when salepoint is added pre album purchase" do
        before do
          #album hasn not been previously purchased and approved
          allow_any_instance_of(Album).to receive(:approved_for_distribution?).and_return(false)
          allow_any_instance_of(Album).to receive(:finalized?).and_return(false)
        end

        it "does not create track Monetizations" do
          expect(salepoint).to_not receive(:monetize_fb_tracks)
          expect(salepoint).to receive(:post_purchase_fb_salepoint?).and_return(false)

          salepoint.finalize
        end
      end
    end
  end
end

describe SalepointableStore do

  it "should be valid if the store belongs to the salepointable type" do
    store = TCFactory.create(:store)
    SalepointableStore.create(store: store, salepointable_type: "Album")

    album = TCFactory.create(:album)
    salepoint = TCFactory.build(:salepoint,
      store: store,
      has_rights_assignment: true,
      salepointable_type: album.class.name,
      salepointable_id: album.id
    )
    expect(salepoint.valid?).to eq(true)
  end

  it "should be invalid if the store does not belong to the salepointable type" do
    store = TCFactory.create(:store)
    album = TCFactory.create(:album)

    salepoint = TCFactory.build(:salepoint,
      store: store,
      has_rights_assignment: true,
      salepointable_type: album.class.name,
      salepointable_id: album.id
    )

    # clear the relationship table, to make sure
    SalepointableStore.delete_all
    expect(salepoint.valid?).to eq(false)
  end

  it "should be valid from the factory" do
    salepoint = TCFactory.build(:salepoint)
    expect(salepoint.valid?).to eq(true)
  end

  it "should raise an error on destroy if the salepoint has already been used" do
    salepoint = TCFactory.build(:salepoint)
    expect(Inventory).to receive(:used?).and_return(true)
    expect{ salepoint.destroy }.to raise_error(Salepoint::DestroyAfterUsed, "Attempt to destroy a used salepoint #{salepoint.inspect}")
  end

  it "should not raise an error on destroy if the salepoint hasn't been used" do
    salepoint = TCFactory.build(:salepoint)
    expect(Inventory).to receive(:used?).and_return(false)
    expect{ salepoint.destroy }.not_to raise_error
  end
end

describe Salepoint, "#takedown!" do
  before(:each) do
    store = TCFactory.create(:store)
    @album = TCFactory.create(:album)
    allow(PetriBundle).to receive(:for).and_return(TCFactory.create(:petri_bundle, album: @album))
    @salepoint = TCFactory.create(:salepoint,
      store: store,
      has_rights_assignment: true,
      salepointable_type: @album.class.name,
      salepointable_id: @album.id
    )
    product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
    @purchase = TCFactory.create(:purchase, product: product, person_id: @album.person_id, related: @salepoint)
  end

  it "should destroy unpaid related purchase" do
    expect {
      @album.takedown!
    }.to change{ Purchase.where("id = ?", @purchase.id).count }.from(1).to(0)
  end

  it "should not destroy paid related purchase" do
    invoice = TCFactory.create(:invoice, person: @album.person) #final_settlement_amount_cents: @purchase.cost_cents
    @purchase.invoice = invoice
    @purchase.save
    expect(@purchase.paid?).to be_falsey
    Purchase.process_purchases_for_invoice(invoice)
    @purchase.reload
    expect(@purchase.paid?).to be_truthy

    @album.takedown!
    expect(Purchase.where(id: @purchase.id).size).to be 1
  end

  it "should update the genre if available" do
    store = Store.find(36)
    ringtone = FactoryBot.create(:ringtone)
    ringtone_salepoint = TCFactory.create(:salepoint,
                                          store: store,
                                          has_rights_assignment: true,
                                          salepointable_type: ringtone.class.name,
                                          salepointable_id: ringtone.id)

    product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
    ringtone_purchase = TCFactory.create(:purchase, product: product, person_id: ringtone.person_id, related: ringtone)
    expect(ringtone).to receive(:ensure_takedown_genre)
    allow(ringtone_salepoint).to receive(:salepointable).and_return(ringtone)
    ringtone_salepoint.takedown!
  end

  it "should not call ensure_takedown_genre if album takedown" do
    store = Store.find(36)
    ringtone = FactoryBot.create(:ringtone)
    ringtone_salepoint = TCFactory.create(:salepoint,
                                          store: store,
                                          has_rights_assignment: true,
                                          salepointable_type: ringtone.class.name,
                                          salepointable_id: ringtone.id)

    product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
    ringtone_purchase = TCFactory.create(:purchase, product: product, person_id: ringtone.person_id, related: ringtone)

    expect(ringtone).not_to receive(:ensure_takedown_genre)

    allow(ringtone_salepoint).to receive(:salepointable).and_return(ringtone)
    ringtone_salepoint.takedown!(true)
  end

  context "salepoint has related salepoint_songs" do
    it "should takedown all related salepoint_songs if they exist" do
      yt_store = Store.find_by(abbrev: "ytsr")
      salepointable_store = FactoryBot.create(:salepointable_store, store: yt_store)
      person = FactoryBot.create(:person, email: "g@gmail.com")
      album = FactoryBot.create(:album, :finalized, payment_applied: true, person: person)
      salepoint = FactoryBot.create(:salepoint, salepointable: album, store: yt_store)
      song1 = FactoryBot.create(:song, album: album)
      song2 = FactoryBot.create(:song, album: album)
      salepoint_song1 = FactoryBot.create(:salepoint_song, takedown_at: nil, song: song1, salepoint: salepoint)
      salepoint_song2 = FactoryBot.create(:salepoint_song, takedown_at: nil, song: song2, salepoint: salepoint)

      salepoint.takedown!

      expect(salepoint_song1.reload.takedown_at).not_to eq nil
      expect(salepoint_song2.reload.takedown_at).not_to eq nil
    end
  end

  context "salepoint has an store not in use" do
    it "should mark the store as takendown but not call functions that exist within the send_metadata_update method" do
      not_used_store = Store.where(in_use_flag: false).last
      salepointable_store = FactoryBot.create(:salepointable_store, store: not_used_store)
      salepoint = FactoryBot.create(:salepoint, store: not_used_store, takedown_at: nil)

      expect(salepoint).not_to receive(:distributions)
      salepoint.takedown!

      expect(salepoint.reload.takedown_at.present?).to eq(true)
    end
  end

  context "salepoint has related YT track_monetizations" do
    let(:ytm_store) { Store.find(Store::GOOGLE_STORE_ID) }
    let(:album) { create(:album, :with_songs) }
    let!(:ytm_salepoint) { create(:salepoint, store: ytm_store, salepointable: album) }

    let!(:tracks) do
      album.songs.map { |song| create(:track_monetization, song: song, store_id: Store::YTSR_STORE_ID) }
    end

    let(:expected_args) {
      {
        "track_mons" => tracks,
        "takedown_source"=>"#{ytm_store.name} album level takedown"
      }
    }

    it "calls TrackMonetization::Takedown Service with the related track monetizations" do
      expect(TrackMonetization::TakedownService).to receive(:takedown).once.with(hash_including(expected_args))
      ytm_salepoint.takedown!
    end
  end

  context "salepoint has related FB track_monetizations" do
    let(:fb_store) { Store.find(Store::FB_REELS_STORE_ID) }
    let(:album) { create(:album, :with_songs) }
    let!(:ytm_salepoint) { create(:salepoint, store: fb_store, salepointable: album) }

    let!(:tracks) do
      album.songs.map { |song| create(:track_monetization, song: song, store_id: Store::FBTRACKS_STORE_ID) }
    end

    let(:expected_args) {
      {
        "track_mons" => tracks,
        "takedown_source"=>"#{fb_store.name} album level takedown"
      }
    }

    it "calls TrackMonetization::Takedown Service with the related track monetizations" do
      expect(TrackMonetization::TakedownService).to receive(:takedown).once.with(hash_including(expected_args))
      ytm_salepoint.takedown!
    end
  end
end

describe Salepoint do
  describe "self.create_batch_salepoints" do
    before(:each) do
      @rdio = Store.find(35)
      @person = TCFactory.create(:person)
      @album1 = TCFactory.create(:album, person: @person)
      @album2 = TCFactory.create(:album, person: @person)
      @album3 = TCFactory.create(:album, person: @person)
    end

    it "should return an emptry array if no salepointables supplied" do
      expect(Salepoint.create_batch_salepoints(@rdio, []).blank?).to eq(true)
    end

    it "should create new salepoints" do
      ids = []

      expect {
        ids = Salepoint.create_batch_salepoints(@rdio, [@album1, @album2, @album3])
      }.to change(Salepoint, :count).by(3)

      results = Salepoint.order(id: :desc).limit(3)
      results.each do |result|
        expect(result.store_id).to eq(@rdio.id)
        expect(result.has_rights_assignment).to eq(true)
        expect(result.status).to eq("new")
      end

      [@album1, @album2, @album3].each do |album|
        expect(results.any? {|r| r.salepointable_type == album.class.name && r.salepointable_id == album.id}).to eq(true)
        expect(ids.any? {|i| i == album.id}).to eq(true)
      end
    end

    context "when one of the albums already has a salepoint of that store" do
      it "should only create new salepoints for albums without a salepoint for that store" do
        TCFactory.create(:salepoint, salepointable: @album1, store: @rdio)
        expect {
          Salepoint.create_batch_salepoints(@rdio, [@album1, @album2, @album3])
        }.to change(Salepoint, :count).by(2)
      end
    end
  end
end

describe Salepoint do
  describe "blocked?" do

    it "should return true or false if latest distribution is blocked" do
      store = TCFactory.create(:store)
      album = TCFactory.create(:album)
      salepoint = TCFactory.create(:salepoint,
                                    store: store,
                                    has_rights_assignment: true,
                                    salepointable_type: album.class.name,
                                    salepointable_id: album.id
                                   )

      bundle = TCFactory.create(:petri_bundle, album: album)
      dist1 = TCFactory.create(:distribution, petri_bundle: bundle)
      dist1.state = 'blocked'
      dist1.save

      salepoint.distributions << dist1
      salepoint.save

      expect(salepoint.blocked?).to eq(true)
    end
  end

  describe "block!" do

    it "should return false if has_delivered_distribution?" do
      salepoint = TCFactory.create(:salepoint)
      allow(salepoint).to receive(:has_delivered_distribution?).and_return(true)

      expect(salepoint.block!(actor: "TuneCore Test:", mesage: "blocking_distribution")).to eq(false)
    end

    it "should return false if distribution not in new state" do
      salepoint = TCFactory.create(:salepoint)
      allow(salepoint).to receive(:has_delivered_distribution?).and_return(false)

      dist = TCFactory.create(:distribution)
      dist.state = 'blocked'
      dist.save!

      salepoint.distributions << dist
      salepoint.save!

      expect(salepoint.block!(actor: "TuneCore Test:", mesage: "blocking_distribution")).to eq(false)
    end

    it "should call block! on dist if not delivered and new" do
      salepoint = TCFactory.create(:salepoint)
      allow(salepoint).to receive(:has_delivered_distribution?).and_return(false)

      dist = TCFactory.create(:distribution)

      salepoint.distributions << dist
      salepoint.save!

      expect(salepoint.block!(actor: "TuneCore Test:", mesage: "blocking_distribution")).to eq(true)
      dist.reload
      expect(dist.state).to eq('blocked')
    end

  end

  describe "unblock!" do
    it "should return false if has_delivered_distribution?" do
      salepoint = create(:salepoint)
      allow(salepoint).to receive(:has_delivered_distribution?).and_return(true)

      expect(salepoint.unblock!(actor: "TuneCore Test:", mesage: "unblocking_distribution")).to eq(false)
    end

    it "should return false if distribution not in blocked state" do
      salepoint = create(:salepoint)
      allow(salepoint).to receive(:has_delivered_distribution?).and_return(false)

      dist = create(:distribution)
      dist.state = 'new'
      dist.save!

      salepoint.distributions << dist
      salepoint.save!

      expect(salepoint.unblock!(actor: "TuneCore Test:", mesage: "unblocking_distribution")).to eq(false)
    end

    it "should call block! on dist if not delivered and new" do
      salepoint = create(:salepoint)
      allow(salepoint).to receive(:has_delivered_distribution?).and_return(false)

      dist = create(:distribution)
      dist.state = 'blocked'
      dist.save!

      salepoint.distributions << dist
      salepoint.save!

      expect(salepoint.unblock!(actor: "TuneCore Test:", mesage: "unblocking_distribution")).to eq(true)
      dist.reload
      expect(dist.state).to eq('new')
    end

  end

  describe "#set_variable_price" do
    it "should set the salepoint variable price equal to the price that was passed" do
      salepoint = TCFactory.create(:salepoint)
      salepoint.update_attribute(:store_id, 36)
      salepoint.set_variable_price(9.99)
      expect(salepoint.variable_price.price).to eq(9.99)
    end
  end

  describe "#remove_takedown" do
    before(:each) do
      store = Store.find_by(short_name: "7Digital")
      FactoryBot.create(:salepointable_store, store: store)
      @seven_dig_sp = FactoryBot.create(:salepoint, salepointable: @album, store: store)

      store = Store.find_by(short_name: "Deezer")
      FactoryBot.create(:salepointable_store, store: store)
      @deezer_sp = FactoryBot.create(:salepoint, salepointable: @album, store: store)
    end

    it "sets takedown_at to nil for self and related salepoints" do
      @seven_dig_sp.remove_takedown
      expect(@seven_dig_sp.takedown_at).to eq nil
      expect(@deezer_sp.takedown_at).to eq nil
    end

  end

  describe "#unpurchasable_salepoint?" do
    before(:each) do
      @album = TCFactory.create(:album)
    end

    context "salepoint with inactive store" do
      it "should return true for this salepoint" do
        store = Store.find(9)
        expect(store.is_active).to eq(false) #make sure another test hasn't set napster to active, thus invalidating the test
        salepoint = TCFactory.create(:salepoint, salepointable: @album, store: store)
        expect(salepoint.unpurchasable_salepoint?).to eq(true)
      end
    end

    context "salepoint with active store" do
      it "should return false for this salepoint" do
        store = Store.find(36)
        expect(store.is_active).to eq(true) #make sure another test hasn't set itunes to inactive, thus invalidating the test
        salepoint = TCFactory.create(:salepoint, salepointable: @album, store: store)
        expect(salepoint.unpurchasable_salepoint?).to eq(false)
      end
    end
  end

  describe "::add_youtubesr_store" do
    it "should call Album#add_stores to add the salepoint and Inventory::use! to create the distribution and it should return a hash" do
      album = FactoryBot.create(:album)
      ytm_purchase = double(Purchase)

      success_salepoint = "success"
      expect(album).to receive(:add_stores).and_return([[success_salepoint], []])
      expect(Inventory).to receive(:use!).with(album.person, {item_to_use: success_salepoint, purchase: ytm_purchase}).and_return(true)
      expect(Salepoint.add_youtubesr_store(album, ytm_purchase)).to eq(success_salepoint)
    end
  end

  describe "#paid_post_proc" do
    context "called after salepoint purchase" do
      it "should call #update_post_purchase" do
        salepoint = TCFactory.create(:salepoint)
        expect(salepoint).to receive(:update_post_purchase)
        salepoint.paid_post_proc
      end

      describe '#create_audit_for_reviewable_stores' do
        let(:user) { FactoryBot.create(:person) }

        before(:each) do
          @album = FactoryBot.create(:album, person: user, payment_applied: true, finalized_at: Time.now, legal_review_state: "APPROVED")
          @spotify  = Store.find(26)
          @itunes   = Store.find(36)
        end

        it "marks approved album as 'NEEDS REVIEW' for reviewable salepoints" do
          allow(@itunes).to receive(:has_variable_pricing?) {false}
          allow(@itunes).to receive(:reviewable) {true}
          salepoint = FactoryBot.create(:salepoint, salepointable: @album, store: @itunes)
          salepoint.paid_post_proc
          expect(@album.reload.legal_review_state).to eq("NEEDS REVIEW")
        end

        it "does not mark approved albums as 'NEEDS REVIEW' for non reviewable salepoints" do
          allow(@spotify).to receive(:needs_rights_assignment?) {false}
          @album.legal_review_state = "APPROVED"
          @album.save
          salepoint = FactoryBot.create(:salepoint, salepointable: @album, store: @spotify)
          salepoint.paid_post_proc
          expect(@album.legal_review_state).to eq("APPROVED")
        end
      end
    end
  end

  describe "#update_post_purchase" do
    context "related album is finalized" do
      it "should update the salepoint's payment_applied and finalized_at" do
        album = TCFactory.create(:album, finalized_at: Time.now)
        salepoint = TCFactory.create(:salepoint, salepointable: album, payment_applied: false)

        expect(salepoint.finalized_at.nil?).to eq(true)
        expect(salepoint.payment_applied).to eq(false)
        salepoint.update_post_purchase

        expect(salepoint.finalized_at.nil?).to eq(false)
        expect(salepoint.payment_applied).to eq(true)
      end
    end

    context "related album is unfinalized" do
      it "should update the salepoint's payment_applied" do
        album = TCFactory.create(:album, finalized_at: nil)
        salepoint = TCFactory.create(:salepoint, salepointable: album, payment_applied: false)

        expect(salepoint.finalized_at.nil?).to eq(true)
        expect(salepoint.payment_applied).to eq(false)
        salepoint.update_post_purchase

        expect(salepoint.finalized_at.nil?).to eq(true)
        expect(salepoint.payment_applied).to eq(true)
      end
    end
  end

  describe "#preorder_data" do
    before(:each) do
      @album = FactoryBot.create(:album)
      @store = Store.find_by(short_name: "iTunesWW")
      FactoryBot.create(:salepointable_store, store: @store)
      @salepoint = FactoryBot.create(:salepoint, store: @store, salepointable: @album, variable_price: Store.ITUNES_US.variable_prices.first)
    end

    context "salepoint does not have preorder_data" do
      it "should return nil" do
        expect(@salepoint.preorder_data).to eq(nil)
      end
    end

    context "salepoint has preorder_data" do
      before(:each) do
        @preorder_purchase = FactoryBot.create(:preorder_purchase, album: @album, itunes_enabled: false)
        @spd = FactoryBot.create(:salepoint_preorder_data, preorder_purchase: @preorder_purchase, salepoint: @salepoint)
      end

      context "spd is not enabled on the preorder_purchase, but the preorder_purchase is paid for" do
        it "should return nil" do
          @preorder_purchase.update_attribute(:paid_at, Time.now)
          expect(@salepoint.preorder_data).to eq(nil)
        end
      end

      context "spd is enabled on the preorder_purchase, but is not purchased on the preorder_purchase level" do
        it "should return nil" do
          @preorder_purchase.update_attribute(:itunes_enabled, true)
          expect(@salepoint.preorder_data).to eq(nil)
        end
      end

      context "spd is enabled on the preorder_purchase and the preorder_purchase is paid for" do
        it "should call SalepointPreorderData#preorder_data" do
          @preorder_purchase.update(itunes_enabled: true, paid_at: Time.now)
          expect(@salepoint.salepoint_preorder_data).to receive(:preorder_data).and_return(true)
          @salepoint.preorder_data
        end
      end
    end
  end

  describe "#check_enable_flag_on_preorder_purchase" do
    before(:each) do
      @album = FactoryBot.create(:album)
      @preorder_purchase = FactoryBot.create(:preorder_purchase, album: @album)
    end

    context "when a itunes salepoint_preorder_data instance is destroyed" do
      it "should set the preorder_purchase flag for itunes_enabled to false" do
        store = Store.find_by(short_name: "iTunesWW")
        FactoryBot.create(:salepointable_store, store: store)
        salepoint = FactoryBot.create(:salepoint, store: store, salepointable: @album, variable_price: Store.ITUNES_US.variable_prices.first)
        spd = FactoryBot.create(:salepoint_preorder_data, preorder_purchase: @preorder_purchase, salepoint: salepoint)
        @preorder_purchase.update(itunes_enabled: true)

        salepoint.destroy
        expect(@preorder_purchase.reload.itunes_enabled).to eq(false)
      end
    end

    context "when a google salepoint_preorder_data instance is destroyed" do
      it "should set the preorder_purchase flag for google_enabled to false" do
        store = Store.find_by(short_name: "Google")
        FactoryBot.create(:salepointable_store, store: store)
        salepoint = FactoryBot.create(:salepoint, store: store, salepointable: @album, variable_price: Store.ITUNES_US.variable_prices.first)
        spd = FactoryBot.create(:salepoint_preorder_data, preorder_purchase: @preorder_purchase, salepoint: salepoint)
        @preorder_purchase.update(google_enabled: true)

        spd.destroy
        expect(@preorder_purchase.reload.google_enabled).to eq(false)
      end
    end
  end

  describe "#admin_update" do
    before(:each) do
      @album = FactoryBot.create(:album)
      @preorder_purchase = FactoryBot.create(:preorder_purchase, album: @album)
    end

    context "for a itunes salepoint_proerder_data" do
      it "should call update on the spd instance twice, update_grat_tracks, and update on the salepoint" do
        store = Store.find_by(short_name: "iTunesWW")
        FactoryBot.create(:salepointable_store, store: store)
        salepoint = FactoryBot.create(:salepoint, store: store, salepointable: @album, variable_price: Store.ITUNES_US.variable_prices.first)
        spd = FactoryBot.create(:salepoint_preorder_data, preorder_purchase: @preorder_purchase, salepoint: salepoint)

        expect(spd).to receive(:update).twice.and_return(true)
        expect(spd.salepoint).to receive(:update).and_return(true)
        expect(spd).to receive(:update_grat_tracks).and_return(true)
        spd.admin_update({start_date: "11/20/2014"},{})
      end
    end

    context "for a google salepoint_preorder_data" do
      it "should only call update once" do
        store = Store.find_by(short_name: "Google")
        FactoryBot.create(:salepointable_store, store: store)
        salepoint = FactoryBot.create(:salepoint, store: store, salepointable: @album, variable_price: Store.ITUNES_US.variable_prices.first)
        spd = FactoryBot.create(:salepoint_preorder_data, preorder_purchase: @preorder_purchase, salepoint: salepoint)

        expect(spd).to receive(:update).and_return(true)
        expect(spd.salepoint).not_to receive(:update)
        expect(spd).not_to receive(:update_grat_tracks)
        spd.admin_update({start_date: "11/20/2014"},{})
      end
    end
  end

  describe "::salepoints_to_add_to_cart" do
    context "album has one salepoint from a inactive store, another that is marked as payment_applied and one that unpurchased and not paid for" do
      before(:each) do
        @album = FactoryBot.create(:album)
        inactive_store = Store.find_by(short_name: "YoutubeSR")
        paid_store = Store.find_by(short_name: "Google")
        unpaid_store = Store.is_active.last
        FactoryBot.create(:salepointable_store, store: inactive_store)
        FactoryBot.create(:salepointable_store, store: paid_store)
        FactoryBot.create(:salepointable_store, store: unpaid_store)
        @inactive_store_salepoint = FactoryBot.create(:salepoint, store: Store.find_by(short_name: "YoutubeSR"), salepointable: @album)
        @paid_salepoint           = FactoryBot.create(:salepoint, store: paid_store, salepointable: @album, payment_applied: true)
        @other_salepoint          = FactoryBot.create(:salepoint, store: unpaid_store, salepointable: @album)
        allow(@paid_salepoint).to receive(:payment_applied?).and_return(true)
      end

      it "should return the other salepoint and not the paid for salepoint or salepoint of an inactive store" do
        allow(@album)
          .to receive_message_chain(:salepoints, :includes)
          .and_return([@paid_salepoint, @other_salepoint, @inactive_store_salepoint])

        expect(Salepoint.salepoints_to_add_to_cart(@album)).to eq([@other_salepoint])
      end
    end
  end

  describe "::send_sns_notification" do
    context "when SNS_RELEASE_NEW_TOPIC is not set" do
      it 'does not send any sns notification' do
        ENV['SNS_RELEASE_NEW_TOPIC'] = nil

        expect(Sns::Notifier).to_not receive(:perform)

        Salepoint.send_sns_notification({})
      end
    end

    context 'when album_store_id_hash is empty' do
      it 'does not send any sns notification' do
        ENV['SNS_RELEASE_NEW_TOPIC'] = 'true'

        expect(Sns::Notifier).to_not receive(:perform)

        Salepoint.send_sns_notification({})
      end
    end

    context 'when single album ID is passed without store ids' do
      it 'sends sns notification with delivery type as none' do
        ENV['SNS_RELEASE_NEW_TOPIC'] = 'SNS_RELEASE_NEW_TOPIC'
        album = create(:album)
        salepoint = double(salepoint)

        allow(Album).to receive(:find).with(album.id).and_return(album)
        allow(album).to receive(:salepoints).and_return(salepoint)
        allow(salepoint).to receive(:pluck).with(:store_id).and_return([1,2,3])
        allow(album).to receive(:approved_for_distribution?).and_return(true)

        expect(Sns::Notifier).to receive(:perform).with(
          topic_arn: "SNS_RELEASE_NEW_TOPIC",
          album_ids_or_upcs: [album.id],
          store_ids: [1,2,3],
          delivery_type: 'none'
        )

        Salepoint.send_sns_notification({album.id => []})
      end
    end

    context 'when multiple album ID are passed with store ids' do
      it 'sends multiple SNS notification' do
        ENV['SNS_RELEASE_NEW_TOPIC'] = 'SNS_RELEASE_NEW_TOPIC'
        album = create(:album)
        album1 = create(:album)
        album2 = create(:album)

        allow_any_instance_of(Album).to receive(:approved_for_distribution?).and_return(true)

        expect(Sns::Notifier).to receive(:perform).with(
          topic_arn: "SNS_RELEASE_NEW_TOPIC",
          album_ids_or_upcs: [album.id],
          store_ids: [1,2,3],
          delivery_type: 'full_delivery'
        )

        expect(Sns::Notifier).to receive(:perform).with(
          topic_arn: "SNS_RELEASE_NEW_TOPIC",
          album_ids_or_upcs: [album1.id],
          store_ids: [4,5,6],
          delivery_type: 'full_delivery'
        )

        expect(Sns::Notifier).to receive(:perform).with(
          topic_arn: "SNS_RELEASE_NEW_TOPIC",
          album_ids_or_upcs: [album2.id],
          store_ids: [],
          delivery_type: 'none'
        )

        Salepoint.send_sns_notification({
          album.id => [1,2,3],
          album1.id => [4,5,6],
          album2.id => []
        })
      end
    end
  end

  describe "#send_sns_notification_takedown" do
    let!(:person) {create(:person)}
    let!(:album) do
      if person.present?
        create(:album, person:person)
      else
        create(:album)
      end
    end
    let!(:salepoint) { create(:salepoint, salepointable: album) }
    subject { salepoint.send_sns_notification_takedown(person) }

    context "when feature is not enabled for user" do
      it "does not send message" do
        expect(FeatureFlipper).to receive(:show_feature?).with(:use_sns, any_args).and_return(false)
        expect(Sns::Notifier).to_not receive(:perform)
        subject
      end
    end

    context "when env var is not present" do
      it "does not send message" do
        expect(FeatureFlipper).to receive(:show_feature?).with(:use_sns, any_args).and_return(true)
        expect(ENV).to receive(:fetch).with("SNS_RELEASE_TAKEDOWN_URGENT_TOPIC", any_args).and_return(nil)
        expect(Sns::Notifier).to_not receive(:perform)
        subject
      end
    end

    context "when nil user" do
      let!(:person) { nil }
      it "raises error" do
        expect(FeatureFlipper).to receive(:show_feature?).with(:use_sns, any_args).and_return(true)
        expect { subject }.to raise_error
      end
    end

    context "with valid inputs" do
      it "calls Sns::Notifier" do
        expect(FeatureFlipper).to receive(:show_feature?).with(:use_sns, any_args).and_return(true)
        expect(Sns::Notifier).to receive(:perform).with(hash_including(
          album_ids_or_upcs: [album.id],
          store_ids:[salepoint.store_id],
          delivery_type:"takedown",
          person_id:person.id
        ))
        subject
      end
    end

  end

end
