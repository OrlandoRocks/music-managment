require "rails_helper"

describe DeliveryBatch do
  let(:store) { create(:store) }
  before do
    create(:store_delivery_config, store: store, batch_size: 5)
  end

  describe "#increment_joined_count" do
    let(:batch) { create(:delivery_batch, store: store, active: true) }

    it "should increase the joined count" do
      batch.increment_joined_count

      expected_batch = DeliveryBatch.find(batch.id)
      expect(expected_batch.count).to eq(1)
    end

    it "should increase the joined count even if it has been updated out of band" do
      DeliveryBatch.find(batch.id).update!(count: 4)
      batch.increment_joined_count

      expected_batch = DeliveryBatch.find(batch.id)
      expect(expected_batch.count).to eq(5)
      expect(expected_batch.active).to eq(false)
    end
  end

  describe "#decrement_joined_count" do
    let(:batch) { create(:delivery_batch, store: store, active: true, count: 3) }

    it "should decrement the joined count" do
      batch.decrement_joined_count

      expected_batch = DeliveryBatch.find(batch.id)
      expect(expected_batch.count).to eq(2)
    end
    it "should decrement the joined count even when it has been updated out of band" do
      DeliveryBatch.find(batch.id).update!(count: 4)
      batch.decrement_joined_count

      expected_batch = DeliveryBatch.find(batch.id)
      expect(expected_batch.count).to eq(3)
      expect(expected_batch.active).to eq(true)
    end
    it "should activate the batch when below the max batch size" do
      full_batch = create(:delivery_batch, store: store, count: 5, active: false)
      full_batch.decrement_joined_count

      expected_batch = DeliveryBatch.find(full_batch.id)
      expect(expected_batch.count).to eq(4)
      expect(expected_batch.active).to eq(true)
    end
  end

  describe "#increment_processed_count" do
    let(:batch) do
      create(:delivery_batch,
             store: store,
             processed_count: 2,
             active: true,
             count: 5)
    end

    it "should increase the processed count even if it has been updated out of band" do
      DeliveryBatch.find(batch.id).update!(processed_count: 3)
      batch.increment_processed_count

      expected_batch = DeliveryBatch.find(batch.id)
      expect(expected_batch.processed_count).to eq(4)
    end
    it "should deactivate the batch when all items have been processed" do
      almost_complete_batch = create(:delivery_batch,
                                     store: store,
                                     processed_count: 4,
                                     active: true,
                                     count: 5)
      almost_complete_batch.increment_processed_count

      expected_batch = DeliveryBatch.find(almost_complete_batch.id)
      expect(expected_batch.processed_count).to eq(5)
      expect(expected_batch.active).to eq(false)
    end
    it "should not deactivate the batch when max batch size has not be achieved" do
      almost_complete_batch = create(:delivery_batch,
                                     store: store,
                                     processed_count: 3,
                                     active: true,
                                     count: 4)
      almost_complete_batch.increment_processed_count

      expected_batch = DeliveryBatch.find(almost_complete_batch.id)
      expect(expected_batch.processed_count).to eq(4)
      expect(expected_batch.active).to eq(true)
    end
  end
end
