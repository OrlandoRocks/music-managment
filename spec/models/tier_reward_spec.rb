require 'rails_helper'

RSpec.describe TierReward, type: :model do
  describe "relationships" do
    subject { build(:tier_reward) }

    it { should belong_to(:tier) }
    it { should belong_to(:reward) }
  end

  describe "validations" do
    let(:subject) { build(:tier_reward) }

    it { should validate_uniqueness_of(:tier_id).scoped_to(:reward_id) }
  end
end
