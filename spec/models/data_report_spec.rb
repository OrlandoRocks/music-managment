require "rails_helper"

describe DataReport, "created from TCFactory" do
  before(:each) do
    @data_report = TCFactory.build_data_report
  end

  it "should be valid" do
    expect(@data_report).to be_valid
  end
end

describe DataReport, "when returning resolution" do
  before(:each) do
    @data_report = TCFactory.build_data_report(:resolution => [:day, :week, :month, :year, :live])
    @data_report.reload
  end

  it "should return an array from the db string" do
    expect(@data_report.resolution).to be_an_instance_of(Array)
  end

  it "should return an array of :symbols from the db string" do
    expect(@data_report.resolution.first).to be_an_instance_of(Symbol)
  end

  it "should return true for a resolution that is present" do
    expect(@data_report.resolution_includes?(:day)).to eq(true)
  end

  it "should return false when asked for resolution that isn't present" do
    expect(@data_report.resolution_includes?(:quarter)).to eq(false)
  end

  it "should return false if asked for an invalid symbol" do
    expect(@data_report.resolution_includes?(:not_on_list)).to eq(false)
  end

  it "should return false if asked for a string" do
    expect(@data_report.resolution_includes?("quarter")).to eq(false)
  end

  it "should return false if asked for an integer" do
    expect(@data_report.resolution_includes?(125)).to eq(false)
  end

  it "should return true when asked if it is a live report" do
    expect(@data_report.resolution_includes?(:live)).to eq(true)
  end
end

describe DataReport, "when setting resolution" do
  it "should not be valid if no resolution is set" do
    data_report = DataReport.create(TCFactory.data_report_defaults.except(:resolution))
    expect(data_report).not_to be_valid
  end

  it "should only accept an array" do
    data_report = DataReport.create(TCFactory.data_report_defaults(:resolution => "Just a string"))
    expect(data_report).not_to be_valid
  end

  it "should only accept symbols in the array" do
    data_report = DataReport.create(TCFactory.data_report_defaults(:resolution => ["string", "test", :symbol, 4]))
    expect(data_report).not_to be_valid
  end

  it "should not accept symbols that are outside of the DataReports resolution scope" do
    data_report = DataReport.create(TCFactory.data_report_defaults(:resolution => [:day, :not_set_on_list, :year]))
    expect(data_report).not_to be_valid
  end

  it "should be valid if just one valid resolution is added" do
    data_report = DataReport.create(TCFactory.data_report_defaults(:resolution => [:day]))
    expect(data_report).to be_valid
  end
end

describe DataReport, "when calling record" do
  before(:each) do
    @data_report = DataReport.create(TCFactory.data_report_defaults(:resolution => [:day]))
  end

  it "should raise an implementation error if build_records is not overridden" do
    expect {@data_report.record}.to raise_error(RuntimeError, /build_records must be overridden by the inheriting class/)
  end
end

describe DataReport, "when calling the #record method with :live report" do
  before(:each) do
    @data_report = TCFactory.build_data_report(:resolution => [:live], :live_update_limit => 15)
    allow(@data_report).to receive(:record_live).and_return(true)
  end

  it "should update the #live_update_at method" do
    expect(@data_report.live_update_at).to be_blank
    expect(@data_report.record).to eq(true)
    expect(@data_report.live_update_at).not_to be_blank
  end

  it "should not update (return false) if the #update_limit time has not passed" do
    expect(@data_report.record).to be_truthy
    expect(@data_report.record).to be_falsey
  end

  it "should update (return true) if the #update_limit time has passed" do
    expect(@data_report.record).to be_truthy
    @data_report.live_update_at = @data_report.live_update_at - (@data_report.live_update_limit * 60) - 1
    expect(@data_report.record).to be_truthy
  end
end

describe DataReport, "when calling the #record method with all resolutions other than live" do
  before(:each) do
    @data_report = TCFactory.build_data_report(:resolution => [:day, :week, :month, :quarter, :year], :start_on => Date.today - 13)
    allow(@data_report).to receive(:record_day).and_return(true)
    allow(@data_report).to receive(:record_week).and_return(true)
    allow(@data_report).to receive(:record_month).and_return(true)
    allow(@data_report).to receive(:record_quarter).and_return(true)
    allow(@data_report).to receive(:record_year).and_return(true)
    #all of these methods are stubbed, because the must be overridden by the base class, but I want to make sure that they get called at this level
  end

  it "should update the #last_calculated_on method" do
    expect(@data_report.last_calculated_on).to be_blank
    expect(@data_report.record).to eq(true)
    @data_report.reload
    expect(@data_report.last_calculated_on).not_to be_blank
  end

  it "should not update a second time if called on the same day" do
    expect(@data_report.record).to eq(true)
    expect(@data_report.record).to eq(false)
  end

end


describe DataReport, "when asking if there is a record for resolution records" do
  before(:each) do
    @data_report = TCFactory.build_data_report
    TCFactory.build_data_record(:data_report => @data_report, :resolution_identifier => :M3_2008.to_s)
    TCFactory.build_data_record(:data_report => @data_report, :resolution_identifier => :W52_2010.to_s)
    TCFactory.build_data_record(:data_report => @data_report, :resolution_identifier => :Q4_1978.to_s)
  end

  it "should believe that it has generated the record for :M3_2008 resolution" do
    expect(@data_report.has_recorded?(:M3_2008)).to eq(true)
  end

  it "should believe that it has generated the record for :W52_2010 resolution" do
    expect(@data_report.has_recorded?(:W52_2010)).to eq(true)
  end

  it "should believe that it has generated the record for :Q4_1978 resolution" do
    expect(@data_report.has_recorded?(:Q4_1978)).to eq(true)
  end

  it "should not believe it has generated the record for :Q1_2008" do
    expect(@data_report.has_recorded?(:Q1_2008)).to eq(false)
  end
end

describe DataReport, "when creating data records" do
  before(:each) do
    @data_report = TCFactory.build_data_report(:resolution => [:day])
  end

  it 'should not create any errors for good data' do
    expect{@data_report.create_record(:day, Date.today, [[15, 'complete albums'], [15, 'errored albums'], [48, 'dollars']])}.not_to raise_error
  end

  it 'should create an error if the resolution passed is not in the records list' do
    expect{@data_report.create_record(:week, Date.today, [[15, 'c albums'], [15, 'e albums'], [48, 'dollars']])}.to raise_error
  end

  it 'should create a number of records equivalent to the number of data points in the data arrray' do
    @data_report.create_record(:day, Date.today, [[15, 'c albums'], [15, 'e albums'], [48, 'dollars']])
    expect(DataRecord.where('data_report_id = ? AND resolution_unit = ?', @data_report.id, :day.to_s).size).to eq(3)
  end
end

describe DataReport, "when calling create_record on a report that has an updateable_past" do
  before(:each) do
    @data_report = TCFactory.build_data_report(:resolution => [:week], :updateable_past => true)
    @data_report.create_record(:week, Date.today, [[15, 'c albums'], [16, 'e albums'], [48, 'dollars']])
  end

  it "should not change the count of the records" do
    expect{@data_report.create_record(:week, Date.today, [[1, 'c albums'], [600, 'e albums'], [1, 'dollars']])}.not_to change(DataRecord, :count)
  end
end


describe DataReport, "when asking questions of the should_update? where all should pass" do
  before(:each) do
    @data_report = TCFactory.build_data_report(:resolution => [:live, :day, :week, :month, :quarter, :year], :start_on => '2007-01-01')
  end

  it "should return true for :live" do
    expect(@data_report.should_update?(:live, '2007-01-01'.to_date)).to be_truthy
  end

  it "should return true for :day" do
    expect(@data_report.should_update?(:day, '2007-01-01'.to_date)).to be_truthy
  end

  it "should return true for :week" do
    expect(@data_report.should_update?(:week, '2007-01-08'.to_date)).to be_truthy
  end

  it "should return true for :month" do
    expect(@data_report.should_update?(:month, '2007-02-01'.to_date)).to be_truthy
  end

  it "should return true for :quarter" do
    expect(@data_report.should_update?(:quarter, '2007-04-01'.to_date)).to be_truthy
  end

  it "should return true for :year" do
    expect(@data_report.should_update?(:year, '2008-01-01'.to_date)).to be_truthy
  end
end

describe DataReport, "when asking questions of the should_update? where most should fail because it is too close to the start_on date" do
  before(:each) do
    @data_report = TCFactory.build_data_report(:resolution => [:live, :day, :week, :month, :quarter, :year], :start_on => '2008-01-01')
  end

  it "should return true for :live" do
    expect(@data_report.should_update?(:live, '2008-01-01'.to_date)).to be_truthy
  end

  it "should return true for :day" do
    expect(@data_report.should_update?(:day, '2008-01-01'.to_date)).to be_truthy
  end

  it "should return true for :week" do
    expect(@data_report.should_update?(:week, '2007-12-31'.to_date)).to be_truthy
  end

  it "should return true for :quarter" do
    expect(@data_report.should_update?(:quarter, '2007-10-01'.to_date)).to be_truthy
  end

  it "should return true for :year" do
    expect(@data_report.should_update?(:year, '2007-01-01'.to_date)).to be_truthy
  end
end

