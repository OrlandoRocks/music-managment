require "rails_helper"

RSpec.describe PeopleFlag, type: :model do
  describe ".latest_for_flag" do
    it "returns most recently created people_flag for the given flag" do
      person = create(:person)
      flag = Flag.find_by(Flag::ADDRESS_LOCKED)
      flag_reasons = FlagReason.address_lock_flag_reasons
      flag_reason_1 = flag_reasons.first
      flag_reason_2 = flag_reasons.second
      people_flag_1 = create(:people_flag, person: person, flag: flag, flag_reason: flag_reason_1)
      people_flag_2 = create(:people_flag, person: person, flag: flag, flag_reason: flag_reason_2)

      subject = PeopleFlag.latest_for_flag(flag)
      expect(subject).to eq(people_flag_2)
    end
  end
end
