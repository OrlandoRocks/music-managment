require "rails_helper"

RSpec.describe Product do
  describe "::price" do
    it "accepts an array of objects" do
      expect {
        Product.price(Person.first, [Album.new, Album.new])
      }.not_to raise_error
    end

    it "accepts a single object" do
      expect {
        Product.price(Person.first, Album.new)
      }.not_to raise_error
    end

    it "is able to price one object" do
      album1 = Album.new
      ad_hoc = Product.new
      allow(ad_hoc).to receive_messages(:calculate_price => 4.0)
      allow(Product).to receive_messages(:find_ad_hoc_product => ad_hoc)

      expect(Product.price(Person.first, album1)).to eq Money.new(4_00)
    end

    it "is able to price multiple_items" do
      album1 = Album.new
      album2 = Album.new
      ad_hoc = Product.new
      allow(ad_hoc).to receive_messages(:calculate_price => 2.0)
      allow(Product).to receive_messages(:find_ad_hoc_product => ad_hoc)

      expect(Product.price(Person.first, [album1, album2])).to eq Money.new(4_00)
    end

    it "returns the original price of the product" do
      build_flat_rate_ad_hoc_album_product
      mock_model(TargetedProduct,
                :display_name => "Override Display Name",
                :price_adjustment_type => "override",
                :price_adjustment => 49.99,
                :price_adjustment_description => "Reg. $57.99",
                :flag_text => nil,
                :show_expiration_date => false)

      allow(TargetedProduct)
        .to receive(:targeted_product_for_active_offer)
        .and_return(nil)

      expect(Product.price(Person.first, Album.new, @product))
        .to eq Money.new(@product.price * 100)
    end

    context "when calculating a price for a flat rate ad-hoc album" do
      before do
        build_flat_rate_ad_hoc_album_product
        mock_album
      end

      it "should have a price of 29.99 when the Album doesn't have any songs or salepoints" do
        expect(Product.price(Person.first, @album)).to eq Money.new(29_99)
      end

      it "should have a price of 29.99 when the Album has 6 songs and 7 salepoints" do
        allow(@songs).to receive(:count).and_return(6)
        allow(@salepoints).to receive(:count).and_return(7)

        expect(Product.price(Person.first, @album)).to eq Money.new(29_99)
      end
    end
  end

  describe "::set_targeted_product_and_price" do
    it "should call set targeted product and set adjusted price on the product" do
      person = Person.first
      product = TCFactory.build(:product)
      allow(product).to receive(:set_targeted_product)
      allow(product).to receive(:set_adjusted_price)

      Product.set_targeted_product_and_price(person, product)

      expect(product).to have_received(:set_targeted_product)
      expect(product).to have_received(:set_adjusted_price)
    end
  end

  describe "::default_products_for" do
    before do
      TCFactory.build_ad_hoc_album_product
      mock_album
    end

    it "returns a US product if the user is from the US" do
      person = FactoryBot.create(
                :person,
                 email: "user@tunecore.com",
               )
      item = FactoryBot.create(:album)
      product = Product.default_products_for(item, person.country_website_id).first

      expect(product.currency).to eq "USD"
      expect(product.country_website_id).to eq 1
    end

    it "returns a CA product if the user is from CA" do
      person = FactoryBot.create(
                :person,
                 email: "doug@greatwhitenorth.ca",
                 country: "CA",
                 postal_code: 'E4Y2E4'
               )
      album  = FactoryBot.create(:album)
      product = Product.default_products_for(album, person.country_website_id).first

      expect(product.currency).to eq "CAD"
      expect(product.country_website_id).to eq 2
    end

    it "returns a DE product if the user is from DE" do
      person = FactoryBot.create(
                :person,
                 email: "fritz@sproketz.de",
                 country: "DE",
                 postal_code: '10117'
               )
      album  = FactoryBot.create(:album)
      products = Product.default_products_for(album, person.country_website_id)

      products.each do |product|
        expect(product.currency).to eq "EUR"
        expect(product.country_website.name).to eq "TuneCore Germany"
      end
    end

    it "has a default price of 29.99" do
      expect(Product.price(Person.first, @album)).to eq Money.new(29_99)
    end
  end

  describe "::add_to_cart" do
    it "adds and prices a soundout product" do
      product = TCFactory.build_soundout_report_product
      soundout_product = TCFactory.create(:soundout_product, product: product)
      report_purchase  = create(:soundout_report, :with_track, soundout_product: soundout_product, person: Person.first )
      purchase = Product.add_to_cart(Person.first, report_purchase, product)

      expect(purchase.cost_cents).to eq(1500)
      expect(purchase).to be_valid

      expect(purchase.related).to eq(report_purchase)
    end

    context "when purchasing a item that responds to add_to_cart_pre_proc" do
      it "calls the pre processing" do
        album   = TCFactory.create(:album)
        song1   = TCFactory.create(:song, :album => album)
        upload1 = TCFactory.create(:upload, :song => song1)

        expect(album).to receive :add_to_cart_pre_proc

        Product.add_to_cart(TCFactory.build(:person),album)
      end

      context "when pre_proc function returns false" do
        it "does not nullify the product to purchase" do
          album   = TCFactory.create(:album)
          song1   = TCFactory.create(:song, :album => album)
          song2   = TCFactory.create(:song, :album => album)
          upload1 = TCFactory.create(:upload, :song => song1)
          upload2 = TCFactory.create(:upload, :song => song2)

          product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)

          expect(product).not_to be nil
          expect(Product)
          .to receive(:create_purchase)
          .with(album.person, album, product)

          Product.add_to_cart(album.person,album, product)
        end
      end

      context "when pre_proc function returns true" do
        it "nullifies the product to purchase" do
          album   = TCFactory.create(:album)
          asset   = TCFactory.create(:s3_asset)
          song1   = TCFactory.create(:song, :album => album, :s3_asset => asset)
          upload1 = TCFactory.create(:upload, :song => song1)

          song1.big_box_meta_data = {'duration'=>[9.minutes * 1000]}
          album.songs << song1

          product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
          expect(product).not_to be nil

          expect(Product)
          .to receive(:create_purchase)
          .with(album.person, kind_of(Single), nil)

          Product.add_to_cart(album.person, album, product)
        end
      end

      context "when an ad_hoc_album_product exists" do
        before do
          TCFactory.build_ad_hoc_album_product
          TCFactory.build_ad_hoc_album_product(49.99)
        end

        it "creates a purchase with the associated album as the related record in the purchase" do
          album = Album.first
          person = album.person
          purchase = Product.add_to_cart(person, album)

          expect(purchase.related).to be === album
        end

        it "sets the person_id of the purchase to the person_id of the submitted person" do
          album = Album.first
          person = album.person
          purchase = Product.add_to_cart(person, album)

          expect(purchase.person_id).to eq(person.id)
        end

        it "picks the first Album product and associate that product with the purchase when added to a cart without a specified product" do
          album = Album.first
          person = album.person
          product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
          purchase = Product.add_to_cart(person, album)

          expect(purchase.product).to eq(product)
        end

        it "associates the specified product with the purchase when added to the cart" do
          album = Album.first
          person = album.person
          product = Product.find_by(country_website: person.country_website)
          purchase = Product.add_to_cart(person, album, product)

          expect(purchase.product).to eq(product)
        end

        it "has the same price calculated by Product.price" do
          album = Album.first
          person = album.person
          product = Product.where(applies_to_product: album.class.name).last
          product_price = Product.price(person, album, product)
          purchase = Product.add_to_cart(person, album, product)

          expect(purchase.cost_cents).to eq((product_price.amount * 100).to_i)
        end
      end
    end
  end

  describe "::create_purchase" do
    context "when credits available" do
      before do
        @person = TCFactory.create(:person)
        @album = TCFactory.create(:album, :person => @person)

        TCFactory.build_ad_hoc_album_product(49.99)
        TCFactory.build_ad_hoc_credit_usage_product
      end

      it "calls CreditUsage.for" do
        expect(CreditUsage).to receive(:for).with(@person, @album)

        Product.create_purchase(@person, @album)
      end

      it "uses credit if available" do
        product = TCFactory.build_album_credit_product(5)
        purchase = TCFactory.create(:purchase, :person_id => @person.id, :product_id => product.id, :related => product, :paid_at => Time.now())
        inventory = TCFactory.create(:inventory,
          :person => @person,
          :inventory_type => 'Album',
          :product_item => product.product_items.first,
          :purchase => purchase,
          :quantity => 5)

        expect(CreditUsage).to receive(:for, &CreditUsage.method(:for))
        expect(Purchase).to receive(:destroy_by_purchase_item).with(@person, @album)
        expect(Product).to receive(:create_purchase_for_unknown_product).with(@person, kind_of(CreditUsage))

        purchase = Product.create_purchase(@person, @album)
      end
    end
  end

  describe "::process_purchase" do
    before do
      build_flat_rate_ad_hoc_album_product
      allow_any_instance_of(Purchase).to receive(:is_plan?).and_return(false)
    end

    it "should take an array of purchases" do
      expect {
        Product.process_purchase(Person.new, [])
      }.not_to raise_error
    end

    it "should take a single purchase" do
      expect {
        Product.process_purchase(Person.new, Purchase.new)
      }.not_to raise_error
    end

    it "calls process_single_purchase" do
      expect(Product).to receive(:process_single_purchase)

      Product.process_purchase(Person.new, Purchase.new)
    end

    it "calls Plans::InventoryShimService" do
      expect(Plans::InventoryShimService).to receive(:call)

      Product.process_purchase(Person.new, Purchase.new)
    end

    it "calls Inventory.use! if is an Ad Hoc product" do
      allow(Inventory).to receive(:use!)

      purchase = TCFactory.build(:purchase)
      product = Product.new(:product_type => 'Ad Hoc')
      purchase.product = product
      purchase.paid_at = Time.now
      Product.process_purchase(Person.new, purchase)

      expect(Inventory).to have_received(:use!)
    end

    it "calls Inventory.use! only once" do
      allow(Inventory).to receive(:use!)

      purchase = Purchase.new(:related => Album.first)
      Product.process_purchase(Person.new, [purchase])

      expect(Inventory).to have_received(:use!)
    end

    it "calls sns notification for purchase" do
      person = create(:person)
      purchase = create(:purchase)

      expect(Product).to receive(:send_sns_notification_for_purchase).with(person, purchase)

      Product.process_purchase(person, purchase)
    end

    it 'does not call salepoint sns notification if non salepoint or salepoint subscription purchase' do
      person = create(:person)
      album = create(:album)
      purchase = Purchase.new(related: album)

      allow(FeatureFlipper).to receive(:show_feature?).with(:use_sns, person).and_return(true)

      expect(Salepoint).to_not receive(:send_sns_notification)

      Product.process_purchase(person, purchase)
    end

    it "calls sns notification with required params when store automator is added" do
      person = create(:person)
      album = create(:album)
      salepoint_subscription = create(:salepoint_subscription, album: album)
      purchase = Purchase.new(related: salepoint_subscription)
      allow(FeatureFlipper).to receive(:show_feature?).with(:use_sns, person).and_return(true)

      expect(Salepoint).to receive(:send_sns_notification).with({album.id => []})

      Product.process_purchase(person, purchase)
    end

    it "calls sns notification when salepoints are added" do
      person = create(:person)
      album = create(:album)
      store1 = create(:store)
      store2 = create(:store)
      salepoint1 = Salepoint.new(salepointable: album, store: store1)
      salepoint2 = Salepoint.new(salepointable: album, store: store2)
      allow(FeatureFlipper).to receive(:show_feature?).with(:use_sns, person).and_return(true)

      expect(Salepoint).to receive(:send_sns_notification).with({album.id => [store1.id, store2.id]})

      Product.process_purchase(person, [
        Purchase.new(related: salepoint1),
        Purchase.new(related: salepoint2)
      ])
    end

    it "calls sns notification when salepoint or store automator is added for multiple albums" do
      person = create(:person)
      album1 = create(:album)
      album2 = create(:album)
      album3 = create(:album)
      store1 = create(:store)
      store2 = create(:store)
      salepoint_subscription = create(:salepoint_subscription, album: album3)
      salepoint1 = Salepoint.new(salepointable: album1, store: store1)
      salepoint2 = Salepoint.new(salepointable: album1, store: store2)
      salepoint3 = Salepoint.new(salepointable: album2, store: store1)
      salepoint4 = Salepoint.new(salepointable: album2, store: store2)

      allow(FeatureFlipper).to receive(:show_feature?).with(:use_sns, person).and_return(true)

      expect(Salepoint).to receive(:send_sns_notification).with({
        album1.id => [store1.id, store2.id],
        album2.id => [store1.id, store2.id],
        album3.id => []
      })

      Product.process_purchase(person, [
        Purchase.new(related: salepoint_subscription),
        Purchase.new(related: salepoint1),
        Purchase.new(related: salepoint2),
        Purchase.new(related: salepoint3),
        Purchase.new(related: salepoint4)
      ])
    end
  end

  describe '#initialize' do
    it "has a nil renewal_product" do
      product = Product.new

      expect(product.renewal_product).to eq(nil)
    end
  end

  describe "#set_current_targeted_product" do
    it "should set and reset current targeted product" do
      product        = TCFactory.build(:product)
      targeted_offer = TCFactory.build(:targeted_offer)

      tp = TargetedProduct.new(:product=>product, :targeted_offer=>targeted_offer)

      product.send(:set_targeted_product,tp)
      expect(product.current_targeted_product).to eq(tp)

      product.send(:set_targeted_product,nil)
      expect(product.current_targeted_product).to eq(nil)
    end
  end

  describe "#get_preorder_product_by_country_website_id" do
    it "every country returns correct product code" do
      country_websites = CountryWebsite.all
      country_websites.each do |country|
        expected_product = Product.find_by(id: Product::PREORDER_CURRENCY_MAP[country.id])
        next if expected_product.blank?

        expect(Product.get_preorder_product_by_country_website_id(country.id)).to eq(expected_product)
      end
    end
  end

  describe "#find_products_for_country" do
    context "when it has DE products" do
      subject { Product.find_products_for_country("DE", :distribution) }

      it "is array of distribution ids" do
        expect(subject).to be_a Array
      end

      it "contains valid product id's" do
        subject.each do |s|
          expect(Product.where(id: s)).not_to be_empty
        end
      end
    end
  end

  describe "#is_pub_admin_product?" do
    context "product has id of one of the publishing product list" do
      subject { Product.find(Product::SONGWRITER_SERVICE_ID).is_pub_admin_product? }

      it { should be_truthy }
    end

    context "product does not have id of the publishing product list" do
      subject { Product.find(Product::US_YTM).is_pub_admin_product? }

      it { should be_falsey }
    end
  end

  describe "#is_ytm_product?" do
    context "product has id of one of the YTM products list" do
      subject { Product.find(Product::US_YTM).is_ytm_product? }

      it { should be_truthy }
    end

    context "product does not have id of the YTM products list" do
      subject { Product.find(Product::US_BOOKLET).is_ytm_product? }

      it { should be_falsey }
    end
  end

  describe "#flag_text_safely_translated" do
    describe "when a product has flag_text" do
      describe "when that flag_text has no translation" do
        it "returns the original flag_text value" do
          flag_text = "untranslateable"

          product = create(:product, flag_text: flag_text)

          expect(product.flag_text_safely_translated).to eq(flag_text)
        end
      end

      describe "when that flag_text has a translation" do
        it "returns the translated flag_text value" do
          flag_text = "save 20"
          translated_value = "Save 20%"

          product = create(:product, flag_text: flag_text)

          expect(product.flag_text_safely_translated).to eq(translated_value)
        end
      end

      describe "when a product has no flag_text" do
        it "returns nil" do
          flag_text = nil

          product = create(:product, flag_text: flag_text)

          expect(product.flag_text_safely_translated).to eq(nil)
        end
      end
    end
  end

  describe "#country" do
    it "should the products associated country code" do
      product = create(:product)

      expect(product.country).to eq("US")
    end
  end

  describe "#india_product?" do
    it "should return true for India products" do
      product = create(:product)
      product.country_website = CountryWebsite.find_by(country: 'IN')

      expect(product.india_product?).to eq(true)
    end

    it "should return false for non-India products" do
      product = create(:product)

      expect(product.india_product?).to eq(false)
    end
  end

  describe "#automator_product" do
    let(:current_user) { create(:person, country_website_id: CountryWebsite.find_by(country: "US")) }
    let(:automator_product) {  Product.automator_product(current_user) }

    context "when feature_gating is live" do
      before do
        allow(FeatureFlipper).to receive(:show_feature?).with(:plans_pricing, current_user).and_return(true)
      end

      context "when user has an eligible plan or has an eligible plan in the cart" do
        before do
          allow(current_user).to receive(:owns_or_carted_plan?).and_return(true)
          allow(current_user).to receive(:cart_plan).and_return(Plan.find(Plan::BREAKOUT_ARTIST))
        end

        it "returns a free store automator product for user's country" do

          expect(automator_product).to have_attributes(price: 0, name: "Store Automator", country_website_id:  current_user.country_website_id)
        end
      end

      context "when user does not have a plan" do

        it "returns a paid store automator product for user's country" do
          expect(automator_product).to have_attributes(price: 10, name: "Store Automator", country_website_id:  current_user.country_website_id)
        end
      end
    end

    context "when feature_gating is off" do
      context "when user does not have a plan" do

        it "returns a paid store automator product for user's country" do
          expect(automator_product).to have_attributes(price: 10, name: "Store Automator", country_website_id:  current_user.country_website_id)
        end
      end

      context "when user has a plan or has a plan in the cart" do
        before do
          allow(current_user).to receive(:owns_or_carted_plan?).and_return(true)
          allow(current_user).to receive(:cart_plan).and_return(Plan.find(Plan::BREAKOUT_ARTIST))
        end

        it "returns a paid store automator product for user's country" do
          expect(automator_product).to have_attributes(price: 10, name: "Store Automator", country_website_id:  current_user.country_website_id)
        end
      end
    end
  end

  def mock_album
    klass = double("class")
    @album = double("album")
    @songs = double("songs")
    @salepoints = double("salepoints")

    allow(@songs).to receive(:count).and_return(0)
    allow(@salepoints).to receive(:count).and_return(0)
    allow(klass).to receive(:name).and_return("Album")
    allow(@album).to receive(:class).and_return(klass)
    allow(@album).to receive(:salepoints).and_return(@salepoints)
    allow(@album).to receive(:songs).and_return(@songs)
    allow(@album).to receive(:booklet).and_return(nil)
  end

  def build_flat_rate_ad_hoc_album_product(flat_price = 29.99)
    @product = Product.find(Product::US_BOOKLET)
  end
end
