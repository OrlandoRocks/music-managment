require "rails_helper"

describe PriorityArtist do
  include ActiveSupport::Testing::TimeHelpers
  let(:person) { create(:person) }

  describe "validate_or_reset_artist" do

    context "person has an active priority_artist" do
      let!(:album) { create(:album, :paid, :with_songs, :approved , person: person) }
      let!(:priority_artist) { create(:priority_artist, artist: album.artist, person: person) }

      it "returns itself" do
        expect(priority_artist.validate_or_reset_artist).to eq(priority_artist)
      end
    end

    context "person has an inactive priority_artist" do
      let!(:taken_down_album) { create(:album, :paid, :with_songs, :taken_down, person: person) }
      let!(:priority_artist) { create(:priority_artist, artist: taken_down_album.artist, person: person) }

      context "and no other artists" do
        it "destroys the priority_artist entry" do
          expect{ priority_artist.validate_or_reset_artist }.to change(PriorityArtist, :count).by(-1)
        end
      end

      context "person has another active artist" do
        let!(:album) { create(:album, :paid, :with_songs, :approved , person: person) }

        it "resets the priority_artist entry to the valid priority artist" do
          expect(person.priority_artist.artist).to eq(taken_down_album.artist)
          expect{ priority_artist.validate_or_reset_artist }.to change(PriorityArtist, :count).by(0)

          person.reload
          expect(person.priority_artist.artist).to eq(album.artist)
        end
      end
    end
  end
end
