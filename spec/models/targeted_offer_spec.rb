require "rails_helper"

describe TargetedOffer do

  describe "#add_people_from_criteria" do

    before(:each) do
      @targeted_offer   = create(:targeted_offer, status: "Inactive")
      @targeted_product = create(:targeted_product, targeted_offer: @targeted_offer, product: Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID))
      @person1          = create(:person)
      @person2          = create(:person)
    end

    it "should require offer to be inactive" do
      expect(@targeted_offer.add_people_from_criteria).to eq(true)

      expect(@targeted_offer.update(status: "Active")).to eq(true)
      expect(@targeted_offer.add_people_from_criteria).to eq(false)
    end

    it "should require existing customer offer" do
      @targeted_offer.update(offer_type: "new")
      expect(@targeted_offer.add_people_from_criteria).to eq(false)
    end

    context "when rebuilding" do

      it "should clear targeted people and add correct number of people" do
        create(:targeted_person, targeted_offer: @targeted_offer, person: @person1)
        create(:targeted_person, targeted_offer: @targeted_offer, person: @person2)
        expect(@targeted_offer.reload.targeted_population_count).to eq(2)

        @targeted_offer.customers_to_add = 1
        expect(@targeted_offer.add_people_from_criteria(true)).to eq(true)

        expect(@targeted_offer.reload.targeted_population_count).to eq(1)
      end

      it "should not delete people who have used the targeted offer" do
        create(:targeted_person, person: @person1, targeted_offer: @targeted_offer, usage_count: 1)
        create(:targeted_person, person: @person2, targeted_offer: @targeted_offer, usage_count: 0)

        expect(@targeted_offer.reload.targeted_population_count).to eq(2)
        @targeted_offer.customers_to_add = 0
        expect(@targeted_offer.add_people_from_criteria(true)).to eq(true)

        expect(@targeted_offer.reload.targeted_population_count).to eq(1)
        expect(@targeted_offer.targeted_people.select{ |tp| tp.person_id == @person1.id}).not_to be_empty
        expect(@targeted_offer.targeted_people.select{ |tp| tp.person_id == @person2.id}).to be_empty
      end

    end

    context "when adding" do

      it "should not clear targeted people and not add the same people" do
        @targeted_offer.set_population_count_from_criteria
        @targeted_offer.customers_to_add = @targeted_offer.population_criteria_count
        expect(@targeted_offer.add_people_from_criteria).to eq(true)
        expect(@targeted_offer.reload.targeted_population_count).to eq(@targeted_offer.population_criteria_count)

        @targeted_offer.customers_to_add = 1
        expect(@targeted_offer.add_people_from_criteria).to eq(true)
        expect(@targeted_offer.reload.targeted_population_count).to eq(@targeted_offer.population_criteria_count)

        expect(@targeted_offer.targeted_people.select{ |tp| tp.person_id == @person1.id}).not_to be_empty
        expect(@targeted_offer.targeted_people.select{ |tp| tp.person_id == @person2.id}).not_to be_empty
      end

      it "should not blow up when trying to add too much" do
        @targeted_offer.set_population_count_from_criteria
        @targeted_offer.customers_to_add = @targeted_offer.population_criteria_count + 1
        expect(@targeted_offer.add_people_from_criteria).to eq(true)
        expect(@targeted_offer.reload.targeted_population_count).to eq(@targeted_offer.population_criteria_count)
      end
    end
  end

  describe "#can_destroy?" do

    before(:each) do
      @targeted_offer = create(:targeted_offer, status: "Inactive")
      create(:targeted_product, targeted_offer: @targeted_offer)
      @targeted_offer.update(status: "Active")
    end

    it "should not delete active offers" do
      #Can never delete active offers
      @targeted_offer.status = "Active"
      expect(@targeted_offer.can_destroy?).to eq(false)

      @targeted_offer.status = "New"
      expect(@targeted_offer.can_destroy?).to eq(true)

      @targeted_offer.status = "Inactive"
      expect(@targeted_offer.can_destroy?).to eq(true)
    end

    it "should delete according to status rules if offer has only people that never used" do
      #Can delete according to status rules if the offer has people that have never used
      person = create(:person)
      expect(person.valid?).to eq(true)

      create(:targeted_person, person: person, targeted_offer: @targeted_offer)
      @targeted_offer.status = "Active"
      expect(@targeted_offer.can_destroy?).to eq(false)

      @targeted_offer.status = "New"
      expect(@targeted_offer.can_destroy?).to eq(true)

      @targeted_offer.status = "Inactive"
      expect(@targeted_offer.can_destroy?).to eq(true)
    end

    it "should not delete if the offer has anyone that's used it" do

      person = create(:person)
      expect(person.valid?).to eq(true)

      create(:targeted_person, person: person, targeted_offer: @targeted_offer, usage_count: 1)

      @targeted_offer.status = "Active"
      expect(@targeted_offer.can_destroy?).to eq(false)

      @targeted_offer.status = "New"
      expect(@targeted_offer.can_destroy?).to eq(false)

      @targeted_offer.status = "Inactive"
      expect(@targeted_offer.can_destroy?).to eq(false)
    end
  end

  describe "#destroy" do

    before(:each) do
      @targeted_offer = create(:targeted_offer, status: "Inactive")
      create(:targeted_product, targeted_offer: @targeted_offer)
      @targeted_offer.update(status: "Active")
    end

    it "should allow deletion of a never activated targeted offer" do
      @targeted_offer.update(:status => "New")
      expect(@targeted_offer.destroy).not_to be_nil
    end

    it "should allow deletion of a never used targeted offer if it is new" do
      create(:targeted_person, targeted_offer: @targeted_offer, person: create(:person))
      @targeted_offer.update(:status => "New")
      expect(@targeted_offer.destroy).not_to be_nil
    end

    it "should allow deletion of a never used targeted offer it is in inactive" do
      @targeted_offer.update(:status => "Inactive")
      expect(@targeted_offer.destroy).not_to be_nil
    end

    it "should not allow deletion of a targeted offer that is activated" do
      create(:targeted_person, targeted_offer: @targeted_offer, person: create(:person))
      @targeted_offer.update(:status => "Active")
      expect(@targeted_offer.destroy).to be_nil
    end

    it "should not allow deletion of a targeted offer that has been used regardless of its status" do
      person = create(:person)
      create(:targeted_person, person: person, targeted_offer: @targeted_offer, usage_count: 1)

      @targeted_offer.update(:status => "Active")
      expect(@targeted_offer.destroy).to be_nil

      @targeted_offer.update(:status => "Inactive")
      expect(@targeted_offer.destroy).to be_nil
    end
  end

  describe "#between_start_and_expiration?" do

    before(:each) do
      @targeted_offer = build_stubbed(:targeted_offer, start_date: "2012-02-01", expiration_date: "2012-02-05")
    end

    it "should not include any dates before the start date" do
      allow(Time).to receive(:now).and_return(Time.parse("2012-01-31 23:59:59"))
      expect(@targeted_offer.send(:between_start_and_expiration?)).to eq(false)

      allow(Time).to receive(:now).and_return(Time.parse("2012-01-31 00:00:00"))
      expect(@targeted_offer.send(:between_start_and_expiration?)).to eq(false)
    end

    it "should include all of start date" do
      allow(Time).to receive(:now).and_return(Time.parse("2012-02-01 00:00:00"))
      expect(@targeted_offer.send(:between_start_and_expiration?)).to eq(true)

      allow(Time).to receive(:now).and_return(Time.parse("2012-02-01 23:59:59"))
      expect(@targeted_offer.send(:between_start_and_expiration?)).to eq(true)
    end

    it "should include all of expiration date" do
      allow(Time).to receive(:now).and_return(Time.parse("2012-02-05 00:00:00"))
      expect(@targeted_offer.send(:between_start_and_expiration?)).to eq(true)

      allow(Time).to receive(:now).and_return(Time.parse("2012-02-05 23:59:59"))
      expect(@targeted_offer.send(:between_start_and_expiration?)).to eq(true)
    end

    it "should not include any dates after 11:59:59 of the expiration date" do
      allow(Time).to receive(:now).and_return(Time.parse("2012-02-06 00:00:01"))
      expect(@targeted_offer.send(:between_start_and_expiration?)).to eq(false)

      allow(Time).to receive(:now).and_return(Time.parse("2012-02-06 23:59:59"))
      expect(@targeted_offer.send(:between_start_and_expiration?)).to eq(false)
    end
  end

  describe "#build_population_criteria" do

    before(:each) do
      @expiration_to = create(:targeted_offer, country_website_id: 2, country: "CA", status: "Inactive")
      @join_plus_to  = create(:targeted_offer, date_constraint: "join plus", join_plus_duration: 2, country_website_id: 2, country: "CA", status: "Inactive")
    end

    context "when there is a join plus targeted offer with the same products as an expiration offer" do

      before(:each) do
        @product1 = Product.find(Product::CA_ONE_YEAR_ALBUM_PRODUCT_ID)
        @product2 = Product.find(Product::CA_TWO_YEAR_ALBUM_PRODUCT_ID)

        @join_plus_to.targeted_products.create(product: @product1)
        @join_plus_to.targeted_products.create(product: @product2)

        @expiration_to.targeted_products.create(product: @product1)
        @expiration_to.targeted_products.create(product: @product2)

      end

      it "should not include people already in a join plus offer" do
        @join_plus_to.update(status: "Active")
        @expiration_to.update(status: "Active")

        #Get original population count
        @expiration_to.set_population_count_from_criteria
        expect(@expiration_to.population_criteria_count).to eq(0)
        pop_count = @expiration_to.population_criteria_count

        allow(CanadaPostalCode).to receive(:find_by_code).and_return(true)
        @person1 = create(:person, country: "CA", postal_code: "abcdef")
        @person2 = create(:person, country: "CA", postal_code: "abcdef")

        #Make sure they fall under this population count
        @expiration_to.set_population_count_from_criteria
        expect(@expiration_to.population_criteria_count).to be > 0
        expect(@expiration_to.population_criteria_count).to be pop_count + 2
        pop_count = @expiration_to.population_criteria_count

        #Add the two peeps to the join plus
        create(:targeted_person, person: @person1, targeted_offer: @join_plus_to)
        create(:targeted_person, person: @person2, targeted_offer: @join_plus_to)
        expect(@join_plus_to.reload.targeted_people.size).to be > 0

        #Get the new count
        @expiration_to.set_population_count_from_criteria

        @expiration_to.population_criteria_count
        expect(@expiration_to.population_criteria_count).to be pop_count - 2

      end

    end

    context "when there is a person that is in a targeted offer that should be excluded and in one that is unrelated" do

      before(:each) do
        @related_to = create(:targeted_offer, country_website_id: 2, country: "CA", status: "New")

        @product1 = Product.find(Product::CA_ONE_YEAR_ALBUM_PRODUCT_ID)
        @product2 = Product.find(Product::CA_TWO_YEAR_ALBUM_PRODUCT_ID)

        #Expiration and related to's are for the same product, join_plus is a diff product
        @expiration_to.targeted_products.create(product: @product1)
        @related_to.targeted_products.create(product: @product1)
        @join_plus_to.targeted_products.create(product: @product2)

        @join_plus_to.update(status: "Active")
        @expiration_to.update(status: "Active")
        @related_to.update(status: "Active")

        allow(CanadaPostalCode).to receive(:find_by_code).and_return(true)
        @person1 = create(:person, country: "CA", postal_code: "abcdef")
        @person2 = create(:person, country: "CA", postal_code: "abcdef")

        @expiration_to.set_population_count_from_criteria
        expect(@expiration_to.population_criteria_count).to eq(2)

        #Add person 2 to unrelated to
        @join_plus_to.targeted_people.create(person: @person2)
        @expiration_to.set_population_count_from_criteria
        expect(@expiration_to.population_criteria_count).to eq(2)
      end

      it "should not include this person" do
        #Add to related targeted offer
        @related_to.targeted_people.create(person: @person2)
        @expiration_to.set_population_count_from_criteria
        expect(@expiration_to.reload.population_criteria_count).to eq(1)
      end
    end

  end

  describe "#active_offers_for_person_and_products" do

    context "using a join plus targeted offer" do

      before(:each) do
        # Create join + 5 days Targeted offer
        @today          = Time.now
        @targeted_offer = create(:targeted_offer, offer_type: 'new', start_date: @today.to_date, expiration_date: @today.to_date+30.days , date_constraint: "join plus", join_plus_duration: 5, status: "New" )
        @product        = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
        @products       = []
        @products      << @product
        create(:targeted_product, targeted_offer: @targeted_offer, product: @product)
        @targeted_offer.update(status: "Active")
        @person         = create(:person)
        create(:targeted_person, targeted_offer: @targeted_offer, person: @person)
      end

      it "should not include if they joined longer than the join duration ago" do
        @person.update_attribute("created_on", @today - 6.days)
        expect(TargetedOffer.active_offers_for_person_and_products(@person, @products)).to be_empty
      end

      it "should include if they joined the join duration ago" do
        @person.update_attribute("created_on", @today - 5.days)
        expect(TargetedOffer.active_offers_for_person_and_products(@person.reload, @products)).not_to be_empty
      end

      it "should include if they joined sometime within the join duration ago" do
        @person.update_attribute("created_on", @today - 3.days)
        expect(TargetedOffer.active_offers_for_person_and_products(@person.reload, @products)).not_to be_empty
      end

      it "should not if they joined today" do
        @person.update_attribute("created_on", @today)
        expect(TargetedOffer.active_offers_for_person_and_products(@person.reload, @products)).not_to be_empty
      end
    end

    context "using an expiration targeted offer" do

      before(:each) do
        @today = Time.now
        @targeted_offer = create(:targeted_offer, offer_type: 'new', start_date: "2012-02-01", expiration_date: "2012-02-05", date_constraint: "expiration", status: "New" )
        @product        = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
        @products       = [@product]
        create(:targeted_product, targeted_offer: @targeted_offer, product: @product)
        @targeted_offer.update(status: "Active")

        @person         = create(:person)
        create(:targeted_person, targeted_offer: @targeted_offer, person: @person)
      end

      it "should not include any dates before the start date" do
        @targeted_offer.update!(start_date: (@today + 1.day), expiration_date: (@today + 5.days))
        expect(TargetedOffer.active_offers_for_person_and_products(@person, @products)).to be_empty
      end

      it "should include all of start date" do
        @targeted_offer.update!(start_date: @today, expiration_date: (@today + 5.days))
        expect(TargetedOffer.active_offers_for_person_and_products(@person, @products)).not_to be_empty
      end

      it "should include all of expiration date" do
        @targeted_offer.update!(start_date: (@today - 5.days), expiration_date: @today)
        expect(TargetedOffer.active_offers_for_person_and_products(@person, @products)).not_to be_empty
      end

      it "should not include any dates after the expiration date" do
        @targeted_offer.update!(start_date: (@today - 5.days), expiration_date: (@today - 1.day))
        expect(TargetedOffer.active_offers_for_person_and_products(@person, @products)).to be_empty
      end
    end
  end

  describe "#dup" do
    before(:each) do
      Timecop.travel(1.day.ago) do
        @targeted_offer = create(
          :targeted_offer,
          :offer_type => "new",
          start_date: "2012-02-01",
          :join_token=>"Hello World",
          expiration_date: "2012-02-05",
          :population_criteria_count=>123,
          :targeted_population_count=>321,
          :population_cap=>123,
          status: "New",
          :job_status=>"Test"
        )

        create(:targeted_product, targeted_offer: @targeted_offer)
        @targeted_offer.update(status: "Active")

        TargetedAdvertisement.create(targeted_offer: @targeted_offer, advertisement: TCFactory.create(:advertisement))

        create(:targeted_person, targeted_offer: @targeted_offer, person: create(:person))
      end

      @new_to = @targeted_offer.reload.dup
    end

    it "should clone correct attributes" do

      #Assert that we replicate right attributes
      should_not_clone = [:id,:join_token, :population_criteria_count, :targeted_population_count, :updated_at, :created_at, :status, :name, :job_status ]
      TargetedOffer.column_names.each do |column|
        if should_not_clone.include? column.to_sym
          expect(@new_to.send(column.to_sym)).not_to eq(@targeted_offer.send(column.to_sym))
        else
          expect(@new_to.send(column.to_sym)).to eq(@targeted_offer.send(column.to_sym))
        end
      end
    end

    it "should add 'cloned' to the name" do
      expect(@new_to.name).to eq("#{@targeted_offer.name} - cloned")
    end

    it "should clone targeted products" do
      #Assert that we copy products
      @targeted_offer.targeted_products.each do |tp|
        expect(@new_to.targeted_products.select{ |tp2| tp2.product_id == tp.product_id }.first.new_record?).to be_falsey
      end

      expect(@targeted_offer.targeted_products.size).to eq(@new_to.targeted_products.size)
    end

    it "should clone targeted advertisements" do
      #Assert that we copy advertisements
      @targeted_offer.targeted_advertisements.each do |ta|
       expect(@new_to.targeted_advertisements.select{ |ta2| ta2.advertisement_id == ta.advertisement_id }).not_to be_empty
      end

      expect(@targeted_offer.targeted_advertisements.size).to eq(@new_to.targeted_advertisements.size)
    end

    it "should not clone targeted people" do
      expect(@targeted_offer.targeted_people.reload.length).to be > 0
      expect(@new_to.targeted_people.reload.length).to eq(0)
    end
  end

  describe "#scheduled_or_processing?" do
    before(:each) do
      @to = build_stubbed(:targeted_offer)
    end

    it "should return true when scheduled" do
      @to.job_status = TargetedOffer::ADD_PEOPLE_SCHEDULED_STATUS
      expect(@to.scheduled_or_processing?).to eq(true)

      @to.job_status = TargetedOffer::REMOVE_PEOPLE_SCHEDULED_STATUS
      expect(@to.scheduled_or_processing?).to eq(true)
    end

    it "should true false when processing" do
      @to.job_status = TargetedOffer::ADD_PEOPLE_PROCESSING_STATUS
      expect(@to.scheduled_or_processing?).to eq(true)

      @to.job_status = TargetedOffer::REMOVE_PEOPLE_PROCESSING_STATUS
      expect(@to.scheduled_or_processing?).to eq(true)
    end

    it "should return false when not scheduled or processing" do
      expect(@to.scheduled_or_processing?).to eq(false)
    end

  end

  describe "#schedule_add_people" do

    before(:each) do
      @to   = create(:targeted_offer)
      @user = double("user")
      allow(@user).to receive(:id).and_return(123)
    end

    it "should schedule if no job is running or scheduled for the targeted offer" do
      expect(TargetedOffer::AddTargetedPeopleWorker).to receive(:perform_async)
      expect(@to.schedule_add_people_job(@user)).to eq(true)
      expect(@to.job_status).to eq(TargetedOffer::ADD_PEOPLE_SCHEDULED_STATUS)
    end

    it "should not schedule if a job is running or scheduled for the targeted offer" do
      expect(TargetedOffer::AddTargetedPeopleWorker).not_to receive(:perform_async)
      @to.job_status = TargetedOffer::ADD_PEOPLE_SCHEDULED_STATUS
      expect(@to.schedule_add_people_job(@user)).to eq(false)
    end

  end

  describe "#schedule_remove_people" do

    before(:each) do
      @to   = create(:targeted_offer)
      allow(FileUtils).to receive(:cp).and_return(true)
      @user = double("user")
      allow(@user).to receive(:id).and_return(123)
    end

    it "should schedule if no job is running or scheduled for the targeted offer" do
      expect(TargetedOffer::RemoveTargetedPeopleWorker).to receive(:perform_async)
      expect(@to.schedule_remove_people_job(@user)).to eq(true)
      expect(@to.job_status).to eq(TargetedOffer::REMOVE_PEOPLE_SCHEDULED_STATUS)
    end

  end

  describe "#schedule_change_activation" do

   before(:each) do
      @to   = create(:targeted_offer)
      @user = double("user")
      allow(@user).to receive(:id).and_return(123)
    end

    it "should schedule activation if no job is running or scheduled for the targeted offer" do
      expect(TargetedOffer::ChangeActivationWorker).to receive(:perform_async).with(@to.id, true, @user.id)
      expect(@to.schedule_change_activation_job(true, @user)).to eq(true)
      expect(@to.job_status).to eq(TargetedOffer::CHANGE_ACTIVATION_SCHEDULED_STATUS)
    end

    it "should schedule deactivation if no job is running or scheduled for the targeted offer" do
      expect(TargetedOffer::ChangeActivationWorker).to receive(:perform_async).with(@to.id, false, @user.id)
      expect(@to.schedule_change_activation_job(false, @user)).to eq(true)
      expect(@to.job_status).to eq(TargetedOffer::CHANGE_ACTIVATION_SCHEDULED_STATUS)
    end

    it "should not schedule activation if a job is running or scheduled for the targeted offer" do
      expect(TargetedOffer::ChangeActivationWorker).not_to receive(:perform_async)
      @to.job_status = TargetedOffer::ADD_PEOPLE_SCHEDULED_STATUS
      expect(@to.schedule_remove_people_job(@user)).to eq(false)
    end

    it "should not schedule deactivation if a job is running or scheduled for the targeted offer" do
      expect(TargetedOffer::ChangeActivationWorker).not_to receive(:perform_async)
      @to.job_status = TargetedOffer::ADD_PEOPLE_SCHEDULED_STATUS
      expect(@to.schedule_remove_people_job(@user)).to eq(false)
    end
  end

  describe "#grab_list_from_s3" do
    it "calls the aws sdk wrapper with the filename as the key" do
      targeted_offer = build_stubbed(:targeted_offer, id: 1)
      identifier = "add"
      csv = "asdf,aksdjf,fkalsdf\niofnasd,asoidjfasd,aisdojf"

      expect(AwsWrapper::S3).to receive(:read).with(
        bucket: TARGETED_OFFER_BUCKET_NAME,
        key: "#{targeted_offer.id}_#{identifier}_targeted_offer.csv",
      ).and_return(csv)

      expect(targeted_offer.grab_list_from_s3(identifier)).to eq(csv.split("\n"))
    end
  end

  describe "targeted offer exclusions" do

    describe "when destroying targeted offers" do

      before(:each) do
        @to1 = create(:targeted_offer, status: "Inactive")
        @to2 = create(:targeted_offer, status: "Inactive")

        expect(TargetedOfferExclusion.count).to eq(0)

        expect(@to1.targeted_offer_exclusions.create(:exclude_targeted_offer=>@to2)).to be_valid

        expect(TargetedOfferExclusion.count).to be > 0
      end

      it "should clean up excluded targeted offers" do
        expect(@to1.can_destroy?).to eq(true)
        @to1.destroy
        expect(TargetedOfferExclusion.count).to eq(0)
      end

      it "should clean up targeted offers excluded in" do
        expect(@to2.can_destroy?).to eq(true)
        @to2.destroy
        expect(TargetedOfferExclusion.count).to eq(0)
      end

    end

    context "when targeted offer has exclusions" do

      before(:each) do
        @targeted_offer  = create(:targeted_offer, country_website_id: 2, country: "CA", status: "New")
        @targeted_offer2 = create(:targeted_offer, country_website_id: 2, country: "CA", status: "New")

        product1 = Product.find(Product::CA_ONE_YEAR_ALBUM_PRODUCT_ID)
        product2 = Product.find(Product::CA_TWO_YEAR_ALBUM_PRODUCT_ID)
        create(:targeted_product, targeted_offer: @targeted_offer, product: product1)
        @targeted_offer2.targeted_products.create(product: product2)

        @targeted_offer.update(status: "Active")
        @targeted_offer2.update(status: "Active")

        @people = []
        #Add people to to1
        @num_people = 5

        allow(CanadaPostalCode).to receive(:find_by_code).and_return(true)
        @num_people.times do
          @people << create(:person, country: "CA", postal_code: "H3Z2Y7")
        end

        #Insure that all people are to be included in the targeted offer
        @targeted_offer.set_population_count_from_criteria
        expect(@targeted_offer.population_criteria_count).to eq(@num_people)

        #Exclude targeted offer 2
        @targeted_offer.targeted_offer_exclusions.create(:exclude_targeted_offer_id=>@targeted_offer2.id)

        #add people to targeted offer 2
        @num_people_in_to2 = @num_people / 2
        @num_people_in_to2.times do |i|
          expect(@targeted_offer2.targeted_people.create(person: @people[i])).to be_valid
        end
      end

      it "should exclude people in the excluded targeted offer" do
        @targeted_offer.set_population_count_from_criteria
        expect(@targeted_offer.population_criteria_count).to eq(@num_people - @num_people_in_to2)
      end

    end
  end

  describe "#ready_to_activate?" do
    let(:targeted_offer) { build(:targeted_offer) }

    subject { targeted_offer.ready_to_activate? }

    context "when status is ready" do
      before { allow(targeted_offer).to receive(:new?).and_return(true) }

      context "when targeted products is present" do
        before { allow(targeted_offer).to receive(:targeted_products).and_return([1]) }

        context "when offer type is not existing" do
          before { allow(targeted_offer).to receive(:offer_type_existing?).and_return(false) }

          it { should be_truthy }
        end

        context "when offer type is existing" do
          before { allow(targeted_offer).to receive(:offer_type_existing?).and_return(true) }

          context "population criteria is prepared" do
            before { allow(targeted_offer).to receive(:population_criteria_ready?).and_return(true) }

            it { should be_truthy }
          end

          context "population criteria is not prepared" do
            before { allow(targeted_offer).to receive(:population_criteria_ready?).and_return(false) }

            it { should be_falsey }
          end
        end
      end

      context "when targeted products is blank" do
        before { allow(targeted_offer).to receive(:targeted_products).and_return(nil) }

        it { should be_falsey }
      end
    end

    context "when status is not of ready state" do
      before { targeted_offer.status = 'Active' }

      it { should be_falsey }
    end
  end
end

describe TargetedOffer, "when activating/deactivating an offer" do

  before(:each) do

    @targeted_offer   = create(:targeted_offer, status: "Inactive")
    @product          = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
    @targeted_product = create(:targeted_product, targeted_offer: @targeted_offer, product: @product, :price_adjustment=>10, :price_adjustment_type=>'percentage_off')

    @person = create(:person)

    #Create ready to purchase album
    album = create(:album,
                              :person => @person,
                              :known_live_on => nil,
                              :finalized_at => nil)

    store = Store.first
    if store.variable_prices.empty?
      salepoint = create(:salepoint, :salepointable => album, :store => store, :has_rights_assignment => true)
    else
      salepoint = create(:salepoint, :salepointable => album, :store => store, :has_rights_assignment => true, :variable_price_id => store.variable_prices.first.id)
    end

    song1 = create(:song, :album => album)
    song2 = create(:song, :album => album)

    upload1 = create(:upload, :song => song1)
    upload2 = create(:upload, :song => song2)

    song1.save
    song2.save

    @purchase       = Product.add_to_cart(@person, album, @product)
    @original_price = @purchase.cost_cents

    @targeted_offer.targeted_people.create(person: @person)

    expect(@original_price).to eq(@purchase.reload.cost_cents)
  end

  describe "Activating" do

    it "should update the users purchases" do
      @targeted_offer.update(status: "Active")

      expect(@person.purchases.reload.first.discount_cents).to be > 0
      expect(@person.purchases.reload.first.targeted_product).to eq(@targeted_product)
    end

    it "should set the purchases targeted_product_id" do
      @targeted_offer.update(status: "Active")

      expect(@person.purchases.reload.first.targeted_product).to eq(@targeted_product)
    end

  end

  describe "Deactivating" do

    it "should update the users purchases" do
      @targeted_offer.update(status: "Active")
      @targeted_offer.update(status: "Inactive")

      expect(@person.purchases.first.discount_cents).to eq(0)
    end

    it "should nulify the purchases targeted_product_id" do
      @targeted_offer.update(status: "Active")
      @targeted_offer.update(status: "Inactive")

      expect(@person.purchases.first.targeted_product).to be nil
    end
  end

end

describe TargetedOffer, "when finding a targeted offer for a person and item" do
  before(:each) do
    build_flat_rate
  end

  it "should return an offer with a product that corresponds to the item provided" do
    targeted_offer = create(:targeted_offer, {:start_date => Time.now - 40.days, :expiration_date => Time.now + 10.days, :offer_type => "existing", status: "New"})
    person = Person.first
    tp = create(:targeted_product, {:product => @product, :targeted_offer => targeted_offer, :price_adjustment => 10.00, :price_adjustment_type => "dollars_off"})
    targeted_offer.update(status: "Active")

    create(:targeted_person, {:person => person, :targeted_offer => targeted_offer})
    expect(TargetedOffer.for_person_and_item(person, Album.new)).to eq(targeted_offer)
  end

  it "should not return an Inactive Offer" do
    targeted_offer = create(:targeted_offer, {:start_date => Time.now - 40.days, :expiration_date => Time.now - 10.days, :status => "Inactive"})
    create(:targeted_product, {:product => @product, :targeted_offer => targeted_offer, :price_adjustment => 10.00})
    create(:targeted_person, {:person => Person.first, :targeted_offer => targeted_offer})

    expect(TargetedOffer.for_person_and_item(Person.first, Album.new)).to eq(nil)
  end

  it "should not return an expired offer if the date_constraint is set to 'expiration'" do
    targeted_offer = create(:targeted_offer, {:start_date => Time.now - 40.days, :expiration_date => Time.now - 10.days, status: "New"})
    create(:targeted_product, {:product => @product, :targeted_offer => targeted_offer, :price_adjustment => 10.00})
    create(:targeted_person, {:person => Person.first, :targeted_offer => targeted_offer})
    targeted_offer.update(status: "Active")

    expect(TargetedOffer.for_person_and_item(Person.first, Album.new)).to eq(nil)
  end

  it "should not return an offer for a new customer if they are past the join plus setting" do
    person = Person.first
    person.created_on = Time.now - 45.days
    person.save

    targeted_offer = create(:targeted_offer, {:start_date => Time.now - 40.days, :expiration_date => Time.now - 10.days, :date_constraint => "join plus", :join_plus_duration => 30, status: "New"})
    create(:targeted_product, {:product => @product, :targeted_offer => targeted_offer, :price_adjustment => 10.00})
    targeted_offer.update(status: "Active")

    create(:targeted_person, {:person => person, :targeted_offer => targeted_offer})

    expect(TargetedOffer.for_person_and_item(person, Album.new)).to eq(nil)
  end

  it "should not return an active offer for a customer if they are not in the targeted_offer" do
    targeted_offer = create(:targeted_offer, {:start_date => Time.now - 40.days, :expiration_date => Time.now + 10.days, :date_constraint => "permanent", status: "New"})
    create(:targeted_product, {:product => @product, :targeted_offer => targeted_offer, :price_adjustment => 10.00})
    targeted_offer.update(status: "Active")

    create(:targeted_person, {:person => Person.last, :targeted_offer => targeted_offer})

    expect(TargetedOffer.for_person_and_item(Person.first, Album.new)).to eq(nil)
  end

  it "should be inactive when the status is inactive" do
    targeted_offer = TargetedOffer.new({:status => "Inactive"})
    expect(targeted_offer.active?).to eq(false)
  end

  it "should be active when the status is active and the offer has not expired yet" do
    targeted_offer = TargetedOffer.new({:status => "Active", :start_date => Time.now - 10.days, :expiration_date => Time.now + 10.days})
    expect(targeted_offer.active?).to eq(true)
  end

  it "should be expired for a person if the date_constraint is expiration and the expiration date has passed" do
    targeted_offer = TargetedOffer.new({:status => "Active", :date_constraint => "expiration", :start_date => Time.now - 40.days, :expiration_date => Time.now - 10.days})
    expect(targeted_offer.active_for_person?(Person.first)).to eq(false)
  end

  it "should not be active for a person if the date_constraint is 'join plus' and it has been longer than the specified duration" do
    person = double("person")
    allow(person).to receive(:created_on).and_return(Time.now - 50.days)
    targeted_offer = TargetedOffer.new({:status => "Active", :date_constraint => "join plus", :start_date => Time.now - 50.days, :expiration_date => Time.now, :join_plus_duration => 30})
    expect(targeted_offer.active_for_person?(person)).to eq(false)
  end

  it "should be active if the date_constraint is 'permanent'" do
    targeted_offer = TargetedOffer.new({:status => "Active", :date_constraint => "permanent"})
    expect(targeted_offer.active_for_person?(Person.first)).to eq(true)
  end

  it "should not be expired for a new customer when the date_constraint is 'join plus' and they are within the specified number of days" do
    person = double("person")
    allow(person).to receive(:created_on).and_return(Time.now - 10.days)
    targeted_offer = TargetedOffer.new({:status => "Active", :date_constraint => "join plus", :start_date => Time.now, :expiration_date => Time.now + 30.days, :join_plus_duration => 30})
    expect(targeted_offer.active_for_person?(person)).to eq(true)
  end

  it "should not be expired for a person if the date is within the start and expiration dates" do
    targeted_offer = TargetedOffer.new({:status => "Active", :date_constraint => "expiration", :start_date => Time.now - 10.days, :expiration_date => Time.now + 30.days})
    expect(targeted_offer.active_for_person?(Person.first)).to eq(true)
  end

  context "adding and removing people to an offer" do
    before(:each) do
      @targeted_offer = create(:targeted_offer, status: "New")
      @targeted_product = create(:targeted_product, targeted_offer: @targeted_offer)
      @targeted_offer.update(status: "Active")
      @person = create(:person)
    end

    it "should add/remove a person and increment population count" do
      expect(TargetedPerson.where(:targeted_offer_id => @targeted_offer.id, :person_id => @person.id).first).to be_nil
      expect(@targeted_offer.targeted_population_count).to eq(0)
      @targeted_offer.add_person(@person)
      expect(@targeted_offer.reload.targeted_population_count).to eq(1)
      expect(TargetedPerson.where(:targeted_offer_id => @targeted_offer.id, :person_id => @person.id).first).not_to be_nil
      @targeted_offer.remove_person(@person)
      expect(@targeted_offer.reload.targeted_population_count).to eq(0)
      expect(TargetedPerson.where(:targeted_offer_id => @targeted_offer.id, :person_id => @person.id).first).to be_nil
      @targeted_offer.add_person(@person)
      expect(@targeted_offer.reload.targeted_population_count).to eq(1) #last test needed since it was cache'ing pop_count
    end

    it "should return nil if you try to remove a person who is not in the offer" do
      expect(@targeted_offer.targeted_population_count).to eq(0)
      expect(@targeted_offer.remove_person(@person)).to be_nil
      expect(@targeted_offer.reload.targeted_population_count).to eq(0)
      expect(TargetedPerson.where(:targeted_offer_id => @targeted_offer.id, :person_id => @person.id).first).to be_nil
    end

    it "should not be able to remove a used offer" do
      @targeted_offer.add_person(@person)
      expect(@targeted_offer.targeted_population_count).to eq(1)
      targeted_person = TargetedPerson.where(:targeted_offer_id => @targeted_offer.id, :person_id => @person.id).first
      targeted_person.use!
      expect(@targeted_offer.remove_person(@person)).to be_nil
      expect(@targeted_offer.targeted_population_count).to eq(1)
      expect(TargetedPerson.where(:targeted_offer_id => @targeted_offer.id, :person_id => @person.id).first).not_to be_nil
    end

  end
end

describe TargetedOffer, "when finding a targeted offer for a product, album and stores" do
  context "album stores and targeted product stores are same" do
    it "should return an offer with a product that corresponds to the item provided" do
      targeted_offer = create(:targeted_offer, start_date: Time.now - 40.days, expiration_date: Time.now + 10.days, offer_type: "existing", status: "New")
      product = Product.find(Product::US_ONE_YEAR_SINGLE_PRODUCT_ID)
      tp = create(:targeted_product, product: product, targeted_offer: targeted_offer, price_adjustment: 10.00, price_adjustment_type: "dollars_off")
      targeted_offer.update(status: "Active")

      album = create(:single)
      salepoint    = create(:salepoint, salepointable: album, finalized_at: DateTime.now)
      create(:targeted_product_store, targeted_product: tp, store: salepoint.store)
      fetch_targeted_offer = TargetedOffer.find_offer_by_person_and_item(album.person, album, 'Ad Hoc')
      expect(fetch_targeted_offer).to eq(targeted_offer)
    end

    it "should not return an Inactive Offer" do
      targeted_offer = create(:targeted_offer, start_date: Time.now - 40.days, expiration_date: Time.now - 10.days, status: "Inactive")
      product = Product.find(Product::US_ONE_YEAR_SINGLE_PRODUCT_ID)
      tp = create(:targeted_product, product: product, targeted_offer: targeted_offer, price_adjustment: 10.00, price_adjustment_type: "dollars_off")

      album = create(:single)
      salepoint    = create(:salepoint, salepointable: album, finalized_at: DateTime.now)
      create(:targeted_product_store, targeted_product: tp, store: salepoint.store)
      fetch_targeted_offer = TargetedOffer.find_offer_by_person_and_item(album.person, album, 'Ad Hoc')
      expect(fetch_targeted_offer).to eq(nil)
    end

    it "should not return an expired offer if the date_constraint is set to 'expiration'" do
      targeted_offer = create(:targeted_offer, start_date: Time.now - 40.days, expiration_date: Time.now - 10.days, status: "New")
      product = Product.find(Product::US_ONE_YEAR_SINGLE_PRODUCT_ID)
      tp = create(:targeted_product, product: product, targeted_offer: targeted_offer, price_adjustment: 10.00, price_adjustment_type: "dollars_off")
      targeted_offer.update(status: "Active")

      album = create(:single)
      salepoint    = create(:salepoint, salepointable: album, finalized_at: DateTime.now)
      create(:targeted_product_store, targeted_product: tp, store: salepoint.store)
      fetch_targeted_offer = TargetedOffer.find_offer_by_person_and_item(album.person, album, 'Ad Hoc')
      expect(fetch_targeted_offer).to eq(nil)
    end

    it "should return an offer based on albums stores and targeted product stores" do
      targeted_offer_1 = create(:targeted_offer, start_date: Time.now - 40.days, expiration_date: Time.now + 10.days, offer_type: "existing", status: "New")
      targeted_offer_2 = create(:targeted_offer, start_date: Time.now - 40.days, expiration_date: Time.now + 10.days, offer_type: "existing", status: "New")
      product = Product.find(Product::US_ONE_YEAR_SINGLE_PRODUCT_ID)
      tp1 = create(:targeted_product, product: product, targeted_offer: targeted_offer_1, price_adjustment: 10.00, price_adjustment_type: "dollars_off")
      tp2 = create(:targeted_product, product: product, targeted_offer: targeted_offer_2, price_adjustment: 10.00, price_adjustment_type: "dollars_off")
      targeted_offer_1.update(status: "Active")
      targeted_offer_2.update(status: "Active")

      album = create(:single)
      salepoint1    = create(:salepoint, salepointable: album, finalized_at: DateTime.now, store: Store.find_by(short_name: "Pandora"))
      salepoint2    = create(:salepoint, salepointable: album, finalized_at: DateTime.now, store: Store.find_by(short_name: "M_Island"))
      create(:targeted_product_store, targeted_product: tp2, store: salepoint1.store)
      create(:targeted_product_store, targeted_product: tp2, store: salepoint2.store)
      fetch_targeted_offer = TargetedOffer.find_offer_by_person_and_item(album.person, album, 'Ad Hoc')
      expect(fetch_targeted_offer).to eq(targeted_offer_2)
    end
  end
end

describe TargetedOffer, "when editing a target offer" do

  before(:each) do
    @targeted_offer   = create(:targeted_offer, status: "New")
    @targeted_product = TargetedProduct.create!(targeted_offer: @targeted_offer, product: Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID))
  end

  it "should not allow change of the country website to a country website different from that of any of its products" do
    @targeted_product = create(:targeted_product, {:product_id => Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID).id, targeted_offer_id: @targeted_offer.id})
    @targeted_offer.update(status: "Active")
    #Make sure there are products and the offer is valid
    expect(@targeted_offer.targeted_products).not_to be_empty
    expect(@targeted_offer).to be_valid

    #Try to change the country website
    @targeted_offer.country_website_id = (@targeted_offer.country_website_id.to_i + 1).to_s
    @targeted_offer.save

    #Make sure there is an error
    expect(@targeted_offer).to have(1).error_on(:country_website)
  end

  it "should not allow change of country website to a country website different from that of any of its people" do

    @targeted_offer.update(status: "Active")

    #Add a person with the same country website and ensure its valid
    @person = create(:person, country_website_id: @targeted_offer.country_website_id)

    @targeted_offer.add_person(@person)
    @targeted_offer.offer_type_new!
    expect(@targeted_offer).to be_valid

    #Change the country website id and save
    @targeted_offer.country_website_id = (@targeted_offer.country_website_id.to_i + 1).to_s
    @targeted_offer.save

    #Make sure theres an error
    expect(@targeted_offer).to have(2).error_on(:country_website)
  end

end

describe TargetedOffer, "when offer has a join_token" do
  before :each do
    @targeted_offer = create(:targeted_offer, {population_cap: 10, status: 'New', join_token: 'JOINTOKEN', usage_limit: 1, offer_type: 'new'})
    @targeted_product = create(:targeted_product, targeted_offer: @targeted_offer, product: create(:product, country_website_id: @targeted_offer.country_website_id))
    @targeted_offer.update_attribute(:status, 'Active')
  end

  context "when redeeming offer" do
    before :each do
      @person = create(:person)
    end

    describe ".add_person_with_join_token" do

      it "should add offer to the person" do
        targeted_person = TargetedOffer.add_person_with_join_token(@person, 'JOINTOKEN')
        expect(targeted_person.person).to eq @person
      end
    end

    describe ".offer_data_by_join_token" do

      it "should not return any offer or message when token does not exist" do
        data = TargetedOffer.offer_data_by_join_token('ABCDEFG')
        expect(data[:targeted_offer]).to be_nil
        expect(data[:error_message]).to be_nil
      end

      it "should return offer and no error message when token is valid" do
        data = TargetedOffer.offer_data_by_join_token('JOINTOKEN')
        expect(data[:targeted_offer]).to eq @targeted_offer
        expect(data[:error_message]).to be_nil
      end

      it "should return offer and error message when token is expired" do
        allow_any_instance_of(TargetedOffer).to receive(:inactive?).and_return(true)
        data = TargetedOffer.offer_data_by_join_token('JOINTOKEN')
        expect(data[:targeted_offer]).to eq @targeted_offer
        expect(data[:error_message]).to include("The offer you're looking for expired on")
      end

      it "should return offer and error message when token has reached population_cap" do
        allow_any_instance_of(TargetedOffer).to receive(:population_cap_exceeded?).and_return(true)
        data = TargetedOffer.offer_data_by_join_token('JOINTOKEN')
        expect(data[:targeted_offer]).to eq @targeted_offer
        expect(data[:error_message]).to include("Sorry. All spaces for this special offer have already been taken.")
      end
    end
  end
end

describe TargetedOffer do
  describe "#person_has_used_limit?" do

    before(:each) do
      @person           = create(:person)
      @targeted_offer   = create(:targeted_offer)
      targeted_product  = create(:targeted_product, targeted_offer: @targeted_offer, product: Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID))
      targeted_person   = create(:targeted_person, person: @person, targeted_offer: @targeted_offer)
    end

    context "when a person is NOT part of the targeted offer population" do
      it "always returns true" do
        not_included_person = create(:person)
        result = @targeted_offer.person_has_used_limit?(not_included_person)
        expect(result).to eq true
      end
    end

    context "when a person is part of the targeted offer population" do

      context "and the targeted_offer usage_limit is zero (i.e. unlimited)" do

        it "returns false regardless of how many times a user has redeemed offer" do
          allow_any_instance_of(TargetedPerson).to receive(:usage_count).and_return(999_999)
          result = @targeted_offer.person_has_used_limit?(@person)
          expect(result).to eq false
        end
      end

      context "and the targeted_offer usage_limit is one" do

        before(:each) do
          allow(@targeted_offer).to receive(:usage_limit).and_return(1)
        end

        it "returns false when the targeted_person.usage_count is less than one" do
          allow_any_instance_of(TargetedPerson).to receive(:usage_count).and_return(0)
          result = @targeted_offer.person_has_used_limit?(@person)
          expect(result).to eq false
        end

        it "returns true when the targeted_person.usage_count is eq to one" do
          allow_any_instance_of(TargetedPerson).to receive(:usage_count).and_return(1)
          result = @targeted_offer.person_has_used_limit?(@person)
          expect(result).to eq true
        end

        it "returns true when the targeted_person.usage_count is greater than one" do
          allow_any_instance_of(TargetedPerson).to receive(:usage_count).and_return(2)
          result = @targeted_offer.person_has_used_limit?(@person)
          expect(result).to eq true
        end
      end
    end
  end
end

describe TargetedOffer do
  describe "#single_use?" do

    context "when usage limit is 0 (i.e. unlimited)" do
      it "returns false" do
        offer = build_stubbed(:targeted_offer, usage_limit: 0)
        expect(offer.single_use?).to eq false
      end
    end

    context "when usage limit is 1" do
      it "returns true" do
        offer = build_stubbed(:targeted_offer, usage_limit: 1)
        expect(offer.single_use?).to eq true
      end
    end

    context "when usage_limit is greater than one" do
      it "returns false" do
        offer = build_stubbed(:targeted_offer, usage_limit: 2)
        expect(offer.single_use?).to eq false
      end
    end
  end
end


describe TargetedOffer do
  describe "effect on Invoice amount" do
  # NOTE: could not find a way to actually process invoices (which is when invoice_settlements are created)
  #       so, instead of testing the final_settlement_amount on an invoice, these tests "settle" an invoice w/o
  #       any actual invoice_settlements and see what the final_settlement_amount would have been by checking
  #       the outstanding_amount on invoices
  #
    context "for products purchased through the cart" do

      before(:each) do
        @person           = create(:person, :with_stored_credit_card)
        @album_credit     = Product.find(Product::US_ONE_ALBUM_CREDIT_PRODUCT_ID)
        @targeted_offer   = create(:targeted_offer)
        @targeted_product = create(:targeted_product, :fifty_percent_off, targeted_offer: @targeted_offer, product: @album_credit)
        @targeted_offer.update_attribute(:status, "Active")
        @targeted_person  = create(:targeted_person, person: @person, targeted_offer: @targeted_offer)
      end

      context "for a person in the targeted_offer" do

        it "the invoice amount is discounted" do
          purchase = Product.add_to_cart(@person, @album_credit)
          manager  = Cart::Payment::Manager.new(@person, [purchase], "fake_ip")
          cc_id    = @person.stored_credit_cards.first.id
          manager.add_strategy(Cart::Payment::Strategies::CreditCard.new(cc_id))
          allow_any_instance_of(Invoice).to receive(:can_settle?).and_return(true) # bypasses invoice_settlement creation
          manager.process!

          total_invoice_amt = manager.purchases.first.invoice.outstanding_amount
          # eq to what final_settlement_amt should be since there are no invoice_settlements for this "settled" invoice
          expect(total_invoice_amt.cents).to eq 1500
        end
      end

      context "for a person in an inactive targeted_offer" do

        it "the invoice amount is NOT discounted" do
          @targeted_offer.update_attribute(:status, "Inactive")
          purchase = Product.add_to_cart(@person, @album_credit)
          manager  = Cart::Payment::Manager.new(@person, [purchase], "fake_ip")
          cc_id    = @person.stored_credit_cards.first.id
          manager.add_strategy(Cart::Payment::Strategies::CreditCard.new(cc_id))
          allow_any_instance_of(Invoice).to receive(:can_settle?).and_return(true) # bypasses invoice_settlement creation
          manager.process!

          total_invoice_amt = manager.purchases.first.invoice.outstanding_amount
          # eq to what final_settlement_amt should be since there are no invoice_settlements for this "settled" invoice
          expect(total_invoice_amt.cents).to eq 2999
        end
      end

      context "for a person NOT in the targeted_offer" do
        it "the invoice amount is NOT discounted" do
          person_2 = create(:person, :with_stored_credit_card)
          purchase = Product.add_to_cart(person_2, @album_credit)
          manager  = Cart::Payment::Manager.new(person_2, [purchase], "fake_ip")
          cc_id    = person_2.stored_credit_cards.first.id
          manager.add_strategy(Cart::Payment::Strategies::CreditCard.new(cc_id))
          allow_any_instance_of(Invoice).to receive(:can_settle?).and_return(true)
          manager.process!

          total_invoice_amt = manager.purchases.first.invoice.outstanding_amount
          # eq to what final_settlement_amt should be since there are no invoice_settlements for this "settled" invoice
          expect(total_invoice_amt.cents).to eq 2999
        end
      end
    end

    context "targeted_product_store_offer products purchased through the cart" do

      context "targeted_product_store_offer has just one targeted product" do

        before(:each) do
          @person           = create(:person, :with_stored_credit_card)
          @album_product    = Product.find(Product::US_ONE_YEAR_SINGLE_PRODUCT_ID)
          @targeted_offer   = create(:targeted_offer, start_date: Time.now - 40.days, expiration_date: Time.now + 10.days, offer_type: "existing", status: "New")
          @targeted_product = create(:targeted_product, product: @album_product, targeted_offer: @targeted_offer, price_adjustment: 5.00, price_adjustment_type: "dollars_off")
          @targeted_offer.update_attribute(:status, "Active")
        end

        context "for a purchase where store and product match offer" do

          it "the invoice amount is discounted" do
            album     = create(:single, person: @person)
            salepoint = create(:salepoint, salepointable: album, finalized_at: DateTime.now)
            create(:targeted_product_store, targeted_product: @targeted_product, store: salepoint.store)

            purchase = Product.add_to_cart(@person, album, Product.find(Product::US_ONE_YEAR_SINGLE_PRODUCT_ID))
            manager  = Cart::Payment::Manager.new(@person, [purchase], "fake_ip")
            cc_id    = @person.stored_credit_cards.first.id
            manager.add_strategy(Cart::Payment::Strategies::CreditCard.new(cc_id))
            allow_any_instance_of(Invoice).to receive(:can_settle?).and_return(true)
            manager.process!

            total_invoice_amt = manager.purchases.first.invoice.outstanding_amount
            expect(total_invoice_amt.cents).to eq 499
          end
        end

        context "for a purchase where store DOES NOT match offer" do

          it "the invoice amount is NOT discounted" do
            album       = create(:single, person: @person)
            salepoint   = create(:salepoint, salepointable: album, finalized_at: DateTime.now)
            other_store = Store.where.not(id: salepoint.store).first
            create(:targeted_product_store, targeted_product: @targeted_product, store: other_store)

            purchase = Product.add_to_cart(@person, album, Product.find(Product::US_ONE_YEAR_SINGLE_PRODUCT_ID))
            manager  = Cart::Payment::Manager.new(@person, [purchase], "fake_ip")
            cc_id    = @person.stored_credit_cards.first.id
            manager.add_strategy(Cart::Payment::Strategies::CreditCard.new(cc_id))
            allow_any_instance_of(Invoice).to receive(:can_settle?).and_return(true)
            manager.process!

            total_invoice_amt = manager.purchases.first.invoice.outstanding_amount
            expect(total_invoice_amt.cents).to eq 999
          end
        end

        context "for a purchase where product DOES NOT match offer" do

          it "the invoice amount is NOT discounted" do
            album     = create(:album, person: @person)
            salepoint = create(:salepoint, salepointable: album, finalized_at: DateTime.now)
            create(:targeted_product_store, targeted_product: @targeted_product, store: salepoint.store)

            purchase = Product.add_to_cart(@person, album, Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID))
            manager  = Cart::Payment::Manager.new(@person, [purchase], "fake_ip")
            cc_id    = @person.stored_credit_cards.first.id
            manager.add_strategy(Cart::Payment::Strategies::CreditCard.new(cc_id))
            allow_any_instance_of(Invoice).to receive(:can_settle?).and_return(true)
            manager.process!

            total_invoice_amt = manager.purchases.first.invoice.outstanding_amount
            expect(total_invoice_amt.cents).to eq 2999
          end
        end
      end

      context "targeted_product_store_offer has multiple targeted products" do

        before(:each) do
          @person                  = create(:person, :with_stored_credit_card)
          @single_product          = Product.find(Product::US_ONE_YEAR_SINGLE_PRODUCT_ID)
          @album_product           = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
          @targeted_offer          = create(:targeted_offer, start_date: Time.now - 40.days, expiration_date: Time.now + 10.days, offer_type: "existing", status: "New")
          @targeted_single_product = create(:targeted_product, product: @single_product, targeted_offer: @targeted_offer, price_adjustment: 5.00, price_adjustment_type: "dollars_off")
          @targeted_album_product  = create(:targeted_product, product: @album_product, targeted_offer: @targeted_offer, price_adjustment: 5.00, price_adjustment_type: "dollars_off")
          @targeted_offer.update_attribute(:status, "Active")
        end

        context "for a purchase where store and product match offer" do
          it "the invoice for a single is discounted" do
            album     = create(:single, person: @person)
            salepoint = create(:salepoint, salepointable: album, finalized_at: DateTime.now)
            create(:targeted_product_store, targeted_product: @targeted_single_product, store: salepoint.store)
            create(:targeted_product_store, targeted_product: @targeted_album_product, store: salepoint.store)

            purchase = Product.add_to_cart(@person, album, Product.find(Product::US_ONE_YEAR_SINGLE_PRODUCT_ID))
            manager  = Cart::Payment::Manager.new(@person, [purchase], "fake_ip")
            cc_id    = @person.stored_credit_cards.first.id
            manager.add_strategy(Cart::Payment::Strategies::CreditCard.new(cc_id))
            allow_any_instance_of(Invoice).to receive(:can_settle?).and_return(true)
            manager.process!

            total_invoice_amt = manager.purchases.first.invoice.outstanding_amount
            expect(total_invoice_amt.cents).to eq 499
          end

          it "the invoice for an album is discounted" do
            album     = create(:album, person: @person)
            salepoint = create(:salepoint, salepointable: album, finalized_at: DateTime.now)
            create(:targeted_product_store, targeted_product: @targeted_single_product, store: salepoint.store)
            create(:targeted_product_store, targeted_product: @targeted_album_product, store: salepoint.store)

            purchase = Product.add_to_cart(@person, album, Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID))
            manager  = Cart::Payment::Manager.new(@person, [purchase], "fake_ip")
            cc_id    = @person.stored_credit_cards.first.id
            manager.add_strategy(Cart::Payment::Strategies::CreditCard.new(cc_id))
            allow_any_instance_of(Invoice).to receive(:can_settle?).and_return(true)
            manager.process!

            total_invoice_amt = manager.purchases.first.invoice.outstanding_amount
            expect(total_invoice_amt.cents).to eq 2499
          end
        end

        context "for a purchase where store DOES NOT match offer" do
          it "the invoice for a single is NOT discounted" do
            album       = create(:single, person: @person)
            salepoint   = create(:salepoint, salepointable: album, finalized_at: DateTime.now)
            other_store = Store.where.not(id: salepoint.store).first
            create(:targeted_product_store, targeted_product: @targeted_single_product, store: other_store)
            create(:targeted_product_store, targeted_product: @targeted_album_product, store: other_store)

            purchase = Product.add_to_cart(@person, album, Product.find(Product::US_ONE_YEAR_SINGLE_PRODUCT_ID))
            manager  = Cart::Payment::Manager.new(@person, [purchase], "fake_ip")
            cc_id    = @person.stored_credit_cards.first.id
            manager.add_strategy(Cart::Payment::Strategies::CreditCard.new(cc_id))
            allow_any_instance_of(Invoice).to receive(:can_settle?).and_return(true)
            manager.process!

            total_invoice_amt = manager.purchases.first.invoice.outstanding_amount
            expect(total_invoice_amt.cents).to eq 999
          end

          it "the invoice for an album is NOT discounted" do
            album       = create(:album, person: @person)
            salepoint   = create(:salepoint, salepointable: album, finalized_at: DateTime.now)
            other_store = Store.where.not(id: salepoint.store).first
            create(:targeted_product_store, targeted_product: @targeted_single_product, store: other_store)
            create(:targeted_product_store, targeted_product: @targeted_album_product, store: other_store)

            purchase = Product.add_to_cart(@person, album, Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID))
            manager  = Cart::Payment::Manager.new(@person, [purchase], "fake_ip")
            cc_id    = @person.stored_credit_cards.first.id
            manager.add_strategy(Cart::Payment::Strategies::CreditCard.new(cc_id))
            allow_any_instance_of(Invoice).to receive(:can_settle?).and_return(true)
            manager.process!

            total_invoice_amt = manager.purchases.first.invoice.outstanding_amount
            expect(total_invoice_amt.cents).to eq 2999
          end
        end
      end
    end
  end
end

def build_flat_rate(flat_price = 57.99)
  @product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
end
