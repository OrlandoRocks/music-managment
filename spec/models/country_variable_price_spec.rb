require "rails_helper"

describe CountryVariablePrice do
  describe "::display_wholesale_preorder_album_price?" do
    before(:each) do
      @person = FactoryBot.create(:person)
      @country_website = CountryWebsite.find_by(country: "UK")
      @person.country_website = @country_website
      @country_variable_price1 = FactoryBot.create(:country_variable_price, country_website: @country_website)
      @country_variable_price2 = FactoryBot.create(:country_variable_price, country_website: @country_website)
    end

    it "returns false if all the wholesale_prices for the user's country is empty" do
      expect(CountryVariablePrice.display_wholesale_preorder_album_price?(@person)).to be false
    end

    it "returns false if at least one of the wholesale_prices for the user's country is empty" do
      @country_variable_price1.update(wholesale_price: 5.55)
      expect(CountryVariablePrice.display_wholesale_preorder_album_price?(@person)).to be false
    end

    it "returns true if all the wholesale_prices for the user's country are present" do
      @country_variable_price1.update(wholesale_price: 5.55)
      @country_variable_price2.update(wholesale_price: 5.55)
      expect(CountryVariablePrice.display_wholesale_preorder_album_price?(@person)).to be true
    end
  end

  describe "#display_price" do
    it "returns the wholesale price if present" do
      country_variable_price = build(:country_variable_price, wholesale_price: 5.55)
      expect(country_variable_price.display_price).to eq(5.55)
    end

    it "it calls and returns the value of #variable_price_retail_price if the wholesale price is blank" do
      country_variable_price = build(:country_variable_price)
      expect(country_variable_price).to receive(:variable_price_retail_price).and_return("SOMETHING")
      expect(country_variable_price.display_price).to eq("SOMETHING")
    end

    context "german variable prices" do
      it "always return wholesale price" do
        country_variable_prices = CountryVariablePrice.where(currency: "EUR")
        country_variable_prices.each do |cvp|
          expect(cvp.display_price).to eq(cvp.wholesale_price)
        end
      end
    end

  end

  describe "#variable_price_retail_price" do
    it "returns #variable_price_amount if the attribute exists" do
      FactoryBot.create(:country_variable_price)
      @country_variable_price = CountryVariablePrice.select("country_variable_prices.*, 'SOMETHING' as variable_price_amount").first
      expect(@country_variable_price.variable_price_retail_price).to eq("SOMETHING")
    end

    it "returns the variable_price's price if variable_price_amount does not exist" do
      country_variable_price = build(:country_variable_price)
      allow(country_variable_price).to receive_message_chain(:variable_price, :price).and_return("SOMETHING")
      expect(country_variable_price.variable_price_retail_price).to eq("SOMETHING")
    end
  end
end
