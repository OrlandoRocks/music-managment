require "rails_helper"

describe Label do
  describe 'four byte char validation' do
    let(:label) { build(:label, name: "💿💿💿💿💿") }

    it 'validates first_name and last_name for four byte chars' do
      label.valid?
      expect(label.errors.messages[:name].size).to be 1
    end
  end
  describe 'length validation' do
    let(:label) { build(:label, name: "a"*121)}

    it 'validates length is less than 120 characters' do
      label.valid?
      expect(label.errors.messages[:name].size).to be 1
    end
  end
end
