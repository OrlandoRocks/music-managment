require "rails_helper"

describe PublishingComposer do
  before(:each) do
    allow(FeatureFlipper).to receive(:show_feature?).and_return(false)
  end

  xdescribe "selected_splits?" do
    before :each do
      @publishing_composer = create(:publishing_composer)
    end

    it "should return true when there are splits" do
      @splits = create(:publishing_composition_split, publishing_composer: @publishing_composer)
      expect(@publishing_composer.selected_splits?).to be_truthy
    end

    it "should return false when there are no splits" do
      expect(@publishing_composer.selected_splits?).to be_falsey
    end
  end

  xdescribe "create 'Unallocated Sum' composition" do
    before :each do
      @publishing_composer = create(:publishing_composer)
      @publishing_composer_with_publisher = create(:publishing_composer,
        publisher: create(:publisher,
        name: 'Composer with Publisher'),
        person_id: create(:person).id
      )
    end

    it "should create unallocated sum song after publishing_composer is created" do
      compositions = @publishing_composer.compositions.find_all{|c| c.is_unallocated_sum? }
      expect(compositions.count).to eq(1)
      expect(compositions.first.publishing_splits.first.percent).to eq(100)
    end

    it "should name the unallocated sum appropriately" do
      @publishing_composer.compositions.detect{|c| c.is_unallocated_sum? }.name == @publishing_composer.name
      @publishing_composer_with_publisher.compositions.detect{|c| c.is_unallocated_sum? }.name == @publishing_composer_with_publisher.publisher.name
    end
  end

  xdescribe ".with_no_unallocated_sum_composition" do
    before :each do
      @publishing_composer = create(:publishing_composer)
      @publishing_composer2 = create(:publishing_composer, person_id: create(:person).id)
      create(:publishing_composition_split, publishing_composer: @publishing_composer2)
    end

    it "should return publishing_composer with no unallocated_sum_composition" do
      unallocated_sum_composition = @publishing_composer2.compositions.detect { |c| c.is_unallocated_sum? }
      expect(unallocated_sum_composition).not_to be_nil
      unallocated_sum_composition.destroy
      expect(Composer.with_no_unallocated_sum_composition).to include(@publishing_composer2)
      expect(Composer.with_no_unallocated_sum_composition).not_to include(@publishing_composer)
    end
  end

  describe "#process_paid" do
    before :each do
      @person       = create(:person)
      @account      = create(:account, person: @person)
      @publishing_composer     = create(
        :publishing_composer,
        :active,
        :with_pub_admin_purchase,
        person:   @person,
        account:  @account,
        email:    Faker::Internet.email
      )
    end

    context "BMI reminder email" do
      it "should send BMI reminder email if not affiliated with PRO as songwriter" do
        @publishing_composer.update(performing_rights_organization_id: nil)
        expect(MailerWorker).to receive(:perform_async)
        @publishing_composer.process_paid
      end

      it "should not send BMI reminder email if affiliated with a PRO" do
        expect(MailerWorker).not_to receive(:perform_async)
        @publishing_composer.process_paid
      end
    end

    context "has rights app" do
      before :each do
        allow(FeatureFlipper).to receive(:show_feature?) { true }
        allow_any_instance_of(PublishingComposer).to receive(:has_rights_app?).and_return(true)
      end

      it "should invoke ArtistAccountWriterWorker if active publishing_composer's account does not have provider_account_id" do
        expect(PublishingAdministration::PublishingArtistAccountWriterWorker)
          .to receive(:perform_async).with(@publishing_composer.id)
        @publishing_composer.process_paid
      end

      it "should invoke the PublishingWriterCreationWorker if active publishing_composer's account has provider_account_id" do
        @account.update(provider_account_id: "DMC123")
        expect(PublishingAdministration::PublishingWriterCreationWorker)
          .to receive(:perform_async).with(@publishing_composer.id)
        @publishing_composer.process_paid
      end

      it "should not invoke PublishingArtistAccountWriterWorker or PublishingWriterCreationWorker if publishing_composer is not active" do
        inactive_composer = create(:publishing_composer, account: @account, person: @person)

        expect(PublishingAdministration::PublishingArtistAccountWriterWorker)
          .not_to receive(:perform_async).with(inactive_composer.id)
        expect(PublishingAdministration::PublishingWriterCreationWorker)
          .not_to receive(:perform_async).with(inactive_composer.id)

        inactive_composer.process_paid
      end
    end
  end

  describe "#send_bmi_registration_reminder" do
    it "should log it in the notes" do
      publishing_composer = create(:publishing_composer)
      publishing_composer.send(:send_bmi_registration_reminder)
      expect(Note.find_by(subject: "Publishing Email").note)
        .to match(/#{publishing_composer.name}.*#{publishing_composer.publishing_administrator.email}/)
    end
  end

  xdescribe "::missing_splits?" do
    before(:each) do
      @account      = create(:account)
      @publishing_composer     = create(:publishing_composer, account: @account)
      @album        = create(:album, person: @publishing_composer.person)
      @composition  = create(:composition)
      @song         = create(:song, album: @album)
      @purchase     = create(:purchase,
                        person: @publishing_composer.publishing_administrator,
                        related_id: @publishing_composer.id,
                        related_type: "PublishingComposer")
    end

    context "user has composition" do
      before(:each) do
        @song.update(composition: @composition)
      end

      it "returns true for a user that has a composition with a new state for any his songs" do
        @composition.update_attribute(:state, 'new')
        expect(Composer.missing_splits?(@publishing_composer.person)).to be true
      end

      it "returns true for a user that has a composition with a nil state for any his songs" do
        expect(Composer.missing_splits?(@publishing_composer.person)).to be true
      end

      it "returns false for a user that has a composition of different state" do
        @composition.update_attribute(:state, 'pending_distribution')
        expect(Composer.missing_splits?(@publishing_composer.person)).to be false
      end
    end

    it "returns false for a user without a composition for any of his songs" do
      expect(Composer.missing_splits?(@publishing_composer.person)).to eq true
    end
  end

  xdescribe "agreed_to_terms validation" do
    context "when rights app is enabled" do
      it "returns true and bypasses validation" do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
        allow_any_instance_of(PublishingAdministrationHelper).to receive(:rights_app_enabled?).and_return(true)
        publishing_composer = build(:publishing_composer, agreed_to_terms: "")
        expect(composer.valid?).to be_truthy
      end
    end
  end

  def sample_esignature_response
    {'status' => 'sent', 'guid' => 'ABCDEFG1234567'}
  end

  describe "Publishing Administration Status" do
    before :each do
      @person       = create(:person)
      @account      = create(:account, person: @person)
      @active     = create(:publishing_composer, :active, person: @person, account: @account)
      @terminated = create(:publishing_composer, :active, :terminated, person: @person, account: @account)
    end

    describe "is_active?" do
      before :each do
        create(
          :purchase,
          related_type: "PublishingComposer",
          related_id: @active.id,
          product: Product.find(65),
          person_id: @active.person_id
        )
      end

      it "returns true unless a publishing_composer is terminated" do
        expect(@active.is_active?).to be(true)
        expect(@terminated.is_active?).to be(false)
      end
    end

    describe "#terminate_pub_admin" do
      it "records the date on which the publishing_composer's publishing admin was terminated" do
        @active.terminate_pub_admin
        expect(@active.terminated_at).not_to be(nil)
      end
    end

    describe "reactivate_pub_admin" do
      before :each do
        create(
          :purchase,
          related_type: "PublishingComposer",
          related_id: @terminated.id,
          product: Product.find(65),
          person_id: @terminated.person_id
        )
      end
      it "deletes the termination date" do
        @terminated.reactivate_pub_admin
        expect(@terminated.terminated_at).to be(nil)
        expect(@terminated.is_active?).to be(true)
      end
    end
  end

  describe "a valid composer" do
    describe "a composer with a blank name" do
      it "should be valid" do
        composer = build(:publishing_composer, first_name: "", last_name: "")

        expect(composer.valid?).to be_truthy
      end
    end
  end

  xdescribe "terminated publishing_composers" do
    context "fully terminated publishing_composers" do
      let(:publishing_composer) { build_stubbed(:publishing_composer, :terminated) }
      let!(:terminated_composer) { build_stubbed(:terminated_composer, :fully, publishing_composer: publishing_composer) }

      describe ".does_not_have_fully_terminated_composers" do
        it "checks if the publishing_composer is fully terminated" do
          expect(composer.does_not_have_fully_terminated_composers).to eq(["cannot edit terminated publishing_composers"])
        end
      end

      describe ".has_fully_terminated_composers?" do
        it "returns true for fully terminated publishing_composers" do
          expect(composer.has_fully_terminated_composers?).to eq(true)
        end
      end
    end

    context "partially terminated publishing_composers" do
      before :each do
        @publishing_composer = create(:publishing_composer)
        @terminated_composer = create(:terminated_composer, :partially, publishing_composer: @publishing_composer)
      end

      describe ".does_not_have_fully_terminated_composers" do
        it "returns nil if the publishing_composer is partially terminated" do
          expect(@publishing_composer.does_not_have_fully_terminated_composers).to eq(nil)
        end
      end

      describe ".has_fully_terminated_composers?" do
        it "returns false for partially terminated publishing_composers" do
          expect(@publishing_composer.has_fully_terminated_composers?).to eq(false)
        end
      end
    end
  end

  xdescribe "#assign_publishing_role" do
    it "sets the publishing role after creating a publishing_composer" do
      publishing_composer = build(:publishing_composer)
      expect(composer.publishing_role).to be_nil
      publishing_composer.save
      expect(composer.publishing_role).not_to be_nil
    end
  end

  xdescribe "#letter_of_direction" do
    it "should return the letter of direction document of the publishing_composer" do
      publishing_composer = create(:publishing_composer)
      lod = create(:legal_document, name: DocumentTemplate::LOD, subject_id: publishing_composer.id, subject_type: 'composer')
      expect(composer.letter_of_direction).to eq(lod)
    end

    it "should return nil if there is no letter of direction" do
      publishing_composer = create(:publishing_composer)
      expect(composer.letter_of_direction).to be_nil
    end
  end

  xdescribe "valid_letter_of_direction" do
    it "should return true if letter of direction is not at all needed for the publishing_composer" do
      publishing_composer = build_stubbed(:publishing_composer)
      expect(composer).to receive(:letter_of_direction).and_return(nil)
      expect(composer.valid_letter_of_direction?).to be_truthy
    end

    it "should return true if the letter of direction is present with valid signed_at date" do
      publishing_composer = build_stubbed(:publishing_composer)
      lod = create(:legal_document, signed_at: Date.today())
      expect(composer).to receive(:letter_of_direction).and_return(lod)
      expect(composer.valid_letter_of_direction?).to be_truthy

    end

    it "should return false if the letter of direction is not yet signed" do
      publishing_composer = build_stubbed(:publishing_composer)
      lod = create(:legal_document, signed_at: nil)
      expect(composer).to receive(:letter_of_direction).and_return(lod)
      expect(composer.valid_letter_of_direction?).to be_falsey
    end
  end

  xdescribe "#create_or_update_publisher" do
    context "when a publishing_composer does NOT have a publisher" do
      it "should update the publisher" do
        publishing_composer = create(:publishing_composer)

        publisher_params = {
          name: "Pub Name",
          cae: "123456789",
          performing_rights_organization_id: 56
        }

        expect(composer.publisher).to be_nil
        publishing_composer.create_or_update_publisher(publisher_params)

        publisher = publishing_composer.reload.publisher
        expect(publisher.name).to eq publisher_params[:name]
        expect(publisher.cae).to eq publisher_params[:cae]
      end
    end

    context "when a publishing_composer has a publisher" do
      it "should create a new publisher" do
        publishing_composer = create(:publishing_composer, :with_publisher)

        publisher_params = {
          name: "Pub Name",
          cae: "123456789",
          performing_rights_organization_id: 56
        }

        publishing_composer.create_or_update_publisher(publisher_params)

        publisher = publishing_composer.reload.publisher
        expect(publisher.name).to eq publisher_params[:name]
        expect(publisher.cae).to eq publisher_params[:cae]
      end
    end
  end
end
