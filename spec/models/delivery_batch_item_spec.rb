require "rails_helper"

describe DeliveryBatchItem do
  describe "#mark_as_sent" do
    it "changes the status to sent" do
      item = create(:delivery_batch_item)
      expect(item.reload.status).to eq "present"
      item.mark_as_sent
      expect(item.reload.status).to eq "sent"
    end
  end

  describe "#add_to_manifest" do
    it "changes the status to 'sent' and creates a manifest item" do
      params = {
        xml_remote_path: "some/fake/url/for_whut.xml",
        xml_hashsum: "Whuuut",
        uuid: "numbers, yo",
        upc: "letters and numbers, son",
        takedown: "false"
      }

      item = create(:delivery_batch_item)
      item.mark_as_sent
      item.add_to_manifest(params)
      expect(item.reload.status).to eq "sent"
      expect(item.delivery_manifest_item).not_to be nil
    end
  end
end
