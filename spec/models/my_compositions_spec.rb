require "rails_helper"

describe MyCompositions do
  before :each do
    @person = TCFactory.build_person
    @composer = TCFactory.build_composer(:person => @person)
    @compositions = TCFactory.create_compositions(@composer)
  end

  describe '.search' do
    it "should return keyword search results in conjunction with filters" do
      options = { :keyword => 'Song', :filters => {:statuses => ['Conflict', 'Split Submitted'] , :splits => ['50','100'], :album_names => ['Album For Song 0','Album For Song 1']},
        :sort => "songs.name desc", :per_page => 10 ,:page => 1 }
      expect{ MyCompositions.new(@person).search(options) }.not_to raise_error
    end

    it "should sort by correct status order" do
      options = {:sort => ['status asc', 'compositions.name desc'], :per_page => 10 ,:page => 1 }
      expect{ MyCompositions.new(@person).search(options) }.not_to raise_error
    end
  end

end
