require "rails_helper"

describe SongLibraryUpload, "when creating a song library upload" do
  it { is_expected.to validate_presence_of(:artist_name) }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:genre) }
  it { is_expected.to validate_presence_of(:person) }
end

describe SongLibraryUpload do
  describe "#save" do
    context "when song_library_asset_data changes" do
      it "sets duration" do
        song_path = Rails.root.join("spec", "assets", "track_90_seconds.mp3")
        song_library_upload = build(:song_library_upload, song_library_asset: File.new(song_path))

        song_library_upload.save

        expect(song_library_upload.duration).to be > 89 # seconds
      end
    end

    context "when song_library_asset_data has not changed" do
      it "does not change duration" do
        song_library_upload = build(:song_library_upload, song_library_asset: nil)

        song_library_upload.save

        expect(song_library_upload.saved_change_to_duration?).to eq(false)
      end
    end
  end
end
