require "rails_helper"

describe Cert do
  context "with TargetedProduct set on purchase" do
    before :each do
      @targeted_product = FactoryBot.create(:targeted_product)
      @product = @targeted_product.product
      @targeted_product.targeted_offer.update(status: "Active")
      @targeted_person = FactoryBot.create(:targeted_person, targeted_offer: @targeted_product.targeted_offer)
      @person = @targeted_person.person
      @purchase = Product.add_to_cart(@person, @product)
      @cert = FactoryBot.create(:cert)
    end

    it "should apply the cert and disassociate the TP if the discount is greater" do
      Cert.verify(entered_code: @cert.cert, purchase: @purchase)
      expect(@purchase.cert).to eq @cert
      expect(@purchase.targeted_product).to be_nil
    end

    it "should not apply the cert or dissasociate the TP if the discount is smaller" do
      @cert.update(engine_params: 1)
      Cert.verify(entered_code: @cert.cert, purchase: @purchase)
      expect(@purchase.cert).to be_nil
      expect(@purchase.targeted_product).to eq @targeted_product
    end

    it "should reapply the targeted_product if cert is set and then disassociated" do
      Cert.verify(entered_code: @cert.cert, purchase: @purchase)
      @purchase.cert.disassociate
      @purchase.reload.recalculate
      expect(@purchase.cert).to be_nil
      expect(@purchase.targeted_product).to eq @targeted_product
    end

    it "should return error if the single use cert is already used" do
      cert_batch = FactoryBot.create(:cert_batch, spawning_code: 9876,cert_engine: 'SpawningAllDistribution')
      cert = FactoryBot.create(:cert, :cert => 'DEFAULT_SINGLE_USE_CERT', cert_batch: cert_batch, cert_engine: 'SpawningAllDistribution')
      product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
      album = FactoryBot.create(:album, person_id: @person.id)
      purchase = FactoryBot.create(:purchase, :unpaid, person: @person, product: product, related_id: album.id, related_type: "Album")
      expect(Cert.verify(entered_code: cert.cert, purchase: purchase)).to be_valid

      new_album = FactoryBot.create(:album, person_id: @person.id)
      new_purchase = FactoryBot.create(:purchase, :unpaid, person: @person, product: product, related_id: new_album.id, related_type: "Album")
      expect(Cert.verify(entered_code: cert.cert, purchase: new_purchase)).to eq("Certificate already used  by: \"#{album.class}\" #{album.id}")
    end
  end
end
