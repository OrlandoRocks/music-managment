require "rails_helper"

describe CountryWebsite do
  context "show_cookie_warning?" do
    it "false for non uk, ger countries" do
      expect(CountryWebsite.show_cookie_warning?("US")).to eq(false)
      expect(CountryWebsite.show_cookie_warning?("AU")).to eq(false)
      expect(CountryWebsite.show_cookie_warning?("CA")).to eq(false)
    end

    it "true for non uk, ger" do
      expect(CountryWebsite.show_cookie_warning?("DE")).to eq(true)
      expect(CountryWebsite.show_cookie_warning?("UK")).to eq(true)
      expect(CountryWebsite.show_cookie_warning?("FR")).to eq true
    end
  end

  context "on create" do
    it "should have accurate factory" do
      country_website = TCFactory.create(:country_website)
      expect(country_website).to be_valid
    end

    it "should require a name" do
      country_website = TCFactory.build(:country_website, :name => nil)
      country_website.save
      expect(country_website.errors.messages[:name].size).to eq 1
    end

    it "should require a currency" do
      country_website = TCFactory.build(:country_website, :currency => nil)
      country_website.save
      expect(country_website.errors.messages[:currency].size).to eq 2
    end

    it "should require a country" do
      country_website = TCFactory.build(:country_website, :country => nil)
      country_website.save
      expect(country_website.errors.messages[:country].size).to eq 1
    end

    it "should require a currency of exactly 3 characters" do
      country_website_long = build(:country_website, currency: '1234')
      country_website_long.save
      expect(country_website_long.errors.messages[:currency].size).to eq 1

      country_website_short = build(:country_website, currency: '12')
      country_website_short.save

      expect(country_website_short.errors.messages[:currency].size).to eq 1
      country_website = build(:country_website, currency: '123')
      country_website.save

      expect(country_website.errors.messages[:currency]).to be_empty
    end

  end

  context "set processors correctly for US" do
    before(:each) do
      @us_country_website = CountryWebsite.find_by(country: 'US')
      @ca_country_website = CountryWebsite.find_by(country: 'CA')
    end

    it "should have a braintree_processor for US and CA" do
      @us_country_website.braintree_processor == 'ccprocessora'
      @ca_country_website.braintree_processor == 'ccprocessorb'
    end

    it "should have a paypal_checkout_processor for US and CA" do
      expect(@us_country_website.paypal_checkout_credentials['USER']).to eq('dan_1303924341_biz_api1.tunecore.com')
      expect(@us_country_website.paypal_checkout_credentials['PWD']).to eq("1303924353")
      expect(@us_country_website.paypal_checkout_credentials['SIGNATURE']).to eq('AiPC9BjkCyDFQXbSkoZcgqH3hpacALmKZwEGSugwVD3ZwshtnIwLGLFz')
      expect(@us_country_website.paypal_checkout_credentials).to eq({"USER"=>"dan_1303924341_biz_api1.tunecore.com", "SIGNATURE"=>"AiPC9BjkCyDFQXbSkoZcgqH3hpacALmKZwEGSugwVD3ZwshtnIwLGLFz", "PWD"=>"1303924353"})
      expect(@ca_country_website.paypal_checkout_credentials['USER']).to eq('causd_1317758079_biz_api1.tunecore.com')
      expect(@ca_country_website.paypal_checkout_credentials['PWD']).to eq("1317758112")
      expect(@ca_country_website.paypal_checkout_credentials['SIGNATURE']).to eq('AE8Z1e9pCI.by00Q2BDitFIgdkYJAvHnkuaUwdFaFCAa06MV.N0Zbswb')
      expect(@ca_country_website.paypal_checkout_credentials).to eq({"USER"=>"causd_1317758079_biz_api1.tunecore.com", "SIGNATURE"=>"AE8Z1e9pCI.by00Q2BDitFIgdkYJAvHnkuaUwdFaFCAa06MV.N0Zbswb", "PWD"=>"1317758112"})
    end

    it "should have a paypal_withdrawal_processor for US and CA" do
      expect(@us_country_website.paypal_withdrawal_account).to be_a_kind_of(PayPalBusiness::API)
      expect(@ca_country_website.paypal_withdrawal_account).to be_a_kind_of(PayPalBusiness::API)
      expect(@us_country_website.paypal_withdrawal_account.username).to eq("sb-pckge979661_api1.business.example.com")
      expect(@ca_country_website.paypal_withdrawal_account.username).to eq("sb-njz6y967451_api1.business.example.com")
    end
  end

  context "set processors correctly for non US and CA currencies" do
    before(:each) do
      @uk_country_website = CountryWebsite.find_by(country: 'UK')
      @de_country_website = CountryWebsite.find_by(country: 'DE')
      @fr_country_website = CountryWebsite.find_by(country: 'FR')
    end

    it "should have a paypal_checkout_processor for DE and UK" do
      expect(@uk_country_website.paypal_checkout_credentials).to eq({"USER"=>"sandbox-uk_api1.tunecore.com", "SIGNATURE"=>"AiPC9BjkCyDFQXbSkoZcgqH3hpacAVYow.HE-wat9ynUnCozVQK2z3PV", "PWD"=>'7E53CEWCUD6S5NVL'})
      expect(@de_country_website.paypal_checkout_credentials).to eq({"USER"=>"sandbox-uk_api1.tunecore.com", "SIGNATURE"=>"AiPC9BjkCyDFQXbSkoZcgqH3hpacAVYow.HE-wat9ynUnCozVQK2z3PV", "PWD"=>'7E53CEWCUD6S5NVL'})
      expect(@fr_country_website.paypal_checkout_credentials).to eq({"USER"=>"sandbox-uk_api1.tunecore.com", "SIGNATURE"=>"AiPC9BjkCyDFQXbSkoZcgqH3hpacAVYow.HE-wat9ynUnCozVQK2z3PV", "PWD"=>'7E53CEWCUD6S5NVL'})
    end

    it "should have a paypal_withdrawal_processor for DE and UK" do
      expect(@uk_country_website.paypal_withdrawal_account).to be_a_kind_of(PayPalBusiness::API)
      expect(@uk_country_website.paypal_withdrawal_account.username).to eq("sandbox-uk-payouts_api1.tunecore.com")
      expect(@de_country_website.paypal_withdrawal_account).to be_a_kind_of(PayPalBusiness::API)
      expect(@de_country_website.paypal_withdrawal_account.username).to eq("sandbox-uk-payouts_api1.tunecore.com")
      expect(@fr_country_website.paypal_withdrawal_account).to be_a_kind_of(PayPalBusiness::API)
      expect(@fr_country_website.paypal_withdrawal_account.username).to eq("sandbox-uk-payouts_api1.tunecore.com")
    end
  end

  context "#is_eur?" do
    it "should return true for euro currencies" do
      country_website = TCFactory.create(:country_website, currency:"EUR")
      expect(country_website.is_eur?).to eq(true)
    end

    it "should return false for non-euro currencies" do
      country_website = TCFactory.create(:country_website, currency:"USD")
      expect(country_website.is_eur?).to eq(false)
    end
  end

  describe "#full_country_name" do
    it "should return country names from country table where not UK" do
      CountryWebsite.all.each do |cw|
        next if cw.country == "UK"
        country = Country.find_by(iso_code: cw.country)
        expect(cw.full_country_name).to eq(country.name)
      end
    end

    it "should return United Kingdom for UK" do
      cw = CountryWebsite.find_by(country: "UK")
      expect(cw.full_country_name).to eq("United Kingdom")
    end
  end

  describe "#url" do
    it "returns DEPLOY_ENV env var if set" do
      stub_const("DEPLOY_ENV", "web-test")
      country_websites = CountryWebsite.all
      country_websites.each do |country_website|
        expect(country_website.url).to eq("#{COUNTRY_URLS[country_website.country]}")
      end
    end

    it "returns 'web' if DEPLOY_ENV env if set" do
      country_websites = CountryWebsite.all
      country_websites.each do |country_website|
        expect(country_website.url).to eq("#{COUNTRY_URLS[country_website.country]}")
      end
    end
  end
end
