require "rails_helper"

describe KnowledgebaseLink do
  it "validates article_id and article slug upon creation" do
    kb = KnowledgebaseLink.create
    expect(kb.persisted?).to be_falsey
    expect(kb.update(article_id: "111")).to be_falsey
    expect(kb.update(article_id: "111", article_slug: "model-test")).to be_truthy
  end
end
