require "rails_helper"

def create_ready_to_purchase_album

  album = TCFactory.create(:album,
                            :person => @person,
                            :known_live_on => nil,
                            :finalized_at => nil)

  store = Store.first
  if store.variable_prices.empty?
    salepoint = TCFactory.create(:salepoint, :salepointable => album, :store => store, :has_rights_assignment => true)
  else
    salepoint = TCFactory.create(:salepoint, :salepointable => album, :store => store, :has_rights_assignment => true, :variable_price_id => store.variable_prices.first.id)
  end

  song1 = TCFactory.create(:song, :album => album)
  song2 = TCFactory.create(:song, :album => album)

  upload1 = TCFactory.create(:upload, :song => song1)
  upload2 = TCFactory.create(:upload, :song => song2)

  song1.save
  song2.save

  album

end

describe TargetedPerson, "when creating a targeted_person" do
  before(:each) do
    @targeted_offer   = create(:targeted_offer, :start_date => Time.now - 40.days, :expiration_date => Time.now + 10.days, :status=>"Inactive")
    @product          = Product.where(name: "1 Year Album", country_website_id: 1, status: "Active").first
    @targeted_product = TargetedProduct.create(:targeted_offer=>@targeted_offer, :product=>@product, :price_adjustment=>10, :price_adjustment_type=>'percentage_off')
    @targeted_offer.update(:status=>"Active")
  end

  it "should not add a person to the population if they were already found in another population" do
    allow(TargetedOffer).to receive(:active_offers_for_person_and_products).and_return([TargetedOffer.new])
    expect { TargetedPerson.create({:person => Person.first, :targeted_offer => @targeted_offer}) }.to_not change {TargetedPerson.count}
  end

  it "should add a person to the population if they were not found in another similar population" do
    allow(TargetedOffer).to receive(:active_offers_for_person_and_products).and_return([])
    expect{ TargetedPerson.create({:person => Person.first, :targeted_offer => @targeted_offer}) }.to change{TargetedPerson.count}.by(1)
  end

  it "should not let you create a person from a different country website than the targeted offer" do

    #Make sure we can add a valid person
    @person          = TCFactory.create(:person, :country_website_id=>@targeted_offer.country_website_id)
    @targeted_person = TargetedPerson.create({:person => @person, :targeted_offer => @targeted_offer})
    expect(@targeted_person).to be_valid

    #Make sure we can't add an invalid person
    wrong_country_website_id    = (@targeted_offer.country_website_id.to_i + 1).to_s
    @person2                    = TCFactory.create(:person)
    @person2.country_website_id = wrong_country_website_id
    @person2.save

    #Ensure theres an error
    @targeted_person = TargetedPerson.create({:person => @person2, :targeted_offer => @targeted_offer})
    expect(@targeted_person.errors.messages[:person].size).to eq 1
  end

  context "when the targeted offer is active" do

    it "should update the persons purchases that are affected by this targeted offer" do

      @person = create(:person)

      @album = create_ready_to_purchase_album

      #Create a purchase
      Product.add_to_cart(@person, @album, @product)
      discount_cents_before = @person.purchases.first.discount_cents
      expect(@person.purchases.first.targeted_product).to be_nil

      #Add the person to the targeted offer
      @targeted_person = TargetedPerson.create!(:targeted_offer=>@targeted_offer, :person=>@person)

      #Make sure that the price was recaculated and that the purchase has the targeted product
      purchase = @person.purchases.reload.first
      expect(purchase.targeted_product).not_to be_nil
      expect(purchase.discount_cents).to be > discount_cents_before
      expect(purchase.targeted_product_id).to     eq(@targeted_product.id)
    end

    it "should leave paid purchases alone" do

      @person = TCFactory.create(:person)
      @album  = create_ready_to_purchase_album
      @album2 = create_ready_to_purchase_album

      #Create a purchase
      purchase = Product.add_to_cart(@person, @album, @product)
      purchase.update!(:paid_at=>Time.now)

      #Create a purchase that will remain unpaid
      purchase2 = Product.add_to_cart(@person, @album2, @product)
      purchase2_discount_cents_before = purchase2.discount_cents
      expect(purchase2.paid_at).to be_nil

      #Add the person to the offer
      TargetedPerson.create!(:targeted_offer=>@targeted_offer, :person=>@person)

      #Purchase 1 is paid and should not change
      expect(purchase.reload.discount_cents).to eq(0)

      #Purchase 2 is unpaid and should be changed
      expect(purchase2.reload.discount_cents).to be > purchase2_discount_cents_before
    end

    it "should leave adjusted purchases alone" do

      @person = TCFactory.create(:person)
      @admin  = TCFactory.create(:person)
      @album  = create_ready_to_purchase_album

      #Create a purchase and adjust it
      purchase = Product.add_to_cart(@person, @album, @product)
      purchase.adjust_price(499, "Damn customers!", @admin)

      #Add the person to the offer
      TargetedPerson.create!(:targeted_offer=>@targeted_offer, :person=>@person)

      #Make sure it didn't change
      expect(purchase.reload.cost_cents).to eq(499)
    end

  end

  context "when the targeted offer is not active" do

    before(:each) do
      @targeted_offer.update(:status=>"Inactive")
    end

    it "should not update any of the persons purchases that are affected by this targeted offer" do
      @person = TCFactory.create(:person)

      @album = create_ready_to_purchase_album

      #Create a purchase
      Product.add_to_cart(@person, @album, @product)

      cost_cents_before = @person.purchases.first.cost_cents
      expect(@person.purchases.first.targeted_product).to be_nil

      #Add the person to the targeted offer
      @targeted_person = TargetedPerson.create!(:targeted_offer=>@targeted_offer, :person=>@person)

      #Make sure that the price was recaculated and that the purchase has the targeted product
      purchase = @person.purchases.reload.first
      expect(purchase.targeted_product).to be_nil
      expect(purchase.cost_cents).to eq(cost_cents_before)
    end

  end

end

describe TargetedPerson, "when destroying a targeted person" do

  before(:each) do
    @targeted_offer   = TCFactory.create(:targeted_offer, :start_date => Time.now - 40.days, :expiration_date => Time.now + 10.days, :status=>"Inactive")
    @product          = Product.where(name: "1 Year Album", country_website_id: 1, status: "Active").first
    @targeted_product = TargetedProduct.create(:targeted_offer=>@targeted_offer, :product=>@product, :price_adjustment=>10, :price_adjustment_type=>'percentage_off')
    @targeted_offer.update(:status=>"Active")
    @person           = TCFactory.create(:person)
    @targeted_person  = TargetedPerson.create!(:targeted_offer=>@targeted_offer, :person=>@person)
    @album            = create_ready_to_purchase_album
  end

  context("when the targeted offer is active") do

    it "should update the persons purchases that are affected by not having this targeted offer" do

      #Create a purchase
      Product.add_to_cart(@person, @album, @product)

      expect(@person.purchases.first.targeted_product_id).not_to be_nil
      discount_cents_before = @person.purchases.first.discount_cents

      @targeted_person.destroy

      expect(@person.purchases.reload.first.targeted_product_id).to be_nil
      expect(discount_cents_before).to be > @person.purchases.first.discount_cents

    end

    it "should leave paid purchases alone" do

      @album  = create_ready_to_purchase_album
      @album2 = create_ready_to_purchase_album

      #Create a purchase
      @product.set_targeted_product(@targeted_product)
      purchase = Product.add_to_cart(@person, @album, @product)
      purchase.update!(:paid_at=>Time.now)
      purchase1_discount_cents = purchase.discount_cents

      #Create a purchase that will remain unpaid
      purchase2 = Product.add_to_cart(@person, @album2, @product)
      expect(purchase2.discount_cents).to be > 0

      #Remove the person from the offer
      @targeted_person.destroy

      #Purchase 1 is paid and should not change
      expect(purchase.reload.discount_cents).to eq(purchase1_discount_cents)

      #Purchase 2 is unpaid and should be changed
      expect(purchase2.reload.discount_cents).to eq(0)
    end

    it "should leave price adjusted purchases alone" do

      @admin  = TCFactory.create(:person)
      @album  = create_ready_to_purchase_album

      #Create a purchase and adjust it
      @product.set_targeted_product(@targeted_product)
      purchase = Product.add_to_cart(@person, @album, @product)

      expect(purchase.discount_cents).to be > 0
      purchase.adjust_price(499, "Damn customers!", @admin)

      #Remove the person from the offer
      @targeted_person.destroy

      #Make sure it didn't change
      expect(purchase.reload.cost_cents).to eq(499)
    end

  end

  context "when the targeted offer is not active" do

    before(:each) do
      @targeted_offer.update(:status=>"Inactive")
    end

    it "should leave the persons purchases that are affected by not having this targeted offer alone" do

      #Create a purchase
      Product.add_to_cart(@person, @album, @product)

      expect(@person.purchases.first.targeted_product_id).to be_nil
      cost_cents_before = @person.purchases.first.cost_cents

      @targeted_person.destroy

      expect(@person.purchases.reload.first.targeted_product_id).to be_nil
      expect(cost_cents_before).to eq(@person.purchases.first.cost_cents)

    end
  end

end

describe TargetedPerson do

  describe "#to_csv" do
    before(:each) do
      @targeted_offer = TCFactory.create(:targeted_offer, :status=>"Inactive")
      TargetedProduct.create!(:product=>Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID), :targeted_offer=>@targeted_offer)
      @targeted_offer.update(:status=>"Active")

      3.times do
        TargetedPerson.create!(:targeted_offer=>@targeted_offer, :person=>TCFactory.create(:person))
      end
    end

    it "should export to csv" do
      expected = "Person ID,Person Name,Person Email\r\n"

      @targeted_offer.targeted_people.each do |tp|
        expected+="#{tp.person.id},#{tp.person.name},#{tp.person.email}\r\n"
      end

      expect(@targeted_offer.export_people_to_csv).to eq(expected)
    end
  end
end

describe TargetedPerson do
  before(:each) do
    person           = create(:person)
    targeted_offer   = create(:targeted_offer)
    targeted_product = create(:targeted_product, targeted_offer: targeted_offer)
    @targeted_person = create(:targeted_person, person: person, targeted_offer: targeted_offer)
  end

  describe "#has_used?" do
    context "when usage_count is 0" do
      it "returns false" do
        allow(@targeted_person).to receive(:usage_count).and_return(0)
        expect(@targeted_person.has_used?).to eq false
      end
    end

    context "when usage_count is greater than 0" do
      it "returns true" do
        allow(@targeted_person).to receive(:usage_count).and_return(1)
        expect(@targeted_person.has_used?).to eq true
      end
    end
  end
end
