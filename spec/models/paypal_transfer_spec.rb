# == Schema Information
#
# Table name: paypal_transfers
#
#  id                 :integer          not null, primary key
#  country_website_id :string(3)        not null
#  created_at         :datetime         default(NULL), not null
#  updated_at         :datetime         default(NULL), not null
#  transfer_status    :string(20)       default(""), not null
#  person_id          :integer          default(0), not null
#  payment_cents      :integer          default(0), not null
#  admin_charge_cents :integer          default(0), not null
#  currency           :string(3)        not null
#  paypal_address     :string(80)       default(""), not null
#  approved_by_id     :integer
#  suspicious_flags   :text(65535)
#  correlation_id     :string(255)
#
require "rails_helper"

describe PaypalTransfer, "Testing a paypal withdrawal" do
  it "should sent a confirmation email to the account email" do
    ActionMailer::Base.deliveries = []
    customer = create(:person, email: "customer@123.com")
    customer.person_balance.update_attribute(:balance, 100.0)
    paypal_transfer = PaypalTransfer.generate(customer, {'paypal_address'=> "123@3.com", paypal_address_confirmation: "123@3.com",
                      'password_entered' => "Testpass123!", 'payment_amount' => 50.00})
    expect(last_email.subject).to eq("TuneCore Withdrawal Confirmation")
    expect(last_email.to).to eq(["customer@123.com"])
  end

  it "should be successful" do
    customer = create(:person, email: "paypal_withdrawal@tunecore.com")
    customer.person_balance.update_attribute(:balance, 50.00)
    paypal_transfer = PaypalTransfer.generate(customer, {'paypal_address'=> "123@3.com", paypal_address_confirmation: "123@3.com",
                      'password_entered' => "Testpass123!", 'payment_amount' => 50.00})
    expect(customer.balance).to eq(0)
  end

  it "should error if current password is incorrect" do
    customer = create(:person, email: "paypal_withdrawal@tunecore.com")
    paypal_transfer = PaypalTransfer.generate(customer, {'paypal_address'=> "123@3.com", paypal_address_confirmation: "123@3.com",
                      'password_entered' => "lol", 'payment_amount' => 50.00})
    expect(paypal_transfer.errors[:base]).not_to be_empty
  end

  it "should strip whitespace from emails" do
    customer = create(:person, email: 'paypal_withdraw@tunecore.com')
    customer.person_balance.update_attribute(:balance, 50.00)
    paypal_transfer = PaypalTransfer.generate(customer, {'paypal_address'=> " 123@3.com ", paypal_address_confirmation: " 123@3.com", 'password_entered' => "Testpass123!", 'payment_amount' => 50.00})

    expect(customer.balance).to eq(0)
    expect(paypal_transfer.paypal_address).to eq(" 123@3.com ".strip)
    expect(paypal_transfer.paypal_address_confirmation).to eq(paypal_transfer.paypal_address)
  end

  def last_email
    ActionMailer::Base.deliveries.last
  end
end

describe PaypalTransfer do
  it "should limit US withdrawals to 10k" do
    @person = create(:person)

    @paypal_transfer = build(:paypal_transfer, person: @person, current_balance_cents: 1000000)
    expect(@paypal_transfer).to be_valid

    @paypal_transfer.payment_amount = "12,000"
    expect(@paypal_transfer).not_to be_valid
    expect(@paypal_transfer.errors[:withdrawal_amount]).not_to be_blank

    @paypal_transfer.payment_amount = "10,001"
    expect(@paypal_transfer).not_to be_valid
    expect(@paypal_transfer.errors[:withdrawal_amount]).not_to be_blank

    @paypal_transfer.payment_amount = "10,000"
    expect(@paypal_transfer).to be_valid
    expect(@paypal_transfer.errors[:withdrawal_amount]).to be_blank

    @paypal_transfer.payment_amount = "00.01"
    expect(@paypal_transfer).to be_valid
    expect(@paypal_transfer.errors[:withdrawal_amount]).to be_blank

    @paypal_transfer.payment_amount = "00.00"
    expect(@paypal_transfer).not_to be_valid
    expect(@paypal_transfer.errors[:withdrawal_amount]).not_to be_blank
  end

  it "should limit CA withdrawals to 12.5k" do
    @canadian = create(:person, country: "CA", postal_code: 'E4Y2E4' )

    @paypal_transfer = build(:paypal_transfer, person: @canadian, current_balance_cents: 1250000)
    expect(@paypal_transfer).to be_valid

    @paypal_transfer.payment_amount = "13,000"
    expect(@paypal_transfer).not_to be_valid
    expect(@paypal_transfer.errors[:withdrawal_amount]).not_to be_blank

    @paypal_transfer.payment_amount = "12,501"
    expect(@paypal_transfer).not_to be_valid
    expect(@paypal_transfer.errors[:withdrawal_amount]).not_to be_blank

    @paypal_transfer.payment_amount = "12,000"
    expect(@paypal_transfer).to be_valid
    expect(@paypal_transfer.errors[:withdrawal_amount]).to be_blank

    @paypal_transfer.payment_amount = "00.01"
    expect(@paypal_transfer).to be_valid
    expect(@paypal_transfer.errors[:withdrawal_amount]).to be_blank

    @paypal_transfer.payment_amount = "00.00"
    expect(@paypal_transfer).not_to be_valid
    expect(@paypal_transfer.errors[:withdrawal_amount]).not_to be_blank
  end

  context "when using workflow-activerecord" do
    let(:pending_status)    { "pending" }
    let(:processing_status) { "processing" }
    let(:completed_status)  { "completed" }
    let(:cancelled_status)  { "cancelled" }
    let(:failed_status)     { "failed" }

    describe '#ensure_state_change_is_workflow_compatible' do
      let(:paypal_transfer) { create(:paypal_transfer, transfer_status: pending_status ) }

      context 'when transfer_status change is blank' do

        it 'does not raise an error' do
          expect { paypal_transfer.touch }.not_to raise_error
        end
      end

      context 'when next_transfer_status is in legal_transition_states' do

        it 'does not raise an error' do
          expect { paypal_transfer.update(transfer_status: processing_status) }.not_to raise_error
        end
      end

      context 'when next_transfer_status is not in legal_transition_states' do

        it 'raises an error' do
          expect { paypal_transfer.update(transfer_status: completed_status)}
          .to raise_error(RuntimeError)
        end
      end
    end

    context "when in the 'pending' transfer_status" do

      let(:paypal_transfer_pending_admin_approval) { create(:paypal_transfer, transfer_status: pending_status ) }

      it "transfers to the 'processing' status upon the event, 'mark_as_processing'" do
        paypal_transfer_pending_admin_approval.mark_as_processing!

        expect(paypal_transfer_pending_admin_approval.transfer_status).to eq(processing_status)
      end

      it "transfers to the 'cancelled' status upon the event, 'mark_as_cancelled'" do
        paypal_transfer_pending_admin_approval.mark_as_cancelled!

        expect(paypal_transfer_pending_admin_approval.transfer_status).to eq(cancelled_status)
      end

      it "throws a Workflow::NoTransitionAllowed error upon the event, 'mark_as_completed'" do
        expect{paypal_transfer_pending_admin_approval.mark_as_completed!}
         .to raise_error(Workflow::NoTransitionAllowed)

         expect(paypal_transfer_pending_admin_approval.transfer_status).to eq(pending_status)
      end

      it "throws a Workflow::NoTransitionAllowed error upon the event, 'revert_to_pending'" do
        expect{paypal_transfer_pending_admin_approval.revert_to_pending!}
         .to raise_error(Workflow::NoTransitionAllowed)

         expect(paypal_transfer_pending_admin_approval.transfer_status).to eq(pending_status)
      end

      it "throws a Workflow::NoTransitionAllowed error upon the event, 'mark_as_completed'" do
        expect{paypal_transfer_pending_admin_approval.mark_as_completed!}
         .to raise_error(Workflow::NoTransitionAllowed)

         expect(paypal_transfer_pending_admin_approval.transfer_status).to eq(pending_status)
      end

      it "throws a Workflow::NoTransitionAllowed error upon the event, 'mark_as_failed'" do
        expect{paypal_transfer_pending_admin_approval.mark_as_failed!}
         .to raise_error(Workflow::NoTransitionAllowed)

         expect(paypal_transfer_pending_admin_approval.transfer_status).to eq(pending_status)
      end
    end

    context "when in the 'processing' transfer_status" do
      let(:paypal_transfer_being_processed_by_paypal_api) do
        create(:paypal_transfer, transfer_status: pending_status ).tap do |paypal_transfer|
          paypal_transfer.mark_as_processing!
        end
      end

      it "transfers to the 'pending' status upon the event, 'revert_to_pending'" do
        paypal_transfer_being_processed_by_paypal_api.revert_to_pending!

        expect(paypal_transfer_being_processed_by_paypal_api.transfer_status).to eq(pending_status)
      end

      it "transfers to the 'completed' status upon the event, 'mark_as_completed'" do
        paypal_transfer_being_processed_by_paypal_api.mark_as_completed!

        expect(paypal_transfer_being_processed_by_paypal_api.transfer_status).to eq(completed_status)
      end

      it "transfers to the 'failed' status upon the event, 'mark_as_failed'" do
        paypal_transfer_being_processed_by_paypal_api.mark_as_failed!

        expect(paypal_transfer_being_processed_by_paypal_api.transfer_status).to eq(failed_status)
      end
    end

    context "when in the 'completed' transfer_status" do
      let(:paypal_transfer_successfully_payout) do
        create(:paypal_transfer, transfer_status: pending_status).tap do |paypal_transfer|
          paypal_transfer.mark_as_processing!
          paypal_transfer.mark_as_completed!
        end
      end

      it "throws a Workflow::NoTransitionAllowed error upon the event, 'revert_to_pending'" do
        expect{paypal_transfer_successfully_payout.revert_to_pending!}
         .to raise_error(Workflow::NoTransitionAllowed)

         expect(paypal_transfer_successfully_payout.transfer_status).to eq(completed_status)
      end

      it "throws a Workflow::NoTransitionAllowed error upon the event, 'mark_as_completed'" do
        expect{paypal_transfer_successfully_payout.mark_as_completed!}
         .to raise_error(Workflow::NoTransitionAllowed)

         expect(paypal_transfer_successfully_payout.transfer_status).to eq(completed_status)
      end

      it "throws a Workflow::NoTransitionAllowed error upon the event, 'mark_as_failed'" do
        expect{paypal_transfer_successfully_payout.mark_as_failed!}
         .to raise_error(Workflow::NoTransitionAllowed)

         expect(paypal_transfer_successfully_payout.transfer_status).to eq(completed_status)
      end
    end

    context "when in the 'cancelled' transfer_status" do
      let(:paypal_transfer_cancelled_by_admin) do
        create(:paypal_transfer, transfer_status: pending_status).tap do |paypal_transfer|
          paypal_transfer.mark_as_cancelled!
        end
      end

      it "throws a Workflow::NoTransitionAllowed error upon the event, 'revert_to_pending'" do
        expect{paypal_transfer_cancelled_by_admin.revert_to_pending!}
         .to raise_error(Workflow::NoTransitionAllowed)

         expect(paypal_transfer_cancelled_by_admin.transfer_status).to eq(cancelled_status)
      end

      it "throws a Workflow::NoTransitionAllowed error upon the event, 'mark_as_completed'" do
        expect{paypal_transfer_cancelled_by_admin.mark_as_completed!}
         .to raise_error(Workflow::NoTransitionAllowed)

         expect(paypal_transfer_cancelled_by_admin.transfer_status).to eq(cancelled_status)
      end

      it "throws a Workflow::NoTransitionAllowed error upon the event, 'mark_as_failed'" do
        expect{paypal_transfer_cancelled_by_admin.mark_as_failed!}
         .to raise_error(Workflow::NoTransitionAllowed)

         expect(paypal_transfer_cancelled_by_admin.transfer_status).to eq(cancelled_status)
      end
    end

    context "when in the 'failed' transfer_status" do
      let(:paypal_transfer_failed_by_api) do
        create(:paypal_transfer, transfer_status: pending_status ).tap do |paypal_transfer|
          paypal_transfer.mark_as_processing!
          paypal_transfer.mark_as_failed!
        end
      end

      it "throws a Workflow::NoTransitionAllowed error upon the event, 'revert_to_pending'" do
        expect{paypal_transfer_failed_by_api.revert_to_pending!}
         .to raise_error(Workflow::NoTransitionAllowed)

         expect(paypal_transfer_failed_by_api.transfer_status).to eq(failed_status)
      end

      it "throws a Workflow::NoTransitionAllowed error upon the event, 'mark_as_completed'" do
        expect{paypal_transfer_failed_by_api.mark_as_completed!}
         .to raise_error(Workflow::NoTransitionAllowed)

         expect(paypal_transfer_failed_by_api.transfer_status).to eq(failed_status)
      end

      it "throws a Workflow::NoTransitionAllowed error upon the event, 'mark_as_failed'" do
        expect{paypal_transfer_failed_by_api.mark_as_failed!}
         .to raise_error(Workflow::NoTransitionAllowed)

         expect(paypal_transfer_failed_by_api.transfer_status).to eq(failed_status)
      end
    end
  end

  describe "cancel" do

    before(:each) do
      @person = create(:person)
      @paypal_transfer = create(:paypal_transfer)
      @paypal_transfer
    end

    it "should rollback the fee and the payment in seperate transactions" do
      expect {
        @paypal_transfer.cancel
      }.to change(PersonTransaction, :count).by(2)
    end

    it "should return false if cannot rollback" do
      @paypal_transfer.approving_transfer(1)
      @paypal_transfer.mark_as_completed!
      expect(@paypal_transfer.cancel).to eq(false)
    end

    it "should return true if rollback occurs" do
      expect(@paypal_transfer.cancel).to eq(true)
    end

    it "should update the status to 'cancelled'" do
      @paypal_transfer.cancel
      expect(@paypal_transfer.reload.transfer_status).to eq("cancelled")
    end

    it "should work in a transaction" do
      allow(PersonTransaction).to receive(:create!).and_raise(ActiveRecord::Rollback )
      expect {
         expect(@paypal_transfer.cancel).to eq(false)
       }.not_to change(PersonTransaction, :count)
    end
  end

  describe "#set_suspicious_flags" do
    before(:each) do
      @customer = create(:person, email: 'paypal_withdraw@tunecore.com')
      @customer.person_balance.update_attribute(:balance, 150.00)
      @test_params = {'paypal_address'=> "123@3.com", paypal_address_confirmation: "123@3.com", 'password_entered' => "Testpass123!", 'payment_amount' => 50.00}
    end

    it "should not flag a first paypal withdrawal" do
      paypal_transfer = PaypalTransfer.generate(@customer, @test_params)
      expect(paypal_transfer.suspicious_flags["paypal_address"]).to eq(false)
    end

    it "should not flag a paypal withdrawal when no completed withdrawals exist" do
      paypal_transfer = PaypalTransfer.generate(@customer, @test_params)
      expect(paypal_transfer.suspicious_flags["paypal_address"]).to eq(false)
    end

    it "should not flag a paypal withdrawal when a completed withdrawal exists with same email" do
      paypal_transfer = PaypalTransfer.generate(@customer, @test_params)
      expect(paypal_transfer.suspicious_flags["paypal_address"]).to eq(false)
    end

    it "should flag a paypal withdrawal when a completed withdrawal exists with a different email" do
      paypal_transfer =
        PaypalTransfer.generate(@customer, @test_params).tap do |transfer|
          transfer.mark_as_processing!
          transfer.mark_as_completed!
        end

      new_transfer =
        PaypalTransfer.generate(
          @customer,
          @test_params.merge!(
            'paypal_address' => 'error@tunecore.com',
            paypal_address_confirmation: "error@tunecore.com"
          )
        )

      expect(new_transfer.suspicious_flags["paypal_address"]).to eq(true)
    end
  end

  describe "flagged?" do
    let(:customer) { create(:person, :with_balance, amount: 150) }
    let(:transfer_params) {
      {
        paypal_address: "test@email.com",
        paypal_address_confirmation: "test@email.com",
        password_entered: "Testpass123!",
        payment_amount: 50.00
      }
    }
    let(:transfer) { PaypalTransfer.generate(customer, transfer_params.with_indifferent_access) }

    it "returns false when the person's acount is active" do
      expect(transfer.flagged?).to eq(false)
    end

    it "returns true when the person's acount is locked" do
      customer.lock!("Credit Card Fraud", false)
      expect(transfer.flagged?).to eq(true)
    end

    it "returns true when the person's acount is marked suspicious" do
      customer.mark_as_suspicious!(create(:note), "Credit Card Fraud")
      expect(transfer.flagged?).to eq(true)
    end

    it "returns true when none of the completed transfers used the current paypal account" do
      transfer.mark_as_processing!
      transfer.mark_as_completed!
      new_transfer = PaypalTransfer.generate(
        customer,
        transfer_params.merge(
          paypal_address: "test1@email.com",
          paypal_address_confirmation: "test1@email.com"
        ).with_indifferent_access
      )
      expect(new_transfer.flagged?).to eq(true)
    end
  end

  describe "#withdrawal_account" do
    it "should return us credentials for us country website" do
      expect(PaypalTransfer.withdrawal_account(CountryWebsite.find_by(country: "US"))).to be_a_kind_of(PayPalBusiness::API)
      expect(PaypalTransfer.withdrawal_account(CountryWebsite.find_by(country: "US")).username).to eq("sb-pckge979661_api1.business.example.com")
    end

    it "should return ca credentials for ca country website" do
      expect(PaypalTransfer.withdrawal_account(CountryWebsite.find_by(country: "CA"))).to be_a_kind_of(PayPalBusiness::API)
      expect(PaypalTransfer.withdrawal_account(CountryWebsite.find_by(country: "CA")).username).to eq("sb-njz6y967451_api1.business.example.com")
    end

    it "should return default credentials for uk country website" do
      expect(PaypalTransfer.withdrawal_account(CountryWebsite.find_by(country: "UK"))).to be_a_kind_of(PayPalBusiness::API)
      expect(PaypalTransfer.withdrawal_account(CountryWebsite.find_by(country: "UK")).username).to eq("sandbox-uk-payouts_api1.tunecore.com")
    end

    it "should return default credentials for de country website" do
      expect(PaypalTransfer.withdrawal_account(CountryWebsite.find_by(country: "DE"))).to be_a_kind_of(PayPalBusiness::API)
      expect(PaypalTransfer.withdrawal_account(CountryWebsite.find_by(country: "DE")).username).to eq("sandbox-uk-payouts_api1.tunecore.com")
    end
  end
end

describe PaypalTransfer do
  describe "#save" do
    let(:customer) { create(:person, :with_balance, amount: 50_000, email: "paypal_transfer_1234@tunecore.com") }
    let(:paypal_transfer) { build(:paypal_transfer, person: customer, password_entered: "Testpass123!", current_balance_cents: 5000) }
    let(:login_event) { LoginEvent.create(person_id: customer.id) }

    before do
      allow(customer).to receive(:last_login_event) { login_event }
    end

    context "when a customer makes a paypal withdrawal" do
      before { paypal_transfer.save }

      it "associates the withdrawal to the customer's last login event" do
        expect(paypal_transfer.login_tracks.last.trackable).to eq paypal_transfer
        expect(paypal_transfer.login_tracks.last.login_event).to eq login_event
      end

      context "on creation" do
        it "creates a new version with the previous state (which is nil before creation)", versioning: true do
          expect(paypal_transfer.versions.count).to eq(1)
          expect(paypal_transfer.versions[0].object).to be_nil
        end
      end

      context "on update" do
        before { paypal_transfer.update(transfer_status: described_class::PROCESSING_STATUS) }

        it "creates a new version with the previous state", versioning: true do
          expect(paypal_transfer.versions.count).to eq(2)
          expect(paypal_transfer).to have_a_version_with(
            transfer_status: described_class::PENDING_STATUS
          )
        end
      end
    end
  end
end

# TODO: remove PaypalTransfer.do_mass_pay, replace with Transfers::ApproveService.call
describe PaypalTransfer do
  describe "#do_mass_pay" do
    let(:au_paypal_transfer_tc) { create(:paypal_transfer, :with_person, country_code: "AU", corporate_entity_id: 1) }
    let(:au_paypal_transfer_bi) { create(:paypal_transfer, :with_person, country_code: "AU", corporate_entity_id: 2) }
    let(:ca_paypal_transfer_tc) { create(:paypal_transfer, :with_person, country_code: "CA", corporate_entity_id: 1) }
    let(:ca_paypal_transfer_bi) { create(:paypal_transfer, :with_person, country_code: "CA", corporate_entity_id: 2) }
    let!(:admin) { Person.joins(:roles).find_by(roles: {name: "Admin"}) }

    context "when using PaypalMasspayService" do
      let(:client_double) { double("PayPalBusiness::API") }
      let(:success_response_double) { double(:response, ack: "Success", correlationID: "1234", timestamp: Time.now.utc.iso8601) }
      let(:error_response_double) { double(:response, ack: "Error", correlationID: "1234", timestamp: Time.now.utc.iso8601) }
      let(:error_object_double) { double(:error_object_double) }

      before do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(false)
        allow(FeatureFlipper).to receive(:show_feature?).with(:bi_transfer, any_args).and_return(true)

        allow(error_object_double).to receive(:longMessage).and_return("Error")
        allow(error_response_double).to receive(:errors).and_return(error_object_double)
        allow(client_double).to receive(:mass_pay).and_return(*client_responses)
        allow(PayPalBusiness::API).to receive(:new).and_return(client_double)
        allow(Admin::PaypalMasspayService)
      end

      context "when a request is successful" do
        let(:client_responses) { [success_response_double] }

        it "marks transactions as complete" do
          PaypalTransfer.do_mass_pay([au_paypal_transfer_tc], admin.id)

          expect(au_paypal_transfer_tc.reload.transfer_status).to eq("completed")
        end

        it "creates a new version with the previous state", versioning: true do
          PaypalTransfer.do_mass_pay([au_paypal_transfer_tc], admin.id)

          expect(au_paypal_transfer_tc).to have_a_version_with(
            transfer_status: described_class::PROCESSING_STATUS
          )
        end

        it "creates audit entries for processing and complete states" do
          expect { PaypalTransfer.do_mass_pay([au_paypal_transfer_tc], admin.id) }
            .to change { AuditEntry.count }.from(0).to(2)
        end
      end

      context "when a request fails" do
        let(:client_responses) { [error_response_double] }

        it "does not create another version due to being rolled back", versioning: true do
          expect { PaypalTransfer.do_mass_pay([au_paypal_transfer_tc], admin.id) }
            .not_to change { au_paypal_transfer_tc.versions.count }
          expect(au_paypal_transfer_tc.versions.count).to eq(1)
        end
      end


      context "when requests with different corporate entity IDs fail" do
        let(:client_responses) { [success_response_double, error_response_double] }

        it "rolls back only the failing API calls" do
          PaypalTransfer.do_mass_pay([au_paypal_transfer_tc, ca_paypal_transfer_tc], admin.id)

          expect(au_paypal_transfer_tc.reload.transfer_status).to eq("completed")
          expect(ca_paypal_transfer_tc.reload.transfer_status).to eq("pending")
        end

        it "returns an array of error messages" do
          msgs = PaypalTransfer.do_mass_pay([au_paypal_transfer_tc, ca_paypal_transfer_tc], admin.id)

          expect(msgs.size).to eq(1)
        end
      end


      context "with VAT applicable BI user" do
        let(:client_responses) { [success_response_double] }
        let(:outbound_vat_calculator) { double(:outbound_vat_calculator, fetch_vat_info: { tax_rate: "17.0" }) }

        before do
          allow(TcVat::OutboundVatCalculator).to receive(:new).and_return(outbound_vat_calculator)
          allow_any_instance_of(Person).to receive(:vat_applicable?).and_return(true)
        end

        it "creates an outbound invoice" do
          # TODO - replace
          PaypalTransfer.do_mass_pay([au_paypal_transfer_bi], admin.id)
          au_paypal_transfer_bi.reload

          expect(au_paypal_transfer_bi.transfer_status).to eql('completed')
          expect(au_paypal_transfer_bi.outbound_invoice).to be_present
          expect(au_paypal_transfer_bi.vat_tax_adjustment).to be_present
          expect(au_paypal_transfer_bi.vat_tax_adjustment.person_transactions.count).to eql(2)
        end
      end
    end
  end
end
