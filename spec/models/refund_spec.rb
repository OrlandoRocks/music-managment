require "rails_helper"

RSpec.describe Refund, type: :model do
  context "Associations" do
    it { should belong_to(:invoice) }
    it { should belong_to(:corporate_entity) }
    it { should belong_to(:refunded_by) }
  end

  context "Validations" do
    it { should validate_presence_of(:currency) }
    it { should validate_presence_of(:corporate_entity) }
    it { should validate_presence_of(:invoice) }
    it { should validate_presence_of(:refunded_by) }

    it "validates admin has refunds role" do
      non_refund_admin = create(:person, :with_role)
      refund_admin = create(:person, :with_role, role_name: Role::REFUNDS)

      subject.refunded_by = non_refund_admin
      subject.validate
      expect(subject.errors[:base]).to include("Admin doesn't have permission to refund")

      subject.refunded_by = refund_admin
      subject.validate
      expect(subject.errors[:base]).to_not include("Admin doesn't have permission to refund")
    end
  end

  describe ".last_generated_invoice_sequence" do
    it "returns last generated invoice sequence for corporate_entity" do
      refund = create(:refund)
      refund.update(invoice_sequence: 1)
      expect(Refund.last_generated_invoice_sequence(refund.corporate_entity))
        .to eq(refund.invoice_sequence)
    end
  end

  describe "#credit_note_invoice_number" do
    it "returns invoice sequence for refund with prefix" do
      refund = create(:refund)
      refund.update(invoice_sequence: 1)

      expect(refund.credit_note_invoice_number)
        .to eq("TC-CR-INV00001")
    end
  end

  describe "#euro_exchange_rate" do
    it "returns euro exchange rate" do
      fx_rate = create(
        :foreign_exchange_rate,
        target_currency: CurrencyCodeType::EUR
      )
      invoice = create(
        :invoice,
        :with_two_purchases,
        :settled,
        foreign_exchange_rate_id: fx_rate.id)

      refund = create(:refund, invoice: invoice)

      expect(refund.euro_exchange_rate).to eq(fx_rate.exchange_rate)
    end
  end
end
