require "rails_helper"

describe ForeignExchangeRate do
  describe ".current_exchange_rate" do
    it "should return the latest exchange rate by the currency of given country's currency as TARGET currency" do
      create(:foreign_exchange_rate, exchange_rate: 77, valid_from: Time.now, source_currency: 'USD', target_currency: 'INR')
      create(:foreign_exchange_rate, exchange_rate: 73, valid_from: Time.now - 1, source_currency: 'USD', target_currency: 'INR')
      country_id = Country.find_by(iso_code: 'IN').id
      forex_rate = ForeignExchangeRate.current_exchange_rate(country_id)
      expect(forex_rate.exchange_rate).to eq(77)
    end

    it "should return nil if there is no data for the given country's currency as TARGET currency" do
      create(:foreign_exchange_rate, exchange_rate: 78, valid_from: Time.now - 1, source_currency: 'USD', target_currency: 'INR')
      country_id = Country.find_by(iso_code: 'US').id
      forex_rate = ForeignExchangeRate.current_exchange_rate(country_id)
      expect(forex_rate).to be_nil
    end
  end

  describe ".current_rate" do
    it "should return the latest exchange rate by the currency of given country's currency by TARGET currency" do
      create(:foreign_exchange_rate, exchange_rate: 74.55, valid_from: Time.now - 3, source_currency: 'USD', target_currency: 'INR')
      create(:foreign_exchange_rate, exchange_rate: 78, valid_from: Time.now - 1, source_currency: 'USD', target_currency: 'INR')
      forex_rate = ForeignExchangeRate.current_rate(iso: "IN")
      expect(forex_rate.exchange_rate).to eq(78)
    end

    it "should return nil if there is no data for the given country's currency as TARGET currency" do
      create(:foreign_exchange_rate, exchange_rate: 78, valid_from: Time.now - 1, source_currency: 'USD', target_currency: 'INR')
      forex_rate = ForeignExchangeRate.current_rate(iso: "US")
      expect(forex_rate).to be_nil
    end
  end

  describe ".latest_by_currency" do
    before(:each) do
      create(:foreign_exchange_rate,
             exchange_rate: 0.8425,
             valid_from: Time.now,
             source_currency: CurrencyCodeType::USD,
             target_currency: CurrencyCodeType::EUR)
      create(:foreign_exchange_rate,
             exchange_rate: 0.9425,
             valid_from: Time.now - 1,
             source_currency: CurrencyCodeType::USD,
             target_currency: CurrencyCodeType::EUR)
    end

    it "should return latest exchange rate for given source & target currency" do
      rate = ForeignExchangeRate.latest_by_currency(source: CurrencyCodeType::USD,
                                                    target: CurrencyCodeType::EUR)

      expect(rate.exchange_rate).to eq(0.8425)
    end

    it "should return nil if there is no data for given source & target currency" do
      rate = ForeignExchangeRate.latest_by_currency(source: CurrencyCodeType::INR,
                                                    target: CurrencyCodeType::USD)

      expect(rate).to be_nil
    end
  end

  describe ".for_date" do
    let!(:old_rate) do
      create(:foreign_exchange_rate,
             exchange_rate: 0.0098,
             valid_from: 10.days.ago,
             source_currency: CurrencyCodeType::INR,
             target_currency: CurrencyCodeType::GBP)
    end

    let!(:new_rate) do
      create(:foreign_exchange_rate,
             exchange_rate: 0.0097,
             valid_from: 9.days.ago,
             source_currency: CurrencyCodeType::INR,
             target_currency: CurrencyCodeType::GBP)
    end

    it "returns exchange rate for given day" do
      subject = described_class.for_date(
                  source: CurrencyCodeType::INR,
                  target: CurrencyCodeType::GBP,
                  date: 9.days.ago
                )
      expect(subject.exchange_rate).to eq(new_rate.exchange_rate)
    end

    it "returns nil when there is no exchange_rate found" do
      subject = described_class.for_date(
                  source: CurrencyCodeType::INR,
                  target: CurrencyCodeType::GBP,
                  date: 8.days.ago
                )
      expect(subject).to be_nil
    end
  end

  describe "update_previous_rate_valid_till" do
    it "should update the old record valid_till so that this new forex is in effect" do
      first = create(:foreign_exchange_rate, exchange_rate: 74.55, valid_from: Time.now - 3, valid_till: nil)
      second = create(:foreign_exchange_rate, exchange_rate: 78, valid_from: Time.now - 1)
      expect(first.reload.valid_till).to eq(second.valid_from)
    end
  end
end
