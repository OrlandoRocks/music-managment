require "rails_helper"

describe TwoFactorAuth do
  before(:each) { Timecop.freeze }
  after(:each)  { Timecop.return }

  describe "#force_sms_auth" do
    it "forces sms if the notification method is app" do
      tfa = build(:two_factor_auth, notification_method: 'app')
      expect(tfa.valid?).to eq true
      expect(tfa.notification_method).to eq 'sms'
    end

    it "does nothing if the notification method is notapp" do
      tfa = build(:two_factor_auth, notification_method: 'call')
      expect(tfa.valid?).to eq true
      expect(tfa.notification_method).to eq 'call'
    end
  end


  describe "#active?" do
    it "returns false if never activated" do
      tfa = build(:two_factor_auth, activated_at: nil)
      expect(tfa.active?).to eq false
    end

    it "returns true if activated and never disabled" do
      tfa = build(:two_factor_auth, activated_at: Date.yesterday, disabled_at: nil)
      expect(tfa.active?).to eq true
    end

    it "returns false is activated but disabled after" do
      tfa = build(:two_factor_auth, activated_at: Date.yesterday, disabled_at: Date.today)
      expect(tfa.active?).to eq false
    end

    it "returns true if once disabled but activated since" do
      tfa = build(:two_factor_auth, activated_at: Date.today, disabled_at: Date.yesterday - 1.day)
      expect(tfa.active?).to eq true
    end
  end

  describe "#auth_success!" do
    it "updates last_verified_at timestamp" do
      tfa = create(
        :two_factor_auth,
        last_verified_at: Time.now - 24.hours
      )
      tfa.auth_success!
      expect(tfa.last_verified_at).to be_within(1.second).of Time.now
    end

    it "sets activated_at if its nil" do
      tfa = create(:two_factor_auth, :incomplete)
      expect(tfa.activated_at).to be_nil
      tfa.auth_success!
      expect(tfa.activated_at).to be_within(1.second).of Time.now
    end

    it "resets activated_at if its been disabled" do
      tfa = create(:two_factor_auth, :disabled)
      expect(tfa.active?).to eq false
      old_activated_at = tfa.activated_at
      tfa.auth_success!
      expect(tfa.activated_at).to be > old_activated_at
    end

    it "does NOT reset activated_at if it has not been disabled" do
      tfa = create(:two_factor_auth)
      expect(tfa.active?).to eq true
      orig_activated_at = tfa.activated_at
      tfa.auth_success!
      expect(tfa.activated_at).to eq orig_activated_at
    end
  end

  describe "#auth_failure!" do
    it "updates last_failed_at timestamp" do
      tfa = create(
        :two_factor_auth,
        last_failed_at: Time.now - 24.hours
      )
      tfa.auth_failure!
      expect(tfa.last_failed_at).to be_within(1.second).of Time.now
    end
  end

  describe "#disable!" do
    it "updates disabled_at with timestamp" do
      tfa = create(:two_factor_auth)
      expect(tfa.disabled_at).to be_nil
      tfa.disable!
      expect(tfa.disabled_at.to_s).to eq Time.now.to_s
    end
  end

  describe "#save_previous_preferences" do
    it "saves the notification method" do
      tfa = create(:two_factor_auth)
      tfa.save_previous_preferences
      expect(tfa).to have_attributes ({
        previous_notification_method: tfa.previous_notification_method,
      })
    end
  end

  describe "#restore_previous_preferences" do
    it "does nothing if all previous_preferences are nil" do
      nil_tfa = create(:two_factor_auth, previous_notification_method: nil)
      last_update_value = nil_tfa.updated_at

      nil_tfa.restore_previous_preferences

      expect(nil_tfa.updated_at).to eq last_update_value
      expect(nil_tfa).to have_attributes ({ previous_notification_method: nil })
    end

    it "does nothing if ANY of previous_preferences are nil" do
      nil_tfa = create(:two_factor_auth, previous_notification_method: nil)
      last_update_value = nil_tfa.updated_at

      nil_tfa.restore_previous_preferences

      expect(nil_tfa.updated_at).to eq last_update_value
    end

    it "does not fire an update if saved preferences are identical" do
      nil_tfa = create(:two_factor_auth, previous_notification_method: nil)
      nil_tfa.save_previous_preferences

      saved_tfa = nil_tfa
      last_update_value = saved_tfa.updated_at

      saved_tfa.restore_previous_preferences
      expect(saved_tfa.updated_at).to eq last_update_value
    end
  end

  describe "#within_authentication_window?" do
    it "is false when last_verified_at is nil" do
      tfa = create(
        :two_factor_auth,
        last_verified_at: nil,
      )
      expect(tfa.within_authentication_window?).to eq false
    end

    it "is true when last_verified_at is now" do
      tfa = create(
        :two_factor_auth,
        last_verified_at: Time.now
      )
      expect(tfa.within_authentication_window?).to eq true
    end

    it "is true when last_verified_at is 1 minute ago" do
      tfa = create(
        :two_factor_auth,
        last_verified_at: 1.minute.ago
      )
      expect(tfa.within_authentication_window?).to eq true
    end

    it "is false when last_verified_at is 3 minutes ago" do
      tfa = create(
        :two_factor_auth,
        last_verified_at: 3.minutes.ago
      )
      expect(tfa.within_authentication_window?).to eq false
    end
  end
end
