require "rails_helper"

describe TargetedOfferExclusion do

  describe "validation" do

    before(:each) do
      @targeted_offer_exclusion = TargetedOfferExclusion.new
    end

    it "should not allow exclude targeted offer to equal the targeted offer" do

      @targeted_offer_exclusion.targeted_offer_id = 1
      @targeted_offer_exclusion.exclude_targeted_offer_id = 1

      expect(@targeted_offer_exclusion).not_to be_valid
      expect(@targeted_offer_exclusion.errors.messages[:exclude_targeted_offer].size).to eq 1
    end

    it "should allow an exclusion of a different targeted offer" do
      @targeted_offer_exclusion.targeted_offer_id = 1
      @targeted_offer_exclusion.targeted_offer_id = 2

      expect(@targeted_offer_exclusion).to be_valid
      expect(@targeted_offer_exclusion.errors.messages[:exclude_targeted_offer]).to be_empty
    end
  end
end
