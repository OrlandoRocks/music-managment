require "rails_helper"

describe SubscriptionProduct do
  describe "#create_subscription_purchase" do
    before(:each) do
      @person = create(:person)
      @social_product = SubscriptionProduct.first
      @product = @social_product.product
    end

    it "should create a new social purchase" do
      purchase_count = SubscriptionPurchase.all.size
      SubscriptionProduct.create_subscription_purchase(@person, @product, "Social")
      expect(SubscriptionPurchase.all.size).to eq purchase_count + 1
    end

    it "should not create a purchase if product is in cart" do
      expect(@person).to receive(:has_subscription_product_in_cart_for?).and_return(true)
      purchase_count = SubscriptionPurchase.all.size
      SubscriptionProduct.create_subscription_purchase(@person, @product, "Social")
      expect(SubscriptionPurchase.all.size).to eq purchase_count
    end

    it "adds payment_channel of Tunecore" do
      purchase = SubscriptionProduct.create_subscription_purchase(@person, @product, "Social")
      expect(purchase.related.payment_channel).to eq "Tunecore"
    end
  end

  describe "#create_subscription_purchase_for_social" do
    before do
      @person = create(:person)
      @social_product = SubscriptionProduct.first
      @product = @social_product.product
    end

    it "should create a new purchase for social" do
      expect {
        SubscriptionProduct.create_subscription_purchase_for_social(@person, @product, "Social")
      }.to change { SubscriptionPurchase.count }.by(1)
    end
  end

  describe "::active_product_for_user" do
    context "person is an us user" do
      before(:each) do
        @person = create(:person)
      end

      it "returns an instance of the product" do
        expect(SubscriptionProduct.active_product_for_user(@person, "Social").class.name).to eq("Product")
      end

      it "returns the us social product" do
        expect(SubscriptionProduct.active_product_for_user(@person, "Social").country_website_id).to eq(1)
      end
    end

    context "person is a ca user" do
      it "returns the ca social product" do
        @person = create(:person, country: "CA")
        expect(SubscriptionProduct.active_product_for_user(@person, "Social").country_website_id).to eq(2)
      end
    end
  end

  describe "#subscription_products_by_country_website" do
    it "returns a list of social products for the given country website id" do
      products = SubscriptionProduct.subscription_products_by_country_website("Social", 2)
      expect(products.map(&:product_type)).to match_array(["annually", "monthly"])
      expect(products.map(&:display_name)).to match_array(["tc_social_monthly", "tc_social_annually"])
      expect(products.first).to respond_to(:price)
      expect(products.second).to respond_to(:price)
      expect(products.first).to respond_to(:currency)
      expect(products.second).to respond_to(:currency)
    end
  end
end
