require "rails_helper"

RSpec.describe TaxForm, type: :model do
  before do
    PersonEarningsTaxMetadatum.delete_all
    TaxForm.delete_all
    allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking).and_return(true)
    allow(FeatureFlipper).to receive(:show_feature?).with(:progressive_withholding_exemption).and_return(true)
  end

  describe "Associations" do
    subject { build(:tax_form) }
    it { should belong_to(:person) }
    it { should have_many(:pub_earnings_metadata) }
    it { should have_many(:dist_earnings_metadata) }
  end

  describe "Scopes" do
    describe ".active" do
      let(:person) { create(:person) }

      context "with W9 tax forms" do
        let!(:tax_form_1) { create(:tax_form, person_id: person.id) }
        let!(:tax_form_2) { create(:tax_form, person_id: person.id, expires_at: 1.year.ago) }

        it "returns all W9 forms, including expired forms" do
          expect(person.tax_forms.active).to contain_exactly(tax_form_1, tax_form_2)
        end
      end

      context "with W8 tax forms" do
        let!(:tax_form_1) { create(:tax_form, :w8, person_id: person.id) }
        let!(:tax_form_2) { create(:tax_form, :w8, person_id: person.id, expires_at: 1.year.ago) }

        it "returns only W8 forms that have not expired" do
          expect(person.tax_forms.active).to contain_exactly(tax_form_1)
        end
      end

      context "with both W8 and W9 forms" do
        let!(:tax_form_1) { create(:tax_form, person_id: person.id) }
        let!(:tax_form_2) { create(:tax_form, :w8, person_id: person.id) }
        let!(:tax_form_3) { create(:tax_form, person_id: person.id, expires_at: 1.year.ago) }
        let!(:tax_form_4) { create(:tax_form, :w8, person_id: person.id, expires_at: 1.year.ago) }

        it "returns W8 forms that have not expired and all W9 forms" do
          expect(person.tax_forms.active).to contain_exactly(tax_form_1, tax_form_2, tax_form_3)
        end
      end

      context ".secondary" do
        let!(:tax_form1) do
          create(:tax_form, :w9_individual, person: person)
        end

        let!(:tax_form2) do
          create(
            :tax_form, :w9_individual,
            person_id: person.id, payout_provider_config: PayoutProviderConfig.secondary_tax_form_config
          )
        end

        let!(:tax_form3) do
          create(
            :tax_form, :w9_entity,
            person_id: person.id, payout_provider_config: PayoutProviderConfig.secondary_tax_form_config
          )
        end

        it "returns tax-forms submitted under secondary-tax-form payoneer program" do
          expect(person.tax_forms.secondary).to contain_exactly(tax_form2, tax_form3)
        end
      end
    end
  end

  describe "Validations" do
    it { is_expected.to validate_presence_of(:tax_form_type_id) }
  end

  describe "Callbacks" do
    let(:person) { create(:person) }
    let(:exempt_person) {
      eperson = create(:person)
      create(:withholding_exemption, person: eperson, withholding_exemption_reason: WithholdingExemptionReason.find_by(name: :other))
      eperson
    }

    

    it "creates a metadata record if it doensn't exist" do
      tax_form = create(:tax_form, person_id: person.id)
      expect(PersonEarningsTaxMetadatum.where(dist_taxform: tax_form, person: person).count).to eq(1)
    end

    it "updates the existing metadata record" do
      tax_form1 = create(:tax_form, person_id: person.id)
      meta_data = PersonEarningsTaxMetadatum.find_by(person_id: person.id, tax_year: DateTime.current.year)
      expect(meta_data.dist_taxform_id).to eq(tax_form1.id)

      tax_form2 = create(:tax_form, person_id: person.id)
      expect(meta_data.reload.dist_taxform_id).to eq(tax_form2.id)
    end

    it "doesn't add metadata for non-payoneer provider" do
      create(:tax_form, person_id: person.id, provider: "non-payoneer")
      expect(PersonEarningsTaxMetadatum.find_by(person_id: person.id, tax_year: DateTime.current.year)).to be_nil
    end

    context "on save" do
      it "doesn't unblock withdrawal when a person is blocked for withdrawal" do
        tax_form = create(:tax_form, person_id: person.id)
        person.flags << Flag.find_by(category: Flag::WITHDRAWAL[:category], name: Flag::WITHDRAWAL[:name])
        tax_form.update(tax_form_type_id: 2)
        expect(person.flags).to include(Flag.find_by(category: Flag::WITHDRAWAL[:category], name: Flag::WITHDRAWAL[:name]))
      end


      xit "removes the person on the taxform from the withholding exemption list if the taxform is usable" do
        tax_form = create(:tax_form, person_id: exempt_person.id, payout_provider_config: PayoutProviderConfig.find_by(program_id: PayoutProviderConfig::TC_US_PROGRAM_ID, sandbox: true))
        tax_form.update(tax_form_type_id: 2)
        expect(tax_form.person.reload.exempt_from_withholding?).to be(false)
      end
    end
  end
end
