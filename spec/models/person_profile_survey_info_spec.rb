require "rails_helper"

describe PersonProfileSurveyInfo do

  before :each do
    @survey_info = FactoryBot.create(:person_profile_survey_info)
    $redis.flushdb
    FeatureFlipper.add_feature("Account Profile Survey")
    FeatureFlipper.update_feature(:account_profile_survey, 100, "")
  end

  describe "show_interstitial?" do
    it "should return true if the user has never seen the interstitial" do
      expect(@survey_info.show_interstitial?).to eq(true)
    end

    it "should return true if the user has not seen the interstitial for 30 days" do
      @survey_info.update(last_visited_at: Time.now - 31.days)
      expect(@survey_info.show_interstitial?).to eq(true)
    end

    it "should return false if the user has seen the interstitial within the past 30 days" do
      @survey_info.update(last_visited_at: Time.now - 29.days)
      expect(@survey_info.show_interstitial?).to eq(false)
    end

    it "should return false if the user has answered the first question" do
      @survey_info.update(segment_question_response: "I'm in a band or duo.")
      expect(@survey_info.show_interstitial?).to eq(false)
    end
  end

  describe "already_viewed?" do
    it "should return true if already viewed" do
      @survey_info.update(last_visited_at: Time.now - 31.days)
      expect(@survey_info.already_viewed?).to eq(true)
    end

    it "should return false if not already viewed" do
      expect(@survey_info.already_viewed?).to eq(false)
    end
  end

  describe "restart_survey" do
    it "should blank out the survey token and segment response" do
      token = @survey_info.get_survey_token
      @survey_info.set_segment_response "I'm in a band or duo."
      expect(@survey_info.survey_token).to eq(token)
      expect(@survey_info.segment_question_response).to eq("I'm in a band or duo.")

      @survey_info.restart_survey
      expect(@survey_info.survey_token).to eq(nil)
      expect(@survey_info.segment_question_response).to eq(nil)
    end
  end

  after :each do
    $redis.flushdb
  end
end
