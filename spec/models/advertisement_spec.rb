require "rails_helper"

describe Advertisement do
  context "person with a plan" do
    it "returns no ads" do
      person = create(:person)
      create(:person_plan, person: person, plan: Plan.first)
      expect(Advertisement.for_person(person, "Album")).to eq []
    end
  end
end
