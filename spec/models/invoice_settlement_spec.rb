require "rails_helper"

describe InvoiceSettlement do
  
  describe "#settled_by_payments_os?" do
    it "should return true if the source is referring to Payments OS transaction" do
      payments_os_transaction = create(:payments_os_transaction)
      settlement = create(:invoice_settlement, source: payments_os_transaction)
      expect(settlement.settled_by_payments_os?).to be_truthy
    end

    it "should return for all the other types of transactions" do
      person_transaction = create(:person_transaction)
      settlement = create(:invoice_settlement, source: person_transaction)
      expect(settlement.settled_by_payments_os?).to be_falsey
    end
  end
end