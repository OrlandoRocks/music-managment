require "rails_helper"

describe Notification do
  context "when a notification is a targeted notification" do
    context "can inherit elements from the targeted notification" do
      it "uses the text from the targeted notification if text blank" do
        targeted = build(:targeted_notification)
        notification = build(:notification, targeted_notification: targeted, text: nil)
        expect(notification.text).to eq targeted.text
      end

      it "uses the title from the targeted notification if title blank" do
        targeted = build(:targeted_notification)
        notification = build(:notification, targeted_notification: targeted, title: nil)
        expect(notification.title).to eq targeted.title
      end

      it "uses the url from the targeted notification if url blank" do
        targeted = build(:targeted_notification)
        notification = build(:notification, targeted_notification: targeted, url: nil)
        expect(notification.url).to eq targeted.url
      end

      it "uses the link text from the targeted notification if link text blank" do
        targeted = build(:targeted_notification)
        notification = build(:notification, targeted_notification: targeted, link_text: nil)
        expect(notification.link_text).to eq targeted.link_text
      end
    end
  end

  context "when taking action on a notification" do
    let(:notification) { build(:notification) }

    it "sets first_clicked" do
      expect(notification.first_clicked).to be_blank
      notification.mark_actioned
      expect(notification.first_clicked).not_to be_blank
    end

    it "does not set first_clicked again if already set" do
      Timecop.freeze(Time.current) do
        first_clicked_at = 1.week.ago
        notification.update_attribute(:first_clicked, first_clicked_at)
        notification.mark_actioned
        expect(notification.first_clicked).to be_within(1.second).of first_clicked_at
      end
    end

    it "increments click count" do
      expect { notification.mark_actioned }.to change { notification.click_count }.from(nil).to(1)
      expect { notification.mark_actioned }.to change { notification.click_count }.by(1)
    end

    it "sets last clicked every time it's clicked" do
      Timecop.freeze(Time.current) do
        notification.mark_actioned

        expect(notification.last_clicked).to be_within(1.second).of Time.current
      end
    end
  end

  context "when marking as archived" do
    let(:notification) { build(:notification) }

    describe "#mark_archived" do
      it "sets first_archived" do
        Timecop.freeze(Time.current) do
          notification.mark_archived

          expect(notification.first_archived).to be_within(1.second).of Time.current
        end
      end

      it "does not set first_archived again if already set" do
        Timecop.freeze(Time.current) do
          first_archived_at = 1.week.ago
          notification.update_attribute(:first_archived, first_archived_at)

          notification.mark_archived

          expect(notification.first_archived.to_i).to eq first_archived_at.to_i
        end
      end
    end
  end

  context "when deciding which notifications to show" do
    describe "#self.unseen_notification_count" do
      it "should count unseen notifications and global notifications" do
        notification = create(:notification)
        create(:targeted_notification)

        person = notification.person

        expect(Notification.unseen_notification_count(person)).to eq 2
      end
    end

    context "when getting global notifications" do
      let!(:targeted) { create(:targeted_notification, global: true) }
      let(:person) { create(:person) }

      describe "#self.unarchived_notifications" do
        it "creates notification for new global targeted notifications when the user was created before the targeted notification" do
          person.update_attribute(:created_on, 1.day.ago)

          expect { Notification.unarchived_notifications(person) }.to change(person.notifications, :count).by(1)
        end

        it "does not create notification when the user is created after the global targeted notification was created" do
          targeted.update_attribute(:created_at, 1.day.ago)

          expect { Notification.unarchived_notifications(person) }.to change(person.notifications, :count).by(0)
        end
      end
    end

    describe "#self.mark_seen" do
      it "should mark the collection of notifications seen" do
        person = create(:person)

        notifications = [create(:notification, person: person), create(:notification, person: person)]

        expect do
          Notification.mark_seen(person, notifications)
        end.to change(person.notifications.seen(true), :count).by(2)
      end
    end
  end
end
