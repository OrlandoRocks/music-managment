require 'rails_helper'

RSpec.describe OutboundInvoice, type: :model do
  let(:person) { FactoryBot.create(:person, :with_tc_social, country: "Luxembourg") }
  let(:payout_transfer) { FactoryBot.create(:payout_transfer, person: person) }

  describe "Associations" do
    it { should belong_to(:person) }
  end

  describe "Validations" do
    it "invoice_sequence should be unique for a particular user_invoice_prefix" do
      next_sequence = person.last_generated_outbound_sequence + 1

      OutboundInvoice.create(related: payout_transfer,
                             person: person,
                             invoice_sequence: next_sequence,
                             user_invoice_prefix: person.outbound_invoice_prefix,
                             invoice_date: Date.today)

      outbound_invoice = OutboundInvoice.new(related: payout_transfer,
                                             person: person,
                                             invoice_sequence: next_sequence,
                                             user_invoice_prefix: person.outbound_invoice_prefix,
                                             invoice_date: Date.today)

      expect(outbound_invoice).to_not be_valid
    end
  end
end
