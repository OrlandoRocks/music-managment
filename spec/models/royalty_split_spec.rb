# frozen_string_literal: true

require "rails_helper"

RSpec.describe RoyaltySplit, type: :model do
  describe "validation and associations" do
    subject { build :royalty_split }
    it {
      should have_many(:recipients).dependent(:destroy).inverse_of(:royalty_split).class_name("RoyaltySplitRecipient")
    }
    it { should have_many(:royalty_split_songs).dependent(:destroy) }
    it { should have_many(:songs).through(:royalty_split_songs) }
    it { should belong_to(:owner).class_name("Person") }

    it { should validate_uniqueness_of(:title).scoped_to(:owner_id) }
    it { should allow_value("New t!tle w/ chars").for(:title) }
    it { should_not allow_value(nil).for(:title) }
    it { should validate_presence_of(:owner) }
    it { should accept_nested_attributes_for(:recipients).allow_destroy(true) }
  end

  describe "valid record" do
    subject { create :royalty_split }
    it { should be_valid }
    it { expect(subject.owner_recipient).to be_present }
    it { expect(subject.owner_recipient).to be_valid }
  end

  describe ".create with nested attributes" do
    let(:people) { create_list :person, 2 }
    let(:person) { people.first }
    let(:split_recipients_data) do
      [
        { percent: 55.0, person: people[0] },
        { percent: 45.0, person: people[1] },
      ]
    end
    let!(:split) do
      described_class.create owner: person,
                             title: "Testing split",
                             recipients_attributes: split_recipients_data
    end
    let(:songs) { create_list :songs, 3, royalty_split: split }

    subject { split }

    it { should be_valid }
    it { expect(subject.recipients).to have(2).items }
    it { expect(subject.recipients.map(&:person)).to include(people[0], people[1]) }
  end

  describe ".new_with_owner and title attribute" do
    let(:owner_person) { create :person }
    subject { described_class.new_with_owner(owner_person) }
    before do
      subject.title = "Test"
    end
    it { should be_valid }
    it { should have_attributes(owner: owner_person) }

    it { expect(subject.owner).to eq(owner_person) }
    it do
      expect { subject.save! }.not_to raise_error
    end
    it do
      expect { subject.save! }.to change(RoyaltySplitRecipient, :count).by(1)
    end
    it do
      expect { subject.save! }.to change(RoyaltySplit, :count).by(1)
    end
    context "when saved" do
      let(:owner_recipient) { RoyaltySplitRecipient.where(person: owner_person, royalty_split: subject).first }
      before do
        subject.save!
      end
      it { expect(subject.recipients).to have_attributes(count: 1) }
      it { expect(subject.recipients).to include(owner_recipient) }
    end
  end

  describe ".recipients_attributes=" do
    let(:owner_person) { create :person }
    let(:owner_recipient_attributes) {
      attributes_for :royalty_split_recipient, person_id: owner_person.id, percent: 10
    }
    subject { build :royalty_split, owner: owner_person }
    context "with an existing user email" do
      let(:test_email) { "test567@example.com" }
      let!(:test_person) { create :person, email: test_email }
      let(:recipient_attributes) { attributes_for :royalty_split_recipient, email: test_email, percent: 90 }
      before do
        subject.recipients_attributes = [recipient_attributes, owner_recipient_attributes]
        subject.save
      end
      it { should be_valid }
      it "should change the email to nil and use the person_id" do
        expect(subject.recipients).to include(have_attributes(email: nil, person_id: test_person.id))
      end
    end
    context "with an empty string user id" do
      let(:recipient_attributes) { attributes_for :royalty_split_recipient, id: "", email: "hi@test", percent: 90 }
      before do
        subject.recipients_attributes = [recipient_attributes, owner_recipient_attributes]
      end
      it { should be_valid }
      it "should change the id to nil" do
        expect(subject.recipients).to include(have_attributes(email: "hi@test", id: nil))
      end
    end
  end

  describe ".songs=" do
    context "with existing, un-split songs" do
      let(:owner) { create :person }
      let(:royalty_split) { create :royalty_split, owner: owner }
      let(:songs) { create_list :song, 3 }
      subject { royalty_split }
      before do
        subject.songs = songs
      end
      it { should be_valid }
      it { expect(subject).to have(3).songs }
      it { expect(subject).to have(3).royalty_split_songs }
    end
    context "with unsaved songs" do
      let(:owner) { create :person }
      let(:royalty_split) { create :royalty_split, owner: owner }
      let(:songs) { build_list :song, 3 }
      subject { royalty_split }
      before do
        subject.songs = songs
      end
      it { should be_valid }
      it { expect(subject).to have(3).songs }
      it { expect(subject).to have(3).royalty_split_songs }
    end
    describe "assigned new songs" do
      let(:owner) { create :person }
      let(:royalty_split) { create :royalty_split, owner: owner }
      let!(:royalty_split_songs) { create_list :royalty_split_song, 3, royalty_split: royalty_split }
      let(:songs) { create_list :song, 2 }
      subject { royalty_split }
      it { expect { subject.songs = songs }.to change { subject.songs.size }.from(3).to(2) }
      it { expect { subject.songs = songs }.not_to raise_error }
    end
  end

  describe ".associate_songs!" do
    context "with already-split songs" do
      let(:owner) { create :person }
      let(:royalty_split) { create :royalty_split, owner: owner }
      let(:royalty_split_songs) { create_list :royalty_split_song, 3 }
      let!(:songs) { royalty_split_songs.map(&:song) }
      subject { royalty_split }
      before do
        subject.associate_songs! songs
      end
      it { should be_valid }
      it { expect(subject).to have(3).songs }
      it { expect(subject).to have(3).royalty_split_songs }
    end

    context "when adding to songs" do
      let(:owner) { create :person }
      let(:royalty_split) { create :royalty_split, owner: owner }
      let!(:royalty_split_songs) { create_list :royalty_split_song, 3, royalty_split: royalty_split }
      let(:songs) { create_list :song, 2 }
      subject { royalty_split }
      it do
        expect { subject.associate_songs! songs }.to change { subject.songs.size }.from(3).to(5)
      end
    end
  end

  describe ".top_albums" do
    let(:apple_album) { create :album, :with_songs, number_of_songs: 3, name: "Apple" }
    let(:banana_album) { create :album, :with_songs, number_of_songs: 5, name: "Banana" }
    let(:cherry_album) { create :album, :with_songs, number_of_songs: 4, name: "Cherry" }
    subject { create :royalty_split }
    before do
      subject.associate_songs! apple_album.songs
      subject.associate_songs! [banana_album.songs.first]
      subject.associate_songs! cherry_album.songs.slice(0, 2)
    end
    it { expect(subject.top_albums.first).to eq(apple_album) }
    it { expect(subject.top_albums.second).to eq(cherry_album) }
    it { expect(subject.top_albums.third).to eq(banana_album) }
  end

  describe "validates :percent_sum_validated_to_be_100_on_owner with recipient_attributes" do
    let(:owner) { create :person }
    let(:royalty_split) { build :royalty_split, owner: owner }
    let(:owner_recipient_attributes) {
      attributes_for :royalty_split_recipient, :owner, percent: 51, person_id: owner.id
    }
    let(:recipient_attributes) { attributes_for :royalty_split_recipient, :with_email, percent: 52 }
    subject { royalty_split }
    before do
      subject.recipients_attributes = [owner_recipient_attributes, recipient_attributes]
      subject.valid?
    end
    it { should be_invalid }
    it { should have(1).errors }
  end

  describe "validates :percent_sum_validated_to_be_100_on_owner" do
    let(:owner) { create :person }
    let(:royalty_split) { build :royalty_split, owner: owner }
    let(:owner_recipient) { build :royalty_split_recipient, percent: 51, person_id: owner.id, email: nil }
    let(:recipient) { build :royalty_split_recipient, percent: 52 }
    subject { royalty_split }
    before do
      subject.recipients = [owner_recipient, recipient]
      subject.valid?
    end
    it { should be_invalid }
    it { should have(1).errors }
  end

  describe "prevent person to be added twice through email" do
    let(:email) { "fakeemail@domain.com" }
    let(:owner) { create :person, email: email }
    let(:royalty_split) { build :royalty_split, owner: owner }
    let(:owner_recipient) { build :royalty_split_recipient, percent: 50, person_id: owner.id, email: nil }
    let(:recipient) { build :royalty_split_recipient, percent: 50, email: email }
    subject { royalty_split }
    before do
      subject.recipients = [owner_recipient, recipient]
      subject.valid?
    end
    it { should be_invalid }
    it { should have(1).errors }
  end

  describe "check for is_active - not deactivated" do
    it "should be active if deactivated_at is not set" do
      owner = create(:person)
      royalty_split = build(:royalty_split, owner: owner)
      expect(royalty_split.active?).to be true
    end

    it "should be active if deactivated_at is set to a future date" do
      owner = create(:person)
      royalty_split =  build(:royalty_split, owner: owner, deactivated_at: Time.now + 3)
      expect(royalty_split.active?).to be true
    end
  end

  describe "check for is_active - deactivated" do
    let(:owner) { create :person }
    let(:royalty_split) { build :royalty_split, owner: owner, deactivated_at: Time.now - 2 }

    it "should not be active" do
      expect(royalty_split.active?).to be false
    end
  end
end
