require 'rails_helper'

describe NonTunecoreSong do
  describe "validations" do
    let(:subject) { build(:non_tunecore_song) }

    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:non_tunecore_album_id) }
  end

  describe "#update_album" do
    let(:ntc_album) { create(:non_tunecore_album) }
    let(:ntc_song) { create(:non_tunecore_song, non_tunecore_album: ntc_album) }
    let(:new_album_name) { 'skalbum' }

    context "no changes to name" do
      it "should return without any changes" do
        return_value = ntc_song.update_album(ntc_album.name)
        expect(return_value).to eq(nil)
        expect(ntc_song.non_tunecore_album_id).to eq(ntc_album.id)
      end
    end

    context "album exists with the same name" do
      let!(:existing_album) do
        create(:non_tunecore_album, name: new_album_name, composer: ntc_album.composer)
      end

      it "should update album id to the existing album" do
        ntc_song.update_album(new_album_name)

        expect(ntc_song.reload.non_tunecore_album_id).to eq(existing_album.id)
      end

      context "old album has no other songs" do
        it "should delete the old album" do
          ntc_song.update_album(new_album_name)

          expect(NonTunecoreAlbum.find_by(id: ntc_album.id)).to be_nil
        end
      end
    end

    context "album does not exist with the same name" do
      context "old album has other songs" do
        before { create(:non_tunecore_song, non_tunecore_album: ntc_album) }

        it "should create new album and map the song to that album" do
          expect {
            ntc_song.update_album(new_album_name)
          }.to change{ NonTunecoreAlbum.count }.by(1)
        end
      end

      context "old album has no other songs" do
        it "should rename the current album as it is" do
          ntc_song.update_album(new_album_name)

          expect(ntc_album.reload.name).to eq(new_album_name)
        end
      end
    end
  end

  describe "#update_album_for_publishing_composer" do
    let(:ntc_album) { create(:non_tunecore_album) }
    let(:ntc_song) { create(:non_tunecore_song, non_tunecore_album: ntc_album) }
    let(:new_album_name) { 'skalbum' }
    let(:new_record_label) { 'label ipsum' }

    context "no changes to name" do
      it "should return without any changes" do
        return_value = ntc_song.update_album_for_publishing_composer(name: ntc_album.name, record_label: ntc_album.record_label)
        expect(return_value).to eq(nil)
        expect(ntc_song.non_tunecore_album_id).to eq(ntc_album.id)
      end
    end

    context "album exists with the same name and record label" do
      let!(:existing_album) do
        create(:non_tunecore_album, name: new_album_name, record_label: new_record_label, publishing_composer: ntc_album.publishing_composer)
      end

      it "should update album id to the existing album" do
        ntc_song.update_album_for_publishing_composer(name: new_album_name, record_label: new_record_label)

        expect(ntc_song.reload.non_tunecore_album_id).to eq(existing_album.id)
      end

      context "old album has no other songs" do
        it "should delete the old album" do
          ntc_song.update_album_for_publishing_composer(name: new_album_name, record_label: new_record_label)

          expect(NonTunecoreAlbum.find_by(id: ntc_album.id)).to be_nil
        end
      end
    end

    context "album does not exist with the same name and record_label" do
      context "old album has other songs" do
        before { create(:non_tunecore_song, non_tunecore_album: ntc_album) }

        it "should create new album and map the song to that album" do
          expect {
            ntc_song.update_album_for_publishing_composer(name: new_album_name, record_label: new_record_label)
          }.to change{ NonTunecoreAlbum.count }.by(1)
        end
      end

      context "old album has no other songs" do
        it "should rename the current album as it is" do
          ntc_song.update_album_for_publishing_composer(name: new_album_name, record_label: new_record_label)

          expect(ntc_album.reload.name).to eq(new_album_name)
        end
      end
    end
  end
end
