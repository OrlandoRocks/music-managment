require "rails_helper"

class FakeTransport
  def post person
    Struct.new(:body).new( {token: person.user_id + person.email}.to_json )
  end
end

describe CashAdvances::Registration do
  it "retrieves a token from the advance service" do
    CashAdvancesSpecPerson = Struct.new(:name, :email, :user_id)
    person = CashAdvancesSpecPerson.new("A. User", "auser@example.com", "12345")

    token = CashAdvances::Registration.register(person, FakeTransport.new)

    expect(token).to eq "12345auser@example.com"
  end
end
