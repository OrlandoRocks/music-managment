require "rails_helper"

describe LoginEvent do
  let(:person) { build_stubbed(:person) }

  describe "validates_presence_of person" do
    it "is valid with a person" do
      expect(LoginEvent.new(person: person).valid?).to be true
    end

    it "is invalid without a person" do
      expect(LoginEvent.new.invalid?).to be true
    end
  end

  it "is valid with an ip address" do
    expect(LoginEvent.new(person: person, ip_address: '127.0.0.1').valid?).to eq(true)
  end

  it "is valid with a user_agent" do
    user_agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36"
    expect(LoginEvent.new(person: person, user_agent: user_agent).valid?).to eq(true)
  end

  describe "geolocates a user after create" do
    it "enqueues a job" do
      login_event = build(:login_event, person: person, ip_address: "100.37.214.12")

      expect(LoginEventGeocodeWorker).to receive(:perform_async)

      login_event.save!
    end
  end

  describe ".matching_previous_logins" do
    before(:each) do
      @event_1 = create(:login_event, person: person, subdivision: "state_1", city: "city_1", country: "country_1")
      @event_2 = create(:login_event, person: person, subdivision: "state_2", city: "city_2", country: "country_2")
      @event_3 = create(:login_event, person: person, subdivision: "state_1", city: "city_1", country: "country_1")
      @event_4 = create(:login_event, person: person, subdivision: "state_4", city: "city_4", country: "country_4")
      @event_5 = create(:login_event, person: person, subdivision: "state_5", city: "city_5", country: "country_5")
    end

    it "returns all matching events from past n events" do
      sample_event = create(:login_event, person: person, subdivision: "state_1", city: "city_1", country: "country_1")

      expect(LoginEvent.matching_previous_logins(sample_event, 5)).to contain_exactly(@event_1, @event_3)
      expect(LoginEvent.matching_previous_logins(sample_event, 2)).to be_empty
    end

    it "doesn't include the event being compared" do
      sample_event = create(:login_event, person: person, subdivision: "state_1", city: "city_1", country: "country_1")

      expect(LoginEvent.matching_previous_logins(sample_event, 5)).not_to include(sample_event)
    end
  end
end
