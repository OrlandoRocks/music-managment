require "rails_helper"

describe TaxWithholdings::IRSPayablesReport do
  let(:irs_transaction) do
    create(:irs_transaction, :with_irs_tax_withholdings_and_person_transactions, status: :reported)
  end

  before do
    allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(true)
  end

  describe "#filename" do
    subject(:report) { described_class.new(irs_transaction: irs_transaction) }
    let(:irs_transaction_2020_w1) { create(:irs_transaction, status: :reported, created_at: Date.parse("2020-01-01")) }
    let(:irs_transaction_2020_w2) { create(:irs_transaction, status: :reported, created_at: Date.parse("2020-01-08")) }
    let(:irs_transaction_2021_w1) { create(:irs_transaction, status: :reported, created_at: Date.parse("2020-01-01")) }

    let!(:irs_tax_withholding_2020_w1) { create(:irs_tax_withholding, :with_person_transaction, irs_transaction: irs_transaction_2020_w1, created_at: irs_transaction_2020_w1.created_at) }
    let!(:irs_tax_withholding_2020_w2) { create(:irs_tax_withholding, :with_person_transaction, irs_transaction: irs_transaction_2020_w2, created_at: irs_transaction_2020_w2.created_at) }
    let!(:irs_tax_withholding_2021_w1) { create(:irs_tax_withholding, :with_person_transaction, irs_transaction: irs_transaction_2021_w1, created_at: irs_transaction_2021_w1.created_at) }

    it "returns a unique filename by irs_transaction" do
      filename1 = described_class.new(irs_transaction: irs_transaction_2020_w1).filename
      filename2 = described_class.new(irs_transaction: irs_transaction_2020_w2).filename
      filename3 = described_class.new(irs_transaction: irs_transaction_2021_w1).filename

      expect([filename2, filename3]).not_to include(filename1)
      expect([filename1, filename3]).not_to include(filename2)
      expect([filename1, filename2]).not_to include(filename3)
    end
  end

  describe "#generate" do
    subject(:report) { described_class.new(irs_transaction: irs_transaction) }

    context "when not running async" do
      it "calls TaxWithholdings::Reporting::IRSPayablesReportWorker to generate a report" do
        double = double("worker")
        allow(TaxWithholdings::Reporting::IRSPayablesReportWorker).to receive(:new).and_return(double)
        expect(double).to receive(:perform)
        report.generate(async: false)
      end
    end

    context "when running async" do
      it "calls TaxWithholdings::Reporting::IRSPayablesReportWorker to generate a report" do
        expect(TaxWithholdings::Reporting::IRSPayablesReportWorker).to receive(:perform_async)
        report.generate
      end
    end
  end

  describe "#fetch" do
    subject(:report) { described_class.new(irs_transaction: irs_transaction) }
    let(:rds_double) { double("RakeDatafileService") }
    let(:rds_path) { Rails.root.join("tmp", "development") }
    let(:csv_filename) { report.filename }
    let(:csv_file) do
      fullname = File.join(rds_path, csv_filename)
      CSV.open(fullname, "wb") do |csv|
        csv << %w[person_id person_email total_tax_withholding_amount currency]
        csv << %w[123 test@tunecore.com 0.2891796 USD]
      end
      File.open(fullname)
    end

    before do
      allow(rds_double).to receive(:local_file).and_return(csv_file)
    end

    context "when requested report has been previously generated" do
      it "returns saved CSV data" do
        expect(RakeDatafileService).to receive(:download).with(csv_filename).and_return(rds_double)
        report.fetch
      end
    end

    context "when requested report has not been generated" do
      it "raises an error" do
        allow(RakeDatafileService).to receive(:download)
          .with(csv_filename)
          .and_raise(AWS::S3::Errors::NoSuchKey.new(nil, nil, nil, "No Such Key"))
        expect { report.fetch }.to raise_error(AWS::S3::Errors::NoSuchKey)
      end
    end
  end
end
