require 'rails_helper'

RSpec.describe Achievement, type: :model do
  describe "association" do
    subject { build(:achievement) }

    it { should have_many(:tier_achievements) }
    it { should have_many(:tiers).through(:tier_achievements) }
  end

  describe "validations" do
    let(:subject) { build(:achievement) }

    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:points) }
    it { should validate_presence_of(:category) }
    it "should validate is_active is boolean" do
      subject.is_active = true
      expect(subject).to be_valid

      subject.is_active = false
      expect(subject).to be_valid

      subject.is_active = nil
      expect(subject).to_not be_valid
    end
  end
end
