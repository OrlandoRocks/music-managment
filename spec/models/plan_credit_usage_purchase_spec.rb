require "rails_helper"

describe PlanCreditUsagePurchase do
  let(:plan)  { create(:plan) }
  let(:purchase) { create(:purchase) }
  let(:plan_credit_usage) { create(:plan_credit_usage) }

  describe "self.log_purchase" do

    context "when all values are provided" do
      it "saves a new entry in the DB" do
        expect{ PlanCreditUsagePurchase.log_purchase(
          plan_id: plan.id,
          purchase_id: purchase.id,
          credit_usage_id: plan_credit_usage.id
        )}.to change{ PlanCreditUsagePurchase.count }.by(1)
      end
    end
  end

  describe "when given an incorrect value" do
    it "raises an airbrake" do
      expect(Tunecore::Airbrake).to receive(:notify)

      PlanCreditUsagePurchase.log_purchase(
        plan_id: plan.id,
        purchase_id: purchase.id,
        credit_usage_id: CreditUsage.last&.id.to_i + 1
      )
    end
  end
end
