require "rails_helper"

describe CmsSetAttribute do
  describe "define_method creates the correct methods for attached files" do
    context "there exists instance of cms_set_attribute with attr_name= 'background_img'" do
      let(:cms_set_attribute) { build_stubbed(:cms_set_attribute, attr_name: "background_img") }

      it "should respond to CmsSetAttribute#background_img" do
        expect(cms_set_attribute.respond_to?(:background_img_file_name)).to eq(true)
      end

      it "should respond to CmsSetAttribute#background_img=" do
        expect(cms_set_attribute.respond_to?(:background_img_file_name=)).to eq(true)
      end

      it "should respond to CmsSetAttribute#banner_img" do
        expect(cms_set_attribute.respond_to?(:banner_img_file_name)).to eq(true)
      end

      it "should respond to CmsSetAttribute#banner_img=" do
        expect(cms_set_attribute.respond_to?(:banner_img_file_name=)).to eq(true)
      end

      it "should respond to CmsSetAttribute#login_img" do
        expect(cms_set_attribute.respond_to?(:login_img_file_name)).to eq(true)
      end

      it "should respond to CmsSetAttribute#login_img=" do
        expect(cms_set_attribute.respond_to?(:login_img_file_name=)).to eq(true)
      end

      it "should respond to CmsSetAttribute#dashboard_img" do
        expect(cms_set_attribute.respond_to?(:dashboard_img_file_name)).to eq(true)
      end

      it "should respond to CmsSetAttribute#dashboard_img=" do
        expect(cms_set_attribute.respond_to?(:dashboard_img_file_name=)).to eq(true)
      end

      describe "#attached_file_name=" do
        it "should set the attr_value equal to the value passed in for the instance" do
          cms_set_attribute.background_img_file_name = "file name"
          expect(cms_set_attribute.attr_value).to eq("file name")
        end

        it "should not set anything if the attr_name does not match the setter_method" do
          cms_set_attribute.banner_img_file_name = "file name"
          expect(cms_set_attribute.attr_value).to_not eq("file name")
        end
      end

      describe "#attached_file_name" do
        it "should get the attr_value of the instance" do
          cms_set_attribute.attr_value = "file name"
          expect(cms_set_attribute.background_img_file_name).to eq("file name")
        end
      end
    end
  end
end
