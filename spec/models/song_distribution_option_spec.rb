require "rails_helper"
describe  SongDistributionOption do
  describe ".all_options" do
    it "returns a Hash of options" do
      expect(SongDistributionOption.all_options).to be_a(Hash)
    end
  end
end
