require "rails_helper"

describe Song do
  before(:all) do
    Song.update_all(content_fingerprint: nil)
  end

  before(:each) do
    @song = create(:song)
  end

  it "should be valid" do
    expect(@song).to be_valid
  end

  describe "associations" do
    subject { create :song }
    it { should belong_to(:album) }
    it { should have_one(:royalty_split).inverse_of(:songs) }
    it { should have_one(:royalty_split_song).inverse_of(:song) }
  end

  describe "#as_json" do
    def expected_hash(song)
      {
        song: {
          "id" => song.id,
          "album_id" => song.album.id,
          "name" => song.name,
          "artist_name" => song.artist_name,
          "track_num" => 1,
          "longer_than_90s" => nil
        }
      }
    end

    context "using default root option" do
      it "should produce valid json" do
        song = create(:song)

        result = song.as_json(
          only: [:id, :album_id, :name, :artist_name, :track_num, :longer_than_90s]
        )

        expect(result).to eq expected_hash(song)
      end
    end

    context "using root: false" do
      it "should produce valid json" do
        song = create(:song)

        result = song.as_json(
          root: false,
          only: [:id, :album_id, :name, :artist_name, :track_num, :longer_than_90s]
        )

        expect(result).to eq expected_hash(song)[:song]
      end
    end
  end

  describe "#available_for_streaming?" do
    it "returns true if no excluded from streaming option exists" do
      expect(@song.available_for_streaming?).to eq(true)
    end

    it "returns false if excluded from streaming option is and set to false" do
      SongDistributionOption.create(
        song_id: @song.id,
        option_name: :available_for_streaming,
        option_value: false
      )
      expect(@song.available_for_streaming?).to eq(false)
    end

    it "returns true if excluded from streaming option is and set to true" do
      SongDistributionOption.create(
        song_id: @song.id,
        option_name: :available_for_streaming,
        option_value: true
      )
      expect(@song.available_for_streaming?).to eq(true)
    end

    it "returns false when updating with value of nil" do
      @song.available_for_streaming = nil
      expect(@song.available_for_streaming?).to eq(false)
    end

    it "returns false when updating with value of 0" do
      @song.available_for_streaming = "0"
      expect(@song.available_for_streaming?).to eq(false)
    end

    it "returns true when updating with value of 1" do
      @song.available_for_streaming = "1"
      expect(@song.available_for_streaming?).to eq(true)
    end
  end

  describe "#valid_previously_released_at_date" do
    let(:song) { create(:song, :with_s3_asset) }

    context "when no date is passed" do
      it "does not enter the method and saves successfully" do
        expect(song.update(previously_released_at: nil)).to be_truthy
        expect(song).to be_valid
        expect(song.errors).to be_empty
      end
    end
  end

  describe "class methods" do
    describe "Song#missing_fingerprint" do
      context "when a song's album is not finalized" do
        it "should not be returned" do
          album = create(:album, finalized_at: nil)
          song = create(:song, album: album)
          missing_fingerprint_songs = Song.missing_fingerprint

          expect(missing_fingerprint_songs).not_to include song
        end
      end

      context "when a song's album is finalized but does not have an s3 asset" do
        it "should not be returned" do
          album = create(:album, finalized_at: Time.current)
          song = create(:song, album: album)
          missing_fingerprint_songs = Song.missing_fingerprint

          expect(missing_fingerprint_songs).not_to include song
        end
      end

      context "when a song's album is finalized and has an s3 asset" do
        it "should be returned" do
          s3_asset = create(:s3_asset)
          album = create(:album, finalized_at: Time.current)
          song = create(:song, album: album, s3_asset: s3_asset)

          missing_fingerprint_songs = Song.missing_fingerprint

          expect(missing_fingerprint_songs).to include song
        end
      end
    end

    describe "Song#all_fingerprinted_content" do
      before(:each) do
        @song.album.finalized_at = Time.now
        @song.album.save
        @second_song = create(:song, :with_s3_asset)
      end

      it "should not return songs if no fingerprinted content exists" do
        songs = Song.all_fingerprinted_content
        expect(songs).to match_array([])
      end

      it "should return one fingerprinted song" do
        s3_asset = create(:s3_asset)
        @song.s3_asset = s3_asset
        @song.content_fingerprint = Digest::MD5.hexdigest("This is a music file")
        @song.save!
        songs = Song.all_fingerprinted_content
        expect(songs).to match_array([@song])
      end

      it "should return two fingerprinted song" do
        s3_asset = create(:s3_asset)
        @song.s3_asset = s3_asset
        @song.content_fingerprint = Digest::MD5.hexdigest("This is a music file")
        @song.save!
        s3_asset = create(:s3_asset)
        @second_song.s3_asset = s3_asset
        @second_song.album.update(finalized_at: Time.now)
        @second_song.content_fingerprint = Digest::MD5.hexdigest("This is another music file")
        @second_song.save!
        songs = Song.all_fingerprinted_content
        expect(songs).to match_array([@song, @second_song])
      end
    end
  end
end

describe Song, "#streaming_url" do
  it "returns the result of calling the aws wrapper with the streaming bucket, key, and options" do
    song = build_stubbed(:song, album: build_stubbed(:album, id: 1), track_num: 1)
    response_url = "response_url"
    expires_in = 1.hour

    expect(AwsWrapper::S3).to receive(:read_url).with(
      bucket: "streaming.tunecore.com",
      key: "1/1.mp3",
      options: {
        expires: expires_in,
        port: 443
      }
    ).and_return(response_url)

    expect(song.streaming_url(expires_in: expires_in)).to eq(response_url)
  end
end

describe Song, "when validating optional isrcs" do

  before(:each) do
    @album = create(:album)
  end

  it "should not allow duplicate optional isrcs on the same album" do
    song = @album.songs.create(:name=>"BOOM", :optional_isrc=>"ABC123456789")
    expect(song).to be_valid

    song = @album.songs.create(:name=>"BAM", :optional_isrc=>"ABC123456789")
    expect(song.errors.messages[:optional_isrc].size).to eq 1
  end

  it "should allow duplicate optional isrcs on different albums" do
    @album2 = create(:album)
    song = @album.songs.create(:name=>"BOOM", :optional_isrc=>"ABC123456789")
    expect(song).to be_valid

    song = @album2.songs.create(:name=>"BAM", :optional_isrc=>"ABC123456789")
    expect(song).to be_valid
  end

  it "should not allow an optional isrc that is a tunecore isrc on another track" do
    song = @album.songs.create(:name => "Song1")
    expect(song.tunecore_isrc.blank?).to be_falsey
    song2 = @album.songs.create(:name => "Song2", :optional_isrc => song.tunecore_isrc)
    expect(song2.errors.messages[:optional_isrc].size).to eq 1
  end

  it "should allow optional isrcs to start with TC" do
    song = @album.songs.create(:name => "Song1")
    song.optional_isrc = song.tunecore_isrc

    expect(song.optional_isrc[0..1]).to eq("TC")
    expect(song.valid?).to eq(true)
  end

end

describe Song, "when it has artists with non-word characters" do
  before(:each) do
    @album = TCFactory.build_album(:creatives => [{"name"=>"Philip J. Fry", "role"=>"primary_artist"}])
    @album.songs << Song.new(:name => 'Black and White')
    @song = @album.songs.first
    @album.reload
    @song.reload
  end

  it "should only add one artist" do
    @song.add_featured_artist('Emily "Strange" Rotter')
    @song.save
    expect(@song.reload.creatives.size).to eq(1)
    expect(@song.creatives[0].name).to eq('Emily "Strange" Rotter')
    expect(@song.creatives[1]).to eq(nil)
    expect(@song.name).to eq('Black and White')
    expect(@album.reload.songs.size).to eq(1)
  end
end

describe Song, "when it has duplicate featuring artists" do
  before(:each) do
    @album = TCFactory.build_album(:creatives => [{"name"=>"Artist Name", "role"=>"primary_artist"}])
    @album.songs << Song.new(:name => 'Idol Dalliance', :creatives => [{"name"=>"Bobo Bagins", "role"=>"featuring"}, {"name"=>"Bobo Bagins", "role"=>"featuring"}], :album_id => @album.id)
    @song = @album.songs.first
  end

  it "should not create the song with duplicate artists" do
    expect(@song.reload.creatives.size).to eq(1)
    expect(@song.creatives[0].name).to eq("Bobo Bagins")
    expect(@song.creatives[1]).to eq(nil)
    expect(@song.name).to eq('Idol Dalliance')
    expect(@album.reload.songs.size).to eq(1)
  end

  it "should not add duplicate artists to a song" do
    @song.add_featured_artist('Bobo Bagins')
    @song.save
    expect(@song.reload.creatives.size).to eq(1)
    expect(@song.creatives[0].name).to eq("Bobo Bagins")
    expect(@song.creatives[1]).to eq(nil)
    expect(@song.name).to eq('Idol Dalliance')
    expect(@album.reload.songs.size).to eq(1)
  end
end

describe Song, "when it has a featured artist creative" do
  before(:each) do
    @album = TCFactory.build_album(:creatives => [{"name"=>"Artist Name", "role"=>"primary_artist"}])
    @album.songs << Song.new(:name => 'idol dalliance')
    @song = @album.songs.first
  end

  it "should not force the title of the track to include (feat. blank)" do
    @song.add_featured_artist('Bobo Bagins')
    @song.save
    expect(@song.name).to eq('Idol Dalliance')
  end
end

describe Song, "when creating a song and its track number" do
  it "should handle a null album" do
    album = TCFactory.create(:album)
    song = Song.new(TCFactory.song_defaults)
    song.album = album
    song.save!
    expect(song.track_number).to eq(1)
  end
end


describe Song, "when it has a featured artist creative and the album is single" do
  before(:each) do
    @album = FactoryBot.create(:single,
                              :creatives => [{"name"=>"artist", "role"=>"primary_artist"}],
                              :selected_artist_name => 'Artist Name',
                              :name => 'Idol Dalliance')
    @song = @album.songs.first
  end

   # This is the current behavior so this test is disabled.
   #
   # it "should not force the album title to include (feat.) if it is a single and allow_different_format is turned on" do
   #   @album.allow_different_format = true
   #   @album.save
   #   @song.reload
   #   @song.add_featured_artist('Bobo Bagins')
   #   @song.allow_different_format?.should be_truthy
   #   @song.save
   #   @song.name.should == 'Idol Dalliance'
   #   @album.name.should == 'Idol Dalliance - Single'
   # end
end

describe Song, "when the album also has a featured artist" do
  it "should not concatenate the albums featured arists and the songs featured arists" do
    @album = TCFactory.build_album(
      :creatives => [{"name"=>"Artist Name", "role"=>"primary_artist"}],
      :name => 'Idol Dalliance'
    )
    @album.songs << Song.new(:name => 'idol dalliance')
    @album.songs << Song.new(:name => 'idol dalliance 2')

    @song = @album.songs.first

    @album.add_featured_artist('Marcus')
    @album.add_featured_artist('Alex')
    @album.save!

    @song.reload

    @song.add_featured_artist('Taylor')
    @song.save!


    expect(@song.name).to eq('Idol Dalliance')

  end
end


describe Song, "when it has an optional isrc" do
  before(:each) do
    @album = TCFactory.build_album(:creatives => [{"name"=>"Artist Name", "role"=>"primary_artist"}])
    @album.songs << Song.new(:name => 'idol dalliance')
    @song = @album.songs.first
  end

  it "should make the optional isrc uppercased" do
    @song.optional_isrc = 'usabc1000001'
    @song.save
    expect(@song.optional_isrc).to eq('USABC1000001')
  end
end

describe Song, "attached to a various artist album" do
  before(:each) do
    @album = TCFactory.create( :album, :is_various => true, :name => 'Idol Dalliance')
    @song = @album.songs.create({"track_num"=>"1", "name"=>"idol dalliance", "creatives"=>[{"name"=>"Bob", "role"=>"primary_artist"}, {"name"=>"", "role"=>"featuring"}], "optional_isrc"=>"", "parental_advisory"=>"0", :album_id => @album.id})
  end

  it "should be attached to the album" do
    expect(@album.id).to eq(@song.album.id)
  end

  it 'should not be valid if there are no creatives' do
    @song2 = @album.songs.build(:name => 'song1')
    @song2.creatives=[{"name"=>"", "role"=>"primary_artist"},{"name"=>"", "role"=>"featuring"}]
    expect(@song2.valid?).to be_falsey
  end

  it 'should not be valid if there are no primary artists' do
    @song3 = @album.songs.build(:name => 'song2')
    @song3.creatives=[{"name"=>"", "role"=>"primary_artist"},{"name"=>"Bob", "role"=>"featuring"}]
    expect(@song3.valid?).to be_falsey
  end

  it 'should be valid if there are primary artists' do
    @song4 = @album.songs.create!(:name => 'song2', :creatives => [{"name"=>"Bob", "role"=>"primary_artist"},{"name"=>"", "role"=>"featuring"}], :album_id => @album.id )
    expect(@song4.valid?).to be_truthy
  end

  it 'should not allow updating if there are blank artists' do
    @song.update({"track_num"=>"1",
                             "name"=>"Hit Single",
                             "creatives"=>
                             [{"name"=>"", "role"=>"primary_artist"},
                              {"name"=>"Bob", "role"=>"featuring"}],
                             "optional_isrc"=>"",
                             "parental_advisory"=>"0"})
    expect(@song.save).to be_falsey

    # Reloading the song to make sure that the creative didn't get deleted
    @song.reload
    expect(@song.primary_artists).not_to be_blank
  end

  it 'should allow updating if there are artists' do
    @song.update({"track_num"=>"1",
                             "name"=>"Hit Single",
                             "creatives"=>
                             [{"name"=>"Bob", "role"=>"primary_artist"},
                              {"name"=>"Robin", "role"=>"featuring"}],
                             "optional_isrc"=>"",
                             "parental_advisory"=>"0"})
    expect(@song.save).to be_truthy

    # Reloading the song to make sure that the creative didn't get deleted
    @song.reload
    expect(@song.creatives.detect {|creative| creative['role'] == 'primary_artist'}).not_to be_blank
  end

  it 'should not titleize the primary artist' do
    @song.update({"track_num"=>"1",
                             "name"=>"Hit Single",
                             "creatives"=>
                             [{"name"=>"single artist", "role"=>"primary_artist"},
                              {"name"=>"Robin", "role"=>"featuring"}],
                             "optional_isrc"=>"",
                             "parental_advisory"=>"0"})
    expect(@song.save).to be_truthy
    expect(@song.creatives.size).to eq(2)
    expect(@song.primary_artists.first.name).to eq("single artist")
  end

  it 'should not allow blank artists' do
    expect {@song.creatives << Creative.new(:name => '', :role => "primary_artist")}.to raise_error(StandardError)
  end

  it 'should allow new artists' do
    expect {@song.creatives << Creative.new(:name => 'Robin', :role => "primary_artist")}.to change(@song.creatives, :count).by(1)
  end
end

describe Song, "attached to a non-various artist album" do
  before(:each) do
    @album = TCFactory.build_album(:is_various => false, :creatives => [{"name"=>"Bob", "role"=>"primary_artist"}], :name => 'Idol Dalliance')
    @song = @album.songs.build({"track_num"=>"", "name"=>"idol dalliance", "creatives"=>[{"name"=>"Bob", "role"=>"primary_artist"}, {"name"=>"", "role"=>"featuring"}], "optional_isrc"=>"", "parental_advisory"=>"0"})

    @album.songs << @song
  end

  it 'should be attached to the album' do
    expect(@album.id).to eq(@song.album.id)
  end

  it 'should be valid if there are no creatives' do
    @song2 = @album.songs.build(:name => 'song1' )
    @song.creatives =  [{"name"=>"", "role"=>"primary_artist"},{"name"=>"", "role"=>"featuring"}]
    expect(@song2.save).to be_truthy
  end

  it 'should be valid if there are primary artists' do
    @song3 = @album.songs.build(:name => 'song1')
    @song3.creatives = [{"name"=>"Bob", "role"=>"primary_artist"},{"name"=>"Robin", "role"=>"featuring"}]
    expect(@song3.save).to be_truthy
  end
end

describe Song, "#big_box_meta_data" do
  context "when there is a previous value" do
    it "returns the previous value" do
      song = build_stubbed(:song, :with_s3_asset)
      song.big_box_meta_data = "previous_value"
      allow(AwsWrapper::SimpleDb).to receive(:get_attributes).and_return("new_value")

      expect(song.big_box_meta_data).to eq("previous_value")
    end
  end

  context "with an s3_asset" do
    context "and when the asset has a uuid" do
      context "and when the asset has attributes" do
        it "sets and returns the attributes" do
          api_response = double("get_attributes_response")
          song = build_stubbed(:song, :with_s3_asset)

          allow(api_response).to receive(:empty?).and_return(false)

          expect(AwsWrapper::SimpleDb).to receive(:get_attributes)
            .with(domain: BIGBOX_ASSETS_SDB_DOMAIN, item: song.s3_asset.uuid)
            .and_return(api_response)

          expect(song.big_box_meta_data).to eq(api_response)
        end
      end

      context "and when the asset does not have attributes" do
        it "sets and returns nil" do
          api_response = double("get_attributes_response")
          song = build_stubbed(:song, :with_s3_asset)

          allow(api_response).to receive(:empty?).and_return(true)

          expect(AwsWrapper::SimpleDb).to receive(:get_attributes)
            .with(domain: BIGBOX_ASSETS_SDB_DOMAIN, item: song.s3_asset.uuid)
            .and_return(api_response)

          expect(song.big_box_meta_data).to eq(nil)
        end
      end
    end

    context "and when the asset does not have a uuid" do
      it "sets and returns nil" do
        song = build_stubbed(:song, s3_asset: build_stubbed(:s3_asset, uuid: nil))

        allow(AwsWrapper::SimpleDb).to receive(:get_attributes)

        expect(song.big_box_meta_data).to eq(nil)
      end
    end
  end

  context "without an s3_asset" do
    it "return nil" do
      song = build_stubbed(:song)

      allow(AwsWrapper::SimpleDb).to receive(:get_attributes)

      expect(song.big_box_meta_data).to eq(nil)
    end
  end
end

describe Song, "duration" do

  before(:each) do
    @song = TCFactory.build(:song)
    @song.s3_asset = nil
  end

  it "should return nil if pre-bigbox asset" do
    expect(@song.duration).to be_blank
  end

  it "should return 'duration' attribute from bigbox meta data" do
    @song.s3_asset = mock_model(S3Asset)
    expect(@song).to receive(:big_box_meta_data).at_least(1).times.and_return({"duration"=>["3600"]})
    expect(@song.duration).to eq(4)
  end

  it "should fail validation if 2 seconds or less" do
    ENV["VALIDATE_SONG_MINIMUM_DURATION"] = "true"
    allow(@song).to receive(:duration).and_return(144)
    allow(@song).to receive(:not_big_box_asset?).and_return(false)
    expect(@song.save).to be_falsey
  end

  it "should pass validation if 2 seconds or more" do
    ENV["VALIDATE_SONG_MINIMUM_DURATION"] = "true"
    allow(@song).to receive(:duration).and_return(2001)
    allow(@song).to receive(:not_big_box_asset?).and_return(false)
    expect(@song.save).to be_truthy
  end

  describe "#longer_than_90s" do
    context "when a song has value in duration_in_seconds" do
      context "when that value is more than 90" do
        it "returns true" do
          @song.update(duration_in_seconds: 100)

          expect(@song.longer_than_90s).to be_truthy
        end
      end

      context "when that value is equal to 90" do
        it "returns true" do
          @song.update(duration_in_seconds: 90)

          expect(@song.longer_than_90s).to be_truthy
        end
      end

      context "when that value is less than 90" do
        it "returns false" do
          @song.update(duration_in_seconds: 80)

          expect(@song.longer_than_90s).to be_falsey
        end
      end
    end
  end

end

describe Song do
  describe "titleizing" do
    before :each do
      @song = FactoryBot.create :song
    end

    context "with autoformatting enabled on the parent album" do

      it "titleizes" do
        @song.update(name: "jethro q. walrus")
        expect(@song.name).to eq "Jethro Q. Walrus"
      end

      it "does not adds featured artists, if present, to title" do
        creative = FactoryBot.create(:creative, creativeable: @song.album, role: "featuring")
        @song.album.reload
        @song.update(name: "Tarquin Fin-tim-lin-bin-whin-bim-lim-bus-stop-F'tang-F'tang-Ole-Biscuitbarrel")
        expect(@song.name).to eq "Tarquin Fin-Tim-Lin-Bin-Whin-Bim-Lim-Bus-Stop-F'tang-F'tang-Ole-Biscuitbarrel"
      end

      it "does save (feat). to the title when the title contains (feat)." do
        @song.update(name: "Blah (feat. Some Artist)")
        expect(@song.name).to eq("Blah (feat. Some Artist)")
      end

    end


    context "with autoformatting disabled on the parent album" do

      before :each do
        @song.album.update(allow_different_format: true)
      end

      it "doesn't titleize" do
        @song.update(name: "jethro q. walrus")
        expect(@song.name).to eq "jethro q. walrus"
      end

      it "doesn't add featured artists to title" do
        FactoryBot.create(:creative, creativeable: @song.album, role: "featuring")
        @song.album.reload
        @song.update(name: "Tarquin Fin-tim-lin-bin-whin-bim-lim-bus-stop-F'tang-F'tang-Ole-Biscuitbarrel")
        expect(@song.name).to eq "Tarquin Fin-tim-lin-bin-whin-bim-lim-bus-stop-F'tang-F'tang-Ole-Biscuitbarrel"
      end

    end
  end
end

describe Song do
  let(:person)         { FactoryBot.create(:person) }
  let(:album)          { FactoryBot.create(:album, person: person) }
  let(:approved_album) { FactoryBot.create(:album, :approved, person: person) }
  let(:song)           { FactoryBot.build(:song, album: approved_album)   }

  it "creates a version on update of paid/finalized album and paid song" do
    with_versioning do
      song.songstatus =  "paid"
      song.save
      expect(song.versions.size).to eq 1
    end
  end

  it "does not create a version on create" do
    with_versioning do
      song.save
      expect(song.versions).to be_empty
    end
  end

  it "does not create a version if paid but album not ready for distribution" do
    with_versioning do
      song.songstatus = "paid"
      song.album = album
      song.save
      expect(song.versions).to be_empty
    end
  end

  it "does not create a version if not paid" do
    with_versioning do
      song.save
      expect(song.versions).to be_empty
    end
  end

  describe "#isrc" do
    it "is optional if it is present" do
      song = create(:song, optional_isrc: "NLC369800009", tunecore_isrc: "USTCG1076674")
      expect(song.isrc).to eq song.optional_isrc
    end

    it "is tunecore if optional is not present" do
      song = create(:song, optional_isrc: "", tunecore_isrc: "USTCG1076674")
      expect(song.isrc).to eq song.tunecore_isrc
    end
  end

  describe "#ytm?" do
    context "when the song has a ytsr track_monetization" do
      let(:song) { create(:song) }
      let!(:track_monetization) { create(:track_monetization, store: Store.find_by(short_name: "YoutubeSR"), song: song) }

      it "should return true" do
        expect(song.ytm?).to eq true
      end
    end

    context "when the song is blocked for ytsr" do
      let(:song) { create(:song) }
      let!(:track_monetization_blocker) { create(:track_monetization_blocker, store: Store.find_by(short_name: "YoutubeSR"), song: song) }

      it "should return true" do
        expect(song.ytm?).to eq true
      end
    end

    context "when the song is eligible for ytsr but has not ytsr track_monetization and is not blocked for ytsr" do
      it "should return false" do
        song = build(:song)
        expect(song.ytm?).to eq false
      end
    end
  end

  describe "#update_composition_name" do
    context "given the account does not have rights app enabled" do
      it "should not update the composition name" do
        song = create(:song)
        song.update(name: "Song name")
        song.update(composition: create(:composition))
        expect(song.composition.name).not_to eq "Song name"
      end
    end

    context "given the account has rights app enabled" do
      it "should update composition name" do
        allow(FeatureFlipper).to receive(:show_feature?) { true }
        person = create(:person)
        person.account = create(:account)
        publishing_composer = create(:publishing_composer, :with_pub_admin_purchase, person: person, account: person.account)
        album = create(:album, person: publishing_composer.person)
        song = create(:song, name: "Song name", album: album)
        song.update(publishing_composition: create(:publishing_composition, account: person.account))

        song.update(name: "New Song Name")

        expect(song.publishing_composition.name).to eq "New Song Name"
      end
    end
  end

  describe "destroying a composition after deleting a song" do
    context "if there are no other dependent songs" do
      it "deletes the composition" do
        composition = create(:composition)
        song = create(:song, name: "Song name", album: album, composition: composition)

        song.destroy

        expect(Composition.exists?(composition.id)).to eq(false)
      end
    end

    context "if at least one other dependent song exists" do
      it "does not delete the composition" do
        composition = create(:composition)
        song = create(:song, name: "Song name", album: album, composition: composition)
        create(:song, name: "Song name2", album: album, composition: composition)

        song.destroy

        expect(Composition.exists?(composition.id)).to eq(true)
      end
    end
  end

  describe ".save" do
    describe "tunecore_isrc" do
      context "when assigning isrc 'TC%'" do
        it "creates tunecore_isrc with 'TC'" do
          song = create(:song, name: "Song name", album: album)

          expect(song.tunecore_isrc).not_to be_nil
          expect(song.tunecore_isrc[/^TC/]).to eq('TC')
        end

        it "assigns optional_isrc to tunecore_isrc"  do
          song = create(:song, name: "Song name", album: album, optional_isrc: 'TCJVM1400002')
          expect(song.tunecore_isrc).to eq('TCJVM1400002')
        end

        it "if isrc exists in db, creates new isrc for a song"  do
          song1 = create(:song, name: "Song name", album: album)
          expect {
            create(:song, name: "Song name", album: album, optional_isrc: song1.tunecore_isrc)
          }.to raise_error(ActiveRecord::RecordInvalid)
        end
      end

      context "when assigning isrc is not 'TC%'" do
        it "creates tunecore_isrc with 'TC'" do
          song = create(:song, name: "Song name", album: album, optional_isrc: 'IEJVM1400002')
          expect(song.tunecore_isrc[/^TC/]).to eq('TC')
        end
      end
    end
  end

  describe "#clean_version" do
    context "when #parental_advisory is true" do
      it "corrects #clean_version to false" do
        song.parental_advisory = true
        song.clean_version = true
        song.save

        song.reload

        expect(song.parental_advisory).to eq(true)
        expect(song.clean_version).to eq(false)
      end
    end

    context "when #parental_advisory is false" do
      it "retains the original #clean_version value" do
        song.parental_advisory = false
        song.clean_version = true
        song.save

        song.reload

        expect(song.parental_advisory).to eq(false)
        expect(song.clean_version).to eq(true)
      end
    end
  end

  describe "#s3_url" do
    let(:s3_generator_dbl) { double(S3::QueryStringAuthGenerator, :expires_in= => true) }
    let(:song) { build(:song, :with_s3_flac_asset, :with_s3_orig_asset) }

    before do
      allow(S3::QueryStringAuthGenerator).to receive(:new).and_return(s3_generator_dbl)
    end

    context "with s3_streaming_asset" do
      let(:song) { build(:song, :with_s3_streaming_asset) }

      it "passes the s3_streaming_asset as an argument to the generator" do
        expect(s3_generator_dbl).to receive(:get).with(song.s3_streaming_asset.bucket, song.s3_streaming_asset.key)

        song.s3_url(3600)
      end
    end

    context "without s3_streaming_asset" do
      it "passes the s3_flac_asset as an argument to the generator" do
        expect(s3_generator_dbl).to receive(:get).with(song.s3_flac_asset.bucket, song.s3_flac_asset.key)

        song.s3_url(3600)
      end
    end

    context "without s3_streaming_asset and s3_asset" do
      it "passes the s3_orig_asset as an argument to the generator" do
        expect(s3_generator_dbl).to receive(:get).with(song.s3_orig_asset.bucket, song.s3_orig_asset.key)

        song.s3_url(3600)
      end
    end

    context "when check_flac is set to true" do
      it "should pass in the s3_flac_asset as an argument to the generator" do
        expect(s3_generator_dbl).to receive(:get).with(song.s3_flac_asset.bucket, song.s3_flac_asset.key)

        song.s3_url(3600, true)
      end
    end
  end

  describe "#s3_original_url"  do
  let(:s3_generator_dbl) { double(S3::QueryStringAuthGenerator, :expires_in= => true) }
  let(:song) { build(:song, :with_s3_flac_asset, :with_s3_asset, :with_s3_streaming_asset) }

  before do
    allow(S3::QueryStringAuthGenerator).to receive(:new).and_return(s3_generator_dbl)
  end

  context "with s3_original_url" do
    let(:song) { build(:song, :with_s3_orig_asset, :with_s3_asset) }

    it "passes the s3_original_url as an argument to the generator" do
      expect(s3_generator_dbl).to receive(:get).with(song.s3_orig_asset.bucket, song.s3_orig_asset.key)

      song.s3_original_url(3600)
    end
  end

  context "without s3_original_url" do

    let(:song) { build(:song, :with_s3_asset, :with_s3_orig_asset) }

    it "passes the s3_asset as an argument to the generator" do
      expect(s3_generator_dbl).to receive(:get).with(song.s3_asset.bucket, song.s3_asset.key)

      song.s3_original_url(3600)
    end

  end

  context "without s3_original_url and s3_asset" do
    it "passes the s3_flac_asset as an argument to the generator" do
      expect(s3_generator_dbl).to receive(:get).with(song.s3_flac_asset.bucket, song.s3_flac_asset.key)

      song.s3_original_url(3600)
    end
  end

  context "when check_flac is set to true" do
    it "should pass in the s3_flac_asset as an argument to the generator" do
      expect(s3_generator_dbl).to receive(:get).with(song.s3_flac_asset.bucket, song.s3_flac_asset.key)

      song.s3_original_url(3600, true)
    end
  end

  context "without s3_original_url, s3_asset and s3_flac_asset" do
    it "passes the s3_streaming_asset as an argument to the generator" do
      expect(s3_generator_dbl).to receive(:get).with(song.s3_streaming_asset.bucket, song.s3_streaming_asset.key)

      song.s3_original_url(3600)
    end
  end

end

  describe "#notify_airbrake_after_optional_isrc_update" do
    before do
      allow(Rails).to receive_message_chain(:env, :production?).and_return true
    end

    it 'should notify airbrake when an optional ISRC number gets updated to nil' do
      previous_optional_isrc = "TCAFR2118414"
      new_optional_isrc = nil
      song = create(:song, optional_isrc: previous_optional_isrc)

      msg = "Song ID #{song.id} updated their optional ISRC number " \
            "from #{previous_optional_isrc} to #{new_optional_isrc}"

      expect(Airbrake).to receive(:notify).with(msg)

      song.update(optional_isrc: new_optional_isrc)
    end
  end
end

describe 'four byte char validation' do
  let(:song) { build(:song, name: "💿💿💿💿💿") }

  it 'validates name for four byte chars' do
    song.valid?
    expect(song.errors.messages[:name].size).to be 1
  end
end
