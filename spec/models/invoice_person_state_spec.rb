require "rails_helper"

describe InvoicePersonState do

  let(:invoice) { build_stubbed(:invoice) }
  let(:invoice_person_state) { InvoicePersonState.new(invoice) }
  let(:return_value) { "Unique return value" }

  describe "#initialize" do
    it "sets the invoice on the instance" do
      expect(invoice_person_state.instance_variable_get(:@invoice)).to eq(invoice)
    end

    it "sets the person on the instance" do
      expect(invoice_person_state.instance_variable_get(:@person)).to eq(invoice.person)
    end
  end

  describe "#is_first_purchase" do
    it "calls invoice#first_purchase?" do
      expect(invoice).to receive(:first_purchase?).and_return(return_value)
      invoice_person_state.is_first_purchase
    end

    it "does not call invoice#first_purchase? after the first call of #is_first_purchase" do
      expect(invoice).to receive(:first_purchase?).once.and_return(return_value)
      invoice_person_state.is_first_purchase
      invoice_person_state.is_first_purchase
    end
  end

  describe "#has_distro_purchase" do
    it "calls invoice#album_or_credit_purchase?" do
      expect(invoice).to receive(:album_or_credit_purchase?).and_return(return_value)
      invoice_person_state.has_distro_purchase
    end
  end

  describe "#is_first_time_dist_customer" do
    it "calls invoice#check_and_set_first_time_distribution_or_credit" do
      expect(invoice).to receive(:check_and_set_first_time_distribution_or_credit).and_return(return_value)
      invoice_person_state.is_first_time_dist_customer
    end
  end

  describe "#has_purchased_publishing_before" do
    it "calls invoice#purchased_publishing_before?" do
      expect(invoice).to receive(:purchased_publishing_before?).and_return(return_value)
      invoice_person_state.has_purchased_publishing_before
    end
  end

  describe "#user_has_active_ytm" do
    it "calls has_active_ytm? on the invoice's person" do
      expect(invoice.person).to receive(:has_active_ytm?).and_return(return_value)
      invoice_person_state.user_has_active_ytm
    end
  end

  describe "#has_ytm_purchase" do
    it "calls invoice#ytm_purchase?" do
      expect(invoice).to receive(:ytm_purchase?).and_return(return_value)
      invoice_person_state.has_ytm_purchase
    end
  end

  describe "#user_has_publishing" do
    it "calls paid_for_composer? for the invoices person" do
      expect(invoice.person).to receive(:paid_for_composer?).and_return(return_value)
      invoice_person_state.user_has_publishing
    end
  end

  describe "#songwriter_product_purchase" do
    it "calls invoice#publishing_purchase?" do
      expect(invoice).to receive(:publishing_purchase?).and_return(return_value)
      invoice_person_state.songwriter_product_purchase
    end
  end

  describe "#purchased_product_names" do
    it "returns a list of the product names for the invoices related purchases" do
      album = FactoryBot.create(:album, person: invoice.person)
      purchase = FactoryBot.create(:purchase, invoice: invoice, related: album, person: invoice.person)

      expect(invoice_person_state.purchased_product_names).to eq(purchase.product.name)
    end
  end

  describe "#recently_purchased_first_distro_and_publishing" do
    it "calls invoice#recently_purchased_first_distro_and_publishing?" do
      expect(invoice).to receive(:recently_purchased_first_distro_and_publishing?).and_return(return_value)
      invoice_person_state.recently_purchased_first_distro_and_publishing
    end
  end

  describe "#invoice_bought_recently" do
    it "returns true if the invoice was settled at within 5 minutes" do
      allow(invoice).to receive(:settled_at).and_return(Time.now)
      expect(invoice_person_state.invoice_bought_recently).to eq(true)
    end

    it "returns false if the invoice was settled at before 5 minutes" do
      allow(invoice).to receive(:settled_at).and_return(Time.now - 6.minutes)
      expect(invoice_person_state.invoice_bought_recently).to eq(false)
    end
  end

  describe "#has_missing_splits" do
    it "calls Composer::missing_splits? with the invoice's person if the user has publishing" do
      expect(invoice_person_state).to receive(:user_has_publishing).and_return(true)
      expect(Composer).to receive(:missing_splits?).with(invoice.person).and_return(return_value)
      expect(invoice_person_state.has_missing_splits).to eq(return_value)
    end

    it "returns nil if the user does not have publishing" do
      expect(invoice_person_state).to receive(:user_has_publishing).and_return(false)
      expect(invoice_person_state.has_missing_splits).to eq(nil)
    end
  end

  describe "#has_unactioned_ytm_tracks" do
    it "calls YtmTracks#has_unactioned_songs? if the user has ytm" do
      expect(invoice_person_state).to receive(:user_has_active_ytm).and_return(true)
      mock_ytm_tracks = double(YtmTracks)
      expect(YtmTracks).to receive(:new).and_return(mock_ytm_tracks)
      expect(mock_ytm_tracks).to receive(:has_unactioned_songs?).and_return(return_value)
      expect(invoice_person_state.has_unactioned_ytm_tracks).to eq(return_value)
    end

    it "returns nil if the user does not have ytm" do
      expect(invoice_person_state).to receive(:user_has_active_ytm).and_return(false)
      expect(invoice_person_state.has_unactioned_ytm_tracks).to eq(nil)
    end
  end

  describe "#has_first_distro_before_publishing_purchase" do
    it "returns true if the invoice contains the first distribution purchase for the user and the user had not bought publishing before" do
      expect(invoice_person_state).to receive(:is_first_time_dist_customer).and_return(true)
      expect(invoice_person_state).to receive(:has_purchased_publishing_before).and_return(false)
      expect(invoice_person_state.has_first_distro_before_publishing_purchase).to eq(true)
    end

    it "returns false if the invoice is not a first time distribution and the user did not purchase publishing before" do
      expect(invoice_person_state).to receive(:is_first_time_dist_customer).and_return(false)
      expect(invoice_person_state).to receive(:has_purchased_publishing_before).and_return(false)
      expect(invoice_person_state.has_first_distro_before_publishing_purchase).to eq(false)
    end

    it "returns false if the user purchased publishing before" do
      expect(invoice_person_state).to receive(:has_purchased_publishing_before).and_return(true)
      expect(invoice_person_state.has_first_distro_before_publishing_purchase).to eq(false)
    end
  end

  describe "#has_publishing_and_first_purchase" do
    it "returns true if the invoice is the first purchase for the user and includes the publishing product" do
      expect(invoice_person_state).to receive(:songwriter_product_purchase).and_return(true)
      expect(invoice_person_state).to receive(:is_first_purchase).and_return(true)
      expect(invoice_person_state.has_publishing_and_first_purchase).to eq(true)
    end

    it "returns false if the invoice does not include the publishing product" do
      expect(invoice_person_state).to receive(:songwriter_product_purchase).and_return(false)
      expect(invoice_person_state.has_publishing_and_first_purchase).to eq(false)
    end

    it "returns false if the invoice has the publishing product but is not the user's first purchase" do
      expect(invoice_person_state).to receive(:songwriter_product_purchase).and_return(true)
      expect(invoice_person_state).to receive(:is_first_purchase).and_return(false)
      expect(invoice_person_state.has_publishing_and_first_purchase).to eq(false)
    end
  end

  describe "#missing_ytm_tracks_or_splits" do
    it "returns true if the invoice has the ytm product" do
      allow(invoice_person_state).to receive(:has_ytm_purchase).and_return(true)
      allow(invoice_person_state).to receive(:songwriter_product_purchase).and_return(false)
      allow(invoice_person_state).to receive(:has_unactioned_ytm_tracks).and_return(false)
      allow(invoice_person_state).to receive(:has_missing_splits).and_return(false)
      expect(invoice_person_state.missing_ytm_tracks_or_splits).to be true
    end

    it "returns true if the invoice has the publishing product" do
      allow(invoice_person_state).to receive(:has_ytm_purchase).and_return(false)
      allow(invoice_person_state).to receive(:songwriter_product_purchase).and_return(true)
      allow(invoice_person_state).to receive(:has_unactioned_ytm_tracks).and_return(false)
      allow(invoice_person_state).to receive(:has_missing_splits).and_return(false)
      allow(invoice_person_state).to receive(:active_composer_with_rights_app?).and_return(true)
      expect(invoice_person_state.missing_ytm_tracks_or_splits).to be true
    end

    it "returns true if the user is missing monetizatble tracks" do
      allow(invoice_person_state).to receive(:has_ytm_purchase).and_return(false)
      allow(invoice_person_state).to receive(:songwriter_product_purchase).and_return(false)
      allow(invoice_person_state).to receive(:has_unactioned_ytm_tracks).and_return(true)
      allow(invoice_person_state).to receive(:has_missing_splits).and_return(false)
      expect(invoice_person_state.missing_ytm_tracks_or_splits).to be true
    end

    it "returns true if the user is missing publishing splits" do
      allow(invoice_person_state).to receive(:has_ytm_purchase).and_return(false)
      allow(invoice_person_state).to receive(:songwriter_product_purchase).and_return(false)
      allow(invoice_person_state).to receive(:has_unactioned_ytm_tracks).and_return(false)
      allow(invoice_person_state).to receive(:has_missing_splits).and_return(true)
      allow(invoice_person_state).to receive(:active_composer_with_rights_app?).and_return(true)
      expect(invoice_person_state.missing_ytm_tracks_or_splits).to be true
    end

    it "returns false if the invoice does not have the ytm or publishing product and also the user also does not have missing publishing splits or ytm_tracks" do
      allow(invoice_person_state).to receive(:has_ytm_purchase).and_return(false)
      allow(invoice_person_state).to receive(:songwriter_product_purchase).and_return(false)
      allow(invoice_person_state).to receive(:has_unactioned_ytm_tracks).and_return(false)
      allow(invoice_person_state).to receive(:has_missing_splits).and_return(false)
      allow(invoice_person_state).to receive(:active_composer_with_rights_app?).and_return(false)
      expect(invoice_person_state.missing_ytm_tracks_or_splits).to be false
    end
  end

  describe "#active_composer_with_rights_app?" do
    it "returns false if there is no Album or Composer purchase" do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

      expect(invoice_person_state.active_composer_with_rights_app?).to be(false)
    end

    let(:purchase) { create(:purchase, person: invoice.person, related_type: "Album") }
    let(:composer) { create(:composer, person: invoice.person) }

    it "returns false if rights app is not enabled" do
      invoice.purchases << purchase
      invoice.person.composers << composer
      allow(FeatureFlipper).to receive(:show_feature?).and_return(false)

      expect(invoice_person_state.active_composer_with_rights_app?).to be(false)
    end

    it "returns false if there is no active Composer" do
      invoice.purchases << purchase
      invoice.person.composers << composer

      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
      allow(composer).to receive(:is_active?).and_return(false)

      expect(invoice_person_state.active_composer_with_rights_app?).to be(false)
    end

    it "returns true if rights app is enabled and there is a composer or album present" do
      invoice.purchases << purchase
      invoice.person.composers << composer

      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
      allow(composer).to receive(:is_active?).and_return(true)

      expect(invoice_person_state.active_composer_with_rights_app?).to be(true)
    end
  end

  describe "#has_distribution_purchase?" do
    let(:distribution_purchase) { create(:purchase, person: invoice.person, related_type: "Album") }
    let(:purchase) { create(:purchase, person: invoice.person, related_type: "YoutubeMonetization")}

    it "returns true if there are any album, ringtone, or single purchases" do
      invoice.purchases << distribution_purchase
      expect(invoice_person_state.has_distribution_purchase?).to be(true)
    end

    it "returns false if there are no album, ringtone, or single purchases" do
      invoice.purchases << purchase
      expect(invoice_person_state.has_distribution_purchase?).to be(false)
    end
  end
end
