require "rails_helper"

describe SoundoutReportNotification do

  it "should require a soundout_report" do
    report = TCFactory.build(:soundout_report)

    notification = SoundoutReportNotification.new(notification_item: report)
    expect(notification).to be_valid
  end

  it "should set fields" do
    report = TCFactory.build(:soundout_report)

    notification = SoundoutReportNotification.new(notification_item: report)
    expect(notification).to be_valid

    expect(notification.url).not_to be_blank
    expect(notification.title).not_to be_blank
    expect(notification.link_text).not_to be_blank
    expect(notification.person).to eq(report.person)
  end

end
