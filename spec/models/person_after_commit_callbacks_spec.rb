require 'rails_helper'

describe Person::AfterCommitCallbacks do
  before(:each) do
    allow_any_instance_of(Believe::ApiClient).to receive(:post).and_return(true)
    allow(Believe::PersonWorker).to receive(:perform_async).and_return(true)
    @person = create(:person, is_verified: false)
  end

  describe ".after_commit" do
    context "a person is unverified" do
      it "should not call Believe::Person.add_or_update" do
        expect(Believe::PersonWorker).not_to receive(:perform_async)
        @person.recent_login = Time.now
        @person.save
      end
    end

    context "a person becomes verified" do
      it "should call Believe::PersonWorker.perform_async on save" do
        expect(Believe::PersonWorker).to receive(:perform_async).with(@person.id)
        @person.is_verified = true
        @person.save
      end
    end

    context "a person is verified" do
      before(:each) do
        @verified_person = create(:person, is_verified: true, email: "verified@tunecore.com")
      end

      it "should call Believe::PersonWorker.perform_async on change of believe attribute" do
        expect(Believe::PersonWorker).to receive(:perform_async).with(@verified_person.id)
        @verified_person.zip = "11238"
        @verified_person.save
      end

      it "should not call Believe::PersonWorker.perform_async on change of non believe attribute" do
        expect(Believe::PersonWorker).not_to receive(:perform_async)
        @verified_person.login_attempts = true
        @verified_person.save
      end
    end
  end
end
