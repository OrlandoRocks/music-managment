require "rails_helper"

describe PayoutTransfer::Refund do
  let(:payout_provider) { create(:payout_provider) }
  let(:payout_transfer) { create(:payout_transfer, payout_provider: payout_provider) }
  describe "#valid?" do
    it "is false if no payout transfer" do
      params = {}
      refund = PayoutTransfer::Refund.new(params)
      expect(refund.valid?).to eq(false)
    end
  end

  describe ".refund" do
    context "when the payout provider is marked inactive" do
      it "saves the payout transfer" do
        rejected_provider = create(:payout_provider, provider_status: PayoutProvider::DECLINED, active_provider: false)
        transfer_with_rejected_provider = create(:payout_transfer, payout_provider: rejected_provider)

        transfer_with_rejected_provider.tunecore_status = PayoutTransfer::REJECTED
        params = { payout_transfer: transfer_with_rejected_provider }
        PayoutTransfer::Refund.refund(params)

        expect(transfer_with_rejected_provider.reload.tunecore_status).to eq(PayoutTransfer::REJECTED)
        expect(transfer_with_rejected_provider.reload.rolled_back).to be_truthy
      end
    end

    it "saves the payout transfer" do
      payout_transfer.tunecore_status = PayoutTransfer::REJECTED
      params = { payout_transfer: payout_transfer }
      PayoutTransfer::Refund.refund(params)

      expect(payout_transfer.reload.tunecore_status).to eq(PayoutTransfer::REJECTED)
      expect(payout_transfer.reload.rolled_back).to be_truthy
    end

    it "creates a payout transaction (person_transaction)" do
      params = { payout_transfer: payout_transfer }

      PayoutTransfer::Refund.refund(params)

      expect(payout_transfer.payout_transactions.last).not_to eq(nil)

      expect(
        payout_transfer.payout_transactions.last.target
      ).to eq(payout_transfer)

      expect(
        payout_transfer.payout_transactions.last.credit
      ).to eq(payout_transfer.requested_amount.to_d)
    end

    it "updates the persons balance" do
      params = { payout_transfer: payout_transfer }

      previous_balance = payout_transfer.person.person_balance.balance

      PayoutTransfer::Refund.refund(params)

      expect(
        payout_transfer.person.person_balance.balance
      ).to eq(previous_balance + payout_transfer.requested_amount.to_d)
    end
  end
end
