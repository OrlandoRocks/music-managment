require "rails_helper"

describe SalepointSong do
  describe "::admin_query" do
    before(:each) do
      @person          = TCFactory.create(:person)
      @album           = TCFactory.create(:album, person: @person)
      @composer        = TCFactory.create(:composer, person: @person)
      @song            = TCFactory.create(:song, name: "Test", album: @album)
      @song2           = TCFactory.create(:song, name: "Test2", album: @album)
      @composition     = TCFactory.create(:composition, song: @song)
      @composition2    = TCFactory.create(:composition, song: @song2)
      @upc             = TCFactory.create(:upc, number: "12875281", upcable: @album)
      @salepoint       = TCFactory.create(:salepoint, album: @album)
      @muma_song       = TCFactory.create(:muma_song, song: @song)
      @muma_song2      = TCFactory.create(:muma_song, song: @song2)
      @salepoint_song  = TCFactory.create(:salepoint_song, song: @song, salepoint: @salepoint)
      @salepoint_song2 = TCFactory.create(:salepoint_song, song:@song2, salepoint: @salepoint)
      @ytm             = FactoryBot.create(:youtube_monetization, person: @person, effective_date:Date.yesterday)
      @artist          = FactoryBot.create(:artist)
    end

    it "should return correct salepoint_song when searching by Account ID" do
      @searched_items = SalepointSong.admin_query(query: "Account ID", search: @person.id, order: nil)
      expect(@searched_items).to eq([@salepoint_song, @salepoint_song2])
    end

    it "should return correct salepoint_song when searching by Account Email" do
      @searched_items = SalepointSong.admin_query(query: "Account Email", search: @person.email, order: nil)
      expect(@searched_items).to eq([@salepoint_song, @salepoint_song2])
    end

    it "should return correct salepoint_song when searching by Account Name" do
      @searched_items = SalepointSong.admin_query(query: "Account Name", search: @person.name, order: nil)
      expect(@searched_items).to eq([@salepoint_song, @salepoint_song2])
    end

    it "should return correct salepoint_song when searching by Song Name" do
      @searched_items = SalepointSong.admin_query(query: "Song Name", search: @song.name, order: nil)
      expect(@searched_items).to eq([@salepoint_song])
    end

    it "should return correct salepoint_song when searching by Release UPC" do
      @searched_items = SalepointSong.admin_query(query: "Release UPC", search: @upc.number, order: nil)
      expect(@searched_items).to eq([@salepoint_song, @salepoint_song2])
    end

    it "should return correct salepoint_song when searching by Release Name" do
      @searched_items = SalepointSong.admin_query(query: "Release Name", search: @album.name, order: nil)
      expect(@searched_items).to eq([@salepoint_song, @salepoint_song2])
    end

    it "should return correct salepoint_song when searching by song primary Artist Name" do
      @creative = TCFactory.create(:creative, creativeable: @song, artist: @artist)
      @searched_items = SalepointSong.admin_query(query: "Artist Name", search: @artist.name, order: nil)
      expect(@searched_items).to eq([@salepoint_song])
    end

    it "should return correct salepoint_song when searching by song secondary Artist Name" do
      TCFactory.create(:creative, creativeable: @song2, artist: @artist, role: "featuring")
      @searched_items = SalepointSong.admin_query(query: "Artist Name", search: @artist.name, order: nil)
      expect(@searched_items).to eq([@salepoint_song2])
    end

    it "should return correct salepoint_song when searching by album primary Artist Name" do
      TCFactory.create(:creative, creativeable: @album, artist: @artist)
      @searched_items = SalepointSong.admin_query(query: "Artist Name", search: @artist.name, order: nil)
      expect(@searched_items).to eq([@salepoint_song, @salepoint_song2])
    end

    it "should return correct salepoint_song when searching by album secondary Artist Name" do
      TCFactory.create(:creative, creativeable: @album, artist: @artist, role: "featuring")
      @searched_items = SalepointSong.admin_query(query: "Artist Name", search: @artist.name, order: nil)
      expect(@searched_items).to eq([@salepoint_song, @salepoint_song2])
    end

    it "should empty array when the search does not match any salepoint_songs" do
      @searched_items = SalepointSong.admin_query(query: "Release Name", search: "Fake Name")
      expect(@searched_items).to eq([])
    end

    it "should return all results when no search term exists" do
      @searched_items = SalepointSong.admin_query(query: "Release Name")
      expect(@searched_items).to eq([@salepoint_song, @salepoint_song2])
    end

    it "should reutrn all salepoint_songs that are new if searching by new state" do
      @searched_items = SalepointSong.admin_query(valid_state: "New")
      expect(@searched_items).to eq([@salepoint_song, @salepoint_song2])
    end

    it "should return all salepoint_songs that are valid if searching by valid state" do
      @salepoint_song.update_attribute(:state, "approved")
      @searched_items = SalepointSong.admin_query(valid_state: "approved")
      expect(@searched_items).to eq([@salepoint_song])
    end

    it "should return all salepoint_songs that are invalid if searching by invalid state" do
      @salepoint_song2.update_attribute(:state, "rejected")
      @searched_items = SalepointSong.admin_query(valid_state: "rejected")
      expect(@searched_items).to eq([@salepoint_song2])
    end
  end

  describe "::joins_and_conditions" do
    it "should not raise error when query field is valid" do
      expect{ SalepointSong.joins_and_conditions("Test Search", "Composition ID") }.to_not raise_error
    end
  end

  describe "::state_condition" do
    it "should return nil when state is all" do
      expect(SalepointSong.state_condition("all")).to eq(nil)
    end

    it "should not raise error when state is invalid, valid, or new" do
      expect{ SalepointSong.state_condition("approved") }.to_not raise_error
    end

    it "should raise error when the state is not within accepted states" do
      expect{ SalepointSong.state_condition("bad_state") }.to raise_error
    end
  end

  describe "::create_by_salepoint" do
    before :each do
      @album = TCFactory.create(:album)
      @song = TCFactory.create(:song, album: @album)
      @song2 = TCFactory.create(:song, album: @album)
      @song3 = TCFactory.create(:song, album: @album)
      @song4 = TCFactory.create(:song, album: @album)
      @song5 = TCFactory.create(:song, album: @album)
      @salepoint = TCFactory.create(:salepoint, album: @album)
      @album.reload
    end

    it "should create salepoint_songs for all the songs in the album passed in" do
      expect {
        SalepointSong.create_by_salepoint(@salepoint, @album)
      }.to change{SalepointSong.count}.by(5)
    end

    it "should create salepoint_songs for all the songs for the album related to the salepoint passed in" do
      expect {
        SalepointSong.create_by_salepoint(@salepoint)
      }.to change{SalepointSong.count}.by(5)
    end
  end

  describe "::update_states" do

    before(:each) do
      @salepoint_song   = TCFactory.create(:salepoint_song)
    end

    it "should update the salepoint_songs by the ids and states passed in" do
      @salepoint_song2  = TCFactory.create(:salepoint_song)
      SalepointSong.update_states( {  "#{@salepoint_song.id}" => { "state" => "approved", "reason_for_rejection" => "", "comments_for_rejection" => "" },
                                      "#{@salepoint_song2.id}"=> { "state" => "rejected", "reason_for_rejection" => "PD Works", "comments_for_rejection" => "" } }, Person.first )
      expect(@salepoint_song.reload.state).to eq("approved")
      expect(@salepoint_song2.reload.state).to eq("rejected")
    end

    it "should not update the salepoint_songs to rejected if no reason is provided" do
      errored = SalepointSong.update_states( { "#{@salepoint_song.id}"=> { "state" => "rejected", "reason_for_rejection" => "", "comments_for_rejection" => "" } }, Person.first )
      expect(errored).to eq(false)
    end

    it "should not update the salepoint_songs to rejected if no comments are provided for a reason of 'Suspcicious Assets'" do
      errored = SalepointSong.update_states( { "#{@salepoint_song.id}"=> { "state" => "rejected", "reason_for_rejection" => "Suspicious Assets", "comments_for_rejection" => "" } }, Person.first )
      expect(errored).to eq(false)
    end

    it "should destroy any ytm_blocked_song instances of approved salepoint_songs" do
      album = create(:album, :with_songs)
      song = create(:song, album: album)
      store = Store.find_by(id: Store::YTSR_STORE_ID)
      salepoint = create(:salepoint, salepointable: album, store: store)
      salepoint_song = create(:salepoint_song, salepoint: salepoint, song: song)

      create(:ytm_blocked_song, song: song)

      expect(song.reload.ytm_blocked_song).to be_an_instance_of(YtmBlockedSong)

      SalepointSong.update_states({
        "#{salepoint_song.id}" => {
          "state" => "approved",
          "reason_for_rejection" => "",
          "comments_for_rejection" => ""
        }
      })

      expect(song.reload.ytm_blocked_song).to eq(nil)
    end
  end

  describe "#create_distribution_song_if_approved" do
    context "when the salepoint_song has not been approved" do
      it "should not call DistributionSongWorker" do
        salepoint_song = TCFactory.create(:salepoint_song)
        salepoint_song.state = "rejected"
        expect(Delivery::DistributionSongWorker).not_to receive(:perform_async)
        salepoint_song.send(:create_distribution_song_if_approved)
      end
    end

    context "when the salepoint_song already has a distribution_song" do
      it "should not call DistributionSongWorker" do
        salepoint_song = TCFactory.create(:salepoint_song)
        distribution_song = TCFactory.create(:distribution_song, salepoint_song: salepoint_song)
        salepoint_song.state = "approved"
        expect(Delivery::DistributionSongWorker).not_to receive(:perform_async)
        salepoint_song.send(:create_distribution_song_if_approved)
      end
    end

    context "when the salepoint_song has been approved and does not have a distribution_song" do
      it "should call DistributionSongWorker with the salepoint_song ID" do
        salepoint_song = TCFactory.create(:salepoint_song)
        salepoint_song.state = "approved"
        expect(Delivery::DistributionSongWorker).to receive(:perform_async).with(salepoint_song.id)
        salepoint_song.send(:create_distribution_song_if_approved)
      end
    end
  end

  describe "#create" do
    context "when the salepoint_song has not been approved" do
      it "should not call DistributionSongWorker" do
        song = build_stubbed(:song)
        salepoint = build_stubbed(:salepoint, salepointable: song.album)
        expect(Delivery::DistributionSongWorker).not_to receive(:perform_async)
        create(:salepoint_song, salepoint: salepoint, song: song, state: "rejected", reason_for_rejection: "raisins")
      end
    end

    context "when the salepoint_song is created with a distribution_song" do
      it "should not call DistributionSongWorker" do
        song = build_stubbed(:song)
        salepoint = build_stubbed(:salepoint, salepointable: song.album)
        expect(Delivery::DistributionSongWorker).not_to receive(:perform_async)
        create(:salepoint_song, :with_distribution_song, salepoint: salepoint, song: song, state: "approved")
      end
    end

    context "when the salepoint_song is approved and does not have a distribution_song" do
      it "should call DistributionSongWorker" do
        song = build_stubbed(:song)
        salepoint = build_stubbed(:salepoint, salepointable: song.album)
        expect(Delivery::DistributionSongWorker).to receive(:perform_async)
        create(:salepoint_song, salepoint: salepoint, song: song, state: "approved")
      end
    end
  end

  describe "::takedown!" do
    before(:each) do
      @yt_store = Store.find_by(abbrev: "ytsr")
      @salepointable_store = FactoryBot.create(:salepointable_store, store: @yt_store)
      @person = FactoryBot.create(:person, email: "g@gmail.com")
      @album = FactoryBot.create(:album, :finalized, payment_applied: true, person: @person)
      @salepoint = FactoryBot.create(:salepoint, salepointable: @album, store: @yt_store)
      @song = FactoryBot.create(:song, album: @album)
      @salepoint_song = FactoryBot.create(:salepoint_song, takedown_at: nil, song: @song, salepoint: @salepoint)
      @distribution_song = TCFactory.create(:distribution_song, salepoint_song: @salepoint_song)
    end

    it "should set salepoint_song takedown_at" do
      allow(@song).to receive(:salepoint_songs).and_return([@salepoint_song])
      SalepointSong.takedown(@song)
      expect(@song.salepoint_songs.first.takedown_at.present?).to eq(true)
    end

    it "should call SalepointSong#send_metadata_update" do
      expect_any_instance_of(SalepointSong).to receive(:send_metadata_update).once
      SalepointSong.takedown(@song)
    end
  end

  describe "::remove_takedown" do
    before(:each) do
      @album = TCFactory.create(:album)
      @song = TCFactory.create(:song, album: @album)
      store = Store.find_by(short_name: "YoutubeSR")
      @salepoint = TCFactory.create(:salepoint, salepointable: @album, store: store, takedown_at: Time.now)
      @salepoint_song = TCFactory.create(:salepoint_song, takedown_at: Time.now, song: @song, salepoint: @salepoint)
      @distribution_song = TCFactory.create(:distribution_song, salepoint_song: @salepoint_song)
    end

    it "should remove salepoint_song takedown from all salepoint_songs and salepoints" do
      allow(@song).to receive(:salepoint_songs).and_return([@salepoint_song])
      SalepointSong.remove_takedown(@song)
      expect(@song.salepoint_songs.first.takedown_at.nil?).to eq(true)
      expect(@song.salepoint_songs.first.salepoint.takedown_at.nil?).to eq(true)
    end

    it "should call SalepointSong#send_metadata_update once" do
      expect_any_instance_of(SalepointSong).to receive(:send_metadata_update).once
      SalepointSong.remove_takedown(@song)
    end
  end

  describe "#send_metadata_update" do
    before(:each) do
      @salepoint_song = TCFactory.create(:salepoint_song, takedown_at: nil)
      @distribution_song = TCFactory.create(:distribution_song, salepoint_song: @salepoint_song, delivery_type: nil)
      converter = double
    end

    it "should set the distribution_song delivery type to metadata_only" do
      allow(@salepoint_song.distribution_song).to receive(:retry).and_return(true)
      @salepoint_song.send_metadata_update
      expect(@salepoint_song.distribution_song.delivery_type).to eq("metadata_only")
    end

    it "should call retry on the distribution_song with default params passed in if no argument is passed" do
      expect(@salepoint_song.distribution_song).to receive(:retry).with(:actor=>"Admin", :message=>"Sending/retrying metadata update")
      @salepoint_song.send_metadata_update
    end
  end

  describe "::mass_takedown" do
    before(:each) do
      @salepoint_song = TCFactory.create(:salepoint_song)
      @salepoint_song2 = TCFactory.create(:salepoint_song)
      @distribution_song = TCFactory.create(:distribution_song, salepoint_song: @salepoint_song, delivery_type: nil)
      @distribution_song = TCFactory.create(:distribution_song, salepoint_song: @salepoint_song2, delivery_type: nil)

      @song  = @salepoint_song.song
      @song2 = @salepoint_song2.song
    end

    it "calls SalepointSong::takedown for each song passed in" do
      expect(SalepointSong).to receive(:takedown).with(@song)
      expect(SalepointSong).to receive(:takedown).with(@song2)
      SalepointSong.mass_takedown([@song, @song2])
    end
  end
end
