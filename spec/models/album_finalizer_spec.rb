require "rails_helper"

describe Album, " when calling .should_finalize?" do
  before(:each) do
    #@album = #mock_model(Album)
    @album = TCFactory.build(:album)

    @album_finalizer = @album
    #AlbumFinalizer.new(@album)
    @song1 = TCFactory.build(:album)
    @songs = [@song1]
  end

  it "should return false to .has_songs? if the album has no songs" do
    expect(@album).to receive(:songs).once.and_return([])
    expect(@album_finalizer.has_songs?).to be_falsey
  end

  it "should return true to .has_songs? if the album has songs" do
    expect(@album).to receive(:songs).once.and_return(@songs)
    expect(@album_finalizer.has_songs?).to be_truthy
  end

  it "should return true to .has_artwork? if there is an artwork object, the artwork is on s3 and there are no errors" do
    @artwork = mock_model(Artwork, {:last_errors => nil, :image_file_name => "test.jpeg", :uploaded=>true})
    expect(@album).to receive(:artwork).at_least(1).times.and_return(@artwork)
    expect(@artwork).to be_truthy
    expect(@artwork.last_errors).to be_nil
    expect(@album_finalizer.has_artwork?).to be_truthy
  end

  it "should return false to .has_artwork? if the artwork has a last error" do
    @artwork = mock_model(Artwork, {:is_tiff_actually_on_s3? => true, :last_errors => 'your artwork sucks', :external_asset => false})
    expect(@album).to receive(:artwork).at_least(1).times.and_return(@artwork)
    expect(@album_finalizer.has_artwork?).to be_falsey
  end
end

describe Album, " when calling .stores_with_variable_pricing" do
  before(:each) do
    @album = TCFactory.build(:album)
    @album_finalizer = @album
    @salepoint = create(:salepoint, store_id: Store::IG_STORE_ID, salepointable: @album)
    @album.reload
  end

  it "should return an empty array if there are no stores with variable pricing" do
    allow_any_instance_of(Store).to receive(:variable_pricing_required?).and_return false

    expect(@album_finalizer.stores_with_variable_pricing).to be_empty
  end

  it "should return an array of the stores that have variable pricing" do
    allow_any_instance_of(Store).to receive(:variable_pricing_required?).and_return true
    expect(@album_finalizer.stores_with_variable_pricing.size).to eq 1
    expect(@album_finalizer.stores_with_variable_pricing).to include(@salepoint)
  end
end

describe Album, " when calling .has_stores_with_variable_pricing?" do
  before(:each) do
    @album = TCFactory.build(:album)
    @album_finalizer = @album
  end

  it "should return false if there are no stores returned from stores_with_variable_pricing" do
    expect(@album_finalizer).to receive(:stores_with_variable_pricing).once.and_return([])
    expect(@album_finalizer.has_stores_with_variable_pricing?).to be_falsey
  end

  it "should return true if there are stores returned from stores_with_variable_pricing" do
    expect(@album_finalizer).to receive(:stores_with_variable_pricing).once.and_return([mock_model(Salepoint)])
    expect(@album_finalizer.has_stores_with_variable_pricing?).to be_truthy
  end
end

describe Album, " when calling .has_artwork_template_info?" do
  before(:each) do
    @album = TCFactory.build(:album)
    @album_finalizer = @album
  end

  it "should return false if a store that requires a template has not chosen a template" do
    expect(@album_finalizer).to receive(:stores_with_artwork_templates).once.and_return(
      [mock_model(Salepoint, :requires_template? => true, :has_chosen_template? => false)]
    )
    expect(@album_finalizer.has_artwork_template_info?).to be_falsey
  end

  it "should return true if a store that requires a template has chosen a template" do
    expect(@album_finalizer).to receive(:stores_with_artwork_templates).once.and_return(
      [mock_model(Salepoint, :requires_template? => true, :has_chosen_template? => true)]
    )
    expect(@album_finalizer.has_artwork_template_info?).to be_truthy
  end
end

describe Album, "when calling .has_stores?" do
  before(:each) do
    @album = TCFactory.build(:album)
    @album_finalizer = @album
    @salepoint = create(:salepoint, store_id: Store::IG_STORE_ID, salepointable: @album)
    @album.reload
  end

  it "should return false if no stores have been selected" do
    expect(@album).to receive(:salepoints).once.and_return([])

    expect(@album_finalizer.has_stores?).to be_falsey
  end

  it "should return true if a store without variable pricing has been selected" do
    allow_any_instance_of(Store).to receive(:variable_pricing_required?).and_return false

    expect(@album_finalizer.has_stores?).to be_truthy
  end

  it "should return false if a store has been selected that requires variable pricing, but variable pricing has not been set" do
    allow_any_instance_of(Store).to receive(:variable_pricing_required?).and_return true
    allow_any_instance_of(Salepoint).to receive(:has_chosen_price_code?).and_return false

    expect(@album_finalizer.has_stores?).to be_falsey
  end

  it "should return true if a store has been selected and that store has its variable price set" do
    allow_any_instance_of(Store).to receive(:variable_pricing_required?).and_return true
    allow_any_instance_of(Salepoint).to receive(:has_chosen_price_code?).and_return true

    expect(@album_finalizer.has_stores?).to be_truthy
  end

end
