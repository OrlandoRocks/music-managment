require "rails_helper"

describe IneligibilityRule do
  describe "#matches?" do
    before do
      @person = create(:person)
      @store = Store.find_by(short_name: "FBTracks")
      @ineligible_rule = create(:ineligibility_rule,
            property: "primary_genre_name",
            operator: "equal",
            value: "Classical",
            store: @store
      )
      @album = create(:album, primary_genre: Genre.find_by(name: "Classical"))
      @song = create(:song, album: @album)
      @track_monetization = create(:track_monetization,
                                  song: @song,
                                  person: @person,
                                  store: @store
      )
    end

    context "using 'equal' operator" do
      it "matches an ineligible track monetization" do
        has_match = @ineligible_rule.matches?(@track_monetization.song)
        expect(has_match).to eq true
      end

      it "matches an ineligible track monetization regardless of casing" do
        @album.update(primary_genre: Genre.find_by_name("CLAssiCAL"))

        has_match = @ineligible_rule.matches?(@track_monetization.reload.song)

        expect(has_match).to be_truthy
      end

      it "does not match an eligible track monetization" do
        @album.update(primary_genre: Genre.find_by(name: "Rock"))
        has_match = @ineligible_rule.matches?(@track_monetization.reload.song)
        expect(has_match).not_to eq true
      end
    end

    context "using 'contains' operator" do
      before do
        @ytsr = Store.find_by(abbrev: "ytsr")
        @ineligible_song = create(:song, name: "something about meditation")
        @ytsr_monetization = create(:track_monetization,
                                    store:  @ytsr,
                                    song:   @ineligible_song,
                                    person: @person)
        @contains_rule = create(:ineligibility_rule,
                                property: "song_name",
                                operator: "contains",
                                value:    "Meditation",
                                store:    @ytsr)
      end

      it "matches an ineligible track monetization" do
        has_match = @contains_rule.matches?(@ytsr_monetization.song)
        expect(has_match).to eq true
      end


      it "matches an ineligible track monetization regardless of casing" do
        @ineligible_song.update(name: "medITATion")

        has_match = @contains_rule.matches?(@ytsr_monetization.song)

        expect(has_match).to be_truthy
      end

      it "does not match an eligible track monetization" do
        @ineligible_song.update(name: "something else")
        has_match = @contains_rule.matches?(@ytsr_monetization.song)
        expect(has_match).to eq false
      end
    end
  end
end
