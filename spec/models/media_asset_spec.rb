require "rails_helper"

describe MediaAsset do
  context "when trying to create a new media asset with blank fields" do
    it "should require person" do
      media_asset = build(:media_asset, person: nil)

      expect(media_asset).not_to be_valid
      expect(media_asset.errors.messages)
    end
  end

  context "test field limitation" do
    it "should not exceed size limitation for description, width, height, bit depth, and file size fields" do
      invalid_description_string = "a" * 501
      media_asset = build(
        :media_asset,
        description: invalid_description_string,
        width: 65536,
        height: 65536, bit_depth: 256,
        size: 4294967296
      )

      expect(media_asset.valid?).to be_falsey
      expect(media_asset.errors.messages[:description])
      expect(media_asset.errors.messages[:width])
      expect(media_asset.errors.messages[:bit_depth])
      expect(media_asset.errors.messages[:height])
      expect(media_asset.errors.messages[:size])
    end
  end

  context "when trying to save an invalid mime type" do
    it "should return an error" do
      media_asset = build(:media_asset, mime_type: "This Mime Type is Invalid")

      expect(media_asset.errors.messages[:mime_type])
      expect(media_asset.errors.messages[:image_format])
    end
  end

  context "When all validations pass" do
    it "should successfully save" do
      media_asset = build_stubbed(:media_asset)

      expect(media_asset).to be_valid
    end
  end

  context "when updating a record" do
    it "should remove any local or s3 assets first" do
      media_asset = create(:media_asset)

      expect(media_asset).to receive(:remove_assets).once

      media_asset.update(size: "10")
    end
  end

  context "when destroying a record" do
    it "should destroy local or s3 assets too" do
      media_asset = create(:media_asset)

      expect(media_asset).to receive(:destroy_assets).once

      media_asset.destroy
    end
  end

  context "acceptable mime type" do
    it "should successfully save it as a non-empty string in the database" do
      acceptable_mime_types = %w(image/jpeg image/pjpeg image/png image/x-png image/tiff image/gif image/bmp)
      acceptable_mime_types.each do |mime_type|
        media_asset = build(:media_asset, mime_type: mime_type)

        expect(media_asset).to be_valid
        media_asset.save

        expect(media_asset.mime_type).to eq(mime_type)
      end
    end
  end
end

