require 'rails_helper'

RSpec.describe RecoverAsset, type: :model do
  it "should upload and save recover assets file" do
    recover_asset = create(:recover_asset)
    pdf_file = Rack::Test::UploadedFile.new("spec/assets/booklet.pdf", 'application/pdf')
    uploaded = recover_asset.upload_zip(pdf_file)
    expect(uploaded).to eq(true)
  end
end
