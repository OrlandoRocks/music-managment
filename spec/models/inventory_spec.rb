require "rails_helper"

describe Inventory do
  describe "when using Inventory" do

    it "should raise an error if the inventory's item has already been used" do
      expect(Inventory).to receive(:can_use?).and_return(true)
      expect {
        Inventory.use!(Person.new, :item_to_use => Album.new, :product => Product.new)
      }.to raise_error
    end

    it "should raise an error inventory is not found for an item" do
      expect(Inventory).to receive(:find_best_inventory_match).and_return(nil)
      expect {
        Inventory.use!(Person.new, :item_to_use => Album.new, :product => Product.new)
      }.to raise_error
    end

    it "should call use single item" do
      expect(Inventory).to receive(:use_single_item!)
      expect(InventoryItem).to receive(:distribute!).and_return(true)
      Inventory.use!(Person.new, :items_to_use => [{:item_to_use => Album.new, :product => Product.new}])
    end

    it "should allow an array of items" do
      expect {
        expect(Inventory).to receive(:use_single_item!).and_return(true)
        expect(InventoryItem).to receive(:distribute!).and_return(true)
        Inventory.use!(Person.new, :items_to_use => [{:item_to_use => Album.new, :product => Product.new}])
      }.not_to raise_error
    end

    it "should allow a single item" do
      expect {
        expect(Inventory).to receive(:use_single_item!).and_return(true)
        expect(InventoryItem).to receive(:distribute!).and_return(true)
        Inventory.use!(Person.new, :item_to_use => Album.new)
      }.not_to raise_error
    end

    it "should raise error if you pass in a single item to use! and don't have the item_to_use set" do
      expect {
        Inventory.use!(Person.new, {})
      }.to raise_error
    end

    it "should call use single item and distribute! for an album usage" do
      expect(Inventory).to receive(:use_single_item!)
      expect(InventoryItem).to receive(:distribute!).and_return(true)
      Inventory.use!(Person.new, :items_to_use => [{:item_to_use => Album.new, :product => Product.new}])
    end

    it "should call on the Delivery::DistributionWorker if the item_to_use is a salepoint" do
      person = TCFactory.create(:person)
      album = TCFactory.create(:album, person: person)
      salepoint = TCFactory.create(:salepoint)
      product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
      product_item = TCFactory.create(:product_item, product: product)
      purchase = TCFactory.create(:purchase, related: album, person: person, product: product)
      inventory = TCFactory.create(:inventory, purchase: purchase, product_item: product_item)
      expect(Delivery::SalepointWorker).to receive(:perform_async).with(salepoint.id)
      allow(Inventory).to receive(:find_best_inventory_match).and_return(inventory)
      Inventory.use!(TCFactory.create(:person), items_to_use: [{item_to_use: salepoint, product: product, distribute: true}])
    end

    it "should not call on the Delivery::DistributionWorker if distribute is false" do
      person = TCFactory.create(:person)
      album = TCFactory.create(:album, person: person)
      salepoint = TCFactory.create(:salepoint)
      product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
      product_item = TCFactory.create(:product_item, product: product)
      purchase = TCFactory.create(:purchase, related: album, person: person, product: product)
      inventory = TCFactory.create(:inventory, purchase: purchase, product_item: product_item)
      expect(Delivery::SalepointWorker).not_to receive(:perform_async).with(salepoint.id)
      allow(Inventory).to receive(:find_best_inventory_match).and_return(inventory)
      items = Inventory.use!(TCFactory.create(:person), items_to_use: [{item_to_use: salepoint, product: product}], distribute: false)
      expect(items.first.inventory).to eq(inventory)
      expect(items.first.item).to eq(salepoint)
    end
  end

  describe "used?" do

    it "should be true if usage finds anything" do
      expect(Inventory).to receive(:usage).and_return(InventoryUsage.new)
      expect(Inventory.used?(Album.new)).to eq(true)
    end

    it "should return false if usage does not find anything" do
      expect(Inventory).to receive(:usage).and_return(nil)
      expect(Inventory.used?(Album.new)).to eq(false)
    end

  end

  describe "usage" do

    it "should look in InventoryUsage with the incoming item as the related" do
      item = Album.new
      expect(item).to receive(:id).and_return(1)
      Inventory.usage(item)
    end

  end

  describe "total left" do

    it "should perform the query and return Zero results correctly" do
      expect(Inventory.total_left(Person.new, "Album")).to eq(0)
    end

    it "should be able to give the total left" do
      inventory = Inventory.new
      expect(Inventory).to receive(:total_left).and_return(2)
      Inventory.total_left(Person.new, Album)
    end

  end

  describe "all available" do

    it "should be able to provide an empty list by performing SQL query" do
      person = Person.new(:id => 1)
      expect(Inventory.all_available(person)).to eq([])
    end

    it "should be able to give all available inventories for a person" do
      person = Person.new
      expect(person).to receive(:id).and_return(1)
      Inventory.all_available(person)
    end

  end

  describe "an object with sub_items" do

    it "should return true if sub_items is empty" do
      inventory = Inventory.new
      expect(inventory).to receive(:sub_items).and_return([])
      expect(inventory.sub_items_match?).to eq(true)
    end

    it "should return true if sub_items contains a match" do
      inventory = Inventory.new(:id => 1)
      sub = Inventory.new(:id => 2, :parent_id => 1)
      inventory.sub_items = [sub]
      expect(inventory).to receive(:sub_item_match?).with(sub).and_return(true)
      expect(inventory.sub_items_match?).to eq(true)
    end

    it "should return false if sub_items does not contain a match" do
      inventory = Inventory.new(:id => 1)
      sub = Inventory.new(:id => 2, :parent_id => 1)

      inventory.sub_items = [sub]
      expect(inventory).to receive(:sub_item_match?).with(sub).and_return(false)
      expect(inventory.sub_items_match?).to eq(false)
    end

    it "sub_item_match? should not match if not unlimited and quantity exceeds inventory" do
      inventory = Inventory.new(:id => 1)
      sub = Inventory.new(:id => 2, :parent_id => 1)

      allow(sub).to receive(:is_unlimited?).and_return(false)
      allow(sub).to receive(:quantity_is_within_inventory_limit?).and_return(false)
      expect(inventory.sub_item_match?(sub)).to eq(false)
    end

    it "sub_item_match? should match if not unlimited and quantity does not exceed inventory" do
      inventory = Inventory.new(:id => 1)
      sub = Inventory.new(:id => 2, :parent_id => 1)

      allow(sub).to receive(:is_unlimited?).and_return(false)
      allow(sub).to receive(:quantity_is_within_inventory_limit?).and_return(true)
      expect(inventory.sub_item_match?(sub)).to eq(true)
    end

    it "sub_item_match? should match if unlimited and quantity does not exceed inventory" do
      inventory = Inventory.new(:id => 1)
      sub = Inventory.new(:id => 2, :parent_id => 1)

      allow(sub).to receive(:is_unlimited?).and_return(true)
      allow(sub).to receive(:quantity_is_within_inventory_limit?).and_return(true)
      expect(inventory.sub_item_match?(sub)).to eq(true)
    end

    it "sub_item_match? should match if unlimited and quantity does exceed inventory" do
      inventory = Inventory.new(:id => 1)
      sub = Inventory.new(:id => 2, :parent_id => 1)

      allow(sub).to receive(:is_unlimited?).and_return(true)
      allow(sub).to receive(:quantity_is_within_inventory_limit?).and_return(false)

      expect(inventory.sub_item_match?(sub)).to eq(true)
    end

  end

  describe ".distribution_credits" do
    let(:person){ TCFactory.build_person }
    let(:credit_product){ TCFactory.build_album_credit_product(1) }
    let(:product_item){ ProductItem.find_by(product_id: credit_product.id) }
    let(:non_credit_product){ TCFactory.build_ad_hoc_album_product }
    let(:non_credit_product_item){ ProductItem.find_by(product_id: non_credit_product.id) }

    before :each do
      @credit_inventory = TCFactory.create(:inventory, product_item_id: product_item.id, person: person, purchase_id: TCFactory.build_purchase(person: person, product_id: credit_product.id, related: TCFactory.build_album(person: person)).id)
      @non_credit_inventory = TCFactory.create(:inventory, product_item_id: non_credit_product_item.id, person: person, purchase_id: TCFactory.build_purchase(person: person, product_id: non_credit_product.id, related: TCFactory.build_album(person: person)).id)
      credit_product.update_attribute(:id, Product.find_products_for_country(person.country_domain,:distribution_credit).first) # need to update the id to match a distribution product id
      @credit_inventories = Inventory.distribution_credits(person)
    end

    it "should return all credits inventory" do
      expect(@credit_inventories).not_to be_blank
      expect(@credit_inventories.any?{|i| i == @credit_inventory}).to be_truthy
    end

    it "should not return non credits inventory" do
      expect(@credit_inventories.any?{|i| i == @non_credit_inventory}).to be_falsey
    end
  end
end
