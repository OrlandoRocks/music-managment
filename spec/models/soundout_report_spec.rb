require "rails_helper"

describe SoundoutReport do
  describe "#paid_post_proc" do
    context "when the track is a song" do
      it "calls a transcoder" do
        report_purchase = build(:soundout_report, :with_track)

        expect(report_purchase).to receive(:status=).with("paid")
        expect(report_purchase).to receive(:save)
        expect(Soundout::Transcoder).to receive_message_chain(:new, :run)

        report_purchase.paid_post_proc
      end
    end

    context "when the track is not a song" do
      it "orders report in a delayed job" do
        report_purchase = build(:soundout_report, track: create(:song_library_upload))

        expect(report_purchase).to receive(:status=).with("paid")
        expect(report_purchase).to receive(:save)
        expect(Soundout::OrderWorker).to receive(:perform_async)

        report_purchase.paid_post_proc
      end
    end
  end

  describe "#process_order" do
    it "should set the provider identifier, status, and timestamp" do
      soundout_id = "1234"

      soundout_report = build(:soundout_report)
      allow(soundout_report).to receive(:paid?).and_return(true)
      expect(soundout_report).to receive(:status=).with("requested")
      expect(soundout_report).to receive(:soundout_id=).with(soundout_id)
      expect(soundout_report).to receive(:report_requested_tmsp=)
      expect(soundout_report).to receive(:save)

      soundout_report.process_order(soundout_id)
    end

    it "should set the status to error if no soundout_id given" do
      soundout_report = build(:soundout_report)
      expect(soundout_report).to receive(:status=).with("error")
      expect(soundout_report).to receive(:save)
      soundout_report.process_order(nil)
    end
  end

  describe "#process_report_fetch" do
    it "should save the report data, set status and report_received_tmsp" do
      report_data = { data: { MktPot: 80 }, reviews: [] }.to_json
      soundout_report = create(:soundout_report, :with_track)

      soundout_report.process_report_fetch(true, report_data)

      expect(soundout_report.market_potential).to eq(80)
    end

    it "should update retrieval attempts" do
      soundout_report = build(:soundout_report, :with_track)
      allow(soundout_report).to receive(:id).and_return(1)
      expect(soundout_report).to receive(:save!).at_least(:once)
      expect(soundout_report).to receive(:report_data=).with("Data")

      soundout_report.process_report_fetch(true, "Data")
      expect(soundout_report.retrieval_attempts).to eq(1)
    end

    it "should set status to error if unsuccessful" do
      soundout_report = build(:soundout_report, :with_track)
      allow(soundout_report).to receive(:paid?).and_return(true)

      expect(soundout_report).to receive(:save!)

      expect(soundout_report).to receive(:status=).with("error")
      soundout_report.process_report_fetch(false, nil)
    end

    it "should create notification when report is available" do
      soundout_report = build(:soundout_report, :with_track)

      expect do
        allow(soundout_report).to receive(:market_potential).and_return(80)
        soundout_report.process_report_fetch(true, "Data")
      end.to change(SoundoutReportNotification, :count).by(1)
    end

    it "should send an email when report is available" do
      soundout_report = build(:soundout_report, :with_track)
      mail = double("mail")
      expect(SoundoutMailer).to receive(:report_available).and_return(mail)
      expect(mail).to receive(:deliver)
      allow(soundout_report).to receive(:market_potential).and_return(80)
      soundout_report.process_report_fetch(true, "Data")
    end

    it "should not send an email if it can't save the report" do
      soundout_report = build(:soundout_report, :with_track)
      mail = double("mail")
      expect(SoundoutMailer).not_to receive(:report_available)
      soundout_report.process_report_fetch(true, "{ bad,,, ")
      expect(soundout_report.status).to eq("error")
    end
  end

  it "should require a song with 90 second duration" do
    report = build(:soundout_report, :with_track)
    song = report.track
    report.track = nil

    # No Song
    expect(report).not_to be_valid

    s3_asset = song.s3_asset
    song.s3_asset = nil
    report.track = song

    # Song without duration information
    expect(report).not_to be_valid

    # Valid song
    petri_bundle = report.track.album.petri_bundles.create!
    streaming_distribution = create(:distribution, petri_bundle: petri_bundle)
    expect(streaming_distribution).to be_persisted
    report.track.s3_asset = s3_asset
    expect(report).to be_valid

    # Duration too short
    allow(report.track).to receive(:duration).and_return(50)
    expect(report).not_to be_valid
  end

  it "should require a song_library_upload with 90 second duration" do
    report = build(:soundout_report)
    report.track = create(:song_library_upload, person: Person.first)

    # Song Library Upload without duration information
    expect(report).not_to be_valid
    expect(report.errors.full_messages).to include(
      I18n.t(
        "models.reporting.must_be_90_or_more_seconds_long",
        track_name: report.track.name
      )
    )

    # Song Library Upload Duration too short
    allow(report.track).to receive(:duration).and_return(50)
    expect(report).not_to be_valid
    expect(report.errors.full_messages).to include("#{report.track.name} must be 90 or more seconds long")
  end

  it "should require a song that belongs to an album that has been distributed to any store" do
    album = create(
      :album,
      :with_salepoints,
      :with_uploaded_songs,
      :with_artwork,
      :with_songwriter,
      :paid,
      known_live_on: nil,
      takedown_at: nil
    )
    song = album.songs.first
    report = build(:soundout_report, track: song)

    allow(report.track).to receive(:duration).and_return(95000)
    expect(report).not_to be_valid

    petri_bundle = report.track.album.petri_bundles.create!
    distribution = create(:distribution, :delivered_to_spotify_find_or_create_salepoint, petri_bundle: petri_bundle)
    expect(distribution).to be_persisted
    expect(report).to be_valid
  end

  describe "#as_json" do
    it "returns the expected json" do
      report = create(:soundout_report, :with_track)

      result = report.as_json(only: [:id, :artist_name, :artwork_url])

      expect(result).to eq expected_report_json(report)
    end

    def expected_report_json(report)
      {
        soundout_report: {
          "artist_name" => report.track.artist_name,
          "artwork_url" => {
            "small_url" => report.track.album.artwork.url(:small),
            "medium_url" => report.track.album.artwork.url(:medium),
            "large_url" => report.track.album.artwork.url(:large)
          },
          "id" => report.id
        }
      }
    end
  end
end
