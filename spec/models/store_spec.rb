require "rails_helper"

describe Store do
  describe "self.stores_on_sale" do
    it "returns a list of stores currently on sale" do
      store1 = FactoryBot.create(:store, on_sale: true,  name: "Test 1", short_name: "test1", abbrev: "one")
      store2 = FactoryBot.create(:store, on_sale: true,  name: "Test 2", short_name: "test2", abbrev: "two")
      store3 = FactoryBot.create(:store, on_sale: false, name: "Test 3", short_name: "test3", abbrev: "three")

      expect(Store.stores_on_sale).to eq [store1, store2]
      expect(Store.stores_on_sale).to_not include store3
    end
  end

  describe "self.stores_on_flash_sale" do
    it "returns a list of stores currently on flash sale" do
      store1 = FactoryBot.create(:store, on_flash_sale: true,  name: "Test 1", short_name: "test1", abbrev: "one")
      store2 = FactoryBot.create(:store, on_flash_sale: true,  name: "Test 2", short_name: "test2", abbrev: "two")
      store3 = FactoryBot.create(:store, on_flash_sale: false, name: "Test 3", short_name: "test3", abbrev: "three")

      expect(Store.stores_on_flash_sale).to eq [store1, store2]
      expect(Store.stores_on_flash_sale).to_not include store3
    end
  end

  describe "#default_variable_price=" do
    it "should create a new recrod for default variable prices" do
      variable_prices_store = FactoryBot.create(:variable_prices_store)
      variable_price        = variable_prices_store.variable_price
      store                 = variable_prices_store.store

      expect {
        store.default_variable_price = variable_price
      }.to change(DefaultVariablePrice, :count).by(1)
    end

    it "should remove Default Variable Price records when passing nil" do
      variable_prices_store         = FactoryBot.create(:variable_prices_store)
      variable_price                = variable_prices_store.variable_price
      store                         = variable_prices_store.store
      store.default_variable_price  = variable_price

      expect {
        store.default_variable_price = nil
      }.to change(DefaultVariablePrice, :count).by(-1)

    end
  end

  describe "#default_variable_price" do
    it "should give a VariablePrice object" do
      variable_prices_store = FactoryBot.create(:variable_prices_store)
      variable_price        = variable_prices_store.variable_price
      store                 = variable_prices_store.store

      store.default_variable_price = variable_price
      expect(store.default_variable_price).to be_a_kind_of(VariablePrice)
    end

    it "should return the variable price" do
      new_store         = FactoryBot.create(:store)
      variable_price_1  = FactoryBot.create(:variable_price)
      variable_price_2  = FactoryBot.create(:variable_price)
      vp_store_1        = FactoryBot.create(:variable_prices_store, store: new_store, variable_price: variable_price_1)
      vp_store_2        = FactoryBot.create(:variable_prices_store, store: new_store, variable_price: variable_price_2)
      FactoryBot.create(:default_variable_price, variable_prices_store: vp_store_1, store: new_store)

      expect(new_store).not_to receive(:implicit_default_variable_price)
      expect(new_store.has_variable_pricing?).to          eq(true)
      expect(new_store.variable_prices.length).to         eq(2)
      expect(new_store.default_variable_price.blank?).to  eq(false)
      expect(new_store.default_variable_price.id).to      eq(variable_price_1.id)
    end

    it "should return the implicit variable price" do
      new_store         = FactoryBot.create(:store)
      variable_price_1  = FactoryBot.create(:variable_price)
      vp_store_1        = FactoryBot.create(:variable_prices_store, store: new_store, variable_price: variable_price_1)

      expect(new_store.has_variable_pricing?).to eq(true)
      expect(new_store.variable_prices.length).to eq(1)
      expect(new_store.default_variable_price.blank?).to eq(false)
      expect(new_store.default_variable_price.id).to eq(variable_price_1.id)
    end

    it "should return no default varialbe price if multiple variables prices and no default" do
      new_store         = FactoryBot.create(:store)
      variable_price_1  = FactoryBot.create(:variable_price)
      variable_price_2  = FactoryBot.create(:variable_price)
      vp_store_1        = FactoryBot.create(:variable_prices_store, store: new_store, variable_price: variable_price_1)
      vp_store_2        = FactoryBot.create(:variable_prices_store, store: new_store, variable_price: variable_price_2)

      expect(new_store.variable_prices.length).to eq(2)
      expect(new_store.default_variable_price.blank?).to eq(true)
    end
  end

  describe "#launch" do
    before(:each) do
      @person = FactoryBot.create(:person)
      @store  = FactoryBot.create(:store, launched_at: nil)
    end

    it "sets the launched_at time" do
      expect(@store.launched_at).to be_blank
      @store.launch(@person)
      expect(@store.launched_at).not_to be_blank
    end

    it "queues a Sidekiq job" do
      expect(BulkStoreLaunchEmailWorker).to receive(:perform_async).with(@store.id)
      @store.launch(@person)
    end
  end

  describe "store_distro_types" do
    it "should have distro information for all stores" do
      stores = Store.is_active
      stores.each do |store|
        expect(Store::STORE_DISTRO_TYPES.has_key?(store.abbrev)).to eq(true)
      end
    end
  end

  describe "store abbreviation uniqueness" do
    it "should only ensure uniquness within is_valid scope" do
      itunes_us = Store.find(1)
      expect(itunes_us.valid?).to eq(true)

      itunes_match = Store.find(30)
      expect(itunes_match.valid?).to eq(true)

      itunes_match.is_active = true
      itunes_match.save(validate: false)

      expect(itunes_us.valid?).to eq(false)
      expect(itunes_match.valid?).to eq(false)
    end
  end

  describe "::mass_takedown_stores" do
    it "should not include streaming" do
      expect(Store.all.include?(Store.find_by(abbrev: "st"))).to eq(true)
      expect(Store.mass_takedown_stores.include?(Store.find_by(abbrev: "st"))).to eq(false)
    end
  end

  describe "#get_implicit_track_variable_price" do
    it "is always nil for google stores" do
      store = Store.find_by(short_name: "Google")
      expect(Store.get_implicit_track_variable_price(store)).to  eq(nil)
    end
  end

  describe "#get_implicit_default_variable_price" do
    it "is always nil for google stores" do
      store = Store.find_by(short_name: "Google")
      expect(Store.get_implicit_default_variable_price(store,"Single")).to eq(nil)
    end
  end

  describe "#converter_class" do
    context "when the store has a DistributionSystem converter class" do
      xit "gets the DistributionSystem converter class" do
        store = Store.find_by(abbrev: "ttunes")
        expect(store.converter_class.to_s).to eq("DistributionSystem::DDEX::Converter")
      end
    end

    context "when the store has a MusicStores converter class" do
      it "gets the MusicStores converter class" do
        store = Store.ITUNES_US
        expect(store.converter_class.to_s).to eq("MusicStores::Itunes::Converter")
      end
    end
  end

  describe "self.orphaned_salepoint_stores_for_album" do
    it "should not include stores that the album has salepoints for" do
      stores = Store.where.not(abbrev: ["fb", "gn", "sz", "sp"])
      album = create(:album, :with_salepoints)

      orphaned_sps = stores.orphaned_salepoint_stores_for_album(album)
      expect((orphaned_sps.pluck('stores.abbrev') & ["fb", "gn", "sz", "sp"]).empty?).to be(true)
    end

    it "should include salepoints the album is missing salepoints for" do
      stores = Store.where.not(abbrev: ["fb", "gn", "sz", "sp"])
      album = create(:album, :with_salepoints)

      orphaned_sps = stores.orphaned_salepoint_stores_for_album(album)
      expect((orphaned_sps.pluck(:id) & stores.pluck(:id)).size).to eq(stores.count)
    end
  end
end
