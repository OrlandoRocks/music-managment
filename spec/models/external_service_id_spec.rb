require "rails_helper"

describe ExternalServiceId do
  describe ".spotify" do
    before :each do
      create(:external_service_id, service_name: "deezer")
      create_list(:external_service_id, 3, service_name: "spotify")
    end

    it "returns a collection of external_ids for spotify" do
      spots = ExternalServiceId.spotify
      expect(spots.length).to eq(3)
    end
  end
end

describe ExternalServiceId do

  context "scopes" do

    describe "by_type" do

      context "given 'song' " do
        it "returns external_ids for songs" do
          song  = create(:song)
          album = create(:album)

          song_esis = [
            create(:external_service_id, linkable: song, service_name: "apple"),
            create(:external_service_id, linkable: song, service_name: "spotify")
          ]

          album_esi = create(:external_service_id, linkable: album)

          results = ExternalServiceId.by_type("song")
          expect(results.length).to eq 2
          expect(results.all?{|id| id.linkable_type == "Song"}).to be true
        end
      end

      context "given 'album' " do
        it "returns external_ids for songs" do
          song  = create(:song)
          album = create(:album)

          album_esis = [
            create(:external_service_id, linkable: album, service_name: "apple"),
            create(:external_service_id, linkable: album, service_name: "spotify")
          ]

          song_esi = create(:external_service_id, linkable: song)

          results = ExternalServiceId.by_type("album")
          expect(results.length).to eq 2
          expect(results.all?{|id| id.linkable_type == "Album"}).to be true
        end
      end
    end

    describe "by_service" do
      before(:each) do
        ExternalServiceId.destroy_all
      end
      it "returns a collection of external_ids for spotify" do
        create_list(:external_service_id, 2, service_name: "spotify")
        create_list(:external_service_id, 1, service_name: "apple")
        expect(ExternalServiceId.count).to eq 3

        results = ExternalServiceId.by_service("spotify")
        expect(results.length).to eq 2
        expect(results.all?{|id| id.service_name == "spotify"}).to be true
      end
    end
  end

  describe ".ids_for" do
    it "returns the linkable's name and the external id" do
      album  = create(:album, name: "Awesome Album 1")
      album2 = create(:album, name: "Awesome Album 2")
      create(:external_service_id, linkable: album, service_name: "deezer")
      create(:external_service_id, linkable: album, identifier: "12345", service_name: "apple")
      create(:external_service_id, linkable: album2, identifier: "67890", service_name: "apple")

      results = ExternalServiceId.ids_for(Album, "apple")
      expect(results.length).to eq 2
      expect(results.as_json(root: false, except: :id)).to match_array [{"name"=>"Awesome Album 1", "identifier"=>"12345"}, {"name"=>"Awesome Album 2", "identifier"=>"67890"}]
    end
  end

  describe ".by_creative_ids" do
    let(:album)    { create(:album) }
    let(:creative) { album.creatives.first }

    it "returns a collection of external_service_ids associated to creatives" do
      album_esi    = create(:external_service_id, linkable: album, service_name: "apple")
      expected_esi = create(:external_service_id, linkable: creative, service_name: "apple")
      results      = ExternalServiceId.by_creative_ids(album.creatives.pluck(:id))

      expect(results).to eq [expected_esi]
      expect(results).not_to include album_esi
    end
  end

  describe ".freeze_new_artist_editing?" do
    it "should label ids with an identifier that's not 'unavailable', as not available for a change to new artist" do
      album  = create(:album, name: "Awesome Album 1")
      ext_id = create(:external_service_id, linkable: album, service_name: "spotify", identifier: "l1k23lkm12")

      expect(ext_id.freeze_new_artist_editing?).to eq(true)
    end
  end
end
