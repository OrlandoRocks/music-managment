require "rails_helper"

describe PetriBundle do
  describe "distributions" do
    before(:each) do
      @person = TCFactory.create(:person)
      @album = TCFactory.create(:album, :person => @person)
    end

    it "should not return blocked distributions" do

      bundle = create(:petri_bundle, album: @album)
      dist1 = create(:distribution, petri_bundle: bundle)
      dist2 = create(:distribution, petri_bundle: bundle)
      dist3 = create(:distribution, petri_bundle: bundle)

      expect(bundle.distributions.count).to eq(3)

      dist1.block!

      bundle.reload
      expect(bundle.distributions.count).to eq(2)
    end
  end

  describe "#add_salepoints" do
    let(:person)       { create(:person, email: "a@b.com") }
    let(:album)        { create(:album, person: person) }
    let(:petri_bundle) { create(:petri_bundle, album: album) }

    before(:each) do
      @salepoints = []
      @salepoints << create(:salepoint, salepointable: album, store: Store.find_by(short_name: "Facebook"))
      @salepoints << create(:salepoint, salepointable: album, store: Store.find_by(short_name: "Amazon"), variable_price_id: 1)
      @salepoints << create(:salepoint, salepointable: album, store: Store.find_by(short_name: "Zvooq"))
      @salepoints << create(:salepoint, salepointable: album, store: Store.find_by(short_name: "Yandex"))
      @salepoints << create(:salepoint, salepointable: album, store: Store.find_by(short_name: "Pandora"))
    end

    it "Adds salepoint to existing distribution" do
      petri_bundle.add_salepoints(@salepoints, actor: "Test User", reason: "Add Salepoint")
      expect(album.reload.distributions.map(&:converter_class)).to include "MusicStores::DDEX::Converter"
      expect(album.reload.distributions.map(&:converter_class)).to include "MusicStores::Amazon::Converter"
      expect(album.reload.distributions.count).to eq 5
    end
  end

  describe "::recreate!" do
    let(:person) { TCFactory.create(:person) }
    let(:active_bundle) { TCFactory.create(:petri_bundle, album: @album) }

    before(:each) do
      @album = TCFactory.create(:album, person: person, payment_applied: true)
      allow(@album).to receive(:has_required_elements?).and_return(true)
      allow_any_instance_of(PetriBundle).to receive(:album).and_return(@album)

      @salepoints = []
      stores = Store.where("in_use_flag = 1 and id <> 18").limit(5)
      stores.each do |s|
        @salepoints << TCFactory.build_salepoint(store: s, album: @album)
      end
      expect(active_bundle).to eq(@album.petri_bundle)
    end

    it "dismiss all old active bundles" do
      another_active_bundle = TCFactory.create(:petri_bundle, album: @album)
      expect(@album.petri_bundles.active.count).to eq(2)
      new_bundle = PetriBundle.recreate!(@album)
      expect(@album.reload.petri_bundles.active.count).to eq(1)
    end

    it "dismiss old bundle and recreate a new bundle" do
      new_bundle = PetriBundle.recreate!(@album.reload)
      expect(active_bundle.reload.state).to eq('dismissed')
      expect(@album.petri_bundle).to eq(new_bundle)
    end

    it "keep old bundle attached to the album" do
      PetriBundle.recreate!(@album.reload)
      # for some reason, active_bundle.reload.album still grabs the correct album even though
      # the album_id get sets to nil
      expect(active_bundle.reload.album_id).to eq(@album.id)
    end

    it "throws exception when new bundle creation failed" do
      petri_bundles = double('petri_bundles')
      allow(@album).to receive(:petri_bundles).and_return(petri_bundles)
      allow(petri_bundles).to receive(:create!).and_raise(Exception)
      expect(lambda { PetriBundle.recreate!(@album) }).to raise_exception
    end

    it "throws exception when adding salepoints to bundle failed" do
      allow_any_instance_of(Album).to receive(:salepoints).and_return([])
      expect(lambda { PetriBundle.recreate!(@album) }).to raise_exception
    end
  end

  describe "::recreate!" do
    context "preserves the state of salepoints' distributions in some conditions" do
      it "does not update blocked distributions to new" do
        allow_any_instance_of(Album).to receive(:has_required_elements?).and_return(true)
        distro = create(:distribution, :delivered_to_spotify)
        alb = distro.album
        alb.paid_post_proc
        alb.salepoints.each{|sp| sp.paid_post_proc}

        distro.block(actor: "rspec", message: "blocked, yo")
        PetriBundle.recreate!(distro.album)
        distros = alb.salepoints.first.distributions

        expect(distros.all?(&:blocked?)).to be true
        expect(alb.salepoints.first.blocked?).to be true
      end

      it "does not update pending_approval distributions to new" do
        allow_any_instance_of(Album).to receive(:has_required_elements?).and_return(true)
        distro = create(:distribution, :delivered_to_spotify)
        alb = distro.album
        alb.paid_post_proc
        alb.salepoints.each{|sp| sp.paid_post_proc}

        distro.pending_approval(actor: "rspec", message: "pending approval, yo")
        alb.locked_by = nil
        alb.save
        PetriBundle.recreate!(alb)
        distros = alb.salepoints.first.distributions

        expect(distros.all?(&:pending_approval?)).to be true
      end

      it "does not update dismissed distributions to new" do
        allow_any_instance_of(Album).to receive(:has_required_elements?).and_return(true)
        distro = create(:distribution, :delivered_to_spotify)
        alb = distro.album
        alb.paid_post_proc
        alb.salepoints.each(&:paid_post_proc)

        distro.dismiss(actor: "rspec", message: "dismissed, yo")
        PetriBundle.recreate!(distro.album)
        distros = alb.salepoints.first.distributions
        expect(distros.all?(&:dismissed?)).to be true
        expect(Distribution.last.state).not_to be("new")
      end
    end
  end

  describe ".dismiss" do
    let(:album) { TCFactory.create(:album, person: person, payment_applied: true) }
    let(:person) { TCFactory.create(:person) }
    let(:active_bundle) { TCFactory.create(:petri_bundle, album: album, state: 'new') }

    before(:each) do
      expect(album.locked_by_id).to be_nil
    end

    context "dismissing 'new' bundle" do
      it "does not lock the album" do
        active_bundle.dismiss({})
        expect(album.locked_by_id).to be_nil
      end
    end
  end
end
