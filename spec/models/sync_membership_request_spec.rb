require "rails_helper"

describe SyncMembershipRequest do

  it "should set a default status of pending" do
    sr = TCFactory.create(:sync_membership_request)

    assert sr.status == "pending"
  end

  it "should not allow duplicate emails" do
    sr = TCFactory.create(:sync_membership_request)
    assert sr.valid?

    sr = TCFactory.build(:sync_membership_request)
    assert !sr.valid?
    assert !sr.errors[:email].blank?
  end

  it "should request status to be pending,approved,declined" do
    sr = TCFactory.build(:sync_membership_request)
    sr.status = "pending"
    assert sr.valid?

    sr.status = "approved" 
    assert sr.valid?

    sr.status = "declined"
    assert sr.valid?

    sr.status = "test"
    assert !sr.valid?
    assert !sr.errors[:status].blank?
  end


  describe "approve" do

    it "should mark the request as approved" do
      sr = TCFactory.create(:sync_membership_request)
      expect(sr.status).to eq("pending")

      sr.approve
      expect(sr.reload.status).to eq("approved")
    end

  end

  describe "decline" do 

    it "should mark the request as declined" do
      sr = TCFactory.create(:sync_membership_request)
      expect(sr.status).to eq("pending")

      sr.decline
      expect(sr.reload.status).to eq("declined")
    end
    
  end

end

