require 'rails_helper'

describe YoutubeSrMailTemplate::EmailBuilder do
  let(:user)            { FactoryBot.create(:person, email: "testuser1@test.com") }
  let(:another_user)    { FactoryBot.create(:another_person) }
  let(:album)           { FactoryBot.create(:album, :with_uploaded_songs, person: user) }
  let(:another_album)   { FactoryBot.create(:album, :with_uploaded_songs, person: another_user) }
  let(:songs)           { extract_songs([album, another_album]) }
  let(:email_template)  { YoutubeSrMailTemplate.return_template("removed_monetization") }
  let(:data_hash)       { prepare_email_data_hash(email_template, [album, another_album])["email_data"] }
  let(:email_builder)   { YoutubeSrMailTemplate::EmailBuilder.new(email_template, data_hash) }

  it "stores an email template" do
    expect(email_builder.template).to eq(email_template)
  end

  it "stores email data" do
    expect(email_builder.email_data).to eq(data_hash)
  end

  context "processing emails" do

    describe "#process" do
      before(:each) do
        @process_complete = email_builder.process
      end

      it "stores the number of processed people" do
        expect(email_builder.processed_people.size).to eq(2)
      end

      it "stores the number of processed songs" do
        expect(email_builder.processed_songs.size).to eq(songs.size)
      end

      it 'returns true when completed' do
        expect(@process_complete).to eq true
      end
    end

    describe "#processed_isrcs" do
      it "returns an array of of isrc numbers that were processed" do
        email_builder.process
        expected = songs.map(&:isrc)
        expect(email_builder.processed_isrcs).to eq(expected)
      end
    end

    describe "#send_to_mailer" do
      it "adds an email to sidekiq table" do
        expect(Ytsr::EmailWorker).to receive(:perform_async).twice
        email_builder.process
      end
    end
  end
end
