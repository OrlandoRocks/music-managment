require "rails_helper"

describe Booklet do
  before(:each) do
    @person = TCFactory.create(:person)
    @album = TCFactory.create(:album, :person => @person, :album_type => "Album")
    @booklet = TCFactory.create(:booklet, :album => @album)
  end

  describe "#can_distribute?" do
    it "should properly determine if distributable" do
      expect(@booklet.can_distribute?).to eq(false)

      product = TCFactory.build_ad_hoc_album_product(:product)
      Product.add_to_cart(@person, @album)
      purchase = @album.purchases.first

      expect(@booklet.can_distribute?).to eq(true)

      invoice = TCFactory.create(:invoice)
      purchase.invoice = invoice
      purchase.save!

      expect(@booklet.can_distribute?).to eq(false)

      @album.update(:finalized_at => Time.now())

      expect(@booklet.can_distribute?).to eq(false)
    end

  end

  describe "#paid?" do
    it "should return true if either booklet or Inventory.used? or puchase paid" do
      product = TCFactory.build_ad_hoc_booklet_product(20)
      purchase = TCFactory.create(:purchase, :person_id => @person.id, :product_id => product.id, :related => @booklet, :paid_at => nil)

      @booklet.purchase
      expect(@booklet.paid?).to eq(false)

      @booklet.update(:paid_at => Time.now())
      expect(@booklet.paid?).to eq(true)

      @booklet.update(:paid_at => nil)
      expect(@booklet.paid?).to eq(false)

      @booklet.purchase.update(:paid_at => Time.now())
      expect(@booklet.paid?).to eq(true)

      @booklet.purchase.update(:paid_at => nil)
      expect(@booklet.paid?).to eq(false)

      product = TCFactory.build_ad_hoc_booklet_product
      inventory = TCFactory.create(:inventory,  :person => @person,
                                                :purchase_id => purchase.id,
                                                :product_item => product.product_items.first)

      usage = InventoryUsage.create(:inventory => inventory,
                                    :related => @booklet)

      expect(Inventory).to receive(:used?, &Inventory.method(:used?))
      expect(@booklet.paid?).to eq(true)

    end

  end

end
