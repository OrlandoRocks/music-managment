require "rails_helper"

describe PersonPreference, "checking the person helper methods" do
  before(:each) do
    @person = TCFactory.build_person
    allow_any_instance_of(StoredCreditCard).to receive(:update_with_braintree)
  end

  context "when a person does NOT have preferences" do
    before(:each) do
      expect(@person.person_preference.nil?).to eq(true)
    end
    specify {expect(@person.preferences_set?).to  eq(false)}
    specify {expect(@person.autorenew?).to    eq(true)}
    specify {expect(@person.renew_with_balance?).to   eq(true)}
    specify {expect(@person.renew_with_credit_card?).to eq(false)}
    specify {expect(@person.renew_with_paypal?).to    eq(false)}
    specify {expect(@person.preferred_paypal_account?).to   eq(false)}
    specify {expect(@person.preferred_credit_card?).to  eq(false)}
    specify {expect(@person.preferred_paypal_account).to  eq(nil)}
    specify {expect(@person.preferred_credit_card_id).to  eq(nil)}
    specify {expect(@person.preferred_payment_account).to  eq(nil)}
  end

  context "when a person has preferences with balance (no CC or paypal)" do
    before(:each) do
      @person.create_person_preference(:pay_with_balance => 1, :do_not_autorenew=>0)
    end
    specify {expect(@person.preferences_set?).to  eq(true)}
    specify {expect(@person.autorenew?).to    eq(true)}
    specify {expect(@person.renew_with_balance?).to   eq(true)}
    specify {expect(@person.renew_with_credit_card?).to eq(false)}
    specify {expect(@person.renew_with_paypal?).to    eq(false)}
    specify {expect(@person.preferred_paypal_account?).to   eq(false)}
    specify {expect(@person.preferred_credit_card?).to  eq(false)}
    specify {expect(@person.preferred_credit_card_expired?).to  eq(false)} #since it doesn't exist, it is not expired
    specify {expect(@person.preferred_paypal_account).to  eq(nil)}
    specify {expect(@person.preferred_credit_card_id).to  eq(nil)}
    specify {expect(@person.preferred_payment_account).to  eq(nil)}
  end

  context "when a person has preferences with balance AND Valid Credit Card" do
    before(:each) do
      @stored_credit_card = StoredCreditCard.create(:person=>@person, :customer_vault_id=>1234, :expiration_year=>'2100', :expiration_month=>'01')
      @person.create_person_preference(:pay_with_balance => 1, :do_not_autorenew=>0, :preferred_credit_card=>@stored_credit_card)
    end
    specify {expect(@person.preferences_set?).to  eq(true)}
    specify {expect(@person.autorenew?).to    eq(true)}
    specify {expect(@person.renew_with_balance?).to   eq(true)}
    specify {expect(@person.renew_with_credit_card?).to eq(true)}
    specify {expect(@person.renew_with_paypal?).to    eq(false)}
    specify {expect(@person.preferred_paypal_account?).to   eq(false)}
    specify {expect(@person.preferred_credit_card?).to  eq(true)}
    specify {expect(@person.preferred_credit_card_expired?).to  eq(false)}
    specify {expect(@person.preferred_paypal_account).to  eq(nil)}
    specify {expect(@person.preferred_credit_card_id).to  eq(@stored_credit_card.id)}
    specify {expect(@person.preferred_payment_account).to  eq(@stored_credit_card)}
  end

  context "when a person has preferences with balance AND Expired Credit Card" do
    before(:each) do
      @stored_credit_card = StoredCreditCard.create(:person=>@person, :customer_vault_id=>1234, :expiration_year=>'2000', :expiration_month=>'01')
      @person.create_person_preference(:pay_with_balance => 1, :do_not_autorenew=>0, :preferred_credit_card=>@stored_credit_card)
    end

    specify {expect(@person.preferences_set?).to  eq(true)}
    specify {expect(@person.autorenew?).to    eq(true)}
    specify {expect(@person.renew_with_balance?).to   eq(true)}
    specify {expect(@person.renew_with_credit_card?).to eq(false)} #since it is expired, does not register as a preferred card
    specify {expect(@person.renew_with_paypal?).to    eq(false)}
    specify {expect(@person.preferred_paypal_account?).to   eq(false)}
    specify {expect(@person.preferred_credit_card?).to  eq(false)} #since it is expired, does not register as a preferred card
    specify {expect(@person.preferred_credit_card_expired?).to  eq(true)}
    specify {expect(@person.preferred_paypal_account).to  eq(nil)}
    specify {expect(@person.preferred_credit_card_id).to  eq(nil)} #since it is expired, does not register as a preferred card
    specify {expect(@person.preferred_payment_account).to  eq(nil)} #since it is expired, does not register as a preferred card
  end

  context "when a person has preferences with balance AND Valid Paypal Account" do
    before(:each) do
      @stored_paypal_account = TCFactory.create(:stored_paypal_account, :person=>@person)
      @person.person_preference.reload.update(:pay_with_balance => 1, :do_not_autorenew=>0,
                                       :preferred_paypal_account=>@stored_paypal_account, :preferred_payment_type=>'PayPal')
    end
    specify {expect(@person.preferences_set?).to  eq(true)}
    specify {expect(@person.autorenew?).to    eq(true)}
    specify {expect(@person.renew_with_balance?).to   eq(true)}
    specify {expect(@person.renew_with_credit_card?).to eq(false)}
    specify {expect(@person.renew_with_paypal?).to    eq(true)}
    specify {expect(@person.preferred_paypal_account?).to   eq(true)}
    specify {expect(@person.preferred_credit_card?).to  eq(false)}
    specify {expect(@person.preferred_credit_card_expired?).to  eq(false)}
    specify {expect(@person.preferred_paypal_account).to  eq(@stored_paypal_account)}
    specify {expect(@person.preferred_credit_card_id).to  eq(nil)}
    specify {expect(@person.preferred_payment_account).to  eq(@stored_paypal_account)}
  end

  context "when a person has preferences with balance, has both stored credit card and paypal, selects 'PayPal' as preferred_type" do
    before(:each) do
      @stored_paypal_account = TCFactory.create(:stored_paypal_account, :person=>@person)
      @stored_credit_card = StoredCreditCard.create(:person=>@person, :customer_vault_id=>1234, :expiration_year=>'2100', :expiration_month=>'01')
      @person.person_preference.reload.update(:pay_with_balance => 1, :do_not_autorenew=>0,
                                       :preferred_paypal_account=>@stored_paypal_account,
                                       :preferred_credit_card=>@stored_credit_card, :preferred_payment_type=>'PayPal')
    end
    specify {expect(@person.preferences_set?).to  eq(true)}
    specify {expect(@person.autorenew?).to    eq(true)}
    specify {expect(@person.renew_with_balance?).to   eq(true)}
    specify {expect(@person.renew_with_credit_card?).to eq(false)}
    specify {expect(@person.renew_with_paypal?).to    eq(true)}
    specify {expect(@person.preferred_paypal_account?).to   eq(true)}
    specify {expect(@person.preferred_credit_card?).to  eq(true)}
    specify {expect(@person.preferred_credit_card_expired?).to  eq(false)}
    specify {expect(@person.preferred_paypal_account).to  eq(@stored_paypal_account)}
    specify {expect(@person.preferred_credit_card_id).to  eq(@stored_credit_card.id)}
    specify {expect(@person.preferred_payment_account).to  eq(@stored_paypal_account)}
  end

  context "when a person has preferences with balance, has both stored credit card and paypal, selects 'CreditCard' as preferred_type" do
    before(:each) do
      @stored_paypal_account = TCFactory.create(:stored_paypal_account, :person=>@person)
      @stored_credit_card = StoredCreditCard.create(:person=>@person, :customer_vault_id=>1234, :expiration_year=>'2100', :expiration_month=>'01')
      @person.person_preference.reload.update(:pay_with_balance => 1, :do_not_autorenew=>0,
                                       :preferred_paypal_account=>@stored_paypal_account,
                                       :preferred_credit_card=>@stored_credit_card, :preferred_payment_type=>'CreditCard')
    end
    specify {expect(@person.preferences_set?).to  eq(true)}
    specify {expect(@person.autorenew?).to    eq(true)}
    specify {expect(@person.renew_with_balance?).to   eq(true)}
    specify {expect(@person.renew_with_credit_card?).to eq(true)}
    specify {expect(@person.renew_with_paypal?).to    eq(false)}
    specify {expect(@person.preferred_paypal_account?).to   eq(true)}
    specify {expect(@person.preferred_credit_card?).to  eq(true)}
    specify {expect(@person.preferred_credit_card_expired?).to  eq(false)}
    specify {expect(@person.preferred_paypal_account).to  eq(@stored_paypal_account)}
    specify {expect(@person.preferred_credit_card_id).to  eq(@stored_credit_card.id)}
    specify {expect(@person.preferred_payment_account).to  eq(@stored_credit_card)}
  end

  context "when a person has preferences with balance, has both stored credit card and paypal, selects 'CreditCard' as preferred_type" do
    before(:each) do
      @stored_paypal_account = TCFactory.create(:stored_paypal_account, :person=>@person)
      @stored_credit_card = StoredCreditCard.create(:person=>@person, :customer_vault_id=>1234, :expiration_year=>'2100', :expiration_month=>'01')
      @person.person_preference.reload.update(:pay_with_balance => 1, :do_not_autorenew=>0,
                                       :preferred_paypal_account=>@stored_paypal_account,
                                       :preferred_credit_card=>@stored_credit_card, :preferred_payment_type=>'CreditCard')
    end
    specify {expect(@person.preferences_set?).to  eq(true)}
    specify {expect(@person.autorenew?).to    eq(true)}
    specify {expect(@person.renew_with_balance?).to   eq(true)}
    specify {expect(@person.renew_with_credit_card?).to eq(true)}
    specify {expect(@person.renew_with_paypal?).to    eq(false)}
    specify {expect(@person.preferred_paypal_account?).to   eq(true)}
    specify {expect(@person.preferred_credit_card?).to  eq(true)}
    specify {expect(@person.preferred_credit_card_expired?).to  eq(false)}
    specify {expect(@person.preferred_paypal_account).to  eq(@stored_paypal_account)}
    specify {expect(@person.preferred_credit_card_id).to  eq(@stored_credit_card.id)}
    specify {expect(@person.preferred_payment_account).to  eq(@stored_credit_card)}
  end

  context "when a person has preferences TO NOT pay with balance, has both stored credit card and paypal, selects 'CreditCard' as preferred_type" do
    before(:each) do
      @stored_paypal_account = TCFactory.create(:stored_paypal_account, :person=>@person)
      @stored_credit_card = StoredCreditCard.create(:person=>@person, :customer_vault_id=>1234, :expiration_year=>'2100', :expiration_month=>'01')
      @person.person_preference.reload.update(:pay_with_balance => 0, :do_not_autorenew=>0,
                                       :preferred_paypal_account=>@stored_paypal_account,
                                       :preferred_credit_card=>@stored_credit_card, :preferred_payment_type=>'CreditCard')
    end
    specify {expect(@person.preferences_set?).to  eq(true)}
    specify {expect(@person.autorenew?).to    eq(true)}
    specify {expect(@person.renew_with_balance?).to   eq(false)}
    specify {expect(@person.renew_with_credit_card?).to eq(true)}
    specify {expect(@person.renew_with_paypal?).to    eq(false)}
    specify {expect(@person.preferred_paypal_account?).to   eq(true)}
    specify {expect(@person.preferred_credit_card?).to  eq(true)}
    specify {expect(@person.preferred_credit_card_expired?).to  eq(false)}
    specify {expect(@person.preferred_paypal_account).to  eq(@stored_paypal_account)}
    specify {expect(@person.preferred_credit_card_id).to  eq(@stored_credit_card.id)}
    specify {expect(@person.preferred_payment_account).to  eq(@stored_credit_card)}
  end

  context "when a person has selected to not autorenew (has both stored credit card and paypal, selects 'CreditCard' as preferred_type)" do
    before(:each) do
      @stored_paypal_account = TCFactory.create(:stored_paypal_account, :person=>@person)
      @stored_credit_card = StoredCreditCard.create(:person=>@person, :customer_vault_id=>1234, :expiration_year=>'2100', :expiration_month=>'01')
      @person.person_preference.reload.update(:pay_with_balance => 1, :do_not_autorenew=>1,
                                       :preferred_paypal_account=>@stored_paypal_account,
                                       :preferred_credit_card=>@stored_credit_card, :preferred_payment_type=>'CreditCard')
    end
    specify {expect(@person.preferences_set?).to  eq(true)}
    specify {expect(@person.autorenew?).to    eq(false)}
    specify {expect(@person.renew_with_balance?).to   eq(false)}
    specify {expect(@person.renew_with_credit_card?).to eq(false)}
    specify {expect(@person.renew_with_paypal?).to    eq(false)}
    specify {expect(@person.preferred_paypal_account?).to   eq(true)}
    specify {expect(@person.preferred_credit_card?).to  eq(true)}
    specify {expect(@person.preferred_credit_card_expired?).to  eq(false)}
    specify {expect(@person.preferred_paypal_account).to  eq(@stored_paypal_account)}
    specify {expect(@person.preferred_credit_card_id).to  eq(@stored_credit_card.id)}
    specify {expect(@person.preferred_payment_account).to  eq(nil)}
  end
end

describe PersonPreference, "When setting a preferred payment account" do

  before(:each) do
    @person                = TCFactory.create(:person)

    mock_braintree_query
    @stored_credit_card    = TCFactory.create(:stored_credit_card, :person_id=>@person.id, :expiration_year=>Date.today.year + 2, :expiration_month=>Date.today.month)
    @stored_paypal_account = TCFactory.create(:stored_paypal_account, :person=>@person)

    #Create a preference and assert its valid as a base case
    @person.person_preference.reload.update(:preferred_credit_card_id=>@stored_credit_card.id)
    expect(@person.person_preference).not_to eq(nil)
    expect(@person.person_preference).to be_valid

    @person2                = TCFactory.create(:person)
    @stored_credit_card2    = TCFactory.create(:stored_credit_card, :person_id=>@person2.id)
    @stored_paypal_account2 = TCFactory.create(:stored_paypal_account, :person=>@person2)

  end

  it "should not let you save a different person's stored credit card" do
    @person.person_preference.preferred_credit_card = @person2.stored_credit_cards.first
    @person.person_preference.save
    expect(@person.person_preference).not_to be_valid
  end

  it "should not let you save a different person's stored paypal account" do
    @person.person_preference.preferred_paypal_account = @person2.stored_paypal_accounts.first
    @person.person_preference.save
    expect(@person.person_preference).not_to be_valid
  end

end

describe PersonPreference, "Removing stored credit cards and paypal accounts" do

  before(:each) do
    allow(Braintree::PaymentMethod).to receive(:delete).and_return(true)
    #Create a person
    @person = TCFactory.create(:person)

    mock_braintree_query
    #Create stored credit cards and paypal accounts
    2.times do
      TCFactory.create(:stored_credit_card, :person_id=>@person.id)
      TCFactory.create(:stored_paypal_account, :person=>@person)
    end

    @person.person_preference.reload.update(:preferred_payment_type=>"CreditCard", :preferred_credit_card_id=>@person.stored_credit_cards.first.id)
    expect(@person.person_preference).to be_valid
    expect(@person.stored_credit_cards).not_to be_empty
    expect(@person.stored_paypal_accounts).not_to be_empty
  end

  context "when a person prefers CreditCard"  do
    before(:each) do
      allow(Braintree::PaymentMethod).to receive(:delete).and_return(true)
      @person.person_preference.preferred_payment_type == "CreditCard"
      expect(@person.person_preference).to be_valid
    end

    context "when there are no paypal accounts" do

      before(:each) do
        @person.stored_paypal_accounts.each { |account| account.destroy(@person) }
        expect(@person.stored_paypal_accounts.active).to be_empty
      end

      it "should not delete the only stored credit card" do

        #Delete cards and make sure we can't delete the last one
        size = @person.stored_credit_cards.active.size
        i = 0
        size.times do
          i = i + 1
          if i == size
            expect(@person.stored_credit_cards.active.first.destroy("123", @person)).to eq(false)
            expect(@person.person_preference.reload).to be_valid
          else
            expect(@person.stored_credit_cards.active.first.destroy("123", @person)).to eq(true)
          end
        end
      end
    end

    context "when there is a paypal account" do

      it "should be able to delete all stored credit cards and update to paypal" do

        expect(@person.person_preference.reload.preferred_payment_type).to eq("CreditCard")

        @person.stored_credit_cards.each do  |card|
          expect(card.destroy("123", @person)).to eq(true)
        end

        expect(@person.person_preference.reload.preferred_payment_type).to eq("PayPal")
        expect(@person.person_preference.reload.preferred_paypal_account_id).not_to be_nil

      end

      it "should be able to delete all paypal accounts" do

        @person.stored_paypal_accounts.each do |account|
          expect(account.destroy(@person)).to eq(true)
        end
        expect(@person.person_preference.reload.preferred_payment_type).to eq("CreditCard")

      end

    end

    it  "should allow deletion of the preferred credit card when there are more than one and update the preferred card" do
      #Set a preferred card and make sure we're still valid
      @person.person_preference.preferred_credit_card_id = @person.stored_credit_cards.first.id
      @person.person_preference.save
      expect(@person.person_preference).to be_valid
      expect(@person.stored_credit_cards.size).to be > 1

      #Make sure that when we delete the preferred credit card that its update the preferred credit card
      old_card_id = @person.person_preference.preferred_credit_card_id
      expect(@person.person_preference.preferred_credit_card.destroy("123", @person)).to eq(true)
      expect(@person.person_preference.reload.preferred_credit_card_id).not_to be_nil
      expect(@person.person_preference.reload.preferred_credit_card_id).not_to eq(old_card_id)

    end

  end

  context "when a person prefers PayPal" do
    before(:each) do
      allow(Braintree::PaymentMethod).to receive(:delete).and_return(true)
      @person.person_preference.preferred_payment_type == "PayPal"
      @person.person_preference.preferred_paypal_account = @person.stored_paypal_accounts.active.first
      expect(@person.person_preference).to be_valid
    end

    context "when there is a stored credit card" do

      it "should be able to delete all paypal accounts" do

        #Delete cards and make sure we can't delete the last one
        @person.stored_paypal_accounts.each do |account|
          expect(account.destroy(@person)).to eq(true)
        end

        expect(@person.stored_paypal_accounts.active).to be_empty

      end

      it "should be able to delete all stored credit cards" do
        #Make sure that we can delete all stored cards
        @person.stored_credit_cards.size.times do
            expect(@person.stored_credit_cards.first.destroy("123", @person)).to eq(true)
        end

      end

    end

    context "when there are no stored credit cards" do
      it "should not allow deletion of the paypal account" do
        person = create(:person, :with_stored_paypal_account)
        old_id = person.preferred_paypal_account.id

        expect(person.person_preference).to be_valid
        expect(person.preferred_paypal_account.destroy(person)).to eq(false)
        expect(person.person_preference).to be_valid
        expect(person.person_preference.preferred_paypal_account_id).to eq(old_id)
        expect(person.person_preference.preferred_paypal_account.reload.archived_at).to be_nil
      end
    end
  end
end
