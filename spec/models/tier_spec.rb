require 'rails_helper'

RSpec.describe Tier, type: :model do
  describe "relationships" do
    subject { build(:tier) }

    it { should belong_to(:parent).class_name('Tier') }
    it { should have_many(:tier_people) }
    it { should have_many(:people).through(:tier_people) }
    it { should have_many(:tier_rewards) }
    it { should have_many(:rewards).through(:tier_rewards) }
    it { should have_many(:tier_thresholds) }
    it { should have_many(:tier_certifications) }
    it { should have_many(:certifications).through(:tier_certifications) }
    it { should have_many(:tier_achievements) }
    it { should have_many(:achievements).through(:tier_achievements) }
  end

  describe "validations" do
    let(:subject) { build(:tier) }

    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:hierarchy) }
    it { should validate_uniqueness_of(:name) }

    context "hierarchy" do
      before do
        Tier.skip_callback(:validation, :before, :assign_hierarchy, raise: false)
      end

      after do
        Tier.set_callback(:validation, :before, :assign_hierarchy)
      end

      it { should validate_presence_of(:hierarchy) }
      it { should validate_uniqueness_of(:hierarchy) }
    end

    context "slug" do
      before do
        Tier.skip_callback(:validation, :before, :generate_slug, raise: false)
      end

      after do
        Tier.set_callback(:validation, :before, :generate_slug)
      end

      it { should validate_presence_of(:slug) }
      it { should validate_uniqueness_of(:slug) }
    end

    context "parent" do
      let(:parent_tier) { create(:tier, is_sub_tier: true, parent: create(:tier)) }
      let(:tier) { build(:tier, is_sub_tier: true) }

      context "when parent is blank" do
        before do
          tier.parent_id = 0
          tier.validate
        end

        it "should add error to parent key" do
          expect(tier.errors[:parent]).to include("Invalid parent tier id. Parent not assigned.")
        end

        it "should add error to hierarchy key" do
          expect(tier.errors[:hierarchy]).to include("Invalid parent tier id. Hierarchy not assigned.")
        end
      end

      context "when parent is sub tier" do
        it "should add error to parent key" do
          tier.parent_id = parent_tier.id
          tier.validate

          expect(tier.errors[:parent]).to include("cannot be a sub tier")
        end
      end
    end
  end

  describe "callbacks" do
    describe "#nullify_parent_id" do
      let(:tier) { build(:tier) }

      it "should remove parent id if not sub tier" do
        tier.parent_id = 1
        tier.save

        expect(tier.parent_id).to be_nil
      end
    end

    describe "#assign_hierarchy" do
      it "should assign hierarchy to next number from last used hierarchy" do
        create(:tier, hierarchy: 2.0)
        next_tier = create(:tier, hierarchy: nil)

        expect(next_tier.hierarchy.to_f).to eq(3.0)
      end

      it "should assign hierarchy to next sub number if sub tier" do
        parent = create(:tier, hierarchy: 1.0)
        child_1 = create(:tier, parent: parent, hierarchy: 1.1, is_sub_tier: true)
        next_tier = create(:tier, hierarchy: nil, is_sub_tier: true, parent: parent)

        expect(next_tier.hierarchy.to_f).to eq(1.2)
      end
    end

    describe "#generate_slug" do
      let(:tier) { build(:tier) }

      it "should generate and assign slug" do
        tier.name = "New ARTIST"
        tier.save

        expect(tier.slug).to eq('new_artist')
      end
    end
  end

  describe "#handler_service" do
    it "should return handler service class" do
      tier = create(:tier, name: 'new_artist')

      expect(tier.handler_service).to be_an_instance_of(Tier::ELIGIBILITY_HANDLERS[:new_artist])
    end

    it "should return fallback class for unknown names" do
      tier = create(:tier, name: 'unknown')

      expect(tier.handler_service).to be_an_instance_of(RewardSystem::Tiers::Fallback)
    end
  end
end
