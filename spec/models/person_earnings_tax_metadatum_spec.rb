# frozen_string_literal: true

require "rails_helper"

RSpec.describe PersonEarningsTaxMetadatum, type: :model do
  let(:person) { create(:person) }

  before do
    allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking).and_return(true)
    create(:tax_form, person_id: person.id)
  end

  subject { person.person_earnings_tax_metadata.find_by(tax_year: Date.current.year) }

  describe "association" do
    it { should belong_to(:person) }
    it { should belong_to(:pub_taxform).class_name("TaxForm") }
    it { should belong_to(:dist_taxform).class_name("TaxForm") }
  end

  describe "validations" do
    it { should validate_presence_of(:person) }
  end

  describe "callbacks" do
    before(:each) do
      allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking).and_return(true)
    end

    context "when specific attributes are updated for current year" do
      PersonEarningsTaxMetadatum::TAXFORM_ATTRIBUTES.each do |attr|
        it "updates tax eligibility" do
          expect(TaxBlocking::UpdatePersonTaxblockStatusService).to receive(:call).with(person)
          subject.update(attr => 123)
        end
      end
    end

    context "when specific attributes are updated for non-current year" do
      PersonEarningsTaxMetadatum::TAXFORM_ATTRIBUTES.each do |attr|
        it "doesn't update tax eligibility" do
          meta_data = create(:person_earnings_tax_metadatum, :tax_blocked, person: person, tax_year: 2.years.ago.year)
          expect(TaxBlocking::UpdatePersonTaxblockStatusService).not_to receive(:call).with(person)
          meta_data.update(attr => 123)
        end
      end
    end

    describe "notifications" do
      context "when a person gets tax-blocked" do
        before do
          # Enables the person to get tax-blocked
          person.tax_forms.destroy_all
          PersonTransaction.create(
            person: person, credit: 12.15, previous_balance: person.balance, target_type: "PersonIntake"
          )
        end

        it "notifies the user of tax-block" do
          # Evaluates tax-blocking
          TaxBlocking::PersonEarningsSummarizationService.new(person.id, Date.current.year).summarize
          TaxBlocking::TaxformInfoUpdateService.new(person.id, Date.current.year).summarize
          TaxBlocking::UpdatePersonTaxblockStatusService.call(person)

          expect(person.notifications.last).not_to be_blank
          expect(person.notifications.last).to have_attributes(
            text: I18n.t("notifications.tax_form_request_body"),
            title: I18n.t("notifications.tax_form_request_title")
          )
        end

        context "when the person has a payoneer account" do
          it "sets notification redirection to 'Taxpayer ID' tab" do
            # Evaluates tax-blocking
            allow_any_instance_of(Person).to receive(:payoneer_account?).and_return(true)
            TaxBlocking::PersonEarningsSummarizationService.new(person.id, Date.current.year).summarize
            TaxBlocking::TaxformInfoUpdateService.new(person.id, Date.current.year).summarize
            TaxBlocking::UpdatePersonTaxblockStatusService.call(person)

            expect(person.notifications.last).not_to be_blank
            expect(person.notifications.last).to have_attributes(
              text: I18n.t("notifications.tax_form_request_body"),
              title: I18n.t("notifications.tax_form_request_title"),
              url: Rails.application.routes.url_helpers.account_settings_path(tab: "taxpayer_id")
            )
          end
        end

        context "when the person doesn't have a payoneer account" do
          it "sets notification redirection to 'Payout Preferences' tab" do
            # Evaluates tax-blocking
            TaxBlocking::PersonEarningsSummarizationService.new(person.id, Date.current.year).summarize
            TaxBlocking::TaxformInfoUpdateService.new(person.id, Date.current.year).summarize
            TaxBlocking::UpdatePersonTaxblockStatusService.call(person)

            expect(person.notifications.last).not_to be_blank
            expect(person.notifications.last).to have_attributes(
              text: I18n.t("notifications.tax_form_request_body"),
              title: I18n.t("notifications.tax_form_request_title"),
              url: Rails.application.routes.url_helpers.account_settings_path(tab: "payoneer_payout")
            )
          end
        end

        it "doesn't notify the user when the 'advanced_tax_blocking' feature flag is unset" do
          allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking).and_return(false)

          # Evaluates tax-blocking
          TaxBlocking::PersonEarningsSummarizationService.new(person.id, Date.current.year).summarize
          TaxBlocking::TaxformInfoUpdateService.new(person.id, Date.current.year).summarize
          TaxBlocking::UpdatePersonTaxblockStatusService.call(person)

          expect(person.notifications.last).to be_blank
        end
      end
    end

    context "when non-specific attributes are updated" do
      it "doesn't update tax eligibility" do
        expect(TaxBlocking::UpdatePersonTaxblockStatusService).not_to receive(:call).with(person)
        subject.update(tax_blocked: false)
      end
    end

    context "when the feature flag 'advanced_tax_blocking' is not set" do
      it "doesn't update tax eligibility" do
        allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking).and_return(false)
        expect(TaxBlocking::UpdatePersonTaxblockStatusService).not_to receive(:call).with(person)
        subject.update(dist_taxform_id: 123)
      end
    end
  end
end
