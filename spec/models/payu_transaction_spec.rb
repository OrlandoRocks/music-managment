require 'rails_helper'

describe PayuTransaction, type: :model do
  let!(:payu_transaction) { create(:payu_transaction) }
  let!(:not_fetched_response) { Faraday::Response.new(
    status: "200",
    body: Oj.dump(
      "status": 0,
      "msg": "0 out of 1 Transactions Fetched Successfully",
      "transaction_details": {
        "7fa6c4783a363b3da57": {
              "mihpayid": "Not Found",
              "status": "Not Found",
        }
      }
    )
  )}
  let!(:failure_response) { Faraday::Response.new(
    status: "200",
    body: Oj.dump(
      "status": 1,
      "msg": "1 out of 1 Transactions Fetched Successfully",
      "transaction_details": {
            "7fa6c4783a363b3da573": {
                  "mihpayid": "403993715521889530",
                  "txnid": "7fa6c4783a363b3da573",
                  "status": "failed",
                  "error_code": "E202"
            }
      }
    )
  )}
  let!(:success_response) { Faraday::Response.new(
    status: "200",
    body: Oj.dump(
      "status": 1,
      "msg": "1 out of 1 Transactions Fetched Successfully",
      "transaction_details": {
            "7fa6c4783a363b3da573": {
                  "mihpayid": "403993715521889530",
                  "txnid": "7fa6c4783a363b3da573",
                  "status": "success",
                  "error_code": "E000",
                  "PG_TYPE": "CC"
            }
      }
    )
  )}

  context "#failed!" do
    it "marks txn as failed" do
      payu_transaction.failed!
      expect(payu_transaction.status).to be false
    end
  end

  context "#succeeded!" do
    it "marks txn as successful" do
      payu_transaction.succeeded!
      expect(payu_transaction.status).to be true
    end

    it "settles invoice if possible" do
      payu_transaction.invoice.update!(settled_at: nil)
      expect(payu_transaction.invoice.settled?).to be false

      payu_transaction.succeeded!
      expect(payu_transaction.invoice.settled?).to be true
    end
  end

  context "#refresh!" do
    before(:each) do
      allow(payu_transaction).to receive(:log_response_to_s3)
    end

    context "when the txn is found and successful" do
      before(:each) do
        allow(payu_transaction).to receive(:payu_verify_payment).and_return(Payu::Responses::VerifyPayment.new(success_response))
      end

      it "sets the txn status as success" do
        payu_transaction.refresh!
        expect(payu_transaction.status).to be true
      end

      it "sets the txn's PayU ID (a.k.a. the mihpayid)" do
        payu_transaction.refresh!
        expect(payu_transaction.payu_id).to eq("403993715521889530")
      end

      it "stores the payment_method" do
        payu_transaction.refresh!
        expect(payu_transaction.payment_method).to eq("CC")
      end

      it "stores the error_code as 'E000', meaning payment succeeded" do
        payu_transaction.refresh!
        expect(payu_transaction.error_code).to eq("E000")
      end

    end

    context "when the txn is found and unsuccesful" do
      before(:each) do
        allow(payu_transaction).to receive(:payu_verify_payment).and_return(Payu::Responses::VerifyPayment.new(failure_response))
      end

      it "marks the txn as failed" do
        expect{ payu_transaction.refresh! }.to raise_error(PayuTransaction::PayuTransactionError)

        expect( payu_transaction.status ).to be false
      end

      it "sets the txn's PayU ID (a.k.a. the mihpayid)" do
        expect{ payu_transaction.refresh! }.to raise_error(PayuTransaction::PayuTransactionError)

        expect(payu_transaction.payu_id).to eq("403993715521889530")
      end

      it "stores the error code" do
        expect{ payu_transaction.refresh! }.to raise_error(PayuTransaction::PayuTransactionError)

        expect(payu_transaction.error_code).to eq("E202")
      end
    end

    context "when the txn is not found" do
      before(:each) do
        allow(payu_transaction).to receive(:payu_verify_payment).and_return(Payu::Responses::VerifyPayment.new(not_fetched_response))
      end

      it "marks the txn as failed" do
        expect{ payu_transaction.refresh! }.to raise_error(PayuTransaction::PayuTransactionError)

        expect(payu_transaction.status).to be false
      end

      it "the txn's PayU ID is NULL" do
        expect{ payu_transaction.refresh! }.to raise_error(PayuTransaction::PayuTransactionError)

        expect(payu_transaction.payu_id).to be nil
      end

      it "the txn's payment_method is NULL" do
        expect{ payu_transaction.refresh! }.to raise_error(PayuTransaction::PayuTransactionError)

        expect(payu_transaction.payment_method).to be nil
      end

      it "there is no error_code" do
        expect{ payu_transaction.refresh! }.to raise_error(PayuTransaction::PayuTransactionError)

        expect(payu_transaction.error_code).to be nil
      end
    end
  end
end
