require "rails_helper"

describe ExternalPurchase do
  describe "when validating" do
    let(:external_purchase) { create(:external_purchase) }

    it "should be valid all required attrs exist" do
      expect(external_purchase.valid?).to eq(true)
    end

    it "should be invalid if currency is missing" do
      external_purchase.currency = nil
      expect(external_purchase.valid?).to eq(false)
    end

    it "should be invalid if person is missing" do
      external_purchase.person = nil
      expect(external_purchase.valid?).to eq(false)
    end

    it "should be invalid if external_product_id is missing" do
      external_purchase.external_product_id = nil
      expect(external_purchase.valid?).to eq(false)
    end
  end

  describe "when adding associated objects" do
    describe "when associating songs" do
      let(:external_purchase) { create(:external_purchase) }

      it "create a ExternalPurchaseItems record" do
        expect(ExternalPurchaseItem.count).to eq(0)
        external_purchase.songs << create(:song)

        expect(ExternalPurchaseItem.count).to eq(1)
      end
    end
  end
end
