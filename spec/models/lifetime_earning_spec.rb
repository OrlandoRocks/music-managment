require 'rails_helper'

describe LifetimeEarning do
  describe "#recalculate!" do
    let(:person) { create(:person) }

    let(:earning) { create(:lifetime_earning, person: person) }

    it "updates the total of person & youtube intakes for the person" do
      create(:person_intake, person: person, amount: 100)
      create(:you_tube_royalty_intake, person: person, amount: 25)
      create(:you_tube_royalty_intake, person: person, amount: 25)

      earning.recalculate!
      expect(earning.person_intake).to eq(100)
      expect(earning.youtube_intake).to eq(50)
    end
  end

  describe "#total_intake" do
    it "should return sum of the intakes" do
      earning = create(:lifetime_earning, person_intake: 10, youtube_intake: 15)

      expect(earning.total_intake).to eq(25.0)
    end
  end
end
