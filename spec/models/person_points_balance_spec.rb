require 'rails_helper'

RSpec.describe PersonPointsBalance, type: :model do
  describe "relationships" do
    subject { build(:person_points_balance) }

    it { should belong_to(:person) }
  end

  describe "validations" do
    let(:subject) { build(:person_points_balance) }

    it { should validate_presence_of(:balance) }
    it { should validate_uniqueness_of(:person_id) }
  end

  describe "#update_balance" do
    it "should update balance with the given amount" do
      balance = create(:person_points_balance, balance: 100)

      balance.update_balance(-10)
      expect(balance.balance).to eq(90)
    end
  end
end
