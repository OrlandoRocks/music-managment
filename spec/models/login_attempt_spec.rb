require "rails_helper"

describe LoginAttempt do
  let(:ip) { "192.168.1.1" }

  describe "#record_failed_attempt" do
    let(:login_attempt) { TCFactory.create(:login_attempt) }

    context "when less than 300 failures" do

      it "increments counts" do
        login_attempt
        LoginAttempt.record_failed_attempt(ip)
        expect(login_attempt.reload.all_time_count).to eq(2)
      end

    end

    context "when reaching 300 failures in < 45 days" do
      before :each do
        login_attempt.all_time_count = 299
        login_attempt.save
      end

      it "continues counting failures" do
        LoginAttempt.record_failed_attempt(ip)
        expect(login_attempt.reload.all_time_count).to eq(300)
      end

    end

    context "when reaching 300 failures in > 45 days" do
      before :each do
        login_attempt.all_time_count = 299
        login_attempt.created_at = Time.now - 50.days
        login_attempt.save
      end

      it "resets counting" do
        LoginAttempt.record_failed_attempt(ip)
        expect(login_attempt.reload.all_time_count).to eq(1)
      end

      it "resets created_at" do
        LoginAttempt.record_failed_attempt(ip)
        expect(login_attempt.reload.created_at).to be > Time.now - 50.days
      end
    end

    context "when failing for the first time" do

      it "creates a login attempt" do
        expect {
          LoginAttempt.record_failed_attempt(ip)
        }.to change{ LoginAttempt.count }.by(1)
      end

    end

  end

  describe "#reset_since_last_login_count" do

    context "when there are no failed login attempts" do

      it "should return nil" do
        expect(LoginAttempt.reset_since_last_login_count(ip)).to be_nil
      end

    end

    context "when there are failed login attempts" do
      before(:each) { LoginAttempt.record_failed_attempt(ip) }
      let(:login_attempt) { LoginAttempt.last }

      it "should reset the since_last_login_count" do
        LoginAttempt.reset_since_last_login_count(ip)
        expect(login_attempt.reload.since_last_login_count).to eq(0)
      end

    end

  end

end
