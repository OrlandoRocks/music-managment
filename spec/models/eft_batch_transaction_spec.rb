require "rails_helper"

#####################################
# = READ ME
# These spec tests cover the EftBatchTransaction Model methods. Normally spec tests should only test one model at a time,
# but in this case, each method call in EftBatchTransaction creates EftBatchTransactionHistory lines and sometimes a PersonTransaction,
# which is what we need to test.
# So these are integration tests, rather than single model tests.
# Note, most of the contexts are nested and build upon earlier before(:each) conditions
#####################################

def create_initial_eft_supporting_items
  @person = TCFactory.build_person(:name => 'Test User', :email => 'eft_test@test.com', :status=>'Active')
  @person.person_balance.update_attribute(:balance, 1000.00)
  @person.original_password = "Testpass123!"

  allow_any_instance_of(StoredBankAccount).to receive(:mark_previous_account_as_deleted)
  @stored_bank_account = StoredBankAccount.new(:person=>@person, :customer_vault_id=>1234)
  @stored_bank_account.save!

  @payout_service_fee = TCFactory.create(:payout_service_fee, :fee_type=>'eft_service', :amount=>2.00)
  @failure_fee = TCFactory.create(:payout_service_fee,:fee_type=>'eft_failure', :amount=>5.00)
  @eft_query = EftQuery.create
  @admin = TCFactory.build_person(:name => 'Admin User', :email => 'eft_test@test.com', :status=>'Active')
end

def should_create_full_rollback
  expect(@person.person_balance.reload.balance).to eq(1000.00)
  rollback_debit = @eft_batch_transaction.eft_batch_transaction_history.find_by(status: 'rollback_debit')
  expect(rollback_debit.amount).to eq(48)
  expect(rollback_debit.person_transaction.credit).to eq(48.00)
  rollback_service_fee = @eft_batch_transaction.eft_batch_transaction_history.find_by(status: 'rollback_service_fee')
  expect(rollback_service_fee.amount).to eq(2)
  expect(rollback_service_fee.person_transaction.credit).to eq(2.00)
end

def eft_history_should_include(*statuses)
  history_statuses = @eft_batch_transaction.eft_batch_transaction_history.reload.collect(&:status)
  statuses.each do |status|
    expect(history_statuses).to include(status)
  end
  # the next two steps verify that there are no duplicates besides email_sent
  history_statuses.delete("email_sent")
  expect(history_statuses.uniq!).to eq(nil) # array.uniq! returns nil if there are no duplicates
end

def eft_history_should_not_include(*statuses)
  history_statuses = @eft_batch_transaction.eft_batch_transaction_history.reload.collect(&:status)
  statuses.each do |status|
    expect(history_statuses).not_to include(status)
  end
end

# this new describe block to isolate
# tests using FactoryBot with let.  Was getting
# some bad data into the db if inside the other
# top level describe that calls all the helper
# methods above.

describe EftBatchTransaction do
  let(:person) do
    create(:person, :with_balance, amount: 50_000,
                       email: "developer@tunecore.com",
                       password: "Testpass123!"
                       )
  end

  let(:stored_bank_account) do
    create(:stored_bank_account,
                       person: person,
                       updated_at: DateTime.new(2015, 01, 01)
                       )
  end

  let(:latest_eft_batch_transaction) do
    create(:eft_batch_transaction,
                       amount: 50,
                       stored_bank_account:  stored_bank_account,
                       password_entered: "Testpass123!",
                       )
  end

  let(:new_eft_batch_transaction) do
    create(:eft_batch_transaction,
                       amount: 50,
                       stored_bank_account:  stored_bank_account,
                       password_entered: "Testpass123!",
                       status: "pending_approval",
                       )
  end

  describe "#suspicious_flags" do
    it "is a hash" do
      expect(latest_eft_batch_transaction.suspicious_flags).to be_a(Hash)
    end

    it "is an empty hash if attribute is nil" do
      latest_eft_batch_transaction.save
      expect(latest_eft_batch_transaction.suspicious_flags).to be_a(Hash)
    end

    it "is false if key exists and is set to false" do
      latest_eft_batch_transaction.suspicious_flags = { "exists" => false }
      expect(latest_eft_batch_transaction.suspicious_flags["exists"]).to eq false
    end

    it "is true if key exists and is set to true" do
      latest_eft_batch_transaction.suspicious_flags = { "exists" => true }
      expect(latest_eft_batch_transaction.suspicious_flags["exists"]).to eq true
    end
  end

  describe "#set_suspicious_flags" do
    it "does not mark as suspicious if this is the first eft_batch" do
      expect(latest_eft_batch_transaction.suspicious_flags["stored_bank_account"]).to eq false
    end

    it "does not mark for non-updated stored bank accounts" do
      latest_eft_batch_transaction.updated_at = DateTime.new(2015, 01, 02)
      latest_eft_batch_transaction.status = "success"
      latest_eft_batch_transaction.save
      expect(new_eft_batch_transaction.suspicious_flags["stored_bank_account"]).to eq false
    end

    it "marks as suspicious for new updated accounts" do
      # if updated_at on stored bank account is greater
      # than the updated_at time
      # on the latest and greatest previous eft_batch
      # then there should be a suspicious flag set
      # on this transaction.
      latest_eft_batch_transaction.updated_at = DateTime.new(2014, 01, 01)
      latest_eft_batch_transaction.status = "success"
      latest_eft_batch_transaction.save
      expect(new_eft_batch_transaction.suspicious_flags["stored_bank_account"]).to eq true
    end

    it "only checks for the most recent successful eft_batch_transaction" do
      latest_eft_batch_transaction.updated_at = DateTime.new(2014, 01, 03)
      latest_eft_batch_transaction.status = "success"
      latest_eft_batch_transaction.save
      yet_another = FactoryBot.create(:eft_batch_transaction,
                                       amount: 50,
                                       stored_bank_account:  stored_bank_account,
                                       password_entered: "Testpass123!",

                                       )
      yet_another.status = "rejected"
      yet_another.save
      expect(new_eft_batch_transaction.suspicious_flags["stored_bank_account"]).to eq true
    end
  end

  describe "#suspicious?" do
    it "is false when flag doesnt exist" do
      expect(latest_eft_batch_transaction.suspicious?(:doesnt_exist)).to eq false
    end

    it "is false when flag is set to false" do
      latest_eft_batch_transaction.suspicious_flags = { "exists" => false }
      latest_eft_batch_transaction.save
      expect(latest_eft_batch_transaction.suspicious?("exists")).to eq false
    end

    it "is true when flag is set to true" do
      latest_eft_batch_transaction.suspicious_flags = { "exists" => true }
      expect(latest_eft_batch_transaction.suspicious?("exists")).to eq true
    end
  end
end

describe EftBatchTransaction do
  describe "flag_new_bank_account?" do
    before(:each) do
      person         = create(:person, :with_balance, amount: 50_000, password: "Testpass123!")
      @bank_account  = create(:stored_bank_account, person: person)
      @eft_bt        = create(:eft_batch_transaction, stored_bank_account: @bank_account, password_entered: "Testpass123!")
    end

    context "false scenarios" do
      context "EFT BT status does NOT equal 'pending_approval'" do
        it "returns false" do
          allow(@eft_bt).to receive(:status).and_return("success")
          expect(@eft_bt.flag_new_bank_account?).to eq false
        end
      end

      context "EFT BT to a bank_account that has previous successful withdrawals and has not been updated" do
        it "returns false" do
          prev_eft_bt = create(:eft_batch_transaction, stored_bank_account: @bank_account, password_entered: "Testpass123!")
          prev_eft_bt.update_attribute("status", "success")
          allow_any_instance_of(StoredBankAccount).to receive(:updated_at).and_return(Time.now - 5.minutes)
          expect(@eft_bt.flag_new_bank_account?).to eq false
        end
      end
    end

    context "true scenarios" do
      context "EFT BT to a bank_account that has NO previous successful withdrawals" do
        it "returns true" do
          allow(@bank_account).to receive(:successful_transaction_total).and_return(0)
          expect(@eft_bt.flag_new_bank_account?).to eq true
        end
      end

      context "EFT BT to a bank_account that has been updated since last successful withdrawal" do
        it "returns true" do
          allow(@bank_account).to receive(:successful_transaction_total).and_return(1201.89)
          @bank_account.update_attribute("bank_name", "Bank of Fraud")
          expect(@eft_bt.flag_new_bank_account?).to eq true
        end
      end
    end
  end
end

describe EftBatchTransaction do
  before(:each) do
    create_initial_eft_supporting_items
  end

  it 'should not allow an overdraft of account' do
    eft_batch_transaction = EftBatchTransaction.new(:amount=>1000.01, :stored_bank_account=>@stored_bank_account, :password_entered => 'Testpass123!')
    expect(eft_batch_transaction).not_to be_valid
  end

  it 'should not allow a negative amount' do
    eft_batch_transaction = EftBatchTransaction.create(:amount=>-1, :stored_bank_account=>@stored_bank_account, :password_entered => 'Testpass123!')
    expect(eft_batch_transaction).not_to be_valid
  end

  it 'should allow a second withdrawal to be made while one is still pending' do
    eft_batch_transaction1 = EftBatchTransaction.create(:amount=>50, :stored_bank_account=>@stored_bank_account, :password_entered => 'Testpass123!')
    eft_batch_transaction2 = EftBatchTransaction.create(:amount=>50, :stored_bank_account=>@stored_bank_account, :password_entered => 'Testpass123!')
    expect(eft_batch_transaction1).to be_valid
    expect(eft_batch_transaction2).to be_valid
  end

  it 'should mark Locked Accounts with a status of pending_approval' do
    @person.update_attribute(:status, 'Locked')
    eft_batch_transaction = EftBatchTransaction.create(:amount=>50, :stored_bank_account=>@stored_bank_account, :password_entered => 'Testpass123!')
    expect(eft_batch_transaction.status).to eq('pending_approval')
  end

  it 'should mark Suspicious Accounts with a status of pending_approval' do
    @person.update_attribute(:status, 'Suspicious')
    eft_batch_transaction = EftBatchTransaction.create(:amount=>50, :stored_bank_account=>@stored_bank_account, :password_entered => 'Testpass123!')
    expect(eft_batch_transaction.status).to eq('pending_approval')
  end

  it 'should not allow an eft withdrawal by a non-US user' do
    #we have to change the country_website_id this way because the country_website_id is defined as an attr_accesor variable in the person model
    @person.send("country_website_id=",2)
    @person.save
    eft_batch_transaction = EftBatchTransaction.create(:amount=>50, :stored_bank_account=>@stored_bank_account, :password_entered => 'Testpass123!')
    expect(eft_batch_transaction.errors.messages[:country_website_id].size).to eq 1
    expect(eft_batch_transaction.errors.messages[:currency].size).to eq 1
  end

  context "Initial Creation | " do
    before(:each) do
      # note that the service fee is taken out of the amount. So passing in :amount => 50, results in amount=48, service_fee = 2
      @eft_batch_transaction = EftBatchTransaction.create(:amount=>50, :stored_bank_account=>@stored_bank_account, :password_entered => 'Testpass123!')
    end

    it 'should debit the service fee and amount from the persons account' do
      expect(@eft_batch_transaction.amount).to eq(48)
      expect(@eft_batch_transaction.service_fee).to eq(2)
      expect(@person.person_balance.reload.balance).to eq(950.00)
    end

    specify { eft_history_should_include('requested','debit_amount','debit_service_fee', 'email_sent')}

    it 'should debit the correct service fee from the persons balance' do
      debit_service_fee = @eft_batch_transaction.eft_batch_transaction_history.find_by(status: 'debit_service_fee')
      expect(debit_service_fee.amount).to eq(-2)
      expect(debit_service_fee.person_transaction.debit).to eq(2.00)
    end

    it 'should debit the correct debit amount from the persons balance' do
      debit_amount = @eft_batch_transaction.eft_batch_transaction_history.find_by(status: 'debit_amount')
      expect(debit_amount.amount).to eq(-48)
      expect(debit_amount.person_transaction.debit).to eq(48.00)
    end

    it 'should generate an order_id automatically after save' do
      expect(@eft_batch_transaction.order_id).not_to eq(nil)
    end

    context "Rollback debit amount | " do
      before(:each) do
        @eft_batch_transaction.send(:rollback_debit)
      end

      specify {eft_history_should_include('rollback_debit')}

      it 'should credit the correct amount to the persons balance' do
        expect(@person.person_balance.reload.balance).to eq(998.00)
        rollback_debit = @eft_batch_transaction.eft_batch_transaction_history.find_by(status: 'rollback_debit')
        expect(rollback_debit.amount).to eq(48)
        expect(rollback_debit.person_transaction.credit).to eq(48.00)
      end

      it 'should not be able to rollback twice' do
        @eft_batch_transaction.send(:rollback_debit)
        expect(@person.person_balance.reload.balance).to eq(998.00)
        history_statuses = @eft_batch_transaction.eft_batch_transaction_history.reload.collect(&:status)
        expect(history_statuses.uniq!).to eq(nil) # array.uniq! returns nil if there are no duplicates
      end
    end

    context "Rollback service fee | " do
      before(:each) do
        @eft_batch_transaction.send(:rollback_service_fee)
      end

      specify { eft_history_should_include('rollback_service_fee') }

      it 'should credit the correct service fee to the persons balance' do
        expect(@person.person_balance.reload.balance).to eq(952.00)
        rollback_service_fee = @eft_batch_transaction.eft_batch_transaction_history.find_by(status: 'rollback_service_fee')
        expect(rollback_service_fee.amount).to eq(2)
        expect(rollback_service_fee.person_transaction.credit).to eq(2.00)
      end

      it 'should not be able to rollback service fee twice' do
        @eft_batch_transaction.send(:rollback_service_fee)
        expect(@person.person_balance.reload.balance).to eq(952.00)
        history_statuses = @eft_batch_transaction.eft_batch_transaction_history.reload.collect(&:status)
        expect(history_statuses.uniq!).to eq(nil) # array.uniq! returns nil if there are no duplicates
      end
    end

    context "Canceled by customer | " do
      before(:each) do
        @eft_batch_transaction.update_attribute(:status, 'pending_approval')
        @eft_batch_transaction.cancel
      end

      specify { eft_history_should_include('canceled','rollback_debit','rollback_service_fee') }
      specify { should_create_full_rollback }
      specify { expect(@eft_batch_transaction.status).to eq('canceled')}

      it 'should be able to canceled if waiting for batch' do
        @eft_batch_transaction = EftBatchTransaction.create(:amount=>50, :stored_bank_account=>@stored_bank_account, :password_entered => 'Testpass123!')
        @eft_batch_transaction.update_attribute(:status, 'waiting_for_batch')
        @eft_batch_transaction.cancel
        eft_history_should_include('canceled','rollback_debit','rollback_service_fee')
      end
    end

    context "Rejected by TC Admin | " do
      before(:each) do
        @eft_batch_transaction.update_attribute(:status, 'pending_approval')
        @eft_batch_transaction.reject(@admin)
      end

      specify { eft_history_should_include('rejected','rollback_debit','rollback_service_fee')}
      specify { should_create_full_rollback }
      specify { expect(@eft_batch_transaction.status).to eq('rejected')}
    end

    context "TC Admin Approved | " do
      before(:each) do
        @eft_batch_transaction.update_attribute(:status, 'pending_approval')
        @eft_batch_transaction.approve(@admin)
      end

      specify { eft_history_should_include('approved')}
      specify { expect(@eft_batch_transaction.status).to eq('waiting_for_batch')}

      context "Errors out during braintree check | " do
        before(:each) do
          @eft_batch_transaction.update_attribute(:status, 'processing_in_batch')
          @eft_batch_transaction.error('12345', '300', 'Unknown Processing Error')
        end

        specify { eft_history_should_include('error','rollback_debit','rollback_service_fee')}
        specify { should_create_full_rollback }
        specify { expect(@eft_batch_transaction.status).to eq('error')}
        specify { expect(@eft_batch_transaction.transaction_id).to eq(12345)}
        specify { expect(@eft_batch_transaction.response_code).to eq('300')}
        specify { expect(@eft_batch_transaction.response_text).to eq('Unknown Processing Error')}
      end

      context "Testing transaction ID as an integer" do
        it 'should store trnasaction ID as an integer if passed in as an integer' do
          @eft_batch_transaction.update_attribute(:status, 'processing_in_batch')
          @eft_batch_transaction.sent_to_bank(123456)
          expect(@eft_batch_transaction.transaction_id).to eq(123456)
        end

        it 'should store transaction ID as an integer if passed in as a string' do
          @eft_batch_transaction.update_attribute(:status, 'processing_in_batch')
          @eft_batch_transaction.sent_to_bank("123456")
          expect(@eft_batch_transaction.transaction_id).to eq(123456)
        end
      end

      context "Successfully sent to bank | " do
        before(:each) do
          @eft_batch_transaction.update_attribute(:status, 'processing_in_batch')
          @eft_batch_transaction.sent_to_bank("123456")
        end

        specify { eft_history_should_include('sent_to_bank')}
        specify { expect(@eft_batch_transaction.status).to eq('sent_to_bank')}
        specify { eft_history_should_not_include('rollback_debit', 'rollback_service_fee')}
        specify { expect(@eft_batch_transaction.transaction_id).to eq(123456)}
        specify { expect(@eft_batch_transaction.response_code).to eq(nil)}
        specify { expect(@eft_batch_transaction.response_text).to eq(nil)}

        context "Failed during deposit to bank | " do
          before(:each) do
            @eft_batch_transaction.failure('3','Unknown Failure on Bank End', @eft_query)
          end

          specify { eft_history_should_include('failure', 'rollback_debit', 'debit_failure_fee')}
          specify { expect(@eft_batch_transaction.status).to eq('failure')}
          specify { expect(@eft_batch_transaction.response_code).to eq('3')}
          specify { expect(@eft_batch_transaction.failure_fee_id).not_to eq(nil)}
          specify { expect(@eft_batch_transaction.response_text).to eq('Unknown Failure on Bank End')}

          # refund debit, charge failure fee
          specify { expect(@person.person_balance.reload.balance).to eq(993.00) }

          it 'should have a history item for debiting failure fee' do
            debit_failure_amount = @eft_batch_transaction.eft_batch_transaction_history.find_by(status: 'debit_failure_fee')
            expect(debit_failure_amount.amount).to eq(-5)
            expect(debit_failure_amount.person_transaction.debit).to eq(5.00)
          end

          it 'should not be able to debit failure fee more than once' do
            @eft_batch_transaction.send(:debit_failure_fee)
            expect(@person.person_balance.reload.balance).to eq(993.00)
          end

          it 'should not be able to be marked as a success after a failure' do
            @eft_batch_transaction.success('1','SUCCESS',eft_query = EftQuery.create)
            expect(@eft_batch_transaction.status).to eq('failure')
            eft_history_should_not_include('success')
          end
        end

        context "Successfully deposited | " do
          before(:each) do
            @eft_batch_transaction.success('1','SUCCESS',@eft_query)
          end

          specify { eft_history_should_include('success')}
          specify { eft_history_should_not_include('rollback_debit', 'rollback_service_fee')}
          specify { expect(@eft_batch_transaction.status).to eq('success')}
          specify { expect(@eft_batch_transaction.response_code).to eq('1')}
          specify { expect(@eft_batch_transaction.response_text).to eq('SUCCESS')}

          it 'should allow a late return failure (after a success)' do
            @eft_batch_transaction.failure('3','Late Return Failure', eft_query = EftQuery.create)
            eft_history_should_include('success','failure', 'rollback_debit', 'debit_failure_fee')
            expect(@eft_batch_transaction.status).to eq('failure')
            expect(@eft_batch_transaction.failure_fee_id).not_to eq(nil)
            expect(@eft_batch_transaction.response_text).to eq('Late Return Failure')
          end
        end
      end
    end
  end
end

describe EftBatchTransaction do
  describe "#save" do
    context "when a customer makes an eft withdrawal" do
      let(:person)                { create(:person, :with_balance, amount: 50_000, password: "Testpass123!") }
      let(:eft_batch_transaction) { build(:eft_batch_transaction, stored_bank_account: stored_bank_account, password_entered: "Testpass123!") }
      let(:stored_bank_account)   { create(:stored_bank_account, person: person) }
      let(:login_event)           { LoginEvent.create(person_id: person.id) }

      it "the withdrawl should be associated to the customers last login event" do
        allow(person).to receive(:last_login_event) { login_event }
        eft_batch_transaction.save

        expect(eft_batch_transaction.login_tracks.last.trackable).to eq eft_batch_transaction
        expect(eft_batch_transaction.login_tracks.last.login_event).to eq login_event
      end
    end
  end

  describe "#can_be_updated_by_bank?" do
    context "when the Eft Batch Transaction status is not 'sent_to_bank' or 'success'" do
      it "returns false" do
        eft_batch_transaction = create(:eft_batch_transaction, :pending_approval, :with_person_balance)

        expect(eft_batch_transaction.can_be_updated_by_bank?).to be_falsy
      end
    end

    context "when the Eft Batch Transaction status is 'sent_to_bank'" do
      it "returns true" do
        eft_batch_transaction = create(:eft_batch_transaction, :sent_to_bank, :with_person_balance)

        expect(eft_batch_transaction.can_be_updated_by_bank?).to be_truthy
      end
    end

    context "when the Eft Batch Transaction status is 'success'" do
      it "returns true" do
        eft_batch_transaction = create(:eft_batch_transaction, :success, :with_person_balance)

        expect(eft_batch_transaction.can_be_updated_by_bank?).to be_truthy
      end
    end
  end

  describe "#failed?" do
    context "when the Eft Batch Transaction status is NOT an eligible Failed Status" do
      it "returns false" do
        eft_batch_transaction = create(:eft_batch_transaction, :pending_approval, :with_person_balance)

        expect(eft_batch_transaction.failed?).to be_falsy
      end
    end

    context "when the Eft Batch Transaction status is an eligible Failed Status" do
      it "returns true" do
        eft_batch_transaction = create(:eft_batch_transaction, :failure, :with_person_balance)

        expect(eft_batch_transaction.failed?).to be_truthy
      end
    end
  end

  describe "#success" do
    context "when the Eft Batch Transaction is NOT waiting for a bank response" do
      it "should not update" do
        eft_batch_transaction = create(:eft_batch_transaction, :pending_approval, :with_person_balance)

        expect(eft_batch_transaction.status).to eq "pending_approval"
        eft_batch_transaction.failure(nil, nil, nil)
        expect(eft_batch_transaction.status).to eq "pending_approval"
      end
    end

    context "when the Eft Batch Transaction is waiting for a bank response" do
      it "updates the transaction to be successful" do
        eft_batch_transaction = create(:eft_batch_transaction, :sent_to_bank, :with_person_balance)

        query_response_code = "1"
        response_text       = "Response Text"
        eft_query           = create(:eft_query)

        eft_batch_transaction.success(query_response_code, response_text, eft_query)

        expect(eft_batch_transaction.reload.status).to eq "success"
        expect(eft_batch_transaction.response_code).to eq query_response_code
        expect(eft_batch_transaction.response_text).to eq response_text
        expect(eft_batch_transaction.eft_query).to eq eft_query
      end

      it "creates an Eft Batch Transaction History record with a status of 'success'" do
        eft_batch_transaction = create(:eft_batch_transaction, :sent_to_bank, :with_person_balance)

        eft_batch_transaction.success(nil, nil, nil)

        eft_batch_transaction.reload

        expect(eft_batch_transaction.eft_batch_transaction_history.pluck(:status)).to include "success"
      end

      it "creates an Eft Batch Transaction History record with a status of 'email_sent'" do
        eft_batch_transaction = create(:eft_batch_transaction, :sent_to_bank, :with_person_balance)

        eft_batch_transaction.success(nil, nil, nil)

        eft_batch_transaction.reload

        expect(eft_batch_transaction.eft_batch_transaction_history.pluck(:status)).to include "email_sent"
      end

      it "issues a success notice" do
        eft_batch_transaction = create(:eft_batch_transaction, :sent_to_bank, :with_person_balance)

        eft_notifier = double(EftNotifier)
        expect(EftNotifier).to receive(:success_notice).with(eft_batch_transaction.id).and_return(eft_notifier)
        expect(eft_notifier).to receive(:deliver)

        eft_batch_transaction.success(nil, nil, nil)
      end
    end
  end

  describe "#failure" do
    context "when the Eft Batch Transaction can NOT be updated by the bank" do
      it "should not update" do
        eft_batch_transaction = create(:eft_batch_transaction, :failure, :with_person_balance)

        expect(eft_batch_transaction.status).to eq "failure"
        eft_batch_transaction.failure(nil, nil, nil)
        expect(eft_batch_transaction.status).to eq "failure"
      end
    end

    context "when the Eft Batch Transaction can be updated by the bank" do
      it "updates the transaction to failure" do
        eft_batch_transaction = create(:eft_batch_transaction, :sent_to_bank, :with_person_balance)

        query_response_code = "1"
        response_text       = "Response Text"
        eft_query           = create(:eft_query)
        failure_fee         = PayoutServiceFee.current("eft_failure")

        eft_batch_transaction.failure(query_response_code, response_text, eft_query)

        expect(eft_batch_transaction.reload.status).to eq "failure"
        expect(eft_batch_transaction.response_code).to eq "1"
        expect(eft_batch_transaction.response_text).to eq "Response Text"
        expect(eft_batch_transaction.eft_query).to eq eft_query
        expect(eft_batch_transaction.failure_fee).to eq failure_fee
      end

      it "creates an Eft Batch Transaction History record with a status of 'failure'" do
        eft_batch_transaction = create(:eft_batch_transaction, :sent_to_bank, :with_person_balance)

        eft_batch_transaction.failure(nil, nil, nil)

        expect(eft_batch_transaction.eft_batch_transaction_history.pluck(:status)).to include "failure"
      end

      it "creates an Eft Batch Transaction History record with a status of 'email_sent'" do
        eft_batch_transaction = create(:eft_batch_transaction, :sent_to_bank, :with_person_balance)

        eft_batch_transaction.failure(nil, nil, nil)

        expect(eft_batch_transaction.eft_batch_transaction_history.pluck(:status)).to include "email_sent"
      end

      it "creates an Eft Batch Transaction History record with a status of 'rollback_debit'" do
        eft_batch_transaction = create(:eft_batch_transaction, :sent_to_bank, :with_person_balance)

        eft_batch_transaction.failure(nil, nil, nil)

        expect(eft_batch_transaction.eft_batch_transaction_history.pluck(:status)).to include "rollback_debit"
      end

      it "creates Person Transactions for a rollback debit and debit failure fee" do
        failure_fee           = PayoutServiceFee.current("eft_failure")
        eft_batch_transaction = create(:eft_batch_transaction, :sent_to_bank, :with_person_balance, failure_fee: failure_fee)

        expect(PersonTransaction).to receive(:create).with(
          person_id: eft_batch_transaction.person.id,
          credit:    eft_batch_transaction.amount,
          currency:  eft_batch_transaction.currency,
          target:    eft_batch_transaction,
          comment:   "Rollback of Electronic Funds Transfer"
        ).and_call_original

        expect(PersonTransaction).to receive(:create).with(
          person_id: eft_batch_transaction.person.id,
          debit:     eft_batch_transaction.failure_fee_amount,
          currency:  eft_batch_transaction.currency,
          target:    eft_batch_transaction,
          comment:   "Electronic Funds Transfer Failure Fee"
        ).and_call_original

        eft_batch_transaction.failure(nil, nil, nil)
      end

      it "creates an Eft Batch Transaction History record with a status of 'debit_failure_fee'" do
        eft_batch_transaction = create(:eft_batch_transaction, :sent_to_bank, :with_person_balance)

        eft_batch_transaction.failure(nil, nil, nil)

        eft_batch_transaction.reload

        expect(eft_batch_transaction.eft_batch_transaction_history.pluck(:status)).to include "debit_failure_fee"
      end

      it "issues a failure notice" do
        eft_batch_transaction = create(:eft_batch_transaction, :sent_to_bank, :with_person_balance)

        eft_notifier = double(EftNotifier)
        expect(EftNotifier).to receive(:failure_notice).with(eft_batch_transaction.id).and_return(eft_notifier)
        expect(eft_notifier).to receive(:deliver)

        eft_batch_transaction.failure(nil, nil, nil)
      end
    end
  end
end
