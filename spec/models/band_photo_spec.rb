require "rails_helper"

describe BandPhoto do
  context "when trying to create a new band photo" do
    it "image's meta data should be set before validation" do
      band_photo = build(:band_photo)
      band_photo.valid?

      expect(band_photo.errors.messages[:size]).to be_empty
      expect(band_photo.errors.messages[:width]).to be_empty
      expect(band_photo.errors.messages[:height]).to be_empty
      expect(band_photo.errors.messages[:mime_type]).to be_empty
      expect(band_photo.size).not_to be_nil
      expect(band_photo.width).not_to be_nil
      expect(band_photo.height).not_to be_nil
      expect(band_photo.mime_type).not_to be_nil
    end
  end

  context "when trying to create a new band photo with blank fields" do
    it "should require person" do
      band_photo = build(:band_photo, file: nil)

      expect(band_photo).not_to be_valid
      expect(band_photo.errors.messages).to be_present
    end
  end

  context "test field limitation" do
    it "should not exceed size range for width, height, bit depth, and size" do
      band_photo = build(:band_photo, width: 4033, height: 3161, bit_depth: 33, size: 5_242_881)
      band_photo.valid?

      expect(band_photo.errors.messages).to be_present
    end

    it "should have a minimal value for width, height, bit depth, and size" do
      band_photo = build(:band_photo, width: 0, height: 0, bit_depth: 0, size: 0)
      band_photo.valid?

      expect(band_photo.errors.messages).to be_present
    end
  end

  context "When all validations pass" do
    it "should successfully save" do
      BandPhoto.const_set("ASSET_BUCKET_NAME", "s3assets.tunecore.com.test")
      BandPhoto.const_set("UPLOAD_ARTWORK_PATH", 'tmp')
      band_photo = build(:band_photo)
      expect(band_photo).to be_valid
      band_photo.save

      # make sure s3 asset is created for original band photo
      expect(band_photo.s3_asset).to be_present
      # make sure thumbnail is generated
      expect(band_photo.derived_media_assets).to be_present
      # make sure thumbnail have an s3 asset
      expect(band_photo.derived_media_assets.first.s3_asset).to be_present
    end

    it "should not allow me to create another one" do
      band_photo = create(:band_photo)
      new_band_photo = build(:band_photo, person: band_photo.person)

      expect(new_band_photo).not_to be_valid
      expect(new_band_photo.errors.messages).to be_present
    end
  end
end
