require "rails_helper"

describe EncumbranceSummary do
  context "#create" do
    it "creates a detail record when a new summary record is created" do
      summary = FactoryBot.create(:encumbrance_summary)
      details = summary.encumbrance_details

      expect(details.length).to eq(1)
      expect(details.first.transaction_amount).to eq(summary.total_amount)
    end
  end
end
