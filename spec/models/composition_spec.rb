#!/bin/env ruby
# encoding: utf-8

require "rails_helper"

describe Composition do
  describe "save cwr_name with transliteration values" do
    it "should convert and save with correct transliteration mapping" do
      invalid_value = "ŠŒœŸÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßĄąĆćĘęŁłŃńŚśŻżŹźđČčĚěŇňŘřŤťŮůŨũĀāĒēĢģĪīĶķĻļŅņŪūĖėĮįŲųĊċĠġĦħỲỳŶŷȘșĂăŢţĎďŔŕĞğİŞşŰűŽž"
      transliterated_value = "SOEOEYAAAAAEAAAECEEEEIIIIDNOOOOOEOEUUUUEYTHSSAaCcEeLlNnSsZzZzdCcEeNnRrTtUuUuAaEeGgIiKkLlNnUuEeIiUuCcGgHhYyYySsAaTtDdRrGgISsUuZz"
      composition = Composition.create(:name => invalid_value)
      expect(composition.cwr_name).to eq(transliterated_value)
    end

    it "should stripped characters that does not transliterate" do
      invalid_value = "begin`‚ƒ„…†‡ˆ‰‹‘’“”•–—˜™š›¡¢¤¥¦§¨©ª«¬®¯°±²³´µ¶·¸¹º»¼½¾¿×|end"
      composition = Composition.create(:name => invalid_value)
      expect(composition.cwr_name).to eq("beginend")
    end

    it "should use original name when transliteration falied" do
      foreign_name = "Memories of Kuala Lumpour クアラルンプール?"
      composition = Composition.create(:name => foreign_name)
      expect(composition.cwr_name).to eq(foreign_name)
    end

  end

  describe "#has_splits?" do
    it "should return true when composition has passed the submitted state" do
      composition = TCFactory.build_composition
      expect(composition.has_splits?).to be_falsey
      composition.submit_split!
      expect(composition.has_splits?).to be_truthy
      composition.send_for_processing!
      expect(composition.has_splits?).to be_truthy
    end
  end

  describe "updating status and details" do
    before :each do
      create_albums_and_songs
      @album.update_attribute(:payment_applied, true)
      @album.reload.songs.each do |song|
        composition = TCFactory.build_composition(:song => song)
        composition.update_attribute(:state, 'pending_distribution')
      end
    end

    context "when an album is paid for" do
      it "should update pending distributions" do
        Composition.update_pending_submissions(@album)
        @album.reload.songs.collect(&:composition).each do |composition|
          expect(composition.state).to eq('split_submitted')
        end
      end

      it "should not update composition that's non-pending distributions" do
        composition = @album.reload.songs.first.composition
        composition.update_attribute(:state, 'accepted')
        Composition.update_pending_submissions(@album)
        expect(composition.state).not_to eq('split_submitted')
      end
    end

    context "when an album is finalized" do
      it "should update composition's name to reflect the final song name" do
        new_song_name = 'New Song Name'
        song = @album.songs.first
        @album.update_attribute(:finalized_at, Time.now)
        expect(song.name).not_to eq(new_song_name)
        song.update_attribute(:name, new_song_name)
        Composition.update_details_post_finalized(@album)
        expect(song.composition.reload.name).to eq(new_song_name)
      end
    end
  end

  def create_albums_and_songs
    person = TCFactory.build_person
    @composer = TCFactory.build_composer(:person => person)
    @album = TCFactory.build_album(:person => person, :payment_applied => true, :finalized_at => Time.now)
    @song = TCFactory.build_song(:album => @album)
    expect(@song.composition).to be_nil
    @songs = []
    @songs_and_splits_param = []

    (1..5).each do |i|
      @songs << TCFactory.build_song(:album => @album)
      @songs_and_splits_param << {:song_id => @songs.last.id, :percent => (i*10)}
    end
  end

  describe "#account" do
    it "returns composer's account if the composition has an associated composer" do
      composer    = create(:composer)
      composition = create(:composition)
      create(:publishing_split, composition: composition, composer: composer)
      expect(composition.reload.account).to eq composer.account
    end

    it "returns the composition's song's person's publishing administration account" do
      account       = create(:account)
      composition   = create(:composition)
      album         = create(:album, person: account.person)
      create(:song, album: album, composition_id: composition.id)
      expect(composition.reload.account).to eq account
    end
  end

  describe "#composer" do
    it "returns the composer associated to the composition's splits" do
      composer    = create(:composer)
      composition = create(:composition)
      create(:publishing_split, composition: composition, composer: composer)
      expect(composition.reload.composer).to eq composer
    end

    it "returns the composer associated to the composition's non_tunecore_songs" do
      composer    = create(:composer)
      composition = create(:composition)
      ntc_album   = create(:non_tunecore_album, composer: composer)
      create(:non_tunecore_song, non_tunecore_album: ntc_album, composition: composition)
      expect(composition.reload.composer).to eq composer
    end
  end

  describe "terminate!" do
    it "updates the compositions state to 'terminated'" do
      composition = create(:composition)
      expect(composition.state).not_to eq "terminated"
      composition.terminate!
      expect(composition.state).to eq "terminated"
    end

    it "creates a partially terminated composer" do
      composition = create(:composition, :with_non_tunecore_songs)
      composer    = composition.composer

      expect(composer.terminated_composer.nil?).to be true
      composition.terminate!
      expect(composer.reload.terminated_composer.present?).to be true
      expect(composer.reload.terminated_composer.partially_terminated?).to be true
    end
  end

  describe "#submitted!" do
    it "updates the compositions state to 'submitted'" do
      composition = create(:composition)

      expect{
        composition.submitted!
      }.to change {
        composition.state
      }.from("new").to("submitted")
    end
  end

  describe "#resubmitted!" do
    it "updates the compositions state to 'resubmitted'" do
      composition = create(:composition)

      expect{
        composition.resubmitted!
      }.to change {
        composition.state
      }.from("new").to("resubmitted")
    end
  end

  describe "#verified!" do
    context "when the composition is terminated" do
      it "updates the state to 'accepted'" do
        composition = create(:composition)
        composition.terminate!

        expect{
          composition.verified!
        }.to change {
          composition.state
        }.from("terminated").to("verified")
      end
    end
  end

  describe "#disqualify!" do
    context "when the composition is not ineligible" do
      it "updates the state to 'ineligible'" do
        composition = create(:composition)

        expect{
          composition.disqualify!
        }.to change {
          composition.state
        }.from("new").to("ineligible")
      end
    end

    context "when the composition is ineligible" do
      it "does not update the state" do
        composition = create(:composition)

        composition.disqualify!

        expect{
          composition.disqualify!
        }.to_not change {
          composition.state
        }
      end
    end
  end

  describe "#qualify!" do
    context "when the composition is ineligible" do
      it "updates the state to the previous state" do
        composition = create(:composition)

        composition.verified!
        composition.disqualify!
        composition.reload

        expect(composition.state).to eq("ineligible")
        expect(composition.prev_state).to eq("verified")

        expect{
          composition.qualify!
        }.to change {
          composition.state
        }.from("ineligible").to("verified")
      end
    end

    context "when the composition is not ineligible" do
      it "does not update the state" do
        composition = create(:composition)

        composition.verified!

        expect{
          composition.qualify!
        }.to_not change {
          composition.state
        }
      end
    end
  end

  describe "#composition_name" do
    it "returns the composition name if translated name is not present" do
      composition = build_stubbed(:composition, translated_name: "My Translated Name")
      expect(composition.composition_name).to eq "My Translated Name"
    end

    it "returns the translated name if present" do
      composition = build_stubbed(:composition)
      expect(composition.composition_name).to eq composition.name
    end
  end

  describe "#send_to_rights_app" do
    context "when the composition does not have a composer" do
      it "returns 'Composer Not Present'" do
        composition = create(:composition)
        expect(composition.send_to_rights_app).to eq "Composer Not Present"
      end
    end

    context "when the composition has a composer" do
      it "returns nil when the delivery to rights app is successful" do
        composition = create(:composition)
        composer = create(:composer)
        create(:publishing_split, composer: composer, composition: composition)

        allow(PublishingAdministration::WorkListService).to receive(:list)
        allow(PublishingAdministration::ApiClientServices::RecordingService).to receive(:post_recording)

        expect(composition.reload.send_to_rights_app).to be_nil
      end

      it "returns the error message if the delivery to rights app fails" do
        composition = create(:composition)
        composer = create(:composer)
        create(:publishing_split, composer: composer, composition: composition)
        msg =  "Error from POST: Work. Message: Writer not found, Url: https://uatapi.rightsapp.co.uk/api/v1/Works"

        allow(PublishingAdministration::WorkListService).to receive(:list).and_raise(PublishingAdministration::ErrorService::ApiError, msg)
        allow(PublishingAdministration::ApiClientServices::RecordingService).to receive(:post_recording)

        expect(composition.reload.send_to_rights_app).to eq msg
      end
    end
  end

  describe "#isrc" do
    it "should return isrc from an associated song or ntc song" do
      composition = create(:composition)
      song = build(:song)

      composition.songs << song

      expect(composition.isrc).to eq(song.tunecore_isrc)
    end

    it "should prioritize optional isrc's over tunecore isrcs" do
      composition = create(:composition)
      song = build(:song)
      song.update!(optional_isrc: "TCBBB1900122")

      composition.songs << song

      expect(composition.isrc).to eq(song.optional_isrc)
    end
  end

  describe "#share_submitted_date" do
    it "should return the latest updated_at on its associated publishing splits" do
      composition = create(:composition)
      split = create(:publishing_split, composition: composition)

      expect(composition.share_submitted_date.to_i).to eq(split.updated_at.to_i)
    end
  end
end
