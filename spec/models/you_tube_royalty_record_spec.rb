require "rails_helper"

describe YouTubeRoyaltyRecord do

  describe "factory" do
    it "should create a valid you tube royalty record" do
      ytr = FactoryBot.build(:you_tube_royalty_record)
      expect(ytr).to be_valid
    end

    it "should save to the database" do
      ytr = FactoryBot.create(:you_tube_royalty_record)
      expect(ytr.id).not_to be_nil
    end
  end

end
