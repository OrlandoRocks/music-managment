require "rails_helper"

describe BackgroundEffect do
  describe "#render" do
    let(:source)  { Rails.root.join("spec", "fixtures", "album_artwork.tiff") }
    let(:target) { "album_artwork_output.tiff" }

    after(:each) do
      File.delete(target) if File.exists?(target)
    end

    context "name is none" do 
      it "copies the file" do 
        background_effect = BackgroundEffect.new(name: "None")
        background_effect.render(source, target)
        expect(File.exists?(target)).to be true
      end 
    end 

    context "name is antique" do 
      it "converts and creates a composite of the file" do 
        background_effect = BackgroundEffect.new(name: "Antique")
        background_effect.render(source, target)
        expect(File.exists?(target)).to be true
      end 
    end 

    context "name is blur" do 
      it "raises error when there is no level" do 
        background_effect = BackgroundEffect.new(name: "Blur")
        expect{background_effect.render(source, target)}.to raise_error
      end 

      it "converts the file" do 
        background_effect = BackgroundEffect.new(name: "Blur", level: 1)
        background_effect.render(source, target)
        expect(File.exists?(target)).to be true 
      end 
    end 

    context "name is black_and_white" do
      it "raises error when there is no level" do 
        background_effect = BackgroundEffect.new(name: "Black_and_white")
        expect{background_effect.render(source, target)}.to raise_error
      end 

      it "converts the file" do 
         background_effect = BackgroundEffect.new(name: "Black_and_white", level: 1)
         background_effect.render(source, target)
         expect(File.exists?(target)).to be true
      end
    end

    context "name is halftone" do 
      it "converts the file" do 
        background_effect = BackgroundEffect.new(name: "Halftone")
        background_effect.render(source, target)
        expect(File.exists?(target)).to be true
      end 
    end 

    context "name is gray_halftone" do 
      it "converts the file" do 
        background_effect = BackgroundEffect.new(name: "Gray_halftone")
        background_effect.render(source, target)
        expect(File.exists?(target)).to be true
      end 
    end 

    context "name is obama" do 
      it "converts the file" do 
        background_effect = BackgroundEffect.new(name: "Obama")
        background_effect.render(source, target)
        expect(File.exists?(target)).to be true 
      end 
    end 

    context "name is glow" do 
      it "raises error when there is no level" do 
        background_effect = BackgroundEffect.new(name: "Glow")
        expect{background_effect.render(source, target)}.to raise_error
      end

      it "converts the file" do 
        background_effect = BackgroundEffect.new(name: "Glow", level: 1)
        background_effect.render(source, target)
        expect(File.exists?(target)).to be true
      end 
    end 

    context "name is video" do 
      it "raises error when there is no level" do 
        background_effect = BackgroundEffect.new(name: "Video")
        expect{background_effect.render(source, target)}.to raise_error
      end

      it "converts the file" do 
        background_effect = BackgroundEffect.new(name: "Video", level: 1)
        background_effect.render(source, target)
        expect(File.exists?(target)).to be true
      end
    end

    context "name is fail" do 
      it "converts the file" do 
        background_effect = BackgroundEffect.new(name: "Fail")
        background_effect.render(source, target)
        expect(File.exists?(target)).to be true
      end 
    end 
  end
end