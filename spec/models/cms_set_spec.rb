require "rails_helper"

describe CmsSet do
  before :each do
    CmsSet.all.each { |set| set.update_attribute(:start_tmsp, nil) }

    @frozen_time = Timecop.freeze(Time.local(2016, 1, 1))

    # had to randomize due to the MAX() function in
    # MySQL returning multiple results if time
    # is in same for a set of results
    states = {
      active:      Time.local(2015, 12, 31),
      old:         Time.local(2015, 12, 30),
      future:      Time.local(2016, 1, 2),
      unscheduled: nil,
    }

    { us: [1, 1], de: [5, 6] }.each do |country_code, country_website|
      ["login", "interstitial", "dashboard"].each do |callout_type|
        states.each do |callout_state, time_offset|
          time_offset += rand(1..1000) if time_offset
          instance_variable_set(
            "@#{callout_state}_#{country_code}_#{callout_type}_set",
            FactoryBot.create(
              "#{callout_type}_set".to_sym,
              :no_date_check,
              callout_name: "#{callout_state} #{country_code} #{callout_type}",
              country_website_id: country_website[0],
              country_website_language_id: country_website[1],
              start_tmsp: time_offset,
            )
          )
        end
      end
    end
  end

  after do
    Timecop.return
  end

  describe "#activate" do
    it "sets startp_tsmp to now" do
      @active_de_login_set.activate
      expect(@active_de_login_set.start_tmsp).to eq @frozen_time
    end
  end

  describe "#deactivate" do
    it "sets none set startp_tsmp to now" do
       FactoryBot.create(
        :login_set,
        callout_name: "None",
        country_website_id:5,
        country_website_language_id:6
        )
      @active_de_login_set.deactivate
      expect(@active_de_login_set.start_tmsp).to eq @frozen_time
    end
  end
end

describe CmsSet do
  before(:each) do
    @cms_set = FactoryBot.create(:interstitial_set, callout_name: "Test")
  end

  describe "#create_attributes" do
    context "should create a cms_set_attribute" do
      it "should set the attr_text for any cms_set_attribute being created with attr_name within CmsSetAttribute::TEXT_FIELD_ATTRIBUTE_NAMES" do
        @cms_set.create_attributes("markup"=>true)
        expect(@cms_set.cms_set_attributes.first.attr_text).to eq("t")
        expect(@cms_set.cms_set_attributes.first.attr_value).to eq(nil)
      end

      it "should call has_attached_file setter if the attr_name matches any CmsSetAttribute::UPLOADED_IMAGE_TYPES" do
        expect_any_instance_of(CmsSetAttribute).to receive(:background_img=).and_return(nil)
        @cms_set.create_attributes("background_img"=>true)
      end

      it "should set the attr_value for any other attr_names" do
        @cms_set.create_attributes("any_attr_name_not_in_constants"=>true)
        expect(@cms_set.cms_set_attributes.first.attr_text).to eq(nil)
        expect(@cms_set.cms_set_attributes.first.attr_value).to eq("1")
      end
    end
  end

  describe "#update_cms_set_attributes" do
    before :each do
      FactoryBot.create(:cms_set_attribute, attr_name: "button_1_url", attr_value: "google", cms_set: @cms_set)
      FactoryBot.create(:cms_set_attribute, attr_name: "background_img", attr_value: "farm.jpg", cms_set: @cms_set)
      FactoryBot.create(:cms_set_attribute, attr_name: "markup", attr_text: "hi guys", cms_set: @cms_set)
    end

    context "when updating existing attributes" do
      it "should set the attr_text for any cms_set_attribute being updated with attr_name within CmsSetAttribute::TEXT_FIELD_ATTRIBUTE_NAMES" do
        expect(@cms_set).not_to receive(:create_attributes)
        @cms_set.update_cms_set_attributes("markup"=>"hello girls")
        expect(@cms_set.cms_set_attributes.find_by(attr_name: "markup").attr_text).to eq("hello girls")
      end

      it "should call has_attached_file setter if the attr_name matches any CmsSetAttribute::UPLOADED_IMAGE_TYPES" do
        expect_any_instance_of(CmsSetAttribute).to receive(:background_img=).and_return(nil)
        expect(@cms_set).not_to receive(:create_attributes)
        @cms_set.update_cms_set_attributes("background_img"=>true)
      end

      it "should set the attr_value for any other attr_names" do
        expect(@cms_set).not_to receive(:create_attributes)
        @cms_set.update_cms_set_attributes("button_1_url" => "geronimo")
        expect(@cms_set.cms_set_attributes.find_by(attr_name: "button_1_url").attr_value).to eq("geronimo")
      end
    end

    it "should delete existing attributes if passed a blank value" do
      expect(@cms_set).not_to receive(:create_attributes)
      @cms_set.update_cms_set_attributes("button_1_url" => "")
      expect(@cms_set.cms_set_attributes.find_by(attr_name: "button_1_url")).to be_nil
    end

    it "should call #create_attributes to create new attributes" do
      expect(@cms_set).to receive(:create_attributes)
      @cms_set.update_cms_set_attributes("button_2_url" => "space")
    end
  end
end

describe CmsSet do
  describe "#as_json" do
    it "returns the expected json" do
      cms_set = create(:cms_set)
      create(:cms_set_attribute, cms_set: cms_set)

      result = cms_set.as_json

      expect(result).to eq expected_cms_set_json(cms_set)
    end

    def expected_cms_set_json(cms_set)
      {
        cms_set: {
          "id" => cms_set.id,
          "callout_name" => cms_set.callout_name,
          "callout_type" => cms_set.callout_type,
          "country_website_id" => cms_set.country_website_id,
          "country_website_language_id" => cms_set.country_website_language_id,
          "start_tmsp" => cms_set.start_tmsp,
          "created_at" => cms_set.created_at.as_json,
          "updated_at" => cms_set.updated_at.as_json,
          "bg_color" => nil
        }
      }
    end
  end
end
