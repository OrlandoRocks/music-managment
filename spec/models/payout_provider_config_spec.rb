require 'rails_helper'

describe PayoutProviderConfig, type: :model do
  let(:password) { "passwordio" }
  let(:ppc) { create(:payout_provider_config, password: password) }

  it 'should not store the raw password in the DB' do
    stored_ppc = PayoutProviderConfig.find(ppc.id)
    expect(stored_ppc.password).to be_nil
  end

  it 'should store the encrypted password on the cipher_datum object' do
    expect(ppc.decrypted_password).to eq(password)
  end

  it 'should add a cipher datum for the password decryption' do
    expect do
      create(:payout_provider_config, password: password)
    end.to change {
      CipherDatum.count
    }.by(1)
  end
end
