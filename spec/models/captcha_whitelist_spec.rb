require 'rails_helper'

describe CaptchaWhitelist do
  describe "validations" do
    let(:subject) { build(:captcha_whitelist) }

    it { should validate_presence_of(:ip) }
    it { should validate_presence_of(:description) }
    it { should validate_uniqueness_of(:ip).case_insensitive }
  end
end
