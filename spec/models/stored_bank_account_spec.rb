require "rails_helper"

describe StoredBankAccount  do
  it "should require a customer_vault_id" do
    stored_bank_account = StoredBankAccount.new(:customer_vault_id => nil, :person_id => 1)
    expect(stored_bank_account.valid?).to be_falsey
    expect(stored_bank_account.errors.messages[:customer_vault_id].size).to eq 1
  end

  it "should belong to a person" do
    stored_bank_account = StoredBankAccount.new(:customer_vault_id => 1, :person_id => nil)
    expect(stored_bank_account.valid?).to be_falsey
    expect(stored_bank_account.errors.messages[:person_id].size).to eq 1
  end

  it "should mark a previous stored bank account as deleted if you create a new one for the same person" do
    bvt = double('braintree_vault_txn')
    allow(BraintreeVaultTransaction).to receive(:log_deletion).and_return(bvt)

    stored_bank_account1 = create_bank_account_with_braintree_response(example_return_data)
    expect(stored_bank_account1.deleted_at).to be_nil
    stored_bank_account2 = create_bank_account_with_braintree_response(example_return_data)
    expect(stored_bank_account1.reload.deleted_at).not_to be_nil
    expect(stored_bank_account2.reload.deleted_at).to be_nil
  end

  context "Create a Stored Bank Account" do
    before(:each) do
      create_bank_account_with_braintree_response(example_return_data)
    end

    it "should Mark the bank account as deleted when the destroy method is called" do
      response = double
      allow(response).to receive(:success?).and_return(true)
      allow_any_instance_of(Braintree::Gateway).to receive_message_chain(:payment_method, :delete).and_return(response)
      expect(BraintreeVaultTransaction).to receive(:log_deletion)

      @stored_bank_account.destroy('127.0.0.1')
      expect(@stored_bank_account.deleted_at).not_to be_nil
      expect(@stored_bank_account.status).to eq('destroyed')
    end

    it "should set the status to 'destruction_error' when braintree sends an error response" do
      response = double
      payment_method = double
      allow(response).to receive(:success?).and_return(false)
      allow(response).to receive_message_chain(:message, :match).and_return(false)
      allow_any_instance_of(Braintree::Gateway).to receive_message_chain(:payment_method, :delete).and_return(response)
      allow_any_instance_of(Braintree::Gateway).to receive(:payment_method).and_return(payment_method)
      allow(payment_method).to receive(:delete).and_return(response)
      expect(BraintreeVaultTransaction).to receive(:log_deletion)

      @stored_bank_account.destroy('127.0.0.1')
      expect(@stored_bank_account.deleted_at).not_to be_nil
      expect(@stored_bank_account.status).to eq('destruction_error')
    end
  end
end


# generated the example_return_data by storing a bank account in Development, getting a customer_vault_id, and then running the following query in IRB>
# a = Tunecore::Braintree::Query.new
# a.execute({"customer_vault_id" => 1863315693, "report_type" => "customer_vault" })
# on 6/30/2010
def example_return_data
  {"customer_vault"=>{"customer"=>{"shipping_carrier"=>nil, "shipping_email"=>nil, "shipping_state"=>nil, "shipping_address_2"=>nil, "city"=>"Brooklyn", "company"=>nil, "processor_id"=>nil, "cc_number"=>nil, "shipping_company"=>nil, "customertaxid"=>nil, "postal_code"=>"11205", "customer_vault_id"=>"1863315693", "check_name"=>"Test User for check", "check_aba"=>"111000025", "shipping_date"=>nil, "shipping_last_name"=>nil, "check_account"=>"1xxx8956", "shipping_first_name"=>nil, "country"=>"US", "account_type"=>"checking", "account_holder_type"=>"personal", "check_hash"=>"5e09b02146a1f301d46bdb65cdb797c7", "shipping_country"=>nil, "website"=>nil, "id"=>"1863315693", "merchant_defined_field"=>"Wachovia", "cc_hash"=>nil, "tracking_number"=>nil, "shipping_city"=>nil, "cell_phone"=>nil, "address_1"=>"1234 Street address", "shipping"=>nil, "fax"=>nil, "phone"=>"(973) 123-4567", "address_2"=>"Suite 822", "cc_bin"=>nil, "cc_exp"=>nil, "first_name"=>nil, "sec_code"=>"CCD", "shipping_postal_code"=>nil, "last_name"=>nil, "shipping_address_1"=>nil, "email"=>nil, "state"=>"NY"}}}
end

def create_bank_account_with_braintree_response(braintree_response)
  @person ||= TCFactory.create(:person)
  @stored_bank_account = StoredBankAccount.create(:customer_vault_id => '1863315693', :person=>@person)
  @stored_bank_account
end
