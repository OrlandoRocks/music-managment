require "rails_helper"

describe Country do

  let(:sanctioned) { Country.unscoped.sanctioned_iso_codes }

  context ".country_name" do
    it "returns country name when passed a country code" do
      expect(Country.country_name("US")).to eq("United States")
    end
  end

  describe ".available_countries" do
    it "does not include sanctioned countries" do
      expect(Country.available_countries).not_to include(sanctioned)
    end
  end

  describe ".country_name" do
    it "does not include sanctioned countries" do
      sanctioned_names = sanctioned.map do |code|
        Country.country_name(code)
      end

      expect(sanctioned_names.uniq!).to eq([nil])
    end

    it "returns countries for the supplied legacy iso_codes" do
      expect(Country.country_name("UK")).to eq("United Kingdom")
      expect(Country.country_name("KV")).to eq("Kosovo")
    end
  end

  describe "by_album_country" do
    it "does not include sanctioned countries" do
      album = create(:album, :live, :with_salepoints)
      album.album_countries.create(country_id: Country.unscoped.sanctioned.first.id)

      expect(Country.unscoped.by_album_country(album.album_countries).present?).to be false
    end
  end

  describe "sanctioned_iso_codes" do
    it "should return an array of ISO Codes for sanctioned countries" do
      iso_list = Country.sanctioned_iso_codes
      sanctioned = Country.unscoped.where(is_sanctioned: true)

      expect(iso_list.size).to eq(sanctioned.count)
      expect(iso_list).to include(*["CU", "IR", "KP", "SD", "SY"])
    end
  end

  describe "default_scope" do
    it "should not include sanctioned countries" do
      all_countries = Country.all
      sanctioned_countries = Country.unscoped.where(is_sanctioned: true)

      expect(all_countries).to_not include(*sanctioned_countries)
    end

    it "should include unsanctioned countries" do
      all_countries = Country.all
      unsanctioned_countries = Country.unscoped.where(is_sanctioned: false)

      expect(all_countries).to include(*unsanctioned_countries)
    end
  end

  describe "#search_by_iso_code" do
    context "alternative country code is passed" do
      it "returns the correct country record" do
        uk = Country.find_by(iso_code: "GB")

        expect(Country.search_by_iso_code("UK")).to eq(uk)
      end
    end

    it "returns correct country from #find_by_iso_code" do
      canada = Country.find_by(iso_code: "CA")

      expect(Country.search_by_iso_code("CA")).to eq(canada)
    end
  end

  context "mainland china" do
    it "is the name of iso code 'CN'" do
      cn = Country.find_by(iso_code: "CN")

      expect(cn[:name]).to eq("Mainland China")
    end
  end
end
