require 'rails_helper'

RSpec.describe OutboundRefund, type: :model do
  context "Associations" do
    it { should belong_to(:refund) }
    it { should belong_to(:outbound_invoice) }
    it { should belong_to(:vat_tax_adjustment) }
    it { should belong_to(:refund_settlement) }
  end

  context "Validations" do
    it "should be invalid if refund or outbound_invoice is missing" do
      invoice = OutboundRefund.new
      invoice.valid?

      expect(invoice.errors.keys).to eq %i[refund outbound_invoice refund_settlement]
    end
  end
end
