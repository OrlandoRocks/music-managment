require "rails_helper"

describe StoreDeliveryConfig do
  before do
    @store_delivery_config = Store.first.store_delivery_config
    @store_delivery_config.update(
      delivery_queue: "delivery-default",
      paused: false
    )
  end

  describe "queue" do
    it "returns the delivery queue that it is set to" do
      expect(@store_delivery_config.queue).to eq("delivery-default")
    end

    it "returns the paused queue when paused" do
      @store_delivery_config.update_attribute(:paused, true)
      expect(@store_delivery_config.queue).to eq("delivery-pause")
    end
  end

  describe 'pause!' do
    it 'pause a store' do
      store = create(:store)
      store_delivery_config = create(:store_delivery_config, store: store, paused: false)
      expect(DistributorAPI::Store).to receive(:pause).with(store.id)

      store_delivery_config.pause!
      store_delivery_config.reload

      expect(store_delivery_config.paused).to be_truthy
    end
  end

  describe 'unpause!' do
    it 'unpause a store' do
      store = create(:store)
      store_delivery_config = create(:store_delivery_config, store: store, paused: true)
      expect(DistributorAPI::Store).to receive(:unpause).with(store.id)

      store_delivery_config.unpause!
      store_delivery_config.reload

      expect(store_delivery_config.paused).to be_falsy
    end
  end
end
