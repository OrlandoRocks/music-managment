require "rails_helper"

describe Purchase, "when validating" do

  fixtures :albums

  before :each do
    @person = TCFactory.build_person
    @product = mock_model(
      Product,
      :id => 6,
      :price => 9.99,
      :product_type => 'Ad Hoc',
      :country_website_id => 1,
      name: "Foo Bar",
      marketing_event_triggers: MarketingEventTrigger.all
    )
    @album   = mock_model(Album, :id => 888, :person_id => @person.id)
    expect { Purchase.new(:related => @album, :product => @product).valid? }.to raise_error(NoMethodError)
  end

  it "should be invalid when creating a purchase without a product" do
    expect(Purchase.new(:related => @album, :person => @person).valid?).to eq(false)
  end

  it "should be invalid when creating a purchase without a related item" do
    person = TCFactory.build_person
    expect(Purchase.new(:product => @product, :person => @person).valid?).to eq(false)
  end

  it 'should test that a purchase with a person, product, and related_item can be created' do
    person = TCFactory.build_person
    album = mock_model(Album, :id => 888, :person_id => @person.id)
    expect { FactoryBot.create(:purchase, :person => @person, :related => album, :product => @product)}.to change{Purchase.count}.by(1)
  end

  it 'should test that a valid purchase with a person, product, and related_item will fail if cost_cents is nil' do
    person = TCFactory.build_person
    album = mock_model(Album, :id => 888, :person_id => @person.id)
    purchase = FactoryBot.build(:purchase, :person => @person, :related => album, :product => @product)
    purchase.cost_cents = nil
    expect(purchase.save).to eq(false)
    expect(purchase.errors.values.flatten.include?("purchases.errors.issue_adding_to_cart")).to eq(true)
  end

  it 'should have a validation error if the person_id of the related and purchase do not match' do
    person = TCFactory.build_person
    album = mock_model(Album, :id => 888, :person_id => 555)
    purchase = Purchase.create(:person => @person, :related => album)
    expect(purchase.errors[:base].first.include?("the person_id for the related and the purchase must be the same")).to eq(true)
  end

  it 'should have a validation error if the person_id of the related and purchase do not match for a preorder purchase' do
    person = TCFactory.build_person
    album = TCFactory.create(:album)
    salepoint = TCFactory.create(:salepoint, :salepointable_type => album.class.name, :salepointable_id => album.id)
    salepoint_preorder_data = SalepointPreorderData.create(salepoint: salepoint)
    purchase = Purchase.create(:person => @person, :related => album)
    expect(purchase.errors[:base].first.include?("the person_id for the related and the purchase must be the same")).to eq(true)
  end

  it 'should not allow duplicate purchases to be created for albums' do
    person = TCFactory.build_person
    album = mock_model(Album, :id => 888, :person_id => @person.id)
    #person = mock_model(Person, :id => 999)
    expect { Purchase.create(:person => @person, :related => album, :product => @product, :cost_cents => 0)}.to change{Purchase.count}.by(1)

    failed_purchase = Purchase.create(:person => @person, :related => album, :product => @product, :cost_cents => 0)
    expect(failed_purchase.errors[:related_id].first).to eq('has already been taken')
  end

  it 'should allow duplicate purchases to be created for products' do
    person = TCFactory.build_person
    expect { FactoryBot.create(:purchase, :person => @person, :related => @product, :product => @product)}.to change{Purchase.count}.by(1)
    expect { FactoryBot.create(:purchase, :person => @person, :related => @product, :product => @product)}.to change{Purchase.count}.by(1)
  end

  it 'should NOT allow a second purchase to be made if the related item is not a product' do
    person = TCFactory.build_person
    album = mock_model(Album, :id => 888, :person_id => @person.id)
    FactoryBot.create(:purchase, :person => @person, :related => album, :paid_at => Time.now, :product => @product)
    expect(Purchase.new(:person => @person, :related => album, :product => @product).valid?).to eq(false)
  end

  it 'should test that a created purchase has a USD currency if the person a US customer' do
    person = TCFactory.build_person
    album = mock_model(Album, :id => 888, :person_id => @person.id)
    expect(FactoryBot.create(:purchase, :person => @person, :related => album, :product => @product).currency).to eq("USD")
  end

  it "should allow product with the same country_website_id as the person" do
    person = TCFactory.build_person(:country => "CA", :postal_code=>'E4Y2E4' )
    product = mock_model(
      Product,
      :id => 6,
      :price => 9.99,
      :product_type => 'Ad Hoc',
      :country_website_id => 2,
      name: "Foo Bar",
      marketing_event_triggers: MarketingEventTrigger.all
    )

    expect(FactoryBot.create(:purchase, :person => person, :related => product, :product => product).valid?).to eq(true)
  end

  it "should not allow product with different country_website_id as the person" do
    person = TCFactory.build_person
    product = mock_model(
      Product,
      :id => 6,
      :price => 9.99,
      :product_type => 'Ad Hoc',
      :country_website_id => 2,
      marketing_event_triggers: MarketingEventTrigger.all
    )

    expect { FactoryBot.create(:purchase, :person => person, :related => product, :product => product) }.to raise_error(ActiveRecord::RecordInvalid)
  end

  #TODO: GC - uncomment this when we check for the presence of cost_cents
  # it "should raise error when cost_cents is nil" do
  #   purchase = Purchase.create!(:person => @person, :related => @product, :product => @product, :cost_cents => nil)
  #   purchase.should have(1).error_on(:cost_cents)
  # end
end

describe Purchase, "scopes" do
  context "paid / unpaid named scope" do
    it "should return a Purchase with a paid status" do
      paid_purchase = create(:purchase, paid_at: Time.current)
      unpaid_purchase = create(:purchase, related_id: 2, paid_at: nil)

      result = Purchase.paid

      expect(result).to include(paid_purchase)
      expect(result).to_not include(unpaid_purchase)
    end

    it "should return a Purchase with an unpaid status" do
      unpaid_purchase = create(:purchase, paid_at: nil)
      paid_purchase = create(:purchase, related_id: 2, paid_at: Time.current)

      result = Purchase.unpaid

      expect(result).to include(unpaid_purchase)
      expect(result).to_not include(paid_purchase)

    end
  end

  context "not_in_invoice named scope" do
    it "should return a Purchase without an invoice" do
      invoice = mock_model(Invoice, :id => 20, :batch_status => "waiting_for_batch")
      purchase_with_invoice = create(:purchase, invoice_id: invoice.id)
      purchase_without_invoice = create(:purchase, related_id: 2, invoice_id: nil)

      result = Purchase.not_in_invoice

      expect(result).to include(purchase_without_invoice)
      expect(result).to_not include(purchase_with_invoice)
    end
  end

  context "deleted_albums named scope" do
    it "should return an unpaid purchase with deleted albums" do
      person = create(:person)
      deleted_album = create(:album, :is_deleted, person: person)
      deleted_album2 = create(:album, :is_deleted, person: person)
      standard_album = create(:album, person: person)

      unpaid_purchase_with_deleted_albums = create(:purchase,
        person: person,
        related_id: deleted_album.id,
        related_type: "Album",
        paid_at: nil,
      )

      paid_purchase_with_deleted_albums = create(:purchase,
        person: person,
        related_id: deleted_album2.id,
        related_type: "Album",
      )
      unpaid_purchase_with_albums = create(:purchase,
        person: person,
        related_id: standard_album.id,
        related_type: "Album",
        paid_at: nil,
      )

      result = Purchase.deleted_albums

      expect(result).to include(unpaid_purchase_with_deleted_albums)
      expect(result).to_not include(paid_purchase_with_deleted_albums)
      expect(result).to_not include(unpaid_purchase_with_albums)
    end
  end
end

describe Purchase, "when calling recalculate_or_create" do
  before(:each) do
    @product = TCFactory.build_ad_hoc_album_product
    @album = Album.first
    @person = @album.person
  end

  it "should create a new purchase for an album if one does not exist" do
    expect { Purchase.recalculate_or_create(@person, @product, @album) }.to change{Purchase.count}.by(1)
  end

  it "should return the existing purchase for an album" do
    existing_purchase = Purchase.recalculate_or_create(@person, @product, @album)
    expect(Purchase.recalculate_or_create(@person, @product, @album)).to eq(existing_purchase)
  end
end

describe Purchase, "when calculating price with a targeted offer discount" do

  before(:each) do
    @product = TCFactory.build_ad_hoc_album_product(29.99)
    @album = Album.first
    @person = @album.person
  end

  it "should round properly" do
    @targeted_offer = TCFactory.create(:targeted_offer,:status=>"Inactive")
    @targeted_offer.targeted_products.create(:product=>@product, :price_adjustment=>50)
    @targeted_offer.update(:status=>"Active")
    @targeted_offer.targeted_people.create(:person=>@person)
    @purchase = Product.add_to_cart(@person, @album, @product)
    expect(@purchase.cost_cents - @purchase.discount_cents).to eq(1500)
  end

  it "should use the targeted product for a distribution credit when purchase loaded from db" do

    @product        = Product.find(Product::US_RINGTONE_PRODUCT_IDS.first)
    @product_item   = TCFactory.create(:product_item, :price=>0, :does_not_renew=>0, :renewal_type=>"distribution", :product_id=>@product.id)

    @targeted_offer = TCFactory.create(:targeted_offer, :status=>"Inactive")
    @tp = @targeted_offer.targeted_products.create(:product=>@product, :price_adjustment=>50)
    @targeted_offer.update(:status=>"Active")
    @targeted_offer.targeted_people.create(:person=>@person)

    @purchase = Product.add_to_cart(@person, @product)
    expect(@purchase.targeted_product_id).to eq(@tp.id)

    @purchase = Purchase.find(@purchase.id)
    expect(@purchase.product_id).to   eq(@product.id)
    expect(@purchase.related_id).to   eq(@product.id)
    expect(@purchase.related_type).to eq("Product")

    @purchase.product.set_targeted_product(nil)
    @purchase.related.set_targeted_product(nil)

    @purchase.recalculate
    @purchase = @purchase.reload
    expect(@purchase.targeted_product_id).to eq(@tp.id)
  end

end

describe Purchase, "when adding purchases to a cart" do
  before(:each) do
    @person = TCFactory.build_person(:name => "billy", :email => 'bill@protocool.net')
    @product = mock_model(
      Product,
      :id => 7,
      :price => 9.99,
      :product_type => "Ad Hoc",
      :country_website_id => 1,
      :name => "Foo Bar",
      marketing_event_triggers: MarketingEventTrigger.all
    )
    @album = mock_model(Album, :id => 888, :person_id => @person.id)
    @album2 = mock_model(Album, :id => 777, :person_id => @person.id)
  end

  it "should return the number of items in the cart" do
    FactoryBot.create(:purchase, :person => @person, :related => @album, :product => @product, paid_at: nil)
    FactoryBot.create(:purchase, :person => @person, :related => @album2, :product => @product, paid_at: nil)
    expect(@person.cart_items_count).to eql(2)
  end

  it "should only count items with no invoice" do
    @album3 = mock_model(Album, :id => 666, :person_id => @person.id)
    invoice = mock_model(Invoice, :id => 20, :batch_status => "waiting_for_batch")
    FactoryBot.create(:purchase, :person => @person, :related => @album2, :product => @product, :invoice_id => invoice.id, paid_at: nil)
    FactoryBot.create(:purchase, :person => @person, :related => @album3, :product => @product, paid_at: nil)
    expect(@person.cart_items_count).to eql(1)
  end

  it "should count items with an invoice with batch_status of 'visible to customer'" do
    invoice = TCFactory.create(:invoice, {:batch_status => "visible_to_customer", :person => @person})
    FactoryBot.create(:purchase, :person => @person, :related => @album, :product => @product, :invoice_id => invoice.id, paid_at: nil)
    expect(@person.cart_items_count).to eql(1)
  end

  it "should not count purchases with an invoice that is partially paid" do
    invoice = TCFactory.create(:invoice, :person => @person)
    FactoryBot.create(:purchase, :person => @person, :related => @album, :product => @product, :invoice_id => invoice.id)
    invoice_settlement = TCFactory.create(:invoice_settlement, {:invoice => invoice})
    expect(@person.cart_items_count).to eql(0)
  end

  it "should allow products of the same country website" do
    person = people(:tunecore_admin)
    product = Product.new
    product.country_website_id = 1

    purchase = Purchase.new
    purchase.person  = person
    purchase.product = product

    assert !purchase.valid?
    assert !purchase.errors.full_messages.include?(I18n.t("models.billing.the_product_you_tried_to_purchase"))
  end

  it "should not allow products from different country websites" do
    person = people(:tunecore_admin)
    product = Product.new
    product.country_website_id = 2

    purchase = Purchase.new
    purchase.person  = person
    purchase.product = product

    assert !purchase.valid?
    assert purchase.errors.full_messages.include?(I18n.t("models.billing.the_product_you_tried_to_purchase"))
  end

end

describe Purchase, "when updating a price on an existing purchase" do
  before(:each) do
    @person = TCFactory.build_person(:name => "billy", :email => 'bill@protocool.net')
    @product = mock_model(
      Product,
      :id => 7,
      :price => 9.99,
      :product_type => "Ad Hoc",
      :applies_to_product => "Single",
      :name => "Test",
      :country_website_id => 1,
      marketing_event_triggers: MarketingEventTrigger.all
    )
    @album = mock_model(Album, :id => 888, :person_id => @person.id, :person => @person, :name => "Test Name")
  end

  it "should add an error and disallow price update when note isn't supplied" do
    purchase = FactoryBot.create(:purchase, :person => @person, :related => @album, :product => @product, :cost_cents => 999)
    purchase.adjust_price(899, nil, @person)
    expect(purchase.cost_cents).to eq(999)
    expect(purchase.errors["base"].include?("Admin note is required.")).to eq(true)
  end

  it "should add an error and disallow price update when a new price isn't supplied" do
    purchase = FactoryBot.create(:purchase, :person => @person, :related => @album, :product => @product, :cost_cents => 999)
    purchase.adjust_price(nil, "Test", @person)
    expect(purchase.cost_cents).to eq(999)
    expect(purchase.errors["base"]).to include("Please enter a valid price (0 - 9999.00).")
  end

  it "should not update the price if previously paid" do
    purchase = FactoryBot.create(:purchase, :person => @person, :related => @album, :product => @product, :cost_cents => 999, :paid_at => Time.now)
    purchase.adjust_price(899, "Test", @person)
    expect(purchase.cost_cents).to eq(999)
    expect(purchase.errors["base"].include?(I18n.t("models.billing.you_cannot_adjust_the_price"))).to eq(true)
  end

  it "should adjust the cost_cents and discount_cents" do
    purchase = FactoryBot.create(:purchase, :person => @person, :related => @album, :product => @product, :cost_cents => 999, :discount_cents => 0, paid_at: nil)
    purchase.adjust_price(899, "Test", @person)
    expect(purchase.cost_cents).to eq(899)
    expect(purchase.discount_cents).to eq(0)
  end

  it "should add a note on a successful adjustment" do
    purchase = FactoryBot.create(:purchase, :person => @person, :related => @album, :product => @product, :cost_cents => 999, :discount_cents => 0, paid_at: nil)
    expect {purchase.adjust_price(899, "Test", @person)}.to change(@person.notes, :count).by(1)
  end

  it "should create a Price Adjustment History and allow access to the history" do
    purchase = FactoryBot.create(:purchase, :person => @person, :related => @album, :product => @product, :cost_cents => 999, :discount_cents => 0, paid_at: nil)
    expect {purchase.adjust_price(899, "Test", @person)}.to change(PriceAdjustmentHistory, :count).by(1)

    expect(purchase.price_adjustment_histories.count).to eq(1)
    expect(purchase.price_adjustment_histories.reload).not_to be_empty
  end

  it "should order Price Adjustment Histories by created_at" do

    purchase = FactoryBot.create(:purchase, :person => @person, :related => @album, :product => @product, :cost_cents => 999, :discount_cents => 0)
    purchase.adjust_price(899, "Test", @person)

    sleep(1)

    purchase.adjust_price(999, "Test", @person)

    date = Time.now
    purchase.price_adjustment_histories.each do  |history|
      expect(history.created_at).to be <= date
      date = history.created_at
    end
  end

  context "Canadian customer scenarios" do
    it 'should test that a created purchase has a CAD currency' do
      person = TCFactory.build_person(:country => "CA", :postal_code=>'E4Y2E4')
      album = mock_model(Album, :id => 888, :person_id => person.id)
      product = mock_model(
        Product,
        :id => 6,
        :price => 9.99,
        :product_type => "Ad Hoc",
        :country_website_id => 2,
        name: "Foo Bar",
        marketing_event_triggers: MarketingEventTrigger.all
      )
      expect(FactoryBot.create(:purchase, :person => person, :related => album, :product => product).currency).to eq("CAD")
    end
  end
end

describe Purchase, "when destroying a purchase" do

  before(:each) do
    @person   = TCFactory.create(:person)
    @purchase = TCFactory.create(:purchase, :person=>@person, :product=>Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID), :related=>TCFactory.create(:album, :person=>@person))
  end

  it "should clean up price adjustment histories for the purchase" do
    @admin = TCFactory.create(:person)

    @purchase.adjust_price(499, "You suck customer!", @admin)

    purchase_id = @purchase.id
    expect(@purchase.price_adjustment_histories.reload).not_to be_empty

    @purchase.destroy

    expect(PriceAdjustmentHistory.where("purchase_id = :purchase_id", {:purchase_id=>purchase_id})).to be_empty
  end

  it "should destroy a related SoundoutProduct" do
    soundout_product = TCFactory.build(:soundout_report)
    purchase = TCFactory.build(:purchase)
    allow(purchase).to receive(:related).and_return(soundout_product)

    expect(soundout_product).to receive(:destroy)
    purchase.destroy
  end

  it "should attempt to call remove_from_cart_pre_proc for related" do
    related = @purchase.related
    expect(related).to receive(:remove_from_cart_pre_proc)
    @purchase.destroy
  end
end

describe Purchase do

  before(:each) do
    @purchases = []

    purchase1 = double("purchase")
    product   = double("product")
    allow(product).to receive(:applies_to_product).and_return("Ringtone")
    allow(purchase1).to receive(:product).and_return(product)
    @purchases << purchase1

    purchase2 = double("purchase")
    product   = double("product")
    allow(product).to receive(:applies_to_product).and_return("Album")
    allow(purchase2).to receive(:product).and_return(product)
    @purchases << purchase2

    purchase3 = double("purchase")
    product   = double("product")
    allow(product).to receive(:applies_to_product).and_return("Single")
    allow(purchase3).to receive(:product).and_return(product)
    @purchases << purchase3

    purchase4 = double("purchase")
    product   = double("product")
    allow(product).to receive(:applies_to_product).and_return("Album")
    allow(purchase4).to receive(:product).and_return(product)
    @purchases << purchase4

    purchase5 = double("purchase")
    product   = double("product")
    allow(product).to receive(:applies_to_product).and_return("Single")
    allow(purchase5).to receive(:product).and_return(product)
    @purchases << purchase5

    purchase6 = double("purchase")
    product   = double("product")
    allow(product).to receive(:applies_to_product).and_return("Ringtone")
    allow(purchase6).to receive(:product).and_return(product)
    @purchases << purchase6

  end

  it "should prioritize purchases correctly" do

    @sorted = Purchase.prioritize_purchases(@purchases)

    expected       = 0
    expected_order = ["Album", "Single", "Ringtone"]
    @sorted.each do |purchase|
      expect(purchase.product.applies_to_product == expected_order[expected] || ((expected < expected_order.count - 1) && purchase.product.applies_to_product == expected_order[expected+1])).to eq(true)
      expected += 1 if purchase.product.applies_to_product != expected_order[expected]
    end

  end

end

describe Purchase do

  describe "#mark_targeted_offers_as_used" do

    it "should increment the usage_count for all targeted_people associated with an invoice" do
      product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)

      targeted_offer = create(:targeted_offer, status: "Inactive")
      create(:targeted_product, targeted_offer_id: targeted_offer.id, product_id: product.id)

      person1 = create(:person)
      person2 = create(:person)
      tp_should_be_used     = create(:targeted_person, person: person1, targeted_offer: targeted_offer)
      tp_should_not_be_used = create(:targeted_person, person: person2, targeted_offer: targeted_offer)

      person1_album = create(:album, person: person1)
      invoice1 = create(:invoice, person_id: person1.id)
      create(:purchase, related: person1_album, person: person1, invoice: invoice1, product_id: product.id, paid_at: nil)

      person2_album = create(:album, person: person2)
      invoice2 = create(:invoice, person_id: person2.id)
      create(:purchase, related: person2_album, person: person2, invoice: invoice2, product_id: product.id, paid_at: nil)

      targeted_offer.update_attribute(:status, "Active")

      Purchase.mark_targeted_offers_as_used(invoice1)

      expect(tp_should_be_used.reload.usage_count).to eq 1
      expect(tp_should_not_be_used.reload.usage_count).to eq 0
    end
  end
end

describe Purchase do

  describe "#process_purchases_for_invoice" do
    before(:each) do
      Timecop.freeze
      @person = TCFactory.create(:person)
      @product = TCFactory.build_ad_hoc_salepoint_subscription_product(5.00)
      @album = TCFactory.create(:album, :person => @person, :finalized_at => Date.today)

      @sp_sub = TCFactory.create(:salepoint_subscription, :album => @album)

      @invoice = TCFactory.create(:invoice, :person_id => @person.id)
      @purchase = TCFactory.create(:purchase, :related => @sp_sub, :person => @person, :invoice => @invoice,
                                   :product_id => @product.id)

    end

    after(:each) do
      Timecop.return
    end

    it "should call paid_post_proc" do
      Purchase.process_purchases_for_invoice(@invoice)

      @sp_sub.reload
      @purchase.reload

      expect(@purchase.paid_at.blank?).to eq(false)
      expect(@sp_sub.effective.blank?).to eq(false)

      expect(@purchase.paid_at).to eq(@sp_sub.effective)
    end
    it "should create an invoice log" do
      initial_invoice_log_count = InvoiceLog.count

      Purchase.process_purchases_for_invoice(@invoice)

      expect(initial_invoice_log_count + 1).to eq(InvoiceLog.count)
    end
  end
end

describe Purchase do
  before(:each) do
    @person = TCFactory.create(:person)
    @album = TCFactory.create(:album, :person => @person)
  end

  describe "#currently_in_cart?" do
    it "should return true if item currently in cart" do
      product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
      purchase = TCFactory.create(:purchase, :product => product, :person => @person, :related => @album, :invoice => nil)

      expect(Purchase.currently_in_cart?(@person, @album)).to eq(true)
    end

    it "should return false if no purchase record exists for item" do
      expect(Purchase.currently_in_cart?(@person, @album)).to eq(false)
    end

    it "should return false if purchase exists but is in invoice" do
      product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
      purchase = TCFactory.create(:purchase, :product => product, :person => @person, :related => @album, :invoice => TCFactory.create(:invoice))

      expect(Purchase.currently_in_cart?(@person, @album)).to eq(false)
    end

    it "should return false if purcahse is finalized" do
      product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
      purchase = TCFactory.create(:purchase, :product => product, :person => @person, :related => @album, :paid_at => Time.now())

      expect(Purchase.currently_in_cart?(@person, @album)).to eq(false)
    end

    it "should return true if credit item exists for item and is in cart" do
      allow(CreditUsage).to receive(:credit_available_for?).and_return(true)
      credit = TCFactory.create(:credit_usage, :person => @person, :related => @album)

      product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
      purchase = TCFactory.create(:purchase, :product => product, :person => @person, :related => credit, :invoice => nil)

      expect(Purchase.currently_in_cart?(@person, @album)).to eq(true)
    end
  end

  describe "::releases_in_cart" do
    let!(:person) {create(:person)}
    let!(:cart_album) { create(:album, :with_salepoints, person: person) }
    let(:plan_credit_usage) { create(:plan_credit_usage, related: cart_album, person: person) }
    let!(:paid_album) { create(:album, :approved, :social_only, person: person) }
    let!(:uncarted_album) { create(:album, person: person ) }
    subject {Purchase.releases_in_cart(person)}
    before(:each) do
      store = Store.find_by(abbrev: "sc")
      uncarted_album.salepoints = [create(:salepoint, salepointable: uncarted_album, store: store)]
      create(:purchase, :unpaid, related: plan_credit_usage, person: person)
    end
    it "returns cart_album, ignores paid and uncarted albums" do
      result_id = subject.map(&:id)
      expect(result_id).to eq([cart_album.id])
      expect(result_id).not_to eq([paid_album])
      expect(result_id).not_to eq([uncarted_album])
    end
  end

end

describe Purchase do

  before(:each) do
    @person = TCFactory.create(:person)
  end

  describe "self.create_batch_purchases" do
    it "should return empty array if no items supplied" do
      expect(Purchase.create_batch_purchases(@person, []).blank?).to eq(true)
    end

    it "should create purchase record for supplied array" do
      TCFactory.build_ad_hoc_album_product(29.99)
      album1 = TCFactory.create(:album, :person => @person)
      album2 = TCFactory.create(:album, :person => @person)
      ids = []

      expect {
        ids = Purchase.create_batch_purchases(@person, [album1, album2])
      }.to change(Purchase, :count).by(2)

      records = Purchase.limit(2).order("id desc")

      records.each do |record|
        expect(record.cost_cents).to eq(2999)
        expect(record.discount_cents).to eq(0)
      end

      [album1, album2].each do |album|
        expect(records.any? {|r| r.related_type == album.class.name && r.related_id == album.id}).to eq(true)
        expect(ids.any? {|i| i == album.id}).to eq(true)
      end
    end

  end
end


describe Purchase do
  describe ".destroy_by_purchase_item" do
    it "should remove item from cart" do
      person = create(:person)
      album = create(:album, person: person)
      product = create(:product)
      _purchase = create(:purchase, product: product, person: person, related: album, invoice: nil, paid_at: nil)

      expect {
        Purchase.destroy_by_purchase_item(person, album)
      }.to change(Purchase, :count).by(-1)
    end

    it "should remove item from cart if added with credit" do
      person = create(:person)
      album = create(:album, person: person)
      product = create(:product)
      allow(CreditUsage).to receive(:credit_available_for?).and_return(true)
      credit_usage = create(:credit_usage, person: person, related: album, applies_to_type: 'Album')
      _purchase = create(:purchase, product: product, person: person, related: credit_usage, invoice: nil, paid_at: nil)

      expect {
          Purchase.destroy_by_purchase_item(person, album)
        }.to change(CreditUsage, :count).by(-1)
        .and change(Purchase, :count).by(-1)
    end

    it "should remove purchase for album and purchase for credit usage item from cart if both exist" do
      person = create(:person)
      album = create(:album, person: person)
      product = create(:product)
      allow(CreditUsage).to receive(:credit_available_for?).and_return(true)
      credit_usage = create(:credit_usage, person: person, related: album, applies_to_type: 'Album')
      credit_purchase = create(:purchase, product: product, person: person, related: credit_usage, invoice: nil, paid_at: nil)
      album_purchase = create(:purchase, product: product, person: person, related: album, invoice: nil, paid_at: nil)

      expect {
        Purchase.destroy_by_purchase_item(person, album)
      }.to change(CreditUsage, :count).by(-1)
      .and change(Purchase, :count).by(-2)
    end
  end
end

describe Purchase do

  before(:each) do
    @person = TCFactory.create(:person)
    allow(CreditUsage).to receive(:credit_available_for?).and_return(true)

    product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)

    album = TCFactory.create(:album, :person => @person)
    salepoint = TCFactory.create(:salepoint, :salepointable => album)
    credit_usage = TCFactory.create(:credit_usage, :related => album, :person => @person)

    @p1 = TCFactory.create(:purchase, :person => @person, :related => salepoint, :product => product)
    @p2 = TCFactory.create(:purchase, :person => @person, :related => album, :product => product)
    @p3 = TCFactory.create(:purchase, :person => @person, :related => credit_usage, :product => product)

    #@p1 = TCFactory.create(:purchase, :person => @person, :related => 0, :related_type => 'Salepoint')
    #@p2 = TCFactory.create(:purchase, :person => @person, :related => 0, :related_type => 'Album')
    #@p3 = TCFactory.create(:purchase, :person => @person, :related => 0, :related_type => 'CreditUsage')
  end

  describe "self.related_type_filter" do
    it "should properly filter purchases" do
      purch = @person.purchases.related_type_filter('Salepoint')
      expect(purch.size).to eq(1)
      expect(purch.first).to eq(@p1)

      purch = @person.purchases.related_type_filter(['Album', 'CreditUsage'])
      expect(purch.size).to eq(2)
      expect(purch.include?(@p2)).to eq(true)
      expect(purch.include?(@p3)).to eq(true)
    end
  end

  describe "self.related_type_exclude" do
    it "should properly exclude purchases" do
      purch = @person.purchases.related_type_exlude(['Album'])
      expect(purch.size).to eq(2)

      expect(purch.include?(@p1)).to eq(true)
      expect(purch.include?(@p3)).to eq(true)
    end
  end
end

describe Purchase do
  describe "::albums_in_cart" do
    before(:each) do
      @person = TCFactory.create(:person)
      @album1 = TCFactory.create(:album, person: @person)
      @album2 = TCFactory.create(:album, person: @person)
      @album3 = TCFactory.create(:album, person: @person)
    end

    context "user has 2 albums with purchase and 1 without" do
      it "should return all unpaid album purchases in the cart" do
        #albums with purchase
        TCFactory.create(
          :purchase,
          person: @person,
          related: @album1,
          product: Product.find_by(country_website: @person.country_website)
        )
        TCFactory.create(
          :purchase,
          person: @person,
          related: @album2,
          product: Product.find_by(country_website: @person.country_website)
        )

        expect(Purchase.albums_in_cart(@person)).to eq([@album1, @album2])
      end
    end

    context "use has an album purchase with a credit" do
      it "should return the album being purchased with a credit" do
        allow(CreditUsage).to receive(:credit_available_for?).and_return(true)
        credit_usage = TCFactory.create(:credit_usage, related: @album3, applies_to_type: 'Album', person: @person)
        TCFactory.create(:purchase, person: @person, related: credit_usage, product: Product.find_by(country_website: @person.country_website))

        expect(Purchase.albums_in_cart(@person)).to eq([@album3])
      end
    end
  end
end

describe Purchase do
  describe ".paid" do
    it "returns all paid purchases" do
      albums = create_list(:album, 2)
      purchase = create(:purchase, related: albums.first, person: albums.first.person, paid_at: Time.now)
      unpaid_purchase = create(:purchase, related: albums.last, person: albums.last.person, paid_at: nil)

      result = Purchase.paid

      expect(result).to include(purchase)
      expect(result).not_to include(unpaid_purchase)
    end
  end

  describe ".unpaid" do
    it "returns all unpaid purchases" do
      albums = create_list(:album, 2)
      purchase = create(:purchase, related: albums.first, person: albums.first.person, paid_at: Time.now)
      unpaid_purchase = create(:purchase, related: albums.last, person: albums.last.person, paid_at: nil)

      result = Purchase.unpaid

      expect(result).to include(unpaid_purchase)
      expect(result).not_to include(purchase)
    end
  end

  describe ".deleted_albums" do
    it "returns all unpaid purchases with deleted albums" do
      albums = create_list(:album, 2)
      deleted_albums = create_list(:album, 2, :is_deleted)
      purchase = create(:purchase, related: albums.first, person: albums.first.person, paid_at: Time.now)
      unpaid_purchase = create(:purchase, related: albums.last, person: albums.last.person, paid_at: nil)
      deleted_album_unpaid_purchase = create(:purchase, related: deleted_albums.first, person: deleted_albums.first.person, paid_at: nil)
      deleted_album_purchase = create(:purchase, related: deleted_albums.last, person: deleted_albums.last.person, paid_at: Time.now)

      result = Purchase.deleted_albums

      expect(result).to include(deleted_album_unpaid_purchase)
      expect(result).not_to include(deleted_album_purchase)
      expect(result).not_to include(unpaid_purchase)
      expect(result).not_to include(purchase)
    end
  end

  describe ".purchase_for" do
    context "when paid" do
      it "returns the first paid purchase for person and item" do
        item = create(:album)
        person = item.person
        first_purchase = create(:purchase, related: item, person: person, paid_at: Time.now)

        result = Purchase.purchase_for(person, item, true)

        expect(result).to eq first_purchase
      end
    end

    context "when unpaid" do
      it "returns the first unpaid purchase for person and item" do
        item = create(:album)
        person = item.person
        first_purchase = create(:purchase, related: item, person: person, paid_at: nil)

        result = Purchase.purchase_for(person, item, false)

        expect(result).to eq first_purchase
      end
    end
  end
end

describe Purchase do
  describe "#save (callbacks)", run_sidekiq_worker: true do
    let(:person)    { Person.find(69) } # Active customer
    let(:album)     { create(:album, person: person) }
    let(:composer)  { create(:composer, person: person, account: nil) }

    before(:each) do
      allow(Believe::PersonWorker).to receive(:perform_async).and_return(true)
    end

    it "calls believe api if related is Composer and paid_at is not null" do
      expect(Believe::PersonWorker).to receive(:perform_async)
      create(:purchase,
        person: person,
        related_type: "Composer",
        related_id: composer.id,
        paid_at: Time.now)
    end

    it "doesn't call believe api if related is not Composer" do
      expect(Believe::PersonWorker).not_to receive(:perform_async)
      create(:purchase,
        person: person,
        related_type: "Album",
        related_id: album.id,
        paid_at: Time.now)
    end

    it "doesn't call believe api if paid_at is null" do
      expect(Believe::PersonWorker).not_to receive(:perform_async)
      create(:purchase,
        person: person,
        related_type: "Composer",
        related_id: composer.id,
        paid_at: nil)
    end
  end
end

describe "#gst_tax_amount" do
  it "should return nil if there is not gst config" do
    invoice = create(:invoice, final_settlement_amount_cents: 400, gst_config_id: nil)
    purchase = create(:purchase, invoice: invoice, cost_cents: 500)
    expect(purchase.gst_tax_amount).to be_nil
  end

  it "should return the igst tax amount if igst is configured" do
    gst_config = create(:gst_config, igst: 18)
    invoice = create(:invoice, final_settlement_amount_cents: 400, gst_config_id: gst_config.id)
    purchase = create(:purchase, invoice: invoice, cost_cents: 200)
    expect(purchase.gst_tax_amount.igst.to_f).to eq(0.36)
    expect(purchase.gst_tax_amount.cgst).to be_nil
    expect(purchase.gst_tax_amount.sgst).to be_nil
  end

  it "should return the cgst and sgst tax amount if igst is null" do
    gst_config = create(:gst_config, cgst: 9, sgst: 8, igst: nil)
    invoice = create(:invoice, final_settlement_amount_cents: 400, gst_config_id: gst_config.id)
    purchase = create(:purchase, invoice: invoice, cost_cents: 200)
    expect(purchase.gst_tax_amount.cgst.to_f).to eq(0.18)
    expect(purchase.gst_tax_amount.sgst.to_f).to eq(0.16)
    expect(purchase.gst_tax_amount.igst).to be_nil
  end
end

describe '#need_sns_notification?' do
  context 'related is Salepoint' do
    it 'when returns true' do
      salepoint = Salepoint.new
      purchase = Purchase.new(related: salepoint)

      expect(purchase.need_sns_notification?).to eq(true)
    end
  end

  context 'when related is SalepointSubscription' do
    it 'returns true' do
      salepoint_subscription = create(:salepoint_subscription)
      purchase = Purchase.new(related: salepoint_subscription)

      expect(purchase.need_sns_notification?).to eq(true)
    end
  end

  context 'when related is not Salepoint or SalepointSubscription' do
    it 'returns false' do
      album = create(:album)
      purchase = Purchase.new(related: album)

      expect(purchase.need_sns_notification?).to eq(false)
    end
  end
end

describe '#related_album_id' do
  context 'when related is salepoint' do
    it 'returns salepointable id' do
      album = create(:album)
      salepoint = Salepoint.new(salepointable: album)
      purchase = Purchase.new(related: salepoint)

      expect(purchase.related_album_id).to eq(album.id)
    end
  end

  context 'when related is salepoint subscription' do
    it 'returns album id' do
      album = create(:album)
      salepoint_subscription = create(:salepoint_subscription, album: album)
      purchase = Purchase.new(related: salepoint_subscription)

      expect(purchase.related_album_id).to eq(album.id)
    end
  end

  context 'when related is other than salepoint or salepoint subscription' do
    it 'returns nil' do
      album = create(:album)
      purchase = Purchase.new(related: album)

      expect(purchase.related_album_id).to eq(nil)
    end
  end
end

describe '#related_store_id' do
  context 'when related is salepoint' do
    it 'return store id' do
      store = create(:store)
      salepoint = Salepoint.new(store: store)
      purchase = Purchase.new(related: salepoint)

      expect(purchase.related_store_id).to eq(store.id)
    end
  end

  context 'when related is not salepoint' do
    it 'returns nil' do
      album = create(:album)
      purchase = Purchase.new(related: album)

      expect(purchase.related_store_id).to be(nil)
    end
  end

describe 'Plan Addons' do
  let!(:aa_purchase) { create(:purchase, :additional_artist) }
  let!(:purchase) { create(:purchase) }

    describe '#plan_addon?' do
      it "returns true if purchase is a plan addon item" do
        expect(aa_purchase.plan_addon?).to be true
      end

      it "returns false if purchase is not a plan addon item" do
        expect(purchase.plan_addon?).to be false
      end
    end

    describe '#additional_artist?' do
      it "returns true if add on is an additional artist" do
        expect(aa_purchase.plan_addon?).to be true
      end

      it "returns false if purchase is not an additional artist" do
        expect(purchase.plan_addon?).to be false
      end
    end
  end

  context 'when related is not salepoint' do
    it 'returns nil' do
      album = create(:album)
      purchase = Purchase.new(related: album)

      expect(purchase.related_store_id).to be(nil)
    end
  end
end

describe '#proration_discounted?' do
  let(:purchase) { create(:purchase) }
  subject { purchase.proration_discounted? }

  context "when discount_reason contains text which matches 'proration'" do
    before { purchase.update_attributes!(discount_cents: 100, discount_reason: Purchase::PLAN_PRORATION) }
    it { should be true }
  end

  context "when discount_reason contains text which doesn't match 'proration'" do
    before { purchase.update_attributes!(discount_reason: Purchase::NO_DISCOUNT) }
    it { should be false }
  end
end

describe '#purchased_release' do
  it "returns the album when a user purchases an album" do
    album_purchase = create(:purchase, :with_variable_related_item)

    expect(album_purchase.purchased_release).to eq(album_purchase.related)
  end

  it "returns the album when a user purchases a release using a Plan or Distribution Credit" do
    person = create(:person)
    album = create(:album, person: person)
    create(:person_plan, person: person)
    credit_usage = CreditUsage.for(person, album)
    purchase = create(:purchase, :free_with_plan, person: person, related_id: credit_usage.id)

    expect(purchase.purchased_release).to eq(purchase.related.related)
  end

  it "returns nil when a user purchases a salepoint" do
    salepoint_purchase = create(:purchase)

    expect(salepoint_purchase.purchased_release).to be nil
  end

  it "returns nil when a user purchases an additional artist" do
    additional_artist_purchase = create(:purchase, :additional_artist)

    expect(additional_artist_purchase.purchased_release).to be nil
  end
end

describe '#paid_release?' do
  it "returns true when a user purchases an album with salepoints" do
    album_purchase = create(:purchase, :with_variable_related_item)
    allow(FeatureFlipper).to receive(:show_feature?)
      .with(:discovery_platforms, nil)
      .and_return(true)

    expect(album_purchase.paid_release?).to be true
  end

  it "returns false when a user purchases an album with only free salepoints" do
    album_purchase = create(:purchase, :with_free_album)
    allow(FeatureFlipper).to receive(:show_feature?)
      .with(:discovery_platforms, nil)
      .and_return(true)

    expect(album_purchase.paid_release?).to be false
  end

  it "returns false when a user purchases a salepoint" do
    salepoint_purchase = create(:purchase)

    expect(salepoint_purchase.paid_release?).to be false
  end

  it "returns false when a user purchases an additional artist" do
    additional_artist_purchase = create(:purchase, :additional_artist)

    expect(additional_artist_purchase.paid_release?).to be false
  end
end
