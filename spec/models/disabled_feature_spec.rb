require "rails_helper"

describe Feature do

  before(:each) do
    @disabled_feature = Feature.new()
  end

  it "should provide a relationship to country websites" do
    expect(@disabled_feature.disabled_features_country_websites.size).to eq(0)
  end

  it "should be able to add country websites" do
    @disabled_feature.disabled_country_websites << CountryWebsite.first
    assert @disabled_feature.disabled_country_websites.size == 1
  end

end

