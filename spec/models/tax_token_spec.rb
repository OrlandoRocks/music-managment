require "rails_helper"

describe TaxToken do

  before(:each) do
    @person    = create(:person)
  end

  describe ".for" do

    context "given a person without a tax token" do
      it "returns nil" do
        expect(TaxToken.for(@person)).to be_nil
      end
    end

    context "given a person with a tax token with is_visible set to false" do
      it "returns nil" do
        create(:tax_token, person: @person, is_visible: false)
        expect(TaxToken.for(@person).try(:token)).to be_nil
      end
    end

    context "given a person with a tax token with is_visible set to true" do
      it "returns the tax_token's token" do
        tt = create(:tax_token, person: @person, is_visible: true)
        expect(TaxToken.for(@person).try(:token)).to eq tt.token
      end
    end

    context "given a person with multiple 'visible' tax tokens" do
      it "returns the most recently created tax_token's token" do
        tt1 = create(:tax_token, person: @person, is_visible: true)
        tt2 = create(:tax_token, person: @person, is_visible: true)
        expect(TaxToken.for(@person).try(:token)).to eq tt2.token
      end
    end
  end

  describe "#form_submitted?" do
    context "when tax_form_type is present" do
      it "is true" do
        tt = build_stubbed(:tax_token, person: @person, is_visible: true, tax_form_type: "01")
        expect(tt.form_submitted?).to be true
      end
    end

    context "when tax_form_type is not present" do
      it "is false" do
        tt = build_stubbed(:tax_token, person: @person, is_visible: true)
        expect(tt.form_submitted?).to be false
      end
    end
  end
end
