require 'rails_helper'

describe RoyaltyPayment do
  describe "#downloadable?" do
    context "when the royalty_payment belongs to the user that is passed in" do
      it "returns true" do
        person = create(:person)
        royalty_payment = create(:royalty_payment, person: person)

        expect(royalty_payment.downloadable?(person)).to be_truthy
      end
    end

    context "when the user passed in is an admin user" do
      it "returns true" do
        person = create(:person, :admin)
        royalty_payment = create(:royalty_payment)

        expect(royalty_payment.downloadable?(person)).to be_truthy
      end
    end

    context "when the user passed in is not an admin or connected to the royalty_payment" do
      it "returns false" do
        person = create(:person)
        royalty_payment = create(:royalty_payment)

        expect(royalty_payment.downloadable?(person)).to be_falsey
      end
    end
  end
end
