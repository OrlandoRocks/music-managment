require "rails_helper"

describe YtmTracks do

  describe "#default_conditions" do

    context "inlcude_all_monetized is true" do
      it "should include all_monetized_conditions" do
        expect(YtmTracks.default_conditions(true).include?(YtmTracks.all_monetized_conditions)).to eq(true)
      end
    end

    context "no parameters given" do
      it "should not include all_monetized_conditions" do
        expect(YtmTracks.default_conditions.include?(YtmTracks.all_monetized_conditions)).to eq(false)
        expect(YtmTracks.default_conditions(true).include?(YtmTracks.all_monetized_conditions)).to eq(true)
      end

      it "should include live_album_conditions" do
        expect(YtmTracks.default_conditions.include?(YtmTracks.live_album_conditions)).to eq(true)
      end
    end
  end

  describe "#unsent_songs_search" do
    before :each do
      @song1  = create(:song)
      @album  = @song1.album
      @song2  = create(:song, album: @album)
      @song3  = create(:song, album: @album)
      @person = @album.person
      @ytm_tracks = YtmTracks.new(@person)
      allow(@ytm_tracks).to receive(:search).with(status: "eligible_not_sent", per_page: 1000000).and_return([[@song1, @song2, @song3], true])
    end

    it "returns relevant information about unsent songs" do
      expect(@ytm_tracks.unsent_songs_search).to eq(
        [[@song1, @song2, @song3].map do |song|
          {"id" => song.id, "name" => song.name, "artist_names" => [], "album_name" => song.album_name}
        end, true])
    end
  end

  describe "#has_unactioned_songs?" do
    before(:each) do
      @person = create(:person)
      @album  = create(:album, :approved, person: @person)
      @song   = create(:song, album: @album)
      @album2 = create(:album, person: @person, payment_applied: true)
      @song2  = create(:song, album: @album2)
    end

    context "user has youtube_monetization active" do
      before(:each) do
        allow(@person).to receive(:has_active_ytm?).and_return(true)
      end

      it "returns true if the users songs do not have a related salepoint_song or ytm_ineligible_song or ytm_blocked_song" do
        expect(YtmTracks.new(@person).has_unactioned_songs?).to be true
      end

      it "returns false if all the user's finalized and paid for albums have a salepoint_song" do
        store = Store.find_by(abbrev: "ytsr")
        salepointable_store = create(:salepointable_store, store: store)
        create(:salepoint_song, song: @song, salepoint: create(:salepoint, salepointable: @album, store: store))
        expect(YtmTracks.new(@person).has_unactioned_songs?).to be false
      end

      it "returns false if all the user's finalized and paid for albums have a ytm_ineligible song" do
        create(:ytm_ineligible_song, song: @song)
        expect(YtmTracks.new(@person).has_unactioned_songs?).to be false
      end

      it "returns false if all the user's finalized and paid for albums have a ytm_blocked song" do
        create(:ytm_blocked_song, song: @song)
        expect(YtmTracks.new(@person).has_unactioned_songs?).to be false
      end
    end

    it "returns false if the user does not have active ytm" do
      allow(@person).to receive(:has_active_ytm?).and_return(false)
      expect(YtmTracks.new(@person).has_unactioned_songs?).to be false
    end
  end
end
