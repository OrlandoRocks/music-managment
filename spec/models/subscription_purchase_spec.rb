require "rails_helper"

describe SubscriptionPurchase do
  let(:person) {build_stubbed(:person)}

  describe "validates presence of person" do
    it "is invalid without a person" do
      purchase = build(:subscription_purchase, :social_us, person: nil)
      expect(purchase).not_to be_valid
    end

    it "is valid witha a person" do
      purchase = build_stubbed(:subscription_purchase, :social_us, person: person)
      expect(purchase).to be_valid
    end
  end

  describe "#expires?" do
    it "returns true if purchase's subscription_product has an effective_date and termination_date" do
      purchase = build_stubbed(:subscription_purchase, :social_us,
        effective_date: DateTime.now,
        termination_date: DateTime.now + 1.month)

        expect(purchase.expires?).to be true
    end

    it "returns false if purchase's subscription_product has no effective_date or termination_date" do
      purchase = build_stubbed(:subscription_purchase, :fb_tracks)

      expect(purchase.expires?).to be false
    end
  end

  describe "#next_termination_date" do
    it "returns termination_date based on subscription_product's term_length" do
      date_time = DateTime.now
      subscription_product = build_stubbed(:subscription_product)
      next_term_date = date_time + subscription_product.term_length.month
      purchase = build_stubbed(
        :subscription_purchase,
        :social_us,
        effective_date: date_time - 1.month,
        termination_date: date_time
      )

      expect(purchase.next_termination_date(date_time)).to eq(next_term_date)
      expect(purchase.next_termination_date(date_time).present?).to be true
    end

    it "returns nil if subscription_purchase does not expire" do
      date_time = DateTime.now
      purchase = build_stubbed(:subscription_purchase, :fb_tracks)

      expect(purchase.next_termination_date(date_time).nil?).to be true
    end

  end

  describe "#name" do
    it "should return name on product for social purchase" do
      purchase = build_stubbed(:subscription_purchase, :social_us, person: person)
      product = purchase.subscription_product
      expect(purchase.name).to eq(product.product_name)
    end
  end

  describe "#paid_post_proc" do
    it "calls SubscriptionEvent::create_purchase_event" do
      purchase = build_stubbed(:subscription_purchase, :social_us, person: person)
      expect(SubscriptionEvent).to receive(:create_purchase_event)
      purchase.paid_post_proc
    end
  end

  describe "#find_related_person_subscription_status" do
    it "calls subscription_status_by_type on the related purchase" do
      purchase = build_stubbed(:subscription_purchase, :social_us, person: person)
      expect(purchase.person).to receive(:subscription_status_for).with("Social")
      purchase.find_related_person_subscription_status("Social")
    end
  end
end
