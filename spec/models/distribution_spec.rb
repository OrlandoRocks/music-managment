require "rails_helper"

describe Distribution do
  let(:album)        { FactoryBot.create(:album) }
  let(:petri_bundle) { FactoryBot.create(:petri_bundle, album: album) }
  let(:distro)       do
    distribution = create(:distribution,
      petri_bundle: petri_bundle,
      converter_class: "MusicStores::SevenDigital::Converter",
      delivery_type: nil)

    distribution.salepoints << create(:salepoint, salepointable: album, store: Store.find_by(short_name: "7Digital"))

    distribution
  end

  it "should initialize in the new state" do
    expect(distro).to be_valid
  end

  it "should start" do
    expect(DistributorAPI::Store).to receive(:unpause).with(distro.store.id)
    distro.store.store_delivery_config.unpause!

    expect(distro.petri_bundle_id).not_to be_nil
    distro.start(:actor => 'System', :message => "Enqueuing distribution")
    expect(distro.state).to eq("enqueued")
  end

  it "should pause" do
    expect(DistributorAPI::Store).to receive(:pause).with(distro.store.id)
    distro.store.store_delivery_config.pause!

    expect(distro.petri_bundle_id).not_to be_nil
    distro.start(:actor => 'System', :message => "Enqueuing distribution")
    expect(distro.state).to eq("paused")
  end

  describe "blocked?" do
    it "should retrun true if state is blocked" do
      distro.state = 'blocked'
      expect(distro.blocked?).to eq(true)
    end
  end

  describe "block" do
    it "should switch state to blocked and log transition" do
      expect {
        distro.block(:actor => "TuneCore Test:", :mesage => "blocking_distribution")
      }.to change(Transition, :count).by(1)

      expect(distro.state).to eq('blocked')
    end
  end

  describe "#store" do
    it "should return the store for the first salepoint" do
      salepoint     = FactoryBot.build(:salepoint, store: Store.first)
      distribution  = FactoryBot.build(:distribution, salepoints: [salepoint])
      expect(distribution.store).to eq(Store.first)
    end
  end

  describe "#taken_down?" do
    it "is false if takedown_at is null" do
      album = build_stubbed(:album)
      petri_bundle = build_stubbed(:petri_bundle, album: album)
      distribution  = build_stubbed(:distribution, petri_bundle: petri_bundle)
      expect(distribution.taken_down?).to eq false
    end

    it "is true if takedown_at is not null" do
      album        = build_stubbed(:album, :taken_down)
      petri_bundle = build_stubbed(:petri_bundle, album: album)
      distribution = build_stubbed(:distribution, petri_bundle: petri_bundle)
      expect(distribution.taken_down?).to eq true
    end
  end

  describe ".deliver" do
    let(:converted_album)  { build(:distribution_system_album, store_name: "BelieveExp") }
    let(:distro)       { create(:distribution,
                          converter_class: "MusicStores::SevenDigital::Converter",
                          delivery_type: nil) }
    let(:salepoint)    {create(:salepoint, store: Store.find_by(short_name: "7Digital")) }
    let(:s3_asset) {  double(key: double("key")) }

    before(:each) do
      allow_any_instance_of(Store).to receive(:delivery_service).and_return("tunecore")
      distro.salepoints << salepoint
      distro.save
      allow(distro).to receive(:store_is_in_use?).and_return(true)
      allow(converted_album).to receive(:to_yaml).and_return("")
      artwork = double("artwork")
      allow(artwork).to receive(:s3_asset).and_return(s3_asset)
      allow_any_instance_of(Album).to receive(:artwork).and_return(artwork)
    end

    it "dismisses and returns if the converter class isn't defined" do
      allow_any_instance_of(Distribution).to receive(:approved_for_distribution?).and_return(true)

      distribution = create(:distribution, :with_salepoint, converter_class: "MusicStores::Debunk::Converter", delivery_type: nil)
      distribution.deliver("metadata_only")
      expect(distribution.state).to eq "error"
    end

    it "marks the distribution as 'pending_approval' of the album is not approved" do
      allow_any_instance_of(DistributionSystem::Deliverer).to receive(:distribute).and_return(true)
      fake_converted_album = double(has_valid_converter_class?: true, convert: double(s3_asset: s3_asset))
      allow(Delivery::AlbumConverter).to receive(:new).and_return(fake_converted_album)
      allow(distro.petri_bundle.album).to receive(:legal_review_state).and_return("NEEDS REVIEW")
      expect(distro.state).to eq("new")
      distro.deliver('full_delivery', nil)
      expect(distro.reload.state).to eq("pending_approval")
    end

    context "when the distribution does not have salepoints for a store that is in use" do
      before(:each) do
        allow(distro).to receive(:store_is_in_use?).and_return(false)
      end
    end

    context "when the distribution requires track level distribution" do
      before do
        allow(distro).to receive(:requires_track_level_distribution?).and_return(true)
      end

      context "when the distribution does not have monetized tracks" do
        it "should not distribute the distribution songs" do
          allow_any_instance_of(DistributionSystem::Deliverer).to receive(:distribute).and_return(true)
          allow(distro).to receive(:archive_metadata).and_return(true)
          distro.deliver('metadata_only', nil)
        end
      end

      context "when the distribution has monetized tracks" do
        let(:distro) { create(:distribution, :with_distribution_songs) }

        it "should distribute the distribution songs" do
          distro.distribution_songs.each do |dist|
            expect(dist).to receive(:retry).with(actor: "Youtubesr Distribution Retry", message: "Full YoutubeSr Distro Retry")
          end
          distro.deliver('metadata_only', nil)
        end
      end
    end

    context "when the distribution has been blocked" do
      let(:distro) { create(:distribution, :with_distribution_songs) }

      before :each do
        distro.state = "blocked"
      end

      it "returns immediately when the distribution has been blocked" do
        expect(distro).not_to receive(:store_is_in_use?)
        distro.deliver("metadata_only")
      end

      it "does not raise an error" do
        expect(distro.deliver("metadata_only")).to eq(false)
      end

      it "logs the blocked distribution" do
        expect(Rails.logger).to receive(:info).with("Skipping enqueue Distribution (#{distro.id}) for album #{distro.petri_bundle.album.id} because it has been blocked.")
        distro.deliver("metadata_only")
      end
    end
  end

  describe ".find_by_store_name_and_upc" do
    it "returns a distribution in a non-dismissed petri bundle" do
      album                  = create(:album)
      salepoint              = create(:salepoint, salepointable: album, store: Store.find_by(short_name: "7Digital"))
      dismissed_petri_bundle = create(:petri_bundle, album: album)
      dismissed_distribution = create(:distribution, converter_class: "MusicStores::SevenDigital::Converter", petri_bundle: dismissed_petri_bundle)
      dismissed_distribution.salepoints << salepoint
      dismissed_petri_bundle.dismiss({})
      active_petri_bundle    = create(:petri_bundle, album: album)
      active_distribution    = create(:distribution, converter_class: "MusicStores::SevenDigital::Converter", petri_bundle: active_petri_bundle)
      active_distribution.salepoints << salepoint
      expect(Distribution.find_by_store_name_and_upc("7Digital", album.upc.number)).not_to eq dismissed_distribution
      expect(Distribution.find_by_store_name_and_upc("7Digital", album.upc.number)).to eq active_distribution
    end
  end

  describe Distribution, "scopes" do
    context ".unreviewed_albums" do
      it "finds distributions for albums that need review" do
        album = create(:album, :finalized, legal_review_state: "NEEDS REVIEW")
        approved_album = create(:album, :finalized, legal_review_state: "APPROVED")
        petri_bundle = create(:petri_bundle, album: album)
        approved_petri_bundle = create(:petri_bundle, album: approved_album)
        distribution = create(:distribution, petri_bundle: petri_bundle)
        approved_distribution = create(:distribution, petri_bundle: approved_petri_bundle)

        result = Distribution.unreviewed_albums

        expect(result).to include(distribution)
        expect(result).to_not include(approved_distribution)
      end
    end
  end
end
