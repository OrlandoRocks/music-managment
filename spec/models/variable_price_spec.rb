require "rails_helper"

describe VariablePrice do
  describe "#price_by_country_website" do
    before (:each) do
      @variable_price = VariablePrice.find_by(price_code: "99")
    end
    context "country website is US" do
      it "returns non-wholesale price" do
        usa = CountryWebsite.find_by(currency: "USD")
        expect(@variable_price.price_by_country_website(usa)).to eq(@variable_price.price)
      end
    end

    context "country website is non-us" do
      it "returns wholesale price" do
        uk = CountryWebsite.find_by(currency: "GBP")
        uk_variable_price = CountryVariablePrice.where("variable_price_id = ? and country_website_id = ?", @variable_price.id, uk.id).first
        expect(@variable_price.price_by_country_website(uk)).to eq(uk_variable_price.wholesale_price)
      end
    end
  end
end
