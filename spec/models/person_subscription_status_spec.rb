require "rails_helper"

describe PersonSubscriptionStatus do
  describe ".active" do
    let!(:person_subscription_status_without_effective_date) { create(:person_subscription_status, effective_date: nil) }
    let!(:person_subscription_status_without_termination_date) { create(:person_subscription_status, termination_date: nil) }
    let!(:person_subscription_status_with_past_termination_date) { create(:person_subscription_status, termination_date: 1.day.ago) }
    let!(:active_person_subscription_status) { create(:person_subscription_status) }

    it "returns only records that are considered to be active" do
      active_records = described_class.active
      expect(active_records).to include(
        active_person_subscription_status,
        person_subscription_status_without_termination_date
      )
      expect(active_records).not_to include(
        person_subscription_status_without_effective_date,
        person_subscription_status_with_past_termination_date
      )
    end
  end

  describe ".with_termination_date" do
    let!(:active_person_subscription_status) { create(:person_subscription_status) }
    let!(:person_subscription_status_without_termination_date) { create(:person_subscription_status, termination_date: nil) }
    let!(:person_subscription_status_with_past_termination_date) { create(:person_subscription_status, termination_date: 1.day.ago) }

    it "returns only records that are considered to be active and expiring after today" do
      records_with_termination_date = described_class.with_termination_date
      expect(records_with_termination_date).to include(
        active_person_subscription_status,
        person_subscription_status_with_past_termination_date
      )
      expect(records_with_termination_date).not_to include(
        person_subscription_status_without_termination_date
      )
    end
  end

  describe ".social" do
    let!(:non_social_person_subscription_status) { create(:person_subscription_status, subscription_type: "FBTracks") }
    let!(:social_person_subscription_status) { create(:person_subscription_status, subscription_type: "Social")}

    it "returns only records that are 'social' subscriptions" do
      social_records = described_class.social
      expect(social_records).to include(social_person_subscription_status)
      expect(social_records).not_to include(non_social_person_subscription_status)
    end
  end

  describe "#create_subscription" do
    it "should create an active subscription with a valid email" do
      new_user = create(:person, email:"foobar@tunecore.com")
      expect(PersonSubscriptionStatus.create_subscription_by_email("foobar@tunecore.com", "2020-10-10", "Social")).to eq true
    end

    it "should not create a subscription with an invalid email" do
      expect(PersonSubscriptionStatus.create_subscription_by_email(nil, "2020-10-10", "Social")).to eq false
    end
  end

  describe "#is_active?" do
    it "returns true for a nil termination date" do
      person_subscription_status = build_stubbed(:person_subscription_status, effective_date: Time.now)
      expect(person_subscription_status).to be_is_active
    end

    it "should return true when termination date is in the future" do
      person_subscription_status = build_stubbed(:person_subscription_status, termination_date: Time.now + 30.days, effective_date: Time.now)
      expect(person_subscription_status.is_active?).to eq true
    end

    it "should return false when termination date is in the past" do
      person_subscription_status = build_stubbed(:person_subscription_status, termination_date: Time.now - 30.days, effective_date: Time.now)
      expect(person_subscription_status.is_active?).to eq false
    end

    it "should return false when effective date is nil" do
      person_subscription_status = build_stubbed(:person_subscription_status, effective_date: nil)
      expect(person_subscription_status.is_active?).to eq false
    end
  end

  describe "#expires?" do
    it "returns true if subscription_status has an effective_date and termination_date" do
    person_subscription_status = build_stubbed(:person_subscription_status, subscription_type: "Social")
      expect(person_subscription_status.expires?).to be true
    end

    it "returns false if subscription_status has no effective_date or termination_date" do
      person_subscription_status = build_stubbed(:person_subscription_status,
        subscription_type: "FBTracks",
        effective_date: nil,
        termination_date: nil)

      expect(person_subscription_status.expires?).to be false
    end
  end

  describe "#next_purchase_event_type" do
    context "termination_date is after the current time" do
      it "returns 'Renewal'" do
        person_subscription_status = build_stubbed(:person_subscription_status, subscription_type: "Social")
        expect(person_subscription_status.next_purchase_event_type).to eq("Renewal")
      end
    end

    context "termination_date is before the current time" do
      it "returns 'Purchase'" do
        person_subscription_status = build_stubbed(:person_subscription_status, canceled_at: Time.now)
        expect(person_subscription_status.next_purchase_event_type).to eq("Purchase")
      end
    end
  end

  describe "#next_purchase_effective_start_date" do
    context "termination_date is after the current time" do
      it "returns the termination_date" do
        person_subscription_status = build_stubbed(:person_subscription_status, termination_date: DateTime.now + 2.days)
        expect(person_subscription_status.next_purchase_effective_start_date).to eq(person_subscription_status.termination_date)
      end
    end

    context "termination_date is before the current time" do
      it "returns the current time" do
        person_subscription_status = build_stubbed(:person_subscription_status, termination_date: DateTime.now - 2.days)
        date_time = DateTime.now
        allow(DateTime).to receive(:now).and_return(date_time)

        expect(person_subscription_status.next_purchase_effective_start_date).to eq(date_time)
      end
    end

    context "termination_date is nil (subscription never expires)" do
      it "returns the existing effective date" do
        person_subscription_status = build_stubbed(:person_subscription_status, termination_date: nil)
        effective_date = person_subscription_status.effective_date

        expect(person_subscription_status.next_purchase_effective_start_date).to eq(effective_date)
      end
    end
  end

  describe "#grace_period_ends_on" do
    it "is Today for 0 grace period day" do
      time = Time.local(2017, 5, 1, 12, 0, 0) # May 1st, 2017 is a Monday
      Timecop.freeze(time)
      subscription_status = build_stubbed(:person_subscription_status,
        termination_date: Date.today,
        grace_period_length: 0)

      expect(subscription_status.grace_period_ends_on.to_date).to eq Date.today
      Timecop.return
    end

    it "is 5 days for default grace period" do
      time = Time.local(2017, 4, 29, 12, 0, 0) # Saturday
      Timecop.freeze(time)
      subscription_status = build_stubbed(:person_subscription_status,
        termination_date: Date.today,
        grace_period_length: 5)

      expect(subscription_status.grace_period_ends_on.to_date).to eq Date.today + 5.days
      Timecop.return
    end
  end

  describe "#within_grace_period?" do
    it "is true when grace period active" do
      subscription_status = build_stubbed(:person_subscription_status,
        termination_date: Date.today - 2.days,
        grace_period_length: 5)

      expect(subscription_status.within_grace_period?).to eq true
    end

    it "is true when grace period ends today" do
      subscription_status = build_stubbed(:person_subscription_status,
        termination_date: Date.today - 5.days,
        grace_period_length: 5)

      expect(subscription_status.within_grace_period?).to eq true
    end

    it "is false when subscription_status never expires" do
      subscription_status = build_stubbed(:person_subscription_status,
        subscription_type: "FBTracks",
        termination_date: nil)

      expect(subscription_status.within_grace_period?).to be false
    end

    it "is false when grace period inactive" do
      subscription_status = build_stubbed(:person_subscription_status,
        termination_date: Date.today - 10.days,
        grace_period_length: 5)

      expect(subscription_status.within_grace_period?).to eq false
    end
  end

  describe "grace_period_ends_today?" do
    it "is true when it is today" do
      subscription_status = build_stubbed(:person_subscription_status,
        termination_date: Date.today - 5.days,
        grace_period_length: 5)

      expect(subscription_status.grace_period_ends_today?).to eq true
    end

    it "is true when it is today and you have a 0 day grace period" do
      subscription_status = build_stubbed(:person_subscription_status,
        termination_date: Date.today,
        grace_period_length: 0)

      expect(subscription_status.grace_period_ends_today?).to eq true
    end

    it "is false when it is not today" do
      subscription_status = build_stubbed(:person_subscription_status,
        termination_date: Date.today - 4.days,
        grace_period_length: 5)

      expect(subscription_status.grace_period_ends_today?).to eq false
    end

    it "is false when subscription_status never expires" do
      subscription_status = build_stubbed(:person_subscription_status,
        subscription_type: "FBTracks",
        termination_date: nil)

      expect(subscription_status.grace_period_ends_today?).to be false
    end
  end

  describe "grace_period_ends_in_N_days?" do
    (1..20).each do |grace_period_length|
      it "is true if expires in " do
        subscription_status = build_stubbed(:person_subscription_status,
          termination_date: Date.today,
          grace_period_length: grace_period_length)

        expect(subscription_status.send("grace_period_ends_in_#{grace_period_length}_days?")).to eq true
      end
    end
  end

  describe "#days_until_grace_period_ends" do
    it "is 5 when termination date is today and grace period is 5 days" do
      subscription_status = build_stubbed(:person_subscription_status,
        termination_date: Date.today,
        grace_period_length: 5)

      expect(subscription_status.days_until_grace_period_ends).to eq 5
    end

    it "is 3 when termination date is 2 days ago and grace period is 5 days" do
      subscription_status = build_stubbed(:person_subscription_status,
        termination_date: Date.today - 2.days,
        grace_period_length: 5)

      expect(subscription_status.days_until_grace_period_ends).to eq 3
    end
  end

  describe "#product_type" do
    it "is the product_type in the related subscription_product" do
      subscription_status   = create(:person_subscription_status)
      subscription_purchase = create(:subscription_purchase, :social_us)
      subscription_event    = create(:subscription_event, :social,
        person_subscription_status: subscription_status,
        subscription_purchase: subscription_purchase)

        expect(subscription_status.product_type).to eq subscription_purchase.subscription_product.product_type
    end
  end

  describe "#terminated?" do
    let(:person_subscription) { build_stubbed(:person_subscription_status) }

    subject { person_subscription.terminated? }

    context "when the termation date is in the future" do
      before do
        allow(person_subscription).to receive(:termination_date).and_return(DateTime.current + 10.days)
      end

      it { should be_falsey }
    end

    context "when the termation date is in the past" do
      before do
        allow(person_subscription).to receive(:termination_date).and_return(DateTime.yesterday)
      end

      it { should be_truthy }
    end
  end
end
