require "rails_helper"

describe TypefaceEffect do 
  describe "#render" do 
    let(:destination_raw_file) { Rails.root.join("spec", "fixtures", "artist.jpg") }
    let(:destination_trimmed_file) { Rails.root.join("spec", "fixtures", "typeface_effect_destination.png") }

    before do
      FileUtils.copy_file(destination_raw_file, "#{Rails.root}/spec/fixtures/artist.jpg.bak")
    end

    after(:each) do 
      FileUtils.copy_file("#{Rails.root}/spec/fixtures/artist.jpg.bak", destination_raw_file)
      File.delete("#{Rails.root}/spec/fixtures/artist.jpg.bak")
      File.delete(destination_trimmed_file)
    end 

   it "creates the typeface file" do
    cover           = FactoryBot.create(:cover)
    typeface_effect = TypefaceEffect.new

    typeface_effect.render(
      cover, 
      :artist,
      'test_title',
      cover.artist_typeface.name,
      cover.artist_typeface_pointsize_for_render,
      destination_raw_file,
      destination_trimmed_file
      )

      expect(File.exists?(destination_trimmed_file)).to be true
   end
  end 
end 