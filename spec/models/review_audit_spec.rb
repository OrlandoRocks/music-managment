require "rails_helper"

describe ReviewAudit do
  let(:admin_person) { create(:person, :admin, :with_crt_manager_role, email: "reallycoolguy@tunecore.com") }

  before(:each) do
    Sns::Notifier.instance_variable_set("@sns_client", nil)
    Sns::Notifier.instance_variable_set("@topic", nil)
  end

  context "when album is invalid" do
    it "sets an error" do
      album = create(:album, :with_salepoints, :with_artwork, :with_uploaded_songs)
      audit =  build(:review_audit, album: album, person: admin_person)
      allow(audit.person).to receive(:has_role?).and_return(true)
      audit.valid?
      expect(audit.errors[:base])
    end
  end

  context "when user does not have authority to overide state" do
    it "sets an error" do
      audit =  FactoryBot.build(:review_audit, person: admin_person)
      allow(audit.person).to receive(:has_role?).and_return(false)

      audit.valid?

      expect(audit.errors[:base])
    end
  end

  context "when creating an audit for event NOT SURE" do
    it "skips validation for presence of reasons" do
      album = create(:album, :with_salepoints, :with_artwork, :with_uploaded_songs)
      audit =  FactoryBot.build(:review_audit, album: album, event: "NOT SURE", person: admin_person, note: FactoryBot.create(:note, related: album, note_created_by: admin_person))
      allow(audit.person).to receive(:has_role?).and_return(true)
      expect(audit.valid?).to be == true
    end
  end

  context "when user does not have authority to choose the reason" do
    it "sets an error" do
      audit =  FactoryBot.build(:review_audit, event: "FLAGGED", person: admin_person)
      reason = double("reason")
      allow(reason).to receive(:roles).and_return([])
      allow(reason).to receive(:reason).and_return("")
      allow(reason).to receive(:reason_type).and_return("")
      allow(audit).to receive(:review_reasons).and_return([reason])
      audit.valid?
      expect(audit.errors[:base])
    end
  end

  describe "#unfinalize_if_rejected" do
    before :each do
      @album = create(:album, :finalized, :with_salepoints, :with_artwork, :with_uploaded_songs)
      @admin_role = double("Role")
      allow(admin_person).to receive(:roles).and_return([@admin_role])
      allow(admin_person).to receive(:has_role?).with("Content Review Manager").and_return(true)
      @review_audit = build(:review_audit, event: "REJECTED", album: @album, person: admin_person)
      allow(@review_audit).to receive(:notify_customer)
      @review_reason = build(:review_reason, should_unfinalize: true)
      allow(@review_reason).to receive(:roles).and_return([@admin_role])
      allow(@review_audit).to receive(:review_reasons).and_return([@review_reason])
    end

    it "unfinalizes an album rejected for a reason requiring unfinalizing" do
      @review_audit.save

      expect(@album.finalized_at).to be_nil
    end

    it "doesn't unfinalize otherwise" do
      @review_reason.update(should_unfinalize: false)
      @review_audit.save

      expect(@album.finalized_at).to_not be_nil
    end
  end

  describe "#send_facebook_tracks" do
    context "for users without a FBTracks subscription" do
      it "does not create track_monetizations" do
        allow(admin_person).to receive(:roles).and_return(double("Role"))
        allow(admin_person).to receive(:has_role?).with("Content Review Manager").and_return(true)
        album = create(:album, :payment_applied)
        create(:petri_bundle, album: album)
        review_audit = build(:review_audit, album_id: album.id, person_id: album.person.id, event: "APPROVED")
        expect{
          review_audit.save
        }.not_to change(TrackMonetization, :count)
      end
    end

    context "for users with FBTracks subscriptions" do
      before :each do
        allow(admin_person).to receive(:roles).and_return(double("Role"))
        allow(admin_person).to receive(:has_role?).with("Content Review Manager").and_return(true)
        @subscriber = create(:person, :with_fb_tracks)
        @new_album  = create(:album, :payment_applied, person: @subscriber)
        create(:petri_bundle, album: @new_album)
        @review_audit = build(:review_audit, album_id: @new_album.id, person_id: admin_person.id, event: "APPROVED")
      end

      it "creates track_monetizations for each of the user's songs" do
        expect{
          @review_audit.save
        }.to change(TrackMonetization, :count).by(@new_album.songs.count)
      end

      it "does not send the tracks" do
        expect_any_instance_of(TrackMonetization).not_to receive(:start)
      end
    end

    context "for users without FBtracks Subscription" do
      context "albums that have facebook freemium salepoint" do
        before :each do
          allow(admin_person).to receive(:roles).and_return(double("Role"))
          allow(admin_person).to receive(:has_role?).with("Content Review Manager").and_return(true)
          @new_album  = create(:album, :payment_applied)
          create(:salepoint, salepointable: @new_album, store_id: Store::FB_REELS_STORE_ID)
          create(:petri_bundle, album: @new_album)
          create(:song, album: @new_album)
          @review_audit = build(:review_audit, album_id: @new_album.id, person_id: admin_person.id, event: "APPROVED")
          allow(FeatureFlipper).to receive(:show_feature?).and_return(false)
          allow(FeatureFlipper).to receive(:show_feature?).with(:freemium_flow, an_instance_of(Person)).and_return(true)
        end
        it "creates TrackMonetizations for FB" do
          expect(TrackMonetization::FbPostApprovalWorker).to receive(:perform_async).with(@new_album.id, Store::FBTRACKS_STORE_ID)
          @review_audit.save
        end
      end
    end
  end

  describe ".send_album_to_petri" do
    before { allow_any_instance_of(ReviewAudit).to receive(:send_sns_notification) }

    context "when distributing to stores that are paused" do
      it "moves distributions to the correct state whether or not the store is paused" do
        album = create(:album, :with_salepoints, :with_artwork, :with_uploaded_songs)
        create(:petri_bundle, album: album)

        allow(admin_person).to receive(:has_role?).with("Content Review Manager").and_return(true)
        spotify = Store.find_by(short_name: "Spotify")
        shazam = Store.find_by(short_name: "Shazam")

        expect(DistributorAPI::Store).to receive(:pause).with(spotify.id)
        spotify.store_delivery_config.pause!

        spotify_distribution = DistributionCreator.create(album, spotify.short_name).distribution
        shazam_distribution = DistributionCreator.create(album, shazam.short_name).distribution

        create(
          :review_audit,
          album:  album,
          event:  "APPROVED",
          person: admin_person,
          note:   create(:note, related: album, note_created_by: admin_person)
        )

        expect(spotify_distribution.reload.state).to eq("paused")
        expect(shazam_distribution.reload.state).to eq("enqueued")
      end
    end
  end

  describe '#send_sns_notification' do
    it 'notify when album is approved' do
      album = create(:album, :with_salepoints, :with_petri_bundle)
      album_store_ids = album.salepoints.pluck(:store_id)
      excluded_store_ids = Store::EXCLUDED_FROM_NOTIFICATION_IDS
      valid_store_ids = (album_store_ids - excluded_store_ids).sort
      valid_store_ids.map { |store_id| Store.find(store_id).update(in_use_flag: true, is_active: true) }

      @sns_message = double
      @sns_client = instance_double(Aws::SNS::Resource)
      @topic = instance_double(Aws::SNS::Topic)
      allow_any_instance_of(Store).to receive(:paused?).and_return(false)

      sns_expected_message = {
        message: {
          album_id: album.id,
          stores: valid_store_ids.map { |store_id| { id: store_id, delivery_type: 'full_delivery', delivered: false } },
          person_id: admin_person.id
        }.to_json
      }

      allow(admin_person).to receive(:has_role?).with("Content Review Manager").and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
      expect(Aws::SNS::Resource).to receive(:new).and_return(@sns_client).once
      expect(@sns_client).to receive(:topic).with(any_args).and_return(@topic)
      expect(@topic).to receive(:publish).with(sns_expected_message)

      create(:review_audit,
             album_id: album.id,
             event: "APPROVED",
             person: admin_person,
             note: create(:note, related: album, note_created_by: admin_person))
    end
  end

  describe "#create_youtube_monetizations" do
    before :each do
      allow(admin_person).to receive(:roles).and_return(double("Role"))
      allow(admin_person).to receive(:has_role?).with("Content Review Manager").and_return(true)

      allow_any_instance_of(ReviewAudit).to receive(:send_sns_notification).and_return(nil)
    end

    context "for users who have not opted into youtube monetization in any way" do
      it "does not call upon the TrackMonetization::PostApprovalWorker" do
        album = create(:album, :payment_applied, :with_songs)

        create(:petri_bundle, album: album)

        review_audit = build(:review_audit, album: album, person: admin_person, event: "APPROVED")

        expect(TrackMonetization::YoutubePostApprovalWorker).to_not receive(:perform_in)

        expect(review_audit.save).to eq(true)
      end
    end

    context "for user who is blocked from Youtube Money via People Flag" do
      it "does not call upon the TrackMonetization::PostApprovalWorker" do
        person = create(:person, :with_yt_tracks)

        album = create(:album, :payment_applied, :with_songs, person: person)

        create(:petri_bundle, album: album)

        review_audit = build(:review_audit, album: album, person: admin_person, event: "APPROVED")

        Person::FlagService
          .flag!(person: person, flag_attributes: Flag::BLOCKED_FROM_YT_MONEY)

        expect(TrackMonetization::PostApprovalWorker).to_not receive(:perform_async)

        expect(review_audit.save).to eq(true)
      end
    end

    context "for users with YTTracks subscriptions" do
      it "calls upon the TrackMonetization::PostApprovalWorker" do
        subscriber = create(:person, :with_yt_tracks)

        album = create(:album, :payment_applied, :with_songs, person: subscriber)

        create(:petri_bundle, album: album)

        review_audit = build(:review_audit, album: album, person: admin_person, event: "APPROVED")

        expect(TrackMonetization::YoutubePostApprovalWorker).to receive(:perform_in)
          .with(instance_of(ActiveSupport::TimeWithZone), album.id, Store::YTSR_STORE_ID)

        expect(review_audit.save).to eq(true)
      end
    end

    context "for users with an active youtube monetization" do
      it "calls upon the TrackMonetization::PostApprovalWorker" do
        subscriber = create(:person)

        Timecop.freeze

        create(:youtube_monetization, effective_date: Time.now, person: subscriber)

        Timecop.travel(Time.now + 3.years)

        album = create(:album, :payment_applied, :with_songs, person: subscriber)

        create(:petri_bundle, album: album)

        review_audit = build(:review_audit, album: album, person: admin_person, event: "APPROVED")

        expect(TrackMonetization::YoutubePostApprovalWorker).to receive(:perform_in)
          .with(instance_of(ActiveSupport::TimeWithZone), album.id, Store::YTSR_STORE_ID)

        expect(review_audit.save).to eq(true)

        Timecop.return
      end
    end

    context "when user has a YTSR proxy salepoint" do
      it "calls the the TrackMonetization::PostApprovalWorker" do
        album = create(:album, :payment_applied, :with_songs)
        create(:petri_bundle, album: album)
        create(:salepoint, salepointable: album, store: Store.find_by(short_name: "YTSRProxy"))

        review_audit = build(:review_audit, album: album, person: admin_person, event: "STARTED_REVIEW")


        expect(TrackMonetization::YoutubePostApprovalWorker).to receive(:perform_in).with(instance_of(ActiveSupport::TimeWithZone), album.id, Store::YTSR_STORE_ID)

        review_audit.update(event: "APPROVED")
      end
    end

    context "when the album has been takendown" do
      let(:album) {  create(:album, :payment_applied, :with_songs) }
      let!(:petri_bundle) { create(:petri_bundle, album: album)}
      let!(:ty_salepoint) { create(:salepoint, salepointable: album, store: Store.find_by(short_name: "YTSRProxy")) }
      let(:review_audit) {  build(:review_audit, album: album, person: admin_person, event: "STARTED_REVIEW")}

      before do
        album.takedown!
      end
      it "does not create tracks" do
        expect(TrackMonetization::YoutubePostApprovalWorker).not_to receive(:perform_in)
        review_audit.update(event: "APPROVED")
      end
    end

    context "with youtube music salepoint" do
      let(:album) {  create(:album, :payment_applied, :with_songs) }
      let!(:petri_bundle) { create(:petri_bundle, album: album)}
      let!(:yt_proxy_salepoint) { create(:salepoint, salepointable: album, store: Store.find_by(short_name: "YTSRProxy")) }
      let!(:ytm_salepoint) { create(:salepoint, salepointable: album, store: Store.find_by(short_name: "Google")) }
      let(:review_audit) {  build(:review_audit, album: album, person: admin_person, event: "STARTED_REVIEW")}

      context "when album and salepoint are live" do
        it "calls the the TrackMonetization::PostApprovalWorker" do
          expect(TrackMonetization::YoutubePostApprovalWorker).to receive(:perform_in).with(instance_of(ActiveSupport::TimeWithZone), album.id, Store::YTSR_STORE_ID)
          review_audit.update(event: "APPROVED")
        end
      end

      context "when album is takendown" do
        before do
          album.takedown!
        end

        it "does not create tracks" do
          expect(TrackMonetization::YoutubePostApprovalWorker).not_to receive(:perform_in)
          review_audit.update(event: "APPROVED")
        end
      end

      context "when youtube_music salepoint is taken down" do
        before do
          ytm_salepoint.takedown!
        end

        it "does not create tracks" do
          expect(TrackMonetization::YoutubePostApprovalWorker).not_to receive(:perform_in)
          review_audit.update(event: "APPROVED")
        end
      end
    end
  end

  describe "#update_you_tube_salepoints" do
    before :each do
      allow(admin_person).to receive(:roles).and_return(double("Role"))
      allow(admin_person).to receive(:has_role?).with("Content Review Manager").and_return(true)

      allow_any_instance_of(ReviewAudit).to receive(:send_sns_notification).and_return(nil)
    end

    it "does not call upon the Ytsr::SalepointWorker" do
      subscriber = create(:person)

      album = create(:album, :payment_applied, :with_songs, person: subscriber)

      create(:petri_bundle, album: album)

      review_audit = build(:review_audit, album: album, person: admin_person, event: "APPROVED")

      expect(Ytsr::SalepointWorker).to_not receive(:perform_async)

      expect(review_audit.save).to eq(true)
    end
  end

  describe "#create_publishing_compositions" do
    before :each do
      allow(admin_person).to receive(:roles).and_return(double("Role"))
      allow(admin_person).to receive(:has_role?).with("Content Review Manager").and_return(true)

      allow_any_instance_of(ReviewAudit).to receive(:send_sns_notification).and_return(nil)
    end

    context "for users who have not opted into publishing" do
      it "does not call upon the PublishingAdministration::PostApprovalWorker" do
        album = create(:album, :payment_applied, :with_songs)

        create(:petri_bundle, album: album)

        review_audit = build(:review_audit, album: album, person: admin_person, event: "APPROVED")

        expect(PublishingAdministration::PostApprovalWorker).to_not receive(:perform_async)

        expect(review_audit.save).to eq(true)
      end
    end

    context "for users who have opted into publishing" do
      it "calls upon the PublishingAdministration::PostApprovalWorker" do
        subscriber = create(:publishing_composer, :with_pub_admin_purchase).person

        album = create(:album, :payment_applied, :with_songs, person: subscriber)

        create(:petri_bundle, album: album)

        review_audit = build(:review_audit, album: album, person: admin_person, event: "APPROVED")

        expect(PublishingAdministration::PostApprovalWorker).to receive(:perform_async).with(album.id)

        expect(review_audit.save).to eq(true)
      end
    end
  end

  describe "#verify_album_transcoding" do
    let(:album) { create(:album, :payment_applied, :with_songs) }

    context "when album is missing duration_in_seconds" do

      before do
        allow(admin_person).to receive(:roles).and_return(double("Role"))
        allow(admin_person).to receive(:has_role?).with("Content Review Manager").and_return(true)
        allow_any_instance_of(ReviewAudit).to receive(:start_distribution).and_return(true)
        album.songs.first.update(s3_flac_asset_id: create(:s3_asset), duration_in_seconds: 100)
      end

      it "notifies Airbrake and calls the transcode_assets on album" do
        expect(Airbrake).to receive(:notify).at_least(:once)
        expect(album).to receive(:transcode_assets)
        create(:review_audit, album: album, event: "APPROVED", person: admin_person)
      end
    end

    context "when album is missing s3_flac_asset_id" do

      before do
        allow(admin_person).to receive(:roles).and_return(double("Role"))
        allow(admin_person).to receive(:has_role?).with("Content Review Manager").and_return(true)
        allow_any_instance_of(ReviewAudit).to receive(:start_distribution).and_return(true)
        album.songs.first.update(s3_flac_asset_id: create(:s3_asset), duration_in_seconds: 100)
      end

      it "notifies Airbrake and calls the transcode_assets on album" do
        expect(Airbrake).to receive(:notify).at_least(:once)
        expect(album).to receive(:transcode_assets)
        create(:review_audit, album: album, event: "APPROVED", person: admin_person)
      end
    end
  end

  describe "#check_tier_eligibility" do
     it "should return a string" do
      album = create(:album,
                     :approved,
                     :with_streaming_distribution,
                     legal_review_state: "APPROVED",
                     style_review_state: "DO NOT REVIEW",
                     finalized_at: Time.now.to_s,
                     is_deleted: false,
                     takedown_at: nil
                    )
      review_audit = create(:review_audit, album: album, person: admin_person, event: "APPROVED")

      expect(review_audit.check_tier_eligibility).to be_an_instance_of(String)
     end

     it "should call RewardSystem::Events" do
      album = create(:album,
                     :approved,
                     :with_streaming_distribution,
                     legal_review_state: "APPROVED",
                     style_review_state: "DO NOT REVIEW",
                     finalized_at: Time.now.to_s,
                     is_deleted: false,
                     takedown_at: nil
                    )
      review_audit = create(:review_audit, :approved, album: album, person: admin_person, event: "APPROVED")

      expect(RewardSystem::Events).to receive(:release_approved).with(album.person.id)
      review_audit.check_tier_eligibility
    end
  end

  describe "#notify_customer" do
    it "should invoke the RejectionEmailService with the review audit" do
      review_audit = build(:review_audit)
      rejection_email_dbl = double(RejectionEmailService, notify: true)

      expect(RejectionEmailService)
        .to receive(:new)
        .with(review_audit)
        .and_return(rejection_email_dbl)

      review_audit.notify_customer
    end
  end

  describe "#notify_distribution_api_services" do
    context "when the review audit's event is ineligible for updating the service" do
      it "should NOT invoke the Bytedance::ApiClientWorker" do
        album = create(:album, :with_petri_bundle, :with_distribution_api_album)
        album.distribution_api_service.update(name: DistributionApiService::BYTEDANCE)
        review_audit = create(:review_audit, album: album, event: ReviewAudit::SKIPPED, person: admin_person)

        expect(Bytedance::ApiClientWorker).not_to receive(:perform_async)

        review_audit.run_callbacks(:commit)
      end
    end

    context "when the review audit's event is eligible for updating the service" do
      it "should invoke the Bytedance::ApiClientWorker" do
        album = create(:album, :with_petri_bundle, :with_distribution_api_album)
        album.distribution_api_service.update(name: DistributionApiService::BYTEDANCE)
        review_audit = create(:review_audit, album: album, event: ReviewAudit::APPROVED, person: admin_person)

        expect(Bytedance::ApiClientWorker)
          .to receive(:perform_async)
          .with(Bytedance::Requests::ContentReview,  review_audit.id, album.id)

        review_audit.run_callbacks(:commit)
      end
    end
  end
end
