require 'rails_helper'

RSpec.describe IRSTransaction, type: :model do

  describe "advance_status" do
    let(:irs_transaction_pending)  { create(:irs_transaction) }
    let(:irs_transaction_reported) { create(:irs_transaction, status: :reported) }
    it "advances from 'pending' to 'reported'" do
      irs_transaction_pending.advance_status!
      irs_transaction_pending.reload
      expect(irs_transaction_pending.reported?).to be true
    end

    it "does not advances from 'reported' to 'completed'" do
      irs_transaction_reported.advance_status!
      irs_transaction_reported.reload
      expect(irs_transaction_reported.completed?).to be false
    end

    it "does not advances from 'reported' to 'completed' if transaction_code missing" do
      irs_transaction_reported.update(status: :completed)
      irs_transaction_reported.reload
      expect(irs_transaction_reported.completed?).to be false
    end

    it "advances from 'reported' to 'completed'" do
      irs_transaction_reported.update(status: :completed, transaction_code: :foo)
      irs_transaction_reported.reload
      expect(irs_transaction_reported.completed?).to be true
    end

    it "updates transacted_at when status moves from 'reported' to 'completed'" do
      irs_transaction_reported.update(status: :completed, transaction_code: :foo)
      irs_transaction_reported.reload
      expect(irs_transaction_reported.transacted_at).to be_truthy
    end
  end
end
