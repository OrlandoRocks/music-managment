require "rails_helper"

RSpec.describe RefundItem, type: :model do
  context "Associations" do
    it { should belong_to(:refund) }
    it { should belong_to(:purchase) }
  end

  context "Validations" do
    it { should validate_presence_of(:purchase) }
    it { should validate_presence_of(:refund) }

    it "validates total_amount_less_than_refundable_amount of the purchase" do
      purchase = create(:purchase, cost_cents: 99)
      refund = create(:refund)

      subject.refund = refund
      subject.purchase = purchase
      subject.base_amount_cents = 100
      subject.tax_amount_cents = 10
      subject.validate

      expect(subject.errors[:base])
        .to include(
          "Requested amount is greater than "\
          "refundable amount for purchase_id #{purchase.id}"
        )

      subject.base_amount_cents = 30
      subject.validate
      expect(subject.errors[:base])
        .not_to include(
          "Requested amount is greater than "\
          "refundable amount for purchase_id #{purchase.id}"
        )
    end
  end
end
