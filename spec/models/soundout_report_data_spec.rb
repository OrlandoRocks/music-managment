require "rails_helper"

describe SoundoutProduct do
  describe "#report_data=" do
    it "should populate fields from the json" do
      json = File.open("#{Rails.root}/spec/fixtures/soundout_report.json", "r").read
      soundout_report = build(:soundout_report, :with_track)
      soundout_report.save

      report_data = SoundoutReportData.new(soundout_report: soundout_report, report_data: json)
      expect(report_data.soundout_report).to eq(soundout_report)
      expect(report_data.market_potential).to eq(65)
      expect(report_data.market_potential_genre).to eq(64)
      expect(report_data.track_rating).to eq(6.8)
      expect(report_data.passion_rating).to eq(3.4)
      expect(report_data.in_genre_class).to eq("good")
    end

    it "should populate the URL field when a URL is present" do
      json_with_url = File.open("#{Rails.root}/spec/fixtures/soundout_report_with_url.json", "r").read
      report_with_url = create(:soundout_report, :with_track)
      report_with_url.save
      report_data_with_url = SoundoutReportData.new(soundout_report: report_with_url, report_data: json_with_url)

      expect(report_data_with_url.soundout_report).to eq(report_with_url)
      expect(report_data_with_url.url).to eq(JSON.parse(json_with_url)["status"]["url"])
    end

    it "should not raise an error when the URL is not present" do
      json = File.open("#{Rails.root}/spec/fixtures/soundout_report.json", "r").read
      soundout_report = create(:soundout_report, :with_track)
      soundout_report.save
      report_data = SoundoutReportData.new(soundout_report: soundout_report, report_data: json)
      expect(report_data.url).to be_nil
    end

    it "should save json without a data section" do
      json = { reviews: [] }.to_json
      report_data = SoundoutReportData.new(report_data: json)
      expect(report_data.report_data).to eq(json)
      expect(report_data.market_potential).to be_nil
      expect(report_data.market_potential_genre).to be_nil
      expect(report_data.track_rating).to be_nil
      expect(report_data.in_genre_class).to be_nil
    end

    it "should save invalid json but not pull out information" do
      json = "{ data: }"
      report_data = SoundoutReportData.new(report_data: json)
      expect(report_data.report_data).to eq(json)
      expect(report_data.market_potential).to be_nil
      expect(report_data.market_potential_genre).to be_nil
      expect(report_data.track_rating).to be_nil
      expect(report_data.in_genre_class).to be_nil
    end
  end

  describe "data extraction" do
    before(:each) do
      json = File.open("#{Rails.root}/spec/fixtures/soundout_report.json", "r").read
      @report_data = SoundoutReportData.new(report_data: json)
    end

    it "should calculate male sample group percentage" do
      expect(@report_data.sample_group_male_percent.to_i).to eq(46)
    end

    it "should calculate female sample group percentage" do
      expect(@report_data.sample_group_female_percent.to_i).to eq(53)
    end

    it "should pull the track appeal preference" do
      expect(@report_data.track_appeal_preference).to eq("0.301")
    end

    it "should pull the track appeal no preference" do
      expect(@report_data.track_appeal_no_preference).to eq("0.410")
    end

    it "should enumerate the sample age groups" do
      expect(@report_data.sample_age_groups).to be_kind_of(Enumerator)

      @report_data.sample_age_groups.each do |age, percent|
        case age
        when "16-24"
          expect(percent.to_i).to eq(60)
        when "25-34"
          expect(percent.to_i).to eq(27)
        when "35-44"
          expect(percent.to_i).to eq(12)
        end
      end
    end

    it "should enumerate the market pot groups" do
      expect(@report_data.market_pot_age_groups).to be_kind_of(Enumerator)

      @report_data.market_pot_age_groups.each do |age, pot|
        case age
        when "16-24"
          expect(pot.to_i).to eq(62)
        when "25-34"
          expect(pot.to_i).to eq(52)
        when "35-44"
          expect(pot.to_i).to eq(80)
        end
      end
    end

    it "should enumerate the market pot groups" do
      expect(@report_data.market_pot_age_genre_groups).to be_kind_of(Enumerator)

      @report_data.market_pot_age_genre_groups.each do |age, pot|
        case age
        when "16-24"
          expect(pot.to_i).to eq(64)
        when "25-34"
          expect(pot.to_i).to eq(51)
        when "35-44"
          expect(pot.to_i).to eq(78)
        end
      end
    end

    it "should pull artists mentioned" do
      expect(@report_data.artists_mentioned).to be_kind_of(Enumerator)

      @report_data.artists_mentioned.each do |artist, mentions|
        case artist
        when "Nirvana"
          expect(mentions.to_i).to eq(4)
        end
      end
    end

    it "handles when there are no artist mentions on a premium report" do
      json = File.open("#{Rails.root}/spec/fixtures/soundout_report_missing_mentions.json", "r").read
      @report_data = SoundoutReportData.new(report_data: json)

      expect(@report_data.artists_mentioned).to be_kind_of(Enumerator)

      @report_data.artists_mentioned.each do |artist, mentions|
        case artist
        when "Nirvana"
          expect(mentions.to_i).to eq(0)
        end
      end
    end

    it "should normalize rating distribution data to an array" do
      rating = @report_data.rating_distribution_data
      expect(rating[0]).to eq("0.0")
      expect(rating[1]).to eq("16.0")
    end

    it "should normalize song analysis into a hash" do
      data = @report_data.song_analysis
      expect(data["artist"]).to eq("52")
    end

    it "should normalize word cloud into a hash" do
      data = @report_data.word_cloud
      expect(data["absolutely"]).to eq("4")
    end
  end

  describe "search_and_filter" do
    before(:each) do
      json = File.open("#{Rails.root}/spec/fixtures/soundout_report.json", "r").read
      @report_data = SoundoutReportData.new(report_data: json)
    end

    it "should filter by rating" do
      reviews, total_count = @report_data.search_and_filter_reviews(rating_gte: 10, rating_lte: 10)
      expect(reviews).not_to be_empty
      reviews.each { |r| expect(r[SoundoutReportData::RATING].to_i).to eq(10) }
    end

    it "should filter by age" do
      reviews, total_count = @report_data.search_and_filter_reviews(age_gte: 20, age_lte: 25)
      expect(reviews).not_to be_empty
      reviews.each do |r|
        expect(r[SoundoutReportData::AGE].to_i).to be >= 20
        expect(r[SoundoutReportData::AGE].to_i).to be <= 25
      end
    end

    it "should return reviews when filtering by gender on a report without gender information" do
      json = { reviews:  { :"1" => { comment: "Hi", age: "12" } } }
      @report_data.report_data = json.to_json

      reviews, total_count = @report_data.search_and_filter_reviews(gender: "Male")
      expect(total_count).to eq(json[:reviews].size)
    end

    it "should return reviews when filtering by age on a report without age information" do
      json = { reviews:  { :"1" => { comment: "Hi" } } }
      @report_data.report_data = json.to_json

      reviews, total_count = @report_data.search_and_filter_reviews(age_gte: "10", age_lte: "20")
      expect(total_count).to eq(json[:reviews].size)
    end

    it "should filter by MALE" do
      reviews, total_count = @report_data.search_and_filter_reviews(gender: "Male")
      expect(reviews).not_to be_empty
      reviews.each do |r|
        expect(r[SoundoutReportData::GENDER].to_i).to eq(1)
      end
    end

    it "should filter by FEMALE" do
      reviews, total_count = @report_data.search_and_filter_reviews(gender: "Female")
      expect(reviews).not_to be_empty
      reviews.each do |r|
        expect(r[SoundoutReportData::GENDER].to_i).to eq(2)
      end
    end

    it "should sort by rating ascending" do
      reviews, total_count = @report_data.search_and_filter_reviews(order: "Rating ASC")
      expect(reviews).not_to be_empty
      last_rating = -1
      reviews.each do |r|
        expect(r[SoundoutReportData::RATING].to_i).to be >= last_rating
        last_rating = r[SoundoutReportData::RATING].to_i
      end
    end

    it "should sort by rating descending" do
      reviews, total_count = @report_data.search_and_filter_reviews(order: "Rating DESC")
      expect(reviews).not_to be_empty
      last_rating = 100
      reviews.each do |r|
        expect(r[SoundoutReportData::RATING].to_i).to be <= last_rating
        last_rating = r[SoundoutReportData::RATING].to_i
      end
    end

    it "should sort by age ascending" do
      reviews, total_count = @report_data.search_and_filter_reviews(order: "Age ASC")
      expect(reviews).not_to be_empty
      last_age = 0
      reviews.each do |r|
        expect(r[SoundoutReportData::AGE].to_i).to be >= last_age
        last_age = r[SoundoutReportData::AGE].to_i
      end
    end

    it "should sort by age descending" do
      reviews, total_count = @report_data.search_and_filter_reviews(order: "Age DESC")
      expect(reviews).not_to be_empty
      last_age = 200
      reviews.each do |r|
        expect(r[SoundoutReportData::AGE].to_i).to be <= last_age
        last_age = r[SoundoutReportData::AGE].to_i
      end
    end

    it "should sort by gender ascending" do
      reviews, total_count = @report_data.search_and_filter_reviews(order: "Gender ASC")
      expect(reviews).not_to be_empty
      last_gender = 1
      reviews.each do |r|
        expect(r[SoundoutReportData::GENDER].to_i).to be >= last_gender
        last_gender = r[SoundoutReportData::GENDER].to_i
      end
    end

    it "should sort by gender desc" do
      reviews, total_count = @report_data.search_and_filter_reviews(order: "Gender DESC")
      expect(reviews).not_to be_empty
      last_gender = 2
      reviews.each do |r|
        expect(r[SoundoutReportData::GENDER].to_i).to be <= last_gender
        last_gender = r[SoundoutReportData::GENDER].to_i
      end
    end

    it "should search non case sensitive" do
      reviews, total_count = @report_data.search_and_filter_reviews(search: "INTERESTING SONG")
      expect(reviews).not_to be_empty
      reviews.each do |r|
        expect(r[SoundoutReportData::COMMENT].include?("interesting song")).to be_truthy
      end
    end

    it "should page" do
      page1, total_count = @report_data.search_and_filter_reviews(page: 1, per_page: 1)
      expect(page1.size).to eq(1)

      page2, total_count = @report_data.search_and_filter_reviews(page: 2, per_page: 1)
      expect(page2.size).to eq(1)

      expect(page1.first[SoundoutReportData::COMMENT]).not_to eq(page2.first[SoundoutReportData::COMMENT])

      size2_page, total_count = @report_data.search_and_filter_reviews(page: 1, per_page: 2)
      expect(size2_page.size).to eq(2)
      expect(page1.first[SoundoutReportData::COMMENT]).to eq(size2_page.first[SoundoutReportData::COMMENT])
      expect(page2.first[SoundoutReportData::COMMENT]).to eq(size2_page.last[SoundoutReportData::COMMENT])
    end
  end
end
