require "rails_helper"

describe SalepointableStore do

  before(:each) do
    @store = TCFactory.create(:store)
  end

  it "should provide all stores set for a salepointable type of Album" do
    SalepointableStore.create(
      :store => @store,
      :salepointable_type => "Album"
    )
    expect(Album.stores).to eq([@store])
  end

  it "should provide all stores set for a salepointable type Ringtone, a sub-class of Album" do
    SalepointableStore.create(
      :store => @store,
      :salepointable_type => "Ringtone"
    )
    expect(Ringtone.stores).to eq([@store])
  end

  it "should provide an empty list for a salepointable type of Ringtone if stores are added to the superclass Album" do
    SalepointableStore.create(
      :store => @store,
      :salepointable_type => "Album"
    )
    expect(Ringtone.stores).to eq([])
  end

end
