require "rails_helper"

describe BatchBalanceAdjustmentDetail do
  before(:each) do
    @batch_balance_adjustment_details = TCFactory.build(:batch_balance_adjustment_detail)
  end

  describe "#payment_held?" do
    it "returns true for 'Y' or 'Yes'" do
      @batch_balance_adjustment_details.update_attribute(:hold_payment, 'Y')
      expect(@batch_balance_adjustment_details.payment_held?).to be_truthy
      @batch_balance_adjustment_details.update_attribute(:hold_payment, 'Yes')
      expect(@batch_balance_adjustment_details.payment_held?).to be_truthy
    end

    it "returns false for everything else" do
      @batch_balance_adjustment_details.update_attribute(:hold_payment, 'N')
      expect(@batch_balance_adjustment_details.payment_held?).to be(false)
      @batch_balance_adjustment_details.update_attribute(:hold_payment, 'Nada')
      expect(@batch_balance_adjustment_details.payment_held?).to be(false)
    end
  end
end
