require "rails_helper"

describe SalepointSubscription do
  describe "finalized?" do
    before(:each) do
      @person = TCFactory.create(:person)
    end

    it "should properly determine if finalized" do
      album = TCFactory.create(:album,
                               person: @person)
      sp_sub = TCFactory.create(:salepoint_subscription,
                                album: album)
      expect(sp_sub.valid?).to eq(true)
      expect(sp_sub.finalized?).to eq(false)

      sp_sub.finalized_at = Date.today
      expect(sp_sub.finalized?).to eq(true)
    end
  end

  describe "type of album" do
    before(:each) do
      @person = TCFactory.create(:person)
    end

    it "should detect valid album types" do
      album = TCFactory.create(:album,
                               person: @person,
                               album_type: "Album")

      sp_sub = TCFactory.build(:salepoint_subscription, album: album)
      expect(sp_sub.valid?).to eq(true)

      sp_sub.album.album_type = "Single"
      expect(sp_sub.valid?).to eq(true)

      sp_sub.album.album_type = "Ringtone"
      expect(sp_sub.valid?).to eq(false)
    end
  end

  describe "is_active=" do
    before(:each) do
      @person = TCFactory.create(:person)
      @album = TCFactory.create(:album, person: @person, album_type: "Album")
    end
    it "should update effective when setting to true" do
      sp_sub = TCFactory.create(:salepoint_subscription, album: @album, is_active: false)
      allow(sp_sub).to receive(:finalized?).and_return(true)

      sp_sub.is_active = true

      expect(sp_sub.effective_changed?).to eq(true)
      expect(sp_sub.save).to eq(true)
    end

    it "should not update effective when setting to false" do
      sp_sub = TCFactory.create(:salepoint_subscription, album: @album, is_active: true)
      allow(sp_sub).to receive(:finalized?).and_return(true)

      sp_sub.is_active = false

      expect(sp_sub.effective_changed?).to eq(false)
      expect(sp_sub.save).to eq(true)
    end

    it "should only update effective is status is changing" do
      sp_sub = TCFactory.create(:salepoint_subscription, album: @album, is_active: true)
      allow(sp_sub).to receive(:finalized?).and_return(true)

      sp_sub.is_active = true
      expect(sp_sub.effective_changed?).to eq(false)

      sp_sub.is_active = false
      expect(sp_sub.effective_changed?).to eq(false)

      sp_sub.is_active = true
      expect(sp_sub.effective_changed?).to eq(true)

      expect(sp_sub.save).to eq(true)
    end
  end

  describe "finalize!" do
    before(:each) do
      person = TCFactory.create(:person)
      album = TCFactory.create(:album, person: person, album_type: "Album")
      @sp_sub = TCFactory.create(:salepoint_subscription, album: album, is_active: true)
    end

    it "should update finalized_at" do
      expect(@sp_sub.finalized_at.blank?).to eq(true)

      @sp_sub.finalize!
      expect(@sp_sub.finalized_at.blank?).to eq(false)
    end

    it "should update effective if status is active" do
      expect(@sp_sub.is_active).to eq(true)
      expect(@sp_sub.finalized_at.blank?).to eq(true)
      expect(@sp_sub.effective.blank?).to eq(true)

      @sp_sub.finalize!
      expect(@sp_sub.effective.blank?).to eq(false)
    end

    it "should not update effective if status is inactive" do
      @sp_sub.is_active = false
      expect(@sp_sub.finalized_at.blank?).to eq(true)
      expect(@sp_sub.effective.blank?).to eq(true)

      @sp_sub.finalize!
      expect(@sp_sub.effective.blank?).to eq(true)
    end

    it "should persist changes to db" do
      expect(@sp_sub.finalized_at.blank?).to eq(true)
      expect(@sp_sub.effective.blank?).to eq(true)

      @sp_sub.finalize!
      @sp_sub.reload

      expect(@sp_sub.finalized_at.blank?).to eq(false)
      expect(@sp_sub.effective.blank?).to eq(false)
    end
  end

  describe "can_distribute?" do
    context "when album is finalized" do
      it "returns true" do
        album = create(:album, :finalized, :purchaseable)
        sp_sub = create(:salepoint_subscription, is_active: true, album: album)

        result = sp_sub.can_distribute?

        expect(result).to eq(true)
      end
    end

    context "when album is not finalized" do
      it "returns false" do
        album = create(:album, :purchaseable)
        sp_sub = create(:salepoint_subscription, is_active: true, album: album)

        result = sp_sub.can_distribute?

        expect(result).to eq(false)
      end
    end

    context "purchase is unpaid" do
      it "returns true" do
        person = create(:person)
        album = create(:album, :purchaseable, person: person)
        sp_sub = create(:salepoint_subscription, is_active: true, album: album)
        product = create(:product)
        create(:purchase, product: product, related: album, paid_at: nil, person: person)

        result = sp_sub.can_distribute?

        expect(result).to eq(true)
      end
    end

    context "purchase is paid" do
      it "returns false" do
        person = create(:person)
        album = create(:album, :purchaseable, person: person)
        sp_sub = create(:salepoint_subscription, is_active: true, album: album)
        product = create(:product)
        create(:purchase, product: product, related: album, person: person)

        result = sp_sub.can_distribute?

        expect(result).to eq(false)
      end
    end
  end

  describe "destroy" do
    before(:each) do
      @person = TCFactory.create(:person)
      @album = TCFactory.create(:album, person: @person, album_type: "Album")
      @sp_sub = TCFactory.create(:salepoint_subscription, album: @album, is_active: true)
    end

    it "should not destroy finalized subscriptions" do
      @sp_sub.finalize!
      expect { @sp_sub.destroy }.to raise_error(SalepointSubscription::DestroyAfterUsed, "Cannot delete a purchased salepoint subscription")
    end

    it "should remove related purchase record" do
      TCFactory.build_ad_hoc_salepoint_subscription_product(10.00)
      Product.add_to_cart(@person, @sp_sub)

      expect do
        expect do
          @sp_sub.destroy
        end.to change(Purchase, :count).by(-1)
      end.to change(SalepointSubscription, :count).by(-1)
    end
  end

  describe "toggle_active!" do
    let!(:person) { create(:person) }
    let!(:person_plan) { create(:person_plan, person: person, plan: Plan.last) }
    let!(:album) { create(:album, person: person) }
    let!(:salepoint_subscription) { create(:salepoint_subscription, album: album, is_active: true) }

    it "should toggle finalized subscriptions for valid Plan users" do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
      salepoint_subscription.finalize!

      expect(salepoint_subscription.toggle_active!).to be true
      expect(salepoint_subscription.is_active).to be false
    end

    it "should not toggle unfinalized subscriptions" do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

      expect(salepoint_subscription.toggle_active!).to be false
      expect(salepoint_subscription.is_active).to be true
    end

    it "should not toggle subscriptions for invalid Plan users" do
      person.person_plan.update!(plan: Plan.first)
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

      expect(salepoint_subscription.toggle_active!).to be false
      expect(salepoint_subscription.is_active).to be true
    end

    it "should not toggle subscriptions for non-Plan users" do
      person.person_plan.destroy!
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

      expect(salepoint_subscription.toggle_active!).to be false
      expect(salepoint_subscription.is_active).to be true
    end
  end

  describe "self.create_batch_subscriptions" do
    before(:each) do
      @person = TCFactory.create(:person)

      @album1 = TCFactory.create(:album, person: @person)
      @album2 = TCFactory.create(:album, person: @person)
      @album3 = TCFactory.create(:album, person: @person)
    end

    it "should return an empty array if no albums are supplied" do
      expect(SalepointSubscription.create_batch_subscriptions([]).blank?).to eq(true)
    end

    it "should create new salepoint subscriptions for albums supplied" do
      ids = []

      expect do
        ids = SalepointSubscription.create_batch_subscriptions([@album1, @album2, @album3])
      end.to change(SalepointSubscription, :count).by(3)

      results = SalepointSubscription.order("id desc").limit(3)
      results.each do |result|
        expect(result.is_active).to eq(true)
        expect(result.effective.blank?).to eq(true)
        expect(result.finalized_at.blank?).to eq(true)
      end

      [@album1, @album2, @album3].each do |album|
        expect(results.any? { |r| r.album_id == album.id }).to eq(true)
        expect(ids.any? { |i| i == album.id }).to eq(true)
      end
    end
  end

  describe "add_to_subscription" do
    it "uses the correct inventory" do
      @person = TCFactory.create(:person)
      TCFactory.generate_album_purchase(@person)

      @album = TCFactory.create(:album, person: @person, album_type: "Album")
      create(:petri_bundle, album_id: @album.id)
      TCFactory.build_ad_hoc_salepoint_subscription_product(10.00)
      @sp_sub = TCFactory.create(
        :salepoint_subscription,
        album: @album,
        is_active: true,
        effective: Time.now
      )
      invoice = Invoice.factory(@person, Product.add_to_cart(@person, @sp_sub))
      Purchase.process_purchases_for_invoice(invoice)

      @new_store = TCFactory.build_store(launched_at: Time.now + 3.days)
      FactoryBot.create(:salepointable_store, store: @new_store)

      inventory_usages = @sp_sub.add_to_subscription([@new_store])

      expect(inventory_usages.first.inventory.title).to eq('Store Automator')
    end
  end
end
