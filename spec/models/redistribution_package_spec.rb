require "rails_helper"

describe RedistributionPackage do
  before(:each) do
    @person      = FactoryBot.create(:person)
    @artist      = FactoryBot.create(:artist)
    album        = FactoryBot.create(:album)
    creative     = FactoryBot.create(:creative, artist_id: @artist.id, creativeable_id: album.id, creativeable_type: 'Album').update(person_id: @person.id)
    petri_bundle = FactoryBot.build(:petri_bundle, album_id: album.id)
    petri_bundle.save
    @distro1 = FactoryBot.create(:distribution, converter_class: "MusicStores::Itunes::Converter", petri_bundle_id: petri_bundle.id)
    @distro2 = FactoryBot.create(:distribution, converter_class: "MusicStores::Itunes::Converter", petri_bundle_id: petri_bundle.id)
    @package = RedistributionPackage.create(person_id: @person.id, artist_id: @artist.id, converter_class: "MusicStores::Itunes::Converter")
  end

  describe ".for" do
    it "returns the redistribution package for the given person, artist and store" do
      expect(RedistributionPackage.for(@person.id, @artist.id, "apple").id).to eq(@package.id)
    end
  end

  describe "#distributions" do
    it "returns the distributions package's person, artist and store" do
      expect(@package.distributions.size).to eq(2)
    end
  end

end
