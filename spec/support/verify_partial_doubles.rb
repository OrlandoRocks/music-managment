RSpec.configure do |config|
  config.around(:each) do |example|
    if example.metadata[:disable_verify_partial_doubles]
      original_setting = false

      config.mock_with :rspec do |mocks|
        original_setting = mocks.verify_partial_doubles?
        mocks.verify_partial_doubles = false
      end

      example.run

      config.mock_with :rspec do |mocks|
        mocks.verify_partial_doubles = original_setting
      end
    else
      example.run
    end
  end
end
