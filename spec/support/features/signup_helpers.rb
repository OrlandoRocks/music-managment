module Features
  module SignupHelpers
    def get_user(user, _opts = {})
      Person.find_by(email: user.email)
    end

    def load_edit_page(user)
      user = get_user user
      visit edit_person_path(user.id)
    end

    def log_user_in(user)
      fill_in "Email", with: user.email
      fill_in "Password", with: user.password
      click_button "Log In"
    end

    def logout_user
      visit "/logout"
    end

    def login_user_js(logged_in_user)
      # NOTE: Add 'test.tunecore.com' to your /etc/hosts
      create_new_account(logged_in_user)
    end

    def login_user(user, password)
      visit login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
      fill_in "Email", with: user.email
      fill_in "Password", with: password
      click_button "Log In"
    end

    def create_new_account(user, opts = {})
      password_confirmation = opts.fetch(:password_confirmation, user.password)
      check_terms = opts.fetch(:check_terms, true)

      visit "/signup"

      fill_in "Name", with: user.name
      fill_in "Email", with: user.email
      fill_in "person[password]", with: user.password
      fill_in "person[password_confirmation]", with: password_confirmation

      check "person[accepted_terms_and_conditions]" if check_terms

      click_button "Create My Free Account"
    end

    def set_browser_cookie(name, data)
      headers = {}
      Rack::Utils.set_cookie_header!(headers, name, data.to_json)
      cookie_string = headers["Set-Cookie"]
      Capybara.current_session.driver.browser.set_cookie(cookie_string)
    end

    def set_current_user(person)
      allow(Person).to receive(:authenticate).and_return(person)

      params = {
        person: {
          email: person.email,
          password: person.password
        }
      }

      post "/session", params: params
    end
  end
end

RSpec.configure do |config|
  config.include Features::SignupHelpers, type: :feature
  config.include Features::SignupHelpers, type: :request
end
