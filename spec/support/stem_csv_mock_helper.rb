module StemCSVMockHelper
  STEM_MOCK_CSV_ROW_HASH = {
    stem_account_email_address: "teststemuser@gmail.com",
    tunecore_optin_email_address: "teststemuser@gmail.com",
    album_title: "Test Album",
    album_artist: "Test Artist 10",
    album_role: "primary",
    apple_artist_id: nil,
    spotify_artist_id: nil,
    label: "Rolling Stones Small",
    genre: "Rock",
    upc: "123456789012",
    original_release: "05/21/2019",
    album_language: "en",
    release: "07/29/2019",
    territory: "IN:US",
    reviewed_at: nil,
    track_title: "Track 01",
    sequence: "1",
    duration: "3:30",
    lyric: "Lorem Ipsum",
    track_artist: "Test Artist 10",
    track_role: "primary",
    isrc: "AB12W1234567",
    track_language: "en",
    clean_version: "true",
    songwriter: nil,
    publishing: nil,
    cline: nil,
    variable_pricing: "front",
    various_artist: nil
  }

  def stem_mock_csv_row(row={})
    STEM_MOCK_CSV_ROW_HASH.merge(row).values.join(",")
  end

  def stem_mock_csv_io(rows)
    io = StringIO.new
    io.puts "#{STEM_MOCK_CSV_ROW_HASH.keys.join(",")}"
    rows.each { |r| io.puts r }
    io.rewind
    io
  end
end
