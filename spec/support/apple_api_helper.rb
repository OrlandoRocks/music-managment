module AppleApiHelper
  def fake_apple_api(attributes)
    status      = attributes[:status] || 200
    body        = attributes[:body] || {}
    method_call = attributes[:method_call] || nil
    response    = double({status: status, body: body.to_json})
    allow($apple_client).to receive(method_call).and_return(response)
  end
end
