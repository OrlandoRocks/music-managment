require File.expand_path(File.join(File.dirname(__FILE__),'..','tcfactory'))

def errors(instance)
  instance.errors.full_messages.join(", ")
end

def login_as(user)
  user_object = \
    case user
    when Person
      user
    when Symbol
      people(user.to_sym)
    when String
      Person.find_by(email: user)
    end

  controller.send(:current_user=, user_object)
end

def stub_association!(association_name, methods_to_be_stubbed = {})
  mock_association = Spec::Mocks::Mock.new(association_name.to_s)
  methods_to_be_stubbed.each do |method, return_value|
    allow(mock_association).to receive(method).and_return(return_value)
  end
  allow(self).to receive(association_name).and_return(mock_association)
end
