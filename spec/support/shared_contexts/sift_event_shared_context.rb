shared_context "sift_event_shared_context" do
  let(:request) {
    double(
      env: {
        "X-Sift_Science_Signature": "",
        "HTTP_USER_AGENT": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Safari/605.1.15",
        "HTTP_ACCEPT_LANGUAGE": "en-us"
      }.with_indifferent_access,
      host: "staging.tunecore.com"
    )
  }

  let(:cookies) {
    {
      "tc-lang": "en-us",
      "tc-sift-session-id": "ABCDEFG"
    }.with_indifferent_access
  }

  let(:person) { create(:person, name: "Test User") }
  let(:person_with_releases) { create(:person, :with_three_delivered_distros, name: "Test User 2") }
  let(:person_with_payment_methods) { create(:person, :with_stored_credit_card, :with_stored_paypal_account) }
  let(:person_with_payu) { create(:person, :with_india_country) }

  let(:credit_card) { create(:stored_credit_card) }

  let(:invoice){ FactoryBot.create(:invoice, person: person_with_payment_methods) }

  let(:purchase_1) {
    create(:purchase, invoice: invoice)
  }

  let(:purchase_2) {
    create(:purchase, invoice: invoice, related_type: "Salepoint", related_id: 2)
  }

  let(:purchases) {
    Purchase.where(invoice: purchase_1.invoice)
  }

  let(:finalize_form) {
    double(
      :person => person_with_payment_methods,
      :use_balance => true,
      :payment_id => person_with_payment_methods.stored_credit_cards[0].id,
      :purchases => purchases
    )
  }
  
  let(:valid_payment_method_json) {
    {
      "$payment_methods": [
        {
          "$payment_type": "$credit_card",
          "card_type": "Mastercard",
          "$card_last4": "4444"
        },{
          "$payment_type": "$credit_card",
          "card_type": "Mastercard",
          "$card_last4": "2662"
        }
      ]
    }
  }

  let(:valid_billing_address_json) {
    {
      "$billing_address": {
        "$name": "Sureka Priya",
        "$address_1": "jkhkjh",
        "$address_2": "kjhkj",
        "$city": "hkjhkj",
        "$region": "HI",
        "$country": "US",
        "$zipcode": "8978"
      }
    }
  }

  let(:valid_purchase_group_json) {
    [
      {
        "$item_id"=>"1", 
        "$product_title"=>"Yearly Album Renewal", 
        "$price"=>9990000, 
        "$currency_code"=>"USD", 
        "$quantity"=>1
      }
    ]
  }

  let(:valid_payment_method_balance) {
    [
      :$payment_type
    ]
  }

  let(:valid_payment_method_credit_card) {
    [
      :$payment_type,
      :$payment_gateway,
      :$card_last4,
      :card_type
    ]
  }

  let(:valid_payment_method_paypal) {
    [
      :$payment_type,
      :$payment_gateway,
      :$paypal_payer_email
    ]
  }

  let(:valid_payment_method_payu) {
    [
      :$payment_type,
      :$payment_gateway
    ]
  }

  let(:valid_payment_method_adyen) {
    [
      :$payment_type,
      :$payment_gateway
    ]
  }

  let(:valid_billing_address) {
    [
      :$name,
      :$phone,
      :$address_1,
      :$address_2,
      :$city,
      :$region,
      :$country,
      :$zipcode
    ]
  }

  let(:valid_browser) {
    [
      :$user_agent,
      :$accept_language,
      :$content_language
    ]
  }

  let(:valid_create_account) {
    [
      :$user_id,
      :$user_email,
      :$name
    ]
  }

  let(:valid_update_account_password) {
    [
      :$changed_password
    ]
  }

  let(:valid_update_account_email) {
    [
      :$user_email
    ]
  }

  let(:valid_update_account_contact_info) {
    [
      :$name,
      :$phone,
      :$billing_address,
      :mobile_phone
    ]
  }

  let(:valid_update_account_payment_methods) {
    [
      :$payment_methods
    ]
  }

  let(:valid_create_order) {
    [
      :$user_id,
      :$order_id,
      :$payment_methods,
      :$amount,
      :$items
    ]
  }

  let(:valid_order_status) {
    [
      :$user_id,
      :$order_id,
      :$order_status
    ]
  }

  let(:valid_transaction) {
    [
      :$user_id,
      :$amount,
      :$currency_code,
      :$user_email,
      :$order_id,
      :$transaction_type,
      :$transaction_status,
      :$payment_method
    ]
  }

  def assert_key_presence(json, expected_keys, should_be_present = true)
    expect(json).to_not eq({})

    json_keys = json.symbolize_keys.keys
    unmatched_keys = expected_keys - json_keys
    expect(unmatched_keys).to eq([]) if should_be_present
    expect(unmatched_keys).to_not eq([]) if !should_be_present
  end

  before(:each) do
    allow(FeatureFlipper).to receive(:show_feature?).with(:crt_cache, nil).and_return(false)
    allow(FeatureFlipper).to receive(:show_feature?).with(:sift_webhooks).and_return(true)
    allow(FeatureFlipper).to receive(:show_feature?).with(:sift_decisions).and_return(true)
    allow(request).to receive(:[]).with(:action).and_return("Test")
  end
end
