shared_context "sift_webhook_shared_context" do
  let(:request) {
    ActionController::TestRequest.new({
      "X-Sift_Science_Signature": ""
    }, {}, nil)
  }

  let(:person) { create(:person, name: "Test User") }
  let(:person_with_releases) { create(:person, :with_three_delivered_distros, name: "Test User 2") }

  let(:sift_webhook_body_with_releases) {
    {
      "entity" => {
        "id" => "#{person_with_releases.id}",
        "type" => "user"
      },
      "decision" => {
        "id" => "spec_bad_decision"
      },
      "time"=>1654101971532
    }
  }

  let(:sift_webhook_body) {
    {
      "entity" => {
        "id" => person.id.to_s,
        "type" => "user"
      },
      "decision" => {
        "id" => "spec_bad_decision"
      },
      "time"=>1654101971532
    }
  }
  let(:note_options) {
    {
      "related_id"=>person.id.to_s,
      "related_type"=>"Person",
      "ip_address"=>"127.0.0.1",
      "note"=>"Sift Automated Decision Marked as Suspicious".freeze,
      "subject"=>"Sift Identified - Will Not Distribute".freeze,
      "note_created_by_id"=>0
    }
  }
  let(:note) {
    create(
      :note,
      note_options
    )
  }
  let(:valid_serializer_response) {
    {
      "body"=>sift_webhook_body,
      "decision"=>"spec_bad_decision",
      "releases_active"=>0,
      "note_options"=>note_options,
      "person_id"=>person.id.to_s,
      "remote_ip"=>"127.0.0.1"
    }
  }

  let(:valid_serializer_json) {
    {
      "body"=>sift_webhook_body,
      "decision"=>"spec_bad_decision",
      "releases_active"=>0,
      "note_options"=>note_options,
      "person_id"=>person.id.to_s,
      "remote_ip"=>"127.0.0.1"
    }
  }

  before(:each) do
    allow_any_instance_of(ActionController::TestRequest).to receive(:body).and_return(StringIO.new(sift_webhook_body.to_json))
    allow_any_instance_of(ActionController::TestRequest).to receive(:remote_ip).and_return("127.0.0.1")
    allow(Note).to receive(:create).with(note_options).and_return(note)

    allow(FeatureFlipper).to receive(:show_feature?).with(:sift_webhooks).and_return(true)
    allow(FeatureFlipper).to receive(:show_feature?).with(:sift_decisions, person).and_return(true)
    allow(FeatureFlipper).to receive(:show_feature?).with(:sift_decisions, nil).and_return(true)
    allow(FeatureFlipper).to receive(:show_feature?).with(:crt_cache, nil).and_return(false)
  end
end
