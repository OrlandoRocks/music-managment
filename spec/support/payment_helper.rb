module PaymentHelper
  # Mocks out a query to braintree.  Whenever you see
  # warning: peer certificate won't be verified in this SSL session when coding a spec test
  # its a good indicator that you are querying braintree.
  def mock_braintree_query
    braintree_query = double("BrainTreeQuery")
    query_result = {
      "customer_vault" => {
        "customer" => {
          "cc_number" => "1234",
          "cc_exp" => "#{(Date.today + 1.year).strftime("%m%y")}",
          "company" => "hi",
          "first_name" => "tunecore",
          "last_name" => "master",
          "address_1" => "9 Imaginary Lane",
          "city" => "Brooklyn","state" => "NY",
          "country" => "US",
          "postal_code" => "wow",
          "check_aba" => "1234",
          "check_account" => "1234",
        }
      }
    }
    allow(braintree_query).to receive(:execute).and_return(query_result)
    allow(Tunecore::Braintree::Query).to receive(:new).and_return(braintree_query)

    braintree_gateway = double('braintree_gw')
    response = double('response')
    allow(ActiveMerchant::Billing::BraintreeGateway).to receive(:new).and_return(braintree_gateway)
    allow(braintree_gateway).to receive(:delete).and_return(response)
    allow(response).to receive(:success?).and_return(true)
    braintree_vault_txn = double('braintree_vault_txn')
    allow(BraintreeVaultTransaction).to receive(:log_deletion).and_return(braintree_vault_txn)
  end

  def mock_braintree_blue_purchase(processor_response_code = "10")
    transaction = double(
      "braintree blue transaction",
      id: 99999,
      processor_response_code: processor_response_code,
      processor_response_text: "processor_response_text",
      processor_response_type: "processor_response_type",
      additional_processor_response: "additional_processor_response",
      network_response_text: "network_response_text",
      avs_error_response_code: "X",
      avs_postal_code_response_code: "Y",
      avs_street_address_response_code: "D",
      cvv_response_code: "M",
    )
    response = double("braintree blue response", transaction: transaction)
    allow_any_instance_of(Braintree::TransactionGateway).to receive(:sale).and_return(response)
  end

  def mock_braintree_purchase
    gateway = double('braintree gateway')
    response = double('braintree response')
    params = {
      "transactionid" => 1234567890,
      "response_code" => 100,
      "currency" => "USD",
      "processor_id" => 1234567,
    }
    allow(response).to receive(:params).and_return(params)
    allow(response).to receive(:avs_result).and_return({ "code" => 99999 })
    allow(response).to receive(:cvv_result).and_return({ "code" => 99999 })
    allow(gateway).to receive(:purchase).and_return(response)
    allow(ActiveMerchant::Billing::BraintreeGateway).to receive(:new).and_return(gateway)
  end

  def mock_paypal_transaction(paymentstatus = 'Completed', options_overwrite={})
    @transaction = double('pp_response')
    allow(PayPalAPI).to receive(:reference_transaction).and_return(@transaction)
    allow(@transaction).to receive(:response).and_return(
      {
        'ACK' => "Success",
        'PAYMENTSTATUS' => paymentstatus,
        'AMT' => "9.99",
        'BILLINGAGREEMENTID' => "B-0RK57377ME0665506",
        'TRANSACTIONID' => "9CB082202F284692D",
        'TRANSACTIONTYPE' => "expresscheckout",
        'FEEAMT' => "0.59",
        'PENDINGREASON' => "None",
        'REASONCODE' => "None",
        'CURRENCYCODE' => "USD"
      }.merge(options_overwrite)
    )
  end

  def mock_paypal_account_added
    transaction = instance_double("transaction", success?: true, response: { "TOKEN" => "12345" })
    allow(PayPalAPI).to receive(:send_to_checkout).and_return(transaction)
    response = instance_double("response", success?: true, response: {})
    allow(PayPalAPI).to receive(:get_checkout_details).and_return(response)
    account = instance_double("account", id: 5555)
    allow(StoredPaypalAccount).to receive(:process_authorization).and_return(["Success", account])
    stub_const(
      "PaypalTransaction::EXPRESS_CHECKOUT_URL",
      "/stored_paypal_accounts/ec_step3?PayerID=abcde&token="
    )
    stub_const(
      "PaypalTransaction::STORED_RETURN_URL",
      PayPalSDKProfiles::Profile.stored_return_url
    )
    stub_const(
      "PaypalTransaction::STORED_CANCEL_URL",
      PayPalSDKProfiles::Profile.stored_cancel_url
    )
  end

  def mock_braintree_create_payment_method_credit_card
    mock_braintree_client_token
    mock_braintree_customer_create

    payment_method = instance_double(
      Braintree::CreditCard,
      token: "123456",
      card_type: "VISA",
      expiration_month: 4.years.from_now.month.to_s,
      expiration_year: 4.years.from_now.year.to_s,
      last_4: "1111"
    )
    result = instance_double(
      Braintree::SuccessfulResult,
      payment_method: payment_method,
      success?: true
    )
    allow_any_instance_of(Braintree::PaymentMethodGateway).to receive(:create).and_return(result)
  end

  def mock_braintree_customer_create
    customer = instance_double(Braintree::Customer, id: "805872984")
    result = instance_double(Braintree::SuccessfulResult, customer: customer)

    allow(Braintree::Customer).to receive(:create).and_return(result)
  end

  def mock_braintree_client_token
    allow(Braintree::ClientToken).to receive(:generate).and_return("123456")
  end
end
