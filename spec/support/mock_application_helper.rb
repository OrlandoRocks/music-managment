#
# This module is necessary in helper_specs where the method being
# tested relies on a current_user or country_website being set in
# the ApplicationController. Controllers are not available in helper
# specs, so without this extra functionality we cannot stub either
# current_user or country_website in helper specs. This module can
# be extened on the Rspec helper object with: helper.extend(MockApplicationHelper)
#
module MockApplicationHelper

  def current_user=(user)
    @current_user = user
  end

  def current_user
    @current_user
  end

  def country_website=(country_website)
    @country_website = country_website
  end

  def country_website
    @country_website
  end

end
