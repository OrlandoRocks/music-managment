module JsonHelper
  # For comparing received params to passed in params in controller specs
  def jsonify_hash(hash)
    hash.reject{ |_key, value| value.blank? }
      .inject({}) do |acc, (key, value)|
        acc[key.to_s] = value.to_s
        acc
      end
  end
end
