module FeatureHelper
  def turn_off_features(features)
    features.each do |feature|
      $rollout.activate_percentage(feature.to_sym, 0)
    end
  end
end
