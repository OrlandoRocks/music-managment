module XmlHelper
  def compare_xml(left, right)
    EquivalentXml.equivalent?(
      Nokogiri::XML(left, nil, 'utf-8'),
      right,
      element_order: false,
      normalize_whitespace: true
    )
  end
end
