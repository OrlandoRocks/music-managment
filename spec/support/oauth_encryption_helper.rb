module OauthEncryptionHelper
  def encrypt_token(token)
    SessionEncryptionEngine.encrypt64(token)
  end
end
