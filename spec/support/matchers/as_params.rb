RSpec::Matchers.define :as_params do |expected|
  match do |actual|
    @actual = ::ActiveSupport::HashWithIndifferentAccess.new(actual.permit!.to_h)
    expected == @actual
  end

  diffable
end
