module Dates
  def parse_mmddyyyy(input)
    Date.strptime(input, '%m/%d/%Y')
  end
end
