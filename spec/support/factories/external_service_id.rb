FactoryBot.define do
  factory :external_service_id do
    association :linkable, factory: :album
    service_name { "service" }
    sequence(:identifier, &:to_s)

    trait :creative_linkable do
      after(:create) do |esi|
        album         = create(:album, :finalized)
        creativeable  = create(:creative, artist: album.artist, creativeable: album)
        esi.linkable  = creativeable
        esi.save
      end
    end

    trait :apple_artist_id do
      creative_linkable
      service_name { "apple" }
    end

    trait :song do
      association :linkable, factory: :song
    end
  end
end
