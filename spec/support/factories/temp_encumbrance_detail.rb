FactoryBot.define do
  factory :temp_encumbrance_detail do
    encumbrance_summary_id { 12 }
    person
    transaction_amount { 10 }
  end
end
