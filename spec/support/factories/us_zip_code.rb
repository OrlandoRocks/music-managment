FactoryBot.define do
  factory :us_zip_code do
    code { "11201" }
    city { "Brooklyn" }
    state { "NY" }
  end
end
