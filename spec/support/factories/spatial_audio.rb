FactoryBot.define do 
  factory :spatial_audio do
    song
    s3_asset
    content_fingerprint { "71fee0281d85ee081f29456ca28ac31a" }
    audio_type { "dolby_atmos" }
  end
end 
