FactoryBot.define do
  factory :check_transfer do
    person
    payee {"Rick Sanchez"}
    transfer_status {"processing"}
    city {"New York"}
    country {"United States"}
    address1 {"45 Main Street"}
    payment_amount {"100.00"}
    state {"NY"}
  end
end
