FactoryBot.define do
  factory :label do
    name { "Test Label #{Faker::Alphanumeric.alpha(number: 10)}" }
  end
end
