FactoryBot.define do
  factory :email_change_request do
    person
    new_email { "new_email@newegg.com" }
    old_email { person.email }
    token { nil }
    finalized { false }
  end
end
