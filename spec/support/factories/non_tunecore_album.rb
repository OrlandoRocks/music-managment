FactoryBot.define do
  factory :non_tunecore_album do
    name {Faker::Book.title}
    composer
    publishing_composer
  end

  trait :with_tunecore_album do
    album
  end

  trait :with_non_tunecore_songs do
    transient do
      num_of_songs {1}
    end

    after(:create) do |ntc_album, evaluator|
      ntc_album.non_tunecore_songs << create_list(:non_tunecore_song, evaluator.num_of_songs, non_tunecore_album: ntc_album)
      ntc_album.save
      ntc_album.reload
    end
  end

end
