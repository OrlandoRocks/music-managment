FactoryBot.define do
  factory :marketing_experiment do
    marketing_service { "Hubspot" }
    experiment_type { "UNCONVERTED" }
    experiment_name { "Experiment 1" }
    number_of_variations { 2 }
    active_flag { true }
  end
end
