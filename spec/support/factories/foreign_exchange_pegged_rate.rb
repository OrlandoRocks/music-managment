FactoryBot.define do
  factory :foreign_exchange_pegged_rate do
    pegged_rate { 73.16 }
    country_id { 101 }
    country_website_id { 8 }
    currency { "INR" }
  end
end
