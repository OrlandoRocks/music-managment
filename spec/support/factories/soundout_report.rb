FactoryBot.define do
  factory :soundout_report do
    person
    retrieval_attempts {0}
    report_data        {nil}
    status             {"new"}
    track_type         {"Song"}
    soundout_product
  end

  trait :with_track do
    after(:build) do |sr|
      album = create(
        :album,
        :with_salepoints,
        :with_uploaded_songs,
        :with_artwork,
        :with_songwriter,
        :paid,
        :live
      )
      petri_bundle = create(:petri_bundle, album: album)
      create(:distribution, :delivered_to_spotify_find_or_create_salepoint, petri_bundle: petri_bundle)
      song = album.songs.first
      song.stub(:duration).and_return(95000)
      sr.track = song
    end
  end

  trait :with_url do
    after(:create) do |sr|
      sr.stub(:url).and_return("dummy_soundout_report_url")
    end
  end
end
