FactoryBot.define do
  factory :cover do
    album
    artist {"Shooby Taylor"}
    background {Background.first}
    background_effect {BackgroundEffect.first}
    render_size {600}
    title {"Cover"}
    layout_class {"ArtworkSuggestion::Layouts::CentercenterByCentercenter"}
    artist_typeface {Typeface.first}
    artist_typeface_effect {TypefaceEffect.first}
    artist_typeface_pointsize {15}
    title_typeface {Typeface.first}
    title_typeface_effect {TypefaceEffect.first}
    title_typeface_pointsize {15}
  end
end
