FactoryBot.define do
  factory :creative do
    artist
    role { "primary_artist" }
    association :creativeable
  end
end
