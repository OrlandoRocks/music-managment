FactoryBot.define do
  factory :publisher do
    name {"Publishing Company"}
    cae {"123456789"}
    performing_rights_organization
  end
end
