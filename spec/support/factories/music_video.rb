FactoryBot.define do
  factory :music_video do
    name { "Default video" }
    length_min { "4" }
    sale_date { Date.today }
    orig_release_year { Date.today }
    selected_artist_name { "default artist" }
    person
    video_type { 'MusicVideo' }
    accepted_format { true }
  end
end
