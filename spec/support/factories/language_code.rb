FactoryBot.define do
  factory :language_code do
    code { "en" }
    description { "English" }
    needs_translated_song_name { false }
  end
end
