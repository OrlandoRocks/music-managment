FactoryBot.define do
  factory :media_asset do
    person
    s3_asset
    size      {1024}
    type      {"BandPhoto"}
    mime_type {"image/jpeg"}
  end
end
