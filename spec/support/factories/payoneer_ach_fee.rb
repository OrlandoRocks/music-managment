FactoryBot.define do
  factory :payoneer_ach_fee do
    target_country_code {'US'}
    transfer_fees {1.0}
    target_currency {'USD'}
    source_currency {'USD'}
  end
end
