FactoryBot.define do
  factory :song_role do
    role_type { "songwriter" }
  end
end
