FactoryBot.define do
  factory :publishing_composition_split do
    percent {100.00}
    publishing_composition
    publishing_composer

    trait :collectable do
      right_to_collect {true}
    end

    trait :non_collectable do
      right_to_collect {false}
    end
  end
end
