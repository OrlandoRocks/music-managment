FactoryBot.define do
  factory :direct_advance_eligibility_record, :class => DirectAdvance::EligibilityRecord do
    person
    eligible        {true}
    report_run_on   {Date.today.to_s}

    trait :eligible do
      eligible  {true}
    end

    trait :ineligible do
      eligible  {false}
    end
  end
end
