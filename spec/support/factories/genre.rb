FactoryBot.define do
  factory :genre, aliases: [:primary_genre, :secondary_genre] do
    name {"Default Genre"}
    is_active {true}
  end
end
