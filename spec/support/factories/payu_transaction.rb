FactoryBot.define do
  factory :payu_transaction do
    invoice
    person
    amount { 100 }
    status { nil }
    created_at { Time.current }
    updated_at { Time.current }
  end
end
