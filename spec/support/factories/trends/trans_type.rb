FactoryBot.define do
  factory :trans_type do
    name { %w[Album Song Stream Ringtone].sample }
  end
end
