FactoryBot.define do
  factory :zipcode do
    zipcode { "11215" }
    state { "NY" }
    city_mixed_case { "Park Slope" }
    city { "Brooklyn" }
    primary_record { "P" }
    cbsa { 1 }
    cbsa_name { "New York-Northern New Jersey-Long Island NY-NJ-PA" }
    cbsa_short_name { "New York NY" }

    # NOTE: These are seeded and we currently don't run trends inserts in
    # transactions, so want to try and avoid creating new zipcodes.
    initialize_with { Zipcode.find_or_create_by(attributes) }
  end
end
