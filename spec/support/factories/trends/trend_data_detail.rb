FactoryBot.define do
  factory :trend_data_detail do
    album { Album.first }
    album_type { 'Album' }
    album_name { album.name }
    artist { Artist.first }
    artist_name { artist.name }
    song
    song_name { song.name }
    import_log
    provider { Provider.first }
    person { album.person }
    trans_type { album_download_trans_type }
    sale_date { 1.month.ago }
    country_code { 'US' }
    zip_code { '11238' }
    qty { 1 }
    tunecore_isrc { nil }
    optional_isrc { nil }
    upc { "859707867658" }
    royalty_currency_id { "USD" }
    royalty_price { 1.00 }
    customer_price { 2.00 }
    is_promo { 0 }

    trait :with_summary_and_geo do
      transient do
        zipcode { create(:zipcode) }
      end

      after(:create) do |trend_data_detail, evaluator|
        summary = create(
          :trend_data_summary,
          trend_data_detail: trend_data_detail
        )
        trend_data_detail.trend_data_summary = summary
        trend_data_detail.save

        create(
          :geo,
          trend_data_summary: summary,
          date: trend_data_detail.sale_date,
          country_code: trend_data_detail.country_code,
          qty: trend_data_detail.qty,
          cbsa: evaluator.zipcode.cbsa
        )
      end
    end
  end
end

def album_download_trans_type
  album_trans_type = TransType.album_download

  if album_trans_type
    album_trans_type
  else
    # TODO: These should be seeds?
    create(:trans_type, name: "Album")
    create(:trans_type, name: "Song")
    create(:trans_type, name: "Stream")
    create(:trans_type, name: "Ringtone")

    TransType.album_download
  end
end
