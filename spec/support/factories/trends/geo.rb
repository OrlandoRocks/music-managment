FactoryBot.define do
  factory :geo do
    trend_data_summary { TrendDataSummary.first }
    country_code { 'US' }
    cbsa { '1' }
    qty { 1 }
    date { 1.month.ago }
  end
end
