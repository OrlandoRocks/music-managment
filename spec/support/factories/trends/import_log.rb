FactoryBot.define do
  factory :import_log do
    started_at { 1.minute.ago }
    ended_at { Time.current }
    file_name { Faker::File.unique.file_name(ext: "txt") }
    provider_id { 1 }
    period_ending { 1.minute.from_now }
  end
end
