FactoryBot.define do
  factory :trend_data_summary do
    trend_data_detail
    person { trend_data_detail.person }
    album { trend_data_detail.album }
    song { trend_data_detail.song }
    trans_type { trend_data_detail.trans_type }
    provider { trend_data_detail.provider }
    date { trend_data_detail.sale_date }
    qty { trend_data_detail.qty }
  end
end
