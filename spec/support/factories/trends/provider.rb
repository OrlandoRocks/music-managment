FactoryBot.define do
  factory :provider do
    name { %w[spotify itunes amazon].sample }
  end
end
