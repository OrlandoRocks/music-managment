FactoryBot.define do
  factory :copyright do
    association :copyrightable, factory: :song
    composition { true }
    recording { true }
  end
end
