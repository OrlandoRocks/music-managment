FactoryBot.define do
  factory :batch_transaction do
    invoice
    payment_batch
    amount {9.99}
    currency {"USD"}
  end
end
