FactoryBot.define do
  factory :refund_item do
    refund
    purchase
    base_amount_cents { purchase.cost_cents }
    tax_amount_cents { purchase.vat_amount_cents }
    currency { "USD" }
  end
end
