FactoryBot.define do
  factory :country_audit do
    person
    country
    country_audit_source
    created_at {Time.current}
  end
end
