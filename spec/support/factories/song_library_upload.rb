FactoryBot.define do
  factory :song_library_upload do
    genre
    person
    artist_name { Faker::Name.unique.name }
    name { Faker::DcComics.unique.title }
  end
end
