FactoryBot.define do
  factory :purchase do
    person
    cost_cents  {999}
    product     {Product.find_by(country_website: person.country_website)}
    paid_at     {Time.now - 10.days}
    related_type {"Salepoint"}
    related_id   {"1"}
    invoice_id  {nil}
    discount_cents {0}
    salepoint_id {nil}
    no_purchase_album_id {nil}
    targeted_product_id {nil}
    vat_amount_cents { 0 }
    discount_reason { Purchase::NO_DISCOUNT }
  end

  trait :pub_admin do
    cost_cents    {7500}
    product       {Product.find(65)}
    related_type  {"Composer"}
    related_id    {Composer.first}
  end

  trait :indian_product do
    product { Product.find_by(id: 419) }
    related_type { "Product" }
    related_id { 419 }
  end

  trait :unpaid do
    paid_at {nil}
  end

  trait :tcs_pro_purchase do
    product { Product.find_by(display_name: "tc_social_monthly") }
  end

  trait :five_album_distribution_credits do
    product { Product.find_by(display_name: "5_album_creds")}
  end

  trait :credit_usage do
    product { Product.find_by(display_name: "credit_usage", country_website_id: 1)}
  end

  trait :with_vat_tax do
    related_type { "Product" }
    association :purchase_tax_information,
                factory: [:purchase_tax_information, :with_vat]
  end

  trait :without_vat_tax do
    related_type { "Product" }
    association :purchase_tax_information,
                factory: [:purchase_tax_information, :without_vat]
  end

  trait :with_variable_related_item do
    transient do
      album { create(:album) }
    end
    product
    person { album.person }
    related_id { album.id }
    related_type { album.class.name }
  end

  trait :with_free_album do
    transient do
      album { create(:album, :social_only) }
    end
    product
    person { album.person }
    related_id { album.id }
    related_type { album.class.name }
  end

  trait :free_with_plan do
    person

    product { Product.find_by(display_name: "credit_usage",
                              country_website_id: person.country_website_id) }
    related_type { "CreditUsage" }
    cost_cents { 0 }
  end

  trait :additional_artist do
    after(:create) do |purchase|
      person = purchase.person
      plan_addon = create(:plan_addon, :additional_artist_addon, :paid, { person: person })
      product = Product.find_by(display_name: PlanAddon::ADDITIONAL_ARTIST, country_website: person.country_website)

      purchase.update!(related: plan_addon, product: product)
    end
  end

  trait :in_invoice do
    after(:create) do |purchase|
      invoice = create(:invoice)
      purchase.update(invoice: invoice)
    end
  end
end
