FactoryBot.define do
  factory :video do
    name { "Test Video #{Faker::Alphanumeric.alpha(number: 10)}" }
    person
    label
    album
  end
end
