FactoryBot.define do
  factory :person_balance do
    person
    balance { BigDecimal("100.00") }
    currency { "USD" }
  end
end
