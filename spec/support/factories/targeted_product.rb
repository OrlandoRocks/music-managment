FactoryBot.define do
  factory :targeted_product do
    targeted_offer
    product                       {Product.find(1)}
    price_adjustment_type         {'dollars_off'}
    price_adjustment              {10.00}
    price_adjustment_description  {'Best Deal'}
    show_expiration_date          {true}
    show_discount_price           {true}

    trait :one_free_month do
      price_adjustment_type         {'percentage_off'}
      price_adjustment              {100}
      price_adjustment_description  {'One Free Month'}
    end

    trait :fifty_percent_off do
      price_adjustment_type         {'percentage_off'}
      price_adjustment              {50}
    end
  end
end
