FactoryBot.define do
  require 'ipaddr'
  factory :payments_os_transaction, class: "PaymentsOSTransaction" do
    person
    stored_credit_card
    ip_address { IPAddr.new(rand(2**32), Socket::AF_INET) }
    payment_id { SecureRandom.uuid }
    charge_id { SecureRandom.uuid }
    reconciliation_id { SecureRandom.hex }
    invoice
    amount { 1200 }
    currency { 'INR' }
    action {}
    created_at {}
    updated_at {}
  end

  trait :successful_refund do
    action { "refund" }
    payment_id { nil }
    charge_id { nil }
    reconciliation_id { nil }
    refund_id { SecureRandom.uuid }
    refund_status { "Succeed" }
    refund_reason { "could not go live with the content" }
    refund_category { "Release could not go live" }
  end

  trait :failed_refund do
    refund_id { nil }
    refund_status { 'Failed' }
  end
end
