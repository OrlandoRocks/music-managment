FactoryBot.define do
  factory :muma_song do
    title {Faker::Book.title}
    composition
  end
end
