FactoryBot.define do
  factory :salepoint_song do
    salepoint
    song
  end

  trait :with_distribution_song do
    before(:create) do |salepoint_song|
      distribution = create(:distribution)

      build(:distribution_song, distribution: distribution, salepoint_song: salepoint_song)
    end
  end
end
