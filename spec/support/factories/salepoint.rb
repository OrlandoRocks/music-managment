FactoryBot.define do
  factory :salepoint do
    association :salepointable, factory: :album
    store {Store.find_by(short_name: Store::SPOTIFY)}
    has_rights_assignment {true}

    trait :aod_store do
      after(:create) do |salepoint|
        store = Store.find_by(short_name: "AmazonOD")
        salepoint.store = store
        salepoint.variable_price = store.variable_prices.first
        salepoint.save
      end
    end
  end

  trait :itunes do
    after(:create) do |salepoint|
      store = Store.find_by(short_name: "iTunesWW")
      salepoint.store = store
      salepoint.variable_price = store.variable_prices.first
      salepoint.save
    end
  end

  trait :freemium do
    after(:create) do |salepoint|
      store = Store.find(Store::FB_REELS_STORE_ID)
      salepoint.store = store
      salepoint.variable_price = store.variable_prices.first
      salepoint.save
    end
  end

  trait :finalized do
    after(:create) do |salepoint|
      salepoint.update(finalized_at: Time.current)
    end
  end
  
  trait :payment_applied do
    after(:create) do |salepoint|
      salepoint.update(payment_applied: true)
    end
  end
end
