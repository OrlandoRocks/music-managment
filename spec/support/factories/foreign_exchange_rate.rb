FactoryBot.define do
  factory :foreign_exchange_rate do
    exchange_rate { 76.29 }
    source_currency { "USD" }
    target_currency { "INR" }
    valid_from { Time.now }

    trait :php do
      target_currency { "PHP" }
    end

    trait :eur do
      source_currency { "EUR" }
      target_currency { "USD" }
    end
  end
end
