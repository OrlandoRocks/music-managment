FactoryBot.define do
  factory :subscription_event do
    event_type {"Purchase"}
    person_subscription_status
    subscription_purchase

    trait :social do
      subscription_type {"Social"}
    end
  end
end
