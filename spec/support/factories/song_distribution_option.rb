FactoryBot.define do
  factory :song_distribution_option do
    option_name {:available_for_streaming}
    option_value {true}
    song
  end
end
