FactoryBot.define do
  factory :corporate_entity do
    name { 'TuneCore US' }
    currency {'USD'}
    address1 {'TuneCore, Inc'}
    address2 {'63 Pearl Street, Box #256'}
    city {'Brooklyn'}
    postal_code {'NY 11201'}
  end
end
