FactoryBot.define do
  factory :payout_transfer do
    association :payout_provider
    client_reference_id {"79e71c95-aa1a-8c04-d221-974f34f2d982"}
    payout_id {"9853043"}
    provider_status {PayoutTransfer::NONE}
    tunecore_status {PayoutTransfer::SUBMITTED}
    amount_cents {1000}
    currency {"USD"}
    fee_cents {100}
    transaction_type {"debit"}
    description {"This is for a provider to attach to a payout"}
    tunecore_processed_at {"2018-03-30 15:33:37"}
    provider_processed_at {"2018-03-30 15:33:37"}

    factory :approved_payout_transfer do
      provider_status {"completed"}
      tunecore_status {"approved"}
    end

    factory :submitted_payout_transfer do
      provider_status {"requested"}
      tunecore_status {"submitted"}
    end

    factory :auto_approval_eligible_payout_transfer do
      after :create do |payout_transfer|
        create :transfer_metadatum, trackable: payout_transfer
      end
    end

    trait :pending do
      provider_status { "pending" }
    end
  end
end
