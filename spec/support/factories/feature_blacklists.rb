FactoryBot.define do
  factory :feature_blacklist do
    person_id { 1 }
    feature { "MyString" }
  end
end