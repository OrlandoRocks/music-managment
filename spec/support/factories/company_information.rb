FactoryBot.define do
  factory :company_information do
    person
    enterprise_number { "AB1234567" }
    company_registry_data { "123 Chamber Street" }
    place_of_legal_seat { "Luxembourg" }
    registered_share_capital { "$15,000" }
  end
end
