FactoryBot.define do
  factory :royalty_split_detail do
    person
    royalty_split_intake
    song
    royalty_split
    owner { person }
    amount { -10 }
    currency { "USD" }
    royalty_split_config { "json_config" }
  end
end

