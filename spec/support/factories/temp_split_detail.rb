FactoryBot.define do
  factory :temp_royalty_split_detail do
    person
    royalty_split
    song
    currency { "USD" }
    corporate_entity_id { 1 }
    transaction_amount_in_usd { 1.34 }
  end
end
