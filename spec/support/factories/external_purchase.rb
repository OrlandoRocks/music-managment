FactoryBot.define do
  factory :external_purchase do
    person
    final_cents {99}
    paid_at {Time.now - 10.days}
    currency {"USD"}
    invoice_url  {nil}
    external_product
  end
end
