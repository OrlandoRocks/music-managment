FactoryBot.define do
  factory :legal_document do
    name              { "Terms and Conditions.pdf" }
    document_template
    person
    association       :subject, factory: :composer
  end
end
