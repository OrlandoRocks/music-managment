FactoryBot.define do
  factory :sip_store do
    name {"iTunes U.S."}
    display_on_status_page {1}
    store_id {36}

    factory :streaming_sip_store do
      name {"Spotify"}
      display_group_id {90}
      store_id {26}
    end
  end
end
