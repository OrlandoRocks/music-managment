FactoryBot.define do
  factory :booklet do
    album

    before(:create) do |booklet|
      s3_asset = create(
        :s3_asset,
        key: booklet.s3_key,
        bucket: BOOKLET_BUCKET_NAME)
        booklet.s3_asset = s3_asset
    end
  end
end
