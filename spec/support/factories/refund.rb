FactoryBot.define do
  factory :refund do
    association :invoice, :with_two_purchases, :settled
    tax_inclusive { false }
    refunded_by { association :person, :with_role, role_name: Role::REFUNDS }
    refund_reason { association :refund_reason }

    trait :with_refund_items do
      after(:create) do |refund, _|
        refund.refund_items << create(:refund_item, refund: refund, purchase: refund.invoice.purchases.first)
      end
    end

    trait :with_balance_settlement do
      after(:create) do |refund, _|
        transaction = create(:person_transaction, credit: Float(refund.total_amount), person: refund.person)
        refund.refund_settlements << create(:refund_settlement, :balance, refund: refund, source_id: transaction.id)
      end
    end

    trait :with_braintree_settlement do
      after(:create) do |refund, _|
        transaction = create(:braintree_transaction, amount: Float(refund.total_amount), person: refund.person)
        refund.refund_settlements << create(:refund_settlement, :braintree, refund: refund, source_id: transaction.id)
      end
    end
  end
end
