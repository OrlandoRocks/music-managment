FactoryBot.define do
  factory :paypal_transaction do
    person
    invoice
    action { "Void" }
    status { "Complete" }
    ack { "Success" }
    country_website_id { "1" }
  end

  trait :refund_transaction do
    after(:create) do |txn|
      txn.update(action: "Refund")
    end
  end
end
