FactoryBot.define do
  factory :person_preference do
    person
    preferred_adyen_payment_method { association :adyen_stored_payment_method }
    pay_with_balance { 1 }
  end
end
