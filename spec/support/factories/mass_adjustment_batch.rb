FactoryBot.define do
  factory :mass_adjustment_batch do
    admin_message {"Message to admin"}
    customer_message {"Message to artist"}
    csv_file_name {"my_test_file.csv"}
    allow_negative_balance {false}
    status {MassAdjustmentBatch::NEW}
    created_at {Time.now}
    created_by_id {9}
    s3_file_name {"2020-01-01.csv"}
    balance_adjustment_category_id { 5 }
  end
end
