FactoryBot.define do
  factory :composition do

    trait :with_songs do
      after(:create) do |composition, evaluator|
        album = create(:album, :with_songs)
        composition.songs << album.songs
      end
    end

    trait :with_non_tunecore_songs do
      after(:create) do |composition, evaluator|
        non_tunecore_album = create(:non_tunecore_album, :with_non_tunecore_songs)
        composition.non_tunecore_songs << non_tunecore_album.non_tunecore_songs
      end
    end

    trait :with_recordings do
      transient do
        num_of_recordings { 1 }
      end

      after(:create) do |composition, evaluator|
        composition.recordings << create_list(:recording, evaluator.num_of_recordings, composition: composition)
      end
    end
  end
end
