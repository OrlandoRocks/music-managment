FactoryBot.define do
  factory :withholding_exemption do
    person
    withholding_exemption_reason
    started_at { DateTime.now }
    revoked_at { nil }
    ended_at { nil }
  end
end
