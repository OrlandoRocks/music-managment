FactoryBot.define do
  factory :posting_royalty do
    posting_id { "posting_id" }
    corporate_entity_id { 1 }
    currency { 'USD' }
    state { 'pending' }
  end
end
