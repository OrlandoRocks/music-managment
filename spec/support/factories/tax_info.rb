FactoryBot.define do
  factory :tax_info do
    association :composer
    name { Faker::Name.name }
    address_1 { Faker::Address.street_address }
    city { Faker::Address.city }
    state { Faker::Address.state_abbr }
    zip { Faker::Address.zip }
    country { Faker::Address.country_code }
    dob { Faker::Date.birthday }
    is_entity {false}
    classification {"individual"}
    agreed_to_w9_at { DateTime.now }
    agreed_to_w9 {true}
  end
end
