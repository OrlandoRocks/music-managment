FactoryBot.define do
  factory :payout_withdrawal_type do
    association :payout_provider
    enabled {true}
    withdrawal_type {"PayPal"}
    disabled_at {"2018-03-30 15:09:06"}
  end
end
