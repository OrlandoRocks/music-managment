FactoryBot.define do
  factory :encumbrance_detail do
    encumbrance_summary
    transaction_date { Date.today }
    transaction_amount {-100}
  end
end
