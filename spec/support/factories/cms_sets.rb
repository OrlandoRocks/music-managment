FactoryBot.define do
  factory :cms_set do
    callout_type {"login"}
    callout_name {"login set"}
    country_website
    factory :login_set do
    end

    factory :interstitial_set do
      callout_type {"interstitial"}
      callout_name {"interstitial set"}
    end

    factory :dashboard_set do
      callout_type {"dashboard"}
      callout_name {"dashboard_set"}
    end

    trait :no_date_check do
      to_create { |instance| instance.save(validate: false) }
    end
  end

  factory :cms_set_attribute do
    attr_name {"bg_color"}
    cms_set
  end
end
