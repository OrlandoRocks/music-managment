FactoryBot.define do
  factory :distribution_song do
    distribution
    salepoint_song
    state {"delivered"}
    retry_count {0}
  end
end
