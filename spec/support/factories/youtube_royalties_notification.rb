FactoryBot.define do
  factory :youtube_royalties_notification do
    person
    association :notification_item, factory: :you_tube_royalty_intake
    type { "YoutubeRoyaltiesNotification" }
  end
end

