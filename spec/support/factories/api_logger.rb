FactoryBot.define do
  factory :api_logger do
    association :requestable, factory: :person

    endpoint { Faker::Internet.url }
    http_method { "post" }
    status_code { "200" }
    state { "processing" }
    job_id { nil }

    trait :with_job_id do
      after(:create) do |api_logger, _|
        api_logger.update(job_id: Faker::Number.number.to_s)
      end
    end
  end
end
