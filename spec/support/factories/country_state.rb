FactoryBot.define do
  factory :country_state do
    name {"Karnataka"}
    iso_code {"IN-KA"}
    entity_type {"state"}
    country_id {101}
    gst_config_id {1}
  end
end
