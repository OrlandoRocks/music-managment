FactoryBot.define do
  factory :downgrade_category do
    name { Faker::Lorem.word }
  end
end
