FactoryBot.define do
  factory :braintree_transaction do
    country_website_id {1}
    person
    response_code {201}
    amount {9.99}
    transaction_id { SecureRandom.hex }
    ip_address {"192.168.2.1"}
    processor_id {"ccprocessora"}
    currency {"USD"}
    invoice
  end

  trait :refund do
    after(:create) do |txn|
      txn.update(refund_category: 'Release did not go live', refund_reason: 'Testing', refunded_amount: txn.amount)
    end
  end

  trait :declined do
    after(:create) do |txn|
      txn.update(status: 'declined')
    end
  end

  trait :succeeded do
    after(:create) do |txn|
      txn.update(status: 'success')
    end
  end

  trait :with_settled_invoice do
    transient do
      association :invoice, :with_braintree_settlement, :settled
    end

    after(:create) do |txn, _|
      txn.invoice.invoice_settlements.first.update(
        source: txn,
        settlement_amount_cents: txn.amount * 100
      )
    end
  end
end
