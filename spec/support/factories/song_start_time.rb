FactoryBot.define do
  factory :song_start_time do
    song
    store
    start_time { 30 }
  end
end
