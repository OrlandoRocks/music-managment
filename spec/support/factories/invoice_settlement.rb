FactoryBot.define do
  factory :invoice_settlement do
    settlement_amount_cents {1000}
    currency {'USD'}
    invoice {build(:invoice)}
  end
end