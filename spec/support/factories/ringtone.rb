FactoryBot.define do
  factory :ringtone, class: Ringtone, parent: :album do
    name {"Default Ringtone"}
    created_on {Date.today}
    sale_date {Date.today}
    creatives {[{"name"=>"default artist", "role"=>"primary_artist"}]}
    is_various {false}
    primary_genre {Genre.active.first}
    person
    initialize_with { new(name: name) }
    golive_date {nil}
    timed_release_timing_scenario {nil}
    created_with {nil}
  end
end
