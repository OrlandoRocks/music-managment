FactoryBot.define do
  factory :two_factor_auth_prompt do
    person {nil}
    dismissed_count {1}
    prompt_at {"2018-02-28 11:11:31"}
  end
end
