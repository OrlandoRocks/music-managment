FactoryBot.define do
  factory :sales_record do
    person
    sales_record_master
    association :related, factory: :song
    distribution_type {"Download"}
    quantity {1}
    revenue_per_unit {0.7}
    revenue_total {0.7}
    amount {0.7}

    factory :streaming_sales_record do
      sales_record_master
      association :related, factory: :song
      distribution_type {"Streaming"}
      quantity {3}
      revenue_per_unit {0.03}
      revenue_total {0.03}
      amount {0.03}
    end
  end
end
