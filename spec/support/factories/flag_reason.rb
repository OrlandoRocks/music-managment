FactoryBot.define do
  factory :flag_reason do
    reason { Faker::Marketing.buzzwords }
    flag
  end
end
