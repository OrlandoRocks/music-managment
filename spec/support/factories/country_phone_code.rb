FactoryBot.define do
  factory :country_phone_code do
    country
    code    {"123"}
  end
end
