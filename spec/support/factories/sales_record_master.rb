FactoryBot.define do
  factory :sales_record_master do
    sip_store
    period_interval {1}
    period_year {2010}
    period_type {'monthly'}
    period_sort { '2010-01-01' }
    revenue_currency {'USD'}
    country {"US"}
    status {"posted"}
    posting_id {}
  end
end
