FactoryBot.define do
  factory :friend_referral_campaign do
    person
    campaign_id { GA_DEFAULT_CAMPAIGN_ID }
    campaign_code { 'yt' }
  end
end
