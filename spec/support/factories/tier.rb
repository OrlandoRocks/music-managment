FactoryBot.define do
  factory :tier do
    name { Faker::Lorem.word }
    description { Faker::Lorem.words(number: 5) }
    badge_url { Faker::Internet.url }
    hierarchy { Faker::Number.decimal(l_digits: 2, r_digits: 1) }
    is_active { true }
    is_sub_tier { false }
    slug { name.to_s.parameterize.underscore }
  end
end
