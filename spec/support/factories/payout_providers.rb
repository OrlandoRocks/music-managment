FactoryBot.define do
  factory :payout_provider do
    association :person
    provider_status {"onboarding"}
    tunecore_status {"active"}
    client_payee_id {"79e71c95-aa1a-8c04-d221-974f34f2d982"}
    registered_email {"developer@tunecore.com"}
    provider_processed_at {"2018-03-30 14:36:42"}
    name {"payoneer"}
    active_provider {true}
    currency {'USD'}
    payout_provider_config {PayoutProviderConfig.find_by_program_id(PayoutProviderConfig::TC_US_PROGRAM_ID)}

    trait :pending do
      provider_status {"pending"}
    end

    trait :approved do
      provider_status {"approved"}
    end

    trait :declined do
      provider_status {"declined"}
    end

    trait :disabled do
      tunecore_status {"disabled"}
    end

    trait :believe do
      payout_provider_config {PayoutProviderConfig.by_env.find_by(program_id: PayoutProviderConfig::BI_US_PROGRAM_ID)}
    end

    trait :skip_validation do
      to_create { |instance| instance.save(validate: false) }
    end
  end
end
