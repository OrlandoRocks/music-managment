FactoryBot.define do
  factory :external_service do
    name {Faker::Company.name}
  end
end
