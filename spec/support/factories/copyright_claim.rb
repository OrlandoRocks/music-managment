FactoryBot.define do
  factory :copyright_claim do
    asset { FactoryBot.create(:album, :with_uploaded_songs) }
    person
    internal_claim_id { 'Test9086' }
    store
    admin_id { 23 }
    fraud_type { 'Streaming Abuse' }
    compute_hold_amount { true }
    is_active { true }
  end
end
