FactoryBot.define do
  factory :royalty_payment do
    person
    composer
  end
end
