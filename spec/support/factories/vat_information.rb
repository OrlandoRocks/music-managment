FactoryBot.define do
  factory :vat_information do
    person
    company_name { 'Test' }
    vat_registration_number { '22326250' }
    vat_registration_status { 'valid' }
    trader_name { 'Test' }
  end
end
