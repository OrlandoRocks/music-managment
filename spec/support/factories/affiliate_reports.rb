FactoryBot.define do
  factory :affiliate_report do
    association :person
    person_email {"person@example.com"}
    association :affiliate, factory: :api_key
    product_name {"Product"}
    units_sold {100}
    sale_amount {999}
    sale_currency {"USD"}
    date_paid {"2017-08-01"}
    data {
      {
        foo: "bar",
        baz: {
          quux: "corge"
        }
      }
    }
  end
end
