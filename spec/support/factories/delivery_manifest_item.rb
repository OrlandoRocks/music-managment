FactoryBot.define do
  factory :delivery_manifest_item do
    xml_remote_path {"fake/remote/path"}
    uuid {"1234567"}
    xml_hashsum {"hashsum, yo"}
    takedown {false}
  end
end