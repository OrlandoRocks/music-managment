FactoryBot.define do
  factory :delivery_batch do
    store {Store.find(26)}
    active {true}
    transient do
      batch_size { 10 }
    end
  end

  trait :with_delivery_batch_items do
    after(:create) do |delivery_batch, evaluator|
      evaluator.batch_size.times do
        create(:delivery_batch_item, :with_distribution,
                                     delivery_batch: delivery_batch,
                                     store: delivery_batch.store)
      end
    end
  end

end
