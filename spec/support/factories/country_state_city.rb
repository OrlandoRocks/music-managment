FactoryBot.define do
  factory :country_state_city do
    name {"Bengaluru"}
    alt_name {"Bengalooru|Bangalore City|Bangalore"}
    state_id {18}
  end
end
