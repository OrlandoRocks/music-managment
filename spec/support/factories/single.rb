FactoryBot.define do
  factory :single, class: Single, parent: :album do
    name {"Default Single"}
    created_on {Date.today}
    sale_date {Date.today}
    creatives {[{ "name" => "default artist", "role" => "primary_artist" }]}
    golive_date {Time.now + 1.days}
    is_various {false}
    primary_genre { Genre.active.first }
    person
    initialize_with { new(name: name) }
    created_with {"songwriter"}

    trait :with_artwork do
      artwork
    end

    trait :explicit do
      parental_advisory {true}
    end

    trait :with_songwriter do
      after(:create) do |single|
        create(:creative_song_role, creative: single.creatives.first, song: single.songs.first)
      end
    end

    factory :finalized_single do
      finalized_at {Time.now}

      factory :paid_single do
        payment_applied {true}
      end
    end
  end
end
