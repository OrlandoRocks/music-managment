FactoryBot.define do
  factory :rejection_email do
    country_website_id {1}
    name {"test_rejection_email"}
    subject {"Test Rejection Email Subject"}
    body {"Hello <%=@person_name%> Thank you for choosing TuneCore to distribute your release <%=@album_name%> <%=@artist_name%>"}
  end
end
