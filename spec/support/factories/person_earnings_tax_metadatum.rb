FactoryBot.define do
  factory :person_earnings_tax_metadatum do
    person
    dist_taxform_id { nil }
    pub_taxform_id { nil }
    total_publishing_earnings { 0.00 }
    total_distribution_earnings { 0.00 }
    tax_year { DateTime.current.year }
    tax_blocked { false }
    created_at { DateTime.current }
    updated_at { DateTime.current }
  end

  trait :with_dist_taxform do
    dist_taxform { create(:tax_form) }
    dist_taxform_submitted_at { DateTime.current }
  end

  trait :with_pub_taxform do
    pub_taxform { create(:tax_form) }
    pub_taxform_submitted_at { DateTime.current }
  end

  trait :tax_blocked do
    tax_blocked { true }
  end

  trait :for_past_year do
    tax_year { 2020 }
  end
end
