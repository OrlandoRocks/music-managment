FactoryBot.define do
  factory :distribution_api_album do
    source_user_id { Faker::Lorem.words(number: 10) }
    source_album_id { Faker::Lorem.words(number: 6) }
    distribution_api_service
    album
  end

  trait :with_distribution_api_song do
    distribution_api_song
  end
end
