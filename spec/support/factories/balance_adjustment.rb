FactoryBot.define do
  factory :balance_adjustment do
    person
    posted_by_id {}
    posted_by_name {}
    admin_note {"admin note text"}
    customer_note {"customer note text"}
    credit_amount {250.0}
    debit_amount {0.00}
    category {'Refund - Renewal'}
  end
end
