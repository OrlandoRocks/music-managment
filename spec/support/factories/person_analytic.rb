FactoryBot.define do
  factory :person_analytic do
    person
    metric_name {"first_distribution_invoice_id"}
  end
end
