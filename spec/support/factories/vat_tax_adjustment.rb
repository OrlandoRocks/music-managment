FactoryBot.define do
  factory :vat_tax_adjustment do
    person
    related_id { related_id }
    related_type { related_type }
    amount { amount }
    tax_rate { 17 }
    tax_type { 'VAT' }
    place_of_supply  { 'Luxembourg' }
  end
end
