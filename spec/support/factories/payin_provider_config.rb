FactoryBot.define do
  factory :payin_provider_config do
    name {'Braintree'}
    currency {'USD'}
    username {'TuneCoreXXXX'}
    sandbox {true}
    corporate_entity_id {1}
    merchant_id {'yoyoyoyoyoyo'}
    public_key {'oyoyoyoyoyoy'}
  end
end
