FactoryBot.define do
  factory :document_template do
    document_title    {"Terms and Conditions Agreement"}
    description       {"Sign me!"}
    template_id       { SecureRandom.uuid }
    document_type     {"publishing"}
    role              {"composer_without_publisher"}
    language_code     {"en"}
    last_revision_at  {DateTime.now}
  end
end
