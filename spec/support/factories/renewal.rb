FactoryBot.define do
  factory :renewal do
    person
    purchase
    takedown_at {1.year.from_now}

    trait :no_takedown_at do
      takedown_at {nil}
    end

    trait :expired do
      after :create do |renewal|
        now = Time.now
        renewal_history = renewal.history.first
        renewal_history.starts_at = now - 1.month
        renewal_history.expires_at = now - 1.day
        renewal_history.save
      end
    end

    trait :plan_renewal do
      item_to_renew { ProductItem.first }
      transient do
        expires_at { nil }
      end
      after :create do |renewal, evaluator|
        person = renewal.person
        create(:person_plan, person: person, plan: Plan.find(Plan::PROFESSIONAL)) if person.person_plan.blank?

        plan_products_ids = PlansProduct.where(plan: person.plan).pluck(:product_id)
        plan_product = Product.find_by(id: plan_products_ids, country_website_id: person.country_website_id)
        product_item = ProductItem.find_by(product_id: plan_product.id)

        renewal.update(item_to_renew: product_item)
        if evaluator.expires_at.present?
          renewal_history = renewal.renewal_history.last
          renewal_history.update(starts_at: evaluator.expires_at - 1.year, expires_at: evaluator.expires_at)
        end
        create(:renewal_item, renewal: renewal, related: person.person_plan)
      end
    end
  end
end
