FactoryBot.define do
  factory :copyright_status do
    status { "pending" }
    related { build(:album) }
  end
end
