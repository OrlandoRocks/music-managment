FactoryBot.define do
  factory :payments_os_customer, class: "PaymentsOSCustomer" do
    person
    customer_id { SecureRandom.uuid }
    customer_reference { SecureRandom.uuid }
  end
end
