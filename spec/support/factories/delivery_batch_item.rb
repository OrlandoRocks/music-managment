FactoryBot.define do
  factory :delivery_batch_item do
    # association :delivery_manifest_item
    status {"present"}
    transient do
      store { nil }
    end
  end

  trait :for_manifest do
    after(:create) do |delivery_batch_item|
      delivery_manifest_item = create(:delivery_manifest_item)
    end
  end
  trait :with_distribution do
    after(:create) do |delivery_batch_item, evaluator|
      distribution = create(:distribution, :with_salepoint, store_short_name: evaluator.store.short_name)
      distribution.update(state: "delivered")
      delivery_batch_item.deliverable = distribution
      delivery_batch_item.save
    end
  end


end
