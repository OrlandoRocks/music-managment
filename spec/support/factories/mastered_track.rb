FactoryBot.define do
  factory :mastered_track do
    person
    name {"file_name"}
    claim_id {"xxxx-xxxx-xxxx-xxxx"}
    job_id {"123abc"}
  end
end
