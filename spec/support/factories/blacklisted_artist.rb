FactoryBot.define do
  factory :blacklisted_artist do
    active {true}
    artist
  end
end
