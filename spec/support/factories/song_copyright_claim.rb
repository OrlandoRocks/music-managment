FactoryBot.define do
  factory :song_copyright_claim do
    song
    person { nil }
    claimant_has_songwriter_agreement { false }
    claimant_has_pro_or_cmo { false }
    claimant_has_tc_publishing { false }
  end
end
