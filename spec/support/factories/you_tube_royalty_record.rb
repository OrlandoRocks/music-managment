FactoryBot.define do
  factory :you_tube_royalty_record do
    person
    total_views {10}
    you_tube_policy_type {'monetize'}
    net_revenue {40.00}
    gross_revenue {44.44}
    net_revenue_currency {"USD"}
    sales_period_start {"2014-11-01"}
    you_tube_video_id {"abc123"}
    song_name {"My Song"}
    album_name {"My Album"}
    label_name {"My Label"}
    artist_name {"My Artist"}

    after(:build) do |ytr|
      ytr.song = build(:song, album: build(:album, person: ytr.person)) if ytr.song.blank?
    end
  end
end
