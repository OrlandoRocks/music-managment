FactoryBot.define do
  factory :s3_detail do
    s3_asset
    file_type {S3Detail::WAV_TYPE}
    metadata_json {
      {"streams"=>
        [
          {
            "sample_rate"=>"44100",
            "bits_per_sample"=>16
          }
        ]
      }
    }
  end
end
