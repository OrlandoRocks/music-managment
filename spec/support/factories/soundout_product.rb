FactoryBot.define do
  factory :soundout_product do
    report_type       {"standard"}
    display_name      {"SoundOut Standard Report"}
    number_of_reviews {40}
    available_in_days {5}
    product           {Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)}
  end
end
