FactoryBot.define do
  factory :youtube_sr_mail_template do
    template_name {"removed_for_too_cool"}
    select_label {"Removed for Too Cool"}
    custom_fields  {"---\n -third_party\n"}
    subject {"{{ isrc }}: Removed for being too cool"}
    header {"<p>Hello {{ person_name }},</p><p>We have been contacted because you are too cool.</p>"}
    body {"@message_data.map do |message|\n  <<-HTML\n  <p>We have been contacted by <strong>\#{message[:custom_fields][\"1\"][\"third_party\"]}</strong> for: </p>\n  <ul>\n    <li>ISRC: \#{message[:song].isrc}</li>\n    <li>Title: \#{message[:song].name}</li>\n    <li>Artist: \#{message[:song].artist_name}</li>\n  </ul>\n  HTML\nend.join(\"\")"}
    footer {"<p>Stop being too cool!</p>"}
    country_website
  end
end
