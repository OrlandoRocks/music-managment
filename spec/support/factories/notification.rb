FactoryBot.define do
  factory :notification do
    person
    association :notification_item, factory: :album
    text {"Notification text"}
    title {"Notification title"}
    link_text {"Notification link text"}
    url {"www.tunecore.com/dashboard"}
  end
end
