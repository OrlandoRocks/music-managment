FactoryBot.define do
  factory :product do
    name               {"Album Distribution (Ad Hoc)"}
    display_name       {"Customer Facing Name"}
    description        {"Album distribution product for customers who haven't pre-paid."}
    status             {"Active"}
    product_type       {"Ad Hoc"}
    country_website_id {1}
    price              {10.00}
    currency           {"USD"}
    applies_to_product {"Album"}
    created_by

    trait :album_renewal do
      description         {"Annual renewal fee for your album."}
      display_name        {"Yearly Album Renewal"}
      name                {"Yearly Album Renewal"}
      price               {49.99}
      product_type        {"Renewal"}
    end

    trait :ringtone_renewal do
      description         {"Annual renewal fee for your ringtone."}
      display_name        {"Ringtone Renewal"}
      name                {"Ringtone Renewal"}
      price               {9.99}
      product_type        {"Renewal"}
    end

    trait :single_renewal do
      description         {"Annual renewal fee for your single."}
      display_name        {"Single Renewal"}
      name                {"Single Renewal"}
      price               {9.99}
      product_type        {"Renewal"}
    end

    trait :store_automator do
      applies_to_product  {"SalepointSubscription"}
      description         {"Release automatically distributed to future stores"}
      display_name        {"Store Automator"}
      name                {"Store Automator"}
      price               {10.00}
      product_family      {"Distribution Add-Ons"}
      product_type        {"Ad Hoc"}
    end

    trait :rf_video_download do
      description         {"Render Forest Video download payment"}
      display_name        {"rf_video_download"}
      name                {"Render Forest Video download"}
      applies_to_product  {"None"}
      renewal_level       {"None"}
    end

    trait :five_album_distribution_credits do
      description         {"Credit to distribute 5 Albums whenever your music is ready. Cannot be used for renewals."}
      display_name        {"5_album_creds"}
      name                {"Album Distribution Credit - 5 Pack"}
      applies_to_product  {"None"}
      renewal_level       {"Item"}
    end

    trait :with_product_item do
      after(:create) do |product|
        product.product_items << create(:product_item, product: product)
      end
    end
  end
end
