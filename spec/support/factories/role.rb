FactoryBot.define do
  factory :role do
    name {"Subscription Admin"}
    long_name {"Subsription Administrator"}
    description {"Gives admins the ability to cancel user subscriptions"}
    is_administrative {1}
  end
end
