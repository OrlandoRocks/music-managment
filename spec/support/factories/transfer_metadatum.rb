FactoryBot.define do
  factory :transfer_metadatum do
    trackable_type { "PayoutTransfer" }
    trackable_id { 1 }
    auto_approved { false }
    auto_approved_at { nil }
    login_event
  end
end
