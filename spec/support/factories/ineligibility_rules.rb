FactoryBot.define do
  factory :ineligibility_rule do
    property {"name"}
    operator {"contains"}
    value {"Video Game"}
    store
  end
end
