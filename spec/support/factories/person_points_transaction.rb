FactoryBot.define do
  factory :person_points_transaction do
    person
    credit_points { Faker::Number.number(digits: 2) }
    debit_points { 0 }
    target { create(:certification) }

    trait :debit_transaction do
      credit_points { 0 }
      debit_points { Faker::Number.number(digits: 2) }
      target { create(:reward) }
    end
  end
end
