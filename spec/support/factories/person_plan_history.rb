FactoryBot.define do
  factory :person_plan_history do
    person
    plan { Plan.find(4) }
    purchase { Purchase.last }
    plan_start_date { DateTime.now }
    plan_end_date { DateTime.now + 1.year }
    change_type { "initial_purchase" }
    discount_reason { "plan proration" }
  end
end
