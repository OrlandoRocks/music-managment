FactoryBot.define do
  factory :lifetime_earning do
    person
    person_intake { Faker::Number.decimal(l_digits: 3, r_digits: 3) }
    youtube_intake { Faker::Number.decimal(l_digits: 3, r_digits: 3) }
  end
end
