FactoryBot.define do
  factory :targeted_offer do
    created_by_id               {33}
    country_website_id          {"1"}
    name                        {"Album Offer $10 off"}
    status                      {"Inactive"}
    offer_type                  {"existing"}
    start_date                  {Time.now}
    expiration_date             {Time.now + 30.days}
    date_constraint             {"expiration"}
    population_criteria_count   {0}
    targeted_population_count   {0}
    product_show_type           {"price_override"}
    usage_limit                 {0}

    trait :join_token do
      offer_type {"new"}
      sequence(:join_token)    {|n| "JOIN_TOKEN_#{n}" }
    end

    trait :active do
      status  {"Active"}
    end
  end
end
