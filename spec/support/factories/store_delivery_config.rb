FactoryBot.define do
  factory :store_delivery_config do
    store
    batch_size {5}
  end
end
