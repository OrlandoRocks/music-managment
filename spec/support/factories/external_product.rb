FactoryBot.define do
  factory :external_product do
    name           {"Album Distribution (Ad Hoc)"}
    description    {"Album distribution product for customers who haven't pre-paid."}
    status         {"active"}
    price          {10.00}
    person
    external_service
  end
end
