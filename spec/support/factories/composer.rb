FactoryBot.define do
  factory :composer do
    account
    person
    first_name {"Waylon"}
    last_name {"Jennings"}
    agreed_to_terms {true}
    dob {"01/01/1970"}
  end

  trait :terminated do
    terminated_at {Date.today}
  end

  trait :active do
    performing_rights_organization
    cae {"123456789"}
  end

  trait :with_related_purchases do
    after(:create) do |composer|
      composer.related_purchases << create(:purchase, related_type: "Purchase", person_id: composer.person_id, related_id: composer.person_id)
    end
  end

  trait :with_pub_admin_purchase do
    after(:create) do |composer|
      composer.related_purchases << create(
        :purchase,
        related_type: "Composer",
        related_id:   composer.id,
        person_id:    composer.account.person_id,
        invoice_id:   create(:invoice, person: composer.account.person),
        paid_at:      Date.today,
        product_id:   Product.find_products_for_country(
          composer.account.person.country_domain, :songwriter_service
        ).first
      )
    end
  end

  trait :with_lod do
    after(:create) do |composer|
      composer.lod = create(:lod)
      composer.save
    end
  end

  trait :with_provider_identifier do
    provider_identifier {"00001C8GW"}
  end

  trait :with_publisher do
    publisher
  end
end
