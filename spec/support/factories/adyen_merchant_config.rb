FactoryBot.define do
  factory :adyen_merchant_config do
    merchant_account { "TestMerchant" }
    api_key { "XXX" }
    client_key { "XXX" }
    hmac_key { "44782DEF547AAA06C910C43932B1EB0C71FC68D9D0C057550C48EC2ACF6BA056" }
    corporate_entity_id { 1 }
  end
end
