FactoryBot.define do
  factory :product_item do
    name {"Album Distribution"}
    price {49.99}
    currency {"USD"}
  end

  trait :yearly do
    renewal_interval {"year"}
  end

  trait :with_duration do
    renewal_duration {11}
  end
end
