FactoryBot.define do
  factory :adyen_payment_method_info do
    payment_method_name { "cup" }
  end
end
