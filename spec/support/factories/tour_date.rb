FactoryBot.define do
  factory :tour_date do
    person
    artist
    country { Country.find_by(iso_code: "US") }
    city { "Watercress Valley" }
    venue { "Buttonhole Junction" }
    us_state { UsState.first }
    event_date_at { Date.today }
  end
end
