FactoryBot.define do
  factory :cover_song_metadata do
    song
    cover_song {false}
    licensed {nil}
    will_get_license {nil}
  end
end
