FactoryBot.define do
  factory :subscription_purchase do
    person
    payment_channel {"Tunecore"}
    subscription_product

    trait :social_us do
      subscription_product_id {4}
      after(:create) do |subscription_purchase, e|
        create(:purchase,
          person: subscription_purchase.person,
          product: subscription_purchase.subscription_product.product,
          related_type: "SubscriptionPurchase",
          related_id: subscription_purchase.id)
      end
    end

    trait :fb_tracks do
      subscription_product_id {12}
      after(:create) do |subscription_purchase, e|
        create(:purchase,
          person: subscription_purchase.person,
          product: subscription_purchase.subscription_product.product,
          related_type: "SubscriptionPurchase",
          related_id: subscription_purchase.id)
      end
    end
  end
end
