FactoryBot.define do
  factory :eft_batch_transaction do
    stored_bank_account
    country_website_id {1}
    amount {100}
    password_entered {"Testpass123!"}
  end

  trait :with_person_balance do
    before(:create) do |eft_batch_transaction|
      person = eft_batch_transaction.stored_bank_account.person
      person.person_balance.update(balance: 1000)
    end
  end

  trait :pending_approval do
    after(:create) do |eft_batch_transaction|
      eft_batch_transaction.update(status: "pending_approval")
    end
  end

  trait :sent_to_bank do
    after(:create) do |eft_batch_transaction|
      eft_batch_transaction.update(status: "sent_to_bank")
    end
  end

  trait :success do
    after(:create) do |eft_batch_transaction|
      eft_batch_transaction.update(status: "success")
    end
  end

  trait :failure do
    after(:create) do |eft_batch_transaction|
      eft_batch_transaction.update(status: "failure")
    end
  end

  trait :processing_in_batch do
    after(:create) do |eft_batch_transaction|
      eft_batch_transaction.update(status: "processing_in_batch")
    end
  end
end
