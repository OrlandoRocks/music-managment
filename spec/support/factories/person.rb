FactoryBot.define do
  factory :person, aliases: [:created_by] do
    name                              { Faker::Name.name }
    email                             { Faker::Internet.email }
    password                          { 'Testpass123!' }
    password_confirmation             { password }
    phone_number                      { '+12125555555' }
    country                           { "US" }
    country_website_id                { 1 }
    us_zip_code_id                    { 1 }
    status                            { "Active" }
    is_verified                       { true }
    invite_code                       { "d694d30688382ad5cde53645da676c2490112648" }
    invite_expiry                     { Time.now() - 30.days }
    accepted_terms_and_conditions_on  { Time.now - 30.days }
    accepted_terms_and_conditions     { true }
    password_reset_tmsp               { Time.now }
    dormant                           { false }
    state                             { nil }
    corporate_entity_id               { 1 }

    country_audit_source {CountryAuditSource::SELF_IDENTIFIED_NAME}

    factory :another_person do
      email { Faker::Internet.email }
    end
  end

  trait :with_india_country_website do
    before(:create) do |person, _|
      person.update(country_website_id: 8, country: "IN")
    end
  end

  trait :with_india_country do
    country { "IN" }
  end

  trait :with_lux_country do
    country { "LU" }
  end

  trait :with_canada_country_website do
    before(:create) do |person, _|
      person.update(country_website_id: 2, country: "CA")
    end
  end

  trait :with_us_country_and_france_website do
    before(:create) do |person, _|
      person.update(country_website_id: 6, country: "FR")
      person.update(country: "US")
    end
  end

  trait :admin do
    after(:create) do |person|
      person.roles << Role.find_by(name: 'Admin')
    end
  end

  trait :with_tc_social do
    after(:create) do |person, evaluator|
      create(:oauth_token, :for_tc_social, user: person, type: "Oauth2Token")
    end
  end

  trait :with_live_album do
    after(:create) do |person, evaluator|
      create(:album, :approved, :with_one_year_renewal, person: person)
    end
  end

  trait :with_live_albums do
    after(:create) do |person|
      create_list(:album, 2, :purchaseable, :approved, :with_one_year_renewal, person: person)
    end
  end

  trait :with_album_in_cart do
    after(:create) do |person|
      create(:album, :with_songs, person: person)
    end
  end

  trait :with_active_social_subscription_status do
    with_tc_social
    after(:create) do |person, evaluator|
      create(:person_subscription_status, person: person, subscription_type: "Social")
    end
  end

  trait :with_active_social_subscription_status_with_no_termination_date do
    with_tc_social
    after(:create) do |person, evaluator|
      create(:person_subscription_status, person: person, subscription_type: "Social", termination_date: nil)
    end
  end

  trait :with_social_subscription_status_and_event_and_purchase do
    with_tc_social
    with_live_album
    after(:create) do |person, evaluator|
      create(:person_subscription_status, :with_subscription_event_and_purchase, person: person, subscription_type: "Social")
    end
  end

  trait :with_inactive_social_subscription_status do
    with_tc_social
    after(:create) do |person, evaluator|
      create(:person_subscription_status, person: person, subscription_type: "Social", termination_date: 1.month.ago)
    end
  end

  trait :with_taken_down_album do
    after(:create) do |person, evaluator|
      create(:album, :taken_down, person: person)
    end
  end

  trait :with_distribution_credits do
    transient do
      number_of_inventories {1}
      inventory_type {"Album"}
    end

    after(:create) do | person, evaluator|
      person.inventories << create_list(:inventory, evaluator.number_of_inventories, person: person, title: "#{evaluator.inventory_type} Distribution", inventory_type: evaluator.inventory_type, quantity: 1)
      person.save
    end

  end

  trait :with_role do
    transient do
      role_name {"Admin"}
    end

    after(:create) do |person, evaluator|
      person.roles << Role.find_by(name: "Admin")
      person.roles << Role.find_by(name: evaluator.role_name)
      person.save
    end
  end

  trait :with_crt_specialist_role do
    after(:create) do |person|
      person.roles << Role.find_by(name:'Content Review Specialist')
    end
  end

  trait :with_crt_manager_role do
    after(:create) do |person|
      person.roles << Role.find_by(name:'Content Review Manager')
    end
  end

  trait :with_refund_role do
    after(:create) do |person|
      person.roles << Role.find_by(name: "Refunds")
    end
  end

  trait :with_balance do
    transient do
      amount { 0 }
    end

    after(:create) do |person, evaluator|
      person.person_balance.update(balance: evaluator.amount)
    end
  end

  trait :with_friend_referral do
    after(:create) do |person|
      person.referral_data << FactoryBot.create(:referral_datum, key: "fbuy_ref_code", value: "abcdefg")
    end
  end

  trait :with_stored_paypal_account do
    after(:create) do |person|
      person.stored_paypal_accounts << create(:stored_paypal_account, person: person, email: person.email)
    end
  end

  trait :with_stored_bank_account do
    after(:create) do |person|
      person.stored_bank_accounts << create(:stored_bank_account, person: person)
    end
  end

  trait :with_braintree_blue_stored_credit_card do
    after(:create) do |person|
      person.stored_credit_cards << create(:bb_stored_credit_card, last_four: '1234', cc_type: "Visa")
    end
  end

  trait :with_expired_braintree_blue_stored_credit_card do
    after(:create) do |person|
      person.stored_credit_cards << create(:bb_stored_credit_card, :expired, last_four: '1234', cc_type: "Visa")
    end
  end

  trait :with_stored_credit_card do
    after(:create) do |person|
      person.stored_credit_cards << create(:stored_credit_card, :with_billing_address, last_four: '1234', cc_type: "Visa")
    end
  end

  trait :with_lux_stored_credit_card do
    after(:create) do |person|
      person.update(country: "LU", corporate_entity_id: 2)
      person.stored_credit_cards << create(:bb_stored_credit_card, :expired, last_four: '1234', cc_type: "Visa", country: "LU")
    end
  end

  trait :with_transactions_old do
    after(:create) do |person|
      create(:person_transaction, person_id: person.id, previous_balance: BigDecimal.new(200), created_at: "2017-01-01")
      create(:person_transaction, debit: BigDecimal("0.00"), credit: BigDecimal("90.00"), previous_balance: BigDecimal.new(200), target_type: "PersonIntake", comment: nil, target_id: 5678, person_id: person.id, created_at: "2017-01-02")
    end
  end

  trait :with_transactions do
    transient do
      transactions_created_at { Time.now }
    end

    after(:create) do |person, evaluator|
      create(:person_transaction, person_id: person.id, created_at: evaluator.transactions_created_at)
      create(:person_transaction, debit: BigDecimal("0.00"), credit: BigDecimal("100.00"), person_id: person.id, created_at: evaluator.transactions_created_at)
    end
  end

  trait :with_person_intake do
    after(:create) do |person, evaluator|
      create(:person_intake, person: person, amount: BigDecimal("100.0"))
    end
  end

  trait :with_you_tube_royalty_intake do
    after(:create) do |person, evaluator|
      create(:you_tube_royalty_intake, person: person, amount: BigDecimal("100.0"))
    end
  end

  trait :with_composer_with_purchases do
    after(:create) do |person|
      person.composers << create(:composer, :with_related_purchases)
    end
  end

  trait :with_publishing_composer_with_purchases do
    after(:create) do |person|
      person.publishing_composers << create(:publishing_composer, :with_related_purchases, :primary)
    end
  end

  trait :with_ytsr do
    with_live_album

    after(:create) do |person|
      create(:youtube_monetization, person: person, effective_date: Time.now)
      store = Store.find_by(abbrev: "ytsr")
      album = person.albums.first
      album.salepoints << create(:salepoint, salepointable: album, store: store)
    end
  end

  trait :with_ytrs_in_cart do
    with_ytsr_purchase
  end

  trait :with_ytsr_purchase do
    with_live_albums

    after(:create) do |person|
      youtube_monetization = create(:youtube_monetization, person: person)
      product = create(:product, name: "Collect Your YouTube Sound Recording Revenue", display_name: "ytsr")
      ytm_purchase = create(:purchase, person: person, product: product, related: youtube_monetization)
      person.youtube_monetization.purchase = ytm_purchase
      person.save
    end
  end

  trait :with_active_ytm do
    with_live_albums

    after(:create) do |person|
      youtube_monetization = create(:youtube_monetization, :with_effecive_date, person: person)
      product = create(:product, name: "Collect Your YouTube Sound Recording Revenue", display_name: "ytsr")
      ytm_purchase = create(:purchase, person: person, product: product, related: youtube_monetization)
      person.youtube_monetization.purchase = ytm_purchase
      person.save
    end
  end

  trait :with_live_distributions do
    after(:create) do |person|
      album        = create(:album, person: person, legal_review_state: "APPROVED", style_review_state: "APPROVED")
      petri_bundle = create(:petri_bundle, album: album)
      distribution = create(:distribution, converter_class: "MusicStores::DDEX::Converter", petri_bundle: petri_bundle)
      salepoint    = create(:salepoint, salepointable: album, store: Store.find_by(abbrev: "bl"))
      distribution.salepoints << salepoint
    end
  end

  trait :with_delivered_distributions do
    after(:create) do |person|
      album        = create(:album, :approved, person: person, style_review_state: "APPROVED")
      petri_bundle = create(:petri_bundle, album: album)
      distribution = create(:distribution, converter_class: "MusicStores::DDEX::Converter", petri_bundle: petri_bundle)
      distribution.update(state: 'delivered')
      salepoint    = create(:salepoint, salepointable: album, store: Store.find_by(abbrev: "bl"))
      distribution.salepoints << salepoint
    end
  end

  trait :with_three_delivered_distros do
    after(:create) do |person|
      3.times do
        album        = create(:album, :approved, person: person, style_review_state: "APPROVED")
        petri_bundle = create(:petri_bundle, album: album)
        distribution = create(:distribution, converter_class: "MusicStores::DDEX::Converter", petri_bundle: petri_bundle)
        distribution.update(state: 'delivered')
        salepoint    = create(:salepoint, salepointable: album, store: Store.find_by(abbrev: "bl"))
        distribution.salepoints << salepoint
      end
    end
  end

  trait :with_three_delivered_distros_goog do
    after(:create) do |person|
      3.times do
        album        = create(:album, :approved, person: person, style_review_state: "APPROVED")
        petri_bundle = create(:petri_bundle, album: album)
        distribution = create(:distribution, converter_class: "MusicStores::Google::Converter", petri_bundle: petri_bundle)
        distribution.update(state: 'delivered')
        salepoint    = create(:salepoint, salepointable: album, store: Store.find_by(short_name: "Google"))
        distribution.salepoints << salepoint
      end
    end
  end

  trait :with_two_factor_auth do
    after(:create) do |person|
      create(:two_factor_auth, person: person)
    end
  end

  trait :with_two_factor_auth_france do
    after(:create) do |person|
      create(:two_factor_auth, person: person, country_code: 33)
    end
  end

  trait :with_payout_provider do
    after(:create) do |person|
      create(:payout_provider, :approved, person: person)
    end
  end

  trait :with_inactive_payout_provider do
    after(:create) do |person|
      create(:payout_provider, active_provider: false, person: person)
    end
  end

  trait :with_lifetime_earnings do
    after(:create) do |person|
      create(:lifetime_earning, person: person)
    end
  end

  trait :with_zero_lifetime_earnings do
    after(:create) do |person|
      create(:lifetime_earning, person_intake: 0, youtube_intake: 0, person: person)
    end
  end

  trait :with_believe_payout_provider do
    after(:create) do |person|
      create(:payout_provider, :approved, :believe, person: person)
    end
  end

  trait :with_unpaid_purchases do
    after(:create) do |person|
      create(:purchase, person: person, paid_at: nil)
    end
  end

  trait :with_refund_rights do
    after(:create) do |person|
      refunds_role = Role.find_by(name: "Refunds")
      customer_role = Role.find_by(name: "Customer")
      person.update(roles: (person.roles + [refunds_role, customer_role].compact))
    end
  end

  trait :with_all_roles do
    after(:create) do |person|
      person.update!(roles: Role.all)
    end
  end

  trait :with_fb_tracks do
    after(:create) do |person|
      create(:person_subscription_status, :with_subscription_event_and_purchase, person: person, subscription_type: "FBTracks")
    end
  end

  trait :with_yt_tracks do
    after(:create) do |person|
      create(:person_subscription_status, :with_subscription_event_and_purchase, person: person, subscription_type: "YTTracks")
    end
  end

  trait :with_monetizable_tracks do
    after(:create) do |person|
      album = create(:album, :approved, person: person)
      create(:song, album: album)
    end
  end

  trait :with_address do
    address1 {Faker::Address.street_address}
    address2 {Faker::Address.secondary_address}
    city {Faker::Address.city}
    state {Faker::Address.state_abbr}
    postal_code {"11201"}
    zip {"11201"}
  end

  trait :with_taxable_earnings do
    after(:create) do |person|
      2.times { create(:taxable_earning, person: person) }
    end
  end

  trait :with_tax_form do
    after(:create) do |person|
      create(:tax_form, person: person)
    end
  end

  trait :with_invalid_tax_form do
    after(:create) do |person|
      create(:tax_form, :w8, person: person)
    end
  end

  trait :with_invalid_country do
    after(:create) do |person, _|
      person.country = "Unidentified"
      person.save(validate: false)
    end
  end

  trait :with_login_events do
    transient do
      login_created_at { Time.now }
    end

    after(:create) do |person, evaluator|
      create(:login_event, person: person, created_at: evaluator.login_created_at, country: "United States")
    end
  end

  trait :with_person_country do
    after(:create) do |person|
      create(:person_country, person: person)
    end
  end

  trait :with_autorenewal_and_current_cc do
    after(:create) do |person|
      stored_credit_card = create(:stored_credit_card, person: person)
      create(:person_preference, person: person, do_not_autorenew: false, preferred_payment_type: 'CreditCard', preferred_credit_card_id: stored_credit_card.id)
    end
  end

  trait :with_compliance_info do
    after(:create) do |person|
      if person.customer_type == "individual"
        create(:compliance_info_field, field_name: "first_name", field_value: person.first_name, person: person)
        create(:compliance_info_field, field_name: "last_name", field_value: person.last_name, person: person)
      elsif person.customer_type == "company"
        create(:compliance_info_field, field_name: "company_name", field_value: person.company_name, person: person)
      end
    end
  end

  trait :with_self_billing_status do
    after(:create) do |person|
      create(:self_billing_status, person: person)
    end
  end

  trait :with_people_flag do
    transient do
      flag_attributes { Flag::SUSPICIOUS }
      flag_reason { nil }
    end

    with_flag
  end

  trait :with_blocked_from_distribution_flag do
    transient do
      flag_attributes { Flag::BLOCKED_FROM_DISTRIBUTION }
      flag_reason { nil }
    end

    with_flag
  end

  trait :with_flag do
    after(:create) do |person, evaluator|
      name = evaluator.flag_attributes[:name]
      category = evaluator.flag_attributes[:category]
      flag = Flag.find_by(name: name, category: category) ||
        create(:flag, name: name, category: category)

      create(:people_flag, person: person, flag: flag, flag_reason: evaluator.flag_reason)
    end
  end

  trait :with_valid_w9_taxform do
    transient do
      taxform { nil }
    end

    after(:create) do |person, _evaluator|
      create(
        :tax_form,
        person: person,
        tax_form_type_id: 2,
        payout_provider_config: PayoutProviderConfig.find_by(program_id: PayoutProviderConfig::TC_US_PROGRAM_ID, sandbox: true),
        expires_at: 3.years.from_now
        )
    end
  end
  trait :with_multiple_valid_w9_taxforms do
    transient do
      taxform { nil }
    end

    after(:create) do |person, _evaluator|
      create(
        :tax_form,
        person: person,
        tax_form_type_id: 2,
        payout_provider_config: PayoutProviderConfig.find_by(program_id: PayoutProviderConfig::TC_US_PROGRAM_ID, sandbox: true),
        expires_at: 3.years.from_now
      )
      create(
        :tax_form,
        person: person,
        tax_form_type_id: 2,
        payout_provider_config: PayoutProviderConfig.find_by(program_id: PayoutProviderConfig::BI_US_PROGRAM_ID, sandbox: true),
        expires_at: 3.years.from_now
      )
    end
  end

  trait :with_active_copyright_claim do
    after(:create) do |person, evaluator|
      create(:copyright_claim, person: person)
    end
  end

  trait :with_locked_account do
    account_locked_until { 1.hour.from_now }
  end

  trait :with_suspicious_account do
    status { "Suspicious" }
  end

  trait :with_you_tube_royalty_record do
    transient do
      posting_id { 1 }
    end

    after(:create) do |person, evaluator|
      create(:you_tube_royalty_record, person: person, posting_id: evaluator.posting_id)
    end
  end

  trait :with_sales_record do
    transient do
      posting_id { 1 }
    end

    after(:create) do |person, evaluator|
      create(:sales_record, person: person, posting_id: evaluator.posting_id)
    end
  end

  trait :with_person_plan do
    transient do
      plan { nil }
    end
    after(:create) do |person, evaluator|
      plan = evaluator.plan.present? ? evaluator.plan : Plan.find(Plan::PROFESSIONAL)
      create(:person_plan, person: person, plan: plan)
    end
  end

  trait :with_plan_renewal do
    with_person_plan
    after(:create) do |person, _|
      create(:renewal, :plan_renewal, person: person)
    end
  end

  trait :with_believe_corporate_entity do
    after(:create) do |person|
      person.corporate_entity_id = 2
      person.save
    end
  end
end
