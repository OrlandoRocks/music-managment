FactoryBot.define do
  factory :variable_prices_store do
    store
    variable_price
    is_active {true}
  end
end
