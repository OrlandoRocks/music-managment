FactoryBot.define do
  factory :stored_paypal_account do
    person
    country_website_id {"1"}
    archived_at {nil}
    email { person.email }
  end
end
