FactoryBot.define do
  factory :publishing_split do
    percent {100.00}
    composition
    composer
  end
end
