FactoryBot.define do
  factory :blacklisted_paypal_payee do
    payee { Faker::Internet.email }
  end
end
