FactoryBot.define do
  factory :reward do
    name { Faker::Lorem.word }
    description { Faker::Lorem.words(number: 5) }
    link { Faker::Internet.url }
    content_type { 'video' }
    category { 'education' }
    is_active { true }
    points { 10 }
  end
end
