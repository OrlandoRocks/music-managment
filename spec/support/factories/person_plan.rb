FactoryBot.define do
  factory :person_plan do
    person
    plan { Plan.find(4) }
  end
end
