FactoryBot.define do
  factory :outbound_refund do
    association :refund, :with_refund_items, :with_balance_settlement
    user_invoice_prefix { refund.person.cr_outbound_invoice_prefix }
    outbound_invoice { outbound_invoice }
    refund_settlement { refund.refund_settlements.first }
  end
end
