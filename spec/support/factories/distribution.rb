FactoryBot.define do
  factory :distribution do
    converter_class {"MusicStores::Streaming::Converter"}
    petri_bundle
    sqs_message_id {"095468ab-72a2-4691-9b74-cb06924a381f"}
  end

  trait :enqueued do
    after(:create) do |distribution|
      distribution.update(state: "enqueued")
    end
  end

  trait :delivered_distro do
    after(:create) do |distribution|
      distribution.update(state: "delivered")
    end
  end

  trait :with_salepoint do
    transient do
      store_short_name {"Anghami"}
    end

    after(:create) do |distribution, evaluator|
      store = Store.find_by(short_name: evaluator.store_short_name)
      store.delivery_service = "tunecore"
      store.save
      create(:salepointable_store, store: store)
      salepoint = create(:salepoint, salepointable: distribution.petri_bundle.album, store: store)
      distribution.salepoints = [salepoint]
    end
  end

  trait :with_distribution_songs do
    transient do
      number_of_distribution_songs {2}
    end

    after(:create) do |distribution, evaluator|
      store = Store.find_by(short_name: "YoutubeSR")
      store.save
      create(:salepointable_store, store: store)
      salepoint = create(:salepoint, store: store, salepointable: distribution.petri_bundle.album, status: "complete")
      distribution.salepoints << salepoint
      (1..evaluator.number_of_distribution_songs).each do |number|
        song              = create(:song, :with_upload, album: distribution.petri_bundle.album)
        salepoint_song    = create(:salepoint_song, salepoint: salepoint, song: song, state: "approved")
        distribution_song = create(:distribution_song, distribution: distribution, salepoint_song: salepoint_song)
      end
    end
  end

  trait :delivered_to_spotify do
    after(:create) do |distro|
      store = Store.find_by(short_name: "Spotify")
      create(:salepointable_store, store: store)
      salepoint = create(:salepoint, store: store, salepointable: distro.petri_bundle.album, status: "complete")
      distro.salepoints << salepoint
      distro.update(state: "delivered", converter_class: store.converter_class.to_s)
    end
  end

  trait :delivered_to_spotify_via_distributor do
    after(:create) do |distro|
      store = Store.find_by(short_name: "Spotify")
      create(:salepointable_store, store: store)
      salepoint = create(:salepoint, store: store, salepointable: distro.petri_bundle.album, status: "complete")
      distro.salepoints << salepoint
      distro.update(state: "delivered_via_tc_distributor", converter_class: store.converter_class.to_s)
    end
  end

  trait :delivered_to_spotify_find_or_create_salepoint do
    after(:create) do |distro|
      store = Store.find_by(short_name: "Spotify")
      create(:salepointable_store, store: store)

      salepoint = distro.petri_bundle.album.salepoints.where(store: store).first

      unless salepoint
        salepoint = create(:salepoint, store: store, salepointable: distro.petri_bundle.album, status: "complete")
      end

      distro.salepoints << salepoint
      distro.update(state: "delivered", converter_class: store.converter_class.to_s)
    end
  end

  trait :delivered_to_apple do
    after(:create) do |distro|
      store = Store.find_by(short_name: "iTunesUS")
      create(:salepointable_store, store: store)
      salepoint = build(:salepoint, store: store, salepointable: distro.petri_bundle.album, status: "complete")
      varprice = create(:variable_price)
      store.variable_prices << varprice
      salepoint.variable_price = varprice
      distro.salepoints << salepoint
      distro.update(state: "delivered", converter_class: store.converter_class.to_s)
    end
  end

  trait :delivered_to_apple_via_distributor do
    after(:create) do |distro|
      store = Store.find_by(short_name: "iTunesUS")
      create(:salepointable_store, store: store)
      salepoint = build(:salepoint, store: store, salepointable: distro.petri_bundle.album, status: "complete")
      varprice = create(:variable_price)
      store.variable_prices << varprice
      salepoint.variable_price = varprice
      distro.salepoints << salepoint
      distro.update(state: "delivered_via_tc_distributor", converter_class: store.converter_class.to_s)
    end
  end

  trait :delivered_to_amazon do
    after(:create) do |distro|
      store = Store.find_by(short_name: "Amazon")
      create(:salepointable_store, store: store)
      salepoint = build(:salepoint, store: store, salepointable: distro.petri_bundle.album, status: "complete")
      varprice = create(:variable_price)
      store.variable_prices << varprice
      salepoint.variable_price = varprice
      distro.salepoints << salepoint
      distro.update(state: "delivered", converter_class: store.converter_class.to_s)
    end
  end

  trait :delivered_to_streaming do
    after(:create) do |distro|
      store = Store.find_by_short_name("Streaming")
      create(:salepointable_store, store: store)
      salepoint = create(:salepoint, store: store, salepointable: distro.petri_bundle.album, status: "complete")
      distro.salepoints << salepoint
      distro.update(state: "delivered", converter_class: store.converter_class.to_s)
    end
  end

  trait :with_songs do
    after(:create) do |distro|
      distro.petri_bundle.album.songs << create(:song)
      distro.petri_bundle.album.save
    end
  end
end
