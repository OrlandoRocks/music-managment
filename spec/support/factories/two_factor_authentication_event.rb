FactoryBot.define do
  factory :two_factor_auth_event do
    action {"sign_in"}
    page {"withdraw_paypal"}
    successful {false}
    type {"authentication"}
    two_factor_auth
  end
end
