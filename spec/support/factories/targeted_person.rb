FactoryBot.define do
  factory :targeted_person do
    person
    targeted_offer
    usage_count {0}
  end
end
