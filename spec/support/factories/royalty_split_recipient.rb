FactoryBot.define do
  factory :royalty_split_recipient do
    percent { 100.0 }
    person
    royalty_split

    trait :with_email do
      person { nil }
      sequence(:email) { |n| "email#{n}@domain.fake" }
    end

    # only works with "build" -- use royalty_split_with_recipients(0)
    trait :owner do
      person
      royalty_split { association :royalty_split, owner: person }
    end

    # factory without a trait is also a "non-owner", but this one has a valid royalty_split
    trait :not_owner do
      after(:create) do |recipient, evaluator|
        split = evaluator.royalty_split
        split.owner_recipient.update(percent: split.what_owner_percent_should_be) 
      end
    end

    trait :accepted do
      after(:create) do |recipient|
        recipient.update(accepted_at: Time.current)
      end
    end
  end
end
