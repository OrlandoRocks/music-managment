FactoryBot.define do
  factory :youtube_preference do
    person
    channel_id {"UCaSZ5NmmBbij0iTCOPTuOcg"}
    whitelist {false}
  end
end
