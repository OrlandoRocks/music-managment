FactoryBot.define do
  factory :targeted_product_store do
    store
    targeted_product
  end
end
