FactoryBot.define do
  factory :album_country do
    relation_type {"exclude"}
    country
    album
  end
end
