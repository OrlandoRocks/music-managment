FactoryBot.define do
  factory :royalty_split_intake do
    person
    amount      {10.00}
    currency    {"USD"}
    created_at  {Date.current}
  end
end

