FactoryBot.define do
  factory :manual_approval do
    approveable_type {approveable_type}
    approveable_id {approveable_id}
    created_by_id {created_by_id}
  end
end
