FactoryBot.define do
  factory :salepoint_preorder_data do
    salepoint
    start_date {Date.today + 11.days}
    preorder_purchase
  end
end
