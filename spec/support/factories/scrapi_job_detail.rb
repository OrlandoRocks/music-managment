FactoryBot.define do
  factory :scrapi_job_detail do
    track_title   {"Bravo"}
    isrc          {"TCBF29887755"}
    duration      {"218"}
    label         {"Sony"}
    album_title   {"Bravo"}
    album_upc     {"8597328587690"}
    artists       {["BLACKPINK"]}
    genres        {}
    score         {100}
    scrapi_job       
  end
end
