FactoryBot.define do
  factory :cowriter do
    first_name  {"Baker"}
    last_name   {"Mayfield"}
    is_unknown  {false}
    composer

    trait :unknown do
      first_name {nil}
      last_name {nil}
      is_unknown {true}
      composer
    end
  end
end
