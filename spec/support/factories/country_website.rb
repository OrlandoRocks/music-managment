FactoryBot.define do
  factory :country_website do
    name {"TC USA"}
    currency {"USD"}
    country {"US"}

    trait :france do
      name     {'France'}
      currency {'EUR'}
      country  {'FR'}

      after(:create) do |cw|
        cw.country_id = Country.find_by(name: 'France').id
        cw.save
      end
    end

  end
end
