# frozen_string_literal: true

FactoryBot.define do
  factory :irs_transaction do
    status { "pending" }

    trait :with_irs_tax_withholdings_and_person_transactions do
      transient do
        number_of_irs_tax_withholdings { 1 }
      end

      after(:create) do |irs_transaction, evaluator|
        create_list(
          :irs_tax_withholding,
          evaluator.number_of_irs_tax_withholdings,
          :with_person_transaction,
          irs_transaction: irs_transaction
        )
      end
    end
  end
end
