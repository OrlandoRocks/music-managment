FactoryBot.define do
  factory :applied_ineligibility_rule do
    ineligibility_rule
    item_type {"TrackMonetization"}
    item_id {1}
  end
end
