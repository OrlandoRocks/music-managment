FactoryBot.define do
  factory :deleted_account do
    person
    delete_type {"GDPR"}
  end
end
