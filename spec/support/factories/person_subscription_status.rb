FactoryBot.define do
  factory :person_subscription_status do
    person
    subscription_type { "Social" }
    effective_date { 6.months.ago }
    termination_date { 6.months.from_now }
    grace_period_length { 5 }

    trait :with_subscription_event_and_purchase do
      after(:create) do |status, evaluator|
        purchase = create(:subscription_purchase, :social_us)
        create(:subscription_event, person_subscription_status: status, subscription_purchase: purchase)
      end
    end
  end
end
