FactoryBot.define do
  factory :captcha_whitelist do
    ip {'1.2.3.4'}
    description {'Test server'}
  end
end
