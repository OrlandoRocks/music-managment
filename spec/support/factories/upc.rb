FactoryBot.define do
  factory :tunecore_upc, class: Upc do
    association :upcable, factory: :album
    tunecore_upc  {true}
    upc_type      {"tunecore"}
  end

  factory :optional_upc, class: Upc do
    association :upcable, factory: :album
    tunecore_upc  {false}
    upc_type      {"optional"}
    number        {"123456789012"}
  end

  factory :physical_upc, class: Upc do
    association :upcable, factory: :album
    tunecore_upc  {false}
    upc_type      {"physical"}
    number        {"123456789012"}
  end
end
