FactoryBot.define do
  factory :person_transaction do
    person
    debit { BigDecimal("100.00") }
    credit { BigDecimal("00.00") }
    previous_balance { BigDecimal("100.00") }
    currency { "USD" }
    target_id { 1234 }
    target_type { "Invoice" }
    comment { "Payment for Invoice 1234" }

    factory :person_transaction_for_person_intake do
      debit { BigDecimal("00.00") }
      credit { BigDecimal("100.00") }
      target_type { "PersonIntake" }
      comment { "Intake" }
    end
  end

  trait :with_irs_tax_withholding do
    after(:create) do |pt, _|
      create(
        :irs_tax_withholding,
        withheld_from_person_transaction_id: pt.id,
        withheld_amount: TaxWithholdings::IRSWithholdingsService::IRS_PERCENTAGE * pt.credit
      )
    end
  end

  trait :taxable_earning_distribution do
    target_type { "PersonIntake" }
  end

  trait :taxable_balance_adjustment do
    debit { BigDecimal("0.0") }
    credit { BigDecimal("100.00") }

    before(:create) do |pt, _|
      bal_adjustment = create(
        :balance_adjustment,
        category: "Songwriter Royalty",
        posted_by: create(:person, :admin),
        posted_by_name: "SYSADMIN(0)"
      )

      pt.update(
        target_type: "BalanceAdjustment",
        target_id: bal_adjustment.id,
      )
    end
  end

  trait :not_meeting_taxable_threshold do
    credit { BigDecimal("0.0") }
  end

  trait :with_rollover_target do
    target_type { "TaxableEarningsRollover" }
  end
end
