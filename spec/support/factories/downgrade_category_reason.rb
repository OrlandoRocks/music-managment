FactoryBot.define do
  factory :downgrade_category_reason do
    name { Faker::Lorem.word }
    downgrade_category { DowngradeCategory.last }
  end
end
