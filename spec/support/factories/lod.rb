FactoryBot.define do
  factory :lod do
    document_guid { '2W4BZGIJELYE5AYJ6ENUCG' }
    template_guid { 'Z83SVPJBYLFAYCTA2HXKI4' }
    last_status { 'scheduled_to_send' }
    last_status_at { Time.current }
    last_status_by { 'admin@tunecore.com' }
  end
end
