FactoryBot.define do
  factory :band_photo do
    person
    width {768}
    size {1024}
    height {1024}
    bit_depth {32}
    mime_type {"image/jpeg"}
    description {"Band photo taken in Paris"}
    file { fixture_file_upload("#{Rails.root}/spec/files/albumcover.jpg", "image/jpeg") }
  end
end
