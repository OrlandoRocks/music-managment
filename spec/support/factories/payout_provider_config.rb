FactoryBot.define do
  factory :payout_provider_config do
    name {'Payoneer'}
    currency {'USD'}
    program_id {'123'}
    username {'TuneCoreXXXX'}
    sandbox {true}
    corporate_entity_id {1}

    trait :for_secondary_taxform_program do
      program_id { PayoutProviderConfig::SECONDARY_TAX_FORM_PROGRAM_ID }
    end
  end
end
