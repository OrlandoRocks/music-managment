FactoryBot.define do
  factory :plan_credit_usage do
    person
    related { build(:album) }
    applies_to_type { "Album" }
  end
end
