FactoryBot.define do
  factory :refund_settlement do
    refund
    settlement_amount_cents { 999 }
    currency { "USD" }

    trait :balance do
      source_id { 1 }
      source_type { "PersonTransactions" }
    end

    trait :braintree do
      association :source, factory: :braintree_transaction
      source_type { "BraintreeTransaction" }
    end
  end
end
