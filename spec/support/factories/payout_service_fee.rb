FactoryBot.define do
  factory :payout_service_fee do
    amount {20}
    fee_type {"check_service"}
    currency {"USD"}
    association :country_website
  end
end
