FactoryBot.define do
  factory :sales_record_master_new do
    sip_store
    period_interval {1}
    period_year {2010}
    period_type {'monthly'}
    period_sort { '2010-01-01' }
    revenue_currency {'USD'}
    country {"US"}
    status {"posted"}
  end
end
