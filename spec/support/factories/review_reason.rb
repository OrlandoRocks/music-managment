FactoryBot.define do
  factory :review_reason do
    reason_type { "REJECTED" }
    reason { "Metadata: Cover Song Formatting (unfinalize release)" }
    review_type { "LEGAL" }
    email_template_path { "album_blocked_from_sales_cover_song_formatting" }
    should_unfinalize { false }
    template_type { }
  end
end
