FactoryBot.define do
  factory :targeted_notification do
    text {"This is an admin created notification"}
    title {"This is a notification title"}
    url {"www.tunecore.com/albums/new"}
    link_text {"Click here to create an album"}
    person
    global {true}
  end
end
