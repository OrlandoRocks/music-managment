FactoryBot.define do
  factory :feature_film do
    name { Faker::Marketing.buzzwords }
    definition { "HD" }
    sale_date { Date.today }
    orig_release_year { Date.today }
    director { "Steven Spielberg" }
    person { person }
    video_type { 'FeatureFilm' }
    accepted_format { true }
  end
end
