FactoryBot.define do
  factory :two_factor_auth do
    person
    authy_id                      {"999999"}
    country_code                  {"001"}
    phone_number                  {"1234567890"}
    notification_method           {"sms"}
    activated_at                  {Date.today - 30.days}
    disabled_at                   {nil}
    last_verified_at              {Date.today}
    last_failed_at                {Date.today - 5.days}

    trait :incomplete do
      authy_id              {nil}
      country_code          {nil}
      phone_number          {nil}
      notification_method   {nil}
      activated_at          {nil}
      last_verified_at      {nil}
      last_failed_at        {nil}
    end

    trait :disabled do
      disabled_at           {Date.today}
    end
  end
end
