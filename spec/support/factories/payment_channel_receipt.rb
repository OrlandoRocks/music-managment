FactoryBot.define do
  factory :payment_channel_receipt do
    receipt_data {"12345abcdef"}
    person
    subscription_purchase
  end
end
