FactoryBot.define do
  factory :distribution_system_itunes_booklet, class: "DistributionSystem::Itunes::BookletFile" do
    asset {"spec/distribution_fixtures/assets/booklet.pdf"}
  end
end
