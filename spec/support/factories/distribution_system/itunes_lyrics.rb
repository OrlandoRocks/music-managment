FactoryBot.define do
  factory :distribution_system_itunes_lyrics, class: "DistributionSystem::Itunes::Lyric" do
    file_name {"song_name.ttml"}
    file_size {522}
    file_checksum {1234456788910}
    content {"lyrics oh yea"}
    language_code {"en"}
    track_title {"song name"}
  end
end
