FactoryBot.define do
  factory :distribution_system_itunes_track, class: "DistributionSystem::Itunes::Track" do
    isrc                  {"1234567890"}
    song_id               { number }
    number                {1}
    title                 { "Track Title #{number}" }
    duration              {100}
    audio_file            {"spec/distribution_fixtures/assets/track.flac"}
    explicit_lyrics       {false}
    album_only            {false}
    original_release_date {"2016-02-01"}
    language_code         {"en"}
    instrumental          {false}

    trait :with_products do
      after(:build) do |track|
        track.products = [build(:distribution_system_itunes_product, sales_start_date: track.original_release_date.to_date)]
      end
    end

    trait :with_lyrics do |track|
      after(:build) do |track|
        track.lyrics = build(:distribution_system_itunes_lyrics)
      end
    end

    trait :with_artists do |track|
      transient do
        artist_count {1}
      end

      after(:build) do |track, evaluator|
        track.creatives = build_list(:distribution_system_itunes_artist, evaluator.artist_count, :songwriter)
      end
    end

    trait :with_instant_grat do
      after(:build) do |track|
        track.instant_grat = true
      end
    end
  end
end
