FactoryBot.define do
  factory :distribution_system_itunes_product, class: "DistributionSystem::Itunes::Product" do
    product_type               {'track'}
    territory                  {'IO'}
    album_wholesale_price_tier {'3'}
    track_wholesale_price_tier {99}
    sales_start_date           {"2016-09-16"}
    cleared_for_sale           {true}
    allow_stream               {true}
    preorder_data              { { preorder_start_date: Date.parse("2016-09-15") } }
  end

  trait :instant_grat_track do
    after(:build) do |product|
      product.track = product.album.tracks.first
      product.track.instant_grat = true
    end
  end
end
