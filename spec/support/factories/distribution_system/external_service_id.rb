FactoryBot.define do
  factory :itunes_external_service_id, class: "ExternalServiceId" do
    identifier { "000000001" }
    service_name { 'apple' }
    linkable_type { 'Creative' }
    state { 'matched' }
  end
end
