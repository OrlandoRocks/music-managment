require_relative "./country"
FactoryBot.define do
  factory :distribution_system_album, class: "DistributionSystem::Believe::Album" do
    language                {"English"}
    customer_name           {"Irwin Fletcher"}
    delivery_type           {"full_delivery"}
    album_type              {"Album"}
    uuid                    {"858c4a2099b60132804612890e781ad7"}
    tunecore_id             {1234567890}
    customer_id             {367972}
    copyright_name          {"Independent"}
    upc                     {"859000000000"}
    sku                     {"A0000000001"}
    title                   {"Album Title"}
    release_date            {"2015-02-15"}
    explicit_lyrics         {false}
    takedown                {false}
    is_various              {false}
    genre                   {15}
    artwork_s3_key          {"spec/distribution_fixtures/assets/cover.jpg"}

    countries               {CountryFactory.all}
    label_name              { copyright_name }
    actual_release_date     { Date.parse(release_date) }
    copyright_pline         { Date.parse(release_date).year.to_s + " " + copyright_name }
    copyright_cline         { copyright_pline }
    deal_list_type          { store_name.downcase }
    audio_codec_type        {"flac"}
    image_size              {nil}
    party_id                {nil}
    party_full_name         {nil}
    ddex_version            {nil}

    after(:build) do |album|
      album.set_data_from_config(StoreDeliveryConfig.for(album.store_name) || album.as_json)
      album.stub(:get_uuid).and_return(album.uuid)
    end

    trait :single do
      album_type {"Single"}
    end

    trait :with_artwork do
      artwork_file  { { asset: artwork_s3_key, bucket: "dummy_bucket" } }
    end

    trait :with_legacy_cover_image do
      artwork_file { DistributionSystem::DDEX::CoverImage.new(asset: artwork_s3_key, bucket: "dummy_bucket") }
    end

    trait :with_booklet do
      booklet_file  { { asset: "spec/distribution_fixtures/assets/booklet.pdf", bucket: "dummy_bucket" } }
    end

    trait :with_tracks do
      transient do
        track_count {1}
        artist_per_track_count {1}
      end

      after(:build) do |album, evaluator|
        album.tracks = []
        evaluator.track_count.times do |i|
          album.tracks << build(:distribution_system_track, :with_artists, number: i + 1, artist_count: evaluator.artist_per_track_count, album: album)
        end
      end
    end

    trait :with_artists do
      transient do
        artist_count {1}
      end

      after(:build) do |album, evaluator|
        album.artists= build_list(:distribution_system_artist, evaluator.artist_count)
      end
    end
  end
end
