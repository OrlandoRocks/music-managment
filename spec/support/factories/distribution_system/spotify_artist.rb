FactoryBot.define do
    factory :spotify_distribution_system_artist, class: "DistributionSystem::Spotify::Artist" do
      name {"Artist Name"}
      roles {["primary"]}
      artist_roles {[]}
      spotify_id {nil}
      artist_id {12345}
  
      trait :is_featured do
        roles { ["featuring"] }
      end
  
      trait :with_artist_roles do
        transient do
          role_types {["songwriter", "performer"]}
        end
  
        artist_roles do
          role_types.map do |r|
            s_role = SongRole.where(role_type: r).first || DDEXRole.where(role_type: r).first
            DistributionSystem::Believe::ArtistRole.new(s_role)
          end
        end
      end
    end
  end
  
