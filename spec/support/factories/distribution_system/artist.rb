FactoryBot.define do
  factory :distribution_system_artist, class: "DistributionSystem::Believe::Artist" do
    name {"Artist Name"}
    roles {["primary"]}
    artist_roles {[]}

    trait :is_featured do
      roles { ["featuring"] }
    end

    trait :with_artist_roles do
      transient do
        role_types {["songwriter", "performer"]}
      end

      artist_roles do
        role_types.map do |r|
          s_role = SongRole.where(role_type: r).first || DDEXRole.where(role_type: r).first
          DistributionSystem::Believe::ArtistRole.new(s_role)
        end
      end
    end
  end

  factory :aod_artist, class: "DistributionSystem::Amazonod::Artist", parent: :artist do
  end
end
