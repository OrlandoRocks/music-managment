FactoryBot.define do
  factory :distribution_system_itunes_artist, class: "DistributionSystem::Itunes::Creative" do
    name            { "Artist Name" }
    apple_artist_id { "123456" }
    roles           { ["Performer", "Composer"] }
    primary         { true }
    id              { 12345 }

    trait :songwriter do |track|
      after(:build) do |track, evaluator|
        track.roles << "Songwriter"
      end
    end
  end
end
