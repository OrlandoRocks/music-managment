FactoryBot.define do
  factory :spotify_bundle, class: "DistributionSystem::Spotify::Bundle" do
    skip_batching {false}
    batch_id {12345}

    transient do
      album
    end
    initialize_with do
      distribution_system = ::DistributionSystem::Deliverer.new(YAML.load_file(Rails.root.join("config", "delivery.yml"))[Rails.env])
      new(distribution_system.work_dir, distribution_system.transcoder, distribution_system.s3_connection, album, StoreDeliveryConfig.for(album.store_name))
    end
  end
end
