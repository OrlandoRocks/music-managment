require_relative "./country"
FactoryBot.define do
  factory :aod_album, class: "DistributionSystem::Amazonod::Album" do
    physical_upc            {"859700933275"}
    store_name              {"AmazonOD"}
    genres                 {["MISCELLANEOUS"]}
    language                {"English"}
    customer_name           {"Irwin Fletcher"}
    delivery_type           {"full_delivery"}
    album_type              {"Album"}
    uuid                    {"858c4a2099b60132804612890e781ad7"}
    tunecore_id             {1234567890}
    customer_id             {367972}
    copyright_name          {"Independent"}
    upc                     {"859000000000"}
    sku                     {"A0000000001"}
    title                   {"Album Title"}
    release_date            {"2015-02-15"}
    explicit_lyrics         {false}
    takedown                {false}
    is_various              {false}
    countries               {CountryFactory.all}
    label_name              { copyright_name }
    actual_release_date     { Date.parse(release_date) }
    copyright_pline         { Date.parse(release_date).year.to_s + " " + copyright_name }
    copyright_cline         { copyright_pline }
    deal_list_type          { store_name.downcase }
    audio_codec_type        {"flac"}
    artwork_s3_key          {"spec/distribution_fixtures/assets/cover.jpg"}

    after(:build) do |album|
      # album.set_data_from_config(StoreDeliveryConfig.for(album.store_name) || {})
      album.stub(:get_uuid).and_return(album.uuid)
      album.products = [build(:aod_product)]
      album.artists = [build(:aod_artist)]
      album.tracks = [build(:aod_track, album: album, artist: album.artists.first)]
    end
  end
end
