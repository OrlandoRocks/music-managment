FactoryBot.define do
  factory :aod_product, class: "DistributionSystem::Amazonod::Product" do
      sales_start_date {"2018-05-01"}
      territory {"US"}
      wholesale_price_tier {"FRONTLINE"}
      sales_end_date {""}
      cleared_for_sale {true}
      aod_retail {8.98}
      takedown {false}
  end
end