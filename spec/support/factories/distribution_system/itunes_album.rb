require_relative "./country"
FactoryBot.define do
  factory :distribution_system_itunes_album, class: "DistributionSystem::Itunes::Album" do
    language_code           {"en"}
    customer_name           {"Irwin Fletcher"}
    delivery_type           {"full_delivery"}
    album_type              {"Album"}
    uuid                    {"858c4a2099b60132804612890e781ad7"}
    tunecore_id             {1234567890}
    customer_id             {367972}
    copyright_name          {"Independent"}
    upc                     {"859000000000"}
    sku                     {"A0000000001"}
    title                   {"Album Title"}
    release_date            {"2015-02-15"}
    explicit_lyrics         {false}
    takedown                {false}
    is_various              {false}
    genre                   {15}
    artwork_s3_key          {"spec/distribution_fixtures/assets/cover.jpg"}
    store_name              {"Itunes"}
    store_id                {36}
    countries               {CountryFactory.all}
    label_name              { copyright_name }
    actual_release_date     { Date.parse(release_date) }
    copyright_pline         { Date.parse(release_date).year.to_s + " " + copyright_name }
    copyright_cline         { copyright_pline }
    audio_codec_type        {"flac"}
    genres                  {["HIP-HOP-RAP-00"]}
    is_approved             { true }

    after(:build) do |album|
      album.stub(:get_uuid).and_return(album.uuid)
    end

    trait :single do
      album_type {"Single"}
    end

    trait :with_products do
      after(:build) do |album|
        album.products = [build(:distribution_system_itunes_product, :instant_grat_track, sales_start_date: album.release_date.to_date, album: album)]
      end
    end

    trait :with_artwork do
      artwork_file  { OpenStruct.new({ asset: artwork_s3_key, bucket: "dummy_bucket" }) }
    end

    trait :with_booklet do
      after(:build) do |album|
        album.booklet_file = build(:distribution_system_itunes_booklet, album: album)
      end
    end

    trait :with_tracks do
      transient do
        track_count {1}
        artist_per_track_count {1}
      end

      after(:build) do |album, evaluator|
        album.tracks = []
        evaluator.track_count.times do |i|
          album.tracks << build(:distribution_system_itunes_track, :with_instant_grat, :with_products, :with_lyrics, :with_artists, number: i + 1, artist_count: evaluator.artist_per_track_count, album: album)
        end
      end
    end

    trait :with_creatives do
      transient do
        artist_count {1}
      end

      after(:build) do |album, evaluator|
        album.creatives = build_list(:distribution_system_itunes_artist, evaluator.artist_count, :songwriter)
      end
    end

    trait :with_new_artist_creatives do
      transient do
        artist_count {1}
      end

      after(:build) do |album, evaluator|
        album.creatives = build_list(:distribution_system_itunes_artist, evaluator.artist_count, :songwriter, name: "Beyoncé", apple_artist_id: "NEW")
      end
    end
  end
end
