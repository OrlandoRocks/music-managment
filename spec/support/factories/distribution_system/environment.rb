FactoryBot.define do
  factory :distribution_system_environment, class: "DistributionSystem::Environment" do

    initialize_with do
      new(YAML.load_file(Rails.root.join("config", "delivery.yml"))[Rails.env])
    end
  end
end
