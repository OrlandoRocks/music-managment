FactoryBot.define do
  factory :distribution_system_track, class: "DistributionSystem::Believe::Track" do
    isrc            {"1234567890"}
    song_id         { number }
    number          {1}
    title           { "Track Title #{number}" }
    duration        {"PT00H00M02S"}
    audio_file      {"spec/distribution_fixtures/assets/track.flac"}
    explicit_lyrics {false}
    album_only      {false}
    temp_filename   { "upc_isrc_track_#{number}" }
    language_code   { 'en' }

    trait :with_artists do |track|
      transient do
        artist_count {1}
      end

      after(:build) do |track, evaluator|
        track.artists = build_list(:distribution_system_artist, evaluator.artist_count)
      end
    end

    trait :with_featured_artist do |track|
      after(:build) do |track|
        track.artists = [build(:distribution_system_artist, :is_featured)]
      end
    end
  end

  factory :aod_track, class: "DistributionSystem::Amazonod::Track" do
    number {"001"}
    song_id         { number }
    audio_file      {"spec/distribution_fixtures/assets/track.flac"}
    title {"AOD track"}
    sku {"S0000000016"}
    isrc {"TC-AAA-17-00016"}
    duration {890}
    explicit_lyrics {true}
    orig_filename {"00000000-c0b0-0b0f-a090-b7616175c96c.m4a"}
    temp_filename {"1921100000000_12354567890.m4a"}
    album_only {true}
    free_song {false}
    asset_url {"some.fake.url"}
    s3_key {"spec/distribution_fixtures/assets/track.flac"}
    s3_bucket {"dummy_bucket"}

    trait :with_artists do |track|
      transient do
        artist_count {1}
      end

      after(:build) do |track, evaluator|
        track.artists = build_list(:distribution_system_artist, evaluator.artist_count)
      end
    end

    trait :with_featured_artist do |track|
      after(:build) do |track|
        track.artists = [build(:distribution_system_artist, :is_featured)]
      end
    end
  end
end
