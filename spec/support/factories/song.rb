FactoryBot.define do
  factory :song do
    name {"This Is A Song"}
    album
    instrumental {false}

    association :lyric_language_code, factory: :language_code
  end

  trait :with_language_code do
    metadata_language_code { create(:language_code) }
  end

  trait :with_upload do
    after(:create) do |song|
      create(:upload, song: song)
    end
  end

  trait :with_s3_asset do
    s3_asset
  end

  trait :with_s3_flac_asset do
    s3_flac_asset factory: :s3_asset

    after(:create) do |song|
      song.s3_flac_asset.key = "s3-flac-asset-key"
      song.save
    end
  end

  trait :with_s3_orig_asset do
    s3_orig_asset factory: :s3_asset

    after(:create) do |song|
      song.s3_flac_asset.key = "s3-orig-asset-key"
      song.save
    end
  end

  trait :with_s3_streaming_asset do
    s3_streaming_asset factory: :s3_asset

    after(:create) do |song|
      song.s3_streaming_asset.key = "s3-streaming-asset-key"
      song.save
    end
  end

  trait :with_optional_isrc do
    optional_isrc {'NLC369800002'}
  end

  trait :with_tunecore_isrc do
    tunecore_isrc {'TUUNECOOORE'}
  end

  trait :with_content_fingerprint do
   content_fingerprint { Faker::Lorem.characters[0..22] }
  end

  trait :with_song_distribution_options do
    after(:create) do |song|
      song.available_for_streaming = true
      song.free_song = false
      song.album_only = false
      create(:upload, song: song)
      song.reload
    end
  end

  trait :with_spotify_song_esi do
    transient do
      song_identifier { Faker::Lorem.characters[0..22] }
    end

    after(:create) do |song, evaluator|
      create(:external_service_id, linkable: song, service_name: 'spotify', identifier: evaluator.song_identifier)
    end
  end
end
