FactoryBot.define do
  factory :api_key do
    key {"abcd1234"}
    partner_name {"Some Company, LLC"}
  end
end
