FactoryBot.define do
  factory :tax_tokens_cohort do
    start_date {Date.today}
    end_date {1.week.from_now}
  end
end
