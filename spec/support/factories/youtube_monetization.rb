FactoryBot.define do
  factory :youtube_monetization do
    agreed_to_terms_at {Time.now.utc}
    person

    trait :with_effecive_date do
      agreed_to_terms_at {Time.now.utc - 1.day}
      effective_date {Time.now.utc - 1.day}
    end

    after(:build) do |youtube_monetization|
      youtube_monetization.person.save
    end
  end
end
