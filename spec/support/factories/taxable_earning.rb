FactoryBot.define do
  factory :taxable_earning do
    credit {4.00}
    revenue_source {"DeezStreams"}
    currency {"USD"}
    person

    trait :old do
      created_at {(Time.current - 2.years).to_s}
    end
  end
end
