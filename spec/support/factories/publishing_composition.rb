FactoryBot.define do
  factory :publishing_composition do

    trait :with_songs do
      after(:create) do |publishing_composition, evaluator|
        album = create(:album, :with_songs)
        publishing_composition.songs << album.songs
      end
    end

    trait :with_non_tunecore_songs do
      after(:create) do |publishing_composition, evaluator|
        non_tunecore_album = create(:non_tunecore_album, :with_non_tunecore_songs)
        publishing_composition.non_tunecore_songs << non_tunecore_album.non_tunecore_songs
      end
    end

    trait :with_recordings do
      transient do
        num_of_recordings { 1 }
      end

      after(:create) do |publishing_composition, evaluator|
        publishing_composition.recordings << create_list(:recording, evaluator.num_of_recordings, publishing_composition: publishing_composition)
      end
    end
  end
end
