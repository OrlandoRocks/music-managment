FactoryBot.define do
  factory :subscription_product do
    product
    term_length {1}
  end
end
