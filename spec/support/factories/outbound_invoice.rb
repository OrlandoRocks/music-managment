FactoryBot.define do
  factory :outbound_invoice do
    person
    vat_tax_adjustment_id { vat_tax_adjustment_id }
    related_id { related_id }
    related_type { related_type }
    invoice_sequence { 1 }
    user_invoice_prefix { "BI-INV#" }
    invoice_date { DateTime.now }
  end
end
