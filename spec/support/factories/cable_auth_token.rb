FactoryBot.define do
  factory :cable_auth_token do
    person
    expires_at { Date.today + 4 }
    token { "sample-token" }
  end
end
