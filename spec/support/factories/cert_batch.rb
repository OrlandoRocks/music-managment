FactoryBot.define do
  factory :cert_batch do
    promotion { Faker::Lorem.words(number: 5) }
    description { Faker::Lorem.words(number: 7) }
    cert_engine {"DefaultPercent"}
    engine_params {25}
    expiry_date {1.year.from_now}
    spawning_code {1235}
    admin_only {false}
    brand_code {''}
  end
end
