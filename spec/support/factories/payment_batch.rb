FactoryBot.define do
  factory :payment_batch do
    country_website
    currency { "USD" }
    batch_date { Date.today }
  end
end
