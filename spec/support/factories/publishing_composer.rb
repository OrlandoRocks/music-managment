FactoryBot.define do
  factory :publishing_composer do
    account
    person
    first_name {"Waylon"}
    last_name {"Jennings"}
    agreed_to_terms_at {"01/01/2020"}
    dob {"01/01/1970"}
    is_primary_composer {true}

    trait :cowriter do
      is_primary_composer {false}
    end

    trait :terminated do
      terminated_at {Date.today}
    end

    trait :active do
      
      performing_rights_organization
      cae {"123456789"}
    end

    trait :with_related_purchases do
      after(:create) do |publishing_composer|
        publishing_composer.related_purchases << create(:purchase, related_type: "Purchase", person_id: publishing_composer.person_id, related_id: publishing_composer.person_id)
      end
    end

    trait :with_pub_admin_purchase do
      after(:create) do |publishing_composer|
        publishing_composer.related_purchases << create(
          :purchase,
          related_type: "PublishingComposer",
          related_id:   publishing_composer.id,
          person_id:    publishing_composer.account.person_id,
          invoice_id:   create(:invoice, person: publishing_composer.account.person),
          paid_at:      Date.today,
          product_id:   Product.find_products_for_country(
            publishing_composer.account.person.country_domain, :songwriter_service
          ).first
        )
      end
    end

    trait :with_lod do
      after(:create) do |publishing_composer|
        publishing_composer.lod = create(:lod)
        publishing_composer.save
      end
    end

    trait :with_provider_identifier do
      provider_identifier {"00001C8GW"}
    end

    trait :with_publisher do
      publisher
    end

    trait :unknown do
      first_name {''}
      last_name {''}
      is_unknown {true}
      is_primary_composer {false}
    end

    trait :skip_cae_validation do
      skip_cae_validation {true}
    end

    trait :primary do
      is_primary_composer {true}
    end
  end
end
