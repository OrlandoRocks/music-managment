FactoryBot.define do
  factory :braintree_vault_transaction do
    person
    related_id {1}
    related_type {"StoredCreditCard"}
    response_code {"response"}
    ip_address {"192.168.2.1"}
    raw_response {"I make sure everything remains raw"}
  end
end
