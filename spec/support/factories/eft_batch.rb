FactoryBot.define do
  factory :eft_batch do
    status {'new'}
    batch_sent_at {Date.today}
  end

  trait :processed do
    response_processed_at {Time.now}
  end

  trait :unprocessed do
    response_processed_at {nil}
  end

  trait :uploaded do
    status {'uploaded'}
  end

  trait :upload_confirmed do
    status {'upload_confirmed'}
  end
end
