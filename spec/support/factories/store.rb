FactoryBot.define do
  factory :store do
    sequence(:name) { |n| "Test Store #{n}" }
    sequence(:abbrev) { |n| "#{n}" }
    sequence(:short_name) { |n| "Store #{n}" }
    position {1}
    needs_rights_assignment {false}
    is_active {true}
    base_price_policy_id {3}
    is_free {false}
    launched_at {Time.now}
    on_sale {false}
    in_use_flag {true}
  end

  trait :tc_distributor do
    delivery_service {"tc_distributor"}
  end

  trait :tunecore do
    delivery_service {"tunecore"}
  end

end
