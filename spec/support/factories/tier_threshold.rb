FactoryBot.define do
  factory :tier_threshold do
    tier
    lte_min_amount { Faker::Number.between(from: 1000, to: 9999) }
    min_release { Faker::Number.non_zero_digit }
    country { Country.find_by(iso_code: "US") } # Countries are pre-created in specs.
  end
end
