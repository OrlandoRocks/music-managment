FactoryBot.define do
  factory :purchase_tax_information do
    association :purchase, factory: [:purchase, :with_variable_related_item]
  end

  trait :with_vat do
    tax_type { "VAT" }
    tax_rate { 17.0 }
  end

  trait :without_vat do
    tax_type { "N/A" }
    tax_rate { 0.0 }
  end

  trait :failed do
    with_vat
    error_code { "Savon::SOAPFault:  (soap:Server) MS_UNAVAILABLE " }
  end

  trait :failure_handled do
    failed
    note { "VatInformation: 1021" }
  end
end
