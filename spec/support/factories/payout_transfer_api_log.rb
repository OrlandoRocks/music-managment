FactoryBot.define do
  factory :payout_transfer_api_log do
    association :payout_transfer
    kind { "response" }
    url { "http://development.tunecore.local:5000/admin/payout/batch_actions" }
    body {
      <<-JSON
      {
        "audit_id": 212139064,
        "code":0,
        "description": "Success",
        "payout_id": "1641697280",
        "amount": 400.0,
        "currency":"USD"
      }
      JSON
    }
  end

  trait :request do
    kind { "request" }
    body {
      <<-RBSTR
        {
          :method => :post,
          :body => "{\"payee_id\":\"eedc654f-936a-4c2e-8c2f-ed7da14e9c97\",\"amount\":\"20.00\",\"client_reference_id\":\"6558b393-9009-4bbf-8bf1-10fcee7b627c\",\"description\":\"TuneCore payout transaction\",\"group_id\":87}",
          :headers=>{
            "User-Agent" => "Faraday v0.17.0",
            "Content-Type"=>"application/json",
            "Authorization"=>"Basic VHVuZUNvcmU0OTgwOlR1bmU0OSQ4MA=="
          },
          :path => "/v2/programs/100084980/payouts",
          :params=>{},
          :options => #<Faraday::RequestOptions (empty)>
        }
      RBSTR
    }
  end

  trait :response do
    kind { "response" }
    body {
      <<-JSON
      {
        "audit_id": 212139064,
        "code":0,
        "description": "Success",
        "payout_id": "1641697280",
        "amount": 400.0,
        "currency":"USD"
      }
      JSON
    }
  end
end
