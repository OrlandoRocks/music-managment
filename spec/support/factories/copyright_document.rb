FactoryBot.define do
  factory :copyright_document do
    association :related, factory: :album
  end
end
