FactoryBot.define do
  factory :encumbrance_summary do
    person
    reference_id { SecureRandom.uuid }
    reference_source {"LYRIC"}
    total_amount {500.00}
    outstanding_amount { total_amount }
    fee_amount {50.00}
  end
end
