FactoryBot.define do
  factory :facebook_recognition_service_purchase do
    person
    effective_date {Time.now}
    termination_date {Time.now + 2.weeks}
  end
end
