FactoryBot.define do
  factory :distribution_api_song do
    source_song_id { Faker::Lorem.words(number: 6) }
    song
    distribution_api_album
  end
end
