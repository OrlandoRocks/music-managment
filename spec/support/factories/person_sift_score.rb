FactoryBot.define do
  factory :person_sift_score do
    person
    score      {0.5}
    sift_order_id    {"default_sift_order_id"}
    created_at  {Date.current}
  end
end
