FactoryBot.define do
  factory :paypal_transfer do
    person
    person_transaction
    transfer_status {'pending'}
    paypal_address {'paypal_transfer_1234@tunecore.com'}
    paypal_address_confirmation {'paypal_transfer_1234@tunecore.com'}
    payment_cents {4500}
    currency {'USD'}
    country_website_id {6}
    admin_charge_cents {300}
    password_entered {'Testpass123!'}
    current_balance_cents {10000}
  end

  after(:build) do |paypal_transfer|
    if paypal_transfer.class == PersonTransaction
      paypal_transfer.person.save
    end
  end

  trait :with_person do
    transient do
      country_code { "US" }
      corporate_entity_id { 1 }
    end

    before(:create) do |paypal_transfer, evaluator|
      person = create(:person, country: evaluator.country_code)
      person.update(corporate_entity_id: evaluator.corporate_entity_id)
      paypal_transfer.person = person
    end
  end

  trait :other_country do
    after(:create) do |paypal_transfer, evaluator|
      paypal_transfer.currency  = evaluator.currency
      paypal_transfer.person    = evaluator.person
    end
  end
end
