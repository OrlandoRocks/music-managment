FactoryBot.define do
  factory :person_intake_sales_record_master do
    person_intake
    sales_record_master
  end
end
