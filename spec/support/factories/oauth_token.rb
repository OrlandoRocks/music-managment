FactoryBot.define do
  factory :oauth_token do
    association :user, factory: :person
    client_application_id { 1 }
    token { "1234567" }
    secret { "35gfgsfg3t35gtrgsf" }
  end

  trait :for_tc_social do
    client_application_id { 3 }
  end
end
