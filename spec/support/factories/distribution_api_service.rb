FactoryBot.define do
  factory :distribution_api_service do
    name { Faker::Name.name }
  end
end
