FactoryBot.define do
  factory :tax_form do
    tax_form_type { TaxFormType.w9_types.first }
    provider { PayoutProvider::PAYONEER }
    submitted_at { Date.today }
    expires_at   { Date.today + 3.years }
    person

    trait :with_revenue_streams do
      transient do
        revenue_stream_codes { [:distribution, :publishing] }
        user_mapped_at { nil }
      end

      after(:create) do |tax_form, evaluator|
        evaluator.revenue_stream_codes.each do |code|
          tax_form.tax_form_revenue_streams.create(
            tax_form: tax_form,
            person: tax_form.person,
            revenue_stream: RevenueStream.find_by(code: code),
            user_mapped_at: evaluator.user_mapped_at
          )
        end
      end
    end

    trait :w9 do
      tax_form_type { TaxFormType.w9_types.first }
    end

    trait :w8 do
      tax_form_type { TaxFormType.w8_types.first }
    end

    trait :w9_individual do
      tax_form_type_id { 2 }
      tax_entity_name { person.name }
      payout_provider_config { PayoutProviderConfig.by_env.find_by(program_id: PayoutProviderConfig::TC_US_PROGRAM_ID) }
    end

    trait :w9_entity do
      tax_form_type_id { 3 }
      tax_entity_name { "#{person.name} Inc" }
      payout_provider_config { PayoutProviderConfig.by_env.find_by(program_id: PayoutProviderConfig::TC_US_PROGRAM_ID) }
    end

    trait :current_active_individual_w9 do
      tax_form_type_id { 2 }
      payout_provider_config
    end
  end
end
