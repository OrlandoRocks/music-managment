
FactoryBot.define do
  factory :cert do
    cert {'DEFAULT_CERT'}
    cert_engine {'DefaultPercent'}
    expiry_date {30.days.from_now}
    engine_params {50}
    admin_only {false}

    trait :spawning_percent_single do
      cert            {"DISCOUNT_SINGLE"}
      cert_engine     {"SpawningPercentSingle"}
      engine_params   {"20"}
      admin_only      {false}
    end

    trait :spawning_percent_album do
      cert            {"DISCOUNT_ALBUM"}
      cert_engine     {"SpawningPercentAlbum"}
      engine_params   {"20"}
      admin_only      {false}
    end

    trait :spawning_percent_distribution_all do
      cert            {"DISTRIBUTION_ALL"}
      cert_engine     {"SpawningPercentDistributionAll"}
      engine_params   {"20"}
      admin_only      {false}
    end

    trait :spawning_percent_distribution_cr do
      cert            {"DISTRIBUTION_CR"}
      cert_engine     {"SpawningPercentDistributionCr"}
      engine_params   {"20"}
      admin_only      {false}
    end

    trait :spawning_default_single_use do
      cert            {"DISTRIBUTION_ALL"}
      cert_engine     {"SpawningAllDistribution"}
      engine_params   {"20"}
      admin_only      {false}
    end
  end
end
