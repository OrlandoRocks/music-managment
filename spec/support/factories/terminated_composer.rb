FactoryBot.define do
  factory :terminated_composer do
    composer
    effective_date {DateTime.now}
  end

  trait :fully do
    termination_type {"fully"}
  end

  trait :partially do
    termination_type {"partially"}
  end
end
