
FactoryBot.define do
  factory :album do
    name { Faker::Book.title[1..15] }
    created_on {Date.today}
    sale_date {Date.today}
    creatives {[{name: Faker::Name.name, role: "primary_artist" }.with_indifferent_access]}
    is_various {false}
    primary_genre {Genre.active.first}
    person
    orig_release_year {nil}
    created_with {"songwriter"}
    language_code {"en"}
    golive_date {(Time.now + 1.days)}
    timed_release_timing_scenario {'absolute_time'}
    recording_location {nil}
    after(:create) do |album|
      album.update(label_name: album.artist_name)
    end

    association :metadata_language_code, factory: :language_code

    trait :finalized do
      finalized_at {Time.now}
    end

    trait :is_deleted do
      is_deleted {true}
    end

    trait :payment_applied do
      payment_applied {true}
    end

    trait :paid do
      finalized
      payment_applied
    end

    trait :approved do
      paid
      after(:create) do |album, e|
        album.update(legal_review_state: "APPROVED")
      end
    end

    trait :rejected do
      paid
      after(:create) do |album, e|
        album.update(legal_review_state: "REJECTED")
      end
    end

    trait :pending_approval do
      paid
      after(:create) do |album, _|
        album.update(legal_review_state: "NEEDS REVIEW")
      end
    end

    trait :live do
      finalized
      known_live_on {Time.now}
      takedown_at {nil}
    end

    trait :with_one_year_renewal do
      live
      after(:create) do |album|
        product          = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
        purchase         = FactoryBot.create(:purchase, person_id: album.person.id, related: album)
        renewal          = FactoryBot.create(:renewal, person_id: album.person.id, item_to_renew: product.product_items.first, purchase: purchase)
        renewal_item     = FactoryBot.create(:renewal_item, renewal: renewal, related_id: album.id, related_type: "Album")
      end
    end

    trait :with_one_year_renewal_expired do 
      with_one_year_renewal
      after(:create) do |album|
        album.renewal.renewal_history.first.update_attribute("expires_at", Date.today - (Renewal::GRACE_DAYS_BEFORE_TAKEDOWN + 3))
      end
    end

    trait :with_one_month_renewal do
      live
      after(:create) do |album|
        product          = Product.find_by(name: "Monthly Album")
        purchase         = FactoryBot.create(:purchase, person_id: album.person.id, related: album)
        renewal          = FactoryBot.create(:renewal, person_id: album.person.id, item_to_renew: product.product_items.first, purchase: purchase)
        renewal_item     = FactoryBot.create(:renewal_item, renewal: renewal, related_id: album.id, related_type: "Album")
      end
    end

    trait :with_one_month_renewal_expired do
      with_one_month_renewal
      after(:create) do |album|
        album.renewal.renewal_history.first.update_attribute("expires_at", Date.today - (Renewal::GRACE_DAYS_BEFORE_TAKEDOWN_MONTHLY + 3))
      end
    end

    trait :taken_down do
      finalized
      known_live_on {Time.local(2014)}
      renewal_due_on {Time.local(2015)}
      takedown_at {Time.local(2015)}
    end

    trait :with_songs do
      transient do
        number_of_songs {1}
      end

      after(:create) do |album, evaluator|
        album.songs << create_list(:song, evaluator.number_of_songs, album: album)
        album.save
        album.reload
      end
    end

    trait :with_uploaded_song do
      after(:create) do |album, evaluator|
        album.songs << create(:song, :with_upload, album: album)
        album.save
        album.reload
      end
    end

    trait :with_streaming_distribution do
      after(:create) do |album|
        distro = create(:distribution, :delivered_to_streaming)
        distro.petri_bundle.update_attribute(:album, album)
      end
    end

    trait :with_apple_distribution do
      transient do
        via_distributor { false }
      end

      after(:create) do |album, e|
        distro = create(:distribution, e.via_distributor ? :delivered_to_apple_via_distributor : :delivered_to_apple)
        distro.petri_bundle.update_attribute(:album, album)
        album.salepoints = distro.salepoints
      end
    end

    trait :with_spotify_distribution do
      transient do
        via_distributor { false }
      end

      after(:create) do |album, e|
        distro = create(:distribution, e.via_distributor ? :delivered_to_spotify_via_distributor : :delivered_to_spotify)
        distro.petri_bundle.update_attribute(:album, album)
        album.salepoints = distro.salepoints
      end
    end

    trait :with_uploaded_songs do
      transient do
        number_of_songs {1}
      end

      after(:create) do |album, evaluator|
        album.songs << create_list(:song, evaluator.number_of_songs, :with_s3_asset, :with_upload, :with_language_code, album: album)
        album.save
        album.reload
      end
    end

    trait :with_uploaded_songs_esi do
      transient do
        number_of_songs {2}
      end

      after(:create) do |album, evaluator|
        album.songs << create_list(:song, evaluator.number_of_songs, :with_s3_asset, :with_upload, :with_language_code, :with_spotify_song_esi, album: album)
        album.save
        album.reload
      end
    end

    trait :with_multiple_uploaded_songs do
      transient do
        number_of_songs {3}
      end

      after(:create) do |album, evaluator|
        album.songs << create_list(:song, evaluator.number_of_songs, :with_upload, :with_language_code, album: album)
        album.save
        album.reload
      end
    end

    trait :with_apple_music do
      live_in_itunes
      apple_music = 1

      after(:create) do |album, evaluator|
        album.external_service_ids << ExternalServiceId.new(service_name: 'apple', identifier: '1000000')
      end
    end

    trait :live_in_itunes do
      apple_identifier = "1000000000"
    end

    trait :with_salepoints do
      transient do
        abbreviation {["fb", "gn", "sz", "sp"]}
      end

      after(:create) do |album, evaluator|
        salepoints = []
        evaluator.abbreviation.each do |store_abbr|
          store = Store.find_by(abbrev: store_abbr)
          FactoryBot.create(:salepointable_store, store: store)
          salepoints << create(:salepoint, salepointable: album, store: store)
        end
        album.salepoints = salepoints
      end
    end

    trait :social_only do
      after(:create) do |album, evaluator|
          store = Store.find_by(abbrev: "yttm")
          album.salepoints = [create(:salepoint, salepointable: album, store: store)]
      end
    end

    trait :with_artwork do
      after(:create) do |album|
        create(:artwork, album: album)
      end
    end

    trait :with_booklet do
      after(:create) do |album|
        create(:booklet, album: album)
      end
    end

    trait :with_uploaded_song_and_artwork do
      with_uploaded_songs
      with_artwork
    end

    trait :with_uploaded_songs_and_artwork do
      with_uploaded_songs
      with_artwork
    end

    trait :with_uploaded_songs_and_artwork_and_esi do
      with_uploaded_songs_esi
      with_artwork
    end

    trait :with_uploaded_songs_and_content_fingerprint do
      transient do
        number_of_songs {1}
      end

      after(:create) do |album, evaluator|
        album.songs << create_list(:song, evaluator.number_of_songs, :with_s3_asset, :with_upload, :with_language_code, :with_content_fingerprint, album: album)
        album.save
        album.reload
      end
    end

    trait :with_uploaded_song_distribution_option do
      transient do
        number_of_songs {1}
      end

      after(:create) do |album, evaluator|
        album.songs << create_list(:song, evaluator.number_of_songs, :with_song_distribution_options, album: album)
        album.save
        album.reload
      end
    end

    trait :with_petri_bundle do
      after(:create) do |album|
        create(:petri_bundle, album: album)
      end
    end

    trait :with_salepoint_song do
      with_uploaded_song

      after(:create) do |album|
        salepoint = create(:salepoint, store: Store.find_by(short_name: "YoutubeSR"), salepointable: album)
        song      = album.songs.first
        create(:salepoint_song, song: song, salepoint: salepoint)
        create(:petri_bundle, album: album)
        album.salepoints << salepoint
      end
    end

    trait :with_petri_bundle do
      after(:create) do |album|
        create(:petri_bundle, album: album)
      end
    end

    trait :with_songwriter do
      after(:create) do |album|
        album.songs.each do |song|
          create(:creative_song_role, creative: album.creatives.first, song: song)
        end
      end
    end

    trait :purchaseable do
      with_salepoints
      with_uploaded_song_and_artwork
      with_songwriter
    end

    trait :with_spotify_album_esi do
      transient do
        alb_identifier {Faker::Lorem.characters[0..22]}
      end
      after(:create) do |album, evaluator|
        create(:external_service_id, linkable: album, service_name: 'spotify', identifier: evaluator.alb_identifier)
      end
    end

    trait :with_apple_album_esi do
      transient do
        alb_identifier {Faker::Lorem.characters[0..22]}
      end
      after(:create) do |album, evaluator|
        create(:external_service_id, linkable: album, service_name: 'apple', identifier: evaluator.alb_identifier)
      end
    end

    trait :with_creative_song_roles do
      transient do
        cr_artist_roles { [ {artist: build(:artist, name: Faker::Name.name), role: 'remixer'}] }
      end
      after(:create) do |album, evaluator|
        evaluator.cr_artist_roles.each do |art_info|
          song = album.songs.first
          song_creative = create(:creative,
                            artist: art_info[:artist],
                            role: 'contributor',
                            creativeable: song
                          )
          create(:creative_song_role,
            creative: song_creative,
            song: song,
            song_role: SongRole.find_by(role_type: art_info[:role])
          )
        end
      end
    end

    trait :with_song_songwriters do
      transient do
        cr_artist_roles { [ {artist: build(:artist, name: Faker::Name.name), role: 'songwriter'}] }
      end
      after(:create) do |album, evaluator|
        evaluator.cr_artist_roles.each do |art_info|
          album.songs.each do |song|
            song_creative = create(:creative,
                              artist: art_info[:artist],
                              role: 'contributor',
                              creativeable: song
                            )
            create(:creative_song_role,
              creative: song_creative,
              song: song,
              song_role: SongRole.find_by(role_type: art_info[:role])
            )
          end
        end
      end
    end

    trait :with_creatives do
        transient do
          artist_roles { [ {artist: build(:artist, name: Faker::Name.name), role: 'primary_artist'}] }
        end

        after(:create) do |album, evaluator|
          evaluator.artist_roles.each do |art_info|
            create(:creative,
              creativeable: album,
              artist: art_info[:artist],
              role: art_info[:role]
            )
          end
        end
    end

    trait :with_song_copyrights do
      transient do
        number_of_songs {1}
      end

      after(:create) do |album, evaluator|
        album.songs << create_list(:song, evaluator.number_of_songs, album: album)
        album.save

        album.songs.each do |song|
          song.copyrights << Copyright.ownerships.values.map{ |ownership| create(:copyright, copyrightable: song, ownership: ownership) }
          song.save
        end

        album.reload
      end
    end

    trait :tiktok_release do
      abbreviation {["byda"]}
      with_salepoints
    end

    trait :freemium_release do
      abbreviation {["reels"]}
      with_salepoints
    end

    trait :without_songwriter do
      created_with { nil }
    end

    trait :ringtone do
      album_type { 'Ringtone' }
      created_with { nil }
    end

    trait :with_distribution_api_album do
      distribution_api_album
    end
  end
end
