FactoryBot.define do
  factory :transition do
    association :state_machine, factory: :distribution
  end
end
