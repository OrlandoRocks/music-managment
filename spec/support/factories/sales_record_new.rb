FactoryBot.define do
  factory :sales_record_new do
    person
    sales_record_master
    association :related, factory: :song
    distribution_type {"Download"}
    quantity {1}
    revenue_per_unit {0.7}
    revenue_total {0.7}
    amount {0.7}
  end
end
