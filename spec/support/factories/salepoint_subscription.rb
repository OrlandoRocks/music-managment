FactoryBot.define do
  factory :salepoint_subscription do
    album
    is_active {true}
    effective {Time.now}
  end
end
