FactoryBot.define do
  factory :refund_reason do
    reason { "Services not received" }
    visible { true }
  end
end
