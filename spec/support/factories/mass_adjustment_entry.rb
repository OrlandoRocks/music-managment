FactoryBot.define do
  factory :mass_adjustment_entry do
    amount {30000}
    balance_turns_negative {false}
    person
    mass_adjustment_batch
    status {nil}
  end
end
