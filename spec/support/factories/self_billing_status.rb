FactoryBot.define do
  factory :self_billing_status do
    person
    status {"accepted"}
  end
end