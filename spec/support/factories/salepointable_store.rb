FactoryBot.define do
  factory :salepointable_store do
    salepointable_type {"Album"}
    store
  end
end
