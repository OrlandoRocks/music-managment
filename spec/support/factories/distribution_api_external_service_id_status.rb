FactoryBot.define do
  factory :distribution_api_external_service_id_api_status do
    album
    distribution_api_service
  end
end
