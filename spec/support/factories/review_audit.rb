FactoryBot.define do
  factory :review_audit do
    person
    album
    event {"STARTED REVIEW"}
  end

  trait :approved do
    after(:build) do |audit|
      audit.review_reasons << create(:review_reason)
      audit.event = "APPROVED"
      audit.save(validate: true)
    end
  end

  trait :rejected do
    after(:build) do |audit|
      review_reason = create(:review_reason, template_type: "changes_requested")
      review_reason.roles << audit.person.roles
      audit.review_reasons << review_reason
      audit.event = "REJECTED"
      audit.save(validate: false)
    end
  end
end
