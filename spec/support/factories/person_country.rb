FactoryBot.define do
  factory :person_country do
    person
    country { Country.find_by(iso_code: 'US') } # Countries are pre-created in specs.
    source { 'payoneer' }
  end
end
