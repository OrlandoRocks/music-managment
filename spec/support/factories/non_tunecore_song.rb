FactoryBot.define do
  factory :non_tunecore_song do
    non_tunecore_album
    name {Faker::Book.title}
    composition
    publishing_composition
  end

  trait :with_tunecore_song do
    song
  end
end
