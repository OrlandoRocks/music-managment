FactoryBot.define do
  factory :note do
    note { Faker::Job.title }
    related { FactoryBot.create(:album) }
    subject { Faker::Job.title }
    note_created_by { FactoryBot.create(:person) }
  end
end