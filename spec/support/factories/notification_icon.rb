FactoryBot.define do
  factory :notification_icon do
    file do
      fixture_file_upload(
        Rails.root.join('spec', 'assets', 'cover.jpg'),
        'image/jpeg'
      )
    end
  end
end
