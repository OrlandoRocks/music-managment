FactoryBot.define do
  factory :dispute_status do
    status { "Open" }

    trait :braintree do
      source_type { :braintree }
    end

    trait :paypal do
      source_type { :paypal }
    end

    trait :paypal do
      source_type { :payu }
    end
  end
end
