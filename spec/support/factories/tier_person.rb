FactoryBot.define do
  factory :tier_person do
    tier
    person
    status { 'active' }
  end
end
