FactoryBot.define do
  factory :knowledgebase_link do
    sequence(:article_id, &:to_s)
    sequence(:article_slug) { |n| "factory-article#{n}" }
  end
end