FactoryBot.define do
  factory :manual_check_transfer do
    person { create(:another_person, :with_balance, amount: 50_000) }
    payee {"Rick Sanchez"}
    transfer_status {"processing"}
    city {"New York"}
    country {"United States"}
    address1 {"45 Main Street"}
    payment_amount {"200.00"}
    state {"NY"}
    password_entered {'Testpass123!'}
  end
end
