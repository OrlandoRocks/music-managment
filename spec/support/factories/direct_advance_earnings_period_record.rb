FactoryBot.define do
  factory :direct_advance_earnings_period_record, :class => DirectAdvance::EarningsPeriodRecord do
    person
    amount          {BigDecimal.new(100)}
    period          {"month_1"}
    report_run_on   {Date.today.to_s}

    trait :eligible_collection do
      period {"month_1"}
      amount {BigDecimal.new(100)}

      after(:create) do |report|
        other_periods = %w(month_2 month_3 month_4 month_5 month_6 month_7 month_8 month_9 month_10 month_11 month_12 month_13 all_prev_months)
        other_periods.each do |period|
          create(:direct_advance_earnings_period_record, period: period, amount: report.amount, person: report.person, report_run_on: report.report_run_on)
        end
      end
    end

    trait :ineligible_collection do
      period {"month_1"}
      amount {BigDecimal.new(40)}

      after(:create) do |report|
        other_periods = %w(month_2 month_3 month_4 month_5 month_6 month_7 month_8 month_9 month_10 month_11 month_12 month_13 all_prev_months)
        other_periods.each do |period|
          create(:direct_advance_earnings_period_record, period: period, amount: report.amount, person: report.person, report_run_on: report.report_run_on)
        end
      end
    end
  end
end
