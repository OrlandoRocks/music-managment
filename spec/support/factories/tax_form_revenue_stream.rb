FactoryBot.define do
  factory :tax_form_revenue_stream do
    tax_form
    revenue_stream
  end
end
