FactoryBot.define do
  factory :iterable_campaign do
    campaign_id { Faker::Number::positive }
    environment { "SANDBOX" }
    name { IterableCampaign::campaign_names.flat_map { |_, v| v }.sample }
  end

  trait :with_invalid_name do
    name { Faker::Alphanumeric::alphanumeric }
  end
end
