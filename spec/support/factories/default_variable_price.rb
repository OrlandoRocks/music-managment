FactoryBot.define do
  factory :default_variable_price do
    store
    variable_prices_store { variable_prices_store(store: store) }
  end
end
