FactoryBot.define do
  factory :compliance_info_field do
    person
    field_name {"first_name"}
    field_value {"greggy"}
  end
end
