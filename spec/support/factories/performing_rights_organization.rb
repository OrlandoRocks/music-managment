FactoryBot.define do
  factory :performing_rights_organization do
    name {"ACAM"}
    provider_identifier {"39e6a3a9-52db-b1d1-ffad-f709c4568830"}
  end
end
