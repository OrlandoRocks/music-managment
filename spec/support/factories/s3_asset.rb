FactoryBot.define do
  factory :s3_asset do
    key {"s3-asset-key"}
    bucket {"s3.asset.bucket"}
    uuid {SecureRandom.uuid}
  end
end
