FactoryBot.define do
  factory :artwork do
    height {1600}
    width {1600}
    uploaded {true}
    album
  end
end
