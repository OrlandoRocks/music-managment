FactoryBot.define do
  factory :inventory do
    person
    title {"Inventory"}
    product_item {ProductItem.find(25)}
    purchase
    inventory_type {"Inventory"}

    factory :salepoint_inventory do
      title {"Salepoint"}
      inventory_type {"Salepoint"}
    end

    factory :album_credit_inventory do
      title {"Album Distribution"}
      inventory_type {"Album"}
    end
  end
end
