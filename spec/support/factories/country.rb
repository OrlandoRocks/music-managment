FactoryBot.define do
  factory :country do
    name {"Djibouti"}
    iso_code {"DJ"}
    region {"Africa"}
    sub_region {"Eastern Africa"}
    tc_region {"EMEA"}
    iso_code_3 {"DJI"}
    is_sanctioned {0}
    corporate_entity_id {2}
  end
end
