FactoryBot.define do
  factory :distribution_api_ingestion_log do
    json_path { Faker::Lorem.words(number: 10) }
    source_album_id { Faker::Lorem.words(number: 6) }
    message_id { Faker::Lorem.words(number: 6) }
  end
end
