FactoryBot.define do
  factory :sync_membership_request do
    email {"johndoe@doe.com"}
    website {"www.johndoe.com"}
    name {"John Doe"}
    request_code {"abc123"}
    status {"pending"}
    company {"John Doe LLC"}
    country {"United States"}
    phone_number {"866-200-1234"}
  end
end
