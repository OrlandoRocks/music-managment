FactoryBot.define do
  factory :invoice do
    person
    final_settlement_amount_cents {0}
    currency {"USD"}
    batch_status {"visible_to_customer"}
    vat_amount_cents { 0 }
    settled_at {nil}
    foreign_exchange_pegged_rate_id {nil}
    gst_config_id {nil}
    corporate_entity_id { 1 }

    trait :with_purchase do
      transient do
        related {build(:album)}
      end

      after(:create) do |invoice, evaluator|
        invoice.purchases << create(:purchase, person: invoice.person, invoice: invoice,
                                    related_type: evaluator.related.class.name, related_id: evaluator.related.id)
        invoice.final_settlement_amount_cents = invoice.purchases.inject(0){ |sum, x| sum + x.cost_cents }
        invoice.save
        invoice.reload
      end
    end

    trait :with_purchase_indian_product do
      transient do
        related { Product.find(419) }
      end

      after(:create) do |invoice, evaluator|
        invoice.purchases << create(
          :purchase,
          :indian_product,
          person: invoice.person,
          invoice: invoice,
          related_type: evaluator.related.class.name,
          related_id: evaluator.related.id
        )

        invoice.final_settlement_amount_cents = invoice.purchases.inject(0){ |sum, x| sum + x.cost_cents }
        invoice.save
        invoice.reload
      end
    end

    trait :with_two_purchases do
      after(:create) do |invoice|
        album_1 = create(:album, person: invoice.person)
        album_2 = create(:album, person: invoice.person)
        purchase_1 = create(:purchase, person: invoice.person, invoice: invoice,
                            related_type: album_1.class.name, related_id: album_1.id)
        purchase_2 = create(:purchase, person: invoice.person, invoice: invoice,
                            related_type: album_2.class.name, related_id: album_2.id)
        invoice.purchases << purchase_1
        invoice.purchases << purchase_2
        invoice.final_settlement_amount_cents = invoice.purchases.inject(0){ |sum, x| sum + x.cost_cents }
        invoice.save
        invoice.reload
      end
    end

    trait :with_every_attribute_purchase do
      # this is a frankenstein purchase that probably couldn't actually exist but we're using for the purposes
      # of testing invoice logs
      transient do
        related {build(:album)}
      end

      after(:create) do |invoice, evaluator|
        salepoint = create(:salepoint)
        no_purchase_album = create(:album, person: invoice.person)
        targeted_offer = create(:targeted_offer)
        product = create(:product, :album_renewal)
        targeted_product = create(:targeted_product, product: product, targeted_offer: targeted_offer)
        invoice.purchases << create(:purchase, person: invoice.person, invoice: invoice,
                                    related_type: evaluator.related.class.name, related_id: evaluator.related.id,
                                    salepoint_id: salepoint.id, no_purchase_album_id: no_purchase_album.id,
                                    targeted_product_id: targeted_product.id, vat_amount_cents: 500
                                    )
        invoice.final_settlement_amount_cents = invoice.purchases.inject(0){ |sum, x| sum + x.cost_cents }
        invoice.save
        invoice.reload
      end
    end

    trait :settled do
      after(:create) do |invoice|
        album = create(:album, person: invoice.person)
        purchase = create(:purchase, person: invoice.person, invoice: invoice,
                            related_type: album.class.name, related_id: album.id)
        invoice.purchases << purchase
        invoice.final_settlement_amount_cents = invoice.purchases.inject(0){ |sum, x| sum + x.cost_cents }
        invoice.settled_at = Time.now
        invoice.save
      end
    end

    trait :unsettled do
      after(:create) do |invoice|
        album = create(:album, person: invoice.person)
        purchase = create(:purchase, person: invoice.person, invoice: invoice,
                            related_type: album.class.name, related_id: album.id)
        invoice.purchases << purchase
        invoice.save
      end
    end

    trait :with_batch_transaction_eligible_batch_transaction do
      after(:create) do |invoice, evaluator|
        batch_transaction = create(:batch_transaction, invoice: invoice)
      end
    end

    trait :with_balance_settlement do
      after(:create) do |invoice, _evaluator|
        amount = invoice.final_settlement_amount.to_f
        person = invoice.person
        person.person_balance.update(balance: 1000)

        person_transaction = create(:person_transaction, debit: amount, person: person)
        invoice.settlement_received(person_transaction, invoice.final_settlement_amount_cents)
      end
    end

    trait :with_braintree_settlement do
      transient do
        transaction_id { SecureRandom.hex }
      end

      after(:create) do |invoice, evaluator|
        amount = invoice.final_settlement_amount.to_f
        person = invoice.person
        cc = person.stored_credit_cards.first
        transaction = create(
          :braintree_transaction,
          :succeeded,
          amount: amount,
          stored_credit_card: cc,
          transaction_id: evaluator.transaction_id,
          invoice: invoice
        )
        invoice.settlement_received(transaction, invoice.final_settlement_amount_cents)
      end
    end

    trait :with_paypal_settlement do
      after(:create) do |invoice, _evaluator|
        amount = invoice.final_settlement_amount.to_f
        person = invoice.person

        transaction = create(:paypal_transaction, invoice: invoice, amount: amount)
        invoice.settlement_received(transaction, invoice.final_settlement_amount_cents)
      end
    end

    trait :with_balance_and_cc_settlement do
      after(:create) do |invoice, _evaluator|
        person = invoice.person
        amount = invoice.final_settlement_amount.to_f
        cc = person.stored_credit_cards.first

        person.person_balance.update(balance: 10)
        balance_transaction = create(:person_transaction, debit: 10, person: person)
        invoice.settlement_received(balance_transaction, 1000)

        cc_transaction = create(:braintree_transaction, :succeeded, amount: amount - 10, stored_credit_card: cc)
        invoice.settlement_received(cc_transaction, invoice.final_settlement_amount_cents - 1000)
      end
    end

    trait :with_outbound_vat do
      after(:create) do |invoice, _evaluator|
        vat_tax = create(
          :vat_tax_adjustment,
          related_id: invoice.id,
          related_type: "Invoice",
          amount: invoice.final_settlement_amount_cents * 0.17
        )

        create(
          :outbound_invoice,
          related_id: invoice.invoice_settlements.first.id,
          related_type: "InvoiceSettlement",
          vat_tax_adjustment_id: vat_tax.id
        )
      end
    end

    trait :with_payu_settlement do
      after(:create) do |invoice, _evaluator|
        transaction = create(:payu_transaction)
        invoice.settlement_received(transaction, invoice.final_settlement_amount_cents)
      end
    end

    trait :with_adyen_settlement do
      after(:create) do |invoice, _evaluator|
       transaction = create(:adyen_transaction,country: Country.find_by(iso_code: "PH"))
        invoice.settlement_received(transaction, invoice.final_settlement_amount_cents)
      end
    end
  end
end
