FactoryBot.define do
  factory :adyen_stored_payment_method do
    person
    country_id { 13 }
    corporate_entity_id { 2 }
    adyen_payment_method_info
    recurring_reference { "Testqwerty1233" }
  end
end
