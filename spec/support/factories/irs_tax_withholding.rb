# frozen_string_literal: true

FactoryBot.define do
  factory :irs_tax_withholding do
    withheld_from_person_transaction_id { 1 }
    withheld_amount { 5.0 }
    # TODO: irs_transaction_id 

    trait :with_person_transaction do
      after(:create) do |irs_tax_withholding, evaluator|
        create(
          :person_transaction_for_person_intake,
          irs_tax_withholding: irs_tax_withholding
        )
      end
    end
  end
end
