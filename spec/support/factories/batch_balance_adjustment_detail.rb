FactoryBot.define do
  factory :batch_balance_adjustment_detail do
    code { Random.rand(1..9000) }
    name { Faker::Name.name }
    minimum_payment { 0 }
    hold_payment { "No" }
    current_balance { Random.rand(1.0..9000.0) }
    status { "posted" }
  end
end
