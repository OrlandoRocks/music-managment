FactoryBot.define do
  factory :flag do
    category { Faker::Marketing.buzzwords }
    name { Faker::Marketing.buzzwords }
  end
end
