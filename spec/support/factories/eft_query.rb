FactoryBot.define do
  factory :eft_query do
    raw_query {'https://sandbox.braintreepaymentgateway.com'}
  end
end
