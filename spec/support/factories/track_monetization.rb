FactoryBot.define do
  factory :track_monetization do
    store {Store.find(83)}
    person
    song
    job_id {"1234567890"}
    delivery_type {"full_delivery"}
    takedown_at {nil}
  end

  trait :eligible do
    eligibility_status {"eligible"}
  end

  trait :ineligible do
    eligibility_status {"ineligible"}
  end

  trait :delivered_via_tc_distributor do
    after(:create) do |track|
      track.state = "delivered_via_tc_distributor"
      track.tc_distributor_state = "start"
      track.save
    end
  end

  trait :pending do
    eligibility_status {"pending"}
  end

  trait :delivered do
    after(:create) do |track|
      track.state = "delivered"
      track.save
    end
  end

  trait :blocked do
    after(:create) do |track|
      track.eligibility_status = "ineligible"
      track.state = "blocked"
      track.save
    end
  end

  trait :errored do
    after(:create) do |track|
      track.errored({})
    end
  end

  trait :with_enqueued_state do
    after(:create) do |track|
      track.start({})
    end
  end

  trait :taken_down do
    eligible
    after(:create) do |track|
      time = Time.local(2018, 1, 1)
      Timecop.freeze(time) do
        track.takedown_at = time
        track.state = "delivered"
        track.save
      end
    end
  end

  trait :with_searchable_metadata do
    with_album_upc
    with_account_artist
  end

  trait :with_album_upc do
    after(:create) do |track|
      create(:tunecore_upc, upcable: track.song&.album)
    end
  end

  trait :with_account_artist do
    after(:create) do |track|
      artist = create(:artist)

      track.person.update(artist_id: artist.id)
    end
  end

  trait :with_monetization_blocked_song do
    after(:create) do |track|
      create(:track_monetization_blocker, song: track.song, store: track.store)
    end
  end
end
