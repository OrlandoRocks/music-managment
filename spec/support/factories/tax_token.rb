FactoryBot.define do
  factory :tax_token do
    token       {SecureRandom.hex(10)}
    is_visible  {false}
    tax_form_type {nil}
    person
  end
end
