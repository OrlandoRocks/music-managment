FactoryBot.define do
  factory :gst_config do
    cgst {9}
    sgst {9}
    igst {nil}
    effective_from {Date.today}
  end
end
