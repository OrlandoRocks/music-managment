FactoryBot.define do
  factory :stored_credit_card do
    customer_vault_id {12_345}
    person_id {1}
    status {"new"}
    expiration_year {Date.today.year + 5}
    expiration_month {Date.today.month}
    country {"US"}
    cc_type {"visa"}
    factory :bb_stored_credit_card do
      bt_token { "1yhgsd" }
    end
    state_id {}
    state_city_id {}
    payin_provider_config {PayinProviderConfig.find_by(name: PayinProviderConfig::BRAINTREE_NAME)}
  end

  trait :braintree_card do
    after(:create) do |card|
      card.update(bt_token: SecureRandom.hex.slice(0..8))
    end
  end

  trait :payos_card do
    after(:create) do |card|
      card.update(payments_os_token: SecureRandom.uuid)
    end
  end

  trait :expired do
    after(:create) do |album|
      album.update_attribute :expiration_year, Date.today.year - 1
    end
  end

  trait :lux_card do
    after(:create) do |card|
      card.update(country: "LU")
    end
  end

  trait :with_billing_address do
    first_name {"John"}
    last_name {"Doe"}
    phone_number {'+12125555555'}
    address1 {Faker::Address.street_address}
    address2 {Faker::Address.secondary_address}
    city {Faker::Address.city}
    state {Faker::Address.state_abbr}
    country {"US"}
    zip {"11201"}
  end
end
