FactoryBot.define do
  factory :recover_asset do
    association :person, factory: :person
    association :admin, factory: :person
  end
end
