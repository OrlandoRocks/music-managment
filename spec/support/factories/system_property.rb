FactoryBot.define do
  factory :system_property do
    name {"system property"}
    value {"property"}
    description {"This is a system property"}
  end
end
