FactoryBot.define do
  factory :temp_person_royalty do
    person
    user_currency { "USD" }
    corporate_entity_id { 1 }
  end
end
