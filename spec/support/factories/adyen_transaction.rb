FactoryBot.define do
  factory :adyen_transaction do
    person
    invoice
    amount { 2999 }
    currency { "USD" }
    country
    local_currency { "PHP" }
    session_id { "CSA77D6D249E170BD1" }
    psp_reference { nil }
    result_code { nil }
    merchant_reference { nil }
    amount_in_local_currency { 2999 }
    adyen_payment_method_info
  end
end
