FactoryBot.define do
  factory :add_on_purchase do
    payment_date {Date.today}
    created_at {Date.today}
    person
    product
  end
end
