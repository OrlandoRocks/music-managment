FactoryBot.define do
  factory :person_points_balance do
    person
    balance { Faker::Number.number(digits: 3) }
  end
end
