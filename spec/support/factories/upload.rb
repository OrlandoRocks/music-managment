FactoryBot.define do
  factory :upload do
    song
    original_filename {"lambparty.mp3"}
    sequence(:uploaded_filename) { |n| [Time.now().to_f, n.to_s, "hamparty.ogg"].join }
    bitrate {3}
  end
end
