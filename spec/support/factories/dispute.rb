FactoryBot.define do
  factory :dispute do
    transient do
      status { "Open" }
    end

    processed_by { association :person, :with_role, role_name: Role::REFUNDS }
    amount_cents { 999 }
    transaction_identifier { "transaction_id123" }
    dispute_identifier { "dispute_id123" }
    reason { "fraud" }
    currency { "USD" }
    initiated_at { Time.zone.today }
    braintree

    trait :braintree do
      association :source, factory: [
        :braintree_transaction,
        :with_settled_invoice
      ]
      source_type { "BraintreeTransaction" }
      association :dispute_status, :braintree
    end

    trait :paypal do
      association :source, factory: :paypal_transaction
      source_type { "PaypalTransaction" }
      association :dispute_status, :paypal
    end

    trait :payu do
      association :source, factory: :payu_transaction
      source_type { "PayuTransaction" }
      association :dispute_status, :payu
    end

    after(:create) do |dispute, evaluator|
      dispute.dispute_status.update(status: evaluator.status)
    end
  end
end
