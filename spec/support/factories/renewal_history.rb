FactoryBot.define do
  factory :renewal_history do
    renewal
    purchase
    starts_at {DateTime.now}
    expires_at {1.year.from_now}
  end
end
