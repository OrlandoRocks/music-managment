FactoryBot.define do
  factory :achievement do
    name { Faker::Lorem.word }
    description { Faker::Lorem.words(number: 5) }
    link { Faker::Internet.url }
    points { Faker::Number.number(digits: 2) }
    category { 'Education' }
    is_active { true }
  end
end
