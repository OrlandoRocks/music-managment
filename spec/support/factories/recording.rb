FactoryBot.define do
  factory :recording do
    composition
    recording_code { Faker::Number.number(12) }
  end
end
