FactoryBot.define do
  factory :tier_achievement do
    tier
    achievement
  end
end
