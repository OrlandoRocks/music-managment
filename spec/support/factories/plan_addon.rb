FactoryBot.define do
  factory :plan_addon do
    person
  end

  trait :additional_artist_addon do
    addon_type { PlanAddon::ADDITIONAL_ARTIST }
  end

  trait :splits_collaborator do
    addon_type { PlanAddon::SPLITS_COLLABORATOR }
  end

  trait :paid do
    paid_at { Time.current }
  end

  trait :canceled do
    canceled_at { Time.current }
  end
end
