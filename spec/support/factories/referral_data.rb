FactoryBot.define do
  factory :referral_datum do
    person {nil}
    key { "gtm_source" }
    value { "facebook" }
    page { "signup" }
    source { "url" }
    timestamp { "2016-04-06 11:36:12" }
  end
end
