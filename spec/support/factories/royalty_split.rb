FactoryBot.define do
  factory :royalty_split do
    sequence(:title) { |n| "Band #{n}" }
    association :owner, factory: :person

    after(:create) do |split|
      # necessary b/c of split after_create hooks
      split.reload
    end
  end
end

def build_royalty_split_with_recipients(recipient_count = 3, **hash_args)
  owner = hash_args[:owner] ||= FactoryBot.create :person
  rounded_even_split = (100.0 / recipient_count).floor(2)

  FactoryBot.build(:royalty_split, **hash_args) do |split|
    non_owners = FactoryBot.build_list(
      :royalty_split_recipient, (recipient_count - 1),
      royalty_split: split,
      percent: rounded_even_split
    )
    owner_recipient = FactoryBot.build :royalty_split_recipient, royalty_split: split, person: owner,
                                                       percent: 100 - non_owners.sum(&:percent)
    split.recipients = non_owners.push owner_recipient
    yield split if block_given?
  end
end

def royalty_split_with_recipients(recipient_count = 3, **hash_args)
  build_royalty_split_with_recipients(recipient_count, **hash_args) do |split|
    yield split if block_given?
    split.tap(&:save!)
  end
end
