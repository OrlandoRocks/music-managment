FactoryBot.define do
  factory :country_website_language do
    country_website
    yml_languages { 'fr' }
    selector_language { 'French' }
    selector_country { 'France' }
    selector_order { 1 }
    redirect_country_website_id { 7 }
  end
end
