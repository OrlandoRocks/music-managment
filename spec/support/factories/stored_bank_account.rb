FactoryBot.define do
  factory :stored_bank_account do
    person
    customer_vault_id {"12345"}
    bank_name {"Chase"}
    last_four_account {"1234"}
    last_four_routing {"4321"}
    name {"Firstie Lastie"}
    phone {"(555)555-5555"}
    account_type {"checking"}
    address1 {"1 Shore Road"}
    city {"New York City"}
    country {"US"}
    state {"NY"}
    status {"valid"}
  end
end
