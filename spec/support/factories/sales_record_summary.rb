FactoryBot.define do
  factory :sales_record_summary do
    person
    sales_record_master
    association :related, factory: :song
  end
end

def summarize_sales_record_master(sales_record_master)
     sql = %Q{
    INSERT into sales_record_summaries
    SELECT person_id, sales_record_master_id, related_type, related_id,
      IF( related_type = 'Video', 'Video', IF( related_type = 'Song', (select album_type from songs s inner join albums a on a.id = s.album_id where s.id = related_id), (select album_type from albums where albums.id = related_id))),
      IF( related_type = 'Video', related_id, if( related_type = 'Album', related_id, (select album_id from songs where songs.id = related_id))),
      SUM(IF(distribution_type='Download',quantity,0)), SUM(IF(distribution_type='Streaming',quantity,0)) AS streams_sold,
      SUM(IF(distribution_type='Download',amount,0)),
      SUM(IF(distribution_type='Streaming',amount,0)),
      person_intake_id
    FROM sales_records
    WHERE sales_records.sales_record_master_id = #{sales_record_master.id}
    GROUP BY person_id, related_type, related_id, sales_record_master_id, person_intake_id}
    SalesRecordMaster.connection.execute(sql)
    sales_record_master.update(:summarized=>1)
end
