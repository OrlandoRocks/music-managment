FactoryBot.define do
  factory :artist do
    sequence(:name) { |n| "Elvis Costello #{n}" }
  end

  trait :blacklisted do
    after(:create) do |a|
      a.create_blacklisted_artist
    end
  end
end
