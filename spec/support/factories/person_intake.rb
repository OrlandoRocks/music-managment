FactoryBot.define do
  factory :person_intake do
    person
    amount      {0.00}
    currency    {"USD"}
    created_at  {Date.current}
  end
end
