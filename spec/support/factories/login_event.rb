FactoryBot.define do
  factory :login_event do
    city { Faker::Address.city }
    country { Faker::Address.country }
    person
  end
end
