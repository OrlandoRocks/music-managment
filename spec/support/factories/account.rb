FactoryBot.define do
  factory :account do
    person
    account_type {"publishing_administration"}
    blocked {false}
  end

  trait :with_account_id do
    provider_account_id { Faker::Number.number(digits: 12) }
  end
end
