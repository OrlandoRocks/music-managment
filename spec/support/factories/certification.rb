FactoryBot.define do
  factory :certification do
    name { Faker::Lorem.word }
    description { Faker::Lorem.words(number: 5) }
    badge_url { Faker::Internet.url }
    link { Faker::Internet.url }
    points { Faker::Number.number(digits: 2) }
    category { 'Advertising' }
    is_active { true }
  end
end
