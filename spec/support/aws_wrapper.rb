RSpec.configure do |config|
  # NOTE: Needed for any specs that call the AwsWrapper module classes in testing.
  # This ensures that when setting the sdk return value of :new to a double
  # that double applies to the test instance and not the memoized value previously
  # assigned by another test.
  config.around(:each, :aws_wrapper) do |example|
    clear_client_instance_vars

    example.run

    clear_client_instance_vars
  end
end

def clear_client_instance_vars
  AwsWrapper::S3.instance_variable_set("@client", nil)
  AwsWrapper::Sqs.instance_variable_set("@client", nil)
  AwsWrapper::SimpleDb.instance_variable_set("@client", nil)
end
