module DistributionApiJsonHelper
  VALID_JSON = {
    "message_id": "byte_dance_defined_message_id",
    "source_user_id": "byte_dance_user_id",
    "source_id": "distribution_api_source_identifier",
    "source_album_id": "byte_dance_album_id",
    "upcs": [
        {
            "upc": "670501008227",
            "type": "optional"
        },
        {
            "upc": "859749184744",
            "type": "tunecore"
        }
    ],
    "title": "Can I Offer You a Song",
    "source_contentreview_notes": "Content Review Notes",
    "album_title_localizations": [
        {
            "title": "Can I Offer You a Song",
            "album_title_localization": "en"
        }
    ],
    "metadata_language": "en",
    "original_release_date_et": "2021-08-13",
    "timed_release_date": "2021-08-13T04:00:00Z",
    "release_date": "2021-08-13T04:00:00Z",
    "preorder_start_date": nil,
    "timed_release_type": "relative_time",
    "primary_genres": [
        {
            "name": "Singer/Songwriter",
            "id": 34
        }
    ],
    "secondary_genres": [
        {
            "name": "Americana",
            "id": 28
        }
    ],
    "label_name": "Train Wreck Records",
    "explicit_lyrics": "clean",
    "artists": [
        {
            "name": "Chip Taylor",
            "apple_artist_id": "489827",
            "spotify_artist_id": "NEW",
            "roles": [
                "primary_artist"
            ]
        }
    ],
    "artwork_file": {
        "asset": "qa/artwork.jpg"
    },
    "release_type": "album",
    "territories": [
        "af",
        "ax",
        "al"
    ],
    "various_artists": false,
    "copyright": {
        "name": "Train Wreck Records",
        "year": "2021"
    },
    "delivery_type": "full_delivery",
    "album_stores": [
        {
            "store_id": 36,
            "store_name": "iTunes"
        },
        {
            "store_id": 26,
            "store_name": "Spotify"
        },
        {
            "store_id": 13,
            "store_name": "Amazon Music"
        },
        {
            "store_id": 28,
            "store_name": "YouTube Music"
        },
        {
            "store_id": 78,
            "store_name": "Pandora"
        },
        {
            "store_id": 32,
            "store_name": "Deezer"
        },
        {
            "store_id": 25,
            "store_name": "iHeartRadio"
        },
        {
            "store_id": 7,
            "store_name": "Napster"
        }
    ],
    "songs": [
        {
            "source_song_id": 14238608,
            "sequence": 1,
            "title": "Test Song 1",
            "track_title_localizations": [],
            "track_lyric_language": "en",
            "track_audio_language": [],
            "clean_version": false,
            "copyright": {
                "name": "Train Wreck Records",
                "year": "2021"
            },
            "artists": [
                {
                    "name": "Artist 1",
                    "creative_role": "contributor",
                    "roles": [
                        "composer_lyricist",
                        "songwriter",
                        "contributor"
                    ],
                    "spotify_artist_id": "NEW",
                    "apple_artist_id": "489824"
                },
                {
                    "name": "Artist 2",
                    "creative_role": "primary_artist",
                    "roles": [
                        "primary_artist"
                    ],
                    "spotify_artist_id": "NEW",
                    "apple_artist_id": "489825"
                }
            ],
            "original_release_date": "2021-09-09",
            "isrc": [
                {
                    "id": "USYVC2108201",
                    "type": "optional"
                }
            ],
            "preorder_flag": false,
            "explicit_lyrics": false,
            "instrumental": false,
            "lyrics": nil,
            "asset_url": "qa/song1.flac",
            "bundled": false,
            "free_song": false,
            "available_for_streaming": true,
            "duration": 225,
            "sample_start_time": "01:10"
        },
        {
            "source_song_id": 14238609,
            "sequence": 2,
            "title": "Test Song 2",
            "track_title_localizations": [],
            "track_lyric_language": "en",
            "track_audio_language": [],
            "clean_version": false,
            "copyright": {
                "name": "Test 2",
                "year": "2021"
            },
            "artists": [
                {
                    "name": "Artist 3",
                    "creative_role": "contributor",
                    "roles": [
                        "composer_lyricist",
                        "songwriter",
                        "contributor"
                    ],
                    "spotify_artist_id": "NEW",
                    "apple_artist_id": "489826"
                },
                {
                    "name": "Artist 4",
                    "creative_role": "primary_artist",
                    "roles": [
                        "primary_artist"
                    ],
                    "spotify_artist_id": "NEW",
                    "apple_artist_id": "489827"
                }
            ],
            "original_release_date": "2021-09-09",
            "isrc": [
                {
                    "id": "USYVC2108202",
                    "type": "optional"
                }
            ],
            "preorder_flag": false,
            "explicit_lyrics": false,
            "instrumental": false,
            "lyrics": nil,
            "asset_url": "qa/song2.flac",
            "bundled": false,
            "free_song": false,
            "available_for_streaming": true,
            "duration": 225,
            "sample_start_time": "01:10"
        }
    ]
  }.to_json
end
