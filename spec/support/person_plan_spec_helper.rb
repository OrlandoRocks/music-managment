module PersonPlanSpecHelper
  def create_person_plan!(person, plan, change_type)
    product = plan.product_by_person(person)
    purchase = create(:purchase, person_id: person.id, related: product, product: product)
    PersonPlanCreationService.call(
      person_id: person.id, plan_id: plan.id, purchase_id: purchase.id,
      change_type: change_type,
      )
  end
end
