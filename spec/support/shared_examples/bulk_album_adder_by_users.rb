RSpec.shared_examples "bulk_album_adder_by_users" do |form_data|
  before(:each) do
    fill_out_bulk_form(form_data)
  end
  it "submits successfully" do
    expect(page.current_url).to match(/\/admin\/bulk_add_stores_by_users\/new/)
    expect(page).to have_content("Added store #{store.id} to album #{album1.id}-#{album1.name}")
    expect(page).to have_content("Added store #{store.id} to album #{album2.id}-#{album2.name}")
  end

  it "adds items to the users cart" do
    check_for_user_cart_items person1
    check_for_user_cart_items person2
  end
end

def fill_out_bulk_form(form_data)
  select store.name
  fill_in "User ids", with: send(form_data[:fill_in_form_with].to_sym)
  click_button "add store"
end

def comma_sep_users
  "#{person1.id}, #{person2.id}"
end

def space_sep_users
  "#{person1.id} #{person2.id}"
end

def check_for_user_cart_items(person)
  visit '/logout'
  login_user(person, "Testpass123!")
  visit '/cart'
  expect(page).to have_content(store.name)
end
