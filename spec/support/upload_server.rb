class UploadServer < ApplicationRecord
  # Returns a random server URL from the pool
  def self.find_random
		new_server = UploadServer.create(:url => 'localhost:3000', :status => 'active')
		return new_server
  end

	def upload_server_path
		"http://#{self.url}/upload/upload_server_mock"
	end
end
