module FakeVatApiHelper

  def self.generate
    vat_response = Response.new
    yield(vat_response)
    vat_response
  end

  class Response
    attr_accessor :valid,
                  :vat_number,
                  :name,
                  :country,
                  :place_of_supply,
                  :tax_rate,
                  :tax_type,
                  :error_message,
                  :data

    def initialize(valid = nil, vat_number = nil, name = nil, country = nil, place_of_supply = nil, tax_rate = nil, tax_type = nil, error_message = nil, data = nil)
      @valid           = valid
      @vat_number      = vat_number
      @name            = name
      @country         = country
      @place_of_supply = place_of_supply
      @tax_rate        = tax_rate
      @tax_type        = tax_type
      @error_message   = error_message
      @data            = data
    end

    def valid?
      valid
    end

    def response_valid?
      valid
    end
  end

end

