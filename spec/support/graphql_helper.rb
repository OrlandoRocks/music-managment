def gql_op(args = {}, context = {})
  described_class.new(object: nil, field: nil, context: context).resolve(args)
end
