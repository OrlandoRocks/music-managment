require "sinatra"

class FakeBigBoxServer < Sinatra::Application
  before do
    headers "Access-Control-Allow-Origin" => "*"
  end

  post "/uploader/create/" do
    key = SecureRandom.hex(8)

    response = {
      status: 0,
      response: {
        orig_filename: "#{key}-track.wav",
        status: 0,
        streaming_url: "http://tcc.tunecore.com.staging.s3.amazonaws.com/1/#{key}-track.wav.mp3?Signature=VRJwAUg0MpI0OmgBGmOkgl8vH6c%3D&Expires=1548280153&AWSAccessKeyId=AKIAISJL6HSZITWXRMSQ",
        :"mime-type" => "audio/x-wav",
        bit_rate: 705600,
        key: "1/#{key}-track.wav",
        file_size: 176444,
        duration: 2000,
        uuid: SecureRandom.uuid,
        created_at: "2019-01-23T15:49:13.947760",
        bucket: "tcc.tunecore.com.staging",
        bit_depth: 16,
        sample_rate: 44100,
        element_id: false,
        waveform_url: "http://tcc.tunecore.com.staging.s3.amazonaws.com/1/#{key}-track.wav.png"
      }
    }

    response.to_json
  end

  def self.while_running
    thread = Thread.new { run! }
    sleep 0.1 until running?

    yield

    thread.kill
  end
end
