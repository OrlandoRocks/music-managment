module AppleMusicApiHelper
  ARTIST_RESPONSE_1 = {
    "results": {
      "artists": {
        "data": [
          {
            "id": "107917",
            "attributes": {
              "name": "Nine Inch Nails"
            },
            "relationships": {
              "albums": {
                "data": [
                  {
                    "id": "1440837096",
                    "href": "/v1/catalog/us/albums/1440837096"
                  },
                  {
                    "id": "1440934840",
                    "href": "/v1/catalog/us/albums/1440934840"
                  }
                ]
              }
            }
          },
          {
            "id": "320366644",
            "attributes": {
              "name": "Ninho"
            },
            "relationships": {
              "albums": {
                "data": [
                  {
                    "id": "1453399177",
                    "type": "albums",
                    "href": "/v1/catalog/us/albums/1453399177"
                  },
                  {
                    "id": "1501355159",
                    "type": "albums",
                    "href": "/v1/catalog/us/albums/1501355159"
                  }
                ]
              }
            }
          }
        ]
      }
    }
  }

  EMPTY_ARTISTS_RESPONSE = {
    "results": {
      "artists": {
        "data": [
        ]
      }
    }
  }

  ALBUM_RESPONSE_1 = {
    "data": [
      {
        "id": "1440837096",
        "attributes": {
          "artwork": {
            "width": 3000,
            "height": 3000,
            "url": "https://is2-ssl.mzstatic.com/image/thumb/Music113/v4/e7/9f/f4/e79ff471-b461-d01d-bd47-6d6a5ec1f1a1/15UMGIM67680.rgb.jpg/{w}x{h}bb.jpeg",
            "bgColor": "ebd9ab",
            "textColor1": "120704",
            "textColor2": "403117",
            "textColor3": "3d3126",
            "textColor4": "625235"
          },
          "artistName": "Nine Inch Nails"
        },
        "relationships": {
          "artists": {
            "data": [
              {
                "id": "107917"
              }
            ]
          }
        }
      },
      {
        "id": "1453399177",
        "attributes": {
          "artwork": {
            "width": 4000,
            "height": 4000,
            "url": "https://is5-ssl.mzstatic.com/image/thumb/Music114/v4/5e/67/77/5e6777d6-3451-6a54-a003-3eee94490704/190295460259.jpg/{w}x{h}bb.jpeg",
            "bgColor": "0073a5",
            "textColor1": "ffffff",
            "textColor2": "ffd080",
            "textColor3": "cbe3ed",
            "textColor4": "cbbd87"
          },
          "artistName": "Ninho"
        },
        "relationships": {
          "artists": {
            "data": [
              {
                "id": "320366644"
              }
            ]
          }
        }
      }
    ]
  }

  DUMMY_ALBUMS_RESPONSE = {
    "data": []
  }

  def fake_apple_music_client
    Faraday.new(headers: { 'Authorization': "Bearer some-token" })
  end
end
