def expected_email_form_data songs, email_template
  expected_result = (songs.empty? ? {} : { custom_fields: email_template.custom_fields })
  songs.each do |song|
    expected_result[song.album.person.email] = [] if expected_result[song.album.person.email].nil?
    expected_result[song.album.person.email] <<
        {
          email: song.album.person.email,
          person_id: song.album.person.id,
          isrc: song.isrc,
          id: song.id,
          country: song.album.person.country
        }
  end

  expected_result
end

def extract_songs albums
  albums.map{ |a| a.songs }.flatten!
end

def prepare_email_data_hash email_template, albums
  songs = extract_songs(albums)
  custom_fields = email_template.custom_fields
  custom_field_values = custom_fields.reduce({}) do |acc, custom_field|
    acc.merge(custom_field => "test")
  end

  result = {"email_data" => {}}
  songs.each do |song|
    result['email_data'][song.album.person.id] = {'send_email' => 'true'} if result['email_data'][song.album.person.id].nil?
    result['email_data'][song.album.person.id][song.id] = {} if result['email_data'][song.album.person.id][song.id].nil?
    result['email_data'][song.album.person.id][song.id]["1"] = custom_field_values
  end
  result
end