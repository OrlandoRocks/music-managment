module SpotifyApiHelper

  def fake_spotify_client
    Faraday.new(SPOTIFY_API[:token_request]) do |builder|
      builder.adapter :test do |stubs|

        stubs.get("albums") do |env|
          return_error(env) || [200, {}, {}.to_json]
        end

        stubs.post("/") do |env|
          [ 200, {}, {:token_type => "Bearer", :access_token => "1234"}.to_json ]
        end

        stubs.get("/v1/search") do |env|
          upc = env[:params]["q"].split(":").last
          return_error(env) || [200, {}, album_from_upc_query([upc])]
        end

        stubs.get("/v1/tracks") do |env|
          uris = env[:params]["ids"].split(",")
          return_error(env) || [200, {}, album_track_objects(uris)]
        end

        stubs.get("/v1/albums/1234") do |env|
          alb = env[:url].to_s.split("/").last
          return_error(env) || [200, {"success" => true}, get_album_tracks(alb)]
        end
      end
    end
  end

  def return_error(env_hash)
    error = env_hash[:request_headers].fetch("error", nil)
    return error_response(error) if error
  end

  def album_from_upc_query(upcs)
    {
      albums: { items: (upcs.first.present? ? simple_albums(upcs) : []) }
    }.to_json
  end

  def multiple_albums_from_upc_query(upc)
    {
      albums: { items: [
        {
          album_type: "Album",
          id: rand(1000).to_s,
          name: Faker::Job.title
        },
        {
          album_type: "Album",
          id: rand(1000).to_s,
          name: Upc.find_by(number: upc).upcable.name
        }
      ]}
    }.to_json
  end

  def albums_from_uri_get(uris=[])
    {
      albums: (uris.first.present? ? full_albums(uris) : [])
    }.to_json
  end

  def get_album_tracks(track_uris=[])
    {
      tracks: { items: (track_uris.first.present? ? simple_tracks([track_uris]) : []) }
    }.to_json
  end

  def album_track_objects(isrcs=[])
    {
      tracks: (isrcs.first.present? ? full_tracks(isrcs) : [])
    }.to_json
  end

  def simple_albums(upcs)
    upcs.map do |u|
      {
        album_type: "Album",
        id: rand(1000).to_s,
        name: Faker::Job.title
      }
    end
  end

  def full_albums(uris=[])
    uris.map do |u|
      {
        artists: [ name: Faker::Name.name ],
        external_ids: {
          upc: u
        },
        id: u,
        tracks: {
          href: Faker::Internet.url,
          items: (uris.first.present? ? full_tracks(uris) : [])
        }
      }
    end
  end

  def simple_tracks(uris=[])
    uris.map do |u|
      {
        id: rand(10),
        name: "Track",
        track_number: rand(10)
      }
    end
  end

  def full_tracks(isrcs=[])
    isrcs.map do |i|
      {
        available_markets: ["ak", "us", "eu", "th"],
        external_ids: {
          isrc: i
        },
        id: rand(10)
      }
    end
  end

  def error_response(error_string)
    [ 400, {}, error_string == "expired" ? expired_token : regular_error ]
  end

  def expired_token
    {
      error: {
        status: 401,
        message: "The access token expired"
      }
    }.to_json
  end

  def regular_error
    {
      error: {
        status: 400,
        message: "NOPE"
      }
    }.to_json
  end

  def get_all_album_tracks(album_uri)
    items = []
    if album_uri.present?

      rand(1..5).times do
        items << 
          {
            artists: simple_artists
          }
      end
    end
    {items: items}.to_json
  end

  def simple_artists
    artists = []
    rand(1..2).times do
      artists << 
        { 
          id: Faker::Lorem.characters[0..22],
          name: Faker::Name.name,
          type: "artist"
        }
    end
    artists
  end

end
