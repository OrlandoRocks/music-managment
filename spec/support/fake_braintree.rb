module FakeBraintree
  def fake_braintree_query
    FakeBraintreeQuery.new
  end

  class FakeBraintreeQuery
    def execute(*args)
      {}
    end
  end
end
