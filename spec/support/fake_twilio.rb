module FakeTwilio
  class FakeTwilioClient
    def initialize(key=nil, secret=nil)
    end

    def lookups
      self
    end

    def phone_number
      self
    end 

    def phone_numbers(num)
      self
    end

    def v1
      self
    end

    def v2
      self
    end

    def verify
      self
    end

    def services(num)
      self
    end

    def verifications
      self
    end

    def verification_checks
      self
    end

    def create(code=nil, channel=nil, to=nil)
      self
    end

    def fetch
      self
    end
  end
end
