module FeatureSpecDbProxy
  module RemoteFactoryProxy
    def remote_create(*args)
      payload = args.to_json

      response = HTTParty.post(
        URI.join(server_url, "/__factory-bot__/"),
        body: payload,
        headers: { "Content-Type" => "application/json" }
      )

      object_attributes = JSON.parse(response.body)
      OpenStruct.new(object_attributes)
    end

    def remote_update(model, id, attributes = {})
      payload = {
        model: model,
        id: id,
        attributes: attributes
      }.to_json

      response = HTTParty.patch(
        URI.join(server_url, "/__factory-bot__/"),
        body: payload,
        headers: { "Content-Type" => "application/json" }
      )

      object_attributes = JSON.parse(response.body)
      OpenStruct.new(object_attributes)
    end

    private

    def server_url
      "http://#{page.server.host}:#{page.server.port}"
    end
  end
end
