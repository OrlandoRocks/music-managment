module FeatureSpecDbProxy
  module RemoteActiveRecordBaseProxy
    def remote_destroy_all(model)
      model_name = model.to_s.underscore

      HTTParty.delete(
        URI.join(server_url, "/__active-record__/"),
        body: { model: model_name }
      )
    end

    private

    def server_url
      "http://#{page.server.host}:#{page.server.port}"
    end
  end
end
