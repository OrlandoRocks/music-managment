require "rails_helper"

describe ::CountryWebsiteUpdater::Policy do
  describe ".can_update?" do
    let(:person) { double('person') }

    subject { ::CountryWebsiteUpdater::Policy.can_update?(person: person) }

    context "when a person has transactions" do
      before do
        allow(person)
          .to receive(:person_transactions)
          .and_return(["fake_transaction"])

        allow(person)
          .to receive_message_chain(:person_balance, :balance)
          .and_return(0)

        allow(person)
          .to receive_message_chain(:invoices).
          and_return([])
      end

      it "has an error" do
        expect(subject.errors.present?).to be true
      end
    end

    context "when a person has balance greater than 0" do
      before do
        allow(person)
          .to receive(:person_transactions)
          .and_return([])

        allow(person)
          .to receive_message_chain(:person_balance, :balance)
          .and_return(9001)

        allow(person)
          .to receive_message_chain(:invoices).
          and_return([])
      end

      it "has an error" do
        expect(subject.errors.present?).to be true
      end
    end

    context "when a person has invoices" do
      before do
        allow(person)
          .to receive(:person_transactions)
          .and_return([])

        allow(person)
          .to receive_message_chain(:person_balance, :balance)
          .and_return(0)

        allow(person)
          .to receive_message_chain(:invoices).
          and_return(["fake_invoice"])
      end

      it "has an error" do
        expect(subject.errors.present?).to be true
      end
    end

    context "when none of the above conditions are true" do
      before do
        allow(person)
          .to receive(:person_transactions)
          .and_return([])

        allow(person)
          .to receive_message_chain(:person_balance, :balance)
          .and_return(0)

        allow(person)
          .to receive_message_chain(:invoices).
          and_return([])
      end

      it "does not have an error" do
        expect(subject.errors.present?).to be false
      end
    end
  end
end
