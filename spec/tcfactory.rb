# 2009-8-14 CH -- added factory method for BankAccount

require 'faker'

Dir['spec/factories/**/*.rb'].each { |factory|
  require "#{Rails.root}/#{factory}"
}

require "#{Rails.root}/spec/legacy_factory_builders"

module TCFactory

  include LegacyFactory
  include Base

  #  provides access to TCFactory's methods
  #  as static methods
  #  example:  TCFactory.create(:some_model, {})
  extend self

  #
  #  TCFactory#create
  #
  #  Helper method to create models in the database
  #
  #  model:  the :symbol of a model
  #  options:  the optional values to change of the defaults
  #
  #  Usage:
  #  person = TCFactory.create(:person, :last_name => "Foobar")
  #  >> #<Person>
  #
  #  person.new_record?
  #  >> false
  #
  def create(model, options={})
    instance = build(model,options)
    instance.save!
    instance
  end

  #
  #  TCFactory#build
  #
  #  Sets up new (unsaved) model objects for testing
  #
  #  model:  the :symbol of a model
  #  options:  the optional values to change of the defaults
  #
  #  Usage:
  #
  #  person = TCFactory.build(:person, :last_name => "Foobar")
  #  >> #<Person>
  #
  #  person.new_record?
  #  >> true
  #
  def build(model, options = {})
    defaults = get_options(model, options)
    instance = model.to_s.camelize.constantize.new
    defaults.each do |attribute, value|
      instance.send("#{attribute}=".to_sym, value)
    end if defaults
    instance
  end

  def get_options(model,options={})
    send("#{model.to_s}_defaults".to_sym, options)
  end

  #
  #  Redirect legacy calls to build methods to newer style
  #
  #  example:  TCFactory.build_album(options) =>  TCFactory.create(:album, options)
  #
  def method_missing(symbol, args={})
    if symbol.to_s =~ /build_/
      begin
        LegacyFactory.send(symbol, args)
      rescue
        send_to_factory(symbol, args)
      end
    end
  end

  #
  #  Send to the New Factory
  #
  def send_to_factory(symbol, args={})
    method = symbol.to_s.gsub(/build_/,'')
    if method
      begin
        self.send(:create, method.to_sym, args)
      rescue StandardError => e
        puts e.message
        puts e.backtrace.join("\n")
      end
    else
      raise "Unknown method: #{symbol}"
    end
  end

  def name_collision?(name)
    LegacyFactory.instance_methods.include?(name.to_sym) || Base.instance_methods.include?(name.to_sym)
  end

  #
  #  Creating a method ahead of time, instead of waiting for
  #  method_missing.
  #
  def create_a_new_method(name, model_name)
    define_method(name) do |*args|
      if args.size == 1
        # follows usual format
        options = args.first || {}
        create(model_name.to_sym, options)
      else
        # calls irregular method
        super({})
      end
    end
  end

  #
  #  preload definition of build and create methods
  #  being cautious to not overwrite existing
  #  methods in the Base or Legacy factories
  #
  methods.each do |method_name|
    if method_name =~ /_defaults/
      model_name = method_name.to_s.gsub(/_defaults/,'')
      new_method_name = "build_#{model_name}"

      if ! name_collision?(new_method_name)
        create_a_new_method(new_method_name, model_name)
      end
    end
  end

end
