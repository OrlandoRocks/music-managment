require "rails_helper"

describe DiscographyHelper do
  context "#can_reactivate_renewal?" do
    let(:album) { FactoryBot.create(:album, :with_one_year_renewal) }

    it "should be true for cancelled renewals of live albums" do
      album.cancel_renewal
      expect(helper.can_reactivate_renewal?(album)).to eq(true)
    end

    it "should be false for denied albums" do
      album.legal_review_state = "REJECTED"
      album.save
      expect(helper.can_reactivate_renewal?(album)).to eq(false)
    end

    it "should be false for albums takendown" do
      album.takedown_at = Time.now
      album.save
      expect(helper.can_reactivate_renewal?(album)).to eq(false)
    end

    it "should be false for albums with no renewal" do
      unpaid_album = FactoryBot.create(:album)
      expect(helper.can_reactivate_renewal?(unpaid_album)).to eq(false)
    end
  end

  describe "#set_view_type" do
    it "sets mobile browser to list view" do
      allow(helper).to receive_message_chain(:browser, :mobile?).and_return true
      expect(helper.set_view_type).to eq 'table'
    end

    it "uses params value when available" do
      allow(helper).to receive(:params).and_return({ view_type: 'grid' })
      expect(helper.set_view_type).to eq 'grid'
    end

    it "uses table if user has more than 250 albums" do
      helper.extend(MockApplicationHelper)
      allow(helper).to receive_message_chain(:current_user, :albums, :not_deleted, :count).and_return 251
      expect(helper.set_view_type).to eq 'table'
    end

    it "falls back to list" do
      helper.extend(MockApplicationHelper)
      allow(helper).to receive_message_chain(:current_user, :albums, :not_deleted, :count).and_return 25
      expect(helper.set_view_type).to eq 'list'
    end

    it "rejects bad values" do
      allow(helper).to receive(:params).and_return({ view_type: 'parallax' })
      expect(helper.set_view_type).to eq 'list'
    end
  end
end
