require "rails_helper"

describe RegionHelper, type: :helper do
  describe "release_split_description" do
    let(:album) { create :album, :with_songs, number_of_songs: 5, name: "My Album" }
    subject { create :royalty_split }
    context "with all songs" do
      before do
        subject.update(songs: album.songs)
      end
      it { expect(subject.album_count).to eql(1) }
      it { expect(subject.song_count).to eql(5) }
      it { expect(helper.release_split_description split: subject, release: album).to eql("My Album") }
    end

    context "with only two songs" do
      let(:first_song) { create :song, album: album, name: "Apple" }
      let(:second_song) { create :song, album: album, name: "Banana" }
      before do
        subject.update(songs: [first_song, second_song])
      end
      it { expect(helper.release_split_description split: subject, release: album).to eql("My Album (Apple, Banana)") }
    end

    context "with many, but not all, songs on album" do
      let(:song_list) { create_list :song, 4, album: album }
      before do
        subject.update(songs: song_list)
      end
      it { expect(helper.release_split_description split: subject, release: album).to eql("My Album (4 songs)") }
    end
  end

  describe "split_description" do
    let(:split) { create :royalty_split }
    context "with 3 albums" do
      let(:albums) { create_list :album, 3, :with_songs }
      before do
        albums.each do |album|
          split.associate_songs! album.songs
          allow(helper).to receive(:release_split_description).with(split: split, release: album).and_return("album")
        end
      end
      it { expect(helper.split_description split).to eq("album, album, album") }
    end
    context "with 7 albums" do
      let(:albums) { create_list :album, 7, :with_songs }
      before do
        albums.each do |album|
          split.associate_songs! album.songs
          allow(helper).to receive(:release_split_description).with(split: split, release: album).and_return("album")
        end
      end
      it { expect(helper.split_description split).to eq("album, album, album and 4 more releases") }
    end
    context "with 4 albums" do
      let(:albums) { create_list :album, 4, :with_songs }
      before do
        albums.each do |album|
          split.associate_songs! album.songs
          allow(helper).to receive(:release_split_description).with(split: split, release: album).and_return("album")
        end
      end
      it { expect(helper.split_description split).to eq("album, album, album and 1 more release") }
    end
  end
end
