require "rails_helper"

describe Admin::ComposersHelper, type: :helper do
  describe "#ineligible_composer_pro_for_pub?" do
    let(:composer) { build(:composer) }

    subject { helper.ineligible_composer_pro_for_pub?(composer) }

    context "when the composer has an eligible pro for publishing" do
      before { allow(composer).to receive(:performing_rights_organization_id).and_return("56") }
      it { should be_falsey }
    end

    context "when the composer does not have an eligible pro for publishing" do
      before { allow(composer).to receive(:performing_rights_organization_id).and_return(nil) }
      it { should be_truthy }
    end
  end
end
