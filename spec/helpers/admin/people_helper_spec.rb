require "rails_helper"

describe Admin::PeopleHelper, type: :helper do
  describe "suspicious flag reason" do
    let!(:person) do
      create(:person,
             :with_people_flag,
             flag_attributes: Flag::BLOCKED_FROM_DISTRIBUTION,
             lock_reason: "Other" )
    end

    describe "when the person has suspicious flag marked" do
      it "should return the suspicious reason with will not distribute if it is blocked from distribution" do
        expect(person.blocked_from_distribution_flag).to be_present
        expect(helper.suspicious_reason(person)).to eq("(Other - Will Not Distribute)")
      end

      it "should return the lock_reason if it is not blocked from distrivution" do
        person.blocked_from_distribution_flag = nil
        expect(helper.suspicious_reason(person)).to eq("(Other)")
      end
    end
  end

  describe "current_tax_form_status" do
    let(:non_us_person_without_tax_form) { create(:person, :with_lux_country) }
    let(:non_us_person_with_tax_form) do
      person = create(:person, :with_lux_country, :with_tax_form)
      create(:tax_form, person: person, payout_provider_config: PayoutProviderConfig.by_env.first)
      person
    end
    let(:us_person_without_tax_form) { create(:person) }
    let(:us_person_with_tax_form) do
      person = create(:person, :with_tax_form)
      create(:tax_form, person: person, payout_provider_config: PayoutProviderConfig.by_env.first)
      person
    end
    let(:us_person_with_invalid_tax_form) do
      person = create(:person, :with_invalid_tax_form)
      create(:tax_form, :w8, person: person, payout_provider_config: PayoutProviderConfig.by_env.first)
      person
    end
    let(:taxable_threshold) { TaxFormCheckService::TAXABLE_THRESHOLD }
    let(:sample_revenue_below_threshold) {
      {
        distribution: { earnings: (taxable_threshold / 2 - 1), withholdings: 0.0 },
        publishing: { earnings: (taxable_threshold / 2 - 1), withholdings: 0.0 }
      }
    }
    let(:sample_revenue_above_threshold) {
      {
        distribution: { earnings: (taxable_threshold / 2 + 1), withholdings: 0.0 },
        publishing: { earnings: (taxable_threshold / 2 + 1), withholdings: 0.0 }
      }
    }

    context "for a non-us person without tax-form" do
      it "displays status appropriately" do
        expect(helper.current_tax_form_status(non_us_person_without_tax_form))
          .to eq("Not Submitted - Form Not Required")
      end
    end

    context "for a non-us person with tax-form" do
      it "displays status appropriately" do
        taxform_submitted_date = helper.last_submitted_tax_form(non_us_person_with_tax_form).submitted_at.strftime("%m/%-d/%y")
        expect(helper.current_tax_form_status(non_us_person_with_tax_form))
          .to eq("Submitted #{taxform_submitted_date} - W9 - Individual")
      end
    end

    context "for a us person without tax-form" do
      context "when income is below taxable threshold" do
        it "displays status appropriately" do
          obj = OpenStruct.new(generate_revenue_stats: sample_revenue_below_threshold)
          allow(TaxReports::RevenueReportService).to receive(:new) { obj }
          expect(helper.current_tax_form_status(us_person_without_tax_form))
            .to eq("Not Submitted - Form Required - Withholding not applied since total income is less than $#{taxable_threshold}")
        end
      end

      context "when income is above taxable threshold" do
        it "displays status appropriately" do
          obj = OpenStruct.new(generate_revenue_stats: sample_revenue_above_threshold)
          allow(TaxReports::RevenueReportService).to receive(:new) { obj }
          expect(helper.current_tax_form_status(us_person_without_tax_form))
            .to eq("Not Submitted - Form Required - Withholding Applied")
        end
      end
    end

    context "for a us person with w9 tax-form" do
      context "when income is below taxable threshold" do
        it "displays status appropriately" do
          obj = OpenStruct.new(generate_revenue_stats: sample_revenue_below_threshold)
          taxform_submitted_date = helper.last_submitted_tax_form(us_person_with_tax_form).submitted_at.strftime("%m/%-d/%y")
          allow(TaxReports::RevenueReportService).to receive(:new) { obj }
          expect(helper.current_tax_form_status(us_person_with_tax_form))
            .to eq("Submitted #{taxform_submitted_date} - W9 - Individual")
        end
      end

      context "when income is above taxable threshold" do
        it "displays status appropriately" do
          obj = OpenStruct.new(generate_revenue_stats: sample_revenue_above_threshold)
          taxform_submitted_date = helper.last_submitted_tax_form(us_person_with_tax_form).submitted_at.strftime("%m/%-d/%y")
          allow(TaxReports::RevenueReportService).to receive(:new) { obj }
          expect(helper.current_tax_form_status(us_person_with_tax_form))
            .to eq("Submitted #{taxform_submitted_date} - W9 - Individual")
        end
      end
    end

    context "for a us person with non-w9 tax-form" do
      context "when income is below taxable threshold" do
        it "displays status appropriately" do
          obj = OpenStruct.new(generate_revenue_stats: sample_revenue_below_threshold)
          allow(TaxReports::RevenueReportService).to receive(:new) { obj }
          expect(helper.current_tax_form_status(us_person_with_invalid_tax_form))
            .to eq("Not Submitted - Incorrect Type - Withholding not applied since total income is less than $#{taxable_threshold}")
        end
      end

      context "when income is above taxable threshold" do
        it "displays status appropriately" do
          obj = OpenStruct.new(generate_revenue_stats: sample_revenue_above_threshold)
          allow(TaxReports::RevenueReportService).to receive(:new) { obj }
          expect(helper.current_tax_form_status(us_person_with_invalid_tax_form))
            .to eq("Not Submitted - Incorrect Type - Withholding Applied")
        end
      end
    end
  end
end
