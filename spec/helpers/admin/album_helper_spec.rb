require "rails_helper"

describe Admin::AlbumHelper do
  describe "#note_created_name" do
    before do
      @admin = create(:person, :with_role)
      @artist = create(:person)
      @album = create(:album, person: @artist)
    end

    it "returns the note created name if it wasn't initiated by a takedown request from the user" do
      note = create(:note, note_created_by: @admin)
      expect(helper.note_created_name(note, @album)).to eq(@admin.name)
    end

    it "returns 'Customer' if it was initiated by a takedown request from the user" do
      note = create(:note, note_created_by: @artist, note: 'Album was taken down')
      expect(helper.note_created_name(note, @album)).to eq("Customer: #{note.note_created_by_name}")
    end
  end

  describe "#show_curated_artist_view?" do
    let(:album) { create(:album) }
    let(:person) { create(:person) }
    let(:distro) { create(:distribution) }

    context "when the distribution does not have itunes" do
      it "returns false" do
        expect(helper.show_curated_artist_view?(distro, person)).to be false
      end
    end

    context "when the distribution does not have curated artist" do
      it "returns false" do
        allow(distro).to receive(:is_itunes?).and_return(true)

        expect(helper.show_curated_artist_view?(distro, person)).to be false
      end
    end

    context "when the admin does not have the curated artist role" do
      it "returns false" do
        allow(distro).to receive(:is_itunes?).and_return(true)
        allow(distro).to receive(:has_curated_artist_flags?).and_return(true)

        expect(helper.show_curated_artist_view?(distro, person)).to be false
      end
    end

    context "when the distribution has itunes, curated artists and the "\
      "admin has the Curated Artist role" do
      it "returns true" do
        allow(distro).to receive(:is_itunes?).and_return(true)
        allow(distro).to receive(:has_curated_artist_flags?).and_return(true)
        allow(person).to receive(:has_role?).with(Role::CURATED_ARTIST).and_return(true)

        expect(helper.show_curated_artist_view?(distro, person)).to be true
      end
    end
  end

  describe "#audio_quality_color" do
    context "when the file type is NOT a low audio quality file type" do
      it "should return an empty string" do
        result = helper.audio_quality_color("wav")

        expect(result).to eq ""
      end
    end

    context "when the file type is a low audio quality file type" do
      it "should return an empty string" do
        result = helper.audio_quality_color("mp3")

        expect(result).to eq "red"
      end
    end
  end

  describe "#song_audio_quality" do
    context "when the bits per sample is 0" do
      it "should return only the song audio quality" do
        song = build(:song)
        allow(song).to receive_message_chain(:s3_asset, :s3_detail, :metadata_json) do
          { "streams": [ "bits_per_sample": 0 ] }.with_indifferent_access
        end
        allow(song).to receive_message_chain(:s3_asset, :s3_detail, :file_type) { "wav" }

        result = helper.song_audio_quality(song)

        expect(result.strip).to eq "WAV"
      end
    end

    context "when the bits per sample is greater than 0" do
      it "should return the song audio quality and the bits_per_sample" do
        song = build(:song)
        allow(song).to receive_message_chain(:s3_asset, :s3_detail, :metadata_json) do
          { "streams": [ "bits_per_sample": 24 ] }.with_indifferent_access
        end
        allow(song).to receive_message_chain(:s3_asset, :s3_detail, :file_type) { "wav" }

        result = helper.song_audio_quality(song)

        expect(result).to eq "WAV 24"
      end
    end
  end

  describe "#display_album_state" do
    let(:album) { create(:album) }

    context "when the album has steps to go" do
      it "returns the amount of steps to go" do
        expect(helper.display_album_state(album)).to include("steps to go")
      end
    end
  end
end
