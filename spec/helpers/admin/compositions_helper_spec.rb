require "rails_helper"

describe Admin::CompositionsHelper, type: :helper do
  describe "#formatted_response_msg" do
    let(:composition) { create(:composition, name: "Composition Name") }

    context "when there is no error message" do
      subject { helper.formatted_response_msg(composition, nil) }

      it { should eq("<span class='success'>Successfully sent #{composition.name} to Rights App</span>") }
    end

    context "when there is an error message" do
      error_msg = "Failed to send to Rights App"
      subject { helper.formatted_response_msg(composition, error_msg) }

      it { should eq("<span class='error'>#{error_msg}</span>") }
    end
  end

  describe "#editable?" do
    let(:composition) { build(:composition) }

    subject { helper.editable?(composition) }

    context "composition does not have a provider identifier" do
      before { composition.provider_identifier = nil }

      it { should be_truthy }
    end

    context "composition does not have a recording recording_code" do
      before do
        composition.update(provider_identifier: "12345")
      end

      it { should be_truthy }
    end

    context "composition has a provider identifier and a recording code" do
      before do
        composition.update(provider_identifier: "12345")
        composition.recordings << create(:recording)
      end

      it { should be_falsey }
    end
  end

  describe "#composition_recording" do
    let(:composition) { create(:composition, :with_recordings) }

    context "when the composition does not have any recordings" do
      before { composition.recordings.destroy_all }

      it "should build a new recording" do
        helper.composition_recording(composition)

        recording = composition.recordings.first
        expect(recording.new_record?).to be_truthy
      end
    end

    context "when the composition has a recording" do
      it "returns the recording" do
        helper.composition_recording(composition)

        recording = composition.recordings.first
        expect(helper.composition_recording(composition)).to eq recording
      end
    end
  end
end
