require "rails_helper"

describe Admin::RightsHelper do
  describe "#admin_role_checkbox_class" do
    it "displays .admin-role-checkbox for an admin role" do
      expect(helper.admin_role_checkbox_class(Role.find_by(name: "Admin"))).to eq("admin-role-checkbox")
    end

    it "displays .other-administrative-role-checkboxes for other administrative roles" do
      role = Role.where(is_administrative: true).where("name != 'Admin'").first
      expect(helper.admin_role_checkbox_class(role)).to eq("other-administrative-role-checkboxes")
    end

    it "displays .other-role-checkboxes for other non-administrative roles" do
      role = Role.where(is_administrative: false).first
      expect(helper.admin_role_checkbox_class(role)).to eq("other-role-checkboxes")
    end
  end

  describe "#admin_role_list_item_class" do
    it "displays .admin-role for an admin role" do
      expect(helper.admin_role_list_item_class(Role.find_by(name: "Admin"))).to eq("admin-role")
    end

    it "displays .other-administrative-roles for other administrative roles" do
      role = Role.where(is_administrative: true).where("name != 'Admin'").first
      expect(helper.admin_role_list_item_class(role)).to eq("other-administrative-roles")
    end

    it "displays .other-roles for other non-administrative roles" do
      role = Role.where(is_administrative: false).first
      expect(helper.admin_role_list_item_class(role)).to eq("other-roles")
    end
  end

  describe "#should_display_initially?" do
    let(:user) { create(:person) }
    let(:admin_role) { Role.find_by(name: "Admin") }
    it "returns true if a user is already an admin" do
      user.roles << admin_role
      selected_roles = user.roles.administrative.group_by(&:name)
      role = Role.where(is_administrative: true).where("name != 'Admin'").first

      expect(helper.should_display_initially?(selected_roles, role)).to be_truthy
    end

    it "returns false if a user is not already an admin" do
      selected_roles = {}
      role = Role.where(is_administrative: true).where("name != 'Admin'").first
      expect(helper.should_display_initially?(selected_roles, role)).to be_falsey
    end

    it "returns true if role is Admin" do
      selected_roles = {}
      expect(helper.should_display_initially?(selected_roles, admin_role)).to eq true
    end

    it "returns true if role is non-administrative" do
      selected_roles = {}
      non_administrative_role = Role.where(is_administrative: false).first
      expect(helper.should_display_initially?(selected_roles, non_administrative_role)).to eq true
    end
  end
end
