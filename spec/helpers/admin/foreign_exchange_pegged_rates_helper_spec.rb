require 'rails_helper'
RSpec.describe Admin::ForeignExchangePeggedRatesHelper, type: :helper do
  describe "price_preview_products" do
    it "returns list of formatted comparitive pegged prices" do
      current_pegged_rate = build(:foreign_exchange_pegged_rate)
      new_pegged_rate = rand(20..100.0).round(2)
      expect(helper.price_preview_products(current_pegged_rate, new_pegged_rate).map(&:keys).uniq.flatten).to include(:name, :usd_price, :pegged_previous_price, :pegged_updated_price)
    end
  end
end
