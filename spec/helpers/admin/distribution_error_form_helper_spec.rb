require "rails_helper"

describe Admin::DistributionErrorFormHelper do
  describe "#decorated_distro_error_form" do
    let(:distro_error_form) { Distribution::ErrorForm.new({}).save }

    it "should instantiate a DistributionErrorFormDecorator instance" do
      expect(DistributionErrorFormDecorator).to receive(:new).with(distro_error_form)
      helper.decorated_distro_error_form(distro_error_form)
    end
  end
end
