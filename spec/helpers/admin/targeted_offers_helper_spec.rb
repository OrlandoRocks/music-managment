require "rails_helper"

describe Admin::TargetedOffersHelper do
  describe "#offer_type_select" do
    it "outputs select dropdown options for offers" do
      expected_output = [
        ["New Customers", "new"],
        ["Existing Customers", "existing"],
        ["Country Targeted Offer", "country_targeted"]
      ]

      output = helper.offer_type_select

      expect(output).to eq(expected_output)
    end
  end
end
