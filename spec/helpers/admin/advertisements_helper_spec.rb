require "rails_helper"

describe Admin::AdvertisementsHelper, type: :helper do
  describe "#set_country_links" do
    before do
      # `set_country_links` needs to be called from within a controller (with `controller_name` set).
      allow_any_instance_of(ActionController::Base).to receive(:controller_name).and_return('/admin/advertisements')
    end

    it "should not create link for country website passed in" do
      country_website = CountryWebsite.find_by(country: "US")

      result_string = helper.set_country_links(country_website).to_s

      expect(result_string.include?("United States |")).to eq(true)
    end

    it "should create links for country websites not passed in " do
      us_country_website = CountryWebsite.find_by(country: "US")

      result_string = helper.set_country_links(us_country_website).to_s

      CountryWebsite.all.each do |cw|
        next if us_country_website.country == cw.country
        expect_string = "#{cw.full_country_name}</a>"

        expect(result_string.include?(expect_string)).to eq(true)
      end
    end
  end
end
