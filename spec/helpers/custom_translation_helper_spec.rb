# encoding: utf-8

require "rails_helper"

describe CustomTranslationHelper, type: :helper do
  context "Locale is not default" do
    before(:each) do
      I18n.locale = :de
    end

    it "shows the default locale text if missing from current locale" do
      expect(helper.custom_t(:only_exists_in_en)).to have_content("Test Key for Fallbacks")
    end
  end

  describe "#translate_dates" do
    before(:each) do
      I18n.locale = :de
      allow(helper).to receive(:controller_name).and_return("album")
    end

    it "replaces a foreign language month name with the english month name" do
      params = fake_params("20 Oktober 2016", "25 Dezember 2016", "long_date")
      allow(helper).to receive(:params).and_return(params)
      helper.translate_dates

      expect(params[:album][:sale_date]).to eq "October 20, 2016"
      expect(params[:album][:orig_release_year]).to eq "December 25, 2016"
    end

    it "replaces a abbr month name with the english abbr month name" do
      params = fake_params("20 Mai 2016", "14 März 2016", "long_date")
      allow(helper).to receive(:params).and_return(params)
      helper.translate_dates

      expect(params[:album][:sale_date]).to eq "May 20, 2016"
      expect(params[:album][:orig_release_year]).to eq "March 14, 2016"
    end

    it "does not translate if the date field in params is empty" do
      params = fake_params("20 Mai 2016", "", "long_date")
      allow(helper).to receive(:params).and_return(params)
      helper.translate_dates

      expect(params[:album][:sale_date]).to eq "May 20, 2016"
      expect(params[:album][:orig_release_year]).to eq ""
    end

    it "translates date formats not containing %B or %b in it" do
      params = fake_params("01-02-2016", "22-12-2016", "dash")
      allow(helper).to receive(:params).and_return(params)
      helper.translate_dates

      expect(params[:album][:sale_date]).to eq "02-01-2016"
      expect(params[:album][:orig_release_year]).to eq "12-22-2016"
    end
  end
end

def fake_params(sale_date, orig_release_year, date_format)
  {
      "album" => {
          "sale_date" => sale_date,
          "orig_release_year" => orig_release_year
      },
      "translated_date_fields" => {
          "sale_date" => date_format,
          "orig_release_year" => date_format
      }
  }.with_indifferent_access
end
