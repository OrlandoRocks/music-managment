require "rails_helper"

describe TerminatedComposerHelper do
  let(:composer) { create(:composer) }

  describe "#existing_partially_terminated_composer" do
    it "returns a partially terminated composer associated to the composer" do
      create(:terminated_composer, :partially, composer: composer)
      expect(helper.existing_partially_terminated_composer(composer.id).present?).to be true
    end

    it "returns nothing if there is no associated partially terminated composer" do
      expect(helper.existing_partially_terminated_composer(composer.id).present?).to be false
    end
  end

  describe "#flash_success_notice" do
    it "returns a success notice" do
      helper.flash_success_notice
      expect(flash[:notice]).to be_present
    end
  end

  describe "#termination_params" do
    it "parses raw params" do
      raw_params = { terminated_composer: { composer_id: composer.id, effective_date: "", admin_note: "", publishing_termination_reason_id: 1 }}

      expect(helper.termination_params(raw_params))
        .to eq(composer_id: composer.id, effective_date: "", admin_note: "", publishing_termination_reason_id: 1)
    end
  end

end
