require "rails_helper"

describe PeopleHelper, type: :helper do
  let(:person) { create(:person) }
  let(:tax_form) { create(:tax_form, :w9_individual, person: person) }

  before(:each) do
    helper.extend(MockApplicationHelper)
    allow(helper).to receive(:current_user).and_return(person)
  end

  context "default_country receives US website" do
    it "should return name of us country" do
      country = Country.find_by(iso_code: "US")
      expect(helper.default_country("US")).to eq(country.iso_code)
    end
  end

  context "default_country receives CA website" do
    it "should return name of ca country" do
      country = Country.find_by(iso_code: "CA")
      expect(helper.default_country("CA")).to eq(country.iso_code)
    end
  end

  context "default_country receives UK website" do
    it "should return name of uk country" do
      country = Country.find_by(iso_code: "GB")
      expect(helper.default_country("UK")).to eq(country.iso_code)
    end
  end

  context "default_country receives DE website" do
    it "should return name of de country" do
      country = Country.find_by(iso_code: "DE")
      expect(helper.default_country("DE")).to eq(country.iso_code)
    end
  end

  context "default_country receives AU website" do
    it "should return name of us" do
      country = Country.find_by(iso_code: "AU")
      expect(helper.default_country("AU")).to eq(country.iso_code)
    end
  end

  context "default_country receives non-existent website" do
    it "should return name of us" do
      expect(helper.default_country("MORDOR")).to eq("US")
    end
  end

  describe "#show_taxform_mapping?" do
    before(:each) do
      assign(:primary_tax_form, "primary-tax-form")
      assign(:secondary_tax_form, "secondary-tax-form")
    end

    context "when 'tax_form_mapping' feature flag is set" do
      before(:each) do
        allow(FeatureFlipper).to receive(:show_feature?)
          .with(:tax_form_mapping, person).and_return(true)
      end

      it "returns true for us users having both primary and secondary tax-forms" do
        expect(person.from_united_states_and_territories?).to eq(true)
        expect(helper.show_taxform_mapping?).to eq(true)
      end
    end

    context "when 'tax_form_mapping' feature flag is set" do
      before(:each) do
        allow(FeatureFlipper).to receive(:show_feature?)
          .with(:tax_form_mapping, person).and_return(false)
      end

      it "returns false for us users having both primary and secondary tax-forms" do
        expect(person.from_united_states_and_territories?).to eq(true)
        expect(helper.show_taxform_mapping?).to eq(false)
      end
    end
  end

  describe "#selected_distribution_tax_form" do
    context "when user's primary tax-form is mapped to distribution stream" do
      it "returns :primary" do
        assign(:primary_tax_form, tax_form)
        assign(:user_mapped_distribution_tax_form, tax_form)
        expect(helper.selected_distribution_tax_form).to eq(:primary)
      end
    end

    context "when user's secondary tax-form is mapped to distribution stream" do
      it "returns :secondary" do
        assign(:secondary_tax_form, tax_form)
        assign(:user_mapped_distribution_tax_form, tax_form)
        expect(helper.selected_distribution_tax_form).to eq(:secondary)
      end
    end
  end

  describe "#selected_publishing_tax_form" do
    context "when user's primary tax-form is mapped to publishing stream" do
      it "returns :primary" do
        assign(:primary_tax_form, tax_form)
        assign(:user_mapped_publishing_tax_form, tax_form)
        expect(helper.selected_publishing_tax_form).to eq(:primary)
      end
    end

    context "when user's secondary tax-form is mapped to distribution stream" do
      it "returns :secondary" do
        assign(:secondary_tax_form, tax_form)
        assign(:user_mapped_publishing_tax_form, tax_form)
        expect(helper.selected_publishing_tax_form).to eq(:secondary)
      end
    end
  end

  describe "#tax_form_display_text" do
    it "returns display text for a tax-form" do
      tax_form = create(:tax_form, :w9_individual)
      expect(helper.tax_form_display_text(tax_form)).to be_present
    end

    it "returns nil when a tax-form is not provided" do
      expect(helper.tax_form_display_text("not-a-tax-form")).to be_nil
    end
  end
end
