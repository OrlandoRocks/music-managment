require "rails_helper"

describe BalanceHistoryHelper do
  describe '#headers' do
    it 'should always include mandatory headers' do
      transaction_headers = helper.headers(:transaction_history)
      pending_headers     = helper.headers(:pending_transactions)

      expect(transaction_headers).to include(BalanceHistoryHelper::MANDATORY_HEADERS.first)
      expect(pending_headers).to include(BalanceHistoryHelper::MANDATORY_HEADERS.first)
    end

    context "transaction history" do
      it 'should include balance if the table key is transaction_history' do
        expect(helper.headers(:transaction_history)).to include(:balance)
      end

      it 'should not include status or action' do
        headers = helper.headers(:transaction_history)
        expect(headers).not_to include(:status, :action)
      end
    end

    context 'pending transactions' do
      it 'should include status and action' do
        headers = helper.headers(:pending_transactions)
        expect(headers).to include(:status, :action)
      end

      it 'should not include balance' do
        headers = helper.headers(:pending_transactions)
        expect(headers).not_to include(:balance)
      end
    end

    context "admin_transaction_history" do
      it "should include debit, credit and balance headers" do
        headers = helper.headers(:admin_transaction_history)
        expect(headers).to include(:debit, :credit, :balance)
      end
    end
  end

  describe "to_date" do
    context "US users" do
      it "converts a date to MM/DD/YY" do
        date = DateTime.now
        formatted = date.strftime("%m/%d/%y")
        expect(helper.to_date(date)).to eq formatted
      end
    end
  end

  describe "#filter_options" do
    helper do
      def current_user; end
    end

    before do
      person = create(:person)
      allow(helper).to receive(:current_user).and_return(person)
    end

    it "renders a list of filter options" do
      allow(Payoneer::FeatureService).to receive(:payoneer_payout_enabled?).and_return(true)
      expect(helper.filter_options).to include([
        "payout_transfer",
        "Payoneer Withdrawals"
      ])
    end
  end

  describe "#get_txn_ids" do
    let(:person) { create(:person) }
    it "should return an Array of ids" do
      5.times do
        create(:payout_transfer, person: person)
      end

      transactions = person.person_transactions

      expect(get_txn_ids(transactions)).to be_a Array
      expect(get_txn_ids(transactions)).to eq transactions.select(:id)
    end
  end

  context "money calculations" do
    let(:parsed_pos_value) { '<span class="balance-history__value--positive">$99,500.00</span>' }

    it "assigns the correct class for a positive transaction" do
      expect(txn_value_class(-100)).to eq 'balance-history__value'
      expect(txn_value_class(100)).to eq 'balance-history__value--positive'
    end

    it "converts a transaction's BigDecimal values to money" do
      amt = BigDecimal.new(1000000)
      to_money = Money.new(100000000)
      expect(helper.parse_money(amt)).to eq to_money
    end

    it "can calculate the value of a transaction" do
      txn = double(credit: 100000, debit: 500)
      expect(helper.current_transaction_value(txn)).to eq parsed_pos_value
    end

    it "converts pending transactions to an absolute value" do
      txn = double(credit: 500, debit: 100000)
      expect(helper.pending_transaction_value(txn)).to eq parsed_pos_value
    end

    it "can calculate the net value change within a transaction" do
      expect(helper.net_value(double(credit: 300, debit: 350))).to eq Money.new(-50_00)
    end

    it "can calculate the change in balance as a result of the transaction" do
      txn = double(credit: 100000, debit: 500, previous_balance: 1)
      expect(helper.balance_change(txn)).to eq '$99,501.00'
    end
  end

  describe "#parsed_transaction_value" do
    let(:transaction) { double(credit: 500, debit: 100000, current: "USD") }

    context "when parsing the debit value" do
      it "correctly formats the transaction's debit" do
        expect(helper.parsed_transaction_value(transaction.debit)).to eq "$100,000.00"
      end
    end

    context "when parsing the credit value" do
      it "correctly formats the transaction's credit" do
        expect(helper.parsed_transaction_value(transaction.credit)).to eq "$500.00"
      end
    end
  end

  context "multiple currency handling" do
    describe "#transaction_currency?" do
      context "when selected currency is nil" do
        it "returns false" do
          assign(:selected_currency, nil)
          assign(:transaction_currency, nil)
          expect(helper.transaction_currency?).to be(false)
        end
      end
      context "when selected and transaction currency differ" do
        it "returns false" do
          assign(:selected_currency, "INR")
          assign(:transaction_currency, "USD")
          expect(helper.transaction_currency?).to be(false)
        end
      end
      context "when selected and transaction currency are the same" do
        it "returns true" do
          assign(:selected_currency, "USD")
          assign(:transaction_currency, "USD")
          expect(helper.transaction_currency?).to be(true)
        end
      end
    end
    describe "#balance_history_money" do
      helper do
        def current_user; end
      end
      before(:each) do
        person = create(:person)
        @transaction = create(:person_transaction, person: person)
        allow(helper).to receive(:current_user).and_return(person)
      end
      context "when mutliple currency domain and transaction currency is selected" do
        it "calls format_to_currency" do
          assign(:multiple_currency_domain, true)
          allow(helper).to receive(:transaction_currency?).and_return(true)

          money = @transaction.debit.truncate(2).to_money(@transaction.currency)

          expect(helper).to receive(:format_to_currency)
          helper.balance_history_money(money)
        end
      end
      context "when not mutliple currency domain" do
        it "calls balance_to_currency" do
          assign(:multiple_currency_domain, false)
          allow(helper).to receive(:transaction_currency?).and_return(true)

          money = @transaction.debit.truncate(2).to_money(@transaction.currency)

          expect(helper).to receive(:balance_to_currency)
          helper.balance_history_money(money)
        end
      end
      context "when not transaction currency is selected" do
        it "calls balance_to_currency" do
          assign(:multiple_currency_domain, true)
          allow(helper).to receive(:transaction_currency?).and_return(false)

          money = @transaction.debit.truncate(2).to_money(@transaction.currency)

          expect(helper).to receive(:balance_to_currency)
          helper.balance_history_money(money)
        end
      end
    end
    describe "#balance_history_currency" do
      before(:each) do
        person = create(:person)
        @transaction = create(:person_transaction, person: person)
      end
      context "when multiple currency domain" do
        it "returns selected currency" do
          assign(:multiple_currency_domain, false)
          transaction_currency = @transaction.currency

          expect(helper.balance_history_currency(transaction_currency)).to eq(transaction_currency)
        end
      end
      context "when not multiple currency domain" do
        it "returns transaction currency" do
          assign(:multiple_currency_domain, true)
          assign(:selected_currency, 'INR')
          selected_currency = assign(:selected_currency, 'INR')
          transaction_currency = @transaction.currency

          expect(helper.balance_history_currency(transaction_currency)).to eq(selected_currency)
        end
      end
    end
  end
end
