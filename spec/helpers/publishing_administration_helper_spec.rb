require "rails_helper"

describe PublishingAdministrationHelper do
  describe ".pro_name" do
    context "when an invalid performing_rights_organization is passed in" do
      it "returns nil" do
        pro_id = "ABC"
        expect(helper.pro_name(pro_id)).to be_nil
      end
    end

    context "when a valid performing_rights_organization is passed in" do
      it "returns the performing_rights_organization name" do
        pro = PerformingRightsOrganization.first
        expect(helper.pro_name(pro.id)).to eq pro.name
      end
    end
  end

  describe ".cae_number_for" do
    context "when the composer has a CAE number" do
      let(:composer) { create(:composer, cae: "123456789") }

      it "returns the CAE number" do
        composer_presenter = ComposerPresenter.new(composer)
        expect(helper.cae_number_for(composer_presenter)).to eq "123456789"
      end
    end
  end

  describe ".send_publisher?" do
    context "when the composer has a publisher with a cae and an eligible PRO" do
      it "returns true" do
        account   = create(:account)
        ascap     = PerformingRightsOrganization.find_by(name: "ASCAP")
        publisher = create(:publisher, performing_rights_organization_id: 56)
        composer  = create(
          :composer,
          account:    account,
          cae:        "123456789",
          publisher:  publisher,
          performing_rights_organization: ascap
        )

        expect(helper.send_publisher?(composer)).to be true
      end
    end

    context "when the composer does not have a publisher" do
      it "returns false" do
        composer = create(:composer)
        expect(helper.send_publisher?(composer)).to be false
      end
    end

    context "when the composer's PRO is not ASCAP, BMI, or SESAC" do
      it "returns false" do
        account   = create(:account)
        pro       = create(:performing_rights_organization)
        publisher = create(:publisher)
        composer  = create(
          :composer,
          account:    account,
          cae:        "123456789",
          publisher:  publisher,
          performing_rights_organization: pro
        )
        expect(helper.send_publisher?(composer)).to be false
      end
    end
  end

  describe ".name_affixes_translations" do
    context "when a prefix" do
      it "returns the name_prefix translations" do
        expected_prefix_values = [nil, "Dr.", "Mr.", "Mrs.", "Ms."]
        expect(helper.name_affixes_translations("prefix").map(&:last)).to match expected_prefix_values
      end
    end

    context "when a suffix" do
      it "returns the name_suffix translations" do
        expected_suffix_values = [nil, "I", "II", "III", "IV", "Jr.", "Sr."]
        expect(helper.name_affixes_translations("suffix").map(&:last)).to match expected_suffix_values
      end
    end
  end

  describe ".current_user_has_composers?" do
    it "returns true when a user has composers" do
      person    = create(:person)
      account   = create(:account, person: person)
      create(:publishing_composer, account: account, person: person)

      helper.extend(MockApplicationHelper)
      helper.current_user = person
      expect(helper.current_user_has_composers?).to eq(true)
    end
  end

  describe ".managed_composer?" do
    before(:each) do
      helper.extend(MockApplicationHelper)
      @person = create(:person)
      @tunecore_person = create(:person)
      @account = create(:account)
      @publishing_composer = create(:publishing_composer, person: @tunecore_person, account: @account)
      helper.current_user = @person
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
    end

    context "when a user is a managing publishing_composer" do
      it "returns false when a user has composers through the account" do
        expect(helper.managed_composer?).to eq(false)
      end
    end

    context "when a user is a managed publishing_composer" do
      it "returns true when a user has composers through the person, not the person's account." do
        @publishing_composer.update_attributes(person_id: @person.id)
        expect(helper.managed_composer?).to eq(true)
      end
    end
  end

  describe ".managing_composer?" do
    context "when a user is a managing publishing_composer" do
      it "returns true when a user has composers through the account" do
        person    = create(:person)
        account   = create(:account, person: person)
        create(:publishing_composer, account: account, person: person)
        helper.extend(MockApplicationHelper)
        helper.current_user = person
        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
        expect(helper.managing_composer?).to eq(true)
      end
    end

    context "when a user is a managed publishing_composer" do
      it "returns false when a user has composers through the person, not the person's account." do
        person = create(:person)
        create(:account, person: person)
        create(:publishing_composer, person: person)
        helper.extend(MockApplicationHelper)
        helper.current_user = person

        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
        expect(helper.managing_composer?).to eq(false)
      end
    end
  end

  describe ".ntc_composition_form_enabled?" do
    let(:person) { create(:person) }

    before(:each) do
      helper.extend(MockApplicationHelper)
      helper.current_user = person
    end

    it "returns false if the user does not manage the composer" do
      composer = create(:composer, person: person)
      account  = create(:account)
      composer.update(account: account)

      expect(helper.ntc_composition_form_enabled?(composer)).to be_falsey
    end

    it "returns true if the user manages the composer" do
      account  = create(:account, person: person)
      composer = create(:composer, account: account)

      expect(helper.ntc_composition_form_enabled?(composer)).to be_truthy
    end
  end

  describe ".edit_composition_feature_enabled?" do
    let(:account) { create(:account) }
    let(:composer) do
      create(
        :composer,
        :with_pub_admin_purchase,
        person: account.person,
        account: account,
        performing_rights_organization_id: 1,
        cae: "123456789"
      )
    end

    subject { helper.edit_composition_feature_enabled?(composer) }

    before(:each) do
      helper.extend(MockApplicationHelper)
      helper.current_user = account.person
    end

    context "when the edit compositions feature is not active for the user" do
      it { should be_falsey }
    end

    context "when the edit compositions feature is active for the user" do
      before { allow(FeatureFlipper).to receive(:show_feature?) { true } }

      it { should be_truthy }
    end
  end

  describe ".display_composition_manager_link?" do
    context "when an account manages one active publishing_composer" do
      it "returns true" do
        allow(FeatureFlipper).to receive(:show_feature?) { true }
        account = create(:account)
        create(:publishing_composer, :with_pub_admin_purchase, account: account, performing_rights_organization_id: 1, cae: "123456789")
        account.person.publishing_composers.primary.create!(
          first_name:         account.person.first_name,
          last_name:          account.person.last_name,
          email:              account.person.email,
          dob:                "1900-01-01",
          agreed_to_terms_at: "1900-01-01",
          account_id:         account.id,
          publishing_role_id: PublishingRole::ON_BEHALF_OF_SELF_ID
        )

        helper.extend(MockApplicationHelper)
        helper.current_user = account.person

        expect(helper.display_composition_manager_link?).to be_truthy
      end
    end

    context "when an account manages more than one active publishing_composer" do
      it "returns false" do
        allow(FeatureFlipper).to receive(:show_feature?) { true }
        account = create(:account)
        create(:publishing_composer, :with_pub_admin_purchase, account: account)
        create(:publishing_composer, :with_pub_admin_purchase, account: account)

        helper.extend(MockApplicationHelper)
        helper.current_user = account.person

        expect(helper.display_composition_manager_link?).to be_falsey
      end
    end
  end

  describe ".tax_info_agreed_to_date" do
    it "returns the W9 agreed to date if present" do
      tax_info = double(TaxInfo, agreed_to_w9_at: "2019-01-01")
      expect(helper.tax_info_agreed_to_date(tax_info)).to eq "2019-01-01"
    end

    it "returns the W8BEN agreed to date if present and W9 date is not" do
      tax_info = double(TaxInfo, agreed_to_w9_at: nil, agreed_to_w8ben_at: "2019-01-02")
      expect(helper.tax_info_agreed_to_date(tax_info)).to eq "2019-01-02"
    end
  end
end
