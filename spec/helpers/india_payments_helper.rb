require 'rails_helper'

describe IndiaPaymentsHelper, type: :helper do
  context "#can_use_payments_os?" do
    let(:india_user) { create(:person, :with_india_country_website) }
    let(:us_user) { create(:person) }
    it "should allow payments os only for India country website users" do
      expect(helper.can_use_payments_os?(india_user)).to be_truthy
    end

    it "should return false for other website users" do
      expect(helper.can_use_payments_os?(us_user)).to be_falsey
    end
  end
end
