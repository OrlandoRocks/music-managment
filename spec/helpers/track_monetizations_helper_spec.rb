require "rails_helper"

describe TrackMonetizationsHelper do
  class FakeTrackMonController < ApplicationController
    include TrackMonetizationsHelper
    attr_accessor :current_user
    def initialize(current_user)
      @current_user = current_user
    end
  end

  before do
    @person = create(:person, :with_monetizable_tracks)
    @fake_helper = FakeTrackMonController.new(@person)
  end

  describe '#already_subscribed?' do

    context 'FBTracks' do
      it 'returns false if user is not yet subscribed' do
        expect(@fake_helper.already_subscribed?('FBTracks')).to be false
      end

      it 'returns true if user is already subscribed' do
        create(
          :person_subscription_status,
          :with_subscription_event_and_purchase,
          subscription_type: "FBTracks",
          person: @person
        )

        expect(@fake_helper.already_subscribed?('FBTracks')).to be true
      end
    end

    context 'YTTracks' do
      it 'returns false if user is not yet subscribed' do
        expect(@fake_helper.already_subscribed?('YTTracks')).to be false
      end

      it 'returns true if user is already subscribed' do
        create(
          :person_subscription_status,
          :with_subscription_event_and_purchase,
          subscription_type: "YTTracks",
          person: @person
        )

        expect(@fake_helper.already_subscribed?('YTTracks')).to be true
      end
    end
  end

  describe '#can_monetize_fb_tracks?' do
    it 'returns false if user is not yet subscribed to FBTracks' do
      allow(FeatureFlipper).to receive(:show_feature?) { true }
      expect(@fake_helper.can_monetize_fb_tracks?).to be false
    end

    it 'returns true if user is subscribed to FBTracks' do
      create(
        :person_subscription_status,
        :with_subscription_event_and_purchase,
        subscription_type: "FBTracks",
        person: @person
      )

      expect(@fake_helper.can_monetize_fb_tracks?).to be true
    end
  end

end
