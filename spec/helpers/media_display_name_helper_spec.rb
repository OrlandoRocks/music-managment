require "rails_helper"

describe MediaDisplayNameHelper, type: :helper do
  describe "display_name" do
    context "for album" do
      context "album name contains (feat)" do
        it "returns the album name containg (feat)" do
          album = create(:album, name: "My Album (feat. Sophie)")
          expect(helper.display_name(album)).to eq("My Album (feat. Sophie)")
        end
      end

      context "album has featured artists" do
        it "returns the album name containing (feat)" do
          single = create(:single, name: "My Album")
          single.add_featured_artist('Sophie')
          expect(helper.display_name(single.reload)).to eq("My Album (feat. Sophie)")
        end
      end
    end

    context "for song" do
      context "song name contains (feat)." do
        it "returns the song name containing '(feat)'" do
          song = create(:song, name: "My Song (feat. Sophie)")
          expect(helper.display_name(song)).to eq("My Song (feat. Sophie)")
        end
      end
      context "if has featured artists" do
        it "returns the song name containing '(feat)'" do
          song = create(:song, name: "My Album")
          song.add_featured_artist("Sophie")
          expect(helper.display_name(song.reload)).to eq("My Album (feat. Sophie)")
        end
      end
    end
  end
end
