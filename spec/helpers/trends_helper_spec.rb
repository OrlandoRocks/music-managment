require "rails_helper"

describe TrendsHelper, type: :helper do
  before :each do
    @person = TCFactory.build_person
    @album = TCFactory.create_purchaseable_album(:person => @person, :album_type => "Album", :name => 'DK Greatest Hits')
    @record = Hash.new
    @record['album_id'] = @album.id
    @record['id'] = @album.songs.first.id
  end

  describe "#trend_song_title" do
    it "should return song title from album if record not present" do
      expect(helper.trend_song_title(@record, @person)).to eq(@album.songs.first.name)
    end

    it "should return song title from record" do
      @record['name'] = "From Record"
      expect(helper.trend_song_title(@record, @person)).to eq("From Record")
    end
  end

  describe "#trend_album_title" do
    it "should return album title from album if record not present" do
      expect(helper.trend_album_title(@record, @person)).to eq(@album.name)
    end

    it "should return album title from record" do
      @record['album_name'] = "From Record"
      expect(helper.trend_album_title(@record, @person)).to eq("From Record")
    end
  end

  describe "#trend_artist_name" do
    it "should return artist name from album if record not present" do
      expect(helper.trend_artist_name(@record, @person)).to eq(@album.artist_name)
    end

    it "should return artist name from record" do
      @record['artist_name'] = "From Record"
      expect(helper.trend_artist_name(@record, @person)).to eq("From Record")
    end
  end

  describe "#trend_release_type" do
    it "should return release_type from album if record not present" do
      expect(helper.trend_release_type(@record, @person)).to eq(@album.album_type)
    end

    it "should return release_type record" do
      @record['album_type'] = "From Record"
      expect(helper.trend_release_type(@record, @person)).to eq("From Record")
    end
  end

end
