require "rails_helper"

describe V2::JwtHelper, type: :helper do
  let!(:person) { create(:person) }
  let!(:payload) do
    {
      id: person.id,
      email: "foo",
      name: "bar",
      roles: ["baz"]
    }
  end
  let!(:valid_token) { generate_jwt(payload: payload.dup) }
  let!(:invalid_signature_token) { "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJleHAiOjE4OTM0NzQwMDB9.8f_6UuFSgggF7DWYQCQjipkzMnGw_rYn6EmY2ktvEOU" }
  let!(:expired_token) { generate_jwt(payload: payload.dup, duration: -1) }

  after do
    redis.del(digest_key(digest(valid_token)))
    redis.del(digest_key(digest(invalid_signature_token)))
    redis.del(digest_key(digest(expired_token)))
  end

  describe "#decode_header_token," do
    context "valid token," do
      it "returns payload" do
        helper.request.env["HTTP_AUTHORIZATION"] = valid_token

        expect(helper.decode_header_token.values).to include(*payload.values)
      end
    end

    context "invalid signature," do
      it "returns {}" do
        helper.request.env["HTTP_AUTHORIZATION"] = invalid_signature_token

        expect(helper.decode_header_token).to eq({})
      end
    end

    context "expired," do
      it "returns {}" do
        helper.request.env["HTTP_AUTHORIZATION"] = expired_token

        expect(helper.decode_header_token).to eq({})
      end
    end
  end

  describe "#generate_jwt," do
    it "returns payload with exp added" do
      result = helper.generate_jwt(payload: { foo: "bar" })
      helper.request.env["HTTP_AUTHORIZATION"] = result

      expect(helper.decode_header_token["foo"]).to eq("bar")
      expect(helper.decode_header_token["exp"]).to be_a(Integer)
    end
  end

  describe "#saved_token," do
    context "when present," do
      before do
        incoming_token = helper.generate_jwt(payload: payload.dup)
        duration = described_class::EXPIRY_DURATION
        save_token_digest(incoming_token, duration, person.id)
        helper.request.env["HTTP_AUTHORIZATION"] = incoming_token
      end

      it "returns payload" do
        expect(helper.saved_token.values).to include(*payload.values)
      end
    end

    context "when expired," do
      before do
        incoming_token = helper.generate_jwt(payload: payload.dup, duration: -1)
        save_token_digest(incoming_token, 1, person.id)
        helper.request.env["HTTP_AUTHORIZATION"] = incoming_token
      end

      it "returns nil" do
        sleep(1)
        expect(helper.saved_token).to be_nil
      end
    end

    context "when absent," do
      before do
        incoming_token = helper.generate_jwt(payload: payload.dup)
        helper.request.env["HTTP_AUTHORIZATION"] = incoming_token
      end

      it "returns nil" do
        expect(helper.saved_token).to be_nil
      end
    end
  end
end
