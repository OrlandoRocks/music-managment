require "rails_helper"

describe AlbumHelper do
  class DummyAlbumController
    include AlbumHelper

    attr_reader :redirected, :redirected_path, :current_user, :request
    attr_accessor :params, :session

    def initialize(params = {})
      @params       = params
      @session      = {}
    end

    def controller_name
      'album'
    end
  end

  describe "#has_itunes_links?" do
    let(:album) { build(:album) }

    it "returns false if the album is not on itunes" do
      expect(helper.has_itunes_links?(album)).to eq false
    end

    it "returns false if the album is a 'Ringtone'" do
      allow(album).to receive(:live_in_itunes?)    { true }
      allow(album).to receive_message_chain(:class, :name) { "Ringtone" }
      expect(helper.has_itunes_links?(album)).to eq false
    end

    it "returns true if the album is on itunes and is not a Ringtone" do
      allow(album).to receive(:live_in_itunes?)    { true }
      allow(album).to receive_message_chain(:class, :name) { "Album" }
      expect(helper.has_itunes_links?(album)).to eq true
    end
  end

  describe "#share_itunes_links_if_eligible" do
    let(:share_itunes_links_partial) { "tc_social/share_itunes_links" }

    context "when the user is not eligible" do
      let(:person) { create(:person, :with_live_album) }
      let(:album)  { person.albums.first }

      context "when the user is not on itunes" do
        it "should not render the share_itunes_links template" do
          allow(helper).to receive(:render).and_call_original

          helper.share_itunes_links_if_eligible(album, person)

          expect(helper).to_not have_received(:render)
        end
      end

      context "when the user does not have tc social" do
        it "should not render the share_itunes_links template" do
          allow(helper).to receive(:render).and_call_original
          allow(album).to receive(:live_in_itunes?)                  { true }
          allow(person).to receive_message_chain(:country_website, :country) { false }

          helper.share_itunes_links_if_eligible(album, person)

          expect(helper).to_not have_received(:render)
        end
      end

      context "when the album has been taken down" do
        it "should not render the share_itunes_links template" do
          allow(helper).to receive(:render).and_call_original
          allow(person).to receive_message_chain(:country_website, :country) { false }
          allow(album).to receive(:live_in_itunes?)                  { true }
          allow(album).to receive(:takedown_at)                      { Date.today }

          helper.share_itunes_links_if_eligible(album, person)

          expect(helper).to_not have_received(:render)
        end
      end
    end

    context "when the user is eligible" do
      let(:person) { create(:person, :with_live_album, :with_tc_social) }
      let(:album)  { person.albums.first }

      it "should render the share_itunes_links template" do
        allow(helper).to receive(:render).and_call_original
        allow(album).to receive(:live_in_itunes?) { true }

        helper.share_itunes_links_if_eligible(album, person)

        expect(helper).to have_received(:render)
          .with(
            partial: share_itunes_links_partial,
            locals: {
              album: album,
              button_text: "Share with Tunecore Social",
              itunes_link: "",
              apple_music_link: ""
            }
          )
          .at_least(1).times
      end
    end
  end

  describe '#select_years_for_selection' do
    it 'should contain the years from 1900 to next year in the list.' do
      years = select_years_for_selection

      expect(years).to include(1900)
      expect(years).to include(Time.now.year)
      expect(years).to include(Time.now.year + 1)
    end

    it 'should not contain other years' do
      years = select_years_for_selection

      expect(years).to_not include(Time.now.year + 2)
      expect(years).to_not include(Time.now.year + 3)
      expect(years).to_not include(Time.now.year + 4)
    end
  end

  describe '#timed_release_meridian' do
    context "when golive date is after 12PM" do
      it 'should return PM ' do
        @album = create(:album)
        @album.golive_date = 1.days.from_now.end_of_day - 1.hour

        meridian = get_go_live_date_attribute(@album, :meridian)

        expect(meridian).to eq('PM')
      end
    end


    context "when golive date is before 12PM" do
      it "should return AM" do
        @album = create(:album)
        @album.golive_date = 1.days.from_now.beginning_of_day + 1.hour

        meridian = get_go_live_date_attribute(@album, :meridian)

        expect(meridian).to eq('AM')
      end
    end

    context "when golive date is at noon" do
      it "should return PM" do
        @album = create(:album)
        @album.golive_date = 1.days.from_now.beginning_of_day + 12.hour

        meridian = get_go_live_date_attribute(@album, :meridian)

        expect(meridian).to eq('PM')
      end
    end

    context "when golive date is at midnight" do
      it "should return PM" do
        @album = create(:album)
        @album.golive_date = 1.days.from_now.midnight

        meridian = get_go_live_date_attribute(@album, :meridian)

        expect(meridian).to eq('AM')
      end
    end
  end

  describe "#timed_release_absolute_time?" do
    context 'when a timed release timing scenario is absolute timing' do
      it 'should return true and same time should return false' do
        @album = create(:album)
        @album.timed_release_timing_scenario = 'absolute_time'

        expect(@album.timed_release_absolute_time?).to be(true)
      end
    end

    context 'when a timed release timing scenario is not absolute timing' do
      it 'should return false' do
        @album = create(:album)
        @album.timed_release_timing_scenario = 'relative_time'

        expect(@album.timed_release_absolute_time?).to be(false)
      end
    end
  end

  describe "#timed_release_same_time?" do
    context 'when a timed release timing scenario is same timing' do
      it 'should return true' do
        @album = create(:album)
        @album.timed_release_timing_scenario = 'relative_time'

        expect(@album.timed_release_relative_time?).to be(true)
      end
    end

    context 'when a timed release timing scenario is not same timing' do
      it 'should return false' do
        @album = create(:album)
        @album.timed_release_timing_scenario = 'absolute_time'

        expect(@album.timed_release_relative_time?).to be(false)
      end
    end
  end

  describe "#album_songs_status" do
    context "when the album should skip songwriter validations" do
      it "checks to see if the album has tracks" do
        has_tracks = double(:has_tracks)
        album = instance_spy(Album, skip_songwriter_validations?: true, has_tracks?: has_tracks)

        result = helper.album_songs_status(album)

        expect(result).to eq has_tracks
      end
    end

    context "when the album should not skip songwriter validations" do
      it "invokes the Album Completion Report Builder" do
        album = instance_spy(Album, skip_songwriter_validations?: false, id: 1)
        current_user = instance_spy(Person, id: 1)
        album_report = instance_spy(Album::CompletionReport, songs_complete?: double(:songs_complete))
        helper.extend(MockApplicationHelper)
        allow(helper).to receive(:current_user).and_return(album.person)
        allow(Album::CompletionReportBuilder).to receive(:build).with(album.id, current_user.id).and_return(album_report)

        result = helper.album_songs_status(album)

        expect(result).to eq album_report.songs_complete?
      end
    end
  end

  describe "#show_addon_header?" do
    let(:album) { build(:album) }
    let(:person) { create(:person) }

    before do
      helper.extend(MockApplicationHelper)
    end

    context "when show_automator_addon? returns true" do
      before do
        allow(helper).to(
          receive(:show_automator_addon?).and_return(true)
        )
      end

      it "returns true" do
        expect(helper.show_addon_header?(album)).to(be_truthy)
      end
    end

    context "when show_automator_addon? returns false" do
      before do
        allow(helper).to(
          receive(:show_automator_addon?).and_return(false)
        )
      end

      context "when the album has a Dolby Atmos song" do
        before do
          allow(album).to(
            receive(:dolby_atmos_audios?).and_return(true)
          )
        end

        it "returns true" do
          expect(helper.show_addon_header?(album)).to(be_truthy)
        end
      end

      context "when the album does not have a Dolby Atmos song" do
        before do
          allow(helper).to(
            receive(:show_automator_addon?).and_return(false)
          )
        end

        it "returns false" do
          expect(helper.show_addon_header?(album)).to(be_falsey)
        end
      end
    end
  end

  describe "#show_automator_addon?" do
    let(:album) { build(:album) }
    let(:person) { create(:person) }

    before do
      helper.extend(MockApplicationHelper)
      allow(helper).to receive(:current_user).and_return(person)
    end

    subject { helper.show_automator_addon?(album) }

    context "salepoints_redesign feature is disabled" do
      before do
        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .and_return(:default)

        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .with(:salepoints_redesign, anything)
          .and_return(false)
      end

      context "album supports subscriptions" do
        before { allow(album).to receive(:supports_subscriptions).and_return(true) }

        context "album is not rejected" do
          before { allow(album).to receive(:rejected?).and_return(false) }

          it { should be_truthy }
        end

        context "album is rejected" do
          before { allow(album).to receive(:rejected?).and_return(true) }

          it { should be_falsey }
        end
      end

      context "album does not support subscriptions" do
        before { allow(album).to receive(:supports_subscriptions).and_return(false) }

        it { should be_falsey }
      end
    end

    context "salepoints_redesign feature is enabled" do
      before do
        allow(FeatureFlipper)
          .to receive(:show_feature?)
          .with(:salepoints_redesign, anything)
          .and_return(true)
      end

      it { should be_falsey }
    end
  end

  describe "#check_store_automator?" do
    let(:album) { create(:album) }

    subject { helper.check_store_automator?(album) }

    context "when the album does not have store automator" do
      it { should be_falsey }
    end

    context "when the album has store automator" do
      before { album.add_automator }

      it { should be_truthy }
    end
  end

  describe "#show_automator_addon_if_finalized?" do
    let(:album) { build(:album) }
    let(:automator) { build(:salepoint_subscription)  }

    subject { helper.show_automator_addon_if_finalized?(album, automator) }

    context "when the album is not finalized" do
      it { should be_falsey }
    end

    context "when the album is finalized and is rejected" do
      before do
        album.finalized_at = Time.current
        album.legal_review_state = "REJECTED"
      end

      it { should be_falsey }
    end

    context "when the album is finalized, is not rejected and automator is finalized" do
      before do
        album.finalized_at = Time.current
        automator.finalized_at = Time.current
      end

      it { should be_falsey }
    end

    context "when the album is finalized, is not rejected and automator is not finalized" do
      before { album.finalized_at = Time.current }

      it { should be_truthy }
    end
  end

  describe "#btn_class_for_explicit_on" do
    context "with parental advisory enabled" do
      it "returns the option selected class" do
        single = build_stubbed(:single, parental_advisory: true)

        expect(helper.btn_class_for_explicit_on(single)).to eq("option-selected")
      end
    end

    context "with parental advisory disabled" do
      it "returns nil" do
        single = build(:single, parental_advisory: false)

        expect(helper.btn_class_for_explicit_on(single)).to be_nil
      end
    end

    context "with parental advisory not set" do
      it "returns nil" do
        single = build(:single, parental_advisory: nil)

        expect(helper.btn_class_for_explicit_on(single)).to be_nil
      end
    end
  end

  describe "#btn_class_for_explicit_off" do
    context "with parental advisory enabled" do
      it "returns nil" do
        single = build_stubbed(:single, parental_advisory: true)

        expect(helper.btn_class_for_explicit_off(single)).to be_nil
      end
    end

    context "with parental advisory disabled" do
      it "returns the option selected class" do
        single = build(:single, parental_advisory: false)

        expect(helper.btn_class_for_explicit_off(single)).to eq("option-selected")
      end
    end

    context "with parental advisory not set" do
      it "returns nil" do
        single = build(:single, parental_advisory: nil)

        expect(helper.btn_class_for_explicit_off(single)).to be_nil
      end
    end
  end

  describe "#get_all_main_artists" do
    let (:person) {create(:person)}

    def current_user
      person
    end

    it "should get all main artists that a given user has associated with them" do
      artist1   = create(:artist)
      artist2   = create(:artist)
      album     = create(:album, person: person)
      album2     = create(:album, person: person)

      creat1 = create(:creative, role: "featuring", creativeable: album, artist: artist1, person: person)
      creat2 = create(:creative, role: "featuring", creativeable: album2, artist: artist2, person: person)

      main_artists = get_all_main_artists

      expect(main_artists.size).to eq(4)
      expect(main_artists).to include(*album.creatives.joins(:artist).pluck('artists.name'))
      expect(main_artists).to include(*album2.creatives.joins(:artist).pluck('artists.name'))
      expect(main_artists).to include(creat1.artist.name)
      expect(main_artists).to include(creat2.artist.name)
    end
  end

  describe "#scrub_artist_names" do
    it "should take out HTML from artist names" do
      controller = DummyAlbumController.new({album: {creatives: [{name: '<div>Billy Bob</div>'}]}}.with_indifferent_access)
      controller.scrub_artist_names

      expect(controller.params[:album][:creatives][0][:name]).to eq('Billy Bob')
    end

    it "should not encode special characters" do
      controller = DummyAlbumController.new({album: {creatives: [{name: 'Billy & Bob'}]}}.with_indifferent_access)
      controller.scrub_artist_names

      expect(controller.params[:album][:creatives][0][:name]).to eq('Billy & Bob')
    end
  end

  describe "#aod_enabled_in_region?" do
    it "should return value from available_in_region?" do
      helper.extend(MockApplicationHelper)
      allow(helper).to receive(:country_website).and_return("US")
      expect(helper).to receive(:available_in_region?).with("US", "amazon_on_demand")

      helper.aod_enabled_in_region?
    end
  end

  describe "#show_aod?" do
    let(:person) { create(:person) }
    let(:album) { create(:album) }

    before do
      helper.extend(MockApplicationHelper)
      allow(helper).to receive(:current_user).and_return(person)
      allow(helper).to receive(:country_website).and_return("US")
    end

    subject { helper.show_aod?(album) }

    context "salepoints redesign enabled" do
      before { allow(helper).to receive(:salepoints_redesign_feature_enabled?).and_return(true) }

      context "album is not a ringtone" do
        context "aod enabled in region" do
          before { allow(helper).to receive(:aod_enabled_in_region?).and_return(true) }

          it { should be_truthy }
        end

        context "aod not enabled in region" do
          before { allow(helper).to receive(:aod_enabled_in_region?).and_return(false) }

          it { should be_falsey }
        end
      end
    end
  end

  describe "#automator_price_with_discount" do
    let(:person) { create(:person) }
    let(:album) { create(:album) }
    let(:product) { Product.find_by(country_website: person.country_website) }

    before do
      helper.extend(MockApplicationHelper)
      allow(helper).to receive(:current_user).and_return(person)
    end

    context "when no product is available" do
      it "should return 0" do
        allow(Product).to receive(:find_ad_hoc_product).and_return(nil)
        subject = helper.automator_price_with_discount(album)

        expect(subject).to eq(0.to_money)
      end
    end

    context "when product is available" do
      it "should invoke money discount" do
        allow(Product).to receive(:find_ad_hoc_product).and_return(product)
        Product.set_targeted_product_and_price(person, product)

        expect(helper)
          .to receive(:money_with_discount)
          .with(product.original_price, product.adjusted_price, product.currency)

        helper.automator_price_with_discount(album)
      end
    end
  end

   describe "#get_distribution_progress_level" do
    let(:person) { create(:person) }

    before do
      helper.extend(MockApplicationHelper)
      allow(helper).to receive(:current_user).and_return(person)
    end

    context "a distribution has artwork and songs" do
      it "should return a level of 3" do
        album = create(:album, :with_uploaded_songs_and_artwork, :with_songwriter)
        allow(album).to receive(:has_tracks?).and_return(true)
        allow(album).to receive(:skip_songwriter_validations?).and_return(true)

        expect(helper.get_distribution_progress_level(album)).to eq(3)
      end
    end

    context "distribution without artwork, songs or stores" do
      it "should return a level of 3" do
        album = create(:album)

        expect(helper.get_distribution_progress_level(album)).to eq(1)
      end
    end

    context "distribution with artwork" do
      it "should return a level of 2 with details" do
        album = create(:album, :with_artwork)

        expect(helper.get_distribution_progress_level(album)).to eq(2)
      end
    end

    context "distribution with artwork, details, songs and stores" do
      it "should return a level of 4" do
        album = create(:album, :with_artwork, :with_salepoints)
        allow(album).to receive(:has_tracks?).and_return(true)
        allow(album).to receive(:skip_songwriter_validations?).and_return(true)
        allow(album).to receive(:has_stores?).and_return(true)

        expect(helper.get_distribution_progress_level(album)).to eq(4)
      end
    end
  end

  describe "#reject_optional_upc" do
    it "rejects the same optional_upc_number" do
      release = double("Release", optional_upc_number: 3, tunecore_upc_number: 4)
      params = {
        optional_upc_number: 3,
        tunecore_upc_number: 4
      }

      expect(helper.reject_optional_upc_number?(release, params)).to eq true
    end

    it "does not reject the different optional_upc_number" do
      release = double("Release", optional_upc_number: 3, tunecore_upc_number: 4)
      params = {
        optional_upc_number: 5,
        tunecore_upc_number: 4
      }

      expect(helper.reject_optional_upc_number?(release, params)).to eq false
    end

    it "does not reject the missing optional_upc_number" do
      release = double("Release", optional_upc_number: 3, tunecore_upc_number: 4)
      params = {
        tunecore_upc_number: 4
      }

      expect(helper.reject_optional_upc_number?(release, params)).to eq false
    end
  end

  describe "#validate_credit_usage" do
    let(:person) { create(:person) }

    before do
      helper.extend(MockApplicationHelper)
      allow(helper).to receive(:current_user).and_return(person)
    end

    context "a credit usage without a purchase" do
      it "should destroy a credit_usage" do
        product = create(:product, :five_album_distribution_credits)
        product_item = create(:product_item, :product => product)
        distribution_credit_purchase = create(:purchase, :five_album_distribution_credits, :person_id => person.id, :related => product)
        inventory = create(:album_credit_inventory,
                           :person => person,
                           :purchase => distribution_credit_purchase,
                           :product_item => product_item,
                           :quantity => 5)
        album = create(:album, :with_uploaded_songs_and_artwork, :with_songwriter, :with_multiple_uploaded_songs)
        credit_usage = create(:credit_usage, :person => person, :related => album, :applies_to_type => album.class.name)
        credit_usage_purchase = create(:purchase, :credit_usage, :person_id => person.id, :related => credit_usage)

        credit_usage_purchase.delete
        helper.validate_credit_usage(person, album)
        expect(CreditUsage.last).to eq(nil)
      end
    end

    context "a credit usage with a purchase" do
      it "should not destroy a credit_usage" do
        product = create(:product, :five_album_distribution_credits)
        product_item = create(:product_item, :product => product)
        distribution_credit_purchase = create(:purchase, :five_album_distribution_credits, :person_id => person.id, :related => product)
        inventory = create(:album_credit_inventory,
                           :person => person,
                           :purchase => distribution_credit_purchase,
                           :product_item => product_item,
                           :quantity => 5)
        album = create(:album, :with_uploaded_songs_and_artwork, :with_songwriter, :with_multiple_uploaded_songs)
        credit_usage = create(:credit_usage, :person => person, :related => album, :applies_to_type => album.class.name)
        credit_usage_purchase = create(:purchase, :credit_usage, :person_id => person.id, :related => credit_usage)

        helper.validate_credit_usage(person, album)
        expect(CreditUsage.last).to eq(credit_usage)
      end
    end
  end

  describe '#display_primary_subgenre' do
    subject { helper.display_primary_subgenre(release) }

    let(:release) { create(:album, primary_genre: genre) }
    let(:genre) { create(:genre, name: 'Alternative') }

    context 'without release' do
      let(:release) { nil }

      it { is_expected.to eq(nil) }
    end

    context 'without subgenre' do
      it { is_expected.to eq(nil) }
    end

    context 'with subgenre' do
      let(:genre) { create(:genre, parent_id: 10, name: 'Blues') }

      it { is_expected.to eq('Blues') }
    end
  end

  describe '#display_secondary_subgenre' do
    subject { helper.display_secondary_subgenre(release) }

    let(:release) { create(:album, secondary_genre: genre) }
    let(:genre) { create(:genre, name: 'Alternative') }

    context 'without release' do
      let(:release) { nil }

      it { is_expected.to eq(nil) }
    end

    context 'without subgenre' do
      it { is_expected.to eq(nil) }
    end

    context 'with subgenre' do
      let(:genre) { create(:genre, parent_id: 10, name: 'Blues') }

      it { is_expected.to eq('Blues') }
    end
  end

  # Supporting tests in can_doable_spec.rb
  describe "#show_buy_plan?," do
    let!(:album) { create(:album) }

    before do
      helper.extend(MockApplicationHelper)
      allow(FeatureFlipper).to receive(:show_feature?).and_return true
      allow(helper).to receive(:current_user).and_return(album.person)
    end

    context "finalized album," do
      before { album.update(finalized_at: DateTime.now) }

      it "shows the CTA" do
        expect(helper.show_buy_plan?(album)).to eq true
      end
    end

    context "unfinalized album," do
      it "hides the CTA" do
        expect(helper.show_buy_plan?(album)).to eq false
      end
    end
  end

  describe "#show_add_stores?," do
    let!(:album) { create(:album) }

    before do
      allow(helper).to receive(:show_buy_plan?).and_return false
    end

    context "album taken down," do
      before { album.update(takedown_at: DateTime.now) }

      it "does NOT show add stores" do
        expect(helper.show_add_stores?(album)).to eq false
      end
    end

    context "album NOT taken down," do
      context "when show_buy_plan? is false" do
        it "shows add stores" do
          expect(helper.show_buy_plan?(album)).to eq false
        end
      end

      context "when show_buy_plan? is true" do
        it "does NOT show add stores" do
          allow(helper).to receive(:show_buy_plan?).and_return true

          expect(helper.show_buy_plan?(album)).to eq true
        end
      end
    end
  end
end
