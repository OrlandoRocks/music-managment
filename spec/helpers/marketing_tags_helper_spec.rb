require "rails_helper"

describe MarketingTagsHelper do
  describe "#data_layer_object" do
    let(:person) { FactoryBot.create(:person) }

    it "outputs script tag with dataLayer =" do
      helper.extend(MockApplicationHelper)
      page = Gtm::Page.new(person, page_type: Gtm::Dashboard, page_name: "test")
      allow(page).to receive(:user_data) { { page_name: "test" } }
      expected_output = "<script type=\"text/javascript\">\n//<![CDATA[\ndataLayer = [{\"page_name\":\"test\"}];\n//]]>\n</script>"
      expect(helper.data_layer_object(page: page)).to eq expected_output
    end
  end
end
