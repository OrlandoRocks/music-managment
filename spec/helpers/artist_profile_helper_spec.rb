require "rails_helper"

describe ArtistProfileHelper do
  describe "#display_song_name" do
    context "when the song does not have an artist name" do
      let(:song) { build(:song, name: "Song Name") }

      it "should return the song name" do
        expect(helper.display_song_name(song)).to eq "Song Name"
      end
    end

    context "when the song has an artist name" do
      let(:album)  { create(:album, :with_uploaded_song) }
      let(:song)   { album.songs.first }
      let(:artist) { double(name: "Artist Name") }
      let(:expected_result) { "This Is a Song<span class=\"various\"> by Artist Name</span>" }

      it "should return the song name and artist name" do
        allow(song).to receive_message_chain(:album, :is_various?) { true }
        allow(song).to receive(:artist)                    { artist }

        expect(helper.display_song_name(song)).to eq expected_result
      end
    end
  end
end
