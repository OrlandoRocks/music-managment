require "rails_helper"

describe CountryDomainHelper do
  before(:each) do
    @subdomain = ENV['DEPLOY_ENV']
  end
  class DummyCountryDomainController
    include CountryDomainHelper

    attr_reader :redirected, :redirected_path, :current_user, :request, :cookies
    attr_accessor :params, :session

    def initialize(user, request, cookies = {})
      @redirected   = false
      @current_user = user
      @request      = request
      @params       = {}
      @session      = {}
      @cookies      = cookies
    end

    def t(translation)
      'Lorem Ipsum'
    end

    def redirect_to(path, params = {})
      @redirected = true
      @redirected_path = path
    end
  end

  describe "#ensure_correct_country_domain" do
    context "with redirect_path" do
      it "redirects to correct country domain and preserves passed in path" do
        request    = double(host: "#{@subdomain}.tunecore.com", port: 80, protocol: "http://", method: "GET")
        user       = double(domain: "#{@subdomain}.tunecore.it", country_domain: "IT")
        controller = DummyCountryDomainController.new(user, request)
        controller.ensure_correct_country_domain("/path/to/page")
        expect(controller.redirected).to eq true
        expect(controller.redirected_path).to eq "http://#{@subdomain}.tunecore.it/path/to/page"
      end

      it "it redirects if domain is already correct but passed in redirect is different than request.fullpath" do
        request    = double(host: "#{@subdomain}.tunecore.it", port: 80, protocol: "http://", method: "GET", fullpath: "/some/other/path")
        user       = double(domain: "#{@subdomain}.tunecore.it", country_domain: "IT")
        controller = DummyCountryDomainController.new(user, request)
        controller.ensure_correct_country_domain("/path/to/page")
        expect(controller.redirected).to eq true
        expect(controller.redirected_path).to eq "/path/to/page"
      end

      it "it redirects if domain is already correct and passed in redirect is same as request.fullpath" do
        request    = double(host: "#{@subdomain}.tunecore.it", port: 80, protocol: "http://", method: "GET", fullpath: "/same/path")
        user       = double(domain: "#{@subdomain}.tunecore.it", country_domain: "IT")
        controller = DummyCountryDomainController.new(user, request)
        controller.ensure_correct_country_domain("/same/path")
        expect(controller.redirected).to eq true
        expect(controller.redirected_path).to eq "/same/path"
      end
    end

    context "with no redirect_path" do
      it "redirects to correct country and preserves request.fullpath" do
        request    = double(host: "#{@subdomain}.tunecore.com", port: 80, protocol: "http://", method: "GET", fullpath: "/same/path")
        user       = double(domain: "#{@subdomain}.tunecore.it", country_domain: "IT")
        controller = DummyCountryDomainController.new(user, request)
        controller.ensure_correct_country_domain
        expect(controller.redirected).to eq true
        expect(controller.redirected_path).to eq "http://#{@subdomain}.tunecore.it/same/path"
      end

      it "redirects to correct country and preserves request.fullpath and alt port" do
        request    = double(host: "#{@subdomain}.tunecore.com", port: 3000, protocol: "http://", method: "GET", fullpath: "/same/path")
        user       = double(domain: "#{@subdomain}.tunecore.it", country_domain: "IT")
        controller = DummyCountryDomainController.new(user, request)
        controller.ensure_correct_country_domain
        expect(controller.redirected).to eq true
        expect(controller.redirected_path).to eq "http://#{@subdomain}.tunecore.it:3000/same/path"
      end

      it "does not redirect if domain is correct" do
        request    = double(host: "#{@subdomain}.tunecore.it", port: 80, protocol: "http://", method: "GET", fullpath: "/diff/path")
        user       = double(domain: "#{@subdomain}.tunecore.it", country_domain: "IT")
        controller = DummyCountryDomainController.new(user, request)
        controller.ensure_correct_country_domain
        expect(controller.redirected).to eq false
      end

      it "doesn't change domain if on studio" do
        request    = double(host: "studio.tunecore.com", port: 80, protocol: "http://", method: "GET", fullpath: "/dashboard")
        user       = double(domain: "#{@subdomain}.tunecore.com", country_domain: "US")
        controller = DummyCountryDomainController.new(user, request)
        controller.ensure_correct_country_domain
        expect(controller.redirected).to eq false
      end

      it "doesn't change domain if on next" do
        request    = double(host: "next.tunecore.com", port: 80, protocol: "http://", method: "GET", fullpath: "/dashboard")
        user       = double(domain: "#{@subdomain}.tunecore.com", country_domain: "US")
        controller = DummyCountryDomainController.new(user, request)
        controller.ensure_correct_country_domain
        expect(controller.redirected).to eq false
      end
    end
  end

  describe "#compute_user_language" do
    context "with the tc-lang cookie present" do
      it "sets the locale to the tc-lang cookie" do
        request    = double(host: "next.tunecore.com", port: 80, protocol: "http://", method: "GET", fullpath: "/dashboard", path: "/dashboard")
        user       = double(domain: "#{@subdomain}.tunecore.com", country_domain: "US")
        controller = DummyCountryDomainController.new(user, request, {'tc-lang' => 'pt-us'})
        expect(controller.compute_user_language)
        expect(I18n.locale.to_s).to eq('pt-us')
      end
    end

    context "with the tc-autoselect cookie present" do
      it "sets the locale to the tc-lang cookie, saves the change to the database, and deletes the tc-autoselect cookie" do
        request    = double(host: "next.tunecore.com", port: 80, protocol: "http://", method: "GET", fullpath: "/dashboard", path: "/dashboard")
        user       = double(domain: "#{@subdomain}.tunecore.com", country_domain: "US")
        controller = DummyCountryDomainController.new(user, request, {'tc-lang' => 'pt-us', 'tc-autoselect' => 'true'})
        expect(user).to receive(:update).with({:country_website_language_id => 9})
        expect(controller.compute_user_language)
        expect(controller.cookies).to eq({"tc-lang" => "pt-us"})
        expect(I18n.locale.to_s).to eq('pt-us')
      end
    end

    context "with the tc-userselect cookie present" do
      it "sets the locale to the tc-lang cookie, saves the change to the database, and deletes the tc-userselect cookie" do
        request    = double(host: "next.tunecore.com", port: 80, protocol: "http://", method: "GET", fullpath: "/dashboard", path: "/dashboard")
        user       = double(domain: "#{@subdomain}.tunecore.com", country_domain: "US")
        controller = DummyCountryDomainController.new(user, request, {'tc-lang' => 'pt-us', 'tc-userselect' => 'true'})
        expect(user).to receive(:update).with({:country_website_language_id => 9})
        expect(controller.compute_user_language)
        expect(controller.cookies).to eq({"tc-lang" => "pt-us"})
        expect(I18n.locale.to_s).to eq('pt-us')
      end
    end
  end

  describe "#country_website" do
    context "as an admin" do
      it "is AU for au site" do
        request    = double(host: "#{@subdomain}.tunecore.com.au", port: 80, protocol: "https://", method: "GET", fullpath: "/dashboard")
        user       = double(is_administrator: true)
        controller = DummyCountryDomainController.new(user, request)
        expect(controller.country_website).to eq("AU")
      end

      it "is UK for United Kingdom" do
        request    = double(host: "#{@subdomain}.tunecore.co.uk", port: 80, protocol: "https://", method: "GET", fullpath: "/dashboard")
        user       = double(is_administrator: true)
        controller = DummyCountryDomainController.new(user, request)
        expect(controller.country_website).to eq("UK")
      end

      it "is US for 'Murica" do
        request    = double(host: "#{@subdomain}.tunecore.com", port: 80, protocol: "https://", method: "GET", fullpath: "/dashboard")
        user       = double(is_administrator: true)
        controller = DummyCountryDomainController.new(user, request)
        expect(controller.country_website).to eq("US")
      end

      it "is CA for the great white north" do
        request    = double(host: "#{@subdomain}.tunecore.ca", port: 80, protocol: "https://", method: "GET", fullpath: "/dashboard")
        user       = double(is_administrator: true)
        controller = DummyCountryDomainController.new(user, request)
        expect(controller.country_website).to eq("CA")
      end

      it "is DE for Germany" do
        ENV['DE_URL_SWITCH'] = 'true'
        request    = double(host: "#{@subdomain}.tunecore.de", port: 80, protocol: "https://", method: "GET", fullpath: "/dashboard")
        user       = double(is_administrator: true)
        controller = DummyCountryDomainController.new(user, request)
        expect(controller.country_website).to eq("DE")
      end

      it "is FR for France" do
        request    = double(host: "#{@subdomain}.tunecore.fr", port: 80, protocol: "https://", method: "GET", fullpath: "/dashboard")
        user       = double(is_administrator: true)
        controller = DummyCountryDomainController.new(user, request)
        expect(controller.country_website).to eq("FR")
      end

      it "is IT for Italy" do
        request    = double(host: "#{@subdomain}.tunecore.it", port: 80, protocol: "https://", method: "GET", fullpath: "/dashboard")
        user       = double(is_administrator: true)
        controller = DummyCountryDomainController.new(user, request)
        expect(controller.country_website).to eq("IT")
      end
    end

    context "as a customer" do
      it "is AU for au site" do
        request         = double(host: "#{@subdomain}.tunecore.com.au", port: 80, protocol: "https://", method: "GET", fullpath: "/dashboard")
        country_website = double(country: "AU")
        user            = double(is_administrator: false, country_website: country_website, country_domain: "AU")
        controller      = DummyCountryDomainController.new(user, request)
        expect(controller.country_website).to eq("AU")
      end

      it "is UK for United Kingdom" do
        request    = double(host: "#{@subdomain}.tunecore.co.uk", port: 80, protocol: "https://", method: "GET", fullpath: "/dashboard")
        country_website = double(country: "UK")
        user            = double(is_administrator: false, country_website: country_website, country_domain: "UK")
        controller = DummyCountryDomainController.new(user, request)
        expect(controller.country_website).to eq("UK")
      end

      it "is US for 'Murica" do
        request    = double(host: "#{@subdomain}.tunecore.com", port: 80, protocol: "https://", method: "GET", fullpath: "/dashboard")
        country_website = double(country: "US")
        user            = double(is_administrator: false, country_website: country_website, country_domain: "US")
        controller = DummyCountryDomainController.new(user, request)
        expect(controller.country_website).to eq("US")
      end

      it "is CA for the great white north" do
        request    = double(host: "#{@subdomain}.ca.tunecore.com", port: 80, protocol: "https://", method: "GET", fullpath: "/dashboard")
        country_website = double(country: "CA")
        user            = double(is_administrator: false, country_website: country_website, country_domain: "CA")
        controller = DummyCountryDomainController.new(user, request)
        expect(controller.country_website).to eq("CA")
      end

      it "is DE for Germany" do
        request    = double(host: "#{@subdomain}.tunecore.com.de", port: 80, protocol: "https://", method: "GET", fullpath: "/dashboard")
        country_website = double(country: "DE")
        user            = double(is_administrator: false, country_website: country_website, country_domain: "DE")
        controller = DummyCountryDomainController.new(user, request)
        expect(controller.country_website).to eq("DE")
      end

      it "is FR for France" do
        request    = double(host: "#{@subdomain}.tunecore.fr", port: 80, protocol: "https://", method: "GET", fullpath: "/dashboard")
        country_website = double(country: "FR")
        user            = double(is_administrator: false, country_website: country_website, country_domain: "FR")
        controller = DummyCountryDomainController.new(user, request)
        expect(controller.country_website).to eq("FR")
      end

      it "is IT for Italy" do
        request    = double(host: "#{@subdomain}.tunecore.it", port: 80, protocol: "https://", method: "GET", fullpath: "/dashboard")
        country_website = double(country: "IT")
        user            = double(is_administrator: false, country_website: country_website, country_domain: "IT")
        controller = DummyCountryDomainController.new(user, request)
        expect(controller.country_website).to eq("IT")
      end
    end
  end

  describe "#parent_genres_for_country_user" do
    it "should return the india genre for an india user, and no subgenres" do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

      request    = double(host: "#{@subdomain}.tunecore.in", port: 80, protocol: "https://", method: "GET", fullpath: "/dashboard")
      country_website = double(country: "IN")
      user            = double(is_administrator: false, country_website: country_website, country_domain: "IN", is_administrator?: false)
      controller = DummyCountryDomainController.new(user, request)

      requested_subgres = controller.parent_genres_for_country_user

      expect(requested_subgres.where(name: 'Indian').count).to eq(1)
      expect(requested_subgres.where.not(parent_id: [nil, 0]).count).to eq(0)
    end
  end

  describe "#fetch_language_selector_options" do
    it "should return JSON with available language selector options" do
      request    = double(host: "#{@subdomain}.tunecore.com", port: 80, protocol: "http://", method: "GET")
      user       = create(:person, country_website_id: 1)
      controller = DummyCountryDomainController.new(user, request)

      serialized = controller.fetch_language_selector_options

      expect(serialized).to include("{\"country_website_id\":1,\"yml_languages\":\"en-us\",\"selector_language\":\"English\",\"selector_country\":\"United States\",\"selector_order\":1,\"redirect_country_website_id\":null}")
    end
  end

  describe "#current_locale_language" do
    it "should return the locale's language for the current user" do
      I18n.locale = "en-us"
      request    = double(host: "#{@subdomain}.tunecore.com", port: 80, protocol: "http://", method: "GET")
      user       = double(domain: "#{@subdomain}.tunecore.com", country_domain: "US", country_website: 1)
      controller = DummyCountryDomainController.new(user, request)
      expect(controller.current_locale_language).to eq("English")
    end
  end
end
