require "rails_helper"

describe AutoRenewalHelper, type: :helper do
  let(:non_us_person_with_ar) { create(:person, :with_autorenewal_and_current_cc, :with_live_albums, country: "UK") }

  before(:all) do
    Timecop.freeze(Time.now)
  end

  before(:each) do
    # Allow any controller including this helper to receive `current_user`
    helper.extend(MockApplicationHelper)
    allow(helper).to receive(:current_user).and_return(non_us_person_with_ar)
  end

  after(:all) do
    Timecop.return
  end

  describe "#first_upcoming_renewal_expires_at" do 
    it "returns the upcoming renewal expiration" do
      non_us_person_with_ar.login_events << create(:login_event, person: non_us_person_with_ar, created_at: Time.now, country: "United Kingdom")
      expected = non_us_person_with_ar.renewals.last.expires_at
      expect(helper.first_upcoming_renewal_expires_at).to eq expected
    end
  end
  
  describe "#should_show_auto_renewal_prompt?" do
    context "with upcoming auto-renewals" do
      it "returns true when person is non-US, has an upcoming auto-renewal, and has not already completed the form" do
        non_us_person_with_ar.login_events << create(:login_event, person: non_us_person_with_ar, created_at: Time.now, country: "United Kingdom")
        expect(helper.should_show_auto_renewal_prompt?(skip_controller_check: true)).to be_truthy
      end

      it "returns false if the person is US" do
        us_person = create(:person, :with_autorenewal_and_current_cc, :with_live_albums, country: "US")
        us_person.login_events << create(:login_event, person: us_person, created_at: Time.now, country: "United States")
        expect(helper.should_show_auto_renewal_prompt?(person: us_person, skip_controller_check: true)).to be_falsy
      end

      it "returns false if the person doesn't have any upcoming renewals but is missing compliance info" do
        non_us_person_with_ar.login_events << create(:login_event, person: non_us_person_with_ar, created_at: Time.now, country: "United Kingdom")
        non_us_person_with_ar.renewals.each { |r| r.history.destroy_all }
        expect(helper.should_show_auto_renewal_prompt?(skip_controller_check: true)).to be_falsey
      end

      it "returns false if the person had not filled out the auto-renewal form but is compliant" do
        non_us_person_with_self_billing = create(
          :person,
          :with_autorenewal_and_current_cc,
          :with_live_albums,
          :with_self_billing_status,
          :with_address,
          customer_type: :business,
          country: "UK"
        )
        allow(non_us_person_with_self_billing).to receive(:has_upcoming_autorenewal?).and_return(true)
        non_us_person_with_self_billing.require_address = true

        expect(non_us_person_with_self_billing).to receive(:compliance_company_name).and_return("yo mama is so fat...")
        expect(non_us_person_with_self_billing).to receive(:validate_address_for_non_us?).and_return(true)

        non_us_person_with_self_billing.login_events << create(:login_event, person: non_us_person_with_self_billing, created_at: Time.now, country: "United Kingdom")
        
        expect(helper.should_show_auto_renewal_prompt?(person: non_us_person_with_self_billing, skip_controller_check: true)).to be_falsey
      end

      it "returns false if the person has enabled self billing and is vat_tax_complaint" do
        non_us_person_with_self_billing = create(
          :person,
          :with_autorenewal_and_current_cc,
          :with_live_albums,
          :with_self_billing_status,
          :with_address,
          customer_type: :business,
          country: "UK"
        )
        allow(non_us_person_with_self_billing).to receive(:has_upcoming_autorenewal?).and_return(true)
        non_us_person_with_self_billing.require_address = true

        expect(non_us_person_with_self_billing).to receive(:compliance_company_name).and_return("yo mama is so fat...")
        expect(non_us_person_with_self_billing).to receive(:validate_address_for_non_us?).and_return(true)

        non_us_person_with_self_billing.login_events << create(:login_event, person: non_us_person_with_self_billing, created_at: Time.now, country: "United Kingdom")

        expect(helper.should_show_auto_renewal_prompt?(person: non_us_person_with_self_billing, skip_controller_check: true)).to be_falsy
      end
    end
  end
end