require "rails_helper"

describe DashboardHelper, type: :helper do
  before(:each) do
    @person = FactoryBot.create(:person)
    @album = FactoryBot.create(:album, person: @person)
  end

  it "should display Your Releases when you have no finalized releases" do
    expect(helper.days_since_last_release(@person)).to match /Your Releases/
  end

  it "should display Your Releases when you have at least one release that is finalized" do
    @album.update(finalized_at: Time.now)
    expect(helper.days_since_last_release(@person)).to match /Your Releases/
  end

  describe "#renewal_due_for_album" do
    it "should return true if an album has a renewal due" do
      renewed_album = create(:album, :with_one_year_renewal)
      renewal = renewed_album.renewals.first
      renewal_history = renewed_album.renewal_histories.first

      renewal.update(takedown_at: nil, canceled_at: nil)
      renewal_history.expires_at = Time.current + 2.weeks

      cache = {}
      cache[renewed_album.id] = renewal_history.expires_at

      expect(helper.renewal_due_for_album(renewed_album, cache)).to be (true)
    end

    it "should return false if an album is not in the cache" do
      renewed_album = create(:album, :with_one_year_renewal)
      expect(helper.renewal_due_for_album(renewed_album, {})).to be false
    end
  end

  describe "#get_release_type_path" do
    it "should yield the edit path for albums" do
      album = create(:album)

      path = helper.get_release_type_path('Album', 'edit', album)

      expect(path).to eq("/albums/#{album.id}/edit")
    end

    it "should yield the new path for albums" do
      path = helper.get_release_type_path('Album', 'new')

      expect(path).to eq("/albums/new")
    end

    it "should yield the show path for albums" do
      album = create(:album)
      path = helper.get_release_type_path('Album', 'show', album)

      expect(path).to eq("/albums/#{album.id}")
    end
  end
end
