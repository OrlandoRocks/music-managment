require "rails_helper"

# Specs in this file have access to a helper object that includes
# the NotificationsHelper. For example:
#
# describe NotificationsHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       helper.concat_strings("this","that").should == "this that"
#     end
#   end
# end
describe NotificationsHelper, type: :helper do
  it "generates a class based on notification state" do
    notification = TCFactory.build(:notification)

    expect(helper.notification_class(notification)).to eq("unread")

    notification.first_clicked = Time.now
    expect(helper.notification_class(notification)).to eq("read")
  end
end
