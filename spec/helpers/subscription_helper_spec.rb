require "rails_helper"

describe SubscriptionsHelper, type: :helper do
  before :each do
    @current_user = mock_model(Person)
    allow(@current_user).to receive(:india_user?).and_return(false)
    @album = TCFactory.build_album(:person => @current_user, :name => 'My Album')
    helper.extend(MockApplicationHelper)
    allow(helper).to receive(:current_user).and_return(@current_user)
  end

  describe "#renewal_status" do
    it "should get a renewal message if the album is more than 28 days from expiration and auto renew is disabled" do
      allow(@current_user).to receive_message_chain(:person_preference, :do_not_autorenew?).and_return(true)
      renewal = mock_model(Renewal, :expires_at => Time.now + 28.days, :canceled_at? => false)
      allow(@album).to receive(:renewal).and_return(renewal)
      expect(helper.renewal_status(@album).include?("Your album is due for renewal in 28 days. Auto renewal is disabled, to make sure that your music stays live please")).to eql(true)
    end

    it "should get a message for an expired credit card" do
      allow(@current_user).to receive_message_chain(:person_preference, :do_not_autorenew?).and_return(false)
      allow(@current_user).to receive_message_chain(:stored_credit_cards, :active).and_return([mock_model(StoredCreditCard, :expired? => true)])
      renewal = mock_model(Renewal, :expires_at => Time.now + 28.days, :canceled_at? => false)
      allow(@album).to receive(:renewal).and_return(renewal)
      expect(helper.renewal_status(@album).include?("Your credit card is expired.")).to eql(true)
    end
  end

  describe "#display_renewal?" do

    it "should be true if the album is less than 28 days from expiration" do
      renewal = mock_model(Renewal, :canceled_at? => false, :expires_at => Time.now + 27.days)
      allow(@album).to receive(:renewal).and_return(renewal)
      expect(helper.display_renewal?(@album)).to eql(true)
    end

    it "should be false for a cancelled release" do
      renewal = mock_model(Renewal, :canceled_at => nil, :expires_at => Time.now + 29.days)
      allow(@album).to receive(:renewal).and_return(renewal)
      expect(helper.display_renewal?(@album)).to eql(false)
    end

    it "should be false if the album is more than 28 days from expiraton" do
      renewal = mock_model(Renewal, :expires_at => Time.now + 29.days)
      allow(@album).to receive(:renewal).and_return(renewal)
      expect(helper.display_renewal?(@album)).to eql(false)
    end

  end
end
