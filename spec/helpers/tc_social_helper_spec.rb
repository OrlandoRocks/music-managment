require "rails_helper"

describe TcSocialHelper, type: :helper do
  describe "#tc_social_allowed_for?" do
    let(:person) { create(:person) }

    it "should return value from instance variable if available" do
      helper.instance_variable_set(:@tc_social_allowed, false)
      expect(helper.tc_social_allowed_for?(person)).to be_falsey
    end

    it "should return value from tc_social_visible_for" do
      allow(helper).to receive(:tc_social_visible_for?).and_return(true)

      expect(helper.tc_social_allowed_for?(person)).to be_truthy
    end
  end

  describe '#tc_social_visible_for?' do
    let(:person) { create(:person) }

    subject { helper.tc_social_visible_for?(person) }

    context "user from allowed region" do
      before do
        region = ClientApplication::SOCIAL_ALLOWED_REGIONS.first
        allow(person).to receive(:country_domain).and_return(region)
      end

      context "user has tc social" do
        before { allow(person).to receive(:has_tc_social?).and_return(true) }

        it { should be_truthy }
      end

      context "user does not have tc social" do
        before { allow(person).to receive(:has_tc_social?).and_return(false) }

        it { should be_falsey }
      end
    end

    context "user outside allowed region" do
      before do
        region = CountryWebsite
          .where
          .not(country: ClientApplication::SOCIAL_ALLOWED_REGIONS)
          .first
          .country

        allow(person).to receive(:country_domain).and_return(region)
      end

      context "user has tc social" do
        before { allow(person).to receive(:has_tc_social?).and_return(true) }

        it { should be_falsey }
      end

      context "user does not have tc social" do
        before { allow(person).to receive(:has_tc_social?).and_return(false) }

        it { should be_falsey }
      end
    end
  end
end
