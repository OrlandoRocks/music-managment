require "rails_helper"

describe PublishingTerminatedComposerHelper do
  let(:publishing_composer) { create(:publishing_composer) }

  describe "#existing_partially_terminated_publishing_composer" do
    it "returns a partially terminated publishing_composer associated to the publishing_composer" do
      create(:terminated_composer, :partially, publishing_composer: publishing_composer)
      expect(helper.existing_partially_terminated_publishing_composer(publishing_composer.id).present?).to be true
    end

    it "returns nothing if there is no associated partially terminated publishing_composer" do
      expect(helper.existing_partially_terminated_publishing_composer(publishing_composer.id).present?).to be false
    end
  end

  describe "#flash_success_notice" do
    it "returns a success notice" do
      helper.flash_success_notice
      expect(flash[:notice]).to be_present
    end
  end

  describe "#publishing_composer_termination_params" do
    it "parses raw params" do
      raw_params = { terminated_composer: { publishing_composer_id: publishing_composer.id, effective_date: "", admin_note: "", publishing_termination_reason_id: 1}}

      expect(helper.publishing_composer_termination_params(raw_params))
        .to eq(publishing_composer_id: publishing_composer.id, effective_date: "", admin_note: "", publishing_termination_reason_id: 1)
    end
  end

end
