require "rails_helper"

describe ProductHelper do
  describe "#price_for_product_by_type_and_domain" do
    it "returns a money object that has a fractional value and currency" do
      person = create(:person)
      results = price_for_product_by_type_and_domain(person.country_domain, :songwriter_service)

      expect(results.fractional).to eq(7500)
      expect(results.currency.iso_code).to eq("USD")
    end
  end

  describe "#soundout_product_with_discount" do
    it "should invoke display render with original and discounted price" do
      person = create(:person)
      product = Product.find(Product::US_SOUNDOUT_PRODUCT_IDS.first)
      Product.set_targeted_product_and_price(person, product)

      expect(helper)
        .to receive(:money_with_discount)
        .with(product.original_price, product.adjusted_price, product.currency)

      helper.soundout_product_with_discount(person, :starter)
    end
  end
end
