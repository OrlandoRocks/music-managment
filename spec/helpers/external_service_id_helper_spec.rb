require "rails_helper"

describe ExternalServiceIdHelper do
  describe "#build_album_links" do
    let(:album) { create(:album) }

    it "returns only linkable services" do
      create(:external_service_id, linkable: album, service_name: 'itunes')
      create(:external_service_id, linkable: album, service_name: 'apple')
      create(:external_service_id, linkable: album, service_name: 'spotify')

      result = helper.build_album_links(album)

      expect(result.map{ |l| l[:title] }).to eq(["iTunes", "Spotify URL", "Spotify URI"])
      expect { helper.build_album_links(album) }.not_to raise_exception
    end
  end
end
