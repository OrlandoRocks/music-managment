require "rails_helper"

describe InvoicesHelper, type: :helper do

  let(:person) { create(:person) }

  before(:each) do
    helper.extend(MockApplicationHelper)
    allow(helper).to receive(:current_user).and_return(person)
  end

  describe "#album_store_list" do
    it "builds list of stores" do
      album = create(:album)
      s1 = Store.find(Store::GOOGLE_STORE_ID)
      s2 = Store.find(Store::TENCENT_MUCOIN_STORE_ID)
      sp1 = create(:salepoint, salepointable: album, store: s1)
      sp2 = create(:salepoint, salepointable: album, store: s2)

      expect(helper.album_store_list(album)).to eq "#{s1.name}, #{s2.name}"
    end

    it "returns 'no stores' message" do
      album = create(:album)

      expect(helper.album_store_list(album)).to eq "No Stores Selected"
    end

    it "excludes Facebook" do
      allow(FeatureFlipper).to receive(:show_feature?).and_return true

      album = create(:album)
      s1 = Store.find(Store::FB_REELS_STORE_ID)
      s2 = Store.find(Store::TENCENT_MUCOIN_STORE_ID)
      sp1 = create(:salepoint, salepointable: album, store: s1)
      sp2 = create(:salepoint, salepointable: album, store: s2)

      expect(helper.album_store_list(album)).to eq "#{s2.name}"
    end
  end

  describe "#free_album_store_list" do
    it "includes only facebook" do
      allow(FeatureFlipper).to receive(:show_feature?).and_return true

      album = create(:album)
      s1 = Store.find(Store::FB_REELS_STORE_ID)
      s2 = Store.find(Store::TENCENT_MUCOIN_STORE_ID)
      sp1 = create(:salepoint, salepointable: album, store: s1)
      sp2 = create(:salepoint, salepointable: album, store: s2)

      expect(helper.free_album_store_list(album)).to eq "#{s1.name}"
    end
  end

  describe "#fb_freemium_purchase?" do
    let!(:album) { create(:album, :purchaseable, person: person) }

    before do
      allow(FeatureFlipper).to receive(:show_feature?).and_return true
    end

    context "legacy purchase" do
      let!(:purchase) { create(:purchase, person: person, related: album) }
      let!(:purchases) { [purchase] }

      it "returns true if a purchase is in the Facebook / Instagram Reels store" do
        album.stores << Store.find(Store::FB_REELS_STORE_ID)

        expect(helper.fb_freemium_purchase?(purchases)).to be true
      end

      it "returns false if a purchase is not in the Facebook / Instagram Reels store" do
        expect(helper.fb_freemium_purchase?(purchases)).to be false
      end
    end

    context "free-with-plan purchase" do
      it "returns true if a purchase is in the Facebook / Instagram Reels store" do
        create(:person_plan, person: person)
        credit_usage = CreditUsage.for(person, album)
        purchase = create(:purchase, :free_with_plan, person: person, related_id: credit_usage.id)
        purchases = [purchase]
        album.stores << Store.find(Store::FB_REELS_STORE_ID)

        expect(helper.fb_freemium_purchase?(purchases)).to be true
      end

      it "returns false if a purchase is not in the Facebook / Instagram Reels store" do
        purchase = create(:purchase, person: person, related: album)
        purchases = [purchase]

        expect(helper.fb_freemium_purchase?(purchases)).to be false
      end
    end
  end
end
