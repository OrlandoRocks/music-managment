# encoding: utf-8
require "rails_helper"

USD = "USD".freeze

describe CurrencyHelper, type: :helper do
  let(:person) { create(:person, country: "US") }
  let(:country) { Country.find_by(iso_code: person.country_domain) }
  let(:country_website) { person.country_website }

  before(:each) do
    helper.extend(MockApplicationHelper)
  end

  describe "#format_to_currency" do
    it "should use money amount when full_amount is not in options hash" do
      money_object = BigDecimal(0.125, 3).to_money("USD")
      expect(helper.format_to_currency(money_object, precision:5)).to eq "$0.13000"
    end

    it "should use full_amount amount when full_amount is an option" do
      money_object = BigDecimal(0.125, 3).to_money("USD")
      expect(helper.format_to_currency(money_object, precision:5, full_amount: BigDecimal(0.125, 3))).to eq "$0.12500"
    end

    context "Germany" do
      before(:each) do
        I18n.locale = :de
      end

      after do
        I18n.locale = I18n.default_locale
      end

      it "has euro sign at the end with a space" do
        money_obj = 9.99.to_money("EUR")
        expect(helper.format_to_currency(money_obj)).to eq "9,99 €"
      end

      it "replaces .00 cents with .-" do
        money_obj = 75.00.to_money("EUR")
        expect(helper.format_to_currency(money_obj)).to eq "75,- €"
      end

      it "handles 0.00 price" do
        money_obj = 0.00.to_money("EUR")
        expect(helper.format_to_currency(money_obj)).to eq "0,- €"
      end

      it "handles price > 10000 with .00 cents" do
        money_obj = 10001.00.to_money("EUR")
        expect(helper.format_to_currency(money_obj)).to eq "10.001,- €"
      end

      it "handles price > 10000 with non .00 cents" do
        money_obj = 1921001.99.to_money("EUR")
        expect(helper.format_to_currency(money_obj)).to eq "1.921.001,99 €"
      end
    end

    context "France" do
      before(:each) do
        I18n.locale = :fr
      end

      after do
        I18n.locale = I18n.default_locale
      end

      it "has euro sign at the end without a space" do
        money_obj = 9.99.to_money("EUR")
        expect(helper.format_to_currency(money_obj)).to eq "9,99€"
      end

      it "doesn't replace .00 cents with .-" do
        money_obj = 75.00.to_money("EUR")
        expect(helper.format_to_currency(money_obj)).to eq "75,00€"
      end

      it "handles 0.00 price" do
        money_obj = 0.00.to_money("EUR")
        expect(helper.format_to_currency(money_obj)).to eq "0,00€"
      end

      it "handles price > 10000 with .00 cents" do
        money_obj = 10001.00.to_money("EUR")
        expect(helper.format_to_currency(money_obj)).to eq "10.001,00€"
      end

      it "handles price > 10000 with non .00 cents" do
        money_obj = 1921001.38.to_money("EUR")
        expect(helper.format_to_currency(money_obj)).to eq "1.921.001,38€"
      end
    end
  end

  describe "#money_to_currency" do
    before(:each) do
      allow(helper).to receive(:current_user).and_return(person)
    end

    let(:money) { BigDecimal(0.13, 3).to_money("USD") }

    context "when a foreign_exchange_pegged_rate exists for the current user" do
      let!(:foreign_exchange_pegged_rate) do  create(:foreign_exchange_pegged_rate, pegged_rate: 20,
                                                     country: country, country_website: country_website,
                                                     currency: 'USD')
      end

      it "returns that money object's amount multiplied with the pegged rate value" do
        expect(helper.money_to_currency(money)).to eq("$2.60")
      end
    end


    context "when a foreign_exchange_pegged_rate does not exist for the current user" do
      it "returns that money object's original amount" do
        expect(helper.money_to_currency(money)).to eq("$0.13")
      end
    end

    context "India products" do
      before do
        create(:foreign_exchange_pegged_rate, pegged_rate: 10)
        person.update(country_website_id: CountryWebsite::INDIA)
        I18n.locale = :"en-in"
      end

      after do
        I18n.locale = I18n.default_locale
      end

      it "removes decimals" do
        money_obj = 75.00.to_money("USD")
        expect(helper.money_to_currency(money_obj)).to eq "₹750"
      end

      it "rounds the price" do
        money_obj = 75.84.to_money("USD")
        expect(helper.money_to_currency(money_obj)).to eq "₹758"
      end

      it "should not round when round_value is passed as false" do
        money_obj = 75.84.to_money("USD")
        expect(helper.money_to_currency(money_obj, round_value: false)).to eq "₹758.40"
      end

      it "rounds when round_value is passed as true (which is the default anyway)" do
        money_obj = 75.84.to_money("USD")
        expect(helper.money_to_currency(money_obj, round_value: true)).to eq "₹758"
      end

      it "rounds when an US admin takes over an Indian account" do
        I18n.locale = :"en-us" # since admin's locale remains unchanged when taking over an account
        person.update(country_website_id: CountryWebsite::INDIA) # updates current_user with the account being taken over

        money_obj = 75.84.to_money("USD")
        expect(helper.money_to_currency(money_obj, round_value: true)).to eq "₹758"
      end

      it "should not use pegged rate for INR money" do
        money_obj = 75.84.to_money("INR")
        expect(helper.money_to_currency(money_obj)).to eq "₹76"
      end

      context "with full_amount option" do
        it "returns the INR equivalent of the amount" do
          amount = BigDecimal(75.84, 3)
          expect(helper.money_to_currency(amount.to_money("USD"), precision: 14, full_amount: amount)).to eq "₹758"
        end

        it "returns the INR equivalent when an US admin takes over an Indian account" do
          I18n.locale = :"en-us" # since admin's locale remains unchanged when taking over an account
          person.update(country_website_id: CountryWebsite::INDIA) # updates current_user with the account being taken over

          amount = BigDecimal(75.84, 3)
          expect(helper.money_to_currency(amount.to_money("USD"), precision: 14, full_amount: amount)).to eq "₹758"
        end
      end
    end
  end

  describe "#balance_to_currency" do

    let(:money) { BigDecimal(0.13, 3).to_money("USD") }

    context "when a foreign_exchange_rate exists for the current user" do
      before(:each) do
        pegged_rate = 4
        unless ForeignExchangePeggedRate.find_by(pegged_rate: pegged_rate)
          create(:foreign_exchange_pegged_rate, pegged_rate: pegged_rate)
        end
      end

      it "returns that money object's amount multiplied with the foreign exchange rate value" do
        india_user = create(:person, country_website_id: 8, country: "IN")
        allow(helper).to receive(:available_in_region?).and_return(true)
        allow(helper).to receive(:current_user).and_return(india_user)
        create(:foreign_exchange_rate, exchange_rate: 20, source_currency: "USD", target_currency: "INR")
        expect(helper.balance_to_currency(money, iso_code: india_user.country_domain)).to eq("₹2.60")
      end

       it "but available_in_region? for foreign_exchange_rates_balance_display is turned off, return without conversion" do
        india_user = create(:person, country_website_id: 8, country: "IN")
        allow(helper).to receive(:available_in_region?).and_return(false)
        allow(helper).to receive(:current_user).and_return(india_user)
        create(:foreign_exchange_rate, exchange_rate: 20, source_currency: "USD", target_currency: "INR")
        expect(helper.balance_to_currency(money, iso_code: india_user.country_domain, currency: 'USD')).to eq("$0.13")
       end
    end

    context "when a foreign_exchange_rate does not exist for the current user" do
      it "returns that money object's original amount" do
        allow(helper).to receive(:current_user).and_return(person)
        ForeignExchangeRate.delete_all
        expect(helper.balance_to_currency(money, iso_code: person.country_domain)).to eq("$0.13")
      end
    end
  end

  describe "#locale_currency" do
    it "returns user currency if logged in" do
      allow(helper).to receive(:current_user).and_return(person)
      expect(helper.locale_currency).to eq "USD"
    end

    it "return country website currency of not logged in" do
      allow(helper).to receive(:current_user).and_return(nil)
      expect(helper.locale_currency).to eq "USD"
    end
  end

  describe "#get_current_user_currency" do
    context "current_user present" do
      before(:each) do
        allow(helper).to receive(:current_user).and_return(person)
      end
      context "given options currency" do
        it "returns options currency" do
          options = {currency: "INR"}
          expect(helper.get_current_user_currency(options)).to eq("INR")
        end
      end
      context "no options currency" do
        it "returns current_user.user_currency" do
          options = {}
          expect(helper.get_current_user_currency(options)).to eq("USD")
        end
      end
    end
    context "current_user not present" do
      context "given options currency" do
        it "returns options currency" do
          options = {currency: "INR"}
          expect(helper.get_current_user_currency(options)).to eq("INR")
        end
      end
      context "no options currency" do
        it "returns nil" do
          options = {}
          expect(helper.get_current_user_currency(options)).to be(nil)
        end
      end
    end
  end
end
