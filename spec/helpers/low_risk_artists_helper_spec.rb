require "rails_helper"

describe LowRiskArtistsHelper do
  describe ".artist_names_for_release" do
    let(:album) { create(:album, :paid, :with_songs, legal_review_state: "NEEDS REVIEW") }

    before(:each) do
      create(:creative, creativeable: album)
      create(:low_risk_account, person: album.person)
    end

    context "when there are no song artist names" do
      xit "returns only the album's artists" do
        album_artist_names = album.artists.map(&:name)
        low_risk_album = LowRiskArtistsQueryBuilder.build.first
        expect(helper.artist_names_for_release(low_risk_album)).to eq album_artist_names.join(", ")
      end
    end

    context "when there are song artist names" do
      xit "returns the song artist names as well as the song artist names" do
        song = create(:song, name: "Romeo & Juliet")
        album.songs << song
        create(:creative, creativeable: song)
        album_artist_names = album.artists.map(&:name)
        album_artist_names |= song.reload.creatives.map(&:artist).map(&:name)

        low_risk_album = LowRiskArtistsQueryBuilder.build.first
        expect(helper.artist_names_for_release(low_risk_album))
          .to eq album_artist_names.join(", ")
      end
    end
  end
end
