require "rails_helper"

describe KnowledgebaseLinkHelper do
  let(:base) { "//support.tunecore.com"}
  let(:default) { "//support.tunecore.com/hc/en-us"}
  let(:person)  { create(:person) }

  let(:country_language) { {
    "US" => "/hc/en-us",
    "CA" => "/hc/en-ca",
    "UK" => "/hc/en-gb",
    "AU" => "/hc/en-au",
    "DE" => "/hc/de",
    "FR" => "/hc/fr-fr",
    "IT" => "/hc/it"}
  }

  before(:each) do
    @knowledgebase_link1 = create(:knowledgebase_link)
    @knowledgebase_link2 = create(:knowledgebase_link)
    helper.extend(MockApplicationHelper)
  end

  describe "#knowledgebase_link_for" do
    it "should return links to country and language-specific pages" do
      expect(helper.knowledgebase_link_for('help-homepage', "UK")).to eq(base + country_language["UK"])
      expect(helper.knowledgebase_link_for('help-homepage', "DE")).to_not eq(base + country_language["IT"])
    end

    it "returns the US version of articles when country is not specified" do
      expect(helper.knowledgebase_link_for(@knowledgebase_link2.article_slug)).to eq(default + "/articles/#{@knowledgebase_link2.article_id}")
    end

    it "returns the correct country-language version of the link when the user specifies a country" do
      expect(helper.knowledgebase_link_for(@knowledgebase_link1.article_slug, "IT")).to eq(base + country_language['IT'] + "/articles/#{@knowledgebase_link1.article_id}")
    end

    it "should return the US support link when the article is not found" do
      expect(helper.knowledgebase_link_for('test-article3')).to eq(default)
    end
  end

  describe "#help_homepage_for_country" do
    it "should return the US help homepage" do
      expect(helper.help_homepage_for_country).to eq("//support.tunecore.com/hc/en-us")
    end

    it "should return the France help homepage" do
      helper.country_website=('FR')

      expect(helper.help_homepage_for_country).to eq("//support.tunecore.com/hc/fr-fr")
    end
  end

  describe "#store_info_knowledgebase_link" do
    let(:person) { create(:person) }

    before :each do
      helper.extend(MockApplicationHelper)
      allow(helper).to receive(:current_user).and_return(person)
    end

    context "when the album type is a Ringtone" do
      it "returns the ringtone description link" do
        ringtone_link = double(:link, article_id: '12345', route_segment: nil)

        allow(KnowledgebaseLink)
          .to receive(:find_by)
          .with(article_slug: anything)
          .and_return(ringtone_link)

        result = helper.store_info_knowledgebase_link(anything, true)

        expect(result).to eq base + country_language['US'] + "/articles/#{ringtone_link.article_id}"
      end
    end

    context "when the album type is a not Ringtone" do
      context "when the country language is 'US'" do
        it "returns 'US' store info description link" do
          spotify_link = double(:link, article_id: '12345', route_segment: nil)
          store_group = double(:store_group, abbreviation: "sp")

          allow(KnowledgebaseLink)
            .to receive(:find_by)
            .with(article_slug: anything)
            .and_return(spotify_link)

          result = helper.store_info_knowledgebase_link(store_group)

          expect(result).to eq base + country_language['US'] + "/articles/#{spotify_link.article_id}"
        end
      end

      context "when the country language is 'IT'" do
        it "returns 'IT' store info description link" do
          spotify_link = double(:link, article_id: '12345', route_segment: nil)
          store_group = double(:store_group, abbreviation: "sp")

          allow(person).to receive(:country_domain).and_return("IT")
          allow(KnowledgebaseLink)
            .to receive(:find_by)
            .with(article_slug: anything)
            .and_return(spotify_link)

          result = helper.store_info_knowledgebase_link(store_group)

          expect(result).to eq base + country_language['IT'] + "/articles/#{spotify_link.article_id}"
        end
      end
    end
  end
end
