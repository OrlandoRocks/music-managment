require "rails_helper"


describe DashboardReskinHelper, type: :helper do

  describe "#release_status" do
    let!(:album) { create(:album, :finalized) }

    context "takedown" do
      before { album.update(takedown_at: Time.now) }

      it "returns :takedown" do
        expect(helper.release_status(album)[:value]).to eq :takedown
      end
    end

    context "approved" do
      before { album.update(legal_review_state: "APPROVED") }

      it "returns :sent" do
        expect(helper.release_status(album)[:value]).to eq :sent
      end
    end

    context "processing" do
      before { album.update(legal_review_state: "NEEDS REVIEW") }

      it "returns :processing" do
        expect(helper.release_status(album)[:value]).to eq :processing
      end
    end

    context "needs_action" do
      it "returns :needs_action" do
        expect(helper.release_status(album)[:value]).to eq :needs_action
      end
    end

    context "incomplete" do
      before { album.update(finalized_at: nil) }

      it "returns :incomplete" do
        expect(helper.release_status(album)[:value]).to eq :incomplete
      end
    end
  end

  describe "#show_tfa_modal" do
    subject do
      Class.new do
        extend DashboardReskinHelper
        extend AccountSystem
        extend TwoFactorAuthable
      end
    end

    let!(:person) { create(:person) }
    let!(:tfa_prompt) { create(:two_factor_auth_prompt, person: person) }

    before do
      allow(subject).to receive(:current_user).and_return(person)

      allow_any_instance_of(AccountSystem)
        .to receive(:under_admin_control?).and_return(false)
      allow_any_instance_of(TwoFactorAuthable)
        .to receive(:two_factor_feature_enabled?).and_return(true)
      allow(current_user).to receive(:is_verified?).and_return(true)
      allow(tfa_prompt).to receive(:active?).and_return(true)
      allow(tfa_prompt).to receive(:never_prompted?).and_return(false)
    end

    it "returns false if under admin control" do
      allow_any_instance_of(AccountSystem)
        .to receive(:under_admin_control?).and_return(true)

      expect(subject.show_tfa_modal?).to eq false
    end

    it "returns false if two factor NOT enabled" do
      allow_any_instance_of(TwoFactorAuthable)
        .to receive(:two_factor_feature_enabled?).and_return(false)

      expect(subject.show_tfa_modal?).to eq false
    end

    it "returns false unless user is verified" do
      allow(person).to receive(:is_verified?).and_return(false)

      expect(subject.show_tfa_modal?).to eq false
    end

    it "returns false if prompt NOT active" do
      allow(tfa_prompt).to receive(:active?).and_return(false)

      expect(subject.show_tfa_modal?).to eq false
    end

    it "returns true if never prompted" do
      allow(tfa_prompt).to receive(:never_prompted?).and_return(true)

      expect(subject.show_tfa_modal?).to eq true
    end

    it "returns true if prompt_at < now" do
      allow(tfa_prompt).to receive(:prompt_at).and_return(Time.zone.now - 1.hour)

      expect(subject.show_tfa_modal?).to eq true
    end
  end
end
