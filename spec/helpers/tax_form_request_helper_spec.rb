require 'rails_helper'
include ActiveSupport::Testing::TimeHelpers

describe TaxFormRequestHelper do
  before do
    # TODO: Remove the next line when tax blocking is removed
    allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking, any_args).and_return(true)

    allow(FeatureFlipper).to receive(:show_feature?).with(:auto_approved_withdrawals, any_args).and_return(true)
    allow(FeatureFlipper).to receive(:show_feature?).with(:progressive_withholding_exemption, any_args).and_return(true)

    # Disable automatic creation of tax form mappings on tax_forms
    allow(TaxReports::TaxFormMappingService).to receive(:call!)

    travel_to(Time.current.beginning_of_year.end_of_month)
    helper.extend(MockApplicationHelper)

    person.tax_forms.destroy_all
    person.payout_transfers.destroy_all

    allow(helper).to receive(:current_user).and_return(person)
  end

  after do
    travel_back
  end

  let(:person) { create(:person, :with_login_events) }
  let(:approved_payout_provider) { create(:payout_provider, :approved, person: person) }
  let(:us_payout_provider_config) { PayoutProviderConfig.find_by_program_id(PayoutProviderConfig::TC_US_PROGRAM_ID) }
  let(:tax_form_with_default_revenue_streams) do
    create(
      :tax_form,
      :with_revenue_streams,
      submitted_at: Time.current.beginning_of_year,
      payout_provider_config: us_payout_provider_config,
      person: person
    )
  end
  let(:tax_form_with_user_mapped_revenue_streams) do
    create(
      :tax_form,
      :with_revenue_streams,
      user_mapped_at: Time.current,
      submitted_at: Time.current.beginning_of_year,
      payout_provider_config: us_payout_provider_config,
      person: person
    )
  end
  let(:prior_payout_transfer) do
    create(:approved_payout_transfer, person: person, created_at: 1.year.ago)
  end
  let(:current_payout_transfer) do
    create(:approved_payout_transfer, person: person, created_at: Time.current)
  end

  describe "#should_show_secondary_tax_form_request_prompt?" do
    subject { helper.should_show_secondary_tax_form_request_prompt? }

    context 'with "tax_form_mapping" feature flag set' do
      before do
        allow(FeatureFlipper).to receive(:show_feature?).with(:tax_form_mapping, any_args).and_return(true)
      end

      context 'with "secondary_taxform_submission" feature flag set' do
        before do
          allow(FeatureFlipper).to receive(:show_feature?).with(:advanced_tax_blocking, any_args).and_return(false)
          allow(FeatureFlipper).to receive(:show_feature?).with(:secondary_taxform_submission, any_args).and_return(true)
        end

        context "with approved payout provider" do
          let!(:payout_provider) { approved_payout_provider }

          context "with a valid tax form record that has default revenue streams" do
            let!(:tax_form) { tax_form_with_default_revenue_streams }

            context "with withdrawals (transfers) that don't meet the taxable threshold in both revenue streams" do
              before { create(:person_transaction_for_person_intake, person: person, credit: "50.0") }

              it { is_expected.to be_falsey}
            end

            context "with withdrawals (transfers) that meet the taxable threshold in both revenue streams" do
              before do
                create(:person_transaction_for_person_intake, person: person)
                create(:person_transaction, :taxable_balance_adjustment, person: person)
              end

              context "with withdrawals (transfers) made only before the last tax form was submitted" do
                let!(:payout_transfer) { prior_payout_transfer }

                it { is_expected.to be_truthy }
              end

              context "with withdrawals (transfers) made after the last tax form was submitted" do
                let!(:payout_transfer) { current_payout_transfer }

                it { is_expected.to be_falsey }
              end

              context "without withdrawals (transfers)" do
                it { is_expected.to be_truthy }
              end
            end
          end

          context "with a valid tax form record that has user mapped revenue streams" do
            let!(:tax_form) { tax_form_with_user_mapped_revenue_streams }

            it { is_expected.to be_falsey }
          end

          context "without any valid tax forms" do
            context "with withdrawals (transfers) made before the last tax form was submitted" do
              let!(:payout_transfer) { prior_payout_transfer }

              it { is_expected.to be_falsey }
            end
          end
        end

        context "without approved payout provider" do
          let!(:tax_form) { tax_form_with_user_mapped_revenue_streams }

          it { is_expected.to be_falsey }
        end
      end

      context 'without "secondary_taxform_submission" feature flag set' do
        before do
          allow(FeatureFlipper).to receive(:show_feature?).with(:secondary_taxform_submission, any_args).and_return(false)
        end

        let!(:payout_provider) { approved_payout_provider }
        let!(:tax_form) { tax_form_with_default_revenue_streams }

        it { is_expected.to be_falsey }
      end
    end

    context 'without "tax_form_mapping" feature flag set' do
      before do
        allow(FeatureFlipper).to receive(:show_feature?).with(:tax_form_mapping, any_args).and_return(false)
        allow(FeatureFlipper).to receive(:show_feature?).with(:secondary_taxform_submission, any_args).and_return(true)
      end

      let!(:payout_provider) { approved_payout_provider }
      let!(:tax_form) { tax_form_with_default_revenue_streams }

      it { is_expected.to be_falsey }
    end
  end
end
