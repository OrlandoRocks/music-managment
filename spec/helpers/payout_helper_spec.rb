require "rails_helper"

describe PayoutHelper, type: :helper do
  let(:person) { create(:person, :with_payout_provider, country_website_id: 1, country: "US") }
  let(:canadian_person) { create(:person, country_website_id: 2, country: "CA") }
  let(:person_without_payoneer) { create(:person) }


  before :each do
    helper.extend(MockApplicationHelper)
  end

  describe "#withdraw_money_link" do
    context "user is canadian" do
      it "renders the paypal view" do
        allow(helper).to receive(:current_user).and_return(canadian_person)
        allow(helper).to receive(:country_website).and_return(canadian_person.country_website.country)

        expect(helper.withdraw_money_link).to eq(link_to(custom_t(:withdraw_money), withdraw_path))
      end
    end

    context "user is not canadian" do
      it "renders the withdraw view" do
        allow(helper).to receive(:current_user).and_return(person)
        allow(helper).to receive(:country_website).and_return(person.country_website.country)

        expect(helper.withdraw_money_link).to eq(link_to(custom_t(:withdraw_money), withdraw_path))
      end
    end
  end

  describe "#needs_payoneer_enrollment?" do
    context "the user has no payout provider associated to their account" do
      it "returns false" do
        allow(helper).to receive(:payoneer_payout_enabled?).and_return(true)
        allow(helper).to receive(:current_user).and_return(person_without_payoneer)

        expect(helper.needs_payoneer_enrollment?).to eq(true)
      end
    end
  end

  describe "payout provider state methods" do
    before :each do
      allow(helper).to receive(:current_user).and_return(person)
    end

    describe "#payout_provider_approved?" do
      context "when payout provider is in a approved state" do
        it "returns true" do
          expect(helper.payout_provider_approved?).to eq(true)
        end
      end
    end

    describe "#payout_provider_declined?" do
      context "when the payout provider is in a declined state" do
        it "returns true" do
          person.payout_provider.update_attribute(:provider_status, "declined")
          expect(helper.payout_provider_declined?).to eq(true)
        end
      end
    end

    describe "#payout_provider_pending?" do
      context "when payout provider is in a pending state" do
        it "returns true" do
          person.payout_provider.update_attribute(:provider_status, "pending")
          expect(helper.payout_provider_pending?).to eq(true)
        end
      end
    end
  end

  describe "#payoneer_enabled_and_approved?" do
    before :each do
      allow(helper).to receive(:current_user).and_return(person)
    end

    context "when the Feature Flipper is enabled" do
      context "when the payout provider is in approved state" do
        it "returns true " do
          allow(helper).to receive(:payoneer_payout_enabled?).and_return(true)
          expect(helper.payoneer_enabled_and_approved?).to eq(true)
        end
      end
    end

    context "when the Feature Flipper is disabled" do
      context "payout provider is in approved state" do
        it "returns false" do
          allow(helper).to receive(:payoneer_payout_enabled?).and_return(false)
          expect(helper.payoneer_enabled_and_approved?).to eq(false)
        end
      end
    end

    context "when the Payout Provider is not in the approved state" do
      it "returns false" do
        person.payout_provider.update_attribute(:provider_status, "onboarding")
        allow(helper).to receive(:payoneer_payout_enabled?).and_return(true)
        expect(helper.payoneer_enabled_and_approved?).to eq(false)
      end
    end
  end

  describe "#payout_application_processing" do
    context "When Feature Flag is enabled" do
      before(:each) do
        allow(helper).to receive(:current_user).and_return(person)
        allow(helper).to receive(:payoneer_payout_enabled?).and_return(true)
      end

      context "Payoneer application has been submitted" do

        context "Payoneer visited and app submitted, pending approval" do
          it "returns true" do
            person.payout_provider.update_attribute(:provider_status, "pending")
            person.payout_provider.reload

            expect(helper.payout_application_processing?).to eq(true)
          end
        end

        context "Payout Provider is in the approved state" do
          it "returns false" do
            person.payout_provider.update_attribute(:provider_status, "approved")
            person.payout_provider.reload

            expect(helper.payout_application_processing?).to eq(false)
          end
        end

      end

      context "Payoneer application has not been submitted" do

        context 'Payoneer visited, but not app not submitted' do
          it "returns false" do
            person.payout_provider.update_attribute(:provider_status, "onboarding")
            person.payout_provider.reload

            expect(helper.payout_application_processing?).to eq(false)
          end
        end

        context "Payout Provider is not present" do
          it "returns false" do
            allow(helper).to receive(:current_user).and_return(person_without_payoneer)

            expect(helper.payout_application_processing?).to eq(false)
          end
        end

      end

    end

    context "When Feature Flag is not enabled" do

      context "Payout Provider present that is not in the approved state" do
        it "returns false" do
          person.payout_provider.update_attribute(:provider_status, "onboarding")
          allow(helper).to receive(:current_user).and_return(person)
          allow(helper).to receive(:payoneer_payout_enabled?).and_return(false)

          expect(helper.payout_application_processing?).to eq(false)
        end
      end

    end

  end

  describe "#show_legacy_options?" do
    context "for USA Customers" do
      before(:each) do
        allow(Payoneer::FeatureService).to receive(:payoneer_redesigned_onboarding?).and_return(true)
      end
      it "should return true if payoneer is enabled and optional" do
        new_person = create(:person)
        allow(helper).to receive(:current_user).and_return(new_person)
        allow(helper).to receive(:payoneer_mandatory?).and_return(false)
        expect(helper.show_legacy_options?).to eq(true)
      end

      it "should return false if payoneer is enabled and mandatory" do
        allow(helper).to receive(:current_user).and_return(person)
        allow(helper).to receive(:payoneer_mandatory?).and_return(true)

        expect(helper.show_legacy_options?).to eq(false)
      end

      it "should return false if the person's payoneer application is in onboarding state and payoneer is mandatory" do
        allow(helper).to receive(:current_user).and_return(person)
        allow(helper).to receive(:payoneer_mandatory?).and_return(true)
        person.payout_provider.update_attribute(:provider_status, PayoutProvider::ONBOARDING)
        expect(helper.show_legacy_options?).to eq(false)
      end

      it "should return true if the person's payoneer application is in onboarding state and payoneer is optional" do
        allow(helper).to receive(:current_user).and_return(person)
        allow(helper).to receive(:payoneer_mandatory?).and_return(false)
        person.payout_provider.update_attribute(:provider_status, PayoutProvider::ONBOARDING)
        expect(helper.show_legacy_options?).to eq(true)
      end

      it "should return false if the person's payoneer application is in pending state" do
        allow(helper).to receive(:current_user).and_return(person)
        allow(helper).to receive(:payoneer_mandatory?).and_return(true)
        person.payout_provider.update_attribute(:provider_status, PayoutProvider::PENDING)
        expect(helper.show_legacy_options?).to eq(false)
      end

      it "should return false if the person's payoneer application is rejected" do
        allow(helper).to receive(:current_user).and_return(person)
        person.payout_provider.update_attribute(:provider_status, PayoutProvider::DECLINED)
        expect(helper.show_legacy_options?).to eq(false)
      end

      it "should return false if the person's payoneer application is approved" do
        allow(helper).to receive(:current_user).and_return(person)
        person.payout_provider.update_attribute(:provider_status, PayoutProvider::APPROVED)
        expect(helper.show_legacy_options?).to eq(false)
      end
    end

    context "Rest of the world customers" do
      it "should always return true" do
        allow(Payoneer::FeatureService).to receive(:payoneer_redesigned_onboarding?).and_return(false)
        allow(helper).to receive(:current_user).and_return(canadian_person)
        expect(helper.show_legacy_options?).to eq(true)
      end
    end
  end

  describe "#payout_provider_approved_and_enabled?" do
    context "approved user is from USA" do
      it "should return true" do
        allow(helper).to receive(:payout_provider_approved?).and_return(true)
        allow(helper).to receive(:current_user).and_return(person)
        allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_payout, person).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_opt_out, person).and_return(false)

        expect(helper.payout_provider_approved_and_enabled?).to eq(true)
      end
    end

    context "user is NOT from USA" do
      it "should return false" do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
        allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_opt_out, person).and_return(false)
        allow(helper).to receive(:payout_provider_approved?).and_return(true)
        canadian_person = create(:person, :with_balance, amount: 50_000, country_website_id: 2, country: "CA")
        allow(helper).to receive(:current_user).and_return(canadian_person)
        expect(helper.payout_provider_approved_and_enabled?).to be_falsey
      end
    end

    context "payout provider is not approved regardless of country of person" do
      it "should return false" do
        person = create(:person, :with_balance, amount: 50_000, country_website_id: 2, country: "CA")
        payout_provider = create(:payout_provider, provider_status: "onboarding", person: person)
        allow(helper).to receive(:current_user).and_return(person)
        allow(Payoneer::FeatureService).to receive(:payoneer_payout_enabled?).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_opt_out, person).and_return(false)
        allow(helper).to receive(:payout_provider_approved?).and_return(false)
        expect(helper.payout_provider_approved_and_enabled?).to eq(false)
      end
    end

    context "payoneer is not enabled" do
      it "returns false" do
        allow(helper).to receive(:current_user).and_return(person)
        allow(helper).to receive(:payout_provider_approved?).and_return(true)
        allow(Payoneer::FeatureService).to receive(:payoneer_payout_enabled?).and_return(false)
        expect(helper.payout_provider_approved_and_enabled?).to eq(false)

      end
    end

  end

  describe "#payoneer_payout_enabled?" do
    it "should delegate the call to payout config" do
      allow(Payoneer::FeatureService).to receive(:payoneer_payout_enabled?).and_return(true)
      expect(helper.payoneer_payout_enabled?).to eq(true)

      allow(Payoneer::FeatureService).to receive(:payoneer_payout_enabled?).and_return(false)
      expect(helper.payoneer_payout_enabled?).to eq(false)
    end
  end

  describe "#payoneer_mandatory" do
    context "payoneer is enabled" do
      context "user is not opted out" do
        it "should return true" do
          allow(Payoneer::FeatureService).to receive(:payoneer_payout_enabled?).and_return(true)
          allow(Payoneer::FeatureService).to receive(:payoneer_opted_out?).and_return(false)
          expect(helper.payoneer_mandatory?).to eq(true)
        end
      end

      context "user is opted out" do
        it "should return false" do
          allow(Payoneer::FeatureService).to receive(:payoneer_payout_enabled?).and_return(true)
          allow(Payoneer::FeatureService).to receive(:payoneer_opted_out?).and_return(true)
          expect(helper.payoneer_mandatory?).to eq(false)
        end
      end

      context "user is not yet mandated" do
        context "user needs tax form" do
          it "should return true" do
            allow(helper).to receive(:current_user).and_return(create(:person))
            allow(Payoneer::FeatureService).to receive(:payoneer_payout_enabled?).and_return(true)
            allow(Payoneer::FeatureService).to receive(:payoneer_opted_out?).and_return(false)
            allow_any_instance_of(TaxFormCheckService).to receive(:needs_tax_form?).and_return(true)
            expect(helper.payoneer_mandatory?).to eq(true)
          end
        end
      end
    end

    context "payoneer is not enabled" do
      it "should return false" do
        allow(Payoneer::FeatureService).to receive(:payoneer_payout_enabled?).and_return(false)
        expect(helper.payoneer_mandatory?).to eq(false)
      end
    end
  end

  describe "#show_payoneer_redesigned_onboarding?" do
    it "should return true if payoneer is mandated and redesigned flow is enabled for USA customers" do
      allow(helper).to receive(:current_user).and_return(person)
      allow(helper).to receive(:payoneer_mandatory?).and_return(true)
      allow(Payoneer::FeatureService).to receive(:payoneer_redesigned_onboarding?).and_return(true)
      expect(helper.show_payoneer_redesigned_onboarding?).to eq(true)
    end

    it "should return true if payoneer is mandated and redesigned flow is enabled for IN customers" do
      india_site_user = create(:person, country: "IN")
      allow(helper).to receive(:current_user).and_return(india_site_user)
      allow(helper).to receive(:payoneer_mandatory?).and_return(true)
      allow(Payoneer::FeatureService).to receive(:payoneer_redesigned_onboarding?).and_return(true)
      expect(helper.show_payoneer_redesigned_onboarding?).to eq(true)
    end

    it "should return false if payoneer is mandated and redesigned flow is enabled for other country (decided by Person.country) customers" do
      allow(helper).to receive(:current_user).and_return(canadian_person)
      allow(helper).to receive(:payoneer_mandatory?).and_return(true)
      allow(Payoneer::FeatureService).to receive(:payoneer_redesigned_onboarding?).and_return(true)
      expect(helper.show_payoneer_redesigned_onboarding?).to eq(true)
    end

    it "should return false if payoneer is NOT mandated and redesigned flow is enabled" do
      allow(helper).to receive(:current_user).and_return(person)
      allow(helper).to receive(:payoneer_mandatory?).and_return(false)
      allow(Payoneer::FeatureService).to receive(:payoneer_redesigned_onboarding?).and_return(true)
      expect(helper.show_payoneer_redesigned_onboarding?).to eq(false)
    end

    it "should return false if payoneer is mandated and redesigned flow is NOT enabled" do
      allow(helper).to receive(:current_user).and_return(person)
      allow(helper).to receive(:payoneer_mandatory?).and_return(true)
      allow(Payoneer::FeatureService).to receive(:payoneer_redesigned_onboarding?).and_return(false)
      expect(helper.show_payoneer_redesigned_onboarding?).to eq(false)
    end
  end

  describe "#has_used_legacy_system?" do
    before :each do
      person.person_balance.update(balance: 50000)
      allow(helper).to receive(:current_user).and_return(person)
    end

    context "user has an existing check transfer" do
      it "returns true" do
        transfer = build(:check_transfer, person: person, currency: person.currency)
        transfer.save(validate: false)
        expect(helper.has_used_legacy_system?).to be_truthy
      end
    end

    context "user has an existing eft bank transfer" do
      it "returns true" do
        bank_account = create(:stored_bank_account, person: person)
        fee = create(:payout_service_fee)
        transaction = build(:eft_batch_transaction, currency: person.currency, stored_bank_account: bank_account, payout_service_fee: fee)
        transaction.save(validate: false)
        expect(helper.has_used_legacy_system?).to be_truthy
      end
    end

    context "user has an existing paypal transfer" do
      it "returns true" do
        person.paypal_transfers << create(:paypal_transfer)
        expect(helper.has_used_legacy_system?).to be_truthy
      end
    end

    context "user has no previous withdrawals" do
      it "should return false" do
        expect(helper.has_used_legacy_system?).to be_falsey
      end
    end
  end

  describe "#payoneer_onboard_steps" do
    context "@payoneer_onboard_steps is not set" do
      context "user does not have payout provider" do
        it "should return true for the first step" do
          allow(helper).to receive(:current_user).and_return(person_without_payoneer)

          expect(helper.payoneer_onboard_steps).to eq([true, false, nil, nil])
        end
      end

      context "user is in pending process for application" do
        it "should return true for the second step" do
          allow(helper).to receive(:current_user).and_return(person)
          person.payout_provider.update(provider_status: PayoutProvider::PENDING)

          expect(helper.payoneer_onboard_steps).to eq([false, true, false, false])
        end
      end

      context "user is approved and does not need tax form" do
        it "should return true for the second step" do
          allow(helper).to receive(:current_user).and_return(person)

          expect(helper.payoneer_onboard_steps).to eq([false, false, true, false])
        end
      end

      context "user is approved and needs tax form" do
        it "should return true for the second step" do
          rich_person = create(:person, :with_payout_provider, :with_taxable_earnings, country_website_id: 1, country: "US")
          allow(helper).to receive(:current_user).and_return(rich_person)

          allow_any_instance_of(Payoneer::TaxApiService).to receive(:has_active_forms?).and_return(false)

          expect(helper.payoneer_onboard_steps).to eq([false, false, false, true])
        end
      end
    end

    context "@payoneer_onboard_steps is set" do
      it "returns the original data" do
        @payoneer_onboard_steps = [false, false, false, true]
        expect(helper.payoneer_onboard_steps).to eq(@payoneer_onboard_steps)
      end
    end
  end

  describe "#current_step" do
    it "returns the current step the user is at in the process" do
      allow(helper).to receive(:current_user).and_return(person)

      expect(helper.current_step).to eq(3)
    end
  end

  describe "#payoneer_last_onboard_step?" do
    context "user is on last step of process" do
      it "returns true" do
        rich_person = create(:person, :with_payout_provider, :with_taxable_earnings, country_website_id: 1, country: "US")
        allow(helper).to receive(:current_user).and_return(rich_person)

        allow_any_instance_of(Payoneer::TaxApiService).to receive(:has_active_forms?).and_return(false)

        expect(helper.payoneer_last_onboard_step?).to be_truthy
      end
    end

    context "user is not on last step of process" do
      it "returns false" do
        allow(helper).to receive(:current_user).and_return(person)

        expect(helper.payoneer_last_onboard_step?).to be_falsey
      end
    end
  end

  describe "#total_steps_to_complete" do
    context "user needs tax form" do
      it "returns 4" do
        rich_person = create(:person, :with_payout_provider, :with_taxable_earnings, country_website_id: 1, country: "US")
        allow(helper).to receive(:current_user).and_return(rich_person)

        allow_any_instance_of(Payoneer::TaxApiService).to receive(:has_active_forms?).and_return(false)

        expect(helper.total_steps_to_complete).to eql(4)
      end
    end

    context "user does not need tax form" do
      it "returns 3" do
        allow(helper).to receive(:current_user).and_return(person)

        expect(helper.total_steps_to_complete).to eql(3)
      end
    end
  end

  describe "#show_payoneer_currency_selector?" do
    context "user already has a payout provider" do
      it "should return false" do
        allow(helper).to receive(:current_user).and_return(person)

        expect(helper.show_payoneer_currency_selector?).to be_falsey
      end
    end

    context "user has no payout provider and feature flag is enabled" do
      it "should return true" do
        allow(helper).to receive(:current_user).and_return(person_without_payoneer)

        allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_payout_usa_withdrawal_currency_dropdown, person_without_payoneer).and_return(true)

        expect(helper.show_payoneer_currency_selector?).to be_truthy
      end
    end
  end

  describe "#calculate_status_bar" do
    context "when tax forms are required" do
      it "calculates the amount determined with 4 steps" do
        rich_person = create(:person, :with_payout_provider, :with_taxable_earnings, country_website_id: 1, country: "US")
        allow(helper).to receive(:current_user).and_return(rich_person)
        allow_any_instance_of(Payoneer::TaxApiService).to receive(:has_active_forms?).and_return(false)

        expect(helper.calculate_status_bar).to eql(100)
      end
    end

    context "when tax forms are not required" do
      it "calculates the amount in thirds" do
        allow(helper).to receive(:current_user).and_return(person_without_payoneer)

        expect(helper.calculate_status_bar).to eql(33)
      end
    end
  end
end
