require "rails_helper"

describe FetchStoresHelper do
  fixtures :songs
  describe "#retrieve_song_url" do
    it "returns song url with corresponding esi" do
      album = create(:album, :with_spotify_album_esi)
      song = create(:song, :with_spotify_song_esi, album: album)
      esi = song.external_service_ids.where(service_name: ['apple', 'spotify']).last
      song_url = helper.retrieve_song_url(esi)
      expect(song_url).not_to eq(nil)
    end
  end

  describe "#fetch_odesli_stores" do
    it "returns array of store names" do
      response_hash = '{"entitiesByUniqueId":{"SPOTIFY_SONG::53A2MsBqPdDzZzP3Dm8rfR":{"data": "data"},"ITUNES_SONG::1471508788":{"data": "data"},"YOUTUBE_VIDEO::hSB-IfTzrp8":{"data": "data"},"GOOGLE_SONG::Txrdqsng2w5h5vdgqjyv35hqume":{"data": "data"},"DEEZER_SONG::707260582":{"data": "data"}}}'
      response_hash = JSON.parse(response_hash)
      response = helper.fetch_odesli_stores(response_hash["entitiesByUniqueId"])
      expect(response).to have_at_least(1).things
    end
  end

  describe "#create_odesli_hash" do
    it "returns a valid hash" do
      response_hash = '{"entitiesByUniqueId":{"SPOTIFY_SONG::53A2MsBqPdDzZzP3Dm8rfR":{"data": "data"},"ITUNES_SONG::1471508788":{"data": "data"},"YOUTUBE_VIDEO::hSB-IfTzrp8":{"data": "data"},"GOOGLE_SONG::Txrdqsng2w5h5vdgqjyv35hqume":{"data": "data"},"DEEZER_SONG::707260582":{"data": "data"}}}'
      response_hash = JSON.parse(response_hash)
      response = helper.create_odesli_hash('spotify', response_hash)
      expect(response).to have_key(:name).and have_key(:id)
    end
  end
end