require "rails_helper"

describe MassAdjustmentsHelper, type: :helper do
  let(:batch) { create(:mass_adjustment_batch) }

  describe "#batch_status" do
    subject { helper.batch_status(batch) }

    context "processing completed" do
      before { allow(batch).to receive(:processing_completed?).and_return(true) }

      it { should eq("Processing Complete") }
    end

    context "not completed processing" do
      it { should eq(batch.status.capitalize) }
    end
  end

  describe "#processed_status" do
    context "no entries" do
      it "should return nil" do
        result = helper.processed_status(batch)

        expect(result).to be_nil
      end
    end

    context "has entries" do
      it "should return proper message" do
        entries = create_list(:mass_adjustment_entry, 3, mass_adjustment_batch: batch)

        entries.first.update(status: 'success')

        result = helper.processed_status(batch)

        expected_result = "<div>Total entries: <b>3</b></div>
     <div>Positive entries processed: <b>1</b></div>
     <div>Negative entries processed: <b>0</b></div>
     <div>Cancelled Entries: <b>0</b></div>
     <div>Entries left to process: <b>2</b></div>"

        expect(result).to eq(expected_result)
      end
    end
  end
end
