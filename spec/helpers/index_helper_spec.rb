require "rails_helper"

describe IndexHelper, type: :helper do
  context "#render_terms with publishing_terms" do
    it "should render page for all countries" do
      CountryWebsite.where.not(id: CountryWebsite::INDIA).each do |cw|
        expected_term_country = ["CA","UK"].include?(cw.country) ? "us" : cw.country.downcase
        allow(helper).to receive(:render).and_call_original
        allow(helper).to receive(:get_country_prefix).and_return(expected_term_country)
        helper.render_terms("publishing_terms")

        expect(helper).to have_received(:render)
          .with(partial: "shared/terms/#{expected_term_country}_publishing_terms")
          .at_least(1).times
      end
    end
  end
end
