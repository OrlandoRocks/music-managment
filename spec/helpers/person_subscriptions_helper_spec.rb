require "rails_helper"

describe PersonSubscriptionsHelper do
  describe "#subscription_term_text" do
    context "when the subscription products term length is monthly" do
      it "returns 'month'" do
        expect(helper.subscription_term_text("monthly")).to eq "month"
      end
    end

    context "when the subscription products term length is annually" do
      it "returns 'year'" do
        expect(helper.subscription_term_text("annually")).to eq "year"
      end
    end
  end

  describe "#subscription_type_text" do
    context "when the product is 'Social'" do
      it "returns 'TuneCore Social Pro'" do
        product = build_stubbed(:subscription_product, product_name: "Social")
        expect(helper.subscription_type_text(product)).to eq "TuneCore Social Pro"
      end
    end
  end
end
