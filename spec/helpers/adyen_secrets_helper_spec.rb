require "rails_helper"

describe AdyenSecretsHelper, type: :helper do
  let(:person) { create(:person, country: "PH") }
  let(:invoice) { create(:invoice, person: person) }
  let!(:adyen_merchant_config) { create(:adyen_merchant_config) }

  it "should return adyen merchant config for corporate_entity" do
    response = config_for_corporate_entity(invoice.corporate_entity_id)
    expect(response).to eq(adyen_merchant_config)
  end

  it "should return adyen merchant config for merchant_account" do
    response = config_for_merchant_account(adyen_merchant_config.merchant_account)
    expect(response).to eq(adyen_merchant_config)
  end
end
