require "rails_helper"

describe PayoneerFeesHelper, type: :helper do
  let(:indian_person) { create(:person, country: "IN", country_website_id: 8) }
  let(:us_person) { create(:person) }

  before do
    create(:foreign_exchange_pegged_rate, pegged_rate: 1, country_id: 101, currency: "INR", country_website_id: 8)
    create(:foreign_exchange_rate, exchange_rate: 1, source_currency: "USD", target_currency: "INR")
    helper.extend(MockApplicationHelper)
    allow(helper).to receive(:current_user).and_return(us_person)
    allow(helper).to receive(:country_website).and_return(us_person.country_domain)
  end

  shared_examples "customized price" do
    context "localized fee" do
      it "should return per transaction message" do
        allow(helper).to receive(:current_user).and_return(indian_person)
        allow(helper).to receive(:country_website).and_return(indian_person.country_domain)

        expect(subject).to include("₹#{price}*")
      end
    end

    context "non localized fee" do
      it "should return per transaction message" do
        expect(subject).to include("$#{price}")
      end
    end
  end

  describe "#mastercard_activation_fee" do
    subject { helper.mastercard_activation_fee }
    let(:price) { Money.new(1000) }

    it_behaves_like "customized price"
  end

  describe "#mastercard_atm_fee" do
    subject { helper.mastercard_atm_fee }
    let(:price) { Money.new(175) }

    it_behaves_like "customized price"
  end

  describe "#display_localized_fee?" do
    subject { helper.display_localized_fee? }

    context "user is from supported country" do
      before do
        allow(helper).to receive(:current_user).and_return(indian_person)
        allow(helper).to receive(:country_website).and_return(indian_person.country_domain)
      end

      it { should be_truthy }
    end

    context "user is from unsupported country" do
      it { should be_falsey }
    end
  end
end
