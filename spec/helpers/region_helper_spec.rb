require "rails_helper"

describe RegionHelper, type: :helper do
  let(:sanctioned)       { Country.unscoped.sanctioned_iso_codes }
  let(:country_websites) { CountryWebsite.pluck(:id) }

  context "when calling render_if_available_in_region" do
    before (:each) do
      stub_const("PRODUCT_REGIONS", { "US" => ["facebook", "radio_airplay"], "UK" => ["facebook"] })
    end

    it "return nothing for the product if the product is NOT available in the region" do
      expect(helper.render_if_available_in_region("UK", "radio_airplay", :dashboard)).to be_nil
    end
  end

  context ".allow_credit_cards?" do
    let(:person) { FactoryBot.create(:person, country: "Germany") }

    it "is true for a German person" do
      expect(helper.allow_braintree_credit_cards?(person)).to eq true
    end
  end

  context ".product_url_for" do
    it "returns html_safe string" do
      helper.extend(MockApplicationHelper)
      allow(helper).to receive(:country_website).and_return("US")
      expect(helper.product_url_for(:tcsocial, :dashboard).html_safe?).to eq true
    end
  end

  describe "#country_dropdown_for_website" do
    it "never includes sanctioned countries" do
      country_websites.each do |id|
        results = helper.country_dropdown_for_website(id).flatten
        expect(results.include?(sanctioned)).to be false
      end
    end
  end

  describe "#all_countries" do
    it "never includes sanctioned countries" do
      expect(all_countries.include?(sanctioned)).to be false
    end
  end

  describe "#is_a_cyrillic_language?" do
    it "is true if ru/uk" do
      expect(helper.is_a_cyrillic_language?("ru")).to eq true
    end

    it "is false if not ru/uk" do
      expect(helper.is_a_cyrillic_language?("en")).to eq false
    end
  end
end
