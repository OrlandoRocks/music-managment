require "rails_helper"

describe ApplicationHelper, type: :helper do
  let(:person) { create(:person) }

  before(:each) do
    helper.extend(MockApplicationHelper)
    allow(helper).to receive(:current_user).and_return(person)
  end

  context "when calling #creatives_options_for_customers" do
    it "should return an array" do
      expect(helper.creatives_options_for_customers).to be_an_instance_of(Array)
    end

    it "should return an array of 2-item arrays (each strings)" do
      helper.creatives_options_for_customers.each do |item|
        expect(item.size).to eq(2)
        expect(item.first).to be_an_instance_of(String)
        expect(item.last).to be_an_instance_of(String)
      end
    end

    it "should make break apart camel-cased words (PrimaryArist => Primary Artist)" do
      primary = helper.creatives_options_for_customers.first
      expect(primary.first).to eq("Primary Artist")
      expect(primary.last).to eq("PrimaryArtist")
    end

    it "should have Primary, Featuring, and With" do
      roles = helper.creatives_options_for_customers
      expect(roles[0].first).to eq("Primary Artist")
      expect(roles[1].first).to eq("Featuring")
      expect(roles[2].first).to eq("With")
    end

    it "should only have 3 creative roles" do
      helper.creatives_options_for_customers == 3
    end
  end

  context "when calling #creatives_options_for_admins" do
    it "should return an array" do
      expect(helper.creatives_options_for_admins).to be_an_instance_of(Array)
    end

    it "should return an array of 2-item arrays (each strings)" do
      helper.creatives_options_for_admins.each do |item|
        expect(item.size).to eq(2)
        expect(item.first).to be_an_instance_of(String)
        expect(item.last).to be_an_instance_of(String)
      end
    end

    it "should make break apart camel-cased words (PrimaryArist => Primary Artist)" do
      primary = helper.creatives_options_for_admins.first
      expect(primary.first).to eq("Primary Artist")
      expect(primary.last).to eq("PrimaryArtist")
    end

    it "should always offer the same number of roles as is presented in the Creative.roles" do
      expect(helper.creatives_options_for_admins.size).to eq(Creative.roles.size)
    end
  end

  describe "#money_with_discount" do
    context "no discount available" do
      it "should return the current amount" do
        subject = helper.money_with_discount(10, 10, "USD")

        expect(subject).to eq("$10.00")
      end
    end

    context "discount available" do
      it "should return discount with original amount" do
        subject = helper.money_with_discount(10, 5, "USD")

        expect(subject).to eq("<s>$10.00</s> $5.00")
      end
    end
  end
end
