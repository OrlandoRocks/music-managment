require "rails_helper"

describe OnDemandTemplateHelper do
  context "#template_id_for_slider_index" do
    it "should return 4 when no parameter passed" do
      expect(helper.template_id_for_slider_index).to eq(4)
    end

    it "should return 4 for a nil template_id" do
      expect(helper.template_id_for_slider_index(nil)).to eq(4)
    end

    it "should return 4 for a 1 template_id" do
      expect(helper.template_id_for_slider_index(1)).to eq(4)
    end

    it "should return template_id when not nil or 4" do
      temp_id = 5
      expect(helper.template_id_for_slider_index(temp_id)).to eq(temp_id)
    end
  end
end
