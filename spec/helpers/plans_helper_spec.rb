require "rails_helper"

describe PlansHelper do
  let!(:user) { create(:person) }

  before do
    helper.extend(MockApplicationHelper)
    allow(helper).to receive(:session).and_return({ return_to: "/plans" })
    allow(helper).to receive(:current_user).and_return(user)
    allow_any_instance_of(ActionController::TestRequest).to receive(:referrer).and_return(origin_url)
  end

  describe "#continue_redirect_path" do
    context "when user has a plan in the cart" do
      before { allow(user).to receive(:cart_items_count).and_return(1)}

      context "when there is no referrer" do

        let(:origin_url) { nil }

        it "returns the cart path" do

          expect(helper.continue_redirect_path(user)).to eq("/cart")
        end
      end

      context "when user must fill Payoneer tax_info" do
        let(:origin_url) { "https://tunecore.com/plans" }

        it "returns the edit path" do
          user.tax_tokens.create(token: SecureRandom.hex, is_visible: true)
          allow(FeatureFlipper)
            .to receive(:show_feature?).with(:payoneer_login_redirect, user).and_return true

          expect(helper.continue_redirect_path(user)).to eq(edit_person_path(user, tab: "tax_info"))
        end

      end

      context "when referrer request was redirected from the login path" do

        let(:origin_url) { "https://tunecore.com/login?check=1" }

        it "returns the cart path" do
          expect(helper.continue_redirect_path(user)).to eq("/cart")
        end

      end

      context "when referrer request was redirected from the plans path" do

        let(:origin_url) { "https://tunecore.com/plans" }

        it "returns the cart path" do
          expect(helper.continue_redirect_path(user)).to eq("/cart")
        end

      end

      context "when the when referrer request was not from plans, login or external" do

        let(:origin_url) { "https://tunecore.com/store_manager" }

        it "returns the dashboard path" do
          expect(helper.continue_redirect_path(user)).to eq("https://tunecore.com/store_manager")
        end
      end

      context "when the when referrer request was from an external site" do

        let(:origin_url) { "https://cdbaby.com/tunecore_targeted_advertisment?plans=true%20%wehavekanye=true" }

        it "returns the dashboard path" do
          expect(helper.continue_redirect_path(user)).to eq("/cart")
        end
      end
    end

    context "when user does not have a plan in the cart" do

      before { allow(user).to receive(:cart_items_count).and_return(0)}

      context "when there is no referrer" do

        let(:origin_url) { nil }

        it "returns the dashboard path" do

          expect(helper.continue_redirect_path(user)).to eq("/dashboard")
        end
      end

      context "when referrer request was redirected from the login path" do

        let(:origin_url) { "https://tunecore.com/login?check=1" }

        it "returns the dashboard path" do
          expect(helper.continue_redirect_path(user)).to eq("/dashboard")
        end

      end

      context "when referrer request was redirected from the plans path" do

        let(:origin_url) { "https://tunecore.com/plans" }

        it "returns the dashboard path" do
          expect(helper.continue_redirect_path(user)).to eq("/dashboard")
        end
      end

      context "when referrer request was redirected from the create_account path" do

        let(:origin_url) { "https://tunecore.com/dashboard/create_account" }

        it "returns the cart path" do
          expect(helper.continue_redirect_path(user)).to eq("/dashboard")
        end
      end

      context "when the when referrer request was not from plans, login or external" do

        let(:origin_url) { "https://tunecore.com/store_manager" }

        it "returns the dashboard path" do
          expect(helper.continue_redirect_path(user)).to eq("https://tunecore.com/store_manager")
        end
      end
    end

    context "when the when referrer request was from an external site" do

      let(:origin_url) { "https://cdbaby.com/tunecore_targeted_advertisment?plans=true%20%wehavekanye=true" }

      it "returns the dashboard path" do
        expect(helper.continue_redirect_path(user)).to eq("/dashboard")
      end
    end
  end
end
