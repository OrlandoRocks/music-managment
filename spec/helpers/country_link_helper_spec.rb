require "rails_helper"

describe CountryLinkHelper do
  context "#link_to_pub_admin_page" do
    let(:expected_links) { { "US" => "//pub.help.tunecore.com/app/home",
      "CA" => "//pub.help.tunecore.com/app/home",
      "UK" => "//pub.help.tunecore.com/app/home",
      "AU" => "//pub.help.tunecore.com/app/home",
      "DE" => "//de.help.tunecore.com/app/answers/list/kw/publishing/search/1",
      "FR" => "//fr.help.tunecore.com/app/answers/list/kw/publishing/search/1",
      "IT" => "//it.help.tunecore.com/app/answers/list/kw/publishing/search/1"}
    }
    it "should serve appropriate page for each country" do
      CountryWebsite.all.each do |cw|
        expect(helper.link_to_pub_admin_help_page(cw)).to eq(expected_links[cw])
      end
    end
  end
end
