require "rails_helper"

describe MarketingHelper, type: :helper do
  describe ".cta_callout_enabled_for?" do
    before(:each) do
      allow(FeatureFlipper).to receive(:show_feature?) { true }
    end

    let(:user) { FactoryBot.create(:person) }

    context "enabled for" do
      [
        {"American"   => 1}
      ].each do |country|
        it "#{country.keys.first} users" do
          user.country_website_id = country.values.first
          expect(helper.cta_callout_enabled_for?(user)).to be_truthy
        end
      end
    end

    context "disabled for" do
      [
        {"Canadian"   => 2},
        {"British"    => 3},
        {"Australian" => 4},
        {"German"     => 5},
        {"French"     => 6}
      ].each do |country|
        it "#{country.keys.first} users" do
          user.country_website_id = country.values.first
          expect(helper.cta_callout_enabled_for?(user)).to be_falsey
        end
      end
    end
  end
end
