require "rails_helper"

describe CountryPhoneCodesHelper do
  describe "#custom_t_name_and_phone_codes_for_countries" do
    it "returns country name (with area code) and phone code" do
      united_states = find_country("United States (1)")
      phone_code    = united_states.last
      country_name_with_area_code = united_states.first

      expect(phone_code).to eq "1"
      expect(country_name_with_area_code).to eq "United States (1)"
    end

    context "when the country name contains parentheses" do
      it "returns country name (with area code) and phone code" do
        cocos_islands = find_country("Cocos (Keeling) Islands (61)")
        phone_code    = cocos_islands.last
        country_name_with_area_code = cocos_islands.first

        expect(phone_code).to eq "61"
        expect(country_name_with_area_code).to eq "Cocos (Keeling) Islands (61)"
      end
    end

    context "when the country name has a diacritic character" do
      it "returns country name (with area code) and phone code" do
        reunion                     = find_country("Réunion (262)")
        country_name_with_area_code = reunion.first
        phone_code                  = reunion.last

        expect(phone_code).to eq "262"
        expect(country_name_with_area_code).to eq "Réunion (262)"
      end
    end
  end

  def find_country(country_name_with_area_code)
    country_phone_codes_arr = helper.custom_t_name_and_phone_codes_for_countries
    country_phone_codes_arr.select { |arr| arr.first == country_name_with_area_code }.first
  end
end
