require "rails_helper"

describe Sync::SearchHelper, type: :helper do

  it "should properly convert big box durations" do
    # durations from bigbox are in milliseconds
    duration = 30 * 1000
    expect(helper.convert_duration(duration)).to eq(" 0:30")

    duration = (1 * 60 + 30) * 1000
    expect(helper.convert_duration(duration)).to eq(" 1:30")

    duration = (10 * 60 + 15) * 1000
    expect(helper.convert_duration(duration)).to eq("10:15")

    duration = (61 * 60 + 20) * 1000
    expect(helper.convert_duration(duration)).to eq(" 1:01:20")

    duration = nil
    expect(helper.convert_duration(duration)).to eq("")

    duration = ""
    expect(helper.convert_duration(duration)).to eq("")
  end


  describe '#sync_artwork_url' do
    it "generate the appropriate url" do
      url = 'https://s3.amazonaws.com/test.pub.tunecore.com/artwork/original/3/3.tiff?0000000'

      result = sync_artwork_url(url)

      expect(result).to eq("//pub.tunecore.com/artwork/original/3/3.tiff?0000000")
    end
  end
end
