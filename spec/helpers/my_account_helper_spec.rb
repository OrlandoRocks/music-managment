require "rails_helper"

describe MyAccountHelper, type: :helper do
  describe "#display_person_transaction_headline" do

    let(:person) { create(:person, :admin) }

    it "displays a processing status for a payoneer payout transaction" do
      transaction = create(:person_transaction,
                           person: person,
                           comment: "May 2006 royalty payment for iTunes UK/European Union",
                           target: create(:payout_transfer,
                                          person: person,
                                          amount_cents: 50000,
                                          tunecore_status: PayoutTransfer::SUBMITTED,
                                          provider_status: PayoutTransfer::NONE
                           )
      )
      helper.extend(MockApplicationHelper)
      helper.current_user = person
      output = helper.display_person_transaction_headline(transaction)

      expect(output.include?("Status: Processing")).to eq(true)
    end

    it "displays a Cancelled status for a payoneer payout transaction" do
      transaction = create(:person_transaction,
                           person: person,
                           comment: "May 2006 royalty payment for iTunes UK/European Union",
                           target: create(:payout_transfer,
                                          person: person,
                                          amount_cents: 50000,
                                          tunecore_status: PayoutTransfer::REJECTED,
                                          provider_status: PayoutTransfer::NONE
                           )
      )
      helper.extend(MockApplicationHelper)
      helper.current_user = person
      output = helper.display_person_transaction_headline(transaction)

      expect(output.include?("Status: Cancelled")).to eq(true)
    end

    it "displays a completed status for a payoneer payout transaction" do
      transaction = create(:person_transaction,
                           person: person,
                           comment: "May 2006 royalty payment for iTunes UK/European Union",
                           target: create(:payout_transfer,
                                          person: person,
                                          amount_cents: 50000,
                                          tunecore_status: PayoutTransfer::APPROVED,
                                          provider_status: PayoutTransfer::COMPLETED
                           )
      )
      helper.extend(MockApplicationHelper)
      helper.current_user = person
      output = helper.display_person_transaction_headline(transaction)

      expect(output.include?("Status: Completed")).to eq(true)
    end
  end

  describe "#display_songwriter_royalty_link" do
    it "should not show a link to finish tax info if current user is not the person_transaction's person" do
      helper.extend(MockApplicationHelper)
      person_with_publishing_composer = create(:person)
      person_without_publishing_composer = create(:person)

      expect(person_without_publishing_composer.publishing_composers.first).to be_blank

      publishing_composer = create(:publishing_composer, person_id: person_with_publishing_composer.id)
      ba = create(:balance_adjustment, person: person_with_publishing_composer, category: "Songwriter Royalty", posted_by: person_without_publishing_composer, posted_by_name: person_without_publishing_composer.name)
      transaction = create(:person_transaction, person: person_with_publishing_composer, target_type: 'BalanceAdjustment', target_id: ba.id)
      royalty_payment = RoyaltyPayment.create(person: person_with_publishing_composer, publishing_composer: publishing_composer, posting: ba, hold_payment: true, hold_type: "missing_tax" )

      helper.current_user = person_without_publishing_composer
      output = helper.display_songwriter_royalty_link(transaction, 'PDF')
      expect(output.include?(new_composer_tax_info_path(person_with_publishing_composer.publishing_composers.first, tax_form: 'w9'))).to eq(false)

      helper.current_user = person_with_publishing_composer
      output = helper.display_songwriter_royalty_link(transaction, 'PDF')
      expect(output.include?(new_composer_tax_info_path(person_with_publishing_composer.publishing_composers.first, tax_form: 'w9'))).to eq(true)
    end

    it "should not show a link to a pdf if the person_transaction is not linked to a royalty payment" do
      helper.extend(MockApplicationHelper)
      person_with_publishing_composer = create(:person)
      publishing_composer = create(:publishing_composer, person_id: person_with_publishing_composer.id)
      ba = create(:balance_adjustment, person: person_with_publishing_composer, category: "Songwriter Royalty", posted_by: person_with_publishing_composer, posted_by_name: person_with_publishing_composer.name)
      transaction = create(:person_transaction, person: person_with_publishing_composer, target_type: 'BalanceAdjustment', target_id: ba.id)

      helper.current_user = person_with_publishing_composer
      output = helper.display_songwriter_royalty_link(transaction, 'PDF')
      expect(output.include?(new_composer_tax_info_path(person_with_publishing_composer.publishing_composers.first, tax_form: 'w9'))).to eq(false)
    end

  end

  describe "#display_split_link" do
    let(:person) { create(:person, :admin) }
    let(:royalty_split) { create(:royalty_split, owner: person) }

    it "adds royalty link for mah" do
      helper.extend(MockApplicationHelper)
      helper.current_user = person
      split = RoyaltySplitDetail.new
      split.royalty_split_id = royalty_split.id
      split.royalty_split_title = royalty_split.title
      split.amount = -20

      output = helper.display_split_link(split)

      expect(output.include?(split.royalty_split_title)).to eq(true)
      expect(output.include?(royalty_splits_path(anchor: dom_id(royalty_split)))).to eq(true)
    end

    it "adds royalty invites for collborators" do
      helper.extend(MockApplicationHelper)
      helper.current_user = person
      split = RoyaltySplitDetail.new
      split.royalty_split_id = royalty_split.id
      split.royalty_split_title = royalty_split.title
      split.amount = 20

      output = helper.display_split_link(split)

      expect(output.include?(split.royalty_split_title)).to eq(true)
      expect(output.include?(invited_royalty_splits_path(anchor: dom_id(royalty_split)))).to eq(true)
    end

    it "adds album link if royalty does not exist" do
      song_id = 123
      album = create(:album)
      create(:song, id: song_id, album: album)
      helper.extend(MockApplicationHelper)
      helper.current_user = person
      split = RoyaltySplitDetail.new
      split.song_id = song_id
      split.royalty_split_title = "title"
      split.amount = -20

      output = helper.display_split_link(split)

      expect(output.include?(split.royalty_split_title)).to eq(true)
      expect(output.include?(album_path(album))).to eq(true)
    end
  end
end
