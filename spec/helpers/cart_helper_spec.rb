 require "rails_helper"

describe CartHelper, type: :helper do
  describe "#show_gst_breakdown?" do
    context "returns false" do
      let(:invoice) { create(:invoice) }
      let(:no_invoice) { nil }
      let(:person_with_india_country_site) { create(:person, country: "IN", country_website_id: 8) }
      let(:person_with_us_country_site) { create(:person) }

      it "when user's country website is not .in" do
        @person = person_with_us_country_site
        expect(helper.show_gst_breakdown?(invoice)).to be_falsey
      end

      it "returns false when user has no invoice" do
        @person = person_with_india_country_site
        expect(helper.show_gst_breakdown?(no_invoice)).to be_falsey
        @person = person_with_us_country_site
        expect(helper.show_gst_breakdown?(no_invoice)).to be_falsey
      end
    end
    context "returns true" do
      let(:invoice) { create(:invoice) }
      let(:person_with_india_country_site) { create(:person, country: "IN", country_website_id: 8) }

      it " when user's country site is .in and they have an invoice" do
      @person = person_with_india_country_site
      expect(helper.show_gst_breakdown?(invoice)).to be_truthy
      end
    end
  end

  describe "#skip_confirm_and_pay" do
    let!(:person) { create(:person) }
    let!(:album) { create(:album, person: person) }
    let!(:salepoint_subscription) { create(:salepoint_subscription, album: album) }

    context "user with only purchases which are free via plan" do
      before do
        create(:person_plan, person: person)
        credit_usage = CreditUsage.for(person, album)
        purchase = create(:purchase, :free_with_plan, person: person, related_id: credit_usage.id)
        @purchases = [purchase]
      end

      it "returns true" do
        expect(helper.skip_confirm_and_pay?).to eq true
      end
    end

    context "user with both free plan-related purchases and non-plan purchases" do
      before do
        create(:person_plan, person: person)
        credit_usage = CreditUsage.for(person, album)
        purchase = create(:purchase, :free_with_plan, person: person, related_id: credit_usage.id)

        product_id = Product::PRODUCT_COUNTRY_MAP['US'][:album_distribution].first
        purchase_2 = create(:purchase, person: person, product_id: product_id, related: album)

        @purchases = [purchase, purchase_2]
      end

      it "returns false" do
        expect(helper.skip_confirm_and_pay?).to eq false
      end
    end

    context "user with only non-plan-related purchases" do
      before do
        product_id = Product::PRODUCT_COUNTRY_MAP['US'][:album_distribution].first
        purchase = create(:purchase, person: person, product_id: product_id, related: album)
        @purchases = [purchase]
      end

      it "returns false" do
        expect(helper.skip_confirm_and_pay?).to eq false
      end
    end

    context "eligible plan user with a store automator purchase" do
      before do
        allow(FeatureFlipper).to receive(:show_feature?).and_return true

        create(:person_plan, person: person)
        automator_product = Product.automator_product(person)
        purchase = create(:purchase, person: person, product_id: automator_product.id, related: salepoint_subscription)
        @purchases = [purchase]
      end

      it "returns true" do
        expect(helper.skip_confirm_and_pay?).to eq true
      end
    end

    context "legacy user with a store automator purchase" do
      before do
        allow(FeatureFlipper).to receive(:show_feature?).and_return true

        automator_product = Product.automator_product(person)
        purchase = create(:purchase, person: person, product_id: automator_product.id, related:  salepoint_subscription)
        @purchases = [purchase]
      end

      it "returns false" do
        expect(helper.skip_confirm_and_pay?).to eq false
      end
    end
  end
end
