require "rails_helper"

describe SfaHelper do
  describe "#service_enabled?" do
    let(:person) { create(:person) }

    subject { helper.service_enabled?(ExternalServiceId::APPLE_SERVICE, :apple_music_for_artists, 'iTunesWW') }

    before do
      helper.extend(MockApplicationHelper)
      allow(helper).to receive(:current_user).and_return(person)
    end

    context "feature enabled" do
      before { allow(FeatureFlipper).to receive(:show_feature?).and_return(true) }

      context "with no blacklist" do
        before { allow(FeatureBlacklist).to receive(:exists?).and_return(false) }

        context "artist has service ids attached" do
          before do
            allow_any_instance_of(ServiceForArtists::QueryBuilder)
              .to receive(:artists_with_service_ids)
              .and_return([true])
          end

          context "user has subject store" do
            before do
              album = create(:album, person: person)
              create(:salepoint, :itunes, salepointable: album)
            end

            it { should be_truthy }
          end

          context "user has no subject stores" do
            it { should be_falsey }
          end
        end

        context "artist has distributions with the store" do
          before do
            allow_any_instance_of(ServiceForArtists::QueryBuilder)
              .to receive(:albums_with_service_ids)
              .and_return([true])
          end

          context "user has salepoints" do
            before do
              album = create(:album, person: person)
              create(:salepoint, :itunes, salepointable: album)
            end

            it { should be_truthy }
          end

          context "user has no subject stores" do
            it { should be_falsey }
          end
        end

        context "artist has no service ids or distributions" do
          it { should be_falsey }
        end
      end

      context "with user blacklisted from feature" do
        before { allow(FeatureBlacklist).to receive(:exists?).and_return(true) }

        it { should be_falsey }
      end
    end

    context "feature disabled" do
      before { allow(FeatureFlipper).to receive(:show_feature?).and_return(false) }

      it { should be_falsey }
    end
  end

  describe "#amfa_enabled?" do
    it "should pass appropriate arg to service_enabled?" do
      expect(helper)
        .to receive(:service_enabled?)
        .with(
          ExternalServiceId::APPLE_SERVICE,
          :apple_music_for_artists,
          'iTunesWW'
        )

      helper.amfa_enabled?
    end
  end

  describe "#spotify_enabled?" do
    it "should pass appropriate arg to service_enabled?" do
      expect(helper)
        .to receive(:service_enabled?)
        .with(
          ExternalServiceId::SPOTIFY_SERVICE,
          :spotify_for_artists,
          'Spotify'
        )

      helper.spotify_enabled?
    end
  end
end
