require "rails_helper"

describe TwoFactorAuthHelper do
  before(:each) do
    @tfa = build(:two_factor_auth, phone_number: "1234567890")
  end

  describe "#formatted_phone_number" do
    it "returns the last 4 digits of the user's phone number" do
      expect(helper.formatted_phone_number(@tfa.phone_number)).to eq "7890"
    end
  end

  describe "#notification_message" do
    it "returns correct message for sms notification method" do
      @tfa.notification_method = "sms"
      msg = helper.notification_message(@tfa)
      expect(msg).to include(custom_t(:we_sent_code ))
    end

    it "returns correct message for call notification method" do
      @tfa.notification_method = "call"
      msg = helper.notification_message(@tfa)
      expect(msg).to include(custom_t(:we_are_calling))
    end

    it "returns correct message for app notification method" do
      @tfa.notification_method = "app"
      msg = helper.notification_message(@tfa)
      expect(msg).to include(custom_t(:please_check_app))
    end
  end

  describe "#should_show_tfa_prompt?" do
    class DummyTwoFactorController
      include TwoFactorAuthHelper
      attr_reader :current_user
      def initialize(current_user)
        @current_user = current_user
      end

      def under_admin_control?
        false
      end
    end
    let(:person) { create(:person) }
    let(:fake_helper_obj) { DummyTwoFactorController.new(person) }

    before(:each) do
      Timecop.freeze
    end

    after do
      Timecop.return
    end

    context "when feature flag is off" do
      it "returns nil" do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(false)
        expect(fake_helper_obj.should_show_tfa_prompt?).to be false
      end
    end

    context "when an admin takes over an account" do
      it "returns false" do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
        allow(helper).to receive(:session).and_return({admin: 12345678})
        expect(fake_helper_obj.should_show_tfa_prompt?).to be false
      end
    end

    context "when feature flag is on" do
      it "is false if person has a dashboard state of virgin" do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
        allow(person).to receive(:dashboard_state).and_return("virgin")
        expect(fake_helper_obj.should_show_tfa_prompt?).to be false
      end

      it "is true if person has a dashboard state other than virgin" do
        allow(person).to receive(:dashboard_state).and_return("not_virgin")
        expect(fake_helper_obj.should_show_tfa_prompt?).to be true
      end
    end
  end
end
