require "rails_helper"

describe MissingSalepointPurchaseForm do
  let(:person) { create(:person) }
  describe "#save" do
    it "creates purchases" do
      create(:album, :approved, person: person)
      create(:album, :taken_down, person: person)
      create(:album, :finalized, legal_review_state: "REJECTED", person: person)
      create(:album, :approved, person: person)
      create(:album, person: person)
      create(:album, person: person, salepoints: [
        create(:salepoint, store: Store.find_by(short_name: "iTunesUS"), variable_price_id: 39)
      ])
      form = MissingSalepointPurchaseForm.new(store_name: "iTunesWW", person: person)
      expect {
        form.save
      }.to change {
        person.reload.purchases.unpaid.not_in_invoice.count
      }.by(2)
    end
  end

  describe "valid?" do
    it "has a valid store" do
      form = MissingSalepointPurchaseForm.new(store_name: "abcd", person: person)
      expect(form.valid?).to be_falsey
      expect(form.errors.full_messages).to include("Store is invalid")
    end

    it "has a person" do
      form = MissingSalepointPurchaseForm.new(store_name: "iTunesWW")
      expect(form.valid?).to be_falsey
      expect(form.errors.full_messages).to include("Person can't be blank")
    end

    it "validates that it has a store name" do
      form = MissingSalepointPurchaseForm.new(person: person)
      expect(form.valid?).to be_falsey
      expect(form.errors.full_messages).to include("Store name can't be blank")
    end
  end
end
