require "rails_helper"

describe CreativeExternalServiceIdsForm do
  describe "#save" do
    context "when missing arguments" do
      it "adds an error message when missing a creative_id" do
        params = { creative_id: "" }

        form = CreativeExternalServiceIdsForm.new(params)

        form.save

        expect(form.errors.full_messages).to include("Creative can't be blank")
      end

      it "does not call its #update_creative_external_service_ids method" do
        params = { creative_id: "" }

        form = CreativeExternalServiceIdsForm.new(params)

        expect(form).to_not receive(:update_creative_external_service_ids)

        form.save
      end
    end

    context "when passed in all required arguments" do
      it "calls its #update_creative_external_service_ids method" do
        album = create(:album)
        creative = create(:creative, creativeable: album)

        params = { creative_id: creative.id }

        form = CreativeExternalServiceIdsForm.new(params)

        expect(form).to receive(:update_creative_external_service_ids)

        form.save
      end
    end
  end
end
