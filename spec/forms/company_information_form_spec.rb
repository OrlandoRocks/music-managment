require 'rails_helper'

RSpec.describe CompanyInformationForm, type: :form do
  let!(:person) do
    create(
      :person,
      customer_type: "business",
      password: "Test@123",
      country: "LU"
    )
  end

  before(:each) do
    allow(FeatureFlipper)
      .to receive(:show_feature?)
      .with(:vat_tax, person)
      .and_return(true)
  end

  context 'Validations' do
    it 'create company_information for user with valid attr' do
      form = CompanyInformationForm.new(person)
      params = {
        enterprise_number: "AB12345",
        current_password: "Test@123"
      }
      expect(form.submit(params)).to be true
    end

    it 'fail to create company_information for individal user' do
      person.customer_type = "individual"
      form = CompanyInformationForm.new(person)
      params = {
        enterprise_number: "AB12345",
        current_password: "Test@123"
      }
      expect(form.submit(params)).to be false
    end

    it 'fail to create company_information with indvalid regex' do
      form = CompanyInformationForm.new(person)
      params = {
        enterprise_number: "AB12345<>",
        current_password: "Test@123"
      }
      expect(form.submit(params)).to be false
    end

    it 'fail to create company_information with wrong password' do
      form = CompanyInformationForm.new(person)
      params = {
        enterprise_number: "AB12345",
        current_password: "wrong password"
      }
      expect(form.submit(params)).to be false
    end
  end
end
