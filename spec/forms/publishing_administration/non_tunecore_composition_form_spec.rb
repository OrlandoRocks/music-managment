require "rails_helper"
include Dates

xdescribe PublishingAdministration::NonTunecoreCompositionForm do
  describe ".save" do
    it "is invalid if the person_id does not match a person record in tc-www db" do
      params = {
        id:            "1",
        title:         "Composition Title",
        artist_name:   "Charlie",
        percent:       "95",
        person_id:      0
      }

      expect(PublishingAdministration::NonTunecoreCompositionForm.new(params).valid?).to be_falsey
    end

    it "is invalid if the person sends an invalid release_date" do
      composer = create(:composer)
      composer.person.update(account: composer.account)
      params = {
        artist_name:  "Charlie",
        isrc_number:  "1234567890",
        composer_id:  composer.id,
        percent:      "95",
        person_id:    composer.person.id,
        record_label: "my label",
        release_date: "2019-06-01",
        title:        "Composition Title"
      }

      form = PublishingAdministration::NonTunecoreCompositionForm.new(params)
      form.valid?

      expect(form.errors.messages[:release_date]).to be_present
    end

    it "is invalid if the person doesn't have a composer with their person_id" do
      person = create(:person)
      params = {
        id:          "1",
        title:       "Composition Title",
        artist_name: "Charlie",
        percent:     "95",
        person_id:   person.id
      }

      expect(PublishingAdministration::NonTunecoreCompositionForm.new(params).valid?).to be_falsey
    end

    it "is invalid if the percent submitted is less than 1" do
      person = create(:person)
      params = {
        id:          "1",
        title:       "Composition Title",
        artist_name: "Charlie",
        percent:     ".05",
        person_id:   person.id
      }

      expect(PublishingAdministration::NonTunecoreCompositionForm.new(params).valid?).to be_falsey
    end

    it "is invalid if the percent submitted is greater than 100" do
      person = create(:person)
      params = {
        id:          "1",
        title:       "Composition Title",
        artist_name: "Charlie",
        percent:     "100.1",
        person_id:   person.id
      }

      expect(PublishingAdministration::NonTunecoreCompositionForm.new(params).valid?).to be_falsey
    end

    it "is invalid if the release date is not the correct type" do
      person = create(:person)

      params = {
        id:          "1",
        title:       "Composition Title",
        artist_name: "Charlie",
        percent:     "100",
        person_id:   person.id
      }

      expect(PublishingAdministration::NonTunecoreCompositionForm.new(params).valid?).to be_falsey
    end

    it "is invalid if the ISRC is already in use by an NTC Song associated to the Composer" do
      account   = create(:account)
      composer  = create(:composer, person: account.person, account: account)
      ntc_album = create(:non_tunecore_album, composer: composer)
      ntc_song = create(:non_tunecore_song, non_tunecore_album: ntc_album, isrc: "TUN123456789" )
      ntc_song.composition.update(name: "Composition Name")

      params = {
        id:           "1",
        title:        "Too Many Cooks",
        percent:      "100",
        composer_id:  composer.id,
        person_id:    composer.person_id,
        isrc_number:  "TUN123456789"
      }

      instance = PublishingAdministration::NonTunecoreCompositionForm.new(params)

      instance.valid?

      expect(instance.errors.keys).to include(:isrc_number)
    end

    it "is valid if the ISRC isn't already in use by an NTC Song associated to the Composer and other songs by the composer have NULL ISRC values" do
      account   = create(:account)
      composer  = create(:composer, person: account.person, account: account)
      album     = create(:album, person_id: composer.person_id)
      create(:song, optional_isrc: nil, album: album)

      params = {
        id:           "1",
        title:        "Too Many Cooks",
        percent:      "100",
        composer_id:  composer.id,
        person_id:    composer.person_id,
        isrc_number:  "TUN123456789"
      }

      instance = PublishingAdministration::NonTunecoreCompositionForm.new(params)

      instance.valid?

      expect(instance.errors.keys).to_not include(:isrc_number)
    end

    it "is valid if the ISRC is already in use by one of the Songs associated to the Composer's Person on a deleted album" do
      account   = create(:account)
      composer  = create(:composer, person: account.person, account: account)
      album     = create(:album, :is_deleted, person_id: composer.person_id)
      song      = create(:song, :with_optional_isrc, album: album)

      params = {
        id:           "1",
        title:        "Too Many Cooks",
        percent:      "100",
        composer_id:  composer.id,
        person_id:    composer.person_id,
        isrc_number:  song.optional_isrc
      }

      instance = PublishingAdministration::NonTunecoreCompositionForm.new(params)

      instance.valid?

      expect(instance.errors.keys).to_not include(:isrc_number)
    end

    it "is invalid if the ISRC is already in use by one of the Songs associated to the Composer's Person" do
      account   = create(:account)
      composer  = create(:composer, person: account.person, account: account)
      album     = create(:album, person_id: composer.person_id)
      song      = create(:song, :with_optional_isrc, album: album)

      params = {
        id:           "1",
        title:        "Too Many Cooks",
        percent:      "100",
        composer_id:  composer.id,
        person_id:    composer.person_id,
        isrc_number:  song.optional_isrc
      }

      instance = PublishingAdministration::NonTunecoreCompositionForm.new(params)

      instance.valid?

      expect(instance.errors.keys).to include(:isrc_number)
    end

    context "when a person manages more than one composer" do
      it "selects the composer associated to the person" do
        account  = create(:account)
        composer = create(:composer, person: account.person, account: account)
        create(:composer, account: account)

        params = {
          id:          "1",
          title:       "Composition Title",
          artist_name: "Charlie",
          percent:     "100",
          person_id:   account.person.id
        }

        expect(PublishingAdministration::NonTunecoreCompositionForm.new(params).send(:composer)).to eq composer
      end
    end

    it "creates a composition with the title provided" do
      composer = create(:composer)
      composer.person.update(account: composer.account)
      params = {
        id:          "1",
        title:       "Composition Title",
        artist_name: "Charlie",
        percent:     "100",
        person_id:   composer.person.id,
        composer_id: composer.id
      }

      allow(MailerWorker).to receive(:perform_async).and_return true

      PublishingAdministration::NonTunecoreCompositionForm.new(params).save
      expect(Composition.where(name: params[:title])).to exist
    end

    it "updates the composition status to 'Submitted'" do
      composer = create(:composer)
      composer.person.update(account: composer.account)
      params = {
        id:          "1",
        title:       "Composition Title",
        artist_name: "Charlie",
        percent:     "100",
        person_id:   composer.person.id,
        composer_id: composer.id
      }
      allow(MailerWorker).to receive(:perform_async).and_return true

      PublishingAdministration::NonTunecoreCompositionForm.new(params).save

      composition = composer.compositions.last
      expect(composition.status).to eq "Submitted"
    end

    it "creates a NonTunecoreAlbum" do
      composer = create(:composer)
      composer.person.update(account: composer.account)
      params = {
        id:           "1",
        title:        "Composition Title",
        artist_name:  "Charlie",
        isrc_number:  "1234567890",
        percent:      "95",
        album_name:   "My Album",
        album_upc:    "1234567890",
        record_label: "my label",
        release_date: "10/26/2018",
        person_id:    composer.person.id,
        composer_id:  composer.id
      }

      allow(MailerWorker).to receive(:perform_async).and_return true
      PublishingAdministration::NonTunecoreCompositionForm.new(params).save

      non_tunecore_album = NonTunecoreAlbum.where(
        name:               params[:album_name],
        upc:                params[:album_upc],
        composer_id:        composer.id,
        orig_release_year:  parse_mmddyyyy(params[:release_date]),
        account_id:         composer.account_id
      )

      expect(non_tunecore_album).to exist
    end

    context "when the song does not have an album name" do
      it "creates a NonTunecoreAlbum with the composition title being the album name" do
        composer = create(:composer)
        composer.person.update(account: composer.account)
        params = {
          id:           "1",
          title:        "Composition Title",
          artist_name:  "Charlie",
          isrc_number:  "1234567890",
          percent:      "95",
          record_label: "my label",
          release_date: "10/26/2018",
          person_id:    composer.person.id,
          composer_id:  composer.id
        }

        allow(MailerWorker).to receive(:perform_async).and_return true
        PublishingAdministration::NonTunecoreCompositionForm.new(params).save

        non_tunecore_album = NonTunecoreAlbum.where(
          name: params[:title],
          upc: params[:album_upc],
          composer_id: composer.id,
          orig_release_year: parse_mmddyyyy(params[:release_date])
        )

        expect(non_tunecore_album).to exist
      end
    end

    it "creates a NonTunecoreSong" do
      artist             = create(:artist)
      composer           = create(:composer)
      composition        = create(:composition, name: "Comp Title")
      non_tunecore_album = create(:non_tunecore_album)
      composer.person.update(account: composer.account)
      params = {
        id:           "1",
        title:        composition.name,
        artist_name:  artist.name,
        isrc_number:  "1234567890",
        percent:      "95",
        album_name:   "My Album",
        album_upc:    "1234567890",
        record_label: "my label",
        release_date: "10/26/2018",
        person_id:    composer.person.id,
        composer_id:  composer.id
      }

      allow(Composition).to receive(:create!)       { composition }
      allow(NonTunecoreAlbum).to receive(:create!)  { non_tunecore_album }
      allow(MailerWorker).to receive(:perform_async).and_return true

      PublishingAdministration::NonTunecoreCompositionForm.new(params).save

      non_tunecore_song = NonTunecoreSong.where(
        name: params[:title],
        isrc: params[:isrc_number],
        artist_id: artist,
        composition_id: composition,
        release_date: parse_mmddyyyy(params[:release_date]),
        non_tunecore_album_id: non_tunecore_album
      )

      expect(non_tunecore_song).to exist
    end

    context "#send_to_publishing_administration" do
      it "queues up a sidekiq job via the Work Recording Creation Worker if the composer has a provider_identifier" do
        account   = create(:account)
        composer  = create(:composer, account: account, person: account.person, provider_identifier: "12345")
        params    = {
          id:           "1",
          title:        "Composition Title",
          artist_name:  "Sentric",
          isrc_number:  "1234567890",
          percent:      "95",
          album_name:   "My Album",
          album_upc:    "1234567890",
          record_label: "my label",
          release_date: "10/26/2018",
          person_id:    composer.person.id,
          composer_id:  composer.id
        }

        expect(PublishingAdministration::WorkRecordingCreationWorker).to receive(:perform_async)
        PublishingAdministration::NonTunecoreCompositionForm.new(params).save
      end

      it "does not queue up a sidekiq job if the composer does not have a provider_identifier" do
        composer  = create(:composer)
        params    = {
          id:           "1",
          title:        "Composition Title",
          artist_name:  "Sentric",
          isrc_number:  "1234567890",
          percent:      "95",
          album_name:   "My Album",
          album_upc:    "1234567890",
          record_label: "my label",
          release_date: "10/26/2018",
          person_id:    composer.person.id
        }

        expect(PublishingAdministration::WorkRecordingCreationWorker).not_to receive(:perform_async)
        PublishingAdministration::NonTunecoreCompositionForm.new(params).save
      end
    end

    context "when the composer split percentage does not equal 100 percent" do
      it "creates a composer publishing split" do
        composer    = create(:composer)
        composition = create(:composition, name: "Comp Title")
        composer.person.update(account: composer.account)
        params = {
          id:           "1",
          title:        composition.name,
          artist_name:  "Dj Ango",
          isrc_number:  "1234567890",
          percent:      "75",
          album_name:   "My Album",
          album_upc:    "1234567890",
          record_label: "my label",
          release_date: "10/26/2018",
          person_id:    composer.person.id,
          composer_id:  composer.id
        }

        allow(Composition).to receive(:create!) { composition }
        allow(MailerWorker).to receive(:perform_async).and_return true

        PublishingAdministration::NonTunecoreCompositionForm.new(params).save

        composer_publishing_split = composition.publishing_splits.where(
          percent: params[:percent],
          composer_id: composer.id,
          writer_id: composer.id
        )

        expect(composer_publishing_split).to exist
      end

      it "creates an unknown cowriter and a publishing split with the remaining split percentage if remaining percent is > 1" do
        person      = create(:person)
        account     = create(:account, person: person)
        composer    = create(:composer, account: account)
        composition = create(:composition, name: "Comp Title")
        composer.person.update(account: composer.account)
        params = {
          id:           "1",
          title:        composition.name,
          artist_name:  "Dj Ango",
          isrc_number:  "1234567890",
          percent:      "75",
          album_name:   "My Album",
          album_upc:    "1234567890",
          record_label: "my label",
          release_date: "10/26/2018",
          person_id:    composer.person.id,
          composer_id:  composer.id
        }

        allow(Composition).to receive(:create!) { composition }
        allow(MailerWorker).to receive(:perform_async).and_return true

        expect(composer.cowriters.where(is_unknown: true)).to be_empty

        PublishingAdministration::NonTunecoreCompositionForm.new(params).save

        cowriter_publishing_split = composition.publishing_splits.where(
          composer_id: composer.id,
          percent: 25,
          writer_type: "Cowriter",
          composition_id: composition
        )

        expect(cowriter_publishing_split).to exist
        expect(composer.cowriters.where(is_unknown: true)).to exist
      end

      it "gives the remaining share to the composer if the left over percentage is less than 1" do
        composer    = create(:composer)
        composition = create(:composition, name: "Comp Title")
        composer.person.update(account: composer.account)
        params = {
          id:               "1",
          title:            composition.name,
          artist_name:      "Dj Ango",
          isrc_number:      "1234567890",
          percent:          "99.1",
          album_name:       "My Album",
          album_upc:        "1234567890",
          record_label:     "my label",
          release_date:     "10/26/2018",
          person_id:        composer.person.id,
          composer_id:      composer.id,
        }

        allow(Composition).to receive(:create!)       { composition }
        allow(MailerWorker).to receive(:perform_async).and_return true

        form = PublishingAdministration::NonTunecoreCompositionForm.new(params)
        form.save

        composer_publishing_split = composition.publishing_splits.where(
          composer_id: composer.id,
          writer_id: composer.id
        )

        expect(composer_publishing_split.first.percent).to eq 100
      end

      it "doesn't give the remaining share to the composer if their share is 99%" do
        composer    = create(:composer)
        composition = create(:composition, name: "Comp Title")
        composer.person.update(account: composer.account)
        params = {
          id:               "1",
          title:            composition.name,
          artist_name:      "Dj Ango",
          isrc_number:      "1234567890",
          percent:          "99",
          album_name:       "My Album",
          album_upc:        "1234567890",
          record_label:     "my label",
          release_date:     "10/26/2018",
          person_id:        composer.person.id,
          composer_id:      composer.id,
        }

        allow(Composition).to receive(:create!)       { composition }
        allow(MailerWorker).to receive(:perform_async).and_return true

        form = PublishingAdministration::NonTunecoreCompositionForm.new(params)
        form.save

        composer_publishing_split = composition.publishing_splits.where(
          composer_id: composer.id,
          writer_id: composer.id
        )

        expect(composer_publishing_split.first.percent).to eq 99.0
      end
    end

    context "when the composition has cowriters" do
      it "creates a cowriter" do
        composer    = create(:composer)
        composition = create(:composition, name: "Comp Title")
        composer.person.update(account: composer.account)
        params = {
          id:               "1",
          title:            composition.name,
          artist_name:      "Dj Ango",
          isrc_number:      "1234567890",
          percent:          "75",
          album_name:       "My Album",
          album_upc:        "1234567890",
          record_label:     "my label",
          release_date:     "10/26/2018",
          person_id:        composer.person.id,
          composer_id:      composer.id,
          cowriter_params:  [{ first_name: "Tacko  ", last_name: "Fall", cowriter_share: '25' }]
        }

        allow(Composition).to receive(:create!) { composition }
        allow(MailerWorker).to receive(:perform_async).and_return true

        PublishingAdministration::NonTunecoreCompositionForm.new(params).save

        cowriter_publishing_split = composition.publishing_splits.where(
          composer_id: composer.id,
          percent: 25,
          writer_type: "Cowriter",
          composition_id: composition
        )

        expect(cowriter_publishing_split).to exist
        expect(cowriter_publishing_split.first.writer.first_name).to eq "Tacko"
      end

      it "cowriter name with four byte chars fails validation" do
        composer    = create(:composer)
        composition = create(:composition, name: "Comp Title")
        composer.person.update(account: composer.account)
        params = {
          id:               "1",
          title:            composition.name,
          artist_name:      "Dj Ango",
          isrc_number:      "1234567890",
          percent:          "75",
          album_name:       "My Album",
          album_upc:        "1234567890",
          record_label:     "my label",
          release_date:     "10/26/2018",
          person_id:        composer.person.id,
          composer_id:      composer.id,
          cowriter_params:  [{ first_name: "💿💿💿💿💿", last_name: "💿💿💿💿💿", cowriter_share: '25' }]
        }

        allow(Composition).to receive(:create!) { composition }
        allow(MailerWorker).to receive(:perform_async).and_return true

        form = PublishingAdministration::NonTunecoreCompositionForm.new(params)
        form.save
        expect(form.errors.messages[:cowriterErrors][0].size).to be 2
      end
    end
  end

  describe "#response_body" do
    it "returns the correct response" do
      composer    = create(:composer)
      composition = create(:composition, name: "Comp Title")
      composer.person.update(account: composer.account)
      params = {
        id:               "1",
        title:            composition.name,
        artist_name:      "Dj Ango",
        isrc_number:      "1234567890",
        percent:          "75",
        album_name:       "My Album",
        album_upc:        "1234567890",
        record_label:     "my label",
        release_date:     "10/26/2018",
        person_id:        composer.person.id,
        composer_id:      composer.id,
        cowriter_params:  [{ first_name: "Tacko", last_name: "Fall", cowriter_share: "20" }]
      }

      allow(Composition).to receive(:create!) { composition }
      allow(MailerWorker).to receive(:perform_async).and_return true

      non_tunecore_composition_form = PublishingAdministration::NonTunecoreCompositionForm.new(params)
      non_tunecore_composition_form.save

      album_name = non_tunecore_composition_form.send(:non_tunecore_album).name

      expect(non_tunecore_composition_form.response_body[:id]).to eq composition.id
      expect(non_tunecore_composition_form.response_body[:appears_on]).to eq album_name
      expect(non_tunecore_composition_form.response_body[:composition_title]).to eq composition.name
      expect(non_tunecore_composition_form.response_body[:composer_share].to_i).to eq 75
      expect(non_tunecore_composition_form.response_body[:cowriter_share].to_i).to eq 25
      expect(non_tunecore_composition_form.response_body[:unknown_split_percent].to_i).to eq 5
      expect(non_tunecore_composition_form.response_body[:is_ntc_song]).to be_truthy
    end
  end

  describe "#duplicate_title" do
    context "when the ntc composition has a unique title" do
      it "should be valid" do
        account = create(:account)
        composer    = create(:composer, account: account, person: account.person)

        params = {
          id:           "1",
          title:        "Too Many Cooks",
          artist_name:  "Dj Ango",
          percent:      "100",
          composer_id:  composer.id,
          person_id:    composer.person_id
        }

        form = PublishingAdministration::NonTunecoreCompositionForm.new(params)
        form.valid?

        expect(form.errors.keys).not_to include :title
      end
    end

    context "when the ntc composition has a unique title" do
      it "should be invalid" do
        composition_title = "Composition Title"
        account = create(:account)
        composer    = create(:composer, account: account, person: account.person)

        ntc_album = create(:non_tunecore_album, composer: composer)
        ntc_song = create(:non_tunecore_song, non_tunecore_album: ntc_album)
        ntc_song.composition.update(name: composition_title)

        params = {
          id:           "1",
          title:        composition_title,
          artist_name:  "Dj Ango",
          percent:      "100",
          composer_id:  composer.id,
          person_id:    composer.person_id
        }

        form = PublishingAdministration::NonTunecoreCompositionForm.new(params)
        form.save

        expect(form.errors.keys).to include :title
      end
    end
  end
end
