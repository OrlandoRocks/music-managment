require "rails_helper"
include Dates

describe PublishingAdministration::NonTunecorePublishingCompositionForm do
  describe ".save" do
    it "is invalid if the person_id does not match a person record in tc-www db" do
      params = {
        id:            "1",
        title:         "Composition Title",
        artist_name:   "Charlie",
        percent:       "95",
        person_id:      0
      }

      expect(PublishingAdministration::NonTunecorePublishingCompositionForm.new(params).valid?).to be_falsey
    end

    it "is invalid if the person sends an invalid release_date" do
      publishing_composer = create(:publishing_composer, is_primary_composer: true)
      publishing_composer.person.update(account: publishing_composer.account)
      params = {
        artist_name:  "Charlie",
        isrc_number:  "1234567890",
        composer_id:  publishing_composer.id,
        percent:      "95",
        person_id:    publishing_composer.person.id,
        record_label: "my label",
        release_date: "2019-06-01",
        title:        "Composition Title"
      }

      form = PublishingAdministration::NonTunecorePublishingCompositionForm.new(params)
      form.valid?

      expect(form.errors.messages[:release_date]).to be_present
    end

    it "is invalid if the person doesn't have a publishing_composer with their person_id" do
      person = create(:person)
      params = {
        id:          "1",
        title:       "Composition Title",
        artist_name: "Charlie",
        percent:     "95",
        person_id:   person.id
      }

      expect(PublishingAdministration::NonTunecorePublishingCompositionForm.new(params).valid?).to be_falsey
    end

    it "is invalid if the percent submitted is less than 1" do
      person = create(:person)
      params = {
        id:          "1",
        title:       "Composition Title",
        artist_name: "Charlie",
        percent:     ".05",
        person_id:   person.id
      }

      expect(PublishingAdministration::NonTunecorePublishingCompositionForm.new(params).valid?).to be_falsey
    end

    it "is invalid if the percent submitted is greater than 100" do
      person = create(:person)
      params = {
        id:          "1",
        title:       "Composition Title",
        artist_name: "Charlie",
        percent:     "100.1",
        person_id:   person.id
      }

      expect(PublishingAdministration::NonTunecorePublishingCompositionForm.new(params).valid?).to be_falsey
    end

    it "is invalid if the release date is not the correct type" do
      person = create(:person)

      params = {
        id:          "1",
        title:       "Composition Title",
        artist_name: "Charlie",
        percent:     "100",
        person_id:   person.id
      }

      expect(PublishingAdministration::NonTunecorePublishingCompositionForm.new(params).valid?).to be_falsey
    end

    it "is invalid if the public domain flag isn't boolean" do
      person = create(:person)

      params = {
        id:          "1",
        title:       "Composition Title",
        artist_name: "Charlie",
        percent:     "100",
        person_id:   person.id,
        public_domain: "bad value"
      }

      expect(PublishingAdministration::NonTunecorePublishingCompositionForm.new(params).valid?).to be_falsey
    end

    it "is invalid if the ISRC is already in use by an NTC Song associated to the Composer" do
      account   = create(:account)
      publishing_composer  = create(:publishing_composer, person: account.person, account: account, is_primary_composer: true)
      ntc_album = create(:non_tunecore_album, publishing_composer: publishing_composer)
      ntc_song = create(:non_tunecore_song, non_tunecore_album: ntc_album, isrc: "TUN123456789" )
      ntc_song.publishing_composition.update(name: "Composition Name")

      params = {
        id:           "1",
        title:        "Too Many Cooks",
        percent:      "100",
        composer_id:  publishing_composer.id,
        person_id:    publishing_composer.person_id,
        isrc_number:  "TUN123456789"
      }

      instance = PublishingAdministration::NonTunecorePublishingCompositionForm.new(params)

      instance.valid?

      expect(instance.errors.keys).to include(:isrc_number)
    end

    it "is valid if the ISRC isn't already in use by an NTC Song associated to the Composer and other songs by the publishing_composer have NULL ISRC values" do
      account   = create(:account)
      publishing_composer  = create(:publishing_composer, person: account.person, account: account, is_primary_composer: true)
      album     = create(:album, person_id: publishing_composer.person_id)
      create(:song, optional_isrc: nil, album: album)

      params = {
        id:           "1",
        title:        "Too Many Cooks",
        percent:      "100",
        composer_id:  publishing_composer.id,
        person_id:    publishing_composer.person_id,
        isrc_number:  "TUN123456789"
      }

      instance = PublishingAdministration::NonTunecorePublishingCompositionForm.new(params)

      instance.valid?

      expect(instance.errors.keys).to_not include(:isrc_number)
    end

    it "is valid if the ISRC is already in use by one of the Songs associated to the Composer's Person on a deleted album" do
      account   = create(:account)
      publishing_composer = create(:publishing_composer, person: account.person, account: account, is_primary_composer: true)
      album     = create(:album, :is_deleted, person_id: publishing_composer.person_id)
      song      = create(:song, :with_optional_isrc, album: album)

      params = {
        id:           "1",
        title:        "Too Many Cooks",
        percent:      "100",
        composer_id:  publishing_composer.id,
        person_id:    publishing_composer.person_id,
        isrc_number:  song.optional_isrc
      }

      instance = PublishingAdministration::NonTunecorePublishingCompositionForm.new(params)

      instance.valid?

      expect(instance.errors.keys).to_not include(:isrc_number)
    end

    it "is invalid if the ISRC is already in use by one of the Songs associated to the Composer's Person" do
      account   = create(:account)
      publishing_composer  = create(:publishing_composer, person: account.person, account: account, is_primary_composer: true)
      album     = create(:album, person_id: publishing_composer.person_id)
      song      = create(:song, :with_optional_isrc, album: album)

      params = {
        id:           "1",
        title:        "Too Many Cooks",
        percent:      "100",
        composer_id:  publishing_composer.id,
        person_id:    publishing_composer.person_id,
        isrc_number:  song.optional_isrc
      }

      instance = PublishingAdministration::NonTunecorePublishingCompositionForm.new(params)

      instance.valid?

      expect(instance.errors.keys).to include(:isrc_number)
    end

    it "is valid when public domain is set to true" do
      account   = create(:account)
      publishing_composer  = create(:publishing_composer, person: account.person, account: account, is_primary_composer: true)
      album     = create(:album, person_id: publishing_composer.person_id)
      song      = create(:song, :with_optional_isrc, album: album)

      params = {
        id:           "1",
        title:        "Too Many Cooks",
        percent:      "100",
        composer_id:  publishing_composer.id,
        person_id:    publishing_composer.person_id,
        isrc_number:  song.optional_isrc,
        public_domain: "true"
      }

      instance = PublishingAdministration::NonTunecorePublishingCompositionForm.new(params)

      instance.valid?

      expect(instance.errors.keys).to_not include(:public_domain)
    end

    context "when a person manages more than one publishing_composer" do
      it "selects the publishing_composer associated to the person" do
        account  = create(:account)
        publishing_composer = create(:publishing_composer, person: account.person, account: account, is_primary_composer: true)
        create(:publishing_composer, account: account)

        params = {
          id:          "1",
          title:       "Composition Title",
          artist_name: "Charlie",
          percent:     "100",
          person_id:   account.person.id
        }

        expect(PublishingAdministration::NonTunecorePublishingCompositionForm.new(params).send(:primary_composer)).to eq publishing_composer
      end
    end

    it "creates a composition with the title provided" do
      publishing_composer = create(:publishing_composer, is_primary_composer: true)
      publishing_composer.person.update(account: publishing_composer.account)
      params = {
        id:          "1",
        title:       "Composition Title",
        artist_name: "Charlie",
        percent:     "100",
        person_id:   publishing_composer.person.id,
        composer_id: publishing_composer.id
      }

      allow(MailerWorker).to receive(:perform_async).and_return true

      PublishingAdministration::NonTunecorePublishingCompositionForm.new(params).save
      expect(PublishingComposition.where(name: params[:title])).to exist
    end

    it "updates the composition status to 'Submitted'" do
      publishing_composer = create(:publishing_composer, is_primary_composer: true, is_primary_composer: true)
      publishing_composer.person.update(account: publishing_composer.account)
      params = {
        id:          "1",
        title:       "Composition Title",
        artist_name: "Charlie",
        percent:     "100",
        person_id:   publishing_composer.person.id,
        composer_id: publishing_composer.id
      }
      allow(MailerWorker).to receive(:perform_async).and_return true

      PublishingAdministration::NonTunecorePublishingCompositionForm.new(params).save

      publishing_composition = publishing_composer.publishing_compositions.last
      expect(publishing_composition.status).to eq "Submitted"
    end

    it "creates a NonTunecoreAlbum" do
      publishing_composer = create(:publishing_composer, is_primary_composer: true)
      publishing_composer.person.update(account: publishing_composer.account)
      params = {
        id:           "1",
        title:        "Composition Title",
        artist_name:  "Charlie",
        isrc_number:  "1234567890",
        percent:      "95",
        album_name:   "My Album",
        record_label: "Record Label",
        album_upc:    "1234567890",
        record_label: "my label",
        release_date: "10/26/2018",
        person_id:    publishing_composer.person.id,
        composer_id:  publishing_composer.id
      }

      allow(MailerWorker).to receive(:perform_async).and_return true
      PublishingAdministration::NonTunecorePublishingCompositionForm.new(params).save

      non_tunecore_album = NonTunecoreAlbum.where(
        name:               params[:album_name],
        record_label:       params[:record_label],
        upc:                params[:album_upc],
        publishing_composer_id:        publishing_composer.id,
        orig_release_year:  parse_mmddyyyy(params[:release_date])
      )

      expect(non_tunecore_album).to exist
    end

    context "when the song does not have an album name" do
      it "creates a NonTunecoreAlbum with the composition title being the album name" do
        primary_composer = create(:publishing_composer, is_primary_composer: true)
        primary_composer.person.update(account: primary_composer.account)
        params = {
          id:           "1",
          title:        "Composition Title",
          artist_name:  "Charlie",
          isrc_number:  "1234567890",
          percent:      "95",
          record_label: "my label",
          release_date: "10/26/2018",
          person_id:    primary_composer.person.id,
          composer_id:  primary_composer.id
        }

        allow(MailerWorker).to receive(:perform_async).and_return true
        PublishingAdministration::NonTunecorePublishingCompositionForm.new(params).save

        non_tunecore_album = NonTunecoreAlbum.where(
          name: params[:title],
          upc: params[:album_upc],
          publishing_composer_id: primary_composer.id,
          orig_release_year: parse_mmddyyyy(params[:release_date])
        )

        expect(non_tunecore_album).to exist
      end
    end

    it "creates a NonTunecoreSong" do
      artist             = create(:artist)
      account = create(:account)
      publishing_composer           = create(:publishing_composer, is_primary_composer: true)
      non_tunecore_album = create(:non_tunecore_album)
      publishing_composer.person.update(account: publishing_composer.account)
      params = {
        id:           "1",
        title:        "Comp Title",
        artist_name:  artist.name,
        isrc_number:  "1234567890",
        percent:      "95",
        album_name:   "My Album",
        record_label: "Record Label",
        album_upc:    "1234567890",
        record_label: "my label",
        release_date: "10/26/2018",
        person_id:    publishing_composer.person.id,
        composer_id:  publishing_composer.id
      }

      allow(NonTunecoreAlbum).to receive(:create!)  { non_tunecore_album }
      allow(MailerWorker).to receive(:perform_async).and_return true

      form = PublishingAdministration::NonTunecorePublishingCompositionForm.new(params)
      form.save

      publishing_composition = PublishingComposition.find(form.response_body[:id])

      non_tunecore_song = NonTunecoreSong.where(
        name: params[:title],
        isrc: params[:isrc_number],
        artist_id: artist,
        publishing_composition_id: publishing_composition,
        release_date: parse_mmddyyyy(params[:release_date]),
        non_tunecore_album_id: non_tunecore_album
      )

      expect(non_tunecore_song).to exist
    end

    context "#send_to_publishing_administration" do
      it "queues up a sidekiq job via the Work Recording Creation Worker if the publishing_composer has a provider_identifier" do
        account   = create(:account)
        publishing_composer  = create(:publishing_composer, account: account, person: account.person, provider_identifier: "12345", is_primary_composer: true)
        params    = {
          id:           "1",
          title:        "Composition Title",
          artist_name:  "Sentric",
          isrc_number:  "1234567890",
          percent:      "100",
          album_name:   "My Album",
          record_label: "Record Label",
          album_upc:    "1234567890",
          record_label: "my label",
          release_date: "10/26/2018",
          person_id:    publishing_composer.person.id,
          composer_id:  publishing_composer.id
        }

        expect(PublishingAdministration::PublishingWorkRecordingCreationWorker).to receive(:perform_async)
        PublishingAdministration::NonTunecorePublishingCompositionForm.new(params).save
      end

      it "does not queue a worker because there is unknown splits" do
        account   = create(:account)
        publishing_composer  = create(:publishing_composer, account: account, person: account.person, provider_identifier: "12345", is_primary_composer: true)
        params    = {
          id:           "1",
          title:        "Composition Title",
          artist_name:  "Sentric",
          isrc_number:  "1234567890",
          percent:      "95",
          album_name:   "My Album",
          record_label: "Record Label",
          album_upc:    "1234567890",
          record_label: "my label",
          release_date: "10/26/2018",
          person_id:    publishing_composer.person.id,
          composer_id:  publishing_composer.id
        }

        expect(PublishingAdministration::PublishingWorkRecordingCreationWorker).to_not receive(:perform_async)
        PublishingAdministration::NonTunecorePublishingCompositionForm.new(params).save
      end

      it "does not queue up a sidekiq job if the publishing_composer does not have a provider_identifier" do
        person = create(:person)
        account = create(:account, person: person)
        publishing_composer  = create(:publishing_composer, account: account, is_primary_composer: true)
        params    = {
          id:           "1",
          title:        "Composition Title",
          artist_name:  "Sentric",
          isrc_number:  "1234567890",
          percent:      "100",
          album_name:   "My Album",
          record_label: "Record Label",
          album_upc:    "1234567890",
          record_label: "my label",
          release_date: "10/26/2018",
          person_id:    person.id
        }

        expect(PublishingAdministration::PublishingWorkRecordingCreationWorker).not_to receive(:perform_async)
        PublishingAdministration::NonTunecorePublishingCompositionForm.new(params).save
      end
    end

    context "when the publishing_composer split percentage does not equal 100 percent" do
      it "creates a publishing_composer publishing split" do
        account = create(:account)
        primary_composer    = create(:publishing_composer, is_primary_composer: true, account: account)
        primary_composer.person.update(account: primary_composer.account)
        params = {
          id:           "1",
          title:        "Comp Title",
          artist_name:  "Dj Ango",
          isrc_number:  "1234567890",
          percent:      "75",
          album_name:   "My Album",
          record_label: "Record Label",
          album_upc:    "1234567890",
          record_label: "my label",
          release_date: "10/26/2018",
          person_id:    primary_composer.person.id,
          composer_id:  primary_composer.id
        }

        allow(MailerWorker).to receive(:perform_async).and_return true

        form = PublishingAdministration::NonTunecorePublishingCompositionForm.new(params)
        form.save

        publishing_composition = PublishingComposition.find(form.response_body[:id])

        primary_composer_split = publishing_composition.publishing_composition_splits.where(
          percent: params[:percent],
          publishing_composer_id: primary_composer.id,
        )

        expect(primary_composer_split).to exist
      end

      it "Does not create an unknown cowriter and a publishing split with the remaining split percentage if remaining percent is > 1" do
        person      = create(:person)
        account     = create(:account, person: person)
        primary_composer    = create(:publishing_composer, account: account, is_primary_composer: true)
        primary_composer.person.update(account: primary_composer.account)
        params = {
          id:           "1",
          title:        "Comp Title",
          artist_name:  "Dj Ango",
          isrc_number:  "1234567890",
          percent:      "75",
          album_name:   "My Album",
          record_label: "Record Label",
          album_upc:    "1234567890",
          record_label: "my label",
          release_date: "10/26/2018",
          person_id:    primary_composer.person.id,
          composer_id:  primary_composer.id
        }

        allow(MailerWorker).to receive(:perform_async).and_return true

        expect(PublishingComposer.where(is_unknown: true, account: account)).to be_empty

        form = PublishingAdministration::NonTunecorePublishingCompositionForm.new(params)
        form.save

        publishing_composition = PublishingComposition.find(form.response_body[:id])

        cowriter_publishing_split = publishing_composition.publishing_composition_splits.where(
          percent: 25,
          right_to_collect: false,
          publishing_composition_id: publishing_composition.id
        )

        expect(publishing_composition.publishing_composers.where(is_unknown: true)).to_not exist
      end

      it "gives the remaining share to the publishing_composer if the left over percentage is less than 1" do
        account = create(:account)
        publishing_composer    = create(:publishing_composer, is_primary_composer: true, account: account)
        publishing_composer.person.update(account: publishing_composer.account)
        params = {
          id:               "1",
          title:            "Comp Title",
          artist_name:      "Dj Ango",
          isrc_number:      "1234567890",
          percent:          "99.1",
          album_name:       "My Album",
          record_label:     "Record Label",
          album_upc:        "1234567890",
          record_label:     "my label",
          release_date:     "10/26/2018",
          person_id:        publishing_composer.person.id,
          composer_id:      publishing_composer.id,
        }

        allow(MailerWorker).to receive(:perform_async).and_return true

        form = PublishingAdministration::NonTunecorePublishingCompositionForm.new(params)
        form.save

        publishing_composition = PublishingComposition.find(form.response_body[:id])

        publishing_composer_publishing_split = publishing_composition.publishing_composition_splits.where(
          publishing_composer_id: publishing_composer.id
        )

        expect(publishing_composer_publishing_split.first.percent).to eq 99.1.to_d
      end

      it "doesn't give the remaining share to the publishing_composer if their share is 99%" do
        account = create(:account)
        publishing_composer    = create(:publishing_composer, is_primary_composer: true, account: account)
        publishing_composer.person.update(account: publishing_composer.account)
        params = {
          id:               "1",
          title:            "Comp Title",
          artist_name:      "Dj Ango",
          isrc_number:      "1234567890",
          percent:          "99",
          album_name:       "My Album",
          record_label:     "Record Label",
          album_upc:        "1234567890",
          record_label:     "my label",
          release_date:     "10/26/2018",
          person_id:        publishing_composer.person.id,
          composer_id:      publishing_composer.id,
        }

        allow(MailerWorker).to receive(:perform_async).and_return true

        form = PublishingAdministration::NonTunecorePublishingCompositionForm.new(params)
        form.save

        publishing_composition = PublishingComposition.find(form.response_body[:id])

        publishing_composer_publishing_split = publishing_composition.publishing_composition_splits.where(
          publishing_composer_id: publishing_composer.id,
        )

        expect(publishing_composer_publishing_split.first.percent).to eq 99.0
      end
    end

    context "when the composition has cowriters" do
      it "creates a cowriter" do
        account = create(:account)
        primary_composer    = create(:publishing_composer, is_primary_composer: true, account: account)
        primary_composer.person.update(account: primary_composer.account)
        params = {
          id:               "1",
          title:            "Comp Title",
          artist_name:      "Dj Ango",
          isrc_number:      "1234567890",
          percent:          "75",
          album_name:       "My Album",
          record_label:     "Record Label",
          album_upc:        "1234567890",
          record_label:     "my label",
          release_date:     "10/26/2018",
          person_id:        primary_composer.person.id,
          composer_id:      primary_composer.id,
          cowriter_params:  [{ first_name: "Tacko  ", last_name: "Fall", cowriter_share: '25' }]
        }

        allow(MailerWorker).to receive(:perform_async).and_return true

        form = PublishingAdministration::NonTunecorePublishingCompositionForm.new(params)
        form.save

        publishing_composition = PublishingComposition.find(form.response_body[:id])

        cowriter_publishing_split = publishing_composition.publishing_composition_splits.where(
          percent: 25,
          right_to_collect: false,
          publishing_composition_id: publishing_composition
        )

        expect(cowriter_publishing_split).to exist
        expect(cowriter_publishing_split.first.publishing_composer.first_name).to eq "Tacko"
      end
    end
  end

  describe "#response_body" do
    it "returns the correct response" do
      primary_composer    = create(:publishing_composer, is_primary_composer: true)
      primary_composer.person.update(account: primary_composer.account)
      params = {
        id:               "1",
        title:            "Comp Title",
        artist_name:      "Dj Ango",
        isrc_number:      "1234567890",
        percent:          "75",
        album_name:       "My Album",
        record_label:     "Record Label",
        album_upc:        "1234567890",
        record_label:     "my label",
        release_date:     "10/26/2018",
        person_id:        primary_composer.person.id,
        composer_id:      primary_composer.id,
        cowriter_params:  [{ first_name: "Tacko", last_name: "Fall", cowriter_share: "20" }]
      }

      allow(MailerWorker).to receive(:perform_async).and_return true

      non_tunecore_publishing_composition_form = PublishingAdministration::NonTunecorePublishingCompositionForm.new(params)
      non_tunecore_publishing_composition_form.save

      album_name = non_tunecore_publishing_composition_form.send(:non_tunecore_album).name
      record_label = non_tunecore_publishing_composition_form.send(:non_tunecore_album).record_label


      expect(non_tunecore_publishing_composition_form.response_body[:appears_on]).to eq album_name
      expect(non_tunecore_publishing_composition_form.response_body[:record_label]).to eq record_label
      expect(non_tunecore_publishing_composition_form.response_body[:composition_title]).to eq "Comp Title"
      expect(non_tunecore_publishing_composition_form.response_body[:composer_share].to_i).to eq 75
      expect(non_tunecore_publishing_composition_form.response_body[:cowriter_share].to_i).to eq 20
      expect(non_tunecore_publishing_composition_form.response_body[:is_ntc_song]).to be_truthy
    end
  end

  describe "#duplicate_title" do
    context "when the ntc composition has a unique title" do
      it "should be valid" do
        account = create(:account)
        publishing_composer    = create(:publishing_composer, account: account, person: account.person, is_primary_composer: true)

        params = {
          id:           "1",
          title:        "Too Many Cooks",
          artist_name:  "Dj Ango",
          percent:      "100",
          composer_id:  publishing_composer.id,
          person_id:    publishing_composer.person_id
        }

        form = PublishingAdministration::NonTunecorePublishingCompositionForm.new(params)
        form.valid?

        expect(form.errors.keys).not_to include :title
      end
    end

    context "when the ntc composition does not have a unique title" do
      it "should be invalid" do
        composition_title = "Composition Title"
        account = create(:account)
        publishing_composer    = create(:publishing_composer, account_id: account.id, person: account.person, is_primary_composer: true)

        ntc_album = create(:non_tunecore_album, publishing_composer: publishing_composer)
        ntc_song = create(:non_tunecore_song, non_tunecore_album: ntc_album)
        ntc_song.publishing_composition.update(name: composition_title, account_id: account.id)

        params = {
          id:           "1",
          title:        composition_title,
          artist_name:  "Dj Ango",
          percent:      "100",
          composer_id:  publishing_composer.id,
          person_id:    publishing_composer.person_id
        }

        form = PublishingAdministration::NonTunecorePublishingCompositionForm.new(params)
        form.save

        expect(form.errors.keys).to include :title
      end
    end
  end
end
