require "rails_helper"

describe PublishingAdministration::MultiTenantPublishingCompositionSplitsForm do
  describe "#save" do
    let(:person)      { create(:person) }
    let!(:account)    { create(:account, person: person) }
    let(:publishing_composition) { create(:publishing_composition, account: account) }
    let(:publishing_composer)    { create(:publishing_composer, account: account, person: account.person) }
    let(:publishing_composer2)   { create(:publishing_composer, account: account, person: account.person) }

    context "validations" do
      it "returns false if the person creating the split does not match the publishing_composers publishing administrator" do
        publishing_composition_splits_params = {
          publishing_composer: publishing_composer,
          composition: publishing_composition,
          person: create(:person)
        }

        form = PublishingAdministration::MultiTenantPublishingCompositionSplitsForm.new(publishing_composition_splits_params)
        expect(form.valid?).to eq(false)
        expect(form.errors.full_messages).to include("Publishing split user does not have access")
      end

      it "returns true if the person creating the split does match the publishing_composers publishing administrator" do
        publishing_composition_splits_params = {
          composition: publishing_composition,
          person: person,
          composer_params: [{id: publishing_composer.id, composer_share: 50}]
        }

        form = PublishingAdministration::MultiTenantPublishingCompositionSplitsForm.new(publishing_composition_splits_params)
        expect(form.valid?).to eq(true)
        expect(form.errors.full_messages).to be_empty
      end

      it "returns false if the total shares exceed 100 percent" do
        publishing_composition_splits_params = {
          composition: publishing_composition.reload,
          person: person,
          composer_params: [{id: publishing_composer.id, composer_share: "50"}],
          cowriter_params: [{ first_name: "Sam", last_name: "Darnold", cowriter_share: "100" }]
        }

        form = PublishingAdministration::MultiTenantPublishingCompositionSplitsForm.new(publishing_composition_splits_params)
        expect(form.valid?).to eq(false)
        expect(form.errors.full_messages.present?).to be true
      end

      it "returns true if the total shares are less than or equal to 100 percent" do
        publishing_composition_splits_params = {
          composition: publishing_composition,
          person: person,
          composer_params: [{id: publishing_composer.id, composer_share: "50"}],
        }

        form = PublishingAdministration::MultiTenantPublishingCompositionSplitsForm.new(publishing_composition_splits_params)
        expect(form.valid?).to eq(true)
        expect(form.errors.full_messages).to be_empty
      end

      it "returns false if the cowriter is missing a required field" do
        publishing_composition_splits_params = {
          composition: publishing_composition,
          person: person,
          composer_params: [{id: publishing_composer.id, composer_share: "50"}],
          cowriter_params: [{ first_name: "", last_name: "", cowriter_share: "5" }]
        }

        form = PublishingAdministration::MultiTenantPublishingCompositionSplitsForm.new(publishing_composition_splits_params)
        expect(form.valid?).to eq(false)
        expect(form.errors.full_messages).to include("Publishing split has invalid cowriter values")
      end

      it "returns true if the cowriter params are all present" do
        publishing_composition_splits_params = {
          composition: publishing_composition,
          person: person,
          composer_params: [{id: publishing_composer.id, composer_share: "50"}],
          cowriter_params: [{ first_name: "Tom", last_name: "Savage", cowriter_share: "5" }]
        }

        form = PublishingAdministration::MultiTenantPublishingCompositionSplitsForm.new(publishing_composition_splits_params)
        expect(form.valid?).to eq(true)
        expect(form.errors.full_messages).to be_empty
      end

      it "returns true if the cowriters share is between the desired ranged" do
        publishing_composition_splits_params = {
          composition: publishing_composition,
          person: person,
          composer_params: [{id: publishing_composer.id, composer_share: "50"}],
          cowriter_params: [{ first_name: "Tom", last_name: "Savage", cowriter_share: "1" }]
        }

        form = PublishingAdministration::MultiTenantPublishingCompositionSplitsForm.new(publishing_composition_splits_params)
        expect(form.valid?).to eq(true)
        expect(form.errors.full_messages).to be_empty
      end
    end

    context "when cowriter params are present" do
      it "creates a new cowriter if it doesn't already exist" do
        publishing_composition_splits_params = {
          composition: publishing_composition,
          person: person,
          composer_params: [{id: publishing_composer.id, composer_share: "75"}],
          cowriter_params: [{ first_name: "Jamaal", last_name: "Adams", cowriter_share: "25" }]
        }

        expect do
          PublishingAdministration::MultiTenantPublishingCompositionSplitsForm.new(publishing_composition_splits_params).save
        end.to change { account.publishing_composers.cowriting.count }.by(1)
      end

      it "finds the cowriter if it already exists and does not create a new one" do
        create(:publishing_composer, :cowriter, first_name: "Jamaal", last_name: "Adams", account: account, person: account.person)

        publishing_composition_splits_params = {
          composition: publishing_composition,
          person: person,
          composer_params: [{id: publishing_composer.id, composer_share: "75"}],
          cowriter_params: [{ first_name: "Jamaal", last_name: "Adams", cowriter_share: "25" }]
        }

        expect do
          PublishingAdministration::MultiTenantPublishingCompositionSplitsForm.new(publishing_composition_splits_params).save
        end.to_not change { account.publishing_composers.cowriting.count }
      end
    end

    context "when the total publishing_composition shares percentage is less than 100" do
      it "Ensures unknown cowriter is no longer created" do
        publishing_composition_splits_params = {
          composition: publishing_composition,
          person: person,
          composer_params: [{id: publishing_composer.id, composer_share: "75"}],
          cowriter_params: [{ first_name: "Tom", last_name: "Savage", cowriter_share: "20" }]
        }

        expect(PublishingComposer.cowriting.exists?(account: account, is_unknown: true)).to be false
        PublishingAdministration::MultiTenantPublishingCompositionSplitsForm.new(publishing_composition_splits_params).save
        expect(PublishingComposer.cowriting.exists?(account: account, is_unknown: true)).to be false
      end

      it "gives the remaining share to the publishing_composer if the left over percentage is less than 1" do
        publishing_composition_splits_params = {
          composition: publishing_composition,
          person: person,
          composer_params: [{id: publishing_composer.id, composer_share: "75"}],
          cowriter_params: [{ first_name: "Serena", last_name: "Williams", cowriter_share: "24.25" }]
        }

        form = PublishingAdministration::MultiTenantPublishingCompositionSplitsForm.new(publishing_composition_splits_params)
        form.save
        expect(form.composer_percent).to eq 75
      end
    end

    it "creates publishing splits for the publishing_composers" do
      publishing_composition_splits_params = {
        composition: publishing_composition,
        person: person,
        composer_params: [
          {id: publishing_composer.id, composer_share: "25"},
          {id: publishing_composer2.id, composer_share: "75"}
        ],
      }

      form = PublishingAdministration::MultiTenantPublishingCompositionSplitsForm.new(publishing_composition_splits_params)
      form.save
      first_publishing_composition_split = publishing_composition.publishing_composition_splits.first
      second_publishing_composition_split = publishing_composition.publishing_composition_splits.second

      expect(first_publishing_composition_split.right_to_collect).to eq(true)
      expect(first_publishing_composition_split.publishing_composer).to eq(publishing_composer)
      expect(first_publishing_composition_split.percent).to eq(25)

      expect(second_publishing_composition_split.right_to_collect).to eq(true)
      expect(second_publishing_composition_split.publishing_composer).to eq(publishing_composer2)
      expect(second_publishing_composition_split.percent).to eq(75)
    end

    it "should not update Rights App if the publishing_composer doesn't have an account" do
      publishing_composition_splits_params = {
        composition: publishing_composition,
        person: person,
        composer_params: [{id: publishing_composer.id, composer_share: "100"}]
      }

      expect(PublishingAdministration::PublishingWorkRecordingCreationWorker).not_to receive(:perform_async)
      PublishingAdministration::MultiTenantPublishingCompositionSplitsForm.new(publishing_composition_splits_params).save
    end

    it "should update Rights App if the publishing_composer has an account" do
      publishing_composer.update(provider_identifier: "12345")
      publishing_composition_splits_params = {
        composition: publishing_composition,
        person: person,
        composer_params: [{id: publishing_composer.id, composer_share: "100"}]
      }

      expect(PublishingAdministration::PublishingWorkRecordingCreationWorker)
        .to receive(:perform_async)
        .with(publishing_composer.id, publishing_composition.id)
      PublishingAdministration::MultiTenantPublishingCompositionSplitsForm.new(publishing_composition_splits_params).save
    end

    it "updates the publishing_compositions translated_name" do
      publishing_composition_splits_params = {
        composition: publishing_composition,
        person: person,
        composer_params: [{id: publishing_composer.id, composer_share: "50"}],
        cowriter_params: [],
        translated_name: "Translated Name"
      }

      PublishingAdministration::MultiTenantPublishingCompositionSplitsForm.new(publishing_composition_splits_params).save
      expect(publishing_composition.translated_name).to eq "Translated Name"
    end

    it "updates the publishing_compositions status to 'Submitted'" do
      publishing_composition_splits_params = {
        composition: publishing_composition,
        person: person,
        composer_params: [{id: publishing_composer.id, composer_share: "50"}],
        cowriter_params: [],
      }

      PublishingAdministration::MultiTenantPublishingCompositionSplitsForm.new(publishing_composition_splits_params).save
      expect(publishing_composition.status).to eq "Draft"
    end
  end

  describe "#cowriter_percent" do
    let(:person)      { create(:person) }
    let!(:account)    { create(:account, person: person) }
    let(:publishing_composition) { create(:publishing_composition, account: account) }
    let(:publishing_composer)    { create(:publishing_composer, account: account, person: account.person) }
    let(:publishing_composition_splits_params) do
      {
        composition: publishing_composition,
        person: person,
        composer_params: [{id: publishing_composer.id, composer_share: "50"}],
        cowriter_params: [{ first_name: "Serena", last_name: "Williams", cowriter_share: "10" }]
      }
    end

    it "sums together all the cowriter publishing splits -- including unknown -- associated to the publishing_composition" do
      form = PublishingAdministration::MultiTenantPublishingCompositionSplitsForm.new(publishing_composition_splits_params)
      form.save
      expect(form.cowriter_percent.to_i).to eq(10)
    end
  end

  describe "#composer_percent" do
    let(:person)      { create(:person) }
    let!(:account)    { create(:account, person: person) }
    let(:publishing_composition) { create(:publishing_composition, account: account) }
    let(:publishing_composer)    { create(:publishing_composer, account: account, person: account.person) }
    let(:publishing_composer2)   { create(:publishing_composer, account: account, person: account.person) }
    let(:publishing_composition_splits_params) do
      {
        composition: publishing_composition,
        person: person,
        composer_params: [
          {id: publishing_composer.id, composer_share: "30"},
          {id: publishing_composer2.id, composer_share: "30"}
        ],
        cowriter_params: [{ first_name: "Serena", last_name: "Williams", cowriter_share: "10" }]
      }
    end

    it "sums together all the publishing_composer publishing splits associated to the publishing_composition" do
      form = PublishingAdministration::MultiTenantPublishingCompositionSplitsForm.new(publishing_composition_splits_params)
      form.save
      expect(form.composer_percent.to_i).to eq(60)
    end
  end
end
