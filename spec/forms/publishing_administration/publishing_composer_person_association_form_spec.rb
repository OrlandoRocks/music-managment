require "rails_helper"

describe PublishingAdministration::PublishingComposerPersonAssociationForm do
  describe "#save" do
    before :each do
      @account  = create(:account)
      @publishing_composer = create(:publishing_composer, account: @account, person: @account.person)
    end

    context "when validation fails" do
      it "does not update the publishing_composer if the person_id doesn't match an existing person" do
        params  = { publishing_composer: @publishing_composer, person_id: 100000 }
        form    = PublishingAdministration::PublishingComposerPersonAssociationForm.new(params)

        expect { form.save }.not_to change { @publishing_composer.reload.person_id }
        expect(form.errors[:person].present?).to be true
      end

      it "does not update the publishing_composer if the person_id is alraeady associated to another publishing_composer" do
        other_publishing_composer  = create(:publishing_composer, person: @account.person)
        params          = { publishing_composer: @publishing_composer, person_id: other_publishing_composer.person_id }
        form            = PublishingAdministration::PublishingComposerPersonAssociationForm.new(params)

        expect { form.save }.not_to change { @publishing_composer.reload.person_id }
        expect(form.errors[:person].present?).to be true
      end
    end

    context "when the person_id is valid" do
      it "updates the publishing_composer's person_id if the person_id is not associated to any other publishing_composers" do
        person  = create(:person)
        params  = { publishing_composer: @publishing_composer, person_id: person.id }
        form    = PublishingAdministration::PublishingComposerPersonAssociationForm.new(params)

        expect { form.save }.to change { @publishing_composer.reload.person_id }
        expect(form.errors[:person].present?).to be false
      end
    end
  end
end
