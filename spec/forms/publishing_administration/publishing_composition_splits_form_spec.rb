require "rails_helper"

describe PublishingAdministration::PublishingCompositionSplitsForm do
  describe "#save" do
    let(:person) { create(:person) }
    let!(:account) { create(:account, person: person) }
    let(:publishing_composition) { create(:publishing_composition, account: account) }
    let(:publishing_composer) { create(:publishing_composer, account: account, person: account.person) }

    context "validations" do
      it "returns false if the person creating the split does not match the composers publishing administrator" do
        publishing_splits_params = {
          composer: publishing_composer,
          composition: publishing_composition,
          person: create(:person),
          composer_share: 50
        }

        form = PublishingAdministration::PublishingCompositionSplitsForm.new(publishing_splits_params)
        expect(form.valid?).to eq(false)
        expect(form.errors.full_messages).to include("Publishing split user does not have access")
      end

      it "returns true if the person creating the split does match the composers publishing administrator" do
        publishing_splits_params = {
          composer: publishing_composer,
          composition: publishing_composition,
          person: person,
          composer_share: 50
        }

        form = PublishingAdministration::PublishingCompositionSplitsForm.new(publishing_splits_params)
        expect(form.valid?).to eq(true)
        expect(form.errors.full_messages).to be_empty
      end

      it "returns false if the total shares exceed 100 percent" do
        publishing_splits_params = {
          composer: publishing_composer,
          composition: publishing_composition.reload,
          person: person,
          composer_share: 50,
          cowriter_params: [{ first_name: "Sam", last_name: "Darnold", cowriter_share: "100" }]
        }

        form = PublishingAdministration::PublishingCompositionSplitsForm.new(publishing_splits_params)
        expect(form.valid?).to eq(false)
        expect(form.errors.full_messages.present?).to be true
      end

      it "returns true if the total shares are less than or equal to 100 percent" do
        publishing_splits_params = {
          composer: publishing_composer,
          composition: publishing_composition,
          person: person,
          composer_share: 50
        }

        form = PublishingAdministration::PublishingCompositionSplitsForm.new(publishing_splits_params)
        expect(form.valid?).to eq(true)
        expect(form.errors.full_messages).to be_empty
      end

      it "returns false if the cowriter is missing a required field" do
        publishing_splits_params = {
          composer: publishing_composer,
          composition: publishing_composition,
          person: person,
          composer_share: 50,
          cowriter_params: [{ first_name: "", last_name: "", cowriter_share: "5" }]
        }

        form = PublishingAdministration::PublishingCompositionSplitsForm.new(publishing_splits_params)
        expect(form.valid?).to eq(false)
        expect(form.errors.full_messages).to include("Publishing split has invalid cowriter values")
      end

      it "returns true if the cowriter params are all present" do
        publishing_splits_params = {
          composer: publishing_composer,
          composition: publishing_composition,
          person: person,
          composer_share: 50,
          cowriter_params: [{ first_name: "Tom", last_name: "Savage", cowriter_share: "5" }]
        }

        form = PublishingAdministration::PublishingCompositionSplitsForm.new(publishing_splits_params)
        expect(form.valid?).to eq(true)
        expect(form.errors.full_messages).to be_empty
      end

      it "returns false if the cowriters share is not between the desired ranged" do
        publishing_splits_params = {
          composer: publishing_composer,
          composition: publishing_composition,
          person: person,
          composer_share: 50,
          cowriter_params: [{ first_name: "Tom", last_name: "Savage", cowriter_share: "1" }]
        }

        form = PublishingAdministration::PublishingCompositionSplitsForm.new(publishing_splits_params)
        expect(form.valid?).to eq(true)
        expect(form.errors.full_messages).to be_empty
      end
    end

    context "when cowriter params are present" do
      it "creates a new cowriter if it doesn't already exist" do
        publishing_splits_params = {
          composer: publishing_composer,
          composition: publishing_composition,
          person: person,
          composer_share: 75,
          cowriter_params: [{ first_name: "Jamaal", last_name: "Adams", cowriter_share: "25" }]
        }

        expect do
          PublishingAdministration::PublishingCompositionSplitsForm.new(publishing_splits_params).save
        end.to change { account.cowriting_composers.count }.by(1)
      end

      it "finds the cowriter if it already exists and does not create a new one" do
        create(:publishing_composer, :cowriter, first_name: "Jamaal", last_name: "Adams", account: account, person: account.person)

        publishing_splits_params = {
          composer: publishing_composer,
          composition: publishing_composition,
          person: person,
          composer_share: 75,
          cowriter_params: [{ first_name: "Jamaal", last_name: "Adams", cowriter_share: "25" }]
        }

        expect do
          PublishingAdministration::PublishingCompositionSplitsForm.new(publishing_splits_params).save
        end.to_not change { account.cowriting_composers.count }
      end
    end

    context "when the total composition shares percentage is less than 100" do
      it "creates an unknown cowriter if remaining percentage is greater than 1" do
        publishing_splits_params = {
          composer: publishing_composer,
          composition: publishing_composition,
          person: person,
          composer_share: 75,
          cowriter_params: [{ first_name: "Tom", last_name: "Savage", cowriter_share: "20" }]
        }

        expect(publishing_composition.unknown_composers.exists?).to be false
        PublishingAdministration::PublishingCompositionSplitsForm.new(publishing_splits_params).save
        expect(publishing_composition.unknown_composers.exists?).to be true
      end

      it "gives the remaining share to the composer if the left over percentage is less than 1" do
        publishing_splits_params = {
          composer: publishing_composer,
          composition: publishing_composition,
          person: person,
          composer_share: 75.50,
          cowriter_params: [{ first_name: "Serena", last_name: "Williams", cowriter_share: "24.25" }]
        }

        form = PublishingAdministration::PublishingCompositionSplitsForm.new(publishing_splits_params)
        form.save
        expect(form.composer_percent).to eq 75.5.to_d
      end
    end

    it "creates publishing splits for the composer" do
      publishing_splits_params = {
        composer: publishing_composer,
        composition: publishing_composition,
        person: person,
        composer_share: 100
      }

      form = PublishingAdministration::PublishingCompositionSplitsForm.new(publishing_splits_params)
      form.save
      publishing_composition_split = publishing_composition.publishing_composition_splits.first
      expect(publishing_composition_split.right_to_collect).to eq(true)
      expect(publishing_composition_split.publishing_composer).to eq(publishing_composer)
      expect(publishing_composition_split.percent).to eq(100)
    end

    xit "should not update Rights App if the composer doesn't have an account" do
      publishing_splits_params = {
        composer: publishing_composer,
        composition: publishing_composition,
        person: person,
        composer_share: 100
      }

      expect(PublishingAdministration::WorkRecordingCreationWorker).not_to receive(:perform_async)
      PublishingAdministration::PublishingCompositionSplitsForm.new(publishing_splits_params).save
    end

    xit "should update Rights App if the composer has an account" do
      composer.update(provider_identifier: "12345")
      publishing_splits_params = {
        composer: publishing_composer,
        composition: publishing_composition,
        person: person,
        composer_share: 100
      }

      expect(PublishingAdministration::WorkRecordingCreationWorker)
        .to receive(:perform_async)
        .with(composer.id, composition.id)
      PublishingAdministration::PublishingCompositionSplitsForm.new(publishing_splits_params).save
    end

    it "updates the compositions translated_name" do
      publishing_splits_params = {
        composer: publishing_composer,
        composition: publishing_composition,
        person: person,
        composer_share: 50,
        cowriter_params: [],
        translated_name: "Translated Name"
      }

      PublishingAdministration::PublishingCompositionSplitsForm.new(publishing_splits_params).save
      expect(publishing_composition.translated_name).to eq "Translated Name"
    end

    it "updates the compositions status to 'Draft'" do
      publishing_splits_params = {
        composer: publishing_composer,
        composition: publishing_composition,
        person: person,
        composer_share: 50,
        cowriter_params: [],
      }

      PublishingAdministration::PublishingCompositionSplitsForm.new(publishing_splits_params).save
      expect(publishing_composition.status).to eq "Draft"
    end
  end

  describe "#cowriter_percent" do
    let(:person) { create(:person) }
    let!(:account) { create(:account, person: person) }
    let(:publishing_composition) { create(:publishing_composition, account: account) }
    let(:publishing_composer) { create(:publishing_composer, account: account, person: account.person) }
    let(:publishing_splits_params) do
      {
        composer: publishing_composer,
        composition: publishing_composition,
        person: person,
        composer_share: 50,
        cowriter_params: [{ first_name: "Serena", last_name: "Williams", cowriter_share: "10" }]
      }
    end

    it "sums together all the cowriter publishing splits -- excluding unknown -- associated to the composition" do
      form = PublishingAdministration::PublishingCompositionSplitsForm.new(publishing_splits_params)
      form.save
      expect(form.cowriter_percent.to_i).to eq 10
    end
  end
end
