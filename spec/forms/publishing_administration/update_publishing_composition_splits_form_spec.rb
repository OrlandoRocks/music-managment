require "rails_helper"

describe PublishingAdministration::UpdatePublishingCompositionSplitsForm do
  describe "#update" do
    let(:person) { create(:person) }
    let(:account) { create(:account) }
    let(:publishing_composer) { create(:publishing_composer, is_primary_composer: true, person: person, account: account) }
    let(:publishing_composition) { create(:publishing_composition, account: account) }
    let(:composer_split) do
      create(:publishing_composition_split,
             percent: 50.to_d,
             publishing_composer: publishing_composer)
    end

    before do
      publishing_composition.publishing_composition_splits << composer_split
    end

    describe "#update_composition" do
      it "updates the publishing_composition name, translated name and composer share" do
        params = {
          composition_title: Faker::Name.name,
          translated_name: Faker::Name.name,
          composer_share: 100.to_d,
          cowriter_params: [],
          composition_id: publishing_composition.id
        }

        expect {
          PublishingAdministration::UpdatePublishingCompositionSplitsForm.new(params).update
        }.to change { composer_split.reload.percent }.from(50.to_d).to(100.to_d)

        expect(publishing_composition.reload.name).to eq params[:composition_title]
        expect(publishing_composition.translated_name).to eq params[:translated_name]
      end
    end

    describe "#update_cowriters" do
      context "when the cowriter already exists" do
        it "updates the existing cowriter" do
          cowriter = create(:publishing_composer, is_primary_composer: false)
          cowriter_split = publishing_composition.publishing_composition_splits.create(
            publishing_composer: cowriter,
            percent: 50.to_d,
            right_to_collect: false
          )
          cowriter_params = {
            id: cowriter_split.id,
            first_name: cowriter.first_name,
            last_name: cowriter.last_name,
            cowriter_share: "25",
            uuid: cowriter.id,
            is_unknown: false
          }

          params = {
            composer_share: "50.0",
            cowriter_params: [cowriter_params],
            composition_id: publishing_composition.id
          }

          PublishingAdministration::UpdatePublishingCompositionSplitsForm.new(params).update

          expect(cowriter.reload.first_name).to eq cowriter_params[:first_name]
          expect(cowriter.last_name).to eq cowriter_params[:last_name]
          expect(cowriter_split.reload.percent.to_d).to eq cowriter_params[:cowriter_share].to_d
        end
      end

      context "when the cowriter does not exist" do
        it "creates a new cowriter" do
          cowriter_params = {
            first_name: Faker::Name.name,
            last_name: Faker::Name.name,
            cowriter_share: "25",
            uuid: Faker::Number.number(8),
            is_unknown: false
          }

          params = {
            composer_share: "50.0",
            cowriter_params: [cowriter_params],
            composition_id: publishing_composition.id
          }

          PublishingAdministration::UpdatePublishingCompositionSplitsForm.new(params).update

          cowriter = publishing_composition.reload.publishing_composers.where(is_primary_composer: false).first
          expect(cowriter.first_name).to eq cowriter_params[:first_name]
          expect(cowriter.last_name).to eq cowriter_params[:last_name]

          cowriter_split = cowriter.publishing_composition_splits.first
          expect(cowriter_split.percent.to_d).to eq cowriter_params[:cowriter_share].to_d
        end
      end

      context "when changing the cowriter name to a name of an existing cowriter on the account" do
        it "should use that existing cowriter record, and not create a new one" do
          cowriter = create(:publishing_composer, is_primary_composer: false)
          cowriter_split = publishing_composition.publishing_composition_splits.create(
            publishing_composer: publishing_composer,
            percent: 50.to_d,
            right_to_collect: false
          )
          cowriter_params = {
            id: cowriter_split.id,
            first_name: cowriter.first_name,
            last_name: cowriter.last_name,
            cowriter_share: "25",
            uuid: cowriter.id,
            is_unknown: false
          }

          params = {
            composer_share: "50.0",
            cowriter_params: [cowriter_params],
            composition_id: publishing_composition.id
          }

          PublishingAdministration::UpdatePublishingCompositionSplitsForm.new(params).update

          expect(publishing_composer.account.publishing_composers.where(
            first_name: cowriter.first_name,
            last_name: cowriter.last_name
          ).count).to eq(1)
        end
      end

      it "should update Rights App" do
        params = {
          composition_title: Faker::Name.name,
          translated_name: Faker::Name.name,
          composer_share: 100.to_d,
          cowriter_params: [],
          composition_id: publishing_composition.id
        }

        expect(PublishingAdministration::PublishingWorkUpdateWorker)
          .to receive(:perform_async)
          .with(publishing_composition.id)
        PublishingAdministration::UpdatePublishingCompositionSplitsForm.new(params).update
      end
    end

    context "when the publishing_composition has an unknown cowriter" do
      context "when the composer share is updated" do
        it "should update both the composer share and unknown cowriter share" do
          params = {
            composition_title: "unknown",
            translated_name: nil,
            appears_on: "unknown",
            record_label: "Record Label",
            composer_share: "75.0",
            cowriter_share: "0.0",
            status: "accepted",
            isrc: "",
            performing_artist: "asdf",
            release_date: "",
            is_ntc_song: false,
            cowriter_params: [],
            composition_id:  publishing_composition.id
          }

          PublishingAdministration::UpdatePublishingCompositionSplitsForm.new(params).update

          expect(composer_split.reload.percent.to_d).to eq params[:composer_share].to_d
        end
      end

      context "when the composer share and the cowriter share is updated" do
        it "updates the composer, cowriter, and unknown cowriter shares" do
          cowriter = create(:publishing_composer, is_primary_composer: false)
          cowriter_split = publishing_composition
            .publishing_composition_splits
            .create(publishing_composer: cowriter, percent: 25.to_d, right_to_collect: false)
          cowriter_params = {
            id: cowriter_split.id,
            first_name: Faker::Name.name,
            last_name: Faker::Name.name,
            cowriter_share: "20.0",
            uuid: cowriter.id,
            is_unknown: false
          }

          params = {
            composition_title: "unknown",
            translated_name: nil,
            appears_on: "unknown",
            record_label: "Record Label",
            composer_share: "60.0",
            cowriter_share: "0.0",
            status: "accepted",
            isrc: "",
            performing_artist: "asdf",
            release_date: "",
            is_ntc_song: false,
            cowriter_params: [cowriter_params],
            composition_id:  publishing_composition.id
          }

          PublishingAdministration::UpdatePublishingCompositionSplitsForm.new(params).update

          expect(composer_split.reload.percent.to_d).to eq params[:composer_share].to_d
          expect(cowriter_split.reload.percent.to_d).to eq cowriter_params[:cowriter_share].to_d
        end
      end

      context "when the composer share will udpate the total share pct above 100%" do
        it "updates the composer and cowriter shares" do
          cowriter = create(:publishing_composer, is_primary_composer: false)
          cowriter_split = publishing_composition
            .publishing_composition_splits
            .create(publishing_composer: cowriter, percent: 50.to_d, right_to_collect: false)
          cowriter_params = {
            id: cowriter_split.id,
            first_name: Faker::Name.name,
            last_name: Faker::Name.name,
            cowriter_share: "30.0",
            uuid: cowriter.id,
            is_unknown: false
          }

          params = {
            composition_title: "unknown",
            translated_name: nil,
            appears_on: "unknown",
            record_label: "Record Label",
            composer_share: "70.0",
            cowriter_share: "0.0",
            status: "accepted",
            isrc: "",
            performing_artist: "asdf",
            release_date: "",
            is_ntc_song: false,
            cowriter_params: [cowriter_params],
            composition_id:  publishing_composition.id
          }

          PublishingAdministration::UpdatePublishingCompositionSplitsForm.new(params).update

          expect(composer_split.reload.percent.to_d).to eq params[:composer_share].to_d
          expect(cowriter_split.reload.percent.to_d).to eq cowriter_params[:cowriter_share].to_d
        end
      end
    end

    context "when the publishing_composition has a cowriter and an unknown cowriter and you add a new cowriter" do
      context "when the total sum does not add up to 100%" do
        it "creates a new cowriter and updates the splits accordingly" do
          cowriter = create(:publishing_composer, is_primary_composer: false)
          cowriter_split = publishing_composition.publishing_composition_splits.create(
            publishing_composer: cowriter,
            percent: 25.to_d,
            right_to_collect: false
          )
          cowriter_params = [
            {
              id: cowriter_split.id,
              first_name: Faker::Name.name,
              last_name: Faker::Name.name,
              cowriter_share: "10.0",
              uuid: cowriter.id,
              is_unknown: false
            },{
              first_name: Faker::Name.name,
              last_name: Faker::Name.name,
              cowriter_share: "10",
              uuid: "e7748778-5153-4061-bbb5-e2b4d5e38377"
            }
          ]

          params = {
            composition_id:  publishing_composition.id,
            appears_on: "new title",
            record_label: "Record Label",
            composition_title: "new title",
            composer_share: "60.0",
            cowriter_share: "0.0",
            isrc: "",
            is_ntc_song: false,
            status: "accepted",
            cowriter_params:  cowriter_params,
          }

          PublishingAdministration::UpdatePublishingCompositionSplitsForm.new(params).update

          expect(composer_split.reload.percent.to_d).to eq params[:composer_share].to_d
          expect(cowriter_split.reload.percent.to_d).to eq cowriter_params.first[:cowriter_share].to_d

          new_cowriter_split = publishing_composition.publishing_composition_splits.reload.last
          expect(new_cowriter_split.percent.to_d).to eq cowriter_params.last[:cowriter_share].to_d

          new_cowriter = new_cowriter_split.publishing_composer
          expect(new_cowriter.first_name).to eq cowriter_params.last[:first_name]
          expect(new_cowriter.last_name).to eq cowriter_params.last[:last_name]
        end
      end

      context "when the total sum adds up to 100%" do
        it "creates a new cowriter, updates the splits accordingly, and deletes the unknown split" do
          cowriter = create(:publishing_composer, is_primary_composer: false)
          cowriter_split = publishing_composition.publishing_composition_splits.create(
            publishing_composer: cowriter,
            percent: 25.to_d,
            right_to_collect: false
          )
          cowriter_params = [
            {
              id: cowriter_split.id,
              first_name: Faker::Name.name,
              last_name: Faker::Name.name,
              cowriter_share: "20.0",
              uuid: cowriter.id,
              is_unknown: false
            },{
              first_name: Faker::Name.name,
              last_name: Faker::Name.name,
              cowriter_share: "20",
              uuid: "e7748778-5153-4061-bbb5-e2b4d5e38377"
            }
          ]

          params = {
            composition_id:  publishing_composition.id,
            appears_on: "new title",
            record_label: "Record Label",
            composition_title: "new title",
            composer_share: "60.0",
            cowriter_share: "0.0",
            isrc: "",
            is_ntc_song: false,
            status: "accepted",
            cowriter_params:  cowriter_params,
          }

          PublishingAdministration::UpdatePublishingCompositionSplitsForm.new(params).update

          expect(composer_split.reload.percent.to_d).to eq params[:composer_share].to_d
          expect(cowriter_split.reload.percent.to_d).to eq cowriter_params.first[:cowriter_share].to_d

          new_cowriter_split = publishing_composition.publishing_composition_splits.reload.last
          expect(new_cowriter_split.percent.to_d).to eq cowriter_params.last[:cowriter_share].to_d

          new_cowriter = new_cowriter_split.publishing_composer
          expect(new_cowriter.first_name).to eq cowriter_params.last[:first_name]
          expect(new_cowriter.last_name).to eq cowriter_params.last[:last_name]
        end
      end
    end

    context "when the publishing_composition is associated to a NonTunecore Song" do
      let!(:ntc_song) { create(:non_tunecore_song, publishing_composition: publishing_composition) }
      let(:ntc_album) { ntc_song.non_tunecore_album }
      let(:artist) { create(:artist) }

      it "updates the the associated non tunecore song and non tunecore album" do
        params = {
          composition_id:  publishing_composition.id,
          appears_on:  Faker::Name.name,
          record_label: "Record Label",
          composition_title: "New Title",
          composer_share: "60.0",
          cowriter_share: "0.0",
          isrc: Faker::Number.number(12).to_s,
          release_date:  "11/11/2011",
          performing_artist:  Faker::Name.name,
          is_ntc_song: true,
          status: "accepted",
          cowriter_params:  [],
        }

        PublishingAdministration::UpdatePublishingCompositionSplitsForm.new(params).update

        expect(ntc_song.reload.isrc).to eq params[:isrc]
        expect(ntc_song.release_date.to_date).to eq params[:release_date].to_date
        expect(ntc_song.artist.name).to eq params[:performing_artist]
        expect(ntc_album.reload.name).to eq params[:appears_on]
        expect(ntc_album.record_label).to eq params[:record_label]
      end
    end

    context "when the publishing_composition is updated with an isrc belonging to a distro song" do
      let!(:ntc_song) { create(:non_tunecore_song, publishing_composition: publishing_composition) }
      let(:ntc_album) { ntc_song.non_tunecore_album }
      let(:artist) { create(:artist) }
      let(:album) { create(:album, person: person) }
      let!(:song) { create(:song, :with_tunecore_isrc, album: album) }

      it "returns an error" do
        params = {
          composition_id:  publishing_composition.id,
          appears_on:  Faker::Name.name,
          record_label: "Record Label",
          composition_title: "New Title",
          composer_share: "60.0",
          cowriter_share: "0.0",
          isrc: song.tunecore_isrc,
          release_date:  "11/11/2011",
          performing_artist:  Faker::Name.name,
          is_ntc_song: true,
          status: "accepted",
          cowriter_params:  [],
        }

        form = PublishingAdministration::UpdatePublishingCompositionSplitsForm.new(params)
        form.update

        expect(form.errors.messages[:isrc].first).to eq 'isrc_in_use'
        expect(ntc_song.reload.isrc).to eq nil
      end
    end
  end
end
