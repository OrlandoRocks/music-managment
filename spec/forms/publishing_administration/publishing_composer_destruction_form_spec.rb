require "rails_helper"

describe PublishingAdministration::PublishingComposerDestructionForm do
  let!(:admin) { create(:person, :admin) }
  let!(:ip_address) { "127.0.0.1" }
  let!(:person) { create(:person) }
  let!(:account) { create(:account, person: person) }
  let!(:publishing_composer) { create(:publishing_composer, account: account, person: nil) }

  describe "#save" do
    context "when the params are valid" do
      let!(:terminated_composer) { create(:terminated_composer, :fully, publishing_composer: publishing_composer) }
      let!(:extra_publishing_composer) { create(:publishing_composer, account: account, person_id: nil) }

      let(:params) { { publishing_composer: publishing_composer, account: account, admin: admin, ip_address: ip_address } }
      let(:form) { PublishingAdministration::PublishingComposerDestructionForm.new(params) }

      it "returns true" do
        expect(form.save).to be_truthy
      end

      it "deletes the publishing_composer" do
        expect{
          form.save
        }.to change(PublishingComposer, :count).by(-1)

        expect(PublishingComposer.find_by(id: publishing_composer.id)).to be_nil
      end

      it "creates a note for the account user indicating the publishing_composer was deleted" do
        expect{
          form.save
        }.to change{
          person.notes.count
        }.by(1)

        note = person.reload.notes.last
        expect(note.ip_address).to eq(ip_address)
        expect(note.note_created_by_id).to eq(admin.id)
        expect(note.subject).to eq('PublishingComposer Fully Deleted')
        expect(note.note).to include("PublishingComposer #{publishing_composer.name} with writer code #{publishing_composer.provider_identifier} was deleted")
      end

      context "when the publishing_composer has publishing_composition_splits" do
        let!(:publishing_composition_split) { create(:publishing_composition_split, publishing_composer: publishing_composer) }

        it "deletes those publishing_composition_splits" do
          expect{
            form.save
          }.to change(PublishingCompositionSplit, :count).by(-2)

          expect(PublishingCompositionSplit.find_by(id: publishing_composition_split.id)).to be_nil
        end
      end
    end

    context "when the params are not valid" do
      context "when some params are missing" do
        context "when the publishing_composer is missing" do
          it "fails and returns an error" do
            params = { publishing_composer: nil, account: account, admin: admin, ip_address: ip_address }
            form = PublishingAdministration::PublishingComposerDestructionForm.new(params)

            expect(form.save).to be_falsy
            expect(form.errors[:publishing_composer].present?).to be true
          end
        end

        context "when the account is missing" do
          it "fails and returns an error" do
            publishing_composer.update(account_id: nil)

            params = { publishing_composer: nil, account: nil, admin: admin, ip_address: ip_address }
            form = PublishingAdministration::PublishingComposerDestructionForm.new(params)

            expect(form.save).to be_falsy
            expect(form.errors[:account].present?).to be true
          end
        end

        context "when the admin is missing" do
          it "fails and returns an error" do
            params = { publishing_composer: publishing_composer, account: account, admin: nil, ip_address: ip_address }
            form = PublishingAdministration::PublishingComposerDestructionForm.new(params)

            expect(form.save).to be_falsy
            expect(form.errors[:admin].present?).to be true
          end
        end

        context "when the ip_address is missing" do
          it "fails and returns an error" do
            params = { publishing_composer: publishing_composer, account: account, admin: admin, ip_address: nil }
            form = PublishingAdministration::PublishingComposerDestructionForm.new(params)

            expect(form.save).to be_falsy
            expect(form.errors[:ip_address].present?).to be true
          end
        end
      end

      context "when all params are present" do
        let(:params) { { publishing_composer: publishing_composer, account: account, admin: admin, ip_address: ip_address } }

        context "when the publishing_composer does not have a terminated_composer" do
          let!(:extra_publishing_composer) { create(:publishing_composer, account: account, person_id: nil) }

          it "fails and returns an error" do
            form = PublishingAdministration::PublishingComposerDestructionForm.new(params)

            expect(form.save).to be_falsy
            expect(form.errors[:publishing_composer].present?).to be true
          end
        end

        context "when the publishing_composer does have a terminated_composer, but it is only partially terminated" do
          let!(:extra_publishing_composer) { create(:publishing_composer, account: account, person_id: nil) }
          let!(:terminated_composer) { create(:terminated_composer, :partially, publishing_composer: publishing_composer) }

          it "fails and returns an error" do
            form = PublishingAdministration::PublishingComposerDestructionForm.new(params)

            expect(form.save).to be_falsy
            expect(form.errors[:publishing_composer].present?).to be true
          end
        end

        context "when the publishing_composer is the only publishing_composer on the account" do
          let!(:terminated_composer) { create(:terminated_composer, :fully, publishing_composer: publishing_composer) }

          it "fails and returns an error" do
            form = PublishingAdministration::PublishingComposerDestructionForm.new(params)

            expect(form.save).to be_falsy
            expect(form.errors[:publishing_composer].present?).to be true
          end
        end

        context "when that publishing_composer is the primary account publishing_composer" do
          let(:publishing_composer) { create(:publishing_composer, account: account, person_id: account.person.id) }
          let!(:terminated_composer) { create(:terminated_composer, :fully, publishing_composer: publishing_composer) }
          let!(:extra_publishing_composer) { create(:publishing_composer, account: account, person_id: nil) }

          it "fails and returns an error" do
            form = PublishingAdministration::PublishingComposerDestructionForm.new(params)

            expect(form.save).to be_falsy
            expect(form.errors[:publishing_composer].present?).to be true
          end
        end
      end
    end
  end
end
