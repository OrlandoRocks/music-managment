require "rails_helper"

describe PublishingAdministration::TerminateComposerForm do
  let(:composer)  { create(:composer) }

  describe "#save" do
    context "when the effective_date is" do
      it "formatted incorrectly returns false" do
        params            = { composer_id: composer.id, effective_date: "10-18-1987", admin_note: "Fraud" }
        termination_form  = PublishingAdministration::TerminateComposerForm.new(params)

        expect(termination_form.save).to be_falsey
      end

      it "formatted incorrectly adds an effective_date error" do
        params            = { composer_id: composer.id, effective_date: "10/18/87", admin_note: "Fraud" }
        termination_form  = PublishingAdministration::TerminateComposerForm.new(params)

        termination_form.save

        expect(termination_form.errors[:effective_date].present?).to be true
      end

      it "formatted correctly saves without errors" do
        params            = { composer_id: composer.id, effective_date: "06/27/2019", admin_note: "Fraud", publishing_termination_reason_id: 1 }
        termination_form  = PublishingAdministration::TerminateComposerForm.new(params)

        termination_form.save

        expect(termination_form.errors[:effective_date].present?).to be false
      end
    end

    context "when data is valid" do
      it "creates a fully terminated composer" do
        params            = { composer_id: composer.id, effective_date: "", admin_note: "", publishing_termination_reason_id: 1 }
        termination_form  = PublishingAdministration::TerminateComposerForm.new(params)

        termination_form.save

        expect(TerminatedComposer.exists?(composer_id: composer.id)).to be true
        expect(composer.reload.terminated_at.nil?).to be false
      end

      it "updates a partially terminated composer to fully terminated" do
        terminated_composer = create(:terminated_composer, :partially, composer: composer)
        params              = { composer_id: composer.id, effective_date: "", admin_note: "", publishing_termination_reason_id: 1 }
        termination_form    = PublishingAdministration::TerminateComposerForm.new(params)

        termination_form.save

        expect(terminated_composer.reload.fully_terminated?).to be true
        expect(composer.reload.terminated_at.nil?).to be false
      end
    end
  end

  describe "#destroy" do
    context "composer has terminated compositions" do
      it "updates a fully terminated composer to partially terminated instead of destroying" do
        terminated_composer = create(:terminated_composer, :fully, composer: composer)
        params              = { terminated_composer: terminated_composer }
        termination_form    = PublishingAdministration::TerminateComposerForm.new(params)

        allow(terminated_composer).to receive(:terminated_compositions?).and_return(true)
        termination_form.destroy

        expect(terminated_composer.reload.partially_terminated?).to be true
        expect(composer.terminated_at).to eq nil
      end
    end

    context "composer has no terminated compositions" do
      it "destroys the terminated composer" do
        terminated_composer = create(:terminated_composer, :fully, composer: composer)
        params              = { terminated_composer: terminated_composer }
        termination_form    = PublishingAdministration::TerminateComposerForm.new(params)

        termination_form.destroy

        expect(composer.reload.terminated_composer).to eq nil
        expect(composer.terminated_at).to eq nil
      end
    end
  end
end
