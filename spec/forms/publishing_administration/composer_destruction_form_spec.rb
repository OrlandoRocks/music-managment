require "rails_helper"

describe PublishingAdministration::ComposerDestructionForm do
  let!(:admin) { create(:person, :admin) }
  let!(:ip_address) { "127.0.0.1" }
  let!(:person) { create(:person) }
  let!(:account) { create(:account, person: person) }
  let!(:composer) { create(:composer, account: account, person: nil) }

  describe "#save" do
    context "when the params are valid" do
      let!(:terminated_composer) { create(:terminated_composer, :fully, composer: composer) }
      let!(:extra_composer) { create(:composer, account: account, person_id: nil) }

      let(:params) { { composer: composer, account: account, admin: admin, ip_address: ip_address } }
      let(:form) { PublishingAdministration::ComposerDestructionForm.new(params) }

      it "returns true" do
        expect(form.save).to be_truthy
      end

      it "deletes the composer" do
        expect{
          form.save
        }.to change(Composer, :count).by(-1)

        expect(Composer.find_by(id: composer.id)).to be_nil
      end

      it "creates a note for the account user indicating the composer was deleted" do
        expect{
          form.save
        }.to change{
          person.notes.count
        }.by(1)

        note = person.reload.notes.last
        expect(note.ip_address).to eq(ip_address)
        expect(note.note_created_by_id).to eq(admin.id)
        expect(note.subject).to eq('Composer Fully Deleted')
        expect(note.note).to include("Composer #{composer.name} with writer code #{composer.provider_identifier} was deleted")
      end

      context "when the composer has publishing_splits" do
        let!(:publishing_split) { create(:publishing_split, composer: composer) }

        it "deletes those publishing_splits" do
          expect{
            form.save
          }.to change(PublishingSplit, :count).by(-2)

          expect(PublishingSplit.find_by(id: publishing_split.id)).to be_nil
        end
      end
    end

    context "when the params are not valid" do
      context "when some params are missing" do
        context "when the composer is missing" do
          it "fails and returns an error" do
            params = { composer: nil, account: account, admin: admin, ip_address: ip_address }
            form = PublishingAdministration::ComposerDestructionForm.new(params)

            expect(form.save).to be_falsy
            expect(form.errors[:composer].present?).to be true
          end
        end

        context "when the account is missing" do
          it "fails and returns an error" do
            composer.update(account_id: nil)

            params = { composer: nil, account: nil, admin: admin, ip_address: ip_address }
            form = PublishingAdministration::ComposerDestructionForm.new(params)

            expect(form.save).to be_falsy
            expect(form.errors[:account].present?).to be true
          end
        end

        context "when the admin is missing" do
          it "fails and returns an error" do
            params = { composer: composer, account: account, admin: nil, ip_address: ip_address }
            form = PublishingAdministration::ComposerDestructionForm.new(params)

            expect(form.save).to be_falsy
            expect(form.errors[:admin].present?).to be true
          end
        end

        context "when the ip_address is missing" do
          it "fails and returns an error" do
            params = { composer: composer, account: account, admin: admin, ip_address: nil }
            form = PublishingAdministration::ComposerDestructionForm.new(params)

            expect(form.save).to be_falsy
            expect(form.errors[:ip_address].present?).to be true
          end
        end
      end

      context "when all params are present" do
        let(:params) { { composer: composer, account: account, admin: admin, ip_address: ip_address } }

        context "when the composer does not have a terminated_composer" do
          let!(:extra_composer) { create(:composer, account: account, person_id: nil) }

          it "fails and returns an error" do
            form = PublishingAdministration::ComposerDestructionForm.new(params)

            expect(form.save).to be_falsy
            expect(form.errors[:composer].present?).to be true
          end
        end

        context "when the composer does have a terminated_composer, but it is only partially terminated" do
          let!(:extra_composer) { create(:composer, account: account, person_id: nil) }
          let!(:terminated_composer) { create(:terminated_composer, :partially, composer: composer) }

          it "fails and returns an error" do
            form = PublishingAdministration::ComposerDestructionForm.new(params)

            expect(form.save).to be_falsy
            expect(form.errors[:composer].present?).to be true
          end
        end

        context "when the composer is the only composer on the account" do
          let!(:terminated_composer) { create(:terminated_composer, :fully, composer: composer) }

          it "fails and returns an error" do
            form = PublishingAdministration::ComposerDestructionForm.new(params)

            expect(form.save).to be_falsy
            expect(form.errors[:composer].present?).to be true
          end
        end

        context "when that composer is the primary account composer" do
          let(:composer) { create(:composer, account: account, person_id: account.person.id) }
          let!(:terminated_composer) { create(:terminated_composer, :fully, composer: composer) }
          let!(:extra_composer) { create(:composer, account: account, person_id: nil) }

          it "fails and returns an error" do
            form = PublishingAdministration::ComposerDestructionForm.new(params)

            expect(form.save).to be_falsy
            expect(form.errors[:composer].present?).to be true
          end
        end
      end
    end
  end
end
