require "rails_helper"

describe PublishingAdministration::PublishingEnrollmentForm do
  let(:person)    { create(:person) }
  let!(:account)  { create(:account, person: person) }

  before(:each) do
    allow(FeatureFlipper).to receive(:show_feature?) { true }
    allow(DocuSign::LodApi).to receive(:send_lod_mail)
  end

  describe "#save" do
    let(:params) do
      {
        name_prefix: "Ms.",
        first_name: "Tom",
        middle_name: "Edward",
        last_name: "Brady",
        name_suffix: "Jr.",
        dob_y: "2000",
        dob_m: "1",
        dob_d: "1",
        composer_pro_id: "5",
        composer_cae: "123456789",
        publisher_cae: "123456789",
        publisher_pro_id: "4",
        publisher_name: "Deflate Gate",
        publishing_role_id: "5",
        email: person.email,
        person_id: person.id,
        tos_agreed: "1"
      }.with_indifferent_access
    end

    context "when the publishing_composer is invalid" do
      it "returns false" do
        params[:first_name] = ""
        enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
        expect(enrollment_form.save).to be_falsey
      end

      it "returns false if the publishing_composer doesn't have a valid publishing_role_id" do
        params[:publishing_role_id] = "100"

        enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)

        expect(enrollment_form.save).to be_falsey
        expect(enrollment_form.errors.full_messages)
          .to eq ["Publishing role is not included in the list"]
      end

      it "returns false if the publishing_composer is missing a pro number, but has a cae number" do
        params[:composer_pro_id] = nil

        enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
        expect(enrollment_form.save).to be_falsey
        expect(enrollment_form.errors.size).to eq(1)
        expect(enrollment_form.errors.first).to eq([:composer_cae, "can't be present without PRO"])
      end

      it "returns false if the publishing_composer has a cae number of invalid length (between 9 and 11)" do
        params[:composer_cae] = "12345"

        enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
        expect(enrollment_form.save).to be_falsey
        expect(enrollment_form.errors.size).to eq(1)
        expect(enrollment_form.errors.first)
          .to eq([:composer_cae, "CAE length must be between 9 and 11"])
      end

      it "returns false if the publishing_composer cae number starts with 55" do
        params[:composer_cae] = "553456789"

        enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
        expect(enrollment_form.save).to be_falsey
        expect(enrollment_form.errors.size).to eq(1)
        expect(enrollment_form.errors.first)
          .to eq([:composer_cae, "Your IPI/CAE cannot start with 55"])
      end

      it "returns false if the publishing_composer's cae number is not a number" do
        params[:composer_cae] = "abcdefgas"

        enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
        expect(enrollment_form.save).to be_falsey
        expect(enrollment_form.errors.size).to eq(1)
      end

      it "returns false if user did not agree to tos" do
        params[:tos_agreed] = "0"

        enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
        expect(enrollment_form.save).to be_falsey
        expect(enrollment_form.errors.size).to eq(1)
        expect(enrollment_form.errors.first)
          .to eq([:tos_agreed, "Please agree to the Terms of Service to proceed"])
      end
    end

    context "when the publishing_composer is valid" do
      let(:params_with_no_dob) do
        {
          name_prefix: "Ms.",
          first_name: "Tom",
          middle_name: "Edward",
          last_name: "Brady",
          name_suffix: "Jr.",
          composer_pro_id: "5",
          composer_cae: "123456789",
          publisher_pro_id: "4",
          publisher_cae: "123456789",
          publisher_name: "Deflate Gate",
          publishing_role_id: "5",
          email: person.email,
          person_id: person.id,
          tos_agreed: "1"
        }.with_indifferent_access
      end

      let(:param_with_no_pub_cae) do
        {
          name_prefix: "Ms.",
          first_name: "Tom",
          middle_name: "Edward",
          last_name: "Brady",
          name_suffix: "Jr.",
          dob: "2000-1-1",
          composer_pro_id: "5",
          composer_cae: "123456789",
          publisher_pro_id: "4",
          publisher_name: "Deflate Gate",
          publishing_role_id: "5",
          email: person.email,
          person_id: person.id,
          tos_agreed: "1"
        }.with_indifferent_access
      end

      let(:param_with_no_pub_name) do
        {
          name_prefix: "Ms.",
          first_name: "Tom",
          middle_name: "Edward",
          last_name: "Brady",
          name_suffix: "Jr.",
          dob: "2000-1-1",
          composer_pro_id: "36",
          composer_cae: "123456789",
          publisher_pro_id: "4",
          publishing_role_id: "5",
          publisher_cae: "123456789",
          email: person.email,
          person_id: person.id,
          tos_agreed: "1"
        }.with_indifferent_access
      end

      it "creates a publishing_composer and associated publisher record with the correct fields" do
        enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
        enrollment_form.save
        publishing_composer = enrollment_form.publishing_composer

        expect(publishing_composer.is_primary_composer).to be_truthy
        expect(publishing_composer.name_prefix).to eq "Ms."
        expect(publishing_composer.first_name).to eq "Tom"
        expect(publishing_composer.middle_name).to eq "Edward"
        expect(publishing_composer.last_name).to eq "Brady"
        expect(publishing_composer.name_suffix).to eq "Jr."

        expect(publishing_composer.dob.strftime("%Y-%m-%d")).to eq "2000-01-01"
        expect(publishing_composer.performing_rights_organization_id).to eq 5
        expect(publishing_composer.cae).to eq "123456789"
        expect(publishing_composer.publisher.name).to eq "Deflate Gate"
        expect(publishing_composer.publisher.cae).to eq "123456789"
        expect(publishing_composer.publishing_role_id).to eq 5
        expect(publishing_composer.email).to eq person.email
        expect(publishing_composer.person).to eq person
        expect(publishing_composer.account).to eq account
      end

      it "valid without dob" do
        enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params_with_no_dob)
        enrollment_form.save
        publishing_composer = enrollment_form.publishing_composer
        expect(publishing_composer.valid?).to eq(true)
      end

      it "is invalid if publisher name is present but publisher cae is not" do
        enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(param_with_no_pub_cae)
        enrollment_form.save
        expect(enrollment_form.valid?).to eq(false)
      end

      it "is invalid if publisher cae is present but publisher name is not" do
        enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(param_with_no_pub_name)
        enrollment_form.save
        expect(enrollment_form.valid?).to eq(false)
      end

      it "leaves person_id nil if publishing_composer is not being created/managed by one's self" do
        params[:publishing_role_id] = 4

        enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
        enrollment_form.save
        publishing_composer = enrollment_form.publishing_composer

        expect(publishing_composer.person).to be nil
        expect(publishing_composer.account.person).to eq person
      end

      context "when id is nil" do
        it "finds publishing_composer by email to avoid creating duplicate publishing_composer" do
          account = create(:account)
          publishing_composer = create(
            :publishing_composer,
            account: account,
            person: account.person,
            email: "ruthbaderginsburg@notorious.rbg"
          )

          params = {
            name_prefix: "",
            first_name: "Ruth",
            middle_name: "",
            last_name: "Bader Ginsburg",
            name_suffix: "",
            composer_pro_id: "5",
            composer_cae: "123456789",
            publishing_role_id: "5",
            email: publishing_composer.email,
            person_id: publishing_composer.person_id
          }.with_indifferent_access

          enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
          enrollment_form.save
          expect(enrollment_form.publishing_composer).to eq(publishing_composer)
          expect(account.publishing_composers.where(is_primary_composer: true).count).to eq(1)
        end

        it "creates a new publishing_composer if one does not already exist with given email" do
          account = create(:account)
          params = {
            name_prefix: "",
            first_name: "Ruth",
            middle_name: "",
            last_name: "Bader Ginsburg",
            name_suffix: "",
            composer_pro_id: "5",
            composer_cae: "123456789",
            publishing_role_id: "5",
            email: "ruthbaderginsburg@notorious.rbg",
            person_id: account.person_id,
            tos_agreed: "1"
          }.with_indifferent_access

          expect(account.publishing_composers.where(is_primary_composer: true).count).to eq(0)
          enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
          enrollment_form.save
          expect(account.publishing_composers.where(is_primary_composer: true).count).to eq(1)
        end
      end

      context "when id is NOT nil" do
        let(:account) { create(:account) }

        let(:publishing_composer) do
          create(
            :publishing_composer,
            :with_pub_admin_purchase,
            account: account,
            person: account.person,
            email: "ruthbaderginsburg@notorious.rbg"
          )
        end

        let(:params) do
          {
            name_prefix: "",
            first_name: "Ruth",
            middle_name: "",
            last_name: "Bader Ginsburg",
            name_suffix: "",
            composer_pro_id: "5",
            composer_cae: "123456789",
            publishing_role_id: "5",
            email: publishing_composer.email,
            person_id: publishing_composer.person_id,
            tos_agreed: "1"
          }.with_indifferent_access
        end

        it "find the existing publishing_composer to edit" do
          enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
          enrollment_form.save
          expect(enrollment_form.publishing_composer).to eq(publishing_composer)
          expect(account.publishing_composers.where(is_primary_composer: true).count).to eq 1
        end

        context "when the :update action is not passed in" do
          it "does not call PublishingAdministration::PublishingCompositionUpdateWorker" do
            enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)

            expect(PublishingAdministration::PublishingCompositionUpdateWorker).to_not receive(:perform_async)

            enrollment_form.save
          end
        end

        context "when the :update action is passed in" do
          let(:update_params) do
            params.merge(action: :update).with_indifferent_access
          end

          it "calls PublishingAdministration::PublishingCompositionUpdateWorker with the publishing_composer's id" do
            enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(update_params)

            expect(PublishingAdministration::PublishingCompositionUpdateWorker).to receive(:perform_async).with(publishing_composer.id)

            enrollment_form.save
          end
        end
      end

      context "when publisher attributes are NOT present" do
        let(:params) do
          {
            first_name: "Tom",
            middle_name: "",
            last_name: "Brady",
            dob: "2000-1-1",
            composer_pro_id: "5",
            composer_cae: "123456789",
            publisher_pro_id: "4",
            publisher_name: "",
            publishing_role_id: "5",
            email: person.email,
            person_id: person.id,
            tos_agreed: "1"
          }.with_indifferent_access
        end

        it "does not create a publisher associated to the publishing_composer" do
          enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
          enrollment_form.save

          expect(enrollment_form.publishing_composer.publisher).to be_nil
        end
      end

      context "when publisher attributes are present" do
        let(:params) do
          {
            first_name: "Tom",
            middle_name: "",
            last_name: "Brady",
            dob: "2000-1-1",
            composer_pro_id: "36",
            composer_cae: "123456789",
            publisher_cae: "987654321",
            publisher_pro_id: "36",
            publisher_name: "Deflate Gate",
            publishing_role_id: "5",
            email: person.email,
            person_id: person.id,
            tos_agreed: "1"
          }.with_indifferent_access
        end

        it "creates a publisher associated to the publishing_composer" do
          enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
          enrollment_form.save
          publisher = enrollment_form.publishing_composer.publisher

          expect(publisher.name).to eq "Deflate Gate"
          expect(publisher.performing_rights_organization_id).to eq 36
          expect(publisher.cae).to eq "987654321"
        end

        it "associates the publisher to an existing publisher if one exists" do
          publisher = create(
            :publisher,
            name: "Deflate Gate",
            cae: "987654321",
            performing_rights_organization_id: 36
          )

          enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
          enrollment_form.save
          publishing_composer = enrollment_form.publishing_composer

          expect(publishing_composer.publisher_id).to eq publisher.id
        end

        it "raises an error when the publisher CAE matches the composer CAE" do
          params[:publisher_cae] = "123456789"

          enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
          enrollment_form.save
          expect(enrollment_form.errors.messages[:publisher_cae].size).to eq 1
        end
      end

      context "when the user's PRO isn't ASCAP, BMI, SECAC and it has no preexisting publisher" do
        it "creates a publisher" do
          params = {
            first_name: "Tom",
            middle_name: "",
            last_name: "Brady",
            dob: "2000-1-1",
            composer_pro_id: "3",
            composer_cae: "123456789",
            publisher_cae: "987654321",
            publisher_pro_id: "",
            publisher_name: "Deflate Gate",
            publishing_role_id: "5",
            email: person.email,
            person_id: person.id,
            tos_agreed: "1"
          }

          enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
          enrollment_form.save
          expect(enrollment_form.publishing_composer.publisher.name).to eq "Deflate Gate"
          expect(enrollment_form.publishing_composer.publisher.cae).to eq "987654321"
        end
      end

      context "when the publishing_composer does NOT have a pro affiliation" do
        let(:params) do
          {
            first_name: "Tom",
            middle_name: "",
            last_name: "Brady",
            dob: "2000-1-1",
            composer_pro_id: "",
            composer_cae: "",
            publisher_cae: "123456789",
            publisher_pro_id: "",
            publisher_name: "Deflate Gate",
            publishing_role_id: "1",
            email: person.email,
            person_id: person.id,
            tos_agreed: "1"
          }.with_indifferent_access
        end
      end

      context "when the publishing_composer has a pro affiliation" do
        let(:params) do
          {
            first_name: "Tom",
            middle_name: "",
            last_name: "Brady",
            dob: "2000-1-1",
            composer_pro_id: "7",
            composer_cae: "123456789",
            publisher_cae: "123456789",
            publisher_pro_id: "",
            publisher_name: "Deflate Gate",
            publishing_role_id: "5",
            email: person.email,
            person_id: person.id,
            tos_agreed: "1"
          }.with_indifferent_access
        end
      end

      context "when the publishing_composer has a pro affliation eligible for publishing" do
        let(:params) do
          {
            first_name: "Tom",
            middle_name: "",
            last_name: "Brady",
            dob: "2000-1-1",
            composer_pro_id: "36",
            composer_cae: "123456789",
            publisher_cae: "",
            publisher_pro_id: "",
            publisher_name: "",
            publishing_role_id: "5",
            email: person.email,
            person_id: person.id,
            tos_agreed: "1"
          }.with_indifferent_access
        end

        it "is valid if neither the publisher name or publisher cae number are provided" do
          enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
          expect(enrollment_form.save).to be_truthy
          expect(enrollment_form.errors[:publisher_name]).to be_empty
          expect(enrollment_form.errors[:publisher_cae]).to be_empty
        end

        it "is invalid if the publisher name is provided without the cae number" do
          params[:publisher_name] = "Deflate Gate"
          enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
          expect(enrollment_form.save).to be_falsey
          expect(enrollment_form.errors[:publisher_cae]).not_to be_empty
        end

        it "is invalid if the publisher cae is provided without the publisher name" do
          params[:publisher_cae] = "123456789"
          enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
          expect(enrollment_form.save).to be_falsey
          expect(enrollment_form.errors[:publisher_name]).not_to be_empty
        end
      end

      context "when the publishing_composer record already exists" do
        let(:publisher) { create(:publisher, name: "May Field") }

        let(:publishing_composer) do
          create(
            :publishing_composer,
            first_name: "Baker",
            publisher: publisher,
            performing_rights_organization_id: 1,
            cae: "123456789"
          )
        end

        let(:person) { publishing_composer.person }

        let(:params) do
          {
            first_name: first_name,
            middle_name: publishing_composer.middle_name,
            last_name: publishing_composer.last_name,
            dob: publishing_composer.dob,
            composer_cae: publishing_composer.cae,
            composer_pro_id: publishing_composer.performing_rights_organization_id.to_s,
            publisher_cae: "123456789",
            publishing_role_id: "1",
            email: person.email,
            person_id: publishing_composer.account.person_id,
            id: publishing_composer.id,
            publisher_name: "Deflate Gate",
            publisher_pro_id: publisher.performing_rights_organization_id.to_s,
            tos_agreed: "1"
          }.with_indifferent_access
        end

        context "when the publishing_composer's name has NOT changed" do
          let(:first_name) { "Baker" }

          it "publishing_composer_name_changed should return false" do
            enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
            enrollment_form.save
            expect(enrollment_form.publishing_composer_name_changed?).to be_falsey
          end
        end

        context "when the publishing_composer's name has changed" do
          let(:first_name) { "Patrick" }

          it "publishing_composer_name_changed should return true" do
            enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
            enrollment_form.save
            expect(enrollment_form.publishing_composer_name_changed?).to be_truthy
          end

          it "updates the existing record if user is an admin" do
            person.roles = [Role.new(name: 'Admin')]
            person.save

            expect(publishing_composer.first_name).to eq "Baker"
            enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
            enrollment_form.save
            new_composer = enrollment_form.publishing_composer
            expect(new_composer.id).to eq publishing_composer.id
            expect(new_composer.first_name).to eq "Patrick"
          end
        end
      end

      context "when the publishing_composer is created on behalf of self" do
        it "should not create a publishing_composer associated to the account" do
          params["publishing_role_id"] = PublishingRole.find_by(title: "self").id
          enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
          expect{ enrollment_form.save }.to change(PublishingComposer, :count).by(1)
        end
      end

      context "when the publishing_composer is created on behalf of another" do
        context "and the account manager does NOT have a publishing_composer" do
          it "creates a publishing_composer associated to the account" do
            params["publishing_role_id"] = PublishingRole.find_by(title: "manager").id
            enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
            expect{ enrollment_form.save }.to change(PublishingComposer, :count).by(2)
            expect(account.publishing_composers.where(is_primary_composer: true, person_id: nil).first.account_id).to eq account.id
            expect(person.publishing_composers.first.publishing_role_id).to eq PublishingRole::ON_BEHALF_OF_SELF_ID
          end
        end

        context "and the account manager has a publishing_composer" do
          it "should not create a publishing_composer associated to the account" do
            params["publishing_role_id"] = PublishingRole.find_by(title: "manager").id
            account.person.publishing_composers << create(:publishing_composer)
            enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
            expect{ enrollment_form.save }.to change(PublishingComposer, :count).by(1)
          end
        end
      end

      context "when the publishing_composer does NOT have a rights app account" do
        it "should not invoke the Publishing Writer Worker" do
          expect(PublishingAdministration::PublishingWriterCreationWorker).not_to receive(:perform_async)
          enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
          enrollment_form.save
        end
      end

      context "when the publishing_composer has a rights app account" do
        let(:publishing_composer) do
          create(
            :publishing_composer,
            :with_pub_admin_purchase,
            person: person,
            account: account,
            provider_identifier: "12345"
          )
        end

        it "should invoke the Publishing Writer Worker if the publishing_composer has a provider_identifier and is 'active'" do
          params["id"] = publishing_composer.id
          enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)

          allow(PublishingAdministration::PublishingCompositionCreationWorker).to receive(:perform_async)
          expect(PublishingAdministration::PublishingWriterCreationWorker).to receive(:perform_async).with(publishing_composer.id)

          enrollment_form.save
        end

        it "should invoke the Publishing Artist Account Writer Worker when the publishing_composer does not have a
          provider_identifier but is 'active'" do
          publishing_composer.update(provider_identifier: nil)
          params["id"] = publishing_composer.id
          enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)

          allow(PublishingAdministration::PublishingCompositionCreationWorker).to receive(:perform_async)
          expect(PublishingAdministration::PublishingArtistAccountWriterWorker).to receive(:perform_async).with(publishing_composer.id)

          enrollment_form.save
        end

        it "should not invoke the Publishing Writer Worker if the publishing_composer is not 'active'" do
          other_composer = create(:publishing_composer, person: person, account: account)
          params["id"] = other_composer.id
          enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)

          expect(PublishingAdministration::PublishingWriterCreationWorker).not_to receive(:perform_async).with(other_composer.id)
          enrollment_form.save
        end

        # Hotfix removed this functionality for now as it is causing thousands of compositions to be created
        # This will be reenabled in a high priority ticket soon
        xit "should invoke the Publishing Composition Creation Worker if the publishing_composer becomes active" do
          params["id"] = publishing_composer.id
          enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)

          allow(PublishingAdministration::PublishingWriterCreationWorker).to receive(:perform_async)
          expect(PublishingAdministration::PublishingCompositionCreationWorker).to receive(:perform_async).with(publishing_composer.id)

          enrollment_form.save
        end
      end
    end

    context "current_user is present" do
      it "shouldn't let non-admins edit preexisting fields" do
        account = create(:account)
        publisher = create(:publisher, name: "May Field", cae: "0987654321")
        publishing_composer = create(:publishing_composer, account: account, publisher: publisher)
        orig_dob = publishing_composer.dob

        params = {
          first_name:         publishing_composer.first_name,
          middle_name:        publishing_composer.middle_name,
          last_name:          publishing_composer.last_name,
          composer_pro_id:    publishing_composer.performing_rights_organization_id,
          composer_cae:       publishing_composer.cae,
          publishing_role_id: "5",
          dob_m:              "3",
          dob_d:              "26",
          dob_y:              "1990",
          email:              publishing_composer.email,
          person_id:          account.person_id,
          publisher_name:     "Broken Records LLC.",
          publisher_cae:      "1234567890"
        }.with_indifferent_access

        enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
        enrollment_form.save

        expect(publishing_composer.dob).to eq(orig_dob)
        expect(publishing_composer.publisher.name).to eq("May Field")
        expect(publishing_composer.publisher.cae).to eq("0987654321")
      end
    end
  end

  describe ".status" do
    it "returns the publishing_composer's status" do
      account = create(:account)
      publishing_composer = create(:publishing_composer, account: account)
      params = {
        name_prefix:        publishing_composer.name_prefix,
        first_name:         publishing_composer.first_name,
        middle_name:        publishing_composer.middle_name,
        last_name:          publishing_composer.last_name,
        name_suffix:        publishing_composer.name_suffix,
        composer_pro_id:    publishing_composer.performing_rights_organization_id,
        composer_cae:       publishing_composer.cae,
        publishing_role_id: "5",
        email:              publishing_composer.email,
        person_id:          account.person_id,
        tos_agreed: "1"
      }.with_indifferent_access

      enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
      expect(enrollment_form.status).to eq(:pending)
    end
  end

  describe ".submittable?" do
    it "returns true if a PRO and CAE are present" do
      account = create(:account)
      publishing_composer = create(:publishing_composer, :skip_cae_validation, account: account, cae: "123456789")
      params = {
        name_prefix:        publishing_composer.name_prefix,
        first_name:         publishing_composer.first_name,
        middle_name:        publishing_composer.middle_name,
        last_name:          publishing_composer.last_name,
        name_suffix:        publishing_composer.name_suffix,
        composer_pro_id:    "56",
        composer_cae:       publishing_composer.cae,
        publishing_role_id: "5",
        email:              publishing_composer.email,
        person_id:          account.person_id,
        tos_agreed: "1"
      }.with_indifferent_access

      enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
      expect(enrollment_form.submittable?).to be true
    end

    it "returns true if a PRO and CAE are NOT present" do
      account = create(:account)
      publishing_composer = create(:publishing_composer, account: account, cae: nil)
      params = {
        name_prefix:        publishing_composer.name_prefix,
        first_name:         publishing_composer.first_name,
        middle_name:        publishing_composer.middle_name,
        last_name:          publishing_composer.last_name,
        name_suffix:        publishing_composer.name_suffix,
        composer_pro_id:    "",
        composer_cae:       publishing_composer.cae,
        publishing_role_id: "5",
        email:              publishing_composer.email,
        person_id:          account.person_id,
        tos_agreed: "1"
      }.with_indifferent_access

      enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
      expect(enrollment_form.submittable?).to be true
    end

    it "returns false if only one of PRO or CAE is present" do
      account = create(:account)
      publishing_composer = create(:publishing_composer, account: account, cae: nil)
      params = {
        name_prefix:        publishing_composer.name_prefix,
        first_name:         publishing_composer.first_name,
        middle_name:        publishing_composer.middle_name,
        last_name:          publishing_composer.last_name,
        name_suffix:        publishing_composer.name_suffix,
        composer_pro_id:    "32",
        composer_cae:       publishing_composer.cae,
        publishing_role_id: "5",
        email:              publishing_composer.email,
        person_id:          account.person_id,
        tos_agreed: "1"
      }.with_indifferent_access

      enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
      expect(enrollment_form.submittable?).to be false
    end
  end

  describe "#set_publisher" do
    context "when a publishing_composer adds a new publisher" do
      it "should send the lod mail to the user and should set added_publisher to true" do
        account = create(:account)
        publishing_composer = create(:publishing_composer, :skip_cae_validation, account: account)

        params = {
          first_name: publishing_composer.first_name,
          last_name: publishing_composer.last_name,
          composer_pro_id: "36",
          composer_cae: "123234345",
          publishing_role_id: "1",
          email: person.email,
          person_id: publishing_composer.account.person_id,
          id: publishing_composer.id,
          publisher_cae: "123456789",
          publisher_name: "Deflate Gate",
          tos_agreed: "1"
        }.with_indifferent_access

        expect(DocuSign::LodApi).to receive(:send_lod_mail).with(publishing_composer.account.person, publishing_composer)

        enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
        enrollment_form.save

        expect(enrollment_form.added_publisher).to be_truthy
      end
    end

    context "when a publishing_composer already has a publisher adds a new publisher" do
      it "should not send the lod mail to the user and should set added_publisher to false" do
        account = create(:account)
        publishing_composer = create(:publishing_composer, :with_publisher, account: account)

        params = {
          first_name: publishing_composer.first_name,
          last_name: publishing_composer.last_name,
          composer_pro_id: "36",
          publishing_role_id: "1",
          email: person.email,
          person_id: publishing_composer.account.person_id,
          id: publishing_composer.id,
          publisher_cae: "123456789",
          publisher_name: "Deflate Gate",
          tos_agreed: "1"
        }.with_indifferent_access

        expect(DocuSign::LodApi).not_to receive(:send_lod_mail)

        enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(params)
        enrollment_form.save

        expect(enrollment_form.added_publisher).to be_falsy
      end
    end
  end
end
