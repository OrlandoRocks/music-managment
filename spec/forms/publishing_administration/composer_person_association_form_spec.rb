require "rails_helper"

describe PublishingAdministration::ComposerPersonAssociationForm do

  describe "#save" do

    before :each do
      @account  = create(:account)
      @composer = create(:composer, account: @account, person: @account.person)
    end

    context "when validation fails" do

      it "does not update the composer if the person_id doesn't match an existing person" do
        params  = { composer: @composer, person_id: 100000 }
        form    = PublishingAdministration::ComposerPersonAssociationForm.new(params)

        expect { form.save }.not_to change { @composer.reload.person_id }
        expect(form.errors[:person].present?).to be true
      end

      it "does not update the composer if the person_id is alraeady associated to another composer" do
        other_composer  = create(:composer, person: @account.person)
        params          = { composer: @composer, person_id: other_composer.person_id }
        form            = PublishingAdministration::ComposerPersonAssociationForm.new(params)

        expect { form.save }.not_to change { @composer.reload.person_id }
        expect(form.errors[:person].present?).to be true
      end
    end

    context "when the person_id is valid" do

      it "updates the composer's person_id if the person_id is not associated to any other composers" do
        person  = create(:person)
        params  = { composer: @composer, person_id: person.id }
        form    = PublishingAdministration::ComposerPersonAssociationForm.new(params)

        expect { form.save }.to change { @composer.reload.person_id }
        expect(form.errors[:person].present?).to be false
      end
    end
  end

end
