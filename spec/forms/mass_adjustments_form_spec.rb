require "rails_helper"

describe MassAdjustmentsForm, type: :model do
  it { should validate_presence_of(:category) }

  describe "#validate_csv_file" do
    it "should return an error when the file type is not a csv" do
      csv_file = ActionDispatch::Http::UploadedFile.new({
          filename: "mass_adjustment.abc",
          type: "text",
          tempfile: double(:file, read: "person_id,correction_amount\n234,567")
      })
      form = MassAdjustmentsForm.new({file: csv_file, admin_message: "msg1", customer_message: "msg2"})
      form.send(:validate_csv_file)

      expect(form.errors.full_messages).to eq(["Csv file should have .csv extension"])
    end

    it "should return an error when the csv does not have correct headers" do
      csv_file = ActionDispatch::Http::UploadedFile.new({
          filename: "mass_adjustment.csv",
          type: "text/csv",
          tempfile: double(:file, read: "person,correction_amount\n23,4567")
      })
      form = MassAdjustmentsForm.new({file: csv_file, admin_message: "msg1", customer_message: "msg2"})
      form.send(:validate_csv_file)

      expect(form.errors.full_messages).to eq(["Csv file headers should be person_id, correction_amount"])
    end

    it "should parse the valid csv file" do
      csv_file = ActionDispatch::Http::UploadedFile.new({
          filename: "mass_adjustment.csv",
          type: "text/csv",
          tempfile: double(:file, read: StringIO.new("person_id,correction_amount\n9,-4567.5"))
      })
      form = MassAdjustmentsForm.new({file: csv_file, admin_message: "msg1", customer_message: "msg2"})
      form.send(:validate_csv_file)

      expect(form.errors.size).to eq(0)
    end

    context "duplicate request is submitted" do
      it "should return an error" do
        admin_msg = "msg1"
        customer_msg = "msg2"
        file_name = "mass_adjustment.csv"
        last_batch = create(:mass_adjustment_batch, admin_message: admin_msg, customer_message: customer_msg, csv_file_name: file_name)

        csv_file = ActionDispatch::Http::UploadedFile.new({
            filename: "mass_adjustment.csv",
            type: "text/csv",
            tempfile: double(:file, read: StringIO.new("person_id,correction_amount\n9,-4567.5"))
        })
        form = MassAdjustmentsForm.new({file: csv_file, admin_message: "msg1", customer_message: "msg2"})
        form.send(:duplicate_request)

        expect(form.errors.size).to eq(1)
        expect(form.errors.full_messages.first).to eq("Csv file looks like a duplicate request. Please review the data you are submitting")
      end
    end
  end

  describe "#save" do
    before { Timecop.freeze(Time.now) }

    after { Timecop.return }

    it "should upload the csv to s3 bucket" do
      user = create(:person)
      person1 = create(:person, :with_balance, amount: 100)
      person2 = create(:person, :with_balance, amount: 50_000)
      csv = "person_id,correction_amount\n#{person1.person_balance.person_id},-456\n#{person2.person_balance.person_id},50"
      csv_file = ActionDispatch::Http::UploadedFile.new({
                                                            filename: "test.csv",
                                                            type: "text/csv",
                                                            tempfile: double(:file, read: StringIO.new(csv))
                                                        })
      form = MassAdjustmentsForm.new(admin_message: "adm", customer_message: "usr", current_user: user, file: csv_file, category: "Tidal DAP")
      file_name = Time.now.strftime("%Y-%m-%d-%H-%M-%S-%L") + ".csv"

      # Mocking the write from s3 bucket
      allow_any_instance_of(AWS::S3).to receive(:buckets) {
        OpenStruct.new({
            ENV["MASS_BALANCE_ADJUSTMENT_BUCKET"] => OpenStruct.new({
                "objects" => {
                    "test/#{file_name}" => double(write: true)
                }
            })
        })
      }

      expect {
        form.send(:save)
      }.to change {MassAdjustmentBatch.count}.by(1)

      mass_adjustment_batch = MassAdjustmentBatch.last
      expect(mass_adjustment_batch.admin_message).to eq("adm")
      expect(mass_adjustment_batch.customer_message).to eq("usr")
      expect(mass_adjustment_batch.csv_file_name).to eq("test.csv")
      expect(mass_adjustment_batch.created_by_id).to eq(user.id)
      expect(mass_adjustment_batch.balance_adjustment_category_id).to eq(8)
    end
  end
end
