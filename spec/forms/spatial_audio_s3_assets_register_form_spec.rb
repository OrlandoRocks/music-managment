require "rails_helper"

describe SpatialAudioS3AssetsRegisterForm do
  let(:person) { create(:person) }
  let(:album)  { create(:album, person: person) }
  let(:song)   { create(:song, album: album, name: "BOOM!") }
  let(:song2)  { create(:song, album: album, name: "BAM!") }

  describe "#valid?" do
    it "is false if missing params" do
      params = { song_id: song.id, person: person }

      form = SpatialAudioS3AssetsRegisterForm.new(params)

      expect(form.valid?).to eq false
    end

    it "is false if song doesnt belong to person" do
      nother_song = create(:song)

      params = { song_id: nother_song.id, person: person }

      form = SpatialAudioS3AssetsRegisterForm.new(params)

      expect(form.valid?).to eq false
    end

    it "is false if orig_filename has four byte chars" do
      nother_song = create(:song)

      params = { song_id: nother_song.id, person: person, orig_filename: "💿💿💿💿💿" }

      form = SpatialAudioS3AssetsRegisterForm.new(params)
      form.save
      expect(form.errors.messages[:orig_filename].size).to eq 1
    end
  end

  describe "#register" do
    it "should create an s3 asset and update the song with the s3 asset id" do
      expect(song.spatial_audio_asset&.id).to be_blank

      params = {
        song_id: song.id,
        s3_asset: { bucket: "bucket", key: "key", uuid: "uuid" },
        upload: { orig_filename: "1-Boom.wav", song_id: song.id, bit_rate: "123kbps", duration: 1000 }
      }

      formatted_params = params[:upload].merge(params[:s3_asset]).merge(person: person)
      form             = SpatialAudioS3AssetsRegisterForm.new(formatted_params)

      expect { form.save }.to change(S3Asset, :count).by(1)
      expect(song.reload.spatial_audio_asset&.id).not_to be_blank
      expect(song.spatial_audio_asset.bucket).to eq "bucket"
      expect(song.spatial_audio_asset.key).to    eq "key"
      expect(song.spatial_audio_asset.uuid).to   eq "uuid"
    end

    context "when there is an existing spatial_audio asset" do
      let(:spatial_audio_asset) { create(:s3_asset, key: "old_key", bucket: "old_bucket") }
      let(:spatial_audio) { create(:spatial_audio, s3_asset_id: spatial_audio_asset.id) }

      before do
        song.spatial_audio_asset = spatial_audio_asset
      end

      it "updates the s3_asset record spatial_audio_asset" do
        with_rails_env(:development) do
          params = {
            song_id: song.id,
            s3_asset: { bucket: "bucket", key: "key/test", uuid: "uuid" },
            upload: { orig_filename: "1-Boom.wav", song_id: song.id, bit_rate: "123kbps" }
          }

          formatted_params = params[:upload].merge(params[:s3_asset]).merge(person: person)
          form             = SpatialAudioS3AssetsRegisterForm.new(formatted_params)

          form.save
          expect(spatial_audio_asset.reload.key).to eq(params[:s3_asset][:key])
          expect(spatial_audio_asset.reload.bucket).to eq(params[:s3_asset][:bucket])
        end
      end
    end

    context "when environment is production" do

      context "when feature flag is set" do
        it "calls AssetCleanupWorker when an old asset is present" do
          with_rails_env(:production) do

            spatial_audio_asset = create(:s3_asset, key: "s3_asset_key", bucket: "s3_asset_bucket")
            song3 = create(:song, album: album, name: "BOOM!")
            song3.spatial_audio_asset = spatial_audio_asset

            params = {
              song_id: song3.id,
              s3_asset: { bucket: "bucket", key: "key/test", uuid: "uuid" },
              upload: { orig_filename: "1-Boom.wav", song_id: song3.id, bit_rate: "123kbps" }
            }

            allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
            formatted_params = params[:upload].merge(params[:s3_asset]).merge(person: person)
            form             = SpatialAudioS3AssetsRegisterForm.new(formatted_params)

            expect(AssetCleanupWorker).to receive(:perform_async).with("s3_asset_key", "s3_asset_bucket")

            form.save
          end  
        end

        it "does not call AssetCleanupWorker when old assets are not present" do
          with_rails_env(:production) do

            song3 =  create(:song, album: album, name: "BOOM!")

            params = {
              song_id: song3.id,
              s3_asset: { bucket: "bucket", key: "key/test", uuid: "uuid" },
              upload: { orig_filename: "1-Boom.wav", song_id: song3.id, bit_rate: "123kbps" }
            }

            allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
            formatted_params = params[:upload].merge(params[:s3_asset]).merge(person: person)
            form             = SpatialAudioS3AssetsRegisterForm.new(formatted_params)

            expect(AssetCleanupWorker).to_not receive(:perform_async)

            form.save
          end
        end
      end

      context "when feature flag is not set" do
        it "does not call AssetCleanupWorker" do
          s3_asset = create(:s3_asset, key: "s3_asset_key", bucket: "s3_asset_bucket")
          s3_orig_asset = create(:s3_asset, key: "s3_orig_asset_key", bucket: "s3_orig_asset_bucket")
          s3_flac_asset = create(:s3_asset, key: "s3_flac_asset_key", bucket: "s3_flac_asset_bucket")
          song3 =  create(
            :song, album: album, name: "BOOM!", s3_asset_id: s3_asset.id,
                   s3_flac_asset_id: s3_flac_asset.id, s3_orig_asset_id: s3_orig_asset.id
          )

          params = {
            song_id: song3.id,
            s3_asset: { bucket: "bucket", key: "key/test", uuid: "uuid" },
            upload: { orig_filename: "1-Boom.wav", song_id: song3.id, bit_rate: "123kbps" }
          }

          allow(FeatureFlipper).to receive(:show_feature?).and_return(false)
          formatted_params = params[:upload].merge(params[:s3_asset]).merge(person: person)
          form             = SpatialAudioS3AssetsRegisterForm.new(formatted_params)

          expect(AssetCleanupWorker).to_not receive(:perform_async)

          form.save
        end
      end
    end

    context "when environment is non production" do
      it "does not call AssetCleanupWorker" do
        with_rails_env(:staging) do

          s3_asset = create(:s3_asset, key: "s3_asset_key", bucket: "s3_asset_bucket")
          s3_orig_asset = create(:s3_asset, key: "s3_orig_asset_key", bucket: "s3_orig_asset_bucket")
          s3_flac_asset = create(:s3_asset, key: "s3_flac_asset_key", bucket: "s3_flac_asset_bucket")
          song3 =  create(
            :song, album: album, name: "BOOM!", s3_asset_id: s3_asset.id,
                  s3_flac_asset_id: s3_flac_asset.id, s3_orig_asset_id: s3_orig_asset.id
          )

          params = {
            song_id: song3.id,
            s3_asset: { bucket: "bucket", key: "key/test", uuid: "uuid" },
            upload: { orig_filename: "1-Boom.wav", song_id: song3.id, bit_rate: "123kbps" }
          }

          allow(FeatureFlipper).to receive(:show_feature?).and_return(false)
          formatted_params = params[:upload].merge(params[:s3_asset]).merge(person: person)
          form             = SpatialAudioS3AssetsRegisterForm.new(formatted_params)

          expect(AssetCleanupWorker).to_not receive(:perform_async)

          form.save
        end
      end
    end

    it "should not create an s3_asset model if it will not be able to save the song" do
      song.optional_isrc = "ABC123456789"
      song.save

      song2.optional_isrc = "ABC123456789"
      song2.save(validate: false)

      params = {
        song_id: song.id,
        s3_asset: { bucket: "bucket", key: "key", uuid: "uuid" },
        upload: { orig_filename: "1-Boom.wav", song_id: song.id, bit_rate: "123kbps" }
      }

      formatted_params = params[:upload].merge(params[:s3_asset]).merge(person: person)
      form             = SpatialAudioS3AssetsRegisterForm.new(formatted_params)

      expect { expect { form.save }.not_to change(S3Asset, :count) }
    end

    it "should update the s3_asset on re-upload" do
      expect(song.spatial_audio_asset&.id).to be_blank

      params = {
        song_id: song.id,
        s3_asset: { bucket: "bucket", key: "key", uuid: "uuid" },
        upload: { orig_filename: "1-Boom.wav", song_id: song.id, bit_rate: "123kbps" }
      }

      formatted_params = params[:upload].merge(params[:s3_asset]).merge(person: person)
      form             = SpatialAudioS3AssetsRegisterForm.new(formatted_params)

      expect { form.save }.to change(S3Asset, :count).by(1)
      expect(song.reload.spatial_audio_asset&.id).not_to be_blank
      expect(song.spatial_audio_asset.bucket).to eq("bucket")
      expect(song.spatial_audio_asset.key).to    eq("key")
      expect(song.spatial_audio_asset.uuid).to   eq("uuid")

      params = {
        song_id: song.id,
        s3_asset: { bucket: "bucket", key: "key2", uuid: "uuid2" },
        upload: { orig_filename: "2-Boom.wav", song_id: song.id, bit_rate: "123kbps" }
      }

      formatted_params = params[:upload].merge(params[:s3_asset]).merge(person: person)
      form             = SpatialAudioS3AssetsRegisterForm.new(formatted_params)

      expect { form.save }.not_to change(S3Asset, :count)
      expect(song.reload.spatial_audio_asset&.id).not_to be_blank
      expect(song.spatial_audio_asset.bucket).to eq("bucket")
      expect(song.spatial_audio_asset.key).to    eq("key2")
      expect(song.spatial_audio_asset.uuid).to   eq("uuid2")
    end
  end
end
