require "rails_helper"

describe MassTakedownForm do
  describe "#save" do
    let(:admin)      { create(:person, :admin) }
    let(:ip_address) { "127.0.0.1" }

    context "when missing arguments" do
      it "adds an error message when missing selected_stores" do
        params = { album_ids: "1", admin: 1 }
        mass_takedown_form = MassTakedownForm.new(params)
        mass_takedown_form.save
        expect(mass_takedown_form.errors.full_messages).to eq ["Stores must have at least 1 selection made"]
      end

      it "adds an error message when missing album_ids" do
        params = { album_level_stores: [26], admin: 1 }
        mass_takedown_form = MassTakedownForm.new(params)
        mass_takedown_form.save
        expect(mass_takedown_form.errors.full_messages).to eq ["Album ids can't be blank"]
      end
    end

    context "when taking down a release from ALL stores" do
      let(:album)              { create(:album, :live) }
      let(:mass_takedown_form) { MassTakedownForm.new(params) }
      let(:params) do
        {
          album_level_stores: ["all"],
          admin: admin,
          album_ids: album.id.to_s,
          ip_address: ip_address
        }
      end

      before(:each) do
        create(:petri_bundle, album: album)
      end

      it "sets the album takedown_at date" do
        expect(album.takedown_at).to be_nil
        mass_takedown_form.save
        expect(album.reload.takedown_at).not_to be_nil
      end

      it "creates an album takedown note" do
        mass_takedown_form.save
        note = album.reload.notes.first
        expect(note.related_id).to eq album.id
        expect(note.subject).to eq "Mass Album takedown"
        expect(note.note).to eq "Album was taken down"
      end
    end

    context "when taking down a release from an individual store" do
      let(:album)              { create(:album, :live) }
      let(:salepoint)          { create(:salepoint, salepointable: album) }
      let(:mass_takedown_form) { MassTakedownForm.new(params) }
      let(:params) do
        {
          album_level_stores: ["26"],
          admin: admin,
          album_ids: album.id.to_s,
          ip_address: ip_address
        }
      end

      before(:each) do
        album.salepoints << salepoint
      end

      it "sets the salepoint takedown_at date" do
        expect(salepoint.takedown_at).to be_nil
        mass_takedown_form.save
        expect(salepoint.reload.takedown_at).not_to be_nil
      end

      it "creates an salepoint takedown note" do
        mass_takedown_form.save
        note = album.reload.notes.first
        expect(note.related_id).to eq album.id
        expect(note.subject).to eq "Mass Album takedown"
        expect(note.note).to eq "Album was taken down from #{salepoint.store.name}"
      end

      it 'sends sns notification for these stores' do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
        expect(Sns::Notifier).to receive(:perform).with(
          topic_arn: ENV.fetch('SNS_RELEASE_TAKEDOWN_TOPIC'),
          album_ids_or_upcs: [album.id],
          store_ids: [26],
          delivery_type: 'takedown',
          person_id: admin.id
        )
        mass_takedown_form.save
      end
    end

    context "when taking down a Youtube Sound Recording release" do
      let(:album)              { create(:album, :live, :with_uploaded_song) }
      let(:song)               { album.songs.first }
      let(:ytsr_store_id)      { Store.find_by(abbrev: "ytsr").id }
      let(:mass_takedown_form) { MassTakedownForm.new(params) }

      let(:params) do
        {
          album_level_stores: [],
          track_level_stores: [ytsr_store_id.to_s],
          admin: admin,
          album_ids: album.id.to_s,
          ip_address: ip_address
        }
      end

      let!(:track_monetization) { create(:track_monetization, song_id: song.id, store_id: ytsr_store_id) }

      it "successfully takes down youtube track monetizations for an album" do
        mass_takedown_form.save
        expect(track_monetization.reload.takedown_at.present?).to be(true)
        expect(track_monetization.reload.delivery_type).to eq("metadata_only")
      end

      context "when ALL stores is selected" do
        let(:params) do
          {
            album_level_stores: ["all"],
            track_level_stores: [ytsr_store_id.to_s],
            admin: admin,
            album_ids: album.id.to_s,
            ip_address: ip_address
          }
        end

        before(:each) do
          create(:petri_bundle, album: album)
        end

        it "does not create a ytsr track takedown note for each song on the album" do
          expect(song.notes).to be_empty
          mass_takedown_form.save
          expect(song.reload.notes).to be_empty
        end
      end

      context "when ALL stores is NOT selected" do
        let(:params) do
          {
            album_level_stores: ["26"],
            track_level_stores: [ytsr_store_id.to_s],
            admin: admin,
            album_ids: album.id.to_s,
            ip_address: ip_address
          }
        end

        it "does not create a ytsr track takedown note for each song on the album" do
          expect(song.notes).to be_empty
          mass_takedown_form.save
          expect(song.reload.notes).to be_empty
        end
      end
    end

    context "when taking down a fb tracks release" do
      let(:album)              { create(:album, :live, :with_uploaded_song) }
      let(:song)               { album.songs.first }
      let(:fbtracks_store_id)  { Store.find_by(abbrev: "fbt").id }

      let(:params) do
        {
            album_level_stores: [],
            track_level_stores: [fbtracks_store_id.to_s],
            admin: admin, album_ids: album.id.to_s, ip_address: ip_address
        }
      end

      let(:mass_takedown_form) { MassTakedownForm.new(params) }
      let!(:track_monetization) { create(:track_monetization, song_id: song.id, person_id: album.person.id) }

      it "successfully takes down fb track monetizations for an album" do
        mass_takedown_form.save
        expect(track_monetization.reload.takedown_at.present?).to be(true)
        expect(track_monetization.reload.delivery_type).to eq("metadata_only")
      end
    end

    context "when taking down a fb/ig tracks release" do
      let(:album)              { create(:album, :live, :with_uploaded_song) }
      let(:song)               { album.songs.first }
      let(:fbtracks_store_id)  { Store.find_by(abbrev: "fbt").id }
      let(:ig_store_id)        { Store.find_by(abbrev: "ig").id }
      let!(:track_monetization) { create(:track_monetization, song_id: song.id, person_id: album.person.id) }
      let(:salepoint) { create(:salepoint, salepointable: album, store_id: ig_store_id) }

      before(:each) do
        album.salepoints << salepoint
      end

      let(:params) do
        {
            album_level_stores: [],
            track_level_stores: [fbtracks_store_id.to_s],
            admin: admin, album_ids: album.id.to_s, ip_address: ip_address
        }
      end

      let(:mass_takedown_form) { MassTakedownForm.new(params) }

      it "successfully takes down fb/ig for an album" do
        mass_takedown_form.save
        expect(track_monetization.reload.takedown_at.present?).to be(true)
        expect(track_monetization.reload.delivery_type).to eq("metadata_only")
        expect(salepoint.reload.takedown_at).not_to be_nil
      end
    end

    context "when the 'Send Email' checkbox is NOT checked" do
      let(:album)              { create(:album, :live) }
      let(:mass_takedown_form) {  MassTakedownForm.new(params) }
      let(:params) do
        {
          album_level_stores: ["26"],
          admin: admin,
          album_ids: album.id.to_s,
          ip_address: ip_address,
          send_email: '0',
        }
      end
      it "does NOT send the user an email" do
        expect(TakedownNotifier).not_to receive(:takedown_notification)
        mass_takedown_form.save
      end
    end

    context "when the 'Send Email' checkbox is checked" do
      let(:album)              { create(:album, :live) }
      let(:mass_takedown_form) {  MassTakedownForm.new(params) }
      let(:params) do
        {
          album_level_stores: ["26"],
          send_email: 1,
          admin: admin,
          album_ids: album.id.to_s,
          ip_address: ip_address,
          template_type: 'Default'
        }
      end

      it "sends the user an email" do
        expect(TakedownNotifier).to receive(:takedown_notification)
          .with(album.person, [{ album: album , stores: [] }]).and_call_original
        mass_takedown_form.save
      end
    end
  end
end
