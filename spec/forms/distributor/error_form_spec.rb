require "rails_helper"
include ActiveSupport::Testing::TimeHelpers

describe Distributor::ErrorForm do
  let(:distro_error_form) { Distributor::ErrorForm.new(params) }
  let(:params)            { { } }

  describe "#save" do
    it "should call query for errored_releases" do
      expect(distro_error_form).to receive(:query_for_errored_releases)
      distro_error_form.save
    end
  end

  describe '#error_type' do
    before do
      allow_any_instance_of(described_class).to receive(:query_for_errored_releases)
    end

    it 'return error_types' do
      ERROR_TYPES = [
        ['All States', 'All'],
        ['Asset Ingestion Error', 'asset_ingestion'],
        ["Delivery Error", 'delivery'],
        ["Json Ingestion Error", 'json_ingestion'],
        ["Technical Error", 'technical'],
        ["TC-Distributor Error", 'tc_distributor_error']
      ]

      expect(distro_error_form.error_types).to eq(ERROR_TYPES)
    end
  end

  describe '#stores' do
    it 'return all stores with delivery service as tc_distributor with option of All' do
      stores = Store.where(delivery_service: "tc_distributor").map { |store| [store.name, store.id] }
      stores.unshift(["All", 0])
      create :store, delivery_service: "tunecore"
      expect(distro_error_form.stores).to match_array(stores)
    end
  end

  describe "#start_date" do
    before do
      allow_any_instance_of(described_class).to receive(:query_for_errored_releases)
    end

    context "when a start_date is passed in" do
      let(:start_date) { Date.today - 1 }
      let(:params)     { { start_date: start_date } }

      it "should return the start date" do
        expect(distro_error_form.start_date.to_date).to eq start_date.to_date
      end
    end

    context "when a start_date is not passed in" do
      before do
        travel_to Time.local(2020)
      end

      after do
        travel_back
      end
      it "should return yesterdays date" do
        yesterday = DateTime.now - 1
        expect(distro_error_form.start_date.to_date).to eq yesterday.to_date
      end
    end
  end

  describe "#end_date" do
    before do
      allow_any_instance_of(described_class).to receive(:query_for_errored_releases)
    end

    context "when an end_date is passed in" do
      let(:end_date) { "12/1/2017" }
      let(:params)   { { end_date: end_date } }

      it "should return the end date" do
        expect(distro_error_form.end_date.to_date).to eq Date.new(2017, 12, 1)
      end
    end

    context "when a end_date is not passed in" do
      before do
        travel_to Time.local(2020)
      end

      after do
        travel_back
      end
      it "should return todays date" do
        expect(distro_error_form.end_date.to_date).to eq DateTime.now.to_date
      end
    end
  end

  describe '#query_for_errored_releases' do
    it 'should call failed_deliveries API and set instance variables' do
      expect(DistributorAPI::ReleaseError).to receive(:failed_deliveries).with({
        end_date: DateTime.strptime("12/3/2017", "%m/%d/%Y"),
        start_date: DateTime.strptime("12/1/2017", "%m/%d/%Y"),
        limit: 21000,
        store_id: 4,
        error_type: "tc_distributor_error"
      }).and_return(JSON.parse([
        {
          http_body: {
            total: 3,
            failed_deliveries: [{
              id: 1
            }, {
              id: 2
            }]
          }
        }].to_json, object_class: OpenStruct)
      )

      Distributor::ErrorForm.new({
        start_date: "12/1/2017",
        end_date: "12/3/2017",
        error_type: "tc_distributor_error",
        store_id: 4
      }).query_for_errored_releases
    end
  end
end
