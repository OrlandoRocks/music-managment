require "rails_helper"

describe TrackMonetizationJudgementForm do
  describe "#save" do
    context "when missing arguments" do
      it "adds an error message when missing a track_monetizations hash param" do
        params = { track_monetizations: "" }

        track_monetization_judgement_form = TrackMonetizationJudgementForm.new(params)

        track_monetization_judgement_form.save

        expect(track_monetization_judgement_form.errors.full_messages).to eq ["Track monetizations can't be blank"]
      end

      it "does not call its #judge_track_monetizations method" do
        params = { track_monetizations: "" }

        track_monetization_judgement_form = TrackMonetizationJudgementForm.new(params)

        expect(track_monetization_judgement_form).to_not receive(:judge_track_monetizations)

        track_monetization_judgement_form.save
      end
    end

    context "when passed in all required arguments" do
      it "calls its #judge_track_monetizations method" do
        track_monetization_1 = create(:track_monetization)
        track_monetization_2 = create(:track_monetization)
        track_monetization_3 = create(:track_monetization)

        params = {
          track_monetizations: {
            track_monetization_1.id.to_s => { "eligibility_status" => "eligible" },
            track_monetization_2.id.to_s => { "eligibility_status" => "ineligible" },
            track_monetization_3.id.to_s => { "eligibility_status" => "pending" },
          }
        }

        track_monetization_judgement_form = TrackMonetizationJudgementForm.new(params)

        expect(track_monetization_judgement_form).to receive(:judge_track_monetizations)

        track_monetization_judgement_form.save
      end
    end
  end
end
