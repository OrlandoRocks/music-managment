require 'rails_helper'

describe Admin::RecoverUserAssetsForm do
  let(:person1) { create(:person) }
  let(:person2) { create(:person) }
  let(:album1) { create(:album, person: person2) }
  let(:album2) { create(:album, person: person2) }
  let(:album3) { create(:album, person: person1) }

  context "save" do
    it "should save and create recover asset for one album" do
      allow(RecoverUserAssetsWorker).to receive(:perform_async).and_return(true)
      form =  Admin::RecoverUserAssetsForm.new(
        person: person2,
        admin: person1,
        album_ids_and_isrcs: "#{album1.id}",
        include_assets: ["metadata"]
      )
      expect{form.save}.to change(RecoverAsset, :count).to(1)
      expect(form.save).to eq(true)
    end

    it "should save and create recover asset for all of the user albums" do
      allow(RecoverUserAssetsWorker).to receive(:perform_async).and_return(true)
      form =  Admin::RecoverUserAssetsForm.new(
        person: person2,
        admin: person1,
        album_ids_and_isrcs: '',
        include_assets: ["metadata"]
      )
      expect{form.save}.to change(RecoverAsset, :count).to(1)
      expect(form.save).to eq(true)
      expect(form.instance_variable_get(:@recover_asset).albums_ids).to eq([])
      expect(form.instance_variable_get(:@recover_asset).state).to eq('enqueued')
    end

    it "should save and create recover asset for one multiple albums with comma seperated" do
      allow(RecoverUserAssetsWorker).to receive(:perform_async).and_return(true)
      form =  Admin::RecoverUserAssetsForm.new(
        person: person2,
        admin: person1,
        album_ids_and_isrcs: "#{album1.id}, #{album2.id}",
        include_assets: ["metadata"]
      )
      expect{form.save}.to change(RecoverAsset, :count).to(1)
      expect(form.save).to eq(true)
    end

    it "should save and create recover asset for one multiple albums with line seperated" do
      allow(RecoverUserAssetsWorker).to receive(:perform_async).and_return(true)
      form =  Admin::RecoverUserAssetsForm.new(
        person: person2,
        admin: person1,
        album_ids_and_isrcs: "#{album1.id}\r\n#{album2.id}",
        include_assets: ["metadata"]
      )
      expect{form.save}.to change(RecoverAsset, :count).to(1)
      expect(form.save).to eq(true)
    end


    it "should save and create recover asset for one multiple albums with space seperated" do
      allow(RecoverUserAssetsWorker).to receive(:perform_async).and_return(true)
      form =  Admin::RecoverUserAssetsForm.new(
        person: person2,
        admin: person1,
        album_ids_and_isrcs: "#{album1.id} #{album2.id}",
        include_assets: ["metadata"]
      )
      expect{form.save}.to change(RecoverAsset, :count).to(1)
      expect(form.save).to eq(true)
    end
  end

  context "validations" do
    it "should not save and return false with error messages for include_assets" do
      form =  Admin::RecoverUserAssetsForm.new(
        person: person2,
        admin: person1,
        album_ids_and_isrcs: "#{album1.id}",
        include_assets: []
      )
      expect(form.save).to eq(false)
      expect(form.errors.details[:include_assets]).to be_present
    end

    it "should not save and return false with error messages for invalid albums" do
      form =  Admin::RecoverUserAssetsForm.new(
        person: person2,
        admin: person1,
        album_ids_and_isrcs: "#{album1.id}, 123",
        include_assets: ['metadata']
      )
      expect(form.save).to eq(false)
      expect(form.errors.details[:album_ids_and_isrcs]).to be_present
    end

    it "should not save and return false with error messages for authorized albums" do
      form =  Admin::RecoverUserAssetsForm.new(
        person: person2,
        admin: person1,
        album_ids_and_isrcs: "#{album1.id}, #{album3.id}",
        include_assets: ['metadata']
      )
      expect(form.save).to eq(false)
      expect(form.errors.details[:album_ids_and_isrcs]).to be_present
    end
  end
end
