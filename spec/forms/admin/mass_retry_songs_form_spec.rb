require 'rails_helper'

describe Admin::MassRetrySongsForm do

  before(:each) do
    allow_any_instance_of(Store).to receive(:delivered_by_tc_distributor?).and_return(false)
  end


  it "retries song distributions en masse, but not for youtube distribution songs" do
    admin = create(:person, :admin)
    album = create(:album, :purchaseable, :paid, :with_salepoint_song, :approved, :taken_down)
    store_ids = Store.mass_retry_song_stores.pluck(:id)
    song = album.songs.last
    tm_store = Store.joins(:store_delivery_config).where(store_delivery_configs: { distributable_class: "TrackMonetization" }, stores: { id: store_ids }).first
    track_monetization = create(:track_monetization, person: album.person, song: song, store: tm_store)

    distribution_worker = double("Delivery::DistributionWorker")
    allow(Delivery::DistributionWorker).to receive(:set).and_return(distribution_worker)

    expect(distribution_worker).to receive(:perform_async).with("TrackMonetization", track_monetization.id, "metadata_only")

    form = Admin::MassRetrySongsForm.new(
      store_ids: store_ids,
      song_ids_and_isrcs: [song.tunecore_isrc],
      admin: admin,
      delivery_type: "metadata_only"
    )
    form.save
  end

  let!(:admin) { create(:person, :admin) }
  let!(:album) { create(:album, :purchaseable, :paid, :with_salepoint_song, :approved, :taken_down) }
  let!(:song1) { album.songs.first }
  let!(:song2) { album.songs.last }


  context "with TC-Distributor delivery" do

    let!(:store) { create(:store, :tc_distributor) }
    let!(:store_delivery_config) { create(:store_delivery_config, store_id: store.id, distributable_class: "TrackMonetization") }
    let!(:track_monetization1) { create(:track_monetization, store_id: store.id, song_id: song1.id) }
    let!(:track_monetization2) { create(:track_monetization, store_id: store.id, song_id: song2.id) }

    it "retries with track_monetization store and feature flag turned off" do
      distribution_worker = class_double("Delivery::DistributionWorker")
      stub_const("Store::TRACK_MONETIZATION_STORES", [store.id])
      stub_const("Store::MASS_RETRY_SONG_STORES", [store.id])

      allow_any_instance_of(TrackMonetization).
        to receive(:tc_distributor_delivery?).
        with(:tc_distributor_track_cutover_users).
        and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).and_return(false)
      allow(Delivery::DistributionWorker).to receive(:set).and_return(distribution_worker)

      form = Admin::MassRetrySongsForm.new(
        store_ids: [store.id],
        song_ids_and_isrcs: [song1.id, song2.tunecore_isrc],
        admin: admin,
        delivery_type: "metadata_only"
      )

      expect(distribution_worker).not_to receive(:perform_async)

      form.save

      expect(track_monetization1.reload.state).to eql("delivered_via_tc_distributor")
      expect(track_monetization2.reload.state).to eql("delivered_via_tc_distributor")
      expect(track_monetization1.reload.tc_distributor_state).to eql("start")
      expect(track_monetization2.reload.tc_distributor_state).to eql("start")
    end

  end

  context "with Tunecore delivery" do

    let!(:store) { create(:store, :tunecore) }
    let!(:store_delivery_config) { create(:store_delivery_config, store_id: store.id, distributable_class: "TrackMonetization") }
    let!(:track_monetization1) { create(:track_monetization, store_id: store.id, song_id: song1.id) }
    let!(:track_monetization2) { create(:track_monetization, store_id: store.id, song_id: song2.id) }

    context "with feature flag off" do

      before(:each) do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(false)
      end

      it "deliver with tc-www" do
        distribution_worker = class_double("Delivery::DistributionWorker")
        stub_const("Store::TRACK_MONETIZATION_STORES", [store.id])
        stub_const("Store::MASS_RETRY_SONG_STORES", [store.id])
        allow(Delivery::DistributionWorker).to receive(:set).and_return(distribution_worker)
  
        form = Admin::MassRetrySongsForm.new(
          store_ids: [store.id],
          song_ids_and_isrcs: [song1.id, song2.tunecore_isrc],
          admin: admin,
          delivery_type: "metadata_only"
        )
  
        expect(distribution_worker).to receive(:perform_async).with("TrackMonetization", track_monetization1.id, "metadata_only")
        expect(distribution_worker).to receive(:perform_async).with("TrackMonetization", track_monetization2.id, "metadata_only")
  
        form.save
  
        expect(track_monetization1.reload.state).to eql("enqueued")
        expect(track_monetization2.reload.state).to eql("enqueued")
        expect(track_monetization1.reload.tc_distributor_state).to eql("start")
        expect(track_monetization2.reload.tc_distributor_state).to eql("start")
      end 

    end

    context "with feature flag on" do

      before(:each) do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
        allow($redis).to receive(:smembers).with(:TC_DISTRIBUTOR_CUTOVER_STORES).and_return(["#{store.id}"])
        allow(FeatureFlipper).to receive(:show_feature?).with(:tc_distributor_track_cutover_users, song1.album.person_id).and_return(true)
      end

      it "retries with track_monetization store and feature flag turned on" do
        distribution_worker = class_double("Delivery::DistributionWorker")
        stub_const("Store::TRACK_MONETIZATION_STORES", [store.id])
        stub_const("Store::MASS_RETRY_SONG_STORES", [store.id])
        allow(Delivery::DistributionWorker).to receive(:set).and_return(distribution_worker)
  
        form = Admin::MassRetrySongsForm.new(
          store_ids: [store.id],
          song_ids_and_isrcs: [song1.id, song2.tunecore_isrc],
          admin: admin,
          delivery_type: "metadata_only"
        )
  
        expect(distribution_worker).not_to receive(:perform_async)
  
        form.save
  
        expect(track_monetization1.reload.state).to eql("delivered_via_tc_distributor")
        expect(track_monetization2.reload.state).to eql("delivered_via_tc_distributor")
        expect(track_monetization1.reload.tc_distributor_state).to eql("start")
        expect(track_monetization2.reload.tc_distributor_state).to eql("start")
      end
  
    end

  end

end
