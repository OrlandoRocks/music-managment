require 'rails_helper'

describe PersonTransaction::ExportForm do
  let(:person) { create(:person, :with_balance, amount: 50_000) }
  let(:transactions) { person.person_transactions }
  let(:form) {
    PersonTransaction::ExportForm.new(
      transactions: person.person_transactions,
      person: person
    )
  }

  let(:csv_headers) {
    ["Date", "Description", "Sales", "Debit", "Credit", "Balance", "Currency"]
  }

  before(:each) do
    5.times do
      create(:person_transaction, person: person)
    end
  end

  it "builds a timestamped filename" do
    Timecop.freeze do
      form.filename = nil

      filename_for_time =  "tunecore-transactions_#{form.localized_time}"
      expect(form.filename).to eq filename_for_time
    end
  end

  context "#build_csv_for_export" do
    it "passes data to PersonTransaction to build the CSV" do
      empty_sales_set = {}
      expect(PersonTransaction).to receive(:generate_csv).with(transactions, empty_sales_set)
      form.csv
    end
  end


end
