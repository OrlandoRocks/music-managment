require "rails_helper"

describe UploadsRegisterForm do
  let(:person) { create(:person) }
  let(:album)  { create(:album, person: person) }
  let(:song)   { create(:song, album: album, name: "BOOM!")}
  let(:song2)  { create(:song, album: album, name: "BAM!")}

  describe "#valid?" do
    it "is false if missing params" do
      params = { song_id: song.id, person: person }

      form = UploadsRegisterForm.new(params)

      expect(form.valid?).to eq false
    end

    it "is false if song doesnt belong to person" do
      nother_song = create(:song)

      params = { song_id: nother_song.id, person: person }

      form = UploadsRegisterForm.new(params)

      expect(form.valid?).to eq false
    end

    it "is false if orig_filename has four byte chars" do
      nother_song = create(:song)

      params = { song_id: nother_song.id, person: person, orig_filename: '💿💿💿💿💿' }

      form = UploadsRegisterForm.new(params)
      form.save
      expect(form.errors.messages[:orig_filename].size).to eq 1
    end

  end

  describe "#register" do
    it "should create an s3 asset and update the song with the s3 asset id" do
      expect(song.s3_asset_id).to be_blank

      params = {
        song_id: song.id,
        s3_asset: {bucket: "bucket", key: "key", uuid: "uuid"},
        upload: {orig_filename: "1-Boom.wav", song_id: song.id, bit_rate: "123kbps", duration: 1000}
      }

      formatted_params = params[:upload].merge(params[:s3_asset]).merge(person: person)
      form             = UploadsRegisterForm.new(formatted_params)

      expect { form.save }.to change(S3Asset, :count).by(1)
      expect(song.reload.s3_asset_id).not_to be_blank
      expect(song.reload.duration_in_seconds).to eq(1)
      expect(song.s3_asset.bucket).to eq "bucket"
      expect(song.s3_asset.key).to    eq "key"
      expect(song.s3_asset.uuid).to   eq "uuid"
    end

    it "should create an upload" do
      expect(song.s3_asset_id).to be_blank

      params = {
        song_id: song.id,
        s3_asset: {bucket: "bucket", key: "key/test", uuid: "uuid"},
        upload: { orig_filename: "1-Boom.wav", song_id: song.id, bit_rate: "123kbps" }
      }

      formatted_params = params[:upload].merge(params[:s3_asset]).merge(person: person)
      form             = UploadsRegisterForm.new(formatted_params)

      expect { form.save }.to change(Upload, :count).by(1)
      expect(song.reload.upload.reload).not_to be_blank
      expect(song.upload.original_filename).to eq("Boom.wav")
      expect(song.upload.uploaded_filename).to eq("test")
    end

    context "when there is an existing flac asset" do
      let(:flac_asset) { create(:s3_asset) }

      before do
        song.update(s3_flac_asset: flac_asset, duration_in_seconds: 200)
      end

      it "should delete the s3 flac asset to nil" do
        params = {
          song_id: song.id,
          s3_asset: {bucket: "bucket", key: "key/test", uuid: "uuid"},
          upload: { orig_filename: "1-Boom.wav", song_id: song.id, bit_rate: "123kbps" }
        }

        formatted_params = params[:upload].merge(params[:s3_asset]).merge(person: person)
        form             = UploadsRegisterForm.new(formatted_params)

        form.save
        expect(song.reload.s3_flac_asset_id).to be_blank
        expect(song.reload.s3_flac_asset).to be_blank
      end

      it "should not set the duration_in_seconds on songs to nil" do
        params = {
          song_id: song.id,
          s3_asset: {bucket: "bucket", key: "key/test", uuid: "uuid"},
          upload: { orig_filename: "1-Boom.wav", song_id: song.id, bit_rate: "123kbps", duration: 3000 }
        }

        formatted_params = params[:upload].merge(params[:s3_asset]).merge(person: person)
        form             = UploadsRegisterForm.new(formatted_params)

        form.save
        expect(song.reload.duration_in_seconds).to eq(3)
      end
    end

    context 'when environment is production' do

      context 'when feature flag is set' do

        it 'calls AssetCleanupWorker when old assets are present' do
          with_rails_env(:production) do
            s3_asset = create(:s3_asset, key: 's3_asset_key', bucket: 's3_asset_bucket')
            s3_orig_asset = create(:s3_asset, key: 's3_orig_asset_key', bucket: 's3_orig_asset_bucket')
            s3_flac_asset = create(:s3_asset, key: 's3_flac_asset_key', bucket: 's3_flac_asset_bucket')
            song3 =  create(:song, album: album, name: "BOOM!", s3_asset_id: s3_asset.id, s3_flac_asset_id: s3_flac_asset.id, s3_orig_asset_id: s3_orig_asset.id)

            params = {
              song_id: song3.id,
              s3_asset: {bucket: "bucket", key: "key/test", uuid: "uuid"},
              upload: { orig_filename: "1-Boom.wav", song_id: song3.id, bit_rate: "123kbps" }
            }

            allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
            formatted_params = params[:upload].merge(params[:s3_asset]).merge(person: person)
            form             = UploadsRegisterForm.new(formatted_params)

            expect(AssetCleanupWorker).to receive(:perform_async).with('s3_flac_asset_key', 's3_flac_asset_bucket')
            expect(AssetCleanupWorker).to receive(:perform_async).with('s3_orig_asset_key', 's3_orig_asset_bucket')
            expect(AssetCleanupWorker).to receive(:perform_async).with('s3_asset_key', 's3_asset_bucket')

            form.save
          end
        end

        it 'calls AssetCleanupWorker even when one of the asset is present' do
          with_rails_env(:production) do

            s3_asset = create(:s3_asset, key: 's3_asset_key', bucket: 's3_asset_bucket')
            song3 =  create(:song, album: album, name: "BOOM!", s3_asset_id: s3_asset.id)

            params = {
              song_id: song3.id,
              s3_asset: {bucket: "bucket", key: "key/test", uuid: "uuid"},
              upload: { orig_filename: "1-Boom.wav", song_id: song3.id, bit_rate: "123kbps" }
            }

            allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
            formatted_params = params[:upload].merge(params[:s3_asset]).merge(person: person)
            form             = UploadsRegisterForm.new(formatted_params)

            expect(AssetCleanupWorker).to receive(:perform_async).with('s3_asset_key', 's3_asset_bucket')
            form.save
          end
        end

        it 'does not call AssetCleanupWorker when old assets are not present' do
          with_rails_env(:production) do

            song3 =  create(:song, album: album, name: "BOOM!")

            params = {
              song_id: song3.id,
              s3_asset: {bucket: "bucket", key: "key/test", uuid: "uuid"},
              upload: { orig_filename: "1-Boom.wav", song_id: song3.id, bit_rate: "123kbps" }
            }

            allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
            formatted_params = params[:upload].merge(params[:s3_asset]).merge(person: person)
            form             = UploadsRegisterForm.new(formatted_params)

            expect(AssetCleanupWorker).to_not receive(:perform_async)

            form.save
          end
        end
      end

      context 'when feature flag is not set' do

        it 'does not call AssetCleanupWorker' do
          with_rails_env(:production) do

            s3_asset = create(:s3_asset, key: 's3_asset_key', bucket: 's3_asset_bucket')
            s3_orig_asset = create(:s3_asset, key: 's3_orig_asset_key', bucket: 's3_orig_asset_bucket')
            s3_flac_asset = create(:s3_asset, key: 's3_flac_asset_key', bucket: 's3_flac_asset_bucket')
            song3 =  create(:song, album: album, name: "BOOM!", s3_asset_id: s3_asset.id, s3_flac_asset_id: s3_flac_asset.id, s3_orig_asset_id: s3_orig_asset.id)

            params = {
              song_id: song3.id,
              s3_asset: {bucket: "bucket", key: "key/test", uuid: "uuid"},
              upload: { orig_filename: "1-Boom.wav", song_id: song3.id, bit_rate: "123kbps" }
            }

            allow(FeatureFlipper).to receive(:show_feature?).and_return(false)
            formatted_params = params[:upload].merge(params[:s3_asset]).merge(person: person)
            form             = UploadsRegisterForm.new(formatted_params)

            expect(AssetCleanupWorker).to_not receive(:perform_async)

            form.save
          end
        end
      end
    end

    context 'when environment is non production' do
      it 'does not call AssetCleanupWorker' do
       
        with_rails_env(:staging) do
          s3_asset = create(:s3_asset, key: 's3_asset_key', bucket: 's3_asset_bucket')
          s3_orig_asset = create(:s3_asset, key: 's3_orig_asset_key', bucket: 's3_orig_asset_bucket')
          s3_flac_asset = create(:s3_asset, key: 's3_flac_asset_key', bucket: 's3_flac_asset_bucket')
          song3 =  create(:song, album: album, name: "BOOM!", s3_asset_id: s3_asset.id, s3_flac_asset_id: s3_flac_asset.id, s3_orig_asset_id: s3_orig_asset.id)

          params = {
            song_id: song3.id,
            s3_asset: {bucket: "bucket", key: "key/test", uuid: "uuid"},
            upload: { orig_filename: "1-Boom.wav", song_id: song3.id, bit_rate: "123kbps" }
          }

          allow(FeatureFlipper).to receive(:show_feature?).and_return(false)
          formatted_params = params[:upload].merge(params[:s3_asset]).merge(person: person)
          form             = UploadsRegisterForm.new(formatted_params)

          expect(AssetCleanupWorker).to_not receive(:perform_async)

          form.save
        end
      end
    end

    it "should not create an upload model if it will not be able to save the song" do
      song.optional_isrc = "ABC123456789"
      song.save

      song2.optional_isrc = "ABC123456789"
      song2.save(validate: false)

      params = {
        song_id: song.id,
        s3_asset: {bucket: "bucket", key: "key", uuid: "uuid"},
        upload: {orig_filename: "1-Boom.wav", song_id: song.id, bit_rate: "123kbps" }
      }

      formatted_params = params[:upload].merge(params[:s3_asset]).merge(person: person)
      form             = UploadsRegisterForm.new(formatted_params)

      expect { expect { form.save }.not_to change(S3Asset, :count) }.not_to change(Upload, :count)
    end

    it "should update the s3_asset on re-upload" do
      expect(song.s3_asset_id).to be_blank

      params =  {
        song_id: song.id,
        s3_asset: {bucket: "bucket", key: "key", uuid: "uuid"},
        upload: {orig_filename: "1-Boom.wav", song_id: song.id, bit_rate: "123kbps" }
      }

      formatted_params = params[:upload].merge(params[:s3_asset]).merge(person: person)
      form             = UploadsRegisterForm.new(formatted_params)

      expect { form.save }.to change(S3Asset, :count).by(1)
      expect(song.reload.s3_asset_id).not_to be_blank
      expect(song.s3_asset.bucket).to eq("bucket")
      expect(song.s3_asset.key).to    eq("key")
      expect(song.s3_asset.uuid).to   eq("uuid")

      params = {
        song_id: song.id,
        s3_asset: {bucket: "bucket", key: "key2", uuid: "uuid2"},
        upload: {orig_filename: "2-Boom.wav", song_id: song.id, bit_rate: "123kbps" }
      }

      formatted_params = params[:upload].merge(params[:s3_asset]).merge(person: person)
      form             = UploadsRegisterForm.new(formatted_params)

      expect { form.save }.not_to change(S3Asset, :count)
      expect(song.reload.s3_asset_id).not_to be_blank
      expect(song.s3_asset.bucket).to eq("bucket")
      expect(song.s3_asset.key).to    eq("key2")
      expect(song.s3_asset.uuid).to   eq("uuid2")
    end
  end
end
