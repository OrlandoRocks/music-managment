require "rails_helper"

describe Distribution::ErrorForm do
  let(:distro_error_form) { Distribution::ErrorForm.new(params) }
  let(:params)            { { } }

  describe "#save" do
    it "should query for erred distributions" do
      expect(Distribution::ErrorQueryBuilder).to receive(:build)
      distro_error_form.save
    end
  end

  describe "#start_date" do
    context "when a start_date is passed in" do
      let(:start_date) { Date.today - 1 }
      let(:params)     { { start_date: start_date } }

      it "should return the start date" do
        expect(distro_error_form.save.start_date.to_date).to eq start_date.to_date
      end
    end

    context "when a start_date is not passed in" do
      it "should return yesterdays date" do
        yesterday = DateTime.now - 1
        expect(distro_error_form.save.start_date.to_date).to eq yesterday.to_date
      end
    end
  end

  describe "#end_date" do
    context "when an end_date is passed in" do
      let(:end_date) { "12/1/2017" }
      let(:params)   { { end_date: end_date } }

      it "should return the end date" do
        expect(distro_error_form.save.end_date.to_date).to eq Date.new(2017, 12, 1)
      end
    end

    context "when a end_date is not passed in" do
      it "should return todays date" do
        expect(distro_error_form.save.end_date.to_date).to eq DateTime.now.to_date
      end
    end
  end

  describe "#filter_type" do
    context "when a filter_type is passed in" do
      let(:params) { { filter_type: "release" } }

      it "should return the filter type" do
        expect(distro_error_form.save.filter_type).to eq "release"
      end
    end

    context "when a filter_type is not passed in" do
      it "should default to 'store'" do
        expect(distro_error_form.save.filter_type).to eq "store"
      end
    end
  end
end
