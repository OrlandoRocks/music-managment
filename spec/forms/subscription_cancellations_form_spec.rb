require 'rails_helper'

describe SubscriptionCancellationForm do
  describe "#save" do
    let(:person) { create(:person, :with_social_subscription_status_and_event_and_purchase) }


    let(:other_person) { create(:person) }
    let(:admin) { create(:person, :with_role, role_name: "Subscription Admin")}

    it "changes and cancels a subscription status" do
      subscription = person.person_subscription_statuses.first
      cancellation_form = SubscriptionCancellationForm.new(
          subscription: subscription, admin: admin
      )
      cancellation_form.save

      expect(subscription.reload.canceled_at).not_to be(nil)
    end

    it "does not approve a tunecore status change from an admin with bad credentials" do
      subscription = person.person_subscription_statuses.first
      cancellation_form = SubscriptionCancellationForm.new(
          subscription: subscription, admin: other_person
      )
      cancellation_form.save

      expect(subscription.canceled_at).to be(nil)
    end
  end
end
