require "rails_helper"

describe TaxTokensCohortForm do
  let(:file) { fixture_file_upload("/tax_tokens_user_list.csv", 'text/csv') }

  let(:tax_tokens_cohort_params) {
    {
      name: "Example Cohort",
      start_date: "2018-10-23",
      end_date: "2018-10-25",
      file: file
    }
  }

  let(:tax_tokens_cohort_update_params) {
    {
      id: 1,
      name: "Example Cohort Edit",
      start_date: "2018-10-23",
      end_date: "2019-10-25"
    }
  }

  let(:tax_tokens_cohort_form) { TaxTokensCohortForm.new(tax_tokens_cohort_params) }

  describe ".save" do
    context "valid" do
      it "creates new tax tokens for the tax tokens cohort" do
        expect{tax_tokens_cohort_form.save}.to change{TaxTokensCohort.count}.by(1)
      end

      it "has a name" do
        expect(tax_tokens_cohort_form.name).to eq("Example Cohort")
      end

      it "has a start and end date" do
        expect(tax_tokens_cohort_form.start_date).to eq("2018-10-23")
      end

      it "has a file to upload if it's a new tax tokens cohort" do
        expect(tax_tokens_cohort_form.end_date).to eq("2018-10-25")
      end
    end
  end

  describe ".update" do

    let(:tax_tokens_cohort_params) {
      {
        name: "Example Cohort",
        start_date: "2018-10-23",
        end_date: "2018-10-25",
        file: file
      }
    }

    let(:tax_tokens_cohort_update_params) {
      {
        name: "Example Cohort Edit",
        start_date: "2018-10-23",
        end_date: "2019-10-25"
      }
    }

    let(:tax_tokens_cohort_form_1) { TaxTokensCohortForm.new(tax_tokens_cohort_params) }

    context "valid" do
      it "updates the name" do
        tax_tokens_cohort_form_1.update(tax_tokens_cohort_update_params)
        expect(tax_tokens_cohort_form_1.tax_tokens_cohort.name).to eq("Example Cohort Edit")
      end

      it "updates the start date" do
        tax_tokens_cohort_form_1.update(tax_tokens_cohort_update_params)
        expect(tax_tokens_cohort_form_1.tax_tokens_cohort.start_date).to eq(Time.parse("2018-10-23"))
      end

      it "updates the end date" do
        tax_tokens_cohort_form_1.update(tax_tokens_cohort_update_params)
        expect(tax_tokens_cohort_form_1.tax_tokens_cohort.end_date).to eq(Time.parse("2019-10-25"))
      end
    end
  end
end
