require 'rails_helper'

describe PayoutTransfer::RejectionForm do
  let(:person) { create(:person, :with_balance, amount: 50_000) }
  let!(:payout_provider) { create(:payout_provider, person: person) }
  let(:other_person) { create(:person) }
  let(:person_balance) { person.person_balance }
  let(:admin) { create(:person, :with_role, role_name: "Payout Service")}
  let(:payout_transfer) do
    create(:payout_transfer,
           person: person,
           tunecore_status: PayoutTransfer::SUBMITTED,
           amount_cents: 100000)
  end

  describe "#valid?" do
    it "returns true if payout transfer tunecore status is pending" do
      form = PayoutTransfer::RejectionForm.new(
        payout_transfer: payout_transfer,
        admin: admin
      )

      expect(form.valid?).to eq true
    end

    it "returns false if not payout transfer" do
      form = PayoutTransfer::RejectionForm.new(
        admin: admin
      )

      expect(form.valid?).to eq false
    end

    it "returns false if not payout transfer" do
      form = PayoutTransfer::RejectionForm.new(
        payout_transfer: payout_transfer,
      )

      expect(form.valid?).to eq false
    end

    it "returns false if payout transfer has wrong status" do
      payout_transfer.tunecore_status = "approved"
      form = PayoutTransfer::RejectionForm.new(
        payout_transfer: payout_transfer,
        admin: admin
      )

      expect(form.valid?).to eq false
      expect(form.errors.full_messages).to eq ["Tunecore status incorrect for transfer ID: #{payout_transfer.id}"]
    end
  end

  describe "#save" do
    it "denies and refunds a user" do
      form = PayoutTransfer::RejectionForm.new(
        payout_transfer: payout_transfer,
        admin: admin
      )

      form.save

      expect(payout_transfer.reload.tunecore_status).to eq(PayoutTransfer::REJECTED)
      expect(payout_transfer.tunecore_processor).to eq(admin)
      expect(payout_transfer.tunecore_processed_at).to be_present
    end
  end
end
