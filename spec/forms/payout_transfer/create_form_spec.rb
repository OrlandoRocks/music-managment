require "rails_helper"

describe PayoutTransfer::CreateForm do

  before(:each) do
    payee_extended_details_stub = double(:payee_extended_details, withdrawal_type: "ACCOUNT", country: "US")
    allow(PayoutProvider::ApiService).to receive_message_chain(:new, :payee_extended_details) { payee_extended_details_stub }
    payee_status_stub = double(:payee_status, withdrawal_type: "ACCOUNT")
    allow(PayoutProvider::ApiService).to receive_message_chain(:new, :payee_status) { payee_status_stub }
  end

  describe ".initialize" do
    let(:person) { create(:person) }
    let(:country) { Country.find_by(iso_code: "US") }

    describe "record payoneer country" do
      context "recognized iso code" do
        it "should record payoneer country" do
          expect(PersonCountry).to receive(:record_country).with(
            PersonCountry.sources[:payoneer],
            person.id,
            country.id
          )

          PayoutTransfer::CreateForm.new(person: person)
        end
      end

      context "invalid iso code" do
        it "should report to Airbrake" do
          invalid_iso_stub = double(:payee_extended_details, country: "Agea")
          airbrake_details = { person: person.sanitize!, payee_details: invalid_iso_stub }
          allow(PayoutProvider::ApiService).to receive_message_chain(:new, :payee_extended_details) { invalid_iso_stub }

          expect(Airbrake).to receive(:notify).with("Unknown ISO code from Payoneer - Agea", airbrake_details)

          PayoutTransfer::CreateForm.new(person: person)
        end
      end
    end
  end

  describe "#save" do
    it "creates a payout" do
      person = create(:person)
      create(:payout_provider, person: person)
      person.person_balance.update_attribute(:balance, "1000.00")
      expect{PayoutTransfer::CreateForm.new(person: person, amount_cents_raw: 1000).save}.to change{PayoutTransfer.count}.by(1)

      payout_transfer = PayoutTransfer.where(payout_provider_id: person.payout_provider.id).last

      expect(payout_transfer.amount_cents).to eq(900)
      expect(payout_transfer.fee_cents).to eq(100)
      expect(payout_transfer.transaction_type).to eq("debit")
      expect(payout_transfer.withdraw_method).to eq("ACCOUNT")
      expect(payout_transfer.person_id).to eq(person.id)
    end

    it "updates a person's balance" do
      person = create(:person)
      create(:payout_provider, person: person)
      person.person_balance.update_attribute(:balance, "1000.00")
      PayoutTransfer::CreateForm.new(person: person, amount_cents_raw: "10000").save
      expect(person.person_balance.balance).to eq(900)
    end

    it "adds an error when a person's balance is not enough to cover the withdrawal" do
      person = create(:person)
      person.person_balance.update_attribute(:balance, "500.00")

      payout_transfer_form = PayoutTransfer::CreateForm.new(person: person, amount_cents_raw: 60000)
      payout_transfer_form.save
      expect(payout_transfer_form.errors[:balance]).to include("unavailable_balance")
    end

    context "with an unknown withdrawal type" do
      it "adds 'unkown_withdrawl_type' to validation errors" do
        person = create(:person)
        unknown_withdrawal_method_stub = double(
          :payee_extended_details,
          withdrawal_type: "TEST_WITHDRAWAL_TYPE",
          country: person.country_website.country
        )

        allow(PayoutProvider::ApiService)
          .to receive_message_chain(:new, :payee_extended_details) { unknown_withdrawal_method_stub }

        payout_transfer_form = PayoutTransfer::CreateForm.new(person: person, amount_cents_raw: 5000)
        payout_transfer_form.save
        expect(payout_transfer_form.errors[:withdrawal_type]).to include("unknown_withdrawal_type")
      end
    end
  end

  describe '#transfer_amount' do
    it 'should return the amount minus the fees as transfer amount' do
      person = create(:person)
      person.person_balance.update_attribute(:balance, '50.00')
      payout_transfer_form = PayoutTransfer::CreateForm.new(person: person, amount_cents_raw: 1000)
      expect(payout_transfer_form.transfer_amount).to eq(Money.new(900))
    end
  end

  describe '#requested_amount' do
    it 'should return the amount the user submitted as money object' do
      person = create(:person)
      person.person_balance.update_attribute(:balance, '50.00')
      payout_transfer_form = PayoutTransfer::CreateForm.new(person: person, amount_cents_raw: 1000)
      expect(payout_transfer_form.requested_amount).to eq(Money.new(1000))
    end
  end

  describe '#available_balance' do
    it 'should return the available balance considering the fees and withdrawal amount' do
      person = create(:person)
      person.person_balance.update_attribute(:balance, '50.00')
      payout_transfer_form = PayoutTransfer::CreateForm.new(person: person, amount_cents_raw: 1000)
      expect(payout_transfer_form.available_balance).to eq(Money.new(4000))
    end
  end

  describe 'transfer_amount_valid?' do
    it 'should return true if the amount that can be transferred is greater than or equal to $1 after fees' do
      person = create(:person)
      person.person_balance.update_attribute(:balance, '10.00')
      payout_transfer_form = PayoutTransfer::CreateForm.new(person: person, amount_cents_raw: 500)
      expect(payout_transfer_form.transfer_amount_valid?).to be_truthy
    end

    it 'should return false if the amount that can be transferred is lesser than $1 after fees' do
      person = create(:person)
      person.person_balance.update_attribute(:balance, '10.00')
      payout_transfer_form = PayoutTransfer::CreateForm.new(person: person, amount_cents_raw: 150)
      expect(payout_transfer_form.transfer_amount_valid?).to be_falsey
    end

    context "for paypal withdrawals" do
      let!(:person) { create(:person, :with_balance, amount: 10) }

      before(:each) do
        payee_extended_details_stub = double(:payee_extended_details, withdrawal_type: "PAYPAL", country: "US")
        allow(PayoutProvider::ApiService).to receive_message_chain(:new, :payee_extended_details) { payee_extended_details_stub }
      end

      it "returns true if the amount that can be transferred is greater than or equal to $2 after fees" do
        person.person_balance.update(balance: "10.00")
        payout_transfer_form = PayoutTransfer::CreateForm.new(person: person, amount_cents_raw: 205)
        expect(payout_transfer_form.transfer_amount_valid?).to be_truthy
      end

      it "returns false if the amount that can be transferred is lesser than $2 after fees" do
        person.person_balance.update(balance: "10.00")
        payout_transfer_form = PayoutTransfer::CreateForm.new(person: person, amount_cents_raw: 200)
        expect(payout_transfer_form.transfer_amount_valid?).to be_falsey
      end
    end
  end
end
