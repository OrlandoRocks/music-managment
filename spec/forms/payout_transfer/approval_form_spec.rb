require 'rails_helper'

describe PayoutTransfer::ApprovalForm do
  describe "#save" do

    # before(:each) do
    #   allow(FeatureFlipper).to receive(:show_feature?).with(:use_payoneer_v4_api).and_return(false)
    # end

    let(:person) { create(:person, :with_balance, amount: 50_000) }
    let(:eu_person) { create(:person, :with_balance, country: 'Luxembourg', amount: 50_000) }
    let!(:payout_provider) { create(:payout_provider, person: person) }
    let(:other_person) { create(:person) }
    let(:person_balance) { person.person_balance }
    let(:admin) { create(:person, :with_role, role_name: "Payout Service")}
    let(:payout_transfer) {
      create(:payout_transfer,
        person: person,
        tunecore_status: PayoutTransfer::SUBMITTED,
        provider_status: PayoutTransfer::NONE,
        amount_cents: 100000
      )
    }
    let(:eu_payout_transfer) {
      create(:payout_transfer,
        person: eu_person,
        tunecore_status: PayoutTransfer::SUBMITTED,
        provider_status: PayoutTransfer::NONE,
        amount_cents: 100000
      )
    }

    it "approves a payout transfer" do
      submit_payout = double(:submit_payout, {success?: true, contents: JSON.dump({payout_id: 123}), description: "details", code: "0"})
      fake_api_service = double(:fake_api_service, submit_payout: submit_payout)
      allow(PayoutProvider::ApiService).to receive(:new).and_return(fake_api_service)

      payout_transfer_approval_form = PayoutTransfer::ApprovalForm.new(payout_transfer: payout_transfer, admin: admin)
      payout_transfer_approval_form.save

      payout_transfer.reload
      expect(payout_transfer.tunecore_status).to eq(PayoutTransfer::APPROVED)
      expect(payout_transfer.provider_status).to eq(PayoutTransfer::REQUESTED)
      expect(payout_transfer.payout_id).to eq("123")
    end

    it "does not approve a payout transfer from an admin with bad credentials" do
      payout_transfer_approval_form = PayoutTransfer::ApprovalForm.new(payout_transfer: payout_transfer, admin: other_person)
      payout_transfer_approval_form.save

      expect(payout_transfer.reload.tunecore_status).not_to eq(PayoutTransfer::APPROVED)
      expect(payout_transfer.reload.provider_status).not_to eq(PayoutTransfer::REQUESTED)
    end

    it "does not approve a payout transfer not in the pending on tunecore state" do
      payout_transfer.update_attribute(:tunecore_status, PayoutTransfer::DECLINED)
      payout_transfer_approval_form = PayoutTransfer::ApprovalForm.new(payout_transfer: payout_transfer, admin: other_person)
      payout_transfer_approval_form.save

      expect(payout_transfer.reload.tunecore_status).not_to eq(PayoutTransfer::APPROVED)
      expect(payout_transfer.reload.provider_status).not_to eq(PayoutTransfer::REQUESTED)
    end

    context "when the payout transfer returns from payoneer with errors" do
      it "does not approve a payout transfer with errors from the service provider" do
        expect(payout_transfer.payout_transactions.size).to eq(0)
        submit_payout = double(:submit_payout, {
            success?: false,
            contents: "{\"audit_id\":1169687819,\"code\":10301,\"description\":\"insufficient_balance\"}",
            description: "insufficient_balance",
            code: "10301"
        })

        fake_api_service = double(:fake_api_service, submit_payout: submit_payout)
        allow(PayoutProvider::ApiService).to receive(:new).and_return(fake_api_service)

        payout_transfer_approval_form = PayoutTransfer::ApprovalForm.new(payout_transfer: payout_transfer, admin: admin)
        payout_transfer_approval_form.save

        expect(payout_transfer.reload.tunecore_status).to eq(PayoutTransfer::REJECTED)
        expect(payout_transfer.reload.provider_status).to eq(PayoutTransfer::ERRORED)
        expect(payout_transfer.payout_transactions.size).to eq(1)
      end

      it "does not process a refund for duplicate payout transfers" do
        expect(payout_transfer.payout_transactions.size).to eq(0)

        # 1st send
        submit_payout = double(:submit_payout, {
            success?: true,
            contents: JSON.dump({payout_id: 123}),
            description: "details",
            code: "0"
        })
        fake_api_service = double(:fake_api_service, submit_payout: submit_payout)
        allow(PayoutProvider::ApiService).to receive(:new).and_return(fake_api_service)

        payout_transfer_approval_form = PayoutTransfer::ApprovalForm.new(payout_transfer: payout_transfer, admin: admin)
        payout_transfer_approval_form.save

        payout_transfer.update(tunecore_status: PayoutTransfer::SUBMITTED, provider_status: PayoutTransfer::NONE)

        # 2nd send
        submit_payout = double(:submit_payout, {
            success?: false,
            contents: "{\"audit_id\":1169687819,\"code\":10304,\"description\":\"Payout already exists\"}",
            description: "Payout already exists",
            code: "10304"
        })

        fake_api_service = double(:fake_api_service, submit_payout: submit_payout)
        allow(PayoutProvider::ApiService).to receive(:new).and_return(fake_api_service)

        payout_transfer_approval_form = PayoutTransfer::ApprovalForm.new(payout_transfer: payout_transfer, admin: admin)
        payout_transfer_approval_form.save

        expect(payout_transfer.reload.tunecore_status).to eq(PayoutTransfer::REJECTED)
        expect(payout_transfer.reload.provider_status).to eq(PayoutTransfer::ERRORED)
        expect(payout_transfer.payout_transactions.size).to eq(0)
      end

      it "EU payout transfer should have an outbound invoice" do
        submit_payout = double(:submit_payout, { success?: true, contents: JSON.dump({ payout_id: 123 }), description: "details", code: "0" })
        fake_api_service = double(:fake_api_service, submit_payout: submit_payout)


        allow(PayoutProvider::ApiService).to receive(:new).and_return(fake_api_service)
        allow_any_instance_of(TcVat::OutboundVatCalculator).to receive(:fetch_vat_info).and_return({ tax_rate: '17.0' })
        allow(FeatureFlipper).to receive(:show_feature?).with(:use_payoneer_v4_api, eu_person).and_return(false)
        allow(FeatureFlipper).to receive(:show_feature?).with(:vat_tax, eu_person).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).with(:auto_approved_withdrawals, eu_person.id).and_return(false)


        payout_transfer_approval_form = PayoutTransfer::ApprovalForm.new(payout_transfer: eu_payout_transfer, admin: admin)
        payout_transfer_approval_form.save

        eu_payout_transfer.reload
        expect(eu_payout_transfer.tunecore_status).to eq(PayoutTransfer::APPROVED)
        expect(eu_payout_transfer.outbound_invoice.user_invoice_prefix).to eq(eu_person.outbound_invoice_prefix)
      end

      it "Non EU payout transfer should not have an outbound invoice" do
        submit_payout = double(:submit_payout, { success?: true, contents: JSON.dump({ payout_id: 123 }), description: "details", code: "0" })
        fake_api_service = double(:fake_api_service, submit_payout: submit_payout)

        allow(PayoutProvider::ApiService).to receive(:new).and_return(fake_api_service)
        allow(FeatureFlipper).to receive(:show_feature?).with(:use_payoneer_v4_api, person).and_return(false)
        allow(FeatureFlipper).to receive(:show_feature?).with(:vat_tax, person).and_return(true)
        allow(FeatureFlipper).to receive(:show_feature?).with(:auto_approved_withdrawals, person.id).and_return(false)


        payout_transfer_approval_form = PayoutTransfer::ApprovalForm.new(payout_transfer: payout_transfer, admin: admin)
        payout_transfer_approval_form.save

        payout_transfer.reload
        expect(payout_transfer.tunecore_status).to eq(PayoutTransfer::APPROVED)
        expect(payout_transfer.outbound_invoice).not_to be_present
      end
    end
  end
end
