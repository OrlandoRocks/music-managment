require 'rails_helper'

describe PayoutTransfer::AdminFilterForm do
  describe "#save" do
    let!(:person) { create(:person, :admin, name: "Devonta Freeman") }
    let!(:person2) { create(:person, :admin, name: "Todd Gurley") }
    let!(:payout_provider)  { create(:payout_provider, person: person) }
    let!(:payout_provider2) { create(:payout_provider, person: person2) }
    let!(:payout_transfer1) { create(:payout_transfer, tunecore_status: PayoutTransfer::SUBMITTED, provider_status: PayoutTransfer::NONE, amount_cents: 3000, payout_provider: payout_provider, withdraw_method: PayoutTransfer::ACCOUNT, withdraw_method_changed: true) }
    let!(:payout_transfer2) { create(:payout_transfer, tunecore_status: PayoutTransfer::SUBMITTED, provider_status: PayoutTransfer::NONE, amount_cents: 5000, payout_provider: payout_provider, withdraw_method: PayoutTransfer::PAYPAL, withdraw_method_changed: true) }
    let!(:payout_transfer3) { create(:payout_transfer, tunecore_status: PayoutTransfer::APPROVED, provider_status: PayoutTransfer::REQUESTED, amount_cents: 8000, payout_provider: payout_provider, withdraw_method: PayoutTransfer::PAPER_CHECK, withdraw_method_changed: true) }
    let!(:payout_transfer4) { create(:payout_transfer, tunecore_status: PayoutTransfer::APPROVED, provider_status: PayoutTransfer::COMPLETED, amount_cents: 10000, payout_provider: payout_provider, withdraw_method: PayoutTransfer::PAPER_CHECK, withdraw_method_changed: false) }
    let!(:payout_transfer5) { create(:payout_transfer, tunecore_status: PayoutTransfer::REJECTED, provider_status: PayoutTransfer::NONE, amount_cents: 10000, payout_provider: payout_provider2, withdraw_method: PayoutTransfer::PAPER_CHECK, withdraw_method_changed: true) }

    it "only returns transfers that have been marked as rolled back" do
      payout_transfer5.update_attribute(:rolled_back, true)
      query_params = {
        rollback: true
      }

      results = Payoneer::PayoutTransferQueryBuilder.build(query_params)

      expect(results.size).to eq(1)
      expect(results.include?(payout_transfer5)).to be(true)
    end

    it "loads in all payout transfers with submitted status with no other filters enabled" do
      form_params = {}

      form = PayoutTransfer::AdminFilterForm.new(form_params)

      expect(form.save).to be_truthy
      expect(form.payout_transfers.size).to eq(5)
    end

    it "loads in payout transfers that match the conditions of the provider status filter" do
      form_params = {
        provider_status:   PayoutTransfer::COMPLETED,
      }

      form = PayoutTransfer::AdminFilterForm.new(form_params)

      expect(form.save).to be_truthy
      expect(form.payout_transfers.size).to eq(1)
      expect(form.payout_transfers.first).to eq(payout_transfer4)
    end

    it "loads in payout transfers that match the conditions of the tunecore status filter" do
      form_params = {
        tunecore_status:   PayoutTransfer::REJECTED,
      }

      form = PayoutTransfer::AdminFilterForm.new(form_params)

      expect(form.save).to be_truthy
      expect(form.payout_transfers.size).to eq(1)
      expect(form.payout_transfers.first).to eq(payout_transfer5)
    end

    it "loads in payout transfers that match the conditions of the withdrawal method filter" do
      form_params = {
        withdrawal_method: PayoutTransfer::PAYPAL
      }

      form = PayoutTransfer::AdminFilterForm.new(form_params)

      expect(form.save).to be_truthy

      expect(form.payout_transfers.size).to eq(1)
      expect(form.payout_transfers.first).to eq(payout_transfer2)
    end

    it "loads in payout transfers that match the conditions of the > filter" do
      form_params = {
        filter_amount:     "95.00",
        filter_type:       ">"
      }

      form = PayoutTransfer::AdminFilterForm.new(form_params)

      expect(form.save).to be_truthy
      expect(form.payout_transfers.size).to eq(2)
      expect(form.payout_transfers.include?(payout_transfer4)).to eq(true)
      expect(form.payout_transfers.include?(payout_transfer5))
    end

    it "loads in payout transfers that match the conditions of the = filter" do
      form_params = {
        filter_amount: "50.00",
        filter_type:   "="
      }

      form = PayoutTransfer::AdminFilterForm.new(form_params)

      expect(form.save).to be_truthy
      expect(form.payout_transfers.first).to eq(payout_transfer2)
      expect(form.payout_transfers.size).to eq(1)
    end

    it "loads in payout transfers that match the conditions of the < filter" do
      form_params = {
        filter_amount: "45.00",
        filter_type:   "<"
      }

      form = PayoutTransfer::AdminFilterForm.new(form_params)

      expect(form.save).to be_truthy
      expect(form.payout_transfers.first).to eq(payout_transfer1)
      expect(form.payout_transfers.size).to eq(1)
    end

    it "loads in payout transfers that match the conditions of the name filter" do
      form_params = {
        person_name: "gurley"
      }

      form = PayoutTransfer::AdminFilterForm.new(form_params)

      expect(form.save).to be_truthy
      expect(form.payout_transfers.size).to eq(1)
      expect(form.payout_transfers.first).to eq(payout_transfer5)
    end

    it "doesn't save if invalid filter type params are passed to the form" do
      form_params = {
        filter_amount: "45.00",
        filter_type:   "abcd"
      }

      form = PayoutTransfer::AdminFilterForm.new(form_params)

      expect(form.save).to be_falsey
    end

    it "doesn't save if invalid withdrawal method params are passed to the form" do
      form_params = {
        withdrawal_method: "abcd"
      }

      form = PayoutTransfer::AdminFilterForm.new(form_params)

      expect(form.save).to be_falsey
    end

    it "returns an appropriate map of users who have previously approved payout transfers" do
      form_params = {}

      form = PayoutTransfer::AdminFilterForm.new(form_params)

      expect(form.save).to be_truthy
      expect(form.approvals_map[payout_provider.id]).to eq(2)
      expect(form.approvals_map[payout_provider2.id]).to be_falsey
    end

    it "returns an appropriate map of users who have previously approved payout transfers" do
      form_params = {}

      form = PayoutTransfer::AdminFilterForm.new(form_params)

      expect(form.save).to be_truthy
      expect(form.approvals_map[payout_provider.id]).to eq(2)
      expect(form.approvals_map[payout_provider2.id]).to be_falsey
    end

    it "creates a map of the most recently submitted payout_transfers that have changed payout_methods" do
      most_recent_transfer = create(:payout_transfer, tunecore_status: PayoutTransfer::APPROVED, provider_status: PayoutTransfer::COMPLETED, amount_cents: 10000, payout_provider: payout_provider, withdraw_method: PayoutTransfer::PAYPAL, withdraw_method_changed: true)
      form_params = {}

      form = PayoutTransfer::AdminFilterForm.new(form_params)
      form.save

      expect(form.changed_payout_method_ids.size).to eq(2)
      expect(form.changed_payout_method_ids.include?(most_recent_transfer.id)).to be(true)
      expect(form.changed_payout_method_ids.include?(payout_transfer5.id)).to be(true)
    end
  end
end
