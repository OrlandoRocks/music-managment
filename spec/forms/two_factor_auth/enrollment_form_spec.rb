require "rails_helper"

describe TwoFactorAuth::EnrollmentForm do

  before(:each) do
    @person   = create(:person)
    @tfa      = create(:two_factor_auth, :incomplete, person: @person)
    cache_key = TwoFactorAuth::EnrollmentForm::KEY_PREFIX + @person.id.to_s
    $redis.set(cache_key, nil)
    allow(FeatureFlipper).to receive(:show_feature?).with(:verify_2fa, @person).and_return(true)
    allow_any_instance_of(TwoFactorAuth::ApiClient).to receive(:log).and_return(true)
  end

  describe ".new" do
    context "when a user is just starting the wizard" do
      it "sets the current_step to 'intro'" do
        wizard = TwoFactorAuth::EnrollmentForm.new(@person)
        expect(wizard.current_step).to eq "intro"
      end

      it "sets error to an empty array" do
        wizard = TwoFactorAuth::EnrollmentForm.new(@person)
        expect(wizard.error).to be_nil
      end
    end

    context "when a user is in the middle of the wizard" do
      it "retrieves the current_step from redis" do
        key = TwoFactorAuth::EnrollmentForm::KEY_PREFIX + @person.id.to_s
        allow($redis).to receive(:get).with(key).and_return({ current_step: "authentication", error: nil }.to_json)
        wizard = TwoFactorAuth::EnrollmentForm.new(@person)
        expect(wizard.current_step).to eq "authentication"
      end
    end
  end

  describe "#submit_step" do
    context "STEP 1: Intro" do
      before(:each) do
        @wizard = TwoFactorAuth::EnrollmentForm.new(@person)
        @step_params = nil
      end

      it "updates current_step to the next step" do
        expect(@wizard.current_step).to eq "intro"
        @wizard.submit_step(@step_params)
        expect(@wizard.current_step).to eq "sign_in"
      end

      it "does not add any error" do
        @wizard.submit_step(@step_params)
        expect(@wizard.error).to be_nil
      end
    end

    context "STEP 2: Sign In Credentials" do
      before(:each) do
        key     = TwoFactorAuth::EnrollmentForm::KEY_PREFIX + @person.id.to_s
        state   = { current_step: "sign_in", error: nil }
        $redis.set(key, state.to_json)
      end

      context "successful sign in credentials" do
        before(:each) do
          @wizard      = TwoFactorAuth::EnrollmentForm.new(@person)
          @step_params = { email: @person.email, password: "Testpass123!" }
        end

        it "updates current_step to the next step" do
          expect(@wizard.current_step).to eq "sign_in"
          @wizard.submit_step(@step_params)
          expect(@wizard.current_step).to eq "preferences"
        end

        it "does not add any error" do
          @wizard.submit_step(@step_params)
          expect(@wizard.error).to be_nil
        end
      end

      context "failed sign in credentials" do
        before(:each) do
          @wizard      = TwoFactorAuth::EnrollmentForm.new(@person)
          @step_params = { email: @person.email, password: "BAD_password" }
        end

        it "does NOT update current_step to the next step" do
          expect(@wizard.current_step).to eq "sign_in"
          @wizard.submit_step(@step_params)
          expect(@wizard.current_step).to eq "sign_in"
        end

        it "adds an error" do
          @wizard.submit_step(@step_params)
          expect(@wizard.error).to eq "Wrong Credentials. Try Again."
        end
      end
    end

    context "STEP 3: Notfication Preferences" do
      before(:each) do
        key     = TwoFactorAuth::EnrollmentForm::KEY_PREFIX + @person.id.to_s
        state   = { current_step: "preferences", error: nil }
        $redis.set(key, state.to_json)
      end

      context "successful submission of phone number and notification preference" do
        before(:each) do
          @wizard      = TwoFactorAuth::EnrollmentForm.new(@person)
          @step_params = { country_code: "1", phone_number: "1234567890", notification_method: "sms" }
          allow_any_instance_of(TwoFactorAuth::ApiClient).to receive(:validate_phone_number).and_return('+11234567890')
        end

        it "updates current_step to the next step" do
          expect(@wizard.current_step).to eq "preferences"
          @wizard.submit_step(@step_params)
          expect(@wizard.current_step).to eq "authentication"
        end

        it "does not add any error" do
          @wizard.submit_step(@step_params)
          expect(@wizard.error).to be_nil
        end

        it "saves notification preference" do
          expect(@person.two_factor_auth.notification_method).to be_nil
          @wizard.submit_step(@step_params)
          expect(@person.two_factor_auth.notification_method).to eq "sms"
        end

        it "saves the country_code" do
          expect(@person.two_factor_auth.notification_method).to be_nil
          @wizard.submit_step(@step_params)
          expect(@person.two_factor_auth.country_code).to eq('1')
        end

        context "when the preferences originally had a different country_code" do
          before(:each) do
            @tfa = create(:two_factor_auth, :incomplete, person: @person, country_code: '2')
          end

          it "sets previous_country_code to that previous country_code" do
            expect(@person.two_factor_auth.notification_method).to be_nil
            @wizard.submit_step(@step_params)
            expect(@person.two_factor_auth.previous_country_code).to eq('2')
          end
        end
      end

      context "failed submission of notification preference" do
        before(:each) do
          @wizard      = TwoFactorAuth::EnrollmentForm.new(@person)
          @step_params = { country_code: "1", phone_number: "1234567890", notification_method: "sms" }
          allow(@person.two_factor_auth).to receive(:update).and_return(false)
        end

        it "does NOT update current_step to the next step" do
          expect(@wizard.current_step).to eq "preferences"
          @wizard.submit_step(@step_params)
          expect(@wizard.current_step).to eq "preferences"
        end

        it "adds an error" do
          @wizard.submit_step(@step_params)
          expect(@wizard.error).to eq "We couldn't update your preferences. Please try again."
        end
      end
    end

    context "STEP 4: Verification Code" do
      before(:each) do
        key     = TwoFactorAuth::EnrollmentForm::KEY_PREFIX + @person.id.to_s
        state   = { current_step: "authentication", error: nil }
        $redis.set(key, state.to_json)
      end

      context "submission of valid authentication code" do
        before(:each) do
          @wizard       = TwoFactorAuth::EnrollmentForm.new(@person)
          @step_params  = { code: "mock_auth_code"}
          allow_any_instance_of(TwoFactorAuth::ApiClient).to receive(:submit_auth_code).and_return(double(successful?: true))
        end

        it "updates current_step to the next step" do
          expect(@wizard.current_step).to eq "authentication"
          @wizard.submit_step(@step_params)
          expect(@wizard.current_step).to eq "confirmation"
        end

        it "does not add an error" do
          @wizard.submit_step(@step_params)
          expect(@wizard.error).to be_nil
        end

        it "sets activated_at on tfa" do
          expect(@tfa.activated_at).to be_nil
          @wizard.submit_step(@step_params)
          expect(@tfa.reload.activated_at).to_not be_nil
        end
      end

      context "submission of INVALID authentication code" do
        before(:each) do
          @wizard        = TwoFactorAuth::EnrollmentForm.new(@person)
          @step_params   = { code: "mock_auth_code"}
          allow_any_instance_of(TwoFactorAuth::ApiClient).to receive(:submit_auth_code).and_return(double(successful?: false, error_message: :invalid_verification_code))
        end

        it "does NOT update current_step" do
          expect(@wizard.current_step).to eq "authentication"
          @wizard.submit_step(@step_params)
          expect(@wizard.current_step).to eq "authentication"
        end

        it "adds an error" do
          @wizard.submit_step(@step_params)
          expect(@wizard.error).to eq :invalid_verification_code
        end

        it "does not set activated_at on tfa" do
          expect(@tfa.activated_at).to be_nil
          @wizard.submit_step(@step_params)
          expect(@tfa.reload.activated_at).to be_nil
        end
      end
    end

    context "STEP 5: Confirmation" do
      before(:each) do
        key           = TwoFactorAuth::EnrollmentForm::KEY_PREFIX + @person.id.to_s
        state         = { current_step: "confirmation", error: nil }
        $redis.set(key, state.to_json)

        @wizard       = TwoFactorAuth::EnrollmentForm.new(@person)
        @step_params  = nil
      end

      it "updates current_step to the next step" do
        expect(@wizard.current_step).to eq "confirmation"
        @wizard.submit_step(@step_params)
        expect(@wizard.current_step).to eq "finish_and_redirect"
      end

      it "does not add any error" do
        @wizard.submit_step(@step_params)
        expect(@wizard.error).to be_nil
      end
    end
  end

  describe "#step_back" do
    context "when a user is on authentication step and clicks go back to change password" do
      before(:each) do
        key     = TwoFactorAuth::EnrollmentForm::KEY_PREFIX + @person.id.to_s
        state   = { current_step: "authentication", error: nil }
        $redis.set(key, state.to_json)
        @wizard = TwoFactorAuth::EnrollmentForm.new(@person)
      end

      it "updates @current_step to go one step back to 'preferences'" do
        expect(@wizard.current_step).to eq "authentication"
        @wizard.step_back
        expect(@wizard.current_step).to eq "preferences"
      end
    end
  end

  describe "#reset_steps" do
    context "when a user is past the intro step in the wizard and reset_steps is called" do
      before(:each) do
        key     = TwoFactorAuth::EnrollmentForm::KEY_PREFIX + @person.id.to_s
        state   = { current_step: "authentication", error: nil }
        $redis.set(key, state.to_json)
        @wizard = TwoFactorAuth::EnrollmentForm.new(@person)
      end

      it "clears all progress and sets the @current_step back to 'intro'" do
        expect(@wizard.current_step).to eq "authentication"
        @wizard.reset_steps
        expect(@wizard.current_step).to eq "intro"
      end
    end
  end
end
