require "rails_helper"

describe TwoFactorAuth::PreferenceForm do
  let(:person) { create(:person, :with_two_factor_auth) }
  let(:preference_form) { TwoFactorAuth::PreferenceForm }

  let(:step_one_params) { { person: person } }
  let(:step_two_params) { { person: person, tfa_action: "authentication" } }
  let(:step_three_params) { { person: person, tfa_action: "complete" } }

  before(:each) do
    allow(FeatureFlipper).to receive(:show_feature?).with(:verify_2fa, person).and_return(true)
    allow_any_instance_of(TwoFactorAuth::ApiClient).to receive(:log).and_return(true)
  end

  describe "ACTIONS" do
    let(:auth_form) { TwoFactorAuth::AuthenticationForm }

    it "executes actions in a set order" do
      initial = preference_form::INITIAL_STEP
      auth    = preference_form::AUTHENTICATION_STEP
      final   = preference_form::FINAL_STEP
      expect(preference_form::ACTIONS).to eq [ initial, auth, final ]
    end

    it "uses the same constant values for authentication and complete the authentication form" do
      expect(preference_form::AUTHENTICATION_STEP).to eq auth_form::AUTH_STEP
      expect(preference_form::FINAL_STEP).to eq auth_form::FINAL_STEP
    end

    it "triggers sending a code on the initial step" do
      expect(preference_form::SEND_CODE_STEP).to eq preference_form::INITIAL_STEP
    end

    it "does not start at the same step as the auth form" do
      expect(preference_form::INITIAL_STEP).not_to eq auth_form::INITIAL_STEP
      expect(preference_form::INITIAL_STEP).to eq 'preferences'
    end
  end

  describe "#initialize" do

    context "tfa_action is missing" do
      let(:form) { preference_form.new(step_one_params) }

      it "sets the initial tfa_action" do
        expect(form.tfa_action).to eq 'preferences'
      end
    end

    context "tfa action was received" do
      let(:form) { preference_form.new(step_two_params) }
      it "does not overwrite the initial tfa acton" do
        expect(form.tfa_action).to eq 'authentication'
      end
    end
  end

  describe "#valid?" do
    context "params are valid" do
      let (:current_event) {
        create(
          :two_factor_auth_event,
          two_factor_auth: person.two_factor_auth,
          action: "authentication",
          created_at: 1.minute.ago
        )
      }

      let (:old_event) {
        create(
          :two_factor_auth_event,
          two_factor_auth: person.two_factor_auth,
          action: "authentication",
          created_at: 20.minutes.ago
        )
      }

      it "returns true if the last event is recent" do
          params = { person: person, tfa_action: 'preferences'}
          form = preference_form.new(params)
          expect(form.valid?).to eq true
      end

      it "is false if last event is too old" do
        allow(person.two_factor_auth).to receive(:try)
                                          .with(:last_event)
                                          .and_return(old_event)

        params = { person: person, tfa_action: "preferences" }
        form = preference_form.new(params)
        expect(form.valid?).to eq false
      end

    end

    it "is false if missing required params" do
      form = preference_form.new({})
      expect { form.valid? }.to raise_error(NoMethodError)
      expect(form.person).to eq nil
    end

    it "is false if not a valid action" do
      params = { person: person,
                 tfa_action: "ddos_tunecore" }
      form = preference_form.new(params)
      expect(form.valid?).to eq false
    end

  end

  describe "#perform" do
    let(:fake_step_success) { double(successful?: true) }
    let(:fake_step_failure) { double(successful?: false) }
    let(:form) { preference_form.new(step_one_params) }

    context "success" do
      it "creates a successful event" do
        allow(form).to receive(:execute_two_factor_auth_step).and_return(fake_step_success)
        form.perform

        expect(person.two_factor_auth.last_event.action).to eq "preferences"
        expect(person.two_factor_auth.last_event.successful?).to eq true
      end
      context "on step one" do
        let(:fake_tfa_api_client) { double(request_authorization: true) }

        it "sends an auth code" do
          expect(TwoFactorAuth::ApiClient).to receive(:new)
                                                .with(person)
                                                .and_return(fake_tfa_api_client)
          allow(form).to receive(:execute_two_factor_auth_step).and_return(fake_step_success)
          expect(form.tfa_action).to eq preference_form::SEND_CODE_STEP

          form.perform
        end

        it "references the initial step constant" do
          expect(form.tfa_action).to eq preference_form::INITIAL_STEP
        end
      end

      context "on step two" do
        let(:form) { preference_form.new(step_two_params) }

        it "references the authentication step constant" do
          expect(form.on_authentication_action?).to eq true
          expect(form.tfa_action).to eq preference_form::AUTHENTICATION_STEP
        end
      end

      context "on step three" do
        let(:form) { preference_form.new(step_three_params) }

        it "references the final step constant" do
          expect(form.tfa_action).to eq preference_form::FINAL_STEP
        end

        it "is complete" do
          expect(form.is_complete?).to eq true
        end
      end
    end

    context "failure" do
      let(:form) { preference_form.new(step_two_params)}
      it "creates an unsuccessful event and adds errors" do
        allow(form).to receive(:execute_two_factor_auth_step).and_return(fake_step_failure)

        form.perform

        expect(form.errors.full_messages.first).to eq TwoFactorAuth::AuthenticationForm::ERROR_MESSAGES["authentication"]
        expect(person.two_factor_auth.last_event.action).to eq "authentication"
        expect(person.two_factor_auth.last_event.successful?).to eq false
      end
    end
  end

  describe "#next_action" do
    let(:form) { preference_form.new(step_two_params)}

    it "returns the next action if last event was successful" do
      create(:two_factor_auth_event, two_factor_auth:  person.two_factor_auth, successful: true)
      expect(form.next_action).to eq "complete"
    end

    it "returns the current action if last event was not successful" do
      create(:two_factor_auth_event, two_factor_auth:  person.two_factor_auth, successful: false)
      expect(form.next_action).to eq "authentication"
    end
  end

  describe "#is_complete?" do
    let(:form) { preference_form.new(step_three_params)}

    it "is true and creates a new event if on complete action" do
      expect(form.is_complete?).to eq true
      expect(person.two_factor_auth.last_event.action).to eq "complete"
    end

    it "is false if not on complete action" do
      params = { person: person, tfa_action: "preferences" }
      incomplete_form = preference_form.new(params)

      expect(incomplete_form.is_complete?).to eq false
    end
  end

  describe "#terminate" do
    let(:form) { preference_form.new(step_two_params)}

    it "creates a new, successful 'abandoned' event" do
      form.terminate
      expect(form.valid?).to eq true
      expect(form.last_event.action).to eq "abandoned"
      expect(form.last_event.successful).to eq true
    end

  end

  describe "#on_authentication_action?" do
    it "is false if action is not authentication" do
      params = { person: person, tfa_action: "complete" }
      not_authentication_form = preference_form.new(params)

      expect(not_authentication_form.on_authentication_action?).to eq false
    end
  end

  describe "save_previous_state" do
    let(:saveable_form) { preference_form.new(step_one_params)}
    let(:unsaveable_form) { preference_form.new(step_two_params)}

    it "only saves if the form is on an initial step" do
      expect(saveable_form.save_previous_state).to eq true
      expect(unsaveable_form.save_previous_state).to eq false
    end
  end
end
