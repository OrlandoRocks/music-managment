require "rails_helper"

describe TwoFactorAuth::AuthenticationForm do
  let(:person) { create(:person, :with_two_factor_auth) }
  let(:auth_form) { TwoFactorAuth::AuthenticationForm }

  before(:each) do
    allow(FeatureFlipper).to receive(:show_feature?).with(:verify_2fa, person).and_return(true)
    allow_any_instance_of(TwoFactorAuth::ApiClient).to receive(:log).and_return(true)
  end

  describe "ACTIONS" do
    let(:pref_form) { TwoFactorAuth::PreferenceForm }

    it "executes actions in a set order" do
      initial = auth_form::INITIAL_STEP
      auth    = auth_form::AUTH_STEP
      final   = auth_form::FINAL_STEP
      expect(auth_form::ACTIONS).to eq [ initial, auth, final ]
    end

    it "shares constants for authentication and completion with the preferences form" do
      expect(auth_form::AUTH_STEP).to eq pref_form::AUTHENTICATION_STEP
      expect(auth_form::FINAL_STEP).to eq pref_form::FINAL_STEP
    end

    it "triggers sending a code on sign in" do
      expect(auth_form::SEND_CODE_STEP).to eq auth_form::INITIAL_STEP
    end

    it "does not start at the same step as the preferences form" do
      expect(auth_form::INITIAL_STEP).not_to eq pref_form::INITIAL_STEP
      expect(auth_form::INITIAL_STEP).to eq 'sign_in'
    end
  end

  describe "#valid?" do
    it "is true if params valid" do
      params = { person: person,
                 page: "account_settings",
                 tfa_action: "sign_in" }
      form = auth_form.new(params)
      expect(form.valid?).to eq true
    end

    it "is false if missing required params" do
      params = { person: person }
      form = auth_form.new(params)
      expect(form.valid?).to eq false
    end

    it "is false if not a valid action" do
      params = { person: person,
                 page: "account_settings",
                 tfa_action: "not_a_valid_action" }
      form = auth_form.new(params)
      expect(form.valid?).to eq false
    end

    context "last page is different from current page" do
      it "is false if the last event is not a completion" do
        create(:two_factor_auth_event,
          two_factor_auth:  person.two_factor_auth,
          page: "eft_withdrawal",
          action: "authentication"
        )
        params = {
          person: person,
          page: "account_settings",
          tfa_action: "sign_in"
        }
        form = auth_form.new(params)
        expect(form.valid?).to eq false
      end

      it "is false if the last event failed on a different page" do
        create(:two_factor_auth_event,
          two_factor_auth:  person.two_factor_auth,
          page: "account_settings",
          action: "complete",
          successful: false
        )

        params = {
          person: person,
          page: "change_preferences",
          tfa_action: "authentication"
        }

        form = auth_form.new(params)
        expect(form.valid?).to eq false
      end

      it "is true if the last event was a successful completion" do
        create(:two_factor_auth_event,
          two_factor_auth:  person.two_factor_auth,
          page: "account_settings",
          action: "complete",
          successful: true
        )

        params = {
          person: person,
          page: "change_preferences",
          tfa_action: "authentication"
        }
        form = auth_form.new(params)
        expect(form.valid?).to eq true
      end
    end

    it "is false if last event is outside time window" do
      create(:two_factor_auth_event,
        two_factor_auth: person.two_factor_auth,
        page: "account_settings",
        action: "authentication",
        created_at: 20.minutes.ago
      )
      params = { person: person,
                 page: "account_settings",
                 tfa_action: "sign_in" }
      form = auth_form.new(params)
      expect(form.valid?).to eq false
    end
  end

  describe "#perform" do
    context "success" do
      it "creates a successful event" do
        params = { person: person,
                   page: "account_settings",
                   tfa_action: "authentication" }
        form = auth_form.new(params)
        fake_step = double(successful?: true)
        allow(form).to receive(:two_factor_auth_step).and_return(fake_step)

        form.perform

        expect(person.two_factor_auth.last_event.page).to eq "account_settings"
        expect(person.two_factor_auth.last_event.action).to eq "authentication"
        expect(person.two_factor_auth.last_event.successful?).to eq true
      end
      it "sends auth code if the current tfa_action is sign_in" do
        params = { person: person,
                   page: "account_settings",
                   tfa_action: "sign_in" }
        form = auth_form.new(params)
        fake_step = double(successful?: true)
        fake_tfa_api_client = double(request_authorization: true)
        expect(TwoFactorAuth::ApiClient).to receive(:new).with(person).and_return(fake_tfa_api_client)
        allow(form).to receive(:two_factor_auth_step).and_return(fake_step)
        form.perform
      end
    end

    context "failure" do
      it "creates an unsuccessful event and adds errors" do
        params = { person: person,
                   page: "account_settings",
                   tfa_action: "authentication" }
        form = auth_form.new(params)
        fake_step = double(successful?: false)
        allow(form).to receive(:two_factor_auth_step).and_return(fake_step)

        form.perform

        expect(form.errors.full_messages.first).to eq TwoFactorAuth::AuthenticationForm::ERROR_MESSAGES["authentication"]
        expect(person.two_factor_auth.last_event.page).to eq "account_settings"
        expect(person.two_factor_auth.last_event.action).to eq "authentication"
        expect(person.two_factor_auth.last_event.successful?).to eq false
      end
    end
  end

  describe "#next_action" do
    it "returns the next action if last event was successful" do
      create(:two_factor_auth_event, two_factor_auth:  person.two_factor_auth, successful: true)
      params = { person: person,
                 page: "account_settings",
                 tfa_action: "authentication" }

      form = auth_form.new(params)

      expect(form.next_action).to eq "complete"
    end
    it "returns the current action if last event was not successful" do
      create(:two_factor_auth_event, two_factor_auth:  person.two_factor_auth, successful: false)
      params = { person: person,
                 page: "account_settings",
                 tfa_action: "authentication" }

      form = auth_form.new(params)

      expect(form.next_action).to eq "authentication"
    end
  end

  describe "#is_complete?" do
    it "is true and creates a new event if on complete action" do
      params = { person: person,
                 page: "account_settings",
                 tfa_action: "complete" }

      form = auth_form.new(params)

      expect(form.is_complete?).to eq true
      expect(person.two_factor_auth.last_event.page).to eq "account_settings"
      expect(person.two_factor_auth.last_event.action).to eq "complete"
    end

    it "is false if not on complete action" do
      params = { person: person,
                 page: "account_settings",
                 tfa_action: "sign_in" }

      form = auth_form.new(params)

      expect(form.is_complete?).to eq false
    end
  end

  describe "#on_authentication_action?" do
    it "is true if action is authentication" do
      params = { person: person,
                 page: "account_settings",
                 tfa_action: "authentication" }
      form = auth_form.new(params)
      expect(form.on_authentication_action?).to eq true
    end

    it "is false if action is not authentication" do
      params = { person: person,
                 page: "account_settings",
                 tfa_action: "complete" }
      form = auth_form.new(params)
      expect(form.on_authentication_action?).to eq false
    end
  end

  describe "#execute_success_callbacks" do
    let(:complete_form) {
      auth_form.new(
        person: person,
        tfa_action: 'complete',
        page: "account_settings"
      )
    }

    context "when no success callbacks are defined" do
      it "has valid callbacks" do
        complete_form.success_callbacks = []
        expect(complete_form.success_callbacks.blank?).to be_truthy
        expect(complete_form).to receive(:execute_success_callbacks)
        expect(complete_form.is_complete?).to be_truthy
      end
    end

    context "when the form receives a bad callback" do
      it "has invalid callbacks" do
        complete_form.success_callbacks = [ :ddos_tunecore ]

        expect(complete_form.respond_to?(:ddos_tunecore)).to be_falsey
        expect(complete_form).to receive(:execute_success_callbacks)
        expect(complete_form.is_complete?).to be_truthy
      end
    end

    context "when the form receives a valid success callback" do
      let(:form_with_callbacks) do
        auth_form.new(
          person: person,
          tfa_action: 'complete',
          page: 'account_settings',
          success_callbacks: auth_form::VALID_SUCCESS_CALLBACKS
        )
      end

      it "will execute all callbacks" do
        expect(form_with_callbacks).to receive(:execute_success_callbacks)
        expect(form_with_callbacks.is_complete?).to be_truthy
      end

      it "can disable the user" do
        expect(form_with_callbacks).to receive(:disable_two_factor_auth).and_return(true)
        expect(form_with_callbacks.is_complete?).to be_truthy
      end
    end
  end
end
