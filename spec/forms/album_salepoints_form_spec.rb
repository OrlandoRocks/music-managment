require "rails_helper"

describe AlbumSalepointsForm do
  let(:person) { create(:person) }
  let(:album) { create(:album, person: person) }

  let(:args) do
    {
      album_id: album.id,
      deliver_automator: false,
      apple_music: false,
      store_ids: [],
    }
  end

  describe "#save" do
    context "when successful" do

      context "regarding store_ids" do
        let(:store_ids) {
          [
            Store::ITUNES_WW_ID,
            Store::IHEART_RADIO_STORE_ID,
            Store::NAPSTER_STORE_ID,
          ]
        }

        context "when new store ids are passed in" do
          it "creates salepoints" do
            custom_args = {
              **args,
              store_ids: store_ids
            }

            expect {
              described_class.new(custom_args).save
            }.to change(album.salepoints, :count).by(3)
          end
        end

        context "when store ids are not passed in" do
          context "when salepoints already exist for the album" do
            before(:each) do
              store_ids.each do |store_id|
                if store_id == Store::ITUNES_WW_ID
                  create(:salepoint, :itunes, salepointable: album)
                else
                  create(:salepoint, salepointable: album, store_id: store_id)
                end
              end
            end

            it "destroys salepoints" do
              custom_args = {
                **args,
                store_ids: []
              }

              expect {
                described_class.new(custom_args).save
              }.to change(album.salepoints, :count).by(-3)
            end
          end

          context "when salepoints do not exist for the album" do
            it "nothing changes" do
              custom_args = {
                **args,
                store_ids: []
              }

              expect {
                described_class.new(custom_args).save
              }.to_not change(album.salepoints, :count)
            end
          end
        end
      end

      context "regarding deliver_automator" do
        context "when deliver_automator is set to true" do
          context "when the album does not have a salepoint_subscription" do
            it "creates a salepoint_subscription" do
              custom_args = {
                **args,
                deliver_automator: true
              }

              expect {
                described_class.new(custom_args).save
              }.to change(SalepointSubscription, :count).by(1)

              expect(album.reload.salepoint_subscription).to be_truthy
            end
          end

          context "when the album has a salepoint_subscription" do
            before(:each) do
              create(:salepoint_subscription, album: album)
            end

            it "does nothing" do
              custom_args = {
                **args,
                deliver_automator: true
              }

              expect {
                described_class.new(custom_args).save
              }.to change(SalepointSubscription, :count).by(0)

              expect(album.reload.salepoint_subscription).to be_truthy
            end
          end
        end

        context "when deliver_automator is set to false" do
          context "when the album has a salepoint_subscription" do
            before(:each) do
              create(:salepoint_subscription, album: album)
            end

            it "destroys the salepoint_subscription" do
              custom_args = {
                **args,
                deliver_automator: false
              }

              expect {
                described_class.new(custom_args).save
              }.to change(SalepointSubscription, :count).by(-1)

              expect(album.reload.salepoint_subscription).to be_falsy
            end
          end

          context "when the album does not have a salepoint_subscription" do
            it "does nothing" do
              custom_args = {
                **args,
                deliver_automator: false
              }

              expect {
                described_class.new(custom_args).save
              }.to change(SalepointSubscription, :count).by(0)

              expect(album.reload.salepoint_subscription).to be_falsy
            end
          end
        end
      end

      context "regarding apple_music" do
        context "when apple_music is set to true" do
          it "sets apple_music to true on the album" do
            custom_args = {
              **args,
              apple_music: true
            }

            described_class.new(custom_args).save

            expect(album.reload.apple_music).to eq(true)
          end
        end

        context "when apple_music is set to false" do
          it "sets apple_music to false on the album" do
            custom_args = {
              **args,
              apple_music: false
            }

            described_class.new(custom_args).save

            expect(album.reload.apple_music).to eq(false)
          end
        end
      end
    end

    context "when unsuccessful" do
      context "when an album_id is not present" do
        it "returns false with an error" do
          custom_args = {
            **args,
            album_id: nil,
          }

          form_instance = described_class.new(custom_args)

          expect(form_instance.save).to eq(false)

          expect(form_instance.errors.messages[:album_id]).to be_truthy
        end
      end

      context "when any newly created salepoint fails to save" do
        let(:store_ids) {
          [
            Store::ITUNES_WW_ID,
            Store::IHEART_RADIO_STORE_ID,
            Store::NAPSTER_STORE_ID,
          ]
        }

        context "when an album does not have pre-existing salepoints" do
          it "returns false with an error" do
            Salepoint.any_instance.stub(:save).and_return(false)

            custom_args = {
              **args,
              store_ids: store_ids,
            }

            form_instance = described_class.new(custom_args)

            expect(form_instance.save).to eq(false)

            expect(form_instance.errors.messages[:salepoints]).to be_truthy
          end
        end

        context "when an album has pre-existing salepoints" do
          before(:each) do
            create(:salepoint, :freemium, salepointable: album)
          end

          it "returns false with an error" do
            Salepoint.any_instance.stub(:save).and_return(false)

            custom_args = {
              **args,
              store_ids: store_ids,
            }

            form_instance = described_class.new(custom_args)

            expect(form_instance.save).to eq(false)

            expect(form_instance.errors.messages[:salepoints]).to be_truthy
          end

          it "does not delete the original salepoints" do
            Salepoint.any_instance.stub(:save).and_return(false)

            custom_args = {
              **args,
              store_ids: store_ids,
            }

            expect {
              described_class.new(custom_args)
            }.to_not change(album.salepoints, :count)
          end
        end
      end
    end
  end
end
