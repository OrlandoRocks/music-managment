require "rails_helper"

DEFAULT_COPYRIGHTS =  { composition: true, recording: true }
DEFAULT_COVER_SONG_METADATA = {
  cover_song: true,
  licensed: true,
  will_get_license: true,
}

describe SongDataForm do
  let(:artist)       { create(:artist, name: "Rick Sanchez") }
  let(:person)       { create(:person) }
  let(:album)        { create(:album, person_id: person.id) }
  let(:single)       { create(:single, person_id: person.id) }
  let(:create_song_params)  {
    {
      person_id: person.id,
      song_params: {
        id: nil,
        name: "Get Schwifty",
        language_code_id: nil,
        translated_name: nil,
        version: nil,
        cover_song: false,
        made_popular_by: nil,
        explicit: false,
        clean_version: true,
        optional_isrc: nil,
        lyrics: "Here are my new lyrics. So great and catchy",
        asset: "TBD",
        album_id: album.id,
        instrumental: false,
        artists: [{artist_name: "Rick Sanchez", associated_to: "Album", credit: "primary_artist", role_ids: [1, 2, 3]}]
      }
    }
  }

  let(:create_song_new_artist_params) {
    {
      person_id: person.id,
      song_params: {
        id: nil,
        name: "Get Schwifty",
        language_code_id: nil,
        translated_name: nil,
        version: nil,
        cover_song: false,
        made_popular_by: nil,
        explicit: false,
        clean_version: false,
        optional_isrc: nil,
        lyrics: "Here are my new lyrics. So great and catchy",
        asset: "TBD",
        album_id: album.id,
        artists: [{artist_name: "Morty Smith", associated_to: "Album", credit: "primary_artist", role_ids: [1, 2, 3]}]
      }
    }
  }

  let(:create_song_new_copyrights_params) {
    {
      person_id: person.id,
      should_build_copyrights: true,
      song_params: {
        id: nil,
        name: "Solidarity Forever",
        language_code_id: nil,
        translated_name: nil,
        version: nil,
        cover_song: false,
        made_popular_by: nil,
        explicit: false,
        clean_version: false,
        optional_isrc: nil,
        lyrics: "It is we who plowed the prairies, Built the cities where they trade",
        asset: "TBD",
        album_id: album.id,
        artists: [{artist_name: "Ralph Chaplin", associated_to: "Album", credit: "primary_artist", role_ids: [1, 2, 3]}],
        copyrights: DEFAULT_COPYRIGHTS
      }
    }
  }

  let(:create_song_new_start_times_params) {
    {
      person_id: person.id,
      should_build_start_times: true,
      song_params: {
        id: nil,
        name: "Solidarity Forever",
        language_code_id: nil,
        translated_name: nil,
        version: nil,
        cover_song: false,
        made_popular_by: nil,
        explicit: false,
        clean_version: false,
        optional_isrc: nil,
        lyrics: "It is we who plowed the prairies, Built the cities where they trade",
        asset: "TBD",
        album_id: album.id,
        artists: [{artist_name: "Ralph Chaplin", associated_to: "Album", credit: "primary_artist", role_ids: [1, 2, 3]}],
        song_start_times: [{store_id: 87, start_time: 30}, {store_id: 36, start_time: 45}]
      }
    }
  }

  let(:create_song_new_cover_song_metadata_params) {
    {
      person_id: person.id,
      should_build_cover_song_metadata: true,
      song_params: {
        id: nil,
        name: "Solidarity Forever",
        language_code_id: nil,
        translated_name: nil,
        version: nil,
        cover_song: false,
        made_popular_by: nil,
        explicit: false,
        clean_version: false,
        optional_isrc: nil,
        lyrics: "It is we who plowed the prairies, Built the cities where they trade",
        asset: "TBD",
        album_id: album.id,
        artists: [{artist_name: "Ralph Chaplin", associated_to: "Album", credit: "primary_artist", role_ids: [1, 2, 3]}],
        cover_song_metadata: {cover_song: true, licensed: false, will_get_license: false}
      }
    }
  }

  before(:each) do
    create(:creative, creativeable_type: "Album", creativeable_id: album.id, artist: artist)
    create_list(:song_role, 2)
  end

  describe ".save" do
    context "creating a new song" do
      let(:song_data_form)  { SongDataForm.new(create_song_params) }
      let(:song_data_form_new_artist) { SongDataForm.new(create_song_new_artist_params) }
      let(:song_data_form_new_copyrights) { SongDataForm.new(create_song_new_copyrights_params) }
      let(:song_data_form_new_start_times) { SongDataForm.new(create_song_new_start_times_params) }
      let(:song_data_form_new_cover_song_metadata) { SongDataForm.new(create_song_new_cover_song_metadata_params) }

      context "song is not valid" do
        let(:invalid_song_data_form) { SongDataForm.new({}) }
        it "returns false" do
          expect(invalid_song_data_form.save).to be false
        end

        it "sets errors" do
          invalid_song_data_form.save
          expect(invalid_song_data_form.errors.any?).to be true
        end
      end

      it "creates a new song for the person and album" do
        expect(ArtistIdMatcherWorker).to receive(:perform_async)
        expect{song_data_form.save}.to change{Song.count}.by(1)
        expect(Song.last.album_id).to eq(album.id)
      end

      it "creates new lyrics" do
        expect{song_data_form.save}.to change{Lyric.count}.by(1)
      end

      context "for an existing artist" do
        it "does not create a new artist" do
          expect{song_data_form.save}.to_not change{Artist.count}
        end
      end

      context "new artist" do
        it "creates a new artist" do
          expect{song_data_form_new_artist.save}.to change{Artist.count}.by(1)
        end
      end

      context "for an existing album creative" do
        it "does not create a new creative" do
          expect{song_data_form.save}.to_not change{Creative.count}
        end
      end

      context "new creative for song" do
        it "creates a new creative for the given song" do
          expect{song_data_form_new_artist.save}.to change{Creative.count}.by(1)
        end

        it "does not create a new creative for the same artist on the given song" do
          expect{song_data_form_new_artist.save}.to change{Creative.count}.by(1)
          expect{song_data_form_new_artist.save}.to_not change{Creative.count}
        end
      end

      context "when the song is instrumental" do
        it "sets a language_code_id of nil" do
          create_song_params[:song_params][:instrumental] = true

          form = SongDataForm.new(create_song_params)
          form.save
          expect(form.song.instrumental).to be_truthy
        end
      end

      context "when the album's primary_artist is also featured in a song" do
        it "does not create a new creative" do
          create_song_params[:skip_has_songwriter] = true
          create_song_params[:song_params][:artists] << {artist_name: "Rick Sanchez", associated_to: "Album", credit: "primary_artist", role_ids: [1,2]}
          create_song_params[:song_params][:artists] << {artist_name: "Rick Sanchez    ", associated_to: "Song", credit: "featuring", role_ids: [1,2,3]}
          form = SongDataForm.new(create_song_params)
          expect(form.save).to be false
          expect(form.errors.messages[:creatives]).to_not eq nil
        end
      end

      context "when the song has song_start_times" do
        context "when should_build_start_times is set to true" do
          it "creates new song_start_times" do
            expect{song_data_form_new_start_times.save}.to change{SongStartTime.count}.by(2)
          end

          it "does not create new song_start_times if the given params match the database" do
            expect{song_data_form_new_start_times.save}.to change{SongStartTime.count}.by(2)
            expect{song_data_form_new_start_times.save}.to_not change{SongStartTime.count}
          end

          context "when the copyrights are invalid" do
            context "when the copyrights are missing values" do
              it "returns false" do
                create_song_new_start_times_params[:song_params][:song_start_times][0][:start_time] = "   "

                expect(song_data_form_new_start_times.save).to eq(false)
              end
            end
          end
        end

        context "when should_build_start_times is set to false" do
          it "does not create song_start_times" do
            create_song_new_start_times_params[:should_build_start_times] = false
            expect{song_data_form_new_start_times.save}.to_not change{SongStartTime.count}
          end
        end
      end

      context "when the song has cover_song_metadata" do
        context "when should_build_cover_song_metadata is set to true" do
          it "creates a new cover_song_metadata" do
            expect{song_data_form_new_cover_song_metadata.save}.to change{CoverSongMetadata.count}.by(1)
          end

          it "does not create a new cover_song_metadata if the given params match the database" do
            expect{song_data_form_new_cover_song_metadata.save}.to change{CoverSongMetadata.count}.by(1)
            expect{song_data_form_new_cover_song_metadata.save}.to_not change{CoverSongMetadata.count}
          end

          context "when the cover_song_metadata is invalid" do
            context "when the cover_song_metadata is missing a necessary value" do
              it "returns false" do
                create_song_new_cover_song_metadata_params[:song_params][:cover_song_metadata][:cover_song] = ""

                expect(song_data_form_new_cover_song_metadata.save).to eq(false)
              end
            end
          end
        end

        context "when should_build_cover_song_metadata is set to false" do
          it "does not create cover_song_metadata" do
            create_song_new_cover_song_metadata_params[:should_build_cover_song_metadata] = false
            expect{song_data_form_new_cover_song_metadata.save}.to_not change{CoverSongMetadata.count}
          end
        end
      end

      it "returns the new song data object" do
        song_data_form.save
        song_data = song_data_form.song_data
        expect(song_data.album_id).to eq(album.id)
        expect(song_data.id).to eq(Song.last.id)
        expect(song_data.name).to eq("Get Schwifty")
        expect(song_data.artists.length).to eq(2)
      end
    end

    context "updating a song for a single" do
      let(:english)      { create(:language_code) }
      let(:korean)       { create(:language_code, code: "ko", description: "Korean" )}
      let(:single)       { create(:single, person_id: person.id, language_code: "en", name: "Single Name") }
      let(:create_single_song_params)  {
        {
          person_id: person.id,
          song_params: {
            id: single.songs.first.id,
            name: "Get Schwifty",
            language_code_id: korean.id,
            translated_name: nil,
            version: nil,
            cover_song: false,
            made_popular_by: nil,
            explicit: false,
            clean_version: true,
            optional_isrc: nil,
            lyrics: "Here are my new lyrics. So great and catchy",
            asset: "TBD",
            album_id: single.id,
            artists: [{artist_name: "Rick Sanchez", associated_to: "Album", credit: "primary_artist", role_ids: [1, 2, 3]}]
          }
        }
      }

      let(:song_data_form) { SongDataForm.new(create_single_song_params) }

      before(:each) do
        song_data_form.save
        single.reload
      end

      xit "updates the associated single's language code" do
        expect(single.language_code).to eq(korean.code)
      end

      it "updates the associated single's title" do
        expect(single.name).to eq("Get Schwifty")
      end

      it "updates the associated single's artists/creatives" do
        expect(single.creatives.count).to eq(1)
        expect(single.creatives.last.name).to eq("Rick Sanchez")
      end

      it "removes the artist/creative from the single" do
        expect(single.creatives.count).to eq(1)
        expect(single.creatives.last.name).to eq("Rick Sanchez")

        create_single_song_params[:song_params][:artists] = [{creative_id: single.creatives.last.id, name: "Rick Sanchez", credit: "primary_artist", role_ids: [3]}]
        song_data_form = SongDataForm.new(create_single_song_params)
        song_data_form.save
        single.reload

        expect(single.creatives.count).to eq(1)
        expect(single.creatives.first.name).to eq("Rick Sanchez")
      end
    end

    context "updating a song" do
      let(:song)         { create(:song, album_id: album.id, name: "Terryfold") }
      let(:update_song_params) {
        {
          person_id: person.id,
          song_params: {
            id: song.id,
            name: "New Song Name",
            language_code_id: song.metadata_language_code_id,
            translated_name: song.translated_name,
            version: song.song_version,
            cover_song: true,
            made_popular_by: song.made_popular_by,
            explicit: song.parental_advisory?,
            clean_version: song.clean_version?,
            optional_isrc: song.optional_isrc,
            lyrics: "Here are my new lyrics. So great and catchy",
            asset: "TBD",
            album_id: song.album_id,
            artists: [{artist_name: "Rick Sanchez", associated_to: "Song", credit: "primary_artist", role_ids: [1, 2, 3]}]
          }
        }
      }

      let(:update_song_new_artist_params) {
        {
          person_id: person.id,
          song_params: {
            id: song.id,
            name: song.name,
            language_code_id: song.metadata_language_code_id,
            translated_name: song.translated_name,
            version: song.song_version,
            cover_song: song.cover_song,
            made_popular_by: song.made_popular_by,
            explicit: song.parental_advisory?,
            clean_version: song.clean_version?,
            optional_isrc: song.optional_isrc,
            lyrics: "Here are my new lyrics. So great and catchy",
            asset: "TBD",
            album_id: song.album_id,
            artists: [{artist_name: "Terry", associated_to: "Album", credit: "primary_artist", role_ids: [3]}]
          }
        }
      }

      let(:remove_artist_params) {
        {
          person_id: person.id,
          song_params: {
            id: song.id,
            name: song.name,
            language_code_id: song.metadata_language_code_id,
            translated_name: song.translated_name,
            version: song.song_version,
            cover_song: song.cover_song,
            made_popular_by: song.made_popular_by,
            explicit: song.parental_advisory?,
            clean_version: song.clean_version?,
            optional_isrc: song.optional_isrc,
            lyrics: "Here are my new lyrics. So great and catchy",
            asset: "TBD",
            album_id: song.album_id,
            artists: [{creative_id: song.album.creatives.first.id, artist_name: "Rick Sanchez", associated_to: "Album", credit: "primary_artist", role_ids: [3]}]
          }
        }
      }

      let(:invalid_song_params) {
        {
          person_id: person.id,
          song_params: {
            id: song.id,
            name: '💿💿💿💿💿',
            language_code_id: song.metadata_language_code_id,
            translated_name: song.translated_name,
            version: song.song_version,
            cover_song: song.cover_song,
            made_popular_by: song.made_popular_by,
            explicit: song.parental_advisory?,
            clean_version: song.clean_version?,
            optional_isrc: "12345",
            lyrics: "💿💿💿💿💿",
            asset: "TBD",
            album_id: song.album_id,
            artists: [{creative_id: song.album.creatives.first.id, artist_name: "Rick Sanchez", associated_to: "Album", credit: "primary_artist", role_ids: [11]}]
          }
        }
      }

      let(:new_copyrights_params) {
        {
          person_id: person.id,
          should_build_copyrights: true,
          song_params: {
            id: song.id,
            name: song.name,
            language_code_id: song.metadata_language_code_id,
            translated_name: song.translated_name,
            version: song.song_version,
            cover_song: song.cover_song,
            made_popular_by: song.made_popular_by,
            explicit: song.parental_advisory?,
            clean_version: song.clean_version?,
            optional_isrc: song.optional_isrc,
            lyrics: "It is we who plowed the prairies, Built the cities where they trade",
            asset: "TBD",
            album_id: song.album_id,
            artists: [
              {
                creative_id: song.album.creatives.first.id,
                artist_name: "Ralph Chaplin",
                associated_to: "Album",
                credit: "primary_artist",
                role_ids: [1, 2, 3]
              }
            ],
            copyrights: DEFAULT_COPYRIGHTS
          }
        }
      }

      let(:new_start_times_params) {
        {
          person_id: person.id,
          should_build_start_times: true,
          song_params: {
            id: song.id,
            name: song.name,
            language_code_id: song.metadata_language_code_id,
            translated_name: song.translated_name,
            version: song.song_version,
            cover_song: song.cover_song,
            made_popular_by: song.made_popular_by,
            explicit: song.parental_advisory?,
            clean_version: song.clean_version?,
            optional_isrc: song.optional_isrc,
            lyrics: "It is we who plowed the prairies, Built the cities where they trade",
            asset: "TBD",
            album_id: song.album_id,
            artists: [
              {
                creative_id: song.album.creatives.first.id,
                artist_name: "Ralph Chaplin",
                associated_to: "Album",
                credit: "primary_artist",
                role_ids: [1, 2, 3]
              }
            ],
            song_start_times: [
              {id: song.song_start_times.first.id, store_id: 87, start_time: 30},
              {store_id: 36, start_time: 45}
            ]
          }
        }
      }

      let(:update_cover_song_metadata_params) {
        {
          person_id: person.id,
          should_build_cover_song_metadata: true,
          song_params: {
            id: song.id,
            name: song.name,
            language_code_id: song.metadata_language_code_id,
            translated_name: song.translated_name,
            version: song.song_version,
            cover_song: song.cover_song,
            made_popular_by: song.made_popular_by,
            explicit: song.parental_advisory?,
            clean_version: song.clean_version?,
            optional_isrc: song.optional_isrc,
            lyrics: "It is we who plowed the prairies, Built the cities where they trade",
            asset: "TBD",
            album_id: song.album_id,
            artists: [
              {
                creative_id: song.album.creatives.first.id,
                artist_name: "Ralph Chaplin",
                associated_to: "Album",
                credit: "primary_artist",
                role_ids: [1, 2, 3]
              }
            ],
            cover_song_metadata: DEFAULT_COVER_SONG_METADATA
          }
        }
      }

      let(:song_data_form)               { SongDataForm.new(update_song_params) }
      let(:song_data_form_invalid_song)  { SongDataForm.new(invalid_song_params) }
      let(:song_data_form_new_artist)    { SongDataForm.new(update_song_new_artist_params) }
      let(:song_data_form_remove_artist) { SongDataForm.new(remove_artist_params) }
      let(:song_data_form_new_copyrights) { SongDataForm.new(new_copyrights_params) }
      let(:song_data_form_new_start_times) { SongDataForm.new(new_start_times_params) }
      let(:song_data_form_update_cover_song_metadata) { SongDataForm.new(update_cover_song_metadata_params) }
      before(:each) do
        create(:creative, creativeable_type: "Song", creativeable_id: song.id, role: "featuring", artist_id: artist.id, person_id: person.id)

        create(:song_start_time, song: song, store_id: Store::SONG_START_TIME_STORES.first, start_time: 15)

        create(:cover_song_metadata, song: song, cover_song: false, licensed: nil, will_get_license: nil)
      end

      it "updates the song's attributes" do
        expect(ArtistIdMatcherWorker).to receive(:perform_async).with(song.album.creatives.map(&:id))
        song_data_form.save
        song.reload
        expect(song.name).to eq("New Song Name")
        expect(song.cover_song).to eq(true)
      end

      context "song is not valid" do
        it "adds the song's errors to the song_data_form" do
          song_data_form_invalid_song.save
          error_messages = song_data_form_invalid_song.errors.messages
          expect(song_data_form_invalid_song.valid?).to eq false
          expect(error_messages[:songwriter]).to_not eq nil
          expect(error_messages[:lyrics].size).to eq 1
          expect(error_messages[:name].size).to eq 1
        end
      end

      context "existing artist" do
        it "does not create a new artist" do
          expect{song_data_form.save}.to_not change{Artist.count}
        end
      end

      context "new artist" do
        it "creates a new artist" do
          expect{song_data_form_new_artist.save}.to change{Artist.count}.by(1)
        end
      end

      context "removing an artist" do
        it "destroys the creative record and does not include the artist in the list of artists" do
          creative_id = song.reload.creatives.first.id
          expect(creative_id).to_not be nil
          song_data_form_remove_artist.save
          expect(Creative.find_by(id: creative_id)).to eq(nil)
        end
      end

      context "when there is a new copyright" do
        it "creates a new copyright" do
          expect{song_data_form_new_copyrights.save}.to change{Copyright.count}.by(1)
        end
      end

      context "when there is a new song_start_time" do
        it "creates a new song_start_times" do
          expect{song_data_form_new_start_times.save}.to change{SongStartTime.count}.by(1)
        end
      end

      context "when a cover_song_metadata already exists" do
        it "updates the cover_song_metadata record" do
          expect(song.cover_song_metadata.cover_song).to eq(false)
          expect(song.cover_song_metadata.licensed).to eq(nil)
          expect(song.cover_song_metadata.will_get_license).to eq(nil)

          expect{song_data_form_update_cover_song_metadata.save}.to_not change{SongStartTime.count}

          song.reload

          expect(song.cover_song_metadata.cover_song).to eq(true)
          expect(song.cover_song_metadata.licensed).to eq(true)
          expect(song.cover_song_metadata.will_get_license).to eq(true)
        end
      end

      context "existing lyrics" do
        before do
          @lyric =  Lyric.create(song_id: song.id, content: "blah blah blah la di da")
        end
        it "updates existing lyrics" do
          song_data_form_new_artist.save
          @lyric.reload
          expect(@lyric.content).to eq("Here are my new lyrics. So great and catchy")
        end

        it "does not create a new lyric record" do
          expect{song_data_form_new_artist.save}.to_not change{Lyric.count}
        end
      end

      context "new lyrics" do
        it "creates new lyrics with the given content" do
          song_data_form_new_artist.save
          lyric = Lyric.where(song_id: song.id).first
          expect(lyric.content).to eq("Here are my new lyrics. So great and catchy")
        end

        it "creates a new lyric record" do
          expect{song_data_form_new_artist.save}.to change{Lyric.count}.by(1)
        end
      end

      context "existing creative" do
        it "updates the creative" do
          song_data_form.save
          creative = Creative.where(creativeable_type: "Song", creativeable_id: song.id).first
          expect(creative.role).to eq("primary_artist")
        end

        it "does not create a new creative record" do
          expect{song_data_form.save}.to_not change{Creative.count}
        end
      end

      context "new creative" do
        it "creates a new creative" do
          song_data_form_new_artist.save

          artist = Artist.find_by(name: "Terry")
          creative = Creative.where(artist_id: artist.id)

          expect(creative.count).to eq 1
        end
      end

      context "song roles" do
        it "creates a creative_song_role record for each creative/song role pair" do
          song_data_form.save

          creative_song_roles = CreativeSongRole.where(creative_id: song.reload.creatives.first.id, song_id: song.id)
          expect(creative_song_roles.length).to eq(3)
        end
      end

      xcontext "album's genre is 'instrumental' but song has language_code_id" do
        before(:each) do
          genre = create(:genre, name: "Instrumental")
          album.update(primary_genre_id: genre.id)
          song.update(language_code_id: 1)
        end
        it "sets the song's language_code_id to nil" do
          form = SongDataForm.new(update_song_params)
          form.save
          expect(song.reload.language_code_id).to eq(nil)
        end
      end
    end
  end
  describe ".build" do
    context "without an exisiting song" do
      it "creates a song data form with a deafult song_data object" do
        song_data_form = SongDataForm.build(album_id: album.id, person_id: person.id)
        expect(song_data_form.errors.any?).to be false
        expect(song_data_form.data.id).to eq(nil)
        expect(song_data_form.data.album_id).to eq(album.id)
      end
    end
    context "with an existing song" do
      let(:song) { create(:song, album_id: album.id) }
      it "creates a song data form for the given song" do
        song_data_form = SongDataForm.build(album_id: album.id, person_id: person.id, song: song)
        expect(song_data_form.errors.any?).to be false
        expect(song_data_form.data.id).to eq(song.id)
        expect(song_data_form.data.album_id).to eq(album.id)
      end
    end
  end

  describe "#skip_has_songwriter" do
    it "should set skip_has_songwriter to false by default" do
      song_data_form = SongDataForm.new({})
      expect(song_data_form.skip_has_songwriter).to be false
    end

    context "When skip_has_songwriter=true" do
      it "should not invoke has_songwriter validation." do
        song_data_form = SongDataForm.new({skip_has_songwriter: true})
        expect(song_data_form).not_to receive(:has_songwriter)
        song_data_form.valid?
      end
    end

    context "When skip_has_songwriter=false" do
      it "should invoke has_songwriter validation." do
        song_data_form = SongDataForm.new({skip_has_songwriter: false})
        expect(song_data_form).to receive(:has_songwriter)
        song_data_form.valid?
      end
    end
  end

  describe "#should_build_start_times" do
    it "should set should_build_start_times to false by default" do
      song_data_form = SongDataForm.new({})
      expect(song_data_form.should_build_start_times).to be false
    end

    context "When should_build_start_times=true" do
      it "should invoke has_song_start_times validation." do
        song_data_form = SongDataForm.new({should_build_start_times: true})
        expect(song_data_form).to receive(:has_song_start_times)
        song_data_form.valid?
      end
    end

    context "When should_build_start_times=false" do
      it "should not invoke has_song_start_times validation." do
        song_data_form = SongDataForm.new({should_build_start_times: false})
        expect(song_data_form).to_not receive(:has_song_start_times)
        song_data_form.valid?
      end
    end
  end
end
