require "rails_helper"

describe BulkAlbumApprovalForm do
  describe "#save" do
    it "is false if album_ids is missing" do
      form = BulkAlbumApprovalForm.new
      expect(form.save).to eq false
    end

    it "is false if person_id is missing" do
      form = BulkAlbumApprovalForm.new(album_ids: [1, 2, 3])
      expect(form.save).to eq false
    end

    it "is true of approval service runs succesfully" do
      service_params = { person_id: 1, album_ids: [1, 2, 3] }
      expect(BulkAlbumApprovalService).to receive(:approve).with(service_params).and_return(true)
      form = BulkAlbumApprovalForm.new(service_params)
      expect(form.save).to eq true
    end

    it "is has errors approval service runs succesfully" do
      service_params = { person_id: 1, album_ids: [1, 2, 3] }
      expect(BulkAlbumApprovalService).to receive(:approve).with(service_params).and_return(false)
      form = BulkAlbumApprovalForm.new(service_params)
      expect(form.save).to eq false
      expect(form.errors).not_to be_empty
    end
  end
end
