require 'rails_helper'

describe TrackMonetization::DistributionForm do
  let(:track) { create(:track_monetization, :eligible) }

  describe "#save" do
    let(:params) {{ track_monetization_ids: [track.id], takedown: false }}

    it "updates the track monetization's state" do
      expect {
        TrackMonetization::DistributionForm.new(params).save
      }.to change { track.reload.state }
    end

    context "when issuing a takedown" do
      let(:params) {{ track_monetization_ids: [track.id], takedown: true }}

      it "sets the takedown_at field" do
        time = Time.local(2018, 1, 1)
        Timecop.freeze(time)
        TrackMonetization::DistributionForm.new(params).save
        expect(track.reload).to have_attributes(takedown_at: time)
        Timecop.return
      end
    end

    context "when issuing a reverse takedown" do
      it "resets the takedown_at field" do
        track.update(takedown_at: Time.now)
        TrackMonetization::DistributionForm.new(params).save
        expect(track.reload).to have_attributes(takedown_at: nil)
      end
    end

    context "when track's album is not on YouTube" do
      before do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
        allow_any_instance_of(DistributionCreator).to receive(:ready_to_distribute?).and_return(true)
      end

      context "when track monetization is FB track" do
        let!(:track) { create(:track_monetization, :eligible, :with_album_upc, :with_account_artist, store_id: Store::FBTRACKS_STORE_ID) }
        let!(:petri_bundle) { create(:petri_bundle, album: track.album) }
        let(:params) { { track_monetization_ids: [track.id], takedown: false } }
        it "does not create a distribution for Youtube Music" do

          expect{ subject }.to change { Distribution.count }.by(0)
        end

        it "does not create a google salepoint" do

          expect{ subject }.to change { Salepoint.count }.by(0)
        end
      end
    end
  end

  describe "#response_data" do

    context "a track is being monetized" do
      let(:params) {{ track_monetization_ids: [track.id], takedown: false }}
      let(:expected_response) {
        [{ track_id: track.id, status: "processing" }]
      }

      it "returns a processing response object" do
        distribution_form        = TrackMonetization::DistributionForm.new(params)
        distribution_form.save
        expect(distribution_form.response_data).to eq expected_response
      end
    end

    context "a track is being taken down" do
      let(:params) {{ track_monetization_ids: [track.id], takedown: true }}
      let(:expected_response) {
        [{ track_id: track.id, status: "processing_takedown" }]
      }

      it "returns a processing response object" do
        distribution_form        = TrackMonetization::DistributionForm.new(params)
        distribution_form.save
        expect(distribution_form.response_data).to eq expected_response
      end
    end
  end
end
