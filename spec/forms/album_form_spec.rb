require "rails_helper"

describe AlbumForm do
  let(:person) { create(:person) }
  let(:album) {
    create(:album,
      creatives: [
        { "name" => Faker::Name.name, "role" => "primary_artist" },
        { "name" => Faker::Name.name, "role" => "primary_artist" }
      ]
    )
  }

  let(:single) {
    create(:single,
      creatives: [
        { "name" => Faker::Name.name, "role" => "primary_artist" },
        { "name" => Faker::Name.name, "role" => "primary_artist" }
      ]
    )
  }

  let(:valid_new_album_params) {
    {
      person: person,
      album_params: {
        album_type: "Album",
        is_various: false,
        label_name: "sdfgfvcdsdfgf",
        language_code: "en",
        name: "awerfcxsaqwer",
        orig_release_year: "",
        primary_genre_id: "1",
        recording_location: "etyhgfdsertgfde",
        sale_date: "2020-07-04T17:07:00-05:00",
        secondary_genre_id: "3",
        timed_release_timing_scenario: "relative_time",
        golive_date: DateTime.now + 13.days,
        creatives: [
          {
            name: "sdgfds wertgfdswd",
            role: "primary_artist"
          },
          {
            name: "iujmkmnjk",
            role: "primary_artist"
          }
        ],
        created_with: "songwriter"
      }
    }
  }

  let(:valid_new_album_invalid_label_params) {
    {
      **valid_new_album_params,
      album_params: valid_new_album_params[:album_params].merge({
        label_name: "🚾😁😁😁",
      })
    }
  }

  let(:invalid_sale_date_params) {
    {
      person: person,
      album_params: {
        album_type: "Album",
        is_various: false,
        label_name: "sdfgfvcdsdfgf",
        language_code: "en",
        name: "awerfcxsaqwer",
        orig_release_year: "2020-08-04",
        primary_genre_id: "1",
        recording_location: "etyhgfdsertgfde",
        sale_date: "2020-07-04T17:07:00-05:00",
        secondary_genre_id: "3",
        timed_release_timing_scenario: "relative_time",
        golive_date: "2020-07-04T17:07:00-05:00",
        creatives: [
          {
            name: "sdgfds wertgfdswd",
            role: "primary_artist"
          },
          {
            name: "iujmkmnjk",
            role: "primary_artist"
          }
        ],
        created_with: "songwriter"
      }
    }
  }

  let(:invalid_new_album_params) { {} }

  let(:valid_update_album_params) {
    {
      **valid_new_album_params,
      album: album,
      album_params: valid_new_album_params[:album_params].merge({
        name: "a reasonable name"
      })
    }
  }

  let(:valid_update_album_params_no_label) {
    {
      **valid_new_album_params,
      album: album,
      album_params: valid_new_album_params[:album_params].except(:label_name).merge({
        name: "a reasonable name"
      })
    }
  }

  let(:invalid_update_album_params) {
    {
      **valid_update_album_params,
      album_params: valid_update_album_params[:album_params].merge({
        album_type: "an album type that does not exist"
      })
    }
  }

  let(:deleted_creative_album_params) {
    {
      **valid_update_album_params,
      album_params: valid_update_album_params[:album_params].merge({
        creatives: [
          {
            name: "the only creative on this album",
            role: "primary_artist"
          }
        ]
      })
    }
  }

  let(:valid_new_single_params) {
    {
      **valid_new_album_params,
      album_params: valid_new_album_params[:album_params].merge({
        album_type: "Single"
      })
    }
  }

  let(:valid_update_single_params) {
    {
      **valid_new_album_params,
      album: single,
      album_params: valid_new_album_params[:album_params].merge({
        album_type: "Single",
        name: "a reasonable name"
      })
    }
  }

  let(:invalid_spotify_required_params) {
    {
      **valid_new_album_params,
      spotify_artists_required: true,
      album_params: valid_new_album_params[:album_params].merge({
        creatives: [
          {
            name: Faker::Name.name,
            role: "primary_artist",
            itunes: {
              identifier: "489827"
            }
          },
          {
            name: Faker::Name.name,
            role: "primary_artist",
            spotify: {}
          },
          {
            name: Faker::Name.name,
            role: "primary_artist"
          }
        ]
      })
    }
  }

  let(:valid_spotify_required_params) {
    {
      **valid_new_album_params,
      spotify_artists_required: true,
      album_params: valid_new_album_params[:album_params].merge({
        creatives: [
          {
            name: Faker::Name.name,
            role: "primary_artist",
            spotify: {
              identifier: "489827"
            },
            itunes: {}
          },
          {
            name: Faker::Name.name,
            role: "primary_artist",
            spotify: {
              create_page: true
            }
          },
          {
            name: Faker::Name.name,
            role: "primary_artist",
            spotify: {
              identifier: "489827"
            }
          }
        ]
      })
    }
  }

  let(:valid_new_album_form) { AlbumForm.new(valid_new_album_params) }
  let(:invalid_new_album_form) { AlbumForm.new(invalid_new_album_params) }
  let(:valid_album_invalid_label_form) { AlbumForm.new(valid_new_album_invalid_label_params) }

  let(:valid_update_album_form) { AlbumForm.new(valid_update_album_params) }
  let(:valid_update_album_form_no_label) { AlbumForm.new(valid_update_album_params_no_label) }

  let(:invalid_update_album_form) { AlbumForm.new(invalid_update_album_params) }

  let(:deleted_creative_album_form) { AlbumForm.new(deleted_creative_album_params) }

  let(:valid_new_single_form) { AlbumForm.new(valid_new_single_params) }
  let(:valid_update_single_form) { AlbumForm.new(valid_update_single_params) }

  let(:invalid_sale_date_form) { AlbumForm.new(invalid_sale_date_params) }

  let(:invalid_spotify_required_form) { AlbumForm.new(invalid_spotify_required_params) }
  let(:valid_spotify_required_form) { AlbumForm.new(valid_spotify_required_params) }

  describe "#save" do
    context "creating a new album" do
      context "when the album is not valid" do
        it "returns false" do
          expect(invalid_new_album_form.save).to be_falsy
        end

        it "sets errors" do
          invalid_new_album_form.save
          expect(invalid_new_album_form.errors.any?).to be_truthy
        end
      end

      context "when the album sale date is not valid" do
        it "returns false" do
          expect(invalid_sale_date_form.save).to be_falsy
        end

        it "sets errors" do
          invalid_sale_date_form.save
          expect(invalid_sale_date_form.errors.details[:golive_date][0][:error]).to eq('Release date must be greater than or equal to original release date')
        end
      end

      context "when the album label_name is not valid" do
        it "returns false" do
          expect(valid_album_invalid_label_form.save).to be_falsy
        end

        it "sets errors" do
          valid_album_invalid_label_form.save
          expect(valid_album_invalid_label_form.errors.messages[:label_name]).to include(a_string_matching(/🚾, 😁, 😁, 😁/))
        end
      end

      context "when spotify_artists_required is true" do
        context "when a creative does not have valid spotify artist data" do
          it "returns false" do
            expect(invalid_spotify_required_form.save).to be_falsy
          end

          it "sets errors" do
            invalid_spotify_required_form.save
            expect(invalid_spotify_required_form.errors.messages[:creatives]).to include(a_string_matching("need spotify artist id"))
          end
        end

        context "when all creatives have valid spotify artist data" do
          it "returns true" do
            expect(valid_spotify_required_form.save).to be_truthy
          end
        end
      end

      context "when the album is valid" do
        it "returns true" do
          expect(valid_new_album_form.save).to be(true)
        end

        it "creates a new album" do
          expect{valid_new_album_form.save}.to change{Album.count}.by(1)
        end

        it "creates a new creative" do
          expect{valid_new_album_form.save}.to change{Creative.count}.by(2)
        end

        context "when the album is a single" do
          it "creates a new single" do
            expect{valid_new_single_form.save}.to change{Single.count}.by(1)
          end

          it "creates a new song" do
            expect{valid_new_single_form.save}.to change{Song.count}.by(1)
          end
        end
      end
    end

    context "updating a new album" do
      context "when the album is not valid" do
        it "returns false" do
          expect(invalid_update_album_form.save).to be_falsy
        end

        it "sets errors" do
          invalid_update_album_form.save
          expect(invalid_update_album_form.errors.any?).to be_truthy
        end
      end

      context "when the album is valid" do
        it "returns true" do
          expect(valid_update_album_form.save).to be(true)
        end

        it "updates the album" do
          expect{valid_update_album_form.save}.to change{album.name}.to("A Reasonable Name")
        end

        context "when the album is a single" do
          it "returns true" do
            expect(valid_update_single_form.save).to be(true)
          end

          it "updates the song" do
            expect{valid_update_single_form.save}.to change{single.song.name}.to("A Reasonable Name")
          end
        end
      end

      context "when the a creative is removed" do
        it "deletes the removed creative" do
          expect{deleted_creative_album_form.save}.to change{album.creatives.count}.by(-1)
        end
      end

      describe "label_name" do
        context "when given a label name" do
          it "creates a label" do
            expect(album).to receive(:set_default_label_name)
            valid_update_album_form.save
          end
        end

        context "when given an empty label name" do
          it "creates a label" do
            expect(album).to receive(:set_default_label_name)
            valid_update_album_form.save
          end
        end

        context "when not given a label name" do
          it "does not create a label" do
            expect(album).not_to receive(:set_default_label_name)
            valid_update_album_form_no_label.save
          end
        end
      end

      context "payment applied" do
        before do
          album.update(payment_applied: true)
        end

        it "adjusts additional artists" do
          expect(Plans::AdditionalArtistService).to receive(:adjust_additional_artists)
          valid_update_album_form.save
        end
      end

      context "payment NOT applied" do
        it "does NOT adjust additional artists" do
          expect(Plans::AdditionalArtistService).not_to receive(:adjust_additional_artists)
          valid_update_album_form.save
        end
      end
    end
  end
end
