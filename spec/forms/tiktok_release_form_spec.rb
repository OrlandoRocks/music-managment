require "rails_helper"

describe TiktokReleaseForm do
  let(:person) { create(:person) }

  let(:tiktok_store_ids) {
    [
      # Store::BELIEVE_STORE_ID,
      Store::TIK_TOK_STORE_ID
    ]
  }

  let(:release) {
    create(:single, :tiktok_release,
      creatives: [
        { "name" => Faker::Name.name, "role" => "primary_artist" },
        { "name" => Faker::Name.name, "role" => "primary_artist" }
      ]
    )
  }

  let(:valid_new_release_params) {
    {
      person: person,
      album_params: {
        album_type: "Single",
        is_various: false,
        label_name: "sdfgfvcdsdfgf",
        language_code: "en",
        name: "awerfcxsaqwer",
        orig_release_year: "",
        primary_genre_id: "1",
        recording_location: "etyhgfdsertgfde",
        sale_date: "2020-07-04T17:07:00-05:00",
        secondary_genre_id: "3",
        timed_release_timing_scenario: "relative_time",
        golive_date: DateTime.now + 1.years,
        creatives: [
          {
            name: "sdgfds wertgfdswd",
            role: "primary_artist"
          },
          {
            name: "iujmkmnjk",
            role: "primary_artist"
          }
        ],
        created_with: "songwriter"
      }
    }
  }

  let(:invalid_new_release_params) {{}}

  let(:valid_update_release_params) {
    {
      **valid_new_release_params,
      album: release,
      album_params: valid_new_release_params[:album_params].merge({
        name: "a reasonable name"
      })
    }
  }

  let(:invalid_update_release_params) {
    {
      **valid_update_release_params,
      album_params: valid_update_release_params[:album_params].merge({
        album_type: "an album type that does not exist"
      })
    }
  }

  let(:valid_new_release_form) { TiktokReleaseForm.new(valid_new_release_params) }
  let(:invalid_new_release_form) { TiktokReleaseForm.new(invalid_new_release_params) }

  let(:valid_update_release_form) { TiktokReleaseForm.new(valid_update_release_params) }
  let(:invalid_update_release_form) { TiktokReleaseForm.new(invalid_update_release_params) }

  describe "#save" do
    context "creating a new tiktok_release" do
      context "when the release is not valid" do
        it "returns false" do
          expect(invalid_new_release_form.save).to be_falsy
        end

        it "does not create any new salepoints" do
          expect {
            invalid_new_release_form.save
          }.to change{
            Salepoint.count
          }.by(0)
        end
      end

      context "when the release is valid" do
        it "creates new salepoints" do
          expect {
            valid_new_release_form.save
          }.to change{
            Salepoint.count
          }.by(TiktokReleaseForm::STORE_IDS.count)

          expect(valid_new_release_form.album.salepoints.map(&:store_id).sort)
            .to eq(tiktok_store_ids.sort)
        end
      end
    end

    context "updating a tiktok_release" do
      context "when the release is not valid" do
        it "returns false" do
          expect(invalid_update_release_form.save).to be_falsy
        end

        it "does not create any new salepoints" do
          expect {
            invalid_new_release_form.save
          }.to change{
            Salepoint.count
          }.by(0)
        end
      end

      context "when the release is valid" do
        it "returns true" do
          expect(valid_update_release_form.save).to be(true)
        end

        it "does not create new salepoints if the release already has them" do
          expect(release.salepoints.map(&:store_id).sort)
            .to eq(tiktok_store_ids.sort)

          expect {
            valid_update_release_form.save
          }.to change{
            Salepoint.count
          }.by(0)
        end
      end
    end
  end
end
