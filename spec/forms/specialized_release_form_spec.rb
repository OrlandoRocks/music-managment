require "rails_helper"

describe SpecializedReleaseForm do
  let(:person) { create(:person) }
  let(:fb_reels_store_id) { Store::FB_REELS_STORE_ID }
  let(:discovery_platforms_ids) { Store.discovery_platforms.pluck(:id) }

  let!(:release) {
    create(:single,
           :freemium_release,
           creatives: [
             { "name" => Faker::Name.name, "role" => "primary_artist" },
             { "name" => Faker::Name.name, "role" => "primary_artist" }
           ]
    )
  }

  let(:valid_new_release_params) {
    {
      person: person,
      album_params: {
        album_type: "Single",
        is_various: false,
        label_name: "sdfgfvcdsdfgf",
        language_code: "en",
        name: "awerfcxsaqwer",
        orig_release_year: "",
        primary_genre_id: "1",
        recording_location: "etyhgfdsertgfde",
        sale_date: "2020-07-04T17:07:00-05:00",
        secondary_genre_id: "3",
        specialized_release_type: release_type,
        creatives: [
          {
            name: "sdgfds wertgfdswd",
            role: "primary_artist"
          },
          {
            name: "iujmkmnjk",
            role: "primary_artist"
          }
        ],
        created_with: "songwriter"
      }
    }
  }

  let(:invalid_new_release_params) {{}}

  let(:valid_update_release_params) {
    {
      **valid_new_release_params,
      album: release,
      album_params: valid_new_release_params[:album_params].merge({
                                                                    name: "a reasonable name"
                                                                  })
    }
  }

  let(:valid_new_release_form) { SpecializedReleaseForm.new(valid_new_release_params) }
  let(:invalid_new_release_form) { SpecializedReleaseForm.new(invalid_new_release_params) }

  let(:valid_update_release_form) { SpecializedReleaseForm.new(valid_update_release_params) }

  describe "#save" do
    let(:release_type) { "discovery_platforms" }

    before do
      allow(FeatureFlipper).to receive(:show_feature?).and_return true
    end

    context "creating a new discovery platforms release" do
      context "when the release is not valid" do
        it "returns false" do
          expect(invalid_new_release_form.save).to be_falsy
        end

        it "does not create any new salepoints" do
          expect {
            invalid_new_release_form.save
          }.to change{
            Salepoint.count
          }.by(0)
        end
      end

      context "when the release is valid" do
        it "creates discovery platform salepoints" do
          expect {
            valid_new_release_form.save
          }.to change{
            Salepoint.count
          }.by(discovery_platforms_ids.count)

          expect(valid_new_release_form.album.salepoints.map(&:store_id).sort)
            .to match_array(discovery_platforms_ids)
        end
      end
    end

    context "updating a new discovery platforms release" do
      context "when the release is valid" do
        it "does NOT change salepoints count" do
          expect(valid_update_release_form.album.salepoints.count).to eq 1

          valid_update_release_form.save
          expect(valid_update_release_form.album.salepoints.count).to eq 1
        end
      end
    end
  end
end
