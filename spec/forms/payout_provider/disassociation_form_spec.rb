require 'rails_helper'

describe PayoutProvider::DisassociationForm do
  let(:person) { create(:person) }
  let(:admin) { create(:person, :admin) }
  let(:payout_provider) { create(:payout_provider, provider_status: PayoutProvider::APPROVED, person: person) }

  describe "#valid?" do
    it "returns false when an admin does not have the correct role" do
      form_params = {
        admin: admin,
        provider_id: payout_provider.id
      }
      form = PayoutProvider::DisassociationForm.new(form_params)

      expect(form.save).to be_falsey
      expect(form.errors.full_messages.include?("Admin does not have required payout admin role")).to be(true)
    end
  end

  describe "#save" do
    it "disassociates the account on success" do
      expect(payout_provider.active_provider).to be(true)
      expect(payout_provider.provider_status).to eq(PayoutProvider::APPROVED)

      admin.roles << Role.find_by(name: "Payout Admin")

      form_params = {
        admin: admin,
        provider_id: payout_provider.id
      }
      form = PayoutProvider::DisassociationForm.new(form_params)

      expect(form.save).to be_truthy
      expect(payout_provider.reload.active_provider).to be(false)
      expect(payout_provider.reload.provider_status).to eq(PayoutProvider::DECLINED)
    end
  end
end
