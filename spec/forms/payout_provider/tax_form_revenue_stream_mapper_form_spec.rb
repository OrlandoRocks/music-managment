require "rails_helper"

describe PayoutProvider::TaxFormRevenueStreamMapperForm, type: :model do
  let(:person) { create(:person, :with_payout_provider) }
  let(:valid_mappings) do
    {
      distribution: "primary",
      publishing: "secondary"
    }
  end
  let(:invalid_mappings) do
    {
      distribution: "bogus1",
      publishing: "bogus2"
    }
  end

  describe "Validations" do
    it { should validate_presence_of(:person) }
    it { should validate_presence_of(:mappings) }

    describe "tax-form mappings" do
      it "validates mapping params" do
        form = described_class.new(person: person, mappings: invalid_mappings)
        expect(form).to be_invalid
        expect(form.errors.full_messages).to include("invalid mapping")
      end

      it "validates existence of mappable tax-froms" do
        form = described_class.new(person: person, mappings: valid_mappings)
        expect(form).to be_invalid
        expect(form.errors.full_messages).to include("invalid mapping")
      end
    end
  end

  it "creates mappings" do
    primary_tax_form = create(:tax_form, :w9_individual, person: person)
    secondary_tax_form = create(
      :tax_form, :w9_entity,
      person: person,
      payout_provider_config: PayoutProviderConfig.secondary_tax_form_config
    )

    described_class.new(person: person, mappings: valid_mappings).save
    expect(person.user_mapped_distribution_tax_form).to eq(primary_tax_form)
    expect(person.user_mapped_publishing_tax_form).to eq(secondary_tax_form)
  end
end
