require "rails_helper"

describe PayoutProvider::LoginForm do
  let(:person) { create(:person) }

  before(:each) do
    @link             = "https://payouts.sandbox.payoneer.com/partners/lp.aspx?token = 1234569&langid = 1"
    api_response_body = { login_link: @link }.to_json
    api_response      = double(:api_response, body: api_response_body)
    api_service       = double(:api_service, register_existing_user: api_response)
    allow(PayoutProvider::ApiService).to receive(:new).and_return(api_service)
  end

  describe "#save" do
    it "creates payout provider with tunecore status of active" do
      login_form = PayoutProvider::LoginForm.new(person: person, provider_name: "payoneer")
      expect(login_form.save).to eq(true)
      expect(login_form.link).to eq(@link)
      expect(login_form.person.payout_provider.provider_status).to eq PayoutProvider::ONBOARDING
    end

    it "creates payout provider with tunecore status of active" do
      login_form = PayoutProvider::LoginForm.new(person: person, provider_name: "payoneer")
      expect(login_form.save).to eq(true)
      expect(login_form.link).to eq(@link)
      expect(login_form.person.payout_provider.tunecore_status).to eq PayoutProvider::ACTIVE
    end
  end
end
