require 'rails_helper'

describe PayoutProvider::RegistrationForm do
  let(:person) { create(:person) }

  describe "#save" do
    let(:link) { "https://payouts.sandbox.payoneer.com/partners/lp.aspx?token=1234569&langid=1" }

    before do
      api_response = double(:response, registration_link: link)
      api_service = double(:api_service, register_new_user: api_response)
      allow(PayoutProvider::ApiService).to receive(:new).and_return(api_service)
    end

    it "returns true for valid form submission" do
      registration_form = PayoutProvider::RegistrationForm.new(person: person, currency: 'USD')

      expect(registration_form.save).to eq(true)
      expect(registration_form.link).to eq(link)
      expect(registration_form.person.payout_provider.tunecore_status).to eq PayoutProvider::ACTIVE
    end

    context 'currency' do
      it 'should be invalid if currency is nil' do
        registration_form = PayoutProvider::RegistrationForm.new(person: person)

        expect(registration_form.valid?).to be_truthy
      end

      it 'should be invalid if currency is not one of PayoutProvider:WITHDRAWAL_CURRENCIES' do
        registration_form = PayoutProvider::RegistrationForm.new(person: person, currency: 'INR')

        expect(registration_form.valid?).to be_falsey
      end

      it 'should be valid if currency is one of PayoutProvider::WITHDRAWAL_CURRENCIES' do
        registration_form = PayoutProvider::RegistrationForm.new(person: person, currency: 'USD')

        expect(registration_form.valid?).to be_truthy

        registration_form = PayoutProvider::RegistrationForm.new(person: person, currency: 'NUD')

        expect(registration_form.valid?).to be_truthy
      end
    end
  end
end
