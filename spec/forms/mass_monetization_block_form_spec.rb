require "rails_helper"

describe MassMonetizationBlockForm do
  describe "#save" do
    let(:admin)      { create(:person, :admin) }
    let(:ip_address) { "127.0.0.1" }

    context "when missing arguments" do
      it "adds an error message when missing selected_stores" do
        params = { song_identifiers: "1" }
        mass_monetization_block_form = MassMonetizationBlockForm.new(params)

        mass_monetization_block_form.save

        expect(mass_monetization_block_form.errors.full_messages).to eq ["Store ids can't be blank"]
      end

      it "adds an error message when missing song_identifiers" do
        store = create(:store)
        params = { store_ids: [store.id] }
        mass_monetization_block_form = MassMonetizationBlockForm.new(params)

        mass_monetization_block_form.save

        expect(mass_monetization_block_form.errors.full_messages).to eq ["Song identifiers can't be blank"]
      end

      it "does not call its #block_and_disqualify method" do
        params = { store_ids: [], song_identifiers: "" }
        mass_monetization_block_form = MassMonetizationBlockForm.new(params)

        mass_monetization_block_form.save

        expect_any_instance_of(TrackMonetization).to_not receive(:block_and_disqualify)
      end
    end

    context "when passed in all required arguments" do
      it "calls its blocks monetization" do
        song = create(:song)
        store = create(:store)
        track_monetization = create(:track_monetization, song: song, store: store)
        params = { song_identifiers: song.id.to_s, store_ids: [store.id] }

        mass_monetization_block_form = MassMonetizationBlockForm.new(params)
        mass_monetization_block_form.save

        expect(track_monetization.reload.eligibility_status).to eq("ineligible")
        expect(track_monetization.reload.state).to eq("blocked")
        expect(track_monetization.reload.track_monetization_blocker).to be_present
      end
    end
  end
end
