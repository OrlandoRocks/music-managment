require "rails_helper"

describe DeleteAccountForm do
  describe "#create" do
    let!(:person) { create(:person) }
    let!(:admin) { create(:person, :admin) }
    let!(:form) { DeleteAccountForm.new(person_id: person.id, delete_type: "OFAC", admin: admin) }
    it "scrubs person" do
      Timecop.freeze do
        form.save

        expect(person.reload.email).to eq "#{DeletedAccount::SCRUB_STRING}-#{person.deleted_account.id}@tunecore.com"
        expect(person.name).to eq "#{DeletedAccount::SCRUB_STRING.upcase} AS OF #{DateTime.now.strftime('%Y-%m-%d %H:%M:%S')}"
        %w(address1 address2 city state zip phone_number).each do |attr|
          expect(person.send(attr)).to eq DeletedAccount::SCRUB_STRING
        end
        expect(person.last_logged_in_ip).to eq ""
        expect(person.status).to eq "Locked"
        expect(person.lock_reason).to eq "#{person.deleted_account.delete_type.upcase} ACCOUNT DELETE"
        expect(person.deleted).to eq 1
      end
    end

    it "scrubs login_events" do
      login_events = []
      10.times.each do
        login_events << create(:login_event, person: person)
      end

      form.save

      login_events.each do |login_event|
        %w(subdivision city country user_agent).each do |attr|
          expect(login_event.reload.send(attr)).to eq DeletedAccount::SCRUB_STRING
          expect(login_event.reload.latitude).to eq 0
          expect(login_event.reload.longitude).to eq 0
        end
      end
    end

    let!(:approved_payout_provider) { create(:payout_provider, provider_status: 'approved', tunecore_status: 'active') }
    let!(:payout_providers) { person.payout_providers <<  approved_payout_provider }
    it "scrubs/deactivates payout_provider" do
      form.save

      payout_providers.each do |payout_provider|
        expect(payout_provider.provider_status).to eq(PayoutProvider::DECLINED)
        expect(payout_provider.active_provider).to be false
      end
    end

    it "creates a note" do
      form.save
      note_subject = "#{person.deleted_account.delete_type.upcase} LOCKED ACCOUNT PROCESSED"
      expect(person.notes.last.subject).to eq note_subject
    end
  end
end
