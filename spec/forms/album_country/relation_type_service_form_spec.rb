require "rails_helper"

describe AlbumCountry::RelationTypeServiceForm do
  context "#save" do
    let(:album) { create(:album) }
    let(:iso_codes) { ["US", "DJ"] }

    it "is false when not valid" do
      form = AlbumCountry::RelationTypeServiceForm.new
      expect(form).not_to be_valid
      expect(form.save).to be_falsey
    end

    it "is false when not service returns falsey" do
      allow(AlbumCountry::RelationTypeService).to receive(:update).and_return(false)
      form = AlbumCountry::RelationTypeServiceForm.new(album: album, iso_codes: iso_codes)
      expect(form).to be_valid
      expect(form.save).to be_falsey
      expect(form.countries).to eq Country.where(iso_code: iso_codes)
    end

    it "is true when not service returns truthy" do
      allow(AlbumCountry::RelationTypeService).to receive(:update).and_return(true)
      form = AlbumCountry::RelationTypeServiceForm.new(album: album, iso_codes: iso_codes)
      expect(form).to be_valid
      expect(form.save).to be_truthy
      expect(form.countries).to eq Country.where(iso_code: iso_codes)
    end
  end
end
