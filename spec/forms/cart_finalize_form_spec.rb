require "rails_helper"

describe CartFinalizeForm do
  let(:person)  { create(:person,
    :with_unpaid_purchases,
    :with_braintree_blue_stored_credit_card,
    :with_balance, amount: 50_000) }

  let(:person_pref) { create(:person_preference,
    pay_with_balance: true,
    person: person,
    preferred_credit_card: person.stored_credit_cards.last) }

  let(:fake_manager) { double }

  let(:purchases) { person.purchases.unpaid.not_in_invoice }

  let(:ip) { "1.2.3.4" }

  before(:each) do
    allow(person).to receive(:purchased_before?).and_return(true)
    expect(Cart::Payment::Manager).to receive(:new).with(person, purchases, ip).and_return(fake_manager)
  end

  it "should add balance strategy when use balance passed" do
    params = {
      person: person,
      use_balance: 1,
      payment_id: nil,
      purchases: purchases,
      ip_address: ip
    }

    form = CartFinalizeForm.new(params)

    expect(form.manager).to receive(:add_strategy).with(instance_of(Cart::Payment::Strategies::Balance))
    allow(form.manager).to receive(:successful?).and_return(true)
    allow(form.manager).to receive(:process!).and_return(true)
    expect(form.valid?).to eq true
    expect(form.save).to eq true
  end

  it "should be able to add credit card strategy" do
    params = {
      person: person,
      use_balance: 0,
      payment_id: person.stored_credit_cards.last.id,
      purchases: purchases,
      ip_address: ip
    }

    form = CartFinalizeForm.new(params)

    expect(fake_manager).to receive(:process!)
    allow(fake_manager).to receive(:successful?).and_return(true)
    expect(Cart::Payment::Strategies::CreditCard).to receive(:new, &Cart::Payment::Strategies::CreditCard.method(:new)).with(person.stored_credit_cards.last.id, nil)
    expect(fake_manager).to receive(:add_strategy).with(instance_of Cart::Payment::Strategies::CreditCard)
    expect(form.valid?).to eq true
    expect(form.save).to eq true
  end

  it "should be able to add paypal strategy" do
    params = {
      person: person,
      use_balance: 0,
      payment_id: "paypal",
      purchases: purchases,
      ip_address: ip
    }

    form = CartFinalizeForm.new(params)
    expect(fake_manager).to receive(:process!)
    allow(fake_manager).to receive(:successful?).and_return(true)
    fake_paypal = double(StoredPaypalAccount)
    expect(StoredPaypalAccount).to receive(:currently_active_for_person).with(person).and_return(fake_paypal)
    expect(Cart::Payment::Strategies::Paypal).to receive(:new, &Cart::Payment::Strategies::Paypal.method(:new)).with(fake_paypal, person)
    expect(fake_manager).to receive(:add_strategy).with(instance_of Cart::Payment::Strategies::Paypal)
    expect(form.valid?).to eq true
    expect(form.save).to eq true
  end

  it "should be able to update gstin on successful purchase" do
    gstin = '1234567890ABCDE'
    params = {
      person: person,
      use_balance: 1,
      payment_id: nil,
      purchases: purchases,
      ip_address: ip,
      gstin: gstin
    }

    # TODO: TECH-DEBT:
    # add `allow(FeatureFlipper).to receive(:show_feature?).with(:enable_gst, person).and_return(true)`
    # add `allow(FeatureFlipper).to receive(:show_feature?).with(:bi_transfer, person).and_return(true)`
    # remove `allow(FeatureFlipper).to receive(:show_feature?) { true }`

    allow(FeatureFlipper).to receive(:show_feature?) { true }

    form = CartFinalizeForm.new(params)
    allow(form.manager).to receive(:add_strategy).with(instance_of(Cart::Payment::Strategies::Balance))
    allow(form.manager).to receive(:successful?).and_return(true)
    allow(form.manager).to receive(:process!).and_return(true)
    expect(form.manager).to receive_message_chain(:invoice, :update).and_return(true)

    form.save
    expect(person.latest_gst_info.gstin).to eq(gstin)
  end

  describe "#use_balance" do
    it "is false if person has no money" do
      person.person_balance.balance = 0
      person.person_balance.save
      params = { person: person, use_balance: '1', purchases: purchases, ip_address: ip }
      form = CartFinalizeForm.new(params)
      expect(form.use_balance).to eq false
    end

    it "is false if use_balance param is '0'" do
      params = { person: person, use_balance: '0', purchases: purchases, ip_address: ip  }
      form = CartFinalizeForm.new(params)
      expect(form.use_balance).to eq false
    end

    it "is false if all purchases are free albums" do
      allow(Product).to receive(:free_release?).and_return true
      params = { person: person, use_balance: '1', purchases: purchases, ip_address: ip  }
      form = CartFinalizeForm.new(params)
      expect(form.use_balance).to eq false
    end

    it "is false if use_balance param is 0" do
      params = { person: person, use_balance: 0, purchases: purchases, ip_address: ip  }
      form = CartFinalizeForm.new(params)
      expect(form.use_balance).to eq false
    end

    it "is true if use_balance param is 1" do
      params = { person: person, use_balance: 1, purchases: purchases, ip_address: ip  }
      form = CartFinalizeForm.new(params)
      expect(form.use_balance).to eq true
    end

    it "is true if use_balance param is anything else" do
      params = { person: person, use_balance: 240924200, purchases: purchases, ip_address: ip  }
      form = CartFinalizeForm.new(params)
      expect(form.use_balance).to eq true
    end

    it "is false if person preference to pay with balance is false" do
      person_pref.pay_with_balance = false
      person_pref.save!
      params = { person: person, use_balance: nil, purchases: purchases, ip_address: ip  }
      form = CartFinalizeForm.new(params)
      expect(form.use_balance).to eq false
    end

    it "is false if use_balance is string 'false'" do
      params = { person: person, use_balance: 'false', purchases: purchases, ip_address: ip  }
      form = CartFinalizeForm.new(params)
      expect(form.use_balance).to eq false
    end

    it "is true if use_balance is string 'true'" do
      params = { person: person, use_balance: 'true', purchases: purchases, ip_address: ip  }
      form = CartFinalizeForm.new(params)
      expect(form.use_balance).to eq true
    end

    it "is true if force_pay_with_balance_for_free_albums" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:force_pay_with_balance_for_free_albums, an_instance_of(Person)).and_return(true)
      params = { person: person, use_balance: 'false', purchases: purchases, ip_address: ip  }
      form = CartFinalizeForm.new(params)
      expect(form.use_balance).to eq true
    end
  end
end

describe CartFinalizeForm, "payments os flow" do
  let(:person) {
    create(:person,
           country: 'IN',
           country_website_id: CountryWebsite::INDIA)
  }
  let(:purchases) {
    create_list(:purchase, 1,
                person: person,
                product_id: Product::IN_ALBUM_PRODUCT_IDS.first,
                related_id: 2,
                related_type: 'Salepoint')
  }
  let(:fake_manager) { double }
  let(:ip) { "1.2.3.4" }

  it "should be able to add payments os strategy" do
    allow(person).to receive(:purchased_before?).and_return(true)
    expect(Cart::Payment::Manager).to receive(:new).with(person, purchases, ip).and_return(fake_manager)

    mock_card = double(StoredCreditCard, person: person, id: 123)
    allow(person).to receive_message_chain(:stored_credit_cards, :find).and_return(mock_card)

    params = {
      person: person,
      use_balance: 0,
      payment_id: mock_card.id,
      purchases: purchases,
      ip_address: ip,
      cvv: "123"
    }

    form = CartFinalizeForm.new(params)

    expect(fake_manager).to receive(:process!)
    allow(fake_manager).to receive(:successful?).and_return(true)
    expect(Cart::Payment::Strategies::PaymentsOSCard).to receive(:new, &Cart::Payment::Strategies::PaymentsOSCard.method(:new))
      .with(mock_card.id, params[:cvv])
    expect(fake_manager).to receive(:add_strategy).with(instance_of Cart::Payment::Strategies::PaymentsOSCard)
    expect(form.valid?).to eq true
    expect(form.save).to eq true
  end
end
