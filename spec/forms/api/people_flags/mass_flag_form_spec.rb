require "rails_helper"

describe Api::PeopleFlags::MassFlagForm do
  describe "#valid" do
    let(:person) { create(:person) }

    context "when there is a person id that does not exist" do
      it 'is invalid' do
        form = Api::PeopleFlags::MassFlagForm.new(person_ids: "invalid")

        expect(form).not_to be_valid
      end
    end

    context "when there is a person email that does not exist" do
      it 'is invalid' do
        form = Api::PeopleFlags::MassFlagForm.new(person_ids: "invalid@gmail.com")

        expect(form).not_to be_valid
      end
    end

    context "when there is a valid person id but no flag reason" do
      it 'is invalid' do
        form = Api::PeopleFlags::MassFlagForm.new(person_ids: person.id.to_s)

        expect(form).not_to be_valid
      end
    end

    context "when a person's status is already marked as suspicious" do
      it 'is invalid' do
        person.update(status: Person::SUSPICIOUS)
        suspicious_flag = Flag.suspicious_flag
        flag_reason = build(:flag_reason, flag: suspicious_flag)

        form = Api::PeopleFlags::MassFlagForm.new(
          person_ids: person.id.to_s,
          flag_reason: flag_reason.reason,
          flag_name: Flag::SUSPICIOUS[:name]
        )

        expect(form).to be_invalid
        expect(form.errors.messages[:already_have_flag]).to include person.id.to_s
      end
    end

    context "when a person already has the people flag" do
      it 'is invalid' do
        suspicious_flag = Flag.suspicious_flag
        person.flags << suspicious_flag
        flag_reason = build(:flag_reason, flag: suspicious_flag)

        form = Api::PeopleFlags::MassFlagForm.new(
          person_ids: person.email,
          flag_reason: flag_reason.reason,
          flag_name: suspicious_flag.name
        )

        expect(form).to be_invalid
        expect(form.errors.messages[:already_have_flag]).to include person.email
      end
    end

    context "when there is a valid person id and a flag reason" do
      it 'is valid' do
        flag_reason = build(:flag_reason)
        form = Api::PeopleFlags::MassFlagForm.new(
          person_ids: person.id.to_s,
          flag_reason: flag_reason.reason
        )

        expect(form).to be_valid
        expect(form.valid_people).to include person
      end
    end

    context "when there is a valid person email and a flag reason" do
      it 'is valid' do
        flag_reason = build(:flag_reason)
        form = Api::PeopleFlags::MassFlagForm.new(
          person_ids: person.email,
          flag_reason: flag_reason.reason
        )

        expect(form).to be_valid
        expect(form.valid_people).to include person
      end
    end
  end

  describe "#save" do
    let(:person) { create(:person) }
    let(:suspicious_flag) { Flag.suspicious_flag }

    it "creates a people_flag and a flag_transition" do
      flag_reason = create(:flag_reason, flag: suspicious_flag)
      params = {
        person_ids: person.id.to_s,
        flag_reason: flag_reason.reason,
        flag_name: suspicious_flag.name
      }

      expect {
        Api::PeopleFlags::MassFlagForm.new(params).save
      }.to change(person.flag_transitions, :count).by(1)

      expect(person.reload.suspicious_flag).to be_present
    end

    context "when block_from_distribution is set to true" do
      it "should create a block_from_distribution people_flag" do
        params = {
          person_ids: person.id.to_s,
          flag_name: suspicious_flag.name,
          block_from_distribution: true
        }

        expect {
          Api::PeopleFlags::MassFlagForm.new(params).save
        }.to change(person.flag_transitions, :count).by(2)

        expect(person.blocked_from_distribution_flag).to be_present
      end
    end

    context "when a note is present" do
      it "creates a note related to the FlagTransition" do
        admin = create(:person, :admin)
        note = "This is the note"
        params = {
          person_ids: person.id.to_s,
          flag_name: suspicious_flag.name,
          note: note,
          admin_id: admin.id
        }

        Api::PeopleFlags::MassFlagForm.new(params).save

        flag_transition = person.suspicious_flag.flag_transitions.first
        suspicious_note = flag_transition.notes.first

        expect(suspicious_note.note).to eq note
      end
    end

    it "follows the legacy flow and updates the person as suspicious" do
      flag_reason = create(:flag_reason, flag: suspicious_flag)
      admin = create(:person, :admin)
      params = {
        person_ids: person.id.to_s,
        flag_name: suspicious_flag.name,
        flag_reason: flag_reason.reason,
        admin_id: admin.id
      }

      Api::PeopleFlags::MassFlagForm.new(params).save

      expect(person.reload.status).to eq "Suspicious"
      expect(person.lock_reason).to eq flag_reason.reason
    end

    context "when takedown is set to true" do
      it "kicks off a job to takedown a person's releases" do
        admin = create(:person, :admin)
        ip_address = "127.0.0.1"
        params = {
          person_ids: person.id.to_s,
          flag_name: suspicious_flag.name,
          admin_id: admin.id,
          takedown: true,
          ip_address: ip_address
        }

        expect(TakedownPersonReleasesWorker)
          .to receive(:perform_async)
          .with(person.id, admin.id, ip_address)

        Api::PeopleFlags::MassFlagForm.new(params).save
      end
    end

    context "when a valid person and an invalid person is passed in" do
      it "should process the valid person and add the invalid person to the errors object" do
        valid_person = create(:person)
        invalid_email = "invalid_email@gmail.com"
        params = {
          person_ids: "#{valid_person.id}\n#{invalid_email}",
          flag_name: suspicious_flag.name,
        }

        flag_form = Api::PeopleFlags::MassFlagForm.new(params)
        flag_form.save

        expect(valid_person.reload.suspicious_flag).to be_present
        expect(flag_form.errors.messages[:invalid_people]).to include(invalid_email)
      end
    end
  end
end
