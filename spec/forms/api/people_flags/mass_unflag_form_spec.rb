require "rails_helper"

describe Api::PeopleFlags::MassUnflagForm do
  let!(:person) { create(:person) }
  let(:suspicious) { Flag::SUSPICIOUS[:name] }

  before do
    person.flags << Flag.suspicious_flag
  end

  describe "#save" do
    it "removes the people_flag and creates a flag_transition" do
      params = {
        person_ids: person.id.to_s,
        flag_name: suspicious
      }

      expect(person.suspicious_flag).to be_present

      expect {
        Api::PeopleFlags::MassUnflagForm.new(params).save
      }.to change(person.flag_transitions, :count).by(1)

      expect(person.reload.suspicious_flag).not_to be_present
    end

    context "when unblock_distributions is set to true" do
      it "removes the block_from_distribution people_flag" do
        flag = Flag.blocked_from_distribution_flag
        person.flags << flag

        params = {
          person_ids: person.id.to_s,
          flag_name: suspicious,
          unblock_from_distribution: "true"
        }

        expect(person.blocked_from_distribution_flag).to be_present

        expect {
          Api::PeopleFlags::MassUnflagForm.new(params).save
        }.to change(person.flag_transitions, :count).by(2)

        expect(person.reload.blocked_from_distribution_flag).not_to be_present
      end
    end

    context "when a note is present" do
      it "creates a note related to the FlagTransition" do
        admin = create(:person, :admin)
        note = "This is the note"
        params = {
          person_ids: person.id.to_s,
          flag_name: suspicious,
          note: note,
          admin_id: admin.id
        }

        Api::PeopleFlags::MassUnflagForm.new(params).save

        flag_transition = person.flag_transitions.last
        remove_suspicious_flag_note = flag_transition.notes.first

        expect(remove_suspicious_flag_note.note).to eq note
      end
    end

    it "follows the legacy flow and unmarks a person as suspicious" do
      admin = create(:person, :admin)
      flag_reason = build(:flag_reason)
      person.update(status: "Suspicious", lock_reason: flag_reason.reason)
      params = {
        person_ids: person.id.to_s,
        flag_name: suspicious,
        admin_id: admin.id
      }

      Api::PeopleFlags::MassUnflagForm.new(params).save

      expect(person.reload.status).to eq "Active"
      expect(person.lock_reason).to be_nil
    end

    context "when untakedown is set to true" do
      it "invokes the MassTakedown mass_untakedown tool with the proper arguments" do
        admin = create(:person, :admin)
        person = create(:person, :with_taken_down_album)
        params = {
          person_ids: person.id.to_s,
          flag_name: suspicious,
          admin_id: admin.id,
          untakedown: "true",
        }

        albums_ids = person.albums.where.not(takedown_at: nil).pluck(:id).join(",")
        store_ids = Store.is_active.pluck(:id)

        expect(MassTakedown)
          .to receive(:mass_untakedown)
          .with(albums_ids, store_ids, person)

        Api::PeopleFlags::MassUnflagForm.new(params).save
      end
    end

    context "when a valid person and an invalid person is passed in" do
      it "should process the valid person and add the invalid person to the errors object" do
        invalid_email = "invalid_email@gmail.com"
        params = {
          person_ids: "#{person.email}\n#{invalid_email}",
          flag_name: suspicious,
        }

        flag_form = Api::PeopleFlags::MassUnflagForm.new(params)
        flag_form.save

        expect(person.reload.suspicious_flag).not_to be_present
        expect(flag_form.errors.messages[:invalid_people]).to include(invalid_email)
      end
    end
  end
end
