require "rails_helper"


describe SocialNotifier, type: :mailer do
  include ActionView::Helpers::TagHelper
  ["apple", "tunecore"].each do |payment_channel|
    describe "##{payment_channel}_final_grace_period" do
      { with: :with_live_album, without: :with_taken_down_album }.each do |k,v|
        context "#{k} distro" do
          let(:person) { create(:person, v) }
          let!(:status) { create(:person_subscription_status,
            termination_date: Date.today - 5.days,
            person: person, subscription_type: "Social") }
          let(:mail) { SocialNotifier.send("#{payment_channel}_final_grace_period", person.id) }

          it "sets the headers" do
            expect(mail.to).to eq [status.person.email]
            expect(mail.subject).to eq I18n.t("social_notifier.#{payment_channel}_final_grace_period.subject_#{k}_distro")
          end

          it "renders body" do
            dashboard_link_text = I18n.t('social_notifier.log_in_to_social')
            expect(mail.body).to include("#{I18n.t("social_notifier.#{payment_channel}_final_grace_period.body_#{k}_distro",
              subscription_term: status.product_type,
              dashboard_link: content_tag(:a, dashboard_link_text, href: tc_social_index_url(host: person.domain)))}")
          end
        end
      end
    end # with
  end # payment_channel

  describe "#tunecore_initial_grace_period" do
    let(:person) { create(:person) }
    let!(:status) { create(:person_subscription_status,
      termination_date: Date.today,
      person: person, subscription_type: "Social") }
    let(:mail) { SocialNotifier.send("tunecore_initial_grace_period", person.id) }

    it "sets the headers" do
      expect(mail.to).to eq [status.person.email]
      expect(mail.subject).to eq I18n.t("social_notifier.tunecore_initial_grace_period.subject")
    end

    it "renders body" do
      payment_link_text = I18n.t('social_notifier.payment_information')
      body = I18n.t("social_notifier.tunecore_initial_grace_period.body",
        payment_preferences_link: content_tag(:a, payment_link_text, href: "#{edit_person_url(person, host: person.domain)}/?tab=payment"),
        subscription_term: status.product_type,
        grace_period_end: status.days_until_grace_period_ends)
      expect(mail.body).to include(body)
    end
  end

  describe "#tunecore_second_grace_period" do
    let(:person) { create(:person) }
    let!(:status) { create(:person_subscription_status,
      termination_date: Date.today - 2.days,
      person: person, subscription_type: "Social") }
    let(:mail) { SocialNotifier.send("tunecore_second_grace_period", person.id) }

    it "sets the headers" do
      expect(mail.to).to eq [status.person.email]
      expect(mail.subject).to eq I18n.t("social_notifier.tunecore_second_grace_period.subject")
    end

    it "renders body" do
      payment_link_text = I18n.t('social_notifier.payment_information')
      body = I18n.t("social_notifier.tunecore_second_grace_period.body",
        payment_preferences_link: content_tag(:a, payment_link_text, href: "#{edit_person_url(person, host: person.domain)}/?tab=payment"),
        subscription_term: status.product_type,
        grace_period_end: status.days_until_grace_period_ends)
      expect(mail.body).to include(body)
    end
  end

  describe "#pro_user_cancellation" do
    let(:person) { create(:person) }
    let!(:status) { create(:person_subscription_status,
      termination_date: Date.today - 10.days,
      person: person, subscription_type: "Social") }
    let(:mail) { SocialNotifier.send("pro_user_cancellation", person.id) }

    it "sets the headers" do
      expect(mail.to).to eq [status.person.email]
      expect(mail.subject).to eq I18n.t('social_notifier.pro_user_cancellation.subject')
    end

    it "renders body" do
      body = I18n.t("social_notifier.pro_user_cancellation.body", termination_date: ::Social::PlanStatus.for(person).plan_expires_at.strftime("%b %d %Y"))
      expect(mail.body).to include (body)
    end
  end
end
