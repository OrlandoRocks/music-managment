require "rails_helper"

module DummyOrderModule
  def order(ord_str)
    #to make the stub happy
  end
end

def generate_album_purchase(customer, monthly = false)

  renewal  = double("renewal")
  rh       = double("rh")
  product  = double("product")
  allow(renewal).to receive(:price_to_renew).and_return(49.99)
  allow(renewal).to receive(:related_type).and_return("Album")
  allow(renewal).to receive(:related).and_return(TCFactory.build(:album))
  allow(renewal).to receive(:renewal_duration).and_return(1)
  allow(renewal).to receive(:renewal_interval).and_return("year")
  allow(renewal).to receive(:takedown_date).and_return(Date.today)
  allow(renewal).to receive(:person).and_return(customer)
  allow(renewal).to receive_message_chain(:item_to_renew, :product).and_return(product)

  allow(renewal).to receive(:renewal_interval).and_return(monthly ? "month" : "year")
  rh_array = [rh]
  allow(renewal).to receive(:renewal_history).and_return(rh_array)

  allow(TargetedProduct).to receive(:targeted_product_for_active_offer).with(customer, product, renewal).and_return(nil)

  ri = double("ri")
  allow(ri).to receive(:related_type).and_return("Album")
  allow(ri).to receive(:related_id).and_return(1)
  allow(ri).to receive(:related).and_return(TCFactory.build(:album))
  allow(renewal).to receive(:renewal_items).and_return([ri])

  allow(renewal).to receive_message_chain(:renewal_history, :last).and_return(rh)
  rh_array.extend(DummyOrderModule)
  allow(renewal).to receive_message_chain(:renewal_history, :order).and_return([rh])

  allow(customer).to receive(:renewals).and_return([renewal])
end

def build_person(country_website_id, options)
  allow_message_expectations_on_nil
  options[:no_preferences] ||= false
  person = build(:person)
  person.country_website = CountryWebsite.find(country_website_id)

  #Create the person preference
  if options[:no_preferences]
    pref = PersonPreference.new(:person=>person, :do_not_autorenew=>!options[:auto_renew], :pay_with_balance=>options[:use_balance], :preferred_payment_type=>options[:prefers])
    allow(person).to receive(:person_preference).and_return(pref)
  else
    allow(person).to receive(:person_preference).and_return(nil)
  end

  #Create a stored cc if necessary
  if options[:stored_cc]
    cc = double("cc")
    allow(cc).to receive(:last_four).and_return("1234")
    allow(cc).to receive(:expiration_month).and_return("05")
    allow(cc).to receive(:expiration_year).and_return("12")

    allow(pref).to receive(:preferred_credit_card).and_return(cc)
  end

  #Create a stored pp if necessary
  if options[:stored_pp]
    pp = double("pp")
    allow(pp).to receive(:email).and_return("jdoe@gmail.com")
    allow(pref).to receive(:preferred_paypal_account).and_return(pp)
  end

  generate_album_purchase(person, rand(1000)%2 )

  #give a balance (do this at the end, so it does error out the generated purchases)
  allow(person).to receive(:person_balance).and_return(double("pb"))
  allow(person.person_balance).to receive(:balance).and_return([0,0,0,20.00,50.00,1000.00].rand)

  person
end

describe BatchNotifier, type: :mailer do

  describe "Renewal Reminder" do
    before(:each) do

      #Set the correct expiration dates for testing these emails
      @expiration_dates    = [Date.today+28.days, Date.today+4.days, Date.today - 14.days, Date.today - 35.days]
      @country_website_ids = CountryWebsite.all.collect{ |website| website.id }

      @people = []

      #Create people with all combinations for all country websites
      @country_website_ids.each do |country_website_id|

        #Create a person with no payment preferences
        @people << build_person(country_website_id, :no_preferences=>true)

        #Person Preference permission when use_balance is false then there must be a stored pp or stored cc
        #Don't use balance cases
        @people << build_person(country_website_id,:auto_renew=>false, :use_balance=>false, :stored_cc=>true,  :stored_pp=>false, :prefers=>"CreditCard")

        @people << build_person(country_website_id,:auto_renew=>false, :use_balance=>false, :stored_cc=>true,  :stored_pp=>true,  :prefers=>"CreditCard")
        @people << build_person(country_website_id,:auto_renew=>false, :use_balance=>false, :stored_cc=>false, :stored_pp=>true,  :prefers=>"PayPal")
        @people << build_person(country_website_id,:auto_renew=>false, :use_balance=>false, :stored_cc=>true,  :stored_pp=>true,  :prefers=>"PayPal")

        @people << build_person(country_website_id,:auto_renew=>true, :use_balance=>false, :stored_cc=>false, :stored_pp=>false, :prefers=>"CreditCard")
        @people << build_person(country_website_id,:auto_renew=>true, :use_balance=>false, :stored_cc=>true,  :stored_pp=>false, :prefers=>"CreditCard")
        @people << build_person(country_website_id,:auto_renew=>true, :use_balance=>false, :stored_cc=>true,  :stored_pp=>true,  :prefers=>"CreditCard")
        @people << build_person(country_website_id,:auto_renew=>true, :use_balance=>false, :stored_cc=>false, :stored_pp=>true,  :prefers=>"PayPal")
        @people << build_person(country_website_id,:auto_renew=>true, :use_balance=>false, :stored_cc=>true,  :stored_pp=>true,  :prefers=>"PayPal")
        @people << build_person(country_website_id,:auto_renew=>true, :use_balance=>false, :stored_cc=>false, :stored_pp=>false, :prefers=>"PayPal")

        #Auto Renew and use balance cases disabled cases
        @people << build_person(country_website_id,:auto_renew=>false, :use_balance=>true, :stored_cc=>false, :stored_pp=>false, :prefers=>"CreditCard")
        @people << build_person(country_website_id,:auto_renew=>false, :use_balance=>true, :stored_cc=>false, :stored_pp=>true,  :prefers=>"CreditCard")
        @people << build_person(country_website_id,:auto_renew=>false, :use_balance=>true, :stored_cc=>true,  :stored_pp=>false, :prefers=>"CreditCard")
        @people << build_person(country_website_id,:auto_renew=>false, :use_balance=>true, :stored_cc=>true,  :stored_pp=>true,  :prefers=>"CreditCard")
        @people << build_person(country_website_id,:auto_renew=>false, :use_balance=>true, :stored_cc=>false, :stored_pp=>false, :prefers=>"PayPal")
        @people << build_person(country_website_id,:auto_renew=>false, :use_balance=>true, :stored_cc=>false, :stored_pp=>true,  :prefers=>"PayPal")
        @people << build_person(country_website_id,:auto_renew=>false, :use_balance=>true, :stored_cc=>true,  :stored_pp=>false, :prefers=>"PayPal")
        @people << build_person(country_website_id,:auto_renew=>false, :use_balance=>true, :stored_cc=>true,  :stored_pp=>true,  :prefers=>"PayPal")

        #Auto Renew and don't use balance Cases
        @people << build_person(country_website_id,:auto_renew=>true, :use_balance=>true, :stored_cc=>false, :stored_pp=>false, :prefers=>"CreditCard")
        @people << build_person(country_website_id,:auto_renew=>true, :use_balance=>true, :stored_cc=>false, :stored_pp=>true,  :prefers=>"CreditCard")
        @people << build_person(country_website_id,:auto_renew=>true, :use_balance=>true, :stored_cc=>true,  :stored_pp=>false, :prefers=>"CreditCard")
        @people << build_person(country_website_id,:auto_renew=>true, :use_balance=>true, :stored_cc=>true,  :stored_pp=>true,  :prefers=>"CreditCard")
        @people << build_person(country_website_id,:auto_renew=>true, :use_balance=>true, :stored_cc=>false, :stored_pp=>false, :prefers=>"PayPal")
        @people << build_person(country_website_id,:auto_renew=>true, :use_balance=>true, :stored_cc=>false, :stored_pp=>true,  :prefers=>"PayPal")
        @people << build_person(country_website_id,:auto_renew=>true, :use_balance=>true, :stored_cc=>true,  :stored_pp=>false, :prefers=>"PayPal")
        @people << build_person(country_website_id,:auto_renew=>true, :use_balance=>true, :stored_cc=>true,  :stored_pp=>true,  :prefers=>"PayPal")
      end

      #Update the renewal expirations
      @people.each do |person|

        person.renewals.each do |renewal|
          allow(renewal).to receive(:id).and_return(1)
          allow(renewal).to receive(:price_to_renew_in_money).and_return(49.99.to_money)
          renewal.renewal_history.each do |renewal_history|
            allow(renewal_history).to receive(:expires_at).and_return(@expiration_dates[rand(200) % @expiration_dates.size])
            allow(renewal_history).to receive(:created_at).and_return(Date.today - 365.days)
          end
        end

      end
    end

    it "should render succesfully for all variations and be added to delivery queue" do
      allow_any_instance_of(Person).to receive(:is_uk?).and_return(false)

      expect do
        expect do
          @people.each do |person|
            # Create renewal reminder email.
            # Note that this function expects a date class and not a time class
            BatchNotifier.renewal_reminder(
              person,
              "Test",
              person.renewals,
              Date.parse(person.renewals.first.renewal_history.first.expires_at.strftime('%Y/%m/%d'))
            ).deliver
          end
        end.not_to(raise_error)
      end.to change(ActionMailer::Base.deliveries,:size).by(@people.size)
    end

    it "should contain categories in header" do
      mail = BatchNotifier.renewal_reminder(@people.first, "Test", @people.first.renewals, Date.today).deliver
      expect(mail.header.to_s).to include('X-SMTPAPI: {"category":["RenewalReminder"]}')
    end
  end

  describe "batch failure notice" do

    before(:each) do
      @person  = create(:person)
      allow(@person).to receive(:domain).and_return("www")
      allow(@person).to receive(:email).and_return("yo@yo.com")
      allow(@person).to receive(:country_website).and_return(CountryWebsite.first)

      @invoice = double("invoice")

      allow(@invoice).to receive(:person).and_return(@person)

      allow(TargetedProduct).to receive(:targeted_product_for_active_offer).and_return(nil)

      #build purchases
      purchases = []
      3.times do
        purchase = double(:purchase)
        renewal  = double(:renewal)
        product  = double(:product)
        allow(renewal).to receive(:person).and_return(@person)
        allow(renewal).to receive_message_chain(:item_to_renew, :product).and_return(product)
        allow(renewal).to receive(:price_to_renew_in_money).and_return(49.99.to_money)
        allow(renewal).to receive(:takedown_date).and_return(Date.today)
        allow(renewal).to receive(:renewal_duration).and_return(1)
        allow(renewal).to receive(:renewal_interval).and_return("year")

        ri = double("ri")
        allow(ri).to receive(:related_type).and_return("Album")
        allow(ri).to receive(:related_id).and_return(1)
        allow(ri).to receive(:related).and_return(TCFactory.build(:album))

        allow(renewal).to receive(:renewal_items).and_return([ri])

        allow(purchase).to receive(:renewal).and_return(renewal)
        purchases << purchase
      end

      allow(@invoice).to receive(:purchases).and_return(purchases)
    end

    it "should render succesfully for failed paypal" do
      allow(@person).to receive(:renew_with_credit_card?).and_return(false)
      allow(@person).to receive(:renew_with_paypal?).and_return(true)
      allow(@person).to receive(:preferred_credit_card?).and_return(false)
      allow(@person).to receive(:preferred_paypal_account?).and_return(true)
      allow(@person).to receive(:pay_with_balance?).and_return(false)


      pp = double("paypal_account")
      allow(@person).to receive(:preferred_paypal_account).and_return(pp)
      allow(pp).to receive(:email).and_return("jdoe@gmail.com")

      expect {
        BatchNotifier.batch_failure_notice(@invoice).deliver
      }.to change(ActionMailer::Base.deliveries,:size).by(1)
    end

    it "should render succesfully for failed cc" do
      allow(@person).to receive(:renew_with_credit_card?).and_return(true)
      allow(@person).to receive(:preferred_credit_card?).and_return(true)
      allow(@person).to receive(:preferred_paypal_account?).and_return(false)
      allow(@person).to receive(:pay_with_balance?).and_return(false)


      cc = double("credit_card")
      allow(@person).to receive(:preferred_credit_card).and_return(cc)
      allow(@person).to receive(:preferred_credit_card_id).and_return(1)
      allow(cc).to receive(:last_four).and_return("1234")
      allow(cc).to receive(:expiration_month).and_return("05")
      allow(cc).to receive(:expiration_year).and_return("12")

      expect {
        BatchNotifier.batch_failure_notice(@invoice).deliver
      }.to change(ActionMailer::Base.deliveries,:size).by(1)

    end

    it "should render succesfully for no payment method" do
      allow(@person).to receive(:renew_with_credit_card?).and_return(false)
      allow(@person).to receive(:renew_with_paypal?).and_return(false)
      allow(@person).to receive(:preferred_credit_card?).and_return(false)
      allow(@person).to receive(:preferred_paypal_account?).and_return(false)
      allow(@person).to receive(:pay_with_balance?).and_return(false)
      allow(@person).to receive(:autorenew?).and_return(true)

      expect {
        BatchNotifier.batch_failure_notice(@invoice).deliver
      }.to change(ActionMailer::Base.deliveries,:size).by(1)
    end

    it "should render succesfully for do not auto renew" do

      allow(@person).to receive(:renew_with_credit_card?).and_return(false)
      allow(@person).to receive(:renew_with_paypal?).and_return(false)
      allow(@person).to receive(:preferred_credit_card?).and_return(false)
      allow(@person).to receive(:preferred_paypal_account?).and_return(false)
      allow(@person).to receive(:pay_with_balance?).and_return(false)
      allow(@person).to receive(:autorenew?).and_return(false)

      expect {
        BatchNotifier.batch_failure_notice(@invoice).deliver
      }.to change(ActionMailer::Base.deliveries,:size).by(1)
    end

  end

end
