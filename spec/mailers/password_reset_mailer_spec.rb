require "rails_helper"

describe PasswordResetMailer do
  describe "#reset" do
    it "should deliver successfully" do
      person = create(:person)
      expect {
        PasswordResetMailer.reset(person, url: "http://www.tunecore.com").deliver
      }.to change(ActionMailer::Base.deliveries,:size).by(1)
    end

    it "should generate translation keys correctly" do
      person = create(:person)

      user_mail = PasswordResetMailer.reset(person, url: "http://www.tunecore.com", initiator: :user)
      expect(user_mail.body).to include(I18n.t 'password_reset_mailer.reset.hello_tunecore_artist')

      social_mail = PasswordResetMailer.reset(person, url: "http://www.tunecore.com", initiator: :social)
      expect(social_mail.body).to include(I18n.t 'password_reset_mailer.reset.hello_tunecore_artist')

      admin_mail = PasswordResetMailer.reset(person, url: "http://www.tunecore.com", initiator: :admin)
      expect(admin_mail.body).to include(I18n.t 'password_reset_mailer.reset.admin.reset_your_password')

      system_mail = PasswordResetMailer.reset(person, url: "http://www.tunecore.com", initiator: :system)
      expect(system_mail.body).to include(I18n.t 'password_reset_mailer.reset.system.clicking')
    end
  end
end
