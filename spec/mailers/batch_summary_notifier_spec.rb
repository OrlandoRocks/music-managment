require "rails_helper"

describe BatchSummaryNotifier, type: :mailer do
  before(:each) do
    ActionMailer::Base.deliveries = []
  end

  describe "reminders_summary" do
    it "should deliver and be added to delivery queue" do
      expect {
        BatchSummaryNotifier.reminders_summary({
        :email_id=>{
        :renewal_interval       => :yearly,
        :sent_count             => 20,
        :expiration_date        => Date.today - 28.days,
                  "Test" => {
        :sent_count             => 15,
        :no_preferences_count   => 3,
        :no_paypal_count        => 5,
        :expired_card_count     => 5,
        :no_preferred_card_or_paypal => 2},
                  "Test2" => {
        :sent_count             => 5,
        :no_preferences_count   => 1,
        :no_paypal_count        => 1,
        :expired_card_count     => 1,
        :no_preferred_card_or_paypal => 2}
      }}).deliver
      }.to change(ActionMailer::Base.deliveries,:size).by(1)
    end
  end

  describe "batch_summary" do
    it "should be added to delivery queue" do
      batch         = PaymentBatch.new(:batch_date => Time.now, :country_website_id=>CountryWebsite::UNITED_STATES, currency: "USD")
      allow(batch).to receive(:id).and_return(1)
      batch_summary = Batch::BatchSummary.new(batch)

      expect(PaymentBatch).to receive(:find).with(1).and_return(batch)
      expect(Batch::BatchSummary).to receive(:new).and_return(batch_summary)

      mailer = BatchSummaryNotifier.batch_summary(1).deliver
      expect(ActionMailer::Base.deliveries.size).to eql(1)
    end
  end

  describe "not_processed_summary" do
    it "should be added to delivery queue" do
      payment_batch = PaymentBatch.new(batch_date: Time.now, country_website_id: 1)
      batch_transactions = [BatchTransaction.new(:amount=>10), BatchTransaction.new(amount: 5, invoice: nil)]
      allow(batch_transactions).to receive_message_chain(:first, :payment_batch).and_return(payment_batch)
      allow(payment_batch).to receive_message_chain(:batch_transactions).and_return(batch_transactions)

      allow(CSV).to receive(:open).and_return("")
      allow(File).to receive(:read).and_return("")

      mailer = BatchSummaryNotifier.not_processed_summary(batch_transactions).deliver
      expect(ActionMailer::Base.deliveries.size).to eql(1)
    end
  end

  describe "cc_failure_summary" do
    it "should not deliver if there are no failed transactions" do
      payment_batch = create(:payment_batch)
      create(:batch_transaction, payment_batch: payment_batch, processed: 'processed')

      BatchSummaryNotifier.cc_failure_summary(payment_batch.id).deliver
      expect(ActionMailer::Base.deliveries.size).to eq(0)
    end

    it "should deliver a mail if there are any failed transactions" do
      batch = create(:payment_batch)
      invoice1 = create(:invoice)
      create(:batch_transaction, payment_batch: batch, processed: 'processed', invoice: invoice1)
      failed_transaction = create(:braintree_transaction, status: 'error')
      invoice2 = create(:invoice)
      create(:batch_transaction, matching: failed_transaction, payment_batch: batch, processed: 'processed', invoice: invoice2)
      BRAINTREE_BATCH[:run_limit] = 1

      BatchSummaryNotifier.cc_failure_summary(batch.id).deliver
      expect(ActionMailer::Base.deliveries.size).to eq(1)
    end

    it "should deliver a mail if there are any failed payments os transactions" do
      batch = create(:payment_batch)
      invoice1 = create(:invoice)
      create(:batch_transaction, payment_batch: batch, processed: 'processed', invoice: invoice1)
      failed_transaction = create(:payments_os_transaction, charge_status: 'Failed')
      invoice2 = create(:invoice)
      create(:batch_transaction, matching: failed_transaction, payment_batch: batch, processed: 'processed', invoice: invoice2)
      BRAINTREE_BATCH[:run_limit] = 1

      BatchSummaryNotifier.cc_failure_summary(batch.id).deliver
      expect(ActionMailer::Base.deliveries.size).to eq(1)
    end

  end

end
