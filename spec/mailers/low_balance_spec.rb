require "rails_helper"

include ActionView::Helpers::NumberHelper

describe LowBalanceMailer, type: :mailer do
  let(:person) { create(:person) }
  let(:amount) { 1000.00 }

  it 'renders' do
    test_to = "test_to@example.com"

    mail = LowBalanceMailer.low_balance(amount: amount,
                                        person: person,
                                        to: test_to)

    expect(mail.to).to      include(test_to)
    expect(mail.subject).to eql("TuneCore low balance notification")
    expect(mail.body).to    include("Your account (#{person.email}) balance is below the " \
                                    "notification threshold (#{number_to_currency(amount)}) for your account.")
  end
end
