require "rails_helper"

describe AutomatorMailer, type: :mailer do

  describe "#store_launch" do

    before(:each) do
      @new_store = create(:store)

      @person = create(:person)

      @album1  = create(:album, :person => @person, :finalized_at => Date.today)
      @sp_sub1 = create(:salepoint_subscription, :album => @album1)

      @album2  = create(:album, :person => @person, :finalized_at => Date.today)
      @sp_sub2 = create(:salepoint_subscription, :album => @album2)
    end

    it "should deliver successfully" do
      expect {
        AutomatorMailer.store_launch(@new_store, @person, [@sp_sub1, @sp_sub2]).deliver
      }.to change(ActionMailer::Base.deliveries,:size).by(1)
    end

    it "should add a csv attachment if there are more than 25 releases" do

      subs = [@sp_sub1, @sp_sub2]

      25.times do
        album = create(:album, :person => @person, :finalized_at => Date.today)
        subs << create(:salepoint_subscription, :album => album)
      end

      expect {
        AutomatorMailer.store_launch(@new_store, @person, subs).deliver
      }.to change(ActionMailer::Base.deliveries,:size).by(1)

      expect(ActionMailer::Base.deliveries.last.attachments).not_to be_empty
    end

  end

end

