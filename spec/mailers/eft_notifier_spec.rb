require "rails_helper"

describe EftNotifier, type: :mailer do
  before(:each) do
    allow_any_instance_of(EftNotifier).to receive(:get_text_body).and_return("text body")
  end

  describe "#withdrawal_batch_summary" do

    it "should be added to delivery queue with csv attachment" do

      eft_batch = build(:eft_batch)
      eft_batch.batch_sent_at = Time.now
      eft_batch.save!

      ba = create(:stored_bank_account)
      person = ba.person
      person.person_balance.balance = 100
      person.person_balance.save!

      trans = create(:eft_batch_transaction, :stored_bank_account_id=>ba.id, :eft_batch=>eft_batch, confirm_page: true)
      trans.status = "error"
      trans.save!

      expect {
        mailer = EftNotifier.withdrawal_batch_summary(eft_batch.id).deliver
        expect(mailer.attachments).not_to be_empty
      }.to change(ActionMailer::Base.deliveries, :size).by(1)
    end

  end

  describe "#daily_query_summary" do

    it "is added to the delivery queue with a csv attachment" do
      eft_query = TCFactory.build(:eft_query)

      ba = create(:stored_bank_account)
      person = ba.person
      person.person_balance.balance = 100
      person.person_balance.save!

      trans = create(:eft_batch_transaction, :stored_bank_account_id=>ba.id, :eft_query=>eft_query, confirm_page: true)
      trans.status = "failure"
      trans.save!

      expect {
        mailer = EftNotifier.daily_query_summary(eft_query.id).deliver
        expect(mailer.attachments).not_to be_empty
      }.to change(ActionMailer::Base.deliveries, :size).by(1)
    end

  end

end
