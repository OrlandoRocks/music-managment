require "rails_helper"

describe CheckTransferExportMailer, type: :mailer do
  describe ".check_export" do

    it "should deliver successfully" do
      export = CheckPayment::Export.new(dir= File.expand_path(File.join(Rails.root, "spec", "files")))
      File.new(export.filename, 'w')
      person = create(:person)

      expect{ CheckTransferExportMailer.check_export(export, person.email).deliver }.to change(ActionMailer::Base.deliveries,:size).by(1)

      File.delete(export.filename)
    end

    it "should add a file attachment" do
      export = CheckPayment::Export.new(dir= File.expand_path(File.join(Rails.root, "spec", "files")))
      File.new(export.filename, 'w')
      person = create(:person)

      CheckTransferExportMailer.check_export(export, person.email).deliver

      expect(ActionMailer::Base.deliveries.last.attachments).not_to be_empty

      File.delete(export.filename)
    end
  end
end
