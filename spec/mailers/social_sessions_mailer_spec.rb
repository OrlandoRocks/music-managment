require "rails_helper"

describe SocialSessionsMailer do
  describe "#suspicious_login_mailer" do
    it "should deliver successfully" do
      person = create(:person)
      time = Time.find_zone('Eastern Time (US & Canada)').now
      params = { device_type: 'device_type', ip: 'ip' }
      expect {
        SocialSessionsMailer.suspicious_login_mailer(person, params, time).deliver
      }.to change(ActionMailer::Base.deliveries, :size).by(1)
    end
  end
end
