require "rails_helper"

describe RoyaltySplitsMailer do
  let(:royalty_split) { royalty_split_with_recipients }
  let(:recipient) { royalty_split.recipients.first }

  before do
    allow(FeatureFlipper).to receive(:show_feature?).and_call_original
  end

  describe ".removed_from_split" do
    let(:songs) { create_list :song, 3 }
    before do
      allow(FeatureFlipper).to receive(:show_feature?).with(:royalty_splits_email, royalty_split.owner).and_return(true)
      royalty_split.update!(songs: songs)
    end

    it "should send an email" do
      expect {
        described_class.removed_from_split(
          person_id: recipient.person_id, owner_id: royalty_split.owner_id,
          title: royalty_split.title, song_ids: songs.map(&:id)
        ).deliver
      }.to change(ActionMailer::Base.deliveries, :size).by(1)
    end
    context
  end

  describe ".split_percent_changed" do
    before do
      allow(FeatureFlipper).to receive(:show_feature?).with(:royalty_splits_email, royalty_split.owner).and_return(true)
    end

    it "should send an email" do
      expect {
        described_class.split_percent_changed(recipient_id: recipient.id, from: 10.0).deliver
      }.to change(ActionMailer::Base.deliveries, :size).by(1)
    end
  end

  describe ".new_split_invite" do
    context "with songs" do
      let(:songs) { create_list :song, 3 }
      before do
        allow(FeatureFlipper).to receive(:show_feature?).with(
          :royalty_splits_email,
          royalty_split.owner
        ).and_return(true)
        royalty_split.update!(songs: songs)
      end
      it "should send an email" do
        expect {
          described_class.new_split_invite(recipient_id: recipient.id, locale: "en-us").deliver
        }.to change(ActionMailer::Base.deliveries, :size).by(1)
      end
    end

    context "with no songs" do
      let(:recipient) { create :royalty_split_recipient }
      before do
        allow(FeatureFlipper).to receive(:show_feature?).with(
          :royalty_splits_email,
          recipient.royalty_split.owner
        ).and_return(true)
      end
      it "should send an email" do
        expect {
          described_class.new_split_invite(recipient_id: recipient.id, locale: "en-us").deliver
        }.to change(ActionMailer::Base.deliveries, :size).by(1)
      end
    end

    context "with non-existing user" do
      let(:recipient) { create :royalty_split_recipient, :with_email }
      before do
        allow(FeatureFlipper).to receive(:show_feature?).with(
          :royalty_splits_email,
          recipient.royalty_split.owner
        ).and_return(true)
      end
      it "should send an email" do
        expect {
          described_class.new_split_invite(recipient_id: recipient.id, locale: "en-us").deliver
        }.to change(ActionMailer::Base.deliveries, :size).by(1)
      end
    end
  end

  describe ".split_songs_added" do
    let(:songs) { create_list :song, 3 }
    before do
      allow(FeatureFlipper).to receive(:show_feature?).with(:royalty_splits_email, royalty_split.owner).and_return(true)
      royalty_split.update!(songs: songs)
    end

    it "should send an email" do
      expect {
        described_class.split_songs_added(new_song_ids: songs.map(&:id), split_id: royalty_split.id).deliver
      }.to change(ActionMailer::Base.deliveries, :size).by(1)
    end
  end
end
