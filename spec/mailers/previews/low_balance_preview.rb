# Preview all emails at http://localhost:3000/rails/mailers/low_balance
class LowBalancePreview < ActionMailer::Preview
  def low_balance
    person = Person.first
    LowBalanceMailer.low_balance(amount: 1000,
                                 person: person,
                                 to: "test_recipient@example.com")
  end
end
