# Preview email at http://localhost:3000/rails/mailers/blocked_renewals
class BlockedRenewalsPreview < ActionMailer::Preview
  def blocked_email
    person   = Person.first
    product  = Product.first
    purchase = Purchase.new(person: person, product: product)
    renewal  = Renewal.new(person: person, purchase: purchase, item_to_renew: product)

    BlockedRenewalsMailer.blocked_renewals(person: person, renewals: [renewal])
  end
end
