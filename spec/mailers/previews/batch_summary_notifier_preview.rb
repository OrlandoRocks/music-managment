class BatchSummaryNotifierPreview < ActionMailer::Preview
  def batch_summary
    BatchSummaryNotifier.batch_summary(PaymentBatch.select(:id).order('id desc').limit(100).sample.id)
  end
end