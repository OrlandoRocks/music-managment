class RoyaltySplitsPreview < ActionMailer::Preview
  def new_split_invite
    @recipient = RoyaltySplitRecipient.where.not(person_id: nil).last
    RoyaltySplitsMailer.new_split_invite(recipient_id: @recipient.id)
  end

  def split_percent_changed
    @recipient = RoyaltySplitRecipient.where.not(person_id: nil).last
    RoyaltySplitsMailer.split_percent_changed(recipient_id: @recipient.id, from: 99.1)
  end

  def removed_from_split
    @song_ids = Song.order(updated_at: :desc).limit(10).pluck(:id)
    RoyaltySplitsMailer.removed_from_split(person_id: Person.last.id, owner_id: Person.first.id, title: "Test mailer split", song_ids: @song_ids)
  end

  def split_songs_added
    @song_ids = Song.order(updated_at: :desc).limit(10).pluck(:id)
    RoyaltySplitsMailer.split_songs_added(split_id: RoyaltySplit.last.id, new_song_ids: @song_ids)
  end
end
