class RejectionEmailsPreview < ActionMailer::Preview
  def changes_requested
    RejectionEmailMailer.changes_requested(ReviewAudit.first)
  end

  def rights_verification
    RejectionEmailMailer.rights_verification(ReviewAudit.first)
  end

  def fb_ppp_rejection
    RejectionEmailMailer.fb_ppp_rejection(ReviewAudit.first)
  end
end
