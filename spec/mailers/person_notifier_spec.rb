require "rails_helper"

describe PersonNotifier, type: :mailer do
  describe "album_renewal_change_cancel_status" do
    it "should be added to delivery queue when there is no label" do
      person = FactoryBot.create(:person)
      TCFactory.generate_album_purchase(person)
      person.albums.first.update_attribute(:label_id, nil)

      expect {
        PersonNotifier.album_renewal_change_cancel_status(person.albums.first).deliver
      }.to change(ActionMailer::Base.deliveries,:size).by(1)
    end
  end

  describe "publishing_welcome" do
    it "should be added to delivery queue" do
      composer = FactoryBot.create(:composer)
      expect {
        PersonNotifier.publishing_welcome(composer.person).deliver
      }.to change(ActionMailer::Base.deliveries,:size).by(1)
    end
  end

  context "handling sync memberships" do
    let(:sync_request){ TCFactory.create(:sync_membership_request) }

    describe "sync_mass_invite" do
      it "should be added to the delivery queue" do
        expect {
          PersonNotifier.sync_mass_invite(sync_request, "www.tunecore.com").deliver
        }.to change(ActionMailer::Base.deliveries, :size).by(1)
      end
    end

    describe "sync_approval" do
      it "should be added to the delivery queue" do
        expect {
          PersonNotifier.sync_approval(sync_request, "www.tunecore.com").deliver
        }.to change(ActionMailer::Base.deliveries, :size).by(1)
      end
    end

    describe "sync_register_reminder" do
      it "should be added to the delivery queue" do
        expect {
          PersonNotifier.sync_register_reminder(sync_request, "www.tunecore.com").deliver
        }.to change(ActionMailer::Base.deliveries, :size).by(1)
      end
    end
  end

  context "YouTube notifications" do
    let(:tc_isrc_only)       { FactoryBot.create(:song) }
    let(:optional_isrc)      { FactoryBot.create(:song, :with_optional_isrc) }
    let(:person)             { FactoryBot.create(:person) }
    let(:ytsr_mail_template) { YoutubeSrMailTemplate.return_template('removed_monetization') }
    let(:message_data)       {
      [
        {
          song_id: tc_isrc_only.id,
          custom_fields: {
            "1" => {third_party: person.name}
          }
        },
        {
          song_id: optional_isrc.id,
          custom_fields: {
            "1" => {third_party: person.name}
          }
        }
      ]
    }

    it "creates the youtube sound recording update email body" do
      email = PersonNotifier.youtube_sound_recording_update(person.id, ytsr_mail_template.template_name, message_data)
      expect(email.subject).to eq("Multiple ISRCs: Removed from Monetization")
      expect(email.body.encoded).to include(tc_isrc_only.isrc, optional_isrc.isrc)
    end
  end
end
