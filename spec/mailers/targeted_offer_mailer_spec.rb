require "rails_helper"

describe TargetedOfferMailer, type: :mailer do
  describe "add_people_job_status" do
    before(:each) do
      @targeted_offer = TCFactory.create(:targeted_offer, status: "New")
      @targeted_product = @targeted_offer.targeted_products.create(product: Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID))
      @targeted_offer.update(status: "Active")

      @person         = TCFactory.create(:person)
      @tp1            = @targeted_offer.targeted_people.create(person: @person)
      @tp2            = @targeted_offer.targeted_people.create(person: @person)
      expect(@tp1).to be_valid
      expect(@tp2).not_to be_valid
    end

    it "should deliver successfully" do
      expect do
        TargetedOfferMailer
          .add_people_job_status(
            @targeted_offer,
            [1],
            { @person.id.to_s => @tp2.errors.full_messages },
            [4, 5],
            [6, 7],
            @person,
            10
          ).deliver_now
      end.to(
        change(ActionMailer::Base.deliveries, :size).by(1)
      )
    end
  end
end
