require "rails_helper"

describe SoundoutMailer, type: :mailer do

  describe "report_available" do

    before(:each) do
      @soundout_report = FactoryBot.create(:soundout_report, :with_track, :with_url)
      @mail            = SoundoutMailer.report_available(@soundout_report)
    end

    it 'renders the subject' do
      expect(@mail.subject).to eql("#{@soundout_report.track.name}:  Your TuneCore Fan Reviews report is ready")
    end

    it 'renders the receiver email' do
      expect(@mail.to).to eql([@soundout_report.person.email])
    end

    it 'renders text' do
      expect(@mail.body.encoded).to include("#{@soundout_report.soundout_product.display_name}")
      expect(@mail.body.encoded).to include("#{@soundout_report.track.name}")
    end

    it 'includes a link to the soundout report URL if the report has one' do
      expect(@mail.body.encoded).to include("dummy_soundout_report_url")
    end

    it 'links to the main fan reviews index if the soundout_report does not have a url' do
      @old_sr_report = FactoryBot.create(:soundout_report, :with_track)
      @mail2         = SoundoutMailer.report_available(@old_sr_report)

      expect(@mail2.body.encoded).to include("#{soundout_reports_path}")
    end

  end

end
