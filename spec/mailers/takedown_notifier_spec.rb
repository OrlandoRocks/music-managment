require "rails_helper"

describe TakedownNotifier, type: :mailer do
  describe ".albums_for_takedown" do
    it "should write the store name, takedown_at time, album UPC number, artist name, album name, and a blank string if the album has no optional UPCs to the CSV" do
      person = create(:person)
      recipients = [person.email]
      store = Store.find(Store::AOD_STORE_ID)
      album = create(:album, name: "Will Fear No E", creatives: [{ "name" => "The Allman Brothers", "role" => "primary_artist" }])
      tunecore_upc = create(:tunecore_upc, number: Faker::Number.number(digits: 12))
      album.upcs << tunecore_upc
      time = Time.now
      salepoint = create(:salepoint, :aod_store, takedown_at: time)
      album.salepoints << salepoint
      filename = 'takedown_for_expired_albums'+ "_#{store.short_name}_" + Time.now.strftime("%Y_%m_%d_%H%M%S") + '.csv'
      filepath = TAKEDOWN_REPORT_PATH + "/#{filename}"

      TakedownNotifier.albums_for_takedown([album], {recipients: recipients, store_id: Store::AOD_STORE_ID}).deliver

      CSV.foreach(filepath, headers: true) do |row|
        expect(row['StoreName']).to eq(store.name)
        expect(row['TakenDownAt']).to eq(salepoint.takedown_at.strftime("%Y-%m-%d %H:%M:%S"))
        expect(row['TunecoreUPC']).to eq(tunecore_upc.number)
        expect(row['OptionalUPC']).to eq("")
        expect(row['ArtistName']).to eq(album.artist_name)
        expect(row['AlbumName']).to eq(album.name)
      end

      File.delete(filepath)
    end

    it "should write the album's optional UPC number to the CSV if the album has any optional UPCs" do
      person = create(:person)
      recipients = [person.email]
      store = Store.find(Store::AOD_STORE_ID)
      album_with_optional_upc = create(:album, name: "Will Fear No E", creatives: [{ "name" => "The Allman Brothers", "role" => "primary_artist" }])
      optional_upc = create(:optional_upc)
      album_with_optional_upc.upcs << optional_upc
      time = Time.now
      salepoint_with_optional_upc = create(:salepoint, :aod_store, takedown_at: time)
      album_with_optional_upc.salepoints << salepoint_with_optional_upc
      filename = 'takedown_for_expired_albums'+ "_#{store.short_name}_" + Time.now.strftime("%Y_%m_%d_%H%M%S") + '.csv'
      filepath = TAKEDOWN_REPORT_PATH + "/#{filename}"
      TakedownNotifier.albums_for_takedown([album_with_optional_upc], {recipients: recipients, store_id: Store::AOD_STORE_ID}).deliver

      CSV.foreach(filepath, headers: true) do |row|
        expect(row['OptionalUPC']).to eq(optional_upc.number)
      end
    end

    it "should successfully send a mailer" do
      person = create(:person)
      recipients = [person.email]
      store = Store.find(Store::AOD_STORE_ID)
      album = create(:album, name: "Will Fear No E", creatives: [{ "name" => "The Allman Brothers", "role" => "primary_artist" }])
      tunecore_upc = create(:tunecore_upc, number: Faker::Number.number(digits: 12))
      album.upcs << tunecore_upc
      time = Time.now
      salepoint = create(:salepoint, :aod_store, takedown_at: time)
      album.salepoints << salepoint
      filename = 'takedown_for_expired_albums'+ "_#{store.short_name}_" + Time.now.strftime("%Y_%m_%d_%H%M%S") + '.csv'
      filepath = TAKEDOWN_REPORT_PATH + "/#{filename}"

      expect {TakedownNotifier.albums_for_takedown([album], {recipients: recipients, store_id: Store::AOD_STORE_ID}).deliver}.to change(ActionMailer::Base.deliveries, :count).by(1)

      File.delete(filepath)
    end

    it "should attach a CSV file to the mailer" do
      person = create(:person)
      recipients = [person.email]
      store = Store.find(Store::AOD_STORE_ID)
      album = create(:album, name: "Will Fear No E", creatives: [{ "name" => "The Allman Brothers", "role" => "primary_artist" }])
      tunecore_upc = create(:tunecore_upc, number: Faker::Number.number(digits: 12))
      album.upcs << tunecore_upc
      time = Time.now
      salepoint = create(:salepoint, :aod_store, takedown_at: time)
      album.salepoints << salepoint
      filename = 'takedown_for_expired_albums'+ "_#{store.short_name}_" + Time.now.strftime("%Y_%m_%d_%H%M%S") + '.csv'
      filepath = TAKEDOWN_REPORT_PATH + "/#{filename}"

      TakedownNotifier.albums_for_takedown([album], {recipients: recipients, store_id: Store::AOD_STORE_ID}).deliver

      expect(ActionMailer::Base.deliveries.last.attachments).not_to be_empty

      File.delete(filepath)
    end
  end

  describe "#takedown_notification" do
    it "sends an email" do
      person = create(:person)
      album = create(:album, person: person)
      salepoint = create(:salepoint, salepointable: album)
      artist = Artist.new(name: "John Doe")

      upc = mock_model(Upc)
      allow(upc).to receive(:number).and_return("123456")
      allow(album).to receive(:upc).and_return(upc)
      album.creatives << Creative.new(role: "primary_artist", artist: artist)

      expect {
        TakedownNotifier.takedown_notification(person,[{album: album, stores: [salepoint.store]}]).deliver
      }.to change(ActionMailer::Base.deliveries, :count).by(1)
    end
  end

  describe "specific templates" do
    let(:person) { create(:person) }
    let(:album) { create(:album, person: person) }
    let(:salepoint) { create(:salepoint, salepointable: album) }
    let(:artist) { Artist.new(name: "John Doe") }

    let(:params) do
      {
        person: person,
        albums: [{album: album}],
        stores: [salepoint.store.id]
      }
    end

    before(:each) do
      upc = mock_model(Upc)
      allow(upc).to receive(:number).and_return("123456")
      allow(album).to receive(:upc).and_return(upc)
      album.creatives << Creative.new(role: "primary_artist", artist: artist)
    end

    describe "#streaming_fraud_notification" do
      it "sends from streaming@tunecore.com" do
        TakedownNotifier.streaming_fraud_notification(params).deliver

        delivery = ActionMailer::Base.deliveries.last

        expected_email = "streaming@tunecore.com"

        expect(delivery.from).to eq([expected_email])
      end
    end

    describe "#chargeback_on_renewals_notification" do
      it "sends from copyright@tunecore.com" do
        TakedownNotifier.chargeback_on_renewals_notification(params).deliver

        delivery = ActionMailer::Base.deliveries.last

        expected_email = "copyright@tunecore.com"

        expect(delivery.from).to eq([expected_email])
      end
    end

    describe "#internal_review_for_rights_notification" do
      it "sends from copyright@tunecore.com" do
        TakedownNotifier.internal_review_for_rights_notification(params).deliver

        delivery = ActionMailer::Base.deliveries.last

        expected_email = "copyright@tunecore.com"

        expect(delivery.from).to eq([expected_email])
      end
    end

    describe "#internal_claim_from_stores_notification" do
      it "sends from copyright@tunecore.com" do
        TakedownNotifier.internal_claim_from_stores_notification(params).deliver

        delivery = ActionMailer::Base.deliveries.last

        expected_email = "copyright@tunecore.com"

        expect(delivery.from).to eq([expected_email])
      end
    end
  end
end
