require "rails_helper"

describe SyncNotifier, type: :mailer do

  describe "license" do

    it "should be added to delivery queue" do
      sr = TCFactory.create(:sync_license_request)

      expect {
        mailer = SyncNotifier.license(sr).deliver
      }.to change(ActionMailer::Base.deliveries,:size).by(1)
    end

  end

end

