require "rails_helper"

include ActionView::Helpers::NumberHelper

describe BlockedRenewalsMailer, type: :mailer do
  let(:person)  { create(:person) }
  let(:product) { create(:product)}
  let(:renewal) { create(:renewal, item_to_renew: product) }

  it 'renders' do
    mail = BlockedRenewalsMailer.blocked_renewals(person: person, renewals: [renewal])

    expect(mail.to).to      include(BlockedRenewalsMailer::FINANCE_TEAM_EMAIL)
    expect(mail.subject).to eql(BlockedRenewalsMailer::BLOCKED_RENEWALS_SUBJECT)
    expect(mail.body).to    include("Renewals have been blocked for #{person.email}.")

    expected_body = "#{BlockedRenewalsMailer::SANCTIONED_COUNTRY_REASON}: #{person.country}."
    expect(mail.body).to include(ERB::Util.html_escape(expected_body))

    expect(mail.body).to include("[#{renewal.id}]")
  end
end
