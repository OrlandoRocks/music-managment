require "rails_helper"

describe Payoneer::Configurable do
  describe "#payout_provider_config" do
    context "when not given program_id" do
      before(:each) do
        allow(FeatureFlipper).to receive(:show_feature?).with(:bi_transfer, an_instance_of(Person)).and_return(true)
      end

      context "user already has payout_provider" do
        it "returns payout config for payout_provider" do
          person = create(:person)
          au_config = PayoutProviderConfig.find_by_program_id(PayoutProviderConfig::TC_AU_PROGRAM_ID)
          create(:payout_provider, person: person, payout_provider_config: au_config)

          class DummyClass
            include Payoneer::Configurable
            attr_reader :person, :program_id

            def initialize(p)
              @person = p
            end
          end

          dc = DummyClass.new(person)

          expect(dc.payout_provider_config).to eq(au_config)
          expect(dc.payout_provider_config.sandbox).to eq(true)
        end
      end

      context "for US user" do
        it "returns payout config for TC corporate_entity_id" do
          person = create(:person)

          class DummyClass
            include Payoneer::Configurable
            attr_reader :person, :program_id

            def initialize(p)
              @person = p
            end
          end

          dc = DummyClass.new(person)
          config_corporate_entity = dc.payout_provider_config.corporate_entity
          us_corporate_entity = CorporateEntity.find_by_name(CorporateEntity::TUNECORE_US_NAME)
          expect(config_corporate_entity).to eq(us_corporate_entity)
        end
      end

      context "for international user" do
        it"returns payout config for BI corporate_entity_id" do
          person = create(:person, country: "IN")

          class DummyClass
            include Payoneer::Configurable
            attr_reader :person, :program_id

            def initialize(p)
              @person = p
            end
          end
          dc = DummyClass.new(person)
          config_corporate_entity = dc.payout_provider_config.corporate_entity
          lux_corporate_entity = CorporateEntity.find_by_name(CorporateEntity::BI_LUXEMBOURG_NAME)
          expect(config_corporate_entity).to eq(lux_corporate_entity)
        end
      end
    end
    context "when given program_id" do
      before(:each) do
        class DummyClass
          include Payoneer::Configurable
          attr_reader :person, :program_id

          def initialize(person, program_id)
            @person = person
            @program_id = program_id
          end
        end
      end
      context "for .in site user" do
        it"returns payout config for program_id provided" do
          person = create(:person, country: "IN")
          us_config = PayoutProviderConfig.find(1)
          dc = DummyClass.new(person, us_config.program_id)
          expect(dc.payout_provider_config).to eq(us_config)
        end
      end
      context "for .com site user" do
        it"returns payout config for program_id provided" do
          person = create(:person)
          lux_config = PayoutProviderConfig.find(2)
          dc = DummyClass.new(person, lux_config.program_id)
          expect(dc.payout_provider_config).to eq(lux_config)
        end
      end
    end
  end
end
