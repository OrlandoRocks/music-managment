require 'rails_helper'

describe Payoneer::Requests::PayeeReport do
  let(:person) { create(:person) }
  let!(:payout_provider) { create(:payout_provider, person: person) }
  it "generates a request" do
    request = Payoneer::Requests::PayeeReport.new(person: person)
    expect(request.url).to eq("/reports/payee_details")
    expect(request.request_method).to eq(:get)
    expect(request.params).to eq({ payee_id: payout_provider.client_payee_id })
  end
end
