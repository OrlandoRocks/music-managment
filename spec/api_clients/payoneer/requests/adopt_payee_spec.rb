require "rails_helper"

describe Payoneer::Requests::AdoptPayee do
  let(:person) { create(:person) }
  let!(:payout_provider) { create(:payout_provider, person: person) }

  shared_examples "a valid request" do
    it "generates a valid request" do
      expect(request.url).to eq(expected_url)
      expect(request.request_method).to eq(:post)
      expect(request.params).to eq(expected_params)
    end
  end

  context "using v2 API" do
    let(:request) { Payoneer::Requests::AdoptPayee.new(person: person) }
    let(:expected_url) { "/payees/#{person.payout_provider.client_payee_id}/adopt-payee-id" }
    let(:expected_params) do
      {
        partner_id_to_adopt: person.payout_provider_config.program_id,
        payee_id_to_adopt: person.client_payee_id
      }
    end

    it_behaves_like "a valid request"
  end

  context "using v4 API" do
    let(:request) { Payoneer::Requests::AdoptPayee.new({ person: person }, :v4) }
    let(:expected_url) { "/payees/adopt_payee" }
    let(:expected_params) do
      {
        partner_id_to_adopt: person.payout_provider_config.program_id,
        payee_id_to_adopt: person.client_payee_id,
        calling_partner_payee_id: person.client_payee_id
      }
    end

    it_behaves_like "a valid request"
  end
end
