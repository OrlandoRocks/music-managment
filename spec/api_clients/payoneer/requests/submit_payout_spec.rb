require "rails_helper"

describe Payoneer::Requests::SubmitPayout do
  let(:person) { create(:person) }
  let!(:payout_provider) { create(:payout_provider, person: person) }
  let(:transfer) { create(:payout_transfer, amount_cents: 10_000) }
  it "generates a request" do
    request = Payoneer::Requests::SubmitPayout.new({ transfer: transfer, person: person })
    expect(request.url).to eq("/payouts")
    expect(request.request_method).to eq(:post)
    expect(request.params).to include(
      payee_id: payout_provider.client_payee_id,
      amount: transfer.amount_to_transfer.to_s,
      client_reference_id: transfer.client_reference_id,
      description: "TuneCore payout transaction",
      group_id: person.id
    )
  end

  it "formats requested amount as per the US locale" do
    I18n.with_locale("fr") do
      request = Payoneer::Requests::SubmitPayout.new({ transfer: transfer, person: person })
      expect(request.params[:amount]).to eq(I18n.with_locale("en-us") { transfer.amount_to_transfer.to_s })
    end
  end
end
