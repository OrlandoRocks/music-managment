require 'rails_helper'

describe Payoneer::Requests::PayeeExtendedDetail do
  let(:person) { create(:person) }
  let!(:payout_provider) { create(:payout_provider, person: person) }

  context "for version v2" do
    before(:each) do
      allow(FeatureFlipper).to receive(:show_feature?).with(:use_payoneer_v4_api).and_return(false)
    end

    it "generates a request for v2 endpoint" do
      request = Payoneer::Requests::PayeeExtendedDetail.new(person: person)
      expect(request.url).to eq("/payees/#{person.payout_provider.client_payee_id}/extended_details")
      expect(request.request_method).to eq(:get)
      expect(request.params).to eq({})
    end
  end

  context "for version v4" do
    before(:each) do
      allow(FeatureFlipper).to receive(:show_feature?).with(:use_payoneer_v4_api).and_return(true)
    end

    it "generates a request for v4 endpoint" do
      request = Payoneer::Requests::PayeeExtendedDetail.new({ person: person }, :v4)
      expect(request.url).to eq("/payees/#{person.payout_provider.client_payee_id}/details")
      expect(request.request_method).to eq(:get)
      expect(request.params).to eq({})
    end
  end
end
