require 'rails_helper'

describe Payoneer::Requests::PayoutDetail do
  let(:transfer) { create(:payout_transfer) }
  it "generates a request" do
    request = Payoneer::Requests::PayoutDetail.new(transfer: transfer)
    expect(request.url).to eq("/payouts/#{transfer.client_reference_id}")
    expect(request.request_method).to eq(:get)
    expect(request.params).to eq({})
  end
end
