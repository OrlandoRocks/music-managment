require 'rails_helper'

describe Payoneer::Requests::RegisterExistingUser do
  let(:person) { create(:person) }
  let!(:payout_provider) { create(:payout_provider, person: person) }
  it "generates a request" do
    request = Payoneer::Requests::RegisterExistingUser.new(person: person, redirect_url: "https://test.tunecore.com/my_account")
    expect(request.url).to eq("/payees/login-link")
    expect(request.request_method).to eq(:post)
    expect(request.params).to eq(
      payee_id: payout_provider.client_payee_id,
      language_id: 1,
      redirect_url: "https://test.tunecore.com/my_account",
      redirect_time: 0
    )
  end
end
