require 'rails_helper'

describe Payoneer::Requests::CancelPayout do
  let(:transfer) { create(:payout_transfer) }
  it "generates a request" do
    request = Payoneer::Requests::CancelPayout.new(transfer: transfer)
    expect(request.url).to eq("/payouts/#{transfer.client_reference_id}/cancel")
    expect(request.request_method).to eq(:post)
    expect(request.params).to eq({})
  end
end
