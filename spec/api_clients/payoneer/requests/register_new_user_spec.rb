require 'rails_helper'

describe Payoneer::Requests::RegisterNewUser do
  let(:person) { create(:person) }
  let!(:payout_provider) { create(:payout_provider, person: person) }

  context "for version v2" do
    before(:each) do
      allow(FeatureFlipper).to receive(:show_feature?).with(:use_payoneer_v4_api).and_return(false)
    end

    it "generates a request" do
      request_params = {
        person: person,
        redirect_url: "http://test.tunecore.com/my_account",
        payout_method_list: ["PAYPAL"]
      }
      request = Payoneer::Requests::RegisterNewUser.new(request_params)
      expect(request.url).to eq("/payees/registration-link")
      expect(request.request_method).to eq(:post)
      expect(request.params.keys).to include(:payee_id, :language_id, :type, :contact)
    end
  end

  context "for version v4" do
    before(:each) do
      allow(FeatureFlipper).to receive(:show_feature?).with(:use_payoneer_v4_api).and_return(true)
    end

    it "generates a request" do
      request_params = {
        person: person,
        redirect_url: "http://test.tunecore.com/my_account",
        payout_method_list: ["PAYPAL"]
      }
      request = Payoneer::Requests::RegisterNewUser.new(request_params, :v4)
      expect(request.url).to eq("/payees/registration-link")
      expect(request.request_method).to eq(:post)
      expect(request.params.keys).to include(:payee_id, :language_id, :payee, :payout_methods)
    end
  end
end
