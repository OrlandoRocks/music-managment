require "rails_helper"

describe Payoneer::Requests::PayeeStatus do
  let(:person) { create(:person) }
  let!(:payout_provider) { create(:payout_provider, person: person) }
  it "generates a request" do
    request = Payoneer::Requests::PayeeStatus.new(person: person)
    expect(request.url).to eq("/payees/#{payout_provider.client_payee_id}/status")
    expect(request.request_method).to eq(:get)
    expect(request.params).to eq({})
  end
end
