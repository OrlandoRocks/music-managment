require 'rails_helper'

describe Payoneer::Requests::MoveProgram do
  let!(:person) { create(:person) }
  let!(:payout_provider) { create(:payout_provider, person: person) }
  let!(:program) { create(:payout_provider_config) }
  it "generates a request" do
    request = Payoneer::Requests::MoveProgram.new({person: person, target_program: program}, :v2)

    expect(request.url).to eq("/payees/#{person.payout_provider.client_payee_id}/move-program")
    expect(request.request_method).to eq(:post)
    expect(request.params).to eq({ target_program_id: program.program_id})
  end
  it "raises error in production" do
    with_rails_env(:production) do
      expect{Payoneer::Requests::MoveProgram.new({person: person, target_program: program}, :v2)}
        .to raise_error(Payoneer::Requests::MoveProgram::WrongEnvError)
    end
  end
end
