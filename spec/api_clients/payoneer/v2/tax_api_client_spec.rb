require "rails_helper"

describe Payoneer::V2::TaxApiClient do
  before(:context) do
    WebMock.disable_net_connect!(allow_localhost: true)
  end

  after(:context) do
    WebMock.allow_net_connect!
  end

  let(:payoneer_tax_api_base_url) { "https://api.sandbox.payoneer.com/Payouts/HttpApi/API.aspx" }

  describe ".post" do
    let!(:person) { create(:person, :with_payout_provider, :with_tax_form, :with_address) }
    let(:form_xml_response_fixture) do
      file_fixture("payoneer_apis/tax_forms_collection_service/get_submitted_tax_forms_for_payee/form.xml")
    end
    let(:xml_response_doc) do
      Nokogiri::XML::Builder.new(encoding: "UTF-8") do |xml|
        xml.GetSubmittedTaxFormsForPayee {
          xml.Code(xml_response_code)
          xml.Description(xml_response_description)
          xml.NumberOfTaxForms(number_of_tax_forms)
          xml.TaxForms
        }
      end.doc
    end

    context "when API is running" do
      before do
        stub_request(:post, payoneer_tax_api_base_url).to_return(status: 200, body: response_body)
      end

      context "with all params set" do
        let(:post_params) do
          {
            PartnerPayeeID: person.client_payee_id,
            mname: "GetSubmittedTaxFormsForPayee",
            FromDate: Date.parse("2000-01-01"),
            ToDate: Date.today
          }
        end
        let(:xml_response_code) { "000" }
        let(:xml_response_description) { "OK (Request accepted)" }

        context "when a user has no tax forms with Payoneer" do
          let(:number_of_tax_forms) { 0 }
          let(:response_body) do
            xml_response_doc.root.at_css("NumberOfTaxForms").content = number_of_tax_forms
            tax_forms_element = xml_response_doc.at_css("TaxForms")

            number_of_tax_forms.times do
              form_element = Nokogiri::XML(form_xml_response_fixture.read).root
              tax_form_response_attrs.each { |key, value| form_element.at_css(key).content = value }
              form_element.parent = tax_forms_element
            end

            xml_response_doc.to_xml
          end

          it "returns the number of tax forms as 0" do
            client = Payoneer::V2::TaxApiClient.post(person, post_params)
            expect(client.formatted_response["GetSubmittedTaxFormsForPayee"]["NumberOfTaxForms"]).to eq "0"
          end
        end

        context "when a user has filed tax forms with Payoneer" do
          let(:tax_form_response_attrs) do
            {
              "Name" => person.name,
              "Address" => person.address1 + ", " + person.address2,
              "City" => person.city,
              "Zip" => person.zip,
              "Country" => "United States of America",
              "State" => person.state,
              "TaxPayerIdType" => "Social security number",
              "TaxPayerIdNumber" => "000000000"
            }
          end

          let(:response_body) do
            xml_response_doc.root.at_css("NumberOfTaxForms").content = number_of_tax_forms
            tax_forms_element = xml_response_doc.at_css("TaxForms")

            number_of_tax_forms.times do |i|
              form_element = Nokogiri::XML(form_xml_response_fixture.read).root
              tax_form_response_attrs.each { |k, v| form_element.at_css(k).content = v }
              form_element.parent = tax_forms_element
            end

            xml_response_doc.to_xml
          end

          context "with 1 tax form" do
            let(:number_of_tax_forms) { 1 }

            it "returns the correct number of tax forms that the person has on Payoneer" do
              client = Payoneer::V2::TaxApiClient.post(person, post_params)
              expect(client.formatted_response["GetSubmittedTaxFormsForPayee"]["NumberOfTaxForms"]).to eq "1"
            end

            it "returns the expected attributes for the tax form" do
              client = Payoneer::V2::TaxApiClient.post(person, post_params)
              expect(client.formatted_response["GetSubmittedTaxFormsForPayee"]["TaxForms"]["Form"]).to include(
                tax_form_response_attrs.reject { |k, _v| k.start_with?("TaxPayerId") }
              )
            end

            it "removes sensitive attributes from the returned tax form" do
              client = Payoneer::V2::TaxApiClient.post(person, post_params)
              expect(client.formatted_response["GetSubmittedTaxFormsForPayee"]["TaxForms"]["Form"]).to include(
                "TaxPayerIdType" => "",
                "TaxPayerIdNumber" => ""
              )
            end
          end

          context "with more than 1 tax form" do
            let(:number_of_tax_forms) { 2 }
            let(:response_body) do
              xml_response_doc.root.at_css("NumberOfTaxForms").content = number_of_tax_forms
              tax_forms_element = xml_response_doc.at_css("TaxForms")

              number_of_tax_forms.times do |i|
                form_element = Nokogiri::XML::DocumentFragment.parse(form_xml_response_fixture.read)
                tax_form_response_attrs.each { |key, value| form_element.at_css(key).content = value }
                form_element.parent = tax_forms_element
              end

              xml_response_doc.to_xml
            end

            it "returns all tax forms" do
              client = Payoneer::V2::TaxApiClient.post(person, post_params)
              tax_forms = client.formatted_response["GetSubmittedTaxFormsForPayee"]["TaxForms"]["Form"]
              expect(tax_forms.count).to eq 2
            end

            it "returns the correct number of tax forms that the person has on Payoneer" do
              client = Payoneer::V2::TaxApiClient.post(person, post_params)
              expect(client.formatted_response["GetSubmittedTaxFormsForPayee"]["NumberOfTaxForms"]).to eq "2"
            end

            it "returns the expected attributes for the tax forms" do
              client = Payoneer::V2::TaxApiClient.post(person, post_params)
              tax_forms = client.formatted_response["GetSubmittedTaxFormsForPayee"]["TaxForms"]["Form"]
              tax_forms.each do |tax_form|
                expect(tax_form).to include(
                  tax_form_response_attrs.reject { |k, v| k =~ /^(TaxPayerId|TaxPayerId)/ }
                )
              end
            end

            it "removes sensitive attributes from the returned tax forms" do
              client = Payoneer::V2::TaxApiClient.post(person, post_params)
              tax_forms = client.formatted_response["GetSubmittedTaxFormsForPayee"]["TaxForms"]["Form"]
              tax_forms.each do |tax_form|
                expect(tax_form).to include(
                  "TaxPayerIdType" => "",
                  "TaxPayerIdNumber" => ""
                )
              end
            end
          end
        end
      end

      context "when required mname param is missing" do
        let(:post_params) do
          {
            PartnerPayeeID: person.client_payee_id,
            FromDate: Date.parse("2000-01-01"),
            ToDate: Date.today
          }
        end
        let(:response_body) { nil }

        it "raises an error" do
          expect {
            Payoneer::V2::TaxApiClient.post(person, post_params)
          }.to raise_error(
            Payoneer::V2::TaxApiClient::ApiError,
            "Error communicating with Payoneer API.\n" \
            "Description: Missing or incorrect params: mname"
          )
        end
      end

      context "when required params, other than mname, are missing" do
        let(:post_params) do
          {
            FromDate: Date.parse("2000-01-01"),
            ToDate: Date.today,
            mname: "GetSubmittedTaxFormsForPayee"
          }
        end
        let(:response_body) { nil }

        it "raises an error" do
          expect {
            Payoneer::V2::TaxApiClient.post(person, post_params)
          }.to raise_error(
            Payoneer::V2::TaxApiClient::ApiError,
            "Error communicating with Payoneer API.\n" \
            "Description: Missing or incorrect params: PartnerPayeeID"
          )
        end
      end

      context "when an error is received from the API" do
        let(:post_params) do
          {
            PartnerPayeeID: "ABC-123",
            mname: "GetSubmittedTaxFormsForPayee",
            FromDate: Date.parse("2000-01-01"),
            ToDate: Date.today
          }
        end
        let(:xml_response_doc) do
          Nokogiri::XML::Builder.new(encoding: "UTF-8") do |xml|
            xml.GetSubmittedTaxFormsForPayee {
              xml.Code(xml_response_code)
              xml.Description(xml_response_description)
            }
          end.doc
        end
        let(:response_body) { xml_response_doc.to_xml }

        context "when payee ID doesn't exist in Payoneer" do
          let(:xml_response_code) { "PE1011" }
          let(:xml_response_description) { "Required field is missing" }

          xit "raises an error" do
            expect {
              Payoneer::V2::TaxApiClient.post(person, post_params)
            }.to raise_error(
              Payoneer::V2::TaxApiClient::ApiError,
              "Error communicating with Payoneer API.\n" \
              "Error Code: PE1011\n" \
              "Description: Required field is missing"
            )
          end
        end

        context "when payee ID doesn't match the program ID" do
          let(:xml_response_code) { "054" }
          let(:xml_response_description) { "Payee does not match the Partner" }

          xit "raises an error" do
            expect {
              Payoneer::V2::TaxApiClient.post(person, post_params)
            }.to raise_error(
              Payoneer::V2::TaxApiClient::ApiError,
              "Error communicating with Payoneer API.\n" \
              "Error Code: 054\n" \
              "Description: Payee does not match the Partner"
            )
          end
        end

        context "when access is unauthorized" do
          let(:xml_response_code) { "A00B556F" }
          let(:xml_response_description) do
            "Unauthorized Access or invalid parameters, " \
            "please check your IP address and parameters."
          end

          xit "raises an error" do
            expect {
              Payoneer::V2::TaxApiClient.post(person, post_params)
            }.to raise_error(
              Payoneer::V2::TaxApiClient::ApiError,
              "Error communicating with Payoneer API.\n" \
              "Error Code: A00B556F\n" \
              "Description: Unauthorized Access or invalid parameters, " \
              "please check your IP address and parameters."
            )
          end
        end
      end
    end
  end
end
