require "rails_helper"

describe Payoneer::V2::ApiClient do
  let(:person) {FactoryBot.create(:person)}
  let(:client) {Payoneer::V2::ApiClient.new}
  let(:response) {double("response")}

  describe "#post" do
    context "InitializeTaxFormForPayee request" do
      let(:success_response_body) {
        {
            "InitializeTaxFormForPayee" => {"Code" => "000"}
        }
      }

      let(:failure_response_body) {
        {
            "InitializeTaxFormForPayee" => {"Code" => "055"}
        }
      }
      context "success response" do
        before do
          allow(client).to receive(:execute_post).and_return(response)
          allow(response).to receive(:body).and_return("")
          allow(Hash).to receive(:from_xml).and_return(success_response_body)
        end
        it "does not raise an error" do
          expect{
            client.post({PartnerPayeeID: person.id, mname: "InitializeTaxFormForPayee"})
          }.to_not raise_error
        end
      end

      context "error response" do
        before do
          allow(client).to receive(:execute_post).and_return(response)
          allow(response).to receive(:body).and_return("")
          allow(Hash).to receive(:from_xml).and_return(failure_response_body)
        end
        it "raises and error" do
          expect{
            client.post({PartnerPayeeID: person.id, mname: "InitializeTaxFormForPayee"})
          }.to raise_error
        end
      end
    end

    context "GetPendingTaxFormInvitedPayees request" do
      let(:response_body) {
        {
            "GetPendingTaxFormInvitedPayees" => {
                "TaxFormLinks" => {
                    "Link" => {
                        "LinkFormInviteUrl" => "https://payoneer.com/tax_forms?tk=12345"
                    }
                }
            }
        }
      }
      before do
        allow(client).to receive(:execute_post).and_return(response)
        allow(response).to receive(:body).and_return("")
        allow(Hash).to receive(:from_xml).and_return(response_body)
      end
      it "returns the token associated to the refreshed user" do
        expect(
            client.post({PartnerPayeeID: person.id, mname: "GetPendingTaxFormInvitedPayees"})
        ).to eq("12345")
      end
    end
  end
end
