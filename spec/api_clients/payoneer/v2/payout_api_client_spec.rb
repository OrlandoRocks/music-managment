require "rails_helper"

describe Payoneer::V2::PayoutApiClient do

  let(:person) { create(:person) }
  let!(:payout_provider) { create(:payout_provider, person: person) }
  let(:transfer) { create(:payout_transfer, payout_provider: payout_provider) }

  describe "#send_request" do
    subject { Payoneer::V2::PayoutApiClient.new(person: person) }

    it "sends a person_status request" do
      expect(subject).to receive(:get_response).with("/payees/#{payout_provider.client_payee_id}/status", {})
      subject.send_request(:payee_status, { person: person })
    end

    it "sends a register_existing_user request" do
      expect(subject).to receive(:post_response).with("/payees/login-link", { payee_id: payout_provider.client_payee_id, language_id: 1, redirect_url: "https://test.tunecore.com/my_account", redirect_time: 0 }, :register_existing_users, nil)
      subject.send_request(:register_existing_users, { person: person, redirect_url: "https://test.tunecore.com/my_account" })
    end

    it "sends a register_new_user request" do
      register_new_user_params = {
        payee_id: person.client_payee_id,
        language_id: 1,
        type: "INDIVIDUAL",
        redirect_url: "https://test.tunecore.com/my_account",
        contact: { email: person.email },
        address: {
          country: person.country,
          state: person.state,
          address_line_1: person.address1,
          address_line_2: person.address2,
          city: person.city,
          zip_code: person.zip
        },
        payout_methods_list: ["PAYPAL"]
      }
      expect(subject).to receive(:post_response).with("/payees/registration-link", register_new_user_params, :register_new_users, nil)
      subject.send_request(:register_new_users, { person: person, redirect_url: "https://test.tunecore.com/my_account", payout_methods_list: "PAYPAL", language_id: 1 })
    end

    it "sends a submit_payout request" do
      submit_request_params = {
        payee_id: person.client_payee_id,
        client_reference_id: transfer.client_reference_id,
        amount: "11.00",
        description: "TuneCore payout transaction",
        group_id: person.id
      }
      expect(subject).to receive(:post_response).with("/payouts", submit_request_params, :submit_payouts, "sample-url")
      subject.send_request(:submit_payouts, { person: person, transfer: transfer, referrer_url: "sample-url" })
    end

    it "sends a cancel_payout request" do
      expect(subject).to receive(:post_response).with("/payouts/#{transfer.client_reference_id}/cancel", {}, :cancel_payouts, nil)
      subject.send_request(:cancel_payouts, { transfer: transfer })
    end

    it "sends a payee_report request" do
      expect(subject).to receive(:get_response).with("/reports/payee_details", { payee_id: payout_provider.client_payee_id })
      subject.send_request(:payee_reports, { person: person })
    end

    it "sends a payout_details request" do
      expect(subject).to receive(:get_response).with("/payouts/#{transfer.client_reference_id}", { })
      subject.send_request(:payout_details, { transfer: transfer })
    end

    it "raises airbrake issue for response failures" do
      response = double(:response, body: "sample", status: 403)
      http_client = double(:http_client, get: response).as_null_object
      expect(Faraday).to receive(:new).and_return(http_client)

      expect(http_client).to receive(:get).with("/v2/programs/100084980/payouts/#{transfer.client_reference_id}")
      expect(Tunecore::Airbrake).to receive(:notify)
      subject.send_request(:payout_details, { transfer: transfer })
    end

    it "should not raise any airbrake error for successful responses from payoneer" do
      response = double(:response, body: "sample", status: 200)
      http_client = double(:http_client, get: response).as_null_object
      expect(Faraday).to receive(:new).and_return(http_client)

      expect(http_client).to receive(:get).with("/v2/programs/100084980/payouts/#{transfer.client_reference_id}")
      expect(Tunecore::Airbrake).to_not receive(:notify)
      subject.send_request(:payout_details, { transfer: transfer })
    end

    it "raises an error when amount includes a comma" do
      expect {
        subject.send(:post_response, "sample-url", { amount: "11,50", client_reference_id: transfer.client_reference_id }, :submit_payouts, referrer_url: "sample-url")
      }.to raise_error(RuntimeError, "Malformed/Mismatched amount in payout transfer API request")
    end

    it "raises an airbrake error when requested amount doesn't match disbursed amount" do
      response = double(:response, body: "{\"amount\":1100.0}", status: 200)
      http_client = double(:http_client, post: response).as_null_object
      expect(Faraday).to receive(:new).and_return(http_client)

      expect(http_client).to receive(:post).with("/v2/programs/100084980/payouts")
      expect(transfer.amount_to_transfer.to_s).to eq("11.00")
      expect(Tunecore::Airbrake).to receive(:notify)
      subject.send_request(:submit_payouts, { person: person, transfer: transfer, referrer_url: "sample-url" })
    end

    it "doesn't raise an airbrake error when requested amount matches disbursed amount" do
      response = double(:response, body: "{\"amount\":11.0}", status: 200)
      http_client = double(:http_client, post: response).as_null_object
      expect(Faraday).to receive(:new).and_return(http_client)

      expect(http_client).to receive(:post).with("/v2/programs/100084980/payouts")
      expect(transfer.amount_to_transfer.to_s).to eq("11.00")
      expect(Tunecore::Airbrake).not_to receive(:notify)
      subject.send_request(:submit_payouts, { person: person, transfer: transfer, referrer_url: "sample-url" })
    end
  end
end
