require "rails_helper"

describe Payoneer::V4::PayoutApiClient do
  let(:person) { create(:person) }
  let!(:payout_provider) { create(:payout_provider, person: person) }
  let(:transfer) { create(:payout_transfer, payout_provider: payout_provider) }

  before do
    allow(Payoneer::V4::TokenGenerationService).to receive(:fetch_token).and_return("ABCD")
  end

  describe "#send_request" do
    subject { described_class.new(person: person) }
    let(:endpoint_url) { "/v4/programs/100084980/masspayouts" }

    context "when sending 'submit_payouts' request" do
      it "sends a submit_payout request" do
        submit_request_params = {
          Payments: [{
            payee_id: person.client_payee_id,
            client_reference_id: transfer.client_reference_id,
            amount: "11.00",
            description: "TuneCore payout transaction",
            group_id: person.id
          }]
        }
        expect(subject).to receive(:post_response).with("/masspayouts", submit_request_params, :submit_payouts, "sample-url")
        subject.send_request(:submit_payouts, { person: person, transfer: transfer, referrer_url: "sample-url" })
      end

      it "raises airbrake issue for response failures" do
        response = double(:response, body: "", status: 403)
        http_client = double(:http_client, post: response).as_null_object
        expect(Faraday).to receive(:new).and_return(http_client)

        expect(http_client).to receive(:post).with(endpoint_url)
        expect(Tunecore::Airbrake).to receive(:notify)
        subject.send_request(:submit_payouts, { person: person, transfer: transfer, referrer_url: "sample-url" })
      end

      it "should not raise any airbrake error for successful responses from payoneer" do
        response = double(:response, body: "", status: 200)
        http_client = double(:http_client, post: response).as_null_object
        expect(Faraday).to receive(:new).and_return(http_client)

        expect(http_client).to receive(:post).with(endpoint_url)
        expect(Tunecore::Airbrake).to_not receive(:notify)
        subject.send_request(:submit_payouts, { person: person, transfer: transfer, referrer_url: "sample-url" })
      end

      it "raises an error when amount includes a comma" do
        expect {
          subject.send(:post_response, "sample-url", { Payments: [{ amount: "11,50", client_reference_id: transfer.client_reference_id }] }, :submit_payouts, referrer_url: "sample-url")
        }.to raise_error(RuntimeError, "Malformed amount in payout transfer API request")
      end
    end
  end
end
