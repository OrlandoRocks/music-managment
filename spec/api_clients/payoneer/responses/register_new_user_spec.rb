require "rails_helper"

describe Payoneer::Responses::RegisterNewUser do
  context "#registration_link" do

    it "should return a registration link from the response" do
      response_object = {registration_link: "www.google.com"}
      response = double("response", body: JSON.dump(response_object))
      payee_detail = Payoneer::Responses::RegisterNewUser.new(response)
      expect(payee_detail.registration_link).to eq("www.google.com")
    end

    it "should return an empty string if no link is found in the response" do
      response = double("response", body: JSON.dump({}))
      payee_detail = Payoneer::Responses::RegisterNewUser.new(response)
      expect(payee_detail.registration_link).to eq("")
    end
  end

  context "#success?" do
    it "should return true if 'Success' is returned in the description" do
      response_object = {description: "Success"}
      response = double("response", body: JSON.dump(response_object))
      payee_detail = Payoneer::Responses::RegisterNewUser.new(response)
      expect(payee_detail.success?).to eq(true)
    end

    it "should return false if anything other than 'Success' is returned" do
      response = double("response", body: JSON.dump({}))
      payee_detail = Payoneer::Responses::RegisterNewUser.new(response)
      expect(payee_detail.success?).to eq(false)
    end
  end
end