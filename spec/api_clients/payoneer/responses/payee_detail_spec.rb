require "rails_helper"

describe Payoneer::Responses::PayeeDetail do
  describe "#withdrawal_type" do
    it "should return the withdrawal type from the response" do
      response_object = {payout_method: {type: "ACCOUNT"}}
      response = double("response", body: JSON.dump(response_object))
      payee_detail = Payoneer::Responses::PayeeDetail.new(response)
      expect(payee_detail.withdrawal_type).to eq("ACCOUNT")
    end

    context "the response does not have a withdrawal type" do
      it "should return an empty string" do
        response = double("response", body: JSON.dump({}))
        payee_detail = Payoneer::Responses::PayeeDetail.new(response)
        expect(payee_detail.withdrawal_type).to eq("")
      end
    end
  end
  
  describe "#country" do
    it "should return the country from the response" do
      response_object = {address: {country: "US"}}
      response = double("response", body: JSON.dump(response_object))
      payee_detail = Payoneer::Responses::PayeeDetail.new(response)
      expect(payee_detail.country).to eq("US")
    end

    context "the response does not have a country" do
      it "should return an empty string" do
        response = double("response", body: JSON.dump({}))
        payee_detail = Payoneer::Responses::PayeeDetail.new(response)
        expect(payee_detail.country).to eq("")
      end
    end
  end
end
