require "rails_helper"

describe Payoneer::Responses::SubmitPayout do
  describe "#description" do
    it "should return the description from the response" do
      response_object = {description: "success"}
      response = double('response', body: JSON.dump(response_object))
      submit_payout_response = Payoneer::Responses::SubmitPayout.new(response)
      expect(submit_payout_response.description).to eq("success")
    end


    context "no description key is found" do
      it "should return nil" do
        response = double('response', body: JSON.dump({}))
        submit_payout_response = Payoneer::Responses::SubmitPayout.new(response)
        expect(submit_payout_response.description).to eq(nil)
      end
    end
  end

  describe "#code" do
    it "should return the code from the response" do
      response_object = {code: "0"}
      response = double('response', body: JSON.dump(response_object))
      submit_payout_response = Payoneer::Responses::SubmitPayout.new(response)
      expect(submit_payout_response.code).to eq("0")
    end

    context "no code key is found" do
      it "should return nil" do
        response = double('response', body: JSON.dump({}))
        submit_payout_response = Payoneer::Responses::SubmitPayout.new(response)
        expect(submit_payout_response.code).to eq(nil)
      end
    end
  end

  describe "#contents" do
    it "returns the unparsed JSON response body" do
      response_body = JSON.dump({description: "success"})
      response = double('response', body: response_body)

      submit_payout_response = Payoneer::Responses::SubmitPayout.new(response)
      expect(submit_payout_response.contents).to eq(response_body)
    end
  end
end
