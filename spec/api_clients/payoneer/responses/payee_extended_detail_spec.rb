require "rails_helper"

describe Payoneer::Responses::PayeeExtendedDetail do
  describe "#withdrawal_type" do

    it "should return the withdrawal type from the response" do
      response_object = {payout_method: {type: "ACCOUNT"}}
      response = double("response", body: JSON.dump(response_object), status: 200)
      payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
      expect(payee_extended_detail.withdrawal_type).to eq("ACCOUNT")
    end

    context "response does not have the withdrawal type" do
      it "should return an empty string" do
        response = double("response", body: JSON.dump({}), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
        expect(payee_extended_detail.withdrawal_type).to eq("")
      end
    end

    context "for v4 API version" do
      it "returns an empty string" do
        response_object = {payout_method: {type: "ACCOUNT"}}
        response = double("response", body: JSON.dump(response_object), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response, :v4)
        expect(payee_extended_detail.withdrawal_type).to eq("")
      end
    end
  end

  describe "#withdrawal_currency" do
    it "should return the withdrawal currency from the response" do
      response_object = { payout_method: { type: "ACCOUNT", currency: "USD" } }
      response = double("response", body: JSON.dump(response_object), status: 200)
      payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
      expect(payee_extended_detail.withdrawal_currency).to eq("USD")
    end

    context "response does not have the withdrawal currency" do
      it "should return an empty string" do
        response = double("response", body: JSON.dump({}), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
        expect(payee_extended_detail.withdrawal_type).to eq("")
      end
    end

    context "for v4 API version" do
      it "returns an empty string" do
        response_object = { payout_method: { type: "ACCOUNT", currency: "USD" } }
        response = double("response", body: JSON.dump(response_object), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response, :v4)
        expect(payee_extended_detail.withdrawal_currency).to eq("")
      end
    end
  end

  describe "#withdrawal_details" do
    it "should return the withdrawal details hash from the response" do
      response_object = { payout_method: { details: { bank_name: "chase", account_type: "C" } } }
      response = double("response", body: JSON.dump(response_object), status: 200)
      payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
      expect(payee_extended_detail.withdrawal_details).to eq({ bank_name: "chase", account_type: "C" }.with_indifferent_access)
    end

    context "response does not have details section" do
      it "should return nil" do
        response = double("response", body: JSON.dump({}), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
        expect(payee_extended_detail.withdrawal_details).to be_nil
      end
    end

    context "for v4 API version" do
      it "returns nil" do
        response_object = { payout_method: { details: { bank_name: "chase", account_type: "C" } } }
        response = double("response", body: JSON.dump(response_object), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response, :v4)
        expect(payee_extended_detail.withdrawal_details).to be_nil
      end
    end
  end

  describe "#withdrawal_bank" do
    it "should return the withdrawal bank name from the response" do
      response_object = { payout_method: { details: { bank_name: "chase", account_type: "C" } } }
      response = double("response", body: JSON.dump(response_object), status: 200)
      payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
      expect(payee_extended_detail.withdrawal_bank).to eq("chase")
    end

    context "response does not have the withdrawal bank" do
      it "should return an empty string" do
        response = double("response", body: JSON.dump({}), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
        expect(payee_extended_detail.withdrawal_bank).to eq("")
      end
    end

    context "for v4 API version" do
      it "returns an empty string" do
        response_object = { payout_method: { details: { bank_name: "chase", account_type: "C" } } }
        response = double("response", body: JSON.dump(response_object), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response, :v4)
        expect(payee_extended_detail.withdrawal_bank).to eq("")
      end
    end
  end

  describe "#withdrawal_bank_account_type" do
    it "should return the withdrawal bank account type from the response" do
      response_object = { payout_method: { details: { bank_name: "chase", account_type: "C" } } }
      response = double("response", body: JSON.dump(response_object), status: 200)
      payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
      expect(payee_extended_detail.withdrawal_bank_account_type).to eq("C")
    end

    context "response does not have the withdrawal bank account type" do
      it "should return an empty string" do
        response = double("response", body: JSON.dump({}), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
        expect(payee_extended_detail.withdrawal_bank_account_type).to eq("")
      end
    end

    context "for v4 API version" do
      it "returns an empty string" do
        response_object = { payout_method: { details: { bank_name: "chase", account_type: "C" } } }
        response = double("response", body: JSON.dump(response_object), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response, :v4)
        expect(payee_extended_detail.withdrawal_bank_account_type).to eq("")
      end
    end
  end

  describe "#country" do
    it "should return the country from the response" do
      response_object = {address: {country: "US"}}
      response = double("response", body: JSON.dump(response_object), status: 200)
      payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
      expect(payee_extended_detail.country).to eq("US")
    end

    context "response does not have country" do
      it "should return nil" do
        response = double("response", body: JSON.dump({}), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
        expect(payee_extended_detail.country).to eq(nil)
      end
    end

    context "for v4 API version" do
      it "returns the country from the response" do
        response_object = {result: {address: {country: "US"}}}
        response = double("response", body: JSON.dump(response_object), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response, :v4)
        expect(payee_extended_detail.country).to eq("US")
      end
    end
  end

  describe "#address_line_1" do
    it "should return the address_line_1 from the response" do
      response_object = {address: {address_line_1: "1234 Counting Lane"}}
      response = double("response", body: JSON.dump(response_object), status: 200)
      payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
      expect(payee_extended_detail.address_line_1).to eq("1234 Counting Lane")
    end

    context "response does not have address_line_1" do
      it "should return nil" do
        response = double("response", body: JSON.dump({}), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
        expect(payee_extended_detail.address_line_1).to eq(nil)
      end
    end

    context "for v4 API version" do
      it "returns the address_line_1 from the response" do
        response_object = {result: {address: {address_line_1: "1234 Counting Lane"}}}
        response = double("response", body: JSON.dump(response_object), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response, :v4)
        expect(payee_extended_detail.address_line_1).to eq("1234 Counting Lane")
      end  
    end
  end

  describe "#address_line_2" do
    it "should return the address_line_2 from the response" do
      response_object = {address: {address_line_2: "Apt 9A"}}
      response = double("response", body: JSON.dump(response_object), status: 200)
      payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
      expect(payee_extended_detail.address_line_2).to eq("Apt 9A")
    end

    context "response does not have address_line_2" do
      it "should return nil" do
        response = double("response", body: JSON.dump({}), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
        expect(payee_extended_detail.address_line_2).to eq(nil)
      end
    end

    context "for v4 API version" do
      it "returns the address_line_2 from the response" do
        response_object = {result: {address: {address_line_2: "Apt 9A"}}}
        response = double("response", body: JSON.dump(response_object), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response, :v4)
        expect(payee_extended_detail.address_line_2).to eq("Apt 9A")
      end  
    end
  end

  describe "#city" do
    it "should return the city from the response" do
      response_object = {address: {city: "Baltimore"}}
      response = double("response", body: JSON.dump(response_object), status: 200)
      payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
      expect(payee_extended_detail.city).to eq("Baltimore")
    end

    context "response does not have city" do
      it "should return nil" do
        response = double("response", body: JSON.dump({}), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
        expect(payee_extended_detail.city).to eq(nil)
      end
    end

    context "for v4 API version" do
      it "returns the city from the response" do
        response_object = {result: {address: {city: "Baltimore"}}}
        response = double("response", body: JSON.dump(response_object), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response, :v4)
        expect(payee_extended_detail.city).to eq("Baltimore")
      end
    end
  end

  describe "#state" do
    it "should return the state from the response" do
      response_object = {address: {state: "New York"}}
      response = double("response", body: JSON.dump(response_object), status: 200)
      payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
      expect(payee_extended_detail.state).to eq("New York")
    end

    context "response does not have state" do
      it "should return nil" do
        response = double("response", body: JSON.dump({}), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
        expect(payee_extended_detail.state).to eq(nil)
      end
    end

    context "for v4 API version" do
      it "returns the state from the response" do
        response_object = {result: {address: {state: "New York"}}}
        response = double("response", body: JSON.dump(response_object), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response, :v4)
        expect(payee_extended_detail.state).to eq("New York")
      end
    end
  end

  describe "#zip_code" do
    it "should return the zip_code from the response" do
      response_object = {address: {zip_code: "11111"}}
      response = double("response", body: JSON.dump(response_object), status: 200)
      payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
      expect(payee_extended_detail.zip_code).to eq("11111")
    end

    context "response does not have zip_code" do
      it "should return nil" do
        response = double("response", body: JSON.dump({}), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
        expect(payee_extended_detail.zip_code).to eq(nil)
      end
    end

    context "for v4 API version" do
      it "returns the zip_code from the response" do
        response_object = {result: {address: {zip_code: "11111"}}}
        response = double("response", body: JSON.dump(response_object), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response, :v4)
        expect(payee_extended_detail.zip_code).to eq("11111")
      end
    end
  end

  describe "#company_name" do
    it "should return the company_name from the response" do
      response_object = {company: {name: "Beep Beep Who Got the Keys To The Jeep"}}
      response = double("response", body: JSON.dump(response_object), status: 200)
      payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
      expect(payee_extended_detail.company_name).to eq("Beep Beep Who Got the Keys To The Jeep")
    end

    context "response does not have company_name" do
      it "should return nil" do
        response = double("response", body: JSON.dump({}), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
        expect(payee_extended_detail.company_name).to eq(nil)
      end
    end

    context "for v4 API version" do
      it "returns the company_name from the response when KYC user type is COMPANY" do
        response_object = {result: {type: "COMPANY", contact: {first_name: "Beep Beep Who Got the Keys To The Jeep"}}}
        response = double("response", body: JSON.dump(response_object), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response, :v4)
        expect(payee_extended_detail.company_name).to eq("Beep Beep Who Got the Keys To The Jeep")
      end

      it "returns nil when KYC user type is INDIVIDUAL" do
        response_object = {result: {type: "INDIVIDUAL", contact: {first_name: "Beep Beep Who Got the Keys To The Jeep"}}}
        response = double("response", body: JSON.dump(response_object), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response, :v4)
        expect(payee_extended_detail.company_name).to eq(nil)
      end
    end
  end

  describe "#company?" do
    it "should return true if the KYC user type is COMPANY" do
      response_object = {type: "COMPANY"}
      response = double("response", body: JSON.dump(response_object), status: 200)
      payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
      expect(payee_extended_detail.company?).to be true
    end

    context "KYC response is not COMPANY" do
      it "should return false" do
        response_object = {type: "INDIVIDUAL"}
        response = double("response", body: JSON.dump({}), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
        expect(payee_extended_detail.company?).to be false
      end
    end

    context "for v4 API version" do
      it "returns true when KYC user type is COMPANY" do
        response_object = {result: {type: "COMPANY"}}
        response = double("response", body: JSON.dump(response_object), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response, :v4)
        expect(payee_extended_detail.company?).to be true  
      end
    end
  end

  describe "#individual?" do
    it "should return true if the KYC user type is INDIVIDUAL" do
      response_object = {type: "INDIVIDUAL"}
      response = double("response", body: JSON.dump(response_object), status: 200)
      payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
      expect(payee_extended_detail.individual?).to be true
    end

    context "KYC response is not INDIVIDUAL" do
      it "should return false" do
        response_object = {type: "COMPANY"}
        response = double("response", body: JSON.dump({}), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
        expect(payee_extended_detail.individual?).to be false
      end
    end

    context "for v4 API version" do
      it "returns true when KYC user type is INDIVIDUAL" do
        response_object = {result: {type: "INDIVIDUAL"}}
        response = double("response", body: JSON.dump(response_object), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response, :v4)
        expect(payee_extended_detail.individual?).to be true  
      end
    end
  end

  describe "#tc_customer_type" do
    it "return a valid TuneCore customer_type based on the KYC response" do
      response_object = {type: "INDIVIDUAL"}
      response = double("response", body: JSON.dump(response_object), status: 200)
      payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
      expect(payee_extended_detail.tc_customer_type).to eq("individual")
    end
    it "return a valid TuneCore customer_type based on the KYC response" do
      response_object = {type: "COMPANY"}
      response = double("response", body: JSON.dump(response_object), status: 200)
      payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
      expect(payee_extended_detail.tc_customer_type).to eq("business")
    end

    context "response does not have customer type" do
      it "should return nil" do
        response = double("response", body: JSON.dump({}), status: 200)
        payee_extended_detail = Payoneer::Responses::PayeeExtendedDetail.new(response)
        expect(payee_extended_detail.tc_customer_type).to eq(nil)
      end
    end
  end
end
