require "rails_helper"

describe Payoneer::Responses::AdoptPayee do
  let(:response_object) { Payoneer::Responses::AdoptPayee.new(response_double, version) }

  shared_examples "a success response" do
    context "with success response" do
      let(:status_code) { 200 }
      let(:response_body) { version == :v4 ? { result: {} } : { description: "Success" } }

      context "#success?" do
        subject(:described_method) { response_object.success? }
        it { is_expected.to eq true }
      end

      context "#payee_id_exists?" do
        subject(:described_method) { response_object.payee_id_exists? }
        it { is_expected.to eq false }
      end
    end
  end

  shared_examples "a generic error response" do
    context "with generic error response" do
      let(:status_code) { 400 }
      let(:response_body) { version == :v4 ? { error_details: { code: 2109 } } : {} }
      
      context "#success?" do
        subject(:described_method) { response_object.success? }
        it { is_expected.to eq false }
      end

      context "#payee_id_exists?" do
        subject(:described_method) { response_object.payee_id_exists? }
        it { is_expected.to eq false }
      end
    end
  end

  context "using v2 API" do
    let(:version) { :v2 }
    let(:response_double) { double("response", body: JSON.dump(response_body)) }

    it_behaves_like "a success response"
    it_behaves_like "a generic error response"

    context "#payee_id_exists?" do
      it "returns true when API reports error 12103" do
        response = double("response", body: JSON.dump({ code: 12_103 }))
        adopt_payee_response = Payoneer::Responses::AdoptPayee.new(response)
        expect(adopt_payee_response.payee_id_exists?).to eq(true)
      end

      it "returns false when API reports an error other than 12103" do
        response = double("response", body: JSON.dump({ code: 1234 }))
        adopt_payee_response = Payoneer::Responses::AdoptPayee.new(response)
        expect(adopt_payee_response.payee_id_exists?).to eq(false)
      end

      it "returns false when API reports success" do
        response = double("response", body: JSON.dump({ description: "Success" }))
        adopt_payee_response = Payoneer::Responses::AdoptPayee.new(response)
        expect(adopt_payee_response.payee_id_exists?).to eq(false)
      end
    end
  end

  context "using v4 API" do
    let(:version) { :v4 }
    let(:response_double) { double("response", body: JSON.dump(response_body), status: status_code) }

    it_behaves_like "a success response"
    it_behaves_like "a generic error response"

    context "with error response 12103 - payee ID already exists" do
      let(:status_code) { 400 }
      let(:response_body) { { error_details: { code: 2111 } } }

      context "#payee_id_exists?" do
        subject(:described_method) { response_object.payee_id_exists? }
        it { is_expected.to eq true }
      end
    end
  end
end
