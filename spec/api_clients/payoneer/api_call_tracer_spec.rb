require "rails_helper"

describe ApiCallTracer do
  let(:person) { create(:person) }
  let(:url) { "http://sample.url.com"}
  let(:params) do
    {
        sample: "sample param",
        another_sample: "another sample param"
    }
  end

  describe "#call" do
    context "a good response" do
      it "logs in Rails logger" do
        response = double(:response, body: "sample", status: 200)
        expected_log = "API Client Call Response : #{response.body}"

        expect(Rails.logger).to receive(:info).with(expected_log)
        ApiCallTracer.call(response, person.id, params, url)
      end

      it "does not notify Airbrake" do
        response = double(:response, body: "sample", status: 200)

        expect(Airbrake).to_not receive(:notify)
        ApiCallTracer.call(response, person.id, params, url)
      end
    end

    context "a bad response" do
      it "logs in Rails logger" do
        response = double(:response, body: "sample", status: 403)
        expected_error = "API Client Call Failed: #{response.body} Person_Id: #{person.id} Request_Params: #{params} URL: #{url}"

        expect(Rails.logger).to receive(:info).with(expected_error)

        call_tracer = ApiCallTracer.new(response, person.id, params, url)
        allow(call_tracer).to receive(:trace_exception).and_return(expected_error)
        call_tracer.call
      end

      it "notifies Airbrake" do
        response = double(:response, body: "sample", status: 403)
        expected_error = "API Client Call Failed: #{response.body} Person_Id: #{person.id} Request_Params: #{params} URL: #{url}"

        expect(Airbrake).to receive(:notify).with(Exception.exception(expected_error), {})

        call_tracer = ApiCallTracer.new(response, person.id, params, url)
        allow(call_tracer).to receive(:trace_exception).and_return(expected_error)
        call_tracer.call
      end
    end
  end
end
