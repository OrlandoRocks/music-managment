require "rails_helper"

describe PaymentsOS::ApiClient do
  describe "#send_request" do
    let(:person) { create(:person) }
    let(:payos_customer) { create(:payments_os_customer, person: person) }
    it "returns a returns a valid Faraday Response object" do
      allow_any_instance_of(Faraday::Connection).to receive(:post).and_return(Faraday::Response.new(body: '{"token":"90158e81-c648-48ec-b747-915bc3ac5e18","token_type":"credit_card","state":"created","holder_name":"John Mark","expiration_date":"10/2029","last_4_digits":"1111","pass_luhn_validation":true,"vendor":"VISA","card_type":"CREDIT","country_code":"USA","billing_address":{"country":"USA","state":"NY","city":"NYC","line1":"fifth avenue 10th"}}'))
      response = PaymentsOS::ApiClient.new(person).send_request(:new_payment_method, {
        customer_id: payos_customer.customer_id,
        stored_cc_params: {}
      })
      expect(response).to be_instance_of(PaymentsOS::Responses::NewPaymentMethod)
    end
  end
end
