require "rails_helper"

RSpec.describe Currencylayer::Requests::Historical, type: :api_client do
  let(:date) { Date.new }
  let(:source_currency) { "USD" }
  let(:target_currencies) { %w[GBP INR] }
  let(:subject) do
    described_class.new(
      date: date,
      source_currency: source_currency,
      target_currencies: target_currencies
    )
  end

  describe "#url" do
    it "returns url" do
      expect(subject.url).to eq(Currencylayer::Requests::Historical::URL)
    end
  end

  describe "#response_parser" do
    it "returns historical response parser" do
      expect(subject.response_parser).to eq(Currencylayer::Responses::Historical)
    end
  end

  describe "#request_method" do
    it "returns get" do
      expect(subject.request_method).to eq(:get)
    end
  end

  describe "#params" do
    it "returns params" do
      expect(subject.params)
        .to eq(
          date: date,
          source: source_currency,
          currencies: target_currencies.join(",")
        )
    end
  end

  describe "#attach_request_params" do
    it "attach params to farady object" do
      faraday_request = double(
        params: {
          api_key: "API_KEY"
        }
      )

      expect(
        subject.attach_request_params(faraday_request)
      ).to eq(
        {
          api_key: "API_KEY",
          date: date,
          source: source_currency,
          currencies: target_currencies.join(",")
        }
      )
    end
  end
end
