require "rails_helper"

describe Currencylayer::ApiClient do
  describe "#send_request" do
    let(:stubs)  { Faraday::Adapter::Test::Stubs.new }
    let(:conn)   { Faraday.new { |b| b.adapter(:test, stubs) } }
    let(:api_client) { described_class.new }

    before(:each) do
      api_client.instance_variable_set(:@http_client, conn)
    end

    context "/historical" do
      it "parses historical api" do
        stubs.get("/historical") do |env|
          expect(env.params["source"]).to eq("EUR")
          expect(env.params["currencies"]).to eq("USD,GBP")
          [
            200,
            { "Content-Type": "application/json" },
            '{"success": true}'
          ]
        end

        subject = api_client
                  .send_request(
                    :historical,
                    source_currency: "EUR",
                    target_currencies: ["USD", "GBP"],
                    date: "20-02-2022"
                  )

        expect(subject).to be_instance_of(Currencylayer::Responses::Historical)
        expect(subject.success?).to eq(true)
        stubs.verify_stubbed_calls
      end
    end
  end

  describe "#get_forex" do
    before(:each) do
      @stubs = Faraday::Adapter::Test::Stubs.new
      test_conn = Faraday.new { |b| b.adapter(:test, @stubs) }
      @api_client = Currencylayer::ApiClient.new
      @api_client.instance_variable_set(:@http_client, test_conn)
    end

    it "should return the right response for given params" do
      @stubs.get("/live") do |env|
        expect(env.params["source"]).to eq("EUR")
        expect(env.params["currencies"]).to eq("USD,GBP")
        [
          200,
          { 'Content-Type': "application/json" },
          '{"success" : true}'
        ]
      end

      expect(@api_client.get_forex("EUR", "USD,GBP")).to eq({ "success" => true })
    end

    it "should call API call tracer so that failures will be handled appropriately" do
      @stubs.get("/live") do |_env|
        [
          500,
          { 'Content-Type': "application/json" },
          '{"success" : false}'
        ]
      end
      expect(ApiCallTracer).to receive(:call)
      @api_client.get_forex("EUR", "USD")
    end
  end
end
