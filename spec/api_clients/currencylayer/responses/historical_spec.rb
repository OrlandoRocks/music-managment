require "rails_helper"

RSpec.describe Currencylayer::Responses::Historical, type: :api_client do
  let(:successful_response) do
    double(
      "response", status: 200, body: JSON.dump(
        success: true,
        source: "USD",
        historical: true,
        date: "2005-02-01",
        timestamp: 1_430_401_802,
        quotes: {
          USDAED: 3.672982,
          USDAFN: 57.8936
        }
      )
    )
  end

  let(:failed_response) do
    double(
      "response", status: 200, body: JSON.dump(
        success: false,
        code: 105,
        info: "Error"
      )
    )
  end

  describe "#date" do
    it "returns requested date of exchange_rate with successful response" do
      subject = described_class.new(successful_response)

      expect(subject.date).to eq("2005-02-01".to_date)
    end

    it "returns nil with failed response" do
      subject = described_class.new(failed_response)

      expect(subject.date).to be_nil
    end
  end

  describe "#historical" do
    it "returns true with successful response" do
      subject = described_class.new(successful_response)

      expect(subject.historical?).to be(true)
    end

    it "returns false with failed response" do
      subject = described_class.new(failed_response)

      expect(subject.historical?).to be(false)
    end
  end
end
