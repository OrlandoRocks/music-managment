require "rails_helper"

RSpec.describe Currencylayer::Responses::Base, type: :api_client do
  let(:successful_response) do
    double(
      "response", status: 200, body: JSON.dump(
        success: true,
        source: "USD",
        timestamp: 1_430_401_802,
        quotes: {
          USDAED: 3.672982,
          USDAFN: 57.8936
        }
      )
    )
  end

  let(:failed_response) do
    double(
      "response", status: 200, body: JSON.dump(
        success: false,
        code: 105,
        info: "Error"
      )
    )
  end

  describe "#sueccess?" do
    it "returns true with successful response" do
      subject = described_class.new(successful_response)

      expect(subject.success?).to be(true)
    end

    it "returns false with failed response" do
      subject = described_class.new(failed_response)

      expect(subject.success?).to be(false)
    end
  end

  describe "#source" do
    it "returns souce currency with successful response" do
      subject = described_class.new(successful_response)

      expect(subject.source).to eq("USD")
    end

    it "returns nil with failed response" do
      subject = described_class.new(failed_response)

      expect(subject.source).to be_nil
    end
  end

  describe "#timestamp" do
    it "returns timestamp with successful response" do
      subject = described_class.new(successful_response)

      expect(subject.timestamp).to eq(1_430_401_802)
    end

    it "returns nil with failed response" do
      subject = described_class.new(failed_response)

      expect(subject.timestamp).to be_nil
    end
  end

  describe "#quotes" do
    it "return quotes with successful response" do
      subject = described_class.new(successful_response)
      expected_result = Oj.load(successful_response.body)["quotes"]

      expect(subject.quotes).to eq(expected_result)
    end

    it "returns nil with failed response" do
      subject = described_class.new(failed_response)

      expect(subject.quotes).to be_nil
    end
  end
end
