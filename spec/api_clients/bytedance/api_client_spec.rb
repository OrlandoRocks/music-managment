require "rails_helper"

describe Bytedance::ApiClient do
  let(:album) { create(:album, :with_distribution_api_album) }
  let(:admin) { create(:person, :admin, :with_role, role_name: "Content Review Manager")}
  let(:review_audit) { create(:review_audit, :rejected, person: admin, album: album) }

  before(:each) do
    @stubs = Faraday::Adapter::Test::Stubs.new
    @test_conn = Faraday.new { |b| b.adapter(:test, @stubs) }
    allow(ENV).to receive(:fetch).with("BD_ACCESS_KEY").and_return("15555551212")
    allow(ENV).to receive(:fetch).with("BD_SECRET_KEY").and_return("25555551212")
    @api_client = Bytedance::ApiClient.new("Bytedance::Requests::ContentReview", review_audit.id, album.id)
    @api_client.instance_variable_set(:@http_client, @test_conn)
  end

  describe "#post" do
    it "should invoke the request with the correct params" do
      params = {}
      allow_any_instance_of(Bytedance::Requests::ContentReview).to receive(:params) { params }

      create(:review_audit, :rejected, person: admin, album: album)
      url = "review"

      expect(@api_client.http_client)
        .to receive(:post).with(url, {}.to_json)
        .and_return(double(success?: true))

      @api_client.post
    end

    it "should raise an Airbrake if the response is not successful" do
      params = { tc_album_id: album.id, source_id: "1234" }
      allow_any_instance_of(Bytedance::Requests::ContentReview).to receive(:params) { params }

      url = "review"
      response_dbl = double(success?: false, body: {})

      allow(@api_client.http_client)
        .to receive(:post)
        .with(url, params.to_json)
        .and_return(response_dbl)

      msg = "Unable to send Bytedance API post request to #{url} endpoint"
      error_obj = {
        album_id: params[:tc_album_id],
        source_album_id: params[:source_id],
        error: JSON.parse(response_dbl.body)
      }

      expect(Airbrake).to receive(:notify).with(msg, error_obj)

      @api_client.post
    end
  end
end
