require "rails_helper"

describe Bytedance::Requests::ContentReview do
  let(:album) { create(:album, :with_distribution_api_album) }
  let(:admin) { create(:person, :admin, :with_role, role_name: "Content Review Manager") }
  let(:review_audit) { create(:review_audit, :rejected, person: admin, album: album) }

  before do
    album.distribution_api_album.distribution_api_songs << create(:distribution_api_song)
  end

  subject { described_class.new(review_audit.id, album.id) }

  it "should return the url" do
    expect(subject.url).to eq "review"
  end

  it "should return params" do
    note = "This is a note"
    distribution_api_album = album.distribution_api_album
    review_audit.update(note: note)

    album_errors =
      review_audit.review_reasons.map do |review_reason|
        {
          error: review_reason.id,
          error_message: review_reason.reason
        }
      end

    distribution_api_songs =
      distribution_api_album.distribution_api_songs.map do |distribution_api_song|
        {
          tc_song_id: distribution_api_song.song_id,
          source_song_id: distribution_api_song.source_song_id,
          isrc: distribution_api_song.song.isrc
        }
      end

    params = {
      tc_album_id: album.id,
      source_id: distribution_api_album.source_album_id,
      upc: album.upc.number,
      state: "rejected",
      time: review_audit.updated_at,
      content_review_notes: note,
      errors: album_errors,
      songs: distribution_api_songs
    }

    expect(subject.params).to eq params
  end

  context "when the review audit event is 'STARTED REVIEW'" do
    it "should return params with a state of 'inprocess'" do
      review_audit.update(event: "STARTED REVIEW")

      expect(subject.params[:state]).to eq 'inprocess'
    end
  end

  it "should raise an error if an album is not related to a DistributionApiAlbum" do
    album = create(:album)
    review_audit = create(:review_audit, :rejected, person: admin, album: album)


    expect {
      described_class.new(review_audit.id, album.id)
    }.to raise_error(
      Bytedance::Requests::DistributionApiAlbumNotFound,
      "DistributionApiAlbum record related to Album was not found"
    )
  end
end
