require "rails_helper"

module Scrapi
  describe ApiClient do
    let(:client) { ApiClient.new('test', 'test') }

    before { $redis.set(Scrapi::ApiClient::AUTH_TOKEN, nil) }

    describe "#get_access_token" do
      it "returns access token with valid credentials" do
        resp_body = <<-EOF
          {"access_token":"f4beedbaf75413c1ce02a75289287d6019258a1d","expires_in":3600,"token_type":"Bearer","scope":"api-system-contentrecognition"}
        EOF
        stub_request(:post, ApiClient::HTTP_SCRAPI_AUTH_URL + "auth/getAccessToken")
          .with(body: {grant_type: "client_credentials"})
          .to_return(body: resp_body)
        expect($redis).to receive(:set).and_return(true)
        access_token = client.get_access_token
        expect(client.access_token).to eq("f4beedbaf75413c1ce02a75289287d6019258a1d")
      end

      it "raises exception with invalid credentials" do
        resp_body = <<-EOF
          {"error":"Invalid token"}
        EOF
        stub_request(:post, ApiClient::HTTP_SCRAPI_AUTH_URL + "auth/getAccessToken")
          .with(body: {grant_type: "client_credentials"})
          .to_return(body: resp_body, status: 401)
        expect { client.get_access_token }.to raise_error(ApiError)
      end

      it "should retry and pass if Net Timeout error" do
        expect(client).to receive(:retry_get_access_token).once
        stub_request(:post, ApiClient::HTTP_SCRAPI_AUTH_URL + "auth/getAccessToken")
          .with(body: {grant_type: "client_credentials"})
          .to_raise(Net::OpenTimeout)
        access_token = client.get_access_token
      end

      it "should raise ApiError if retries is 0" do
        stub_request(:post, ApiClient::HTTP_SCRAPI_AUTH_URL + "auth/getAccessToken")
          .with(body: {grant_type: "client_credentials"})
          .and_raise(Net::OpenTimeout)
        expect { client.get_access_token(0) }.to raise_error(ApiError)
      end

      it "should fetch access token from redis when it exists" do
        $redis.set('Believe::ScrapiAuthToken', "f4beedbaf75413c1ce02a75289287d6019258a1d")
        client.get_access_token
        expect(client.access_token).to eq("f4beedbaf75413c1ce02a75289287d6019258a1d")
      end
    end

    describe "#create_job" do
      before do
        song_file = FactoryBot.build(:s3_asset, bucket: "bucket", key: "key")
        @song = double(:song, id: 42)
        allow(@song).to receive(:song_file).and_return(song_file)
      end

      it "returns a job when success" do
        job = double(:job, id: "createjobid")
        allow(job).to receive(:song_id=).with(@song.id)
        allow(job).to receive(:save)

        resp_body = <<-EOF
          {"status":{"code":200,"message":"OK","description":"","hostname":"blv-th3-apibk-6013.blvs.io","details":[]},"data":{"id":"createjobid", "uploadType":"aws_tunecore","uploadParams":"bigbox.tunecore.com:2508274/1561164431-song.mp3","status":"waiting_file"}}
        EOF

        stub_request(:post, ApiClient::HTTP_SCRAPI_BASE_URL + "recognitionjob")
          .with(body: {
              uploadType: "aws_tunecore",
              uploadParams: "bucket:key",
              callback: "https://api.tunecore.com/api/external/scrapi/callback?api_key=#{API_CONFIG['API_KEY']}",
            },
            headers: {"Authorization": "Bearer token"}
          ).to_return(body: resp_body, status: 200)
        client.instance_variable_set(:@access_token, "token")
        expect(Job).to receive(:from_content).with(resp_body).and_return(job)
        resp = client.create_job(@song)

        expect(resp).to eq(job)
      end

      it "raises exception with incorrect request" do
        resp_body = <<-EOF
          {"error":"Invalid request"}
        EOF
        stub_request(:post, ApiClient::HTTP_SCRAPI_BASE_URL + "recognitionjob")
          .with(body: {
              uploadType: "aws_tunecore",
              uploadParams: "bucket:key",
              callback: "https://api.tunecore.com/api/external/scrapi/callback?api_key=#{API_CONFIG['API_KEY']}",
            },
            headers: {"Authorization": "Bearer token"}
          ).to_return(body: resp_body, status: 422)
        client.instance_variable_set(:@access_token, "token")
        expect { client.create_job(@song) }.to raise_error(RuntimeError)
      end

      it "raises exception with rescue" do
        resp_body = <<-EOF
          {"error":"Invalid request"}
        EOF

        stub_request(:post, ApiClient::HTTP_SCRAPI_BASE_URL + "recognitionjob")
          .with(body: {
              uploadType: "aws_tunecore",
              uploadParams: "bucket:key",
              callback: "https://api.tunecore.com/api/external/scrapi/callback?api_key=#{API_CONFIG['API_KEY']}",
            },
            headers: {"Authorization": "Bearer token"}
          ).to_return(body: resp_body, status: 200)
        client.instance_variable_set(:@access_token, "token")
        allow(Job).to receive(:from_content).and_raise('Error')
        expect { client.create_job(@song) }.to raise_error(RuntimeError)
      end
    end

    describe "#get_job" do
      let(:job) {
        resp_body = <<-EOF
          {"status":{"code":200,"message":"OK","description":"","hostname":"blv-th3-apibk-6013.blvs.io","details":[]},"data":{"id":"createjobid", "uploadType":"aws_tunecore","uploadParams":"bigbox.tunecore.com:2508274/1561164431-song.mp3","status":"waiting_file"}}
        EOF
        Job.from_content(resp_body)
      }

      context 'job done' do
        let(:resp_body) { <<-EOF
          {"status":{"code":200,"message":"OK","description":"","hostname":"blv-th3-apibk-6013.blvs.io","details":[]},"data":{"id":"createjobid", "uploadType":"aws_tunecore","uploadParams":"bigbox.tunecore.com:2508274/1561164431-song.mp3","status":"done"}}
        EOF
        }

        before do
          response = double
          allow(response).to receive(:success?).and_return(true)
          allow(response).to receive(:body).and_return(resp_body)
          allow(Job).to receive(:from_id).and_return(job)
          allow(client.scrapi_client).to receive(:get).and_return(response)
        end

        it 'should update content' do
          expect {
            client.get_job('createjobid')
          }.to change {
            job.status
          }.from('waiting_file').to('done')
        end
      end

      context 'job error' do
        let(:resp_body) { <<-EOF
          {"status":{"code":200,"message":"OK","description":"","hostname":"blv-th3-apibk-6013.blvs.io","details":[]},"data":{"id":"createjobid", "uploadType":"aws_tunecore","uploadParams":"bigbox.tunecore.com:2508274/1561164431-song.mp3","status":"error"}}
                          EOF
        }

        before do
          response = double
          allow(response).to receive(:success?).and_return(true)
          allow(response).to receive(:body).and_return(resp_body)
          allow(Job).to receive(:from_id).and_return(job)
          allow(client.scrapi_client).to receive(:get).and_return(response)
          ScrapiJob.create(job_id: 'createjobid')
        end

        it 'should update content' do
          expect {
            client.get_job('createjobid')
          }.to change {
            job.status
          }.from('waiting_file').to('error')
        end
      end
    end
  end
end
