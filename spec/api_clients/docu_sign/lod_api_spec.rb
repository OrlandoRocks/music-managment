require "rails_helper"

describe DocuSign::LodApi do

  let(:api_client) {double('api_client').as_null_object}
  let(:envelopes_api) {double('envelope_api').as_null_object}
  let(:person) { create(:person) }

  before(:each) do
    allow(DocuSign_eSign::ApiClient).to receive(:new).with(anything) {api_client}
    allow(DocuSign_eSign::EnvelopesApi).to receive(:new) {envelopes_api}
  end

  context "when passed in a composer" do
    let(:composer) { create(:composer) }

    it "should send request to docusign with correct LOD template id" do
      create(:publisher, name: "pub1", composers: [composer])
      create(:document_template, document_title: DocumentTemplate::LOD, language_code: "en")
      expect(envelopes_api).to receive(:create_envelope) do |_account_id, definition|
        expect(definition.status).to eq("sent")
        expect(definition.email_subject).to be_truthy
        expect(definition.template_id).to be_truthy

        role = definition.template_roles[0]
        expect(role.role_name).to eq('Signer')
        expect(role.email).to eq(person.email)
        expect(role.name).to eq(composer.full_name_affixed)
        expect(role.tabs).to be_truthy
        double(:envelope, envelope_id: 5)
      end

      DocuSign::LodApi.send_lod_mail(person, composer)
    end

    it "should make the mail resend request to docusign" do
      create(:legal_document, name: DocumentTemplate::LOD, provider_identifier: "iden1", person: composer.person,
             subject_id: composer.id, subject_type: "Composer")
      expect(envelopes_api).to receive(:update) do |_account_id, envelope_id, envelope, options|
        expect(envelope_id).to eq("iden1")
        expect(envelope.status).to eq("sent")
        expect(options.resend_envelope).to eq(true)
      end

      DocuSign::LodApi.resend_lod_mail(person, composer)
    end

    describe "#resend_lod_mail" do
      let(:lod) {
        create(:legal_document, {
          name: DocumentTemplate::LOD,
          provider_identifier: "iden1",
          person: composer.person,
          subject_id: composer.id,
          subject_type: "Composer"
        })
      }

      context "legal document is signed" do
        it "should not call envelope api resend" do
          lod.update(signed_at: Time.current)

          expect(envelopes_api).to_not receive(:update)

          DocuSign::LodApi.resend_lod_mail(person, composer)
        end
      end

      context "legal document is voided" do
        it "should call send_mail instead of resend" do
          lod.voided!
          expect_any_instance_of(DocuSign::LodApi).to receive(:send_mail)

          DocuSign::LodApi.resend_lod_mail(person, composer)
        end
      end
    end

    describe "build_tags" do
      context "when a publisher has a performing_rights_organization_id" do
        it "should create the right tags" do
          create(:publisher, name: "pub1", composers: [composer], performing_rights_organization_id: 56)
          lod_api = DocuSign::LodApi.new(person, composer)
          tabs = lod_api.send(:build_tabs)
          expect(tabs).to eq({
                              textTabs: [
                                {
                                    tabLabel: "\\*company_name",
                                    value: "pub1 (BMI)"
                                },
                                {
                                    tabLabel: "\\*publisher_pro",
                                    value: "TuneCore Digital Music (BMI)"
                                }
                              ]
                            })
        end
      end

      context "when a publisher does not have a performing_rights_organization_id" do
        it "should build tags with nil values" do
          publisher = create(:publisher, name: "pub1", composers: [composer], performing_rights_organization_id: 56)

          publisher.update_columns(performing_rights_organization_id: nil)
          lod_api = DocuSign::LodApi.new(person, composer)
          tabs = lod_api.send(:build_tabs)
          expect(tabs).to eq({
                              textTabs: [
                                {
                                    tabLabel: "\\*company_name",
                                    value: "pub1 ()"
                                },
                                {
                                    tabLabel: "\\*publisher_pro",
                                    value: ""
                                }
                              ]
                            })
        end
      end
    end
  end

  context "when passed in a publishing_composer" do
    let(:publishing_composer) { create(:publishing_composer) }

    it "should send request to docusign with correct LOD template id" do
      create(:publisher, name: "pub1", publishing_composers: [publishing_composer])
      create(:document_template, document_title: DocumentTemplate::LOD, language_code: "en")

      expect(envelopes_api).to receive(:create_envelope) do |_account_id, definition|
        expect(definition.status).to eq("sent")
        expect(definition.email_subject).to be_truthy
        expect(definition.template_id).to be_truthy

        role = definition.template_roles[0]
        expect(role.role_name).to eq('Signer')
        expect(role.email).to eq(person.email)
        expect(role.name).to eq(publishing_composer.full_name_affixed)
        expect(role.tabs).to be_truthy
        double(:envelope, envelope_id: 5)
      end

      DocuSign::LodApi.send_lod_mail(person, publishing_composer)
    end

    it "should make the mail resend request to docusign" do
      create(:legal_document,
        name: DocumentTemplate::LOD,
        provider_identifier: "iden1",
        person: publishing_composer.person,
        subject_id: publishing_composer.id,
        subject_type: "PublishingComposer"
      )

      expect(envelopes_api).to receive(:update) do |_account_id, envelope_id, envelope, options|
        expect(envelope_id).to eq("iden1")
        expect(envelope.status).to eq("sent")
        expect(options.resend_envelope).to eq(true)
      end

      DocuSign::LodApi.resend_lod_mail(person, publishing_composer)
    end

    describe "#resend_lod_mail" do
      let(:lod) {
        create(:legal_document, {
          name: DocumentTemplate::LOD,
          provider_identifier: "iden1",
          person: publishing_composer.person,
          subject_id: publishing_composer.id,
          subject_type: "PublishingComposer"
        })
      }

      context "legal document is signed" do
        it "should not call envelope api resend" do
          lod.update(signed_at: Time.current)

          expect(envelopes_api).to_not receive(:update)

          DocuSign::LodApi.resend_lod_mail(person, publishing_composer)
        end
      end

      context "legal document is voided" do
        it "should call send_mail instead of resend" do
          lod.voided!
          expect_any_instance_of(DocuSign::LodApi).to receive(:send_mail)

          DocuSign::LodApi.resend_lod_mail(person, publishing_composer)
        end
      end
    end

    describe "build_tags" do
      it "should create the right tags" do
        create(:publisher, name: "pub1", publishing_composers: [publishing_composer], performing_rights_organization_id: 56)

        lod_api = DocuSign::LodApi.new(person, publishing_composer)
        tabs = lod_api.send(:build_tabs)

        expect(tabs).to eq(
          {
            textTabs: [
              {
                  tabLabel: "\\*company_name",
                  value: "pub1 (BMI)"
              },
              {
                  tabLabel: "\\*publisher_pro",
                  value: "TuneCore Digital Music (BMI)"
              }
            ]
          }
        )
      end
    end

    describe "#update_legal_document" do
      let(:composer) { create(:composer) }
      context "Legal document with the provided envelope id does not exist" do
        let!(:lod) {
          create(:legal_document, {
            name: DocumentTemplate::LOD,
            provider_identifier: "test",
            person: composer.person,
            subject_id: composer.id,
            subject_type: "Composer"
          })
        }

        it "creates a new legal document" do
          expect { DocuSign::LodApi.update_legal_document(person, composer, "test1") }.to change { LegalDocument.count }.by(1)
        end

        it "resends the email if the legal document is unsigned" do
          expect(envelopes_api).to receive(:get_envelope) do |_account_id|
            double(:envelope, status: "sent", status_changed_date_time: Time.current)
          end

          expect(envelopes_api).to receive(:update)
          expect { DocuSign::LodApi.update_legal_document(person, composer, "test1") }.to change { LegalDocument.count }.by(1)
        end

        it "does not resend the email if the legal document is signed" do
          expect(envelopes_api).to receive(:get_envelope) do |_account_id|
            double(:envelope, status: "completed", status_changed_date_time: Time.current)
          end

          expect(envelopes_api).to_not receive(:update)
          expect { DocuSign::LodApi.update_legal_document(person, composer, "test1") }.to change { LegalDocument.count }.by(1)
        end
      end

      context "Legal document exists with the provided envelope id" do
        let!(:lod) {
          create(:legal_document, {
            name: DocumentTemplate::LOD,
            provider_identifier: "test2",
            person: composer.person,
            subject_id: composer.id,
            subject_type: "Composer"
          })
        }

        it "does not create a new legal document" do
          expect(envelopes_api).to receive(:get_envelope) do |_account_id|
            double(:envelope, status: "sent", status_changed_date_time: Time.current)
          end

          expect { DocuSign::LodApi.update_legal_document(person, composer, "test2") }.to change { LegalDocument.count }.by(0)
        end

        it "does not update the legal document status if it is unsigned" do
          expect(envelopes_api).to receive(:get_envelope) do |_account_id|
            double(:envelope, status: "sent", status_changed_date_time: Time.current)
          end

          DocuSign::LodApi.update_legal_document(person, composer, "test2")
          expect(lod.reload.status).to eq "sent"
        end

        it "updates the legal document status to completed if it is signed" do
          expect(envelopes_api).to receive(:get_envelope) do |_account_id|
            double(:envelope, status: "completed", status_changed_date_time: Time.current)
          end

          DocuSign::LodApi.update_legal_document(person, composer, "test2")
          expect(lod.reload.status).to eq "completed"
        end

        it "resends the email if the legal document is unsigned" do
          expect(envelopes_api).to receive(:get_envelope) do |_account_id|
            double(:envelope, status: "sent", status_changed_date_time: Time.current)
          end

          expect_any_instance_of(DocuSign::LodApi).to receive(:resend_mail)
          DocuSign::LodApi.update_legal_document(person, composer, "test2")
        end

        it "does not resend the email if the legal document is signed" do
          expect(envelopes_api).to receive(:get_envelope) do |_account_id|
            double(:envelope, status: "completed", status_changed_date_time: Time.current)
          end

          expect_any_instance_of(DocuSign::LodApi).to_not receive(:resend_mail)
          DocuSign::LodApi.update_legal_document(person, composer, "test2")
        end
      end
    end
  end
end
