require "rails_helper"

describe DocuSign::TemplatesApi do
  let(:configuration) { double(:configuration) }
  let(:api_client)    { double(:api_client) }
  let(:templates_api) { double(:templates_api) }

  before(:each) do
    stub_const("DocuSign::ApiClient::ACCOUNT_ID", SecureRandom.uuid)
    allow(DocuSign_eSign::Configuration).to receive(:new)                 { configuration }
    allow(configuration).to receive(:host=)                               { "www.test.com" }
    allow(DocuSign_eSign::ApiClient).to receive(:new).with(configuration) { api_client }
    allow(api_client).to receive(:configure_jwt_authorization_flow)
    allow(DocuSign_eSign::TemplatesApi).to receive(:new)                  { templates_api }
  end

  describe "#initialize" do
    it "sets the Templates Api Client" do
      expect(DocuSign_eSign::TemplatesApi).to receive(:new).with(api_client)
      DocuSign::TemplatesApi.new
    end
  end

  describe ".list_templates" do
    it "creates and envelope and returns the docusign url" do
      expect(templates_api).to receive(:list_templates).with(DocuSign::ApiClient::ACCOUNT_ID)
      DocuSign::TemplatesApi.list_templates
    end
  end

  describe ".get_template" do
    let(:template_id) { SecureRandom.uuid }

    it "creates and envelope and returns the docusign url" do
      expect(templates_api).to receive(:get).with(DocuSign::ApiClient::ACCOUNT_ID, template_id)
      DocuSign::TemplatesApi.get_template(template_id)
    end
  end
end
