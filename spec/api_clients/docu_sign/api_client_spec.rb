require "rails_helper"

describe DocuSign::ApiClient do
  it "sets the Docusign Api Client" do
    configuration = double(:configuration)
    api_client    = double(:api_client)
    allow(configuration).to receive(:host=) { "www.test.com" }

    expect(DocuSign_eSign::Configuration).to receive(:new)                 { configuration }
    expect(DocuSign_eSign::ApiClient).to receive(:new).with(configuration) { api_client }
    expect(api_client).to receive(:configure_jwt_authorization_flow)
    DocuSign::ApiClient.new.api_client
  end
end
