require "rails_helper"

describe Adyen::Responses::NewSession do
  let(:response_object) do
    {
      sessionData: "Ab02b4c0!BQABAgCS1GEAfwnynQRaAn/hz6CKKd+j3X4iNvfr9z2v2JgodCC8lGAdRwZT3IgX8csQIvlL7",
      id: "CSF17219D84AF2BA1D",
      amount: { currency: "PHP", value: 1000 }
    }
  end

  describe "#id" do
    it "should return the session id from the response" do
      response = double("response", body: JSON.dump(response_object))
      new_session = Adyen::Responses::NewSession.new(response)
      expect(new_session.id).to eq("CSF17219D84AF2BA1D")
    end
  end

  describe "#session_data" do
    it "should return the sessionData from the response" do
      response = double("response", body: JSON.dump(response_object))
      new_session = Adyen::Responses::NewSession.new(response)
      expect(new_session.session_data).to eq("Ab02b4c0!BQABAgCS1GEAfwnynQRaAn/hz6CKKd+j3X4iNvfr9z2v2JgodCC8lGAdRwZT3IgX8csQIvlL7")
    end
  end

  describe "#amount_in_local_currency" do
    it "should return the amount value from the response" do
      response = double("response", body: JSON.dump(response_object))
      new_session = Adyen::Responses::NewSession.new(response)
      expect(new_session.amount_in_local_currency).to eq(1000)
    end
  end
end
