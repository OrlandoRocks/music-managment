require "rails_helper"

describe Adyen::Responses::PaymentDetail do
  let(:response_object) do
    {
      "pspReference": "NC6HT9CRT65ZGN82",
      "resultCode": "Authorised",
      "merchantReference": "430"
    }
  end

  describe "#psp_reference" do
    it "should return the psp_reference from the response" do
      response = double("response", body: JSON.dump(response_object))
      payment_detail = Adyen::Responses::PaymentDetail.new(response)
      expect(payment_detail.psp_reference).to eq("NC6HT9CRT65ZGN82")
    end

    context "the response does not have a psp_reference" do
      it "should return an empty string" do
        response = double("response", body: JSON.dump({}))
        payment_detail = Adyen::Responses::PaymentDetail.new(response)
        expect(payment_detail.psp_reference).to be_nil
      end
    end
  end
  
  describe "#result_code" do
    it "should return the result_code from the response" do
      response = double("response", body: JSON.dump(response_object))
      payment_detail = Adyen::Responses::PaymentDetail.new(response)
      expect(payment_detail.result_code).to eq("Authorised")
    end

    context "the response does not have a result_code" do
      it "should return an empty string" do
        response = double("response", body: JSON.dump({}))
        payment_detail = Adyen::Responses::PaymentDetail.new(response)
        expect(payment_detail.result_code).to be_nil
      end
    end
  end

  describe "#merchant_reference" do
    it "should return the merchant_reference from the response" do
      response = double("response", body: JSON.dump(response_object))
      payment_detail = Adyen::Responses::PaymentDetail.new(response)
      expect(payment_detail.merchant_reference).to eq("430")
    end

    context "the response does not have a merchant_reference" do
      it "should return an empty string" do
        response = double("response", body: JSON.dump({}))
        payment_detail = Adyen::Responses::PaymentDetail.new(response)
        expect(payment_detail.merchant_reference).to be_nil
      end
    end
  end

  describe "#payment_succeeded" do
    it "should return the true if result_code from the response is AUTHORIZED" do
      response = double("response", body: JSON.dump(response_object))
      payment_detail = Adyen::Responses::PaymentDetail.new(response)
      expect(payment_detail.payment_succeeded?).to eq(true)
    end

    context "the response does not have a result_code" do
      it "should return false" do
        response = double("response", body: JSON.dump({}))
        payment_detail = Adyen::Responses::PaymentDetail.new(response)
        expect(payment_detail.payment_succeeded?).to eq(false)
      end
    end
  end
end
