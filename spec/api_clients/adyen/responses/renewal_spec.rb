require "rails_helper"

describe Adyen::Responses::Renewal do
  let(:response_object) do
    {
      "additionalData" =>
            {
              "recurring.shopperReference" => "PERSON-35",
              "recurring.contractTypes" => "ONECLICK,RECURRING",
              "recurring.recurringDetailReference" => "ZP482RGBF6KXWD82",
              "recurring.firstPspReference" => "NQ3H5BQQ63RZNN82"
            },
      "pspReference" => "W8Z8X2JTQGXXGN82",
      "resultCode" => "Authorised",
      "amount" => { "currency" => "PHP", "value" => 728_730 },
      "merchantReference" => "212"
    }
  end

  describe "#psp_reference" do
    it "should return the pspReference from the response" do
      response = double("response", body: JSON.dump(response_object))
      renewal = Adyen::Responses::Renewal.new(response)
      expect(renewal.psp_reference).to eq("W8Z8X2JTQGXXGN82")
    end
  end

  describe "#result_code" do
    it "should return the resultCode from the response" do
      response = double("response", body: JSON.dump(response_object))
      renewal = Adyen::Responses::Renewal.new(response)
      expect(renewal.result_code).to eq("Authorised")
    end
  end

  describe "#merchant_reference" do
    it "should return the merchantReference from the response" do
      response = double("response", body: JSON.dump(response_object))
      renewal = Adyen::Responses::Renewal.new(response)
      expect(renewal.merchant_reference).to eq("212")
    end
  end

  describe "#payment_succeeded?" do
    it "should return true if resultCode is Authorised" do
      response = double("response", body: JSON.dump(response_object))
      renewal = Adyen::Responses::Renewal.new(response)
      expect(renewal.payment_succeeded?).to eq(true)
    end
  end
end
