require "rails_helper"
RSpec.describe Adyen::Responses::ListRecurringDetails, type: :api_client do
  let(:response_object) do
    {
      "details" => [
        {
          "firstPspReference" => "VMK8CVHBMMK2WN82",
          "recurringDetailReference" => "KKFRR3M4F6KXWD82",
        }
      ]
    }
  end
  let(:response) { double(body: JSON.dump(response_object)) }
  let(:subject) { described_class.new(response) }

  describe "#details" do
    it "returns deatils of recurring reference" do
      expect(subject.details).to eq(
        [
          {
              "firstPspReference" => "VMK8CVHBMMK2WN82",
              "recurringDetailReference" => "KKFRR3M4F6KXWD82",
          }
        ]
      )
    end
  end

  describe "#detail_for_psp_reference" do
    it "returns deatils of given psp_reference" do
      expect(subject.detail_for_psp_reference("VMK8CVHBMMK2WN82")).to eq(
        {
          "firstPspReference" => "VMK8CVHBMMK2WN82",
          "recurringDetailReference" => "KKFRR3M4F6KXWD82",
        }
      )
    end
  end

  describe "recurring_detail_reference_for_psp_reference" do
    it "returns recurring detail of given psp_reference" do
      expect(
        subject
        .recurring_detail_reference_for_psp_reference("VMK8CVHBMMK2WN82")
      ).to eq("KKFRR3M4F6KXWD82")
    end
  end
end
