require "rails_helper"

RSpec.describe Adyen::RecurringApiClient, type: :api_client do
  describe "#send_request" do
    let(:stubs)  { Faraday::Adapter::Test::Stubs.new }
    let(:conn)   { Faraday.new { |b| b.adapter(:test, stubs) } }
    let(:adyen_merchant_config) { create(:adyen_merchant_config) }
    let(:api_client) { described_class.new(adyen_merchant_config.corporate_entity_id) }

    before(:each) do
      allow(ENV)
        .to receive(:fetch)
        .with("ADYEN_RECURRING_API_BASE_URL")
        .and_return("http://url")
      api_client.instance_variable_set(:@http_client, conn)
    end

    context "/list_recurring_details" do
      it "returns recurring details for person" do
        stubs.post("/listRecurringDetails") do |_env|
          [
            200,
            { "Content-Type": "application/json" },
            '{"success": true}'
          ]
        end

        subject = api_client
          .send_request(
            :list_recurring_details,
            shopper_reference: "PERSON-12345"
          )

        expect(subject).to be_instance_of(Adyen::Responses::ListRecurringDetails)
        stubs.verify_stubbed_calls
      end
    end
  end
end
