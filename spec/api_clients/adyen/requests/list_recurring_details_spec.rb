require "rails_helper"

RSpec.describe Adyen::Requests::ListRecurringDetails, type: :api_client do
  let(:subject) { described_class.new(shopper_reference: "PERSON-12345") }

  describe "#path" do
    it "returns listRecurringDeatils" do
      expect(subject.path).to eq("listRecurringDetails")
    end
  end

  describe "#response_parser" do
    it "returns Adyen::Responses::ListRecurringDetials" do
      expect(subject.response_parser).to eq(Adyen::Responses::ListRecurringDetails)
    end
  end
end
