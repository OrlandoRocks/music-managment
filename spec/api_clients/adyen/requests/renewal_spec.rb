require "rails_helper"

describe Adyen::Requests::Renewal, type: :api_client do
  let(:adyen_stored_payment_method) { create(:adyen_stored_payment_method) }
  let!(:person) { create(:person, email: "sample@example.com", country: "PH") }
  let!(:person_preference) {
    create(
      :person_preference, person: person, preferred_adyen_payment_method: adyen_stored_payment_method,
                          preferred_payment_type: "Adyen"
    )
  }
  let(:adyen_payment_method_info) { create(:adyen_payment_method_info, payment_method_name: "gcash") }
  let(:invoice) { create(:invoice, :with_two_purchases, :settled, person: person) }
  let(:stubs)  { Faraday::Adapter::Test::Stubs.new }
  let(:conn)   { Faraday.new { |b| b.adapter(:test, stubs) } }
  let(:subject) do
    described_class.new(
      invoice: invoice,
      merchant_account: "TestMerchantAccount"
    )
  end

  describe "#url" do
    it "returns url" do
      expect(subject.url).to eq("payments")
    end
  end

  describe "#response_parser" do
    it "returns adyen payment detail response parser" do
      expect(subject.response_parser).to eq(Adyen::Responses::Renewal)
    end
  end

  describe "#request_method" do
    it "returns post" do
      expect(subject.request_method).to eq(:post)
    end
  end

  describe "#params" do
    it "returns params" do
      expect(JSON.parse subject.params).to include(
        {
          "reference" => invoice.id,
          "amount" => {
            "value" => 2997,
            "currency" => "USD"
          },
          "paymentMethod" => { "storedPaymentMethodId" => "Testqwerty1233" },
          "shopperReference" => "PERSON-#{person.id}",
          "merchantAccount" => "TestMerchantAccount",
          "returnUrl" => "https://test.tunecore.com/cart/adyen_finalize_after_redirect?invoice_id=#{invoice.id}",
          "recurringProcessingModel" => "CardOnFile",
          "shopperInteraction" => "ContAuth",
        }
      )
    end
  end
end
