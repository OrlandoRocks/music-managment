require "rails_helper"

describe Adyen::Requests::PaymentDetail, type: :api_client do
  let(:redirect_result) { "X6XtfGC3!Y..." }
  let(:stubs)  { Faraday::Adapter::Test::Stubs.new }
  let(:conn)   { Faraday.new { |b| b.adapter(:test, stubs) } }
  let(:subject) do
    described_class.new(
      redirect_result: redirect_result
    )
  end

  describe "#url" do
    it "returns url" do
      expect(subject.url).to eq(Adyen::Requests::PaymentDetail::URL)
    end
  end

  describe "#response_parser" do
    it "returns adyen payment detail response parser" do
      expect(subject.response_parser).to eq(Adyen::Responses::PaymentDetail)
    end
  end

  describe "#request_method" do
    it "returns post" do
      expect(subject.request_method).to eq(:post)
    end
  end

  describe "#params" do
    it "returns params" do
      expect(subject.params)
        .to eq(
          {
            details: {
              redirectResult: redirect_result
            }
          }.to_json
        )
    end
  end
end
