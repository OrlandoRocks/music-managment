require "rails_helper"

describe Adyen::Requests::NewSession, type: :api_client do
  let(:person) { create(:person, email: "sample@example.com", country: "PH") }
  let(:invoice) { create(:invoice, :unsettled, person: person ) }
  let(:stubs)  { Faraday::Adapter::Test::Stubs.new }
  let(:conn)   { Faraday.new { |b| b.adapter(:test, stubs) } }
  let(:subject) do
    described_class.new(
      invoice: invoice,
      payment_method: "cup",
      amount_formatter: double("AmountFormatter", amount: invoice.outstanding_amount_cents, currency: person.currency),
      merchant_account: "TestMerchantAccount"
    )
  end

  describe "#url" do
    it "returns url" do
      expect(subject.url).to eq("sessions")
    end
  end

  describe "#response_parser" do
    it "returns adyen payment detail response parser" do
      expect(subject.response_parser).to eq(Adyen::Responses::NewSession)
    end
  end

  describe "#request_method" do
    it "returns post" do
      expect(subject.request_method).to eq(:post)
    end
  end

  describe "#params" do
    it "returns params" do
      expect(JSON.parse subject.params).to include(
        {
          "reference" => invoice.id,
          "amount" => {
            "value" => invoice.outstanding_amount_cents,
            "currency" => person.currency
          },
          "shopperEmail" => "sample@example.com",
          "shopperReference" => person.adyen_customer_reference,
          "countryCode" => "PH",
          "merchantAccount" => "TestMerchantAccount",
          "returnUrl" => "https://test.tunecore.com/cart/adyen_finalize_after_redirect?invoice_id=#{invoice.id}",
          "allowedPaymentMethods" => ["cup"],
          "recurringProcessingModel" => "CardOnFile",
          "shopperInteraction" => "Ecommerce",
          "storePaymentMethod" => true,
        }
      )
    end
  end
end
