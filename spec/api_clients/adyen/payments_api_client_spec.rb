require "rails_helper"

describe Adyen::PaymentsApiClient do
  describe "#send_request" do
    let(:stubs)  { Faraday::Adapter::Test::Stubs.new }
    let(:person) { create(:person, country: "PH") }
    let(:invoice) { create(:invoice, person: person) }
    let(:conn) { Faraday.new { |b| b.adapter(:test, stubs) } }
    let(:adyen_merchant_config) { create(:adyen_merchant_config) }
    let(:payments_api_client) { described_class.new(adyen_merchant_config.corporate_entity_id) }

    before(:each) do
      payments_api_client.instance_variable_set(:@http_client, conn)
    end

    context "/paymentMethods" do
      it "fetch adyen payment methods for each country" do
        stubs.post("/paymentMethods") do
          [
            200,
            { "Content-Type": "application/json" },
            '{"paymentMethods":
    [{"brands":["cup"], "name":"Credit Card", "type":"scheme"}, {"name":"GCash", "type":"gcash"}, {"name":"Maya Wallet", "type":"paymaya_wallet"}]}'
          ]
        end
        subject = payments_api_client
                  .send_request(
                    :payment_method,
                    country_code: "PH",
                  )
        expect(subject).to be_instance_of(Adyen::Responses::PaymentMethod)
        expect(subject.payment_methods).to eq(
          [
            {
              "brands" => ["cup"],
              "name" => "Credit Card",
              "type" => "scheme"
            },
            { "name" => "GCash", "type" => "gcash" },
            { "name" => "Maya Wallet", "type" => "paymaya_wallet" }
          ]
        )
      end
    end
    context "/sessions" do
      it "Initiate new adyen drop-in session" do
        stubs.post("/sessions") do
          [
            200,
            { "Content-Type": "application/json" },
            '{"allowedPaymentMethods":["cup"],
   "amount":{"currency":"PHP", "value":1000},
   "countryCode":"IN",
   "expiresAt":"2022-06-17T16:59:23+02:00",
   "id":"CSF17219D84AF2BA1D",
   "merchantAccount":"TuneCoreECOM",
   "recurringProcessingModel":"CardOnFile",
   "reference":"1",
   "returnUrl":"http://development.tunecore.local.in/cart/adyen_finalize_after_redirect?invoice_id=1",
   "shopperEmail":"vito@sipes.com",
   "shopperIP":"localhost",
   "shopperInteraction":"Ecommerce",
   "shopperReference":"10",
   "storePaymentMethod":true,
   "sessionData":
    "Ab02b4c0!BQABAgCS1GEAfwnynQRaAn/hz6CKKd+j3X4iNvfr9z2v2JgodCC8lGAdRwZT3IgX8csQIvlL7"}'
          ]
        end
        subject = payments_api_client
                  .send_request(
                    :new_session,
                    invoice: invoice,
                    payment_method: "cup",
                    amount_formatter: double("AmountFormatter", amount: 1000, currency: "USD")
                  )
        expect(subject).to be_instance_of(Adyen::Responses::NewSession)
        expect(subject.id).to eq("CSF17219D84AF2BA1D")
        expect(subject.amount_in_local_currency).to eq(1000)
        expect(subject.session_data).to eq("Ab02b4c0!BQABAgCS1GEAfwnynQRaAn/hz6CKKd+j3X4iNvfr9z2v2JgodCC8lGAdRwZT3IgX8csQIvlL7")
      end
    end
    context "/payments/details" do
      it "fetch adyen payment methods for each country" do
        stubs.post("/payments/details") do
          [
            200,
            { "Content-Type": "application/json" },
            '{ "pspReference": "NC6HT9CRT65ZGN82", "resultCode": "Authorised","merchantReference": "430"}'
          ]
        end
        subject = payments_api_client
                  .send_request(
                    :payment_detail,
                    redirect_result: "X6XtfGC3!Y.."
                  )
        expect(subject).to be_instance_of(Adyen::Responses::PaymentDetail)
        expect(subject.psp_reference).to eq("NC6HT9CRT65ZGN82")
        expect(subject.result_code).to eq("Authorised")
        expect(subject.merchant_reference).to eq("430")
      end
    end
  end
end
