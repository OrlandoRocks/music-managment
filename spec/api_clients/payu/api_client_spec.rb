require "rails_helper"

describe Payu::ApiClient do
  describe "#send_request" do

    let(:person) { create(:person) }
    let(:invoice) { create(:invoice, :settled, :with_payu_settlement, person: person) }
    let(:refund) { create(:refund, invoice: invoice) }

    it "returns a valid Faraday Response object" do
      allow_any_instance_of(Faraday::Connection).to receive(:post).and_return(
        Faraday::Response.new(body: { status: 1, msg: "Refund Request Queued" }.to_json)
      )
      response = Payu::ApiClient.new.send_request(:refund_transaction, {
        refund_id: refund.id,
        amount: 10
      })
      expect(response).to be_instance_of(Payu::Responses::RefundTransaction)
    end
  end
end
