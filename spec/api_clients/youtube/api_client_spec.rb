require 'rails_helper'
describe Youtube::ApiClient do
  let(:response) { double("response") }
  let(:auth) { "46>45"}
  let(:refresh_token) { "1/0dIahxVuKBnAiCgYIARA" }
  let(:access_token) { "1/fFAGRNJru1FTz70BzhT3Zg"}
  let(:token_response_body) { {
  "access_token": access_token,
  "refresh_token": refresh_token,
  "expires_in": 3920,
  "scope": "https://www.googleapis.com/auth/drive.metadata.readonly",
  "token_type": "Bearer"
  }.to_json }

  before do
    allow_any_instance_of(Youtube::ApiClient).to receive(:execute_token_post).and_return(response)
    allow(response).to receive(:body).and_return(token_response_body)
    allow(response).to receive(:success?).and_return(true)
  end

  describe "fetch_refresh_token" do
    subject(:client) { Youtube::ApiClient.new(authorization: auth) }


    it "executes request to fetch token from youtube" do
      expect_any_instance_of(Youtube::ApiClient).to receive(:execute_token_post).and_return(response)
      expect(subject.fetch_refresh_token).to eq(refresh_token)
    end
  end

  describe "fetch_channel_id" do
    let(:refresh_response) { double("refresh_response") }
    let(:refresh_response_body) {
          {
            "access_token": "1/fFAGRNJru1FTz70BzhT3Zg",
            "expires_in": 3920,
            "scope": "https://www.googleapis.com/auth/drive.metadata.readonly",
            "token_type": "Bearer"
          }.to_json
        }

    let(:channel_response) { double("channel_response") }
    let(:channel_response_body) {
      {
        "kind": "youtube#channelListResponse",
        "etag": "etag",
        "nextPageToken": "string",
        "prevPageToken": "string",
        "pageInfo": {
          "totalResults": "integer",
          "resultsPerPage": "integer"
        },
        "items": [
          { id: 1234 }
        ]
      }.to_json
    }


    before do
      allow(refresh_response).to receive(:success?).and_return(true)
      allow(refresh_response).to receive(:body).and_return(refresh_response_body)
      allow(channel_response).to receive(:success?).and_return(true)
      allow(channel_response).to receive(:body).and_return(channel_response_body)
      allow(subject).to receive(:execute_channel_request).and_return(channel_response)
      allow(subject).to receive(:execute_refresh_post).and_return(refresh_response)
    end

    context "When request is sucessful" do
      context "with authorization code" do
        subject(:client) { Youtube::ApiClient.new(authorization: auth) }
        it "executes request to fetch channel from youtube" do
          expect(subject).to receive(:execute_channel_request).and_return(channel_response)
          channel = subject.fetch_channel_id
          expect(channel).to eq(1234)
        end
      end

      context "with refresh_token" do
        subject(:client) { Youtube::ApiClient.new(refresh_token: refresh_token) }

        it "executes request to fetch new access_token" do
          expect(subject).to receive(:execute_refresh_post)
          subject.fetch_channel_id
        end

         it "executes request to fetch channel from youtube" do
          expect(subject).to receive(:execute_channel_request).and_return(channel_response)
          channel = subject.fetch_channel_id
          expect(channel).to eq(1234)
        end
      end
    end

    context "when there is a issue connecting to the api" do

      let(:response_body) {
        {"error"=>"bad_gateway", "error_description"=>"Server Error"}.to_json
      }

      before do
        allow(response).to receive(:success?).and_return(false)
        allow(response).to receive(:body).and_return(response_body)
        allow(response).to receive(:status).and_return(500)
      end

      it "raises a custom error" do
        expect{ subject.fetch_channel_id }.to raise_error(Youtube::ApiClient::ServerError)
      end
    end

    context "when there is an error with the request" do 
       let(:response_body) {
        {"error"=>"bad_request", "error_description"=>"Invalid Grant"}.to_json
      }

      before do
        allow(response).to receive(:success?).and_return(false)
        allow(response).to receive(:body).and_return(response_body)
        allow(response).to receive(:status).and_return(400)
      end

      it "raises a custom error" do
        expect{ subject.fetch_channel_id }.to raise_error(Youtube::ApiClient::ApiError)
      end
    end 

    context "when there is an account error" do

      let(:response_body) {
        { "error" => {
          "code" => 403,
          "message" => "The YouTube account of the authenticated user is suspended. In case the authenticated user is acting on behalf of another Google account, then this error refers to the latter.",
          "errors" => [
            {
              "domain" => "youtube.common", 
              "reason" => "authenticatedUserAccountSuspended"
            }
          ]
          }
        }.to_json
      }

      before do
        allow(response).to receive(:success?).and_return(false)
        allow(response).to receive(:body).and_return(response_body)
        allow(response).to receive(:status).and_return(400)
      end

      it "raises a custom error" do
        expect{ subject.fetch_channel_id }.to raise_error(Youtube::ApiClient::AccountError)
      end
    end

    context "when the API Quota is exceeded" do

      let(:response_body) {
        { "error" => {
          "code" => 403,
          "message" => "The request cannot be completed because you have exceeded your \u003ca href=\"/youtube/v3/getting-started#quota\"\u003equota\u003c/a\u003e.",
          "errors" => [
            {
              "message"=> "The request cannot be completed because you have exceeded your \u003ca href=\"/youtube/v3/getting-started#quota\"\u003equota\u003c/a\u003e.",
              "domain"=> "youtube.quota",
              "reason"=> "quotaExceeded"
            }
          ]
          }
        }.to_json
      }

      before do
        allow(response).to receive(:success?).and_return(false)
        allow(response).to receive(:body).and_return(response_body)
        allow(response).to receive(:status).and_return(400)
      end

      it "raises a custom error" do
        expect{ subject.fetch_channel_id }.to raise_error(Youtube::ApiClient::ApiQuotaError)
      end
    end
  end
end
