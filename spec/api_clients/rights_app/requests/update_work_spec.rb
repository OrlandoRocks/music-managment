require "rails_helper"

describe RightsApp::Requests::UpdateWork do
  let(:account)   { create(:account, provider_account_id: "TCM0000000028A") }
  let(:composer)  { create(:publishing_composer, is_primary_composer: true, account: account, provider_identifier: "00000008N") }
  let(:cowriter)  { create(:publishing_composer, is_primary_composer: false, account: account, provider_identifier: "0000000DC") }
  let(:composition) do
    create(
      :publishing_composition,
      account:              account,
      name:                 "Compostilskin",
      provider_identifier:  "TCM0000000087S"
    )
  end
  let!(:composer_split) do
    create(
      :publishing_composition_split,
      publishing_composition_id:  composition.id,
      publishing_composer:     composer,
      right_to_collect: false,
      percent:      75.to_d
    )
  end
  let!(:cowriter_split) do
    create(
      :publishing_composition_split,
      publishing_composition_id:  composition.id,
      right_to_collect: false,
      publishing_composer:     cowriter,
      percent:      25.to_d
    )
  end

  it "uses the correct url" do
    request = RightsApp::Requests::UpdateWork.new(composition: composition)
    expect(request.url).to eq "Works"
  end

  it "uses the correct request_method" do
    request = RightsApp::Requests::UpdateWork.new(composition: composition)
    expect(request.request_method).to eq :put
  end

  context "#params" do
    before :each do
      @params = {
        WorkCode:           composition.provider_identifier,
        ArtistAccountCode:  account.provider_account_id,
        Title:              composition.composition_name,
        WriterSplits: [
          {
            WriterCode:             composer.provider_identifier,
            WriterDesignationCode:  2,
            RightToCollect:         true,
            WriterShare:            composer_split.percent
          },
          {
            WriterCode:             cowriter.provider_identifier,
            WriterDesignationCode:  3,
            RightToCollect:         false,
            WriterShare:            cowriter_split.percent
          }
        ],
        IsPublicDomain: false
      }
    end

    it "creates the correct request params" do
      opts    = { composition: composition.reload }
      request = RightsApp::Requests::UpdateWork.new(opts)
      expect(request.params).to eq @params
    end

    it "only includes the first 60 characters of the composition name" do
      long_name = "oh my god, becky. look at that title. it is so big. it looks like one of those rap guys' titles"

      @params[:Title] = long_name.first(60)
      composition.update(name: long_name)
      opts = { composition: composition.reload }

      expect(RightsApp::Requests::UpdateWork.new(opts).params).to eq @params
    end
  end

  context "#writer_code" do
    it "uses the correct composer code" do
      request = RightsApp::Requests::UpdateWork.new(composition: composition)
      expect(request.send(:writer_code, composer)).to eq 2
    end

    it "uses the correct unknown cowriter code" do
      request           = RightsApp::Requests::UpdateWork.new(composition: composition)
      unknown_cowriter  = create(:cowriter, :unknown)
      expect(request.send(:writer_code, unknown_cowriter)).to eq 0
    end

    it "uses the correct known cowriter code" do
      request = RightsApp::Requests::UpdateWork.new(composition: composition)
      expect(request.send(:writer_code, cowriter)).to eq 3
    end

    it "uses the correct arranger code" do
      public_domain_composition = create(
        :publishing_composition,
        account:              account,
        name:                 "Compostilskin",
        provider_identifier:  "TCM0000000087S",
        public_domain: true
      )
      unknown_cowriter  = create(:cowriter, :unknown)

      request = RightsApp::Requests::UpdateWork.new(composition: public_domain_composition)
      expect(request.send(:writer_code, cowriter)).to eq 4
      expect(request.send(:writer_code, unknown_cowriter)).to eq 4
      expect(request.send(:writer_code, composer)).to eq 4
    end
  end
end
