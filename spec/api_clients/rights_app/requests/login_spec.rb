require 'rails_helper'

describe RightsApp::Requests::Login do
  let(:request) { RightsApp::Requests::Login.new }

  it "uses the correct URL" do
    expect(request.url).to eq 'Login'
  end

  it "uses the correct method" do
    expect(request.request_method).to eq :post
  end

  it "references the correct constants" do
    expect(request.params).to be_eql(
      { "TenantApiKey": ENV["SENTRIC_RIGHTS_APP_TENANT_ID"],
        "Username":     ENV["SENTRIC_RIGHTS_APP_USERNAME"],
        "Password":     ENV["SENTRIC_RIGHTS_APP_PASSWORD"]
      }
    )
  end

end
