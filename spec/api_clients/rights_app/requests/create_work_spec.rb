require "rails_helper"

describe RightsApp::Requests::CreateWork do
  let(:account)         { create(:account, provider_account_id: "TCM0000000028A") }
  let(:composition)     { create(:publishing_composition, name: "Night Man Cometh") }
  let(:composer) do
    create(
      :publishing_composer,
      account:              account,
      is_primary_composer:  true,
      provider_identifier:  "00000008N"
    )
  end
  let(:cowriter) do
    create(
      :publishing_composer,
      is_primary_composer:  false,
      provider_identifier:  "0000000DC"
    )
  end
  let!(:composer_split) do
    create(
      :publishing_composition_split,
      publishing_composition_id: composition.id,
      publishing_composer:       composer,
      right_to_collect:    true,
      percent:        50.50
    )
  end
  let!(:cowriter_split) do
    create(
      :publishing_composition_split,
      publishing_composition_id: composition.id,
      publishing_composer:       cowriter,
      right_to_collect:    false,
      percent:        49.50
    )
  end
  let(:params) do
    {
      ArtistAccountCode:  account.provider_account_id,
      Title:              composition.name,
      IsRemix:            false,
      ContainsSamples:    false,
      WriterSplits: [
        {
          WriterCode:             composer.provider_identifier,
          WriterDesignationCode:  2,
          RightToCollect:         true,
          WriterShare:            composer_split.percent
        },
        {
          WriterCode:             cowriter.provider_identifier,
          WriterDesignationCode:  3,
          RightToCollect:         false,
          WriterShare:            cowriter_split.percent
        }
      ],
      IsPublicDomain: false
    }
  end
  let(:opts)    { { composer: composer, composition: composition } }
  let(:request) { RightsApp::Requests::CreateWork.new(opts) }

  it "uses the correct url" do
    expect(request.url).to eq "Works"
  end

  it "uses the correct request_method" do
    expect(request.request_method).to eq :post
  end

  it "includes the correct params" do
    expect(request.params).to eq params
  end

  it "only includes the first 60 characters of the composition name" do
    long_name = "oh my god, becky. look at that title. it is so big. it looks like one of those rap guys' titles"

    params[:Title] = long_name.first(60)

    composition.update(name: long_name)
    expect(RightsApp::Requests::CreateWork.new(opts).params).to eq params
  end

  context "#writer_code" do
    it "uses the correct composer code" do
      expect(request.send(:writer_code, composer)).to eq 2
    end

    it "uses the correct unknown cowriter code" do
      unknown_cowriter = create(:cowriter, :unknown)
      expect(request.send(:writer_code, unknown_cowriter)).to eq 0
    end

    it "uses the correct known cowriter code" do
      expect(request.send(:writer_code, cowriter)).to eq 3
    end

    it "uses the arranger writer code for public domain works" do
      public_domain_composition = create(:publishing_composition, name: "Night Man Cometh", public_domain: true)
      public_domain_opts        = { composer: composer, composition: public_domain_composition }
      unknown_cowriter = create(:cowriter, :unknown)
      request                   = RightsApp::Requests::CreateWork.new(public_domain_opts)

      expect(request.send(:writer_code, composer)).to eq 4
      expect(request.send(:writer_code, cowriter)).to eq 4
      expect(request.send(:writer_code, unknown_cowriter)).to eq 4
    end
  end
end
