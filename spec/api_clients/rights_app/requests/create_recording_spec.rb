require "rails_helper"

describe RightsApp::Requests::CreateRecording do
  let(:creative)      { [{ "name" => "D, Money", "role" => "primary_artist" }] }
  let(:album)         { create(:album, creatives: creative) }
  let(:composition)   { create(:composition, provider_identifier: "TCM0000000104S", name: "Composition Name") }
  let!(:song)         { create(:song, album: album, composition_id: composition.id) }
  let(:opts)          { { composition: composition, song: song } }
  let(:request)       { RightsApp::Requests::CreateRecording.new(opts) }

  it "uses the correct url" do
    expect(request.url).to eq "Recordings"
  end

  it "uses the correct request_method" do
    expect(request.request_method).to eq :post
  end

  describe "#params" do
    it "sets the correct param keys" do
      param_keys = [:WorkCode, :Title, :PerformingArtist, :Isrc, :RecordLabel, :ReleaseDate]

      expect(request.params.keys).to eq param_keys
    end

    it "sanitizes the Title, Performing Artist and the Record Label" do
      expect(request.params[:Title]).to eq "Composition Name"
      expect(request.params[:PerformingArtist]).to eq "D Money"
      expect(request.params[:RecordLabel]).to eq "D Money"
    end

    it "truncates artist title to 45 characters" do
      album.creatives.first.update(name: Faker::Lorem.characters(number: 50))
      expect(request.params[:PerformingArtist].length).to eq(45)
    end

    context "when the album is a non_tunecore_album" do
      let(:non_tunecore_album) { create(:non_tunecore_album, record_label: "Record Label Name") }
      let(:non_tunecore_song) { create(:non_tunecore_song, non_tunecore_album: non_tunecore_album, composition_id: composition.id) }
      let(:opts) { { composition: composition, non_tunecore_song: non_tunecore_song } }
      let(:request) { RightsApp::Requests::CreateRecording.new(opts) }

      it "sets the RecordLabel key value correctly" do
        expect(request.params[:RecordLabel]).to eq(non_tunecore_album.record_label)
      end
    end
  end
end
