require 'rails_helper'

describe RightsApp::Requests::UpdateWriter do
  let(:ascap)     { create(:performing_rights_organization, name: "ASCAP") }
  let(:publisher) { create(:publisher, performing_rights_organization_id: ascap.id) }
  let(:account)   { create(:account, provider_account_id: "TCM0000000028A") }
  let(:composer) do
    create(
      :composer,
      publisher: publisher,
      account: account,
      performing_rights_organization: ascap,
      provider_identifier: "00000008N",
      cae: "123456789"
    )
  end
  let(:params) do
    { WriterCode:         composer.provider_identifier,
      FirstName:          composer.first_name,
      MiddleName:         composer.middle_name,
      LastName:           composer.last_name,
      ArtistAccountCode:  account.provider_account_id,
      SocietyId:          composer.performing_rights_organization.provider_identifier,
      CaeipiNumber:       composer.cae,
      PublisherName:      composer.publisher.name,
      PublisherCaeipi:    composer.publisher.cae }
  end
  let(:request) { RightsApp::Requests::UpdateWriter.new(writer: composer) }

  it "uses the correct url" do
    expect(request.url).to eq "Writers"
  end

  it "uses the correct request_method" do
    expect(request.request_method).to eq :put
  end

  it "includes the correct params" do
    expect(request.params).to eq params
  end

  it "distinguishes between cowriter and composer" do
    cowriter        = create(:cowriter, composer: composer, provider_identifier: "0000000DC")
    cowriter_params = {
      FirstName:          cowriter.first_name,
      LastName:           cowriter.last_name,
      UnknownWriter:      cowriter.unknown?,
      ArtistAccountCode:  cowriter.composer.account.provider_account_id,
      WriterCode:         cowriter.provider_identifier
    }
    expect(RightsApp::Requests::UpdateWriter.new(writer: cowriter).params).to eq cowriter_params
  end

  context "includes the correct params" do
    it "includes the PublisherName and PublisherCaeipi if the Composer PRO is ASCAP, BMI, or SESAC" do
      expect(request.params).to eq params
    end

    it "does not include the PublisherName and PublisherCaeipi if the Composer PRO is not ASCAP, BMI, or SESAC" do
      pro                       = create(:performing_rights_organization)
      params[:SocietyId]        = pro.provider_identifier
      params[:PublisherName]    = nil
      params[:PublisherCaeipi]  = nil

      composer.update(performing_rights_organization_id: pro.id)
      expect(RightsApp::Requests::UpdateWriter.new(writer: composer).params).to eq params
    end
  end
end
