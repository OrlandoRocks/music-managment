require "rails_helper"

describe RightsApp::Requests::GetArtistAccount do
  let(:account)   { create(:account, provider_account_id: "ABCD1234") }
  let(:opts)      { { account: account } }
  let(:request)   { RightsApp::Requests::GetArtistAccount.new(opts) }

  it "uses the correct url" do
    expect(request.url).to eq "ArtistAccount/#{account.provider_account_id}"
  end

  it "uses the correct request_method" do
    expect(request.request_method).to eq :get
  end

  it "includes the correct params" do
    expect(request.params).to eq({})
  end

end
