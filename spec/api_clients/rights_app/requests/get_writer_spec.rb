require 'rails_helper'

describe RightsApp::Requests::GetWriter do
  let(:writer_code) { "12345678" }
  let(:account)     { create(:account, provider_account_id: "DMC789") }
  let(:composer)    { create(:composer, account: account, provider_identifier: writer_code) }
  let(:opts)        { { writer: composer } }
  let(:request)     { RightsApp::Requests::GetWriter.new(opts) }

  context "when a composer is the writer" do
    context "when a composer is passed in" do
      it "uses the correct url when a writer is passed as an option" do
        expect(request.url).to eq "Writers/#{composer.provider_identifier}/#{account.provider_account_id}"
      end
    end

    context "when a writer code is passed as an option" do
      it "uses the correct url" do
        opts        = { writer_code: writer_code, account: account }
        new_request = RightsApp::Requests::GetWriter.new(opts)
        expect(new_request.url).to eq "Writers/#{writer_code}/#{account.provider_account_id}"
      end
    end
  end

  context "when a publishing_composer is the writer" do
    let(:publishing_composer) { create(:publishing_composer, account: account, provider_identifier: writer_code) }

    context "when the publishing_composer is passed in" do
      let(:opts) { { writer: publishing_composer } }

      it "uses the correct url" do
        expect(request.url).to eq "Writers/#{publishing_composer.provider_identifier}/#{account.provider_account_id}"
      end
    end

    context "when a writer code is passed as an option" do
      let(:opts) { { writer_code: writer_code, account: account } }

      it "uses the correct url" do
        expect(request.url).to eq "Writers/#{publishing_composer.provider_identifier}/#{account.provider_account_id}"
      end
    end
  end

  it "uses the correct request method" do
    expect(request.request_method).to be :get
  end

  it "uses the correct params" do
    expect(request.params).to eq({})
  end
end
