require 'rails_helper'

describe RightsApp::Requests::CreateArtistAccount do
  let(:account)   { create(:account) }
  let(:opts)      { { account: account } }
  let(:request)   { RightsApp::Requests::CreateArtistAccount.new(opts) }

  it "uses the correct url" do
    expect(request.url).to eq "ArtistAccount"
  end

  it "uses the correct request_method" do
    expect(request.request_method).to eq :post
  end

  it "includes the correct params" do
    params = {
      AccountName:    account.person.name,
      CatalogueCode:  account.catalogue_code,
      PerformedLiveLastSixMonths: false
    }
    expect(request.params).to eq params
  end

  it "escapes special characters from the artist account name" do
    account.person.update(name: "D$ ; and th` <dawg} pound?    ")
    request = RightsApp::Requests::CreateArtistAccount.new(account: account)
    expect(request.params[:AccountName]).to eq "DS  and th dawg pound"
  end

end
