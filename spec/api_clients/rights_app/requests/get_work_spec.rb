require "rails_helper"

describe RightsApp::Requests::GetWork do
  let(:composition) { create(:composition, provider_identifier: "TCM0000000028S") }
  let(:request)     { RightsApp::Requests::GetWork.new(work_code: composition.provider_identifier) }

  it "uses the correct url" do
    expect(request.url).to eq "Works/#{composition.provider_identifier}"
  end

  it "uses the correct request_method" do
    expect(request.request_method).to eq :get
  end

  it "includes the correct params" do
    expect(request.params).to eq({})
  end

  it "assigns the found composition to #requestable" do
    expect(request.requestable).to eq(composition)
  end
end
