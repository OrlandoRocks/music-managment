require "rails_helper"

describe RightsApp::Requests::GetWorkList do
  let(:account)   { create(:account, provider_account_id: "TC0000000123") }
  let(:params)    { { artist_account_code: account.provider_account_id, page: 1 } }
  let(:request)   { RightsApp::Requests::GetWorkList.new(params) }

  it "uses the correct URL" do
    expect(request.url).to eq "Works/#{account.provider_account_id}"
  end

  it "uses the correct request method" do
    expect(request.request_method).to eq :get
  end

  it "includes the correct params" do
    expect(request.params).to eq(Page: params[:page], PageSize: 100)
  end
end
