require "rails_helper"

describe RightsApp::Requests::GetRecordingList do
  let(:composition)   { create(:composition, provider_identifier: "TC0000000123W") }
  let(:params)        { { composition: composition, page: 1} }
  let(:request)       { RightsApp::Requests::GetRecordingList.new(params) }

  it "uses the correct URL" do
    expect(request.url).to eq "Recordings/#{composition.provider_identifier}"
  end

  it "uses the correct request method" do
    expect(request.request_method).to eq :get
  end

  it "includes the correct params" do
    expect(request.params).to eq(Page: params[:page], PageSize: 100)
  end
end
