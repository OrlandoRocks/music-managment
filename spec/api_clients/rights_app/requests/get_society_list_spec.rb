require "rails_helper"

describe RightsApp::Requests::GetSocietyList do
  let(:request)   { RightsApp::Requests::GetSocietyList.new(page: 1) }

  it "uses the correct URL" do
    expect(request.url).to eq "Society"
  end

  it "uses the correct request method" do
    expect(request.request_method).to eq :get
  end

  it "includes the correct params" do
    expect(request.params).to eq(Page: 1, PageSize: 100)
  end

end
