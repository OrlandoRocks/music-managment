require 'rails_helper'

describe RightsApp::Requests::GetWriterList do
  let(:composer)  { create(:composer) }
  let(:params)    { { artist_account_code: "ABCD1234", page: 1 } }
  let(:request)   { RightsApp::Requests::GetWriterList.new(params) }

  it "uses the correct URL" do
    expect(request.url).to eq "Writers/#{params[:artist_account_code]}"
  end

  it "uses the correct request method" do
    expect(request.request_method).to eq :get
  end

  it "includes the correct params" do
    expect(request.params).to eq({ Page: params[:page], PageSize: 100 })
  end
end
