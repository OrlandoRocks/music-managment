require "rails_helper"

describe RightsApp::ApiClient do
  let(:api_client)      { RightsApp::ApiClient.new }
  let(:http_client)     { double(:http_client) }
  let(:body)            { { Token: "token" } }
  let(:response)        { double(:response, body: body.to_json, status: 200) }

  let(:headers) do
    { "Authorization" => "token",
      "Content-Type"  => "application/json",
      "If-Match"      => "this is an etag" }
  end

  before :each do
    allow(http_client).to receive(:headers).and_return(headers)
    allow(api_client).to receive(:http_client).and_return(http_client)
    allow(api_client).to receive(:res).and_return(response)
  end

  describe "#build_content_type_header" do
    it "adds a Content-Type header to the http request" do
      api_client.build_content_type_header
      expect(api_client.http_client.headers["Content-Type"]).to eq "application/json"
    end
  end

  describe "#build_path" do
    it "builds a path to the appropriate request class" do
      expect(api_client.build_path("GetWork", "RightsApp::Requests")).to eq RightsApp::Requests::GetWork
    end

    it "builds a path to the appropriate response class" do
      expect(api_client.build_path("Work", "RightsApp::Responses")).to eq RightsApp::Responses::Work
    end
  end

  describe "#login" do
    before :each do
      allow(http_client).to receive(:post).with("Login").and_return(true)
    end

    it "saves the Session Token to redis so it can be shared by TC Users throughout the day" do
      expect($redis).to receive(:set).and_return(true)
      api_client.login
    end
  end

  describe "#send_request" do
    let(:writer_code)  { Faker::Number.new }
    let(:account)      { create(:account, provider_account_id: Faker::Number.new) }
    let(:composer)     { create(:composer, account: account) }
    let(:composition)  { create(:composition) }
    let(:request_name) { "GetWork" }
    let(:response)     { { "WorkCode" => writer_code, "Messages" => [] } }
    let(:opts)         { { composer: composer, composition: composition } }

    before do
      allow(api_client).to receive(:build_headers).and_return(true)
      allow(api_client).to receive(:res).and_return(double(:response, body: body.to_json, status: 200))
      allow($redis)
        .to receive(:get)
              .and_return(headers["Authorization"])
    end

    it "builds a request path to the appropriate response class and generates a response" do
      response_class = RightsApp::Responses::Work
      json_response = OpenStruct.new(body: response.to_json)
      allow_any_instance_of(RightsApp::ApiClient).to receive(:send) { json_response }

      expect(response_class).to receive(:new).with(json_response).and_call_original

      api_client.send_request(request_name, opts)
    end

    context "when the response has an error" do
      it "invokes the error service" do
        response["Messages"] = ["No writer found"]
        json_response = OpenStruct.new(body: response.to_json)
        allow_any_instance_of(RightsApp::ApiClient).to receive(:send) { json_response }

        expect(PublishingAdministration::ErrorService).to receive(:log_errors)

        api_client.send_request(request_name, opts)
      end
    end

    context "when the response does not have an error" do
      it "does not invoke the error service" do
        json_response = OpenStruct.new(body: response.to_json)
        allow_any_instance_of(RightsApp::ApiClient).to receive(:send) { json_response }

        expect(PublishingAdministration::ErrorService).not_to receive(:log_errors)

        api_client.send_request(request_name, opts)
      end
    end

    context "when the requestable already exists" do
      it "does not invoke the error service" do
        response["Messages"] = ["there is an existing record"]
        json_response = OpenStruct.new(body: response.to_json)
        allow_any_instance_of(RightsApp::ApiClient).to receive(:send) { json_response }

        expect(PublishingAdministration::ErrorService).not_to receive(:log_errors)

        api_client.send_request(request_name, opts)
      end
    end

    context "when neither a work, writer or recording is found" do
      it "does not invoke the error service" do
        response["Messages"] = ["there is an existing record"]
        json_response = OpenStruct.new(body: response.to_json)
        allow_any_instance_of(RightsApp::ApiClient).to receive(:send) { json_response }

        expect(PublishingAdministration::ErrorService).not_to receive(:log_errors)

        api_client.send_request(request_name, opts)
      end
    end

    context "there is no existing session token" do
      it "logs in to create a new session" do
        allow($redis)
          .to receive(:get)
                .and_return(nil)

        json_response = OpenStruct.new(body: response.to_json, http_status: 401)

        allow(json_response).to receive(:http_status).and_return(200)
        allow_any_instance_of(RightsApp::ApiClient).to receive(:send) { json_response }
        allow(http_client).to receive(:post).with("Login").and_return(true)
        expect($redis).to receive(:set)

        api_client.send_request(request_name, opts)
      end
    end

    context "existing session is expired" do
      it "gets a new session token" do
        allow($redis)
          .to receive(:get)
                .and_return("asdf")

        json_response = OpenStruct.new(body: response.to_json, status: 401)
        allow(json_response).to receive(:status).and_return(401)
        allow_any_instance_of(RightsApp::ApiClient).to receive(:send) { json_response }
        allow(api_client).to receive(:login).and_return(true)
        allow(http_client).to receive(:post).with("Login").and_return(true)
        expect(api_client).to receive(:login)

        api_client.send_request(request_name, opts)
      end
    end

    context "RightsApp API returns a 500 status code" do
      it "retries exponentially slower up to three times" do
        allow($redis)
          .to receive(:get)
                .and_return("asdf")
        allow_any_instance_of(RightsApp::ApiClient).to receive(:sleep)

        json_response = OpenStruct.new(body: response.to_json, status: 500)
        allow(json_response).to receive(:status).and_return(500)
        allow_any_instance_of(RightsApp::ApiClient).to receive(:send) { json_response }
        allow(api_client).to receive(:login).and_return(true)
        allow(http_client).to receive(:post).with("Login").and_return(true)

        expect(api_client).to receive(:sleep).exactly(3).times

        api_client.send_request(request_name, opts)
      end
    end
  end
end
