require "rails_helper"

describe RightsApp::Responses::WorkList do
  let(:account) { create(:account, provider_account_id: "TC0000000123") }
  let(:work) { double("work").to_json }
  let(:json_response) do
    {
      ArtistAccountCode: account.provider_account_id,
      TotalCount: 2,
      Records: [ work ],
      Messages: []
    }
  end

  describe "#initialize" do
    it "sets the body and json_response" do
      response_dbl = double(:json_response, body: json_response.to_json)
      response = RightsApp::Responses::WorkList.new(response_dbl)

      expect(response.body).to eq json_response.with_indifferent_access
      expect(response.json_response).to eq response_dbl
    end
  end

  describe "#errors?" do
    it "returns true if there are any messages in the response" do
      json_response[:Messages] = ["This is an error message?"]
      response_dbl = double(:json_response, body: json_response.to_json)
      response = RightsApp::Responses::WorkList.new(response_dbl)

      expect(response.errors?).to be_truthy
    end
  end

  describe "#works" do
    it "instantiates a WorkListRecord for each work" do
      response_dbl = double(:json_response, body: json_response.to_json)
      response = RightsApp::Responses::WorkList.new(response_dbl)

      expect(RightsApp::Responses::WorkListRecord).to receive(:new).with(work)

      response.works
    end
  end
end
