require "rails_helper"

describe RightsApp::Responses::ArtistAccount do
  let(:artist_acct_code)  { "ABCD1234" }
  let(:json_response) do
    { ArtistAccountCode: artist_acct_code,
      AccountName: "Dana Czinsky",
      FacebookPageUrl: nil,
      TwitterProfileUrl: nil,
      SoundcloudPageUrl: nil,
      ArtistAccountRegistrationDate: "2018-09-07T16:34:57.5684858Z",
      ReleasedToServiceOrStore: false,
      PerformedLiveLastSixMonths: false,
      CatalogueCode: "TCM0000000001C",
      IsDeleted: false,
      Messages: [] }.to_json
  end

  describe "#initialize" do
    it "sets the body and json_response" do
      response_dbl = double(:resposne, body: json_response)
      response = RightsApp::Responses::ArtistAccount.new(response_dbl)

      expect(response.json_response).to eq response_dbl
      expect(response.body).to eq JSON.parse(json_response)
    end
  end

  describe "#retry?" do
    it "returns true if the response has a status of 429 (Too Many Requests)" do
      response_dbl = double(:resposne, body: json_response, status: 429)
      response = RightsApp::Responses::ArtistAccount.new(response_dbl)

      expect(response.retry?).to be_truthy
    end

    it "returns false if the response has a status that in not 429" do
      response_dbl = double(:resposne, body: json_response, status: 200)
      response = RightsApp::Responses::ArtistAccount.new(response_dbl)

      expect(response.retry?).to be_falsey
    end
  end
end
