require "rails_helper"

describe RightsApp::Responses::Writer do
  let(:publisher)     { create(:publisher) }
  let(:writer_code)   { "00001018" }
  let(:account)       { create(:account, provider_account_id: "DMC123") }
  let(:headers)       { { etag: "random letters and numbers" } }
  let(:composer) do
    create(
      :composer,
      publisher: publisher,
      account: account,
      cae: "987654321",
      performing_rights_organization: publisher.performing_rights_organization
    )
  end

  describe "#initialize" do
    it "parses the raw json response" do
      body = {
        "WriterCode" => writer_code,
        "FirstName"  => composer.first_name,
        "LastName"   => composer.last_name,
        "Messages"   => []
      }.to_json

      json_response   = OpenStruct.new(body: body, headers: headers)
      writer_response = RightsApp::Responses::Writer.new(json_response)

      expect(writer_response.body).to eq JSON.parse(body)
      expect(writer_response.writer_code).to eq writer_code
    end

    it "raises an error if there is no society id" do
      body = {
        "WriterCode" => writer_code,
        "FirstName"  => composer.first_name,
        "LastName"   => composer.last_name,
        "Messages"   => ["No society found."]
      }.to_json

      json_response = OpenStruct.new(body: body, headers: headers)
      expect{ RightsApp::Responses::Writer.new(json_response) }.to raise_error
    end
  end

  describe "#already_exists?" do
    it "returns true if response message includes 'Writer already exists.'" do
      body            = { "WriterCode" => "1", "Message" => "Writer already exists." }.to_json
      json_response   = OpenStruct.new(body: body, headers: headers)
      writer_response = RightsApp::Responses::Writer.new(json_response)

      expect(writer_response.already_exists?).to be_truthy
    end
  end

  describe "#needs_update?" do
    it "returns true if the composer data does not match the writer response data" do
      body = {
        "WriterCode" => writer_code,
        "FirstName"  => composer.first_name,
        "LastName"   => composer.last_name,
        "Messages"   => []
      }.to_json
      json_response   = OpenStruct.new(body: body, headers: headers)
      writer_response = RightsApp::Responses::Writer.new(json_response)

      expect(writer_response.needs_update?(composer)).to be_truthy
    end

    it "returns true if the cowriter data does not match the writer response data" do
      cowriter = create(:cowriter, composer: composer)
      body = {
        "WriterCode" => writer_code,
        "FirstName"  => "Dana",
        "MiddleName" => "",
        "LastName"   => "C",
        "Messages"   => []
      }.to_json

      json_response   = OpenStruct.new(body: body, headers: headers)
      writer_response = RightsApp::Responses::Writer.new(json_response)

      expect(writer_response.needs_update?(cowriter)).to be_truthy
    end

    it "returns false if the writer response data has changes pending" do
      body = {
        "WriterCode" => writer_code,
        "FirstName"  => composer.first_name,
        "LastName"   => composer.last_name,
        "ChangesPending" => true,
        "Messages"   => []
      }.to_json
      json_response   = OpenStruct.new(body: body, headers: headers)
      writer_response = RightsApp::Responses::Writer.new(json_response)

      expect(writer_response.needs_update?(composer)).to be_falsey
    end
  end

  describe "#has_publisher?" do
    context "when the response does NOT include publisher data" do
      it "returns false" do
        body = { "WriterCode" => writer_code }.to_json
        json_response   = double(:response, body: body)
        writer_response = RightsApp::Responses::Writer.new(json_response)

        expect(writer_response.has_publisher?).to be_falsey
      end
    end

    context "when the response includes a publisher name" do
      it "returns true" do
        body = { "WriterCode" => writer_code, "PublisherName" => "Pub Name" }.to_json
        json_response   = double(:response, body: body)
        writer_response = RightsApp::Responses::Writer.new(json_response)

        expect(writer_response.has_publisher?).to be_truthy
      end
    end

    context "when the response includes a publisher cae number" do
      it "returns true" do
        body = { "WriterCode" => writer_code, "PublisherCaeipi" => "123456789" }.to_json
        json_response   = double(:response, body: body)
        writer_response = RightsApp::Responses::Writer.new(json_response)

        expect(writer_response.has_publisher?).to be_truthy
      end
    end
  end
end
