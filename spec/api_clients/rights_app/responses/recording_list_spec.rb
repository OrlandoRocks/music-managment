require "rails_helper"

describe RightsApp::Responses::RecordingList do
  let(:composition)   { create(:composition, provider_identifier: "TC0000000123W") }
  let(:song)          { create(:song, composition_id: composition.id) }
  let(:json_response) do
    {
      WorkCode: composition.provider_identifier,
      Records: [
        {
          RecordingId:  "da1018na-15-d3ad-1n5id3",
          Title:        Faker::Book.title,
          Isrc:         song.isrc
        },
        {
          RecordingId:  "5t1ll-d3ad",
          Title:        Faker::Book.title,
          Isrc:         "NTC666"
        }
      ],
      TotalCount: 2,
      PageSize: 2,
      Messages: ["There was an error"]
    }
  end

  describe "#initialize" do
    it "sets the body and json_response" do
      response_dbl = double(:json_response, body: json_response.to_json)
      response = RightsApp::Responses::RecordingList.new(response_dbl)

      expect(response.body).to eq json_response.with_indifferent_access
      expect(response.json_response).to eq response_dbl
    end
  end

  describe "#errors?" do
    it "returns true if there are any messages in the response" do
      response_dbl = double(:json_response, body: json_response.to_json)
      response = RightsApp::Responses::RecordingList.new(response_dbl)

      expect(response.errors?).to be_truthy
    end
  end

  describe "#empty_list?" do
    it "returns true if there is a 'Writer Not Found' error message" do
      json_response[:Messages] = ["Writer Not Found"]
      response_dbl = double(:json_response, body: json_response.to_json)
      response = RightsApp::Responses::RecordingList.new(response_dbl)

      expect(response.empty_list?).to be_truthy
    end

    it "returns true if there is a 'No Work Found' error message" do
      json_response[:Messages] = ["No Work found."]
      response_dbl = double(:json_response, body: json_response.to_json)
      response = RightsApp::Responses::RecordingList.new(response_dbl)

      expect(response.empty_list?).to be_truthy
    end

    it "returns true if there is a 'No Recordings Found' error message" do
      json_response[:Messages] = ["No recordings found."]
      response_dbl = double(:json_response, body: json_response.to_json)
      response = RightsApp::Responses::RecordingList.new(response_dbl)

      expect(response.empty_list?).to be_truthy
    end

    it "return false if there is no  'No Recordings Found', 'Writer Not Found', or 'No Work Found' error" do
      response_dbl = double(:json_response, body: json_response.to_json)
      response = RightsApp::Responses::RecordingList.new(response_dbl)

      expect(response.empty_list?).to be false
    end
  end
end
