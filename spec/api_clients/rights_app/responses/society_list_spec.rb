require "rails_helper"

describe RightsApp::Responses::SocietyList do
  let(:body_double) { double(:body) }
  let(:json_response) do
    {
      Records: [
        {
          id:         "39ea3ed9-5b98-d775-661d-e356f7851ed0",
          name:       "ASCAP",
          cicaccode:  10,
          isdeleted:  false
        },
        {
          id:         "39ea3ed9-5b98-c09f-32a4-2c7d9bdb8361",
          name:       "ASSCAT",
          cicaccode:  21,
          isdeleted:  false
        }
      ],
      TotalCount: 2,
      Messages: ["Error"]
    }.to_json
  end

  describe "#initialize" do
    it "sets the body and json_response" do
      response_dbl = double(:json_response, body: json_response)
      response = RightsApp::Responses::SocietyList.new(response_dbl)

      expect(response.body).to eq JSON.parse(json_response)
      expect(response.json_response).to eq response_dbl
    end
  end

  describe "#errors?" do
    it "returns true if there are any messages in the response" do
      response_dbl = double(:json_response, body: json_response)
      response = RightsApp::Responses::SocietyList.new(response_dbl)

      expect(response.errors?).to be_truthy
    end
  end
end
