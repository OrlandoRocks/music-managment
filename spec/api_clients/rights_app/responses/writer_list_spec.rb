require "rails_helper"

describe RightsApp::Responses::WriterList do
  let(:artist_account_id) { "ABCD1234" }
  let(:account)           { create(:account, provider_account_id: artist_account_id)}
  let(:composer)          { create(:composer, account: account) }
  let(:other_composer)    { create(:composer, account: account) }
  let(:writer_code)       { "000011" }
  let(:json_response) do
      { ArtistAccountCode: artist_account_id,
        Records: [
          {
            WriterCode: writer_code,
            FirstName: composer.first_name,
            MiddleName: composer.middle_name.nil? ? "" : composer.middle_name,
            LastName: composer.last_name
          },
          {
            WriterCode: "000022",
            FirstName: other_composer.first_name,
            MiddleName: other_composer.middle_name.nil? ? "" : other_composer.middle_name,
            LastName: other_composer.last_name
          }
        ],
        TotalCount: 2,
        PageSize: 2,
        Messages: ["Error"]
      }.to_json
  end

  describe "#initialize" do
    it "parses the raw json response" do
      response_dbl = double(:json_response, body: json_response)
      response = RightsApp::Responses::WriterList.new(response_dbl)

      expect(response.body).to eq JSON.parse(json_response)
    end
  end

  describe "#errors?" do
    it "returns true if there are any messages in the response" do
      response_dbl = double(:json_response, body: json_response)
      response = RightsApp::Responses::WriterList.new(response_dbl)

      expect(response.errors?).to be true
    end
  end
end
