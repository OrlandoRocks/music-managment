require "rails_helper"

describe RightsApp::Responses::Work do
  let(:work_code)   { "TCM0000000104S" }
  let(:account)     { create(:account, provider_account_id: "TCM0000000028A") }
  let(:composer)    { create(:composer, account: account, provider_identifier: "00000008N") }
  let(:cowriter)    { create(:cowriter, composer: composer, provider_identifier: "0000000DC") }
  let(:composition) { create(:composition, name: "Night Man Cometh") }
  let!(:composer_split) do
    create(
      :publishing_split,
      composition_id: composition.id,
      composer:       composer,
      writer_id:      composer.id,
      writer_type:    "Composer",
      percent:        50.50.to_d
    )
  end
  let!(:cowriter_split) do
    create(
      :publishing_split,
      composition_id: composition.id,
      composer:       composer,
      writer_id:      cowriter.id,
      writer_type:    "Cowriter",
      percent:        49.50.to_d
    )
  end
  let(:json_response) do
    {
      WorkCode:           work_code,
      ArtistAccountCode:  composer.account.provider_account_id,
      Title:              composition.name,
      IsDeactivated:      false,
      WriterSplits: [
        {
          WriterCode:     composer.provider_identifier,
          RightToCollect: true,
          WriterShare:    composer_split.percent,
          WriterDesignationCode: 2
        },
        {
          WriterCode:     cowriter.provider_identifier,
          RightToCollect: false,
          WriterShare:    cowriter_split.percent,
          WriterDesignationCode: 1
        }
      ],
      Messages: ["There is an existing record, Work code(s) 'TUN000001018S', matching the criteria you have tried to enter for song title: TOO MANY COOKS"]
    }.to_json
  end

  describe "#initialize" do
    it "parses the raw json response" do
      response_dbl = double(:json_response, body: json_response)
      response = RightsApp::Responses::Work.new(response_dbl)

      expect(response.body).to eq JSON.parse(json_response)
    end

    it "gets the etag from the response headers" do
      e_tag = "0406536f-e34b-4dce-9c06-d6f8ab8c3c97"
      response_dbl = double(:json_response, body: json_response, :headers => { "etag" => e_tag })
      response = RightsApp::Responses::Work.new(response_dbl)

      expect(response.e_tag).to eq e_tag
    end
  end

  describe "#already_exists?" do
    it "returns true if messages include the 'existing record' error message" do
      response_dbl = double(:json_response, body: json_response)
      response     = RightsApp::Responses::Work.new(response_dbl)

      expect(response.already_exists?).to be_truthy
    end
  end

  describe "#existing_work_code" do
    it "parses the 'existing record' error for the existing work_code" do
      response_dbl = double(:json_response, body: json_response)
      response     = RightsApp::Responses::Work.new(response_dbl)

      expect(response.existing_work_code).to eq "TUN000001018S"
    end

    it "returns the response body's work code if the 'existing record' error is not present" do
      res = JSON.parse(json_response).merge(Messages: [])
      response_dbl = double(:json_response, body: res.to_json)
      response     = RightsApp::Responses::Work.new(response_dbl)

      expect(response.existing_work_code).to eq work_code
    end
  end

  describe "#composer_splits" do
    it "returns writers who have a RightToCollect" do
      response_dbl = double(:json_response, body: json_response)
      response     = RightsApp::Responses::Work.new(response_dbl)

      expect(response.composer_splits.count).to eq 1
    end
  end

  describe "#cowriter_splits" do
    it "returns writers who do not have a RightToCollect" do
      response_dbl = double(:json_response, body: json_response)
      response     = RightsApp::Responses::Work.new(response_dbl)

      expect(response.cowriter_splits.count).to eq 1
    end
  end
end
