require "rails_helper"

describe RightsApp::Responses::Recording do
  let(:recording_id)    { "39e92091-f0b9-ac83-9f1c-bb23695b4cde" }
  let(:album)           { create(:album) }
  let(:composition)     { create(:composition, provider_identifier: "TCM0000000104S") }
  let(:song)            { create(:song, album: album, composition_id: composition.id) }
  let(:json_response) do
    { RecordingId: recording_id,
      WorkCode: composition.provider_identifier,
      Title: song.name,
      PerformingArtist: song.artist.name,
      Isrc: song.isrc,
      RecordLabel: album.label.name,
      ReleaseDate: album.orig_release_date,
      IsDeleted: false,
      IsLocked: false,
      ChangesPending: false,
      Messages: []
    }.to_json
  end

  describe "#initialize" do
    it "sets the body and json_response" do
      response_dbl = double(:json_resposne, body: json_response)
      response = RightsApp::Responses::Recording.new(response_dbl)

      expect(response.body).to eq JSON.parse(json_response)
      expect(response.recording_id).to eq recording_id
    end
  end
end
