require "rails_helper"

describe Royalties::ApiClient do
  describe "#finalize" do
    posting_ids = [1, 2, 3]

    it "send a post request" do
      allow(ENV).to receive(:fetch).with("SIP_BASE_URL").and_return("XXX")
      allow(ENV).to receive(:fetch).with("SIP_API_KEY").and_return("KEY")

      headers_double = double("headers")
      response_double = double("response", body: "", success?: true)
      client = double("faraday", post: response_double)
      expect(headers_double).to receive(:[]=).with("Content-Type", "application/json")
      allow(Faraday).to receive(:new).and_return(client)
      allow(client).to receive(:headers).and_return(headers_double)
      expect(client).to receive(:post).with("XXX/api/tcwww/posting_complete", {api_key:'KEY', posting_ids:[1,2,3]}.to_json)

      Royalties::ApiClient.new.finalize(posting_ids)
    end
  end
end
