require 'rails_helper'

module Quaderno
  describe ApiClient do
    let (:http_client) { double(:http_client) }

    describe 'CalculateTax' do
      let (:calculate_tax_response) {
        {
          name: "Sales tax",
          rate: 9.5,
          extra_name: nil,
          extra_rate: nil,
          country:"US",
          region:"CA",
          county:"SAN MATEO",
          city:"BURLINGAME",
          county_tax_code:"41",
          city_tax_code:"746",
          transaction_type:"eservice",
          notes: nil
        }
      }

      before do
        allow(http_client).to receive(:get).and_return(:calculate_tax_response)
      end

      it 'should return tax rate from quaderno api' do
        subject.send_request(:calculate_tax, country: 'US')
      end

      it 'should raise MissingCountryError when missing country' do
        expect {
          subject.send_request(:calculate_tax, postal_code: '11201')
        }.to raise_error(Requests::CountryMissingError)
      end
    end
  end
end
