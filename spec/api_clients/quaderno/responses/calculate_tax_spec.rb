require 'rails_helper'

module Quaderno::Responses
  describe CalculateTax do
    let(:raw_response) {
      raw_response = double(:raw_response)
      allow(raw_response).to receive(:body).and_return({
        name: 'Sales Tax',
        rate: 9.5,
      }.to_json)
      raw_response
    }

    subject { CalculateTax.new(raw_response: raw_response) }

    it 'should provide the correct tax rate name' do
      expect(subject.name).to eq('Sales Tax')
    end

    it 'should provide the correct tax rate' do
      expect(subject.rate).to eq(9.5)
    end
  end
end
