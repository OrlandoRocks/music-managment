require 'rails_helper'

module Quaderno::Requests
  describe CalculateTax do
    subject { CalculateTax.new({country: 'US'}) }

    it 'should use the correct url' do
      expect(subject.url).to eq('/api/taxes/calculate.json')
    end

    it 'should use get request method' do
      expect(subject.method).to eq(:get)
    end

    it 'should the correct params' do
      expect(subject.params).to eq({country: 'US'})
    end

    it 'should have the right response class' do
      expect(subject.response_class).to eq(Quaderno::Responses::CalculateTax)
    end

    context 'without country' do
      it 'should raise CountryMissingError when missing country' do
        expect {
          CalculateTax.new({postal_code: '11201'})
        }.to raise_error(CountryMissingError)
      end
    end
  end
end
