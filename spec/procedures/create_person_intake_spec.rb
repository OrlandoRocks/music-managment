require "rails_helper"

def execute_intake
  sleep(1) # Needed to make sure tmsp will change during test run
  ActiveRecord::Base.connection.execute("call create_person_intake")
  ActiveRecord::Base.clear_active_connections!
end

# NOTE: These specs will not be run by default
# To run them, include the 'procedures' tag in your rspec call
# bundle exec rspec --tag procedures spec/procedures/create_person_intake_spec.rb
describe "create_person_intake", :procedures do

  before(:each) do
    StoredProcedureLoader.load
  end

  around(:each) do |block|
    self.use_transactional_fixtures = false
    block.run
    self.use_transactional_fixtures = true
  end

  before(:each) do
    EncumbranceOption.where(reference_source: "LYRIC", option_name: "WITHHOLDING_MAX_PERCENTAGE").first.update(option_value: "80")
    @person           = FactoryBot.create(:person)
    @album            = FactoryBot.create(:album, :live, person_id: @person.id, primary_genre: Genre.first, label: Label.first, name: Artist.first.name)
    @original_balance = @person.balance
    @srm              = FactoryBot.create(:sales_record_master, status: "new", sip_store_id: SipStore.first.id)
    @sr1              = FactoryBot.create(:sales_record, sales_record_master: @srm, amount: BigDecimal.new("300.44556677889900"), person: @person, related: @person.albums.last)
    @sr2              = FactoryBot.create(:sales_record, sales_record_master: @srm, amount: BigDecimal.new("325.33445566000000"), person: @person, related: @person.albums.last)
  end

  it "fully posts an intake when no encumbrance exists" do
    execute_intake

    @person.reload
    expect(@person.person_balance.balance).to eq (@original_balance + BigDecimal.new("625.780022438899"))
    expect(@person.person_intakes.length).to eq 1
    expect(@person.person_intakes.first.amount).to eq BigDecimal.new("625.780022438899")
    expect(@person.person_transactions.length).to eq 1

    intake_txns = @person.person_transactions.where(target_type: "PersonIntake")
    expect(intake_txns.length).to eq 1
    expect(intake_txns.first.credit).to eq BigDecimal.new("625.780022438899")

  end

  it "withholds the amount of an outstanding encumbrance when the max allowed percent of the posting is greater than the outstanding amount" do
    summary = FactoryBot.create(:encumbrance_summary, person: @person, total_amount: 200.50)
    original_updated_at = summary.updated_at
    execute_intake

    @person.reload
    expect(@person.person_balance.balance).to eq(@original_balance + BigDecimal.new("425.280022438899"))
    expect(@person.person_intakes.length).to eq(1)
    expect(@person.person_intakes.first.amount).to eq BigDecimal.new("625.780022438899")
    expect(@person.person_transactions.length).to eq(2)

    intake_txns = @person.person_transactions.where(target_type: "PersonIntake")
    expect(intake_txns.length).to eq(1)
    expect(intake_txns.first.credit).to eq BigDecimal.new("625.780022438899")

    encumbrance_txns = @person.person_transactions.where(target_type: "EncumbranceDetail")
    expect(encumbrance_txns.length).to eq(1)
    expect(encumbrance_txns.first.debit).to eq(200.50)

    summary.reload
    expect(summary.outstanding_amount).to eq(0)
    expect(summary.encumbrance_details.map(&:transaction_amount).sum).to eq(0)
    expect(original_updated_at).to be < summary.updated_at

    summary.encumbrance_details.each { |ed| ed.destroy }
    summary.destroy
  end

  it "withholds the max allowed percent of the posting when the encumbrance is greater than the posting" do
    full_encumbrance       = 800.45
    full_intake            = BigDecimal.new("625.780022438899")
    intake_remainder       = BigDecimal.new("0.000022438899")
    encumbrance_percentage = (full_intake * 0.8).round(2)
    left_over              = (full_intake * 0.2).round(2)

    summary = FactoryBot.create(:encumbrance_summary, person: @person, total_amount: full_encumbrance)
    original_updated_at = summary.updated_at
    execute_intake

    @person.reload
    expect(@person.person_balance.balance).to eq(@original_balance + left_over + intake_remainder)
    expect(@person.person_intakes.length).to eq(1)
    expect(@person.person_intakes.first.amount).to eq(full_intake)
    expect(@person.person_transactions.length).to eq(2)

    intake_txns = @person.person_transactions.where(target_type: "PersonIntake")
    expect(intake_txns.length).to eq(1)
    expect(intake_txns.first.credit).to eq(full_intake)

    encumbrance_txns = @person.person_transactions.where(target_type: "EncumbranceDetail")
    expect(encumbrance_txns.length).to eq(1)
    expect(encumbrance_txns.first.debit).to eq(encumbrance_percentage)

    summary.reload
    expect(summary.outstanding_amount).to eq(800.45 - encumbrance_percentage)
    expect(summary.encumbrance_details.map(&:transaction_amount).sum).to eq(summary.outstanding_amount)
    expect(original_updated_at).to be < summary.updated_at

    summary.encumbrance_details.each { |ed| ed.destroy }
    summary.destroy
  end

  it "does not withhold anything for negative postings" do
    summary = FactoryBot.create(:encumbrance_summary, person: @person, total_amount: 500)
    @sr2.update(amount: -400)

    execute_intake

    @person.reload
    expect(@person.person_balance.balance).to eq(@original_balance - BigDecimal.new("99.554433221101"))
    expect(@person.person_intakes.length).to eq(1)
    expect(@person.person_intakes.first.amount).to eq(BigDecimal.new("-99.554433221101"))
    expect(@person.person_transactions.length).to eq(1)

    intake_txns = @person.person_transactions.where(target_type: "PersonIntake")
    expect(intake_txns.length).to eq(1)
    expect(intake_txns.first.debit).to eq(BigDecimal.new("99.554433221101"))

    encumbrance_txns = @person.person_transactions.where(target_type: "EncumbranceDetail")
    expect(encumbrance_txns.length).to eq(0)

    summary.reload
    expect(summary.outstanding_amount).to eq(500)
  end

  it "pays off multiple encumbrances when the posting is large enough" do
    full_encumbrance       = 401.11
    full_intake            = BigDecimal.new("625.780022438899")
    intake_remainder       = full_intake - full_encumbrance

    summary1 = FactoryBot.create(:encumbrance_summary, person: @person, total_amount: 200.24)
    summary2 = FactoryBot.create(:encumbrance_summary, person: @person, total_amount: 200.87)

    execute_intake

    @person.reload
    expect(@person.person_balance.balance).to eq(@original_balance + intake_remainder)
    expect(@person.person_intakes.length).to eq(1)
    expect(@person.person_intakes.first.amount).to eq(full_intake)
    expect(@person.person_transactions.length).to eq(3)

    intake_txns = @person.person_transactions.where(target_type: "PersonIntake")
    expect(intake_txns.length).to eq(1)
    expect(intake_txns.first.credit).to eq(full_intake)

    encumbrance_txns = @person.person_transactions.where(target_type: "EncumbranceDetail")
    expect(encumbrance_txns.length).to eq(2)
    expect(encumbrance_txns.first.debit + encumbrance_txns.last.debit).to eq full_encumbrance

    summary1.reload
    expect(summary1.outstanding_amount).to eq(0)
    expect(summary1.encumbrance_details.map(&:transaction_amount).sum).to eq(summary1.outstanding_amount)

    summary2.reload
    expect(summary2.outstanding_amount).to eq(0)
    expect(summary2.encumbrance_details.map(&:transaction_amount).sum).to eq(summary2.outstanding_amount)

    summary1.encumbrance_details.each { |ed| ed.destroy }
    summary1.destroy

    summary2.encumbrance_details.each { |ed| ed.destroy }
    summary2.destroy
  end

  it "pays off one encumbrance and pays down the second when the posting is not large enough" do
    encumb1                = 400.42
    encumb2                = 200.53
    full_intake            = BigDecimal.new("625.780022438899")
    summary1               = FactoryBot.create(:encumbrance_summary, person: @person, total_amount: encumb1)
    summary2               = FactoryBot.create(:encumbrance_summary, person: @person, total_amount: encumb2)

    execute_intake
    @person.reload
    expect(@person.person_balance.balance).to eq(@original_balance + 125.160022438899)
    expect(@person.person_intakes.length).to eq(1)
    expect(@person.person_intakes.first.amount).to eq(full_intake)
    expect(@person.person_transactions.length).to eq(3)

    intake_txns = @person.person_transactions.where(target_type: "PersonIntake")
    expect(intake_txns.length).to eq(1)
    expect(intake_txns.first.credit).to eq(full_intake)

    encumbrance_txns = @person.person_transactions.where(target_type: "EncumbranceDetail").order(:id)
    expect(encumbrance_txns.length).to eq(2)
    # The magic numbers are because of the 80% max withold percent specified at the first before(:each)
    expect(encumbrance_txns.first.debit + encumbrance_txns.last.debit).to eq 400.42 + 100.20

    # The first debit, the person's balance is the entire intake
    expect(encumbrance_txns.first.previous_balance).to eq(full_intake)
    expect(encumbrance_txns.second.previous_balance).to eq(full_intake - encumb1)

    summary1.reload
    expect(summary1.outstanding_amount).to eq(0)
    expect(summary1.encumbrance_details.map(&:transaction_amount).sum).to eq(summary1.outstanding_amount)

    summary2.reload
    expect(summary2.outstanding_amount).to eq(100.33)
    expect(summary2.encumbrance_details.map(&:transaction_amount).sum).to eq(summary2.outstanding_amount)

    summary1.encumbrance_details.each { |ed| ed.destroy }
    summary1.destroy

    summary2.encumbrance_details.each { |ed| ed.destroy }
    summary2.destroy
  end

  after(:each) do
    @sr1.destroy
    @sr2.destroy
    @srm.destroy
    @album.creatives.each { |c| c.destroy }
    @album.upc.destroy
    @album.artist.destroy
    @album.destroy
    @person.person_transactions.each { |t| t.destroy }
    @person.person_intakes.each { |i| i.destroy }
    @person.person_balance.destroy
    @person.destroy
    ActiveRecord::Base.connection.execute("DELETE FROM person_intake_sales_record_masters WHERE sales_record_master_id = #{@srm.id}")
    EncumbranceOption.where(reference_source: "LYRIC", option_name: "WITHHOLDING_MAX_PERCENTAGE").first.update(option_value: "100")
  end
end
