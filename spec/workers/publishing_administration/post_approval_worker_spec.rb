require "rails_helper"

describe PublishingAdministration::PostApprovalWorker do
  let(:person) { create(:person, account: create(:account)) }
  let(:album) { create(:album, :paid, :approved, :with_uploaded_songs, person: person) }

  context "when publishing_changeover is enabled" do
    before(:each) do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
      allow(FeatureFlipper).to receive(:show_feature?).with(:publishing_changeover, album.person).and_return(true)

      PublishingAdministration::PostApprovalWorker.new.perform(album.id)
    end

    context "when an album with no existing songs is submitted" do

      it "creates publishing_compositions for an album's songs" do
        expect(PublishingComposition.where(id: album.songs.map(&:publishing_composition_id)).count).to eq(album.songs.count)
      end

      it "does not create duplicate publishing_compositions for the same songs" do
        expect{
          PublishingAdministration::PostApprovalWorker.new.perform(album.id)
        }.not_to change(PublishingComposition, :count)
      end

      it "does not raise an error when duplicate inserts for the same song are attempted" do
        expect{
          PublishingAdministration::PostApprovalWorker.new.perform(album.id)
        }.not_to raise_error
      end
    end
  end
end
