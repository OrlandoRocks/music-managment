require "rails_helper"

describe PublishingAdministration::IngestionBlockerWorker do
  describe "#perform" do
    before do
      $rights_app_limiter = Sidekiq::Limiter.unlimited
    end

    context "when the account does NOT have multiple writers with the right to collect" do
      it "imports data from rights app" do
        account = create(:account)
        create(:composer, :with_pub_admin_purchase, account: account, person: account.person)

        allow(Account).to receive(:find).and_return(account)
        allow(PublishingAdministration::IngestionBlocker)
          .to receive(:multiple_writers_with_right_to_collect?)
          .with(account.provider_account_id)
          .and_return(false)

        expect(account).to receive(:import_from_rights_app)

        PublishingAdministration::IngestionBlockerWorker.new.perform(account.id)
      end
    end

    context "when the account is added to admin features" do
      it "imports data from rights app" do
        account = create(:account)
        create(:composer, :with_pub_admin_purchase, account: account, person: account.person)

        allow(Account).to receive(:find).and_return(account)
        allow(FeatureFlipper).to receive(:show_feature?).with(:import_data_from_rightsapp, account.person).and_return(true)
        allow(PublishingAdministration::IngestionBlocker)
          .to receive(:multiple_writers_with_right_to_collect?)
          .with(account.provider_account_id)
          .and_return(false)

        expect(account).to receive(:import_from_rights_app)

        PublishingAdministration::IngestionBlockerWorker.new.perform(account.id)
      end
    end

    context "when the account has multiple writers with the right to collect" do
      it "does not import data from rights app" do
        account = create(:account)
        create(:composer, :with_pub_admin_purchase, account: account, person: account.person)

        allow(Account).to receive(:find).and_return(account)
        allow(PublishingAdministration::IngestionBlocker)
          .to receive(:multiple_writers_with_right_to_collect?)
          .with(account.provider_account_id)
          .and_return(true)

        expect(account).not_to receive(:import_from_rights_app)

        PublishingAdministration::IngestionBlockerWorker.new.perform(account.id)
      end
    end

    context "when the account is not added to admin features" do
      it "does not import data from rights app" do
        account = create(:account)
        create(:composer, :with_pub_admin_purchase, account: account, person: account.person)

        allow(Account).to receive(:find).and_return(account)
        allow(FeatureFlipper).to receive(:show_feature?).with(:import_data_from_rightsapp, account.person).and_return(false)
        allow(PublishingAdministration::IngestionBlocker)
          .to receive(:multiple_writers_with_right_to_collect?)
          .with(account.provider_account_id)
          .and_return(true)

        expect(account).not_to receive(:import_from_rights_app)

        PublishingAdministration::IngestionBlockerWorker.new.perform(account.id)
      end
    end
  end
end
