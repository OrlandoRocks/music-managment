require "rails_helper"

describe "Ytsr::EmailWorker" do
  describe ".perform" do
    let(:person_id)           { 1 }
    let(:template_name)       { "template_name" }
    let(:message_data)        { [{ "song_id" => "12345" }] }
    let(:person_notifier_dbl) { double() }


    it "should call the Person Notifier with the correct arguments", run_sidekiq_worker: true do
      expect(PersonNotifier).to receive(:youtube_sound_recording_update)
        .with(person_id, template_name, message_data).and_return(person_notifier_dbl)
      expect(person_notifier_dbl).to receive(:deliver)

      Ytsr::EmailWorker.perform_async(person_id, template_name, message_data)
    end
  end
end
