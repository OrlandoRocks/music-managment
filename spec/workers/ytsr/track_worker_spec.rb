require "rails_helper"

describe Ytsr::TrackWorker do
  describe "#perform" do
    let(:album_id) { 1}

    it "should add a job to the queue" do
      expect { Ytsr::TrackWorker.perform_async(album_id) }.to change(Ytsr::TrackWorker.jobs, :size).by(1)
    end

    it "should invoke Ytsr::Track", run_sidekiq_worker: true do
      expect(Ytsr::Track).to receive(:create).with(album_id)
      Ytsr::TrackWorker.perform_async(album_id)
    end
  end
end
