require "rails_helper"

describe "Ytsr::SalepointWorker" do
  describe "#perform" do
    let(:person_id) { 1 }

    it "should add a job to the queue" do
      expect { Ytsr::SalepointWorker.perform_async(person_id) }.to change(Ytsr::SalepointWorker.jobs, :size).by(1)
    end

    it "should invoke Ytsr::Salepoint", run_sidekiq_worker: true do
      expect(Ytsr::Salepoint).to receive(:create).with(person_id)
      Ytsr::SalepointWorker.perform_async(person_id)
    end
  end
end
