require 'rails_helper'

describe Braintree::AccountUpdaterWorker do

  it "should process the csv file after downloading" do
    csv_file_url = "http://braintree.com/test"
    allow_any_instance_of(Braintree::AccountUpdaterWorker).to receive_message_chain(:open, :read)
                         .and_return('''payment_method_token,update_type,new_last_4,new_expiration
23423sadf,account_updater_daily_report,3456,Sep-19''')
    expect_any_instance_of(Braintree::AccountUpdaterService).to receive(:process).once

    Braintree::AccountUpdaterWorker.new.perform(csv_file_url)
  end
end