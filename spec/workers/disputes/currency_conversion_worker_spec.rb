require "rails_helper"
require "sidekiq/testing"
Sidekiq::Testing.fake!

RSpec.describe Disputes::CurrencyConversionWorker, type: :worker do
  describe "testing worker" do
    it "Job will be enqueued in the critcal queue" do
      described_class.perform_async(1)
      assert_equal :critical, described_class.queue
    end
    it "goes into the jobs array for testing environment" do
      expect { described_class.perform_async(1) }
        .to change(described_class.jobs, :size)
        .by(1)
    end
  end
end
