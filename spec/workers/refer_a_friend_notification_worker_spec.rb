require "rails_helper"

describe ReferAFriendNotificationWorker do
  let(:invoice) { FactoryBot.create(:invoice) }
  describe "#perform" do
    it "calls on the ReferAFriendNotification service" do
      allow(ReferAFriendNotification).to receive(:send_notification).with(invoice.id, true)
      ReferAFriendNotificationWorker.new.perform(invoice.id, true)
    end
  end
end
