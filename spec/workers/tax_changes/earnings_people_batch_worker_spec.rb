# frozen_string_literal: true

require "rails_helper"

describe TaxChanges::EarningsPeopleBatchWorker do
  let!(:person) { create(:person) }
  
  describe "#batch_of_people" do
    context "with people from the US and have lifetime earnings" do
      context "without active payout providers, with or without tax forms" do
        let!(:person_with_inactive_payout_provider) do
          create(:person, :with_inactive_payout_provider)
        end
        let!(:person_with_inactive_payout_provider_and_lifetime_earnings) do
          create(:person, :with_inactive_payout_provider, :with_lifetime_earnings)
        end
        let!(:person_without_payout_providers_and_zero_lifetime_earnings) do
          create(:person, :with_payout_provider)
        end

        it "finds people fulfilling all criteria" do
          results = described_class.new.batch_of_people

          expect(results).to include(
            person_with_inactive_payout_provider_and_lifetime_earnings.id
          )
          expect(results).not_to include(
            person.id,
            person_with_inactive_payout_provider.id,
            person_without_payout_providers_and_zero_lifetime_earnings.id,
          )
        end
      end

      context "with active payout providers and no tax forms" do
        let!(:person_with_active_payout_provider) do
          create(:person, :with_payout_provider) 
        end
        let!(:person_with_active_payout_provider_and_zero_lifetime_earnings) do
          create(:person, :with_payout_provider, :with_zero_lifetime_earnings)
        end
        let!(:person_with_active_payout_provider_and_zero_lifetime_earnings_and_tax_form) do
          create(:person, :with_payout_provider, :with_zero_lifetime_earnings, :with_tax_form)
        end
        let!(:person_with_active_payout_provider_and_lifetime_earnings) do
          create(:person, :with_payout_provider, :with_lifetime_earnings)
        end
        let!(:person_with_active_payout_provider_and_lifetime_earnings_and_tax_form) do
          create(:person, :with_payout_provider, :with_lifetime_earnings, :with_tax_form)
        end

        it "finds people fulfilling all criteria" do
          results = described_class.new.batch_of_people

          expect(results).to include(
            person_with_active_payout_provider_and_lifetime_earnings.id,
          )
          expect(results).not_to include(
            person.id,
            person_with_active_payout_provider.id,
            person_with_active_payout_provider_and_zero_lifetime_earnings.id,
            person_with_active_payout_provider_and_lifetime_earnings_and_tax_form.id,
            person_with_active_payout_provider_and_zero_lifetime_earnings_and_tax_form.id
          )
        end
      end
    end

    context "with people outside of the US" do
      let!(:non_us_person) { create(:person, country: "CA") }
      let!(:non_us_person_with_inactive_payout_provider) do
        create(:person, :with_inactive_payout_provider, country: "CA")
      end
      let!(:non_us_person_with_inactive_payout_provider_and_lifetime_earnings) do
        create(:person, :with_inactive_payout_provider, :with_lifetime_earnings, country: "CA")
      end
      let!(:non_us_person_without_payout_providers_and_zero_lifetime_earnings) do
        create(:person, :with_payout_provider, country: "CA")
      end
      let!(:non_us_person_with_active_payout_provider) do
        create(:person, :with_payout_provider, country: "CA") 
      end
      let!(:non_us_person_with_active_payout_provider_and_zero_lifetime_earnings) do
        create(:person, :with_payout_provider, :with_zero_lifetime_earnings, country: "CA")
      end
      let!(:non_us_person_with_active_payout_provider_and_zero_lifetime_earnings_and_tax_form) do
        create(:person, :with_payout_provider, :with_zero_lifetime_earnings, :with_tax_form, country: "CA")
      end
      let!(:non_us_person_with_active_payout_provider_and_lifetime_earnings) do
        create(:person, :with_payout_provider, :with_lifetime_earnings, country: "CA")
      end
      let!(:non_us_person_with_active_payout_provider_and_lifetime_earnings_and_tax_form) do
        create(:person, :with_payout_provider, :with_lifetime_earnings, :with_tax_form, country: "CA")
      end

      it "does not match any non-US people" do
        results = described_class.new.batch_of_people

        expect(results).not_to include(
          non_us_person.id,
          non_us_person_with_inactive_payout_provider.id,
          non_us_person_with_inactive_payout_provider_and_lifetime_earnings.id,
          non_us_person_without_payout_providers_and_zero_lifetime_earnings.id,
          non_us_person_with_active_payout_provider.id,
          non_us_person_with_active_payout_provider_and_zero_lifetime_earnings.id,
          non_us_person_with_active_payout_provider_and_zero_lifetime_earnings_and_tax_form.id,
          non_us_person_with_active_payout_provider_and_lifetime_earnings.id,
          non_us_person_with_active_payout_provider_and_lifetime_earnings_and_tax_form.id
        )
      end
    end
  end
end
