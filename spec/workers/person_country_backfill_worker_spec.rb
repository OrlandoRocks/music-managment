# frozen_string_literal: true
require 'rails_helper'

RSpec.describe PersonCountryBackfillWorker, type: :worker do
  describe "#perform" do

    it "calls the Person::CountryBackfillService.backfill_all" do
      allow(Person::CountryBackfillService).to receive(:perform_backfill).with('all')
      expect(Person::CountryBackfillService).to receive(:perform_backfill).with('all')
      PersonCountryBackfillWorker.new.perform('backfill_all')
    end

    it "calls Person::CountryBackfillService.backfill_country_audits" do
      allow(Person::CountryBackfillService).to receive(:perform_backfill).with('country_audit')
      expect(Person::CountryBackfillService).to receive(:perform_backfill).with('country_audit')
      PersonCountryBackfillWorker.new.perform('backfill_country_audit')
    end

    it "calls Person::CountryBackfillService.backfill_payoneer_kyc" do
      allow(Person::CountryBackfillService).to receive(:perform_backfill).with('payoneer_kyc')
      expect(Person::CountryBackfillService).to receive(:perform_backfill).with('payoneer_kyc')
      PersonCountryBackfillWorker.new.perform('backfill_payoneer_kyc')
    end

    it "calls Person::CountryBackfillService.backfill_invalid" do
      allow(Person::CountryBackfillService).to receive(:perform_backfill).with('invalid')
      expect(Person::CountryBackfillService).to receive(:perform_backfill).with('invalid')
      PersonCountryBackfillWorker.new.perform('backfill_invalid')
    end
  end
end
