require "rails_helper"

describe CurrencyLayer::GetForeignExchangeRatesWorker do
  describe "#perform" do
    it "stores forex rates for EUR" do
      exchange_service = instance_double("Currencylayer::ExchangeService", store_exchange_rates: {})
      expect(Currencylayer::ExchangeService).to receive(:new).and_return(exchange_service)
      CountryWebsite::VAT_CURRENCIES.each do |source_currency|
        expect(exchange_service)
          .to receive(:store_exchange_rates)
          .ordered
          .with(source_currency, CountryWebsite.find(CountryWebsite::FRANCE).currency)
      end
      CurrencyLayer::GetForeignExchangeRatesWorker.new.perform
    end

    it "stores forex rates for USD" do
      exchange_service = instance_double("Currencylayer::ExchangeService", store_exchange_rates: {})
      expect(Currencylayer::ExchangeService).to receive(:new).and_return(exchange_service)
      CountryWebsite.select(:currency).distinct.pluck(:currency).excluding("USD").each do |source_currency|
        expect(exchange_service)
          .to receive(:store_exchange_rates)
          .ordered
          .with(source_currency, CountryWebsite.find(CountryWebsite::UNITED_STATES).currency)
      end
      CurrencyLayer::GetForeignExchangeRatesWorker.new.perform
    end

    it "handles failures" do
      expect(Currencylayer::ExchangeService).to receive(:new).and_raise("boom")
      expect(Rails.logger)
        .to receive(:error)
        .with("Get latest forex rates rake task failed boom")

      expect(Tunecore::Airbrake)
        .to receive(:notify)
        .with("Unable to get latest forex rates", an_instance_of(RuntimeError))
      CurrencyLayer::GetForeignExchangeRatesWorker.new.perform
    end
  end
end
