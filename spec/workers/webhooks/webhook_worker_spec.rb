require "rails_helper"

describe Webhooks::WebhookWorker do
  let(:any_webhook_service_double) { double }

  before(:each) do
    allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
    allow(any_webhook_service_double).to receive(:process)
    allow(Sift::WebhookService).to receive(:new).with({}).and_return(any_webhook_service_double)
  end

  describe "#perform" do
    it "should hit base case error" do
      expect(Tunecore::Airbrake).to receive(:notify)
      expect(any_webhook_service_double).to_not receive(:process)

      Webhooks::WebhookWorker.new.perform("", {})
    end
    it "should call WebhookService#process" do
      expect(Tunecore::Airbrake).to_not receive(:notify)
      expect(any_webhook_service_double).to receive(:process)

      Webhooks::WebhookWorker.new.perform("sift", {})
    end
  end
end
