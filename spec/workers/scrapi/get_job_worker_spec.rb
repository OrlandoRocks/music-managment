require 'rails_helper'

module Scrapi
  describe GetJobWorker do
    let(:song) { create(:song) }
    let(:api_client) { ApiClient.new('', '') }

    before :each do
      allow(ApiClient).to receive(:new).and_return(api_client)
      allow(api_client).to receive(:get_access_token)
    end

    after :each do
      subject.perform('createjobid')
    end

    context 'job is waiting' do
      let(:resp_body) { <<-EOF
        {"status":{"code":200,"message":"OK","description":"","hostname":"blv-th3-apibk-6013.blvs.io","details":[]},"data":{"id":"createjobid", "uploadType":"aws_tunecore","uploadParams":"bigbox.tunecore.com:2508274/1561164431-song.mp3","status":"waiting_file"}}
      EOF
      }
      let(:scrapi_job) { Job.from_content(resp_body) }

      it 'should get job' do
        expect(api_client).to receive(:get_job).with('createjobid').and_return(scrapi_job)
      end

      it 'should save to database' do
        allow(api_client).to receive(:get_job).and_return(scrapi_job)
        expect(scrapi_job).to receive(:to_model)
      end

    end

    context 'job is done' do
      let(:resp_body) { <<-EOF
        {"status":{"code":200,"message":"OK","description":"","hostname":"blv-th3-apibk-6013.blvs.io","details":[]},"data":{"id":"createjobid", "uploadType":"aws_tunecore","uploadParams":"bigbox.tunecore.com:2508274/1561164431-song.mp3","status":"done"}}
      EOF
      }
      let(:scrapi_job) { Job.from_content(resp_body) }
      let(:response) { double }

      before :each do
        allow(response).to receive(:success?).and_return(true)
        allow(response).to receive(:body).and_return(resp_body)
        allow(api_client.scrapi_client).to receive(:get).and_return(response)
        allow(Job).to receive(:from_id).and_return(scrapi_job)
      end

      it 'should save job to database' do
        expect(scrapi_job).to receive(:to_model)
      end

      it 'should save job to redis' do
        expect(scrapi_job).to receive(:save)
      end
    end

    context 'job errored' do
      let(:resp_body) { <<-EOF
        {"status":{"code":200,"message":"OK","description":"","hostname":"blv-th3-apibk-6013.blvs.io","details":[]},"data":{"id":"createjobid", "uploadType":"aws_tunecore","uploadParams":"bigbox.tunecore.com:2508274/1561164431-song.mp3","status":"error"}}
      EOF
      }
      let(:scrapi_job) { Job.from_content(resp_body) }
      let(:response) { double }

      before :each do
        allow(response).to receive(:success?).and_return(true)
        allow(response).to receive(:body).and_return(resp_body)
        allow(api_client.scrapi_client).to receive(:get).and_return(response)
        allow(Job).to receive(:from_id).and_return(scrapi_job)
      end

      it 'should save job to database' do
        expect(scrapi_job).to receive(:to_model)
      end

      it 'should save job to redis' do
        expect(scrapi_job).to receive(:save)
      end

      it 'should fail job' do
        allow(scrapi_job).to receive(:transitioned_to_error?).and_return(true)
        expect(scrapi_job).to receive(:error!)
      end
    end
  end
end
