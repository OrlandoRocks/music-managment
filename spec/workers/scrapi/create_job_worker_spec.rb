require 'rails_helper'

module Scrapi
  describe CreateJobWorker do
    let(:song) { create(:song) }
    let(:api_client) { ApiClient.new('', '') }
    let(:resp_body) { <<-EOF
        {"status":{"code":200,"message":"OK","description":"","hostname":"blv-th3-apibk-6013.blvs.io","details":[]},"data":{"id":"createjobid", "uploadType":"aws_tunecore","uploadParams":"bigbox.tunecore.com:2508274/1561164431-song.mp3","status":"waiting_file"}}
    EOF
    }
    let(:scrapi_job) { Job.from_content(resp_body) }

    before :each do
      allow(ApiClient).to receive(:new).and_return(api_client)
      allow(api_client).to receive(:get_access_token)
    end

    context 'creation successful' do
      let(:response) { double }

      before :each do
        allow(response).to receive(:success?).and_return(true)
        allow(response).to receive(:body).and_return(resp_body)
        allow(api_client.scrapi_client).to receive(:post).and_return(response)
        allow_any_instance_of(Requests::CreateJob).to receive(:s3_path)
      end

      after :each do
        subject.perform(song.id)
      end

      it 'should create job' do
        expect(api_client).to receive(:create_job).with(song).and_return(scrapi_job)
      end

      it 'should save job' do
        expect(Job).to receive(:from_content).with(resp_body).and_return(scrapi_job)
        expect(scrapi_job).to receive(:song_id=).with(song.id)
        expect(scrapi_job).to receive(:save)
      end

      it 'should save job to database' do
        allow(api_client).to receive(:create_job).and_return(scrapi_job)
        expect(scrapi_job).to receive(:to_model)
      end
    end

    context 'creation failure' do
      let(:response) { double }

      before :each do
        allow(response).to receive(:success?).and_return(false)
        allow(response).to receive(:body).and_return(resp_body)
        allow(api_client.scrapi_client).to receive(:post).and_return(response)
        allow_any_instance_of(Requests::CreateJob).to receive(:s3_path)
      end

      it 'should raise ApiError' do
        expect {
          subject.perform(song.id)
        }.to raise_error(RuntimeError)
      end
    end
  end
end
