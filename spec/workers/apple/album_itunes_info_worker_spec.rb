require "rails_helper"

describe Apple::AlbumItunesInfoWorker do
  let(:album) { create(:album) }
  describe ".perform", run_sidekiq_worker: true do
    let(:limiter) { double(within_limit: true) }
    before(:each) do
      allow_any_instance_of(Believe::ApiClient).to receive(:post).and_return(true)
      allow_any_instance_of(ApiLogger::InitLoggerService).to receive(:call).and_return(true)
      allow(Apple::Album).to receive(:get_itunes_info).with(album.id)
      stub_const("Apple::AlbumItunesInfoWorker::TRANSPORTER_RATE_LIMITER", limiter)
    end
    it "calls the rate limiter" do
      expect(limiter).to receive(:within_limit)
      Apple::AlbumItunesInfoWorker.perform_async(album.id)
    end
  end

  describe ".handle_failure" do
    let(:msg) { {"args" => [album.id]} }
    it "updates the album itunes status record with the failure state" do
      expect(Apple::Album).to receive(:create_or_update_itunes_status_failure).with(album.id)
      Apple::AlbumItunesInfoWorker.handle_failure(msg)
    end
  end
end
