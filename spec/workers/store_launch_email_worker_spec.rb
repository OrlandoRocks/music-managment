require "rails_helper"

describe StoreLaunchEmailWorker do
  let(:store) { create(:store) }
  let(:person)   { create(:person) }
  let(:album)    { create(:album, person: person) }
  let(:releases) { [album.id] }

  describe "#perform" do
    it "calls on the StoreLauncherEmail service" do
      allow(StoreLaunchEmail).to receive(:send_store_launch_email).with(store.id, person.id, releases).and_return(true)
      expect(StoreLaunchEmail).to receive(:send_store_launch_email).with(store.id, person.id, releases)
      StoreLaunchEmailWorker.new.perform(store.id, person.id, releases)
    end
  end
end
