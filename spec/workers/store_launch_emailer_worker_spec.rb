require "rails_helper"

describe BulkStoreLaunchEmailWorker do
  let(:store) { FactoryBot.create(:store) }
  describe "#perform" do
    it "calls on the StoreLauncherEmailer service" do
      allow(BulkStoreLaunchEmail).to receive(:send_store_launch_emails).with(store.id)
      expect(BulkStoreLaunchEmail).to receive(:send_store_launch_emails).with(store.id)
      BulkStoreLaunchEmailWorker.new.perform(store.id)
    end
  end
end
