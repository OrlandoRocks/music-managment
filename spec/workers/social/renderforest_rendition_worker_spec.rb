require "rails_helper"

describe Social::RenderforestRenditionWorker do
  let(:payload) {
    {
      message: "",
      quality: "360",
      status: "ok",
      id: "1",
      thumbnail: "https://s3.amazonaws.test/rf-test.tunecore.com/projects/1/output_360.png",
      video_url: "https://s3.amazonaws.test/rf-test.tunecore.com/projects/1/output_360.mp4"
    }
  }
  subject { described_class.new.perform(payload.to_json) }

  before do
    ENV["SOCIAL_API_BASE_URL"] = "https://socialtest.tunecore.com"
    ENV["SOCIAL_API_KEY_BACKEND"] = "test-key"
  end

  describe "#perform" do
    it "should make API call to TC Social backend" do
      mock_response = { status: "Success" }.to_json
      response_double = double("response", body: mock_response, status: 200)
      expected_post_url = "https://socialtest.tunecore.com/renderforest/project_status"

      expect(Faraday).to receive(:post).with(expected_post_url).and_return(response_double)
      subject
    end

    it "should set the request headers and body for API call to TC Social backend" do
      mock_response = { status: "Success" }.to_json
      request_double = double("request")
      headers_double = double("headers")
      response_double = double("response", body: mock_response, status: 200)
      allow(request_double).to receive(:headers).and_return(headers_double)

      expect(headers_double).to receive(:[]=).with("api-key", "test-key")
      expect(headers_double).to receive(:[]=).with("Content-Type", "application/json")
      expect(request_double).to receive(:body=).with(payload.to_json)
      expect(Faraday).to receive(:post).and_yield(request_double).and_return(response_double)
      subject
    end

    it "should raise error if error response retruned by TC Social backend" do
      error_message = "api.errors.project_not_found"
      mock_response = { status: "unprocessable_entity", errors: [error_message], data: {} }.to_json
      response_double = double("response", body: mock_response, status: 200)
      allow(Faraday).to receive(:post).and_return(response_double)

      expect { subject }.to raise_error(error_message)
    end
  end
end
