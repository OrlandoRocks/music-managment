require "rails_helper"

describe RewardSystem::TierEligibilityEventWorker do
  describe "#perform" do
    it "registers enqueued event" do
      eventer = instance_double(RewardSystem::TierEventer)
      person_id = 1

      expect(RewardSystem::TierEventer).to receive(:new).with('new_event').and_return(eventer)
      expect(eventer).to receive(:register_event).with(person_id)

      RewardSystem::TierEligibilityEventWorker.new.perform(person_id, 'new_event')
    end
  end
end
