require "rails_helper"

describe RewardSystem::LteRecalculationWorker do
  describe "#perform" do
    let(:person) { create(:person) }

    it "calculates recalculate earnings on person" do
      expect_any_instance_of(Person).to receive(:recalculate_lifetime_earning)

      RewardSystem::LteRecalculationWorker.new.perform(person.id)
    end

    it "triggers earnings updated event" do
      expect(RewardSystem::Events).to receive(:earnings_updated).with(person.id)

      RewardSystem::LteRecalculationWorker.new.perform(person.id)
    end
  end
end
