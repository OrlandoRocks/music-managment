require "rails_helper"

describe RewardSystem::TierEligibilityReconcileWorker do
  describe "#perform" do
    let(:person) { create(:person) }

    context "user has no tiers" do
      it "recalculates the tiers" do
        calculator = instance_double(RewardSystem::Eligibility::Calculator)

        expect(RewardSystem::Eligibility::Calculator).to receive(:new).with(person.id).and_return(calculator)
        expect(calculator).to receive(:check_and_upgrade)

        RewardSystem::TierEligibilityReconcileWorker.new.perform(person.id)
      end
    end

    context "user already has tier" do
      it "should not recalculate" do
        create(:tier_person, person: person)

        expect(RewardSystem::Eligibility::Calculator).to_not receive(:new)
        RewardSystem::TierEligibilityReconcileWorker.new.perform(person.id)
      end
    end
  end
end
