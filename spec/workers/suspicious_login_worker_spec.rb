require "rails_helper"

describe SuspiciousLoginWorker do
  describe "#perform" do
    it "calls on the SuspiciousLoginService service investigate" do
      service = instance_double(SuspiciousLoginService, investigate: {})

      expect(SuspiciousLoginService).to receive(:new).with(1).and_return(service)
      expect(service).to receive(:investigate)

      SuspiciousLoginWorker.new.perform(1)
    end
  end
end
