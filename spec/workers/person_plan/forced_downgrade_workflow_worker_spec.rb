# frozen_string_literal: true
require 'rails_helper'

describe PersonPlan::ForcedDowngradeWorkflowWorker do
  describe "#perform" do
    subject { PersonPlan::ForcedDowngradeWorkflowWorker.new.perform }
    let!(:past_grace_1_person) { create(:person) }
    let!(:plan_products_ids) { PlansProduct.where(plan_id: 4).pluck(:product_id) }
    let!(:plan_product) do
      Product.find_by(id: plan_products_ids, country_website_id: past_grace_1_person.country_website_id)
    end
    let!(:plan_purchase_1) do
      create(:purchase,
             product: plan_product,
             related: plan_product,
             person: past_grace_1_person,
             cost_cents: 3500)
    end
    let!(:past_grace_1_renewal) do
      create(:renewal,
             :plan_renewal,
             :no_takedown_at,
             purchase: plan_purchase_1,
             person: past_grace_1_person,
             expires_at: Date.today - 6.months
      )
    end
    let!(:past_grace_1_renewal_purchase) do
      create(:purchase,
             :unpaid,
             person: past_grace_1_person,
             related: past_grace_1_renewal,
             product: past_grace_1_renewal.item_to_renew.renewal_product
      )
    end
    let!(:past_grace_2_person) { create(:person) }
    let!(:plan_purchase_2) do
      create(:purchase,
             product: plan_product,
             related: plan_product,
             person: past_grace_2_person,
             cost_cents: 3500)
    end
    let!(:past_grace_2_renewal) do
      create(:renewal,
             :plan_renewal,
             :no_takedown_at,
             purchase: plan_purchase_2,
             person: past_grace_2_person,
             expires_at: Date.today - 6.months
      )
    end
    let!(:past_grace_2_renewal_purchase) do
      create(:purchase,
             :unpaid,
             person: past_grace_2_person,
             related: past_grace_2_renewal,
             product: past_grace_2_renewal.item_to_renew.renewal_product
      )
    end
    let!(:before_grace_person) { create(:person) }
    let!(:plan_purchase_3) do
      create(:purchase,
             product: plan_product,
             related: plan_product,
             person: before_grace_person,
             cost_cents: 3500)
    end
    let!(:before_grace_renewal) do
      create(:renewal,
             :plan_renewal,
             :no_takedown_at,
             purchase: plan_purchase_3,
             person: before_grace_person,
             expires_at: Date.today - 6.days
      )
    end
    let!(:before_grace_renewal_purchase) do
      create(:purchase,
             :unpaid,
             person: before_grace_person,
             related: before_grace_renewal,
             product: before_grace_renewal.item_to_renew.renewal_product
      )
    end

    it "calls PersonPlan::ForcedDowngradeWorker with renewals past the grace period" do
      expect(PersonPlan::ForcedDowngradeWorker).to receive(:perform_async).once.with(past_grace_1_renewal.id)
      expect(PersonPlan::ForcedDowngradeWorker).to receive(:perform_async).once.with(past_grace_2_renewal.id)
      subject
    end
    it "does not call PersonPlan::ForcedDowngradeWorker with renewals in the grace period" do
      expect(PersonPlan::ForcedDowngradeWorker).not_to receive(:perform_async).with(before_grace_renewal.id)
      subject
    end
  end
end
