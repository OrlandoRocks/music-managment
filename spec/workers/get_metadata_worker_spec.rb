require 'rails_helper'

RSpec.describe GetMetadataWorker, type: :worker do
  describe "#perform" do
    it "calls set_asset_metadata!" do
      s3_asset_id = 42
      s3a = double(:s3_asset)
      expect(S3Asset).to receive(:find).with(s3_asset_id).and_return(s3a)
      expect(s3a).to receive(:set_asset_metadata!)
      GetMetadataWorker.new.perform(s3_asset_id)
    end
  end
end
