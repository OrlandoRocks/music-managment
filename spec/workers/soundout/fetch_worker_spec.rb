require "rails_helper"

describe Soundout::FetchWorker do
  let(:soundout_report) { create(:soundout_report, :with_track) }

  context "when report is successfully downloaded" do

    it "should not raise an exception" do
      expect(SoundoutReport).to receive(:find).with(soundout_report.id).and_return(soundout_report)
      expect_any_instance_of(SoundoutProduct).to receive(:fetch_report).with(soundout_report).and_return([true, 'data'])

      expect{
        Soundout::FetchWorker.new.perform(soundout_report.id)
      }.to_not raise_error
    end

  end

  context "when a report fails to download" do

    it "should raise an exception" do
      expect(SoundoutReport).to receive(:find).with(soundout_report.id).and_return(soundout_report)
      expect_any_instance_of(SoundoutProduct).to receive(:fetch_report).with(soundout_report).and_return([false, nil])

      expect{
        Soundout::FetchWorker.new.perform(soundout_report.id)
      }.to raise_error
    end

  end

end
