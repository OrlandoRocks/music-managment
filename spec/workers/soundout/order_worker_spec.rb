require "rails_helper"

describe Soundout::OrderWorker do

  before(:each) do
    @report = create(:soundout_report, :with_track)
  end

  it "should raise an exception if no soundout_id is given" do
    expect(SoundoutReport).to receive(:find).with(@report.id).and_return(@report)
    expect_any_instance_of(SoundoutProduct).to receive(:order_report).with(@report).and_return(nil)

    expect{
      Soundout::OrderWorker.new.perform(@report.id)
    }.to raise_error
  end

  it "should not raise an exception if a soundout_id is given" do
    expect(SoundoutReport).to receive(:find).with(@report.id).and_return(@report)
    expect_any_instance_of(SoundoutProduct).to receive(:order_report).with(@report).and_return("123")

    expect{
      Soundout::OrderWorker.new.perform(@report.id)
    }.to_not raise_error
  end

end
