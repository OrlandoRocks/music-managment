require "rails_helper"

describe "PersonAnalyticWorker" do
  describe ".perform" do
    let(:invoice_id) { 1 }

    it "should add a job to the queue" do
      expect {
        PersonAnalyticWorker.perform_async(invoice_id)
      }.to change(PersonAnalyticWorker.jobs, :size).by(1)
    end

    it "should call the SalepointCreator", run_sidekiq_worker: true do
      expect(PersonAnalytic).to receive(:update_analytics_for_invoice).with(invoice_id)
      PersonAnalyticWorker.perform_async(invoice_id)
    end
  end
end
