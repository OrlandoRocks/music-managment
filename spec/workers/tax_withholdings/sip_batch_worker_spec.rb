# frozen_string_literal: true

require "rails_helper"

describe TaxWithholdings::SipBatchWorker do
  let(:posting_id) { 123456 }

  before do
    allow(FeatureFlipper).to receive(:show_feature?).and_call_original
    allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(true)
  end

  after(:each) do
    PersonTransaction.destroy_all
    IRSTaxWithholding.destroy_all
  end

  context "with person in the US" do
    let(:us_person_with_earnings_and_no_tax_forms) { create(:person, :with_lifetime_earnings, :with_you_tube_royalty_record, :with_sales_record, :with_person_intake, posting_id: posting_id) }
    let!(:person_transaction_for_us_person_with_earnings_and_no_tax_forms) { create(:person_transaction_for_person_intake, person: us_person_with_earnings_and_no_tax_forms) }

    let(:us_person_with_earnings_and_tax_forms) { create(:person, :with_lifetime_earnings, :with_you_tube_royalty_record, :with_sales_record, :with_person_intake, :with_tax_form, posting_id: posting_id) }
    let!(:person_transaction_for_us_person_with_earnings_and_tax_forms) { create(:person_transaction_for_person_intake, person: us_person_with_earnings_and_tax_forms) }

    let(:us_person_with_earnings_no_tax_forms_and_only_yt_royalty_record) { create(:person, :with_lifetime_earnings, :with_you_tube_royalty_record, :with_person_intake, posting_id: posting_id) }
    let!(:person_transaction_for_us_person_with_earnings_no_tax_forms_and_only_yt_royalty_record) { create(:person_transaction_for_person_intake, person: us_person_with_earnings_no_tax_forms_and_only_yt_royalty_record) }

    let(:us_person_with_earnings_no_tax_forms_and_only_sales_record) { create(:person, :with_lifetime_earnings, :with_sales_record, :with_person_intake, posting_id: posting_id) }
    let!(:person_transaction_for_us_person_with_earnings_no_tax_forms_and_only_sales_record) { create(:person_transaction_for_person_intake, person: us_person_with_earnings_no_tax_forms_and_only_sales_record) }

    let(:us_person_without_earnings_and_with_tax_forms) { create(:person, :with_transactions, :with_you_tube_royalty_record, :with_sales_record, :with_tax_form, :with_person_intake, posting_id: posting_id) }
    let!(:person_transaction_for_us_person_without_earnings_and_with_tax_forms) { create(:person_transaction_for_person_intake, person: us_person_without_earnings_and_with_tax_forms) }

    let(:person_without_intake) { create(:person, :with_lifetime_earnings, :with_you_tube_royalty_record, :with_sales_record, posting_id: posting_id) }
    let!(:person_transaction_for_person_without_intake) { create(:person_transaction_for_person_intake, person: person_without_intake, target_type: :non_taxable_target, comment: "Tally Ho!") }

    let(:person_with_you_tube_royalty_intake) { create(:person, :with_lifetime_earnings, :with_you_tube_royalty_record, :with_sales_record, :with_you_tube_royalty_intake, posting_id: posting_id) }
    let!(:person_transaction_for_person_with_you_tube_royalty_intake) { create(:person_transaction_for_person_intake, person: person_with_you_tube_royalty_intake) }

    it "enqueues a TaxWithholdings::IRSWithholdingServiceWorker for each found person_transaction" do
      worker = described_class.new
      worker.perform

      expect(TaxWithholdings::IRSWithholdingsServiceWorker.jobs.count).to eq(
        worker.person_transaction_ids.count
      )
    end

    context "with person who has earnings and no tax forms" do
      it "finds person_transactions for people with matching YT royalty and/or sales records" do
        worker = described_class.new
        worker.perform

        expect(worker.person_transaction_ids).to include(
          person_transaction_for_us_person_with_earnings_and_no_tax_forms.id,
          person_transaction_for_us_person_with_earnings_no_tax_forms_and_only_yt_royalty_record.id,
          person_transaction_for_us_person_with_earnings_no_tax_forms_and_only_sales_record.id
        )
      end
    end

    context "for person without a person_intake or you_tube_royalty_intake" do
      it "does not find person_transactions" do
        worker = described_class.new
        worker.perform

        expect(worker.person_transaction_ids).not_to include(
          person_transaction_for_person_without_intake.id
        )
      end
    end

    context "for person without earnings" do
      it "does not find person_transactions" do
        worker = described_class.new
        worker.perform

        expect(worker.person_transaction_ids).not_to include(
          person_transaction_for_us_person_without_earnings_and_with_tax_forms.id
        )
      end
    end

    context "for person with you_tube_royalty_intake" do
      it "finds person_transactions" do
        worker = described_class.new
        worker.perform

        expect(worker.person_transaction_ids).to include(
          person_transaction_for_person_with_you_tube_royalty_intake.id
        )
      end
    end

    it "does not find person_transactions for person with tax forms" do
      worker = described_class.new
      worker.perform

      expect(worker.person_transaction_ids).not_to include(
        person_transaction_for_us_person_with_earnings_and_tax_forms.id
      )
    end

    it "does not find person_transactions" do
      # Find number of person_transactions that will be found.
      worker = described_class.new
      worker.perform
      expect(worker.person_transaction_ids.count).to be > 0

      # Run IRS service that SipBatchWorker will run on each found person_transaction.
      worker.person_transaction_ids.each do |id|
        TaxWithholdings::IRSWithholdingsServiceWorker.new.perform(id)
      end

      # Find number of person_transactions that will now be found after running IRS service.
      worker = described_class.new
      worker.perform
      expect(worker.person_transaction_ids.count).to eq(0)
    end
  end

  context "with person not in the US" do
    let(:non_us_person_with_earnings_and_no_tax_forms) { create(:person, :with_lifetime_earnings, :with_you_tube_royalty_record, :with_sales_record, posting_id: posting_id, country: "CA") }
    let!(:person_transaction_for_non_us_person_with_earnings_and_no_tax_forms) { create(:person_transaction_for_person_intake, person: non_us_person_with_earnings_and_no_tax_forms) }

    let(:non_us_person_with_earnings_and_tax_forms) { create(:person, :with_lifetime_earnings, :with_you_tube_royalty_record, :with_sales_record, :with_tax_form, posting_id: posting_id, country: "CA") }
    let!(:person_transaction_for_non_us_person_with_earnings_and_tax_forms) { create(:person_transaction_for_person_intake, person: non_us_person_with_earnings_and_tax_forms) }

    let(:non_us_person_without_earnings_and_no_tax_forms) { create(:person, :with_you_tube_royalty_record, :with_sales_record, :with_tax_form, posting_id: posting_id, country: "CA") }
    let!(:person_transaction_for_non_us_person_without_earnings_and_no_tax_forms) { create(:person_transaction_for_person_intake, person: non_us_person_without_earnings_and_no_tax_forms) }

    let(:non_us_person_without_earnings_and_with_tax_forms) { create(:person, :with_you_tube_royalty_record, :with_sales_record, :with_tax_form, posting_id: posting_id, country: "CA") }
    let!(:person_transaction_for_non_us_person_without_earnings_and_with_tax_forms) { create(:person_transaction_for_person_intake, person: non_us_person_without_earnings_and_with_tax_forms) }

    it "does not find person_transactions that already have IRS tax withholdings" do
      worker = described_class.new
      worker.perform

      expect(worker.person_transaction_ids).not_to include(
        person_transaction_for_non_us_person_with_earnings_and_no_tax_forms.id,
        person_transaction_for_non_us_person_with_earnings_and_tax_forms.id,
        person_transaction_for_non_us_person_without_earnings_and_no_tax_forms.id,
        person_transaction_for_non_us_person_without_earnings_and_with_tax_forms.id
      )
    end
  end
end
