require "rails_helper"
Sidekiq::Testing.fake!

describe TaxWithholdings::MassAdjustmentsBatchWorker do
  include ActiveSupport::Testing::TimeHelpers
  subject { TaxWithholdings::IRSWithholdingsServiceWorker }

  let!(:administrator)     { create(:person, :with_all_roles) }
  let!(:user_with_withholdable_person_transaction)   { create(:person, country: "US") }
  let!(:taxable_balance_adjustment_transactions) do 
    10.times.map do |index|
      create(
        :person_transaction,
        :with_irs_tax_withholding,
        person: user_with_withholdable_person_transaction,
        debit: 0.0,
        credit: 100,
        target: create(:balance_adjustment, category: "Songwriter Royalty", posted_by: administrator, posted_by_name: administrator.name),
        currency: "USD"
      )
    end
  end

  describe ".perform" do
    it "schedules TaxWithholdings::IRSWithholdingsServiceWorker worker for eligible transactions" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return true
      expect { described_class.new.perform }.to change(subject.jobs, :size).by(10)
    end

    it "does not create duplicate tax withholdings when run multiple times" do
      allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return true
      2.times.map { described_class.new.perform }
      expect(IRSTaxWithholding.count).to be(10)
    end
  end
end
