require "rails_helper"
Sidekiq::Testing.fake!

describe TaxWithholdings::Reporting::IRSPayablesReportWorker, type: :worker do
  let(:fake_mailer) { double("fake mailer") }

  before do
    # Do not run callbacks on Person objects when they get updated
    allow(Person::AfterCommitCallbacks).to receive(:send_to_believe).and_return(false)
    allow(Person::AfterCommitCallbacks).to receive(:send_to_zendesk).and_return(true)

    allow(fake_mailer).to receive(:deliver!)
    allow(AdminTaxReportsMailer).to receive(:weekly_tax_withholdings_summary_report).and_return(fake_mailer)
    allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(true)
    allow(ENV).to receive(:fetch).with("ADMIN_TAX_WITHHOLDINGS_REPORT_RECIPIENTS").and_return("test@test1.com")
  end

  describe "#perform" do
    context "with multiple calls" do
      it "is uniq when run multiple times within 30 minutes" do
        Sidekiq::Enterprise.unique! do
          described_class.perform_async
          expect { travel_to(29.minutes.from_now) { described_class.perform_async } }.to(
            change(described_class.jobs, :size).by(0)
          )
        end
      end
  
      it "is no longer unique when run again after 30 minutes" do
        Sidekiq::Enterprise.unique! do
          described_class.perform_async
          expect { travel_to(31.minutes.from_now) { described_class.perform_async } }.to(
            change(described_class.jobs, :size).by(1)
          )
        end
      end
    end

    context "in a single call" do
      subject(:worker) { described_class.new }
      let(:target_file_path) { "tmp/irs_payables_report_test_000_0000w0.csv" }
      let(:target_file_path_regex) { /^tmp\/irs_payables_report_test_.*\.csv/ }

      before do
        allow(File).to receive(:open).with(target_file_path_regex, any_args).and_return(<<~CSV)
          person_id,person_email,total_tax_withholding_amount,currency
          1,tc_1@tunecore.net,0.00308232,USD
        CSV
      end

      around do |example|
        Sidekiq::Testing.inline! { example.run }
      end
      
      shared_examples "csv reporting" do |use_irs_transaction_id|
        context "with CSV reporting" do
          let(:csv_service_double) { double("csv_service") }
          let(:args) { use_irs_transaction_id ? [irs_transaction.id] : [] }

          before do
            allow(csv_service_double).to receive(:generate_csv!).and_return([target_file_path, []])
            allow(TaxWithholdings::Reporting::IRSPayablesReportCsvService).to(
              receive(:new).and_return(csv_service_double)
            )
          end

          it "generates a CSV report" do
            expect(csv_service_double).to receive(:generate_csv!)
            described_class.perform_async(*args)
          end

          it "uploads the CSV report" do
            expect(RakeDatafileService).to receive(:upload).with(target_file_path, any_args)
            described_class.perform_async(*args)
          end

          it "mails the CSV report" do
            expect(fake_mailer).to receive(:deliver!)
            described_class.perform_async(*args)
          end
        end
      end

      context "with a given IRSTransaction ID" do
        let(:irs_transaction) { create(:irs_transaction) }
        let!(:irs_tax_withholdings) do
          create_list(
            :irs_tax_withholding,
            2,
            :with_person_transaction,
            irs_transaction: irs_transaction
          )
        end

        it "uses data from all IRSTaxWithholding records linked to the given IRSTransaction" do
          worker.perform(irs_transaction.id)
          expect(worker.report_stats).to include(total_withheld_accounts: 2)
        end

        include_examples "csv reporting", true
      end

      context "without a given IRSTransaction ID" do
        let!(:irs_tax_withholdings) do
          create_list(:irs_tax_withholding, 2, :with_person_transaction)
        end

        it "uses data from all IRSTaxWithholding records marked as unreported" do
          worker.perform
          expect(worker.report_stats).to include(total_withheld_accounts: 2)
        end

        it "skips data from IRSTaxWithholding records marked as repored" do
          worker.perform
          create(:irs_tax_withholding, :with_person_transaction)
          worker.perform
          expect(worker.report_stats).to include(total_withheld_accounts: 1)
        end

        include_examples "csv reporting", false
      end
    end
  end
end
