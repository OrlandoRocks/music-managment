require "rails_helper"

describe TaxWithholdings::IRSWithholdingsServiceWorker, type: :worker do
  let(:person) { create(:person) }
  let(:person_transaction) {
    create(
      :person_transaction,
      person: person,
      target_id: 1,
      target_type: "BalanceAdjustment",
      credit: 1.00
    )
  }

  before do
    allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(true)
  end

  it "calls IRSWithholdingsService with the given person_transaction" do
    expect(TaxWithholdings::IRSWithholdingsService).to receive(:new).with(person_transaction).and_call_original
    TaxWithholdings::IRSWithholdingsServiceWorker.new.perform(person_transaction.id)
  end

  it "doesn't call IRSWithholdingsService when 'us_tax_withholding' feature flag is not set" do
    allow(FeatureFlipper).to receive(:show_feature?).with(:us_tax_withholding).and_return(false)
    expect(TaxWithholdings::IRSWithholdingsService).not_to receive(:new)
    TaxWithholdings::IRSWithholdingsServiceWorker.new.perform(person_transaction.id)
  end

  it "doesn't call IRSWithholdingsService when the given person_transaction doesn't exist" do
    person_transaction_id = person_transaction.id
    person_transaction.destroy
    expect(TaxWithholdings::IRSWithholdingsService).not_to receive(:new)
    TaxWithholdings::IRSWithholdingsServiceWorker.new.perform(person_transaction_id)
  end
end
