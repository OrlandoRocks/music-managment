# frozen_string_literal: true

require "rails_helper"

describe TaxBlocking::BatchPersonEarningsSummarizationWorker do
  let!(:person_without_transactions) { create(:person) }
  let!(:person_with_recent_transactions) { create(:person, :with_transactions) }
  let!(:person_with_transactions_from_last_year) { create(:person, :with_transactions, transactions_created_at: 1.year.ago) }
  let!(:person_with_transactions_from_two_years_ago) { create(:person, :with_transactions, transactions_created_at: 2.years.ago) }
  
  context "with person_transactions made within the current year" do
    it "finds people with person_transactions" do
      worker = described_class.new
      worker.perform
      
      expect(worker.person_ids).to include(person_with_recent_transactions.id)
      expect(worker.person_ids).not_to include(person_without_transactions.id)
    end

    it "finds people with person_transactions made within the current year only" do
      worker = described_class.new
      worker.perform

      expect(worker.person_ids).to include(person_with_recent_transactions.id)
      expect(worker.person_ids).not_to include(person_with_transactions_from_last_year.id)
    end

    context "with given posting IDs" do
      let(:posting_id) { 123456 }
      let!(:person_with_recent_transactions_and_sales_record) { create(:person, :with_transactions) }
      let!(:sales_record) { create(:sales_record, posting_id: posting_id, person_id: person_with_recent_transactions_and_sales_record.id) }

      it "finds people with sales_records that have a given posting ID" do
        worker = described_class.new
        worker.perform(posting_ids: [posting_id])

        expect(worker.person_ids).to include(person_with_recent_transactions_and_sales_record.id)
        expect(worker.person_ids).not_to include(
          person_without_transactions.id,
          person_with_recent_transactions.id,
          person_with_transactions_from_last_year.id,
          person_with_transactions_from_two_years_ago.id
        )
      end
    end

    context "with given batch balance adjustment IDs" do
      let(:batch_balance_adjustment_id) { 123456 }
      let!(:person_with_recent_transactions_and_batch_balance_adjustments) { create(:person, :with_transactions) }
      let!(:batch_balance_adjustment_detail) { create(:batch_balance_adjustment_detail, batch_balance_adjustment_id: batch_balance_adjustment_id, person_id: person_with_recent_transactions_and_batch_balance_adjustments.id) }

      it "finds people with batch_balance_adjustment_details that have the given batch_balance_adjustment_ids" do
        worker = described_class.new
        worker.perform(batch_balance_adjustment_ids: [batch_balance_adjustment_id])

        expect(worker.person_ids).to include(person_with_recent_transactions_and_batch_balance_adjustments.id)
        expect(worker.person_ids).not_to include(
          person_without_transactions.id,
          person_with_recent_transactions.id,
          person_with_transactions_from_last_year.id,
          person_with_transactions_from_two_years_ago.id
        )
      end
    end
  end

  context "with person_transactions made within the last 2 years" do
    it "finds people with person_transactions made within the past two years only" do
      worker = described_class.new
      worker.perform(years: 1.year.ago.year..Time.now.year)

      expect(worker.person_ids).to include(person_with_recent_transactions.id, person_with_transactions_from_last_year.id)
      expect(worker.person_ids).not_to include(person_with_transactions_from_two_years_ago.id)
    end
  end
end
