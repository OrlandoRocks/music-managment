require "rails_helper"

describe FetchTaxFormsWorker do
  describe "#perform" do
    let(:person) { create(:person) }
    let!(:payout_provider) { create(:payout_provider, person: person) }

    before do
      response_double = { "GetSubmittedTaxFormsForPayee" => { "TaxForms" => nil } }
      api_client_double = double(post: true, formatted_response: response_double)
      allow(Payoneer::V2::TaxApiClient).to receive(:new).and_return(api_client_double)
    end

    it "calls TaxFromCheckService to fetch and save the tax-forms" do
      expect(TaxFormCheckService).to receive(:check_api?).with(person.id)
      FetchTaxFormsWorker.new.perform(person.id)
    end

    it "handles failures" do
      allow_any_instance_of(Payoneer::TaxApiService).to receive(:has_active_forms?).and_return(true)
      allow_any_instance_of(Payoneer::TaxApiService).to receive(:save_tax_form).and_raise("Boom")

      expect(Rails.logger).to receive(:error).with("FetchTaxFormsWorker failure: person_id: #{person.id}, error: Boom")
      expect(Tunecore::Airbrake).to receive(:notify).with("FetchTaxFormsWorker failure: Boom", person_id: person.id)
      FetchTaxFormsWorker.new.perform(person.id)
    end
  end
end
