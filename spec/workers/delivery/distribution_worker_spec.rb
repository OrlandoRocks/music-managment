require "rails_helper"

describe Delivery::DistributionWorker do
  let(:distro) { create(:distribution, converter_class: "MusicStores::SevenDigital::Converter", delivery_type: nil) }

  let(:jid)    { "1234567" }

  before :each do
    allow_any_instance_of(Believe::ApiClient).to receive(:post).and_return(true)
    allow(Person::AfterCommitCallbacks).to receive(:send_to_zendesk).and_return(true)
    allow_any_instance_of(Delivery::DistributionWorker).to receive(:jid).and_return(jid)
  end

  describe ".perform", run_sidekiq_worker: true do
    it "invokes .deliver on the distro with the given ID" do
      expect(Delivery::Distribution).to receive(:deliver).with(distro.class.name, distro.id, "full_delivery", "distribution-queue", jid)
      Delivery::DistributionWorker.perform_async(distro.class.name, distro.id, "full_delivery", "distribution-queue")
    end

    it "calls rate limiter if store delivers via tunecore" do
      fake_rate_limiters = { "7Digital" => double(within_limit: true) }
      distribution      = create(:distribution, :with_salepoint, store_short_name: "7Digital", converter_class: "MusicStores::SevenDigital::Converter")
      store             = distribution.salepoints.last.try(:store)
      store.delivery_service = "tunecore"
      store.save!
      stub_const("Delivery::DistributionWorker::RATE_LIMITERS", fake_rate_limiters)
      expect(fake_rate_limiters["7Digital"]).to receive(:within_limit)
      Delivery::DistributionWorker.perform_async(distribution.class.name, distribution.id, "full_delivery", "distribution-queue")
    end
  end

  describe ".handle_failure" do
    context "when retry limit is reached" do
      before(:each) do
        @msg = {
          "class"=>"Delivery::DistributionWorker",
          "args"=>["Distribution", 123456789],
          "retry"=>10,
          "queue"=>"distribution",
          "jid" => jid,
          "error_message"=>"something went wrong",
          "error_backtrace"=>["a_backtrace_line", "another_backtrace_line"],
        }
      end

      it "finds the correct distribution_song" do
        fake_dds = double(distributable: "fake_distro_song")
        allow(Delivery::Distribution).to receive(:new).and_return fake_dds
        allow(Distribution::StateUpdateService).to receive(:update).and_return(nil)

        expect(Delivery::Distribution).to receive(:new).with("Distribution", 123456789, nil, nil, jid)
        Delivery::DistributionWorker.handle_failure(@msg)
      end

      it "calls Distribution::StateUpdateService" do
        fake_dds = double(distributable: "fake_distro_song")
        allow(Delivery::Distribution).to receive(:new).and_return fake_dds
        params = {
          state: "error",
          message: "something went wrong",
          backtrace: "a_backtrace_line\n\tanother_backtrace_line",
          actor: "Delivery::DistributionWorker"
        }

        expect(Distribution::StateUpdateService).to receive(:update).with("fake_distro_song", params)
        Delivery::DistributionWorker.handle_failure(@msg)
      end
    end
  end
end
