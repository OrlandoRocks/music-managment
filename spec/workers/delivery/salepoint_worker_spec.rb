require "rails_helper"

describe Delivery::SalepointWorker do
  let(:album) { create(:album, :with_salepoints, payment_applied: true, finalized_at: Time.now) }
  let(:salepoint) { album.salepoints.last }

  describe ".perform", run_sidekiq_worker: true do
    it "calls on the deliverer service" do
      allow_any_instance_of(Believe::ApiClient).to receive(:post).and_return(true)
      allow(Person::AfterCommitCallbacks).to receive(:send_to_zendesk).and_return(true)
      expect(Delivery::Salepoint).to receive(:deliver).with(salepoint.id)
      Delivery::SalepointWorker.perform_async(salepoint.id)
    end
  end
end
