require "rails_helper"

describe Delivery::BelieveWorker do
  let(:album) { create(:album, :with_salepoints, payment_applied: true, finalized_at: Time.now) }
  let(:distribution) { create(:distribution, petri_bundle: create(:petri_bundle, album: album))}
  describe ".perform", run_sidekiq_worker: true do
    before(:each) do
      ENV["NOTIFY_BELIEVE"]="true"
      allow_any_instance_of(Believe::ApiClient).to receive(:post).and_return(true)
      allow_any_instance_of(ApiLogger::InitLoggerService).to receive(:call).and_return(true)
      allow(Delivery::Believe).to receive(:deliver).with(distribution.id, album.id, album.person.id)
      allow(Person::AfterCommitCallbacks).to receive(:send_to_zendesk).and_return(true)
    end

    it "calls on the deliverer service" do
      expect(Delivery::Believe).to receive(:deliver).with(distribution.id, album.id, album.person.id)
      Delivery::BelieveWorker.perform_async(distribution.id, album.id, album.person.id)
    end
  end
end
