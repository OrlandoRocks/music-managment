require "rails_helper"

describe Delivery::AlbumWorker do
  let(:album) { create(:album, :with_salepoints, payment_applied: true, finalized_at: Time.now) }

  describe ".perform", run_sidekiq_worker: true do
    before(:each) do
      allow_any_instance_of(Believe::ApiClient).to receive(:post).and_return(true)
      allow(Person::AfterCommitCallbacks).to receive(:send_to_zendesk).and_return(true)
      allow(Delivery::Album).to receive(:deliver).with(album.id)
    end

    it "calls on the deliverer service" do
      expect(Delivery::Album).to receive(:deliver).with(album.id)
      Delivery::AlbumWorker.perform_async(album.id)
    end
  end
end
