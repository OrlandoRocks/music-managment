require "rails_helper"

describe Delivery::Believe::NotifyDeliveryWorker do
  let(:album) { create(:album, :with_salepoints, payment_applied: true, finalized_at: Time.now) }

  describe ".perform", run_sidekiq_worker: true do
    before(:each) do
      allow_any_instance_of(Believe::ApiClient).to receive(:post).and_return(true)
      allow(Person::AfterCommitCallbacks).to receive(:send_to_zendesk).and_return(true)
      allow(Delivery::Believe::NotifyDelivery).to receive(:notify_delivery).with(album.id, album.salepoints.pluck(:store_id), "INSERT")
      ENV["NOTIFY_BELIEVE"]="true"
    end

    it "calls on the deliverer service" do
      expect(Delivery::Believe::NotifyDelivery).to receive(:notify_delivery).with(album.id, album.salepoints.pluck(:store_id), "INSERT")
      Delivery::Believe::NotifyDeliveryWorker.perform_async(album.id, album.salepoints.pluck(:store_id), "INSERT")
    end
  end
end
