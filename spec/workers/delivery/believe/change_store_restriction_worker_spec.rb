require "rails_helper"

describe Delivery::Believe::ChangeStoreRestrictionWorker do
  let(:album) { create(:album, :with_salepoints, payment_applied: true, finalized_at: Time.now) }

  describe ".perform", run_sidekiq_worker: true do
    before(:each) do
      ENV["NOTIFY_BELIEVE"]="true"
      allow_any_instance_of(Believe::ApiClient).to receive(:post).and_return(true)
      allow_any_instance_of(ApiLogger::InitLoggerService).to receive(:call).and_return(true)
      allow(Delivery::Believe::ChangeStoreRestriction).to receive(:change_store_restriction).with(album.id, "allow", album.salepoints.pluck(:store_id))
    end

    it "calls on the deliverer service" do
      expect(Delivery::Believe::ChangeStoreRestriction).to receive(:change_store_restriction).with(album.id, "allow", album.salepoints.pluck(:store_id))
      Delivery::Believe::ChangeStoreRestrictionWorker.perform_async(album.id, "allow", album.salepoints.pluck(:store_id))
    end
  end
end
