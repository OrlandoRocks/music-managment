require "rails_helper"

describe Delivery::DistributionSongWorker do
  describe ".perform" do
    let(:album)             { create(:album, :with_salepoint_song) }
    let(:salepoint_song_id) { album.salepoints.first.salepoint_songs.first.id }

    it "should call DistributionSongCreater with the salepoint_song ID", run_sidekiq_worker: true do
      allow_any_instance_of(Believe::ApiClient).to receive(:post).and_return(true)
      allow(Person::AfterCommitCallbacks).to receive(:send_to_zendesk).and_return(true)
      expect(Delivery::DistributionSong).to receive(:deliver).with(salepoint_song_id)
      Delivery::DistributionSongWorker.perform_async(salepoint_song_id)
    end

    context "when retry limit is reached" do
      describe ".handle_failure" do
        before(:each) do
          @msg = {
            "class"=>"Delivery::DistributionSongWorker",
            "args"=>[123456789],
            "retry"=>10,
            "queue"=>"distribution",
            "error_message"=>"something went wrong",
            "error_backtrace"=>["a_backtrace_line", "another_backtrace_line"],
          }
        end

        it "finds the correct distribution_song" do
          fake_dds = double(find_or_create_distribution: "fake_distro_song")
          allow(Delivery::DistributionSong).to receive(:new).and_return fake_dds
          allow(Distribution::StateUpdateService).to receive(:update).and_return(nil)

          expect(Delivery::DistributionSong).to receive(:new).with(123456789)
          Delivery::DistributionSongWorker.handle_failure(@msg)
        end

        it "calls Distribution::StateUpdateService" do
          fake_dds = double(find_or_create_distribution: "fake_distro_song")
          allow(Delivery::DistributionSong).to receive(:new).and_return fake_dds
          params = {
            :state=>"errored",
            :message=>"something went wrong",
            :backtrace=>"a_backtrace_line\n\tanother_backtrace_line"
          }

          expect(Distribution::StateUpdateService).to receive(:update).with("fake_distro_song", params)
          Delivery::DistributionSongWorker.handle_failure(@msg)
        end
      end
    end
  end
end
