require "rails_helper"

describe BigboxWorker do
  describe "#perform" do
    it "calls on the BigBoxCommunicator service" do
      song   = create(:song)
      sqs    = double
      queue  = double
      queues = double(named: queue)
      allow(sqs).to receive(:queues).and_return(queues)
      allow(sqs).to receive(:queue).and_return(queue)
      stub_const("SQS_CLIENT", sqs)
      expect(BigboxCommunicator).to receive(:enqueue_for_bigbox).with("test queue", "test job", "test locker", ["http://imgr.com/blahblah"], "xxxxx", "xxxxx", {package_name: "test package", song_id: song.id, person_id: song.album.person.id})
      BigboxWorker.new.perform("test queue", "test job", "test locker", ["http://imgr.com/blahblah"], "xxxxx", "xxxxx", {package_name: "test package", song_id: song.id, person_id: song.album.person.id})
    end
  end
end
