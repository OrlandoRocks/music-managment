require 'rspec'

describe BatchTransactionProcessWorker do
  before do
    @invoices = []
    @payment_batch = TCFactory.create(:payment_batch)
    @invoices << Invoice.create(person: TCFactory.create(:person), batch_status: "waiting_for_batch")
    @invoices << Invoice.create(person: TCFactory.create(:person), batch_status: "waiting_for_batch")
    @transactions = @payment_batch.create_transactions_for_invoices(@invoices)
  end

  it 'calls process on every batch transaction' do
    expect(PaymentBatch).to receive(:find).at_least(:once).with(@payment_batch.id).and_return(@payment_batch)
    @transactions.each{|bt| expect(BatchTransaction).to receive(:find).at_least(:once).with(bt.id).and_return(bt)}
    expect(@payment_batch).to receive(:process_transactions).with(@transactions)
    BatchTransactionProcessWorker.new.perform(@payment_batch.id, @transactions.map(&:id))
  end
end