require "rails_helper"

describe TrackMonetization::PostApprovalWorker do
  before :each do
    @album = create(:album, :paid, :approved, :with_one_year_renewal, :with_uploaded_songs)
    TrackMonetization::PostApprovalWorker.new.perform(@album.id, 83)
  end

  it "creates track monetizations for an album's songs and a given store" do
    expect(TrackMonetization.count).to eq(@album.songs.count)
  end

  it "checks the eligibility of each track monetization" do
    expect(TrackMonetization.all.none?{|tm| tm.eligibility_status == "pending"}).to be true
  end

  it "does not create duplicate track monetizations" do
    expect{
      TrackMonetization::PostApprovalWorker.new.perform(@album.id, 83)
    }.not_to change(TrackMonetization, :count)
  end

  it "does not raise an error when duplicate inserts are attempted" do
    expect{
      TrackMonetization::PostApprovalWorker.new.perform(@album.id, 83)
    }.not_to raise_error
  end
end