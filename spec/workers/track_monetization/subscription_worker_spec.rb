require "rails_helper"

describe TrackMonetization::SubscriptionWorker do
  it "invokes the SubscriptionService" do
    @person      = create(:person)

    @params = {
      person_id: @person.id,
      service: "FBTracks"
    }.with_indifferent_access

    expect(TrackMonetization::SubscriptionService).to receive(:subscribe).with(@params)
    TrackMonetization::SubscriptionWorker.new.perform(@params)
  end
end