require 'rails_helper'

describe TrendDataAggregationWorker do
  let(:person)    { FactoryBot.create(:person) }
  let(:job)       { TrendDataAggregationWorker.new }

  before(:each) do
    allow_any_instance_of(TrendDataAggregator).to receive(:aggregate_and_store_trends_by_day)
  end

  context "without a specified start and end date" do
    it "executes the TrendDataAggregator" do
      expect(TrendDataAggregator).to receive(:execute).with(person.id, nil, nil)
      job.perform(person.id)
    end

    it "passes default arguments of start and end dates to TrendDataAggregator initialization" do
      aggregator_default_dates = TrendDataAggregator.new(person.id, 90.days.ago.to_date.to_date.to_s, Date.yesterday.to_s)
      expect(TrendDataAggregator).to receive(:new).with(person.id, 90.days.ago.to_date.to_date.to_s, Date.yesterday.to_s) {aggregator_default_dates}
      job.perform(person.id)
    end
  end

  context "with a specified start and end date" do
    it "executes the TrendDataAggregator" do
      expect(TrendDataAggregator).to receive(:execute).with(person.id, "2017-02-01", "2017-02-14")
      job.perform(person.id, "2017-02-01", "2017-02-14")
    end

     it "passes the given start and end dates to TrendDataAggregator initialization" do
      aggregator = TrendDataAggregator.new(person.id, "2017-02-01", "2017-02-14")
      expect(TrendDataAggregator).to receive(:new).with(person.id, "2017-02-01", "2017-02-14") {aggregator}
      job.perform(person.id, "2017-02-01", "2017-02-14")
    end
  end

end
