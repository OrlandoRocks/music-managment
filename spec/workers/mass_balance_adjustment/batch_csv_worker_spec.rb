require "rails_helper"

describe MassBalanceAdjustment::BatchCsvWorker do

  it "should process the downloaded csv file and create batch entries" do
    csv = <<~CSV
      person_id,correction_amount
      1, 100
      2, 200
    CSV

    # Mocking the read from s3 bucket
    file_name = '123.csv'
    allow_any_instance_of(AWS::S3).to receive(:buckets) {
      OpenStruct.new({
        ENV["MASS_BALANCE_ADJUSTMENT_BUCKET"] => OpenStruct.new({
          "objects" => {
            "test/#{file_name}" => double(:object, read: csv)
          }
        })
      })
    }

    batch = create(:mass_adjustment_batch, s3_file_name: file_name)
    expect {
      MassBalanceAdjustment::BatchCsvWorker.new.perform(batch.id)
    }.to change(MassAdjustmentEntry, :count).by(2)
  end

  describe "validations" do
    it "should record error if person id is invalid" do
      row = {MassAdjustmentsForm::PERSON_ID => "abc", MassAdjustmentsForm::CORRECTION_AMOUNT => "12"}
      csv_worker = MassBalanceAdjustment::BatchCsvWorker.new
      errors = csv_worker.send(:validate_entry, row)
      expect(errors).to eq(["Invalid person id", "Unknown person id"])
    end

    it "should record error if correction amount is invalid" do
      row = {MassAdjustmentsForm::PERSON_ID => Person.first.id.to_s, MassAdjustmentsForm::CORRECTION_AMOUNT => "aa"}
      csv_worker = MassBalanceAdjustment::BatchCsvWorker.new
      errors = csv_worker.send(:validate_entry, row)
      expect(errors).to eq(["Invalid correction amount"])
    end
  end
end
