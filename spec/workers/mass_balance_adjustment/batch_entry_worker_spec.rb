require "rails_helper"

describe MassBalanceAdjustment::BatchEntryWorker do
  before do
    allow_any_instance_of(Logger).to receive(:info).and_return(nil)
    allow_any_instance_of(Logger).to receive(:error).and_return(nil)
  end

  it "should process mass adjustment entry record to correct user balances" do
    person = create(:person, :with_balance, amount: 100)
    batch = create(:mass_adjustment_batch, balance_adjustment_category_id: 8)
    correction_amount = -5
    entry = create(:mass_adjustment_entry, mass_adjustment_batch: batch, person: person, amount: correction_amount, status: "")
    balance = person.person_balance.balance

    expect {
      MassBalanceAdjustment::BatchEntryWorker.new.perform(entry.id)
      expect(entry.reload.status).to eq(MassAdjustmentEntry::SUCCESS)
      expect(person.reload.person_balance.balance).to eq(balance + correction_amount)
    }.to change(BalanceAdjustment, :count).by(1)
    expect(BalanceAdjustment.last.category).to eq("Tidal DAP")
  end

  it "should not process if mass adjustment batch entry is successfully processed already" do
    person = create(:person, :with_balance, amount: 100)
    batch = create(:mass_adjustment_batch)
    entry = create(:mass_adjustment_entry, mass_adjustment_batch: batch, person: person, status: MassAdjustmentEntry::SUCCESS)
    expect {
      MassBalanceAdjustment::BatchEntryWorker.new.perform(entry.id)
    }.to change(BalanceAdjustment, :count).by(0)
  end

  it "should not process if mass adjustment batch entry errored already" do
    person = create(:person, :with_balance, amount: 100)
    batch = create(:mass_adjustment_batch)
    entry = create(:mass_adjustment_entry, mass_adjustment_batch: batch, person: person, status: MassAdjustmentEntry::ERROR)
    expect {
      MassBalanceAdjustment::BatchEntryWorker.new.perform(entry.id)
    }.to change(BalanceAdjustment, :count).by(0)
  end

  it "should not process if person id is not present" do
    batch = create(:mass_adjustment_batch)
    entry = create(:mass_adjustment_entry, mass_adjustment_batch: batch, person: nil)
    expect {
      MassBalanceAdjustment::BatchEntryWorker.new.perform(entry.id)
    }.to change(BalanceAdjustment, :count).by(0)
    entry.reload
    expect(entry.status).to eq(MassAdjustmentEntry::ERROR)
    expect(entry.status_info).to eq("Person not found")
  end

  it "should not process if the account goes negative but not configured to process negative values" do
    person = create(:person, :with_balance, amount: 100)
    batch = create(:mass_adjustment_batch, allow_negative_balance: false)
    entry = create(:mass_adjustment_entry, mass_adjustment_batch: batch, person: person, amount: -300)
    # allow(entry).to receive(:account_goes_negative?).and_return(true)
    expect {
      MassBalanceAdjustment::BatchEntryWorker.new.perform(entry.id)
    }.to change(BalanceAdjustment, :count).by(0)
    entry.reload
    expect(entry.status).to eq(MassAdjustmentEntry::CANCELLED)
    expect(entry.status_info).to eq("Balance can't go negative")
  end

  it "should catch any unexpected error and set the status of batch entry to error" do
    person = create(:person, :with_balance, amount: 100)
    batch = create(:mass_adjustment_batch)
    entry = create(:mass_adjustment_entry, mass_adjustment_batch: batch, person: person, amount: 10, status: "")
    allow(BalanceAdjustment).to receive(:new).and_raise("boom")

    MassBalanceAdjustment::BatchEntryWorker.new.perform(entry.id)
    entry.reload
    expect(entry.status).to eq(MassAdjustmentEntry::ERROR)
    expect(entry.status_info).to eq("Unable to process mass adjustment entry. boom")
  end

  describe "#send_mail" do
    context "when the batch is not complete" do
      it "it should NOT send an email to admin users with the feature flag" do
        mass_adjustments_approver = create(:person, :with_balance, amount: 100)
        batch = create(:mass_adjustment_batch)
        entry = create(:mass_adjustment_entry, {
          mass_adjustment_batch: batch,
          person: mass_adjustments_approver,
          amount: 10,
          status: nil
        })
        create(:mass_adjustment_entry, {
          mass_adjustment_batch: batch,
          person: mass_adjustments_approver,
          amount: 10,
          status: nil
        })

        expect(AdminNotifier)
          .not_to receive(:mass_balance_adjustment)
          .with(
            batch,
            [mass_adjustments_approver.email],
        ).and_call_original

        MassBalanceAdjustment::BatchEntryWorker.new.perform(entry.id)
      end
    end

    context "when the batch is completed" do
      it "it should send an email to admin users with the feature flag" do
        mass_adjustments_approver = create(:person, :with_balance, amount: 100)
        batch = create(:mass_adjustment_batch)
        entry = create(:mass_adjustment_entry, {
          mass_adjustment_batch: batch,
          person: mass_adjustments_approver,
          amount: 10,
          status: nil
        })

        allow(FeatureFlipper).to receive(:get_users)
          .with(:mass_balance_adjustment_email)
          .and_return([mass_adjustments_approver.id])

        expect(AdminNotifier)
          .to receive(:mass_balance_adjustment)
          .with(
            batch,
            [mass_adjustments_approver.email],
        ).and_call_original

        MassBalanceAdjustment::BatchEntryWorker.new.perform(entry.id)
      end
    end
  end
end
