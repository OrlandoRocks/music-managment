require "rails_helper"

describe SuccessfulPurchaseWorker do
  describe "#perform" do
    it "passes invoice to service" do
      invoice = create(:invoice)
      expect(SuccessfulPurchaseService).to receive(:process).with(invoice)
      SuccessfulPurchaseWorker.new.perform(invoice.id)
    end
  end
end
