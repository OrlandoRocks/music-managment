require "rails_helper"

describe BatchBalanceAdjustment::PostAdjustmentWorker do
# TODO, fix these
#  before :each do
#    @admin = Person.find_by_email('admin@tunecore.com')
#    @songwriter_accounts = []
#    @batch_adjustment = TCFactory.build_batch_balance_adjustment
#
#    @songwriter1 = TCFactory.build_person(:name => 'Songwriter 1')
#    TCFactory.build_batch_balance_adjustment_detail(:batch_balance_adjustment => @batch_adjustment,
#                                                    :person_id => @songwriter1.id, :name => 'Songwriter 1', :current_balance => 10.15)
#    TCFactory.build_composer(:person => @songwriter1)
#
#    @songwriter2 = TCFactory.build_person(:name => 'Songwriter 2')
#    TCFactory.build_batch_balance_adjustment_detail(:batch_balance_adjustment => @batch_adjustment,
#                                                    :person_id => @songwriter2.id, :name => 'Songwriter 2', :current_balance => 20.99)
#    TCFactory.build_composer(:person => @songwriter2)
#
#    @songwriter3 = TCFactory.build_person(:name => 'Songwriter 3')
#    TCFactory.build_batch_balance_adjustment_detail(:batch_balance_adjustment => @batch_adjustment,
#                                                    :person_id => @songwriter3.id, :name => 'Songwriter 3', :current_balance => 50)
#    TCFactory.build_composer(:person => @songwriter3)
#
#    expect(@batch_adjustment.status).not_to eq('posted')
#    @posting_period = {:period_interval => 1, :period_year => 2012, :period_type => 'quarterly'}
#    @balance_adjustment_options = {:category => 'Songwriter Royalty',
#                                   :admin_note => 'Songwriter Royalty for 1st Qtr 2012',
#                                   :customer_note => 'Songwriter Royalty for 1st Qtr 2012 Customer Note'
#    }.merge!(@posting_period)
#  end
#
#  it "should mark batch balance adjustment and details as posted" do
#    BatchBalanceAdjustment.post_adjustments(@batch_adjustment.id, @admin, @balance_adjustment_options)
#    expect(@batch_adjustment.reload.status).to eq('posted')
#    expect(@batch_adjustment.details.collect(&:status)).to match_array(['posted', 'posted', 'posted'])
#  end
#
#  it "should post to person's transaction" do
#    BatchBalanceAdjustment.post_adjustments(@batch_adjustment.id, @admin, @balance_adjustment_options)
#    expect(@songwriter1.person_transactions.detect {|t| t.target_type == 'BalanceAdjustment'}.credit).to eq(10.15)
#    expect(@songwriter2.person_transactions.detect {|t| t.target_type == 'BalanceAdjustment'}.credit).to eq(20.99)
#    expect(@songwriter3.person_transactions.detect {|t| t.target_type == 'BalanceAdjustment'}.credit).to eq(50)
#  end
#
#  it "should ignore person that does not match" do
#    expect(Person.find_by_id(9999)).to be_nil
#    detail = TCFactory.build_batch_balance_adjustment_detail(:person_id => 9999, :batch_balance_adjustment => @batch_adjustment)
#    expect{ BatchBalanceAdjustment.post_adjustments(@batch_adjustment.id, @admin, @balance_adjustment_options) }.to raise_error
#    expect(detail.reload.status).not_to eq('posted')
#  end
#
#  it "should add to person's balance" do
#    @songwriter1.person_balance.update_attribute(:balance, 53.15)
#  end
end
