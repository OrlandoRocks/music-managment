require "rails_helper"

describe Distribution::MassRetryWorker do
  describe "#perform" do
    let(:params) do
      { person_id:         1,
        album_ids_or_upcs: ["1", "2"],
        store_ids:         [3],
        metadata_only:     nil,
        retry_in_batches:  nil,
        daily_retry_limit: "",
        remote_ip:         "127.0.0.1" }
    end

    it "should add a job to the queue" do
      expect {
        Distribution::MassRetryWorker.perform_async(params)
      }.to change(Distribution::MassRetryWorker.jobs, :size).by(1)
    end

    it "should call the Distribution::MassRetryForm with the correct args", run_sidekiq_worker: true do
      expect(Distribution::MassRetrier).to receive(:mass_retry).with(params.with_indifferent_access)
      Distribution::MassRetryWorker.perform_async(params)
    end
  end
end

