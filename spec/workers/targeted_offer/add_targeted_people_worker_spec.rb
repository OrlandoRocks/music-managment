require "rails_helper"

describe TargetedOffer::AddTargetedPeopleWorker do
  let(:admin_user) { create(:person) }
  let(:targeted_offer) { create(:targeted_offer) }
  let(:person_ids) { create_list(:person, 2).map(&:id) }
  let(:message_delivery_double) { double }

  before(:example) do
    allow_any_instance_of(TargetedOffer)
      .to receive(:grab_list_from_s3)
      .with("add")
      .and_return(person_ids)

    create(:targeted_product, targeted_offer: targeted_offer)
  end

  it "adds people to Targeted Offer" do
    expect(targeted_offer.targeted_people)
      .to match_array []

    TargetedOffer::AddTargetedPeopleWorker
      .new
      .perform(targeted_offer.id, admin_user.id)

    expect(targeted_offer.reload.targeted_people.pluck(:person_id))
      .to match_array person_ids
  end

  it "sends an email to issuing user on success" do
    expect(TargetedOfferMailer).to receive(:add_people_job_status).and_return(message_delivery_double)
    expect(message_delivery_double).to receive(:deliver_later)

    TargetedOffer::AddTargetedPeopleWorker
      .new
      .perform(targeted_offer.id, admin_user.id)
  end

  it "leaves targeted offer in correct state" do
    TargetedOffer::AddTargetedPeopleWorker
      .new
      .perform(targeted_offer.id, admin_user.id)

    expect(targeted_offer.reload.job_status)
      .to be_nil
  end

  it "leaves targeted offer in correct state if exception thrown" do
    allow(CSV)
      .to receive(:parse)
      .and_raise(Exception.new)

    TargetedOffer::AddTargetedPeopleWorker
      .new
      .perform(targeted_offer.id, admin_user.id)

    expect(targeted_offer.reload.job_status)
      .to be_nil
  end
end
