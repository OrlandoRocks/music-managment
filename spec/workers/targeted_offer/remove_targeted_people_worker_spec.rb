require "rails_helper"

describe TargetedOffer::RemoveTargetedPeopleWorker do
  let(:message_delivery_double) { double }

  before(:each) do
    @admin_user       = create(:person)
    @targeted_offer   = create(:targeted_offer)

    create(:targeted_product, targeted_offer: @targeted_offer)

    @people  = create_list(:person, 2)
    @people.each { |p| create(:targeted_person, person: p, targeted_offer: @targeted_offer ) }

    allow_any_instance_of(TargetedOffer).to receive(:grab_list_from_s3).with("remove").and_return(@people.map(&:id))
  end

  it "removes people from Targeted Offer" do
    expect(@targeted_offer.targeted_people.count).to eq 2

    TargetedOffer::RemoveTargetedPeopleWorker.new.perform(@targeted_offer.id, @admin_user.id)

    expect(@targeted_offer.reload.targeted_people.count).to eq 0
  end

  it "sends an email to issuing user on success" do
    expect(TargetedOfferMailer).to receive(:remove_people_job_status).and_return(message_delivery_double)
    expect(message_delivery_double).to receive(:deliver_later)
    TargetedOffer::RemoveTargetedPeopleWorker.new.perform(@targeted_offer.id, @admin_user.id)
  end


  it "leaves targeted offer in correct state" do
    TargetedOffer::RemoveTargetedPeopleWorker.new.perform(@targeted_offer.id, @admin_user.id)
    expect(@targeted_offer.reload.job_status).to be_nil
  end

  it "leaves targeted offer in correct state if exception thrown" do
    allow(CSV).to receive(:parse).and_raise(Exception.new)
    TargetedOffer::RemoveTargetedPeopleWorker.new.perform(@targeted_offer.id, @admin_user.id)
    expect(@targeted_offer.reload.job_status).to be_nil
  end

end
