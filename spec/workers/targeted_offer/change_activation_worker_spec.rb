require "rails_helper"

describe TargetedOffer::ChangeActivationWorker do
  let(:message_delivery_double) { double }

  before(:each) do
    @admin_user       = create(:person)
    @targeted_offer   = create(:targeted_offer)

    create(:targeted_product, targeted_offer: @targeted_offer)

    @people = create_list(:person, 2)
    @people.each { |p| create(:targeted_person, person: p, targeted_offer: @targeted_offer) }
  end

  context "when activating (active arg = true)" do
    it "changes the status to active" do
      expect(@targeted_offer.status).to eq "Inactive"

      TargetedOffer::ChangeActivationWorker.new.perform(@targeted_offer.id, true, @admin_user.id)

      expect(@targeted_offer.reload.status).to eq "Active"
    end

    it "sends an email on success" do
      expect(TargetedOfferMailer).to receive(:activation_job_status).and_return(message_delivery_double)
      expect(message_delivery_double).to receive(:deliver_later)
      TargetedOffer::ChangeActivationWorker.new.perform(@targeted_offer.id, true, @admin_user.id)
    end

    it "leaves targeted offer in correct state" do
      TargetedOffer::ChangeActivationWorker.new.perform(@targeted_offer.id, true, @admin_user.id)
      expect(@targeted_offer.reload.job_status).to be_nil
    end
  end

  context "when deactivating (active arg = false)" do
    it "changes the status to active" do
      @targeted_offer.update(status: "Active")
      expect(@targeted_offer.reload.status).to eq "Active"

      TargetedOffer::ChangeActivationWorker.new.perform(@targeted_offer.id, false, @admin_user.id)

      expect(@targeted_offer.reload.status).to eq "Inactive"
    end

    it "sends an email on success" do
      expect(TargetedOfferMailer).to receive(:activation_job_status).and_return(message_delivery_double)
      expect(message_delivery_double).to receive(:deliver_later)
      TargetedOffer::ChangeActivationWorker.new.perform(@targeted_offer.id, false, @admin_user.id)
    end

    it "leaves targeted offer in correct state" do
      TargetedOffer::ChangeActivationWorker.new.perform(@targeted_offer.id, false, @admin_user.id)
      expect(@targeted_offer.reload.job_status).to be_nil
    end
  end
end
