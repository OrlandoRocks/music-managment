require "rails_helper"

describe PersonEarningsSummarizationWorker, type: :worker do
  describe "#perform" do
    it "calls TaxBlocking::PersonEarningsSummarizationService for the given user for current year" do
      service = instance_double(TaxBlocking::PersonEarningsSummarizationService, summarize: {})
      expect(TaxBlocking::PersonEarningsSummarizationService).to receive(:new).with(123, nil).and_return(service)
      expect(service).to receive(:summarize)
      PersonEarningsSummarizationWorker.new.perform(123)
    end

    it "calls TaxBlocking::PersonEarningsSummarizationService for a given user and a given year" do
      service = instance_double(TaxBlocking::PersonEarningsSummarizationService, summarize: {})
      expect(TaxBlocking::PersonEarningsSummarizationService).to receive(:new).with(123, "2019").and_return(service)
      expect(service).to receive(:summarize)
      PersonEarningsSummarizationWorker.new.perform(123, "2019")
    end

    it "calls TaxBlocking::PersonEarningsSummarizationService for a given user and a given list of years" do
      service = instance_double(TaxBlocking::PersonEarningsSummarizationService, summarize: {})
      expect(TaxBlocking::PersonEarningsSummarizationService)
        .to receive(:new).with(123, (2019..2021).to_a.map(&:to_s)).and_return(service)
      expect(service).to receive(:summarize)
      PersonEarningsSummarizationWorker.new.perform(123, (2019..2021).to_a.map(&:to_s))
    end
  end
end
