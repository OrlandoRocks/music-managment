require "rails_helper"

describe ArtistMappingWorker do
  before(:each) do
    @person   = FactoryBot.create(:person)
    @artist   = FactoryBot.create(:artist)
    album    = FactoryBot.create(:album)
    FactoryBot.create(:creative, artist_id: @artist.id, creativeable_id: album.id, creativeable_type: 'Album').update(person_id: @person.id)

  end

  let(:without_identifier) {
      {
        "person_id" => @person.id,
        "artist_id" => @artist.id,
        "service_name" => "apple"
      }
    }

  let(:with_identifier) {
      {
        "person_id" => @person.id,
        "artist_id" => @artist.id,
        "service_name" => "apple",
        "identifier" => "12345"
      }
    }
  context "creating an External Service ID when a create request is pending" do
    it "does nothing" do
      ExternalServiceId.create(linkable_type: "Creative", linkable_id: Creative.last.id, service_name: "apple", state: "requested")
      expect_any_instance_of(ArtistMapping::Gatekeeper).to receive(:can_proceed?).and_return(false)
      ArtistMappingWorker.new.perform(without_identifier)
    end
  end

  context "updating and redelivering when an redistributions are pending" do
    it "does nothing" do
      ExternalServiceId.create(linkable_type: "Creative", linkable_id: Creative.last.id, service_name: "apple", state: "matched")
      RedistributionPackage.create(person_id: @person.id, artist_id: @artist.id, state: "pending")
      expect_any_instance_of(ArtistMapping::Gatekeeper).to receive(:can_proceed?).and_return(false)
      ArtistMappingWorker.new.perform(with_identifier)
    end
  end

  context "identifier is provided" do
    it "calls on the ArtistMapping::ArtistIdUpdater" do
      ExternalServiceId.create(linkable_type: "Creative", linkable_id: Creative.last.id, service_name: "apple", state: "processing")
      allow(ArtistMapping::ArtistIdUpdater).to receive(:update_artist_id)
      expect(ArtistMapping::ArtistIdUpdater).to receive(:update_artist_id).with(with_identifier)
      ArtistMappingWorker.new.perform(with_identifier)
    end
  end

  context "identifier is not provided" do
    it "calls on the ArtistMapping::TicketCreator" do
      ExternalServiceId.create(linkable_type: "Creative", linkable_id: Creative.last.id, service_name: "apple", state: "processing")
      allow(ArtistMapping::TicketCreator).to receive(:create_ticket)
      expect(ArtistMapping::TicketCreator).to receive(:create_ticket).with(without_identifier)
      ArtistMappingWorker.new.perform(without_identifier)
    end
  end
end
