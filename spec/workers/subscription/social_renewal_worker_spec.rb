require "rails_helper"

describe Subscription::SocialRenewalWorker do
  describe "#perform", run_sidekiq_worker: true do
    let(:person)    { create(:person, :with_braintree_blue_stored_credit_card, :with_tc_social) }

    before(:each) do
      date = Time.local(2017, 05, 01, 12, 0, 0) # Monday
      Timecop.freeze(date)
      allow_any_instance_of(Believe::ApiClient).to receive(:post).and_return(true)
      allow(Person::AfterCommitCallbacks).to receive(:send_to_zendesk).and_return(true)
      allow($social_api_client).to receive(:post).and_return(double({status: 200, body: {status: "success"}.to_json}))
      @job = double("mail @job")
    end

    after do
      Timecop.return
    end

    context "Tunecore payment channel" do
      it "sends the renewal payload to TCS" do
        status = create(:person_subscription_status,
                        subscription_type: "Social",
                        person: person, termination_date: Date.today + 1.month)
        processor = double("processor", renewed: [person], declined: [])
        allow(Subscription::Renewal::BulkProcessor).to receive(:renew).and_return(processor)
        expect(Social::PlanStatusUpdater).to receive(:update).with([person])
        Subscription::SocialRenewalWorker.new.perform("Tunecore")
      end

      it "sends initial email for person's in grace period" do
        status = create(:person_subscription_status,
                        subscription_type: "Social",
                        person: person, termination_date: Date.today)
        processor = double("processor", renewed: [], declined: [person])
        allow(Subscription::Renewal::BulkProcessor).to receive(:renew).and_return(processor)
        expect(SocialNotifierWorker).to receive(:perform_async).with(:tunecore_initial_grace_period, [person.id])
        Subscription::SocialRenewalWorker.new.perform("Tunecore")
      end

      it "sends second email for person's in grace period" do
        status = create(:person_subscription_status,
                        person: person,
                        subscription_type: "Social",
                        termination_date: 2.days.ago)
        processor = double("processor", renewed: [], declined: [person])
        allow(Subscription::Renewal::BulkProcessor).to receive(:renew).and_return(processor)
        expect(SocialNotifierWorker).to receive(:perform_async).with(:tunecore_second_grace_period, [person.id])
        Subscription::SocialRenewalWorker.new.perform("Tunecore")
      end

      it "sends final email for person's in grace period" do
        status = create(:person_subscription_status,
                        subscription_type: "Social",
                        person: person,
                        termination_date: 5.days.ago)
        processor = double("processor", renewed: [], declined: [person])
        allow(Subscription::Renewal::BulkProcessor).to receive(:renew).and_return(processor)
        expect(SocialNotifierWorker).to receive(:perform_async).with(:tunecore_final_grace_period, [person.id])
        Subscription::SocialRenewalWorker.new.perform("Tunecore")
      end

      it "doesn't send email on 2nd day of person's grace period" do
        status = create(:person_subscription_status,
                        subscription_type: "Social",
                        person: person,
                        termination_date: 1.day.ago)
        processor = double("processor", renewed: [], declined: [person])
        allow(Subscription::Renewal::BulkProcessor).to receive(:renew).and_return(processor)

        expect(SocialNotifierWorker).not_to receive(:perform_async).with(:tunecore_initial_grace_period,[person.id])
        expect(SocialNotifierWorker).not_to receive(:perform_async).with(:tunecore_second_grace_period,[person.id])
        expect(SocialNotifierWorker).not_to receive(:perform_async).with(:tunecore_final_grace_period,[person.id])
        Subscription::SocialRenewalWorker.new.perform("Tunecore")
      end

      it "doesn't send email on 4nd day of person's grace period" do
        status = create(:person_subscription_status,
                        person: person,
                        subscription_type: "Social",
                        termination_date: 3.day.ago)
        processor = double("processor", renewed: [], declined: [person])
        allow(Subscription::Renewal::BulkProcessor).to receive(:renew).and_return(processor)

        expect(SocialNotifierWorker).not_to receive(:perform_async).with(:tunecore_initial_grace_period,[person.id])
        expect(SocialNotifierWorker).not_to receive(:perform_async).with(:tunecore_second_grace_period,[person.id])
        expect(SocialNotifierWorker).not_to receive(:perform_async).with(:tunecore_final_grace_period,[person.id])
        Subscription::SocialRenewalWorker.new.perform("Tunecore")
      end
    end

    context "Apple payment channel" do
      it "sends the renewal payload to TCS" do
        status = create(:person_subscription_status,
                        subscription_type: "Social",
                        person: person, termination_date: Date.today + 1.month, grace_period_length: 0)
        processor = double("processor", renewed: [person], declined: [])
        allow(Subscription::Renewal::BulkProcessor).to receive(:renew).and_return(processor)
        expect(Social::PlanStatusUpdater).to receive(:update).with([person])
        Subscription::SocialRenewalWorker.new.perform("Apple")
      end

      it "sends final email for person's grace period" do
        status = create(:person_subscription_status,
                        person: person,
                        subscription_type: "Social",
                        termination_date: Date.today,
                        grace_period_length: 0)
        processor = double("processor", renewed: [], declined: [person])
        allow(Subscription::Renewal::BulkProcessor).to receive(:renew).and_return(processor)
        expect(SocialNotifierWorker).to receive(:perform_async).with(:apple_final_grace_period, [person.id])
        Subscription::SocialRenewalWorker.new.perform("Apple")
      end
    end
  end
end
