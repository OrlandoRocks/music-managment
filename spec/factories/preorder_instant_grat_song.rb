module TCFactory
  def preorder_instant_grat_song_defaults(options = {})
    options[:salepoint_preorder_data] = TCFactory.create_ready_preorder_data if options[:salepoint_preorder_data].blank?
    options[:song] = options[:salepoint_preorder_data].salepoint.salepointable.songs.first if options[:song].blank?

    options
  end
end
