module TCFactory
  def distribution_song_defaults(options = {})
    {
      distribution: TCFactory.build_distribution,
      state: 'new',
      salepoint_song: TCFactory.create(:salepoint_song)
    }.merge(options)
  end
end
