module TCFactory
  def muma_song_society_defaults(options = {})
    song = options[:muma_song] || TCFactory.create(:muma_song)

    {
    :song_code=>song.code,
    :society_code=>"BMI",
    :registered_at=>Time.now,
    :work_num=>"abc123",
    }.merge(options)
  end
end

