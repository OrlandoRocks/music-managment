module TCFactory
  def upc_defaults(options = {})
    {
      tunecore_upc: true,
      inactive: false
    }.merge(options)
  end
end
