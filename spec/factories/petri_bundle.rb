
module TCFactory
  def petri_bundle_defaults(options = {})
    {
      :album=>options[:album] || TCFactory.build_album
    }.merge(options)
  end
end
