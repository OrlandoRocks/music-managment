module TCFactory
  def salepoint_song_defaults(options = {})
    defaults = {
      state: "new"
    }.merge(options)

    defaults[:salepoint] = TCFactory.create(:salepoint) if defaults[:salepoint].blank?
    defaults[:song] = TCFactory.create(:song, album: defaults[:salepoint].salepointable) if defaults[:song].blank?
    defaults
  end
end
