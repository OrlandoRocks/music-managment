
module TCFactory
  def annual_renewal_event_defaults(options = {})
    {
      :album => TCFactory.build_album(),
      :event_type => 'BeginSubscription',
      :occurred_on => Date.today
    }.merge(options)
  end
end
