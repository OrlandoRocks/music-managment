module TCFactory
  def upload_defaults(options = {})
    {
      :bitrate => "1 million kps",
      :uploaded_filename => "#{rand(9999999)}.wav",
      :original_filename => "upload.wav",
    }.merge(options)
  end
end
