module TCFactory
  def check_transfer_defaults(options = {})
    person = Person.first
    person.person_balance.update_attribute(:balance,100000)
    {
      :transfer_status=>"pending",
      :person=>person,
      :payment_cents=>10000,
      :admin_charge_cents=>300,
      :current_balance_cents=>100000,
      :payee=>"Johnny Appleseed",
      :address1=>"45 Main Street",
      :city=>"Brooklyn",
      :state=>"New York",
      :postal_code=>11215,
      :country=>"US",
      :password_entered=>"Testpass123!",
      :person_transaction=>build(:person_transaction, :person=>person)
    }.merge(options)
  end
end
