
module TCFactory
  def sip_stores_display_group_defaults(options = {})
    {
      :name => "iTunes U.S.",
      :sort_order => 1,
      :group_on_status => 1,
    }.merge(options)
  end
end
