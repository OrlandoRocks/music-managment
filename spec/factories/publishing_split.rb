
module TCFactory
  def publishing_split_defaults(options={})
    {:composer => options[:composer] || TCFactory.create(:composer),
      :composition => options[:composition] || TCFactory.create(:composition),
      :percent => 25
    }.merge(options)
  end
end
