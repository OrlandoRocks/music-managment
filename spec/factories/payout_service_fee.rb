module TCFactory

  def payout_service_fee_defaults(options = {})
    {
      :fee_type => "eft_service",
      :amount => 1.50,
      :country_website_id => 1,
      :currency => 'USD'
    }.merge(options)
  end

end
