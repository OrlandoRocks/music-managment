module TCFactory
  def lod_history_defaults(options = {})
    {
      :status => 'scheduled_to_send',
      :updated_by => 'admin@tunecore.com',
      :lod => TCFactory.create(:lod)
    }.merge(options)
  end
end
