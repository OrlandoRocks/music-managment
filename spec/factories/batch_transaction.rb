module TCFactory
  def batch_transaction_defaults(options = {})
    braintree_transaction = options[:braintree_transaction] || create(:braintree_transaction)
    invoice = options[:invoice] || create(:invoice)
    payment_batch = options[:payment_batch] || create(:payment_batch)
    {
      :payment_batch => payment_batch,
      :invoice => invoice,
      :amount => '10.00',
      :currency => "USD",
      :matching => braintree_transaction
    }.merge(options)
  end
end
