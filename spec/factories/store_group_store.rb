

module TCFactory

  def store_group_store_defaults(options={})
    options.merge({
      :store => build(:store),
      :store_group => build(:store_group)
    })
  end

end
