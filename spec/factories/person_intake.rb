
module TCFactory
  def person_intake_defaults(options = {})
    {
      :amount => 0.70,
    }.merge(options)
  end
end
