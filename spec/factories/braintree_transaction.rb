module TCFactory
  def braintree_transaction_defaults(options = {})
    person = create(:person)
    {
      :country_website_id => 1,
      :person => person,
      :invoice=> create(:invoice, :person=>person),
      :response_code => 201,
      :amount => 9.99,
      :ip_address => '192.168.2.1',
      :processor_id => 'ccprocessora',
      :currency=>'USD'
    }.merge(options)
  end
end
