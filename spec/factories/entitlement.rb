module TCFactory
  def entitlement_defaults(options = {})
    {:entitlement_rights_group_id => 1}.merge(options)
  end
end
