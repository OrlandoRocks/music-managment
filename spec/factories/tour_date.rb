
module TCFactory
  def tour_date_defaults(options={})
     person = build_person(:email => "eddie@tunecore.com") unless options.include?(:person)
     artist = build_artist() unless options.include?(:artist)
     country = build_country() unless options.include?(:country)
     us_state = build_us_state() unless options.include?(:us_state)
    {
      :person => person,
      :artist => artist,
      :country => country,
      :us_state => us_state,
      :city => "NYC",
      :venue => "Madison Square Garden",
      :event_date_at => Time.now,
      :event_time_at => Time.now,
      :information => "This is the greatest tour date ever"
    }.merge(options)
  end
end
