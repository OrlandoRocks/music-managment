
module TCFactory
  def composition_defaults(options={})
    if options[:song]
      name = options[:song].name
      options[:songs] = [options.delete(:song)]
    end

    {:name => name || 'My own song',
     :is_unallocated_sum => 0,
     :cwr_name => 'My own song',
     :state => 'new'
    }.merge(options)
  end

  def create_compositions(composer)
    compositions = []
    Composition.states.each_with_index do |state_array, i|
      song = TCFactory.build_song(:name => "Composition #{i}",:album => TCFactory.build_album(:person => composer.person, :finalized_at => Time.now, :is_deleted => false))
      composition = TCFactory.build_composition(:song => song)
      TCFactory.build_publishing_split(:composer => composer, :composition => composition)
      composition.update(:state => state_array.first, :state_updated_on => Time.now)
      compositions << composition
    end
    return compositions
  end

end
