module TCFactory
  def soundout_report_defaults(options = {})
    defaults = {
      :retrieval_attempts=>0,
      :report_data=>nil,
      :status=>"new",
      track_type: "Song"
    }.merge(options)

    # Add an soundout_product if necessary
    defaults[:soundout_product] = TCFactory.create(:soundout_product) if defaults[:soundout_product].blank? && defaults[:soundout_product_id].blank?
    defaults[:track] = TCFactory.create(:song, album: FactoryBot.create(:album, finalized_at: Time.now)) if defaults[:track_id].blank? && defaults[:track].blank?
    defaults[:track].stub(:duration).and_return(95000)
    defaults[:track].s3_asset = TCFactory.create(:s3_asset)
    defaults[:person] = FactoryBot.create(:person) unless defaults[:person]

    defaults
  end
end
