module TCFactory
  def product_defaults(options = {})
    if !options.include?(:created_by)
      created_by = (Person.find_by(email: "admin@tunecore.com") || TCFactory.build_person({
        :name => 'admin user',
        :email => 'admin@tunecore.com',
        }))
    end

    {
      :created_by => created_by,
      :name => "Album Distribution (Ad Hoc)",
      :display_name => "Customer Facing Name",
      :description => "Album distribution product for customers who haven't pre-paid.",
      :status => "Active",
      :product_type => "Ad Hoc",
      :country_website_id => 1,
      :applies_to_product => "Album",
      :price => 0.00,
      :currency => "USD"
    }.merge(options)
  end
end
