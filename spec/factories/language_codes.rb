
module TCFactory
  def language_code_defaults(options={})
    {:description => "English", :code => "en"}.merge(options)
  end
end
