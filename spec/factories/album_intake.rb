
module TCFactory
  def album_intake_defaults(options ={})
#    album = build_album() unless options.include?(:album)
#    person = Person.first || build_person() unless options.include?(:person)
#    reporting_month = build_reporting_month() unless options.include?(:reporting_month)
#    person_intake = build_person_intake() unless options.include?(:person_intake)
#    {
#      :album => album,
#      :person => person,
#      :reporting_month => reporting_month,
#      :person_intake => person_intake,
#    }.merge(options)

    album_intake_options(options)
  end

  def album_intake_options(options = {})
    {
      :songs_sold => 1,
      :songs_streamed => 1,
      :albums_sold => 1,
      :usd_total_cents => 1
    }.merge(options)
  end

end
