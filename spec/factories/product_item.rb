# +--------------+------------------+------+-----+---------+----------------+
# | Field        | Type             | Null | Key | Default | Extra          |
# +--------------+------------------+------+-----+---------+----------------+
# | id           | int(10) unsigned | NO   | PRI | NULL    | auto_increment | 
# | product_id   | int(10) unsigned | NO   | MUL | NULL    |                | 
# | name         | varchar(150)     | NO   |     | NULL    |                | 
# | description  | varchar(255)     | YES  |     | NULL    |                | 
# | price        | decimal(10,2)    | NO   |     | 0.00    |                | 
# | updated_at   | timestamp        | YES  |     | NULL    |                | 
# | created_at   | timestamp        | YES  |     | NULL    |                | 
# +--------------+------------------+------+-----+---------+----------------+

module TCFactory
  def product_item_defaults(options = {})
    {
      :name => "Album Distribution",
      :price => 49.99,
      :currency => "USD"
    }.merge(options)
  end
end
