
module TCFactory
  def us_state_defaults(options={})
    {
      :name => "NEW YORK",
      :abbreviation => "NY"
    }.merge(options)
  end
end
