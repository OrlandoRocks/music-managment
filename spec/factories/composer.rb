
module TCFactory
  def composer_defaults(options={})
    if options[:person_id]
      options[:person] = Person.find(options[:person_id])
    end
    {
      :city=>"Brooklyn",
      :zip=>"11201",
      :alternate_email=>"",
      :dob=>"04/01/1999",
      :country=>"US",
      :address_1=>"55 Washington St",
      :cae=>"123456789",
      :phone=>"555-333-9999",
      :address_2=>"", :name_suffix=>"",
      :last_name=>"Cheung",
      :name_prefix=>"",
      :alternate_phone=>"",
      :agreed_to_terms_at=>"07/01/2011",
      :state=>"NY",
      :email=>"ed@tunecore.com",
      :middle_name=>"",
      :first_name=>"Ed",
      :person => options[:person] || TCFactory.build_person,
      :agreed_to_terms => true,
      :performing_rights_organization_id => "1"
    }.merge(options)
  end
end
