
module TCFactory
  def s3_asset_defaults(options={})
    {
     :key => "random_key",
     :bucket => "s3_bucket"
    }.merge(options)
  end
end
