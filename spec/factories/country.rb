module TCFactory
  def country_defaults(options={})
    {
      :name => "United States",
      :iso_code => "US",
      :region => "North America",
      :sub_region => "Northern America",
      :tc_region => "NMER",
      :iso_code_3 => "USA"
    }.merge(options)
  end
end
