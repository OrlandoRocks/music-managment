module TCFactory
  def salepoint_preorder_data_defaults(options = {})
    defaults = {
      start_date: Date.today + 11.days,
      preorder_purchase: TCFactory.create(:preorder_purchase)
    }.merge(options)

    defaults[:salepoint] = TCFactory.create(:salepoint) if defaults[:salepoint].blank?
    defaults
  end

  def create_ready_preorder_data
    person = TCFactory.create(:person)
    album = TCFactory.create(:album, person: person)
    song1 = TCFactory.create(:song, album: album)
    song2 = TCFactory.create(:song, album: album)
    song3 = TCFactory.create(:song, album: album)
    song4 = TCFactory.create(:song, album: album)
    song5 = TCFactory.create(:song, album: album)
    song6 = TCFactory.create(:song, album: album)
    song7 = TCFactory.create(:song, album: album)
    song8 = TCFactory.create(:song, album: album)
    salepoint = TCFactory.create(:salepoint, :salepointable_type => "Album", :salepointable_id => album.id)
    salepoint.update_attribute(:store_id, 36)
    TCFactory.create(:salepoint_preorder_data, salepoint: salepoint, start_date: Date.today + 1.days)
  end
end
