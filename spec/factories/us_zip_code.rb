
module TCFactory
  def us_zip_code_defaults(options={})
    {
      :code => "65201",
      :city => "Sedalia", 
      :state => "MO"
    }
  end
end
