module TCFactory
  def friend_referral_defaults(options = {})
    {
      :referrer => options[:referer] || TCFactory.build(:person),
      :referee => options[:referee] || TCFactory.build(:person),
      :invoice => options[:invoice] || TCFactory.build(:invoice),
      :commission => 5,
      :balance => 30,
      :status => 'new',
      :campaign_id => 411,
      :posting => options[:posting] || TCFactory.build(:person_transaction),
      :raw_response => nil
    }.merge(options)
  end
end
