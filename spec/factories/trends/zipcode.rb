module TCFactory
  def zipcode_defaults(options = {})
    {
      :zipcode=>"11215",
      :state=>"NY",
      :city_mixed_case=>"Park Slope",
      :city=>"Brooklyn",
      :primary_record=>"P",
      :cbsa=>1,
      :cbsa_name=>"New York-Northern New Jersey-Long Island NY-NJ-PA",
      :cbsa_short_name=>"New York NY"
    }.merge(options)
  end
end
