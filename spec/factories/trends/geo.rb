module TCFactory
  def geo_defaults(options = {})
    {
      :trend_data_summary_id => options[:trend_data_summary_id] || 1,
      :country_code => options[:country_code] || 'US',
      :cbsa => options[:cbsa] || '1',
      :qty => options[:qty] || 1,
      :date => options[:date] || Date.today-1.month,
      :created_at => Time.now,
      :updated_at => Time.now + 60
    }.merge(options)
  end
end
