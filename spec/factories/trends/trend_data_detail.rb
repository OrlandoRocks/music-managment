module TCFactory
  def trend_data_detail_defaults(options = {})
    album = options[:album]   || Album.first
    artist = options[:artist] || Artist.first || FactoryBot.create(:artist)

    trans_type_id = options[:trans_type_id] || TransType.album_download.try(:id)
    if trans_type_id.blank?
      TCFactory.create(:trans_type, :name=>"Album")
      TCFactory.create(:trans_type, :name=>"Song")
      TCFactory.create(:trans_type, :name=>"Stream")
      TCFactory.create(:trans_type, :name=>"Ringtone")
      trans_type_id = TransType.album_download.id
    end

    defaults = {
      :person_id => album.person.id,
      :sale_date => options[:sale_date] || Date.today-1.month,
      :country_code => options[:country_code] || 'US',
      :zip_code => options[:zip_code] || '11238',
      :qty => options[:qty] || 1,
      :artist_name => artist.name,
      :artist_id => artist.id,
      :album_name => album.name,
      :album_id => album.id,
      :album_type => 'Album',
      :song_name => nil,
      :song_id => 0,
      :tunecore_isrc => nil,
      :optional_isrc => nil,
      :upc => "859707867658",
      :trans_type_id => trans_type_id,
      :provider_id => options[:provider_id] || Provider.first.id || TCFactory.build(:provider).id,
      :royalty_currency_id => "USD",
      :royalty_price => 1.00,
      :customer_price => 2.00,
      :is_promo => 0,
      :import_log => TCFactory.build(:import_log),
      :trend_data_summary_id => 1
    }

    if options[:song]
      defaults[:song_id] = options[:song].id
      defaults[:song_name] = options[:song].name
    end

    options.delete(:album)
    options.delete(:song)

    defaults.merge(options)
  end
end
