module TCFactory
  def trend_data_summary_defaults(options = {})
    trend_data_detail = options[:trend_data_detail] || TCFactory.build(:trend_data_detail)
    options.delete(:trend_data_detail)
    {
      :date => trend_data_detail.sale_date,
      :person_id => trend_data_detail.person_id,
      :album_id => trend_data_detail.album_id,
      :song_id => trend_data_detail.song_id,
      :trans_type_id => trend_data_detail.trans_type_id,
      :qty => options[:qty] || trend_data_detail.qty,
      :provider_id => trend_data_detail.provider_id,
      :created_at => Time.now,
      :updated_at => Time.now + 60
    }.merge(options)
  end
end
