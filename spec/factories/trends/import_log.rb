module TCFactory
  def import_log_defaults(options = {})
    {
      :started_at => Time.now-1.minute,
      :ended_at => Time.now,
      :file_name => "abcdefg.txt",
      :provider_id => 1
    }.merge(options)
  end
end
