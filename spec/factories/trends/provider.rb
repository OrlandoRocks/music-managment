module TCFactory
  def provider_defaults(options = {})
    {
      :name => "iTunes"
    }.merge(options)
  end
end
