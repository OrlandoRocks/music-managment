module TCFactory 

  def muma_song_ip_defaults(options = {})
    muma_song = options[:muma_song]      || TCFactory.create(:muma_song)
    options.delete(:muma_song)
    
    {
      :song_code=>muma_song.code,
      :ip_code=>"TUNE",
      :ip_chain=>1,
      :ip_c_or_p=>"C",
      :controlled=>1,
      :mech_owned_share=>100,
      :composer_first_name=>"John",
      :composer_last_name=>"Rambo"
    }.merge(options)
  end

end

