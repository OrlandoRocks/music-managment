module TCFactory
  def targeted_person_defaults(options = {})
    {
      :person => build(:person),
      :targeted_offer => build(:targeted_offer),
      :used => 0

      }.merge(options)
  end
end
