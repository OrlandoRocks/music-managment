
module TCFactory

  def invoice_settlement_defaults(options = {})
    {
      :invoice => build(:invoice),
      :created_at => Time.now,
      :settlement_amount_cents => 2999
    }.merge(options)
  end

end
