

module TCFactory

  def person_transaction_defaults(options={})
    {
      :created_at => nil,
      :person => create(:person),
      :debit => 10.00,
      :credit => 0,
      :previous_balance => 0,
      :target_id => nil,
      :target_type => nil,
      :comment => "A Person Transaction Comment."
    }.merge(options)
  end

end

