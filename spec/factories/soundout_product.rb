module TCFactory
  def soundout_product_defaults(options = {})
    defaults = {
      report_type: "standard",
      display_name: "SoundOut Standard Report",
      number_of_reviews: 40,
      available_in_days: 5,
    }.merge(options)

    # Add a product if necessary
    defaults[:product] = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID) if defaults[:product].blank? and defaults[:product_id].blank?

    defaults
  end
end
