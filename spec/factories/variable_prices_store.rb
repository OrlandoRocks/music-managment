module TCFactory
  def variable_prices_store_defaults(options={})
    {
      :store => build(:store),
      :variable_price => build(:variable_price),
      :is_active => true
    }.merge(options)
  end
end
