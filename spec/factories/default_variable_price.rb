module TCFactory
  def default_variable_price_defaults(options={})
    {
      :variable_prices_store => (variable_prices_store = build(:variable_prices_store)),
      :store => variable_prices_store.store,
    }.merge(options)
  end
end
