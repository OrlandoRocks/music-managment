module TCFactory
  def salepoint_subscription_defaults(options = {})
    {
      :is_active => true
    }.merge(options)
  end
end
