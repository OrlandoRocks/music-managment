
module TCFactory
  def album_price_policy_defaults(options = {})
    {
      :base_price_cents => 998,
      :price_calculator => 'current_album',
      :description => 'Album',
      :short_code => 'album'
    }.merge(options)
  end
end
