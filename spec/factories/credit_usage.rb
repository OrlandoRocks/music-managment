module TCFactory
  def credit_usage_defaults(options = {})
    applies_type = options[:related].class.name if options[:related]
    {
      :applies_to_type => applies_type
    }.merge(options)
  end
end
