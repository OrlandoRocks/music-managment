module TCFactory
  def targeted_offer_defaults(options = {})
    {
      :created_by_id => 1,
      :country_website_id=> "1",
      :name => "Album Offer $10 off",
      :status => 'Active',
      :offer_type => 'existing',
      :start_date => Time.now,
      :expiration_date => Time.now + 30.days,
      :date_constraint => "expiration",
      :population_criteria_count => 0,
      :targeted_population_count => 0,
      :product_show_type => "price_override",
      :usage_limit => 0
      }.merge(options)
  end
end
