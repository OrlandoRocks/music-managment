#
#
#
#
#
module TCFactory

  def single_defaults(options={})
    options[:creatives] ||= [{"name"=>"artist", "role"=>"primary_artist"}, {"name"=>"Test", "role"=>"featuring"}]
    options = album_defaults(options)
    options[:songs] = [build(:song, :name=>options[:name])]
    options
  end

end
