module TCFactory
  def person_preference_defaults(options = {})
    {
      pay_with_balance: 1
    }.merge(options)
  end
end
