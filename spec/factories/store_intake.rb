
module TCFactory
  def store_intake_defaults(options ={})
    store = Store.ITUNES_US unless options.include?(:store)
    reporting_month = build_reporting_month() unless options.include?(:reporting_month)
    unique_id = 'something_unique' unless options.include?(:unique_id)
    {
      :ruby_type => :StoreIntakeDefault,
      :store => store,
      :reporting_month => reporting_month,
      :local_currency => 'USD',
      :exchange_symbol => 'USDUSD',
      :local_total_cents => 0,
      :usd_actual_cents => 99,
      :usd_total_cents => 99,
      :usd_payout_cents => 99,
      :unique_id => rand.to_s #yeah, ugly      
    }.merge(options)
  end
end
