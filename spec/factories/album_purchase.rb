
module TCFactory
  def album_purchase_defaults(options = {})
    {
      :invoice_id => build(:invoice),
      :person => build(:person),
      :cost_cents => 99998,
      :related => build(:album),
      :related_type => "Album",
      :discount_cents => 0
    }.merge(options)
  end
end

