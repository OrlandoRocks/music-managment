module TCFactory
  def lod_defaults(options = {})
    {
      :document_guid => '2W4BZGIJELYE5AYJ6ENUCG',
      :template_guid => 'Z83SVPJBYLFAYCTA2HXKI4',
      :last_status => 'scheduled_to_send',
      :last_status_at => Time.now,
      :last_status_by => 'admin@tunecore.com'
    }.merge(options)
  end
end
