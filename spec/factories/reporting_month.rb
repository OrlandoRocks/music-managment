
module TCFactory
  def reporting_month_defaults(options = {})
    {
      :report_year => Time.now.year,
      :report_month => Time.now.month,
      :report_date => Time.now
    }.merge(options)
  end
end
