module TCFactory
  def country_website_defaults(options = {})
    {
      :name => 'TuneCore 1',
      :currency => 'USD',
      :country => 'US'
    }.merge(options)
  end
end
