

module TCFactory

  module Base

    def build_soundout_report_product(item_price = 15.00)
      Product.find(Product::US_SOUNDOUT_PRODUCT_IDS.first)
    end

    def build_ad_hoc_album_product(item_price = 29.99)
      Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
    end

    def build_ad_hoc_salepoint_product(item_price = 0.00)
      Product.find(Product::US_MONTHLY_ALBUM_PRODUCT_ID)
    end

    def build_ad_hoc_salepoint_subscription_product(item_price = 10.00)
      Product.find(Product::US_STORE_AUTOMATOR_ID)
    end

    def build_ad_hoc_booklet_product(item_price = 0.00)
      Product.find(Product::US_BOOKLET)
    end

    def build_ad_hoc_preorder_album_product(item_price = 0.00)
      Product.find(Product::US_PREORDER_ID)
    end

    def build_album_credit_product(count)
      Product.find(Product::US_ONE_ALBUM_CREDIT_PRODUCT_ID)
    end

    def build_ad_hoc_credit_usage_product
      Product.find(Product::US_CREDIT_USAGE)
    end

    def generate_album_product(item_price = 29.99, options={})
      return Product.find(Product::US_MONTHLY_ALBUM_PRODUCT_ID) if options[:renewal_interval] == "month"
      Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
    end

    def generate_album_purchase(customer, purchase=true, product_options={})
      product = product_options[:product] || generate_album_product(29.95, product_options)

      album = TCFactory.create(:album,
                               :person => customer,
                               :known_live_on => nil,
                               :finalized_at => nil)

      store = Store.first
      if store.variable_prices.empty?
        salepoint = TCFactory.create(:salepoint,
                                     :salepointable => album,
                                     :store => store,
                                     :has_rights_assignment => true)
      else
        salepoint = TCFactory.create(:salepoint,
                                     :salepointable => album,
                                     :store => store,
                                     :has_rights_assignment => true,
                                     :variable_price_id => store.variable_prices.first.id)
      end

      song1 = TCFactory.create(:song, :album => album)
      song2 = TCFactory.create(:song, :album => album)
      song3 = TCFactory.create(:song, :album => album)
      TCFactory.create(:upload, :song => song1)
      TCFactory.create(:upload, :song => song2)
      TCFactory.create(:upload, :song => song3)
      invoice = Invoice.factory(customer, Product.add_to_cart(customer, album, product) )
      Purchase.process_purchases_for_invoice(invoice) if purchase
    end

  end

  def generate_songwriter_product
    @product_name = SONGWRITER_PRODUCT_NAME
    @display_name = "Songwriter Service"
    @description =  "Collecting, licensing and policing your songwriter copyrights"
    unless Product.find_by(id: Product::SONGWRITER_SERVICE_ID)
      product = Product.new(:name => @product_name,
        :display_name => @display_name,
        :description => @description,
        :status => 'Active',
        :product_type => 'Ad Hoc',
        :is_default => true,
        :sort_order => 1,
        :renewal_level => 'None',
        :price => 75.00,
        :currency => 'USD',
        :created_by_id => 15536,
        :applies_to_product => 'Composer',
        :country_website_id => 1,
        :original_product_id => 0)
      product.id = Product::SONGWRITER_SERVICE_ID
      product.save!

      product_item = ProductItem.create(:product => product,
        :name => @product_name,
        :price=>75.00,
        :currency => 'USD')
      product_item_rule = ProductItemRule.create(:product_item => product_item,
        :inventory_type => 'Composer',
        :rule_type => 'price_for_each',
        :rule => 'price_for_each',
        :quantity => 1,
        :price => 75.00,
        :currency => 'USD')
    end
  end

  def create_standard_test_users
    Person.skip_callback(:validation, :before, :assign_country_website_on_create)
    Person.skip_callback(:save, :before, :crypt_password)

    a  = TCFactory.create(:person, email: "admin@tunecore.com",       name: "Tunecore Admin",    country: "United States", postal_code: "11215", country_website_id: 1, salt: "69729dda93014f6c2c667797ac67d204619840801510f1b9bfd08b6c1ab6e507", invite_code: "d694d30688382ad5cde53645da676c2490112644", password: "8f3998f5625be1dcefdf882b5b1b2bfbcfc702797d8af49e9dd73727272a8a29!", :recent_login=>Time.now ) # 'Testpass123!'
    sa = TCFactory.create(:person, email: "super_admin@tunecore.com", name: "Tunecore Admin",    country: "United States", postal_code: "11215", country_website_id: 1, salt: "69729dda93014f6c2c667797ac67d204619840801510f1b9bfd08b6c1ab6e507", invite_code: "d694d30688382ad5cde53645da676c2490112644", password: "8f3998f5625be1dcefdf882b5b1b2bfbcfc702797d8af49e9dd73727272a8a29!", :recent_login=>Time.now ) # 'Testpass123!'
    c  = TCFactory.create(:person, email: "customer@tunecore.com",    name: "Tunecore Customer", country: "United States", postal_code: "11215", country_website_id: 1, salt: "69729dda93014f6c2c667797ac67d204619840801510f1b9bfd08b6c1ab6e507", invite_code: "d694d30688382ad5cde53645da676c2490112644", password: "8f3998f5625be1dcefdf882b5b1b2bfbcfc702797d8af49e9dd73727272a8a29!", :recent_login=>Time.now ) # 'Testpass123!'

    Person.set_callback(:validation, :before, :assign_country_website_on_create)
    Person.set_callback(:save, :before, :crypt_password)

    sa.roles << Role.where("name=?", "Admin").first
    sa.roles << Role.where("name=?", "Refunds").first
    sa.roles << Role.where("name=?", "Lock").first
    sa.roles << Role.where("name=?", "Assign").first
    sa.roles << Role.where("name=?", "Payout Service").first
    sa.roles << Role.where("name=?", "EFT").first
    sa.roles << Role.where("name=?", "Certs").first
    sa.roles << Role.where("name=?", "CMS").first
    sa.roles << Role.where("name=?", "Targeted Offers").first
    sa.roles << Role.where("name=?", "Publishing Manager").first
    sa.roles << Role.where("name=?", "YTM Approver").first

    a.roles << Role.where("name=?", "Admin").first
    a.roles << Role.where("name=?", "Certs").first
    a.roles << Role.where("name=?", "Targeted Offers").first
  end

  #
  # Use this factory method to summarize the sales_record_master.  Assumes
  # That all sales_records for the master have already been created
  #
  def summarize_sales_record_master(sales_record_master)
       sql = %Q{
      INSERT into sales_record_summaries
      SELECT person_id, sales_record_master_id, related_type, related_id,
        IF( related_type = 'Video', 'Video', IF( related_type = 'Song', (select album_type from songs s inner join albums a on a.id = s.album_id where s.id = related_id), (select album_type from albums where albums.id = related_id))),
        IF( related_type = 'Video', related_id, if( related_type = 'Album', related_id, (select album_id from songs where songs.id = related_id))),
        SUM(IF(distribution_type='Download',quantity,0)), SUM(IF(distribution_type='Streaming',quantity,0)) AS streams_sold,
        SUM(IF(distribution_type='Download',amount,0)),
        SUM(IF(distribution_type='Streaming',amount,0)),
        person_intake_id
      FROM sales_records
      WHERE sales_records.sales_record_master_id = #{sales_record_master.id}
      GROUP BY person_id, related_type, related_id, sales_record_master_id, person_intake_id}
      SalesRecordMaster.connection.execute(sql)
      sales_record_master.update(:summarized=>1)
  end

end
