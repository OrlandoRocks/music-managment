module TCFactory
  def advertisement_defaults(options = {})
    {
      :created_by_id => 1,
      :name => "Album Credit Ad",
      :status => 'Active',
      :category => Advertisement::CATEGORY_ALBUM,
      :thumbnail_title => 'Get a Credit',
      :sidebar_title => 'Album Not Finished?',
      :wide_title => 'Need More Time to Finish Your Album?',
      :display_price => 49.99,
      :display_price_modifier => nil,
      :display_price_notes => 'Reg. $59.99.',
      :thumbnail_copy => 'Buy This',
      :thumbnail_link_text => "Buy This",
      :sidebar_copy => 'Buy This',
      :sidebar_link_text => 'Buy This',
      :wide_copy => 'Buy This',
      :thumbnail_image => 'test.png',
      :sidebar_image => 'test.png',
      :wide_image => 'test.png',
      :wide_link_text => 'But This',
      :image_caption => 'Buy it now',
      :action_link => '/products/buy/16',
      :country_website_id => 1,
      :sort_order => 0,
      }.merge(options)
  end
end
