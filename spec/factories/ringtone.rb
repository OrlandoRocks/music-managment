
module TCFactory

  def ringtone_defaults(options={})
    options[:songs] ||= [build(:song)]
    options[:creatives] ||= [{"name"=>"artist", "role"=>"primary_artist"}, {"name"=>"", "role"=>"featuring"}]

    album_defaults(options)
  end

end
