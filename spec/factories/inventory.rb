module TCFactory
  def inventory_defaults(options = {})
    person = (Person.find_by(email: "inventory@tunecore.com") || TCFactory.build_person({
      :name => 'inventory customer',
      :email => 'inventory@tunecore.com',
    })) unless options.include?(:person)

    {:title => 'Album Distribution',
     :quantity => 10,
     :unlimited => 0,
     :product_item_id => 999,
     :purchase_id => 999,
     :person => person || options[:person]
    }.merge(options)
  end
end
