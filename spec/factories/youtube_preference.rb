module TCFactory
  def youtube_preference_defaults(options = {})
    {
      :whitelist => true,
      :mcn => false,
      :channel_id => 'test_channel_id'
    }.merge(options)
  end
end
