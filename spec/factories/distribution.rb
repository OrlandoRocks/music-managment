
module TCFactory
  def distribution_defaults(options = {})
    {
      :petri_bundle => TCFactory.build_petri_bundle,
      :converter_class => 'MusicStores::YoutubeSr::Converter'
    }.merge(options)
  end
end
