module TCFactory
  def targeted_product_defaults(options = {})
    { :price_adjustment_type => 'dollars_off',
      :price_adjustment => 10.00,
      :price_adjustment_description => 'Best Deal',
      :show_expiration_date => true
    }.merge(options)
  end
end
