

module TCFactory

  def purchase_defaults(options = {})
    {
      :invoice => build(:invoice),
      :person => build(:person),
      :cost_cents => 998,
      :discount_cents => 0,
      :currency => "USD"
    }.merge(options)
  end

end
