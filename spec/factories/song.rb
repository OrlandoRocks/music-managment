
module TCFactory
  def song_defaults(options = {})
    {
      :name => "Joe Schmoe ",
      album: create(:album),
      metadata_language_code: create(:language_code)
    }.merge(options)
  end
end
