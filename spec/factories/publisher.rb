module TCFactory
  def publisher_defaults(options={})
    {
      name:            "My Friend Publisher",
      cae:             "123456789",
      performing_rights_organization_id: 1
    }.merge(options)
  end
end
