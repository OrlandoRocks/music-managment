
module TCFactory
  def price_policy_defaults(options = {})
    {
      :short_code => "test_policy", 
      :price_calculator => 'test_policy_calculator',
      :base_price_cents => '0', 
      :description => 'test price policy'
    }.merge(options)
  end
end
