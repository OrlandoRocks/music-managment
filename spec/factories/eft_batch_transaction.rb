
module TCFactory

  def eft_batch_transaction_defaults(options = {})
    {
      :stored_bank_account_id => stored_bank_account_id,
      :country_website_id => 1,
      :amount => 100,
      :password_entered => 'Testpass123!'
    }.merge(options)
  end
end
