module TCFactory
  def login_attempt_defaults(options = {})
    {
      :ip_address => 3232235777, # 192.168.1.1
      :since_last_login_count => 1,
      :all_time_count => 1
    }.merge(options)
  end
end

