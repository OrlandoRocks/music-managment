module TCFactory
  def related_product_defaults(options = {})
    if !options.include?(:created_by)
      created_by_id = (Person.find_by(email: "admin@tunecore.com") || TCFactory.build_person({
        :name => 'admin user',
        :email => 'admin@tunecore.com',
        })).id
    end

    {
      :created_by_id => created_by_id,
      :display_name => "Overwritten Product Name",
      :description => "Overwritten Product Description",
      :sort_order => 1,
      :start_date => Time.now - 10.days,
      :expiration_date => Time.now + 20.days,
    }.merge(options)
  end
end
