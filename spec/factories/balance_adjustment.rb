
module TCFactory
  def balance_adjustment_defaults(options={})
    {
      :created_at => Time.now,
      :person => build(:person),
      :posted_by => build(:person),
      :posted_by_name => "admin name",
      :related_id => nil,
      :debit_amount => 0.00,
      :credit_amount => 100.00,
      :category => "Other",
      :customer_note => "Test Note",
      :admin_note => "Test Admin Note"
    }.merge(options)
  end

end
