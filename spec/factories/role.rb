
module TCFactory
  def role_defaults(options={})
    {
      :id => 1,
      :name => "Publishing Manager"
    }.merge(options)
  end
end
