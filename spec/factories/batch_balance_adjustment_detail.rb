module TCFactory
  def batch_balance_adjustment_detail_defaults(options={})
    { 
      :person_id => options[:person_id] || TCFactory.build_person.id,
      :code => 14,
      :name => 'Jay-Z',
      :minimum_payment => '12.34',
      :hold_payment => 'No',
      :current_balance => '114.18',
      :status => nil,
      :batch_balance_adjustment => options[:batch_balance_adjustment] || TCFactory.build_batch_balance_adjustment,
    }.merge(options)
  end

end
