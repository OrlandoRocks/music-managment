module TCFactory

  def variable_price_defaults(options={})
    {
      :price_code => "$.99",
      :price_code_display => "$.99",
      :active => 1,
      :position => 1
    }.merge(options)
  end

end
