
module TCFactory
  def sip_store_defaults(options = {})
    {
      :name => "iTunes U.S.",
      :store_id => 1,
      :display_group_id => 1,
      :display_on_status_page => 1
    }.merge(options)
  end
end
