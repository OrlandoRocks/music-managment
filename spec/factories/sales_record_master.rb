
module TCFactory
  def sales_record_master_defaults(options = {})   
    {
      :sip_store_id => 1,
      :period_interval => 1,
      :period_year => 2010,
      :period_type => 'monthly',
      :period_sort => '2010-01-01',
      :revenue_currency => 'USD',
      :exchange_rate => nil,
      :country => "",
      :tariff => nil,
      :tariff_name => nil,
      :status => 'posted'
    }.merge(options)
  end
end
