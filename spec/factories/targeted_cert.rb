module TCFactory
  def targeted_cert_defaults(options = {})
    {
      targeted_offer_id: TCFactory.create_targeted_offer || options[:targeted_offer_id],
      created_at: Time.now - 5.days,
      updated_at: Time.now - 4.days,
    }.merge(options)
  end
end
