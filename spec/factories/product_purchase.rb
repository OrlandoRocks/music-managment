
module TCFactory

  def product_purchase_defaults(options = {})
    {
      :related => build(:product),
      :purchase_items => [build(:product_purchase_item)],
      :paid_at => Time.now
    }.merge(options)
  end

end
