
module TCFactory
  def cert_defaults(options = {})
    {
      :cert => 'DEFAULT_CERT', 
      :cert_batch => build(:cert_batch),
      :date_used => nil,
      :expiry_date => 30.days.from_now,
      :engine_params => 10, 
      :purchase_id => nil,
      :person_id => nil,
      :album_id => nil, 
      :total_amount => nil,
      :admin_only => false
    }.merge(options)
  end
end
