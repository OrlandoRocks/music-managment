module TCFactory

  def paypal_transfer_defaults(options = {})
    {
      :transfer_status=>"pending",
      :person=>Person.first,
      :payment_cents=>1000,
      :country_website_id=>CountryWebsite::UNITED_STATES,
      :currency=>"USD",
      :admin_charge_cents=>300,
      :paypal_address=>"johnnyappleseed@apple.com",
      :paypal_address_confirmation=>"johnnyappleseed@apple.com",
      :password_entered=>"Testpass123!",
      :current_balance_cents=>10000,
      :person_transaction=>build(:person_transaction, :person=>Person.first)
    }.merge(options)
  end
end
