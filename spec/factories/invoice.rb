
module TCFactory

  def invoice_defaults(options = {})
    person = options[:person] || create(:person)
    options.delete(:person)
    {
      :person => person,
      :settled_at => Time.now,
      :created_at => nil,
      :batch_status => 'visible_to_customer',
      :final_settlement_amount_cents => 99,
      :corporate_entity_id => 1
    }.merge(options)
  end

end
