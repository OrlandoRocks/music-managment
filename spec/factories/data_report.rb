
module TCFactory
  def data_report_defaults(options = {})
    {
      :name => 'Test Report',
      :resolution => [:day],
      :start_on => Date.today << 1,
      :live_update_at => nil,
      :live_update_limit => 15
    }.merge(options)
  end
end
