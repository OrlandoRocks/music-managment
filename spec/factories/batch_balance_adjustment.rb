module TCFactory
  def batch_balance_adjustment_defaults(options={})
    {
      :ingested_filename => 'songwriter_royalty_payment_1st_quarter_2012.csv',
      :status => nil
    }.merge(options)
  end

end
