
module TCFactory
  def stored_paypal_account_defaults(options = {})
    person = options[:person] || TCFactory.build_person({
      name: 'Test Person',
      email: 'test@tunecore.com',
    })
    {
      created_at: Time.now,
      person: person,
      country_website: person.country_website
    }.merge(options)
  end
end
