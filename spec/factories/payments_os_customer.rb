module TCFactory
  def payments_os_customer_defaults(opts={})
    {
      customer_id: SecureRandom.uuid,
      customer_reference: SecureRandom.uuid
    }.merge(opts)
  end
end
