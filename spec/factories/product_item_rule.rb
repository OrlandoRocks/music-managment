# +-----------------------------+---------------------------------------------------------------------------------------------------------------------+------+-----+---------+----------------+
# | Field                       | Type                                                                                                                | Null | Key | Default | Extra          |
# +-----------------------------+---------------------------------------------------------------------------------------------------------------------+------+-----+---------+----------------+
# | id                          | int(10) unsigned                                                                                                    | NO   | PRI | NULL    | auto_increment | 
# | product_item_id             | int(10) unsigned                                                                                                    | NO   | MUL | NULL    |                | 
# | parent_id                   | int(10) unsigned                                                                                                    | YES  |     | NULL    |                | 
# | rule_type                   | enum('inventory','entitlement')                                                                                     | YES  |     | NULL    |                | 
# | rule                        | enum('price_for_each','total_included','price_above_included','price_for_each_above_included','range','true_false') | NO   |     | NULL    |                | 
# | inventory_type              | varchar(50)                                                                                                         | NO   |     | NULL    |                | 
# | quantity                    | mediumint(9)                                                                                                        | NO   |     | 1       |                | 
# | model_to_check              | varchar(50)                                                                                                         | YES  |     | NULL    |                | 
# | method_to_check             | varchar(50)                                                                                                         | YES  |     | NULL    |                | 
# | total_included              | mediumint(9)                                                                                                        | YES  |     | NULL    |                | 
# | true_false                  | tinyint(1)                                                                                                          | YES  |     | NULL    |                | 
# | minimum                     | mediumint(9)                                                                                                        | YES  |     | NULL    |                | 
# | maximum                     | mediumint(9)                                                                                                        | YES  |     | NULL    |                | 
# | entitlement_rights_group_id | int(10) unsigned                                                                                                    | YES  |     | NULL    |                | 
# | first_renewal_duration      | tinyint(4)                                                                                                          | YES  |     | NULL    |                | 
# | first_renewal_interval      | enum('month','day','week','year')                                                                                   | YES  |     | NULL    |                | 
# | renewal_duration            | tinyint(4)                                                                                                          | YES  |     | NULL    |                | 
# | renewal_interval            | enum('month','day','week','year')                                                                                   | YES  |     | NULL    |                | 
# | price                       | decimal(10,2)                                                                                                       | NO   |     | 0.00    |                | 
# | updated_at                  | timestamp                                                                                                           | YES  |     | NULL    |                | 
# | created_at                  | timestamp                                                                                                           | YES  |     | NULL    |                | 
# +-----------------------------+---------------------------------------------------------------------------------------------------------------------+------+-----+---------+----------------+

module TCFactory
  def product_item_rule_defaults(options = {})
  
    {
      :quantity => 1,
      :unlimited => false,
      :true_false => false,
      :minimum => 0,
      :maximum => 0,
      :price => "49.99",
      :currency => "USD",
      :rule=>"price_for_each",
      :rule_type=>"inventory"
     }.merge(options)
  end
end
