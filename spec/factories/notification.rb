module TCFactory

  def notification_defaults(options = {})
    options[:person] ||= TCFactory.build(:person)
    options[:notification_item] ||= Album.new(person: options[:person])

    {
      first_seen: nil,
      first_clicked: nil,
      first_archived: nil,
      text: "This is a notification to go to dashboard",
      title: "This is a notification title",
      url: "www.tunecore.com/dashboard"
    }.merge(options)
  end

  def unreleased_notification_defaults(options={})
    notification_defaults(options)
  end

  def itunes_link_notification_defaults(options={})
    notification_defaults(options)
  end
  
  def missing_splits_notification_defaults(options={})
    notification_defaults(options)
  end

end
