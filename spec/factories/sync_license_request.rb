module TCFactory
  def sync_license_request_defaults(options = {})
    song = nil

    if options[:song]
      song = options[:song]
    else
      # Set up song and associations to have valid
      # sync_license_request
      composition = TCFactory.create(:composition)
      muma_song = TCFactory.create(:muma_song, :composition => composition)
      album = TCFactory.create(:album)
      song = TCFactory.create(:song, :album => album, :composition => composition)
    end

    person = options[:person] || TCFactory.create(:person)
    {
    :person => person,
    :song => song
    }.merge(options) 
  end
end
