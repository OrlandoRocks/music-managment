
module TCFactory
  def genre_defaults(options = {})
    {:name => 'Alternative'}.merge(options)
  end
end
