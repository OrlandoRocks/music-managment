
module TCFactory

  def paypal_transaction_defaults(options = {})
    person = TCFactory.build_person({
                                      name: 'Test Person',
                                      email: 'test@tunecore.com',
                                    }) unless options.include?(:person)
    config_person = person || options[:person]
    payin_provider_config = config_person.paypal_payin_provider_config if config_person.present?
    {
      person: person,
      email: "paypalcustomer@tunecore.com",
      amount: "00.01",
      action: "Authorization",
      status: "pending",
      transaction_id: "8HY56260SX208193A",
      transaction_type: "expresscheckout",
      referenced_paypal_account_id: nil,
      token: "EC-05K557426E334835T",
      country_website_id: 1,
      currency: "USD",
      error_code: nil,
      pending_reason: nil,
      ack: "success",
      fee: nil,
      payin_provider_config: payin_provider_config
    }.merge(options)
  end

end
