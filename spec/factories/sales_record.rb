
module TCFactory
  def sales_record_defaults(options = {})   
    {
      :person_id => 1,
      :sip_sales_record_id => 1,
      :sales_record_master_id => 1,
      :related_id => 1,
      :related_type => "Album",
      :distribution_type => "Download",
      :quantity => 1,
      :revenue_per_unit => 0.70000000000000,
      :revenue_total => 0.70000000000000,
      :amount => 0.70000000000000
    }.merge(options)
  end
end
