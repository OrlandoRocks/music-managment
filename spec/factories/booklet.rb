module TCFactory
  def booklet_defaults(options = {})
    {
      :s3_asset_id => 0
    }.merge(options)
  end
end
