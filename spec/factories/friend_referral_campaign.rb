module TCFactory
  def friend_referral_campaign_defaults(options = {})
    {
     :person => options[:person] || create_person,
     :campaign_id => GA_DEFAULT_CAMPAIGN_ID,
     :campaign_code => 'yt'
    }.merge(options)
  end
end
