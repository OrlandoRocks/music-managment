module TCFactory def muma_song_defaults(options = {})
    song      = options[:song]      || TCFactory.create(:song, :composition=>TCFactory.create(:composition), :album=>TCFactory.create(:album, :payment_applied=>1))
    options.delete(:song)
    
    if song.composer.blank?
      TCFactory.create(:composer, :person_id=>song.album.person.id)
    end
    
    {
      :code=>song.id,
      :title=>song.name,
      :label_copy=>"Label Copy",
      :composition=>song.composition,
      :mech_collect_share=>50.00,
      :album_label=>song.album.label_name,
      :album_title=>song.album.name,
      :album_upc=>song.album.upc.number,
      :performing_artist=>song.album.artist_name,
      :isrc=>song.tunecore_isrc,
      :ip_updated_at=>Time.now,
      :source_created_at=>Time.now,
      :source_updated_at=>Time.now,
      :company_code=>"TUNE"
    }.merge(options)
  end
end
