
module TCFactory
  def artwork_defaults(options = {})
    {
      :height => 1600,
      :width => 1600,
      :uploaded=>true,
      album: build(:album)
    }.merge(options)
  end
end
