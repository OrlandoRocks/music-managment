
module TCFactory
  def store_defaults(options = {})
    @count ||= 0
    @count += 1
    {
      :name => "Itunes #{@count}",
      :abbrev => "It#{@count}",
      :short_name => "tunes#{@count}",
      :position => 1,
      :needs_rights_assignment => true,
      :is_active => true#,
      #:launched_at => Time.now
    }.merge(options)
  end
end
