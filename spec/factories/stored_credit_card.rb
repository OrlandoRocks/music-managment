
module TCFactory
  def stored_credit_card_defaults(options = {})
    meta_data = {
      "customer_vault"=> {
         "first_name"=>"Test", 
         "last_name"=>"Testerson", 
         "address_1"=>"123 Main Street", 
         "address_2"=>"APT 1", 
         "state" => "NJ",
         "city"=>"Hoboken", 
         "postal_code"=>"07030", 
         "country"=>"US", 
         "cc_number"=>"4xxxxxxxxxxx1111", 
         "cc_exp" => "1020", 
         "customer_vault_id"=>"123456"
     }
   }

    {
      :customer_vault_id => 12345,
      :person_id=> 1,
      :status => "new",
      :meta_data => meta_data,
      :expiration_year=>Date.today.year + 5,
      :expiration_month=>Date.today.month
    }.merge(options)
  end
end
