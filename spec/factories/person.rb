module TCFactory
  #
  #  All of the options that
  #  will make a valid person
  #
  def person_defaults(options = {})
    @person_counter ||= 0
    @person_counter += 1
    corporate_entity
    {
      name: "Test User",
      email: "testguy#{@person_counter}@test.com",
      password: 'Testpass123!',
      password_confirmation: options[:password].nil? ? 'Testpass123!' : options[:password],
      phone_number: '646-555-7788',
      country: "US",
      postal_code: "123412-12312",
      status: "Active",
      is_verified: true,
      accepted_terms_and_conditions_on: Time.now,
      accepted_terms_and_conditions: true,
      widget_status: :on,
      password_reset_tmsp: Time.now,
      country_audit_source: CountryAuditSource::SELF_IDENTIFIED_NAME,
      corporate_entity: corporate_entity
    }.merge(options)
  end
end
