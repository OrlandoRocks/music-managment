
module TCFactory
  def shipping_label_defaults(options = {})
    {
      :address1 => '90 West Street, Apt. 6X',
      :city => 'New York',
      :state => 'NY',
      :zip => 10006
    }.merge(options)
  end
end
