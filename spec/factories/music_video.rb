
module TCFactory
  def music_video_defaults(options = {})
    person = TCFactory.create(:person)
    {
      :name => "Default video",
      :length_min => "4",
      :sale_date => Date.today,
      :orig_release_year => Date.today,
      :selected_artist_name => "default artist",
      :person => person,
      :video_type => 'MusicVideo',
      :accepted_format => true
    }.merge(options)
  end
end
