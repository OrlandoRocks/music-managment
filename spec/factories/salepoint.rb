
module TCFactory
  def salepoint_defaults(options = {})   
    store = options.include?(:store) ? options[:store] : Store.ITUNES_US 
    album = options.include?(:album) ? options[:album] : build_album()
		variable_price = options.include?(:variable_price) ? options[:variable_price] : Store.ITUNES_US.variable_prices.first
    #remove keys that doesn't exist for salepoint model otherwise TCFactory.create will throw an exception
    options.delete(:album) if options.include?(:album)
    options.delete(:store) if options.include?(:store)

    FactoryBot.create(:salepointable_store, store: store)
    
    {
			:variable_price_id => variable_price.try(:id),
      :has_rights_assignment => true,
      :salepointable_type => 'Album',
      :salepointable_id => album.id,
      :store => store,
      :status => "complete",
      :payment_applied => true
    }.merge(options)
  end

end
