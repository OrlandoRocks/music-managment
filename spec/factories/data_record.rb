
module TCFactory
  def data_record_defaults(options = {})
    {
      :measure => 100,
      :description => "Objects Purchased",
      :resolution_unit => :month,
      :resolution_identifier => :M3_2008
    }.merge(options)
  end
end
