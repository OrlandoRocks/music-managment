module TCFactory
  def creative_defaults(options = {})
    {
      :artist => options[:artist] || TCFactory.build_artist,
      :role => 'primary_artist',
      :creativeable => options[:creativeable] || TCFactory.build_song 
    }.merge(options)
  end
end
