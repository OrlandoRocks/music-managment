
module TCFactory
  def album_defaults(options = {})
    golive_date = Time.now + 1.days

    person = (Person.first || TCFactory.build_person({
      :name => 'mikewazowski',
      :email => 'mikewazowski@tunecore.com',
    })) unless options.include?(:person)
    {
      :creatives => [{"name"=>"default artist", "role"=>"primary_artist"}],
      :label_name => "default label",
      :sale_date => golive_date.to_date,
      :orig_release_year => Date.today,
      :name => "default album #{SecureRandom.hex(5)}",
      :is_various => false,
      :language_code => options[:itunes_language] || "en",
      :primary_genre => (Genre.first || create(:genre)),
      :timed_release_timing_scenario => 'absolute_time',
      :golive_date => golive_date,
      :songs => [],
      :person => person
    }.merge(options)
  end

  def create_purchaseable_album(options = {})
    album = TCFactory.create(:album, options)
    album.salepoints = [TCFactory.create(:salepoint, album: album, payment_applied: false, finalized_at: false)]
    album.songs = [TCFactory.create(:song, name: "1", album: album)]
    album.songs[0].upload = TCFactory.create(:upload, song: album.songs[0])
    album.artwork = TCFactory.create(:artwork, album: album, uploaded: true)
    album.golive_date = Time.now + 1.days
    album.timed_release_timing_scenario = 'absolute'

    return album
  end
end
