ARG BASE_IMAGE_TAG
FROM 738011128897.dkr.ecr.us-east-1.amazonaws.com/tunecore/tc-www/base:$BASE_IMAGE_TAG

ARG BUNDLE_ENTERPRISE__CONTRIBSYS__COM
ARG BUNDLE_GEM__FURY__IO
ARG BUNDLE_WITHOUT

ENV BUNDLER_VERSION='2.3.17'

ENV DOCKERIZE_VERSION v0.6.1
RUN wget -q https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz && \
    tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz && \
    rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz

# Install Chrome
# Borrowed from https://github.com/CircleCI-Public/circleci-dockerfiles/blob/master/ruby/images/2.5.7-stretch/browsers/Dockerfile
RUN apt-get install -fy --allow-remove-essential libtinfo5 libllvm3.5 libgbm1 fonts-liberation libappindicator3-1 xdg-utils unixodbc-dev \
    && curl --silent --show-error --location --fail --retry 3 --output /tmp/google-chrome-stable_current_amd64.deb https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb \
    && (dpkg -i "/tmp/google-chrome-stable_current_amd64.deb" || apt-get -fy install)  \
    && rm -rf /tmp/google-chrome-stable_current_amd64.deb \
    && sed -i 's|HERE/chrome"|HERE/chrome" --disable-setuid-sandbox --no-sandbox|g' \
    "/opt/google/chrome/google-chrome" \
    && google-chrome --version

RUN CHROME_VERSION="$(google-chrome --version)" \
    && export CHROMEDRIVER_RELEASE="$(echo $CHROME_VERSION | sed 's/^Google Chrome //')" && export CHROMEDRIVER_RELEASE=${CHROMEDRIVER_RELEASE%%.*} \
    && CHROMEDRIVER_VERSION=$(curl --silent --show-error --location --fail --retry 4 --retry-delay 5 http://chromedriver.storage.googleapis.com/LATEST_RELEASE_${CHROMEDRIVER_RELEASE}) \
    && curl --silent --show-error --location --fail --retry 4 --retry-delay 5 --output /tmp/chromedriver_linux64.zip "http://chromedriver.storage.googleapis.com/$CHROMEDRIVER_VERSION/chromedriver_linux64.zip" \
    && cd /tmp \
    && unzip chromedriver_linux64.zip \
    && rm -rf chromedriver_linux64.zip \
    && mv chromedriver /usr/local/bin/chromedriver \
    && chmod +x /usr/local/bin/chromedriver \
    && chromedriver --version

ENV INSTALL_PATH /tc-www
RUN mkdir -p $INSTALL_PATH
WORKDIR $INSTALL_PATH
COPY Gemfile* $INSTALL_PATH/
RUN gem install bundler --version $BUNDLER_VERSION && bundle install
COPY . $INSTALL_PATH/
COPY ./docker/ssh/config /root/.ssh/config
RUN chmod 600 /root/.ssh/config
EXPOSE 3005

RUN tic /tc-www/docker/xterm-256color.ti
ENV TERM=xterm-256color

ENTRYPOINT [ "./docker/environment.sh" ]
CMD [ "./docker/start.sh", "spring" ]
