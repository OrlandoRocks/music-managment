source "https://rubygems.org"

source "https://gem.fury.io/techteam/" do
  gem "distributor_api", "~> 0.3.6"
  gem "mimemagic"
  gem "paypal-business"
  gem "soap4r-ruby1.9"
  gem "tc_legalizer", "~> 0.3.0", require: "legalizer"
end

source "https://enterprise.contribsys.com/" do
  gem "sidekiq-ent"
  gem "sidekiq-pro"
end

source "https://rubygems.org" do
  gem "activemerchant", "~> 1.0"
  gem "active_model_serializers", "0.9.7"
  gem "activerecord-import"
  gem "after_commit_everywhere"
  gem "airbrake"
  gem "api-auth"
  gem "apollo-federation", "~> 3.0"
  gem "archive-tar-minitar"
  gem "authy"
  gem "aws-sdk", "~> 1.0"
  gem "aws-sdk-s3", "~> 1.14"
  gem "aws-sdk-sns"
  gem "bcrypt_pbkdf"
  gem "bootsnap", "~> 1.7.4"
  gem "braintree"
  gem "browser"
  gem "builder"
  gem "candy_check"
  gem "color"
  gem "daemons", "1.0.10"
  gem "diffy"
  gem "docusign_esign", "~> 1.0"
  gem "dotenv-rails", require: "dotenv/rails-now"
  gem "einhorn", "~> 0.7.0"
  gem "factory_bot_rails", require: false
  gem "faraday"
  gem "geocoder"
  gem "gon"
  gem "graphql"
  gem "httparty"
  gem "httpclient"
  gem "jquery-rails"
  gem "jwt", "~> 1.0"
  gem "libxml-ruby"
  gem "memoist"
  gem "mime-types", require: "mime/types"
  gem "mini_magick"
  gem "monetize"
  gem "money", "~> 6.0"
  gem "MP4Info", require: "mp4info"
  gem "multipart"
  gem "mysql2"
  gem "net-scp"
  gem "net-sftp"
  gem "net-ssh"
  gem "newrelic_rpm"
  gem "nokogiri", "~> 1.8"
  gem "oauth-plugin"
  gem "oily_png"
  gem "oj"
  gem "oj_mimic_json"
  gem "open4"
  gem "paperclip", "4.2.0"
  gem "paper_trail"
  gem "parallel"
  gem "paypal"
  gem "phraseapp-in-context-editor-ruby"
  gem "puma"
  gem "rails", "~> 6.0.0"
  gem "rails-i18n", "~> 6.0.0"

  # rubocop:disable Bundler/OrderedGems
  # These rbnacl gems have to be in this order
  gem "rbnacl-libsodium"
  gem "rbnacl", "~> 4.0"
  # rubocop:enable Bundler/OrderedGems

  gem "react-rails"
  gem "redis"
  gem "redis-namespace"
  gem "redis-rails"
  gem "redlock"
  gem "responders", "~> 3.0"
  gem "rest-client"
  gem "rollout"
  gem "roo", "~> 2.8"
  gem "RubyInline", require: "inline"
  gem "ruby-mp3info", require: "mp3info"
  gem "ruby-odbc"
  gem "rubyzip"
  gem "sass-rails", "~> 5.0"
  gem "scenic"
  gem "scenic-mysql_adapter"
  gem "sequel"
  gem "shrine", "~> 3.0"
  gem "sidekiq", "5.2.7" # NOTE: was a much earlier version, using this for ruby 2.4
  gem "sidekiq-prometheus-exporter"
  gem "sift", "~> 4.0"
  gem "simple_form"
  gem "simple_xlsx_writer", require: "simple_xlsx"
  gem "sinatra", "~> 2.0.7", require: "sinatra/base"
  gem "sinatra-contrib", "~> 2.0.7"
  gem "spreadsheet"
  gem "terser"
  gem "test-unit", "~> 3.0"
  gem "triez"
  gem "tunecore_direct"
  gem "uglifier"
  gem "uuid"
  gem "uuidtools"
  gem "wicked_pdf"
  gem "will_paginate", "~> 3.0"
  gem "workflow", "~> 2.0.2" # Move to 3.0.0 once we get to ruby 3.0
  gem "workflow-activerecord", "~> 4.1.9"
  gem "xml-simple", require: "xmlsimple"
  gem "zendesk_api"
  gem "zip-zip"

  gem "faker"
  gem "koala", "~> 3.0"
  gem "strong_migrations"

  gem "derailed_benchmarks", require: false
  gem "memory_profiler", require: false

  gem "mustache"
  gem "rounding"

  gem "cloudflare-rails"

  group :tracing do
    gem "jaeger-client", "~> 0.10.0"
    gem "opentracing", "~> 0.4"
    gem "signalfx-rails-instrumentation", "~> 0.1.4"
  end

  group :test do
    gem "capybara"
    gem "capybara-screenshot"
    gem "capybara-selenium"
    gem "chromedriver-helper"
    gem "selenium-webdriver"

    # RAILS_6_UPGRADE: maybe comment out the webdrivers gem
    gem "webdrivers"

    gem "codeclimate-test-reporter", require: false
    gem "equivalent-xml"
    gem "rails-controller-testing"
    gem "rspec_junit_formatter"
    gem "rspec-sidekiq"
    gem "simplecov", require: false
    gem "thin"
    gem "timecop"

    gem "fake_braintree"
    gem "test-prof"
  end

  group :development do
    gem "better_errors"
    gem "binding_of_caller"
    gem "letter_opener"
    gem "letter_opener_web"
    gem "rack-cors", "~> 1.1"
    gem "rack-mini-profiler"
    gem "rubocop", "~> 1.10", require: false
    gem "rubocop-performance", "~> 1.9.2", require: false
    gem "rubocop-rails", "~> 2.9.1", require: false
    gem "web-console"
  end

  group :development, :staging, :production do
    gem "waveform"
  end

  group :development, :test, :staging do
    gem "chargebee"
  end

  group :development, :test do
    gem "active_record_query_trace"
    gem "capistrano"
    gem "capistrano-ext"
    gem "fuubar"
    gem "graphiql-rails", git: "https://github.com/rmosolgo/graphiql-rails.git",
                          ref: "6b34eb1" # A ref containing UI for Custom Headers, not yet in main
    gem "guard"
    gem "guard-rspec"
    gem "launchy"
    gem "pry"
    # rubocop:disable Bundler/OrderedGems
    gem "pry-remote"
    gem "pry-nav"
    # rubocop:enable Bundler/OrderedGems
    gem "pry-rails"
    gem "pry-rescue"
    gem "pry-stack_explorer"
    gem "railroady"
    gem "rb-fsevent"
    gem "rspec"
    gem "rspec-activemodel-mocks"
    gem "rspec-collection_matchers"
    gem "rspec-rails", "4.0.0.beta3" # needed for RAILS_6 as of 1/16/20
    gem "shoulda-matchers"
    gem "spring"
    gem "spring-commands-rspec"
    gem "stackprof"
    gem "webmock"
    gem "webrat"
  end
end

gem "twilio-ruby", "~> 5.73"
