class CreateVideoPurchaseItems < ActiveRecord::Migration[4.2]
  def self.up
    create_table :video_purchase_items do |t|
      t.column :created_at, :datetime, :null => false
      t.column :purchase_id, :int, :null => false
      t.column :purchaseable_id, :int, :null => false
      t.column :purchaseable_type, :string, :limit => 30, :null => false
    end
    
    add_index(:video_purchase_items, :purchase_id)
    add_index(:video_purchase_items, :purchaseable_id)
    
  end

  def self.down
    drop_table :video_purchase_items
  end
end
