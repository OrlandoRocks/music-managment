class AddProductionIndexes < ActiveRecord::Migration[4.2]

  def self.soft_add_index(table,index_name,column)
    begin
      add_index table, column, :name => index_name
    rescue ActiveRecord::StatementInvalid => e
      puts "Index already present or couldn't be added: #{table}.#{column}"
    end
  end

  def self.soft_remove_index(table,index_name,column)
    begin
      remove_index table, :name => index_name
    rescue ActiveRecord::StatementInvalid => e
      puts "Index not present or couldn't be removed: #{table} -- #{index_name}"
    end
  end

  def self.up
    soft_add_index :store_intakes, :store_intakes_store_id_index, :store_id
    soft_add_index :store_intakes, :store_intakes_reporting_month_id_index, :reporting_month_id
    soft_add_index :people, :people_artist_id, :artist_id
    soft_add_index :people, :people_label_id_index, :label_id
    soft_add_index :person_balances, :people_person_id_index, :person_id
    soft_add_index :paypal_ipns, :paypal_ipns_person_index, :person_id
    soft_add_index :paypal_ipns, :paypal_ipns_invoice_id_index, :invoice_id
    soft_add_index :people_roles, :people_roles_person_id_index, :person_id
    soft_add_index :people_roles, :people_roles_role_id_index, :role_id
    soft_add_index :person_intakes, :person_intakes_store_id_index, :store_id
    soft_add_index :album_intakes, :album_intages_album_id_index, :album_id
    soft_add_index :person_transactions, :person_transactions_person_id_index, :person_id
    soft_add_index :certs, :certs_person_id_index, :person_id
    soft_add_index :certs, :certs_album_id_index, :album_id
    soft_add_index :sales_records, :sales_records_album_intake_id_index, :album_intake_id
    soft_add_index :sales_records, :sales_records_reporting_month_id, :reporting_month_id
    soft_add_index :sales_records, :sales_records_person_id_index, :person_id
    soft_add_index :sales_records, :sales_records_song_id_index, :song_id
    soft_add_index :sales_records, :sales_records_album_id_index, :album_id
    soft_add_index :sales_records, :sales_records_store_intake_id_index, :store_intake_id
    soft_add_index :bundles, :bundles_album_id_index, :album_id  
    soft_add_index :songs, :songs_upload_id_index, :upload_id
    soft_add_index :songs, :songs_chargeable_id_index, :chargeable_id
    soft_add_index :cert_batches, :cert_batches_person_id_index, :person_id
    soft_add_index :events, :events_bundle_id_index, :bundle_id
    soft_add_index :events, :events_album_id_index, :album_id
    soft_add_index :invoice_settlements, :invoice_settlements_invoice_id_index, :invoice_id
    soft_add_index :albums, :albums_artist_id_index, :artist_id
    soft_add_index :albums, :albums_label_id_index, :id
    soft_add_index :albums, :albums_chargeable_id_index, :chargeable_id
    soft_add_index :albums, :albums_invoice_id_index, :invoice_id
    soft_add_index :album_intakes, :album_intakes_person_id_index, :person_id
    soft_add_index :salepoints, :salepoints_chargeable_id_index, :chargeable_id
  end

  def self.down
    soft_remove_index :store_intakes, :store_intakes_store_id_index, :store_id
    soft_remove_index :store_intakes, :store_intakes_reporting_month_id_index, :reporting_month_id
    soft_remove_index :people, :people_artist_id, :artist_id
    soft_remove_index :people, :people_label_id_index, :label_id
    soft_remove_index :person_balances, :people_person_id_index, :person_id
    soft_remove_index :paypal_ipns, :paypal_ipns_person_index, :person_id
    soft_remove_index :paypal_ipns, :paypal_ipns_invoice_id_index, :invoice_id
    soft_remove_index :people_roles, :people_roles_person_id_index, :person_id
    soft_remove_index :people_roles, :people_roles_role_id_index, :role_id
    soft_remove_index :person_intakes, :person_intakes_store_id_index, :store_id
    soft_remove_index :album_intakes, :album_intages_album_id_index, :album_id
    soft_remove_index :person_transactions, :person_transactions_person_id_index, :person_id
    soft_remove_index :certs, :certs_person_id_index, :person_id
    soft_remove_index :certs, :certs_album_id_index, :album_id
    soft_remove_index :sales_records, :sales_records_album_intake_id_index, :album_intake_id
    soft_remove_index :sales_records, :sales_records_reporting_month_id, :reporting_month_id
    soft_remove_index :sales_records, :sales_records_person_id_index, :person_id
    soft_remove_index :sales_records, :sales_records_song_id_index, :song_id
    soft_remove_index :sales_records, :sales_records_album_id_index, :album_id
    soft_remove_index :sales_records, :sales_records_store_intake_id_index, :store_intake_id
    soft_remove_index :bundles, :bundles_album_id_index, :album_id  
    soft_remove_index :songs, :songs_upload_id_index, :upload_id
    soft_remove_index :songs, :songs_chargeable_id_index, :chargeable_id
    soft_remove_index :cert_batches, :cert_batches_person_id_index, :person_id
    soft_remove_index :events, :events_bundle_id_index, :bundle_id
    soft_remove_index :events, :events_album_id_index, :album_id
    soft_remove_index :invoice_settlements, :invoice_settlements_invoice_id_index, :invoice_id
    soft_remove_index :albums, :albums_artist_id_index, :artist_id
    soft_remove_index :albums, :albums_label_id_index, :id
    soft_remove_index :albums, :albums_chargeable_id_index, :chargeable_id
    soft_remove_index :albums, :albums_invoice_id_index, :invoice_id
    soft_remove_index :album_intakes, :album_intakes_person_id_index, :person_id
    soft_remove_index :salepoints, :salepoints_chargeable_id_index, :chargeable_id
  end
end

