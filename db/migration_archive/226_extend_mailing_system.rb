class ExtendMailingSystem < ActiveRecord::Migration[4.2]
  def self.up

    add_column :mailing_deliveries, :target_id, :integer
    add_column :mailing_deliveries, :target_type, :string
    add_index :mailing_deliveries, [:target_id, :target_type]
    add_column :mailing_deliveries, :is_bounced, :boolean, :null => false, :default => false
    add_column :mailing_deliveries, :bounced_at, :datetime
    
    add_column :mailings, :is_system, :boolean, :null => false, :default => false
    add_column :mailings, :max_deliveries_per_person, :integer, :null => false, :default => 0
    add_column :mailings, :max_deliveries_per_target, :integer, :null => false, :default => 0   
    remove_column :mailings, :created_by
    remove_column :mailings, :updated_by
    
    add_column :scheduled_mailings, :type, :string
    add_column :scheduled_mailings, :name, :string
    add_index :scheduled_mailings, [:type, :name]
    add_column :scheduled_mailings, :scheduled_to_end_at, :datetime
    add_index :scheduled_mailings, [:scheduled_at, :scheduled_to_end_at], :name => :delivery_times
    ScheduledMailing.connection.execute("update scheduled_mailings set type = 'MassMailing'")
  end

  def self.down

    remove_column :mailing_deliveries, :target_id
    remove_column :mailing_deliveries, :target_type
    remove_column :mailing_deliveries, :is_bounced
    remove_column :mailing_deliveries, :bounced_at

    remove_column :mailings, :is_system
    remove_column :mailings, :max_deliveries_per_person
    remove_column :mailings, :max_deliveries_per_target
    add_column :mailings, :created_by, :date
    add_column :mailings, :updated_by, :date

    remove_column :scheduled_mailings, :type
    remove_column :scheduled_mailings, :name
    remove_column :scheduled_mailings, :scheduled_to_end_at
    remove_index :scheduled_mailings, :name => :delivery_times
  end
end
