class AddMemorialDayCert < ActiveRecord::Migration[4.2]
  def self.up
    Cert.create!(
      :cert => 'guitarcenter',
      :person_id => nil,
      :album_id => nil,
      :date_used => nil,
      :expiry_date => Time.gm(2008,1,1),
      :percent_off => 0,
      :cert_engine => 'FreeSongs',
      :engine_params => "5"
    )
  end

  def self.down
    Cert.find_by(cert: 'guitarcenter').destroy
  end
end
