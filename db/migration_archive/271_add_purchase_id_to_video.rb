class AddPurchaseIdToVideo < ActiveRecord::Migration[4.2]
  def self.up
    add_column :videos, :purchase_id, :integer
    add_index :videos, :purchase_id
  end

  def self.down
    remove_index :videos, :purchase_id
    remove_column :videos, :purchase_id
  end
end
