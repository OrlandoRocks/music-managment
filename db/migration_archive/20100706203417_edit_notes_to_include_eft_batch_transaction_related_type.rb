class EditNotesToIncludeEftBatchTransactionRelatedType < ActiveRecord::Migration[4.2]
  def self.up
    sql = %q(ALTER TABLE notes
                CHANGE `related_type` `related_type` enum('Person','Album','Song', 'EftBatchTransaction') character set utf8 collate utf8_general_ci NULL  comment 'Type of entity.';)
    execute(sql)
  end

  def self.down
    sql = %q(ALTER TABLE notes
                CHANGE `related_type` `related_type` enum('Person','Album','Song') character set utf8 collate utf8_general_ci NULL  comment 'Type of entity.';)
    execute(sql)
  end
end
