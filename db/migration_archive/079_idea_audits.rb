
class IdeaAudits < ActiveRecord::Migration[4.2]
  def self.up
    create_table :ideas do |t|
        t.column :package_date, :string 
        t.column :upc, :string
        t.column :album_id, :integer
        t.column :md5, :string
    end
  end

  def self.down 
    drop_table :ideas
    
  end
end
