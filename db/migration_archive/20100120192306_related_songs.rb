class RelatedSongs < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `ringtone_related_songs` (
      `id` int(11) unsigned NOT NULL auto_increment,
      `song_id` int(11) unsigned NOT NULL,
      `album_id` int(11) unsigned NOT NULL,
      PRIMARY KEY  (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
    
    execute up_sql;

  end

  def self.down
    drop_table :ringtone_related_songs
  end
end
