class AddGlobalShippingLabelMark < ActiveRecord::Migration[4.2]
  def self.up
    add_column :shipping_labels, :global_flag, :boolean, :default => false
  end

  def self.down
    remove_column :shipping_labels, :global_flag
  end
end
