class AddAbstractRelatedIdAndType < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = "ALTER TABLE braintree_vault_transactions 
       CHANGE COLUMN `stored_credit_card_id` `related_id` INT(10) NULL DEFAULT NULL COMMENT 'polymorphic related column' after `person_id`,
       ADD COLUMN `related_type` varchar(30) NULL DEFAULT NULL COMMENT 'polymorphic related type' after `related_id`,
       DROP KEY `stored_credit_card_id`"
    execute up_sql
  end

  def self.down
    down_sql = "ALTER TABLE braintree_vault_transactions 
       CHANGE COLUMN `related_id` `stored_credit_card_id` int(10) unsigned DEFAULT NULL COMMENT 'References stored_credit_cards table' after `person_id`,
       ADD KEY `stored_credit_card_id` (`stored_credit_card_id`),
       DROP COLUMN `related_type`"
    execute down_sql
  end
end
