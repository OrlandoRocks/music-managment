class AddS3AssetIdColumnToSongs < ActiveRecord::Migration[4.2]
  def self.up
    up_sql=%Q(ALTER TABLE `songs`
              ADD COLUMN `s3_asset_id` INT UNSIGNED NULL
            )
    execute up_sql
  end

  def self.down
    remove_column :songs, :s3_asset_id
  end
end
