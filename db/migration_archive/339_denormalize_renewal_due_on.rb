class DenormalizeRenewalDueOn < ActiveRecord::Migration[4.2]
  def self.up
    puts "Moving all BeginSubscription Events to album.known_live_on just as a precaution"
    execute "update albums left join annual_renewal_events on albums.id = annual_renewal_events.album_id and annual_renewal_events.event_type = 'BeginSubscription' set albums.known_live_on = annual_renewal_events.occurred_on"


    puts "Moving all SubscriptionRenewed events to the Album#renewal_due_on + 1 year"
    execute "update albums left join annual_renewal_events on albums.id = annual_renewal_events.album_id and annual_renewal_events.event_type = 'SubscriptionRenewed' set albums.renewal_due_on = ADDDATE(annual_renewal_events.occurred_on , INTERVAL 1 YEAR)"

    puts "Populating the initial Renwal_Due_On where the renewal_due_on is NULL and the Known_live_on is NOT NULL"
    execute "UPDATE albums set renewal_due_on = ADDDATE(known_live_on , INTERVAL 1 YEAR) where renewal_due_on is null AND known_live_on is not null"

    puts "Mark any annual_renewals as finalized if there is a SubscriptionRenewed event"
    execute "update annual_renewals left join annual_renewal_events on annual_renewals.album_id = annual_renewal_events.album_id and annual_renewal_events.event_type = 'SubscriptionRenewed' set annual_renewals.finalized_at = annual_renewal_events.updated_at"

    albums = Album.where("known_live_on IS NULL and renewal_due_on is NOT NULL")
    puts "For some reason there is a handfull of albums where we have a RenewalPaid event, but not a SubscriptionBeginnings event" if albums.size > 0
    albums.each do |album|
      puts "#{album.id} -- #{album.name}"
    end

  end

  def self.down
    # no action needed, this column is dropped in the next downward migration
  end
end
