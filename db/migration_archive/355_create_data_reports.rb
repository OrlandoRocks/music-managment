class CreateDataReports < ActiveRecord::Migration[4.2]
  def self.up
    create_table :data_reports do |t|
      t.column :name, :string, :null => false
      t.column :resolution, :string, :null => false
      #t.column :live_data_report, :boolean, :null => false, :default => false
      t.column :live_update_at, :datetime
      t.column :live_update_limit, :integer, :default => 1440
      t.column :start_on, :date, :null => false
      t.column :last_calculated_on, :date
      t.column :updateable_past, :boolean, :null => false, :default => false
    end
  end

  def self.down
    drop_table :data_reports   
  end
end
