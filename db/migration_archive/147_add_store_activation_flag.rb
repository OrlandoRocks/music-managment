class AddStoreActivationFlag < ActiveRecord::Migration[4.2]
  def self.up
		add_column :stores, :is_active, :boolean, :default => true
	
  end

  def self.down
		remove_column :stores, :is_active
  end
end
