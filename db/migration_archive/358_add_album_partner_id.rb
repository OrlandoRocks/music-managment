class AddAlbumPartnerId < ActiveRecord::Migration[4.2]
  def self.up
    add_column :albums, :partner_id, :integer 
  end

  def self.down
    remove_column :albums, :partner_id
  end
end
