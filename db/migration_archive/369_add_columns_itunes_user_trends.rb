class AddColumnsItunesUserTrends < ActiveRecord::Migration[4.2]
  def self.up
   
    
    
    add_column :itunes_user_trends, :album, :string
    add_column :itunes_user_trends, :song, :string
    add_column :itunes_user_trends, :person, :string, :limit => 10
  end

  def self.down
    remove_column :itunes_user_trends, :album
    remove_column :itunes_user_trends, :song
    remove_column :itunes_user_trends, :person
  end
end
