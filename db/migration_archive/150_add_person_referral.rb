class AddPersonReferral < ActiveRecord::Migration[4.2]
  def self.up
		add_column :people, :referral, :string, :default => nil
  end

  def self.down
		remove_column :people, :referral
  end
end
