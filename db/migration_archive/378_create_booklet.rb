class CreateBooklet < ActiveRecord::Migration[4.2]
  def self.up
    create_table :booklets do |t|
      t.column "album_id", :integer
      t.column "s3_asset_id", :integer
      t.column "created_at", :datetime
      t.column "updated_at", :datetime
    end
  end

  def self.down
    drop_table :booklets
  end
end
