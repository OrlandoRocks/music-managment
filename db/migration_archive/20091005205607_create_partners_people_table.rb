class CreatePartnersPeopleTable < ActiveRecord::Migration[4.2]
  def self.up
    execute "DROP TABLE IF EXISTS `partners_people`"
    
    execute"
      CREATE TABLE `partners_people` (
        `partner_id` SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Foreign Key from Partner table',
        `person_id` INT(10) UNSIGNED NOT NULL COMMENT 'Foreign Key from People table',
        `start_on` DATE NOT NULL COMMENT 'Contract start date',
        `stop_on` DATE DEFAULT NULL COMMENT 'Contract end date',
        KEY `partner_id` (`partner_id`),
        KEY `person_id` (`person_id`),
        CONSTRAINT `FK_partners_people` FOREIGN KEY (`partner_id`) REFERENCES `partners` (`id`)
      ) ENGINE=INNODB DEFAULT CHARSET=utf8;
    "
  end

  def self.down
    execute "DROP TABLE IF EXISTS `partners_people`"
  end
end
