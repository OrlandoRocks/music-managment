class AddCertLastDiscountAmount < ActiveRecord::Migration[4.2]
  def self.up
    add_column :certs, :last_discount_amount_cents, :integer, :null => true, :default => nil
  end

  def self.down
    remove_column :certs, :last_discount_amount_cents
  end
end
