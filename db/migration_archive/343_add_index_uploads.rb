class AddIndexUploads < ActiveRecord::Migration[4.2]
  def self.up
    add_index :uploads, :uploaded_filename
  end

  def self.down
  end
end



# BEFORE INDEX

# mysql> explain SELECT * FROM uploads WHERE (uploads.uploaded_filename = 'a6d53d24-3eab-4006-aa2a-6d5a88d92436.wav')  LIMIT 1;
# +----+-------------+---------+------+---------------+------+---------+------+--------+-------------+
# | id | select_type | table   | type | possible_keys | key  | key_len | ref  | rows   | Extra       |
# +----+-------------+---------+------+---------------+------+---------+------+--------+-------------+
# |  1 | SIMPLE      | uploads | ALL  | NULL          | NULL | NULL    | NULL | 257212 | Using where | 
# +----+-------------+---------+------+---------------+------+---------+------+--------+-------------+
# 1 row in set (0.00 sec)

# BENCHMARK

# mike@joba /var/www/tunecore/release-4.0/lib/tasks $ rake benchmark:upload_select
# (in /var/www/tunecore/release-4.0)
# Rehearsal ------------------------------------------
# select   0.230000   0.030000   0.260000 ( 33.917644)
# --------------------------------- total: 0.260000sec
# 
#              user     system      total        real
# select   0.190000   0.010000   0.200000 ( 34.373244)


# AFTER INDEX

# mysql> explain SELECT * FROM uploads WHERE (uploads.uploaded_filename = 'a6d53d24-3eab-4006-aa2a-6d5a88d92436.wav')  LIMIT 1;
# +----+-------------+---------+------+--------------------------------+--------------------------------+---------+-------+------+-------------+
# | id | select_type | table   | type | possible_keys                  | key                            | key_len | ref   | rows | Extra       |
# +----+-------------+---------+------+--------------------------------+--------------------------------+---------+-------+------+-------------+
# |  1 | SIMPLE      | uploads | ref  | upload_uploaded_filename_index | upload_uploaded_filename_index | 768     | const |    1 | Using where | 
# +----+-------------+---------+------+--------------------------------+--------------------------------+---------+-------+------+-------------+
# 

# BENCHMARK

# mike@joba /var/www/tunecore/release-4.0/lib/tasks $ rake benchmark:upload_select
# (in /var/www/tunecore/release-4.0)
# Rehearsal ------------------------------------------
# select   0.230000   0.040000   0.270000 (  0.270078)
# --------------------------------- total: 0.270000sec
# 
#              user     system      total        real
# select   0.210000   0.000000   0.210000 (  0.236379)

