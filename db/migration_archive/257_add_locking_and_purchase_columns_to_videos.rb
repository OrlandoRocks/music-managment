class AddLockingAndPurchaseColumnsToVideos < ActiveRecord::Migration[4.2]
  def self.up
    add_column :videos, :locked_by_id, :integer, :null => true, :default => nil
    add_column :videos, :locked_by_type, :string, :null => true, :default => nil, :limit => 30
    add_column :videos, :price_policy_id, :integer, :null => false, :default => nil
    default_policy = PricePolicy.refreshed[Video.default_price_policy_name]
    execute "update videos set price_policy_id = #{default_policy.id} where price_policy_id is null or price_policy_id = 0"
  end

  def self.down
    remove_column :videos, :locked_by_id
    remove_column :videos, :locked_by_type
    remove_column :videos, :price_policy_id
  end
end
