class AdSnippets < ActiveRecord::Migration[4.2]
  def self.up
    create_table :javascript_snippets do |t|
      t.text :snippet
      t.string :location_key, :null => false
      t.integer :partner_id, :null => true
    end
  end

  def self.down
    drop_table :javascript_snippets
  end
end
