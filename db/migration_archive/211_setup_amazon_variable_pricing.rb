class SetupAmazonVariablePricing < ActiveRecord::Migration[4.2]
  def self.up
    Store.create(:name => "Amazon", :short_name => "Amazon", :abbrev => 'az', :position => 13, :needs_rights_assignment => false, :is_active => true, :variable_pricing => true)
    VariablePrice.create(:store_id => 13, :price_code => 'FRONTLINE', :price_code_display => 'Frontline', :active => true, :position => 1)
    VariablePrice.create(:store_id => 13, :price_code => 'MIDLINE', :price_code_display => 'Midline', :active => true, :position => 2)
    VariablePrice.create(:store_id => 13, :price_code => 'CATALOG', :price_code_display => 'Catalog', :active => true, :position => 3)
    VariablePrice.create(:store_id => 13, :price_code => 'SPECIAL', :price_code_display => 'Special', :active => true, :position => 4)

  end

  def self.down
    if s = Store.find_by(name: "Amazon")
      s.destroy
    end
    VariablePrice.where("store_id = 13").destroy_all
  end
end
