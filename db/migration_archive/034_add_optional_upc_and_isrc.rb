class AddOptionalUpcAndIsrc < ActiveRecord::Migration[4.2]
  def self.up
    add_column :albums, :optional_upc, :text, :null => true, :default => nil
    add_column :songs, :optional_isrc, :text, :null => true, :default => nil
  end

  def self.down
    remove_column :songs, :optional_isrc
    remove_column :albums, :optional_upc
  end
end
