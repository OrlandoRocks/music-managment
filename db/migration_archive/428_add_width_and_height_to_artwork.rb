class AddWidthAndHeightToArtwork < ActiveRecord::Migration[4.2]
  def self.up
    add_column :artworks, :width, :integer
    add_column :artworks, :height, :integer
  end

  def self.down
    remove_column :artworks, :width
    remove_column :artworks, :height
  end
end
