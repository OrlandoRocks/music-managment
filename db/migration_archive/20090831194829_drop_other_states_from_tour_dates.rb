class DropOtherStatesFromTourDates < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :tour_dates, :other_state
  end

  def self.down
    add_column :tour_dates, :other_state, :string, :limit => 50, :null=> false, :default => ""
  end
end
