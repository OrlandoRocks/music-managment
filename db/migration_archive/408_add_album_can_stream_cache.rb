class AddAlbumCanStreamCache < ActiveRecord::Migration[4.2]
  def self.up
    add_column :albums, :can_stream_cache, :boolean
  end

  def self.down
    remove_column :albums, :can_steam_cache
  end
end
