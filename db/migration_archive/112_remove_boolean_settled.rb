class RemoveBooleanSettled < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :invoices, :settled
  end

  def self.down
    add_column :invoices, :settled, :boolean, :null => false, :default => false
  end
end
