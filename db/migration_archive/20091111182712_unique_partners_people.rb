class UniquePartnersPeople < ActiveRecord::Migration[4.2]

  INDEX_NAME = "person_id_partner_id"

  def self.up

    say_with_time "Removing duplicates" do
      result = ActiveRecord::Base.connection.execute("SELECT partner_id, person_id, MIN(start_on) FROM partners_people GROUP BY partner_id, person_id HAVING COUNT(*) > 1")
      result.each do |row|
        ActiveRecord::Base.connection.execute("DELETE FROM partners_people WHERE partner_id = #{row[0]} AND person_id = #{row[1]}")
        PartnerPerson.create(
          :partner_id => row[0],
          :person_id => row[1],
          :start_on => row[2]
        )
      end
    end

    execute("DROP INDEX person_id ON partners_people")
    execute("CREATE UNIQUE INDEX #{INDEX_NAME} ON partners_people (person_id, partner_id)")
  end

  def self.down
    execute("DROP INDEX #{INDEX_NAME} ON partners_people")
    execute("CREATE INDEX person_id ON partners_people (person_id)")
  end
end
