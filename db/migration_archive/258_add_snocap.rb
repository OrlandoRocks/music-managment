class AddSnocap < ActiveRecord::Migration[4.2]
  def self.up
    Store.create(:name => "SNOCAP", :short_name => "SNOCAP", :abbrev => 'sn', :position => 14, :needs_rights_assignment => false, :is_active => false, :variable_pricing => false)
  end

  def self.down
    if s = Store.find_by(short_name: "SNOCAP")
      s.destroy
    end
  end
end
