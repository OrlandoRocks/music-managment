class CreateInventories < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `inventories` (
                `id` int(10) unsigned NOT NULL auto_increment,
                `person_id` int(10) unsigned NOT NULL COMMENT 'references people.id',
                `title` varchar(30) NOT NULL COMMENT 'the title to display',
                `product_item_id` int UNSIGNED not null COMMENT 'references product_items.id',
                `purchase_id` int(10) unsigned default NULL COMMENT 'references purchases.id',
                `parent_id` int(10) unsigned default NULL COMMENT 'self-referential, ties certain groups of inventory records together for usage',
                `inventory_type` ENUM('Album', 'Single', 'Ringtone', 'Video', 'ShippingLabel', 'ItunesUserReport', 'Booklet', 'Song', 'Salepoint') default NULL COMMENT 'the type of inventory to be used',
                `quantity` mediumint(9) NOT NULL default '0' COMMENT 'initial quantity purchased by a customer',
                `quantity_used` mediumint(9) NOT NULL default '0' COMMENT 'cached quantity used by customer interaction with the system',
                `unlimited` tinyint(1) NOT NULL default '0' COMMENT 'flag for determining if an unlimited number of this item can be used',
                `expires_at` timestamp NULL default NULL COMMENT 'date at which the inventory item is no longer available to the user',
                `created_at` timestamp NULL default NULL,
                PRIMARY KEY  (`id`),
                KEY `person_id` (`person_id`),
                KEY `purchase_id` (`purchase_id`),
                KEY `entitlement_type` (`inventory_type`),
                KEY `parent_id` (`parent_id`),
                KEY `product_item_inventories_id` (`product_item_id`),
                CONSTRAINT `fk_person_inventories_id` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`),
                CONSTRAINT `fk_product_inventories_id` FOREIGN KEY (`product_item_id`) REFERENCES `product_items` (`id`)
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
            
    execute up_sql
  end

  def self.down
    drop_table :inventories
  end
end
