class CreateEvents < ActiveRecord::Migration[4.2]
  def self.up
    create_table :events do |t|
      t.column :bundle_id, :integer
      t.column :album_id, :integer
      t.column :actor, :string
      t.column :description, :text
      t.column :new_status, :string
      t.column :created_at, :datetime
      t.column :modified_at, :datetime
    end
  end

  def self.down
    drop_table :events
  end
end
