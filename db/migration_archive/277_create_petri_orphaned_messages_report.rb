class CreatePetriOrphanedMessagesReport < ActiveRecord::Migration[4.2]
  def self.up
    create_table :petri_orphans do |t|
      t.column :created_at, :datetime, :null => false
      t.column :sqs_message_id, :string, :null => false
      t.column :sqs_message_body, :text, :null => false
    end
  end

  def self.down
    drop_table :petri_orphans
  end
end
