class ChangeFreeStoresEngine < ActiveRecord::Migration[4.2]
  def self.up
    transaction do
      certs = Cert.where('cert_engine = ? and expiry_date = ?', 'free_stores', '2008-01-15 16:56:36')

			if certs != []
      	execute %Q|update certs set cert_engine = 'gc_prepaid', engine_params = '', expiry_date = '#{Date.civil(2008,12,31).to_s}' where id in (#{certs.collect(&:id).join(',')})|
      	raise "mismatch after update" unless Cert.count(:conditions => {:cert_engine => 'gc_prepaid'}) == certs.length
			end
    end
  end

  def self.down
  end
end
