class CreateVariablePrices < ActiveRecord::Migration[4.2]
  def self.up
      create_table :variable_prices do |t|
        t.column :store_id, :integer
        t.column :price_code, :string
        t.column :price_code_display, :string
        t.column :active, :boolean
        t.column :position, :integer
      end
    
  end

  def self.down
    drop_table :variable_prices
  end
end
