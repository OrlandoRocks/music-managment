class CreateAlbumComments < ActiveRecord::Migration[4.2]
  def self.up
    create_table :album_comments do |t|
      t.column :album_id, :integer
      t.column :person_id, :integer
      t.column :created_at, :datetime
      t.column :comment, :text
    end
  end

  def self.down
    drop_table :album_comments
  end
end
