class AddPeoplePartnerId < ActiveRecord::Migration[4.2]
  def self.up
    add_column :people, :partner_id, :integer
  end

  def self.down
    remove_column :people, :partner_id
  end
end
