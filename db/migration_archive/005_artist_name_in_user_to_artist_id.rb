class ArtistNameInUserToArtistId < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :users, :artistname
    add_column :users, :artist_id, :integer
    add_column :albums, :artist_id, :integer
  end

  def self.down
    add_column :users, :artistname, :string, :limit => 80
    remove_column :users, :artist_id 
  end
end
