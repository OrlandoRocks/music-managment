class AddExcelExportColumnsToCheckTransfer < ActiveRecord::Migration[4.2]

  def self.up
    add_column :check_transfers, :export, :boolean, :default => false
    add_column :check_transfers, :export_date, :timestamp, :null => true
  end
  
  def self.down
    remove_column :check_transfers, :export
    remove_column :check_transfers, :export_date
  end
end
