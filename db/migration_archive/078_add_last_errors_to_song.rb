class AddLastErrorsToSong < ActiveRecord::Migration[4.2]
  def self.up
    add_column :songs, :last_errors, :text
  end

  def self.down
    remove_column :songs, :last_errors
  end
end
