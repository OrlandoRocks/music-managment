class AddColsToRecipientLists < ActiveRecord::Migration[4.2]
  def self.up
    add_column :recipient_lists, :mailing_id, :integer
    add_column :recipient_lists, :user_query_id, :integer
    add_column :recipient_lists, :scheduled_mailing_id, :integer
    add_column :recipient_lists, :recipients, :string
  end

  def self.down
    remove_column :recipient_lists, :mailing_id
    remove_column :recipient_lists, :user_query_id
    remove_column :recipient_lists, :scheduled_mailing_id
    remove_column :recipient_lists, :recipients
  end
end
