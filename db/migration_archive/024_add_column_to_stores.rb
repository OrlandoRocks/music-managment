class AddColumnToStores < ActiveRecord::Migration[4.2]
  def self.up
    # OUCH! is this ever a cruel and unusual hack.  Might
    # get less cruel and unusual with rails1.1
    #Dispatcher.reset_application!

    add_column :stores, :short_name,  :string, :limit => 10
    Store.reset_column_information

    name_arr = { 'us'=>'iTunesUS','au'=>'iTunesAU','ca'=>'iTunesCA','eu'=>'iTunesEU','jp'=>'iTunesJP','rh'=>'RhapsodyRH' }

    Store.all.each { |s|
      s.update_attribute(:short_name,name_arr[s.abbrev])
    }

  end

  def self.down
    remove_column :stores, :short_name
  end
end
