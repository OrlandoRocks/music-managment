class ArtistNameTitlecaseInsensitiveMigration < ActiveRecord::Migration[4.2]
  def self.up
    execute "ALTER TABLE `artists` MODIFY COLUMN `name` VARCHAR(120) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL;"
  end

  def self.down
    execute "ALTER TABLE `artists` MODIFY COLUMN `name` VARCHAR(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL;"
  end
end
