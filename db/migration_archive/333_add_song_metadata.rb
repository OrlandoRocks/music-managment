class AddSongMetadata < ActiveRecord::Migration[4.2]
# this is for amazon song level metadata

  def self.up
    add_column :songs, :album_only, :boolean
    add_column :songs, :free_song, :boolean
    add_column :songs, :price_override, :float
  end

  def self.down
    remove_column :songs, :album_only
    remove_column :songs, :free_song
    remove_column :songs, :price_override
  end
end
