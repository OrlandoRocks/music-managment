class CreateVideos < ActiveRecord::Migration[4.2]
  def self.up
      create_table :videos do |t|
        t.column :person_id, :integer
        t.column :label_id, :integer
        t.column :album_id, :integer
        t.column :artist_id, :integer
        t.column :name, :string
        t.column :version, :string
        t.column :closed_captioned, :boolean
        t.column :album_id, :integer
        t.column :director, :string
        t.column :dp, :string
        t.column :editor, :string
        t.column :producer, :string
        t.column :executive_producer, :string
        t.column :production_company, :string
        t.column :first_release_date, :date
        t.column :available_date, :date
        t.column :tunecore_isrc, :string
        t.column :optional_isrc, :string
        t.column :length, :string
        t.column :weight, :float
        t.column :shipping_service_type, :string
        t.column :finalized_at, :datetime
        t.column :orig_release_year, :date
        t.column :is_deleted, :boolean, :default => false
        t.column :accepted_format, :boolean, :default => false
        t.column :shipping_label, :binary
        t.column :tracking_number, :string
      end
  end

  def self.down
    drop_table :videos
  end
end
