class CreatePurchases < ActiveRecord::Migration[4.2]
  def self.up
    create_table :purchases do |t|
      t.column :created_at, :datetime, :null => false, :default => nil
      t.column :sti_type, :string, :limit => 30, :null => false, :default => nil
      t.column :person_id, :integer, :null => false, :default => nil
      t.column :invoice_id, :integer, :null => true, :default => nil
      t.column :related_id, :integer, :null => false, :default => nil
      t.column :related_type, :string, :limit => 30, :null => false, :default => nil
      t.column :cost_cents, :integer, :null => true, :default => nil
      t.column :discount_cents, :integer, :null => true, :default => nil
      t.column :paid_at, :datetime, :null => true, :default => nil
    end

    add_index :purchases, [:person_id]
    add_index :purchases, [:invoice_id]
    add_index :purchases, [:paid_at]
    add_index :purchases, [:related_id, :related_type]
    
    create_table :album_purchase_items do |t|
      t.column :created_at, :datetime, :null => false, :default => nil
      t.column :purchase_id, :integer, :null => false, :default => nil
      t.column :purchaseable_id, :integer, :null => false, :default => nil
      t.column :purchaseable_type, :string, :limit => 30, :null => false, :default => nil
    end

    add_index :album_purchase_items, [:purchase_id]
    add_index :album_purchase_items, [:purchaseable_id, :purchaseable_type], :name => 'album_purchase_items_on_purchaseable'

    add_column :albums, :purchase_id, :integer, :null => true, :default => nil
    add_index :albums, :purchase_id
    add_column :songs, :purchase_id, :integer, :null => true, :default => nil
    add_index :songs, :purchase_id
    add_column :salepoints, :purchase_id, :integer, :null => true, :default => nil
    add_index :salepoints, :purchase_id
    add_column :certs, :purchase_id, :integer, :null => true, :default => nil
    add_index :certs, :purchase_id
    add_column :gainlogs, :purchase_id, :integer, :null => true, :default => nil
    add_index :gainlogs, :purchase_id
  end

  def self.down
    drop_table :purchases
    drop_table :album_purchase_items
    remove_column :albums, :purchase_id
    remove_column :songs, :purchase_id
    remove_column :salepoints, :purchase_id
    remove_column :certs, :purchase_id
    remove_column :gainlogs, :purchase_id
  end
end
