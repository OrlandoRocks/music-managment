class AddCanadaToPartnersForCms < ActiveRecord::Migration[4.2]
  def self.up
    Partner.create(:name=>'Tunecore Canada',:domain=>'ca.tunecore.com', :skin=>'tunecore_canada')
  end

  def self.down
    execute("DELETE FROM partners where domain='tunecore_canada'")
  end
end
