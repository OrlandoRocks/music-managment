class AddStepsRequired < ActiveRecord::Migration[4.2]
  def self.up
    add_column :albums, :steps_required, :integer
  end

  def self.down
    remove_column :albums, :steps_required
  end
end
