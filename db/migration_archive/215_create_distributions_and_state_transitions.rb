class CreateDistributionsAndStateTransitions < ActiveRecord::Migration[4.2]
  
  def self.up
    
    create_table :petri_bundles do |t|
      t.column :album_id, :integer, :null => false
      t.column :state, :string, :null => false
    end
    
    add_index :petri_bundles, :album_id
    
    create_table :distributions do |t|
      t.column :state, :string, :null => false
      t.column :converter_class, :string, :null => false
      t.column :petri_bundle_id, :integer, :null => false
      t.column :sqs_message_id, :string, :null => true
    end
    add_index :distributions, :sqs_message_id
    add_index :distributions, :petri_bundle_id


    create_table(:distributions_salepoints, :id => false) do |t|
      t.column :distribution_id, :integer, :null => false
      t.column :salepoint_id, :integer, :null => false
    end
    
    add_index :distributions_salepoints, :distribution_id
    add_index :distributions_salepoints, :salepoint_id
    
    create_table :transitions do |t|
      t.column :state_machine_id, :integer, :null => false
      t.column :state_machine_type, :string, :null => false
      t.column :actor, :string, :null => false
      t.column :old_state, :string, :null => false
      t.column :new_state, :string, :null => false
      t.column :message, :text
      t.column :backtrace, :text
      t.column :time, :datetime, :null => false
    end
    
    add_index :transitions, :state_machine_id
    
  end
  
  def self.down
    drop_table :petri_bundles
    drop_table :distributions
    drop_table :transitions
    drop_table :distributions_salepoints
  end
end
