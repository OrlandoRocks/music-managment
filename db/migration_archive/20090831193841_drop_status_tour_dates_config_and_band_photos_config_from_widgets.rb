#Created By:    Ed Cheung
#Date:          2009-08-31
#Purpose:       Dropping unused columns from widgets due to new application design changes
#               S3 will no longer be serving tour dates and band photos. Front-end app server
#               will be serving the XMLs to the client's flash app. Status column is never used.
#
################################################################################

class DropStatusTourDatesConfigAndBandPhotosConfigFromWidgets < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :widgets, :tour_dates_config
    remove_column :widgets, :band_photos_config
    remove_column :widgets, :status
  end

  def self.down
    add_column :widgets, :tour_dates_config, :blob
    add_column :widgets, :band_photos_config, :blob
    add_column :widgets,  :status, :integer, :limit => 1, :default => 0
  end
end
