class AddCertsTable < ActiveRecord::Migration[4.2]
  def self.up
    create_table :certs, :force => true do |t|
      t.column :cert, :string, :limit => 16, :null => false
      t.column :date_used, :datetime
      t.column :expiry_date, :datetime
      t.column :percent_off, :integer, :null => false
      t.column :person_id, :integer
      t.column :album_id, :integer      
    end
  end

  def self.down
    drop_table :certs
  end
end
