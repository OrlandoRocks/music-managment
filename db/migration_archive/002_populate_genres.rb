class PopulateGenres < ActiveRecord::Migration[4.2]
  def self.up
    
#     songstates = %w( entered paid uploaded completed )
#     songstates.each{|state|
#       SongsStates.create :songstatus=>state
#     }
#     
#     orderstates = %w( temp user paid uploaded )
#     orderstates.each { |myorder|
#       OrdersStates.create :orderstatus=>myorder
#     }
#     
#     albumstates = %w( entered artwork songs confirmed paid uploaded completed )
#     albumstates.each { |album|
#       AlbumsStates.create :albumstatus=>album
#     }
          
    genrearray = ["Alternative","Audiobooks","Blues","Children\'s Music","Classical","Comedy","Dance", 
              "Electronic","Folk","French Pop","German Folk","German Pop","Hip Hop/Rap","Holiday",
              "Inspirational","Jazz","Latin","New Age","Opera","Pop","R&B/Soul","Reggae","Rock",
              "Soundtrack","Spoken Word","Vocal","World"]
    genrearray.each { |genre|
      Genre.create :name=>genre
    }
    
#     countries = [["United States","us","0"],["Australia","au","99"],["Canada","ca","99"],
#                 ["European Union","eu","99"],["Japan","jp","99"],["United Kingdom","uk","99"]]
#     countries.each { |country|
#       Country.create :name=>country[0],:abbrev=>country[1],:price_cents=>country[2]
#     }
   
    items = [["Album","898"],["Extra Song","75"],["Custom Art","99"]]
    items.each { |item|
      Item.create :name=>item[0], :price_cents=>item[1]
    }
  end

  def self.down    
  end
end
