class RemoveTemplateFromBackgroundEffect < ActiveRecord::Migration[4.2]
  def self.up
    remove_column(:background_effects, :template)
  end

  def self.down
    add_column(:background_effects, :template, :string)
  end
end
