#Created By:    Ed Cheung
#Date:          2009-08-19
#Purpose:       The following migration creates a media assets table to store
#               meta data for user's uploaded files- images, documents, songs, etc.
#               For Widget 2 release, it'll store band photo's meta data and eventually
#               expand to support other file types. This table is also design to
#               use Rails ActiveRecord Single Table Inheritance using MediaAsset
#               class as the base and every other AssetType class will extend to
#               MediaAsset class
#
################################################################################
#
#Column Description:
#id:              unique identifier, primary key
#type:            AssetType, will be map to a class name, for now it'll be 'BandPhoto'
#description:     data provided by the user
#mime_type:       file format, 'image/jpeg', 'image/png', 'image/tiff', 'image/bmp', 'image/gif'
#width:           image width, used when the mime_type is an image
#height:          image height, used when the mime_type is an image
#bit_depth:       bit depth, used when the mime_type is an image
#size:            size of file in bytes
#created_at:      upload time
#updated_at:      upload time
#local_asset_id:  foreign key
#s3_asset_id:     foreign key
#person_id:       foreign key

################################################################################

class CreateMediaAssets < ActiveRecord::Migration[4.2]
  def self.up
    create_media_assets_table=<<-EOSTRING
      CREATE TABLE media_assets (
        id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
        description VARCHAR(500) DEFAULT NULL COMMENT 'Description of asset',
        width SMALLINT(6) UNSIGNED DEFAULT NULL COMMENT 'Image width',
        height SMALLINT(6) UNSIGNED DEFAULT NULL COMMENT 'Image Height',
        bit_depth TINYINT(4) UNSIGNED DEFAULT NULL COMMENT 'Bit Depth',
        size INT(10) UNSIGNED NOT NULL COMMENT 'Size of file in bytes',
        local_asset_id INT(10) UNSIGNED DEFAULT NULL COMMENT 'Foreign key to local_assets table.',
        s3_asset_id INT(10) UNSIGNED DEFAULT NULL COMMENT 'Foreign key to sc_assets table.',
        person_id INT(10) UNSIGNED NOT NULL COMMENT 'Foreigh key to people table.',
        type ENUM('BandPhoto') NOT NULL COMMENT 'Asset type.',
        mime_type ENUM('image/jpeg','image/png','image/tiff','image/bmp', 'image/gif') NOT NULL COMMENT 'Asset mime type.',
        created_at DATETIME NOT NULL COMMENT 'Date record Created.',
        updated_at DATETIME NOT NULL COMMENT 'Date record Updated.',
       PRIMARY KEY  (id),
       KEY s3_asset_id (s3_asset_id),
       KEY person_id (person_id),
       KEY local_asset_id (local_asset_id),
       CONSTRAINT FK_media_assets FOREIGN KEY (local_asset_id) REFERENCES local_assets (id)
      ) ENGINE=INNODB DEFAULT CHARSET=utf8;
    EOSTRING
    execute create_media_assets_table
  end

  def self.down
    drop_table :media_assets
  end
end
