class AddApprovaltoAlbum < ActiveRecord::Migration[4.2]
  def self.up
        add_column :albums, :is_approved, :boolean, :default => false
        add_column :albums, :approval_date, :datetime
  end

  def self.down
        remove_column :albums, :is_approved
        remove_column :albums, :approval_date
  end
end


# this allows the user to approve the terms and conditions for their album.  the account id is the only one that can approve an album. 
# is approved is set when the user clicks on the 'i have read the terms and conditions' shortly before paying.
