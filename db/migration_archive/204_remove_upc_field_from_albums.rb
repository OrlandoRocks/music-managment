class RemoveUpcFieldFromAlbums < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :albums, :tunecore_upc
    remove_column :albums, :optional_upc
  end

  def self.down
    add_column :albums, :tunecore_upc, :string
    add_column :albums, :optional_upc, :string
  end
end
