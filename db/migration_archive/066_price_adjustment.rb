class PriceAdjustment < ActiveRecord::Migration[4.2]
  def self.up
    Item.find_by(name: "album").update_attribute(:price_cents,"998")
    add_column :albums, :legacy_annual_fee, :boolean, :null=>false, :default=>false
    Album.all.each do |a|
      a.update_attribute(:legacy_annual_fee,true)
    end

  end

  def self.down
    Item.find_by(name: "album").update_attribute(:price_cents,"798")
    remove_column :albums, :legacy_annual_fee
  end
end
