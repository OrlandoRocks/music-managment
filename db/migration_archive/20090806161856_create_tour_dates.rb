#Created By:    Ed Cheung
#Date:          2009-08-06
#Purpose:       The following migration file creates a tour dates table that
#               will be used to store the time, location, and venue of an artist's
#               tour date. This table is needed as part of a feature for Widget 2 release
#
################################################################################
#
#Column Description:
#id:            unique identifier for a tour date
#person_id:     id referencing to persons table
#artist_id:     id referencing to artists table
#country_id:    id referencing to countries table
#us_state_id:   id referencing to us states table. If country_id references to
#               any country other than USA then us_state_id will by default
#               reference to a record in us states table called "OTHER"
#other_state:   takes any user inputted data for a foreign, non-USA state. If
#               country_id reference USA then it'll be an empty string otherwise
#               it'll take whatever value user inputs
#city:          takes whatever user inputs for their city
#venue:         takes whatever user inputs for the place/venue where the tour date is hosted
#information:   takes whatever user inputs for additional information regarding the tour date
#               such as links to purchasing tickets, directions to get to the venue, etc.
#created_at:    time the record was created
#updated_at:    time the record was modified
################################################################################
#
#Column Data Type:
#
#id:            medium int, not null, auto increment, primary key
#person_id:     medium int, not null, foreign key
#artist_id:     medium int, not null, foreign key
#country_id:    small int, not null, foreign key
#us_state_id:   tiny int, not null, foreign key
#other_state:   varchar(50), not null, default is empty string
#city:          varchar(50)
#venue:         varchar(50)
#information:   varchar(500)
#created_at:    datetime
#updated_at:    datetime
################################################################################

class CreateTourDates < ActiveRecord::Migration[4.2]
  def self.up
    create_table :tour_dates do |t|
      t.integer   :person_id, :null => false
      t.integer   :artist_id, :null => false
      t.integer   :country_id,  :limit => 2, :null=> false
      t.integer   :us_state_id, :limit => 1, :null=> false
      t.string    :other_state, :limit => 50, :null=> false, :default => ""
      t.string    :city,        :limit => 50
      t.string    :venue,       :limit => 50
      t.string    :information, :limit => 500
      t.datetime  :event_at
      t.timestamps
    end
  end

  def self.down
    drop_table :tour_dates
  end
end
