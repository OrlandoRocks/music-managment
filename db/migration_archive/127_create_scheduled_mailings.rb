class CreateScheduledMailings < ActiveRecord::Migration[4.2]
  def self.up
    create_table :scheduled_mailings do |t|
      t.column :mailing_id, :integer
      t.column :user_query_id, :integer
      t.column :scheduled_at, :datetime
      t.column :job_started_at, :datetime
      t.column :job_finished_at, :datetime
    end
  end

  def self.down
    drop_table :scheduled_mailings
  end
end
