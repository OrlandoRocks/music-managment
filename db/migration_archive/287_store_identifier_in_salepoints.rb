class StoreIdentifierInSalepoints < ActiveRecord::Migration[4.2]
  def self.up
    add_column :salepoints, :store_identifier, :string, :limit => 18
    execute("INSERT INTO stores VALUES(6,'iTunes UK','uk','iTunesUK',6,0,0,0)") #re-add iTunes UK
    create_table :itunes_comprehensives, :id => false  do |t|
      t.column :apple_id, :string, :limit => 19
      t.column :upc, :string, :limit => 18
      t.column :grid, :string, :limit => 2
      t.column :vendor_id, :string, :limit => 19
      t.column :artist, :string, :limit => 25
      t.column :album, :string, :limit => 25
      t.column :genre, :string, :limit => 25
      t.column :total_tracks, :integer
      t.column :total_discs, :integer
      t.column :is_complete, :string, :limit => 7
      t.column :itunes_plus_ready, :string, :limit => 7
      t.column :provider, :string, :limit => 15
      t.column :label_name, :string, :limit => 25
      t.column :status, :string, :limit => 12
      t.column :ww, :string, :limit => 1
      t.column :au, :string, :limit => 1
      t.column :at, :string, :limit => 1
      t.column :be, :string, :limit => 1
      t.column :ca, :string, :limit => 1
      t.column :ch, :string, :limit => 1
      t.column :de, :string, :limit => 1
      t.column :dk, :string, :limit => 1
      t.column :es, :string, :limit => 1
      t.column :fi, :string, :limit => 1
      t.column :fr, :string, :limit => 1
      t.column :gb, :string, :limit => 1
      t.column :gr, :string, :limit => 1
      t.column :ie, :string, :limit => 1
      t.column :it, :string, :limit => 1
      t.column :jp, :string, :limit => 1
      t.column :lu, :string, :limit => 1
      t.column :nl, :string, :limit => 1
      t.column :no, :string, :limit => 1
      t.column :nz, :string, :limit => 1
      t.column :pt, :string, :limit => 1
      t.column :se, :string, :limit => 1
      t.column :us, :string, :limit => 1
      
    end
    add_index :itunes_comprehensives, [:upc]
  end
  def self.down
    remove_column :salepoints,:store_identifier
    execute("DELETE FROM stores where id = 6 AND short_name= 'iTunesUK' limit 1")
    drop_table :itunes_comprehensives
  end
end
