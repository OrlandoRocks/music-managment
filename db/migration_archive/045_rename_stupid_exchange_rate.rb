class RenameStupidExchangeRate < ActiveRecord::Migration[4.2]
  def self.up
    # Worried about clobbering information here so this is going to be 
    # done in two steps - only the second step will be destructive.
    # First, we rename our stupid text column and pull the information 
    # into a newly created exchange_rate_fixed bigint(19) column.  Note
    # that 19 seems to be the largest we can get in there - plenty big 
    # for our needs as far as I can tell.
    # The next step - removing the _stupid column will follow in another 
    # migration after this proves okay in production.
    rename_column :store_intakes, :exchange_rate_fixed, :exchange_rate_stupid

    # Sadly we have to drop to pure sql in the migration to support this...
    execute('ALTER TABLE store_intakes ADD exchange_rate_fixed bigint(19) DEFAULT 0 NOT NULL AFTER exchange_symbol')
    execute('UPDATE store_intakes SET exchange_rate_fixed = exchange_rate_stupid')
  end

  def self.down
    remove_column :store_intakes, :exchange_rate_fixed
    rename_column :store_intakes, :exchange_rate_stupid, :exchange_rate_fixed
  end
end
