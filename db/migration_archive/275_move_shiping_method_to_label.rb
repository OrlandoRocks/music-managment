class MoveShipingMethodToLabel < ActiveRecord::Migration[4.2]
  def self.up
    add_column :shipping_labels, :shipping_service_type, :string, :limit => 30
    
    execute "UPDATE shipping_labels JOIN videos SET shipping_labels.shipping_service_type = videos.shipping_service_type  WHERE shipping_labels.video_id = videos.id"
    
    remove_column :videos, :shipping_service_type
  end

  def self.down
    add_column :videos, :shipping_service_type, :string, :limit => 30
    
    execute "UPDATE videos JOIN shipping_labels SET videos.shipping_service_type = shipping_labels.shipping_service_type WHERE shipping_labels.video_id = videos.id"
    
    remove_column :shipping_labels, :shipping_service_type
  end
end
