class AddStreamingOptionsToAlbums < ActiveRecord::Migration[4.2]
  def self.up
    add_column :albums, :full_stream_widget, :boolean, :default => false
    add_column :albums, :full_stream_iphone, :boolean, :default => false
  end

  def self.down
    remove_column :albums, :full_stream_widget
    remove_column :albums, :full_stream_iphone
  end
end
