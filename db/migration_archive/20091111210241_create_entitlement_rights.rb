class CreateEntitlementRights < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `entitlement_rights`(
                `id` int UNSIGNED not null auto_increment,
                `entitlement_rights_group_id` int UNSIGNED NOT NULL COMMENT 'link to parent group',
                `controller` varchar(40) NOT NULL COMMENT 'name of controller this rights entry has access to',
                `action` varchar(255) NOT NULL COMMENT 'name of action in controller this rights entry has permission to complete',
                `created_at` timestamp null,                
                PRIMARY KEY  (`id`),
                KEY `entitlement_rights_group_id` (`entitlement_rights_group_id`),
                CONSTRAINT `fk_entitlement_rights_group` FOREIGN KEY (`entitlement_rights_group_id`) REFERENCES `entitlement_rights_groups`(`id`) 
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
                
    execute up_sql
  end

  def self.down
    drop_table :entitlement_rights
  end
end
