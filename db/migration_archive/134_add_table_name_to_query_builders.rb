class AddTableNameToQueryBuilders < ActiveRecord::Migration[4.2]
  def self.up
    add_column :query_builders, :tbl, :string
    rename_column :query_builders, :column_name, :col
    rename_column :query_builders, :column_value, :val
  end

  def self.down
    remove_column :query_builders, :tbl
    rename_column :query_builders, :col, :column_name
    rename_column :query_builders, :val, :column_value
  end
end
