class AddDeletedToAlbum < ActiveRecord::Migration[4.2]
  def self.up
        add_column :albums, :is_deleted, :boolean, :default => false
        add_column :albums, :deleted_date, :datetime
  end

  def self.down
        remove_column :albums, :is_deleted
        remove_column :albums, :deleted_date
  end
end
