class AddNameToWidget < ActiveRecord::Migration[4.2]
  def self.up
    up_sql=%Q(ALTER TABLE `widgets`
              ADD COLUMN `name` VARCHAR(64) NULL AFTER `free_song_text`;
            )
    execute up_sql
  end

  def self.down
    remove_column :widgets, :name
  end
end
