class AddUploadCreatedAt < ActiveRecord::Migration[4.2]
  def self.up
    add_column :uploads, :created_at, :datetime
  end

  def self.down
    remove_column :uploads, :created_at
  end
end
