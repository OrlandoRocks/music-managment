class AddSalepointFinalized < ActiveRecord::Migration[4.2]
  def self.up
    add_column :salepoints, :finalized_at, :datetime, :null => true, :default => nil
  end

  def self.down
    remove_column :salepoints, :finalized_at
  end
end
