#Created By:    Ed Cheung
#Date:          2009-08-14
#Purpose:       The following migration file adds tour_dates and band photos config
#               columns to widgets table. This is necessary for widget 2 release
#               where the addition of the new columns will be used to store
#               configuration data for tour dates and band photos. XML files will
#               also be generated based on these two columns and served via Amazon S3
#               
################################################################################
class AddColumnsTourDatesAndBandPhotosToWidgets < ActiveRecord::Migration[4.2]
  def self.up
    add_column :widgets, :tour_dates_config, :blob
    add_column :widgets, :band_photos_config, :blob
  end

  def self.down
    remove_column :widgets, :tour_dates_config
    remove_column :widgets, :band_photos_config
  end
end
