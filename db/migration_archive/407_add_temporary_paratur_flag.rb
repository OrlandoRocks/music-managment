class AddTemporaryParaturFlag < ActiveRecord::Migration[4.2]
  def self.up
    add_column :people, :sent_to_parature, :boolean, :default => false
  end

  def self.down
    remove_column :people, :sent_to_parature
  end
end
