class MigrateRightsAssignmentData < ActiveRecord::Migration[4.2]
  #
  # Put this into own migration so that it doesn't leave tables inconsistent if the data
  # migration blows up for some reason...
  #

  CHECKS = {
    :mn => :musicnet_check,
    :em => :emusic_check,
    :np => :napster_check,
  }

  def self.up

    # don't run this if the album association doesn't exist. For example if we are building the
    # database for the first time.  It won't work because the salepoints have been migrated to a
    # polymorphic relationship with albums, and that migration hasn't run yet.
    return unless Salepoint.first!= nil

    CHECKS.each do |abbrev, check|
      store = Store.find_by(abbrev: abbrev.to_s)
      store.update_attribute :needs_rights_assignment, true

      salepoints = store.salepoints.includes(:album)
      salepoints.each do |salepoint|
        next if salepoint.album.nil?
        puts "migrating #{check.inspect} for album #{salepoint.album.id}: #{salepoint.album.name}"
        salepoint.update_attribute :has_rights_assignment, salepoint.album.send(check)
      end
    end
  end

  def self.down
    CHECKS.each do |abbrev, check|
      store = Store.find_by(abbrev: abbrev.to_s)
      salepoints = store.salepoints.includes(:album)
      salepoints.each do |salepoint|
        next if salepoint.album.nil?
        next if salepoint.album.send(check) == salepoint.has_rights_assignment
        puts "reverse migrating #{check.inspect} for album #{salepoint.album.id}: #{salepoint.album.name}"
        salepoint.album.update_attribute check, salepoint.has_rights_assignment?
      end
    end
  end
end
