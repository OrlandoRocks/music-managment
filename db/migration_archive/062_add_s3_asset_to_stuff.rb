class AddS3AssetToStuff < ActiveRecord::Migration[4.2]
  def self.up
    add_column :songs, :s3_flac_asset_id, :integer
    add_column :artworks, :s3_asset_id, :integer
  end

  def self.down
    remove_column :songs, :s3_flac_asset_id
    remove_column :artworks, :s3_asset_id
  end
end
