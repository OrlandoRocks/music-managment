class AddFailedLoginAttemptsToPerson < ActiveRecord::Migration[4.2]
    def self.up
      add_column :people, :login_attempts, :integer, :default => 0
      add_column :people, :account_locked_until, :datetime
    end

    def self.down
      remove_column :people, :login_attempts
      remove_column :people, :account_locked_until
    end
end
  
