class MoreSerials < ActiveRecord::Migration[4.2]
  def self.up
    add_column :serials, :program, :string, :limit => 128
    execute("update serials set program = 't_racks' ")

    #allow null values in os field
    change_column :serials, :os, :string, :null => true, :limit => 15

    #reduce person uniqueness constraint to handle multiple types of serials
    remove_index :serials, :name => :uniq_person_serial
    add_index :serials, [:person_id, :program], :unique => true, :name => :uniq_person_prog_serial
  end

  def self.down
    remove_column :serials, :program
    remove_index :serials, :name => :uniq_person_prog_serial
    add_index :serials, [:person_id], :unique => true, :name => :uniq_person_serial
    change_column :serials, :os, :string, :null => false, :limit => 15
  end
end
