class AddActiveFieldToGenres < ActiveRecord::Migration[4.2]
  def self.up
    add_column :genres, :is_active, :boolean, :default => true, :null => false

    Genre.find_by(name: 'Classical').update_attribute(:is_active, false)
    Genre.find_by(name: 'Audiobooks ').update_attribute(:is_active, false)
  end

  def self.down
    remove_column :genres, :is_active
  end
end
