class CreatePaypalIpns < ActiveRecord::Migration[4.2]
  def self.up
    create_table :paypal_ipns do |t|
      t.column :updated_at, :datetime, :null => false, :default => nil
      t.column :person_id, :integer, :null => true, :default => nil # maybe we can't figure it out
      t.column :invoice_id, :integer, :null => true, :default => nil # maybe we can't figure it out
      t.column :payment_status, :string, :null => false, :default => nil, :limit => 30
      t.column :paypal_txn_id, :string, :null => false, :default => nil, :limit => 20
      t.column :paypal_item_number, :string, :null => false, :default => nil, :limit => 100
      t.column :payment_amount_cents, :integer, :null => false, :default => 0
    end
  end

  def self.down
    drop_table :paypal_ipns
  end
end
