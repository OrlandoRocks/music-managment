class AddOptimisticLockingToPromoCodes < ActiveRecord::Migration[4.2]
  def self.up
    add_column :promo_codes, :lock_version, :integer, :null => false, :default => 0
  end

  def self.down
    remove_column :promo_codes, :lock_version
  end
end
