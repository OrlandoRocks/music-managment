class AddPageOutsideUrl < ActiveRecord::Migration[4.2]
  def self.up
    add_column :bandpages, :link_to_url, :string
    remove_column :bandpages, :views
  end

  def self.down
    add_column :bandpages, :views, :integer
    remove_column :bandpages, :link_to_url
  end
end
