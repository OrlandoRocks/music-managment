class AddS3ThumbIdsToArtwork < ActiveRecord::Migration[4.2]
  def self.up
    add_column :artworks, :s3_thumb_100x100_asset_id, :integer
    add_column :artworks, :s3_thumb_400x400_asset_id, :integer
  end

  def self.down
    remove_column :artworks, :s3_thumb_100x100_asset_id
    remove_column :artworks, :s3_thumb_400x400_asset_id
  end
end
