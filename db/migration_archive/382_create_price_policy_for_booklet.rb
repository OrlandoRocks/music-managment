class CreatePricePolicyForBooklet < ActiveRecord::Migration[4.2]
  def self.up
    PricePolicy.create(:short_code => 'booklet', :price_calculator => 'current_booklet', :base_price_cents => 2000, :description => "PDF booklet for deliver to iTunes")
  end

  def self.down
    PricePolicy.find_by(short_code: 'booklet').destroy
  end
end
