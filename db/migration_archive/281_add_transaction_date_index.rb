class AddTransactionDateIndex < ActiveRecord::Migration[4.2]
  def self.up
    add_index :person_transactions, [:created_at], :name => :person_transaction_date
  end

  def self.down
    remove_index :person_transactions, :name => :person_transaction_date
  end
end
