class AddUploadServerStatus < ActiveRecord::Migration[4.2]
  def self.up
    add_column :upload_servers, :status, :string, :default => 'active'
    add_column :upload_servers, :failed_attempts, :integer, :default => 0
  end

  def self.down
    remove_column :upload_servers, :status
    remove_column :upload_servers, :failed_attempts
  end
end
