class RemoveNameFromItunesUserReports < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :itunes_user_reports, :name
  end

  def self.down
    add_column :itunes_user_reports, :name, :string, :limit => 40
  end
end
