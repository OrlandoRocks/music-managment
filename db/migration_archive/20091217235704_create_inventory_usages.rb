class CreateInventoryUsages < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `inventory_usages`(
                `id` int UNSIGNED not null auto_increment,
                `inventory_id` int UNSIGNED not null COMMENT 'references the inventory line-item used',
                `related_type` ENUM('Album','Single','Ringtone','Song','Salepoint','Video','Booklet','ItunesUserReport','ShippingLabel') COMMENT 'the model name of the related inventory used', 
                `related_id` int UNSIGNED not null COMMENT 'the id of the related model',
                `created_at` timestamp null,                
                PRIMARY KEY  (`id`),
                KEY `inventory_id` (`inventory_id`),
                KEY `related_type_related_id` (`related_type`, `related_id`),
                CONSTRAINT `fk_inventory_id` FOREIGN KEY (`inventory_id`) REFERENCES `inventories`(`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
            
    execute up_sql
  end

  def self.down
    drop_table :inventory_usages
  end
end
