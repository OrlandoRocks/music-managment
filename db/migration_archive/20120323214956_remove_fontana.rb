class RemoveFontana < ActiveRecord::Migration[4.2]
  def self.up

    if Partner.find_by(skin: "fontana")

      affected_people = Person.includes( :roles, :partners).where("`partners`.id = ?", Partner.find_by(skin: "fontana").id)
      affected_people.each do |person|
        p = Person.find(person)
        next if p.roles.include?(Role.find(1))
        next if p.partners.length > 1
        person.partners.pop
        person.partners << Partner.find_by(skin: "tunecore_neo")
        person.save
      end

    end

  end

  def self.down
    # this is a one way migration
  end
end
