class CreatePartners < ActiveRecord::Migration[4.2]
  def self.up
    execute "DROP TABLE IF EXISTS `partners_people`;"
    execute "DROP TABLE IF EXISTS `partners_stores`;"
    execute "DROP TABLE IF EXISTS `cert_batches_partners`;"
    execute "DROP TABLE IF EXISTS `disabled_features_partners`;"
    
    execute "DROP TABLE IF EXISTS `partners`;"
    
    execute" 
      CREATE TABLE `partners` (
        `id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
        `name` VARCHAR(50) NOT NULL COMMENT 'Partner Name',
        `domain` VARCHAR(255) NOT NULL COMMENT 'Domain for the partner',
        `skin` VARCHAR(50) NOT NULL COMMENT 'Skin Identifier for the partner.',
        PRIMARY KEY  (`id`)
      ) ENGINE=INNODB DEFAULT CHARSET=utf8;
    "
  end

  def self.down
    execute "DROP TABLE IF EXISTS `partners`;"
  end
end
