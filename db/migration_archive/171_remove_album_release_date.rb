class RemoveAlbumReleaseDate < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :albums, :release_date
  end

  def self.down
    add_column :albums, :release_date, :date
  end
end
