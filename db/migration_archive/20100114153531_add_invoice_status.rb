class AddInvoiceStatus < ActiveRecord::Migration[4.2]
  def self.up
    execute("
      ALTER TABLE invoices
      ADD COLUMN batch_status ENUM('visible_to_customer','waiting_for_batch','processing_in_batch') DEFAULT 'visible_to_customer' NULL after final_settlement_amount_cents,
      ADD KEY `batch_status` (`batch_status`);
    ")
  end

  def self.down
    execute("
      ALTER TABLE invoices
      DROP KEY `batch_status`,
      DROP COLUMN batch_status;
    ")    
  end
end
