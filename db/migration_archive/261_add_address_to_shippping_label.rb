class AddAddressToShipppingLabel < ActiveRecord::Migration[4.2]
  def self.up
    add_column :shipping_labels, :address1, :string
    add_column :shipping_labels, :address2, :string
    add_column :shipping_labels, :city, :string
    add_column :shipping_labels, :state, :string
    add_column :shipping_labels, :zip, :string
    add_column :shipping_labels, :tracking_number, :string
    add_column :shipping_labels, :fedex_cost_cents, :integer
  end

  def self.down
    remove_column :shipping_labels, :address1
    remove_column :shipping_labels, :address2
    remove_column :shipping_labels, :city
    remove_column :shipping_labels, :state
    remove_column :shipping_labels, :zip
    remove_column :shipping_labels, :tracking_number
    remove_column :shipping_labels, :fedex_cost_cents
  end
end
