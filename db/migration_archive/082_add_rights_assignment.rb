class AddRightsAssignment < ActiveRecord::Migration[4.2]
  def self.up
    add_column :stores, :needs_rights_assignment, :boolean, :null => false, :default => false
    add_column :salepoints, :has_rights_assignment, :boolean, :null => false, :default => false
  end

  def self.down
    remove_column :stores, :needs_rights_assignment
    remove_column :salepoints, :has_rights_assignment
  end
end
