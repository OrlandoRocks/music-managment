class AddAlbumPriceOverride < ActiveRecord::Migration[4.2]
  def self.up
    add_column :albums, :price_override, :float
  end

  def self.down
    remove :albums, :price_override
  end
end
