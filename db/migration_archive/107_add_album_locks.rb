class AddAlbumLocks < ActiveRecord::Migration[4.2]
  def self.up
    add_column :albums, :invoice_id, :integer, :null => true, :default => nil
    add_column :albums, :locked_by_id, :integer, :null => true, :default => nil
  end

  def self.down
    remove_column :albums, :invoice_id
    remove_column :albums, :locked_by_id
  end
end
