class CreateIndexAlbumFinalizedAt < ActiveRecord::Migration[4.2]
  def self.up
    add_index :albums, :finalized_at
  end

  def self.down
    remove_index :albums, :finalized_at
  end
end
