class AddsMusicnetCheck < ActiveRecord::Migration[4.2]
  def self.up
       add_column :albums, :musicnet_check,  :boolean, :default => false
    end

    def self.down
       remove_column :albums, :musicnet_check
  end
end
