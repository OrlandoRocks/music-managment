class CreateGenresVideos < ActiveRecord::Migration[4.2]
  def self.up
      create_table :genres_videos, :id => false do |t|
        t.column :genre_id, :integer
        t.column :video_id, :integer
      end
    
  end

  def self.down
    drop_table :genres_videos
  end
end
