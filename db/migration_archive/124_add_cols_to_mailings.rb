class AddColsToMailings < ActiveRecord::Migration[4.2]
  def self.up
    add_column :mailings, :created_by, :date
    add_column :mailings, :updated_by, :date
  end

  def self.down
    remove_column :mailings, :created_by
    remove_column :mailings, :updated_by
  end
end
