class AddAlbumKnownLiveOn < ActiveRecord::Migration[4.2]
  def self.up
    add_column :albums, :known_live_on, :date
  end

  def self.down
    remove_column :albums, :known_live_on
  end
end
