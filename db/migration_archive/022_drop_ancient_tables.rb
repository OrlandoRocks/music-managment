class DropAncientTables < ActiveRecord::Migration[4.2]
  def self.up
    drop_table "albums_states"
    drop_table "artists_users"
    drop_table "orders_states"
    drop_table "songs_states"
    drop_table "uploads_files"
    drop_table "users"
  end

  def self.down
  end
end
