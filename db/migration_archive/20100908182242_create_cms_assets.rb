class CreateCmsAssets < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `cms_assets` (
      `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
      `asset_name` varchar(100) NOT NULL COMMENT 'name of asset',
      `asset_category` ENUM('Ad', 'Dashboard', 'General') DEFAULT 'Dashboard',
      `file_name` varchar(100) NULL COMMENT 'file name',
      `file_path` varchar(200) NULL COMMENT 'full path and file name of asset',
      `file_type` varchar(20) NOT NULL COMMENT 'file type',
      `file_size` INT(20) NOT NULL COMMENT 'file size',
      `created_by_id` INT(10) NOT NULL COMMENT 'id of administrator who created the asset',
      `updated_at` TIMESTAMP NULL COMMENT 'Datetime of latest update',
      `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',
      PRIMARY KEY  (`id`),
      KEY `ix_cms_asset_name` (`asset_name`)
    ) ENGINE=INNODB DEFAULT CHARSET=utf8;)
                  
    execute up_sql
  end

  def self.down
    drop_table :cms_assets
  end
end
