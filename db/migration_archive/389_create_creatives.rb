class CreateCreatives < ActiveRecord::Migration[4.2]
  def self.up
    create_table :creatives do |t|
      t.column :creativeable_id, :integer, :null => false
      t.column :creativeable_type, :string, :null => false
      t.column :artist_id, :integer, :null => false
      t.column :role, :string, :limit => 20, :null => false, :default => 'performer'
    end
  end

  def self.down
    drop_table :creatives
  end
end
