class ChangeItunesUserTrends < ActiveRecord::Migration[4.2]
  def self.up
	  drop_table :itunes_user_trends
	  create_table :itunes_user_trends do |t|  
      t.column :upc, :string, :limit => 15
      t.column :isrc, :string, :limit => 20
      t.column :artist, :string
      t.column :title, :string
      t.column :label_name, :string
      t.column :units, :string, :limit => 5
      t.column :zipcode, :string, :limit => 12
      t.column :report_date, :date
      t.column :apple_id, :string, :limit => 19
      t.column :created_at, :datetime
    end
  end

  def self.down
    drop_table :itunes_user_trends
    
    # THIS IS JUST SILLY, BUT IT STOP THE MIGRATIONS FROM BREAKING
    create_table :itunes_user_trends do |t|
      t.column :dumb, :string  
    end
  end
end


#
# CREATE TABLE FOR INGESTING ITUNES USER TRENDING REPORTS
#

#
# SQL COMMAND TO LOAD DATA MANUALLY 
# 
# LOAD DATA LOCAL INFILE '/tmp/itunes_user_trends.txt' INTO TABLE itunes_user_trends FIELDS TERMINATED BY '\t' 
# IGNORE 1 LINES  
# (@dummy, @dummy, @dummy, upc, isrc, artist, title, label_name, @dummy,  units, @dummy, @dummy, @dummy, zipcode, @dummy, @report_date, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, apple_id, @created_at) 
# SET report_date = STR_TO_DATE(@report_date,'%m/%d/%Y'), created_at = CURRENT_TIMESTAMP;
