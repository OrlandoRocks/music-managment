class MakeNapsterVariablePrice < ActiveRecord::Migration[4.2]
  def self.up
    store = Store.find_by(abbrev: 'np')
    store.update_attribute(:variable_pricing, true)
    VariablePrice.create(:store_id => store.id, :price_code => '$.99', :price_code_display => '$.99', :active => true, :position => 1)

  end

  def self.down
    store = Store.find_by(abbrev: 'np')
    VariablePrice.where("store_id = #{store.id}").destroy_all
    store.update_attribute(:variable_pricing, false)
  end
end
