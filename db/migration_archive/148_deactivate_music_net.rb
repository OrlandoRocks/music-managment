class DeactivateMusicNet < ActiveRecord::Migration[4.2]
  def self.up
		Store.find_by(name: 'MusicNet').update_attribute(:is_active, false)
  end

  def self.down
		Store.find_by(name: 'MusicNet').update_attribute(:is_active, true)
  end
end
