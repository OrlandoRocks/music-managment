class ChangeManufacturingOrderNames < ActiveRecord::Migration[4.2]
  def self.up
    rename_column :manufacturing_orders, :o_name, :customer_name
    rename_column :manufacturing_orders, :o_address1, :address
    rename_column :manufacturing_orders, :o_address2, :address2
    rename_column :manufacturing_orders, :o_city, :city
    rename_column :manufacturing_orders, :o_state, :state
    rename_column :manufacturing_orders, :o_zip, :zip
    rename_column :manufacturing_orders, :o_phone, :phone
    rename_column :manufacturing_orders, :o_email, :email
    rename_column :manufacturing_orders, :s_name, :shipping_name
    rename_column :manufacturing_orders, :s_address1, :shipping_address
    rename_column :manufacturing_orders, :s_address2, :shipping_address2
    rename_column :manufacturing_orders, :s_city, :shipping_city
    rename_column :manufacturing_orders, :s_state, :shipping_state
    rename_column :manufacturing_orders, :s_zip, :shipping_zip

    rename_column :manufacturing_orders, :pi_album, :album
    rename_column :manufacturing_orders, :pi_artist, :artist
    rename_column :manufacturing_orders, :pi_upc, :upc
    rename_column :manufacturing_orders, :pd_run, :run
    rename_column :manufacturing_orders, :pd_packaging, :packaging
  end

  def self.down
    rename_column :manufacturing_orders, :customer_name, :o_name
    rename_column :manufacturing_orders, :address, :o_address1
    rename_column :manufacturing_orders, :address2, :o_address2
    rename_column :manufacturing_orders, :city, :o_city
    rename_column :manufacturing_orders, :state, :o_state
    rename_column :manufacturing_orders, :zip, :o_zip
    rename_column :manufacturing_orders, :phone, :o_phone
    rename_column :manufacturing_orders, :email, :o_email
    rename_column :manufacturing_orders, :shipping_name, :s_name
    rename_column :manufacturing_orders, :shipping_address, :s_address1
    rename_column :manufacturing_orders, :shipping_address2, :s_address2
    rename_column :manufacturing_orders, :shipping_city, :s_city
    rename_column :manufacturing_orders, :shipping_state, :s_state
    rename_column :manufacturing_orders, :shipping_zip, :s_zip
    
    
    rename_column :manufacturing_orders, :album, :pi_album
    rename_column :manufacturing_orders, :artist, :pi_artist
    rename_column :manufacturing_orders, :upc, :pi_upc
    rename_column :manufacturing_orders, :run, :pd_run
    rename_column :manufacturing_orders, :packaging, :pd_packaging
  end
end
