class MakeEmailAUniqueConstraint < ActiveRecord::Migration[4.2]
  def self.up
    deleted_by_dups = Person.find_by_sql("select people.id, count(name) as x from people group by email having x > 1")
    
    # the original fix up would have created a few deleted accounts where more than one email address would have been set to deleted_by_admin_process_#{original_email}
    
    # this creates unique deleted emails
    count = 0
    deleted_by_dups.each do |x|
      x.reload
      x.email = "#{count.to_s}_#{x.email}"
      puts "changing email: #{x.email}"
      x.save
      count += 1
    end
    
    #if there are no duplicates, dump the old index and make this baby unique! else it blows up!
    if Person.find_by_sql("select people.id, count(name) as x from people group by email having x > 1").size == 0
      puts "removing index and building new unique index"
      remove_index :people, :email
      add_index :people, :email, :unique => true
    else
      raise "the new index was not built"
    end
      
  end

  def self.down
    remove_index :people, :email
    add_index :people, :email
  end
end
