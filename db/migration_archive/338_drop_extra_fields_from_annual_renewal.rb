class DropExtraFieldsFromAnnualRenewal < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :annual_renewals, :locked_by_id 
    remove_column :annual_renewals, :locked_by_type
    remove_column :annual_renewals, :subscription_renewal_due_event_id
    remove_column :annual_renewals, :person_id
  end

  def self.down
    add_column :annual_renewals, :locked_by_id, :integer
    add_column :annual_renewals, :locked_by_type, :string, :limit => 30, :default => nil
    add_column :annual_renewals, :subscription_renewal_due_event_id, :integer
    add_column :annual_renewals, :person_id, :integer
  end
end
