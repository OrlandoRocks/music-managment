class AddAmiestreetStore < ActiveRecord::Migration[4.2]
  def self.up
    Store.new(:name => "Amie Street", :position => 17, :is_active => false, :base_price_policy_id => 3, :short_name => "Amiestreet", :abbrev => "as").save
  end

  def self.down
    Store.find_by(short_name: "Amiestreet").destroy
  end
end
