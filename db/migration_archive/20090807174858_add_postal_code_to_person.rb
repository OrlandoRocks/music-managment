class AddPostalCodeToPerson < ActiveRecord::Migration[4.2]
  def self.up
    add_column :people, :postal_code, :string, :limit => 32
  end

  def self.down
    remove_column :people, :postal_code
  end
end
