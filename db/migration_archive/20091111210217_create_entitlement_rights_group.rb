class CreateEntitlementRightsGroup < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `entitlement_rights_groups`(
                `id` int UNSIGNED not null auto_increment,
                `name` varchar(40) NOT NULL COMMENT 'name of grouped rights',
                `description` varchar(255) NOT NULL COMMENT 'description of access rights for offered product',
                `created_at` timestamp null,                
                PRIMARY KEY  (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
                
    execute up_sql
  end

  def self.down
    drop_table :entitlement_rights_groups
  end
end
