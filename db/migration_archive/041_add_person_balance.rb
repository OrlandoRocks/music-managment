class AddPersonBalance < ActiveRecord::Migration[4.2]
  def self.up
    create_table :person_balances do |t|
      t.column :person_id, :integer, :null => false
      t.column :balance_cents, :integer, :null => false
      t.column :updated_at, :datetime, :null => false
    end
    execute("insert into person_balances (person_id, balance_cents, updated_at) select id as person_id, 0, NOW() from people")
  end

  def self.down
    drop_table :person_balances
  end
end
