class AddThirdAddress < ActiveRecord::Migration[4.2]
  def self.up
        add_column :manufacturing_orders, :address3, :string
  end

  def self.down
        add_column :manufacturing_orders, :address3
  end
end
