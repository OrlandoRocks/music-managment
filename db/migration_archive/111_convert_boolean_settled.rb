class ConvertBooleanSettled < ActiveRecord::Migration[4.2]
  def self.up
    Invoice.transaction do
      Invoice.where(:settled => true).each do |invoice|
        execute "update invoices set settled_at = (select max(created_at) from invoice_settlements where invoice_id = #{invoice.id}) where invoices.id = #{invoice.id}"
      end
    end
  end

  def self.down
    execute "update invoices set settled = 1 where settled_at is not null"
  end
end
