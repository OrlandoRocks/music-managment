class UpdateIdsToUnsignedInts < ActiveRecord::Migration[4.2]
  def self.up
    execute "alter table artists modify id int UNSIGNED NOT NULL AUTO_INCREMENT;"
    execute "alter table us_zip_codes modify id mediumint UNSIGNED NOT NULL AUTO_INCREMENT;"
    execute "alter table creatives modify id int UNSIGNED NOT NULL AUTO_INCREMENT, modify creativeable_id int UNSIGNED NOT NULL, modify artist_id int UNSIGNED NOT NULL;"
    execute "alter table labels modify id int UNSIGNED NOT NULL AUTO_INCREMENT;"
  end

  def self.down
  end
end
