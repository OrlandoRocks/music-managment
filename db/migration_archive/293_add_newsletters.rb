class AddNewsletters < ActiveRecord::Migration[4.2]
  def self.up
    add_column :mailings, :is_newsletter, :boolean, :null => false, :default => false
    create_table :newsletter_subscribers do |t|
      t.column :address, :string, :null => false, :limit => 128
      t.column :created_on, :datetime
    end
    add_index :newsletter_subscribers, [:address]
  end

  def self.down
    remove_column :mailings, :is_newsletter
    drop_table :newsletter_subscribers
  end
end
