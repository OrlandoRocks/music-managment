class AddStoreIntakeUniqueness < ActiveRecord::Migration[4.2]
  def self.up
    add_column :store_intakes, :unique_id, :string, :limit => 30, :null => true, :default => nil
    StoreIntake.all.each do |si|
      si.update_attribute :unique_id, "legacy:#{si.id}"
    end
    add_index :store_intakes, :unique_id, :unique => true
  end

  def self.down
    remove_column :store_intakes, :unique_id
  end
end
