class VariablePricesTables < ActiveRecord::Migration[4.2]
  def self.up

    #
    #  Variable Prices Stores Many-to-Many
    #
    say_with_time("Creating Tables For Variable Prices Many-To-Many to Stores") do

      create_table :variable_prices_stores do |t|
        t.integer :store_id, :null => false
        t.integer :variable_price_id, :null => false
        t.integer :position
        t.boolean :is_active
      end

      add_index :variable_prices_stores, [:store_id, :variable_price_id], :unique => true

    end


    #
    # Creating Store Groups
    #
    say_with_time("Create Store Groups") do

      create_table :store_groups do |t| 
        t.string :name, :null => false
        t.string :key, :null => false
      end

      create_table :store_group_stores do |t|
        t.integer :store_group_id, :null => false
        t.integer :store_id, :null => false
      end

      add_index :store_group_stores, [:store_group_id, :store_id], :unique => true
      add_index :store_groups, :key
    end

    #
    # Creating Defaults Table
    #
    say_with_time("Creating Default Variable Price Table For Stores") do

      create_table :default_variable_prices do |t|
        t.integer :variable_price_store_id, :null => false
        t.integer :store_id, :null => false
      end

      add_index :default_variable_prices, :store_id, :unique => true
      add_index :default_variable_prices, :variable_price_store_id, :unique => true

      execute("
        ALTER TABLE default_variable_prices
        ADD FOREIGN KEY (variable_price_store_id)
        REFERENCES variable_prices_stores(id);
      ")

    end

  end

  def self.down
    drop_table :default_variable_prices

    drop_table :store_group_stores
    drop_table :store_groups
    drop_table :variable_prices_stores
  end
end
