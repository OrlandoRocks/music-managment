class FixExchangeRateNull < ActiveRecord::Migration[4.2]
  def self.up
    execute('ALTER TABLE store_intakes MODIFY exchange_rate_fixed bigint(19) DEFAULT NULL')
    execute('UPDATE store_intakes SET exchange_rate_fixed = null WHERE exchange_rate_fixed = 0')
    remove_column :store_intakes, :exchange_rate_stupid
  end

  def self.down
    execute('ALTER TABLE store_intakes ADD exchange_rate_stupid long DEFAULT NULL AFTER exchange_rate_fixed')
    execute('UPDATE store_intakes SET exchange_rate_stupid = exchange_rate_fixed')
  end
end
