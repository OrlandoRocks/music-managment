class AddAlbumsGenresIndex < ActiveRecord::Migration[4.2]
  def self.up
    add_index( :albums_genres, [:album_id, :genre_id], :name => :albums_genres_index )
  end

  def self.down
    remove_index( :albums_genres, :name => :albums_genres_index )
  end
end
