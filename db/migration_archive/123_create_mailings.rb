class CreateMailings < ActiveRecord::Migration[4.2]
  def self.up
    create_table :mailings do |t|
      t.column :subject, :string
      t.column :body, :string
      t.column :from, :string
      t.column :created_at, :datetime
      t.column :updated_at, :datetime
    end
  end

  def self.down
    drop_table :mailings
  end
end
