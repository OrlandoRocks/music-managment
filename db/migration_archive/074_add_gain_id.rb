class AddGainId < ActiveRecord::Migration[4.2]
  def self.up
    add_column :people, :gain_employee_id, :string, :limit => 12, :default => nil, :null => true
  end

  def self.down
    remove_column :people, :gain_employee_id
  end
end
