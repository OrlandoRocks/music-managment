class TurnOffAmazonStoreForPetriLaunch < ActiveRecord::Migration[4.2]
  def self.up
    amazon = Store.find_by(name: "Amazon")
    amazon.is_active = false
    amazon.save
  end

  def self.down
    amazon = Store.find_by(name: "Amazon")
    amazon.is_active = true
    amazon.save
  end
end
