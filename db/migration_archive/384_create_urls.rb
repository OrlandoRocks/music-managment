class CreateUrls < ActiveRecord::Migration[4.2]
  def self.up
    create_table :urls do |t|
      t.column :store_id, :integer
      t.column :album_id, :integer
      t.column :link, :string
      t.column :created_on, :datetime 
    end
  end

  def self.down
    drop_table :urls
  end
end
