class CreateCatalogRole < ActiveRecord::Migration[4.2]
  def self.up
    Role.create(:name => 'catalog_access')
  end

  def self.down
    role = Role.find_by(name: "catalog_access")
    role.destroy
  end
end
