class AddBasePolicyIdToStores < ActiveRecord::Migration[4.2]
  def self.up
     add_column :stores, :base_price_policy_id, :integer, :size=>2, :default => PricePolicy.find_by(short_code: "store")
     store=Store.find_by(short_name: "Lala")
     store.base_price_policy_id = 10
     store.save!
  end

  def self.down
    remove_column :stores, :base_price_policy_id
  end
end
