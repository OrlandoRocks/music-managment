class AddTransfersTables < ActiveRecord::Migration[4.2]
  def self.up
    create_table :paypal_transfers do |t|
      t.column :created_at, :datetime, :null => false
      t.column :updated_at, :datetime, :null => false
      t.column :transfer_status, :string, :limit => 20, :null => false
      t.column :person_id, :integer, :null => false
      t.column :payment_cents, :integer, :null => false
      t.column :admin_charge_cents, :integer, :null => false
      t.column :paypal_address, :string, :limit => 80, :null => false
    end

    create_table :check_transfers do |t|
      t.column :created_at, :datetime, :null => false
      t.column :updated_at, :datetime, :null => false
      t.column :transfer_status, :string, :limit => 20, :null => false
      t.column :person_id, :integer, :null => false
      t.column :payment_cents, :integer, :null => false
      t.column :admin_charge_cents, :integer, :null => false
      t.column :payee, :text, :null => false
      t.column :address1, :text, :null => false
      t.column :address2, :text
      t.column :city, :text, :null => false
      t.column :state, :text, :null => false
      t.column :postal_code, :string, :limit => 10, :null => false
      t.column :country, :string, :limit => 56, :null => false #longest official country name...
    end
  end

  def self.down
    drop_table :paypal_transfers
    drop_table :check_transfers
  end
end
