class AddLalaStore < ActiveRecord::Migration[4.2]
  def self.up
    Store.create( :name => "Lala",
                  :short_name => "Lala",
                  :abbrev => 'll',
                  :position => 15,
                  :needs_rights_assignment => false,
                  :is_active => false,
                  :variable_pricing => false)
  end

  def self.down
    if s = Store.find_by(name: "Lala")
      s.destroy
    end
  end

end
