class AddFirstTwoUploadServerInstances < ActiveRecord::Migration[4.2]
  def self.up
		UploadServer.create(:url => 'domU-12-31-37-00-07-6F.usma3.compute.amazonaws.com', :status => 'active')
		UploadServer.create(:url => 'domU-12-31-37-00-06-04.usma3.compute.amazonaws.com', :status => 'active')
		UploadServer.create(:url => 'domU-12-31-37-00-06-03.usma3.compute.amazonaws.com', :status => 'active')
  end

  def self.down
  end
end
