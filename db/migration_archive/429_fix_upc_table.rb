class FixUpcTable < ActiveRecord::Migration[4.2]
  def self.up
    change_column :upcs, :tunecore_upc, :boolean, :default => false, :null => :false
    change_column :upcs, :inactive, :boolean, :null => :false, :default => false
    execute "UPDATE upcs SET tunecore_upc = false WHERE tunecore_upc IS NULL"
  end

  def self.down
  end
end
