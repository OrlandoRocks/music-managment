class ModificationsForVideoModel < ActiveRecord::Migration[4.2]
  def self.up
    add_column :videos, :parental_advisory, :boolean, :default => false
    change_column :videos, :length, :integer
  end

  def self.down
    remove_column :videos, :parental_advisory
    change_column :videos, :length, :string
  end
end
