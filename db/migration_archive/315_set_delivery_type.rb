class SetDeliveryType < ActiveRecord::Migration[4.2]
  def self.up
    # all albums up to this point have been delivered in full; there is no concept of a metadata_only delivery until now
    execute("UPDATE distributions set delivery_type = 'full_delivery'")
  end

  def self.down
  end
end
