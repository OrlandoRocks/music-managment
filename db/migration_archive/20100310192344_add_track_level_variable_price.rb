class AddTrackLevelVariablePrice < ActiveRecord::Migration[4.2]

  def self.up
    add_column :salepoints, :track_variable_price_id, :integer
  end

  def self.down
    remove_column :salepoints, :track_variable_price_id
  end

end
