class CreateBundles < ActiveRecord::Migration[4.2]
  def self.up
    create_table :bundles do |t|
      t.column :album_id, :integer
      t.column :params, :text
      t.column :created_at, :datetime
      t.column :updated_at, :datetime
      t.column :completed_at, :datetime
      t.column :canceled_at, :datetime
      t.column :in_progress_at, :datetime
      t.column :failed, :boolean
      t.column :comment, :text
    end
  end

  def self.down
    drop_table :bundles
  end
end
