class AddMoreIndexes < ActiveRecord::Migration[4.2]
  def self.up
    add_index :salepoints, :store_id
    add_index :salepoints, :album_id
  end

  def self.down
    remove_index :salepoints, :store_id
    remove_index :salepoints, :album_id
  end
end
