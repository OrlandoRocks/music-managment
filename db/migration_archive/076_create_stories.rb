class CreateStories < ActiveRecord::Migration[4.2]
  def self.up
    create_table :stories do |t|
      t.column :title, :string
      t.column :short_title, :string
      t.column :body, :text
      t.column :posted, :datetime
      t.column :online, :boolean
    end
  end

  def self.down
    drop_table :stories
  end
end
