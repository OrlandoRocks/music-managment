class DropAndRecreateMessagesTable < ActiveRecord::Migration[4.2]
  def self.up
    execute("DROP TABLE messages;")
    
    execute("
      CREATE TABLE `messages` (
        `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
        `person_id` INT(10) UNSIGNED NOT NULL COMMENT 'Foreign Key to people',
        `code` ENUM('notice','alert','promotional','money') NOT NULL DEFAULT 'notice' COMMENT 'Indicates type of message.',
        `text` VARCHAR(255) NOT NULL COMMENT 'HTML message',
        `viewed_at` DATETIME DEFAULT NULL COMMENT 'Date and Time when message was viewed',
        `expire_at` DATETIME DEFAULT NULL COMMENT 'Date and Time when message is set to expire',
        `created_at` DATETIME DEFAULT NULL COMMENT 'Date and Tume when message was created.',
        PRIMARY KEY  (`id`),
        KEY `person_id` (`person_id`)
      ) ENGINE=INNODB DEFAULT CHARSET=utf8 ;
    ")
  end

  def self.down
    #no downgrading of this migration.
  end
end
