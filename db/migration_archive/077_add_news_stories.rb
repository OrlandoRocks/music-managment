class AddNewsStories < ActiveRecord::Migration[4.2]

  def self.up
    if Object.const_defined?("Story")
      Story.create(:title=>'Downloadable Accounting and Online Payments Now Available', :short_title => 'Downloadable Accounting and Online Payments Now Available',
                  :body => '<p>You can now download your own accounting and use TuneCore to send payments directly out of your My Account page.</p>

                  <p>Customers with data in their My Accounts pages can now click the "Download Accounting" and get a CSV (comma separated value) readable with any spreadsheet program. There is no limit to how many times you can download your accounting data. TuneCore offers this free of charge as a service to all customers.</p>

                  <p>Your downloaded accounting will show all sales activity from any store, what songs and albums sold, when, how many, what country they sold in and more. In addition you can sort your sales information by artist, album, label, store, number of songs sold and more.</p>

                  <p>Order and arrange the data any way you want to calculate mechanicals, artist and/or producer royalties, band member splits or anything else. If you\'d like, you can use TuneCore to send your payments from your MyAccounts page--one easy, secure source.</p>

                  <p>For more information, please <a href="http://help.tunecore.com/app/answers/list/c/6">click here</a>.</p>', :online => true, :posted => Time.now)



      Story.create(:title=>'Microsoft Zune Now Included When You Choose MusicNet', :short_title => 'Microsoft Zune Now Included When You Choose MusicNet',
                              :body => '<p>TuneCore is proud to announce that our partner store MusicNet now delivers music to the Microsoft <a href="http://www.zune.net/">Zune</a> store. If you select MusicNet as a digital service for TuneCore to deliver your music to, then your music will appear in the Zune store as well as all of the other MusicNet stores such as: Yahoo!, Cdigix (on more than 30 college campuses), Synacor, iMesh, Virgin Digital, HMV Digital and FYE.
                              </p>
                              <p>
                              SPECIAL NOTE: If you\'re already chosen to have your music in the MusicNet stores, all your albums will be added to Zune automatically!
                              </p><p>
                              Microsoft\'s new music service media player, Zune (the name for the both the music store and for the media player that plays songs bought at the Zune store), works just like iTunes: download the Zune software to shop in the Zune music store.
                              </p><p>
                              The following is from Miscrosoft\'s press release about Zune:
                              </p><p>
                              The Zune Experience
                              </p><p>
                              Zune includes a 30GB digital media player, the Zune Marketplace music service and a foundation for an online community that will enable music fans to discover new music. The Zune device features wireless technology,
                              a built-in FM tuner and a bright, 3-inch screen that allows users to not only show off music, pictures and video, but also to customize the experience with personal pictures or themes to truly make the device their own.
                              Zune comes in three colors: black, brown and white.
                              </p><p>
                              Every Zune device creates an opportunity for connection. Wireless Zune-to-Zune sharing lets consumers spontaneously share full-length sample tracks of select songs, homemade recordings, playlists or
                              pictures with friends between Zune devices. Listen to the full track of any song you receive up to three times over three days. If you like a song you hear and want to buy it, you can flag it right on your device and easily purchase it from the Zune Marketplace.
                              </p><p>
                              Zune makes it easy to find music you love — whether it\'s songs in your existing library or new music from the Zune Marketplace. Easily import your existing music, pictures and videos
                              in many popular formats and browse millions of songs on Zune Marketplace, where you can choose to purchase tracks individually or to buy a Zune Pass subscription to download as many songs as you want for a flat fee.
                              </p>', :online => true, :posted => Time.now)

             Story.create(:title=>'Listen to the TuneCore podcast - an interview with Izzy Stradlin from Guns N Roses', :short_title => 'Listen to the TuneCore podcast - an interview with Izzy Stradlin from Guns N Roses',
                                                      :body => '<p>
                                                      <iframe src="http://www.hipcast.com/playweb?audioid=P8eab652546f388f6b2fb5901f4167614YV57Q1REYmN0&amp;buffer=5&amp;fc=FFFFFF&amp;pc=CCFF33&amp;kc=FFCC33&amp;bc=FFFFFF&amp;player=ap21" height="20" width="420" frameborder="0" scrolling="no"></iframe>
                                                      </p>
                                                      <p><a href="http://tunecore.hipcast.com/rss/a_description_of_the_music_industry_and_the_way_it_should_be.xml" target="_blank"><img src="http://www.hipcast.com/images/icons/generic/xml.gif" width="36" height="14" hspace="5" border="0" align="middle" alt="View RSS XML"/></a>
                                                      </p>

                                                      <p>The founder of spinART Records and TuneCore talks about: What the music industry is, how - despite technological changes - artists
                                                        are still getting gouged and finally, the way things should work - plus Birdmonster.
                                                        <p>
                                                        <iframe height="20" width="420" frameborder="0" scrolling="no" src="http://www.audioblog.com/playweb?audioid=Paf8abcdf3c2e6f8ca9430372a231967dYV57Q1REYmN1&amp;buffer=5&amp;shape=6&amp;fc=FFFFFF&amp;pc=CCFF33&amp;kc=FFCC33&amp;bc=FFFFFF&amp;brand=1&amp;player=ap21"></iframe><br />
                                                        <br/><br />
                                                        <a href="http://www.audioblog.com/export/Paf8abcdf3c2e6f8ca9430372a231967dYV57Q1REYmN1.mp3" rel="enclosure">MP3 File</a></p>

                                                        <a href="http://tunecore.audioblog.com/rss/a_description_of_the_music_industry_and_the_way_it_should_be.xml" target="_blank">
                                                          <img src="http://www.audioblog.com/images/icons/generic/xml.gif" width="36" height="14" hspace="5" border="0" align="middle" alt="View RSS XML" /></a>
                                                      </p>', :online => true, :posted => Time.now)

            Story.create(:title=>'EQ Magazine Spreads the Signal About TuneCore', :short_title => 'EQ Magazine Spreads the Signal About TuneCore',
                                                      :body => '
                                                                  <p>Here\'s a wonderful article in this month\'s EQ Magazine that has a lot to say about TuneCore, all of it crystal clear:<p>

                                                                  <p><a href="http://www.eqmag.com/story.asp?sectioncode=36&storycode=15646">Click here to read the EQ Magazine article about TuneCore</a></p>', :online => true, :posted => Time.now)




           Story.create(:title=>'OVER $730,000 Earned by TuneCore Customers Since May 2006', :short_title => 'OVER $730,000 Earned by TuneCore Customers Since May 2006',
                                                       :body => '<p>To date, TuneCore artists and labels have earned over $730,000 in music sales from iTunes,
                                                                                Rhapsody, eMusic, MusicNet, Napster and Sony Connect. During the months of March through December, thousands of TuneCore albums went live, earning
                                                                                tens of thousands of dollars for our customers.  As always, TuneCore takes no back end cut
                                                                                from these sales and all the money goes to the customer!  And it\'s only going to get better
                                                                                 as more digital stores are added
                                                                                earning TuneCore customers even more money.  As always, the information is available to
                                                                                 see and all the money is available to take as soon as the information is received from
                                                                                the stores and services - they usually send it 45 days after the end of the each month.
                                                                                If you have any questions about this don\'t hesitate to email <a href="mailto:support@tunecore.com">support@tunecore.com</a>.
                                                                              </p>', :online => true, :posted => Time.now)

            Story.create(:title=>'$10 back for every seven albums paid!', :short_title => '$10 back for every seven albums paid!',
                                                        :body => '<p>TuneCore is happy to announce volume discounts. For every <b>seven albums</b> you pay to have delivered, TuneCore will give you back $10.00 (U.S.).</p>

                                                        <p>Yet another thank-you from TuneCore.</p>', :online => true, :posted => '2007-01-01 12:00:00')
    end
  end

  def self.down
  end
end
