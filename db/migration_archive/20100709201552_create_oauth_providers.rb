class CreateOauthProviders < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q( CREATE TABLE `oauth_providers` (
                  `id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
                  `name` VARCHAR(50) NOT NULL COMMENT 'Provider Name',
                  PRIMARY KEY (`id`)
                  ) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='OAuth Providers.';)
    execute up_sql
    OauthProvider.create(:name => "soundcloud")
  end

  def self.down
    drop_table :oauth_providers
  end
end
