class AddIndividualIndexesToPromoCodes < ActiveRecord::Migration[4.2]
  def self.up
    add_index :promo_codes, :promo
    #add_index :promo_codes, :used
  end

  def self.down
    remove_index :promo_codes, :promo
    remove_index :promo_codes, :used
  end
end
