class AddPersonIdItunesUserTrends < ActiveRecord::Migration[4.2]
  def self.up
    add_column :itunes_user_trends, :apple_customer_id, :integer
    add_column :trend_report_sales, :apple_identifier, :integer
    remove_column :itunes_user_trends, :person
    add_column :itunes_user_trends, :person_id, :integer
  end

  def self.down
    remove_column :itunes_user_trends, :apple_customer_id
    add_column :itunes_user_trends, :person, :integer
    remove_column :itunes_user_trends, :person_id
  end

end
