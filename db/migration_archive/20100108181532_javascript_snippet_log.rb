class JavascriptSnippetLog < ActiveRecord::Migration[4.2]
  def self.up
    create_table :javascript_snippet_logs do |t|
      t.integer :javascript_snippet_id, :null => :false
      t.integer :person_id, :null => :false
      t.string :action
      t.text :change
      t.datetime :created_at
    end
  end

  def self.down
    drop_table :javascript_snippet_logs
  end
end
