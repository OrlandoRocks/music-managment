class RenameAndAddColumnsOnTheNotesTable < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE notes 
       ADD COLUMN `related_type` enum('Person','Album') NULL COMMENT 'Type of entity.' after `related_id`,
       CHANGE `id` `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', 
       CHANGE `person_id` `related_id` int(10) UNSIGNED NOT NULL COMMENT 'Foreign Key of the entity the note is about', 
       CHANGE `note_created_by_id` `note_created_by_id` int(10) UNSIGNED NOT NULL COMMENT 'Foreign Key references people table for the user that created the note', 
       CHANGE `subject` `subject` varchar(50) character set utf8 collate utf8_general_ci NOT NULL COMMENT 'Subject', 
       CHANGE `note` `note` text character set utf8 collate utf8_general_ci NOT NULL COMMENT 'Note', 
       CHANGE `created_at` `created_at` datetime NULL  COMMENT 'Date note was created', 
       CHANGE `updated_at` `updated_at` datetime NULL  COMMENT 'Date note was updated';")
  end

  def self.down
  end
end
