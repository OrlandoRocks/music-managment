class CreateEntitlements < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `entitlements`(
                `id` int UNSIGNED not null auto_increment,
                `entitlement_rights_group_id` int UNSIGNED NULL COMMENT 'link to system rights provided to the inidividual',
                `updated_at` timestamp null,
                `created_at` timestamp null,                
                PRIMARY KEY  (`id`),
                KEY `entitlement_rights_group` (`entitlement_rights_group_id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
                
    execute up_sql
  end

  def self.down
    drop_table :entitlements
  end
end
