class CreateIndexBundleAlbumId < ActiveRecord::Migration[4.2]
  def self.up
    add_index :bundles, :album_id
  end

  def self.down
    remove_index :bundles, :album_id
  end
end
