class AddPercentOffToGainlogs < ActiveRecord::Migration[4.2]
  def self.up
    add_column :gainlogs, :percent_off, :integer, :null => false, :default => 0
    execute "update gainlogs set percent_off = 100 where album_total_after_discount = 0"
    execute "update gainlogs set percent_off = #{GAIN_DISCOUNT_PERCENTAGE} where album_total_after_discount != 0"
  end

  def self.down
    remove_column :gainlogs, :percent_off
  end
end
