class CreateProductItems < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `product_items`(
                `id` int UNSIGNED not null auto_increment,
                `product_id` int UNSIGNED not null COMMENT 'references product.id',
                `base_item_id` int UNSIGNED not null COMMENT 'references base_items.id',
                `name` varchar(150) not null COMMENT 'name of product item',
                `description` varchar(255) null COMMENT 'optional short description of product item',
                `price` decimal(10,2) not null default 0.00 COMMENT 'overrides any rules set for this item',
                `does_not_renew` tinyint(1) not null default 0,
                `renewal_type` ENUM('entitlement','distribution','inventory') COMMENT 'determines how the renewal is process on success or failure',
                `first_renewal_duration` tinyint COMMENT 'deteremines (with renewal_interval) how often long before the first interval/duration expires',
                `first_renewal_interval` enum('month','day','week','year') null COMMENT 'determines (with renewal_duration) how long before the first interval/duration expires',
                `renewal_duration` tinyint COMMENT 'deteremines (with renewal_interval) how often an entitlement will renew',
                `renewal_interval` enum('month','day','week','year') null COMMENT 'determines (with renewal_duration) how often an entitlement will renew',
                `renewal_product_id` int UNSIGNED COMMENT 'references the product to be purchased when this item renews',
                `updated_at` timestamp null,
                `created_at` timestamp null,                
                PRIMARY KEY  (`id`),
                KEY `product_id` (`product_id`),
                CONSTRAINT `fk_product_id` FOREIGN KEY (`product_id`) REFERENCES `products`(`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
            
    execute up_sql
  end

  def self.down
    drop_table :product_items
  end
end
