class CreateChargeables < ActiveRecord::Migration[4.2]
  def self.up
    create_table :chargeables do |t|
      t.column :short_code, :string, :limit => 20, :null => false, :default => nil
      t.column :price_calculator, :string, :limit => 50, :null => false, :default => nil
      t.column :base_price_cents, :integer, :null => false, :default => nil
      t.column :description, :string, :limit => 100, :null => false, :default => nil
    end

    if Object.const_defined?("Chargeable")
      Chargeable.create(:short_code => 'album', :price_calculator => 'current_album', :base_price_cents => 998, :description => "Album")
      Chargeable.create(:short_code => 'song', :price_calculator => 'current_song', :base_price_cents => 99, :description => "Song")
      Chargeable.create(:short_code => 'store', :price_calculator => 'current_store', :base_price_cents => 99, :description => "Store")
      Chargeable.create(:short_code => 'album_00', :price_calculator => 'album_free_store', :base_price_cents => 798, :description => "Legacy Album - 1 Free Store")
    end
    
    #add_column :albums, :chargeable_id, :integer, :null => false, :default => nil
    #add_column :songs, :chargeable_id, :integer, :null => false, :default => nil
    #add_column :salepoints, :chargeable_id, :integer, :null => false, :default => nil
    
    #execute "update albums set chargeable_id = #{Chargeable['album'].id}"
    #execute "update albums set chargeable_id = #{Chargeable['album_00'].id} where albums.legacy_annual_fee = 1"
    #execute "update songs set chargeable_id = #{Chargeable['song'].id}"
    #execute "update salepoints set chargeable_id = #{Chargeable['store'].id}"
  end

  def self.down
    remove_column :albums, :chargeable_id
    remove_column :songs, :chargeable_id
    remove_column :salepoints, :chargeable_id

    drop_table :chargeables
  end
end
