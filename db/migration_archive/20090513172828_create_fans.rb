#Created By:    Ed Cheung
#Purpose:       The following migration file creates a fans table that
#               will be use to store a fan's personal and contact information.
#               The 'run time' widget will have a field form to allow any user
#               on the internet to post their information.
#Relationship:  A person can have many widgets and a widget can have many fans
################################################################################
#Column Description:
#id:                  will be a unique identifier for a fan
#widget_id:           should foreign key widgets table
#unsubscribed_at:     datetime type stores the time when a
#                     fan unsubscribes from an artist's (person's) emailing list
#created_at:          datetime type stores the time when a record was created
#name:                80 characters long based on people table
#email:               128 characters long based on people table
#zip:                 stores world-wide zip codes. Some zip codes contain characters
#                     such as the ones from UK thus its a varchar type.
#gender:              tinyint type since queries will be slightly faster than varchar
#                     '0' for female and '1' for male
#                     
################################################################################
#Column Stats:
#
#
################################################################################
#Example Queries:
#1. select * from fans where widget_id = 5;
################################################################################

class CreateFans < ActiveRecord::Migration[4.2]
  def self.up
    create_table :fans do |t|
      t.integer   :widget_id
      t.string    :name,      :limit => 80
      t.string    :email,     :limit => 128
      t.string    :zip,       :limit => 10
      t.integer   :gender,    :limit => 1
      t.date      :birthday
      t.datetime  :unsubscribed_at
      t.datetime  :created_at
    end
  end

  def self.down
    drop_table :fans
  end
end
