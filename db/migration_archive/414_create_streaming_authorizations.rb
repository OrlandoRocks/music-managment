class CreateStreamingAuthorizations < ActiveRecord::Migration[4.2]
  def self.up
    create_table :streaming_authorizations do |t|

      t.column :person_id, :integer
      t.column :code, :string
      t.column :streaming_type, :string

      t.column :created_at, :datetime
      t.column :redeemed_at, :datetime
      t.column :redeemed_by_id, :integer

      t.column :expires_on, :datetime
    end
  end

  def self.down
    drop_table :streaming_authorizations
  end
end
