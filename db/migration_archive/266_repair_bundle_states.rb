class RepairBundleStates < ActiveRecord::Migration[4.2]
  def self.up
    puts "adjusting bundle states..."
    orphans = []
    updates = {}
    total_count = 0
    for bundle in PetriBundle.where("state <> 'dismissed'")
      unless bundle.album
        bundle.destroy
        orphans <<  bundle.album_id
        next
      end
      desired_state = bundle.next_state
      if desired_state != bundle.state
        total_count = total_count + 1
        key = "#{bundle.state} -> #{desired_state}"
        count = updates[key]
        if !count
          count = 0
          updates[key] = count
        end
        updates[key] = count + 1
        begin
          bundle.distribution_updated :actor => "bundle repair migration"
        rescue RuntimeError => e
          if e.message =~ /Bundling process unable to release lock on album/
            puts "  #{e.message}"
            next
          else
            raise e
          end
        end
        if (desired_state != bundle.state)
          puts "  forcing bundle #{bundle.id} from #{bundle.state} -> #{desired_state}"
          bundle.state = desired_state
        end
      end
    end
    puts " deleted #{orphans.length} bundles that were orhpaned(during replication??...album ids: #{orphans.join(',')})"
    puts " updated #{total_count} bundles"
    updates.each do |key, count|
      printf("%10d: %s\n", count, key)
    end
  end

  def self.down
    #no can do
  end
end
