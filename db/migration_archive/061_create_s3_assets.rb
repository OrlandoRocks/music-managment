class CreateS3Assets < ActiveRecord::Migration[4.2]
  def self.up
    create_table :s3_assets do |t|
      t.column :key, :string
      t.column :bucket, :string
      t.column :created_at, :datetime
      t.column :updated_at, :datetime
    end
  end

  def self.down
    drop_table :s3_assets
  end
end
