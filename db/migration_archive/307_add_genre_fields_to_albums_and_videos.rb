class AddGenreFieldsToAlbumsAndVideos < ActiveRecord::Migration[4.2]
  def self.up
    add_column :albums, :primary_genre_id, :integer
    add_column :albums, :secondary_genre_id, :integer
    add_column :videos, :primary_genre_id, :integer
    add_column :videos, :secondary_genre_id, :integer
    add_index :albums, :primary_genre_id
    add_index :albums, :secondary_genre_id
    add_index :videos, :primary_genre_id
    add_index :videos, :secondary_genre_id
  end

  def self.down
    remove_column :albums, :primary_genre_id
    remove_column :albums, :secondary_genre_id
    remove_column :videos, :primary_genre_id
    remove_column :videos, :secondary_genre_id
  end
end
