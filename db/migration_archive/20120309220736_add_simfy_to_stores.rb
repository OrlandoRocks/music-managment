class AddSimfyToStores < ActiveRecord::Migration[4.2]
  def self.up
    Store.create(:name=>'simfy',:abbrev=>'sm', :short_name=>'simfy', :position=>280, :needs_rights_assignment=>false, :is_active=>false,:base_price_policy_id=>"3" )
  end

  def self.down
    execute("DELETE FROM stores where name = 'simfy'")
  end
end
