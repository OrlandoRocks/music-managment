class CreateCmsContent < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `cms_content` (
      `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
      `category` ENUM('slug', 'full_page', 'ad') NOT NULL DEFAULT 'slug' COMMENT 'placement of content on page ',
      `display_page_name` varchar(100) NOT NULL DEFAULT 'N/A' COMMENT 'name of the page to display',
      `partner_id` TINYINT NOT NULL DEFAULT 0 COMMENT 'partner_id of this content. 0 means all partners.',
      `position` TINYINT NULL COMMENT 'if a full_page, this determines where the content is loaded into',
      `sort_order` TINYINT NULL COMMENT 'sort of slugs if loaded in a full page context',
      `container` ENUM('wide', 'sidebar', 'header', 'footer') NOT NULL DEFAULT 'sidebar' COMMENT 'determines what div element it should be rendered in',
      `markup` TEXT NOT NULL COMMENT 'html markup to display',
      `created_by_id` INT(10) NOT NULL COMMENT 'id of administrator who created the content',
      `updated_at` TIMESTAMP NULL COMMENT 'Datetime of latest update',
      `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',
      PRIMARY KEY  (`id`),
      KEY `ix_cms_content_page_name` (`display_page_name`)
    ) ENGINE=INNODB DEFAULT CHARSET=utf8;)
                  
    execute up_sql
  end

  def self.down
    drop_table :cms_content
  end
end
