class AddSalesrecordSubstore < ActiveRecord::Migration[4.2]
  def self.up
    add_column :sales_records, :substore, :string, :limit => 30
  end

  def self.down
    remove_column :sales_records, :substore
  end
end
