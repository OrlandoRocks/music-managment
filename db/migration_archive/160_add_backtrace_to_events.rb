
class AddBacktraceToEvents < ActiveRecord::Migration[4.2]
  def self.up
    add_column :events, :backtrace, :text
  end
  
  def self.down
    remove_column :events, :backtrace
  end
end
