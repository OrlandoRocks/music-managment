class AddExternalAssetToArtwork < ActiveRecord::Migration[4.2]
  def self.up
    add_column :artworks, :external_asset_id, :integer
  end

  def self.down
    remove_column :artworks, :external_asset_id
  end
end
