class DeleteEmusic < ActiveRecord::Migration[4.2]
  def self.up
    Store.where("name = 'eMusic'").destroy_all
    remove_column :stores, :price_cents
  end

  def self.down
    Store.create :name=>"eMusic",:abbrev=>"em"
    create_column :stores, :price_cents, :integer, :default => 0, :null => false
  end
end
