class MoreSalepointAudit < ActiveRecord::Migration[4.2]
  def self.up
    execute "delete from salepoint_audits" # we are blatting them to start over
    add_column :salepoint_audits, :reported_status, :string, :limit => 20
    add_column :salepoint_audits, :report_type, :string, :limit => 20
    add_column :salepoint_audits, :created_on, :date, :null => false
    remove_column :salepoint_audits, :created_at
    remove_column :salepoint_audits, :upc
  end

  def self.down
    add_column :salepoint_audits, :created_at, :datetime, :null => false
    add_column :salepoint_audits, :upc, :text
    remove_column :salepoint_audits, :created_on
    remove_column :salepoint_audits, :reported_status
    remove_column :salepoint_audits, :report_type
  end
end
