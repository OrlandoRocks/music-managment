class AddAlbumFinalized < ActiveRecord::Migration[4.2]
  def self.up
		add_column :albums, :finalized_at, :datetime, :null => true, :default => nil
  end

  def self.down
		remove_column :albums, :finalized_at
  end
end
