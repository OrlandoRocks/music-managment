class CreateTouchStatuses < ActiveRecord::Migration[4.2]
  def self.up
    create_table :touch_statuses do |t|
      t.column :touchable_id, :integer
      t.column :touchable_type, :string
      t.column :touched_at, :datetime, :default => nil
    end
  end

  def self.down
    drop_table :touch_statuses
  end
end
