class AddFieldsToItunesUserReport < ActiveRecord::Migration[4.2]
  def self.up
    add_column :itunes_user_reports, :name, :string
  end

  def self.down
    remove_column :itunes_user_reports, :name
  end
end
