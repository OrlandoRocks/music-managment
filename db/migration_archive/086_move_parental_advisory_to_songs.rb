class MoveParentalAdvisoryToSongs < ActiveRecord::Migration[4.2]
  def self.up
    puts "migrating parental advisory to songs"
    execute "update songs set parental_advisory = 0"
    execute "update songs left join albums on songs.album_id = albums.id set songs.parental_advisory = 1 where albums.parental_advisory = '1'"
  end

  def self.down
    puts "migrating parental advisory to album"
    execute "update albums set parental_advisory = '0'"
    execute "update albums left join songs on songs.album_id = albums.id set albums.parental_advisory = '1' where songs.parental_advisory = 1"
  end
end
