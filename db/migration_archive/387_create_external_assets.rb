class CreateExternalAssets < ActiveRecord::Migration[4.2]
  def self.up
    create_table :external_assets do |t|
      t.column :type, :string
      t.column :url, :string
      t.column :created_at, :datetime
      t.column :updated_at, :datetime
    end
  end

  def self.down
    drop_table :external_assets
  end
end
