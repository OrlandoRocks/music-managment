class AddDistributionDeliveryType < ActiveRecord::Migration[4.2]
  def self.up
    add_column :distributions, :delivery_type, :string
  end

  def self.down
    remove_column :distributions, :delivery_type
  end
end
