class AddFreeStorePricePolicy < ActiveRecord::Migration[4.2]
  def self.up
    PricePolicy.create(:short_code => 'freestore', :price_calculator => 'current_store', :base_price_cents => 0, :description => "Free Store")
  end

  def self.down
    PricePolicy.find_by(short_code: 'freestore').destroy
  end
end
