class RemoveArtistsSongsTable < ActiveRecord::Migration[4.2]
  def self.up
    drop_table :artists_songs
  end

  def self.down
    create_table :artists_songs, :id => false, :force => true do |t|
      t.column :artist_id, :integer
      t.column :song_id, :integer
    end
  end
end
