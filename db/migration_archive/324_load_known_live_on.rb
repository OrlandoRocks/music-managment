class LoadKnownLiveOn < ActiveRecord::Migration[4.2]
  def self.up
    albums = Album.where("known_live_on is NULL and finalized_at is not NULL")
    albums.each do |album|
      begin
        album.known_live_on = album.first_known_live_on
        album.save!
      rescue => e
        puts "Couldn't update album #{album.id}: #{e.message}"
      end
    end
  end

  def self.down
  end
end
