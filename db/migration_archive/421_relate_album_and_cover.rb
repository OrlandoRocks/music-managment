class RelateAlbumAndCover < ActiveRecord::Migration[4.2]
  def self.up
    add_column :covers, :album_id, :integer
  end

  def self.down
    remove_column :covers, :album_id
  end
end
