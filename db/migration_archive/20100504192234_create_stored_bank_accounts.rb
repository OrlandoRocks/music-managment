class CreateStoredBankAccounts < ActiveRecord::Migration[4.2]
  def self.up
    upstring = %Q(
      CREATE TABLE  `stored_bank_accounts` (
        `id` int UNSIGNED NOT NULL auto_increment,
        `person_id` int UNSIGNED NOT NULL COMMENT "References people table",
        `customer_vault_id` char(32) NOT NULL COMMENT "References external storage id at Braintree",
        `bank_name` varchar(255) default NULL COMMENT "Bank Name",
        `last_four_routing` char(4) default NULL COMMENT "Last four digits of routing #",
        `last_four_account` char(4) default NULL COMMENT "Last four digits of account #",
        `sec_code` char(3) default NULL COMMENT "Sec code used for processing, all EFT's should be CCD'",
        `account_type` ENUM('checking', 'savings') NULL COMMENT "type of account (used by braintree)",
        `account_holder_type` ENUM('personal', 'business') NULL COMMENT "type of account holder (used by braintree)",
        `name` varchar(120) default NULL COMMENT "Full name on account",
        `company` varchar(100) default NULL COMMENT "Company name on bank account if applicable",
        `address1` varchar(255) default NULL COMMENT "Bank Account Address line 1",
        `address2` varchar(255) default NULL COMMENT "Bank Account Address line 2",
        `city` varchar(100) default NULL COMMENT "Bank Account Address city",
        `state` char(2) default NULL COMMENT "Bank Account Address State - ANSI CODE",
        `country` char(2) default NULL COMMENT "Bank Account Address country - ANSI CODE",
        `zip` varchar(10) default NULL COMMENT "Bank Account Address zip",
        `phone` varchar(50) default NULL COMMENT "contact phone number",
        `status` ENUM("valid", "processing_error", "retrieval_error", "destroyed","destruction_error") default NULL COMMENT "defines the state of the sba record. current means it is up to date",
        `deleted_at` datetime default NULL COMMENT "date customer deleted bank acount",
        `created_at` datetime default NULL,
        `updated_at` datetime default NULL,
        PRIMARY KEY  (`id`),
        KEY `sba_person_id` (`person_id`),
        KEY `sba_customer_vault_id` (`customer_vault_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8)

      execute upstring

  end

  def self.down
    drop_table :stored_bank_accounts
  end
end
