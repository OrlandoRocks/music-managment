class LengthenAlbumName < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = "ALTER TABLE `albums` CHANGE `name` `name` VARCHAR(255) DEFAULT '' NULL;"
    execute up_sql
  end

  def self.down
    down_sql = "ALTER TABLE `albums` CHANGE `name` `name` VARCHAR(129) DEFAULT '' NULL;"
    execute down_sql
  end
end
