class AddAndPopulateStoresTable < ActiveRecord::Migration[4.2]
  def self.up
    create_table "salepoints", :id => false, :force => true do |t|
      t.column "store_id", :integer, :default => 0, :null => false
      t.column "album_id", :integer, :default => 0, :null => false
    end

    create_table "stores", :force => true do |t|
      t.column "name", :string, :limit => 60, :default => "", :null => false
      t.column "abbrev", :string, :limit => 2, :default => "", :null => false
      t.column "price_cents", :integer, :default => 99, :null => false
    end
    
   
  end

  def self.down
    drop_table :albums_stores
    drop_table :stores
  end
end
