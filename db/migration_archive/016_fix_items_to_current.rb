class FixItemsToCurrent < ActiveRecord::Migration[4.2]
  def self.up
     drop_table :items;
     create_table "items", :force => true do |t|
        t.column "name", :string, :limit => 48, :default => "0", :null => false
        t.column "price_cents", :integer, :default => 0, :null => false
      end
      
     items = [["album","798"],["song","99"],["store","99"]]
      items.each { |item|
        Item.create :name=>item[0], :price_cents=>item[1]
      }
  end

  def self.down
    drop_table :items;
     create_table "items", :force => true do |t|
        t.column "name", :string, :limit => 48, :default => "0", :null => false
        t.column "price_cents", :integer, :default => 0, :null => false
      end
      
     items = [["Album","898"],["Extra Song","75"],["Custom Art","99"]]
      items.each { |item|
        Item.create :name=>item[0], :price_cents=>item[1]
      }
  end
end
