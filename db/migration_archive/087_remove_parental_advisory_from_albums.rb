class RemoveParentalAdvisoryFromAlbums < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :albums, :parental_advisory
  end

  def self.down
    add_column :albums, :parental_advisory, :string, :limit => 1, :default => "0", :null => false
  end
end
