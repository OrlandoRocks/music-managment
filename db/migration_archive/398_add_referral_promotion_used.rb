class AddReferralPromotionUsed < ActiveRecord::Migration[4.2]
  def self.up
    add_column :people, :referral_promotion_applied, :boolean, :default => false
  end

  def self.down
    remove_column :people, :referral_promotion_applied
  end
end
