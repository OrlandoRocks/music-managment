class CreatePaymentBatches < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `payment_batches`(
                `id` int UNSIGNED not null auto_increment COMMENT 'Primary KEy',
                `batch_date` DATETIME null COMMENT 'Datetime that batch was run',
                `total_amount` decimal(10,2) not null default 0.00 COMMENT 'Total Amount in Batch',
                `balance_amount` decimal(10,2) not null default 0.00 COMMENT 'Total Paid by Customer Balance in Batch',
                `cannot_process_amount` decimal(10,2) not null default 0.00 COMMENT 'Total Amount for Cannot Process in Batch',
                `braintree_amount_total` decimal(10,2) not null default 0.00 COMMENT 'Total Amount for BrainTree in Batch',
                `braintree_amount_successful` decimal(10,2) not null default 0.00 COMMENT 'Total Successfully Paid by Braintree in Batch',
                `batch_file_name` varchar(200) null COMMENT 'Name of physical csv file saved on server, sent to braintree',
                `batch_sent_at` timestamp null COMMENT 'Datetime file sent to braintree',
                `response_received` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Indicate if the batch has been received',
                `response_processed_at` timestamp null COMMENT 'Datetime file received back from braintree', 
                `response_file_name` varchar(200) null COMMENT 'Name of physical csv file saved on server, received from Braintree',
                `status` ENUM('complete','processing','new') NOT NULL DEFAULT 'new' COMMENT 'Current State',
                `updated_at` TIMESTAMP NULL COMMENT 'Datetime of latest update',
                `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',               
                PRIMARY KEY  (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
                
    execute up_sql
  end

  def self.down
    drop_table :payment_batches
  end
end
