class AddFieldsToVideo < ActiveRecord::Migration[4.2]
  def self.up
    add_column :videos, :created_on, :date
    add_column :videos, :updated_at, :datetime
  end

  def self.down
    remove_column :videos, :created_on
    remove_column :videos, :updated_at
  end
end
