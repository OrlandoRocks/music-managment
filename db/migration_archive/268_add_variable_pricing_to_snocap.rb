class AddVariablePricingToSnocap < ActiveRecord::Migration[4.2]
  def self.up
    store = Store.find_by(short_name: "SNOCAP")

    store.update_attribute(:variable_pricing, true)

    VariablePrice.create(:store_id => store.id, :price_code => 'premium', :price_code_display => 'Premium', :active => true, :position => 1)
    VariablePrice.create(:store_id => store.id, :price_code => 'mid', :price_code_display => 'Medium', :active => true, :position => 2)
    VariablePrice.create(:store_id => store.id, :price_code => 'low', :price_code_display => 'Low', :active => true, :position => 3)
  end

  def self.down
    store = Store.find_by(short_name: "SNOCAP")
    VariablePrice.where("store_id = #{store.id}").destroy_all
    store.update_attribute(:variable_pricing, false)
  end
end
