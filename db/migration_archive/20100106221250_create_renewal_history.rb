class CreateRenewalHistory < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `renewal_history`(
                `id` int UNSIGNED not null auto_increment,
                `renewal_id` int UNSIGNED not null,
                `purchase_id` int UNSIGNED not null,
                `starts_at` timestamp default 0 COMMENT 'start date of renewal history record',
                `expires_at` timestamp default 0 COMMENT 'expiration date on which this particular renewal will expire',
                `created_at` timestamp null,                
                PRIMARY KEY  (`id`),
                KEY `purchase_id` (`purchase_id`),
                KEY `renewal` (`renewal_id`, `starts_at`, `expires_at`),
                CONSTRAINT `fk_renewal` FOREIGN KEY (`renewal_id`) REFERENCES `renewals`(`id`) 
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
                
    execute up_sql
  end

  def self.down
    drop_table  :renewal_history
  end
end
