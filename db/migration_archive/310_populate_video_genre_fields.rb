class PopulateVideoGenreFields < ActiveRecord::Migration[4.2]
    def self.up

      Video.all.each do |video|
        sg = GenresVideos.where(video_id: video.id)

        if sg.size == 1
          video.update_attribute(:primary_genre_id, sg[0].genre_id)
        elsif sg.size > 1
          video.update_attribute(:primary_genre_id, sg[0].genre_id)
          video.update_attribute(:secondary_genre_id, sg[1].genre_id)
        end
      end

    end

    def self.down
      Video.all do |video|
        GenresVideos.create!(:video_id => video.id, :genre_id => video.primary_genre_id) unless video.primary_genre_id.nil?
        GenresVideos.create!(:video_id => video.id, :genre_id => video.secondary_genre_id) unless video.secondary_genre_id.nil?
      end
    end
  end


  #this make the albums_genres table a temporary model used in the migration above
  class GenresVideos < ApplicationRecord

  end
