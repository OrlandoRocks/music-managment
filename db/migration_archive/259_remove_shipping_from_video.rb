class RemoveShippingFromVideo < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :videos, :shipping_label
  end

  def self.down
    add_column :videos, :shipping_label, :binary
  end
end
