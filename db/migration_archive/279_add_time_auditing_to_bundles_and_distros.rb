class AddTimeAuditingToBundlesAndDistros < ActiveRecord::Migration[4.2]
  def self.up
    add_column :petri_bundles, :created_at, :timestamp, :null => false
    add_column :petri_bundles, :updated_at, :timestamp

    add_column :distributions, :created_at, :timestamp, :null => false
    add_column :distributions, :updated_at, :timestamp

    rename_column :transitions, :time, :created_at

    puts "estimating creation time for all non-dismissed bundles."
    bundle_count = 0
    distro_count = 0
    for bundle in PetriBundle.where("state <> 'dismissed'")
      if transition = Transition.where("state_machine_type = 'PetriBundle' AND state_machine_id = #{bundle.id}").order("id ASC").first
        bundle.created_at = transition.created_at
        updated = Transition.where("state_machine_type = 'PetriBundle' AND state_machine_id = #{bundle.id}").order("id DESC").first
        bundle.updated_at = updated.created_at
        bundle.save!
        bundle_count = bundle_count + 1
      end
    end
    for distro in Distribution.where("state <> 'dismissed'")
      if transition = Transition.where("state_machine_type = 'Distribution' AND state_machine_id = #{distro.id}").order("id ASC").first
        distro.created_at = transition.created_at
        updated = Transition.where("state_machine_type = 'Distribution' AND state_machine_id = #{distro.id}").order("id DESC").first
        distro.updated_at = updated.created_at
        distro.save!
        distro_count = distro_count + 1
      end
    end
    puts "updated #{bundle_count} bundles, and #{distro_count} distributions"
  end

  def self.down
    remove_column :petri_bundles, :created_at
    remove_column :petri_bundles, :updated_at
    remove_column :distributions, :created_at
    remove_column :distributions, :updated_at

    rename_column :transitions, :created_at, :time
  end
end
