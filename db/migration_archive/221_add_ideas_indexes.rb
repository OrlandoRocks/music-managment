class AddIdeasIndexes < ActiveRecord::Migration[4.2]

  def self.soft_add_index(table,index_name,column)
    begin
      add_index table, column, :name => index_name
    rescue ActiveRecord::StatementInvalid => e
      puts "Index already present or couldn't be added: #{table}.#{column}"
    end
  end

  def self.soft_remove_index(table,index_name,column)
    begin
      remove_index table, :name => index_name
    rescue ActiveRecord::StatementInvalid => e
      puts "Index not present or couldn't be removed: #{table} -- #{index_name}"
    end
  end
    
  def self.up
    soft_add_index :ideas, :ideas_album_id, :album_id
    soft_add_index :idea_exceptions, :idea_exceptions_album_id, :album_id
    soft_add_index :upcs, :upc_upcable_id, :upcable_id
  end

  def self.down
    soft_remove_index :ideas, :ideas_album_id, :album_id
    soft_remove_index :idea_exceptions, :idea_exceptions_album_id, :album_id
    soft_remove_index :upcs, :upc_upcable_id, :upcable_id
  end
end
