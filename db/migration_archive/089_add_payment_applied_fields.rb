class AddPaymentAppliedFields < ActiveRecord::Migration[4.2]
  def self.up
    add_column :albums, :payment_applied, :boolean, :null => false, :default => false
    add_column :songs, :payment_applied, :boolean, :null => false, :default => false
    add_column :salepoints, :payment_applied, :boolean, :null => false, :default => false
  end

  def self.down
    remove_column :albums, :payment_applied
    remove_column :songs, :payment_applied
    remove_column :salepoints, :payment_applied
  end
end
