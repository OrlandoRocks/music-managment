class AddMuveToPartnerStores < ActiveRecord::Migration[4.2]

  def self.up
    storeid = Store.find_by(name: "Muve Music").id
    if storeid != nil
    Partner.all.each do |partner|
      execute("insert into partners_stores (partner_id,store_id) values (#{partner.id},#{storeid})")
    end
    else
      puts "Sorry, no store by the name of Muve Music"
    end
  end

  def self.down
    storeid = Store.find_by(name: "Muve Music").id
    if storeid !=nil
      execute("delete from partners_stores where store_id = #{storeid}")
    else
      puts "Couldn't find muve music in store db"
    end
  end
end
