class AddIdeaStatus < ActiveRecord::Migration[4.2]
  def self.up
		add_column :ideas, :connect_status, :string
		add_column :ideas, :emusic_status, :string
		add_column :ideas, :groupietunes_status, :string
		add_column :ideas, :itunes_status, :string
		add_column :ideas, :musicnet_status, :string
		add_column :ideas, :napster_status, :string
		add_column :ideas, :rhapsody_status, :string
  end

  def self.down 
    remove_column :ideas, :connect_status
    remove_column :ideas, :emusic_status
		remove_column :ideas, :groupietunes_status
		remove_column :ideas, :itunes_status
		remove_column :ideas, :musicnet_status
		remove_column :ideas, :napster_status
		remove_column :ideas, :rhapsody_status
  end
end
