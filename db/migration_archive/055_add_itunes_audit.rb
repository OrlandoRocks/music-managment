class AddItunesAudit < ActiveRecord::Migration[4.2]
  def self.up
    create_table :salepoint_audits do |t|
      t.column :created_at, :datetime, :null => false
      t.column :upc, :text
      t.column :album_id, :integer
      t.column :salepoint_id, :integer
      t.column :status, :string, :limit => 20, :null => false
    end
    add_index :salepoint_audits, :album_id
    add_index :salepoint_audits, :salepoint_id

    add_column :salepoints, :current_audit_id, :integer, :null => true
    add_index :salepoints, :current_audit_id
  end

  def self.down
    drop_table :salepoint_audits
    remove_index :salepoints, :current_audit_id
    remove_column :salepoints, :current_audit_id
  end
end
