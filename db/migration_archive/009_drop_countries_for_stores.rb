class DropCountriesForStores < ActiveRecord::Migration[4.2]
  def self.up
        drop_table :stores
        drop_table :countries
        drop_table :albums_countries
        create_table "stores", :force => true do |t|
          t.column "name", :string, :limit => 60, :default => "", :null => false
          t.column "abbrev", :string, :limit => 2, :default => "", :null => false
          t.column "price_cents", :integer, :default => 99, :null => false
        end

                
        stores = [["iTunes U.S.","us","0"],["iTunes Australia","au","99"],["iTunes Canada","ca","99"],
                    ["iTunes European Union","eu","99"],["iTunes Japan","jp","99"],["iTunes United Kingdom","uk","99"],
                    ["Rhapsody","rh","99"],["EMusic","em","99"]]
         
       
    stores.each { |store|
      # can't do the following now that we've added validation of newer fields to the model
      #  Store.create :name=>store[0],:abbrev=>store[1],:price_cents=>store[2]
      # so we'll just resort to sql
      Store.connection.insert("INSERT INTO stores(name,abbrev,price_cents) VALUES ('#{store[0]}','#{store[1]}','#{store[2]}')")
    }
  end

  def self.down
      create_table "countries", :force => true do |t|
         t.column "name", :string, :limit => 60, :default => "", :null => false
         t.column "abbrev", :string, :limit => 2, :default => "", :null => false
         t.column "price_cents", :integer, :default => 99, :null => false
       end
#        countries = [["United States","us","0"],["Australia","au","99"],["Canada","ca","99"],
#                    ["European Union","eu","99"],["Japan","jp","99"],["United Kingdom","uk","99"]]
#                    
#        countries.each { |country|
#          Country.create :name=>country[0],:abbrev=>country[1],:price_cents=>country[2]
#        }
       drop_table :stores
       create_table "stores", :force => true do |t|
         t.column "name", :string, :limit => 60, :default => "", :null => false
         t.column "abbrev", :string, :limit => 2, :default => "", :null => false
         t.column "price_cents", :integer, :default => 99, :null => false
       end
       add_index "albums_stores", ["store_id"], :name => "store_id"
       add_index "albums_stores", ["album_id"], :name => "title_id"

       stores = [["iTunes","iT","0"],["Rhapsody","Rh","99"]]
       stores.each { |store|
         Store.create :name=>store[0],:abbrev=>store[1],:price_cents=>store[2]
       }
       create_table "albums_countries", :id => false, :force => true do |t|
         t.column "country_id", :integer, :default => 0, :null => false
         t.column "album_id", :integer, :default => 0, :null => false
       end

       add_index "albums_countries", ["country_id"], :name => "country_id"
       add_index "albums_countries", ["album_id"], :name => "title_id"
       
  end
end
