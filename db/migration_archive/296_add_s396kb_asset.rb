class AddS396kbAsset < ActiveRecord::Migration[4.2]
  def self.up
    add_column :songs, :s3_96kb_asset_id, :integer
  end

  def self.down
    remove_column :songs, :s3_96kb_asset_id
  end
end
