class CreateBaseItemOptions < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `base_item_options`(
                `id` int UNSIGNED not null auto_increment,
                `base_item_id` int UNSIGNED not null COMMENT 'references product.id',
                `sort_order` tinyint not null default 0 COMMENT 'cardinal order for oprtion',
                `parent_id` int UNSIGNED null COMMENT 'reference id for parent rule, required for multiple rules that govern prices',
                `name` varchar(150) not null COMMENT 'name of option',
                `product_type` ENUM('Ad Hoc', 'Package', 'All') default 'Ad Hoc' COMMENT 'determines what type of product, the option applies to',
                `option_type` ENUM('required','select_one','optional') default 'required' COMMENT 'how should this pricing rule option be selected on the UI',
                `rule_type` ENUM('inventory','entitlement', 'price_only') COMMENT 'grants a user inventory or an entitlement',
                `entitlement_rights_group_id` int UNSIGNED null,
                `rule` ENUM('price_for_each', 'total_included', 'price_above_included', 'price_for_each_above_included', 'range', 'true_false') not null COMMENT 'processing rule for determining price',
                `inventory_type` varchar(50) not null COMMENT 'inventory_type of product item being sold',
                `model_to_check` varchar(50) null COMMENT 'required for rules other than price_for_each',
                `method_to_check` varchar(50) null COMMENT 'method to check on the provided model',
                `quantity` mediumint null COMMENT 'how many are granted to the user',
                `unlimited` tinyint(1) not null default 0 COMMENT 'is the user granted unlimited inventory items',
                `true_false` tinyint(1) null COMMENT 'used for true_false rule',
                `minimum` mediumint null COMMENT 'start of range/threshhold',
                `maximum` mediumint null COMMENT 'end of range/threshold',
                `price` decimal(10,2) not null default 0.00 COMMENT 'price when the rules are matched, can be 0.00',
                `created_at` timestamp null,                
                PRIMARY KEY  (`id`),
                KEY `base_item_id` (`base_item_id`),
                CONSTRAINT `fk_base_item_id` FOREIGN KEY (`base_item_id`) REFERENCES `base_items`(`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
            
    execute up_sql
  end

  def self.down
    drop_table :base_item_options
  end
end
