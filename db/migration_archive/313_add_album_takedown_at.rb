class AddAlbumTakedownAt < ActiveRecord::Migration[4.2]
  def self.up
    add_column :albums, :takedown_at, :datetime
  end

  def self.down
    remove_column :albums, :takedown_at
  end
end
