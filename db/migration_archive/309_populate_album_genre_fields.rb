class PopulateAlbumGenreFields < ActiveRecord::Migration[4.2]
  def self.up

    puts "This migration takes a long time (like an hour)"

    Album.all.each do |album|
      sg = AlbumsGenres.where(album_id: album.id)

      #puts "sg size = #{sg.size}"
      if sg.size == 1
        #puts "genre #{sg[0]}, #{sg[0].genre_id}"
        album.update_attribute(:primary_genre_id, sg[0].genre_id)
      elsif sg.size > 1
        album.update_attribute(:primary_genre_id, sg[0].genre_id)
        album.update_attribute(:secondary_genre_id, sg[1].genre_id)
      end
    end

  end

  def self.down

    Album.all do |album|
      AlbumsGenres.create!(:album_id => album.id, :genre_id => album.primary_genre_id) unless album.primary_genre_id.nil?
      AlbumsGenres.create!(:album_id => album.id, :genre_id => album.secondary_genre_id) unless album.secondary_genre_id.nil?

    end
  end
end


#this make the albums_genres table a temporary model used in the migration above
class AlbumsGenres < ApplicationRecord

end
