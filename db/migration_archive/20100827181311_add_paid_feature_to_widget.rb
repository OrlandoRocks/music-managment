class AddPaidFeatureToWidget < ActiveRecord::Migration[4.2]
  def self.up
    up_sql=%Q(ALTER TABLE `widgets`
              ADD COLUMN `deactivated_at` TIMESTAMP NULL AFTER `updated_at`,
              ADD COLUMN `created_as_free` ENUM('Y','N') DEFAULT 'Y' NULL AFTER `deactivated_at`,
              ADD COLUMN `free_song_enable` ENUM('Y','N') DEFAULT 'Y' NULL AFTER `created_as_free`,
              ADD COLUMN `free_song_id` INT UNSIGNED NULL AFTER `free_song_enable`,
              ADD COLUMN `free_song_text` VARCHAR(255) NULL AFTER `free_song_id`;
            ) 
    execute up_sql
  end

  def self.down
    remove_column :widgets, :deactivated_at
    remove_column :widgets, :created_as_free
    remove_column :widgets, :free_song_enable
    remove_column :widgets, :free_song_id
    remove_column :widgets, :free_song_text
  end
end
