class AddCountryToPerson < ActiveRecord::Migration[4.2]
  def self.up
    add_column :people, :country, :string
    add_column :people, :us_zip_code_id, :integer
  end

  def self.down
    remove_column :people, :country
    remove_column :people, :us_zip_code_id
  end
end
