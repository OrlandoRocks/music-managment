class AddUploadsIndex < ActiveRecord::Migration[4.2]
  def self.up
    add_index :uploads, :song_id
  end

  def self.down
    remove_index :uploads, :song_id
  end
end
