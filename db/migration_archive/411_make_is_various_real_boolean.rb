class MakeIsVariousRealBoolean < ActiveRecord::Migration[4.2]
  def self.up
    change_column :albums, :is_various, :boolean, :default => 0
  end

  def self.down
    change_column :albums, :is_various, :string, :limit => 1, :default => "0"
  end
end
