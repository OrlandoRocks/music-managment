class CreatePayoutServiceFees < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `payout_service_fees` (
      `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
      `fee_type` ENUM('eft_service','eft_failure','check_service') NULL COMMENT 'type of service fee',
      `amount` DECIMAL(10,2) NOT NULL DEFAULT '0.00' COMMENT 'Amount',
      `posted_by_id` INT(10) UNSIGNED NULL COMMENT 'Foreign Key to People table',
      `posted_by_name` varchar(255) NULL COMMENT 'Admin name who changed service fee',
      `archived_at` datetime default NULL COMMENT "Date no longer in use",
      `updated_at` TIMESTAMP NULL COMMENT 'Datetime of latest update',
      `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',
      PRIMARY KEY  (`id`),
      KEY `psf_posted_by_id` (`posted_by_id`),
      CONSTRAINT `FK_psf_posted_by_id` FOREIGN KEY (`posted_by_id`) REFERENCES `people` (`id`)
    ) ENGINE=INNODB DEFAULT CHARSET=utf8;)
                  
    execute up_sql
  end

  def self.down
   drop_table :payout_service_fees
  end
end
