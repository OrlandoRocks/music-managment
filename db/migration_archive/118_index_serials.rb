class IndexSerials < ActiveRecord::Migration[4.2]
  def self.up
    add_index :serials, [:person_id], :unique => true, :name => :uniq_person_serial
    add_column :serials, :os, :string, :null => false, :limit => 15
    add_index :serials, [:os], :name => :serial_os_type
  end

  def self.down
    remove_index :serials, :name => :uniq_person_serial
    remove_column :serials, :os
  end

end
