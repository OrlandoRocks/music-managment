class AddFlatRateSingle < ActiveRecord::Migration[4.2]
  def self.up
    PricePolicy.create!(:short_code => 'flat_single', :price_calculator => 'current_flat_rate', :base_price_cents => 999, :description => '$9.99 flat rate single album')
  end

  def self.down
    PricePolicy.find_by(short_code: 'flat_single').destroy
  end
end
