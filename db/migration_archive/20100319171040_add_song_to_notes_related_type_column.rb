class AddSongToNotesRelatedTypeColumn < ActiveRecord::Migration[4.2]
  def self.up
    sqlcmd = %q(ALTER TABLE notes
                CHANGE `related_type` `related_type` enum('Person','Album','Song') character set utf8 collate utf8_general_ci NULL  comment 'Type of entity.';)
    execute(sqlcmd)
  end

  def self.down
    sqlcmd = %q(ALTER TABLE notes
                CHANGE `related_type` `related_type` enum('Person','Album') character set utf8 collate utf8_general_ci NULL  comment 'Type of entity.';)
    execute(sqlcmd)
  end
end
