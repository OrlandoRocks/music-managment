#Created By:    Ed Cheung
#Date:          2009-08-19
#Purpose:       The following migration creates a local assets table to store
#               the file system path of user uploaded files in the front end app
#               server
#
################################################################################
#
#Column Description:
#id:              unique identifier, primary key
#path:            filesystem path
#created_at:      upload time
#updated_at:      upload time
################################################################################

class CreateLocalAssets < ActiveRecord::Migration[4.2]
  def self.up
    create_local_assets_table=<<-EOSTRING
      CREATE TABLE local_assets (
       id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key.',
       path VARCHAR(255) NOT NULL COMMENT 'Path to location of where file is stored.',
       created_at DATETIME NOT NULL COMMENT 'Date record Created.',
       updated_at DATETIME NOT NULL COMMENT 'Date record Updated.',
       PRIMARY KEY (id)
      ) ENGINE=INNODB DEFAULT CHARSET=utf8;
    EOSTRING

    execute create_local_assets_table
  end

  def self.down
    drop_table :local_assets
  end
end
