class UpdatesToLegacyAlbums < ActiveRecord::Migration[4.2]
  ALBUM_PAYMENT_APPLIED_STATES = %w|
    completed
    exported
    uploaded
    errored
    paid
    errored(art)
    pulled
  |.collect {|x| "'#{x}'"}.join(',')

  ALBUM_FINALIZED_STATES = %w|
    completed
    exported
    uploaded
    pulled
    errored
  |.collect {|x| "'#{x}'"}.join(',')

  def self.up
    execute "update albums set payment_applied = 1 where album_state in (#{ALBUM_PAYMENT_APPLIED_STATES})"
    execute "update albums set finalized_at = '#{Time.now.to_formatted_s(:db)}' where album_state in (#{ALBUM_FINALIZED_STATES})"
    execute "update salepoints set payment_applied = 1, finalized_at = '#{Time.now.to_formatted_s(:db)}' where status != 'new'"
  end

  def self.down
  end
end
