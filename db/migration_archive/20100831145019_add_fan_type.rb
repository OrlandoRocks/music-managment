class AddFanType < ActiveRecord::Migration[4.2]
  def self.up
    up_sql=%Q(ALTER TABLE `fans`
              ADD COLUMN `fan_type` ENUM('Fan','FreeSongFan') DEFAULT 'Fan' NULL AFTER `birthday`;
            )
    execute up_sql
  end

  def self.down
    remove_column :fans, :fan_type
  end
end
