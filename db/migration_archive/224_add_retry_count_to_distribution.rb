
class AddRetryCountToDistribution < ActiveRecord::Migration[4.2]
  
  def self.up
    add_column :distributions, :retry_count, :integer, :default => 0
    add_column :distributions, :max_retries, :integer, :default => 10
  end
  
  def self.down
    remove_column :distributions, :retry_count
    remove_column :distributions, :max_retries
  end
end


