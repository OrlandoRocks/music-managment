class IndexSalesRecords < ActiveRecord::Migration[4.2]
  def self.up
    add_index :reporting_months, [:report_date], :name => :reporting_month_date
    add_index :sales_records, [:reporting_month_id, :album_id], :name => :sales_records_month_and_album
    add_index :albums, [:artist_id], :name => :album_artist
  end

  def self.down
    remove_index :reporting_months, :name => :reporting_month_date
    remove_index :sales_records, :name => :sales_records_month_and_album
    remove_index :albums, :name => :album_artist
  end
end
