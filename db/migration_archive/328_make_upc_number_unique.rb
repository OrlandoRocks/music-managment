class MakeUpcNumberUnique < ActiveRecord::Migration[4.2]
  def self.up
    #remove_index :upcs, :number
    add_index :upcs, :number, :unique => true
  
  end

  def self.down
    remove_index :upcs, :number
    add_index :upcs, :number
  end
end
