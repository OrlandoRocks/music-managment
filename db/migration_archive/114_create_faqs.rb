class CreateFaqs < ActiveRecord::Migration[4.2]
  def self.up
    create_table :faqs do |t|
      t.column :anchor, :string, :null => false, :limit => 128
      t.column :title, :string, :null => false, :limit => 128
      t.column :position, :integer, :null => false
      t.column :depth, :integer, :null => false, :default => 0
      t.column :body, :text, :limit => 2.megabytes
      t.column :is_text, :boolean, :null => false, :default => true
      t.column :published, :boolean, :null => false, :default => false
      t.column :created_on, :datetime
      t.column :updated_at, :datetime
    end
    add_index :faqs, :anchor, :unique => true
    add_index :faqs, [:title, :position]    
  end

  def self.down
    drop_table :faqs
  end
end
