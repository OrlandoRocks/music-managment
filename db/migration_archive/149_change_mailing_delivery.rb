class ChangeMailingDelivery < ActiveRecord::Migration[4.2]
  def self.up
    add_column :scheduled_mailings, :worker_key, :string, :null => true
    add_column :scheduled_mailings, :deliveries, :integer, :null => false, :default => 0
    add_column :scheduled_mailings, :failures, :integer, :null => false, :default => 0
    #recipient lists unused
    drop_table :recipient_lists
    #rails_cron deprecated, switching to backgroundrb
    #BUT, rails cron was never installed by a proper migration so won't exist in fresh installs :(
    if ScheduledMailing.connection.respond_to?(:tables) and 
        ScheduledMailing.connection.tables.include? 'rails_crons'
      drop_table :rails_crons
    end
  end

  def self.down
    remove_column :scheduled_mailings, :worker_key
    remove_column :scheduled_mailings, :deliveries
    remove_column :scheduled_mailings, :failures

    create_table :recipient_lists do |t|
      t.column :mailing_id, :integer
      t.column :user_query_id, :integer
      t.column :scheduled_mailing_id, :integer
      t.column :recipients, :string
    end
    
    create_table "rails_crons", :force => true do |t|
      t.column "command",    :text
      t.column "start",      :integer
      t.column "finish",     :integer
      t.column "every",      :integer
      t.column "concurrent", :boolean
    end

  end
end
