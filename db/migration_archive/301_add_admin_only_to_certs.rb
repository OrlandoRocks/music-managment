class AddAdminOnlyToCerts < ActiveRecord::Migration[4.2]
  def self.up
    add_column :certs, :admin_only, :boolean, :null => false, :default => false
    add_column :certs, :admin_id, :integer
    #fix the example certs stored as yaml
    unless CertBatch.count(:conditions => "sample_cert_yaml like '%admin_only%'") > 0
      execute %Q!update cert_batches SET sample_cert_yaml = concat(sample_cert_yaml,"\n  admin_only: 0")!
    end
    execute %Q!update cert_batches set sample_cert_yaml = REPLACE(sample_cert_yaml,'\nnew_record','\n  new_record') where sample_cert_yaml like '%\nnew_record%'!
  end

  def self.down
    remove_column :certs, :admin_only
    remove_column :certs, :admin_id
  end
end
