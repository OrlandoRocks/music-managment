class MakeItunesUserReportsLockable < ActiveRecord::Migration[4.2]
  def self.up
    add_column :itunes_user_reports, :locked_by_id, :integer
    add_column :itunes_user_reports, :locked_by_type, :string, :limit => 30
  end

  def self.down
    remove_column :itunes_user_reports, :locked_by_id
    remove_column :itunes_user_reports, :locked_by_type
  end
end
