class AddTotalAmountToCerts < ActiveRecord::Migration[4.2]
  def self.up
    add_column :certs, :total_amount, :integer    
  end

  def self.down
    remove_column :certs, :short_name
  end
end
