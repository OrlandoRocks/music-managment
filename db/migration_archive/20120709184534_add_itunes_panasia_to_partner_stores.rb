class AddItunesPanasiaToPartnerStores < ActiveRecord::Migration[4.2]
  def self.up
    storeid = Store.find_by(name: "iTunes Asia").id
    Partner.all.each do |partner|
      execute("insert into partners_stores (partner_id,store_id) values (#{partner.id},#{storeid})")
    end
  end

  def self.down
    storeid = Store.find_by(name: "iTunes Asia").id
    execute("delete from partners_stores where store_id = #{storeid}")
  end
end
