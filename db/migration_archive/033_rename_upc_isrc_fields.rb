class RenameUpcIsrcFields < ActiveRecord::Migration[4.2]
  def self.up
    rename_column :albums, :upc_ean, :tunecore_upc
    rename_column :songs, :isrc, :tunecore_isrc
  end

  def self.down
    rename_column :songs, :tunecore_isrc, :isrc
    rename_column :albums, :tunecore_upc, :upc_ean
  end
end
