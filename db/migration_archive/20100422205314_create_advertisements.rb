class CreateAdvertisements < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `advertisements`(
                `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
                `name` varchar(75) NOT NULL COMMENT 'name of ad',
                `category` enum('Dashboard','Album','Single','Ringtone') DEFAULT 'Album' COMMENT 'determines which ad to select',
                `created_by_id` int(10) unsigned NOT NULL COMMENT 'id of administrator who created the offer',
                `thumbnail_title` varchar(75) NULL COMMENT 'ad title for thumbnail ad',
                `sidebar_title` varchar(75) NOT NULL COMMENT 'ad title for sidebar ad',
                `wide_title` varchar(75) NOT NULL COMMENT 'ad title for wide ad',
                `thumbnail_copy` varchar(255) NULL COMMENT 'ad copy for thumbnail ad',
                `sidebar_copy` varchar(255) NOT NULL COMMENT 'ad copy for sidebar ad',
                `wide_copy` varchar(255) NOT NULL COMMENT 'ad copy for wide ad',
                `display_price` decimal(8,2) NULL COMMENT 'display price for product being advertised',
                `display_price_modifier` varchar(10) NULL COMMENT 'textual modifier for the display_price as in - only 47.99',
                `display_price_notes` varchar(100) NULL COMMENT 'parenthetical text that further describes advertised product',
                `thumbnail_image` varchar(100) NULL COMMENT 'thumbnail image url',
                `sidebar_image` varchar(100) NULL COMMENT 'sidebar image url',
                `wide_image` varchar(100) NULL COMMENT 'wide image url',
                `image_caption` varchar(100) NULL COMMENT 'caption to display under selected image',
                `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active' COMMENT 'should this advertisement be displayed',
                `action_link` varchar(100) NOT NULL COMMENT 'url a customer follows when clicking on ad',
                `thumbnail_link_text` varchar(30) NOT NULL COMMENT 'text for the thumbnail link',
                `sidebar_link_text` varchar(30) NOT NULL COMMENT 'text for the sidebar link',
                `wide_link_text` varchar(20) NOT NULL COMMENT 'text to appear in button or link for action - wide',
                `sort_order` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'ad preference',
                `is_default` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'show as default ad',
                `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
                `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',
                PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
          
    execute up_sql
  end

  def self.down
    drop_table :advertisements
  end
end
