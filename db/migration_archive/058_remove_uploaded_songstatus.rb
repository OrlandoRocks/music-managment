class RemoveUploadedSongstatus < ActiveRecord::Migration[4.2]
  def self.up
    songs = Song.where(songstatus: 'uploaded')
    songs.each do |song|
      song.update_attribute(:songstatus,"paid")
    end
  end

  def self.down
    uploads = Upload.find_all
    uploads.each do |upload|
      Song.find(upload.song_id).update_attribute(:songstatus,"uploaded")
    end

  end
end
