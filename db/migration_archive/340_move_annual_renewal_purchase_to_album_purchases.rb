class MoveAnnualRenewalPurchaseToAlbumPurchases < ActiveRecord::Migration[4.2]
  def self.up
     purchases = Purchase.where("related_type = 'AnnualRenewal'")

      purchases.each do |p|
        ar = p.related

        AlbumPurchaseItem.create(:purchase_id => p.id, :purchaseable_id => ar.id, :purchaseable_type => 'AnnualRenewal', :created_at => p.created_at)

        p.sti_type = 'AlbumPurchase'
        p.related_id = ar.album_id
        p.related_type = 'Album'
        p.save!
      end
  end

  def self.down

  end
end
