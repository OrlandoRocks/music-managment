class RemoveLabelField < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :albums, :label
  end

  def self.down
    add_column :albums, :label, :string
  end
end

