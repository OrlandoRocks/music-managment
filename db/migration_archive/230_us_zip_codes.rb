class UsZipCodes < ActiveRecord::Migration[4.2]
  def self.up

    zip_codes_file = RAILS_ROOT + "/test/fixtures/files/full-zips.csv"
    raise "missing zips file" unless File.readable?(zip_codes_file)

    create_table :us_zip_codes, :id => false do |t|
      t.column :code, :string, :limit => 5, :null => false
      t.column :city, :string, :limit => 40, :null => false
      t.column :state, :string, :limit => 2, :null => false
    end

    FasterCSV.foreach(zip_codes_file) do |line|
      zip = line[0].to_s.strip.rjust(5,'0')
      city = line[1]
      state = line[2]
      puts "adding zip code '#{zip}', '#{city}', '#{state}' "
      UsZipCode.create(:code => zip, :city => city, :state => state)      
    end
    
    add_index :us_zip_codes, [:code, :city, :state]

  end
  
  def self.down
    drop_table :us_zip_codes
  end

end
