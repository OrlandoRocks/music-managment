class ConvertAccountsToStatusAttribute < ActiveRecord::Migration[4.2]
  def self.up
    update_fraud = "UPDATE people set `status`='Locked', status_updated_at=NOW(), lock_reason = 'Other' where email like '%fraud%';"

    update_no_rights = "UPDATE people set `status`='Locked', status_updated_at=NOW(), lock_reason = 'No Rights' where email like '%norights%';"

    add_notes = "INSERT INTO person_notes (person_id, note_created_by_id, subject, note, created_at, updated_at) SELECT id, 0, 'Locked', 'System wide conversion of fraud and no rights email signifier to account status', NOW(), NOW() from people WHERE lock_reason IN ('Other', 'No Rights');"

    execute(update_fraud)
    execute(update_no_rights)
    execute(add_notes)
  end

  def self.down
  end
end
