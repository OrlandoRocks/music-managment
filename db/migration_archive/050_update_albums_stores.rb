class UpdateAlbumsStores < ActiveRecord::Migration[4.2]
  # Migration to add support for has_many :through using the albums_stores
  # table.

  # For possible 'status' string definitions, see the documentation on
  # the AlbumsStores model.

  def self.up
    # Rename the join table 'albums_stores' to 'salepoints'
    # this no longer needs to be done as salepoints is in the model and older migrations
    # have been updated to reflect the new name for a clean build from scratch.

    # rename_table :albums_stores, :salepoints

    # Migrations can't do this without the raw SQL yet.  Bleh.
    execute "ALTER TABLE salepoints ADD id INT(11) NOT NULL AUTO_INCREMENT, ADD PRIMARY KEY (id);"

    add_column :salepoints, :status, :string, {:limit => 40, :null => false, :default => 'new'}

    Salepoint.reset_column_information

    # Find all the Albums that are NOT marked 'entered', 'songs', 'artwork',
    # or 'confirmed'.  Update their salepoints to 'complete'
    paid_albums = Album.where("album_state != 'entered' AND album_state != 'songs' AND album_state != 'artwork' AND album_state != 'confirmed'")
    paid_albums.each do |album|
      album.salepoints.find_all.each do |r|
        r.update_attribute(:status, 'complete')
      end
    end

  end

  def self.down
    remove_column :salepoints, :id
    remove_column :salepoints, :status
    rename_table :salepoints, :albums_stores
  end
end
