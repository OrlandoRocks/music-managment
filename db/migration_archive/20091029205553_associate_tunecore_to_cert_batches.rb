class AssociateTunecoreToCertBatches < ActiveRecord::Migration[4.2]
  def self.up
    tunecore_partners = Partner.where(domain: "tunecore.com")

    tunecore_partners.each do |tunecore|
      ActiveRecord::Base.connection.execute("
        INSERT INTO cert_batches_partners
        (partner_id, cert_batch_id) -- columns
        (
          SELECT #{tunecore.id}, id FROM cert_batches
        )
      ")
    end
  end

  def self.down
    tunecore_partners = Partner.where(domain: "tunecore.com")
    tunecore_partners.each do |tunecore|
      ActiveRecord::Base.connection.execute("
        DELETE FROM cert_batches_partners
        WHERE partner_id = #{tunecore.id}
      ")
    end

  end
end
