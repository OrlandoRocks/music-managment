class CreateOldPartners < ActiveRecord::Migration[4.2]
  def self.up
    create_table :partners do |t|
      t.column :name, :string
      t.column :email, :string
      t.column :api_key, :string
      t.column :role_id, :integer
    end
  end

  def self.down
    drop_table :partners
  end
end
