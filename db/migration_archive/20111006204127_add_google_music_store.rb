class AddGoogleMusicStore < ActiveRecord::Migration[4.2]
  def self.up
    Store.create(:name=>'Google Music',:abbrev=>'gm', :short_name=>'Google', :position=>27, :needs_rights_assignment=>false, :is_active=>false,:base_price_policy_id=>"0" )
    
  end

  def self.down
    execute("DELETE FROM stores where name = 'Google Music'")
  end
end
