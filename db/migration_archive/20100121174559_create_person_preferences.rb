class CreatePersonPreferences < ActiveRecord::Migration[4.2]
  def self.up
    # create_table :person_preferences do |t|
    #   t.integer :person_id
    #   t.boolean :pay_with_balance
    #   t.integer :preferred_credit_card_id
    # 
    #   t.timestamps
    # end
    
    up_sql = %Q(CREATE TABLE `person_preferences`(
                `id` INT(10) UNSIGNED not null auto_increment COMMENT 'Primary Key',
                `person_id` INT(10) UNSIGNED NOT NULL COMMENT 'Person foreign key',
                `pay_with_balance` TINYINT(3) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Pay renewals with balance',
                `preferred_credit_card_id` INT(10) UNSIGNED NULL COMMENT 'Foreign key to the stored_credit_card table',
                `updated_at` TIMESTAMP NULL COMMENT 'Datetime of latest update',
                `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',               
                PRIMARY KEY  (`id`),
                KEY `person_id` (`person_id`),
                KEY `preferred_credit_card_id` (`preferred_credit_card_id`),
                CONSTRAINT `FK_person` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
                
    execute up_sql
  end

  def self.down
    drop_table :person_preferences
  end
end
