class AlterTransactionIdToBeBigintForBraintree < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE braintree_transactions CHANGE transaction_id transaction_id BIGINT UNSIGNED NULL COMMENT 'References transaction ID stored at Braintree'")
    execute("ALTER TABLE eft_batch_transactions CHANGE transaction_id transaction_id BIGINT UNSIGNED NULL COMMENT 'References transaction ID stored at Braintree'")
  end

  def self.down
    execute("ALTER TABLE braintree_transactions CHANGE transaction_id transaction_id VARCHAR(35) DEFAULT NULL COMMENT 'References transaction ID stored at Braintree'")
    execute("ALTER TABLE eft_batch_transactions CHANGE transaction_id transaction_id VARCHAR(35) DEFAULT NULL COMMENT 'References transaction ID stored at Braintree'")
  end
end
