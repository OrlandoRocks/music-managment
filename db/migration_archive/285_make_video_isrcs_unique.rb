class MakeVideoIsrcsUnique < ActiveRecord::Migration[4.2]
  def self.up
    add_index :videos, :tunecore_isrc, :unique => true, :name => :unique_tc_isrc
    add_index :videos, :optional_isrc, :unique => true, :name => :unique_optional_isrc
  end

  def self.down
    remove_index :videos, :name => :unique_tc_isrc
    remove_index :videos, :name => :unique_optional_isrc
  end
end
