class AddDataForVariablePricesSalepointableType < ActiveRecord::Migration[4.2]

  def self.up
    transaction do
      create_variable_prices(ALBUM_LIST, 'Album')
      create_variable_prices(ALBUM_LIST, 'Single')
      create_variable_prices(SONG_LIST, 'Song')
      create_variable_prices(RINGTONE_LIST, 'Ringtone')
      backfill_variable_prices
    end
  end

  def self.down
    transaction do
      delete_variable_prices(ALBUM_LIST, 'Album')
      delete_variable_prices(ALBUM_LIST, 'Single')
      delete_variable_prices(SONG_LIST, 'Song')
      delete_variable_prices(RINGTONE_LIST, 'Ringtone')
    end
  end

  protected

  ALBUM_LIST = [
    14, 30, 5, 31, 1, 32, 2, 33, 3, 34, 40, 4
  ]

  SONG_LIST = [
    98, 99, 100
  ]

  RINGTONE_LIST = [
    3
  ]

  def self.delete_variable_prices(list, type)
    list.each do |price_code|
      variable_price = VariablePrice.find_by(price_code: price_code)

      if variable_price
        VariablePriceSalepointableType.where("variable_price_id = #{variable_price.id} AND salepointable_type = '#{type}'").delete_all
      end
    end
  end

  def self.create_variable_prices(list, type)
    list.each do |price_code|
      variable_price = VariablePrice.find_by(price_code: price_code)

      if variable_price
        VariablePriceSalepointableType.create(
          :variable_price_id => variable_price.id,
          :salepointable_type => type
        )
      end
    end
  end

  def self.backfill_variable_prices
    variable_prices = VariablePrice.where("store_id IS NOT NULL")
    puts variable_prices.inspect

    variable_prices.each do |variable_price|
      ["Single", "Album"].each do |type|
        puts "Inserting (#{type}, #{variable_price.id})"

        begin
          VariablePriceSalepointableType.create(
            :salepointable_type => type,
            :variable_price_id => variable_price.id
          )
        rescue
          # means that the value already exists
        end
      end
    end
  end
end
