class RemoveArtistIdFields < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :albums, :artist_id
    remove_column :songs, :artist_id
    remove_column :videos, :artist_id
  end

  def self.down
    add_column :albums, :artist_id, :integer
    add_column :songs, :artist_id, :integer
    add_column :videos, :artist_id, :integer
  end
end
