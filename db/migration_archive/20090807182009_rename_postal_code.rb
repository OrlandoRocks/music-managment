class RenamePostalCode < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :people, :postal_code
    add_column :people, :foreign_postal_code, :string, :limit => 32
  end

  def self.down
    remove_column :people, :foreign_postal_code
    add_column :people, :postal_code, :string, :limit => 32
  end
end
