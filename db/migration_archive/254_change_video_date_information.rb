class ChangeVideoDateInformation < ActiveRecord::Migration[4.2]
  def self.up
    rename_column(:videos, :available_date, :sale_date)
    remove_column(:videos, :first_release_date)
   
    
  end

  def self.down
    rename_column(:videos, :sale_date, :available_date)
    add_column(:videos, :first_release_date, :date)
  end
end
