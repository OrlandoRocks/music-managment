class AddPositionToPromotions < ActiveRecord::Migration[4.2]
  def self.up
    add_column :promotions, :position, :integer
    add_column :promotions, :is_deleted, :boolean, :default => false
  end

  def self.down
    remove_column :promotions, :position
    remove_column :promotions, :is_deleted
  end
end
