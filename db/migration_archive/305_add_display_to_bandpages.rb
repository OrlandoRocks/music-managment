class AddDisplayToBandpages < ActiveRecord::Migration[4.2]
  def self.up
    add_column :bandpages, :display, :boolean, :defaut => false
    add_index :bandpages, :display
  end

  def self.down
    remove_index :bandpages, :display
    remove_column :bandpages, :display
  end
end
