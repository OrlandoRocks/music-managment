class FixBadStatementDates < ActiveRecord::Migration[4.2]
  def self.up
    fixdate('statement_start_date', 6,2,5)
    fixdate('statement_start_date', 6,3,5)
    fixdate('statement_start_date', 6,3,1)

    fixdate('statement_end_date', 6,3,4)
    fixdate('statement_end_date', 6,4,1)
    fixdate('statement_end_date', 6,3,31)
  end

  def self.down
  end

  def self.fixdate(which, year, month, day)
    execute(
      %Q|update sales_records set 
        #{which} = '#{Date.civil(year + 2000, month, day).to_formatted_s(:db)}' 
        where #{which} = '#{Date.civil(year, month, day).to_formatted_s(:db)}'|
    )
  end
end
