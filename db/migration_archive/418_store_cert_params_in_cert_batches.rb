class StoreCertParamsInCertBatches < ActiveRecord::Migration[4.2]
  def self.up
    add_column :cert_batches, :expiry_date, :datetime
    add_column :cert_batches, :admin_only, :boolean, :default => false
    add_column :cert_batches, :cert_engine, :string, :limit => 50
    add_column :cert_batches, :engine_params, :string, :limit => 256
    add_column :cert_batches, :spawning_code, :string, :limit => 30
  end

  def self.down
    remove_column :cert_batches, :expiry_date
    remove_column :cert_batches, :admin_only
    remove_column :cert_batches, :cert_engine
    remove_column :cert_batches, :engine_params
    remove_column :cert_batches, :spawning_code
  end
end
