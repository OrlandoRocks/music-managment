class AlbumUpdatedAt < ActiveRecord::Migration[4.2]
  def self.up
      add_column :albums, :updated_at, :datetime
  end

  def self.down
    remove_column :albums, :updated_at
  end
end
