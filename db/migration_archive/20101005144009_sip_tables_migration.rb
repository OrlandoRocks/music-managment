class SipTablesMigration < ActiveRecord::Migration[4.2]
  def self.up
    # this migration is for show only. 
    # the change in table structure had to be performed in external scripts
    # because of the complex accompanying queries, multi-day deploy schedule and long running insert statements, the migration was performed outside of the
    # normal rails migration.
    # the schema for these tables was put directly into schema_from_production
    # CH - 2010/10/05
    # DCD - 2011/03/03 - Updating the schema
    
    # CREATE TABLE `sip_stores` (
    #   `id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
    #   `name` varchar(100) DEFAULT NULL,
    #   `sort_order` smallint(5) unsigned DEFAULT NULL,
    #   PRIMARY KEY (`id`)
    # ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    # 
    # CREATE TABLE person_intakes (
    #   id int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    #   person_id int(10) UNSIGNED NULL,
    #   amount decimal(23,14) NOT NULL DEFAULT 0.00,
    #   created_at timestamp NULL,
    #   PRIMARY KEY (id),
    #   KEY s_person_intakes_person_id_index (`person_id`),
    #   CONSTRAINT `FK_pi_person_id` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`)
    # ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    # 
    # CREATE TABLE sales_record_masters (
    #   id int(10) unsigned AUTO_INCREMENT,
    #   sip_store_id smallint(5) UNSIGNED NULL,
    #   status ENUM('new','processing','posted') NOT NULL DEFAULT 'new' COMMENT 'status of crediting money to the users account used by the posting stored procedure', 
    #   period_interval tinyint NOT NULL COMMENT 'number representing sales period month or quarter',
    #   period_year smallint(5) NOT NULL COMMENT 'year of sales period',
    #   period_type ENUM('monthly','quarterly') DEFAULT 'monthly' COMMENT  'is this a quarterly or monthly sales period',
    #   period_sort DATE DEFAULT NULL COMMENT 'first day of the sales period month. allows easier sorting',
    #   revenue_currency varchar(3) DEFAULT NULL COMMENT 'Currency the amount is denominated in',
    #   exchange_rate decimal(23,14) DEFAULT NULL COMMENT 'Exchange Rate',
    #   tariff decimal(23,14) DEFAULT NULL COMMENT 'tariff percentage',
    #   tariff_name varchar(50) DEFAULT NULL COMMENT 'tariff name used for display' ,
    #   country varchar(56) NOT NULL COMMENT 'Country of Sale in two character ISO code', #initially just copy field over from sales_records, then convert to two-character ISO code later -> char(2)
    #   reporting_month_id_temp int(10) UNSIGNED NOT NULL DEFAULT 0,
    #   exchange_number_temp bigint(19) DEFAULT NULL,
    #   exchange_rate_scale_temp int(11) DEFAULT NULL,
    #   created_date_temp DATE DEFAULT NULL,
    #   created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,  
    #   PRIMARY KEY (`id`),
    #   CONSTRAINT `FK_srm_sip_store_id` FOREIGN KEY (`sip_store_id`) REFERENCES `sip_stores` (`id`)
    # 
    # ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    # 
    # CREATE TABLE sales_records (
    #   id int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    #   person_id int(10) UNSIGNED NOT NULL COMMENT 'customer identifier',
    #   person_intake_id int(10) UNSIGNED DEFAULT NULL COMMENT 'related person_intake',
    #   sip_sales_record_id int(10) UNSIGNED DEFAULT NULL COMMENT 'pointer back to sip',
    #   sales_record_master_id int(10) UNSIGNED DEFAULT NULL,
    #   related_id int(10) NOT NULL COMMENT 'record identifier',
    #   related_type ENUM('Album','Song','Video') DEFAULT NULL COMMENT 'record type identifier',
    #   distribution_type enum('Download','Streaming') DEFAULT NULL COMMENT 'type of distribution - download or streaming',
    #   quantity INT(10) NOT NULL COMMENT 'total sales for this record',
    #   revenue_per_unit decimal(23,14) NOT NULL COMMENT 'revenue per unit',
    #   revenue_total decimal(23,14) NOT NULL COMMENT 'total revenue',
    #   amount decimal(23,14) DEFAULT NULL COMMENT 'USD Amount Entered',
    #   old_sales_record_id_temp int(10) UNSIGNED DEFAULT NULL COMMENT 'used in migration, to be dropped after.',
    #   PRIMARY KEY (`id`),
    #   KEY s_ix_sales_record_master (`sales_record_master_id`),
    #   KEY s_ix_sip_sales_record_id (`sip_sales_record_id`),
    #   KEY s_ix_sales_records_person (`person_id`),
    #   KEY s_ix_sales_records_person_intakes (`person_intake_id`),
    #   KEY s_ix_sales_records_related (`related_id`, `related_type`),
    #   CONSTRAINT `FK_sr_people_id` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`),
    #   CONSTRAINT `FK_sr_sales_record_master_id` FOREIGN KEY (`sales_record_master_id`) REFERENCES `sales_record_masters` (`id`) #,
    #   #CONSTRAINT `FK_sr_person_intake_id` FOREIGN KEY (`person_intake_id`) REFERENCES `person_intakes` (`id`)
    # ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    # 
    # CREATE TABLE person_transactions (
    #   id int(11) NOT NULL AUTO_INCREMENT,
    #   created_at timestamp NULL,
    #   person_id int(10) UNSIGNED NOT NULL,
    #   debit decimal(23,14) NOT NULL DEFAULT 0.00,
    #   credit decimal(23,14) NOT NULL DEFAULT 0.00,
    #   previous_balance decimal(23,14) NOT NULL DEFAULT 0.00,
    #   target_id int(10) unsigned DEFAULT NULL,
    #   target_type enum('BalanceAdjustment','BatchTransaction','CheckTransfer','EftBatchTransaction','Invoice','PaypalIpn','PaypalTransfer','Person','PersonIntake') DEFAULT NULL,
    #   comment varchar(255) NULL,
    #   PRIMARY KEY (`id`),
    #   KEY person_transactions_target_index (`target_type`,`target_id`),
    #   KEY person_transactions_person_id_index (`person_id`),
    #   KEY person_transaction_date (`created_at`),
    #   CONSTRAINT `FK_pt_people_id` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`)
    # ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    # 
    # # Not putting a foreign key constraint on people_id since there are 2 person_balances that do not have people associated with them
    # CREATE TABLE person_balances (
    #   id int(10) unsigned NOT NULL AUTO_INCREMENT,
    #   person_id int(10) UNSIGNED NOT NULL,
    #   balance decimal(23,14) NOT NULL DEFAULT 0.00,
    #   updated_at timestamp NOT NULL,
    #   PRIMARY KEY (id),
    #   KEY people_person_id_index (`person_id`)
    # ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    # 
    # ALTER TABLE sales_records ADD CONSTRAINT `FK_sales_records_person_intake_id` FOREIGN KEY (`person_intake_id`) REFERENCES `person_intakes`(`id`);
    
  end

  def self.down
  end
end
