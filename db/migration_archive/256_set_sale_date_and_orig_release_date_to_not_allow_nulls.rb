class SetSaleDateAndOrigReleaseDateToNotAllowNulls < ActiveRecord::Migration[4.2]
  def self.up
    execute "update albums set albums.sale_date = albums.orig_release_year where albums.sale_date is null"
    execute "ALTER TABLE albums CHANGE sale_date sale_date date NOT NULL;"
    execute "ALTER TABLE albums CHANGE orig_release_year orig_release_year date NOT NULL;"
    execute "ALTER TABLE videos CHANGE sale_date sale_date date NOT NULL;"
    execute "ALTER TABLE videos CHANGE orig_release_year orig_release_year date NOT NULL;"
  end

  def self.down
    execute "ALTER TABLE albums CHANGE sale_date sale_date date NULL;"
    execute "ALTER TABLE albums CHANGE orig_release_year orig_release_year date NULL;"
    execute "ALTER TABLE videos CHANGE sale_date sale_date date NULL;"
    execute "ALTER TABLE videos CHANGE orig_release_year orig_release_year date NULL;"
  end
end
