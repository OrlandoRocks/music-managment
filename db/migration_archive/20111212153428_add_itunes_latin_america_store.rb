class AddItunesLatinAmericaStore < ActiveRecord::Migration[4.2]
  def self.up
    Store.create(:name=>'iTunes Latin America (incl. Brazil)',:abbrev=>'la', :short_name=>'iTunesLA', :position=>28, :needs_rights_assignment=>false, :is_active=>true,:base_price_policy_id=>"3" )
  end

  def self.down
    execute("DELETE FROM stores where name = 'iTunes Latin America (incl. Brazil)'")
  end
end
