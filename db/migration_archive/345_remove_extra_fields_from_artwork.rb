class RemoveExtraFieldsFromArtwork < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :artworks, :status
    remove_column :artworks, :file
    add_column :artworks, :auto_generated, :boolean, :default => false
  end

  def self.down
    add_column :artworks, :status, :string 
    add_column :artworks, :file, :string
    remove_column :artworks, :auto_generated
  end
end
