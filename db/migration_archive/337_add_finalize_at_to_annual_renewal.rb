class AddFinalizeAtToAnnualRenewal < ActiveRecord::Migration[4.2]
  def self.up
    add_column :annual_renewals, :finalized_at, :datetime, :default => nil
  end

  def self.down
    remove_column :annual_renewals, :finalized_at
  end
end
