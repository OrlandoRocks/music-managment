class AddLockableToAnnualRenewals < ActiveRecord::Migration[4.2]
  def self.up
    add_column :annual_renewals, :locked_by_id, :integer, :null => true, :default => nil
    add_column :annual_renewals, :locked_by_type, :string, :null => true, :default => nil, :limit => 30
    add_column :annual_renewals, :person_id, :integer
    
  end

  def self.down
    remove_column :annual_renewals, :locked_by_id
    remove_column :annual_renewals, :locked_by_type
    remove_column :annual_renewals, :person_id
  end
end
