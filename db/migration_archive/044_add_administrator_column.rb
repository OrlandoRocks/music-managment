class AddAdministratorColumn < ActiveRecord::Migration[4.2]
  def self.up
    add_column :people, :is_administrator, :boolean, :default => false, :null => false
    
    # this is done for specific people on production, and should not be used on a new clean
    # development dbase.  Edit to make  the desired person IDs into administrators.
    
    #Person.find(1,8,13).each do |person|
    #  person.update_attribute(:is_administrator, true)
    #end
  end

  def self.down
    remove_column :people, :is_administrator
  end
end
