class RenameProductsTable < ActiveRecord::Migration[4.2]
  def self.up
    execute "RENAME TABLE products TO promotional_products;"
  end

  def self.down
    execute "RENAME TABLE promotional_products TO products;"    
  end
end
