class AddPolymorphicSalepoints < ActiveRecord::Migration[4.2]
  def self.up
    add_column :salepoints, :salepointable_type, :string
    rename_column :salepoints, :album_id, :salepointable_id
    execute "UPDATE salepoints set salepointable_type = 'Album' "
  end

  # You will loose video salepoints!
  def self.down
    execute "DELETE FROM salepoints WHERE salepointable_type != 'Album' "
    rename_column :salepoints, :salepointable_id, :album_id
    remove_column :salepoints, :salepointable_type
  end
end
