class AddDuplicatedIsrcs < ActiveRecord::Migration[4.2]
  def self.up
    create_table :duplicated_isrcs do |t|
      t.column :created_at, :datetime, :null => false, :default => nil
      t.column :song_id, :integer, :null => false, :default => nil
      t.column :tunecore_isrc, :string, :limit => 12, :null => false, :default => nil
    end
    add_index :duplicated_isrcs, :song_id
    add_index :duplicated_isrcs, :tunecore_isrc
  end

  def self.down
    drop_table :duplicated_isrcs
  end
end
