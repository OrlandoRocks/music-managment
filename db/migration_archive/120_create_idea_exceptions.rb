class CreateIdeaExceptions < ActiveRecord::Migration[4.2]
  def self.up
    create_table :idea_exceptions do |t|
      t.column :created_on, :date
      t.column :album_id, :integer, :null => false
      t.column :description, :text, :null => false
    end
  end

  def self.down
    drop_table :idea_exceptions
  end
end
