class MoreBettererIndexing < ActiveRecord::Migration[4.2]
  def self.up
    add_index :people, :email
    add_index :videos, :person_id
    add_index :videos, :label_id
    add_index :videos, :album_id
    add_index :videos, :artist_id
    add_index :labels, :name
    add_index :artists, :name
    add_index :certs, :cert
  end

  def self.down
    remove_index :people, :email
    remove_index :videos, :person_id
    remove_index :videos, :label_id
    remove_index :videos, :album_id
    remove_index :videos, :artist_id
    remove_index :labels, :name
    remove_index :artists, :name
    remove_index :certs, :cert
  end
end
