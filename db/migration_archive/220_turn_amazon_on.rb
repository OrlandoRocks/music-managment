class TurnAmazonOn < ActiveRecord::Migration[4.2]
  def self.up
    amazon = Store.find_by(short_name: "Amazon")
    amazon.name = "Amazon MP3"
    amazon.is_active = true
    amazon.save
  end

  def self.down
    amazon = Store.find_by(short_name: "Amazon")
    amazon.name = "Amazon"
    amazon.is_active = false
    amazon.save
  end
end
