class RemovePricePolicyIdDependencies < ActiveRecord::Migration[4.2]
  def self.up
    execute "ALTER TABLE albums MODIFY COLUMN price_policy_id int(10) UNSIGNED DEFAULT 0"
    execute "ALTER TABLE annual_renewals MODIFY COLUMN price_policy_id int(10) UNSIGNED DEFAULT 0"
    execute "ALTER TABLE booklets MODIFY COLUMN price_policy_id int(10) UNSIGNED DEFAULT 0"
    execute "ALTER TABLE itunes_user_reports MODIFY COLUMN price_policy_id int(10) UNSIGNED DEFAULT 0"
    execute "ALTER TABLE salepoints MODIFY COLUMN price_policy_id int(10) UNSIGNED DEFAULT 0"
    execute "ALTER TABLE shipping_labels MODIFY COLUMN price_policy_id int(10) UNSIGNED DEFAULT 0"
    execute "ALTER TABLE songs MODIFY COLUMN price_policy_id int(10) UNSIGNED DEFAULT 0"
    execute "ALTER TABLE videos MODIFY COLUMN price_policy_id int(10) UNSIGNED DEFAULT 0"
  end

  def self.down
  end
end
