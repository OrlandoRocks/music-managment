class AddVariablePricingToSalepoints < ActiveRecord::Migration[4.2]
  def self.up
    add_column :salepoints, :variable_price_id, :integer
  end

  def self.down
    remove_column :salepoints, :variable_price_id
  end
end
