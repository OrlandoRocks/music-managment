class AddNextRenewalDueOn < ActiveRecord::Migration[4.2]
  def self.up
    add_column :albums, :renewal_due_on, :date, :default => nil
    add_index :albums, :renewal_due_on
  end

  def self.down
    remove_column :albums, :renewal_due_on
    #don't need to drop the index as the column is deleted.
  end
end
