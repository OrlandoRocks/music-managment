class IndexSongsIsrcs < ActiveRecord::Migration[4.2]
  def self.up
    add_index :songs, :tunecore_isrc
    change_column :songs, :optional_isrc, :string, :limit => 12
    add_index :songs, :optional_isrc
  end

  def self.down
    remove_index :songs, :tunecore_isrc
    remove_index :songs, :optional_isrc
    change_column :songs, :optional_isrc, :text
  end
end
