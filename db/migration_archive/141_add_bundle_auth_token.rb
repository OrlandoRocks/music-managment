
class AddBundleAuthToken < ActiveRecord::Migration[4.2]
  def self.up
    add_column :bundles, :update_token, :string, :limit => 50, :default => "N/A"
  end

  def self.down
    remove_column :bundles, :update_token
  end
end
