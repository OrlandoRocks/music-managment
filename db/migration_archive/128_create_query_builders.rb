class CreateQueryBuilders < ActiveRecord::Migration[4.2]
  def self.up
    create_table :query_builders do |t|
      t.column :user_query_id, :integer
      t.column :column_name, :string
      t.column :operator, :string
      t.column :column_value, :string
    end
  end

  def self.down
    drop_table :query_builders
  end
end
