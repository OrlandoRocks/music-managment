class AddDescriptionFieldsToRoles < ActiveRecord::Migration[4.2]
  def self.up
    upstring = %Q(
      ALTER TABLE roles
        ADD COLUMN long_name varchar(100) NULL,
        ADD COLUMN description varchar(255) NULL,
        ADD COLUMN is_administrative SMALLINT(1) NOT NULL DEFAULT 0)
    
    execute upstring
  end

  def self.down
    downstring = %Q(
      ALTER TABLE roles
        DROP COLUMN long_name,
        DROP COLUMN description,
        DROP COLUMN is_administrative)
    
    execute downstring
  end
end
