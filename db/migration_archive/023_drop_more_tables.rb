class DropMoreTables < ActiveRecord::Migration[4.2]
  def self.up
    drop_table "line_items"
    drop_table "orders"
  end

  def self.down
  end
end
