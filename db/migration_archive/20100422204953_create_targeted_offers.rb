class CreateTargetedOffers < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `targeted_offers` (
              `id` int(10) unsigned NOT NULL auto_increment COMMENT 'Primary Key',
              `name` varchar(75) NOT NULL COMMENT 'name of targeted offer',
              `created_by_id` int(10) unsigned NOT NULL COMMENT 'id of administrator who created the offer',
              `status` enum('Active','Inactive') NOT NULL default 'Active' COMMENT 'active or inactive status for targeted offer',
              `offer_type` enum('new','existing') NOT NULL default 'existing' COMMENT 'does this offer target new or existing customers',
              `start_date` timestamp NOT NULL default '0000-00-00 00:00:00' COMMENT 'start date for the targeted offer',
              `expiration_date` timestamp NOT NULL default '0000-00-00 00:00:00' COMMENT 'expiration date of targeted offer',
              `date_constraint` enum('expiration','join plus','permanent') NOT NULL default 'expiration' COMMENT 'when does this offer expire for members in the targeted population',
              `join_plus_duration` smallint(6) default NULL COMMENT 'number of intervals to use when calculating the expiration of an offer for targeted customers (e.g. 10 days)',
              `join_token` varchar(50) default NULL COMMENT 'url token to match a new customer to a targeted offer',
              `country` varchar(20) NULL COMMENT 'population criteria field for selecting an existing population',
              `zip` varchar(5) NULL COMMENT 'population criteria field for selecting an existing population',
              `created_on_start` date NULL COMMENT 'population criteria field for selecting an existing population',
              `created_on_end` date NULL COMMENT 'population criteria field for selecting an existing population',
              `referral` varchar(20) NULL COMMENT 'population criteria field for selecting an existing population',
              `partner_id` smallint NULL COMMENT 'population criteria field for selecting an existing population',
              `has_never_made_a_purchase` smallint(1) COMMENT 'population criteria field for selecting an existing population',
              `last_purchase_made_start` date NULL COMMENT 'population criteria field for selecting an existing population',
              `last_purchase_made_end` date NULL COMMENT 'population criteria field for selecting an existing population',
              `population_criteria_count` int(10) unsigned default NULL COMMENT 'total count of customers found using population_criteria',
              `targeted_population_count` int(10) unsigned default NULL COMMENT 'cached total count of customers targeted with this offer',
              `population_cap` int(10) unsigned default NULL COMMENT 'total population for a targeted_offer with an offer_type of `new`',
              `product_show_type` enum('price_override','show_only_selected') default 'price_override' COMMENT 'how does this targeted offer display products',
              `updated_at` timestamp NULL default NULL COMMENT 'Datetime of latest update',
              `created_at` timestamp NULL default CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',
              PRIMARY KEY  (`id`),
              KEY `join_token` (`join_token`),
              KEY `date_constraint` (`date_constraint`),
              KEY `expiration_date` (`expiration_date`),
              KEY `start_date` (`start_date`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
            
            
    execute up_sql
  end

  def self.down
    drop_table :targeted_offers
  end
end
