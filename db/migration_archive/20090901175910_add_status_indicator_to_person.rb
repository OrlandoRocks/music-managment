class AddStatusIndicatorToPerson < ActiveRecord::Migration[4.2]
  def self.up
    upstring = %Q(
      ALTER TABLE people
        ADD COLUMN `status` ENUM('Active','Inactive','Locked','Suspicious') NOT NULL DEFAULT 'Active' COMMENT 'Status of user, inactive prevents the user from logging in',
        ADD COLUMN `status_updated_at` datetime NULL COMMENT 'Date and time user status was changed',
        ADD COLUMN `lock_reason` ENUM('Credit Card Fraud', 'PayPal Fraud', 'No Rights', 'Fraudulent Sales', 'Spam', 'Other') NULL)
    execute upstring
  end

  def self.down
    downstring = %Q(
      ALTER TABLE people
        DROP COLUMN `status`,
        DROP COLUMN `status_updated_at`,
        DROP COLUMN `inactive_reason`)
    execute downstring
  end
end
