class StoreAbbreviationLength < ActiveRecord::Migration[4.2]
  def self.up
    change_column :stores, :abbrev, :string, :limit => 6
  end

  def self.down
    change_column :stores, :abbrev, :string, :limit => 2
  end
end
