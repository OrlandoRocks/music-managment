class AddAddAlbumIndexForArtworks < ActiveRecord::Migration[4.2]
  def self.up
    add_index :artworks, :album_id
  end

  def self.down
    remove_index :artworks, :album_id
  end
end
