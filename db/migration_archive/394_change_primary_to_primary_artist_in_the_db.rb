class ChangePrimaryToPrimaryArtistInTheDb < ActiveRecord::Migration[4.2]
  def self.up
    execute 'update creatives set role = "primary_artist" where role = "primary"'
    execute 'update creatives set role = "featured_artist" where role = "featured"'
  end

  def self.down
    execute 'update creatives set role = "primary" where role = "primary_artist"'
    execute 'update creatives set role = "featured" where role = "featured_artist"'
  end
end
