class AddUsedColumnToTargetedPeople < ActiveRecord::Migration[4.2]
  def self.up
    execute "ALTER TABLE targeted_people ADD COLUMN used TINYINT(1) DEFAULT 0 AFTER person_id"
    execute "ALTER TABLE targeted_people ADD INDEX `targeted_people_used` (`used`)"
  end

  def self.down
   execute "ALTER TABLE targeted_people DROP COLUMN used"
  end
end
