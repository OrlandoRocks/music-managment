class AddTitleFormatFlagToAlbum < ActiveRecord::Migration[4.2]
  def self.up
    add_column :albums, :allow_different_format, :boolean, :default => false   
  end

  def self.down
    remove_column :albums, :allow_different_format
  end
end
