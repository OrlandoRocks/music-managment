class UpdatePartnerTableDomains < ActiveRecord::Migration[4.2]
  def self.up
    partners = Partner.all
    partners.each do |partner|
      if partner.skin != "tunecore_canada"
        partner.domain = partner.domain.split("\.").first
      else
        partner.domain = "ca"
      end
      partner.save
    end

  end

  def self.down
    partners = Partner.all
    partners.each do |partner|
      if partner.skin != "tunecore_canada"
        partner.domain = partner.domain + ".com"
      else
        partner.domain = "ca.tunecore.com"
      end
      partner.save
    end
  end
end
