class AddResolutionDisplay < ActiveRecord::Migration[4.2]
  def self.up
    add_column :data_records, :resolution_display, :string, :limit => 30
  end

  def self.down
    remove_column :data_records, :resolution_display
  end
end
