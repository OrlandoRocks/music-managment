class AddItunesUniqueIdentifier < ActiveRecord::Migration[4.2]
  def self.up
    add_column :albums, :apple_identifier, :string, :limit => 30
    add_index :albums, :apple_identifier, :unique => true
  end

  def self.down
    remove_index :albums, :apple_identifier
    remove_column :albums, :apple_identifier
  end
end
