class CreateOauthProvidersPeople < ActiveRecord::Migration[4.2]
  def self.up
    up_sql=%Q(CREATE TABLE `oauth_providers_people` (
              `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
              `person_id` INT(10) UNSIGNED NOT NULL COMMENT 'Foreign key to people table.',
              `oauth_provider_id` SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Foreign key to oauth_providers table.',
              `access_token` VARCHAR(30) NOT NULL COMMENT 'Access token.',
              `access_token_secret` VARCHAR(50) NOT NULL COMMENT 'Access token secret.',
              `is_active` ENUM('Y','N') NOT NULL DEFAULT 'N' COMMENT 'Indicates if tocken is active.',
              `date_added` TIMESTAMP NULL DEFAULT NULL COMMENT 'Date record was created.',
              `date_updated` TIMESTAMP NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date last updated.',
              PRIMARY KEY (`id`),
              KEY `oauth_provider_id` (`oauth_provider_id`),
              KEY `person_id` (`person_id`),
              CONSTRAINT `FK_oauth_providers_person` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`),
              CONSTRAINT `FK_oauth_providers_people` FOREIGN KEY (`oauth_provider_id`) REFERENCES `oauth_providers` (`id`)
            ) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='Links a person account to oauth_provider';)
    execute up_sql
  end

  def self.down
    drop_table :oauth_providers_people
  end
end
