class CreateEftBatches < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `eft_batches`(
                `id` int UNSIGNED not null auto_increment COMMENT 'Primary KEy',
                `total_amount` decimal(10,2) not null default 0.00 COMMENT 'Total Amount in Batch',
                `errored_amount` decimal(10,2) not null default 0.00 COMMENT 'Total Amount for transactions that error out on the initial sent to braintree',
                `sent_to_bank_amount` decimal(10,2) not null default 0.00 COMMENT 'Total Amount for BrainTree in Batch',
                `batch_file_name` varchar(200) null COMMENT 'Name of physical csv file saved on server, sent to braintree',
                `batch_sent_at` timestamp null COMMENT 'Datetime file sent to braintree',
                `response_processed_at` DATETIME null COMMENT 'Datetime file received back from braintree', 
                `response_file_name` varchar(200) null COMMENT 'Name of physical csv file saved on server, received from Braintree',
                `status` ENUM('complete','uploaded','new','upload_confirmed') NOT NULL DEFAULT 'new' COMMENT 'Current State',
                `updated_at` TIMESTAMP NULL COMMENT 'Datetime of latest update',
                `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',               
                PRIMARY KEY  (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
                
    execute up_sql
  end

  def self.down
    drop_table :eft_batches
  end
end
