class UserToPerson < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :albums, :user_id
    add_column :albums, :person_id, :integer
    change_column :people, :artist_id, :integer
  end

  def self.down
    remove_column :albums, :person_id
    add_column :albums, :user_id, :integer
    change_column :people, :artist_id, :string, :limit => 6
  end
end
