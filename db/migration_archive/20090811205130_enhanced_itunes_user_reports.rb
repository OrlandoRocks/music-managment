class EnhancedItunesUserReports < ActiveRecord::Migration[4.2]
  def self.up
    transaction do
      create_table :enhanced_itunes_user_reports do |t|
        t.integer :itunes_user_report_id, :null => false
        t.integer :person_id, :null => false
        t.timestamps
      end

      create_table :enhanced_itunes_user_report_downloads do |t|
        t.integer :enhanced_itunes_user_report_id, :null => false
        t.integer :person_id, :null => false
        t.boolean :is_email_sent, :null => false, :default => false
        t.timestamps
      end

      # Setup Foreign Keys
      execute("
        ALTER TABLE enhanced_itunes_user_reports
        ADD FOREIGN KEY fk_enhanced_itunes_user_report_id (itunes_user_report_id)
        REFERENCES itunes_user_reports(id)
        ON DELETE CASCADE
      ")
      execute("
        ALTER TABLE enhanced_itunes_user_report_downloads
        ADD FOREIGN KEY fk_enhanced_itunes_user_report_download_id (enhanced_itunes_user_report_id)
        REFERENCES enhanced_itunes_user_reports(id)
        ON DELETE CASCADE
      ")
    end
  end

  def self.down
    transaction do
      drop_table :enhanced_itunes_user_report_downloads
      drop_table :enhanced_itunes_user_reports
    end
  end
end
