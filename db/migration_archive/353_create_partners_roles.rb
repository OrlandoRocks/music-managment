class CreatePartnersRoles < ActiveRecord::Migration[4.2]
  def self.up
    # Create join table table
    create_table :partners_roles, :id => false do |t|
      t.column :partner_id, :integer
      t.column :role_id, :integer
    end
  end

  def self.down
    drop_table :partners_roles
  end
end
