class VariablePricesDataMigration < ActiveRecord::Migration[4.2]

  def self.up
    transaction do
      itunes_variable_prices
      variable_price_stores
      store_groups
      default_variable_prices
    end
  end

  def self.down
    transaction do
      execute("DELETE FROM default_variable_prices;")
      execute("DELETE FROM variable_prices_stores;")
      execute("DELETE FROM store_group_stores;")
      execute("DELETE FROM store_groups;")
      execute("DELETE FROM variable_prices WHERE store_id IS NULL")
    end
  end

  protected

  def self.itunes_variable_prices
    #
    #  Variable Prices Data Migration
    #
    say_with_time("
      Moving data from Variable Prices to
      Variable Prices Stores Many-to-Many
      while leaving existing data in
      the VariablePrices table
    ") do

      prices = VariablePrice.all
      prices.each do |price|
        VariablePriceStore.create!(
          :store_id => price.store_id,
          :variable_price_id => price.id,
          :is_active => price.active,
          :position => price.position
        )
      end
    end
  end

  def self.variable_price_stores
    say_with_time("
      Populate iTunes Variable Price Points
    ") do

      # Find all iTunes Stores
      stores = Store.where("name LIKE '%iTunes%'")

      # setup the options
      price_codes = {
        14 => "Digital 45", 30 => "Mini EP", 5 => "EP", 31 => "Mini Album", 1 => "Budget One",
        32 => "Budget Two", 2 => "Back", 33 => "Mid", 3 => "Mid/Front", 40 => "Front Two",
        34 => "Front One",  4 => "Front/Plus", 100 => "Front", 98 => "Back", 99 => "Mid", 100 => "Front"
      }

      # Create Variable Prices
      variable_prices = price_codes.map do |price_code|
        VariablePrice.create!(
          :price_code => price_code[0],
          :price_code_display => price_code[1]
        )
      end

      # Create Many-to-Many Record
      stores.each do |store|
        variable_prices.each do |variable_price|
          VariablePriceStore.create!(
            :store_id => store.id,
            :variable_price_id => variable_price.id,
            :is_active => true,
            :position => variable_price.id
          )
        end
      end
    end
  end

  def self.store_groups
    say_with_time("Store Group creation and Many-to-Many association.") do

      StoreGroup.create!(:name => "iTunes", :key => "itunes") do |store_group|
        store_group.stores = Store.where("name LIKE '%iTunes%'")
      end

    end
  end

  def self.default_variable_prices
    say_with_time("Create Default Variable Price for Stores") do

      price_code = "3"
      stores = Store.where("name LIKE 'iTunes%'")

      stores.each do |store|

        variable_price_store = VariablePriceStore.includes(:variable_price).where("variable_prices_stores.store_id = ? AND variable_prices.price_code = ?", store.id, price_code).first

        DefaultVariablePrice.create!(
          :variable_price_store_id => variable_price_store.id,
          :store_id => store.id
        )
      end

    end
  end

end
