class AddEmusicAndSony < ActiveRecord::Migration[4.2]
  def self.up
    stores = [["eMusic","em","eMusic"],["Sony Connect","sc","Connect"]]
                
    stores.each { |store|
      Store.create :name=>store[0],:abbrev=>store[1],:short_name=>store[2]
    }

    add_column :albums, :emusic_check,  :boolean, :default => false
    
    Store.update_all('position = id')
  end
  

  def self.down
    Store.destroy(10)
    Store.destroy(11)
    remove_column :albums, :sony_check
  end
end
