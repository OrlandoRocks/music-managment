class ChangeAlbumNameToVarchar < ActiveRecord::Migration[4.2]
  def self.up
    change_column :albums, :name, :string, :limit => 129
  end

  def self.down
    change_column :albums, :name, :text
  end
end
