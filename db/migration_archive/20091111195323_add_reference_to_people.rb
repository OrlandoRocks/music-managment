class AddReferenceToPeople < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE partners_people ADD FOREIGN KEY (person_id) REFERENCES people (id)")
  end

  def self.down
    execute("ALTER TABLE partners_people DROP FOREIGN KEY partners_people_ibfk_1") 
  end
end
