class AddUrlSalepointAudits < ActiveRecord::Migration[4.2]
def self.up
    add_column :salepoint_audits, :url, :string
  end

  def self.down
    remove_column :salepoint_audits, :url
  end
end
