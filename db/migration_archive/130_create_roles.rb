class CreateRoles < ActiveRecord::Migration[4.2]
  def self.up

    # Create roles table
    create_table :roles do |t|
      t.column :name, :string
    end

    # populate roles
    Role.create( :name => "Admin" ) # should be id 1
    Role.create( :name => "Affiliate" ) # id 2
    Role.create( :name => "Customer") # id 3

    # Create join table table
    create_table :people_roles, :id => false do |t|
      t.column :person_id, :integer
      t.column :role_id, :integer
    end
    Role.reset_column_information

    ## Populate the join table
    # Every person so far will be a customer
    puts "Assigning user roles ..."
    #people = Person.find(:all)
    #people.each {|p| p.roles << Role.find_by_name("Customer") }
    execute("INSERT INTO people_roles ( SELECT id, '3' FROM people)")

    # People with the admin flag get added to the admin role
    admins = Person.where("is_administrator = '1'")
    admins.each {|p| p.roles << Role.find_by(name: "Admin") }

  end

  def self.down
    drop_table :roles
    drop_table :people_roles
  end
end
