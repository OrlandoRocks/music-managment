class CreateBraintreeVaultTransactions < ActiveRecord::Migration[4.2]
  def self.up
    upstring = %Q(
      CREATE TABLE  `braintree_vault_transactions` (
        `id` int(10) UNSIGNED NOT NULL auto_increment,
        `person_id` int(11) NOT NULL COMMENT 'References people table',
        `stored_credit_card_id` int(10) UNSIGNED COMMENT 'References stored_credit_cards table',
        `response_code` char(4) default NULL COMMENT 'Braintree set response_code',
        `ip_address` char(15) NOT NULL,
        `raw_response` text COMMENT 'Original response from Braintree', 
        `created_at` datetime default NULL,
        `updated_at` datetime default NULL,
        PRIMARY KEY  (`id`),
        KEY `person_id` (`person_id`),
        KEY `stored_credit_card_id` (`stored_credit_card_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8)

      execute upstring
  end

  def self.down
    drop_table :braintree_vault_transactions
  end
end
