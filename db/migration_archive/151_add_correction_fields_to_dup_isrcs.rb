class AddCorrectionFieldsToDupIsrcs < ActiveRecord::Migration[4.2]
  def self.up
    create_table :apple_identifiers do |t|
      t.column :created_at, :datetime, :null => false, :default => nil
      t.column :duplicated_isrc_id, :integer, :null => false, :default => nil
      t.column :apple_id, :string, :limit => 50, :null => true, :default => nil
    end
    
    add_index :apple_identifiers, :duplicated_isrc_id
    add_index :apple_identifiers, :apple_id
    
    add_column :duplicated_isrcs, :corrected_at, :datetime, :null => true, :deafult => nil
    add_column :duplicated_isrcs, :adjusted_isrc, :string, :limit => 12, :null => true, :default => nil
    add_column :duplicated_isrcs, :tunecore_upc, :string, :limit => 32, :null => true, :default => nil
    add_column :duplicated_isrcs, :optional_upc, :string, :limit => 32, :null => true, :default => nil
  end

  def self.down
    drop_table :apple_identifiers
    remove_column :duplicated_isrcs, :corrected_at
    remove_column :duplicated_isrcs, :adjusted_isrc
    remove_column :duplicated_isrcs, :tunecore_upc
    remove_column :duplicated_isrcs, :optional_upc
  end
end
