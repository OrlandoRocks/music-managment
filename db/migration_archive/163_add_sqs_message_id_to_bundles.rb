class AddSqsMessageIdToBundles < ActiveRecord::Migration[4.2]

  def self.up
    add_column :bundles, :sqs_message_id, :string
  end
  
  def self.down
    remove_column :bundles, :sqs_message_id
  end
end
