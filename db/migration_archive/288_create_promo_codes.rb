class CreatePromoCodes < ActiveRecord::Migration[4.2]
  def self.up
    create_table :promo_codes do |t|
      t.column :promo, :string, :limit => 12, :null => false
      t.column :code, :string, :limit => 18, :null => false
      t.column :used, :boolean, :null => false, :default => false
      t.column :created_on, :date
      t.column :used_on, :date
    end
    add_index :promo_codes, [:used]
  end

  def self.down
    drop_table :promo_codes
  end
end
