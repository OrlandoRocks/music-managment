class AddVipFlag < ActiveRecord::Migration[4.2]
  def self.up
    add_column :people, :vip, :boolean, :default => false
  end

  def self.down
    remove_column :people, :vip
  end
end
