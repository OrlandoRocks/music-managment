class AddSalesReportingIndices < ActiveRecord::Migration[4.2]
  def self.up
    add_index :album_intakes, :reporting_month_id
    add_index :album_intakes, :person_intake_id

    add_index :person_intakes, :reporting_month_id
    add_index :person_intakes, :person_id
    add_index :person_intakes, :store_intake_id

    add_index :person_transactions, [:target_type, :target_id], :name => :person_transactions_target_index
  end

  def self.down
    remove_index :album_intakes, :reporting_month_id
    remove_index :album_intakes, :person_intake_id
    
    remove_index :person_intakes, :reporting_month_id
    remove_index :person_intakes, :person_id
    remove_index :person_intakes, :store_intake_id
    
    remove_index :person_transactions, :name => :person_transactions_target_index
  end
end
