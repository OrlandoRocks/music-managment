class CreateItunesUserReportPurchaseItems < ActiveRecord::Migration[4.2]
  def self.up
    create_table :itunes_user_report_purchase_items do |t|
      t.column :created_at, :datetime, :null => false, :default => nil
      t.column :purchase_id, :integer, :null => false, :default => nil
      t.column :purchaseable_id, :integer, :null => false, :default => nil
      t.column :purchaseable_type, :string, :limit => 30, :null => false, :default => nil
    end
  end

  def self.down
    drop_table :itunes_user_report_purchase_items
  end
end
