class AddLinernotesToAlbum < ActiveRecord::Migration[4.2]
  def self.up
    add_column :albums, :label_id, :integer
  end

  def self.down
    remove_column :albums, :label_id
  end
end
