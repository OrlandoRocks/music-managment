#Created By:    Ed Cheung
#Date:          2009-08-05
#Purpose:       The following migration file creates a countries table that
#               will be used to store all countries name and their respective ISO code
#
#               It will be used primarly as a lookup table. This table was created
#               for tour date feature in Widget Version 2 release.
#
################################################################################
#
#Column Description:
#id:            unique identifier for a country
#name:          country's name
#iso_code:      ISO lookup code for a country
#
################################################################################
#
#Column Data Type:
#
#id:            tinyint, unsigned, not null, auto increment, primary key
#name:          varchar(50), not null
#iso_code       varchar(2), not null
#
################################################################################
#
#Table Properties:
#engine:        innodb
#charset:        utf8
#
################################################################################
class CreateCountries < ActiveRecord::Migration[4.2]
  def self.up
    create_countries_table=<<-EOSTRING
      create table countries (id tinyint unsigned not null auto_increment, name varchar(50) not null,
                              iso_code char(2) not null, primary key(id)) engine=innodb charset=utf8;
    EOSTRING

    populate_countries=<<-EOSTRING
      INSERT INTO countries (name, iso_code) VALUES ('AFGHANISTAN', 'AF');
      INSERT INTO countries (name, iso_code) VALUES ('ALAND ISLANDS', 'AX');
      INSERT INTO countries (name, iso_code) VALUES ('ALBANIA', 'AL');
      INSERT INTO countries (name, iso_code) VALUES ('ALGERIA', 'DZ');
      INSERT INTO countries (name, iso_code) VALUES ('AMERICAN SAMOA', 'AS');
      INSERT INTO countries (name, iso_code) VALUES ('ANDORRA', 'AD');
      INSERT INTO countries (name, iso_code) VALUES ('ANGOLA', 'AO');
      INSERT INTO countries (name, iso_code) VALUES ('ANGUILLA', 'AI');
      INSERT INTO countries (name, iso_code) VALUES ('ANTARCTICA', 'AQ');
      INSERT INTO countries (name, iso_code) VALUES ('ANTIGUA AND BARBUDA', 'AG');
      INSERT INTO countries (name, iso_code) VALUES ('ARGENTINA', 'AR');
      INSERT INTO countries (name, iso_code) VALUES ('ARMENIA', 'AM');
      INSERT INTO countries (name, iso_code) VALUES ('ARUBA', 'AW');
      INSERT INTO countries (name, iso_code) VALUES ('AUSTRALIA', 'AU');
      INSERT INTO countries (name, iso_code) VALUES ('AUSTRIA', 'AT');
      INSERT INTO countries (name, iso_code) VALUES ('AZERBAIJAN', 'AZ');
      INSERT INTO countries (name, iso_code) VALUES ('BAHAMAS', 'BS');
      INSERT INTO countries (name, iso_code) VALUES ('BAHRAIN', 'BH');
      INSERT INTO countries (name, iso_code) VALUES ('BANGLADESH', 'BD');
      INSERT INTO countries (name, iso_code) VALUES ('BARBADOS', 'BB');
      INSERT INTO countries (name, iso_code) VALUES ('BELARUS', 'BY');
      INSERT INTO countries (name, iso_code) VALUES ('BELGIUM', 'BE');
      INSERT INTO countries (name, iso_code) VALUES ('BELIZE', 'BZ');
      INSERT INTO countries (name, iso_code) VALUES ('BENIN', 'BJ');
      INSERT INTO countries (name, iso_code) VALUES ('BERMUDA', 'BM');
      INSERT INTO countries (name, iso_code) VALUES ('BHUTAN', 'BT');
      INSERT INTO countries (name, iso_code) VALUES ('BOLIVIA', 'BO');
      INSERT INTO countries (name, iso_code) VALUES ('BOSNIA AND HERZEGOWINA', 'BA');
      INSERT INTO countries (name, iso_code) VALUES ('BOTSWANA', 'BW');
      INSERT INTO countries (name, iso_code) VALUES ('BOUVET ISLAND', 'BV');
      INSERT INTO countries (name, iso_code) VALUES ('BRAZIL', 'BR');
      INSERT INTO countries (name, iso_code) VALUES ('BRITISH INDIAN OCEAN TERRITORY', 'IO');
      INSERT INTO countries (name, iso_code) VALUES ('BRUNEI DARUSSALAM', 'BN');
      INSERT INTO countries (name, iso_code) VALUES ('BULGARIA', 'BG');
      INSERT INTO countries (name, iso_code) VALUES ('BURKINA FASO', 'BF');
      INSERT INTO countries (name, iso_code) VALUES ('BURUNDI', 'BI');
      INSERT INTO countries (name, iso_code) VALUES ('CAMBODIA', 'KH');
      INSERT INTO countries (name, iso_code) VALUES ('CAMEROON', 'CM');
      INSERT INTO countries (name, iso_code) VALUES ('CANADA', 'CA');
      INSERT INTO countries (name, iso_code) VALUES ('CAPE VERDE', 'CV');
      INSERT INTO countries (name, iso_code) VALUES ('CAYMAN ISLANDS', 'KY');
      INSERT INTO countries (name, iso_code) VALUES ('CENTRAL AFRICAN REPUBLIC', 'CF');
      INSERT INTO countries (name, iso_code) VALUES ('CHAD', 'TD');
      INSERT INTO countries (name, iso_code) VALUES ('CHILE', 'CL');
      INSERT INTO countries (name, iso_code) VALUES ('CHINA', 'CN');
      INSERT INTO countries (name, iso_code) VALUES ('CHRISTMAS ISLAND', 'CX');
      INSERT INTO countries (name, iso_code) VALUES ('COCOS (KEELING) ISLANDS', 'CC');
      INSERT INTO countries (name, iso_code) VALUES ('COLOMBIA', 'CO');
      INSERT INTO countries (name, iso_code) VALUES ('COMOROS', 'KM');
      INSERT INTO countries (name, iso_code) VALUES ('CONGO', 'CG');
      INSERT INTO countries (name, iso_code) VALUES ('CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'CD');
      INSERT INTO countries (name, iso_code) VALUES ('COOK ISLANDS', 'CK');
      INSERT INTO countries (name, iso_code) VALUES ('COSTA RICA', 'CR');
      INSERT INTO countries (name, iso_code) VALUES ('COTE D''IVOIRE', 'CI');
      INSERT INTO countries (name, iso_code) VALUES ('CROATIA', 'HR');
      INSERT INTO countries (name, iso_code) VALUES ('CUBA', 'CU');
      INSERT INTO countries (name, iso_code) VALUES ('CYPRUS', 'CY');
      INSERT INTO countries (name, iso_code) VALUES ('CZECH REPUBLIC', 'CZ');
      INSERT INTO countries (name, iso_code) VALUES ('DENMARK', 'DK');
      INSERT INTO countries (name, iso_code) VALUES ('DJIBOUTI', 'DJ');
      INSERT INTO countries (name, iso_code) VALUES ('DOMINICA', 'DM');
      INSERT INTO countries (name, iso_code) VALUES ('DOMINICAN REPUBLIC', 'DO');
      INSERT INTO countries (name, iso_code) VALUES ('ECUADOR', 'EC');
      INSERT INTO countries (name, iso_code) VALUES ('EGYPT', 'EG');
      INSERT INTO countries (name, iso_code) VALUES ('EL SALVADOR', 'SV');
      INSERT INTO countries (name, iso_code) VALUES ('EQUATORIAL GUINEA', 'GQ');
      INSERT INTO countries (name, iso_code) VALUES ('ERITREA', 'ER');
      INSERT INTO countries (name, iso_code) VALUES ('ESTONIA', 'EE');
      INSERT INTO countries (name, iso_code) VALUES ('ETHIOPIA', 'ET');
      INSERT INTO countries (name, iso_code) VALUES ('FALKLAND ISLANDS (MALVINAS)', 'FK');
      INSERT INTO countries (name, iso_code) VALUES ('FAROE ISLANDS', 'FO');
      INSERT INTO countries (name, iso_code) VALUES ('FIJI', 'FJ');
      INSERT INTO countries (name, iso_code) VALUES ('FINLAND', 'FI');
      INSERT INTO countries (name, iso_code) VALUES ('FRANCE', 'FR');
      INSERT INTO countries (name, iso_code) VALUES ('FRENCH GUIANA', 'GF');
      INSERT INTO countries (name, iso_code) VALUES ('FRENCH POLYNESIA', 'PF');
      INSERT INTO countries (name, iso_code) VALUES ('FRENCH SOUTHERN TERRITORIES', 'TF');
      INSERT INTO countries (name, iso_code) VALUES ('GABON', 'GA');
      INSERT INTO countries (name, iso_code) VALUES ('GAMBIA', 'GM');
      INSERT INTO countries (name, iso_code) VALUES ('GEORGIA', 'GE');
      INSERT INTO countries (name, iso_code) VALUES ('GERMANY', 'DE');
      INSERT INTO countries (name, iso_code) VALUES ('GHANA', 'GH');
      INSERT INTO countries (name, iso_code) VALUES ('GIBRALTAR', 'GI');
      INSERT INTO countries (name, iso_code) VALUES ('GREECE', 'GR');
      INSERT INTO countries (name, iso_code) VALUES ('GREENLAND', 'GL');
      INSERT INTO countries (name, iso_code) VALUES ('GRENADA', 'GD');
      INSERT INTO countries (name, iso_code) VALUES ('GUADELOUPE', 'GP');
      INSERT INTO countries (name, iso_code) VALUES ('GUAM', 'GU');
      INSERT INTO countries (name, iso_code) VALUES ('GUATEMALA', 'GT');
      INSERT INTO countries (name, iso_code) VALUES ('GUERNSEY', 'GG');
      INSERT INTO countries (name, iso_code) VALUES ('GUINEA', 'GN');
      INSERT INTO countries (name, iso_code) VALUES ('GUINEA-BISSAU', 'GW');
      INSERT INTO countries (name, iso_code) VALUES ('GUYANA', 'GY');
      INSERT INTO countries (name, iso_code) VALUES ('HAITI', 'HT');
      INSERT INTO countries (name, iso_code) VALUES ('HEARD AND MCDONALD ISLANDS', 'HM');
      INSERT INTO countries (name, iso_code) VALUES ('HOLY SEE (VATICAN CITY STATE)', 'VA');
      INSERT INTO countries (name, iso_code) VALUES ('HONDURAS', 'HN');
      INSERT INTO countries (name, iso_code) VALUES ('HONG KONG', 'HK');
      INSERT INTO countries (name, iso_code) VALUES ('HUNGARY', 'HU');
      INSERT INTO countries (name, iso_code) VALUES ('ICELAND', 'IS');
      INSERT INTO countries (name, iso_code) VALUES ('INDIA', 'IN');
      INSERT INTO countries (name, iso_code) VALUES ('INDONESIA', 'ID');
      INSERT INTO countries (name, iso_code) VALUES ('IRAN, ISLAMIC REPUBLIC OF', 'IR');
      INSERT INTO countries (name, iso_code) VALUES ('IRAQ', 'IQ');
      INSERT INTO countries (name, iso_code) VALUES ('IRELAND', 'IE');
      INSERT INTO countries (name, iso_code) VALUES ('ISLE OF MAN', 'IM');
      INSERT INTO countries (name, iso_code) VALUES ('ISRAEL', 'IL');
      INSERT INTO countries (name, iso_code) VALUES ('ITALY', 'IT');
      INSERT INTO countries (name, iso_code) VALUES ('JAMAICA', 'JM');
      INSERT INTO countries (name, iso_code) VALUES ('JAPAN', 'JP');
      INSERT INTO countries (name, iso_code) VALUES ('JERSEY', 'JE');
      INSERT INTO countries (name, iso_code) VALUES ('JORDAN', 'JO');
      INSERT INTO countries (name, iso_code) VALUES ('KAZAKHSTAN', 'KZ');
      INSERT INTO countries (name, iso_code) VALUES ('KENYA', 'KE');
      INSERT INTO countries (name, iso_code) VALUES ('KIRIBATI', 'KI');
      INSERT INTO countries (name, iso_code) VALUES ('KOREA, DEMOCRATIC PEOPLE''S REPUBLIC OF', 'KP');
      INSERT INTO countries (name, iso_code) VALUES ('KOREA, REPUBLIC OF', 'KR');
      INSERT INTO countries (name, iso_code) VALUES ('KUWAIT', 'KW');
      INSERT INTO countries (name, iso_code) VALUES ('KYRGYZSTAN', 'KG');
      INSERT INTO countries (name, iso_code) VALUES ('LAO PEOPLE''S DEMOCRATIC REPUBLIC', 'LA');
      INSERT INTO countries (name, iso_code) VALUES ('LATVIA', 'LV');
      INSERT INTO countries (name, iso_code) VALUES ('LEBANON', 'LB');
      INSERT INTO countries (name, iso_code) VALUES ('LESOTHO', 'LS');
      INSERT INTO countries (name, iso_code) VALUES ('LIBERIA', 'LR');
      INSERT INTO countries (name, iso_code) VALUES ('LIBYAN ARAB JAMAHIRIYA', 'LY');
      INSERT INTO countries (name, iso_code) VALUES ('LIECHTENSTEIN', 'LI');
      INSERT INTO countries (name, iso_code) VALUES ('LITHUANIA', 'LT');
      INSERT INTO countries (name, iso_code) VALUES ('LUXEMBOURG', 'LU');
      INSERT INTO countries (name, iso_code) VALUES ('MACAO', 'MO');
      INSERT INTO countries (name, iso_code) VALUES ('MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'MK');
      INSERT INTO countries (name, iso_code) VALUES ('MADAGASCAR', 'MG');
      INSERT INTO countries (name, iso_code) VALUES ('MALAWI', 'MW');
      INSERT INTO countries (name, iso_code) VALUES ('MALAYSIA', 'MY');
      INSERT INTO countries (name, iso_code) VALUES ('MALDIVES', 'MV');
      INSERT INTO countries (name, iso_code) VALUES ('MALI', 'ML');
      INSERT INTO countries (name, iso_code) VALUES ('MALTA', 'MT');
      INSERT INTO countries (name, iso_code) VALUES ('MARSHALL ISLANDS', 'MH');
      INSERT INTO countries (name, iso_code) VALUES ('MARTINIQUE', 'MQ');
      INSERT INTO countries (name, iso_code) VALUES ('MAURITANIA', 'MR');
      INSERT INTO countries (name, iso_code) VALUES ('MAURITIUS', 'MU');
      INSERT INTO countries (name, iso_code) VALUES ('MAYOTTE', 'YT');
      INSERT INTO countries (name, iso_code) VALUES ('MEXICO', 'MX');
      INSERT INTO countries (name, iso_code) VALUES ('MICRONESIA, FEDERATED STATES OF', 'FM');
      INSERT INTO countries (name, iso_code) VALUES ('MOLDOVA, REPUBLIC OF', 'MD');
      INSERT INTO countries (name, iso_code) VALUES ('MONACO', 'MC');
      INSERT INTO countries (name, iso_code) VALUES ('MONGOLIA', 'MN');
      INSERT INTO countries (name, iso_code) VALUES ('MONTENEGRO', 'ME');
      INSERT INTO countries (name, iso_code) VALUES ('MONTSERRAT', 'MS');
      INSERT INTO countries (name, iso_code) VALUES ('MOROCCO', 'MA');
      INSERT INTO countries (name, iso_code) VALUES ('MOZAMBIQUE', 'MZ');
      INSERT INTO countries (name, iso_code) VALUES ('MYANMAR', 'MM');
      INSERT INTO countries (name, iso_code) VALUES ('NAMIBIA', 'NA');
      INSERT INTO countries (name, iso_code) VALUES ('NAURU', 'NR');
      INSERT INTO countries (name, iso_code) VALUES ('NEPAL', 'NP');
      INSERT INTO countries (name, iso_code) VALUES ('NETHERLANDS', 'NL');
      INSERT INTO countries (name, iso_code) VALUES ('NETHERLANDS ANTILLES', 'AN');
      INSERT INTO countries (name, iso_code) VALUES ('NEW CALEDONIA', 'NC');
      INSERT INTO countries (name, iso_code) VALUES ('NEW ZEALAND', 'NZ');
      INSERT INTO countries (name, iso_code) VALUES ('NICARAGUA', 'NI');
      INSERT INTO countries (name, iso_code) VALUES ('NIGER', 'NE');
      INSERT INTO countries (name, iso_code) VALUES ('NIGERIA', 'NG');
      INSERT INTO countries (name, iso_code) VALUES ('NIUE', 'NU');
      INSERT INTO countries (name, iso_code) VALUES ('NORFOLK ISLAND', 'NF');
      INSERT INTO countries (name, iso_code) VALUES ('NORTHERN MARIANA ISLANDS', 'MP');
      INSERT INTO countries (name, iso_code) VALUES ('NORWAY', 'NO');
      INSERT INTO countries (name, iso_code) VALUES ('OMAN', 'OM');
      INSERT INTO countries (name, iso_code) VALUES ('PAKISTAN', 'PK');
      INSERT INTO countries (name, iso_code) VALUES ('PALAU', 'PW');
      INSERT INTO countries (name, iso_code) VALUES ('PALESTINIAN TERRITORY, OCCUPIED', 'PS');
      INSERT INTO countries (name, iso_code) VALUES ('PANAMA', 'PA');
      INSERT INTO countries (name, iso_code) VALUES ('PAPUA NEW GUINEA', 'PG');
      INSERT INTO countries (name, iso_code) VALUES ('PARAGUAY', 'PY');
      INSERT INTO countries (name, iso_code) VALUES ('PERU', 'PE');
      INSERT INTO countries (name, iso_code) VALUES ('PHILIPPINES', 'PH');
      INSERT INTO countries (name, iso_code) VALUES ('PITCAIRN', 'PN');
      INSERT INTO countries (name, iso_code) VALUES ('POLAND', 'PL');
      INSERT INTO countries (name, iso_code) VALUES ('PORTUGAL', 'PT');
      INSERT INTO countries (name, iso_code) VALUES ('PUERTO RICO', 'PR');
      INSERT INTO countries (name, iso_code) VALUES ('QATAR', 'QA');
      INSERT INTO countries (name, iso_code) VALUES ('REUNION', 'RE');
      INSERT INTO countries (name, iso_code) VALUES ('ROMANIA', 'RO');
      INSERT INTO countries (name, iso_code) VALUES ('RUSSIAN FEDERATION', 'RU');
      INSERT INTO countries (name, iso_code) VALUES ('RWANDA', 'RW');
      INSERT INTO countries (name, iso_code) VALUES ('SAINT BARTHELEMY', 'BL');
      INSERT INTO countries (name, iso_code) VALUES ('SAINT HELENA', 'SH');
      INSERT INTO countries (name, iso_code) VALUES ('SAINT KITTS AND NEVIS', 'KN');
      INSERT INTO countries (name, iso_code) VALUES ('SAINT LUCIA', 'LC');
      INSERT INTO countries (name, iso_code) VALUES ('SAINT MARTIN', 'MF');
      INSERT INTO countries (name, iso_code) VALUES ('SAINT PIERRE AND MIQUELON', 'PM');
      INSERT INTO countries (name, iso_code) VALUES ('SAINT VINCENT AND THE GRENADINES', 'VC');
      INSERT INTO countries (name, iso_code) VALUES ('SAMOA', 'WS');
      INSERT INTO countries (name, iso_code) VALUES ('SAN MARINO', 'SM');
      INSERT INTO countries (name, iso_code) VALUES ('SAO TOME AND PRINCIPE', 'ST');
      INSERT INTO countries (name, iso_code) VALUES ('SAUDI ARABIA', 'SA');
      INSERT INTO countries (name, iso_code) VALUES ('SENEGAL', 'SN');
      INSERT INTO countries (name, iso_code) VALUES ('SERBIA', 'RS');
      INSERT INTO countries (name, iso_code) VALUES ('SEYCHELLES', 'SC');
      INSERT INTO countries (name, iso_code) VALUES ('SIERRA LEONE', 'SL');
      INSERT INTO countries (name, iso_code) VALUES ('SINGAPORE', 'SG');
      INSERT INTO countries (name, iso_code) VALUES ('SLOVAKIA', 'SK');
      INSERT INTO countries (name, iso_code) VALUES ('SLOVENIA', 'SI');
      INSERT INTO countries (name, iso_code) VALUES ('SOLOMON ISLANDS', 'SB');
      INSERT INTO countries (name, iso_code) VALUES ('SOMALIA', 'SO');
      INSERT INTO countries (name, iso_code) VALUES ('SOUTH AFRICA', 'ZA');
      INSERT INTO countries (name, iso_code) VALUES ('SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'GS');
      INSERT INTO countries (name, iso_code) VALUES ('SPAIN', 'ES');
      INSERT INTO countries (name, iso_code) VALUES ('SRI LANKA', 'LK');
      INSERT INTO countries (name, iso_code) VALUES ('SUDAN', 'SD');
      INSERT INTO countries (name, iso_code) VALUES ('SURINAME', 'SR');
      INSERT INTO countries (name, iso_code) VALUES ('SVALBARD AND JAN MAYEN', 'SJ');
      INSERT INTO countries (name, iso_code) VALUES ('SWAZILAND', 'SZ');
      INSERT INTO countries (name, iso_code) VALUES ('SWEDEN', 'SE');
      INSERT INTO countries (name, iso_code) VALUES ('SWITZERLAND', 'CH');
      INSERT INTO countries (name, iso_code) VALUES ('SYRIAN ARAB REPUBLIC', 'SY');
      INSERT INTO countries (name, iso_code) VALUES ('TAIWAN, PROVINCE OF CHINA', 'TW');
      INSERT INTO countries (name, iso_code) VALUES ('TAJIKISTAN', 'TJ');
      INSERT INTO countries (name, iso_code) VALUES ('TANZANIA, UNITED REPUBLIC OF', 'TZ');
      INSERT INTO countries (name, iso_code) VALUES ('THAILAND', 'TH');
      INSERT INTO countries (name, iso_code) VALUES ('TIMOR-LESTE', 'TL');
      INSERT INTO countries (name, iso_code) VALUES ('TOGO', 'TG');
      INSERT INTO countries (name, iso_code) VALUES ('TOKELAU', 'TK');
      INSERT INTO countries (name, iso_code) VALUES ('TONGA', 'TO');
      INSERT INTO countries (name, iso_code) VALUES ('TRINIDAD AND TOBAGO', 'TT');
      INSERT INTO countries (name, iso_code) VALUES ('TUNISIA', 'TN');
      INSERT INTO countries (name, iso_code) VALUES ('TURKEY', 'TR');
      INSERT INTO countries (name, iso_code) VALUES ('TURKMENISTAN', 'TM');
      INSERT INTO countries (name, iso_code) VALUES ('TURKS AND CAICOS ISLANDS', 'TC');
      INSERT INTO countries (name, iso_code) VALUES ('TUVALU', 'TV');
      INSERT INTO countries (name, iso_code) VALUES ('UGANDA', 'UG');
      INSERT INTO countries (name, iso_code) VALUES ('UKRAINE', 'UA');
      INSERT INTO countries (name, iso_code) VALUES ('UNITED ARAB EMIRATES', 'AE');
      INSERT INTO countries (name, iso_code) VALUES ('UNITED KINGDOM', 'GB');
      INSERT INTO countries (name, iso_code) VALUES ('UNITED STATES', 'US');
      INSERT INTO countries (name, iso_code) VALUES ('UNITED STATES MINOR OUTLYING ISLANDS', 'UM');
      INSERT INTO countries (name, iso_code) VALUES ('URUGUAY', 'UY');
      INSERT INTO countries (name, iso_code) VALUES ('UZBEKISTAN', 'UZ');
      INSERT INTO countries (name, iso_code) VALUES ('VANUATU', 'VU');
      INSERT INTO countries (name, iso_code) VALUES ('VENEZUELA', 'VE');
      INSERT INTO countries (name, iso_code) VALUES ('VIET NAM', 'VN');
      INSERT INTO countries (name, iso_code) VALUES ('VIRGIN ISLANDS, BRITISH', 'VG');
      INSERT INTO countries (name, iso_code) VALUES ('VIRGIN ISLANDS, U.S.', 'VI');
      INSERT INTO countries (name, iso_code) VALUES ('WALLIS AND FUTUNA', 'WF');
      INSERT INTO countries (name, iso_code) VALUES ('WESTERN SAHARA', 'EH');
      INSERT INTO countries (name, iso_code) VALUES ('YEMEN', 'YE');
      INSERT INTO countries (name, iso_code) VALUES ('ZAMBIA', 'ZM');
      INSERT INTO countries (name, iso_code) VALUES ('ZIMBABWE', 'ZW');
    EOSTRING

    execute create_countries_table
    populate_countries.split("\n").each do |populate_country|
      execute populate_country
    end
  end

  def self.down
    drop_table :countries
  end
end
