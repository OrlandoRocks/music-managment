class CreateRenewals < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `renewals`(
                `id` int UNSIGNED not null auto_increment,
                `person_id` int UNSIGNED not null,
                `item_to_renew_id` int UNSIGNED not null,
                `item_to_renew_type` ENUM('Product', 'ProductItem') NOT NULL,
                `canceled_at` timestamp null COMMENT 'when did the user cancel this renewal',
                `takedown_at` timestamp null COMMENT 'set when the user wants to take down a distributed item',
                `created_at` timestamp null,                
                PRIMARY KEY  (`id`),
                KEY `person_id` (`person_id`),
                KEY `item_to_renew` (`item_to_renew_id`, `item_to_renew_type`),
                CONSTRAINT `fk_renewals_person_id` FOREIGN KEY (`person_id`) REFERENCES `people`(`id`) 
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
                
    execute up_sql
  end

  def self.down
    drop_table :renewals
  end
end
