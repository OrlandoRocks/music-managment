class AddNapsterPriceCodeToOlderSalepoints < ActiveRecord::Migration[4.2]
    def self.up
      store = Store.find_by(name: 'Napster')
      execute "UPDATE salepoints SET variable_price_id = #{store.variable_prices.first.id} where store_id = #{store.id}"
    end

    def self.down
      store = Store.find_by(name: 'Napster')
      execute "UPDATE salepoints SET variable_price_id = NULL where store_id = #{store.id}"
    end
end
