class AddMissingGenres < ActiveRecord::Migration[4.2]
  def self.up
    genrearray = ["Americana","Country"]
    genrearray.each { |genre|
      Genre.create :name=>genre
    }
  end

  def self.down
  end
end
