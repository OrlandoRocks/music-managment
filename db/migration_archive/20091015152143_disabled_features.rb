class DisabledFeatures < ActiveRecord::Migration[4.2]
  def self.up
    execute "DROP TABLE IF EXISTS `disabled_features_partners`;"
    execute "DROP TABLE IF EXISTS `features`;"
    
    #
    #  Create Table of Disabled Features
    #  with relation to Partners
    #
    create_table :features do |t|
      t.string :name, :null => false
    end

    create_table :disabled_features_partners do |t|
      t.integer :feature_id, :null => false
      t.integer :partner_id, :null => false
      t.timestamps
    end

    execute("ALTER TABLE disabled_features_partners MODIFY COLUMN partner_id smallint(5) unsigned;")

    # Create a unique index for the features - don't want to allow a feature to be added
    # several times to a partner.
    add_index :disabled_features_partners, [:feature_id,:partner_id], 
      :unique => true, :name => "index_unique_disabled_features_partners_ids"

    # Add foreign key to features
    execute("
      ALTER TABLE disabled_features_partners
      ADD FOREIGN KEY (feature_id)
      REFERENCES features(id)
      ON DELETE CASCADE;
    ")
    execute("
      ALTER TABLE disabled_features_partners
      ADD FOREIGN KEY (partner_id)
      REFERENCES partners(id)
      ON DELETE CASCADE;
    ")
  end

  def self.down
    execute "DROP TABLE IF EXISTS `disabled_features_partners`;"
    execute "DROP TABLE IF EXISTS `features`;"
  end
end
