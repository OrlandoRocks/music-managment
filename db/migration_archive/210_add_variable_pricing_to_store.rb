class AddVariablePricingToStore < ActiveRecord::Migration[4.2]
  def self.up
    add_column :stores, :variable_pricing, :boolean, :default => false
  end

  def self.down
    remove_column :stores, :variable_pricing
  end
end
