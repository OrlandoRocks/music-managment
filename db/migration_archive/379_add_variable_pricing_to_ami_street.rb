class AddVariablePricingToAmiStreet < ActiveRecord::Migration[4.2]

  def self.up
    store = Store.find_by(short_name: "Amiestreet")
    store.update_attribute(:variable_pricing, true)
    VariablePrice.create(:store_id => store.id, :price_code => '1', :price_code_display => '8.98', :active => true, :position => 1)
    VariablePrice.create(:store_id => store.id, :price_code => '2', :price_code_display => '7.00', :active => true, :position => 2)
    VariablePrice.create(:store_id => store.id, :price_code => '3', :price_code_display => '5.00', :active => true, :position => 3)
  end


  def self.down
    store = Store.find_by(short_name: "Amiestreet")
    VariablePrice.where("store_id = #{store.id}").destroy_all
  end

end
