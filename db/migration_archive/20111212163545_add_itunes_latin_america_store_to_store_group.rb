class AddItunesLatinAmericaStoreToStoreGroup < ActiveRecord::Migration[4.2]
  def self.up
    storeid = Store.find_by(name: "iTunes Latin America (incl. Brazil)").id
    execute ("insert into store_group_stores (store_group_id,store_id) values ('1','#{storeid}')")
  end

  def self.down
    storeid = Store.find_by(name: "iTunes Latin America (incl. Brazil)").id
    execute("delete from store_group_stores where store_id = #{storeid}")
  end
end
