class ShippingLabels < ActiveRecord::Migration[4.2]
  def self.up
    create_table :shipping_labels do |t|
      t.column  :video_id, :integer
      t.column  :price_policy_id, :integer
      t.column  :weight, :float
      t.column  :service_type, :string
      t.column  :label, :binary
    end
  end

  def self.down
    drop_table :shipping_labels
  end
end
