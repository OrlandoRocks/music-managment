class MakeRhapsodyVariablePrice < ActiveRecord::Migration[4.2]
  def self.up
    Store.find(7).update_attribute(:variable_pricing, true)
    VariablePrice.create(:store_id => 7, :price_code => '$.99', :price_code_display => '$.99', :active => true, :position => 1)

  end

  def self.down
    VariablePrice.where("store_id = 7").destroy_all
    Store.find(7).update_attribute(:variable_pricing, false)
  end
end
