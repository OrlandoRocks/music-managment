class AddPaidToBooklet < ActiveRecord::Migration[4.2]
  def self.up
    add_column :booklets, :paid_at, :datetime
    add_column :booklets, :price_policy_id, :integer
    add_column :booklets, :purchase_id, :integer
  end

  def self.down
    remove_column :booklets, :paid_at
    remove_column :booklets, :price_policy_id
    remove_column :booklets, :purchase_id
  end
end
