class AddSpawningCerts < ActiveRecord::Migration[4.2]
  def self.up
    add_column :certs, :parent_id, :integer, :null => true, :default => nil
    add_column :certs, :cert_engine, :string, :limit => 30, :null => true, :default => nil
    add_column :certs, :engine_params, :string, :limit => 30, :null => true, :default => nil
  end

  def self.down
    remove_column :certs, :parent_id
    remove_column :certs, :cert_engine
    remove_column :certs, :engine_params
  end
end
