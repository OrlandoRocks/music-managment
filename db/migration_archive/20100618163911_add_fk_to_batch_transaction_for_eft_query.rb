class AddFkToBatchTransactionForEftQuery < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE eft_batch_transactions ADD CONSTRAINT `FK_eft_txn_eft_query` FOREIGN KEY (`eft_query_id`) REFERENCES `eft_queries` (`id`)")
  end

  def self.down
    execute("ALTER TABLE eft_batch_transactions DROP FOREIGN KEY FK_eft_txn_eft_query")
  end
end
