class TermsAndConditions < ActiveRecord::Migration[4.2]
  def self.up
    add_column :people, :accepted_terms_and_conditions_on, :datetime
    execute("
      update people
      set people.accepted_terms_and_conditions_on = (
        SELECT approval_date
        FROM albums
        WHERE albums.person_id = people.id
        AND albums.approval_date IS NOT NULL
        ORDER BY approval_date
        LIMIT 1
      )
    ")
  end

  def self.down
    remove_column :people, :accepted_terms_and_conditions_on
  end
end
