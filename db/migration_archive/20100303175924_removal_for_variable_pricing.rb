
class RemovalForVariablePricing < ActiveRecord::Migration[4.2]
  def self.up
    say_with_time("Remove Variable Pricing from Stores") do
      remove_column :stores, :variable_pricing
    end

    say_with_time("Remove Columns from Variable Prices.  Moved to variable_prices_stores M2M table.") do
     # remove_column :variable_prices, :store_id
     # remove_column :variable_prices, :active
     # remove_column :variable_prices, :position
    end

  end

  def self.down
    say_with_time("Adding variable pricing column back") do
      add_column :stores, :variable_pricing, :boolean
    end

    say_with_time("Adding columns back to variable prices table") do
      add_column :variable_prices, :store_id, :integer
      add_column :variable_prices, :active, :boolean
      add_column :variable_prices, :position, :integer
    end
  end
end
