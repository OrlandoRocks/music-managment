class AddCityStateColumnItunesTrendsTable < ActiveRecord::Migration[4.2]
  def self.up


    add_column :itunes_user_trends, :city, :string
    add_column :itunes_user_trends, :state, :string
    add_column :itunes_user_trends, :country_code, :string

  end

  def self.down
    remove_column :itunes_user_trends, :city
    remove_column :itunes_user_trends, :state
    remove_column :itunes_user_trends, :country_code

  end
end
