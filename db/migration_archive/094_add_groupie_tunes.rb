class AddGroupieTunes < ActiveRecord::Migration[4.2]
  def self.up
		Store.create(:name=> "GroupieTunes", :abbrev=> "gp", :short_name=> "GroupieTunes")
		Store.update_all('position = id')

  end

  def self.down
		store = Store.find_by(abbrev: 'gp')
		store.destroy
  end
end
