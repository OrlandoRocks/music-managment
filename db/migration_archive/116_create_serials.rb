class CreateSerials < ActiveRecord::Migration[4.2]
  def self.up
    create_table :serials do |t|
      t.column :number, :string, :null => false, :limit => 128
      t.column :taken, :boolean, :null => false, :default => false
      t.column :person_id, :integer
      t.column :claimed_at, :datetime
      t.column :created_at, :datetime, :null => false
    end
    add_index :serials, :number, :unique => true
    add_index :serials, [:taken, :person_id]
  end
  def self.down
    drop_table :serials
  end
end
