class RemoveTempPersonFromAlbum < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :albums, :temp_person
  end

  def self.down
    add_column :albums, :temp_person, :integer
  end
end
