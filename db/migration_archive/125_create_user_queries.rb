class CreateUserQueries < ActiveRecord::Migration[4.2]
  def self.up
    create_table :user_queries do |t|
      t.column :label, :string
      t.column :created_at, :date
      t.column :updated_at, :date
    end
  end

  def self.down
    drop_table :user_queries
  end
end
