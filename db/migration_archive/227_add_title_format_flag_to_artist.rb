class AddTitleFormatFlagToArtist < ActiveRecord::Migration[4.2]
  def self.up
    add_column :artists, :allow_different_format, :boolean, :default => false   
  end

  def self.down
    remove_column :artists, :allow_different_format
  end
end
