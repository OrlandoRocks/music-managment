class IsReferred < ActiveRecord::Migration[4.2]
  def self.up
    add_column :users, :is_referred, :boolean, :default => false
  end

  def self.down
    remove_column :users, :is_referred
  end
end
