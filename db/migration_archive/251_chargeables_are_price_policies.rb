class ChargeablesArePricePolicies < ActiveRecord::Migration[4.2]
  def self.up
    rename_table :chargeables, :price_policies
    rename_column :albums, :chargeable_id, :price_policy_id
    rename_column :songs, :chargeable_id, :price_policy_id
    rename_column :salepoints, :chargeable_id, :price_policy_id
  end

  def self.down
    rename_table :price_policies, :chargeables
    rename_column :albums, :price_policy_id, :chargeable_id
    rename_column :songs, :price_policy_id, :chargeable_id
    rename_column :salepoints, :price_policy_id, :chargeable_id
  end
end
