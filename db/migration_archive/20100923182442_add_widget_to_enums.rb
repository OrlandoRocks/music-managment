class AddWidgetToEnums < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = "ALTER TABLE purchases CHANGE related_type related_type ENUM('Album','Single','Ringtone','Entitlement','Product','Renewal','ItunesUserReport','Video','Salepoint','Song','Widget') CHARACTER SET utf8 COLLATE utf8_general_ci NULL;"
    execute up_sql
    
    up_sql = "ALTER TABLE products CHANGE applies_to_product applies_to_product ENUM('Album','Single','Ringtone','Video','None','Widget') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'None' NOT NULL;"
    execute up_sql

    up_sql = "ALTER TABLE renewal_items CHANGE related_type related_type ENUM('Album','Single','Ringtone','Entitlement','Product','Video','Widget') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;"
    execute up_sql

    up_sql = "ALTER TABLE inventory_usages CHANGE related_type related_type ENUM('Album','Single','Ringtone','Song','Salepoint','Video','Booklet','ItunesUserReport','ShippingLabel','Widget') CHARACTER SET utf8 COLLATE utf8_general_ci NULL  COMMENT 'the model name of the related inventory used';"
    execute up_sql

    up_sql = "ALTER TABLE inventories CHANGE inventory_type inventory_type ENUM('Album','Single','Ringtone','Video','ShippingLabel','ItunesUserReport','Booklet','Song','Salepoint','Widget') CHARACTER SET utf8 COLLATE utf8_general_ci NULL  COMMENT 'the type of inventory to be used';"          
    execute up_sql
  end
  

  def self.down
    down_sql = ""
    execute down_sql
  end
end
