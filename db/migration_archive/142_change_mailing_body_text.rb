class ChangeMailingBodyText < ActiveRecord::Migration[4.2]
  def self.up
    change_column :mailings, :body, :text
  end

  def self.down
    change_column :mailings, :body, :string
  end
end
