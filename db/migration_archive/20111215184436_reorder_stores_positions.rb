class ReorderStoresPositions < ActiveRecord::Migration[4.2]
  def self.up
    stores = Store.all
    stores.each do |s|
      s.position = s.position * 10
      s.save
    end
    la = Store.find_by(name: "iTunes Latin America (incl. Brazil)")
    la.position = 71
    la.save
  end

  def self.down
    la = Store.find_by(name: "iTunes Latin America (incl. Brazil)")
    la.position = 280
    la.save
    stores = Store.all
    stores.each do |s|
      s.position = s.position / 10
      s.save
    end
  end
end
