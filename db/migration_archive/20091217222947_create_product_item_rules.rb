class CreateProductItemRules < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `product_item_rules`(
                `id` int UNSIGNED not null auto_increment,
                `product_item_id` int UNSIGNED not null COMMENT 'references product.id',
                `base_item_option_id` int UNSIGNED not null COMMENT 'references base_item_options.id',
                `parent_id` int UNSIGNED null COMMENT 'reference id for parent rule, required for multiple rules that govern prices',
                `rule_type` ENUM('inventory','entitlement', 'price_only') COMMENT 'grants a user inventory or an entitlement',
                `rule` ENUM('price_for_each', 'total_included', 'price_above_included', 'price_for_each_above_included', 'range', 'true_false') not null COMMENT 'processing rule for determining price',
                `inventory_type` varchar(50) not null COMMENT 'inventory_type of product item being sold',
                `model_to_check` varchar(50) null COMMENT 'required for rules other than price_for_each',
                `method_to_check` varchar(50) null COMMENT 'method to check on the provided model',
                `quantity` mediumint not null default 1 COMMENT 'inventory granted',
                `unlimited` tinyint(1) not null default 0 COMMENT 'used when inventory granted is unlimited',
                `true_false` tinyint(1) null COMMENT 'used for true_false rule',
                `minimum` mediumint null COMMENT 'start of range/threshhold',
                `maximum` mediumint null COMMENT 'end of range/threshold',
                `entitlement_rights_group_id` int UNSIGNED null COMMENT 'references entitlement_rights_groups.id when rule_type is entitlement',
                `price` decimal(10,2) not null default 0.00 COMMENT 'price when the rules are matched, can be 0.00',
                `updated_at` timestamp null,
                `created_at` timestamp null,                
                PRIMARY KEY  (`id`),
                KEY `product_item_id` (`product_item_id`),
                CONSTRAINT `fk_product_item_id` FOREIGN KEY (`product_item_id`) REFERENCES `product_items`(`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
            
    execute up_sql
  end

  def self.down
    drop_table :product_item_rules
  end
end
