class CreateProducts < ActiveRecord::Migration[4.2]
  def self.up
    create_table :products do |t|
	    t.column :title, :string
      t.column :short_title, :string
      t.column :body, :text
      t.column :posted, :datetime
      t.column :online, :boolean
      t.column :position, :integer
      t.column :start_at, :datetime
      t.column :finish_at, :datetime
      t.column :online, :boolean, :default => true
      t.column :is_deleted, :boolean, :default => false
    end
  end

  def self.down
    drop_table :products
  end
end
