class AddSong198ToPricePolicy < ActiveRecord::Migration[4.2]
  def self.up
    PricePolicy.create(:short_code=>'song198', :price_calculator=>'current_song', :base_price_cents => 198, :description=> 'Song at the price point $1.98')
    PricePolicy.create(:short_code=>'store198', :price_calculator=>'current_store', :base_price_cents => 198, :description=> 'Store at the price point $1.98')
  end

  def self.down
    #we can use delete_all since short_code is unique
    PricePolicy.where(:short_code=>'song198').delete_all
    PricePolicy.where(:short_code=>'store198').delete_all
  end
end
