class AddCreateOnDateForPersonModel < ActiveRecord::Migration[4.2]
  def self.up
		add_column :people, :created_on, :datetime, :default => Time.now
  end

  def self.down
		remove_column :people, :created_on
  end
end
