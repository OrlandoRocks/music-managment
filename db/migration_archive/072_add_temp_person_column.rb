class AddTempPersonColumn < ActiveRecord::Migration[4.2]
  def self.up
    add_column :albums, :temp_person, :integer
  end

  def self.down
    remove_column :albums, :temp_person
  end
end
