class AddSortOrderDefaultToProducts < ActiveRecord::Migration[4.2]
  def self.up
      execute %Q(ALTER TABLE products ADD COLUMN sort_order SMALLINT default 1 NOT NULL AFTER product_type,
                MODIFY COLUMN product_type enum('Ad Hoc','Package','Renewal', 'Renewal Extension', 'Distribution Extension') NOT NULL DEFAULT 'Package',
                ADD COLUMN applies_to_product enum('Album', 'Single', 'Ringtone', 'Video', 'None') NOT NULL DEFAULT 'None' AFTER `product_type`,  
                ADD COLUMN display_name VARCHAR(50) NULL AFTER `name`,
                ADD COLUMN `flag_text` varchar(15) NULL AFTER `name`,
                ADD COLUMN is_default SMALLINT(1) default 1 NOT NULL AFTER product_type)
      
      execute "UPDATE products set display_name = name;"
  end

  def self.down
      execute %Q(ALTER TABLE products DROP COLUMN `sort_order`,
                MODIFY COLUMN product_type enum('Ad Hoc','Package','Renewal'),
                DROP COLUMN applies_to_product,
                DROP COLUMN `is_default`,
                DROP COLUMN `display_name`,
                DROP COLUMN `flag_text`)
        
  end
end
