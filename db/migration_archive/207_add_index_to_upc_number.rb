class AddIndexToUpcNumber < ActiveRecord::Migration[4.2]
  def self.up    
    add_index :upcs, :number, :unique => false, :name => :unique_upc_number
    #FIXMEMJL this should be :unique => true but we need to clean up duplicate upcs (ticket 128)
  end

  def self.down
    remove_index :upcs, :name => :unique_upc_number
  end
end
