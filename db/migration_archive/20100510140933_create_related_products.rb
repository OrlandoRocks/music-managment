class CreateRelatedProducts < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `related_products`(
                `id` INT(10) UNSIGNED not null auto_increment COMMENT 'Primary Key',
                `related_product_id` int(10) not null COMMENT 'id of related product',
                `product_id` int(10) not null COMMENT 'product this upsell product is related to',
                `display_name` varchar(50) NULL COMMENT 'override display name for product table',
                `description` varchar(100) NULL COMMENT 'override description for product table',
                `sort_order` smallint not null COMMENT 'sort order of upsell offers',
                `start_date` TIMESTAMP NULL COMMENT 'upsell starts on',
                `expiration_date` TIMESTAMP NULL COMMENT 'upsell ends on',
                `created_by_id` INT(10) UNSIGNED not null COMMENT 'id of administrator who created the offer',
                `updated_at` TIMESTAMP NULL COMMENT 'Datetime of latest update',
                `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',
                PRIMARY KEY  (`id`),
                KEY `product_id` (`product_id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
                
    execute up_sql        
  end

  def self.down
    drop_table :related_products
  end
end
