class AddParentalAdvisoryToSongs < ActiveRecord::Migration[4.2]
  def self.up
    add_column :songs, :parental_advisory, :boolean, :null => false, :default => false
  end

  def self.down
    remove_column :songs, :parental_advisory
  end
end
