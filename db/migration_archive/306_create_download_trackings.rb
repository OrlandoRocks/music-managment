class CreateDownloadTrackings < ActiveRecord::Migration[4.2]
  def self.up
    create_table :download_trackings do |t|
      t.column :person_id, :integer, :null => false
      t.column :downloaded_at, :datetime, :null => false
      t.column :promotion, :string, :limit => 64, :null => false
    end
  end

  def self.down
    drop_table :download_trackings
  end
end
