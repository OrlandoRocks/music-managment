class AddNz < ActiveRecord::Migration[4.2]
  def self.up
    store = Store.find_by(abbrev: "au")
    store.update_attribute(:name,"iTunes Australia/N.Z.")
    store.save
  end

  def self.down
    store = Store.find_by(abbrev: "au")
    store.update_attribute(:name,"iTunes Australia")
    store.save
  end
end
