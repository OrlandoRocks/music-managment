class AddMailingBounces < ActiveRecord::Migration[4.2]
  def self.up
    create_table :mailing_bounces do |t|
      t.column :person_id, :integer, :null => false
      t.column :bounced_at, :datetime
      t.column :created_at, :datetime
    end
    add_index :mailing_bounces, [:person_id,:created_at]
  end

  def self.down
    drop_table :mailing_bounces
  end
end
