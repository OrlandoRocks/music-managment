class BackrefSalepoints < ActiveRecord::Migration[4.2]
  def self.up
    transaction do
      say_with_time "Back Reference A Default Price Point for Variable Prices" do
        setup_backreference
      end
    end
  end

  def self.down
    transaction do
      say_with_time "Undo Backreferencing of Salepoints to Variable Prices" do
        undo_backreference
      end
    end
  end

  protected

  def self.setup_backreference
    DefaultVariablePrice.all.each do |default|
      Salepoint.where("store_id = #{default.store_id} and variable_price_id IS NULL").update_all("variable_price_id = #{default.variable_price.id}")
    end
  end

  def self.undo_backreference
    ids = DefaultVariablePrice.all.map { |default| default.id }
    Salepoint.where("store_id IN (#{ids.join(',')})").update_all("variable_price_id = NULL")
  end
end

