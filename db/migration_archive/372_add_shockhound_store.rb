class AddShockhoundStore < ActiveRecord::Migration[4.2]
  def self.up
    Store.new(:name => "Shockhound",
              :needs_rights_assignment => false,
              :abbrev => "sh",
              :variable_pricing => false,
              #:base_price_policy_id => 3,
              :is_active => false,
              :short_name => "Shockhound",
              :position => 16).save
  end

  def self.down
    Store.find_by(name: "Shockhound").destroy
  end
end
