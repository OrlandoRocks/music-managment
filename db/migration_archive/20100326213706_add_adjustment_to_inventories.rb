class AddAdjustmentToInventories < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = "ALTER TABLE inventories 
       ADD COLUMN `quantity_adjustment` INT(10) NOT NULL DEFAULT '0' COMMENT 'quantity adjustment' after `quantity_used`"
    execute up_sql
		#add_column :inventories, :quantity_adjustment, :integer, :null => false, :default => 0
  end

  def self.down
		remove_column :inventories, :quantity_adjustment
  end
end
