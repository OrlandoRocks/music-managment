class IsVariousFix < ActiveRecord::Migration[4.2]
  def self.up
    change_column :albums, :is_various, :string, :limit => 1, :default => "0", :null => false
  end

  def self.down
    change_column :albums, :is_various, :string, :limit => 1, :default => "0", :null => false
  end
end
