class AlterPeopleTable < ActiveRecord::Migration[4.2]
  def self.up
    begin
    execute "
      ALTER TABLE people
      MODIFY id INT UNSIGNED NOT NULL auto_increment,
      MODIFY artist_id INT UNSIGNED DEFAULT NULL,
      MODIFY is_verified TINYINT UNSIGNED DEFAULT '0',
      MODIFY deleted TINYINT UNSIGNED DEFAULT '0',
      MODIFY label_id INT UNSIGNED DEFAULT NULL,
      MODIFY us_zip_code_id MEDIUMINT UNSIGNED DEFAULT NULL,
      MODIFY partner_id INT UNSIGNED DEFAULT NULL;
    "
    rescue
      puts "index already created"
    end
  end

  def self.down
  end
end
