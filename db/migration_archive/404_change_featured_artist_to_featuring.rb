class ChangeFeaturedArtistToFeaturing < ActiveRecord::Migration[4.2]
  def self.up
    execute "UPDATE creatives set role = 'featuring' where role = 'featured_artist'"
  end

  def self.down
    execute "UPDATE creatives set role = 'featured_artist' where role = 'featuring'"
  end
end
