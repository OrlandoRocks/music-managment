class CreateAnnualRenewals < ActiveRecord::Migration[4.2]
  def self.up
    create_table :annual_renewals do |t|
      t.column :album_id, :integer, :null => false
      t.column :subscription_renewal_due_event_id, :integer # the SubscriptionRenewalDue that created it
      # TODO: may also need to associate it with a SubscriptionRenewed event
      t.column :purchase_id, :integer
      t.column :price_policy_id, :integer, :null => false
      t.column :created_on, :date
    end
  end

  def self.down
    drop_table :annual_renewals
  end
end
