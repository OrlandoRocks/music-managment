class CreateAuditEntries < ActiveRecord::Migration[4.2]
  def self.up
    create_table :audit_entries do |t|
      t.column :action,     :string
      t.column :comment,    :string
      t.column :created_at, :datetime
      t.column :actor_type, :string
      t.column :actor_id,   :integer
    end
  end

  def self.down
    drop_table :audit_entries
  end
end
