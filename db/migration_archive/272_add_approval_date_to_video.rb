class AddApprovalDateToVideo < ActiveRecord::Migration[4.2]
  def self.up
    add_column :videos, :approval_date, :date
    add_column :videos, :metadata_picked_up, :boolean, :default => false
  end

  def self.down
    remove_column :videos, :approval_date
    remove_column :videos, :metadata_picked_up
  end
end
