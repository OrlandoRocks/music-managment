class CreateAnnualRenewalEvents < ActiveRecord::Migration[4.2]
  def self.up
    create_table :annual_renewal_events do |t|
      t.column :album_id, :integer, :null => false
      t.column :occurred_on, :date, :null => false
      t.column :event_type, :string, :limit => 40, :null => false
      t.column :overridden_by_admin_user, :string, :limit => 80
      t.column :created_on, :date, :null => false
      t.column :updated_at, :datetime
    end
    
    add_index(:annual_renewal_events, :album_id)
    add_index(:annual_renewal_events, :event_type)
  end

  def self.down
    drop_table :annual_renewal_events
  end
end
