class CreateBraintreeTransactions < ActiveRecord::Migration[4.2]
  def self.up
    upstring = %Q(
      CREATE TABLE  `braintree_transactions` (
        `id` int(10) UNSIGNED NOT NULL auto_increment,
        `person_id` int(10) UNSIGNED NOT NULL COMMENT 'Reference people table',
        `stored_credit_card_id` int(10) UNSIGNED NULL COMMENT 'Reference stored_credit_cards table',
        `invoice_id` int(10) UNSIGNED NULL COMMENT 'References invoices table',
        `related_transaction_id` int(10) UNSIGNED NULL COMMENT 'Self-referential for all refund transactions',
        `response_code` char(4) NOT NULL COMMENT 'Response code sent from Braintree',
        `avs_response` varchar(10) NULL COMMENT 'AVS Response code sent from Braintree',
        `cvv_response` varchar(10) NULL COMMENT 'Security Code (cvv) code sent from Braintree',
        `status` ENUM('success','fraud','declined','error','duplicate') DEFAULT 'success' NOT NULL COMMENT 'Status of the transaction',
        `action` ENUM('sale', 'refund') DEFAULT 'sale' NOT NULL COMMENT 'Type of transaction made',
        `transaction_id` varchar(35) NULL COMMENT 'References transaction ID stored at Braintree',
        `amount` DECIMAL(10,2) NOT NULL COMMENT 'Amount of transaction',
        `refunded_amount` DECIMAL(10,2) NULL COMMENT 'Cached total of all refunds against this transaction',
        `refund_category` varchar(50) NULL COMMENT 'category of refund as defined by CS and Business',
        `refund_reason` varchar(255) NULL COMMENT 'Reason given for refunding the original transaction',
        `refunded_by_id` int(10) UNSIGNED NULL COMMENT 'ID of Customer Service Rep who processed the refund', 
        `ip_address` char(15) NOT NULL,
        `raw_response` text NULL,
        `created_at` datetime default NULL,
        `updated_at` datetime default NULL,
        PRIMARY KEY  (`id`),
        KEY `person_id` (`person_id`),
        KEY `stored_credit_card_id` (`stored_credit_card_id`),
        KEY `related_transaction_id` (`related_transaction_id`),
        KEY `refund_category` (`refund_category`),
        KEY `invoice_id` (`invoice_id`),
        CONSTRAINT `fk_related_transaction_id` FOREIGN KEY (`related_transaction_id`) REFERENCES `braintree_transactions`(`id`),
        CONSTRAINT `fk_stored_credit_card_id` FOREIGN KEY (`stored_credit_card_id`) REFERENCES `stored_credit_cards`(`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
    
    execute upstring
  end

  def self.down
    drop_table :braintree_transactions
  end
end
