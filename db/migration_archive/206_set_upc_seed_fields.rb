class SetUpcSeedFields < ActiveRecord::Migration[4.2]
  def self.up
    seed_number = Upc.where(tunecore_upc: true).size

    seed = UpcSeed.create(:prefix => '8597', :seed => 1, :block_full => false) if Object.const_defined?("UpcSeed")

  end

  def self.down
  end
end
