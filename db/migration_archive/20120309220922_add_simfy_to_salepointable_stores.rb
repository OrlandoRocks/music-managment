class AddSimfyToSalepointableStores < ActiveRecord::Migration[4.2]
  def self.up
    storeid = Store.find_by(name: "simfy").id
    execute ("insert into salepointable_stores (salepointable_type,store_id) values ('Album','#{storeid}')")
    execute ("insert into salepointable_stores (salepointable_type,store_id) values ('Single','#{storeid}')")
  end

  def self.down
    storeid = Store.find_by(name: "simfy").id
    execute("delete from salepointable_stores where store_id = #{storeid}")
  end
end
