class CreateProductPurchaseItems < ActiveRecord::Migration[4.2]
  def self.up
    up_string = %Q(CREATE TABLE `product_purchase_items` (
      `id` int(11) NOT NULL auto_increment,
      `created_at` datetime NOT NULL,
      `purchase_id` int(11) NOT NULL,
      `purchaseable_id` int(11) NOT NULL,
      `purchaseable_type` ENUM('Album', 'Single', 'Ringtone', 'Entitlement', 'Product', 'Renewal', 'ItunesUserReport', 'Video', 'Salepoint', 'Song') NOT NULL,
      PRIMARY KEY  (`id`),
      KEY `ppi_purchase_id` (`purchase_id`),
      KEY `ppi_purchaseable` (`purchaseable_id`, `purchaseable_type`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
    
    execute up_string
  end

  def self.down
    drop_table :product_purchase_items
  end
end
