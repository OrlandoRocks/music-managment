class AddNewStores < ActiveRecord::Migration[4.2]
  def self.up
    stores = [["MusicNet","mn","MusicNet"],["Napster","np","Napster"]]
                
    stores.each { |store|
      Store.create :name=>store[0],:abbrev=>store[1],:short_name=>store[2]
    }
  
  end

  def self.down
    
  end
end
