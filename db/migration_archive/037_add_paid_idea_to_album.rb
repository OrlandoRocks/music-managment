class AddPaidIdeaToAlbum < ActiveRecord::Migration[4.2]
  def self.up
         add_column :albums, "paid_idea", :boolean, :default => false   
  end

  def self.down
         remove_column :albums, "paid_idea"
  end
end
