class AddPhoneNumber < ActiveRecord::Migration[4.2]
  def self.up
    add_column :check_transfers, :phone_number, :string
    
  end

  def self.down
    remove_column :check_transfers, :phone_number
  end
end
