class AddPricePoliesForShipping < ActiveRecord::Migration[4.2]
  def self.up
    PricePolicy.create(:short_code => 'shipping_label', :price_calculator => 'current_shipping', :base_price_cents => 0, :description => "Video Shipping Label")
  end

  def self.down
    PricePolicy.find_by(short_code: 'shipping_label').destroy
  end
end
