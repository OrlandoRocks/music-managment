class LengthenSongName < ActiveRecord::Migration[4.2]
  def self.up
    change_column(:songs, :name, :string, :limit => 255)
  end

  def self.down
    change_column(:songs, :name, :string, :limit => 80)
  end
end
