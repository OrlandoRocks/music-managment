class CreateUpcs < ActiveRecord::Migration[4.2]
  def self.up
      create_table :upcs do |t|
        t.column :number, :string
        t.column :upcable_id, :integer
        t.column :upcable_type, :string
        t.column :tunecore_upc, :boolean
        t.column :inactive, :boolean, :default => false
      end
  end

  def self.down
    drop_table :upcs
  end
end
