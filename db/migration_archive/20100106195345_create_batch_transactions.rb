class CreateBatchTransactions < ActiveRecord::Migration[4.2]
  def self.up
       
    up_sql = %Q(CREATE TABLE `batch_transactions` (
      `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
      `payment_batch_id` INT(10) UNSIGNED NOT NULL COMMENT 'Foreign Key to payment_batch table',
      `invoice_id` INT(11) NOT NULL COMMENT 'Foreign Key to invoice table',
      `stored_credit_card_id` INT(10) UNSIGNED NULL COMMENT 'Foreign Key to stored credit card table (for braintree transactions)',
      `amount` DECIMAL(10,2) NOT NULL DEFAULT '0.00' COMMENT 'Amount',
      `processed` ENUM('pending','processed','cannot_process') NOT NULL DEFAULT 'pending' COMMENT 'Current State of process',
      `updated_at` TIMESTAMP NULL COMMENT 'Datetime of latest update',
      `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',
      `matching_id` INT(10) UNSIGNED NULL COMMENT 'Polymorphic foreign key',
      `matching_type` VARCHAR(255) NULL COMMENT 'Polymorphic foreign key type',
      PRIMARY KEY  (`id`),
      KEY `payment_batch_id` (`payment_batch_id`),
      KEY `stored_credit_card_id` (`stored_credit_card_id`),
      KEY `invoice_id` (`invoice_id`),
      CONSTRAINT `FK_stored_credit_card` FOREIGN KEY (`stored_credit_card_id`) REFERENCES `stored_credit_cards`(`id`),
      CONSTRAINT `FK_payment_batch` FOREIGN KEY (`payment_batch_id`) REFERENCES `payment_batches` (`id`)
    ) ENGINE=INNODB DEFAULT CHARSET=utf8;)
                  
    execute up_sql
  end

  def self.down
    drop_table :batch_transactions
  end
end
