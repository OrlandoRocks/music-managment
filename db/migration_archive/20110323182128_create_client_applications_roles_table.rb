class CreateClientApplicationsRolesTable < ActiveRecord::Migration[4.2]
  def self.up
    create_table :client_applications_roles do |t|
      t.integer :client_application_id
      t.integer :role_id
    end
  end

  def self.down
    drop_table :client_applications_roles
  end
end
