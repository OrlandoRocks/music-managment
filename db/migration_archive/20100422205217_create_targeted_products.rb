class CreateTargetedProducts < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `targeted_products` (
              `id` int(10) unsigned NOT NULL auto_increment COMMENT 'Primary Key',
              `targeted_offer_id` int(10) unsigned NOT NULL COMMENT 'id of the offer this person should see',
              `product_id` int(10) unsigned NOT NULL COMMENT 'id of the product to adjust the price of',
              `display_name` varchar(50) NULL COMMENT 'overriding display_name for targeted product',
              `price_adjustment_type` enum('percentage_off','override','dollars_off','none') NOT NULL default 'percentage_off' COMMENT 'determines which action should be used when adjusting a products price',
              `price_adjustment` decimal(10,2) NOT NULL default '0.00' COMMENT 'decimal value to use in adjusting the price of the selected product',
              `price_adjustment_description` varchar(75) NULL COMMENT 'admin specified description for the price adjustment...shows on price',
              `flag_text` varchar(15) NULL COMMENT 'override text to display in product lists',
              `show_expiration_date` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'specifies whether or not the expiration date of the offer is shown along with the product',
              `sort_order` smallint(6) NOT NULL default '0',
              `created_at` timestamp NULL default CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',
              PRIMARY KEY  (`id`),
              KEY `targeted_offer_id` (`targeted_offer_id`),
              KEY `product_id` (`product_id`),
              CONSTRAINT `fk_targeted_offer_offer_id` FOREIGN KEY (`targeted_offer_id`) REFERENCES `targeted_offers` (`id`),
              CONSTRAINT `fk_targeted_product_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
            
            
    execute up_sql
  end

  def self.down
    drop_table :targeted_products
  end
end
