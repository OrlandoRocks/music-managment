class AddTimeSpanToAnnualRenewalPurchase < ActiveRecord::Migration[4.2]
  def self.up
    add_column :annual_renewals, :number_of_months, :integer, :default => 12
    add_column :annual_renewals, :promotion_identifier, :string, :limit => 30, :default => "Annual Renewal"
  end

  def self.down
    remove_column :annual_renewals, :number_of_days
    remove_column :annual_renewals, :promotion_identifier
  end
end
