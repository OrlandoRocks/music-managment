class AddNapsterCheck < ActiveRecord::Migration[4.2]
  def self.up
     add_column :albums, :napster_check, :boolean, :default => false
  end

  def self.down
     remove_column :albums, :napster_check
  end
end
