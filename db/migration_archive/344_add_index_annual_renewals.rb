class AddIndexAnnualRenewals < ActiveRecord::Migration[4.2]
  def self.up
    add_index(:annual_renewals, [:album_id, :created_on])
  end

  def self.down
    remove_index(:annual_renewals, [:album_id, :created_on]) 
  end
end

# NOTE: WATCH FOR FILESORT IN EXTRA COL WHEN USING COMBINED INDEXES (ORDER BY)

# BEFORE 

# mike@joba /var/www/tunecore/release-4.0/lib/tasks $ rake benchmark:upload_select
# (in /var/www/tunecore/release-4.0)
# Rehearsal ------------------------------------------
# select   0.230000   0.030000   0.260000 (  0.821590)
# --------------------------------- total: 0.260000sec
# 
#              user     system      total        real
# select   0.180000   0.010000   0.190000 (  0.733440)

# AFTER 

# mike@joba /var/www/tunecore/release-4.0/lib/tasks $ rake benchmark:upload_select
# (in /var/www/tunecore/release-4.0)
# Rehearsal ------------------------------------------
# select   0.160000   0.080000   0.240000 (  0.268808)
# --------------------------------- total: 0.240000sec
# 
#              user     system      total        real
# select   0.150000   0.060000   0.210000 (  0.241904)


# mike@joba ~ $ mysql -u root tunecore_development -e "explain SELECT * FROM annual_renewals WHERE (annual_renewals.album_id = 55437) ORDER BY created_on DESC LIMIT 1;"
# +----+-------------+-----------------+------+--------------------------------------------------+--------------------------------------------------+---------+-------+------+-------------+
# | id | select_type | table           | type | possible_keys                                    | key                                              | key_len | ref   | rows | Extra       |
# +----+-------------+-----------------+------+--------------------------------------------------+--------------------------------------------------+---------+-------+------+-------------+
# |  1 | SIMPLE      | annual_renewals | ref  | index_annual_renewals_on_album_id_and_created_on | index_annual_renewals_on_album_id_and_created_on | 4       | const |    1 | Using where | 
# +----+-------------+-----------------+------+--------------------------------------------------+--------------------------------------------------+---------+-------+------+-------------+

