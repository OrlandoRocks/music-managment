class AddOwnershipFlagField < ActiveRecord::Migration[4.2]
  def self.up
    add_column :people, :temp_ownership_flag, :boolean, :default => false, :null => false
    # BY default the flag is set to 1, when an admin takes ownership of the album the flag changes to 0
    # to prevent an admin from taking control of more than one album at a time, and overwrite the albums temp_person field
    # We don't want to lose the id of the albums original owner.. I am open to any suggestions.
    Person.all.each { |p| p.update_attribute(:temp_ownership_flag, false) }
  end

  def self.down
    remove_column :people, :temp_ownership_flag
  end
end
