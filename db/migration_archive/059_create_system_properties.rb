class CreateSystemProperties < ActiveRecord::Migration[4.2]
  def self.up
    create_table :system_properties do |t|
      t.column "name", :string
      t.column "value", :string
      t.column "description", :string
      t.column "created_at", :datetime
      t.column "updated_at", :datetime
    end
    SystemProperty.set(:approval_required_mass_pay_threshold, "50", "Maximum payment size in USD to automatically process on PayPal without admin approval.")
  end

  def self.down
    drop_table :system_properties
  end
end
