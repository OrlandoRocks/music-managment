class AddReferralCampaign < ActiveRecord::Migration[4.2]
  def self.up
    add_column :people, :referral_campaign, :string
    add_column :people, :referral_type, :string, :limit => 10
  end
  def self.down
    remove_column :people, :referral_campaign
    remove_column :people, :referral_type
  end
end
