class AddVideoPricePolicy < ActiveRecord::Migration[4.2]
  def self.up
    PricePolicy.create(:short_code => 'video', :price_calculator => 'current_video', :base_price_cents => 12500, :description => "Video")
  end

  def self.down
    PricePolicy.find_by(short_code: 'video').destroy
  end
end
