class AddOptinFlagToPeople < ActiveRecord::Migration[4.2]
  def self.up
     add_column :people, :is_opted_in, :boolean, :default => false

     Person.where(is_verified: 1).each do |s|
       s.update_attribute(:is_opted_in,true)
     end
   end

   def self.down
     remove_column :people, :is_opted_in
   end
end
