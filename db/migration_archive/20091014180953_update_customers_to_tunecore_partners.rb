class UpdateCustomersToTunecorePartners < ActiveRecord::Migration[4.2]
  def self.up
    partners = [
      {
        :name => "Tunecore",
        :domain => "tunecore.com",
        :skin => "tunecore_neo"
      },

      {
        :name => "Republic Records",
        :domain => "republicdd.com",
        :skin => "republic"
      },

      {
        :name => "Interscope Digital Distribution",
        :domain => "interscopedigitaldistribution.com",
        :skin => "interscope"
      },

      {
        :name => "UniMo Digital Distribution",
        :domain => "unimodigitaldistribution.com",
        :skin => "unimo"
      },

      {
        :name => "Island Def Jam Digital Distribution",
        :domain => "idjfirstlook.com",
        :skin => "islanddj"
      }
    ]
    partners.each do |partner|
      Partner.create(partner)
    end

    tunecore_partners = Partner.where(domain: "tunecore.com")
    time = Time.now.to_s :db

    tunecore_partners.each do |tunecore|
      ActiveRecord::Base.connection.execute("
        INSERT INTO partners_people
        (partner_id, person_id, start_on) -- columns
        (
          SELECT #{tunecore.id}, id, '#{time}' FROM people
        )
      ")
    end
  end

  def self.down
    ActiveRecord::Base.connection.execute("DELETE FROM partners_people")
    ActiveRecord::Base.connection.execute("DELETE FROM partners")
  end
end
