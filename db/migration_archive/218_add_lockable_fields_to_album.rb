class AddLockableFieldsToAlbum < ActiveRecord::Migration[4.2]
  def self.up
    add_column :albums, :locked_by_type, :string
  end

  def self.down
    remove_column :albums, :locked_by_type
  end
end

