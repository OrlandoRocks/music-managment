class AddAlbumType < ActiveRecord::Migration[4.2]
  def self.up
    add_column :albums, :album_type, :string, :limit => 20, :default => 'full', :null => false
  end

  def self.down
    remove_column :albums, :album_type
  end
end
