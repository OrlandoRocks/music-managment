class AddNewInfoSharingPolicy < ActiveRecord::Migration[4.2]
  def self.up
		add_column :people, :info_sharing, :boolean, :default => nil
  end

  def self.down
		remove_column :people, :info_sharing
  end
end
