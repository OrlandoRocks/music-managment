class AddItunesUserReportPricePolicy < ActiveRecord::Migration[4.2]
  def self.up
    PricePolicy.create(:short_code => 'itunes_user_report', :price_calculator => 'current_itunes_user_report', :base_price_cents => 298, :description => "iTunes User Report")
  end

  def self.down
    PricePolicy.find_by(short_code: 'itunes_user_report').destroy
  end
end
