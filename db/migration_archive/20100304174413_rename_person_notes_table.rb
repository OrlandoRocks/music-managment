class RenamePersonNotesTable < ActiveRecord::Migration[4.2]
  def self.up
    execute("RENAME TABLE person_notes TO notes;")
  end

  def self.down
    execute("RENAME TABLE notes TO person_notes;")
  end
end
