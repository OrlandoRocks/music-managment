class CreateStoredCreditCards < ActiveRecord::Migration[4.2]
  def self.up
    upstring = %Q(
      CREATE TABLE  `stored_credit_cards` (
        `id` int UNSIGNED NOT NULL auto_increment,
        `person_id` int UNSIGNED NOT NULL COMMENT "References people table",
        `customer_vault_id` char(32) NOT NULL COMMENT "References external storage id at Braintree",
        `cc_type` ENUM("Visa", "Mastercard", "Discover", "American Express", "Diner's Club") default NULL,
        `last_four` char(4) default NULL COMMENT "Last four digits of stored credit card",
        `expiration_month` tinyint UNSIGNED default NULL COMMENT "expiration month of the stored credit card",
        `expiration_year` smallint UNSIGNED default NULL COMMENT "expiration year of credit card",
        `first_name` varchar(100) default NULL COMMENT "First name on card",
        `last_name` varchar(100) default NULL COMMENT "Last name on card",
        `company` varchar(100) default NULL COMMENT "Company name on card if applicable",
        `address1` varchar(255) default NULL COMMENT "Billing Address line 1",
        `address2` varchar(255) default NULL COMMENT "Billing Address line 2",
        `city` varchar(100) default NULL COMMENT "Billing Address city",
        `state` char(2) default NULL COMMENT "Billing Address State - ANSI CODE",
        `country` char(2) default NULL COMMENT "Billing Address country - ANSI CODE",
        `zip` varchar(10) default NULL COMMENT "Billing Address zip",
        `status` ENUM("new", "current", "processing_error", "retrieval_error", "destruction_error") default "new" NOT NULL COMMENT "defines the state of the scc record. current means it is up to date",
        `deleted_at` datetime default NULL COMMENT "date customer deleted credit card",
        `created_at` datetime default NULL,
        `updated_at` datetime default NULL,
        PRIMARY KEY  (`id`),
        KEY `person_id` (`person_id`),
        KEY `customer_vault_id` (`customer_vault_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8)

      execute upstring

  end

  def self.down
    drop_table :stored_credit_cards
  end
end
