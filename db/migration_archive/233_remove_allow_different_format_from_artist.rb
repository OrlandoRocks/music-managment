class RemoveAllowDifferentFormatFromArtist < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :artists, :allow_different_format
  end

  def self.down
    add_column :artists, :allow_different_format, :boolean, :default => false   
  end
end
