class AddNewRolesAndRights < ActiveRecord::Migration[4.2]
  def self.up
    admin_role = Role.find_or_create_by(name: 'Admin')
    admin_role.long_name = "Administrative Access"
    admin_role.description = "Give this user access to Tunecore.com Administrative functions."
    admin_role.is_administrative = true
    admin_role.save

    refund_role = Role.create(:name => "Refunds", :long_name => "Issue Refunds", :description => "Allow this user to issue credit card refunds.", :is_administrative => true)
    lock_role = Role.create(:name => "Lock", :long_name => "Unlock / Lock Accounts", :description => "Allow this user to either unlock or lock accounts based on fraudulent activity.", :is_administrative => true)
    rights_role = Role.create(:name => "Assign", :long_name => "Assign Rights", :description => "Allow this user to give other users administrative rights.", :is_administrative => true)

    process_refund = Right.create(:controller => "admin/transactions", :action => "process_refund", :name => "process_refund")
    refund = Right.create(:controller => "admin/transactions", :action => "refund", :name => "refund")
    lock = Right.create(:controller => "admin/people", :action => "lock_account", :name => "lock")
    unlock = Right.create(:controller => "admin/people", :action => "unlock_account", :name => "unlock")
    edit_rights = Right.create(:controller => "admin/rights", :action => "edit", :name => "edit_rights")
    update_rights = Right.create(:controller => "admin/rights", :action => "update", :name => "update_rights")

    refund_role.rights << [process_refund, refund]
    lock_role.rights << [lock, unlock]
    rights_role.rights << [edit_rights, update_rights]

    # give Taylor, David, and Chris the correct roles to give out permissions
    taylor = Person.find_by(email: 'taylor@tunecore.com')
    jeff = Person.find_by(email: 'jeff@tunecore.com')
    gian = Person.find_by(email: 'gian@tunecore.com')
    ewald = Person.find_by(email: 'ewald@tuencore.com')
    david = Person.find_by(email: 'david@tunecore.com')
    chris = Person.find_by(email: 'chris@tunecore.com')

    taylor.roles << [refund_role, lock_role] rescue nil
    jeff.roles << [refund_role, lock_role, rights_role] rescue nil
    gian.roles << [refund_role, lock_role, rights_role] rescue nil
    ewald.roles << [refund_role, lock_role, rights_role] rescue nil
    chris.roles << [refund_role, lock_role, rights_role] rescue nil
    david.roles << [refund_role, lock_role, rights_role] rescue nil
  end

  def self.down

  end
end
