class AddMissingIndexes < ActiveRecord::Migration[4.2]
  def self.up  
    execute "ALTER TABLE annual_renewals ADD INDEX album_id_idx (album_id);"
    execute "ALTER TABLE invoice_settlements ADD INDEX source_id_idx (source_id);"
    execute "ALTER TABLE paypal_ipns ADD INDEX paypal_txn_id_idx (paypal_txn_id(8));"
    execute "ALTER TABLE bandpages ADD INDEX person_id_idx (person_id);"
    execute "ALTER TABLE paypal_transfers ADD INDEX person_id_idx (person_id);"
    execute "ALTER TABLE check_transfers ADD INDEX person_id_idx (person_id);"
    execute "ALTER TABLE scheduled_mailings ADD INDEX mailing_id_idx (mailing_id);"
  end

  def self.down
    execute "ALTER TABLE annual_renewals DROP INDEX album_id_idx;"
    execute "ALTER TABLE invoice_settlements DROP INDEX source_id_idx;"
    execute "ALTER TABLE paypal_ipns DROP INDEX paypal_txn_id_idx;"
    execute "ALTER TABLE bandpages DROP INDEX person_id_idx;"
    execute "ALTER TABLE paypal_transfers DROP INDEX person_id_idx;"
    execute "ALTER TABLE check_transfers DROP INDEX person_id_idx;"
    execute "ALTER TABLE scheduled_mailings DROP INDEX mailing_id_idx;"
  end
end
