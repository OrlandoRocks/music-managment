class InvoiceIndexes < ActiveRecord::Migration[4.2]
  def self.up
    add_index(:invoices, 
              [:person_id,:created_at,:final_settlement_amount_cents,:settled_at], 
              :name => :invoice_details)
    add_index(:invoice_items,
              [:invoice_id, :target_id, :target_type],
              :name => :invoice_item_details)
  end

  def self.down
    remove_index(:invoices, :name => :invoice_details)
    remove_index(:invoice_items, :name => :invoice_item_details)
  end
end
