class AddDeezerToStores < ActiveRecord::Migration[4.2]

    def self.up
      Store.create(:name=>'Deezer',:abbrev=>'dz', :short_name=>'Deezer', :position=>290, :needs_rights_assignment=>false, :is_active=>false,:base_price_policy_id=>"3" )
    end

    def self.down
      execute("DELETE FROM stores where name = 'Deezer'")
    end
end
