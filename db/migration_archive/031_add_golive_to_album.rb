class AddGoliveToAlbum < ActiveRecord::Migration[4.2]
  def self.up
    add_column :albums, :golive_date, :datetime, :null => true, :default => nil
  end

  def self.down
    remove :albums, :golive_date, :datetime
  end
end
