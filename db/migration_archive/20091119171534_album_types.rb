class AlbumTypes < ActiveRecord::Migration[4.2]
  def self.up
    begin
    execute("
      alter table albums
      add column album_type enum('Album','Single','Ringtone') DEFAULT 'Album' NULL after partner_id, 
      change album_type album_type_OLD varchar(20) character set utf8 collate utf8_general_ci default 'full' NOT NULL;
    ")
    rescue

    end
    execute("UPDATE albums SET album_type = 'Album' where album_type_OLD = 'full'")
    execute("UPDATE albums SET album_type = 'Single' where album_type_OLD = 'single'")
  end

  def self.down
  end
end
