class CreateManufacturingOrders < ActiveRecord::Migration[4.2]
  def self.up
    create_table :manufacturing_orders do |t|
      t.column :o_name, :string
      t.column :o_address1, :string
      t.column :o_address2, :string
      t.column :o_city, :string
      t.column :o_state, :string
      t.column :o_zip, :string
      t.column :o_phone, :string
      t.column :o_email, :string
      t.column :s_name, :string
      t.column :s_address1, :string
      t.column :s_address2, :string
      t.column :s_city, :string
      t.column :s_state, :string
      t.column :s_zip, :string
      t.column :pi_album, :string
      t.column :pi_artist, :string
      t.column :pi_upc, :string
      t.column :assign_upc, :boolean
      t.column :pd_run, :string
      t.column :pd_packaging, :string
      t.column :quantity, :integer
      t.column :jd_panels, :string
      t.column :jd_insert, :string
      t.column :jd_traycard, :string
      t.column :cw_wallets, :string
      t.column :d_type, :string
      t.column :tray, :string
      t.column :shrinkwrap, :string
      t.column :shrinkwrap_quantity, :integer
      t.column :topspine, :string
      t.column :topspine_quantity, :integer
      t.column :special_instructions, :text
      t.column :send_barcode, :boolean
      
    end
  end

  def self.down
    drop_table :manufacturing_orders
  end
end
