class AddAlternateUploadToPeople < ActiveRecord::Migration[4.2]
  def self.up
    add_column :people, :alternate_upload, :boolean, :null => false, :default => false
  end

  def self.down
    remove_column :people, :alternate_upload
  end
end
