class PopulateDuplicatedIsrcs < ActiveRecord::Migration[4.2]
  def self.up
    # Trevor says: commented out because this will be done manually,
    # not a problem if already done, the manual process involves cleaning
    # and re-populating
    # DuplicatedIsrc.populate
  end

  def self.down
  end
end
