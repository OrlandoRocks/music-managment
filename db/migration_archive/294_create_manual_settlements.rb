class CreateManualSettlements < ActiveRecord::Migration[4.2]
  def self.up
    create_table :manual_settlements do |t|
      t.column :created_at, :datetime, :null => false
      t.column :invoice_id, :integer, :null => false
      t.column :person_id, :integer, :null => false
      t.column :settlement_amount_cents, :integer, :null => false
      t.column :comment, :text
    end    
  end

  def self.down
    drop_table :manual_settlements
  end
end
