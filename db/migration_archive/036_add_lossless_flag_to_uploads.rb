class AddLosslessFlagToUploads < ActiveRecord::Migration[4.2]
  def self.up
     add_column :uploads, "is_probably_lossless", :boolean, :default => false   
  end

  def self.down
    remove_column :uploads, :is_probably_lossless
  end
end
