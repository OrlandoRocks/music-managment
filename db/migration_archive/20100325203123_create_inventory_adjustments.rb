class CreateInventoryAdjustments < ActiveRecord::Migration[4.2]
  
  def self.up
    up_sql = %Q(CREATE TABLE `inventory_adjustments` (
      `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
      `inventory_id` INT(10) UNSIGNED NOT NULL COMMENT 'Foreign Key to Inventory Table',
      `related_id` INT(10) UNSIGNED DEFAULT NULL COMMENT 'Self foreign key',
      `rollback` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Indicated if record is rollback',
      `posted_by_id` INT(10) UNSIGNED NOT NULL COMMENT 'Foreign Key to people table',
      `posted_by_name` VARCHAR(255) DEFAULT NULL COMMENT 'Name of admin for auditing purposes',
      `amount` INT(10) NOT NULL DEFAULT '0' COMMENT 'Amount to credit account',
      `category` ENUM('Other','Refund','Error') NOT NULL DEFAULT 'Other' COMMENT 'Type of adjustment',
      `admin_note` VARCHAR(255) DEFAULT NULL COMMENT 'Adjustment note for Admin only',
      `updated_at` TIMESTAMP NULL COMMENT 'Datetime of latest update',
      `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',
      PRIMARY KEY  (`id`),
      KEY `inventory_id` (`inventory_id`),
      KEY `related_id` (`related_id`),
      KEY `posted_by_id` (`posted_by_id`),
      CONSTRAINT `FK_inventory` FOREIGN KEY (`inventory_id`) REFERENCES `inventories` (`id`),
      CONSTRAINT `FK_inv_adj_people_by` FOREIGN KEY (`posted_by_id`) REFERENCES `people` (`id`),
      CONSTRAINT `FK_inv_adj_self` FOREIGN KEY (`related_id`) REFERENCES `inventory_adjustments` (`id`)
    ) ENGINE=INNODB DEFAULT CHARSET=utf8;)
                  
    execute up_sql
  end

  def self.down
    drop_table :inventory_adjustments
  end
end
