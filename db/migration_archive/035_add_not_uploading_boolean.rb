class AddNotUploadingBoolean < ActiveRecord::Migration[4.2]
  def self.up
    add_column :albums, "is_not_uploading", :boolean, :default => false   
  end

  def self.down
    remove_column :albums, :is_not_uploading
  end
end
