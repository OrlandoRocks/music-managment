class RemoveGroupieTunes < ActiveRecord::Migration[4.2]
  def self.up
		store = Store.find_by(abbrev: 'gp')
		store.destroy		
  end

  def self.down
    Store.create(:name=> "GroupieTunes", :abbrev=> "gp", :short_name=> "GroupieTunes")
		Store.update_all('position = id')
  end
end
