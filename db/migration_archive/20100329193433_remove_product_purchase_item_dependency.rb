class RemoveProductPurchaseItemDependency < ActiveRecord::Migration[4.2]
  def self.up
    execute "ALTER TABLE purchases DROP COLUMN sti_type, DROP COLUMN related_type, CHANGE related_id product_id int(10) UNSIGNED"
    execute "ALTER TABLE purchases ADD COLUMN related_id int(10) UNSIGNED, 
            ADD COLUMN related_type ENUM('Album','Single','Ringtone','Entitlement','Product','Renewal','ItunesUserReport','Video','Salepoint','Song') DEFAULT NULL,
            ADD COLUMN targeted_product_id int(10) UNSIGNED NULL"
    execute "ALTER TABLE purchases ADD INDEX ix_purchases_on_product_id (`product_id`),
            ADD INDEX ix_purchases_on_related (`related_id`, `related_type`),
            ADD INDEX ix_purchases_targeted_product_id (`targeted_product_id`)"
  end

  def self.down
    execute "ALTER TABLE purchases DROP COLUMN related_id, 
            MODIFY COLUMN product_id related_id int(10) UNSIGNED, 
            ADD COLUMN sti_type ENUM('ProductPurchase'), 
            DROP COLUMN targeted_product_id"
    execute "ALTER TABLE purchases MODIFY COLUMN related_type ENUM('Product') DEFAULT NULL"
    execute "ALTER TABLE purchases ADD INDEX `ix_purchases_on_related_id` (`related_id`)"
  end
end
