class CreateEftBatchTransactionHistory < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `eft_batch_transaction_history` (
      `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
      `eft_batch_transaction_id` INT(10) UNSIGNED NOT NULL COMMENT 'Foreign Key to batch_transaction table',
      `amount` DECIMAL(10,2) NULL COMMENT 'Amount',
      `status` ENUM('requested','debit_amount','debit_service_fee','canceled','rejected', 'rollback_debit', 'rollback_service_fee', 'approved','removed_approval', 'processing', 'error', 'sent_to_bank', 'success','failure','debit_failure_fee', 'email_sent') NOT NULL DEFAULT 'requested' COMMENT 'Current State of process',
      `person_transaction_id` INT(11) NULL COMMENT 'Foreign Key to Person Tranasctions table',
      `posted_by_id` INT(10) UNSIGNED NULL COMMENT 'Foreign Key to People table',
      `posted_by_name` varchar(255) NULL COMMENT 'Admin name who approved/rejected/unapproved',
      `comment` varchar(255) NULL COMMENT "Generic Text field used for responses, comments, etc.",
      `updated_at` TIMESTAMP NULL COMMENT 'Datetime of latest update',
      `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',
      PRIMARY KEY  (`id`),
      KEY `eftbh_eft_batch_transaction_id` (`eft_batch_transaction_id`),
      KEY `eftbh_person_transaction_id` (`person_transaction_id`),
      KEY `eftbh_posted_by_id` (`posted_by_id`),
      CONSTRAINT `FK_eft_history_eft_batch_transaction` FOREIGN KEY (`eft_batch_transaction_id`) REFERENCES `eft_batch_transactions` (`id`),
      CONSTRAINT `FK_eft_history_person_transaction` FOREIGN KEY (`person_transaction_id`) REFERENCES `person_transactions` (`id`),
      CONSTRAINT `FK_eft_history_posted_by_id` FOREIGN KEY (`posted_by_id`) REFERENCES `people` (`id`)
    ) ENGINE=INNODB DEFAULT CHARSET=utf8;)
                  
    execute up_sql
  end

  def self.down
    drop_table :eft_batch_transaction_history
  end
end
