class AddRankingToAlbum < ActiveRecord::Migration[4.2]
  def self.up
    add_column :albums, :song_sale_count, :integer
    add_column :albums, :album_sale_count, :integer
    add_column :albums, :paid_streams, :integer
    add_column :albums, :promo_streams, :integer
    add_column :albums, :promo_downloads, :integer
    add_column :albums, :total_revenue_cents, :integer
  end

  def self.down
    remove_column :albums, :song_sale_count
    remove_column :albums, :album_sale_count
    remove_column :albums, :paid_streams
    remove_column :albums, :promo_streams
    remove_column :albums, :promo_downloads
    remove_column :albums, :total_revenue_cents
  end
end
