class AddTemplateIdToSalepoints < ActiveRecord::Migration[4.2]
  def self.up
    add_column :salepoints, :template_id, :integer
  end

  def self.down
    remove_column :salepoints, :template_id
  end
end
