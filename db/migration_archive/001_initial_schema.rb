class InitialSchema < ActiveRecord::Migration[4.2]
  def self.up
    
      create_table "albums", :force => true do |t|
        t.column "user_id", :integer, :default => 0, :null => false
        t.column "name", :text, :default => "", :null => false
        t.column "title_version", :string, :limit => 128
        t.column "label", :string, :limit => 128
        t.column "p_copyright_year", :date
        t.column "c_copyright_year", :date
        t.column "p_copyright", :string, :limit => 128
        t.column "c_copyright", :string, :limit => 128
        t.column "upc_ean", :text
        t.column "price_id", :string, :limit => 3, :default => "", :null => false
        t.column "release_date", :date
        t.column "sale_date", :date
        t.column "orig_release_year", :date
        t.column "recording_location", :string, :limit => 128
        t.column "parental_advisory", :string, :limit => 1, :default => "0", :null => false
        t.column "liner_notes", :text
        t.column "created_on", :date, :null => false
        t.column "album_state", :string, :limit => 50, :default => "entered"
        t.column "is_various", :string, :limit => 1, :default => "f", :null => false
      end

      create_table "albums_countries", :id => false, :force => true do |t|
        t.column "country_id", :integer, :default => 0, :null => false
        t.column "album_id", :integer, :default => 0, :null => false
      end

      add_index "albums_countries", ["country_id"], :name => "country_id"
      add_index "albums_countries", ["album_id"], :name => "title_id"

      create_table "albums_genres", :id => false, :force => true do |t|
        t.column "genre_id", :integer, :default => 0, :null => false
        t.column "album_id", :integer, :default => 0, :null => false
      end

      create_table "albums_states", :force => true do |t|
        t.column "albumstatus", :string, :limit => 32, :default => "entered", :null => false
      end

      create_table "artists", :force => true do |t|
        t.column "name", :string, :limit => 120
      end

      create_table "artists_songs", :id => false, :force => true do |t|
        t.column "artist_id", :integer
        t.column "song_id", :integer
      end

      create_table "artists_users", :id => false, :force => true do |t|
        t.column "artist_id", :integer
        t.column "user_id", :integer
      end

      create_table "artworks", :force => true do |t|
        t.column "uploaded", :boolean, :default => false, :null => false
        t.column "file", :string
        t.column "album_id", :integer, :default => 0, :null => false
      end

      create_table "countries", :force => true do |t|
        t.column "name", :string, :limit => 60, :default => "", :null => false
        t.column "abbrev", :string, :limit => 2, :default => "", :null => false
        t.column "price_cents", :integer, :default => 99, :null => false
      end

      create_table "genres", :force => true do |t|
        t.column "name", :string, :limit => 128, :default => "", :null => false
        t.column "parent_id", :integer, :default => 0, :null => false
      end

      create_table "items", :force => true do |t|
        t.column "name", :string, :limit => 48, :default => "0", :null => false
        t.column "price_cents", :integer, :default => 0, :null => false
      end

      create_table "line_items", :force => true do |t|
        t.column "order_id", :integer
        t.column "item_id", :integer
        t.column "unit_price", :integer
        t.column "quantity", :integer, :default => 1, :null => false
      end

      create_table "orders", :force => true do |t|
        t.column "tempuser_id", :integer
        t.column "user_id", :integer
        t.column "created_on", :datetime
        t.column "expiry_date", :datetime
        t.column "orderstatus", :string, :limit => 32, :default => "temp"
      end

      create_table "orders_states", :force => true do |t|
        t.column "orderstatus", :string, :limit => 32, :default => "temp", :null => false
      end

      create_table "songs", :force => true do |t|
        t.column "name", :string, :limit => 80, :default => "", :null => false
        t.column "album_id", :integer, :default => 0, :null => false
        t.column "upload_id", :integer
        t.column "mp3_ident", :string, :limit => 128, :default => "", :null => false
        t.column "created_on", :date, :null => false
        t.column "songstatus", :string, :limit => 32, :default => "entered", :null => false
        t.column "track_num", :integer
      end

      create_table "songs_states", :force => true do |t|
        t.column "songstatus", :string, :limit => 32, :default => "entered", :null => false
      end
      
      create_table "uploads", :force => true do |t|
        t.column "song_id", :integer
        t.column "original_filename", :string
        t.column "uploaded_filename", :string
        t.column "converted_filename", :string
        t.column "filetype", :string, :limit => 32
        t.column "bitrate", :integer
      end

      create_table "uploads_files", :id => false, :force => true do |t|
        t.column "filetype", :string, :limit => 32, :default => "mp3", :null => false
      end

      create_table "users", :force => true do |t|
        t.column "login", :string, :limit => 80, :default => "", :null => false
        t.column "salted_password", :string, :limit => 40, :default => "", :null => false
        t.column "email", :string, :limit => 60, :default => "", :null => false
        t.column "firstname", :string, :limit => 40
        t.column "lastname", :string, :limit => 40
        t.column "artistname", :string, :limit => 128
        t.column "salt", :string, :limit => 40, :default => "", :null => false
        t.column "verified", :integer, :default => 0
        t.column "role", :string, :limit => 40
        t.column "security_token", :string, :limit => 40
        t.column "token_expiry", :datetime
        t.column "deleted", :integer, :default => 0
        t.column "delete_after", :datetime
      end

      create_table :sessions do |t|
        t.column :session_id, :string
        t.column :data, :text
        t.column :updated_at, :datetime
      end
      
      add_index :sessions, :session_id

  end

  def self.down
    drop_table :sessions
  end
end
