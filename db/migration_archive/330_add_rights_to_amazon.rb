class AddRightsToAmazon < ActiveRecord::Migration[4.2]
  def self.up
    store = Store.find_by(name: 'Amazon MP3')
    store.needs_rights_assignment=true
    store.save
  end

  def self.down
    store = Store.find_by(name: 'Amazon MP3')
    store.needs_rights_assignment=false
    store.save
  end
end
