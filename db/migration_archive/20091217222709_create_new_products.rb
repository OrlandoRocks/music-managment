class CreateNewProducts < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `products`(
                `id` int UNSIGNED not null auto_increment,
                `original_product_id` int UNSIGNED null COMMENT 'references an original product that this was based off of - for comparison only',
                `created_by_id` int UNSIGNED not null COMMENT 'references people.id of admin who created the product',
                `name` varchar(150) not null COMMENT 'name of product',
                `description` text null COMMENT 'optional long description of product',
                `status` ENUM('Active', 'Inactive') not null default 'Inactive' COMMENT 'determines if the product is sold in the system',
                `product_type` ENUM('Ad Hoc', 'Package', 'Renewal') not null default 'Package' COMMENT 'determines how the product is sold',
                `renewal_level` ENUM('Product','Item','None') not null default 'Item' COMMENT 'tells the system if renewal rules are set at the product or product_item level',
                `first_renewal_duration` tinyint COMMENT 'deteremines (with renewal_interval) how often long before the first interval/duration expires',
                `first_renewal_interval` enum('month','day','week','year') null COMMENT 'determines (with renewal_duration) how long before the first interval/duration expires',
                `renewal_duration` tinyint COMMENT 'deteremines (with renewal_interval) how often an entitlement will renew',
                `renewal_interval` enum('month','day','week','year') null COMMENT 'determines (with renewal_duration) how often an entitlement will renew',
                `renewal_product_id` int UNSIGNED COMMENT 'references the product to be purchased when this item renews',
                `price` decimal(10,2) default 0.00 COMMENT 'price for Package types, not used for Ad Hoc products',
                `expires_at` timestamp null COMMENT 'date product can no longer be sold',
                `updated_at` timestamp null,
                `created_at` timestamp null,                
                PRIMARY KEY  (`id`),
                KEY `status` (`status`),
                KEY `product_type` (`product_type`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
            
    execute up_sql
  end

  def self.down
    drop_table :products
  end
end
