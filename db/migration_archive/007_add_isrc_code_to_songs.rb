class AddIsrcCodeToSongs < ActiveRecord::Migration[4.2]
  def self.up
    add_column :songs, :isrc, :string, :limit => 12
  end

  def self.down
    remove_column :songs, :isrc
  end
end
