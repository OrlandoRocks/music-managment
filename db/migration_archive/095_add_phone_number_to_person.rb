class AddPhoneNumberToPerson < ActiveRecord::Migration[4.2]
  def self.up
		add_column :people, :phone_number, :string, :null => true, :default => nil
  end

  def self.down
		remove_column :people, :phone_number
  end
end
