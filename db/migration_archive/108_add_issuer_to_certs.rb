class AddIssuerToCerts < ActiveRecord::Migration[4.2]
  def self.up
    add_column :certs, :issuer, :string, :limit => 30, :null => true, :default => nil
  end

  def self.down
    remove_column :certs, :issuer
  end
end
