class RemoveSpokenWord < ActiveRecord::Migration[4.2]
  #defined here to avoid broken dependency on genre-video join that doesn't exist at this point in the migrations
  class Genre < ApplicationRecord
    has_and_belongs_to_many :albums
  end

  def self.up
    genre = Genre.find_by(name: "Spoken Word")
    genre.destroy
  end

  def self.down
    Genre.create :name=>"Spoken Word"
  end
end
