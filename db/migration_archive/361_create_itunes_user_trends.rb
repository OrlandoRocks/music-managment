class CreateItunesUserTrends < ActiveRecord::Migration[4.2]
  def self.up
    create_table :itunes_user_trends do |t|
      #t.column :provider, :string, :limit => 15
      t.column :provider_country, :string, :limit => 2
      t.column :vendor_id, :string, :limit => 15
      t.column :upc, :string, :limit => 15
      t.column :isrc, :string, :limit => 20
      t.column :artist, :string
      t.column :title, :string
      t.column :label_name, :string, :limit => 25
      t.column :product_type, :string, :limit => 2
      t.column :units, :string, :limit => 5
      t.column :royalty_price, :string, :limit => 8
      t.column :download_date, :string
      t.column :order_id, :string, :limit => 12
      t.column :zipcode, :string, :limit => 12
      t.column :customer_id, :string, :limit => 12
      t.column :report_date, :string
      t.column :sale_return, :string, :limit => 2
      t.column :customer_currency, :string, :limit => 6
      t.column :country_code, :string, :limit => 2
      t.column :royalty_currency, :string, :limit => 6
      t.column :preorder, :string
      t.column :season_pass, :string
      t.column :isan, :string
      t.column :customer_price, :string, :limit => 5
      t.column :apple_id, :string, :limit => 19
      t.column :oma, :string
    end
  end

  def self.down
    drop_table :itunes_user_trends
  end
end
