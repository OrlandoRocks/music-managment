class AddArtistIdToSongs < ActiveRecord::Migration[4.2]
  def self.up
    add_column :songs, :artist_id, :integer, :null => true, :default => nil
  end

  def self.down
    remove_column :songs, :artist_id
  end
end
