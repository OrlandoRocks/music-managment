class MakingShippingLabelsChargeable < ActiveRecord::Migration[4.2]
  def self.up
    add_column :shipping_labels, :purchase_id, :integer
    add_column :shipping_labels, :printed, :boolean, :default => false
  end

  def self.down
    remove_column :shipping_labels, :purchase_id
    remove_column :shipping_labels, :printed
  end
end
