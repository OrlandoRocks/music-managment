class CreateInvoiceTables < ActiveRecord::Migration[4.2]
  def self.up
    create_table :invoices do |t|
      t.column :person_id, :integer, :null => false, :default => nil
      t.column :created_at, :datetime, :null => false, :default => nil
      t.column :final_settlement_amount_cents, :integer, :null => true, :default => nil
      t.column :settled, :boolean, :null => false, :default => false
    end
    
    create_table :invoice_items do |t|
      t.column :invoice_id, :integer, :null => false, :default => nil
      t.column :target_id, :integer, :null => false, :default => nil
      t.column :target_type, :string, :null => false, :default => nil, :limit => 30
      t.column :cost_at_settlement_cents, :integer, :null => true, :default => nil
    end
    
    create_table :invoice_settlements do |t|
      t.column :invoice_id, :integer, :null => false, :default => nil
      t.column :created_at, :datetime, :null => false, :default => nil
      t.column :settlement_amount_cents, :integer, :null => false, :default => nil
      t.column :source_id, :integer, :null => false, :default => nil
      t.column :source_type, :string, :limit => 50, :null => false, :default => nil
    end
  end

  def self.down
    drop_table :invoices
    drop_table :invoice_items
    drop_table :invoice_settlements
  end
end
