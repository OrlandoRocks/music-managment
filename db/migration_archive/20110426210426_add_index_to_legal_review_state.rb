class AddIndexToLegalReviewState < ActiveRecord::Migration[4.2]
  def self.up
    add_index(:albums, :legal_review_state, :name => "index_albums_on_legal_review_state")
  end

  def self.down
    remove_index(:albums, :legal_review_state)
  end
end
