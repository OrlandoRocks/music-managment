class MoveSampleCertYamls < ActiveRecord::Migration[4.2]
  class CertBatch < ApplicationRecord
  end

  def self.up
    CertBatch.all.each do |cb|

      unless cb.sample_cert_yaml.blank?
        #if the cert class is not loaded, YAML can not figure out how to instantiate the object (brilliant, right?)
        #we call Cert.name just to make sure the class is present
        Cert.name
        sample_cert = YAML::load(cb.sample_cert_yaml)

        cb.expiry_date = sample_cert.expiry_date
        cb.admin_only = sample_cert.admin_only
        cb.cert_engine = sample_cert.cert_engine
        cb.engine_params = sample_cert.engine_params
        cb.spawning_code = sample_cert.cert unless sample_cert.blank?
        cb.save
      end
    end
  end

  def self.down
  end
end
