class CreateBandpageViews < ActiveRecord::Migration[4.2]
  def self.up
    create_table :bandpage_views do |t|
      t.column :bandpage_id, :int, :null => false
      t.column :person_id, :int
      t.column :viewed_at, :datetime, :null => false
      t.column :http_referer, :string
      t.column :http_user_agent, :string
      t.column :http_remote_addr, :string, :limit => 128
      t.column :http_cookie, :string, :limit => 128
      t.column :http_accept_language, :string, :limit => 128
    end
    
    add_index :bandpage_views, :bandpage_id
    add_index :bandpage_views, :person_id
    add_index :bandpage_views, :viewed_at
    add_index :bandpage_views, :http_referer
    add_index :bandpage_views, :http_remote_addr
    add_index :bandpage_views, :http_cookie
    add_index :bandpage_views, :http_accept_language
  end

  def self.down
    drop_table :bandpage_views
  end
end
