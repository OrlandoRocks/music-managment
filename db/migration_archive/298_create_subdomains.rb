class CreateSubdomains < ActiveRecord::Migration[4.2]
  def self.up
    create_table :bandpages do |t|
      t.column :lookup_key, :string, :limit => 36
      t.column :views, :int, :default => 0
      t.column :person_id, :int
      t.column :about, :text
      t.column :artwork_id, :int
      t.column :title, :string, :limit => 128
      t.column :active_by_user, :boolean, :default => true
      t.column :active_by_tc, :boolean, :default => true 
    end
    
    add_index :bandpages, :lookup_key, :unique => true
    
  end

  def self.down
    drop_table :bandpages
  end
end
