class AddThirdAddressToShipping < ActiveRecord::Migration[4.2]
  def self.up
            add_column :manufacturing_orders, :shipping_address3, :string
  end

  def self.down
            remove_column :manufacturing_orders, :shipping_address3
  end
end
