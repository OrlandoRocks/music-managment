class RemoveOldRightsColumns < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :albums, :musicnet_check
    remove_column :albums, :emusic_check
    remove_column :albums, :napster_check
  end

  def self.down
    add_column :albums, :musicnet_check, :boolean, :null => false, :default => false
    add_column :albums, :emusic_check, :boolean, :null => false, :default => false
    add_column :albums, :napster_check, :boolean, :null => false, :default => false
  end
end
