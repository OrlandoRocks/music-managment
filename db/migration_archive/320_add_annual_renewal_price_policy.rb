class AddAnnualRenewalPricePolicy < ActiveRecord::Migration[4.2]
  def self.up
    PricePolicy.create(:short_code => 'annual_renewal', :price_calculator => 'current_annual_renewal', :base_price_cents => 0, :description => "Annual Renewal")
  end

  def self.down
    PricePolicy.find_by(short_code: 'annual_renewal').destroy
  end
end
