class ChangePromotions < ActiveRecord::Migration[4.2]
  def self.up
		add_column :promotions, :start, :datetime
		add_column :promotions, :finish, :datetime
		add_column :promotions, :on_homepage, :boolean
		remove_column :promotions, :posted
  end

  def self.down
		remove_column :promotions, :start
    remove_column :promotions, :finish
		remove_column :promotions, :on_homepage
    add_column :promotions, :posted, :datetime
  end
end
