class CertBatchesPartners < ActiveRecord::Migration[4.2]
  def self.up
    create_table :cert_batches_partners do |t|
      t.integer :cert_batch_id, :null => false
      t.integer :partner_id, :null => false
      t.timestamps
    end

    add_index :cert_batches_partners, [:cert_batch_id, :partner_id], :unique => true

    execute("ALTER TABLE cert_batches_partners MODIFY COLUMN partner_id smallint(5) unsigned")
    execute("
      ALTER TABLE cert_batches_partners
      ADD FOREIGN KEY (cert_batch_id)
      REFERENCES cert_batches(id)
      ON DELETE CASCADE;
    ")

    execute("
      ALTER TABLE cert_batches_partners
      ADD FOREIGN KEY (partner_id)
      REFERENCES partners(id)
      ON DELETE CASCADE;
    ")
  end

  def self.down
    drop_table :cert_batches_partners
  end
end
