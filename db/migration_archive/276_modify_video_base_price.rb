class ModifyVideoBasePrice < ActiveRecord::Migration[4.2]
  def self.up
    PricePolicy.find_by(short_code: 'video').update_attribute(:base_price_cents, 0)
  end

  def self.down
    PricePolicy.find_by(short_code: 'video').update_attribute(:base_price_cents, 12500)
  end
end
