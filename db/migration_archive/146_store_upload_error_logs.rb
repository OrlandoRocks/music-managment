class StoreUploadErrorLogs < ActiveRecord::Migration[4.2]
  def self.up
		add_column :songs, :upload_error_log, :string
	
  end

  def self.down
		remove_column :songs, :upload_error_log
  end
end
