class CreateTargetedAdvertisements < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `targeted_advertisements`(
                `id` INT(10) UNSIGNED not null auto_increment COMMENT 'Primary Key',
                `targeted_offer_id` INT(10) UNSIGNED not null COMMENT 'id of the offer this person should see',
                `advertisement_id`  INT(10) UNSIGNED not null COMMENT 'id of advertisment to display',
                `updated_at` TIMESTAMP NULL COMMENT 'Datetime of latest update',
                `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',
                PRIMARY KEY  (`id`),
                KEY `targeted_offer_id` (`targeted_offer_id`),
                KEY `advertisement_id` (`advertisement_id`),
                CONSTRAINT `fk_targeted_ad_offer_id` FOREIGN KEY (`targeted_offer_id`) REFERENCES `targeted_offers` (`id`),
                CONSTRAINT `fk_targeted_ad_ad_id` FOREIGN KEY (`advertisement_id`) REFERENCES `advertisements` (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
          
    execute up_sql
  end

  def self.down
    drop_table :targeted_advertisements
  end
end
