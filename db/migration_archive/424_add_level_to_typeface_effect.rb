class AddLevelToTypefaceEffect < ActiveRecord::Migration[4.2]
  def self.up
    add_column(:typeface_effects, :level, :integer)
    remove_column(:typeface_effects, :template)    
  end

  def self.down
    remove_column(:typeface_effects, :level)
    add_column(:typeface_effects, :template, :string)
  end
end
