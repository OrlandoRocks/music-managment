class RemoveUk < ActiveRecord::Migration[4.2]
  def self.up
    Store.where("abbrev = 'uk'").destroy_all
  end

  def self.down
    Store.create :name=>"iTunes UK",:abbrev=>"uk"
  end
end
