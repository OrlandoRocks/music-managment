class MoveAppleIdendifiers < ActiveRecord::Migration[4.2]
  def self.up
    execute "update albums left join upcs on albums.id = upcs.upcable_id and upcs.upcable_type = 'Album' left join itunes_comprehensives on upcs.number = itunes_comprehensives.upc set albums.apple_identifier = itunes_comprehensives.apple_id where albums.known_live_on is not null and itunes_comprehensives.apple_id is not null"
  
  end

  def self.down
    #downward migration not necessary
  end
end
