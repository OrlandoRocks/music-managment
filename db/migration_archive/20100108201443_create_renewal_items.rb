class CreateRenewalItems < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `renewal_items`(
                `id` int UNSIGNED not null auto_increment,
                `renewal_id` int UNSIGNED not null,
                `related_id` int UNSIGNED not null,
                `related_type` ENUM('Album','Single','Ringtone','Entitlement','Product','Video') NOT NULL,
                `created_at` timestamp null,                
                PRIMARY KEY  (`id`),
                KEY `related` (`related_id`, `related_type`),
                KEY `renewal` (`renewal_id`),
                CONSTRAINT `fk_renewal_items` FOREIGN KEY (`renewal_id`) REFERENCES `renewals`(`id`) 
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
                
    execute up_sql
  end

  def self.down
    drop_table  :renewal_items
  end
end
