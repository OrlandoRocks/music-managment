class CreateDataRecords < ActiveRecord::Migration[4.2]
  def self.up
    create_table :data_records do |t|
      t.column :created_at, :datetime, :null => false
      t.column :data_report_id, :integer, :null => false
      t.column :resolution_unit, :string, :limit => 10, :null => false
      t.column :resolution_identifier, :string, :limit => 20, :null => false
      t.column :measure, :float, :null => false
      t.column :description, :string, :limit => 150, :null => false
    end
    add_index :data_records, :data_report_id
    add_index :data_records, :resolution_unit
    add_index :data_records, [:data_report_id, :resolution_identifier, :description], :name => 'data_records_four_index', :unique => true   
  end

  def self.down
    drop_table :data_records
  end
end
