class AddLabelToPerson < ActiveRecord::Migration[4.2]
  def self.up
    add_column :people, "label_id", :integer
    # as of this migration the albums table alread has a label_id 
    create_table "labels", :force => true do |t|
       t.column "name", :string
     end
  end

  def self.down
    remove_column :people, "label_id"
    remove_column :albums, "label_id"   
    drop_table :labels 
  end
end
