#Created By:    Marcus Lofthouse
#Date:          2009-06-01
#Purpose:       The following migration file creates a messages table that
#               will be used to store tumble-log style messages that 
#               to be displayed on the customer's internal homepage.  
#               
#               There are currently 4 types of messages that we are planning
#               to distribute in order to keep our users informed about their
#               account as well as special promotions/broadcast messages from 
#               Tunecore.  
#
#               MESSAGE CODES:
#               ALERT (yellow)        Typical alert message might indicate to a user
#                                     that their album is stuck in processing, artwork 
#                                     issues, etc.
#
#               NOTICE (blue)         Notice message include positive notifications, such
#                                     as the fact that their album has gone live in iTunes
#
#               MONEY (green)         Money messages inform a user that they have sold
#                                     music and now have sales data.
#
#               PROMOTIONAL (white)   Promotional messages inform users of new offerings 
#                                     from Tunecore such as trending reports, widgets, socks
#             
#               Message content is populated by some external process, either by an admin 
#               manually, or by automated processes such as PETRI or a stored procedure.  
#               
#               Messages can be dismissed by the user (a process that sets the exiry_time). 
#               A reaper process will go through the DB (likely as a cron job) and delete expired
#               messages.  
#         
#               Messages should be brief. Think tweets or sms text messages.
################################################################################
#Column Description:
#id:            will be a unique identifier for a message
# 
#person_id:     should foreign key persons table
#code:          enum of the messages type.  Integers map to the message types 
#                 alert (yellow)
#                 alert-dismissed (faded yellow)
#                 notice (blue)
#                 notice-dismissed (faded blue)
#                 money (green)
#                 money-dismissed (faded green)
#                 promotion (white)
#text:          contains the short message to display to the user 
#viewed_at:     datetime type stores the time when the record was displayed on the customer's homepage
#expire_at:     datetime type saves the time when user dismisses the message, or the system calculated dismissal time
#created_at:    datetime type stores the time a record was created


################################################################################
################################################################################
class CreateMessages < ActiveRecord::Migration[4.2]
  def self.up
    create_table :messages do |t|
      t.column :person_id, :integer, :null => false
      t.column :code, :string, :limit => 25, :null => false
      t.column :text, :string, :limit => 256, :null => false
      t.column :viewed_at, :datetime
      t.column :expire_at, :datetime
      t.column :created_at, :datetime
    end
  end

  def self.down
    drop_table :messages
  end
end
