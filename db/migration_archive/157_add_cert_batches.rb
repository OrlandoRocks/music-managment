class AddCertBatches < ActiveRecord::Migration[4.2]
  def self.up
    create_table :cert_batches do |t|
      t.column :promotion, :string, :null => false
      t.column :description, :text
      t.column :created_at, :datetime, :null => false
      t.column :person_id, :integer
    end
    add_index :cert_batches, [:promotion], :unique => true
    add_column :certs, :cert_batch_id, :integer
    add_index :certs, [:cert_batch_id]

    #duplicate percent_off to engine_params
    Cert.connection.execute("update certs set engine_params = percent_off, cert_engine = 'DefaultPercent' where percent_off != 0 and engine_params is null and cert_engine is null")
    
  end
  def self.down
    drop_table :cert_batches
    remove_column :certs, :cert_batch_id
    Cert.connection.execute("update certs set engine_params = null, cert_engine = null where percent_off != 0 and engine_params is not null and cert_engine = 'DefaultPercent'")
  end
end
