class AddSistineAssetAndGenreData < ActiveRecord::Migration[4.2]
  def self.load_data(name = 'data', ext = 'sql')
    file_name = data_file(name, ext) || fail("Cannot find the data file for #{ name }")
    execute_sql_from_file(file_name)
  end

  def self.execute_sql_from_file(file_name)
    say_with_time("Executing SQL from #{ file_name }") do
      IO.readlines(file_name).join.gsub("\r\n", "\n").split(";\n").each do |s|
        execute(s) unless s == "\n"
      end
    end
  end

  def self.up
    execute_sql_from_file("#{RAILS_ROOT}/lib/sistine_data_to_import/data.sql")
  end

  def self.down
    # No-op
  end
end
