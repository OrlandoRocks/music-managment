class AddSongExternalAssetId < ActiveRecord::Migration[4.2]
  def self.up
    add_column :songs, :external_asset_id, :integer
  end

  def self.down
    remove_column :songs, :external_asset_id
  end
end
