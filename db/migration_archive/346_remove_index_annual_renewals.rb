class RemoveIndexAnnualRenewals < ActiveRecord::Migration[4.2]
  def self.up
    remove_index(:annual_renewals, [:album_id, :created_on])
  end

  def self.down
    # WHY CREATE A SELF.DOWN? DUNNO, so I won't.
  end
end
