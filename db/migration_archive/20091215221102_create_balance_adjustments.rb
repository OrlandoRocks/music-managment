class CreateBalanceAdjustments < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `balance_adjustments` (
      `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
      `related_id` INT(10) UNSIGNED DEFAULT NULL COMMENT 'Self foreign key',
      `rollback` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Indicated if record is rollback',
      `person_id` INT(10) UNSIGNED NOT NULL COMMENT 'Foreign Key to people table',
      `posted_by_id` INT(10) UNSIGNED NOT NULL COMMENT 'Foreign Key to people table',
      `posted_by_name` VARCHAR(255) DEFAULT NULL COMMENT 'Name of admin for auditing purposes',
      `debit_amount` DECIMAL(10,2) NOT NULL DEFAULT '0.00' COMMENT 'Debit Amount',
      `credit_amount` DECIMAL(10,2) NOT NULL DEFAULT '0.00' COMMENT 'Credit Amount',
      `category` ENUM('Refund - Renewal','Refund - Other','Service Adjustment','Other') NOT NULL DEFAULT 'Other' COMMENT 'Type of adjustment',
      `customer_note` VARCHAR(255) DEFAULT NULL COMMENT 'Adjustment note for Customer',
      `admin_note` VARCHAR(255) DEFAULT NULL COMMENT 'Adjustment note for Admin only',
      `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',
      PRIMARY KEY  (`id`),
      KEY `related_id` (`related_id`),
      KEY `person_id` (`person_id`),
      KEY `posted_by_id` (`posted_by_id`),
      CONSTRAINT `FK_people` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`),
      CONSTRAINT `FK_people_by` FOREIGN KEY (`posted_by_id`) REFERENCES `people` (`id`),
      CONSTRAINT `FK_self` FOREIGN KEY (`related_id`) REFERENCES `balance_adjustments` (`id`)
    ) ENGINE=INNODB DEFAULT CHARSET=utf8;)
                  
    execute up_sql
  end

  def self.down
    drop_table :balance_adjustments
  end
end
