class AddSqlToUserQueries < ActiveRecord::Migration[4.2]
  def self.up
    add_column :user_queries, :person_finder_sql, :text
  end

  def self.down
    remove_column :user_queries, :person_finder_sql
  end
end
