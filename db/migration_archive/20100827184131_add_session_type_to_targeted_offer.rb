class AddSessionTypeToTargetedOffer < ActiveRecord::Migration[4.2]
  def self.up
    execute "ALTER TABLE targeted_offers  MODIFY COLUMN status ENUM('New', 'Active', 'Inactive') DEFAULT 'New', 
                                          MODIFY COLUMN offer_type ENUM('new','existing','session'), 
                                          ADD COLUMN session_token varchar(50) AFTER join_token,
                                          ADD COLUMN single_use TINYINT(1) DEFAULT 0 AFTER offer_type"
  end

  def self.down
    execute "ALTER TABLE targeted_offers MODIFY COLUMN status ENUM('Active', 'Inactive') Default 'Inactive', MODIFY COLUMN offer_type ENUM('new','existing'), DROP COLUMN session_token, DROP column single_use"    
  end
end
