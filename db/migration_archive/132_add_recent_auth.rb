class AddRecentAuth < ActiveRecord::Migration[4.2]
  def self.up
    add_column :people, :recent_login, :datetime, :nil => true
    add_column :people, :recent_login_failure, :datetime, :nil => true
    add_index :people, [:recent_login], :name => 'lastlogin'
  end

  def self.down
    remove_column :people, :recent_login
    remove_column :people, :recent_login_failure
  end
end
