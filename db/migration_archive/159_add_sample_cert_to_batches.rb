class AddSampleCertToBatches < ActiveRecord::Migration[4.2]
  def self.up
    add_column :cert_batches, :sample_cert_yaml, :text
    add_column :cert_batches, :finalized, :boolean, :null => false, :default => false
    CertBatch.all.each do |batch|
      if cert = Cert.where(:cert_batch_id => batch.id, :date_used => nil).first
        batch.sample_cert = cert
        batch.finalized = true
        unless batch.save
          puts "BATCH FAILED: #{batch.id}"
        end
      else
        puts "BATCH HAS NO UNUSED CERTS: #{batch.id}"
      end
    end
  end

  def self.down
    remove_column :cert_batches, :sample_cert_yaml
    remove_column :cert_batches, :finalized
  end
end
