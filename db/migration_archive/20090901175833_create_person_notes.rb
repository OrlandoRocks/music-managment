class CreatePersonNotes < ActiveRecord::Migration[4.2]
  def self.up
    upstring = %Q(
      CREATE TABLE  `person_notes` (
        `id` int UNSIGNED NOT NULL auto_increment,
        `person_id` int UNSIGNED NOT NULL COMMENT 'Foreign Key of the user the note is about',
        `note_created_by_id` int UNSIGNED NOT NULL COMMENT 'Foreign Key references people table for the user that created the note',
        `subject` varchar(50) NOT NULL,
        `note` text NOT NULL,
        `created_at` datetime default NULL,
        `updated_at` datetime default NULL,
        PRIMARY KEY  (`id`),
        KEY `person_id` (`person_id`),
        KEY `note_created_by_id` (`note_created_by_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8)
        
    execute upstring
  end

  def self.down
    drop_table :person_notes
  end
end
