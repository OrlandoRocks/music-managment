class FixupMissingGoliveForPurchases < ActiveRecord::Migration[4.2]
  def self.up
    Purchase.transaction do
      # all purchases that were paid after 16:50 on Oct 9th 2007 should have had any
      # enlisted album's golive_date set to be 8.weeks from the moment the purchase was paid
      AlbumPurchase.where('paid_at > ?', Time.local(2007,10,9,16,50)).each do |pur|
        # if this is a purchase of the album's base fee
        # and the golive date has not been set
        # and the album is finalized (this may be a redundant check but I feel happier with it)
        if pur.enlisted?(pur.related) && pur.related.golive_date.nil? && pur.related.finalized?
          # set the golive based on 8.weeks from the paid_at time for the purchase
          pur.related.update_attribute(:golive_date, pur.paid_at + 8.weeks)
        end
      end
    end
  end

  def self.down
  end
end
