class AddPersonTransaction < ActiveRecord::Migration[4.2]
  def self.up
    create_table :person_transactions do |t|
      t.column :created_at, :datetime, :null => false
      t.column :person_id, :integer, :null => false
      t.column :debit_cents, :integer, :null => false
      t.column :credit_cents, :integer, :null => false
      t.column :previous_balance_cents, :integer, :null => false
      t.column :target_id, :integer
      t.column :target_type, :string, :limit => 30
      t.column :comment, :text
    end
    
  end

  def self.down
    drop_table :person_transactions
  end
end
