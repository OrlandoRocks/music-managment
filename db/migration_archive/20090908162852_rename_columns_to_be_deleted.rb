class RenameColumnsToBeDeleted < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE `cert_batches` CHANGE `sample_cert_yaml` `sample_cert_yaml_REMOVE` TEXT NULL;")
    execute("ALTER TABLE `people` CHANGE `alternate_upload` `alternate_upload_REMOVE` TINYINT(1) DEFAULT '0' NOT NULL;")
  end

  def self.down
    execute("ALTER TABLE `cert_batches` CHANGE `sample_cert_yaml_REMOVE` `sample_cert_yaml` TEXT NULL;")
    execute("ALTER TABLE `people` CHANGE `alternate_upload_REMOVE` `alternate_upload` TINYINT(1) DEFAULT '0' NOT NULL;")
  end
end
