class MoveAlbumArtists < ActiveRecord::Migration[4.2]
  def self.up
    execute "INSERT INTO creatives (creativeable_id, creativeable_type, artist_id, role)
      SELECT id, 'Album', artist_id, 'primary'
      FROM albums where artist_id is not NULL AND artist_id > 0"
    
    execute "INSERT INTO creatives (creativeable_id, creativeable_type, artist_id, role)
      SELECT id, 'Song', artist_id, 'primary'
      FROM songs where artist_id is not NULL AND artist_id > 0"
    
     execute "INSERT INTO creatives (creativeable_id, creativeable_type, artist_id, role)
      SELECT id, 'Video', artist_id, 'primary'
      FROM videos where artist_id is not NULL AND artist_id > 0"
  end

  def self.down
    execute "delete from creatives where creativeable_type = 'Album'"
    
    execute "delete from creatives where creativeable_type = 'Song'"
    
    execute "delete from creatives where creativeable_type = 'Video'"
  end
end
