class AlbumPriceIncrease2 < ActiveRecord::Migration[4.2]
  def self.up
    if Object.const_defined?("Chargeable")
      Chargeable.transaction do
        Chargeable[:album].update(
          :short_code => 'album_01',
          :description => 'Legacy album - $9.98 base fee'
        )

        Chargeable.create(:short_code => 'album', :price_calculator => 'current_album', :base_price_cents => 1998, :description => "Album")
      end
    end
  end

  def self.down
    # SHOWING HOW TO BACK OUT OF THIS MIGRATION
    # BUT.... DON'T
    #
    # Do another forward migration that fixes any issues and
    # does fixups of any albums that may have been created with
    # the chargeable you don't like.
    #
    # Chargeable.transaction do
    #   Chargeable[:album].destroy
    #
    #   Chargeable[:album_01].update(
    #     :short_code => 'album',
    #     :description => 'Album'
    #   )
    # end
  end
end
