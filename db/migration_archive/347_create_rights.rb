class CreateRights < ActiveRecord::Migration[4.2]
  def self.up
    create_table :rights do |t|
      t.column :controller, :string
      t.column :action, :string
      t.column :name, :string
    end
    
    create_table :rights_roles, :id => false do |t|
      t.column :right_id, :integer
      t.column :role_id, :integer
    end
  end

  def self.down
    drop_table :rights
    drop_table :rights_roles
  end
end
