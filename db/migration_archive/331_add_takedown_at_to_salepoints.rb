class AddTakedownAtToSalepoints < ActiveRecord::Migration[4.2]
  def self.up
    add_column :salepoints, :takedown_at, :datetime, :default => nil
  end

  def self.down
    remove_column :salepoints, :takedown_at
  end
end
