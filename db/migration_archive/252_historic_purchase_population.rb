class HistoricPurchasePopulation < ActiveRecord::Migration[4.2]
  def self.up
    PopulateHistoricPurchases.build() if HistoricPurchasePopulation if Object.const_defined?("PopulateHistoricPurchases")
  end

  def self.down
  end
end
