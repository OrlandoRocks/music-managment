class RemoveRubyCodeFromPeopleTable < ActiveRecord::Migration[4.2]
  def self.up
    begin
      execute("START TRANSACTION;")
      execute("UPDATE people SET referral = NULL WHERE referral LIKE '%\:null%';")
      execute("ALTER TABLE people ALTER COLUMN referral DROP DEFAULT;")
      execute("COMMIT;")
    rescue StandardError => e
      execute("ROLLBACK;")
      raise("Migration Failed: #{e.message.inspect}")
    end
  end

  def self.down
  end
end
