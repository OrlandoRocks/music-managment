class DropIsAdminFromPerson < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :people, :is_administrator
  end

  def self.down
    add_column :people, :is_administrator, :boolean
  end
end
