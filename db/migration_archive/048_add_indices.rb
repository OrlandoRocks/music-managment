class AddIndices < ActiveRecord::Migration[4.2]
  def self.up
    add_index :songs, :album_id
    add_index :albums, :person_id
  end

  def self.down
    remove_index :songs, :album_id
    remove_index :albums, :person_id
  end
end
