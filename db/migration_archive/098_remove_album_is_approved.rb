class RemoveAlbumIsApproved < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :albums, :is_approved
  end

  def self.down
    add_column :albums, :is_approved, :boolean, :null => false, :default => false
    execute "update albums set is_approved = 1 where approval_date is not null"
  end
end
