class AddEzpayTransfersAndItems < ActiveRecord::Migration[4.2]
  def self.up
    add_column :check_transfers, :backend, :string, :default => "ManualCheckTransfer"
    add_column :check_transfers, :debit_transaction_id, :integer
    add_column :check_transfers, :refund_transaction_id, :integer
    add_column :people, :use_experimental_ezpay_backend, :boolean, :default => false

    execute %q{
      UPDATE check_transfers t SET debit_transaction_id = (
        SELECT id FROM person_transactions 
          WHERE target_type = 'CheckTransfer' 
          AND target_id = t.id LIMIT 1
      )
    
    }    
    
    create_table(:ezpay_items) do |t|
      t.column :ezpay_transfer_id, :integer
      t.column :reference, :integer
      t.column :ezpay_status, :string
    end
		
    add_index :ezpay_items, :ezpay_transfer_id
 
  end

  def self.down
    remove_column :check_transfers, :backend
    remove_column :check_transfers, :debit_transaction_id
    remove_column :check_transfers, :refund_transaction_id
    
    remove_column :people, :use_experimental_ezpay_backend
    execute %q{
      UPDATE person_transactions SET target_id = NULL WHERE target_type = 'EzpayTransfer'
    }
    
    drop_table :ezpay_items
  end
end
