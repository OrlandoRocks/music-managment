class AddStatusToArtwork < ActiveRecord::Migration[4.2]
  def self.up
    add_column :artworks, :status, :string, :default => 'no upload attempted'
    add_column :artworks, :last_errors, :string
  end

  def self.down
    remove_column :artworks, :status
    remove_column :artworks, :last_errors
  end
end
