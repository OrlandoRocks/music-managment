class TrendReports < ActiveRecord::Migration[4.2]
  def self.up

    create_table :trend_reports do |t|
      t.column :date, :date, :null => false
      t.column :imported_at, :datetime
    end
    add_index :trend_reports, [:date]

    create_table :trend_city_totals do |t|
      t.column :trend_report_id, :integer, :null => false
      t.column :city, :string, :limit => 40, :null => false
      t.column :state, :string, :limit => 2, :null => false
      t.column :total_units_sold, :integer, :null => false, :default => 0
      t.column :total_usd_cents, :integer, :null => false, :default => 0
    end
    add_index :trend_city_totals, [:trend_report_id, :city, :state, :total_units_sold, :total_usd_cents], :name => :trend_city_totals
  end
  
  def self.down
    drop_table :trend_reports
    drop_table :trend_city_totals
  end

end
