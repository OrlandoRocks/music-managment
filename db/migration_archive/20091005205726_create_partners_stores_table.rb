class CreatePartnersStoresTable < ActiveRecord::Migration[4.2]
  def self.up
    execute "DROP TABLE IF EXISTS `partners_stores`;"
    
    execute"
        CREATE TABLE `partners_stores` (
          `partner_id` SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Foreign Key to Partners table',
          `store_id` INT(10) UNSIGNED NOT NULL COMMENT 'Foreing Key to Stores table',
          KEY `partner_id` (`partner_id`),
          KEY `store_id` (`store_id`),
          CONSTRAINT `FK_partners_stores` FOREIGN KEY (`partner_id`) REFERENCES `partners` (`id`)
        ) ENGINE=INNODB DEFAULT CHARSET=utf8;
    "
  end

  def self.down
    execute "DROP TABLE IF EXISTS `partners_stores`;"
  end
end
