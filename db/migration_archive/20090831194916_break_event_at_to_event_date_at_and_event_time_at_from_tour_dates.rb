#Created By:    Ed Cheung
#Date:          2009-08-31
#Purpose:       The following migration file drops event_at from tour dates and
#               replaces it with event_time_at and event_date_at. event_time_at
#               will be an optional field for user to enter.
#
################################################################################
class BreakEventAtToEventDateAtAndEventTimeAtFromTourDates < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :tour_dates, :event_at
    add_column :tour_dates,  :event_time_at, :time
    add_column :tour_dates,  :event_date_at, :date
  end

  def self.down
    remove_column :tour_dates, :event_time_at
    remove_column :tour_dates, :event_date_at
    add_column :tour_dates,  :event_at, :datetime
  end
end
