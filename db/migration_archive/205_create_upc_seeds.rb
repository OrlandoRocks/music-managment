class CreateUpcSeeds < ActiveRecord::Migration[4.2]
  def self.up
      create_table :upc_seeds do |t|
        t.column :prefix, :integer
        t.column :seed, :integer
        t.column :block_full, :bool, :default => false
      end
  end

  def self.down
    drop_table :upc_seeds
  end
end
