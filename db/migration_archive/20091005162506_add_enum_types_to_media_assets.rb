class AddEnumTypesToMediaAssets < ActiveRecord::Migration[4.2]
  def self.up
    execute("
      ALTER TABLE `media_assets` CHANGE `mime_type` `mime_type`
      ENUM('image/jpeg','image/png','image/tiff','image/bmp','image/gif','image/pjpeg','image/x-png') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
      COMMENT 'Asset mime type.';
    ")
  end

  def self.down
     execute("
      ALTER TABLE `media_assets` CHANGE `mime_type` `mime_type`
      ENUM('image/jpeg','image/png','image/tiff','image/bmp','image/gif') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
      COMMENT 'Asset mime type.';
    ")
  end
end
