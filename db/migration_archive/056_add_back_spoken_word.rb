class AddBackSpokenWord < ActiveRecord::Migration[4.2]
  def self.up
    Genre.create :name=>"Spoken Word"
  end

  def self.down
    genre = Genre.find_by(name: "Spoken Word")
    genre.destroy
  end
end
