class MoveUpcsFromAlbums < ActiveRecord::Migration[4.2]
  def self.up
    puts "Moving album UPCs to the UPC table"
    execute "insert into upcs (number, upcable_id, id, upcable_type, tunecore_upc) select albums.tunecore_upc, albums.id, albums.id, 'Album' as temp, true as temp2 from albums"
    execute "insert into upcs (number, upcable_id, upcable_type, tunecore_upc) select albums.optional_upc, albums.id, 'Album' as temp, false as temp2 from albums where albums.optional_upc is not null and albums.optional_upc != ''"
  end

  def self.down
    puts "moving album upcs back into the album from the UPC table"
    execute "update albums join upcs on albums.id = upcs.upcable_id set albums.tunecore_upc = upcs.number where upcs.tunecore_upc = true and upcs.upcable_type = 'Album'"
    execute "update albums join upcs on albums.id = upcs.upcable_id set albums.optional_upc = upcs.number where upcs.tunecore_upc = false and upcs.upcable_type = 'Album'"
  end
end
