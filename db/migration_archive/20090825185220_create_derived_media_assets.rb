#Created By:    Ed Cheung
#Date:          2009-08-25
#Purpose:       The following migration creates a derived media assets table to store
#               meta data for converted version of user uploaded files. For widget 2 release
#               it'll store meta data for band photo resize to 160x160 and eventually
#               expand to store meta data of other converted file types. This table
#               has a foreign key column to media assets so we'll know the asset type for a
#               particular record
#
################################################################################
#
#Column Description:
#id:              unique identifier, primary key
#description:     data provided by the user
#mime_type:       file format, 'image/jpeg', 'image/png', 'image/tiff', 'image/bmp', 'image/gif'
#width:           image width, used when the mime_type is an image
#height:          image height, used when the mime_type is an image
#bit_depth:       bit depth, used when the mime_type is an image
#size:            size of file in bytes
#created_at:      upload time
#updated_at:      upload time
#local_asset_id:  foreign key
#s3_asset_id:     foreign key
#media_asset_id:  foreign key

################################################################################

class CreateDerivedMediaAssets < ActiveRecord::Migration[4.2]
  def self.up
    execute "
     CREATE TABLE derived_media_assets (
        id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
        width SMALLINT(6) UNSIGNED DEFAULT NULL COMMENT 'Image width',
        height SMALLINT(6) UNSIGNED DEFAULT NULL COMMENT 'Image Height',
        bit_depth TINYINT(4) UNSIGNED DEFAULT NULL COMMENT 'Bit Depth',
        size INT(10) UNSIGNED NOT NULL COMMENT 'Size of file in bytes',
        local_asset_id INT(10) UNSIGNED DEFAULT NULL COMMENT 'Foreign key to local_assets table.',
        s3_asset_id INT(10) UNSIGNED DEFAULT NULL COMMENT 'Foreign key to sc_assets table.',
        media_asset_id INT(10) UNSIGNED DEFAULT NULL COMMENT 'Foreign key to media_assets table.',
        mime_type ENUM('image/jpeg','image/png','image/tiff','image/bmp', 'image/gif') NOT NULL COMMENT 'Asset mime type.',
        created_at DATETIME NOT NULL COMMENT 'Date record Created.',
        updated_at DATETIME NOT NULL COMMENT 'Date record Updated.',
       PRIMARY KEY  (id),
       KEY media_asset_id (media_asset_id),
       KEY s3_asset_id (s3_asset_id),
       KEY local_asset_id (local_asset_id)
      ) ENGINE=INNODB DEFAULT CHARSET=utf8;"
  end

  def self.down
    drop_table :derived_media_assets
  end
end
