class RemovePaymentAppliedFromSongs < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :songs, :payment_applied
  end

  def self.down
    add_column :songs, :payment_applied, :boolean, :null => false, :default => false
  end
end
