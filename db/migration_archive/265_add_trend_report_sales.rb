class AddTrendReportSales < ActiveRecord::Migration[4.2]
  def self.up
    execute "alter table us_zip_codes ADD id int NOT NULL auto_increment, ADD PRIMARY KEY(id);"
    execute "ALTER TABLE trend_reports CHANGE date sold_on date NOT NULL;"
    create_table :trend_report_sales do |t|
      t.column :trend_report_id, :integer, :null => false
      t.column :target_id, :integer, :null => false
      t.column :target_type, :string, :null => false, :limit => 10
      t.column :units_sold, :integer, :null => false
      t.column :usd_cents, :integer, :null => false, :default => 0
      t.column :is_promo, :boolean, :null => false, :default => false
      t.column :us_zip_code_id, :integer
      t.column :apple_customer_id, :integer
    end
    add_index :trend_report_sales, [:trend_report_id, :target_id, :target_type, :us_zip_code_id], :name => :trend_sales_identifiers
  end
  def self.down
    execute "alter table us_zip_codes drop column id;"
    execute "ALTER TABLE trend_reports CHANGE sold_on date date NOT NULL;"
    drop_table :trend_report_sales
  end
end
