class CreateMailingDeliveries < ActiveRecord::Migration[4.2]
  def self.up
    create_table :mailing_deliveries do |t|
      t.column :person_id, :integer, :null => false
      t.column :scheduled_mailing_id, :integer, :null => false
      t.column :created_at, :datetime
    end
    add_index :mailing_deliveries, [:person_id, :scheduled_mailing_id]
  end

  def self.down
    drop_table :mailing_deliveries
  end
end
