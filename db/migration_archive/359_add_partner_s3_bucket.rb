class AddPartnerS3Bucket < ActiveRecord::Migration[4.2]
  def self.up
    add_column :partners, :s3_bucket, :string
  end

  def self.down
    remove_column :partners, :s3_bucket
  end
end
