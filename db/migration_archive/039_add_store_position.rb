class AddStorePosition < ActiveRecord::Migration[4.2]
  def self.up
    add_column :stores, :position, :integer    
    Store.update_all('position = id')
  end

  def self.down
    remove_column :stores, :position
  end
end
