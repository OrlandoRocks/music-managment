class CreateTrendingReports < ActiveRecord::Migration[4.2]
  def self.up
    create_table :itunes_user_reports do |t|
      t.column :start_on, :date, :null => false
      t.column :stop_on, :date, :null => false
      t.column :created_at, :datetime
      t.column :number_of_records, :integer, :null => false
      t.column :number_of_times_downloaded, :integer, :default => 0, :null => false
      t.column :paid_at, :datetime
      t.column :person_id, :integer
      t.column :price_policy_id, :integer, :null => false
      t.column :purchase_id, :integer
    end
  end

  def self.down
    drop_table :itunes_user_reports
  end
end


#iur = ItunesUserReport.new(:start_on => Date.today - 7, :stop_on => Date.today, :number_of_records => 12, :person_id => 12249)

# Purchase.outstanding_of(iur)

#iur.purchase.destroy
