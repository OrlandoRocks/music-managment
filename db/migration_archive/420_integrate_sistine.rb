class IntegrateSistine < ActiveRecord::Migration[4.2]
  def self.up
    
    
    create_table :background_effects do |t|
      t.column :name, :string
      t.column :template, :string
      t.column :created_at, :datetime
      t.column :updated_at, :datetime
      t.column :level, :integer
    end
    
    create_table :background_genres do |t|
      t.column :background_id, :integer
      t.column :genre_id, :integer
      t.column :created_at, :datetime
      t.column :updated_at, :datetime
    end

    create_table :backgrounds do |t|
      t.column :name, :string
      t.column :url, :string
      t.column :size, :integer
      t.column :created_at, :datetime
      t.column :updated_at, :datetime
    end

    create_table :covers do |t|
      t.column :background_id, :integer
      t.column :effect, :string
      t.column :background_effect_id, :integer
      t.column :artist, :string
      t.column :artist_typeface_id, :integer
      t.column :artist_typeface_pointsize, :integer
      t.column :artist_typeface_effect_id, :integer
      t.column :title, :string
      t.column :title_typeface_id, :integer
      t.column :title_typeface_pointsize, :integer
      t.column :title_typeface_effect_id, :integer
      t.column :layout_class, :string
      t.column :artist_height, :integer
      t.column :artist_width, :integer
      t.column :title_height, :integer
      t.column :title_width, :integer
      t.column :created_at, :datetime
      t.column :updated_at, :datetime
    end
    
    create_table :options do |t|
      t.column :name, :string
      t.column :label, :string
      t.column :setting_type, :string
      t.column :typeface_effect_id, :integer
      t.column :default_value, :string
      t.column :created_at, :datetime
      t.column :updated_at, :datetime
    end

    create_table :settings do |t|
      t.column :option_id, :integer
      t.column :value, :string
      t.column :settable_type, :string
      t.column :settable_id, :integer
      t.column :created_at, :datetime
      t.column :updated_at, :datetime
    end
    
    create_table :typeface_effects do |t|
      t.column :name, :string
      t.column :template, :string
      t.column :use_for_suggested_covers, :boolean
      t.column :created_at, :datetime
      t.column :updated_at, :datetime
    end
    
    create_table :typeface_genres do |t|
      t.column :typeface_id, :integer
      t.column :genre_id, :integer
      t.column :created_at, :datetime
      t.column :updated_at, :datetime
    end

    create_table :typefaces do |t|
      t.column :name, :string
      t.column :description, :string
      t.column :created_at, :datetime
      t.column :updated_at, :datetime
    end

      
     
  end
  
  def self.down
    drop_table :background_effects
    drop_table :background_genres
    drop_table :backgrounds
    drop_table :covers
    drop_table :options
    drop_table :settings
    drop_table :typeface_effects
    drop_table :typeface_genres
    drop_table :typefaces
  end
end
