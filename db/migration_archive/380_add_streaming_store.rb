class AddStreamingStore < ActiveRecord::Migration[4.2]
  def self.up
    Store.new(:name => "Streaming", :abbrev => "st", :short_name => "Streaming", :is_active => false ).save
  end

  def self.down
  end
end
