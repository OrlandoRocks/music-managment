class AddSalepointableTypeVariablePrices < ActiveRecord::Migration[4.2]

  TABLE_NAME = :variable_prices_salepointable_types

  def self.up
    create_table TABLE_NAME do |t|
      t.string :salepointable_type, :null => false
      t.integer :variable_price_id
    end

    add_index TABLE_NAME, 
              [:variable_price_id, :salepointable_type], 
              :unique => true, 
              :name => 'unique_variable_price_id_by_type'
  end

  def self.down
    drop_table TABLE_NAME
  end

end
