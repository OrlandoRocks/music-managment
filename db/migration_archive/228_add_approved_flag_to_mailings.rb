class AddApprovedFlagToMailings < ActiveRecord::Migration[4.2]
  def self.up
    add_column :scheduled_mailings, :is_approved, :boolean, :null => false, :default => false   
  end

  def self.down
    remove_column :scheduled_mailings, :is_approved
  end
end
