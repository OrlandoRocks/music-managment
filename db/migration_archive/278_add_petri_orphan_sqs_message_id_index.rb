class AddPetriOrphanSqsMessageIdIndex < ActiveRecord::Migration[4.2]
  def self.up
    add_index :petri_orphans, :sqs_message_id, :unique
  end

  def self.down
    remove_index :petri_orphans, :sqs_message_id
  end
end
