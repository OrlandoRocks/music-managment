class AddVideoFields < ActiveRecord::Migration[4.2]
  def self.up
    add_column :videos, :c_copyright_date, :date
  end

  def self.down
    remove_column :videos, :c_copyright_date
  end
end
