class AddPeopleTable < ActiveRecord::Migration[4.2]
  def self.up
    create_table "people", :force => true do |t|
       t.column "name", :string, :limit => 80, :default => "", :null => false
       t.column "password", :string, :limit => 40, :default => "", :null => false
       t.column "email", :string, :limit => 128, :default => "", :null => false
       t.column "artist_id", :string, :limit => 6
       t.column "salt", :string, :limit => 40, :default => "", :null => false
       t.column "is_verified", :integer, :default => 0
       t.column "invite_code", :string, :limit => 40
       t.column "invite_expiry", :datetime
       t.column "deleted", :integer, :default => 0
       t.column "delete_after", :datetime
     end
  end

  def self.down
    drop_table :people
  end
end
