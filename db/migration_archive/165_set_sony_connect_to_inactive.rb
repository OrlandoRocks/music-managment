class SetSonyConnectToInactive < ActiveRecord::Migration[4.2]
  def self.up
    sony = Store.find_by(name: 'Sony Connect')
    sony.is_active = false
    sony.save
  end

  def self.down
    sony = Store.find_by(name: 'Sony Connect')
    sony.is_active = true
    sony.save
  end
end
