class SalepointableStore < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE stores CHANGE `id` `id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT;")
    execute("
      CREATE TABLE `salepointable_stores` (
        `id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Priimary Key',
        `salepointable_type` ENUM('Album','Single','Ringtone') NOT NULL COMMENT 'Type of distribution.',
        `store_id` SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Foreign key to stores table.',
        PRIMARY KEY  (`id`),
        KEY `store_id` (`store_id`),
        CONSTRAINT `FK_salepointable_stores` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`)
      ) ENGINE=INNODB DEFAULT CHARSET=utf8
    ")
  end

  def self.down
    execute("DROP TABLE salepointable_stores")
  end
end
