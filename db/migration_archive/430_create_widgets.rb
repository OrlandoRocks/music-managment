#Created By:    Ed Cheung
#Purpose:       The following migration file creates a widgets table that
#               will be used to store a widget's configuration. A person can only
#               have one widget but in the future they can have upto a certain
#               amount like ten widgets for example. Upon a save the application
#               will make a copy of the configuration (config_data) and store it
#               as a public file in Amazon S3. The 'design time' widget
#               will retrieve the configuration from this table (config_data).
#               The 'run time' widget will retrieve the configuration from
#               Amazon S3. The url to the configuration file store in Amazon S3
#               will be generated by the 'run time' widget base on the widget's
#               id. The widget's id will be pass into the 'run time' SWF object
#               as a param in the url embedd code. 
#Relationship:  A person can have many widgets. A widget belongs to a person
################################################################################
#Column Description:
#id:            will be a unique identifier for a widget
#person_id:     should foreign key persons table
#config_data:   is a blob type and will store the widget's configuration file
#               if no configuration file has been saved, it's default value is NULL
#status:        a code identifying the status for the widget. Currently its default
#               value is 0 for 'enable' and 1 for 'disable'. We can have more
#               status code in the future
#deleted_at:    datetime type saves the time when user deletes the widget. Default
#               value is NULL
#created_at:    datetime type stores the time a record was created
#updated_at:    datetime type stores the time when the record was updated
################################################################################
#Column Stats:
#configuration: MAX Size: 65kb
#               AVG Size: N/A
#               MIN Size: NULL
#status:        TINYINT(4) Range from -128..127, Possibly 255 different status
#               codes
################################################################################
#Example Queries from development.log:
#1. SELECT * FROM `widgets` WHERE (`widgets`.person_id = 1) LIMIT 1
################################################################################

class CreateWidgets < ActiveRecord::Migration[4.2]
  def self.up
    create_table :widgets do |t|
      t.column :person_id, :integer
      t.column :config_data, :blob
      t.column :status,    :integer, :limit => 1, :default => 0
      t.column :deleted_at, :datetime
      t.timestamps
    end
  end

  def self.down
    drop_table :widgets if table_exists?("widgets")
    #It use to be called player_widgets
    drop_table :player_widgets if table_exists?("player_widgets")
  end
end
