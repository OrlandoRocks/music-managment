class AddGainLog < ActiveRecord::Migration[4.2]
  def self.up
     create_table :gainlogs, :force => true do |t|
       t.column :date_used, :datetime
       t.column :album_total_after_discount, :integer, :null => false
       t.column :person_id, :integer
       t.column :gain_employee_id, :integer
       t.column :album_id, :integer      
     end
   end

   def self.down
     drop_table :certs
   end
end
