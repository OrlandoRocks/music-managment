class AlterFontanaDomain < ActiveRecord::Migration[4.2]
  def self.up
    execute ("update partners set domain = 'XXfontanaXX' where skin = 'fontana'")
  end

  def self.down
    execute ("update partners set domain = 'fontanaartistnetwork' where skin = 'fontana'")
  end
end
