class RemoveTouchStatusesTable < ActiveRecord::Migration[4.2]
  def self.up
    drop_table :touch_statuses
  end

  def self.down
    create_table :touch_statuses do |t|
      t.column :touchable_id, :integer
      t.column :touchable_type, :string
      t.column :touched_at, :datetime, :default => nil
    end
  end
end
