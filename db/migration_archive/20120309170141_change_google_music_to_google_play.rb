class ChangeGoogleMusicToGooglePlay < ActiveRecord::Migration[4.2]
  def self.up
    if Store.find_by(abbrev: "gm")
      Store.find_by(abbrev: "gm").update_attribute :name, "Google Play"
    end
  end

  def self.down
    if Store.find_by(abbrev: "gm")
      Store.find_by(abbrev: "gm").update_attribute :name, "Google Music"
    end
  end
end
