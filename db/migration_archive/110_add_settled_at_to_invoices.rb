class AddSettledAtToInvoices < ActiveRecord::Migration[4.2]
  def self.up
    add_column :invoices, :settled_at, :datetime, :null => true, :default => nil
  end

  def self.down
    remove_column :invoices, :settled_at
  end
end
