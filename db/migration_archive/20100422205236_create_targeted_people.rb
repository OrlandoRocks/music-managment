class CreateTargetedPeople < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `targeted_people`(
                `id` INT(10) UNSIGNED not null auto_increment COMMENT 'Primary Key',
                `targeted_offer_id` INT(10) UNSIGNED not null COMMENT 'id of the offer this person should see',
                `person_id` INT(10) UNSIGNED not null COMMENT 'id of the targeted customer',
                `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',               
                PRIMARY KEY  (`id`),
                KEY `targeted_offer_id` (`targeted_offer_id`),
                KEY `person_id` (`person_id`),
                CONSTRAINT `fk_targeted_people_offer_id` FOREIGN KEY (`targeted_offer_id`) REFERENCES `targeted_offers` (`id`),
                CONSTRAINT `fk_targeted_people_people_id` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8)

    execute up_sql
  end

  def self.down
    drop_table :targeted_people
  end
end
