class AddInfoSharingDate < ActiveRecord::Migration[4.2]
  def self.up
		add_column :people, :info_sharing_date, :datetime
  end

  def self.down
		remove_column :people, :info_sharing_date
  end
end
