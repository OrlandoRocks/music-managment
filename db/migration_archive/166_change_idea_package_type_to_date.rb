class ChangeIdeaPackageTypeToDate < ActiveRecord::Migration[4.2]
  class Idea < ApplicationRecord
  end

  def self.up
    add_column :ideas, :temp_package_date, :date
    Idea.all.each {|i| i.update(:temp_package_date => i.package_date)}
    remove_column :ideas, :package_date
    rename_column :ideas, :temp_package_date, 'package_date'
  end

  def self.down
    add_column :ideas, :temp_package_date, :string
    Idea.all.each {|i| i.update(:temp_package_date => i.package_date)}
    remove_column :ideas, :package_date
    rename_column :ideas, :temp_package_date, 'package_date'
  end
end
