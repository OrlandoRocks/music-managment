class FixEmusicSalepoingRights < ActiveRecord::Migration[4.2]
  def self.up
		execute "update salepoints set has_rights_assignment = true where finalized_at is not null and has_rights_assignment = false and store_id = 10"
  end

  def self.down
  end
end
