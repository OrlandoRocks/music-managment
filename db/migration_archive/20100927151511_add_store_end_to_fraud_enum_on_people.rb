class AddStoreEndToFraudEnumOnPeople < ActiveRecord::Migration[4.2]
  def self.up
    execute "ALTER TABLE people MODIFY COLUMN lock_reason enum('Credit Card Fraud','PayPal Fraud','No Rights','Fraudulent Sales','Spam','Store End','Other') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL"
  end

  def self.down
    execute "ALTER TABLE people MODIFY COLUMN lock_reason enum('Credit Card Fraud','PayPal Fraud','No Rights','Fraudulent Sales','Spam','Other') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL"
  end
end
