class AddCmsFieldToTargetedOffer < ActiveRecord::Migration[4.2]
  def self.up
    execute "ALTER TABLE targeted_offers ADD COLUMN include_sidebar_cms TINYINT(1) DEFAULT 0 AFTER join_token"
  end

  def self.down
    execute "ALTER TABLE targeted_offers DROP COLUMN include_sidebar_cms"
  end
end
