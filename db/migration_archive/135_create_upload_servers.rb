class CreateUploadServers < ActiveRecord::Migration[4.2]
  def self.up
    create_table :upload_servers do |t|
      t.column :url, :string
    end
  end

  def self.down
    drop_table :upload_servers
  end
end
