class AddCreativesIndices < ActiveRecord::Migration[4.2]
  def self.up
    add_index :creatives, :creativeable_type
    add_index :creatives, :creativeable_id
    add_index :creatives, :artist_id
  end

  def self.down
    remove_index :creatives, :creativeable_type
    remove_index :creatives, :creativeable_id
    remove_index :creatives, :artist_id
  end
end
