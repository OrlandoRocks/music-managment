#Created By:    Ed Cheung
#Date:          2009-08-06
#Purpose:       The following migration file creates a us_states table that
#               will be used to store all us_states name and their respective abbreviation
#
#               It will be used primarly as a lookup table. This table was created
#               for tour date feature in Widget Version 2 release.
#
################################################################################
#
#Column Description:
#id:            unique identifier for a state
#name:          name of state
#abbreviation:  abbreviation of state name
#
################################################################################
#
#Column Data Type:
#
#id:            tinyint, unsigned, not null, auto increment, primary key
#name:          varchar(30), not null
#abbreviation:  char(2), not null
#
################################################################################
#
#Table Properties:
#engine:        innodb
#charset:        utf8
#
################################################################################
class CreateUsStates < ActiveRecord::Migration[4.2]
  def self.up
    create_us_states_table=<<-EOSTRING
      create table us_states (id tinyint unsigned not null auto_increment, name varchar(30) not null,
                              abbreviation char(2) not null, primary key(id)) engine=innodb charset=utf8;
    EOSTRING

    populate_states=<<-EOSTRING
      INSERT INTO us_states(name, abbreviation) VALUES ('ALABAMA', 'AL');
      INSERT INTO us_states(name, abbreviation) VALUES ('ALASKA', 'AK');
      INSERT INTO us_states(name, abbreviation) VALUES ('AMERICAN SAMOA', 'AS');
      INSERT INTO us_states(name, abbreviation) VALUES ('ARIZONA', 'AZ');
      INSERT INTO us_states(name, abbreviation) VALUES ('ARKANSAS', 'AR');
      INSERT INTO us_states(name, abbreviation) VALUES ('CALIFORNIA', 'CA');
      INSERT INTO us_states(name, abbreviation) VALUES ('COLORADO', 'CO');
      INSERT INTO us_states(name, abbreviation) VALUES ('CONNECTICUT', 'CT');
      INSERT INTO us_states(name, abbreviation) VALUES ('DELAWARE', 'DE');
      INSERT INTO us_states(name, abbreviation) VALUES ('DISTRICT OF COLUMBIA', 'DC');
      INSERT INTO us_states(name, abbreviation) VALUES ('FEDERATED STATES OF MICRONESIA', 'FM');
      INSERT INTO us_states(name, abbreviation) VALUES ('FLORIDA', 'FL');
      INSERT INTO us_states(name, abbreviation) VALUES ('GEORGIA', 'GA');
      INSERT INTO us_states(name, abbreviation) VALUES ('GUAM', 'GU');
      INSERT INTO us_states(name, abbreviation) VALUES ('HAWAII', 'HI');
      INSERT INTO us_states(name, abbreviation) VALUES ('IDAHO', 'ID');
      INSERT INTO us_states(name, abbreviation) VALUES ('ILLINOIS', 'IL');
      INSERT INTO us_states(name, abbreviation) VALUES ('INDIANA', 'IN');
      INSERT INTO us_states(name, abbreviation) VALUES ('IOWA', 'IA');
      INSERT INTO us_states(name, abbreviation) VALUES ('KANSAS', 'KS');
      INSERT INTO us_states(name, abbreviation) VALUES ('KENTUCKY', 'KY');
      INSERT INTO us_states(name, abbreviation) VALUES ('LOUISIANA', 'LA');
      INSERT INTO us_states(name, abbreviation) VALUES ('MAINE', 'ME');
      INSERT INTO us_states(name, abbreviation) VALUES ('MARSHALL ISLANDS', 'MH');
      INSERT INTO us_states(name, abbreviation) VALUES ('MARYLAND', 'MD');
      INSERT INTO us_states(name, abbreviation) VALUES ('MASSACHUSETTS', 'MA');
      INSERT INTO us_states(name, abbreviation) VALUES ('MICHIGAN', 'MI');
      INSERT INTO us_states(name, abbreviation) VALUES ('MINNESOTA', 'MN');
      INSERT INTO us_states(name, abbreviation) VALUES ('MISSISSIPPI', 'MS');
      INSERT INTO us_states(name, abbreviation) VALUES ('MISSOURI', 'MO');
      INSERT INTO us_states(name, abbreviation) VALUES ('MONTANA', 'MT');
      INSERT INTO us_states(name, abbreviation) VALUES ('NEBRASKA', 'NE');
      INSERT INTO us_states(name, abbreviation) VALUES ('NEVADA', 'NV');
      INSERT INTO us_states(name, abbreviation) VALUES ('NEW HAMPSHIRE', 'NH');
      INSERT INTO us_states(name, abbreviation) VALUES ('NEW JERSEY', 'NJ');
      INSERT INTO us_states(name, abbreviation) VALUES ('NEW MEXICO', 'NM');
      INSERT INTO us_states(name, abbreviation) VALUES ('NEW YORK', 'NY');
      INSERT INTO us_states(name, abbreviation) VALUES ('NORTH CAROLINA', 'NC');
      INSERT INTO us_states(name, abbreviation) VALUES ('NORTH DAKOTA', 'ND');
      INSERT INTO us_states(name, abbreviation) VALUES ('NORTHERN MARIANA ISLANDS', 'MP');
      INSERT INTO us_states(name, abbreviation) VALUES ('OHIO', 'OH');
      INSERT INTO us_states(name, abbreviation) VALUES ('OKLAHOMA', 'OK');
      INSERT INTO us_states(name, abbreviation) VALUES ('OREGON', 'OR');
      INSERT INTO us_states(name, abbreviation) VALUES ('PALAU', 'PW');
      INSERT INTO us_states(name, abbreviation) VALUES ('PENNSYLVANIA', 'PA');
      INSERT INTO us_states(name, abbreviation) VALUES ('PUERTO RICO', 'PR');
      INSERT INTO us_states(name, abbreviation) VALUES ('RHODE ISLAND', 'RI');
      INSERT INTO us_states(name, abbreviation) VALUES ('SOUTH CAROLINA', 'SC');
      INSERT INTO us_states(name, abbreviation) VALUES ('SOUTH DAKOTA', 'SD');
      INSERT INTO us_states(name, abbreviation) VALUES ('TENNESSEE', 'TN');
      INSERT INTO us_states(name, abbreviation) VALUES ('TEXAS', 'TX');
      INSERT INTO us_states(name, abbreviation) VALUES ('UTAH', 'UT');
      INSERT INTO us_states(name, abbreviation) VALUES ('VERMONT', 'VT');
      INSERT INTO us_states(name, abbreviation) VALUES ('VIRGIN ISLANDS', 'VI');
      INSERT INTO us_states(name, abbreviation) VALUES ('VIRGINIA', 'VA');
      INSERT INTO us_states(name, abbreviation) VALUES ('WASHINGTON', 'WA');
      INSERT INTO us_states(name, abbreviation) VALUES ('WEST VIRGINIA', 'WV');
      INSERT INTO us_states(name, abbreviation) VALUES ('WISCONSIN', 'WI');
      INSERT INTO us_states(name, abbreviation) VALUES ('WYOMING', 'WY');
      INSERT INTO us_states(name, abbreviation) VALUES ('OTHER', '');
    EOSTRING
    execute create_us_states_table
    populate_states.split("\n").each do |populate_state|
      execute populate_state
    end
  end

  def self.down
    drop_table :us_states
  end
end
