class MovePricingToItems < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :stores, :price_cents
    add_column :stores, :price_cents, :integer
    Item.create :name=>"store", :price_cents=>"99"
  end

  def self.down
    add_column :stores, :price_cents, :integer
    drop_table :stores
    stores = [["iTunes U.S.","us","0"],["iTunes Australia","au","99"],["iTunes Canada","ca","99"],
                ["iTunes UK/European Union","eu","99"],["iTunes Japan","jp","99"],["iTunes United Kingdom","uk","99"],
                ["Rhapsody","rh","99"],["EMusic","em","99"]]
                
    stores.each { |store|
      Store.create :name=>store[0],:abbrev=>store[1],:price_cents=>store[2]
    }
  end
end
