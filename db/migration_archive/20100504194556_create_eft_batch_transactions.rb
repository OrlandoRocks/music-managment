class CreateEftBatchTransactions < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `eft_batch_transactions` (
      `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
      `transaction_id` varchar(35) NULL COMMENT 'References transaction ID stored at Braintree',
      `order_id` varchar(35) NULL COMMENT "Unique Order ID used by braintree (currently eft+id)",
      `eft_batch_id` INT(10) UNSIGNED NULL COMMENT 'Foreign Key to eft_batch table',
      `eft_query_id` INT(10) UNSIGNED NULL COMMENT 'Foreign Key to eft_query table',
      `stored_bank_account_id` INT(10) UNSIGNED NULL COMMENT 'Foreign Key to stored bank account table (for braintree transactions)',
      `payout_service_fee_id` INT(10) UNSIGNED NULL COMMENT 'Foreign Key to payout service fee tables',
      `failure_fee_id` INT(10) UNSIGNED NULL COMMENT 'Foreign Key to payout service fee tables',
      `amount` DECIMAL(10,2) NOT NULL DEFAULT '0.00' COMMENT 'Amount',
      `status` ENUM('pending_approval','waiting_for_batch','canceled','rejected','processing_in_batch', 'sent_to_bank', 'error','success', 'failure') NOT NULL DEFAULT 'pending_approval' COMMENT 'Current State of process',
      `response_code` char(4) NULL COMMENT "Response Code returned by Braintree",
      `response_text` varchar(255) NULL COMMENT "Response Text returned by Braintree",
      `email_message` TEXT DEFAULT NULL COMMENT "Email message added by admin",
      `updated_at` TIMESTAMP NULL COMMENT 'Datetime of latest update',
      `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',
      PRIMARY KEY  (`id`),
      KEY `eftb_eft_batch_id` (`eft_batch_id`),
      KEY `eftb_eft_query_id` (`eft_query_id`),
      KEY `eftb_stored_bank_account_id` (`stored_bank_account_id`),
      KEY `eftb_payout_service_fee_id` (`payout_service_fee_id`),
      KEY `eftb_failure_fee_id` (`payout_service_fee_id`),
      CONSTRAINT `FK_eft_txn_stored_bank_account` FOREIGN KEY (`stored_bank_account_id`) REFERENCES `stored_bank_accounts`(`id`),
      CONSTRAINT `FK_eft_txn_eft_batch` FOREIGN KEY (`eft_batch_id`) REFERENCES `eft_batches` (`id`),
      CONSTRAINT `FK_eft_txn_failure_fee` FOREIGN KEY (`failure_fee_id`) REFERENCES `payout_service_fees` (`id`),
      CONSTRAINT `FK_eft_txn_payout_service_fee` FOREIGN KEY (`payout_service_fee_id`) REFERENCES `payout_service_fees` (`id`)
    ) ENGINE=INNODB DEFAULT CHARSET=utf8;)
                  
    execute up_sql
  end

  def self.down
    drop_table :eft_batch_transactions
  end
end
