class MoveArtistIdData < ActiveRecord::Migration[4.2]
  def self.up
    add_index :artists_songs, :song_id
    execute "update songs left join artists_songs on artists_songs.song_id = songs.id set songs.artist_id = artists_songs.artist_id"
    Song.reset_column_information
    Song.where(artist_id: nil).each do |song|
      if song.album.artist_id.to_i == 0
        if song.album.is_various?
          song.update_attribute :artist_id, Artist.find_by(name: 'Various Artists').id
        else
          raise "Unable to fixup song #{song.id} - album has no artist_id and it isn't various so I can't fake it"
        end
      else
        song.update_attribute :artist_id, song.album.artist_id
      end
    end
  end

  def self.down
    execute "delete from artists_songs"
    execute "insert into artists_songs(song_id, artist_id) select songs.id, songs.artist_id from songs"
  end
end
