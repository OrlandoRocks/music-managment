class FillCreatedOnDateWithExpiryDate < ActiveRecord::Migration[4.2]
  def self.up
		execute "update people set created_on = DATE_sub(invite_expiry, INTERVAL 3 DAY) where invite_expiry is not null"
  end

  def self.down
  end
end
