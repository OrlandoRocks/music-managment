class ChangeTargetedOfferPurchaseField < ActiveRecord::Migration[4.2]
  def self.up
    execute "ALTER TABLE targeted_offers ADD COLUMN purchase_criteria_type ENUM('never_made', 'has_made', 'date_range', 'n/a') AFTER has_never_made_a_purchase"
    execute "UPDATE targeted_offers SET purchase_criteria_type = 'never_made' where has_never_made_a_purchase = 1"
    execute "UPDATE targeted_offers SET purchase_criteria_type = 'n/a' where has_never_made_a_purchase = 0 or has_never_made_a_purchase IS NULL"
  end

  def self.down
    execute "ALTER TABLE targeted_offers DROP COLUMN purchase_criteria_type"
  end
end
