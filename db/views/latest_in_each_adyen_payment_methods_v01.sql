 SELECT
  t1.id,
  t1.adyen_payment_method_info_id,
  t1.person_id,
  t1.created_at
FROM
  adyen_stored_payment_methods t1
  JOIN
    (
      SELECT
        adyen_payment_method_info_id,
        MAX(created_at) created_at
      FROM
        adyen_stored_payment_methods
      GROUP BY
        person_id, adyen_payment_method_info_id
    )
    t2
    ON t1.adyen_payment_method_info_id = t2.adyen_payment_method_info_id
    AND t1.created_at = t2.created_at;
