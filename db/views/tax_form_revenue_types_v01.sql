SELECT  
  tax_forms.id           AS tax_form_id
  , revenue_streams.code AS revenue_stream_code
FROM 
  tax_forms
JOIN tax_form_revenue_streams
  ON tax_forms.id = tax_form_revenue_streams.tax_form_id
JOIN revenue_streams
  ON tax_form_revenue_streams.revenue_stream_id = revenue_streams.id
