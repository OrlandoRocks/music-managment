class CreatePreorderInstantGratSongs < ActiveRecord::Migration[4.2]
  def change
    create_table :preorder_instant_grat_songs do |t|
      t.integer :song_id
      t.integer :salepoint_preorder_data_id
      
      t.timestamps
    end
  end
end
