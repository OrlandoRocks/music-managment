class AddCountryWebsiteToAdvertisements < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE advertisements ADD country_website_id CHAR(3) NOT NULL DEFAULT 1 COMMENT 'Country that this advertisement will be displayed in' AFTER id");
  end

  def self.down
    remove_column :advertisements, :country_website_id
  end
end
