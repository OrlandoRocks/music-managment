class FixItalianProductItemRules < ActiveRecord::Migration[4.2]

  # Explanation of Parent_ID on Product Item Rules
  #
  # if it is the first product_item_rule for a product_item, the parent ID should be nil (or zero)
  # if it is not the first product_item_rule and the rule_type is NOT “price_only”, then the parent_id should be the ID of the first product_item_rule
  # if it is not the first product_item_rule and the rule_type is “price_only”, then the parent_id should be the ID of the last product_item_rule
  # if all product_item_rules have rule_type “price_only” then there is no parent ID on any of them
  # if there is only one product_item_rule that is not of type "price_only" and any number of "price_only" product_item_rules, all parent_ids are nil

  def up
    {
      "696" => 695,
      "697" => 696,
      "698" => 695,
      "699" => 698,
      "701" => 700,
      "702" => 701,
      "703" => 700,
      "704" => 703,
      "706" => 705,
      "707" => 706,
      "708" => 705,
      "709" => 708,
      "711" => 710,
      "712" => 711,
      "713" => 710,
      "714" => 713,
      "716" => 715,
      "717" => 715,
      "719" => 718,
      "720" => 718,
      "722" => 721,
      "723" => 721,
      "725" => 724,
      "726" => 724,
      "728" => 727,
      "729" => 727,
      "731" => 730,
      "732" => 730,
      "734" => 733,
      "735" => 733,
      "737" => 736,
      "738" => 736,
      "740" => 739,
      "741" => 739,
      "743" => 742,
      "744" => 742,
      "747" => 746,
      "748" => 746,
      "754" => 753,
      "755" => 753,
      "756" => 755,
      "758" => 757,
      "759" => 757,
      "760" => 759,
      "762" => 761,
      "763" => 761,
      "764" => 763,
      "766" => 765,
      "767" => 765,
      "768" => 767,
      "770" => 769,
      "771" => 769,
      "772" => 771
    }.each do |pir_id, parent_id|
      ProductItemRule.find(pir_id).update_attribute(:parent_id, parent_id)
    end
  end

  def down
    {
      "696"=>44,
      "697"=>45,
      "698"=>44,
      "699"=>47,
      "701"=>49,
      "702"=>50,
      "703"=>49,
      "704"=>52,
      "706"=>54,
      "707"=>55,
      "708"=>54,
      "709"=>57,
      "711"=>59,
      "712"=>60,
      "713"=>59,
      "714"=>62,
      "716"=>64,
      "717"=>64,
      "719"=>68,
      "720"=>68,
      "722"=>72,
      "723"=>72,
      "725"=>76,
      "726"=>76,
      "728"=>90,
      "729"=>90,
      "731"=>94,
      "732"=>94,
      "734"=>98,
      "735"=>98,
      "737"=>110,
      "738"=>110,
      "740"=>114,
      "741"=>114,
      "743"=>118,
      "744"=>118,
      "747"=>254,
      "748"=>254,
      "754"=>270,
      "755"=>270,
      "756"=>272,
      "758"=>274,
      "759"=>274,
      "760"=>276,
      "762"=>278,
      "763"=>278,
      "764"=>280,
      "766"=>282,
      "767"=>282,
      "768"=>284,
      "770"=>286,
      "771"=>286,
      "772"=>288
    }.each do |pir_id, parent_id|
      ProductItemRule.find(pir_id).update_attribute(:parent_id, parent_id)
    end
  end
end

