class RenameKickbackTransactionsTable < ActiveRecord::Migration[4.2]
  def self.up
    rename_table :kickback_transactions, :friend_referrals
  end

  def self.down
    rename_table :friend_referrals, :kickback_transactions
  end
end
