class CreateAdyenStoredPaymentMethods < ActiveRecord::Migration[6.0]
  def change
    create_table :adyen_stored_payment_methods do |t|
      t.references :person, null: false
      t.references :country, null: false
      t.references :corporate_entity, null: false
      t.string :recurring_reference, null: false
      t.timestamps
    end
  end
end
