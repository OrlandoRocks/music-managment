class CreateYoutubePreferencesExports < ActiveRecord::Migration[4.2]
  def self.up
    create_table :youtube_preferences_exports do |t|
      t.datetime  :from
      t.datetime  :to
      t.string    :filename

      t.timestamps
    end
  end

  def self.down
    drop_table :youtube_preferences_exports
  end
end
