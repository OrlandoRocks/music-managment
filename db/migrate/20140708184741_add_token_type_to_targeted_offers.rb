class AddTokenTypeToTargetedOffers < ActiveRecord::Migration[4.2]
  def up
    add_column :targeted_offers, :token_type, :string, :limit=> 15, :default => "join_token"
  end

  def down
    remove_column :targeted_offers, :token_type
  end
end
