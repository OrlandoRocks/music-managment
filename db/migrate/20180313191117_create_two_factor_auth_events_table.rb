class CreateTwoFactorAuthEventsTable < ActiveRecord::Migration[4.2]
  def change
    create_table :two_factor_auth_events do |t|
      t.string :type
      t.string :page
      t.string :action
      t.boolean :successful
      t.references :two_factor_auth
      t.timestamps
    end

    add_index(:two_factor_auth_events, :type)
    add_index(:two_factor_auth_events, :page)
    add_index(:two_factor_auth_events, :action)
    add_index(:two_factor_auth_events, :two_factor_auth_id)
  end
end
