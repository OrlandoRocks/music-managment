class CreateMumaSongRegistrationHistory < ActiveRecord::Migration[4.2]
  def self.up
    create_table :muma_song_registration_history do |t|
      t.integer  :song_code
      t.string   :society_code, :limit => 12
      t.datetime :entry_created_at
      t.datetime :processed_at
      t.string   :status, :limit => 2
      t.text     :message
      t.string   :type, :limit => 10
      t.timestamps
    end
  end

  def self.down
    drop_table :muma_song_registration_history
  end
end
