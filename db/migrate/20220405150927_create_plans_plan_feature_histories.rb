class CreatePlansPlanFeatureHistories < ActiveRecord::Migration[6.0]
  def change
    create_table :plans_plan_feature_histories do |t|
      t.references :plan, null: false
      t.references :plan_feature, null: false
      t.string :change_type, null: false

      t.timestamps
    end
  end
end
