class AddIpToPeople < ActiveRecord::Migration[4.2]
  def change
    add_column :people, :last_logged_in_ip, :string
  end
end
