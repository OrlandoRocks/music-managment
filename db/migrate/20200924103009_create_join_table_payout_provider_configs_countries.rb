class CreateJoinTablePayoutProviderConfigsCountries < ActiveRecord::Migration[6.0]
  def change
    create_join_table :countries, :payout_provider_configs do |t|
      t.index [:country_id, :payout_provider_config_id], name: 'countries_on_payout_provider_configs'
      t.index [:payout_provider_config_id, :country_id], name: 'payout_provider_configs_on_countries'
    end
  end
end
