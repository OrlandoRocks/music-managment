class AddLandrProducts < ActiveRecord::Migration[4.2]
  def up
    execute("ALTER TABLE purchases CHANGE related_type related_type enum('Album','Single','Ringtone','Entitlement','Product','Renewal','ItunesUserReport','MusicVideo','FeatureFilm','Salepoint','Song','Widget','Composer','Video','SalepointSubscription','CreditUsage','Booklet','SalepointPreorderData','SoundoutReport','FacebookRecognitionServicePurchase','YoutubeMonetization','PreorderPurchase', 'MasteredTrack');")

    product = Product.create({
      created_by_id:      1013710,
      country_website_id: 1,
      name:               "LANDR Instant Mastering",
      display_name:       "LANDR Instant Mastering",
      description:        "Instant mastering of one track. Note: You must complete the purchase process in six hours or your order will disappear.",
      status:             "Active",
      product_type:       "Ad Hoc",
      is_default:         1,
      applies_to_product: "None",
      sort_order:         1,
      renewal_level:      "None",
      price:              9.99,
      currency:           "USD"
    })

    product_item = ProductItem.create({
      product_id:   product.id,
      name:         "LANDR Instant Mastering",
      description:  "LANDR Instant Mastering",
      price:        9.99,
      currency:     "USD"
    })

    product_item_rule = ProductItemRule.create({
      product_item_id:      product_item.id,
      rule_type:            "inventory",
      rule:                 "price_for_each",
      inventory_type:       "MasteredTrack",
      true_false:           false,
      quantity:             0,
      unlimited:            1,
      minimum:              0,
      maximum:              0,
      price:                0,
      currency:             "USD"
    })


    product = Product.create({
      created_by_id:      1013710,
      country_website_id: 2,
      name:               "LANDR Instant Mastering",
      display_name:       "LANDR Instant Mastering",
      description:        "Instant mastering of one track. Note: You must complete the purchase process in six hours or your order will disappear.",
      status:             "Active",
      product_type:       "Ad Hoc",
      is_default:         1,
      applies_to_product: "None",
      sort_order:         1,
      renewal_level:      "None",
      price:              9.99,
      currency:           "CAD"
    })

    product_item = ProductItem.create({
      product_id:   product.id,
      name:         "LANDR Instant Mastering",
      description:  "LANDR Instant Mastering",
      price:        9.99,
      currency:     "CAD"
    })

    product_item_rule = ProductItemRule.create({
      product_item_id:      product_item.id,
      rule_type:            "inventory",
      rule:                 "price_for_each",
      inventory_type:       "MasteredTrack",
      true_false:           false,
      quantity:             0,
      unlimited:            1,
      minimum:              0,
      maximum:              0,
      price:                0,
      currency:             "CAD"
    })
  end

  def down
    execute("ALTER TABLE purchases CHANGE related_type related_type enum('Album','Single','Ringtone','Entitlement','Product','Renewal','ItunesUserReport','MusicVideo','FeatureFilm','Salepoint','Song','Widget','Composer','Video','SalepointSubscription','CreditUsage','Booklet','SalepointPreorderData','SoundoutReport','FacebookRecognitionServicePurchase','YoutubeMonetization','PreorderPurchase');")
  end
end
