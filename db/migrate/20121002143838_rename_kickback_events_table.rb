class RenameKickbackEventsTable < ActiveRecord::Migration[4.2]
  def self.up
    rename_table :kickback_events, :friend_referral_events
  end

  def self.down
    rename_table :friend_referral_events, :kickback_events
  end
end
