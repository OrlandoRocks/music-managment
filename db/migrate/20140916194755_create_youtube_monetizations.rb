class CreateYoutubeMonetizations < ActiveRecord::Migration[4.2]
  def up
  	execute("ALTER TABLE purchases CHANGE related_type related_type enum('Album','Single','Ringtone','Entitlement','Product','Renewal','ItunesUserReport','MusicVideo','FeatureFilm','Salepoint','Song','Widget','Composer','Video','SalepointSubscription','CreditUsage','Booklet','SalepointPreorderData','SoundoutReport','FacebookRecognitionServicePurchase', 'YoutubeMonetization');")

    create_table :youtube_monetizations do |t|
    	t.integer	 :person_id, unique: true, null: false
      t.boolean  :pub_opted_in, :default => false
      t.datetime :effective_date
      t.datetime :termination_date
      t.datetime :agreed_to_terms_at

      t.timestamps
    end

    add_index :youtube_monetizations, :person_id

    create_table :ytm_ineligible_songs do |t|
      t.integer :song_id, unique: true, null: false

      t.timestamps
    end
  end

  def down
    execute("ALTER TABLE purchases CHANGE related_type related_type enum('Album','Single','Ringtone','Entitlement','Product','Renewal','ItunesUserReport','MusicVideo','FeatureFilm','Salepoint','Song','Widget','Composer','Video','SalepointSubscription','CreditUsage','Booklet','SalepointPreorderData','SoundoutReport','FacebookRecognitionServicePurchase');")
  
  	drop_table :youtube_monetizations
    drop_table :ytm_ineligible_songs
	end
end

