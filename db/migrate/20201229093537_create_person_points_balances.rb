class CreatePersonPointsBalances < ActiveRecord::Migration[6.0]
  def change
    create_table :person_points_balances do |t|
      t.references :person, foreign_key: true, index: { unique: true }, null: false, type: 'int(11) unsigned'
      t.integer :balance, null: false

      t.timestamps
    end
  end
end
