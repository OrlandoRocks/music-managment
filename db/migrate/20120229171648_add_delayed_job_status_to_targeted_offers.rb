class AddDelayedJobStatusToTargetedOffers < ActiveRecord::Migration[4.2]
  def self.up
    add_column :targeted_offers, :delayed_job_status, :string, :default=>nil
  end

  def self.down
    remove_column :targeted_offers, :delayed_job_status
  end
end
