class CreateYtmBlockedSongs < ActiveRecord::Migration[4.2]
  def up
    create_table :ytm_blocked_songs do |t|
      t.integer :song_id
      t.timestamps
    end

    add_index :ytm_blocked_songs, :song_id
  end

  def down
    drop_table :ytm_blocked_songs
  end
end
