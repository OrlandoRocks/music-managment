class AddIndexToSongCopyrightClaim < ActiveRecord::Migration[6.0]
  def change
    add_index :song_copyright_claims, :song_id
  end
end
