class ChangeTextEvidenceToText < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      change_column :dispute_evidences, :text_evidence, :text, default: nil
      change_column_default :disputes, :amount_cents, from: nil, to: 0
    end
  end

  def down
    safety_assured do
      change_column :dispute_evidences, :text_evidence, :string, default: nil
      change_column_default :disputes, :amount_cents, from: 0, to: nil
    end
  end
end
