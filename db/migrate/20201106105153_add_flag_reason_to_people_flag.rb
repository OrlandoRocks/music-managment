class AddFlagReasonToPeopleFlag < ActiveRecord::Migration[6.0]
  def change
    add_reference :people_flags, :flag_reason, foreign_key: true
  end
end
