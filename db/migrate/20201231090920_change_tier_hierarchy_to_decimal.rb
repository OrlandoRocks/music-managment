class ChangeTierHierarchyToDecimal < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      change_column :tiers, :hierarchy, :decimal, precision: 5, scale: 2
    end
  end

  def down
    safety_assured do
      change_column :tiers, :hierarchy, :float
    end
  end
end
