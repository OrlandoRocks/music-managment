class AddCurrencyToPurchases < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE purchases ADD currency CHAR(3) NOT NULL DEFAULT 'USD'  COMMENT 'Currency for cost_cents' AFTER discount_cents")
  end

  def self.down
    remove_column :purchases, :currency
  end
end
