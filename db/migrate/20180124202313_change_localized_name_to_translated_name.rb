class ChangeLocalizedNameToTranslatedName < ActiveRecord::Migration[4.2]
  def up
    rename_column :songs, :localized_name, :translated_name
  end

  def down
    rename_column :songs, :translated_name, :localized_name
  end
end
