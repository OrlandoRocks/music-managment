class CreateMumaSongSocietiesTable < ActiveRecord::Migration[4.2]
  def self.up
    create_table :muma_song_societies do |t|
      t.integer   :song_code
      t.string    :society_code, :limit => 4
      t.string    :work_num
      t.datetime  :registered_at, :default => nil
      t.integer   :source_id
      t.timestamps
    end
  end

  def self.down
    drop_table :muma_song_societies
  end
end
