class AddIndexesToStores < ActiveRecord::Migration[4.2]
  def change
    add_index :stores, :in_use_flag
    add_index :stores, :is_active
    add_index :stores, :abbrev
    add_index :stores, :short_name
  end
end
