class CreateMassAdjustmentTables < ActiveRecord::Migration[4.2]
  def change
    create_table :mass_adjustment_batches do |t|
      t.string :admin_message
      t.string :customer_message
      t.string :csv_file_name
      t.boolean :allow_negative_balance, default: false
      t.string :status
      t.integer :created_by_id
      t.timestamps
    end

    create_table :mass_adjustment_entries do |t|
      t.integer :person_id
      t.integer :mass_adjustment_batch_id
      t.string :status
      t.decimal :amount, precision: 15, scale: 2
      t.boolean :balance_turns_negative
    end

    execute("ALTER TABLE mass_adjustment_entries ADD FOREIGN KEY (mass_adjustment_batch_id) REFERENCES mass_adjustment_batches(id)")
  end
end
