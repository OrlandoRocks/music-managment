class SetCmsItaly < ActiveRecord::Migration[4.2]
  def up
    italian_site = CountryWebsite.find(7)
    italian_site.cms_sets.create!(callout_type: "interstitial", callout_name: "None", start_tmsp: Time.now + 1.second)
    italian_site.cms_sets.create!(callout_type: "login", callout_name: "None", start_tmsp: Time.now + 1.second)
    italian_site.cms_sets.create!(callout_type: "dashboard", callout_name: "None", start_tmsp: Time.now + 1.second)
  end

  def down
    CountryWebsite.find(7).cms_sets.each { |cs| cs.destroy }
  end
end
