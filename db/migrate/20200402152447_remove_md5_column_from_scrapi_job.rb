class RemoveMd5ColumnFromScrapiJob < ActiveRecord::Migration[6.0]
  def change
    safety_assured { remove_column :scrapi_jobs, :md5, :string }
  end
end
