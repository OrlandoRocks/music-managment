class CreateUserStoreRoyaltyRates < ActiveRecord::Migration[6.0]
  def change
    create_table :user_store_royalty_rates, comment: "Royalty rate config for VIP users at sub store level" do |t|
      t.belongs_to :royalty_sub_store, null: false
      t.belongs_to :person, null: false
      t.decimal :royalty_rate, null: false, default: 0.8, precision: 14, scale: 2, comment: "value between 0 and 1"

      t.timestamps
    end
  end
end
