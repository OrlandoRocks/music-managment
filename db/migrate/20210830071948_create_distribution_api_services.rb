class CreateDistributionApiServices < ActiveRecord::Migration[6.0]
  def up
    create_table :distribution_api_services do |t|
      t.string :uuid, index: { unique: true }, null: false
      t.string :name, index: { unique: true }, null: false
      t.timestamps
    end
  end

  def down
    drop_table :distribution_api_services
  end
end
