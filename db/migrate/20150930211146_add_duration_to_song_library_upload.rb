class AddDurationToSongLibraryUpload < ActiveRecord::Migration[4.2]
  def change
    add_column :song_library_uploads, :duration, :integer
  end
end
