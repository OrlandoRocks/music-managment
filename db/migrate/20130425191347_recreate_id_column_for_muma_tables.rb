class RecreateIdColumnForMumaTables < ActiveRecord::Migration[4.2]
  def self.up
    add_column :muma_song_ips, :id, :primary_key
    add_column :muma_song_societies, :id, :primary_key
  end

  def self.down
    remove_column :muma_song_ips, :id
    remove_column :muma_song_societies, :id
  end
end
