class PopulateHubspotEvents < ActiveRecord::Migration[4.2]
  def up
    hubspot_event_parameters =
    [
      {event_id: "000000148370"},
      {event_id: "000000148371"},
      {event_id: "000000148372"},
      {event_id: "000000159754"},
      {event_id: "000000153107"},
      {event_id: "000000153108"},
      {event_id: "000000153109"},
      {event_id: "000000170859"},
      {event_id: "000000170860"},
      {event_id: "000000170862"},
      {event_id: "000000193170"},
      {event_id: "000000193172"},
      {event_id: "000000193173"},
      {event_id: "000000185085"},
      {event_id: "000000313972"},
      {event_id: "000000148373"},
      {event_id: "000000148374"},
      {event_id: "000000148375"},
      {event_id: "000000159755"},
      {event_id: "000000467440"},
      {event_id: "000000467441"},
      {event_id: "000000467443"},
      {event_id: "000000170863"},
      {event_id: "000000170864"},
      {event_id: "000000170869"},
      {event_id: "000000193174"},
      {event_id: "000000193175"},
      {event_id: "000000193176"},
      {event_id: "000000185087"},
      {event_id: "000000280570"}
    ]

    hubspot_event_parameters.each do |event_params|
      HubspotEvent.create!(event_id: event_params[:event_id]) if HubspotEvent.where(event_id: event_params[:event_id]).empty?
    end

    album_product_ids = [1,6,7,18,19,20,21,26,27,28,29,31,33,36,37,38,39,40,51,52,53,54,66,69,77,78,79,80,86,87,95,96,121,122,125,126,153,157,158,159,160,166,167,168,184,186,198,201,202,203,204,209,210,211,226,228,285,288,289,290,291,296,297,298,313,315]
    single_product_ids = [3,8,22,23,24,25,34,41,42,43,44,45,55,56,57,58,68,70,81,82,83,84,97,98,123,124,127,128,155,161,162,163,164,170,171,172,185,187,199,205,206,207,208,212,213,214,227,229,286,292,293,294,295,299,300,301,314,316]
    ringtone_product_ids = [2,9,13,14,15,16,17,35,46,47,48,49,50,59,60,61,62,67,71,72,73,74,75,76,99,100,101,102,103,104,111,112,113,114,115,116,117,118,119,120,174,175,179,180,181,182,183,216,217,221,222,223,224,225,303,304,308,309,310,311,312]
    youtube_product_ids = [145,146,194,236,323,325]
    facebook_low_product_ids = [1,2,7,10,13]
    facebook_med_product_ids = [3,4,8,11,14]
    facebook_high_product_ids = [5,6,9,12,15]
    fan_reviews_starter_product_ids = [133,136,188,230,317]
    fan_reviews_premium_product_ids = [135,138,190,232,319]
    fan_reviews_enhanced_product_ids = [134,137,189,231,318]
    landr_product_ids = [149,150,196,238]

    product_lists ={
      "Product" => [
                    {ids: album_product_ids, add_to_cart_id: 11, purchase_id: 26},
                    {ids: single_product_ids, add_to_cart_id: 12, purchase_id: 27},
                    {ids: ringtone_product_ids, add_to_cart_id: 13, purchase_id: 28},
                    {ids: youtube_product_ids, add_to_cart_id: 4, purchase_id: 9},
                    {ids: fan_reviews_starter_product_ids, add_to_cart_id: 1, purchase_id: 16},
                    {ids: fan_reviews_enhanced_product_ids, add_to_cart_id: 2, purchase_id: 17},
                    {ids: fan_reviews_premium_product_ids, add_to_cart_id: 3, purchase_id: 18},
                    {ids: landr_product_ids, add_to_cart_id: 14, purchase_id: 29}
                   ],
      "FacebookRecognitionServiceProduct" => [
                                              {ids: facebook_low_product_ids, add_to_cart_id: 5, purchase_id: 20},
                                              {ids: facebook_med_product_ids, add_to_cart_id: 6, purchase_id: 21},
                                              {ids: facebook_high_product_ids, add_to_cart_id: 7, purchase_id: 22}
                                             ]

    }

    product_lists.each do |product_type, product_mappings|
      product_mappings.each do |product_mapping|
        product_mapping[:ids].each do |product_id|
          MarketingEventTrigger.create!(related_id: product_id, related_type: product_type, provider_id: product_mapping[:add_to_cart_id], event_trigger: "ADDED TO CART", provider: "Hubspot") if MarketingEventTrigger.where(related_id: product_id, provider_id: product_mapping[:add_to_cart_id]).empty?
          MarketingEventTrigger.create!(related_id: product_id, related_type: product_type, provider_id: product_mapping[:purchase_id], event_trigger: "PURCHASED", provider: "Hubspot") if MarketingEventTrigger.where(related_id: product_id, provider_id: product_mapping[:purchase_id]).empty?
        end
      end
    end

  end

  def down
  end
end
