class AddStatusInfoToMassAdjustmentEntries < ActiveRecord::Migration[4.2]
  def change
    add_column :mass_adjustment_entries, :status_info, :string
  end
end
