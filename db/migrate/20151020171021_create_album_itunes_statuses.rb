class CreateAlbumItunesStatuses < ActiveRecord::Migration[4.2]
  def change
    create_table :album_itunes_statuses do |t|
      t.integer :album_id, null: false
      t.string :content_review_status
      t.string :itunes_connect_status
      t.string :stores_list, limit: 1000
      t.datetime :itunes_created_at
      t.string :content_state_status

      t.timestamps
    end

    add_index :album_itunes_statuses, :album_id, unique: true
  end
end
