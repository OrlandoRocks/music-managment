class AlterVideosColumn < ActiveRecord::Migration[4.2]
  def self.up
    change_column :videos, :sale_date, :date, :null => true
    change_column :videos, :orig_release_year, :date, :null => true
  end

  def self.down
    change_column :videos, :sale_date, :date, :null => false
    change_column :videos, :orig_release_year, :date, :null => false
  end
end
