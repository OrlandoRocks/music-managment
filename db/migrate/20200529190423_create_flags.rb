class CreateFlags < ActiveRecord::Migration[6.0]
  def change
    create_table :flags do |t|
      t.string :category
      t.string :name

      t.timestamps
    end
  end
end
