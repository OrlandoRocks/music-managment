class CreateAlbumFlaggedContentTable < ActiveRecord::Migration[4.2]
  def up
   create_table :album_flagged_content do |t|
     t.integer :album_id, null: false
     t.text :flagged_content, null: false
     t.timestamps
   end

   add_index :album_flagged_content, :album_id
 end

 def down
   drop_table :album_flagged_content
 end
end
