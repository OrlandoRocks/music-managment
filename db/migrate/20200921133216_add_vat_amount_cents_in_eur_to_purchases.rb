class AddVatAmountCentsInEurToPurchases < ActiveRecord::Migration[6.0]
  def change
    add_column :purchases, :vat_amount_cents_in_eur, :int
  end
end
