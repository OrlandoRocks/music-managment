class CreateCoverSongMetadata < ActiveRecord::Migration[6.0]
  def change
    create_table :cover_song_metadata do |t|
      t.integer :song_id, index: true, foreign_key: true, null: false
      t.boolean :cover_song, default: false, null: false
      t.boolean :licensed, null: true
      t.boolean :will_get_license, null: true

      t.timestamps
    end
  end
end
