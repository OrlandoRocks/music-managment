class BandPageDisplayMediaPlayer < ActiveRecord::Migration[4.2]
  def self.up
    add_column :bandpages, :display_media_player, :boolean, :default => true
  end

  def self.down
    remove_column :bandpages, :display_media_player
  end
end
