class AddPaymentMethodAndErrorCodeToPayuTransactions < ActiveRecord::Migration[6.0]
  def change
    add_column :payu_transactions, :payment_method, :string, null: true
    add_column :payu_transactions, :error_code, :string, null: true
  end
end
