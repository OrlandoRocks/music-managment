class CreateTwoFactorAuthPrompts < ActiveRecord::Migration[4.2]
  def change
    create_table :two_factor_auth_prompts do |t|
      t.belongs_to :person
      t.integer :dismissed_count, default: 0
      t.timestamp :prompt_at

      t.timestamps
    end
    add_index :two_factor_auth_prompts, :person_id
  end
end
