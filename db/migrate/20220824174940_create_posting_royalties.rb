class CreatePostingRoyalties < ActiveRecord::Migration[6.0]
  def change
    create_table :posting_royalties, comment: "Stores posting related information required by finance team." do |t|
      t.string :posting_id, null: false, index: true
      t.string :corporate_entity_id, null: false, index: true
      t.string :currency, null: false, limit: 3, index: true
      t.decimal :total_amount_in_usd, precision: 23, scale: 6
      t.decimal :encumbrance_amount_in_usd, precision: 23, scale: 6
      t.decimal :split_adjustments_in_usd, precision: 23, scale: 6
      t.decimal :total_you_tube_amount_in_usd, precision: 23, scale: 6
      t.decimal :converted_amount, precision: 23, scale: 6
      t.decimal :base_fx_rate, precision: 23, scale: 14
      t.decimal :fx_coverage_rate, precision: 23, scale: 14
      t.decimal :fx_rate, precision: 23, scale: 14
      t.string :approved_by
      t.datetime :approved_on
      t.column :state, "enum('pending', 'approved')", default: 'pending'
      t.timestamps
    end
  end
end
