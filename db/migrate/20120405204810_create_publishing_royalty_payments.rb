class CreatePublishingRoyaltyPayments < ActiveRecord::Migration[4.2]
  def self.up
    
    create_table :royalty_payments do |t|
      t.integer :composer_id
      t.decimal :amount
      t.integer :period_year
      t.integer :period_interval
      t.date    :period_sort
      
      #Use the standard paperclip db columns for potential future compatibility
      t.string  :pdf_summary_file_name
      t.string  :pdf_summary_file_type
      t.integer :pdf_summary_file_size
      t.integer :pdf_summary_updated_at
      
      t.timestamps
    end
    
    execute "ALTER TABLE royalty_payments ADD COLUMN period_type ENUM('monthly', 'quarterly') DEFAULT 'quarterly'"
     
  end

  def self.down
    drop_table :royalty_payments
  end
end
