class AddNullConstraintToPayoutProviderConfigCorporateEntity < ActiveRecord::Migration[6.0]
  def up
    data_file_key = ENV.fetch('CONFIG_TO_CORPORATE_ENTITIES_MAPPING', 'config_to_corporate_entities_mapping.csv')
    # You must have corporate entities, or
    # THIS WILL BREAK
    # this is a brittle migration
    CorporateEntityMappingService.new(data_file_key).map_to_payout_provider_configs

    safety_assured { change_column :payout_provider_configs, :corporate_entity_id, :bigint, null: false }
  end

  def down
    safety_assured { change_column :payout_provider_configs, :corporate_entity_id, :bigint, null: true }
  end
end
