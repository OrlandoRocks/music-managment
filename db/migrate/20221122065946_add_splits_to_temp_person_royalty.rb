class AddSplitsToTempPersonRoyalty < ActiveRecord::Migration[6.0]
  def change
    safety_assured { remove_column :temp_person_royalties, :split_amount_in_usd }
    safety_assured { remove_column :temp_person_royalties, :split_amount_in_user_currency }
    add_column :temp_person_royalties, :split_computed, :boolean
    change_column_default :temp_person_royalties, :split_computed, false
  end
end
