class ChangeRelatableToRelatedOnCopyrightStatuses < ActiveRecord::Migration[6.0]
  def change
    safety_assured { rename_column :copyright_statuses, :relatable_id, :related_id  }
    safety_assured { rename_column :copyright_statuses, :relatable_type, :related_type }

    safety_assured { rename_index :copyright_statuses, [:relatable_id, :relatable_type], [:related_id, :related_type] }
  end
end
