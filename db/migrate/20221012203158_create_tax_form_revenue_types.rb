class CreateTaxFormRevenueTypes < ActiveRecord::Migration[6.0]
  def change
    replace_view :tax_form_revenue_types, version: 1, revert_to_version: 1
  end
end
