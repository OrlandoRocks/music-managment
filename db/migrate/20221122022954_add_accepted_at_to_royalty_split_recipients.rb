class AddAcceptedAtToRoyaltySplitRecipients < ActiveRecord::Migration[6.0]
  def change
    add_column :royalty_split_recipients, :accepted_at, :timestamp
  end
end
