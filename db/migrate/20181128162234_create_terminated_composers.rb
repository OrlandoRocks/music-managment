class CreateTerminatedComposers < ActiveRecord::Migration[4.2]
  def change 
    create_table :terminated_composers do |t|
      t.integer :composer_id
      t.string :termination_type
      t.string :alt_provider_acct_id
      t.string :reason
      t.datetime :effective_date
      t.timestamps
    end

    add_index :terminated_composers, :composer_id
  end
end
