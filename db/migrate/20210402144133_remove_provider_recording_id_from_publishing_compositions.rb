class RemoveProviderRecordingIdFromPublishingCompositions < ActiveRecord::Migration[6.0]
  def self.up
    safety_assured { remove_column :publishing_compositions, :provider_recording_id }
  end

  def self.down
    safety_assured { add_column :publishing_compositions, :provider_recording_id, :string }
  end
end
