class CreateStoredPaypalAccounts < ActiveRecord::Migration[4.2]
  def self.up
     up_sql = %Q(CREATE TABLE `stored_paypal_accounts` (
       `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
       `person_id` INT(10) UNSIGNED DEFAULT NULL COMMENT 'person ID',
       `paypal_transaction_id` int(11) UNSIGNED DEFAULT NULL COMMENT 'foreign key',
       `archived_at` datetime DEFAULT NULL,
       `updated_at` TIMESTAMP NULL COMMENT 'Datetime of latest update',
       `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',
       PRIMARY KEY (`id`),
       KEY `stored_paypal_pt_person_id` (`person_id`),
       KEY `stored_paypal_pt_transaction_id` (`paypal_transaction_id`),
       CONSTRAINT `FK_stored_paypal_pt_person_id` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`),
       CONSTRAINT `FK_stored_paypal_pt_transaction_id` FOREIGN KEY (`paypal_transaction_id`) REFERENCES `paypal_transactions`(`id`)
     ) ENGINE=InnoDB DEFAULT CHARSET=utf8;)
     execute up_sql
     execute("ALTER TABLE paypal_transactions ADD CONSTRAINT `FK_referenced_paypal_account_id` FOREIGN KEY (`referenced_paypal_account_id`) REFERENCES `stored_paypal_accounts`(`id`)")
   end

   def self.down
     execute("ALTER TABLE paypal_transactions DROP FOREIGN KEY `FK_referenced_paypal_account_id`")
     drop_table :stored_paypal_accounts
   end
end
