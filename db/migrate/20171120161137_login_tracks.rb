class LoginTracks < ActiveRecord::Migration[4.2]
  def change
    create_table :login_tracks do |t|
      t.integer    :trackable_id, null: false
      t.string     :trackable_type, null: false
      t.references :login_event, null: false

      t.timestamps
    end

    add_index :login_tracks, [:trackable_id, :trackable_type]
  end
end
