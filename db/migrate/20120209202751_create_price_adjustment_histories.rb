class CreatePriceAdjustmentHistories < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `price_adjustment_histories` (
          `id` int(10) unsigned     NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
          `purchase_id` int(10)     unsigned NOT NULL COMMENT 'Foreign Key to Purchase Table',
          `admin_id`    int(10)     unsigned NOT NULL COMMENT 'Foreign Key to People Table',
          `adjustment_cents`        int(11) DEFAULT NULL,
          `original_cost_cents`     int(11) DEFAULT NULL,
          `original_discount_cents` int(11) DEFAULT NULL,
          `new_cost_cents`          int(11) DEFAULT NULL,
          `note`                    text NOT NULL COMMENT 'Note', 
          `updated_at`              timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
          `created_at`              timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',
          PRIMARY KEY (`id`),
          KEY `purchase_id` (`purchase_id`),
          KEY `admin_id`    (`admin_id`),
          CONSTRAINT `FK_price_adj_purchases` FOREIGN KEY (`purchase_id`) REFERENCES `purchases` (`id`),
          CONSTRAINT `FK_price_adj_people` FOREIGN KEY (`admin_id`) REFERENCES `people` (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;)
    execute up_sql
    
    execute("ALTER TABLE purchases ADD price_adjustment_histories_count INT(11) NOT NULL DEFAULT 0 COMMENT 'Count of times the purchase has been price adjusted' AFTER targeted_product_id");
  end

  def self.down
    drop_table    :price_adjustment_histories
    remove_column :purchases, :price_adjustment_histories_count
  end
end
