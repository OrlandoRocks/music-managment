class CreateFlagTransitions < ActiveRecord::Migration[6.0]
  def change
    create_table :flag_transitions do |t|
      t.integer :admin_id
      t.integer :person_id
      t.integer :flag_id
      t.string :add_remove
      t.text :comment

      t.timestamps
    end
    add_index :flag_transitions, [:admin_id, :person_id, :flag_id]
  end
end
