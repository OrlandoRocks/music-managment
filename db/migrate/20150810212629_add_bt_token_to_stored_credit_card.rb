class AddBtTokenToStoredCreditCard < ActiveRecord::Migration[4.2]
  def change
    add_column :stored_credit_cards, :bt_token, :string
  end
end
