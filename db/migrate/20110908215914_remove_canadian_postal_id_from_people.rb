class RemoveCanadianPostalIdFromPeople < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :people, :canada_postal_code_id
  end

  def self.down
    execute("ALTER TABLE people
     ADD COLUMN canada_postal_code_id int(11) NULL")
  end
end
