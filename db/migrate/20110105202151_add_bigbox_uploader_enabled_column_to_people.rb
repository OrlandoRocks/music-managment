class AddBigboxUploaderEnabledColumnToPeople < ActiveRecord::Migration[4.2]
  def self.up
		add_column :people, :bigbox_uploader_enabled, :boolean, :default => false
  end

  def self.down
		remove_column :people, :bigbox_uploader_enabled
  end
end
