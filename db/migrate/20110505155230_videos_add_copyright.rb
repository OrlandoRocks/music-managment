class VideosAddCopyright < ActiveRecord::Migration[4.2]
  def self.up
    add_column :videos, :copyright, :string
  end

  def self.down
    remove_column :videos, :copyright
  end
end
