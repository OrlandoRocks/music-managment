class UpdateCharsetForInvoiceStaticCustomerAddresses < ActiveRecord::Migration[6.0]
  def self.up
    safety_assured do
        execute %(
          ALTER TABLE invoice_static_customer_infos
            CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
          )
        execute %(
          ALTER TABLE invoice_static_customer_addresses
            CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
          )
    end
 end

 def self.down
    safety_assured do
      execute %(
        ALTER TABLE invoice_static_customer_infos
          CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
        )
      execute %(
        ALTER TABLE invoice_static_customer_addresses
          CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
        )
    end
  end
end
