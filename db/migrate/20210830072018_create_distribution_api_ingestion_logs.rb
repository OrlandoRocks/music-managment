class CreateDistributionApiIngestionLogs < ActiveRecord::Migration[6.0]
  def up
    create_table :distribution_api_ingestion_logs do |t|
      t.references :distribution_api_service, foreign_key: true, index: {name: 'index_distribution_api_ingestion_logs_on_service_id'}, type: :bigint, null: false
      t.string :source_album_id, index: true, null: false
      t.string :json_path, index: {unique: true}, null: false
      t.string :message_id, null: false
      t.string :status
      t.text :source_contentreview_note
      t.text :response_json
      t.text :response_json_received
      t.string :delivery_type
      t.timestamps
    end
  end

  def down
    drop_table :distribution_api_ingestion_logs
  end
end
