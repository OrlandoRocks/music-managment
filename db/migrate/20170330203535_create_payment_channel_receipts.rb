class CreatePaymentChannelReceipts < ActiveRecord::Migration[4.2]
  def up
    create_table :payment_channel_receipts do |t|
      t.references :subscription_purchase
      t.references :person
      t.string :receipt_data
    end
    add_index :payment_channel_receipts, [:person_id, :subscription_purchase_id], :name => 'receipt_person_or_purchase'
  end

  def down
    drop_table :payment_channel_receipts
  end
end
