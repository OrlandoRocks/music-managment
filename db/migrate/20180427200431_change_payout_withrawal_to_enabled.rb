class ChangePayoutWithrawalToEnabled < ActiveRecord::Migration[4.2]
  def change
    rename_column :payout_withdrawal_types, :preferred, :enabled
  end
end
