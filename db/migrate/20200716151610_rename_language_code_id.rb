class RenameLanguageCodeId < ActiveRecord::Migration[6.0]
  def change
    safety_assured { rename_column :songs, :language_code_id, :metadata_language_code_id }
  end
end
