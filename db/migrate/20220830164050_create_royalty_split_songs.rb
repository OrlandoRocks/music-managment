class CreateRoyaltySplitSongs < ActiveRecord::Migration[6.0]
  def change
    create_table :royalty_split_songs do |t|
      t.references :royalty_split, foreign_key: true, null: false, index: true, comment: "Royalty split config id"
      t.references :song, type: :integer, foreign_key: true, null: false, index: { unique: true },
                          comment: "Song associated with a split"

      t.timestamps
    end
  end
end
