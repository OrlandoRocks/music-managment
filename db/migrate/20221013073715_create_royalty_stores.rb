class CreateRoyaltyStores < ActiveRecord::Migration[6.0]
  def change
    create_table :royalty_stores, comment: "Stores information that will be used for royalty purposes. Eventually will be merged with sip_stores" do |t|
      t.string :name, unique: true, null: false

      t.timestamps
    end

    add_index :royalty_stores, :name, :unique => true
  end
end
