class AddFxRateSubmittedByToPostingRoyalties < ActiveRecord::Migration[6.0]
  def change
    add_column :posting_royalties, :fx_rate_submitted_by, :string
    add_column :posting_royalties, :fx_rate_submitted_at, :datetime
  end
end
