class DropTargetedCertsTable < ActiveRecord::Migration[4.2]
  def up
    drop_table :targeted_certs
  end

  def down
    create_table :targeted_certs do |t|
      t.string    :code, :limit => 16
      t.integer   :targeted_offer_id
      t.integer   :redeemed_by
      t.datetime  :redeemed_at
      t.timestamps
    end
  end
end
