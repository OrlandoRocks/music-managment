class AddStatusToLegalDocument < ActiveRecord::Migration[6.0]
  def change
    add_column :legal_documents, :status, :string
  end
end
