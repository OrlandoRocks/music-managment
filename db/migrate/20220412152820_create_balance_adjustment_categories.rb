class CreateBalanceAdjustmentCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :balance_adjustment_categories do |t|
      t.string :kind
      t.string :name, index: { unique: true }, null: false
      t.string :description

      t.timestamps
    end
  end
end
