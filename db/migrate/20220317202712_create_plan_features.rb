class CreatePlanFeatures < ActiveRecord::Migration[6.0]
  def change
    create_table :plan_features do |t|
      t.string :feature, null: false
      t.index :feature, unique: true

      t.timestamps
    end
  end
end
