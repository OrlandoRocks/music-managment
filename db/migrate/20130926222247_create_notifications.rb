class CreateNotifications < ActiveRecord::Migration[4.2]
  def change
    create_table :notifications do |t|
      t.integer :person_id
      t.integer :targeted_notification_id

      t.integer :notification_item_id
      t.string  :notification_item_type

      t.string :type, :default=>"Notification"

      t.text    :text
      t.string  :title
      t.string :url
      t.string :link_text
      t.string :image_url

      t.datetime :first_seen
      t.datetime :first_clicked
      t.datetime :first_archived
      t.integer :click_count

      t.datetime :effective_created_at

      t.timestamps
    end

    add_index :notifications, :person_id
    add_index :notifications, :targeted_notification_id
    add_index :notifications, [:person_id, :targeted_notification_id]

    add_index :notifications, [:notification_item_id, :notification_item_type], :name=>"notification_item"
  end
end
