class AddTerminationToComposers < ActiveRecord::Migration[4.2]
  def change
    add_column :composers, :pub_admin_status, :string, null: true
    add_column :composers, :terminated_at, :datetime, null: true
  end
end
