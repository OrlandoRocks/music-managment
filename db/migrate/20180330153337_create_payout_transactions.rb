class CreatePayoutTransactions < ActiveRecord::Migration[4.2]
  def change
    create_table :payout_transactions do |t|
      t.belongs_to :payout_provider
      t.belongs_to :payout_withdrawal_type
      t.string :client_reference_id
      t.string :provider_status
      t.string :tunecore_status
      t.integer :amount_cents
      t.string :currency
      t.integer :fee_cents
      t.string :transaction_type
      t.text :description
      t.datetime :tunecore_processed_at
      t.datetime :provider_processed_at
      t.belongs_to :tunecore_processor

      t.timestamps
    end
    add_index :payout_transactions, :payout_provider_id
    add_index :payout_transactions, :payout_withdrawal_type_id
    add_index :payout_transactions, :tunecore_processor_id
    add_index :payout_transactions, :client_reference_id
  end
end
