class CreatePayinProviderConfigs < ActiveRecord::Migration[6.0]
  def change
    create_table :payin_provider_configs do |t|
      t.string :name, null: false
      t.column :currency, 'char(3)', null: false
      t.integer :corporate_entity_id, null: false
      t.string :username
      t.string :merchant_account_id
      t.string :merchant_id
      t.string :public_key
      t.string :secrets
      t.boolean :sandbox, null: false

      t.timestamps
    end
  end
end
