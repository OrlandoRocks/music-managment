class AddProviderIdentifierToLegalDocuments < ActiveRecord::Migration[4.2]
  def change
    add_column :legal_documents, :provider_identifier, :string
    add_index  :legal_documents, :provider_identifier
  end
end
