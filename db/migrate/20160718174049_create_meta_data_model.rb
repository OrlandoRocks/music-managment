class CreateMetaDataModel < ActiveRecord::Migration[4.2]
  def change
    create_table :meta_tags do |t|
      t.string :page_name
      t.string :name
      t.string :content
      t.references :country_website
      t.references :person
      t.timestamps
    end

    add_index :meta_tags, [:page_name, :name, :country_website_id], unique: true
    add_index :meta_tags, :country_website_id
    add_index :meta_tags, :person_id

    filename = "db/seed/csv/meta_tags.csv"
    headers  = MetaTag.attribute_names
    body     = CSV.read(filename)

    MetaTag.import(headers, body, validate: false)
  end
end
