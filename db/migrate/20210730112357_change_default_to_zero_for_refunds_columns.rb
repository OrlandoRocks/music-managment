class ChangeDefaultToZeroForRefundsColumns < ActiveRecord::Migration[6.0]
  def change
    change_column_default :refunds, :base_amount_cents, from: nil, to: 0
    change_column_default :refunds, :tax_amount_cents, from: nil, to: 0
    change_column_default :refunds, :total_amount_cents, from: nil, to: 0
    change_column_default :refunds, :vat_cents_in_euro, from: nil, to: 0
  end
end
