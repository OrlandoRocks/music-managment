class CreateRejectionEmails < ActiveRecord::Migration[4.2]
  def change
    create_table :rejection_emails do |t|
      t.string :name, null: false
      t.string :subject, null: false
      t.text :body, null: false
    end
  end
end
