class AddRetryCountToTrackMonetizations < ActiveRecord::Migration[4.2]
  def change
    add_column :track_monetizations, :retry_count, :int, default: 0
  end
end
