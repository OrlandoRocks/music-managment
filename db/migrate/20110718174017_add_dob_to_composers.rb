class AddDobToComposers < ActiveRecord::Migration[4.2]
  def self.up
    add_column :composers, :dob, :date
  end

  def self.down
    remove_column :composers, :dob
  end
end
