class AddReviewableFlagToStores < ActiveRecord::Migration[4.2]
  def up
    add_column :stores, :reviewable, :boolean, default: false
  end

  def down
    remove_column :stores, :reviewable
  end
end
