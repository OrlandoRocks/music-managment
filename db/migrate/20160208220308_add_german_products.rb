class AddGermanProducts < ActiveRecord::Migration[4.2]
  def up
    product_ids = {
      "36"  => 296, # 1 Year Album
      "103" => 304, # 1 Year Ringtone
      "41"  => 299, # 1 Year Single
      "37"  => 297, # 2 Year Album
      "121" => 313, # 2 Year Album Renewal
      "42"  => 300, # 2 Year Single
      "123" => 314, # 2 Year Single Renewal
      "38"  => 298, # 5 Year Album
      "125" => 315, # 5 Year Album Renewal
      "43"  => 301, # 5 Year Single
      "127" => 316, # 5 Year Single Renewal
      "9"   => 287, # Added Store
      "18"  => 288, # Album Distribution Credit
      "20"  => 290, # Album Distribution Credit - 10 Pack
      "21"  => 291, # Album Distribution Credit - 20 Pack
      "19"  => 289, # Album Distribution Credit - 5 Pack
      "109" => 307, # Booklet
      "145" => 323, # Collect Your YouTube Sound Recording Revenue
      "107" => 306, # Credit Usage
      "140" => 321, # Facebook audio recognition
      "139" => 320, # Facebook audio recognition
      "143" => 322, # Facebook audio recognition
      "149" => 325, # LANDR Instant Mastering
      "147" => 324, # Preorder
      "111" => 308, # Ringtone Distribution
      "114" => 311, # Ringtone Distribution Credit - 10 Pack
      "115" => 312, # Ringtone Distribution Credit - 20 Pack
      "112" => 309, # Ringtone Distribution Credit - 3 Pack"9"
      "113" => 310, # Ringtone Distribution Credit - 5 Pack
      "101" => 303, # Ringtone Renewal
      "22"  => 292, # Single Distribution Credit
      "24"  => 294, # Single Distribution Credit - 10 Pack
      "25"  => 295, # Single Distribution Credit - 20 Pack
      "23"  => 293, # Single Distribution Credit - 5 Pack
      "3"   => 286, # Single Renewal
      "65"  => 302, # Songwriter Service
      "105" => 305, # Store Automator
      "134" => 318, # TuneCore Fan Reviews Enhanced
      "135" => 319, # TuneCore Fan Reviews Premium
      "133" => 317, # TuneCore Fan Reviews Starter
      "1"   => 285  # Yearly Album Renewal
    }

    # use ids from above based on name of product
    renewal_product_ids = {
      "288" => 285,
      "289" => 285,
      "290" => 285,
      "291" => 285,
      "292" => 286,
      "293" => 286,
      "294" => 286,
      "295" => 286,
      "296" => 285,
      "297" => 313,
      "298" => 285,
      "299" => 286,
      "300" => 314,
      "301" => 286,
      "304" => 303,
      "308" => 303,
      "309" => 303,
      "310" => 303,
      "311" => 303,
      "312" => 303
    }

    product_ids.each do |us_id, german_id|
      p = Product.find(us_id)
      ger_product = p.dup
      ger_product.id = german_id
      ger_product.country_website_id = 5
      ger_product.currency = "EUR"
      ger_product.created_by = Person.first
      ger_product.save!

      p.product_items.each do |pi|
        ger_product_item = pi.dup
        ger_product_item.update!(
          product_id: ger_product.id,
          currency: "EUR",
          renewal_product_id: renewal_product_ids[ger_product.id.to_s]
        )

        pi.product_item_rules.each do |pir|
          ger_product_item_rule = pir.dup
          ger_product_item_rule.update(
            product_item_id: ger_product_item.id,
            currency: "EUR"
          )
        end
      end
    end

    FacebookRecognitionServiceProduct.create(product_name: "Facebook audio recognition", min_releases: 0, max_releases: 1, product: Product.find(320))
    FacebookRecognitionServiceProduct.create(product_name: "Facebook audio recognition", min_releases: 2, max_releases: 10, product: Product.find(321))
    FacebookRecognitionServiceProduct.create(product_name: "Facebook audio recognition", min_releases: 11, max_releases: 999999999, product: Product.find(322))

    SoundoutProduct.create(report_type: "starter", display_name: "TrackSmarts Starter Report", number_of_reviews: 40, available_in_days: 5, product: Product.find(317))
    SoundoutProduct.create(report_type: "enhanced", display_name: "TrackSmarts Enhanced Report", number_of_reviews: 100, available_in_days: 5, product: Product.find(318))
    SoundoutProduct.create(report_type: "premium", display_name: "TrackSmarts Premium Report", number_of_reviews: 225, available_in_days: 5, product: Product.find(319))

    #clean up some bad GBP products
    ProductItemRule.where(product_item_id: 118).delete_all
    ProductItem.where(product_id: [154,165,169]).delete_all
    Product.where(id: [154,165,169]).delete_all
  end

  def down
    ProductItemRule.where(currency: "EUR").delete_all
    ProductItem.where(currency: "EUR").delete_all
    Product.where(currency: "EUR").delete_all
  end
end
