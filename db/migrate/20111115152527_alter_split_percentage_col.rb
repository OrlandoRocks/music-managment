class AlterSplitPercentageCol < ActiveRecord::Migration[4.2]
  def self.up
    change_column :publishing_splits, :percent, :decimal, :precision => 5, :scale => 2
  end

  def self.down
    change_column :publishing_splits, :percent, :decimal
  end
end
