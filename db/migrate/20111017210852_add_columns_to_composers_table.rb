class AddColumnsToComposersTable < ActiveRecord::Migration[4.2]
  def self.up
    add_column :composers, :salt, :string, :limit => 40
    add_column :composers, :encrypted_tax_id, :string, :limit => 40
  end

  def self.down
    remove_column :composers, :salt
    remove_column :composers, :encrypted_tax_id
  end
end
