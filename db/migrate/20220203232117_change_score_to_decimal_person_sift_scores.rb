class ChangeScoreToDecimalPersonSiftScores < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      change_column :person_sift_scores, :score, :decimal, :default=>0.0, :precision => 17, :scale => 17
    end
  end

  def down
    safety_assured do
      change_column :person_sift_scores, :score, :integer
    end
  end
end
