class CreateCloudSearchBatches < ActiveRecord::Migration[4.2]

  def self.up
    drop_table :cloud_search_batch_date

    create_table :cloud_search_batches do |t|
      t.string   :type
      t.datetime :created_at
      t.integer  :songs_in_cloudsearch
    end
  end

  def self.down
    create_table :cloud_search_batch_date do |t|
      t.datetime :last_add_date
      t.datetime :last_delete_date
    end

    ActiveRecord::Base.connection.execute("insert into cloud_search_batch_date(last_add_date, last_delete_date) values (NULL, NULL)")

    drop_table :cloud_search_batches
  end

end
