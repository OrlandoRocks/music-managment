class AddAutomatorToInventories < ActiveRecord::Migration[4.2]
  def self.up

    # cucumber and test environment are set up from seeds or fixtures
    if !(Rails.env.cucumber? || Rails.env.test?)

      # create new product item rule to create unlimited salepoint type
      # for salepoint add through automator
      product_items = ProductItem.where("name = ?", "Store Automator")
      product_items.each do |pi|
        bio = pi.product_item_rules.first.base_item_option
        base_item_opt = {
          :base_item => bio.base_item,
          :sort_order => 2,
          :parent_id => 0,
          :name => "Price per Salepoint",
          :product_type => "Ad Hoc",
          :option_type => "required",
          :rule_type => "inventory",
          :rule => "price_for_each",
          :quantity => 0,
          :unlimited => 1,
          :true_false => 0,
          :minimum => 0,
          :maximum => 0,
          :price => 0.00,
          :currency => bio.currency,
          :inventory_type => "Salepoint"
        }
        new_bio = BaseItemOption.create!(base_item_opt)

        product_item_rule = {
          :product_item_id => pi.id,
          :base_item_option => new_bio,
          :rule_type => "inventory",
          :rule => "price_for_each",
          :quantity => 0,
          :unlimited => 1,
          :true_false => 0,
          :minimum => 0,
          :maximum => 0,
          :entitlement_rights_group_id => 0,
          :price => 0.00,
          :currency => pi.currency,
          :inventory_type => "Salepoint"
        }
        ProductItemRule.create!(product_item_rule)
      end

      # Create backfill of inventories for all existing automators that have been purchased

      automators = SalepointSubscription.where("finalized_at is not null")
      automators.each do |automator|
        p = automator.purchase
        Inventory.create!(:person => p.person,
                          :product_item_id => p.product.product_items.first.id,
                          :purchase => p,
                          :title => "Store Automator",
                          :quantity => 0,
                          :unlimited => 1,
                          :parent_id => nil,
                          :inventory_type => "Salepoint")
      end
    end
  end

  def self.down
    product_items = ProductItem.where("name = ?", "Store Automator")
    product_items.each do |pi|
      rules = pi.product_item_rules.where("inventory_type = ?", "Salepoint")
      rules.each do |rule|
        rule.base_item_option.destroy
        rule.destroy
      end
    end
  end
end
