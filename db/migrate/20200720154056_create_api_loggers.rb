class CreateApiLoggers < ActiveRecord::Migration[6.0]
  def change
    create_table :api_loggers do |t|
      t.string :endpoint
      t.string :external_service_name
      t.integer :requestable_id
      t.string :requestable_type
      t.string :http_method
      t.string :status_code
      t.text :headers
      t.text :request_body
      t.text :response_body
      t.string :code_location
      t.string :state

      t.timestamps
    end

    add_index :api_loggers, [:requestable_id, :requestable_type]
  end
end
