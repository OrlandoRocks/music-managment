class AddAdyenPaymentMethodInfoToAdyenTransaction < ActiveRecord::Migration[6.0]
  def change
    add_reference :adyen_transactions, :adyen_payment_method_info, foreign_key: true
  end
end
