class ChangeSyncLicenseRequestsToStoreSongId < ActiveRecord::Migration[4.2]
  def self.up
    remove_index :sync_license_requests, :song_code
    rename_column :sync_license_requests, :song_code, :song_id
    add_index :sync_license_requests, :song_id
  end

  def self.down
    remove_index :sync_license_requests, :song_id
    rename_column :sync_license_requests, :song_id, :song_code
    add_index :sync_license_requests, :song_code
  end
end
