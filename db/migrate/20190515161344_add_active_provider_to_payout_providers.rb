class AddActiveProviderToPayoutProviders < ActiveRecord::Migration[4.2]
  def change
    add_column :payout_providers, :active_provider, :boolean
  end
end
