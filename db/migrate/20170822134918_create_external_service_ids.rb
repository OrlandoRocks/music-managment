class CreateExternalServiceIds < ActiveRecord::Migration[4.2]
  def up
    create_table :external_service_ids do |t|
      t.integer :linkable_id, null: false
      t.string  :identifier, null: false
      t.string  :store_name, null: false
      t.string  :linkable_type, null: false
    end

    add_index :external_service_ids, [:linkable_id, :linkable_type]
    add_index :external_service_ids, [:linkable_id, :store_name], unique: true
  end

  def down
    drop_table :external_service_ids
  end
end
