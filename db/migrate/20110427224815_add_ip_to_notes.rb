class AddIpToNotes < ActiveRecord::Migration[4.2]
  def self.up
    add_column :notes, :ip_address, :string
  end

  def self.down
    remove_column :notes, :ip_address
  end
end
