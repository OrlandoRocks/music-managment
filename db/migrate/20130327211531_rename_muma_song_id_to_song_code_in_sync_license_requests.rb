class RenameMumaSongIdToSongCodeInSyncLicenseRequests < ActiveRecord::Migration[4.2]
  def self.up
    rename_column :sync_license_requests, :muma_song_id, :song_code
  end

  def self.down
    rename_column :sync_license_requests, :song_code, :muma_song_id
  end
end
