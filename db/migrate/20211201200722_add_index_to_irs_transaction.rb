class AddIndexToIRSTransaction < ActiveRecord::Migration[6.0]
  def change
    add_index :irs_transactions, :transaction_code, unique: true
    add_index :irs_transactions, :status
  end
end
