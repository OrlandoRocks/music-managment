class CreateOutboundRefunds < ActiveRecord::Migration[6.0]
  def change
    create_table :outbound_refunds do |t|
      t.string :user_invoice_prefix
      t.string :invoice_sequence
      t.references :refund, index: true
      t.references :outbound_invoice, index: true
      t.references :vat_tax_adjustment, index: true
      t.references :refund_settlement, index: true
      t.string :currency

      t.timestamps
    end
  end
end
