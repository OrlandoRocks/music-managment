class AddComposerToInventoriesInventoryType < ActiveRecord::Migration[4.2]
  def up
    change_column :inventories, :inventory_type, <<-SQL.strip_heredoc
      ENUM(
        'Album',
        'Single',
        'Ringtone',
        'MusicVideo',
        'FeatureFilm',
        'ItunesUserReport',
        'Booklet',
        'Song',
        'Salepoint',
        'Widget',
        'Video',
        'Composer'
      )
    SQL
  end

  def down
    change_column :inventories, :inventory_type, <<-SQL.strip_heredoc
      ENUM(
        'Album',
        'Single',
        'Ringtone',
        'MusicVideo',
        'FeatureFilm',
        'ItunesUserReport',
        'Booklet',
        'Song',
        'Salepoint',
        'Widget',
        'Video'
      )
    SQL
  end
end
