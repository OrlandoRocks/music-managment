class AddSpatialAudioToProductAppliesToProduct < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      execute <<-SQL
        ALTER TABLE products
          MODIFY COLUMN applies_to_product enum('Album','Single','Ringtone','Video','None','Widget','CreditUsage','Booklet','SalepointSubscription', 'SalepointPreorderData','SpatialAudio') NOT NULL DEFAULT 'None'
      SQL
    end
  end

  def down
    safety_assured do
      execute <<-SQL
        ALTER TABLE products
          MODIFY COLUMN applies_to_product enum('Album','Single','Ringtone','Video','None','Widget','CreditUsage','Booklet','SalepointSubscription', 'SalepointPreorderData') NOT NULL DEFAULT 'None'
      SQL
    end
  end
end
