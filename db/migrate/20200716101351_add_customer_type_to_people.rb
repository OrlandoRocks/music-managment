class AddCustomerTypeToPeople < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      execute <<-SQL
          ALTER TABLE people ADD customer_type enum('individual', 'business')  DEFAULT 'individual';
      SQL
    end
  end

  def down
    remove_column :people, :customer_type
  end
end
