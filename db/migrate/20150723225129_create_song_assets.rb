class CreateSongAssets < ActiveRecord::Migration[4.2]
  def up
    create_table  :song_assets do |t|
      t.integer   :song_id
      t.string    :bucket_name
      t.string    :original_filename
      t.string    :asset_key
      t.string    :transcoder_job_id
      t.string    :processing_status
      t.text      :error_messages
      t.integer   :duration
      t.text      :song_metadata
      t.timestamps
    end

    execute "ALTER TABLE song_assets ADD CONSTRAINT fk_song_id FOREIGN KEY (song_id) references songs(id)"
  end

  def down
    drop_table :song_assets
  end
end
