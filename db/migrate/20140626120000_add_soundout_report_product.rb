class AddSoundoutReportProduct < ActiveRecord::Migration[4.2]
  def up
    execute("ALTER TABLE purchases CHANGE related_type related_type enum('Album','Single','Ringtone','Entitlement','Product','Renewal','ItunesUserReport','MusicVideo','FeatureFilm','Salepoint','Song','Widget','Composer','Video','SalepointSubscription','CreditUsage','Booklet', 'SalepointPreorderData', 'SoundoutReport');")

    create_table  :soundout_products do |t|
      t.string    :report_type
      t.string    :display_name
      t.integer   :number_of_reviews
      t.integer   :available_in_days
      t.integer   :product_id
      t.timestamps
    end

    create_table  :soundout_reports do |t|
      t.integer   :soundout_product_id
      t.integer   :person_id, :null=>false
      t.integer   :song_id,   :null=>false
      t.string    :status
      t.string    :soundout_id
      t.datetime  :report_requested_tmsp
      t.integer   :retrieval_attempts
      t.datetime  :report_received_tmsp
      t.text      :report_data
      t.timestamps
    end
    add_index :soundout_reports, [:person_id, :song_id]
    add_index :soundout_reports, :soundout_id

    # Product for starter report
    product_1 = Product.create({
      created_by_id:      1,
      country_website_id: 1,
      name:               "TrackSmarts Starter",
      display_name:       "TrackSmarts Starter",
      description:        "Provides insites into the market value of your song based on 40 independent reviews. Reviews and ratings are analysed by semantic technologies and compared against over 50,000 other tracks that have already been processed through SoundOut.",
      status:             "Active",
      product_type:       "Ad Hoc",
      is_default:         1,
      applies_to_product: "None",
      sort_order:         1,
      renewal_level:      "None",
      price:              15,
      currency:           "USD"
    })

    # Product data for enhanced report
    product_2 = Product.create({
      created_by_id:      1,
      country_website_id: 1,
      name:               "TrackSmarts Enhanced",
      display_name:       "TrackSmarts Enhanced",
      description:        "Provides insites into the market value of your song based on 100 independent reviews. Reviews and ratings are analysed by semantic technologies and compared against over 50,000 other tracks that have already been processed through SoundOut.",
      status:             "Active",
      product_type:       "Ad Hoc",
      is_default:         1,
      applies_to_product: "None",
      sort_order:         1,
      renewal_level:      "None",
      price:              40,
      currency:           "USD"
    })

    # Product data for enhanced report
    product_3 = Product.create({
      created_by_id:      1,
      country_website_id: 1,
      name:               "TrackSmarts Premium",
      display_name:       "TrackSmarts Premium",
      description:        "Provides insites into the market value of your song based on 255 independent reviews. Reviews and ratings are analysed by semantic technologies and compared against over 50,000 other tracks that have already been processed through SoundOut.",
      status:             "Active",
      product_type:       "Ad Hoc",
      is_default:         1,
      applies_to_product: "None",
      sort_order:         1,
      renewal_level:      "None",
      price:              115,
      currency:           "USD"
    })

    # Ca product 1
    ca_product_1 = Product.create({
      created_by_id:      1,
      country_website_id: 2,
      name:               "TrackSmarts Starter",
      display_name:       "TrackSmarts Starter",
      description:        "Provides insites into the market value of your song based on 40 independent reviews. Reviews and ratings are analysed by semantic technologies and compared against over 50,000 other tracks that have already been processed through SoundOut.",
      status:             "Active",
      product_type:       "Ad Hoc",
      is_default:         1,
      applies_to_product: "None",
      sort_order:         1,
      renewal_level:      "None",
      price:              15,
      currency:           "CAD"
    })

    # CA Product data for enhanced report
    ca_product_2 = Product.create({
      created_by_id:      1,
      country_website_id: 2,
      name:               "TrackSmarts Enhanced",
      display_name:       "TrackSmarts Enhanced",
      description:        "Provides insites into the market value of your song based on 100 independent reviews. Reviews and ratings are analysed by semantic technologies and compared against over 50,000 other tracks that have already been processed through SoundOut.",
      status:             "Active",
      product_type:       "Ad Hoc",
      is_default:         1,
      applies_to_product: "None",
      sort_order:         1,
      renewal_level:      "None",
      price:              40,
      currency:           "CAD"
    })

    # CA Product data for enhanced report
    ca_product_3 = Product.create({
      created_by_id:      1,
      country_website_id: 2,
      name:               "TrackSmarts Premium",
      display_name:       "TrackSmarts Premium",
      description:        "Provides insites into the market value of your song based on 255 independent reviews. Reviews and ratings are analysed by semantic technologies and compared against over 50,000 other tracks that have already been processed through SoundOut.",
      status:             "Active",
      product_type:       "Ad Hoc",
      is_default:         1,
      applies_to_product: "None",
      sort_order:         1,
      renewal_level:      "None",
      price:              115,
      currency:           "CAD"
    })

     SoundoutProduct.create({
      report_type: 'starter',
      available_in_days:  5,
      product_id:         product_1.id,
      display_name:       "TrackSmarts Starter Report",
      number_of_reviews:  40
    })

     SoundoutProduct.create({
      report_type: 'enhanced',
      available_in_days:  5,
      product_id:         product_2.id,
      display_name:       "TrackSmarts Enhanced Report",
      number_of_reviews:  100
    })

    SoundoutProduct.create({
      report_type: 'premium',
      available_in_days:  5,
      product_id:         product_3.id,
      display_name:       "TrackSmarts Premium Report",
      number_of_reviews:  225
    })

     SoundoutProduct.create({
      report_type: 'starter',
      available_in_days:  5,
      product_id:         ca_product_1.id,
      display_name:       "TrackSmarts Starter Report",
      number_of_reviews:  40
    })

     SoundoutProduct.create({
      report_type: 'enhanced',
      available_in_days:  5,
      product_id:         ca_product_2.id,
      display_name:       "TrackSmarts Enhanced Report",
      number_of_reviews:  100
    })

    SoundoutProduct.create({
      report_type: 'premium',
      available_in_days:  5,
      product_id:         ca_product_3.id,
      display_name:       "TrackSmarts Premium Report",
      number_of_reviews:  255
    })
  end

  def down
    execute("ALTER TABLE purchases CHANGE related_type related_type enum('Album','Single','Ringtone','Entitlement','Product','Renewal','ItunesUserReport','MusicVideo','FeatureFilm','Salepoint','Song','Widget','Composer','Video','SalepointSubscription','CreditUsage','Booklet', 'SalepointPreorderData');")

    drop_table :soundout_reports
    drop_table :soundout_products

    execute("DELETE FROM product_item_rules WHERE inventory_type = 'SoundoutReport';")
    execute("DELETE FROM product_items WHERE name like '%SoundOut%';")
    execute("DELETE FROM products WHERE name like '%SoundOut%';")
  end
end
