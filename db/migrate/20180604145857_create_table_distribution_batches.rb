class CreateTableDistributionBatches < ActiveRecord::Migration[4.2]
  def change
    create_table :distribution_batches do |t|
      t.belongs_to :store
      t.string :batch_id
      t.integer :count, default: 0
      t.boolean :active
    end
    add_index :distribution_batches, :store_id
  end
end
