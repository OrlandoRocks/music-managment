class ChangePersonSiftScoreToPersonSiftScores < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      rename_table :person_sift_score, :person_sift_scores
    end
  end
end
