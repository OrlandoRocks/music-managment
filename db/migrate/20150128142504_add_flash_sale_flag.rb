class AddFlashSaleFlag < ActiveRecord::Migration[4.2]
  def self.up
    add_column :stores, :on_flash_sale, :boolean, :default=>false
  end

  def self.down
    remove_column :stores, :on_flash_sale
  end
end
