class CreateTaxTokens < ActiveRecord::Migration[4.2]
  def change
    create_table :tax_tokens do |t|
      t.string      :token, null: false
      t.boolean     :is_visible, default: false
      t.references  :person
      t.timestamps
    end
  end
end
