class AddForeignExchangeRateToVatTaxAdjustments < ActiveRecord::Migration[6.0]
  def change
    add_reference :vat_tax_adjustments, :foreign_exchange_rate, foreign_key: true
  end
end
