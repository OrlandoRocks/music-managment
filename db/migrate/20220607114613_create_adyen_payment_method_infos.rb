class CreateAdyenPaymentMethodInfos < ActiveRecord::Migration[6.0]
  def change
    create_table :adyen_payment_method_infos do |t|
      t.string :payment_method_name
      t.timestamps
    end
  end
end
