class AddCountryWebsiteValidationToBalanceRates < ActiveRecord::Migration[6.0]
  def change
    safety_assured do  change_column :foreign_exchange_balance_rates, :country_website_id, :integer, default: nil,
                                     null: false
    end
  end
end
