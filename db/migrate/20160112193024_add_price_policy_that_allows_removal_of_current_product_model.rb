class AddPricePolicyThatAllowsRemovalOfCurrentProductModel < ActiveRecord::Migration[4.2]
  def up
    PricePolicy.create(short_code: "tunecore_default", price_calculator: "product_base", base_price_cents: 0, currency: "USD", description: "The only price policy in use as of January 2016 refactor.")
  end

  def down
  end
end
