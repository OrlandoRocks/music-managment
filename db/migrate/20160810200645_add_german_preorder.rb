class AddGermanPreorder < ActiveRecord::Migration[4.2]

  VARIABLE_PRICE_EURO_PRICE_MAP = {
    30 =>	1.62, # EP *
    31 => 0.71, # Single Tier 2 *
    32 =>	4.35, # Mid *
    33 => 0.90, # Single Tier 1 *
    34 => 5.45, # Front One *
    35 => 2.70, # Budget One
    37 => 3.80, # Back
    38 =>	1.08, # Mini EP
    39 => 4.90, # Mid/Front *
    40 =>	0.81, # Digital 45 - $1.99
    41 => 2.18, # Mini Album
    42 => 6.50, # Front Plus
    43 => 0.50, # Single Tier 2
    44 => 3.25, # Budget Two
    45 => 7.05, # Deluxe One
    46 => 7.60, # Deluxe Two
    47 => 8.15, # Deluxe Three
    48 =>	8.70, # Deluxe Four
  }

  def up
    country_website = CountryWebsite.find_by(country: "DE")
    VARIABLE_PRICE_EURO_PRICE_MAP.each do |variable_price_id, price|
      attributes = {variable_price_id: variable_price_id, country_website: country_website, currency:country_website.currency, wholesale_price:price}
      country_variable_price = CountryVariablePrice.create(attributes)
      if country_variable_price.present?
        p "SUCCESS: creating country variable price for #{variable_price_id}"
      else
        p "FAIL: creating country variable price for #{variable_price_id}"
      end
    end
  end

  def down
    cvps = CountryVariablePrice.where(currency: "EUR")
    cvps.each { |cvp| cvp.delete }
  end
end
