class UpdateAmazonStoreName < ActiveRecord::Migration[4.2]
  def up
    store = Store.find_by(short_name: "Amazon")
    store.update_attribute(:name, "Amazon Music")
  end

  def down
    store = Store.find_by(short_name: "Amazon")
    store.update_attribute(:name, "Amazon MP3")
  end
end
