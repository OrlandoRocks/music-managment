class AddPaymentsosTokenToStoredCreditCards < ActiveRecord::Migration[4.2]
  def change
    add_column :stored_credit_cards, :payments_os_token, :string
    add_column :stored_credit_cards, :phone_number, :string
  end
end
