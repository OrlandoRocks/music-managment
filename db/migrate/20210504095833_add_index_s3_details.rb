class AddIndexS3Details < ActiveRecord::Migration[6.0]
  def up
    add_index :s3_details, :s3_asset_id
  end

  def down
    remove_index :s3_details, :s3_asset_id
  end
end
