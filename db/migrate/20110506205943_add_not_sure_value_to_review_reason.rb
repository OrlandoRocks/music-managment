class AddNotSureValueToReviewReason < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE review_reasons
       CHANGE reason_type reason_type enum('FLAGGED', 'REJECTED', 'NOT SURE') NULL DEFAULT NULL COMMENT 'Indicates the type of reason'
      ")
  end

  def self.down
    execute("ALTER TABLE review_reasons
       CHANGE COLUMN reason_type reason_type enum('FLAGGED', 'REJECTED') NULL DEFAULT NULL COMMENT 'Indicates the type of reason'
      ")
  end
end
