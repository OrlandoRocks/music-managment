class AddStateToExternalServiceIds < ActiveRecord::Migration[4.2]
  def change
    add_column :external_service_ids, :state, :string
  end
end
