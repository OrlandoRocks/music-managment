class AddIndexToAlbumCountries < ActiveRecord::Migration[4.2]
  def change
    add_index :album_countries, :album_id
  end
end
