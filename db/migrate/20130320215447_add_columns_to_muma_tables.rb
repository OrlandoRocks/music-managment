class AddColumnsToMumaTables < ActiveRecord::Migration[4.2]
  def self.up
    add_column :muma_songs, :company_code, :string, :limit => 4
    add_column :muma_song_ips, :territory_code, :string, :limit => 4
  end

  def self.down
    remove_column :muma_songs, :company_code
    remove_column :muma_song_ips, :territory_code
  end
end
