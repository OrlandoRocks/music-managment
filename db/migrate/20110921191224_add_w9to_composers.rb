class AddW9toComposers < ActiveRecord::Migration[4.2]
  def self.up
    add_column :composers, :nosocial, :boolean, :default => false
    add_column :composers, :agreed_to_w9_at, :datetime, :default => nil
  end

  def self.down
    remove_column :composers, :nosocial
    remove_column :composers, :agreed_to_w9_at
  end
end
