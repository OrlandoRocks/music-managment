class CreateOmnifoneStore < ActiveRecord::Migration[4.2]
  def self.up
    store = Store.create(:name=>'Omnifone', :abbrev=>'om', :short_name=>'Omnifone', :position=>400, :needs_rights_assignment=>false, :is_active=>false, :base_price_policy_id=>"3")

    if store
      execute ("insert into salepointable_stores (salepointable_type,store_id) values ('Album','#{store.id}')")
      execute ("insert into salepointable_stores (salepointable_type,store_id) values ('Single','#{store.id}')")
    else
      puts "Store creation for Omnifone failed"
    end
  end

  def self.down
    if store = Store.find_by(short_name: "Omnifone")
      execute("delete from salepointable_stores where store_id = #{store.id}")
      execute("DELETE FROM stores where short_name = 'Omnifone'")
    else
      puts "Couldn't find store by the short name of Omnifone"
    end
  end

end
