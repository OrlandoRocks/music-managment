class AddCountryWebsiteIdToPeggedRates < ActiveRecord::Migration[6.0]
  def change
    safety_assured { add_column :foreign_exchange_pegged_rates, :country_website_id, :integer, default: 8 }
  end
end
