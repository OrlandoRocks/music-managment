class AddPersonMasterIndexToSalesRecord < ActiveRecord::Migration[4.2]
  def up
    sql = %Q{
      CREATE TABLE `sales_records_new_index` (
        `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `person_id` int(10) unsigned NOT NULL COMMENT 'customer identifier',
        `person_intake_id` int(10) unsigned DEFAULT NULL COMMENT 'related person_intake',
        `sip_sales_record_id` int(10) unsigned DEFAULT NULL COMMENT 'pointer back to sip',
        `sales_record_master_id` int(10) unsigned DEFAULT NULL,
        `related_id` int(10) NOT NULL COMMENT 'record identifier',
        `related_type` enum('Album','Song','Video') DEFAULT NULL COMMENT 'record type identifier',
        `distribution_type` enum('Download','Streaming') DEFAULT NULL COMMENT 'type of distribution - download or streaming',
        `quantity` int(10) NOT NULL COMMENT 'total sales for this record',
        `revenue_per_unit` decimal(23,14) NOT NULL COMMENT 'revenue per unit',
        `revenue_total` decimal(23,14) NOT NULL COMMENT 'total revenue',
        `amount` decimal(23,14) DEFAULT NULL COMMENT 'USD Amount Entered',
        PRIMARY KEY (`id`),
        KEY `s_ix_sales_record_master` (`sales_record_master_id`),
        KEY `s_ix_sip_sales_record_id` (`sip_sales_record_id`),
        KEY `s_ix_sales_records_person_intakes` (`person_intake_id`),
        KEY `s_ix_sales_records_related` (`related_id`,`related_type`),
        KEY `s_ix_sales_records_person_master` ( `person_id`, `sales_record_master_id` ),
        CONSTRAINT FOREIGN KEY (`person_intake_id`) REFERENCES `person_intakes` (`id`),
        CONSTRAINT FOREIGN KEY (`person_id`) REFERENCES `people` (`id`),
        CONSTRAINT FOREIGN KEY (`sales_record_master_id`) REFERENCES `sales_record_masters` (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    }
    ActiveRecord::Base.connection.execute(sql)

    sql = %Q{
      CREATE PROCEDURE `populate_sales_records_new_index`()
      BEGIN

      DECLARE done BOOLEAN DEFAULT 0;
      DECLARE var_srm_id int;

      DECLARE cur1 cursor for 
      SELECT srm.id from sales_record_masters srm where srm.id not in ( select distinct sales_record_master_id from sales_records_new_index );

      DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

      OPEN cur1;
      fetch cur1 into var_srm_id;
      WHILE( !done ) DO

        select NOW();	
        select var_srm_id;
 
        START TRANSACTION;
          insert into sales_records_new_index
          select * from sales_records where sales_record_master_id = var_srm_id;
        COMMIT;

        fetch cur1 into var_srm_id;

      END WHILE;
      CLOSE cur1;

      END;
    }
    ActiveRecord::Base.connection.execute(sql)

  end

  def down
    ActiveRecord::Base.connection.execute("drop table if exists sales_records_new_index")
    ActiveRecord::Base.connection.execute("drop procedure if exists populate_sales_records_new_index")
  end
end
