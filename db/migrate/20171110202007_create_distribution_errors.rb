class CreateDistributionErrors < ActiveRecord::Migration[4.2]
  create_table :distribution_errors do |t|
    t.text :message

    t.timestamps
  end
end
