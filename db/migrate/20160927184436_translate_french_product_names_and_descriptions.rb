#encoding: utf-8
#

class TranslateFrenchProductNamesAndDescriptions < ActiveRecord::Migration[4.2]

  ENGLISH_AND_FRENCH_PRODUCT_INFO = [
    {
      en: {name: "1 Year Album", desc: "Your album will be live for one year."},
      fr: {name: "Album - 1 an", desc: "Votre album restera en ligne pendant un an"}
    },
    {
      en: {name: "1 Year Ringtone", desc: "Your ringtone will be live for one year."},
      fr: {name: "Sonnerie Mobile - 1 an", desc: "Votre sonnerie mobile restera en ligne pendant un an"}
    },
    {
      en: {name: "1 Year Single", desc: "Your single will be live for one year."},
      fr: {name: "Single - 1 an", desc: "Votre single restera en ligne pendant un an"}
    },
    {
      en: {name: "2 Year Album", desc: "Your album will be live for two years."},
      fr: {name: "Album - 2 ans", desc: "Votre album restera en ligne pendant deux ans"}
    },
    {
      en: {name: "2 Year Album Renewal", desc: "2 year renewal fee for your album."},
      fr: {name: "Album - Renouvellement 2 ans", desc: "Tarif pour 2 ans de renouvellement pour votre album"}
    },
    {
      en: {name: "2 Year Single", desc: "Your single will be live for two years."},
      fr: {name: "Single - 2 ans", desc: "Votre single restera en ligne pendant deux ans"}
    },
    {
      en: {name: "2 Year Single Renewal", desc: "2 year renewal fee for your single."},
      fr: {name: "Single - Renouvellement 2 ans", desc: "Tarif pour 2 ans de renouvellement pour votre single"}
    },
    {
      en: {name: "5 Year Album", desc: "Your album will be live for five years."},
      fr: {name: "Album - 5 ans", desc: "Votre album restera en ligne pendant cinq ans"}
    },
    {
      en: {name: "5 Year Album Renewal", desc: "5 year renewal fee for your album."},
      fr: {name: "Album - Renouvellement 5 ans", desc: "Tarif pour 5 ans de renouvellement pour votre album"}
    },
    {
      en: {name: "5 Year Single", desc: "Your single will be live for five years."},
      fr: {name: "Single - 5 ans", desc: "Votre single restera en ligne pendant cinq ans"}
    },
    {
      en: {name: "5 Year Single Renewal", desc: "5 year renewal fee for your album."},
      fr: {name: "Single - Renouvellement 5 ans", desc: "Tarif pour 5 ans de renouvellement pour votre single"}
    },
    {
      en: {name: "Added Store", desc: "Fee for adding a store to a previously distributed release."},
      fr: {name: "Plateforme supplémentaire", desc: "Tarif pour ajouter une plateforme à une sortie distribuée antérieurement"}
    },
    {
      en: {name: "Album Distribution Credit", desc: "Credit to distribute 1 Album whenever your music is ready. Cannot be used for renewals."},
      fr: {name: "Album - Crédit de Distribution", desc: "Crédit pour distribuer 1 Album dès que votre musique est prête. Ne peux pas être utilsé pour les renouvellements."}
    },
    {
      en: {name: "Album Distribution Credit - 10 Pack", desc: "Credit to distribute 10 Albums whenever your music is ready. Cannot be used for renewals."},
      fr: {name: "Album - lot de 10 Crédits de Distribution", desc: "Crédit pour distribuer 10 Albums dès que votre musique est prête. Ne peux pas être utilsé pour les renouvellements."}
    },
    {
      en: {name: "Album Distribution Credit - 20 Pack", desc: "Credit to distribute 20 Albums whenever your music is ready. Cannot be used for renewals."},
      fr: {name: "Album - lot de 20 Crédits de Distribution", desc: "Crédit pour distribuer 20 Albums dès que votre musique est prête. Ne peux pas être utilsé pour les renouvellements."}
    },
    {
      en: {name: "Album Distribution Credit - 5 Pack", desc: "Credit to distribute 5 Albums whenever your music is ready. Cannot be used for renewals."},
      fr: {name: "Album - lot de 5 Crédits de Distribution", desc: "Crédit pour distribuer 5 Albums dès que votre musique est prête. Ne peux pas être utilsé pour les renouvellements."}
    },
    {
      en: {name: "Booklet", desc: "Distribute Booklet"},
      fr: {name: "Livret", desc: "Distribuez votre livret"}
    },
    {
      en: {name: "Collect Your YouTube Sound Recording Revenue", desc: "TuneCore will submit your tracks to YouTube, collect the revenue your sound recordings earn, and deposit the money into your TuneCore account."},
      fr: {name: "Collectez les revenus générés par l'option Monétisation Youtube", desc: "TuneCore enverra vos titres à YouTune, collectera les royalties générées par vos enregistrements et créditera ces revenus sur votre compte TuneCore"}
    },
    {
      en: {name: "Credit Usage", desc: "Distribute With Credit Cannot be used for renewals."},
      fr: {name: "Utilisation de Crédit", desc: "Les Crédits ne peuvent pas être utilsés pour les renouvellements."}
    },
    {
      en: {name: "LANDR Instant Mastering", desc: "Instant mastering of one track. Note => You must complete the purchase process in six hours or your order will disappear."},
      fr: {name: "Mastering instantané LANDR", desc: "Mastering instantané d'un titre. A noter => vous devez compléter votre achat dans les six heures ou votre commande ne sera pas prise en compte."}
    },
    {
      en: {name: "Preorder", desc: "Make your release available for sale before the street date."},
      fr: {name: "Pré-commande", desc: "Rendez votre sortie disponible à la vente avant la date de sortie officielle"}
    },
    {
      en: {name: "Ringtone Distribution Credit", desc: "Your ringtone in the iPhone store. Distribute whenever you want. Cannot be used for renewals."},
      fr: {name: "Sonnerie mobile - Crédit de Distribution", desc: "Votre sonnerie mobile disponible via l'Apple store. Distribuez la quand vous le voulez. Ne peux pas être utilsé pour les renouvellements."}
    },
    {
      en: {name: "Ringtone Distribution Credit - 10 Pack", desc: "Your ringtone in the iPhone store. Distribute whenever you want. Cannot be used for renewals."},
      fr: {name: "Sonnerie mobile - Lot de 10 Crédits de Distribution", desc: "Votre sonnerie mobile disponible via l'Apple store. Distribuez la quand vous le voulez. Ne peux pas être utilsé pour les renouvellements."}
    },
    {
      en: {name: "Ringtone Distribution Credit - 20 Pack", desc: "Your ringtone in the iPhone store. Distribute whenever you want. Cannot be used for renewals."},
      fr: {name: "Sonnerie mobile - Lot de 20 Crédits de Distribution", desc: "Votre sonnerie mobile disponible via l'Apple store. Distribuez la quand vous le voulez. Ne peux pas être utilsé pour les renouvellements."}
    },
    {
      en: {name: "Ringtone Distribution Credit - 3 Pack", desc: "Your ringtone in the iPhone store. Distribute whenever you want. Cannot be used for renewals."},
      fr: {name: "Sonnerie mobile - Lot de 3 Crédits de Distribution", desc: "Votre sonnerie mobile disponible via l'Apple store. Distribuez la quand vous le voulez. Ne peux pas être utilsé pour les renouvellements."}
    },
    {
      en: {name: "Ringtone Distribution Credit - 5 Pack", desc: "Your ringtone in the iPhone store. Distribute whenever you want. Cannot be used for renewals."},
      fr: {name: "Sonnerie mobile - Lot de 5 Crédits de Distribution", desc: "Votre sonnerie mobile disponible via l'Apple store. Distribuez la quand vous le voulez. Ne peux pas être utilsé pour les renouvellements."}
    },
    {
      en: {name: "Ringtone Renewal", desc: "Annual renewal fee for your ringtone."},
      fr: {name: "Sonnerie mobile - Renouvellement", desc: "Tarif pour un renouvellement annuel de votre sonnerie mobile."}
    },
    {
      en: {name: "Single Distribution Credit", desc: "Credit to distribute 1 Single whenever your music is ready. Cannot be used for renewals."},
      fr: {name: "Single - Crédit de Distribution", desc: "Crédit pour distribuer 1 Single dès que votre musique est prête.  Ne peux pas être utilsé pour les renouvellements."}
    },
    {
      en: {name: "Single Distribution Credit - 10 Pack", desc: "Credit to distribute 10 Singles whenever your music is ready. Cannot be used for renewals."},
      fr: {name: "Single - Lot de 10 Crédits de Distribution", desc: "Crédit pour distribuer 10 Singles dès que votre musique est prête.  Ne peux pas être utilsé pour les renouvellements."}
    },
    {
      en: {name: "Single Distribution Credit - 20 Pack", desc: "Credit to distribute 20 Singles whenever your music is ready. Cannot be used for renewals."},
      fr: {name: "Single - Lot de 20 Crédits de Distribution", desc: "Crédit pour distribuer 20 Singles dès que votre musique est prête.  Ne peux pas être utilsé pour les renouvellements."}
    },
    {
      en: {name: "Single Distribution Credit - 5 Pack", desc: "Credit to distribute 5 Singles whenever your music is ready. Cannot be used for renewals."},
      fr: {name: "Single - Lot de 5 Crédits de Distribution", desc: "Crédit pour distribuer 5 Singles dès que votre musique est prête.  Ne peux pas être utilsé pour les renouvellements."}
    },
    {
      en: {name: "Single Renewal", desc: "Annual renewal fee for your single."},
      fr: {name: "Single - Renouvellement", desc: "Tarif pour un renouvellement annuel de votre single."}
    },
    {
      en: {name: "Songwriter Service", desc: "Collecting, licensing and policing your songwriter copyrights"},
      fr: {name: "Service Auteur-compositeur", desc: "Collecte, licencie et surveille vos droits d'auteur"}
    },
    {
      en: {name: "Store Automator", desc: "Release automatically distributed to future stores"},
      fr: {name: "Option Store Automator", desc: "Distribue automatiquement votre catalogue sur les plateformes ajoutées dans le futur"}
    },
    {
      en: {name: "TuneCore Fan Reviews Enhanced", desc: "Get music fan reviews and helpful analytics for your song."},
      fr: {name: "TuneCore Fan Reviews Enhanced", desc: "Obtenez des évaluations de fans et des analyses structurées sur vos titres."}
    },
    {
      en: {name: "TuneCore Fan Reviews Premium", desc: "TuneCore Fan Reviews Premium"},
      fr: {name: "Get music fan reviews and helpful analytics for your song.", desc: "Obtenez des évaluations de fans et des analyses structurées sur vos titres."}
    },
    {
      en: {name: "TuneCore Fan Reviews Starter", desc: "Get music fan reviews and helpful analytics for your song."},
      fr: {name: "TuneCore Fan Reviews Starter", desc: "Obtenez des évaluations de fans et des analyses structurées sur vos titres."}
    },
    {
      en: {name: "Yearly Album Renewal", desc: "Annual renewal fee for your album."},
      fr: {name: "Album - Renouvellement", desc: "Tarif pour un renouvellement annuel de votre album."}
    }
  ]

  FRENCH_PRODUCTS = Product.where(country_website_id: CountryWebsite.where(country: "FR"))

  def up
    ENGLISH_AND_FRENCH_PRODUCT_INFO.each do |product_info|
      product = FRENCH_PRODUCTS.find_by(name: product_info[:en][:name])
      product.update(name: product_info[:fr][:name], description: product_info[:fr][:desc])
    end
  end

  def down
    ENGLISH_AND_FRENCH_PRODUCT_NAMES.each do |product_info|
      product = FRENCH_PRODUCTS.find_by(name: product_info[:fr][:name])
      product.update(name: product_info[:en][:name], description: product_info[:en][:desc])
    end
  end
end
