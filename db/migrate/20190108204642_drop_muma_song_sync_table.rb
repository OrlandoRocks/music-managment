class DropMumaSongSyncTable < ActiveRecord::Migration[4.2]
  def up
    drop_table :muma_song_sync
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
