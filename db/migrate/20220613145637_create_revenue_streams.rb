class CreateRevenueStreams < ActiveRecord::Migration[6.0]
  def change
    create_table :revenue_streams do |t|
      t.string :code, null: false, uniq: true
      t.string :code_info

      t.timestamps
    end
  end
end
