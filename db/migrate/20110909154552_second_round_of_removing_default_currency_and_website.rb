class SecondRoundOfRemovingDefaultCurrencyAndWebsite < ActiveRecord::Migration[4.2]
  def self.up
    # new default removals
    execute(" ALTER TABLE payment_batches ALTER COLUMN currency DROP DEFAULT;")# new
    execute(" ALTER TABLE payment_batches MODIFY COLUMN country_website_id INT(11) unsigned NOT NULL;")# new, needs not null
    execute(" ALTER TABLE eft_batches MODIFY COLUMN country_website_id INT(11) unsigned NOT NULL;")# new, needs not null
    execute(" ALTER TABLE eft_batches ALTER COLUMN currency DROP DEFAULT;")# new
    execute(" ALTER TABLE advertisements ALTER COLUMN country_website_id DROP DEFAULT;")# new
    
    #adjustments to previous removals
    execute("ALTER TABLE people MODIFY COLUMN country_website_id INT(11) unsigned NOT NULL;") #need to add a not null
    execute(" ALTER TABLE payout_service_fees ALTER COLUMN country_website_id DROP DEFAULT;")# needs not null
    execute(" ALTER TABLE products MODIFY COLUMN country_website_id INT(11) unsigned NOT NULL;")#need to add a not null
    execute(" ALTER TABLE paypal_transfers MODIFY COLUMN currency CHAR(3) NOT NULL;") # needs not null
    #
    execute(" ALTER TABLE braintree_transactions MODIFY COLUMN country_website_id INT(11) unsigned NOT NULL;") # needs not null
  end

  def self.down
    # new default removals
    execute(" ALTER TABLE payment_batches ALTER COLUMN currency SET DEFAULT 'USD';")# new
    execute(" ALTER TABLE payment_batches ALTER COLUMN country_website_id SET DEFAULT '1';")# new
    execute(" ALTER TABLE eft_batches ALTER COLUMN country_website_id SET DEFAULT '1';")# new
    execute(" ALTER TABLE eft_batches ALTER COLUMN currency SET DEFAULT 'USD';")# new
    execute(" ALTER TABLE advertisements ALTER COLUMN country_website_id SET DEFAULT '1';")# new
    
    #adjustments to previous removals
    execute("ALTER TABLE people ALTER COLUMN country_website_id SET DEFAULT '1';")
    execute("ALTER TABLE payout_service_fees ALTER COLUMN country_website_id SET DEFAULT '1';")
    execute("ALTER TABLE products ALTER COLUMN country_website_id SET DEFAULT '1';")
    execute("ALTER TABLE paypal_transfers ALTER COLUMN currency SET DEFAULT 'USD';")
    #
    execute("ALTER TABLE braintree_transactions ALTER COLUMN country_website_id SET DEFAULT '1';")
  end
end
