class RemoveUnusedColumnsFromTargetedOffers < ActiveRecord::Migration[4.2]
  def up
    remove_column :targeted_offers, :token_type
    remove_column :targeted_offers, :session_token
  end

  def down
    add_column :targeted_offers, :token_type, :string, :limit=> 15, :default => "join_token"
    add_column :targeted_offers, :session_token, :string
  end
end
