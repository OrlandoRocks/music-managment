class ChangeInvoiceSequenceTypeToInteger < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      change_column :refunds, :invoice_sequence, :integer
    end
  end

  def down
    safety_assured do
      change_column :refunds, :invoice_sequence, :string
    end
  end
end
