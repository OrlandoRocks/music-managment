class AddUniqueConstraintToCategoryAndNameInFlags < ActiveRecord::Migration[6.0]
  def change
    add_index :flags, [:category, :name], :unique => true
  end
end
