class AddDurationInSecondsToSongs < ActiveRecord::Migration[6.0]
  def change
    add_column :songs, :duration_in_seconds, :integer
    add_index :songs, :duration_in_seconds
  end
end