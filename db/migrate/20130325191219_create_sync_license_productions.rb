class CreateSyncLicenseProductions < ActiveRecord::Migration[4.2]
  def self.up
    create_table :sync_license_productions do |t|
      t.string      :title
      t.string      :description
      t.string      :studio
      t.string      :budget

      t.timestamps
    end

  end

  def self.down
    drop_table :sync_license_productions
  end
end
