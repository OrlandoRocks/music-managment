class RemovedDuplicateTaxColumns < ActiveRecord::Migration[4.2]
  def change
    remove_column :composers, :agreed_to_w9_at
    remove_column :composers, :salt
    remove_column :composers, :encrypted_tax_id
    remove_column :composers, :tax_id
    remove_column :composers, :is_entity
    remove_column :composers, :entity_name
  end
end
