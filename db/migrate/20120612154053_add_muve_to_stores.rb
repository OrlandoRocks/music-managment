class AddMuveToStores < ActiveRecord::Migration[4.2]
  def self.up
    Store.create(:name=>'Muve Music',:abbrev=>'mv', :short_name=>'muve', :position=>290, :needs_rights_assignment=>false, :is_active=>false,:base_price_policy_id=>"3" )
    
  end

  def self.down
    execute("DELETE FROM stores where name = 'muve'")
  end
end
