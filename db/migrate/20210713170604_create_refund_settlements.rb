class CreateRefundSettlements < ActiveRecord::Migration[6.0]
  def change
    create_table :refund_settlements do |t|
      t.references :refund, index: true
      t.integer :settlement_amount_cents, default: 0
      t.integer :source_id
      t.string :source_type
      t.string :currency
      t.timestamps
    end
  end
end
