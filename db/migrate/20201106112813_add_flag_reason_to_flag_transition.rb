class AddFlagReasonToFlagTransition < ActiveRecord::Migration[6.0]
  def change
    add_reference :flag_transitions, :flag_reason, foreign_key: true
  end
end
