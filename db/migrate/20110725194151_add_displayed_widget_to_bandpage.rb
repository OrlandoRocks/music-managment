class AddDisplayedWidgetToBandpage < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE bandpages
     ADD COLUMN displayed_widget int(11) NULL")
  end

  def self.down
    remove_column :bandpages, :displayed_widget
  end
end
