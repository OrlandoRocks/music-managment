class CreateSyncLicenseOptions < ActiveRecord::Migration[4.2]
  def self.up
    create_table :sync_license_options do |t|
      t.string            :media
      t.string            :territory
      t.string            :term
      t.string            :offer
      t.text              :comments

      t.integer           :sync_license_request_id 

      t.timestamps
    end
  end

  def self.down
    drop_table :sync_license_options
  end
end
