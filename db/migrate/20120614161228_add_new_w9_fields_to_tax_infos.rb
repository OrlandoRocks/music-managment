class AddNewW9FieldsToTaxInfos < ActiveRecord::Migration[4.2]
  def self.up
    add_column :tax_infos, :classification, :string, :limit => 30
    add_column :tax_infos, :llc_classification, :string, :limit => 30
  end

  def self.down
    remove_column :tax_infos, :classification
    remove_column :tax_infos, :llc_classification
  end
end
