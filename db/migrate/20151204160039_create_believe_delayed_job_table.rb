class CreateBelieveDelayedJobTable < ActiveRecord::Migration[4.2]
  def up
    sql =  %Q(CREATE TABLE `believe_delayed_jobs` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `priority` int(11) DEFAULT '0',
      `attempts` int(11) DEFAULT '0',
      `handler` mediumtext,
      `last_error` text,
      `run_at` datetime DEFAULT NULL,
      `locked_at` datetime DEFAULT NULL,
      `failed_at` datetime DEFAULT NULL,
      `locked_by` varchar(255) DEFAULT NULL,
      `created_at` datetime DEFAULT NULL,
      `updated_at` datetime DEFAULT NULL,
      `queue` varchar(255) DEFAULT NULL,
      PRIMARY KEY (`id`),
      KEY `believe_delayed_jobs_priority` (`priority`,`run_at`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;)

    execute sql
  end

  def down
    execute "drop table believe_delayed_jobs;"
  end
end
