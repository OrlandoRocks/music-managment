class CreateAlbumGenreWhitelists < ActiveRecord::Migration[6.0]
  def change
    create_table :album_genre_whitelists do |t|
      t.references :genre, foreign_key: true, index: true, null: false, type: :integer
      t.references :album, foreign_key: true, index: true, null: false, type: :integer

      t.timestamps
    end
  end
end
