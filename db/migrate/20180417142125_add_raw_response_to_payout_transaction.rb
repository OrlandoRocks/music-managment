class AddRawResponseToPayoutTransaction < ActiveRecord::Migration[4.2]
  def change
    add_column :payout_transactions, :raw_response, :text
  end
end
