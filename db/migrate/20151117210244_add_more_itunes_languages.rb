class AddMoreItunesLanguages < ActiveRecord::Migration[4.2]
  def up
    {"Afrikaans" => "af", "Bengali" => "bn", "Bulgarian" => "bg", "Haitian/Haitian Creole" => "ht", "Hindi" => "hi", "Irish" => "ga", "Kazakh" => "kk", "Latin" => "la", "Panjabi/Punjabi" => "pa", "Persian" => "fa", "Sanskrit" => "sa", "Slovenian" => "sl", "Tamil" => "ta", "Telugu" => "te", "Urdu" => "ur", "Zulu" => "zu"}.each do |language, code|
      LanguageCode.create(description: language, code: code)
    end
  end

  def down
    ["Afrikaans", "Bengali", "Bulgarian", "Haitian/Haitian Creole", "Hindi", "Irish", "Kazakh", "Latin", "Panjabi/Punjabi", "Persian", "Sanskrit", "Slovenian", "Tamil", "Telugu", "Urdu", "Zulu"].each do |language|
      LanguageCode.find_by(description: language).destroy
    end
  end
end
