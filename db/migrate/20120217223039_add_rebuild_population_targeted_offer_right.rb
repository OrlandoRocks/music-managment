class AddRebuildPopulationTargetedOfferRight < ActiveRecord::Migration[4.2]

  CONTROLLER_NAME   = "admin/targeted_offers"
  BUILD_POP_ACTION  = "build_population"
  BUILD_POP_NAME    = "targeted_offers/build_population"

  ROLE_NAME       = "Targeted Offers"

  def self.up
    rights = []

    rights << Right.create!( :controller=>CONTROLLER_NAME, :action=>BUILD_POP_ACTION, :name=>BUILD_POP_NAME )

    to_role = Role.find_or_create_by(name: ROLE_NAME)

    rights.each do |right|

      to_role.rights << right

    end

    to_role.save!
  end

  def self.down
    destroy_right   = Right.where("controller=:controller and action=:action and name=:name", {:controller=>CONTROLLER_NAME, :action=>BUILD_POP_ACTION,    :name=>BUILD_POP_NAME }).first

    #HABTM relationship cleans up the join table on destroy
    destroy_right.destroy
  end

end
