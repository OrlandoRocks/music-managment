class AddBrandCodeToCertBatches < ActiveRecord::Migration[4.2]
  def self.up
    add_column :cert_batches, :brand_code, :string, :limit => 6
  end

  def self.down
    remove_column :cert_batches, :brand_code
  end
end
