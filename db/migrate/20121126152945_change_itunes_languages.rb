class ChangeItunesLanguages < ActiveRecord::Migration[4.2]
  def self.up
    execute 'update itunes_languages set code = "en" where code = "en-US"'
    execute 'update itunes_languages set code = "nl" where code = "nl-NL"'
    execute 'update itunes_languages set code = "fi" where code = "fi-FI"'
    execute 'update itunes_languages set code = "it" where code = "it-IT"'
    execute 'update itunes_languages set code = "ja" where code = "ja-JP"'
    execute 'update itunes_languages set code = "no" where code = "no-NO"'
    execute 'insert into itunes_languages (description,code) values ("Arabic","ar")'
    execute 'insert into itunes_languages (description,code) values ("Cantonese","yue-Hant")'
    execute 'insert into itunes_languages (description,code) values ("Catalan","ca")'
    execute 'insert into itunes_languages (description,code) values ("Chinese (Simplified)","cmn-Hans")'
    execute 'insert into itunes_languages (description,code) values ("Chinese (Traditional)","cmn-Hant")'
    execute 'insert into itunes_languages (description,code) values ("Croatian","hr")'
    execute 'insert into itunes_languages (description,code) values ("Czech","cs")'
    execute 'insert into itunes_languages (description,code) values ("Danish","da")'
    execute 'insert into itunes_languages (description,code) values ("Estonian","et")'
    execute 'insert into itunes_languages (description,code) values ("Greek","el")'
    execute 'insert into itunes_languages (description,code) values ("Hebrew","he")'
    execute 'insert into itunes_languages (description,code) values ("Hungarian","hu")'
    execute 'insert into itunes_languages (description,code) values ("Icelandic","is")'
    execute 'insert into itunes_languages (description,code) values ("Indonesian","id")'
    execute 'insert into itunes_languages (description,code) values ("Korean","ko")'
    execute 'insert into itunes_languages (description,code) values ("Lao","lo")'
    execute 'insert into itunes_languages (description,code) values ("Latvian","lv")'
    execute 'insert into itunes_languages (description,code) values ("Lithuanian","lt")'
    execute 'insert into itunes_languages (description,code) values ("Malay","ms")'
    execute 'insert into itunes_languages (description,code) values ("Polish","pl")'
    execute 'insert into itunes_languages (description,code) values ("Romanian","ro")'
    execute 'insert into itunes_languages (description,code) values ("Russian","ru")'
    execute 'insert into itunes_languages (description,code) values ("Slovak","sk")'
    execute 'insert into itunes_languages (description,code) values ("Tagalog","tl")'
    execute 'insert into itunes_languages (description,code) values ("Thai","th")'
    execute 'insert into itunes_languages (description,code) values ("Turkish","tr")'
    execute 'insert into itunes_languages (description,code) values ("Ukrainian","uk")'
    execute 'insert into itunes_languages (description,code) values ("Vietnamese","vi")'
  end

  def self.down
    execute 'update itunes_languages set code = "en-US" where code = "en"'
    execute 'update itunes_languages set code = "nl-NL" where code = "nl"'
    execute 'update itunes_languages set code = "fi-FI" where code = "fi"'
    execute 'update itunes_languages set code = "it-IT" where code = "it"'
    execute 'update itunes_languages set code = "ja-JP" where code = "ja"'
    execute 'update itunes_languages set code = "no-NO" where code = "no"'
    execute 'delete from itunes_languages where code = "ar"'
    execute 'delete from itunes_languages where code = "yue-Hant"'
    execute 'delete from itunes_languages where code = "ca"'
    execute 'delete from itunes_languages where code = "cmn-Hans"'
    execute 'delete from itunes_languages where code = "cmn-Hant"'
    execute 'delete from itunes_languages where code = "hr"'
    execute 'delete from itunes_languages where code = "cs"'
    execute 'delete from itunes_languages where code = "da"'
    execute 'delete from itunes_languages where code = "et"'
    execute 'delete from itunes_languages where code = "el"'
    execute 'delete from itunes_languages where code = "he"'
    execute 'delete from itunes_languages where code = "hu"'
    execute 'delete from itunes_languages where code = "is"'
    execute 'delete from itunes_languages where code = "id"'
    execute 'delete from itunes_languages where code = "ko"'
    execute 'delete from itunes_languages where code = "lo"'
    execute 'delete from itunes_languages where code = "lv"'
    execute 'delete from itunes_languages where code = "lt"'
    execute 'delete from itunes_languages where code = "ms"'
    execute 'delete from itunes_languages where code = "pl"'
    execute 'delete from itunes_languages where code = "ro"'
    execute 'delete from itunes_languages where code = "ru"'
    execute 'delete from itunes_languages where code = "sk"'
    execute 'delete from itunes_languages where code = "tl"'
    execute 'delete from itunes_languages where code = "th"'
    execute 'delete from itunes_languages where code = "tr"'
    execute 'delete from itunes_languages where code = "uk"'
    execute 'delete from itunes_languages where code = "vi"'
  end
end
