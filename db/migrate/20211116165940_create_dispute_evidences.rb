class CreateDisputeEvidences < ActiveRecord::Migration[6.0]
  def change
    create_table :dispute_evidences do |t|
      t.references :dispute, index: true, null: false
      t.references :submitted_by, index: true, null: false
      t.string :text_evidence
      t.timestamps
    end
  end
end
