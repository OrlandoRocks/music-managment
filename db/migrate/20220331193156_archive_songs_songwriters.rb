class ArchiveSongsSongwriters < ActiveRecord::Migration[6.0]
  def self.up
    if table_exists? :songs_songwriters
      safety_assured do
        rename_table :songs_songwriters, :archive_songs_songwriters
      end
    end
  end

  def self.down
    if table_exists? :archive_songs_songwriters
      safety_assured do
        rename_table :archive_songs_songwriters, :songs_songwriters
      end
    end
  end
end
