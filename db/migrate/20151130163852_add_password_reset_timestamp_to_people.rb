class AddPasswordResetTimestampToPeople < ActiveRecord::Migration[4.2]
  def change
    add_column :people, :password_reset_tmsp, :timestamp
  end
end
