class AddScrubbedNameColumnToArtist < ActiveRecord::Migration[6.0]
  def change
    add_column :artists, :scrubbed_name, :string
  end
end
