class ConvertCountryWebsiteLanguageNameToUtf8mb4 < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      execute %Q(
        ALTER TABLE country_website_languages
          MODIFY COLUMN selector_language VARCHAR(191)
          CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
          NOT NULL
      )
    end
  end

  def down
    safety_assured do
      execute %Q(
        ALTER TABLE country_website_languages
          MODIFY COLUMN selector_language varchar(255)
          COLLATE utf8_unicode_ci
          NOT NULL
      )
    end
  end
end
