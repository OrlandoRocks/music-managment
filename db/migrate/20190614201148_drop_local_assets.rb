class DropLocalAssets < ActiveRecord::Migration[4.2]
  def up
    ActiveRecord::Base.transaction do
      ActiveRecord::Base.connection.execute("ALTER TABLE media_assets DROP FOREIGN KEY FK_media_assets;")

      remove_column :media_assets, :local_asset_id

      remove_column :derived_media_assets, :local_asset_id

      ActiveRecord::Base.connection.execute("DROP TABLE local_assets;")
    end
  end

  def down
    ActiveRecord::Base.transaction do
      ActiveRecord::Base.connection.execute(
        "CREATE TABLE `local_assets` (
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key.',
          `path` varchar(255) NOT NULL COMMENT 'Path to location of where file is stored.',
          `created_at` datetime NOT NULL COMMENT 'Date record Created.',
          `updated_at` datetime NOT NULL COMMENT 'Date record Updated.',
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
      )

      ActiveRecord::Base.connection.execute("ALTER TABLE media_assets ADD COLUMN local_asset_id int(10) unsigned DEFAULT NULL COMMENT 'Foreign key to local_assets table.';")
      ActiveRecord::Base.connection.execute("ALTER TABLE media_assets ADD CONSTRAINT FK_media_assets FOREIGN KEY(local_asset_id) REFERENCES local_assets (id);")

      add_column :derived_media_assets, :local_asset_id, :integer
      add_index :derived_media_assets, :local_asset_id
    end
  end
end
