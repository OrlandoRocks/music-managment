class CreateSpatialAudios < ActiveRecord::Migration[6.0]
  def change
    create_table :spatial_audios do |t|
      t.integer :song_id, foreign_key: true, index: true
      t.integer :s3_asset_id, foreign_key: true, index: true
      t.index [:song_id, :s3_asset_id], unique: true

      t.timestamps
    end
  end
end
