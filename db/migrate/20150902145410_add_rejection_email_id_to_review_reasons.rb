class AddRejectionEmailIdToReviewReasons < ActiveRecord::Migration[4.2]
  def change
    change_table :review_reasons do |t|
      t.belongs_to :rejection_email
    end

    add_index :review_reasons, :rejection_email_id, :name => 'rejection_email_id_ix'
  end
end
