class AddPaymentsOsTransactionIdToInvoiceLog < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      add_column :invoice_logs, :payments_os_transaction_id, :integer
    end

  end
end
