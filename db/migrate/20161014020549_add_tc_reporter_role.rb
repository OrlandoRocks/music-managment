class AddTcReporterRole < ActiveRecord::Migration[4.2]
  def up
    Role.create(:name=>"TC Reporter", :long_name => "TC Reporter", :is_administrative => true, :description => "Allows access to tc reporter tools")
  end

  def down
    Role.find_by(name: "TC Reporter").delete
  end
end
