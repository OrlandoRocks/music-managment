class AddPersonIdIndexForTierPeopleLifetimeEarnings < ActiveRecord::Migration[6.0]
  def change
    add_index :tier_people, :person_id
    add_index :lifetime_earnings, :person_id
  end
end
