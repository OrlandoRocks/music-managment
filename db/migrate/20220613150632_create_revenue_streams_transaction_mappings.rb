class CreateRevenueStreamsTransactionMappings < ActiveRecord::Migration[6.0]
  def change
    create_table :revenue_streams_transaction_mappings do |t|
      t.references :revenue_stream, foreign_key: true, null: false
      t.string :person_transaction_type, null: false
      t.string :balance_adjustment_category

      t.timestamps
    end
  end
end
