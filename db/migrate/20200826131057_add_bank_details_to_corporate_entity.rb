class AddBankDetailsToCorporateEntity < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      add_column :corporate_entities, :paying_bank, :string, blank: true
      add_column :corporate_entities, :rib, :string, blank: true
      add_column :corporate_entities, :bic, :string, blank: true
      add_column :corporate_entities, :iban, :string, blank: true
    end
  end
end

