class AddPhoneAndCountryToSyncMembershipRequests < ActiveRecord::Migration[4.2]
  def self.up
    add_column :sync_membership_requests, :phone_number, :string
    add_column :sync_membership_requests, :country,      :string

    ActiveRecord::Base.connection.execute("update sync_membership_requests inner join people on person_id = people.id set sync_membership_requests.country = people.country, sync_membership_requests.phone_number = people.phone_number")
  end

  def self.down
    remove_column :sync_membership_requests, :phone_number
    remove_column :sync_membership_requests, :country
  end
end
