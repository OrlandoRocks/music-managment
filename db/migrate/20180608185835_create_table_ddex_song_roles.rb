class CreateTableDdexSongRoles < ActiveRecord::Migration[4.2]
  def change
    create_table :ddex_song_roles do |t|
      t.belongs_to :song_role
      t.belongs_to :ddex_role
    end

    add_index :ddex_song_roles, :song_role_id
    add_index :ddex_song_roles, :ddex_role_id
  end
end
