class AddVatAmountCentsToInvoice < ActiveRecord::Migration[4.2]
  def change
    add_column :invoices, :vat_amount_cents, :int
  end
end
