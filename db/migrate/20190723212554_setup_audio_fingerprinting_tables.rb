class SetupAudioFingerprintingTables < ActiveRecord::Migration[4.2]
  def change
    create_table :scrapi_jobs do |t|
      t.string :md5
      t.string :job_id
      t.string :status

      t.belongs_to :song

      t.timestamps
    end

    add_index :scrapi_jobs, :song_id
    add_index :scrapi_jobs, :job_id

    create_table :scrapi_job_details do |t|
      t.string :track_title
      t.string :isrc
      t.integer :duration
      t.string :label
      t.string :album_title
      t.string :album_upc
      t.string :artists
      t.string :genres
      t.integer :score

      t.belongs_to :scrapi_job

      t.timestamps
    end

    add_index :scrapi_job_details, :scrapi_job_id
  end
end
