class AddYoutubeMonetizationProduct < ActiveRecord::Migration[4.2]
  def up
    american_product = Product.create({
      created_by_id:      1062275,
      country_website_id: 1,
      name:                "YouTube Money",
      display_name:        "YouTube Money",
      description:        "Our YouTube Money service identifies and collects revenue from your sound recordings on YouTube and deposits it into your TuneCore account.",
      status:              "Active",
      product_type:        "Ad Hoc",
      is_default:          1,
      applies_to_product: "None",
      sort_order:          1,
      renewal_level:      "None",
      price:              25.00,
      currency:            "USD"
    })

    american_product_item = ProductItem.create({
      product_id:   american_product.id,
          name:         "YouTube Money",
          description:  "Our YouTube Money service identifies and collects revenue from your sound recordings on YouTube and deposits it into your TuneCore account.",
          price:        25.00,
          currency:     "USD"
    })

    american_product_item_rule = ProductItemRule.create({
      product_item_id:      american_product_item.id,
      rule_type:            "inventory",
      rule:                 "price_for_each",
      inventory_type:       "Salepoint",
      true_false:           false,
      quantity:             0,
      unlimited:            1,
      minimum:              0,
      maximum:              0,
      price:                0,
      currency:             "USD"
    })

    canadian_product = Product.create({
      created_by_id:      1062275,
      country_website_id: 2,
      name:                "YouTube Money",
      display_name:        "YouTube Money",
      description:        "Our YouTube Money service identifies and collects revenue from your sound recordings on YouTube and deposits it into your TuneCore account.",
      status:              "Active",
      product_type:        "Ad Hoc",
      is_default:          1,
      applies_to_product: "None",
      sort_order:          1,
      renewal_level:      "None",
      price:              25.00,
      currency:            "CAD"
    })

    canadian_product_item = ProductItem.create({
      product_id:   canadian_product.id,
          name:         "YouTube Money",
          description:  "Our YouTube Money service identifies and collects revenue from your sound recordings on YouTube and deposits it into your TuneCore account.",
          price:        25.00,
          currency:     "CAD"
    })

    canadian_product_item_rule = ProductItemRule.create({
      product_item_id:      canadian_product_item.id,
      rule_type:            "inventory",
      rule:                 "price_for_each",
      inventory_type:       "Salepoint",
      true_false:           false,
      quantity:             0,
      unlimited:            1,
      minimum:              0,
      maximum:              0,
      price:                0,
      currency:             "CAD"
    })
  end

  def down
    execute("DELETE product_item_rules FROM product_item_rules inner join product_items on product_item_rules.product_item_id = product_items.id where product_items.name = 'YouTube Money';")
    execute("DELETE FROM product_items WHERE name = 'YouTube Money';")
    execute("DELETE FROM products WHERE name = 'YouTube Money';")
  end
end
