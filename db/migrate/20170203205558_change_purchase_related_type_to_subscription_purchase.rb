class ChangePurchaseRelatedTypeToSubscriptionPurchase < ActiveRecord::Migration[4.2]
  def up
    Purchase.where(related_type: "DropkloudPurchase").update_all(related_type: "SubscriptionPurchase")
  end

  def down
    Purchase.where(related_type: "SubscriptionPurchase").update_all(related_type: "DropkloudPurchase")
  end
end
