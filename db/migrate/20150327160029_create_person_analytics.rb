class CreatePersonAnalytics < ActiveRecord::Migration[4.2]
  def up
    create_table :person_analytics do |t|
      t.integer :person_id, null: false
      t.string :metric_name, null: false
      t.integer :metric_value
      t.timestamps
    end

    add_index :person_analytics, [:person_id, :metric_name], unique: true
    add_index :person_analytics, :metric_name
  end

  def down
    drop_table :person_analytics
  end
end
