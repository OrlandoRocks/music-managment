class AddStatusIndexToPlanDowngradeRequests < ActiveRecord::Migration[6.0]
  def change
    add_index :plan_downgrade_requests, :status, algorithm: :default
  end
end