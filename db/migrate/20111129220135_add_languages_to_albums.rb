class AddLanguagesToAlbums < ActiveRecord::Migration[4.2]
  def self.up
    add_column :albums, :language_code, :string, :default => "en-US"
    add_index :albums, :language_code
  end

  def self.down
    remove_column :albums, :language_code
  end
end
