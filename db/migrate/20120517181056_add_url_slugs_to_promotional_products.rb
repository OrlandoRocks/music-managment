class AddUrlSlugsToPromotionalProducts < ActiveRecord::Migration[4.2]
  def self.up
    add_column :promotional_products, :url_slug, :string
  end

  def self.down
    remove_column :promotional_products, :url_slug
  end
end
