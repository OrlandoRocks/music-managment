class AddRollbackToPayoutTransfers < ActiveRecord::Migration[4.2]
  def change
    add_column :payout_transfers, :rolled_back, :boolean, default: false
  end
end
