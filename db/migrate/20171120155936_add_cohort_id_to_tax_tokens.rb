class AddCohortIdToTaxTokens < ActiveRecord::Migration[4.2]
  def change
    add_column :tax_tokens, :tax_tokens_cohort_id, :integer
  end
end
