class AddS3StreamingAssetIdToSongs < ActiveRecord::Migration[6.0]
  def up
    safety_assured {
      execute 'ALTER TABLE songs ADD COLUMN s3_streaming_asset_id INTEGER, ALGORITHM=INPLACE, LOCK=NONE;'
    }
  end

  def down
    safety_assured {
      execute 'ALTER TABLE songs DROP COLUMN s3_streaming_asset_id, ALGORITHM=INPLACE, LOCK=NONE;'
    }
  end
end
