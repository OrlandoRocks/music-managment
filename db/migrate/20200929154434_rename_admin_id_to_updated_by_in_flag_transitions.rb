class RenameAdminIdToUpdatedByInFlagTransitions < ActiveRecord::Migration[6.0]
  def change
    safety_assured { rename_column :flag_transitions, :admin_id, :updated_by }
  end
end
