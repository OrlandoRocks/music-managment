class AddCampaignCodeToFriendReferralCampaign < ActiveRecord::Migration[4.2]
  def self.up
    add_column :friend_referral_campaigns, :campaign_code, :string
  end

  def self.down
    remove_column :friend_referral_campaigns, :campaign_code
  end
end
