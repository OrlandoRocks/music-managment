class AddIndexesToSubscriptionPurchases < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
       add_index :subscription_purchases, :person_id
    end
  end

  def down
    safety_assured do
       remove_index :subscription_purchases, :person_id
    end
  end
end
