class RemoveIsActiveFromPlanDowngradeRequests < ActiveRecord::Migration[6.0]
  def self.up
    safety_assured { remove_column :plan_downgrade_requests, :is_active }
  end

  def self.down
    safety_assured { add_column :plan_downgrade_requests, :is_active, :boolean }
  end
end