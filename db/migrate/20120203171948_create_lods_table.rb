class CreateLodsTable < ActiveRecord::Migration[4.2]
  def self.up
    create_table :lods do |t|
      t.string :esignature_url
      t.string :document_guid
      t.string :template_guid
      t.string :last_status # sent_to_customer, signed_by_customer, resent_to_customer, sent_to_pro, confirmed_by_pro 
      t.datetime :last_status_at
      t.datetime :last_status_by
      t.text :raw_request #the xml of the request to fill the template
      t.timestamps
    end
  end

  def self.down
    drop_table :lods
  end
end
