class CreateTaxInfos < ActiveRecord::Migration[4.2]
  def self.up
    create_table :tax_infos do |t|
      t.integer :composer_id
      t.string :name
      t.string :address_1, :limit => 45
      t.string :address_2, :limit => 45
      t.string :city, :limit => 45
      t.string :state, :limit => 2
      t.string :zip, :limit => 10
      t.string :country, :limit => 2
      t.boolean :nosocial, :default => 0
      t.datetime :agreed_to_w9_at
      t.string :salt, :limit => 40
      t.string :encrypted_tax_id, :limit => 40
      t.boolean :is_entity, :default => 0
      t.string :entity_name
      t.date :dob
      t.string :email, :limit => 100
      t.string :alternate_email, :limit => 100
      t.string :phone, :limit => 20
      t.string :alternate_phone, :limit => 20
      t.timestamps!
    end
  end

  def self.down
    drop_table :tax_infos
  end
end
