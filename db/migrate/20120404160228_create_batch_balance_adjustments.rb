class CreateBatchBalanceAdjustments < ActiveRecord::Migration[4.2]
  def self.up
    create_table :batch_balance_adjustments do |t|
      t.string :ingested_filename
      t.string :status
      t.timestamps
    end
  end

  def self.down
    drop_table :batch_balance_adjustments
  end
end
