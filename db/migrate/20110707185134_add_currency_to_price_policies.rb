class AddCurrencyToPricePolicies < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE price_policies ADD currency CHAR(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency for price policy' AFTER base_price_cents")
  end

  def self.down
    remove_column :price_policies, :currency
  end
end
