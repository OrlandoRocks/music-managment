class AddUniqueConstraintOnSalepointSongsTable < ActiveRecord::Migration[4.2]
  def up
    delivered_salepoint_songs = SalepointSong.find_by_sql("select ss1.* from salepoint_songs ss1
        left join distribution_songs ds on ss1.id = ds.salepoint_song_id
        inner join (select ss1.id as id, ss1.salepoint_id, ss1.song_id, max(ds.updated_at) as dupdated_at from salepoint_songs ss1
        inner join (select min(ss.id) as id, ss.salepoint_id, ss.song_id from salepoint_songs ss
        where ss.state = 'approved'
        group by ss.song_id, ss.salepoint_id
        having count(ss.id) > 1) ss2 on ss1.salepoint_id = ss2.salepoint_id and ss1.song_id = ss2.song_id
        left join distribution_songs ds on ss1.id = ds.salepoint_song_id
        group by ss1.song_id, ss1.salepoint_id) ss2 on ss1.song_id = ss2.song_id and ss1.salepoint_id = ss2.salepoint_id and ds.updated_at <> ss2.dupdated_at")

    ss_ids = delivered_salepoint_songs.map{|ss| ss.id }

    DistributionSong.where("salepoint_song_id IN (?)", ss_ids).delete_all
    SalepointSong.where("id IN (?)", ss_ids).delete_all

    other_delivered_salepoint_songs = SalepointSong.find_by_sql("select ss1.* from salepoint_songs ss1
        inner join (select max(ss.id) as id, ss.salepoint_id, ss.song_id from salepoint_songs ss
        where ss.state = 'approved'
        group by ss.song_id, ss.salepoint_id
        having count(ss.id) > 1) ss2 on ss1.salepoint_id = ss2.salepoint_id and ss1.song_id = ss2.song_id
        where ss1.id <> ss2.id;")

    ss_ids = other_delivered_salepoint_songs.map{|ss| ss.id }

    DistributionSong.where("salepoint_song_id IN (?)", ss_ids).delete_all
    SalepointSong.where("id IN (?)", ss_ids).delete_all

    salepoint_songs = SalepointSong.find_by_sql("select ss1.* from salepoint_songs ss1
        inner join (select max(ss.id) as id, ss.salepoint_id, ss.song_id from salepoint_songs ss
        group by ss.song_id, ss.salepoint_id
        having count(ss.id) > 1) ss2 on ss1.salepoint_id = ss2.salepoint_id and ss1.song_id = ss2.song_id
        where ss1.id <> ss2.id;")

    ss_ids = salepoint_songs.map{|ss| ss.id }
    SalepointSong.where("id IN (?)", ss_ids).delete_all

    add_index :salepoint_songs, [:salepoint_id, :song_id], unique: true, name: "index_unique_salepoint_song_ids"
    remove_index :salepoint_songs, column: :salepoint_id
  end

  def down
    remove_index :salepoint_songs, name: "index_unique_salepoint_song_ids"
    add_index :salepoint_songs, :salepoint_id
  end
end
