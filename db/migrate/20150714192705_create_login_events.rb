class CreateLoginEvents < ActiveRecord::Migration[4.2]
  def change
    create_table :login_events do |t|
      t.integer :person_id
      t.datetime :created_at
    end

    add_index :login_events, :person_id
  end
end
