class AddCurrencyAndCountryToEftBatchTransactions < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE eft_batch_transactions
     ADD COLUMN `country_website_id` int(11) unsigned DEFAULT 1 COMMENT 'Foreign Key to country_websites' AFTER id,
     ADD COLUMN currency CHAR(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency' AFTER amount,
     ADD KEY `ebt_country_website_id` (`country_website_id`)")
  end

  def self.down
    remove_column :eft_batch_transactions, :currency
    remove_column :eft_batch_transactions, :country_website_id
  end
end
