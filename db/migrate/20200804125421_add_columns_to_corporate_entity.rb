class AddColumnsToCorporateEntity < ActiveRecord::Migration[6.0]
  def change
    add_column :corporate_entities, :currency, :string
    add_column :corporate_entities, :address1, :string
    add_column :corporate_entities, :address2, :string
    add_column :corporate_entities, :city, :string
    add_column :corporate_entities, :state, :string
    add_column :corporate_entities, :postal_code, :string
    add_column :corporate_entities, :country, :string
    add_column :corporate_entities, :vat_registration_number, :string
    add_column :corporate_entities, :crn, :string
  end
end
