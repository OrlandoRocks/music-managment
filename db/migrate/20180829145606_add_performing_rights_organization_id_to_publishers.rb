class AddPerformingRightsOrganizationIdToPublishers < ActiveRecord::Migration[4.2]
  def change
    add_column :publishers, :performing_rights_organization_id, :integer

    add_index :publishers, :performing_rights_organization_id
  end
end
