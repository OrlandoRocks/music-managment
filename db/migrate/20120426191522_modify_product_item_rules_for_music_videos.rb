class ModifyProductItemRulesForMusicVideos < ActiveRecord::Migration[4.2]
  def self.up
    execute ("update product_item_rules set model_to_check = 'MusicVideo' where model_to_check = 'Video'")
    execute('update product_item_rules set inventory_type = "MusicVideo" where inventory_type = "Video"')
  end

  def self.down
    execute ("update product_item_rules set model_to_check = 'Video' where model_to_check = 'MusicVideo'")
    execute('update product_item_rules set inventory_type = "Video" where inventory_type = "MusicVideo"')
  end
end
