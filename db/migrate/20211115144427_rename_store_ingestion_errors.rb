class RenameStoreIngestionErrors < ActiveRecord::Migration[6.0]
  def self.up
    safety_assured do
      remove_index :store_ingestion_errors, :store_ingestion_id
      rename_column :store_ingestion_errors, :store_ingestion_id, :archive_store_ingestion_id
      rename_table :store_ingestion_errors, :archive_store_ingestion_errors
      add_index :archive_store_ingestion_errors, :archive_store_ingestion_id,
                name: "index_arc_store_ing_errors_on_arc_store_ing_id"
    end
  end

  def self.down
    safety_assured do
      remove_index :archive_store_ingestion_errors, :archive_store_ingestion_id
      rename_column :archive_store_ingestion_errors, :archive_store_ingestion_id, :store_ingestion_id
      rename_table :archive_store_ingestion_errors, :store_ingestion_errors
      add_index :store_ingestion_errors, :store_ingestion_id
    end
  end
end
