class AddCwrNameToCompositions < ActiveRecord::Migration[4.2]
  def self.up
    add_column :compositions, :cwr_name, :string
  end

  def self.down
    remove_column :compositions, :cwr_name
  end
end
