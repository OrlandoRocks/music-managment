class AddTidalDapCategoryToBalanceAdjustments < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      execute <<-SQL
        ALTER TABLE balance_adjustments MODIFY COLUMN category ENUM('Refund - Renewal','Refund - Other','Service Adjustment','Songwriter Royalty','Other','YouTube MCN Royalty', 'Facebook', 'Tidal DAP') DEFAULT NULL COMMENT 'Category of adjustment'
      SQL
    end
  end

  def down
    safety_assured do
      execute <<-SQL
        ALTER TABLE balance_adjustments MODIFY COLUMN category ENUM('Refund - Renewal','Refund - Other','Service Adjustment','Songwriter Royalty','Other','YouTube MCN Royalty', 'Facebook') DEFAULT NULL COMMENT 'Category of adjustment'
      SQL
    end
  end
end
