class AddColumnToTargetedOffer < ActiveRecord::Migration[4.2]
  def up
    add_column :targeted_offers, :job_status, :string
  end

  def down
    remove_column :targeted_offers, :job_status
  end
end
