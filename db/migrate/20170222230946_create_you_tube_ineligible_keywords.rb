class CreateYouTubeIneligibleKeywords < ActiveRecord::Migration[4.2]
  def up
    create_table :you_tube_ineligible_keywords do |t|
      t.string      :keyword
      t.boolean     :active, default: true
      t.timestamps
    end

    [ "Nature Sounds",
      "Meditation",
      "Relaxation",
      "Video Game",
      "Compilation",
      "Karaoke"
    ].each { |keyword| YouTubeIneligibleKeyword.create(keyword: keyword) }
  end

  def down
    drop_table :you_tube_ineligible_keywords
  end
end


