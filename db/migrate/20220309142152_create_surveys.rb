class CreateSurveys < ActiveRecord::Migration[6.0]
  def change
    create_table :surveys do |t|
      t.boolean :multiple_response, null: false
      t.string :name, unique: true, null: false
      t.text :mandatory_json_keys, array: true
      t.index :name, unique: true

      t.timestamps
    end
  end
end
