class ChangeColumnSongLibraryUploadsUnusedFieldsNullTrue < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      change_column :song_library_uploads, :bucket_name, :string, null: true
      change_column :song_library_uploads, :asset_key, :string, null: true
    end
  end
end
