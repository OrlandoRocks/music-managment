class AddBlockedColumnToAccounts < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      add_column :accounts, :blocked, :boolean, default: false
    end
  end

  def down
    safety_assured do
      remove_column :accounts, :blocked
    end
  end
end
