class RemoveDocumentTemplateTitleByTypeIndex < ActiveRecord::Migration[4.2]
  def change
    remove_index :document_templates, name: "index_type_with_title_by_revision"
  end
end
