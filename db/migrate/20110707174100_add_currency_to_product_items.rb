class AddCurrencyToProductItems < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE product_items ADD currency CHAR(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency for product item' AFTER price")
  end

  def self.down
    remove_column :product_items, :currency
  end
end
