

class AddInfoColumnsToCountries < ActiveRecord::Migration[4.2]
  def up
    add_column :countries, :region, :string
    add_column :countries, :sub_region, :string
    add_column :countries, :tc_region, "char(4)"
    add_column :countries, :iso_code_3, "char(3)"

    info = CSV.open("db/seed/csv/countries.csv")
    info.read.each do |country|
      Country.find(country[0]).update(region: country[3], sub_region: country[4], tc_region: country[5], iso_code_3: country[6])
    end

    change_column :countries, :region, :string, null: false
    change_column :countries, :tc_region, "char(4)", null: false
    change_column :countries, :iso_code_3, "char(3)", null: false
  end

  def down
    remove_column :countries, :region
    remove_column :countries, :sub_region
    remove_column :countries, :tc_region
    remove_column :countries, :iso_code_3
  end
end
