class AddRejectReasonToSalepointSongs < ActiveRecord::Migration[4.2]
  def change
    add_column :salepoint_songs, :reason_for_rejection, :string
    add_column :salepoint_songs, :comments_for_rejection, :string
  end
end
