class CreateDistributionApiExternalServiceIdApiStatuses < ActiveRecord::Migration[6.0]
  def up
    create_table :distribution_api_external_service_id_api_statuses do |t|
      t.references :album, foreign_key: true, index: {name: 'index_distribution_api_esid_statuses_on_album_id'}, null: false, type: :integer
      t.references :distribution_api_service, foreign_key: true, index: {name: 'index_distribution_api_esid_statuses_on_distribution_api_service'}, null: false, type: :bigint
      t.date :creation_date
      t.text :response_status
      t.timestamps
    end
  end

  def down
    drop_table :distribution_api_external_service_id_api_statuses
  end
end
