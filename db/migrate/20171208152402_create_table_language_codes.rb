class CreateTableLanguageCodes < ActiveRecord::Migration[4.2]
  def up
    create_table :language_codes do |t|
      t.string :description
      t.string :code
    end

    execute("INSERT INTO language_codes SELECT * FROM itunes_languages;")
  end

  def down
    drop_table :language_codes
  end
end
