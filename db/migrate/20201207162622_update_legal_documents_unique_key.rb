class UpdateLegalDocumentsUniqueKey < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      remove_index :legal_documents, :external_uuid

      add_index :legal_documents,
        [:external_uuid, :subject_id, :subject_type],
        unique: true,
        name: "index_legal_documents_on_uuid_and_subject"
    end
  end

  def down
    safety_assured do
      execute("ALTER TABLE legal_documents DROP INDEX index_legal_documents_on_uuid_and_subject;")

      add_index :legal_documents,
        :external_uuid,
        unique: true
    end
  end
end
