class CreateRewards < ActiveRecord::Migration[6.0]
  def change
    create_table :rewards do |t|
      t.string :name, null: false
      t.text :description
      t.string :link
      t.string :content_type, null: false
      t.string :category, null: false
      t.boolean :is_active, null: false, default: true
      t.integer :points, null: false

      t.timestamps
    end
  end
end
