class CreateNonTunecoreAlbums < ActiveRecord::Migration[4.2]
  def change
    create_table :non_tunecore_albums do |t|
      t.string  :name, null: :false
      t.integer :composer_id, null: :false
      t.integer :album_id
      t.string  :upc
      t.date    :orig_release_year
      t.string  :record_label, limit: 120
      t.timestamps
    end

    add_index :non_tunecore_albums, :album_id, where: "album_id IS NOT NULL"
    add_index :non_tunecore_albums, :composer_id
  end
end
