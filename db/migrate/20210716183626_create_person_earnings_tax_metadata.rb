class CreatePersonEarningsTaxMetadata < ActiveRecord::Migration[6.0]
  def change
    create_table :person_earnings_tax_metadata do |t|
      t.belongs_to :person, null: false
      t.datetime   :pub_taxform_submitted_at
      t.bigint     :pub_taxform_id
      t.datetime   :dist_taxform_submitted_at
      t.bigint     :dist_taxform_id
      t.decimal    :total_publishing_earnings, default: 0.0, null: false, scale: 14, precision: 23
      t.decimal    :total_distribution_earnings, default: 0.0, null: false, scale: 14, precision: 23
      t.integer    :tax_year
      t.boolean    :tax_blocked, default: false
      t.timestamps
      t.index :tax_year
      t.index :pub_taxform_id
      t.index :dist_taxform_id
    end
  end
end
