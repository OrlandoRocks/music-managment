class CreateAdyenTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :adyen_transactions do |t|
      t.references :adyen_stored_payment_method
      t.references :foreign_exchange_rate
      t.references :invoice, null: false
      t.references :person, null: false
      t.references :country, null: false
      t.float :amount, null: false
      t.float :amount_in_local_currency, null: false
      t.string :auth_code
      t.string :currency, null: false
      t.string :local_currency, null: false
      t.string :error_message
      t.string :merchant_reference
      t.string :psp_reference
      t.string :reason
      t.string :ip_address
      t.column :result_code, "enum('AuthenticationFinished','AuthenticationNotRequired','Authorised','Cancelled','ChallengeShopper','Error','IdentifyShopper','Pending','PresentToShopper','Received','RedirectShopper','Refused')"
      t.string :session_id
      t.column :status, "enum('success','failure')"
      t.timestamps
    end
  end
end
