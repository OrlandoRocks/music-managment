class ChangeSignerIdToPersonId < ActiveRecord::Migration[4.2]
  def change
    rename_column :legal_documents, :signer_id, :person_id
  end
end
