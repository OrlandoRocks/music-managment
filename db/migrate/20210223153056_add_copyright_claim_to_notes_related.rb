class AddCopyrightClaimToNotesRelated < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      execute "ALTER TABLE notes
        MODIFY COLUMN related_type enum(
          'Person',
          'Album',
          'Song',
          'EftBatchTransaction',
          'SubscriptionEvent',
          'Composer',
          'PublishingComposer',
          'FlagTransition',
          'CopyrightClaim'
        ) DEFAULT NULL COMMENT 'Type of entity.'"
    end
  end

  def down
    safety_assured do
      execute "ALTER TABLE notes
        MODIFY COLUMN related_type enum(
          'Person',
          'Album',
          'Song',
          'EftBatchTransaction',
          'SubscriptionEvent',
          'Composer',
          'PublishingComposer',
          'FlagTransition'
        ) DEFAULT NULL COMMENT 'Type of entity.'"
    end
  end
end
