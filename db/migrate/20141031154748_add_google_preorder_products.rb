class AddGooglePreorderProducts < ActiveRecord::Migration[4.2]
  def up
    product = Product.create!(
      :created_by_id=>1013710,
      :country_website_id=>1,
      :name=>"Preorder",
      :display_name=>"Preorder",
      :description=>"Make your release available for sale before the street date.",
      :status=>"Active",
      :product_type=>"Ad Hoc",
      :is_default=>1,
      :applies_to_product=>"PreorderPurchase",
      :sort_order=>1,
      :renewal_level=>"None",
      :price=>0.00,
      :currency=>"USD"
    )
    
    product_item = ProductItem.create!(
      product: product,
      name: "Preorder",
      description: "Make your release available for sale before the street date.",
      price: 0.00,
      currency: "USD",
      does_not_renew: 0
    )
    
    ProductItemRule.create!(
      product_item: product_item,
      rule_type: "price_only",
      rule: "range",
      inventory_type: "preorder_count",
      model_to_check: "PreorderPurchase",
      method_to_check: "enabled_preorder_data_count",
      quantity: 1,
      unlimited: 0,
      true_false: 0,
      minimum: 1,
      maximum: 1,
      price: 15.00,
      :currency => "USD"
    )
    
    ProductItemRule.create!(
      product_item: product_item,
      rule_type: "price_only",
      rule: "range",
      inventory_type: "preorder_count",
      model_to_check: "PreorderPurchase",
      method_to_check: "enabled_preorder_data_count",
      quantity: 1,
      unlimited: 0,
      true_false: 0,
      minimum: 2,
      maximum: 2,
      price: 25.00,
      :currency => "USD"
    )
    
    product = Product.create!(
      :created_by_id=>1013710,
      :country_website_id=>2,
      :name=>"Preorder",
      :display_name=>"Preorder",
      :description=>"Make your release available for sale before the street date.",
      :status=>"Active",
      :product_type=>"Ad Hoc",
      :is_default=>1,
      :applies_to_product=>"PreorderPurchase",
      :sort_order=>1,
      :renewal_level=>"None",
      :price=>0.00,
      :currency=>"CAD"
    )
    
    product_item = ProductItem.create!(
      product: product,
      name: "Preorder",
      description: "Make your release available for sale before the street date.",
      price: 0.00,
      currency: "CAD",
      does_not_renew: 0
    )
    
    ProductItemRule.create!(
      product_item: product_item,
      rule_type: "price_only",
      rule: "range",
      inventory_type: "preorder_count",
      model_to_check: "PreorderPurchase",
      method_to_check: "enabled_preorder_data_count",
      quantity: 1,
      unlimited: 0,
      true_false: 0,
      minimum: 1,
      maximum: 1,
      price: 15.00,
      :currency => "CAD"
    )
    
    ProductItemRule.create!(
      product_item: product_item,
      rule_type: "price_only",
      rule: "range",
      inventory_type: "preorder_count",
      model_to_check: "PreorderPurchase",
      method_to_check: "enabled_preorder_data_count",
      quantity: 1,
      unlimited: 0,
      true_false: 0,
      minimum: 2,
      maximum: 2,
      price: 25.00,
      :currency => "CAD"
    )
  end

  def down
    execute("DELETE product_item_rules FROM product_item_rules inner join product_items on product_item_rules.product_item_id = product_items.id where product_items.name = 'Preorder';")
    execute("DELETE FROM product_items WHERE name = 'Preorder';")
    execute("DELETE FROM products WHERE name = 'Preorder';")
  end
end
