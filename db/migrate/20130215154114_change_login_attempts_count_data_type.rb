class ChangeLoginAttemptsCountDataType < ActiveRecord::Migration[4.2]
  def self.up
    change_column :login_attempts, :count, :integer, :limit => 2
  end

  def self.down
    change_column :login_attempts, :count, :integer
  end
end
