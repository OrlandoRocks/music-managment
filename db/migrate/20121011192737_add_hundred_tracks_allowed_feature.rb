class AddHundredTracksAllowedFeature < ActiveRecord::Migration[4.2]
  def self.up
    Feature.find_or_create_by(:name=>'hundred_tracks_allowed', :restrict_by_user => 1)
  end

  def self.down
    feature = Feature.find_by(name: 'hundred_tracks_allowed')
    feature.destroy if !feature.nil?
  end

end
