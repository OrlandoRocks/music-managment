class Create5YearRenewalProducts < ActiveRecord::Migration[4.2]

  def up
    product = Product.create!(
      :created_by_id=>1,
      :country_website_id=>1,
      :name=>"5 Year Album Renewal",
      :display_name=>"5 Year Album Renewal",
      :description=>"5 year renewal fee for your album.",
      :status=>"Active",
      :product_type=>"Renewal",
      :is_default=>0,
      :applies_to_product=>"Album",
      :sort_order=>1,
      :renewal_level=>"None",
      :renewal_interval=>"year",
      :renewal_duration=>5,
      :currency=>"USD",
      :price=>224.96
    )
    product.renewal_product_id = product.id
    product.save!

    product = Product.create!(
      :created_by_id=>1,
      :country_website_id=>2,
      :name=>"5 Year Album Renewal",
      :display_name=>"5 Year Album Renewal",
      :description=>"5 year renewal fee for your album.",
      :status=>"Active",
      :product_type=>"Renewal",
      :is_default=>0,
      :applies_to_product=>"Album",
      :sort_order=>1,
      :renewal_level=>"None",
      :renewal_interval=>"year",
      :renewal_duration=>5,
      :currency=>"CAD",
      :price=>224.96
    )
    product.renewal_product_id = product.id
    product.save!

    product = Product.create!(
      :created_by_id=>1,
      :country_website_id=>1,
      :name=>"5 Year Single Renewal",
      :display_name=>"5 Year Single Renewal",
      :description=>"5 year renewal fee for your album.",
      :status=>"Active",
      :product_type=>"Renewal",
      :is_default=>0,
      :applies_to_product=>"Single",
      :sort_order=>1,
      :renewal_level=>"None",
      :renewal_interval=>"year",
      :renewal_duration=>5,
      :currency=>"USD",
      :price=>224.96
    )
    product.renewal_product_id = product.id
    product.save!

    product = Product.create!(
      :created_by_id=>1,
      :country_website_id=>2,
      :name=>"5 Year Single Renewal",
      :display_name=>"5 Year Single Renewal",
      :description=>"5 year renewal fee for your album.",
      :status=>"Active",
      :product_type=>"Renewal",
      :is_default=>0,
      :applies_to_product=>"Single",
      :sort_order=>1,
      :renewal_level=>"None",
      :renewal_interval=>"year",
      :renewal_duration=>5,
      :currency=>"CAD",
      :price=>224.96
    )
    product.renewal_product_id = product.id
    product.save!
  end

  def down
    Product.where("name=?","5 Year Album Renewal").each {|p| p.destroy}
    Product.where("name=?","5 Year Single Renewal").each {|p| p.destroy}
  end

end
