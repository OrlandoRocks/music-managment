class AddEventDateToTunecoreTracking < ActiveRecord::Migration[6.0]
  def change
    add_column :tunecore_tracking, :event_date, :datetime, null: true, default: nil
  end
end
