class AddFacebookProducts < ActiveRecord::Migration[4.2]
  def up
    execute("ALTER TABLE purchases CHANGE related_type related_type enum('Album','Single','Ringtone','Entitlement','Product','Renewal','ItunesUserReport','MusicVideo','FeatureFilm','Salepoint','Song','Widget','Composer','Video','SalepointSubscription','CreditUsage','Booklet','SalepointPreorderData','SoundoutReport','FacebookRecognitionServicePurchase');")

    create_table  :facebook_recognition_service_products do |t|
      t.string    :product_name
      t.integer   :min_releases
      t.integer   :max_releases
      t.integer   :product_id
      t.timestamps
    end

    create_table  :facebook_recognition_service_purchases do |t|
      t.integer   :facebook_recognition_service_product_id
      t.integer   :person_id
      t.datetime  :effective_date
      t.datetime  :termination_date
      t.timestamps
    end
    
    add_index :facebook_recognition_service_purchases, :person_id

    product_1 = Product.create({
      created_by_id:      1004636,
      country_website_id: 1,
      name:               "Facebook audio recognition",
      display_name:       "Facebook audio recognition",
      description:        "Add your music to Facebook so Facebook can detect a match and let people promote your music to friends.",
      status:             "Active",
      product_type:       "Ad Hoc",
      is_default:         1,
      applies_to_product: "None",
      sort_order:         1,
      renewal_level:      "None",
      price:              2.25,
      currency:           "USD"
    })

    product_item_1 = ProductItem.create({
      product_id:   product_1.id,
      name:         "Facebook audio recognition",
      description:  "Add your music to Facebook so Facebook can detect a match and let people promote your music to friends.",
      price:        2.25,
      currency:     "USD"
    })
    
    product_item_rule_1b = ProductItemRule.create({
      product_item_id:      product_item_1.id,
      rule_type:            "inventory",
      rule:                 "price_for_each",
      inventory_type:       "Salepoint",
      true_false:           false,
      quantity:             0,
      unlimited:            1,
      minimum:              0,
      maximum:              0,
      price:                0,
      currency:             "USD"
    })

    product_2 = Product.create({
      created_by_id:      1004636,
      country_website_id: 1,
      name:               "Facebook audio recognition",
      display_name:       "Facebook audio recognition",
      description:        "Add your music to Facebook so Facebook can detect a match and let people promote your music to friends.",
      status:             "Active",
      product_type:       "Ad Hoc",
      is_default:         1,
      applies_to_product: "None",
      sort_order:         1,
      renewal_level:      "None",
      price:              5.00,
      currency:           "USD"
    })

    product_item_2 = ProductItem.create({
      product_id:   product_2.id,
      name:         "Facebook audio recognition",
      description:  "Add your music to Facebook so Facebook can detect a match and let people promote your music to friends.",
      price:        5.00,
      currency:     "USD"
    })
    
    product_item_rule_2b = ProductItemRule.create({
      product_item_id:      product_item_2.id,
      rule_type:            "inventory",
      rule:                 "price_for_each",
      inventory_type:       "Salepoint",
      true_false:           false,
      quantity:             0,
      unlimited:            1,
      minimum:              0,
      maximum:              0,
      price:                0,
      currency:             "USD"
    })

    product_3 = Product.create({
      created_by_id:      1004636,
      country_website_id: 2,
      name:               "Facebook audio recognition",
      display_name:       "Facebook audio recognition",
      description:        "Add your music to Facebook so Facebook can detect a match and let people promote your music to friends.",
      status:             "Active",
      product_type:       "Ad Hoc",
      is_default:         1,
      applies_to_product: "None",
      sort_order:         1,
      renewal_level:      "None",
      price:              2.25,
      currency:           "CAD"
    })

    product_item_3 = ProductItem.create({
      product_id:   product_3.id,
      name:         "Facebook audio recognition",
      description:  "Add your music to Facebook so Facebook can detect a match and let people promote your music to friends.",
      price:        2.25,
      currency:     "CAD"
    })

    product_item_rule_3b = ProductItemRule.create({
      product_item_id:      product_item_3.id,
      rule_type:            "inventory",
      rule:                 "price_for_each",
      inventory_type:       "Salepoint",
      true_false:           false,
      quantity:             0,
      unlimited:            1,
      minimum:              0,
      maximum:              0,
      price:                0,
      currency:             "USD"
    })
     
    product_4 = Product.create({
      created_by_id:      1004636,
      country_website_id: 2,
      name:               "Facebook audio recognition",
      display_name:       "Facebook audio recognition",
      description:        "Add your music to Facebook so Facebook can detect a match and let people promote your music to friends.",
      status:             "Active",
      product_type:       "Ad Hoc",
      is_default:         1,
      applies_to_product: "None",
      sort_order:         1,
      renewal_level:      "None",
      price:              5.00,
      currency:           "CAD"
    })

    product_item_4 = ProductItem.create({
      product_id:   product_4.id,
      name:         "Facebook audio recognition",
      description:  "Add your music to Facebook so Facebook can detect a match and let people promote your music to friends.",
      price:        5.00,
      currency:     "CAD"
    })
    
    product_item_rule_4b = ProductItemRule.create({
      product_item_id:      product_item_4.id,
      rule_type:            "inventory",
      rule:                 "price_for_each",
      inventory_type:       "Salepoint",
      true_false:           false,
      quantity:             0,
      unlimited:            1,
      minimum:              0,
      maximum:              0,
      price:                0,
      currency:             "USD"
    })
    
    product_5 = Product.create({
      created_by_id:      1004636,
      country_website_id: 1,
      name:               "Facebook audio recognition",
      display_name:       "Facebook audio recognition",
      description:        "Add your music to Facebook so Facebook can detect a match and let people promote your music to friends.",
      status:             "Active",
      product_type:       "Ad Hoc",
      is_default:         1,
      applies_to_product: "None",
      sort_order:         1,
      renewal_level:      "None",
      price:              10.00,
      currency:           "USD"
    })

    product_item_5 = ProductItem.create({
      product_id:   product_5.id,
      name:         "Facebook audio recognition",
      description:  "Add your music to Facebook so Facebook can detect a match and let people promote your music to friends.",
      price:        10.00,
      currency:     "USD"
    })
    
    product_item_rule_5b = ProductItemRule.create({
      product_item_id:      product_item_5.id,
      rule_type:            "inventory",
      rule:                 "price_for_each",
      inventory_type:       "Salepoint",
      true_false:           false,
      quantity:             0,
      unlimited:            1,
      minimum:              0,
      maximum:              0,
      price:                0,
      currency:             "USD"
    })
    
    product_6 = Product.create({
      created_by_id:      1004636,
      country_website_id: 2,
      name:               "Facebook audio recognition",
      display_name:       "Facebook audio recognition",
      description:        "Add your music to Facebook so Facebook can detect a match and let people promote your music to friends.",
      status:             "Active",
      product_type:       "Ad Hoc",
      is_default:         1,
      applies_to_product: "None",
      sort_order:         1,
      renewal_level:      "None",
      price:              10.00,
      currency:           "CAD"
    })

    product_item_6 = ProductItem.create({
      product_id:   product_6.id,
      name:         "Facebook audio recognition",
      description:  "Add your music to Facebook so Facebook can detect a match and let people promote your music to friends.",
      price:        10.00,
      currency:     "CAD"
    })
    
    product_item_rule_6b = ProductItemRule.create({
      product_item_id:      product_item_6.id,
      rule_type:            "inventory",
      rule:                 "price_for_each",
      inventory_type:       "Salepoint",
      true_false:           false,
      quantity:             0,
      unlimited:            1,
      minimum:              0,
      maximum:              0,
      price:                0,
      currency:             "USD"
    })

    FacebookRecognitionServiceProduct.create({
      product_name: "Facebook audio recognition",
      min_releases: 1,
      max_releases: 1,
      product_id: product_1.id
    })

    FacebookRecognitionServiceProduct.create({
      product_name: "Facebook audio recognition",
      min_releases: 1,
      max_releases: 1,
      product_id: product_3.id
    })

    FacebookRecognitionServiceProduct.create({
      product_name: "Facebook audio recognition",
      min_releases: 2,
      max_releases: 10,
      product_id: product_2.id
    })

    FacebookRecognitionServiceProduct.create({
      product_name: "Facebook audio recognition",
      min_releases: 2,
      max_releases: 10,
      product_id: product_4.id
    })
    
    FacebookRecognitionServiceProduct.create({
      product_name: "Facebook audio recognition",
      min_releases: 11,
      max_releases: 999999999,
      product_id: product_5.id
    })
    
    FacebookRecognitionServiceProduct.create({
      product_name: "Facebook audio recognition",
      min_releases: 11,
      max_releases: 999999999,
      product_id: product_6.id
    })
  end

  def down
    execute("ALTER TABLE purchases CHANGE related_type related_type enum('Album','Single','Ringtone','Entitlement','Product','Renewal','ItunesUserReport','MusicVideo','FeatureFilm','Salepoint','Song','Widget','Composer','Video','SalepointSubscription','CreditUsage','Booklet','SalepointPreorderData','SoundoutReport');")

    drop_table :facebook_recognition_service_products
    drop_table :facebook_recognition_service_purchases

    execute("DELETE product_item_rules FROM product_item_rules inner join product_items on product_item_rules.product_item_id = product_items.id where product_items.name like '%Facebook%';")
    execute("DELETE FROM product_items WHERE name like '%Facebook%';")
    execute("DELETE FROM products WHERE name like '%Facebook%';")

  end
end
