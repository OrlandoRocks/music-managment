class RemoveExpiresAtFromPersonPlans < ActiveRecord::Migration[6.0]
  def change
    safety_assured { remove_column :person_plans, :expires_at, :datetime }
  end
end
