class AddSongIdToCreativeSongRoles < ActiveRecord::Migration[4.2]
  def change
    add_column :creative_song_roles, :song_id, :integer
  end
end
