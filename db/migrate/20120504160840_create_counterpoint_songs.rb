class CreateCounterpointSongs < ActiveRecord::Migration[4.2]
  def self.up
    create_table :counterpoint_songs do |t|
      t.integer :code
      t.string :title
      t.string :composer
      t.string :controlled_mech_share
      t.string :performing_artist_group
      t.string :first_album_label
      t.string :first_album_title
      t.string :created_date
      t.string :album_upc
      t.string :tunecore_song_id
      t.string :tunecore_composition_id
      t.string :grouping_code
      t.string :grouping_code_2
      t.string :credit_notes_label_copy
      t.string :status
      t.timestamps
      # Code  Title Composer  Controlled Mech. Share  Performing Artist/Group First Album Label First Album Title Created Date  Album UPC TuneCore Song ID  TuneCore Composition ID Grouping Code Grouping Code 2 Credit Notes / Label Copy
    end
  end

  def self.down
    drop_table :counterpoint_songs
  end
end
