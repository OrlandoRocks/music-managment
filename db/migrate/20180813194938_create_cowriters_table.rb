class CreateCowritersTable < ActiveRecord::Migration[4.2]
  def change
    create_table :cowriters do |t|
      t.string :first_name
      t.string :last_name
      t.string :cae
      t.integer :composer_id
      t.integer :performing_rights_organization_id

      t.timestamps
    end

    add_index :cowriters, :first_name
    add_index :cowriters, :last_name
    add_index :cowriters, :cae
    add_index :cowriters, :composer_id
    add_index :cowriters, :performing_rights_organization_id
  end
end
