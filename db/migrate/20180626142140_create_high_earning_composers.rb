class CreateHighEarningComposers < ActiveRecord::Migration[4.2]
  def change
    create_table :high_earning_composers do |t|
      t.belongs_to :person

      t.timestamps
    end
    add_index :high_earning_composers, :person_id
  end
end
