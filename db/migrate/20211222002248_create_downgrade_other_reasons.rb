class CreateDowngradeOtherReasons < ActiveRecord::Migration[6.0]
  def change
    create_table :downgrade_other_reasons do |t|
      t.references :plan_downgrade_request
      t.string :description

      t.timestamps
    end
  end
end
