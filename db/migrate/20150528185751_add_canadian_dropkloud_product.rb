class AddCanadianDropkloudProduct < ActiveRecord::Migration[4.2]
  def up
    product_ca = Product.create({
      created_by_id:      1013710,
      country_website_id: 2,
      name:               "Dropkloud",
      display_name:       "Dropkloud",
      description:        "Dropkloud",
      status:             "Active",
      product_type:       "Ad Hoc",
      is_default:         1,
      applies_to_product: "None",
      sort_order:         1,
      renewal_level:      "None",
      price:              8.99,
      currency:           "CAD"
    })

    product_item_ca = ProductItem.create({
      product_id:   product_ca.id,
      name:         "Dropkloud",
      description:  "Dropkloud",
      price:        8.99,
      currency:     "CAD"
    })

    product_item_rule_ca = ProductItemRule.create({
      product_item_id:      product_item_ca.id,
      rule_type:            "inventory",
      rule:                 "price_for_each",
      inventory_type:       "Dropkloud",
      true_false:           false,
      quantity:             0,
      unlimited:            1,
      minimum:              0,
      maximum:              0,
      price:                0,
      currency:             "CAD"
    })

    DropkloudProduct.create({
      product_name: "Dropkloud",
      product_type: "unlimited_monthly",
      term_length: 1,
      product_id: product_ca.id
    })
  end

  def down
    execute("DELETE product_item_rules FROM product_item_rules inner join product_items on product_item_rules.product_item_id = product_items.id where product_items.name = 'Dropkloud';")
    execute("DELETE FROM product_items WHERE name = 'Dropkloud';")
    execute("DELETE FROM products WHERE name = 'Dropkloud';")
  end
end
