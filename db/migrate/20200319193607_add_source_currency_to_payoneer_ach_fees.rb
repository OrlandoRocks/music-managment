class AddSourceCurrencyToPayoneerAchFees < ActiveRecord::Migration[6.0]
  def change
    safety_assured { add_column :payoneer_ach_fees, :source_currency, 'char(3)', default: 'USD'}
  end
end
