class AddPayinProviderConfigToStoredPaypalAccounts < ActiveRecord::Migration[6.0]
  def change
    add_reference :stored_paypal_accounts, :payin_provider_config, index: true
  end
end
