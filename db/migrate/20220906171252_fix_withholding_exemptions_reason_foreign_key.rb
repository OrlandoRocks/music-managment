class FixWithholdingExemptionsReasonForeignKey < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      remove_column :withholding_exemptions, :withholding_exemption_reasons_id, :bigint
      add_reference :withholding_exemptions, :withholding_exemption_reason, index: { name: "withholding_exp_on_withholding_exp_reason" }
    end
  end
end
