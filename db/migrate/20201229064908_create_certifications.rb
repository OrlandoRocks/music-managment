class CreateCertifications < ActiveRecord::Migration[6.0]
  def change
    create_table :certifications do |t|
      t.string :name, null: false
      t.text :description
      t.string :badge_url
      t.string :link
      t.integer :points, null: false
      t.string :category, null: false
      t.boolean :is_active, null: false, default: true

      t.timestamps
    end
  end
end
