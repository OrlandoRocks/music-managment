class CreateOutboundInvoices < ActiveRecord::Migration[6.0]
  def up
    create_table :outbound_invoices do |t|
      t.references :person, null: false
      t.string :invoice_number, null: false
      t.string :invoice_prefix, null: false
      t.integer :related_id, null: false
      t.string  :related_type, null: false
      t.integer :vat_tax_adjustment_id
      t.datetime :invoice_date, null: false
      t.timestamps
    end
  end

  def down
    drop_table :outbound_invoices
  end
end
