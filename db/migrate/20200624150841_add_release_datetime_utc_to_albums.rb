class AddReleaseDatetimeUtcToAlbums < ActiveRecord::Migration[6.0]
  def change
    add_column :albums, :release_datetime_utc, :datetime
  end
end
