class AddIndexToPeopleStatus < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      execute <<~SQL
        ALTER TABLE people
        ADD INDEX people_status_index(status), ALGORITHM=INPLACE, LOCK=NONE;
      SQL
    end
  end
end
