class AddDeliverBookletToStoreDeliveryConfigs < ActiveRecord::Migration[4.2]
  def up
    add_column :store_delivery_configs, :deliver_booklet, :boolean, default: false
  end

  def down
    remove_column :store_delivery_configs, :deliver_booklet
  end
end
