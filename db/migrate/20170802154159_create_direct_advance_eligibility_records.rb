class CreateDirectAdvanceEligibilityRecords < ActiveRecord::Migration[4.2]

  def up
    create_table :direct_advance_eligibility_records do |t|
      t.integer     :person_id
      t.boolean     :eligible
      t.date        :report_run_on
      t.timestamps
    end

    add_index :direct_advance_eligibility_records, :person_id
  end

  def down
    drop_table :direct_advance_eligibility_records
  end

end
