class RemoveIdFromMumaTables < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :muma_songs, :id
    remove_column :muma_song_ips, :id
    remove_column :muma_song_societies, :id
  end

  def self.down
    add_column :muma_songs, :id, :primary_key
    add_column :muma_song_ips, :id, :primary_key
    add_column :muma_song_societies, :id, :primary_key
  end
end
