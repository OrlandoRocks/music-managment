class CreateTrackMonetizationBlockers < ActiveRecord::Migration[4.2]
  def up
    create_table :track_monetization_blockers do |t|
      t.integer :song_id
      t.integer :store_id
      t.timestamps
    end

    add_index :track_monetization_blockers, [:song_id, :store_id]
  end

  def down
    drop_table :track_monetization_blockers
  end
end
