class UpdateUpsellProducts < ActiveRecord::Migration[4.2]
  def self.up
    connection.execute "UPDATE related_products set related_product_id = 18"
  end

  def self.down
    connection.execute "UPDATE related_products set related_product_id = 13"
  end
end
