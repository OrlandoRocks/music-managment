class CreateRoyaltySubStores < ActiveRecord::Migration[6.0]
  def change
    create_table :royalty_sub_stores, comment: "Structure to add sub stores belonging to a royalty store" do |t|
      t.string :name, unique: true, null: false
      t.belongs_to :royalty_store, null: false
      t.decimal :royalty_rate, null: false, comment: "Value between 0 and 1", default: 1, precision: 14, scale: 2
      t.decimal :tariff_rate, null: false, default: 0, precision: 14, scale: 2
      t.column :sales_period_type, "ENUM('monthly', 'quarterly')", null: false, default: "monthly"
      t.column :royalty_source, "ENUM('symphony', 'sip')", null: false, default: "sip"

      t.timestamps
    end

    add_index :royalty_sub_stores, :name, :unique => true
  end
end
