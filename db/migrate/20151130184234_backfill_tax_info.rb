class BackfillTaxInfo < ActiveRecord::Migration[4.2]
  SECRETS = YAML.load_file(File.join(Rails.root, "config", "secrets.yml"))[Rails.env.to_s]

  def up
    TaxInfo.order('id ASC').each do |ti|
      next if ti.encrypted_tax_id.blank?
      p "backfilling record #{ti.id}"
      Rails.logger.info("backfilling record #{ti.id}")
      # decrypt current data with old tax key
      key = EzCrypto::Key.with_password SECRETS["old_tax_key"], ti.salt
      decrypted_str = key.decrypt64(ti.encrypted_tax_id)

      # reassign encrypted tax id and salt with new key using setter
      ti.tax_id = decrypted_str
      Rails.logger.info("Unable to update tax_id for record #{ti.id}") unless ti.save(:validate => false)
    end
  end

  # no down, we can't revert, because salt is generated dynamically!

end
