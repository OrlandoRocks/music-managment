class AddDefaultCmsSets < ActiveRecord::Migration[4.2]
  def up
    CmsSet.create(callout_type: "interstitial", callout_name: "None")
    CmsSet.create(callout_type: "login", callout_name: "None")
    CmsSet.create(callout_type: "dashboard", callout_name: "None")
  end

  def down
    CmsSet.where(callout_name: "None").delete_all
  end
end
