class EnablePlanPurchases < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      change_column :person_plans, :expires_at, :datetime, null: true
    end
  end

  def down
    safety_assured do
      change_column :person_plans, :expires_at, :datetime, null: false
    end
  end
end
