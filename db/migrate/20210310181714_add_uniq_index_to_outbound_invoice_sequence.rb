class AddUniqIndexToOutboundInvoiceSequence < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      add_index :outbound_invoices, %i[user_invoice_prefix invoice_sequence], unique: true, name: 'index_outbound_invoices_on_user_prefix_and_sequence'
    end
  end
end
