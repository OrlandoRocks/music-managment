class RemoveBaseItemContraintsFromProductTables < ActiveRecord::Migration[4.2]
  def up
    change_table :product_items do |t|
      t.change :base_item_id, :integer, null: true
    end
    
    change_table :product_item_rules do |t|
      t.change :base_item_option_id, :integer, null: true
    end
  end

  def down
    change_table :product_items do |t|
      t.change :base_item_id, :integer, null: false
    end
    
    change_table :product_item_rules do |t|
      t.change :base_item_option_id, :integer, null: false
    end
  end
end
