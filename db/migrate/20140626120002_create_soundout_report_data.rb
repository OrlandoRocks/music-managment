class CreateSoundoutReportData < ActiveRecord::Migration[4.2]
  def up
    sql = %Q{ 
    CREATE TABLE `soundout_report_data` (
      `soundout_report_id` int(11) NOT NULL,
      `market_potential` int(11) DEFAULT NULL,
      `market_potential_genre` int(11) DEFAULT NULL,
      `track_rating` float DEFAULT NULL,
      `passion_rating` float DEFAULT NULL,
      `in_genre_class` varchar(255) DEFAULT NULL,
      `report_data` MEDIUMTEXT DEFAULT NULL,
      PRIMARY KEY (`soundout_report_id`),
      KEY `report_id_market_potential` (`soundout_report_id`,`market_potential`),
      KEY `index_soundout_report_data_on_market_potential` (`market_potential`),
      FOREIGN KEY (soundout_report_id) REFERENCES soundout_reports(id) ON DELETE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8}
    ActiveRecord::Base.connection.execute(sql)

    remove_column :soundout_reports, :report_data
  end

  def down
    drop_table :soundout_report_data
    add_column :soundout_reports, :report_data, :text
  end
end
