class CreateLoginAttemptsTable < ActiveRecord::Migration[4.2]
  def self.up
    create_table :login_attempts do |t|
      t.column :ip_address, 'integer unsigned'
      t.integer :count, :default => 0
      t.timestamps
    end
    add_index :login_attempts, :ip_address
  end

  def self.down
    drop_table :login_attempts
  end
end
