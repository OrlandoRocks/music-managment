class CreateFeatureBlacklists < ActiveRecord::Migration[4.2]
  def change
    create_table :feature_blacklists do |t|
      t.integer :person_id
      t.string :feature

      t.timestamps
    end

    add_index :feature_blacklists, [:person_id, :feature]
  end
end
