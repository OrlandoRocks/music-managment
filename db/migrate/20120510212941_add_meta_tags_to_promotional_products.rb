class AddMetaTagsToPromotionalProducts < ActiveRecord::Migration[4.2]
  def self.up
    add_column :promotional_products, :meta_keywords, :text
    add_column :promotional_products, :meta_description, :text
  end

  def self.down
    remove_column :promotional_products, :meta_keywords
    remove_column :promotional_products, :meta_description
  end
end
