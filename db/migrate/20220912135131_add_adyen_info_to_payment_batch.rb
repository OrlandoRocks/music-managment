class AddAdyenInfoToPaymentBatch < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      add_column :payment_batches, :adyen_amount_total, :decimal, precision: 10, scale: 2, after: "braintree_amount_successful", default: 0, null: false, comment: "Total Amount for Adyen in Batch"
      add_column :payment_batches, :adyen_amount_successful, :decimal, precision: 10, scale: 2, after: "adyen_amount_total", default: 0 , null: false, comment: "Total Successfully Paid by Adyen in Batch"
    end
  end
end
