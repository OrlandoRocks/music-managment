class CreateYoutubeSrStore < ActiveRecord::Migration[4.2]
  def up
    store = Store.create(:name => 'Youtube Sound Recording',
      :abbrev => 'ytsr',
      :short_name => 'YoutubeSR',
      :position => 470,
      :needs_rights_assignment => false,
      :is_active => false,
      :base_price_policy_id => "3",
      :is_free => false
    )
  end

  def down
    Store.find_by(short_name: 'YoutubeSR').destroy
  end
end
