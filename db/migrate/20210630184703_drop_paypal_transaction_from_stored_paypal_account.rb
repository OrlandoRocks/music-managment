class DropPaypalTransactionFromStoredPaypalAccount < ActiveRecord::Migration[6.0]
  def change
    safety_assured { remove_column :stored_paypal_accounts, :paypal_transaction_id }
  end
end
