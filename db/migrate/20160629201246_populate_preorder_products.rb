class PopulatePreorderProducts < ActiveRecord::Migration[4.2]
  def up
    products = [
                {name: "iTunes Preorder", add_to_cart_id: 8, purchase_id: 23 },
                {name: "Google Preorder", add_to_cart_id: 9, purchase_id: 24},
                {name: "iTunes and Google Preorder", add_to_cart_id: 10, purchase_id: 25}
               ]

    products.each { |product| PreorderProduct.create!(product_name: product[:name]) }

    products.each do |product|
      preorder_products = PreorderProduct.where(product_name: product[:name])
      preorder_products.each do |preorder_product|
        MarketingEventTrigger.create!(related_id: preorder_product.id, related_type: "PreorderProduct", provider_id: product[:add_to_cart_id], event_trigger: "ADDED TO CART", provider: "Hubspot") if MarketingEventTrigger.where(related_id: preorder_product.id, provider_id: product[:add_to_cart_id]).empty?
        MarketingEventTrigger.create!(related_id: preorder_product.id, related_type: "PreorderProduct", provider_id: product[:purchase_id], event_trigger: "PURCHASED", provider: "Hubspot") if MarketingEventTrigger.where(related_id: preorder_product.id, provider_id: product[:purchase_id]).empty?
      end
    end
  end

  def down
  end
end
