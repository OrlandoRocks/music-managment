class AlterBalanceAdjustmentEnum < ActiveRecord::Migration[4.2]
  def up
    execute("ALTER TABLE balance_adjustments
       CHANGE category category enum('Refund - Renewal','Refund - Other','Service Adjustment','Songwriter Royalty','Other','YouTube MCN Royalty') NULL DEFAULT NULL COMMENT 'Category of adjustment'
      ")
  end

  def down
    execute("ALTER TABLE balance_adjustments
       CHANGE category category enum('Refund - Renewal','Refund - Other','Service Adjustment','Songwriter Royalty','Other') NULL DEFAULT NULL COMMENT 'Category of adjustment'
      ")
  end
end
