class InsertDashboardStoreSaleCalloutFeatureFlag < ActiveRecord::Migration[4.2]
  def up
    Feature.find_or_create_by(:name=>'dashboard_store_sale_callout', :restrict_by_user => 1)
  end

  def down
    feature = Feature.find_by(name: 'dashboard_show_store_sale_callout')
    feature.destroy
  end
end
