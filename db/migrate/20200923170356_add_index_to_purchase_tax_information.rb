class AddIndexToPurchaseTaxInformation < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      add_index(:purchase_tax_information,
                [:vat_registration_number, :customer_location],
                name: 'idx_purchase_tax_info_vat_reg_num_customer_location')
    end
  end
end
