class RenameStoreIngestions < ActiveRecord::Migration[6.0]
  def self.up
    safety_assured do
      rename_table :store_ingestions, :archive_store_ingestions
    end
  end

  def self.down
    safety_assured do
      rename_table :archive_store_ingestions, :store_ingestions
    end
  end
end
