class AddCorrelationIdToPaypalTransfers < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      add_column :paypal_transfers, :correlation_id, :string, null: true
      add_index :paypal_transfers, :correlation_id
    end
  end
end
