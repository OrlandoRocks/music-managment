class AddCountryWebsitesToPeople < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE `people` 
            ADD COLUMN `country_website_id` int(11) unsigned DEFAULT 1 COMMENT 'Foreign Key to country_websites' AFTER id,
            ADD KEY `country_websites_currency` (`country_website_id`);")
  end

  def self.down
    remove_column :people, :country_website_id 
  end
end
