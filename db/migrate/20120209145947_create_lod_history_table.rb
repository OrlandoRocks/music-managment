class CreateLodHistoryTable < ActiveRecord::Migration[4.2]
  def self.up
    create_table :lod_history do |t|
      t.string :status
      t.integer :lod_id
      t.string :updated_by
      t.timestamps
    end
  end

  def self.down
    drop_table :lod_history
  end
end
