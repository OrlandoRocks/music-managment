class UpdateDistroCreditProducts < ActiveRecord::Migration[4.2]
  def self.up
    if !Rails.env.cucumber? && !Rails.env.test?

      begin
        p14 = Product.find(14)
        p14.flag_text = "Save 5%"
        p14.save!

        p18 = Product.find(18)
        p18.description = "Credit to distribute 1 Album whenever your music is ready."
        p18.price = 29.99
        p18.save!

        p19 = Product.find(19)
        p19.description = "Credit to distribute 5 Albums whenever your music is ready."
        p19.price = 134.95
        p19.save!

        p20 = Product.find(20)
        p20.flag_text = "Save 13%"
        p20.description = "Credit to distribute 10 Albums whenever your music is ready."
        p20.price = 260.91
        p20.save!

        p21 = Product.find(21)
        p21.flag_text = "Save 17%"
        p21.description = "Credit to distribute 20 Albums whenever your music is ready."
        p21.price = 497.83
        p21.save!

        p22 = Product.find(22)
        p22.price = 9.99
        p22.save!

        p23 = Product.find(23)
        p23.price = 44.95
        p23.save!

        p24 = Product.find(24)
        p24.flag_text = "Save 13%"
        p24.price = 86.91
        p24.save!

        p25 = Product.find(25)
        p25.flag_text = "Save 17%"
        p25.price = 165.83
        p25.save!

        p73 = Product.find(73)
        p73.flag_text = "Save 5%"
        p73.save!

        p77 = Product.find(77)
        p77.description = "Credit to distribute 1 Album whenever your music is ready."
        p77.price = 29.99
        p77.save!

        p78 = Product.find(78)
        p78.description = "Credit to distribute 5 Albums whenever your music is ready."
        p78.price = 134.95
        p78.save!

        p79 = Product.find(79)
        p79.flag_text = "Save 13%"
        p79.description = "Credit to distribute 10 Albums whenever your music is ready."
        p79.price = 260.91
        p79.save!

        p80 = Product.find(80)
        p80.flag_text = "Save 17%"
        p80.description = "Credit to distribute 20 Albums whenever your music is ready."
        p80.price = 497.83
        p80.save!

        p81 = Product.find(81)
        p81.price = 9.99
        p81.save!

        p82 = Product.find(82)
        p82.price = 44.95
        p82.save!

        p83 = Product.find(83)
        p83.flag_text = "Save 13%"
        p83.price = 86.91
        p83.save!

        p84 = Product.find(84)
        p84.flag_text = "Save 17%"
        p84.price = 165.83
        p84.save!
      rescue StandardError => e
        #Rescue for loading up the db from scratch
      end
    end
  end

  def self.down

  end
end
