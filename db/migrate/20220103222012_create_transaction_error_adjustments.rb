class CreateTransactionErrorAdjustments < ActiveRecord::Migration[6.0]
  def change
    create_table :transaction_error_adjustments do |t|
      t.belongs_to :person, null: false
      t.decimal    :adjusted_debit, default: 0.0, null: false, scale: 14, precision: 23
      t.decimal    :adjusted_credit, default: 0.0, null: false, scale: 14, precision: 23
      t.string     :note, null: false
      t.timestamps
    end
  end
end
