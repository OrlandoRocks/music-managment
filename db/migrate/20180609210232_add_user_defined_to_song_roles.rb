class AddUserDefinedToSongRoles < ActiveRecord::Migration[4.2]
  def change
    add_column :song_roles, :user_defined, :boolean
  end
end
