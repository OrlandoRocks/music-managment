class AddSyncOptedInToComposers < ActiveRecord::Migration[6.0]
  def change
    safety_assured { add_column :composers, :sync_opted_in, :boolean, default: nil }
  end
end
