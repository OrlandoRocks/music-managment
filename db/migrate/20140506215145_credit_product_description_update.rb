class CreditProductDescriptionUpdate < ActiveRecord::Migration[4.2]
  def self.up
    if !Rails.env.cucumber? && !Rails.env.test?

      begin
        products = Product.where("name LIKE '%credit%'")
        products.each do |product|
          product.description += " Cannot be used for renewals."
          product.save!
        end

      rescue StandardError => e
        #Rescue for loading up the db from scratch
      end
    end
  end

  def self.down
  end
end
