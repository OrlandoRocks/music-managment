class CorrectFacebookProductMapping < ActiveRecord::Migration[4.2]
  def up
    mapping = {2 => 320, 4 => 321, 6 => 322}
    [2,4,6].each do |id|
      FacebookRecognitionServiceProduct.find(id).update(product_id: mapping[id])
    end
  end

  def down
  end
end
