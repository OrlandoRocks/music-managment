class UpdateCreativeIndexes < ActiveRecord::Migration[4.2]
  def up
    sql = %Q{
      ALTER TABLE creatives
      DROP INDEX index_creatives_on_creativeable_type,
      DROP INDEX index_creatives_on_creativeable_id,
      DROP INDEX index_creatives_on_artist_id,
      DROP INDEX creatives_record,
      ADD INDEX (creativeable_type,creativeable_id,role,artist_id),
      ADD INDEX (artist_id,creativeable_type,creativeable_id,role),
      ADD INDEX (creativeable_type,creativeable_id,artist_id)
    }
    ActiveRecord::Base.connection.execute(sql)
  end

  def down
    sql = %Q{
      ALTER TABLE creatives
      DROP INDEX creativeable_type,
      DROP INDEX artist_id,
      DROP INDEX creativeable_type_2,
      ADD INDEX index_creatives_on_creativeable_type (creativeable_type),
      ADD INDEX index_creatives_on_creativeable_id (creativeable_id),
      ADD INDEX index_creatives_on_artist_id (artist_id),
      ADD INDEX creatives_record (creativeable_type,creativeable_id)
    }
    ActiveRecord::Base.connection.execute(sql)
  end
end
