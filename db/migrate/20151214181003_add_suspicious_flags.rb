class AddSuspiciousFlags < ActiveRecord::Migration[4.2]
  def change
    add_column :check_transfers, :suspicious_flags, :text
    add_column :eft_batch_transactions, :suspicious_flags, :text
  end
end
