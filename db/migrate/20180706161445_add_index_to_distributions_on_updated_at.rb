class AddIndexToDistributionsOnUpdatedAt < ActiveRecord::Migration[4.2]
  def change
    add_index :distributions, :updated_at
  end
end
