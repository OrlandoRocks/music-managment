class AddComposerRelatedTypeToNotes < ActiveRecord::Migration[4.2]
  def up
    execute "ALTER TABLE notes MODIFY COLUMN related_type enum('Person','Album','Song','EftBatchTransaction', 'SubscriptionEvent', 'Composer') DEFAULT NULL COMMENT 'Type of entity.'"
  end

  def down
    execute "ALTER TABLE notes MODIFY COLUMN related_type enum('Person','Album','Song','EftBatchTransaction', 'SubscriptionEvent') DEFAULT NULL COMMENT 'Type of entity.'"
  end
end
