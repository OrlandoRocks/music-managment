class AlterComposerPersonPubAdminProviderAcctIdColumns < ActiveRecord::Migration[4.2]
  def change 
    change_column_null :composers, :person_id, true
    remove_column :composers, :provider_account_id
    rename_column :composers, :publishing_administrator_id, :account_id
    rename_index :composers, "index_composers_on_publishing_administrator_id", "index_composers_on_account_id"
  end
end
