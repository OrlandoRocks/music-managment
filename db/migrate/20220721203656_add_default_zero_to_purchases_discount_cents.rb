class AddDefaultZeroToPurchasesDiscountCents < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      change_column :purchases, :discount_cents, :integer, null: true, default: 0
    end
  end

  def down
    safety_assured do
      change_column :purchases, :discount_cents, :integer, null: true, default: nil
    end
  end
end
