class AddVatAmountCentsToPurchase < ActiveRecord::Migration[4.2]
  def change
    add_column :purchases, :vat_amount_cents, :int
  end
end
