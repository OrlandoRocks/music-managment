class CreateYoutubePreferences < ActiveRecord::Migration[4.2]
  def self.up
    create_table :youtube_preferences do |t|
      t.integer   :person_id,     :null => false
      t.string    :channel_id,    :null => false
      t.boolean   :whitelist,     :null => false, :default => true
      t.boolean   :mcn,           :null => false, :default => false
      t.datetime  :mcn_agreement
      t.timestamps
    end

    add_index :youtube_preferences, :person_id
  end

  def self.down
    drop_table :youtube_preferences
  end
end
