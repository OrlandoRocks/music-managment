class CreateDisputeStatuses < ActiveRecord::Migration[6.0]
  def change
    create_table :dispute_statuses do |t|
      t.column :source_type, "enum('braintree','paypal','payu')"
      t.string :status
      t.timestamps
    end
  end
end
