class AddClaimantsToCopyrights < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      add_column :copyrights, :person_id, :integer, null: true
      add_column :copyrights, :claimant_has_songwriter_agreement, :boolean, default: false
      add_column :copyrights, :claimant_has_pro_or_cmo, :boolean, default: false
      add_column :copyrights, :claimant_has_tc_publishing, :boolean, default: false
    end
  end
end
