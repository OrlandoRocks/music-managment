class CreateKickbackTransactionsTable < ActiveRecord::Migration[4.2]
  def self.up
    create_table :kickback_transactions do |t|
      t.integer :referrer_id
      t.integer :referee_id
      t.integer :invoice_id
      t.integer :campaign_id
      t.decimal :commission
      t.decimal :balance
      t.string :status
      t.integer :posting_id
      t.integer :posting_type
      t.string :error_message
      t.text :raw_response
      t.timestamps
    end
  end

  def self.down
    drop_table :kickback_transactions
  end
end
