class CreateTierThresholds < ActiveRecord::Migration[6.0]
  def change
    create_table :tier_thresholds do |t|
      t.bigint :tier_id, foreign_key: true
      t.integer :lte_min_amount
      t.integer :min_release
      t.integer :country_id

      t.timestamps
    end
  end
end
