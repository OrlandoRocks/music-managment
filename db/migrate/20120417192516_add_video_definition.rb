class AddVideoDefinition < ActiveRecord::Migration[4.2]
  def self.up
    add_column :videos, :definition, :string, :limit => 2
  end

  def self.down
    remove_column :videos, :definition
  end
end
