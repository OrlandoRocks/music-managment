class CreatePlanDowngradeRequests < ActiveRecord::Migration[6.0]
  def change
    create_table :plan_downgrade_requests do |t|
      t.references :person
      t.integer :requested_plan_id
      t.integer :reason_id
      t.boolean :is_active
      t.string :status
      t.timestamps
    end
  end
end
