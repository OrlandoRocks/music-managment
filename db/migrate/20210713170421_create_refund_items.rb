class CreateRefundItems < ActiveRecord::Migration[6.0]
  def change
    create_table :refund_items do |t|
      t.references :refund, null: false
      t.references :purchase, null: false
      t.integer :base_amount_cents, default: 0
      t.integer :tax_amount_cents, default: 0
      t.integer :vat_cents_in_euro, default: 0
      t.string :currency
      t.boolean :tax_inclusive, default: false
      t.timestamps
    end
  end
end
