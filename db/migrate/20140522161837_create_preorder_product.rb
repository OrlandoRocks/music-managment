class CreatePreorderProduct < ActiveRecord::Migration[4.2]
  def up
    product = Product.create!(
      :created_by_id=>1,
      :country_website_id=>1,
      :name=>"Album Preorder",
      :display_name=>"Preorder",
      :description=>"Make your release available for sale before the street date.",
      :status=>"Active",
      :product_type=>"Ad Hoc",
      :is_default=>1,
      :applies_to_product=>"SalepointPreorderData",
      :sort_order=>1,
      :renewal_level=>"None",
      :price=>25.00,
      :currency=>"USD"
    )
    
    product_item = ProductItem.create!(
      product: product,
      name: "Album Preorder",
      description: "Make your release available for sale before the street date.",
      price: 25.00,
      currency: "USD",
      does_not_renew: 0
    )
    
    ProductItemRule.create!(
      product_item: product_item,
      rule_type: "price_only",
      rule: "price_for_each",
      inventory_type: "SalepointPreorderData",
      quantity: 1,
      :unlimited => 0,
      :true_false => 0,
      :minimum => 0,
      :maximum => 0,
      :price => 25.00,
      :currency => "USD"
    )
    
    product = Product.create!(
      :created_by_id=>1,
      :country_website_id=>2,
      :name=>"Album Preorder",
      :display_name=>"Preorder",
      :description=>"Make your release available for sale before the street date.",
      :status=>"Active",
      :product_type=>"Ad Hoc",
      :is_default=>1,
      :applies_to_product=>"SalepointPreorderData",
      :sort_order=>1,
      :renewal_level=>"None",
      :price=>25.00,
      :currency=>"CAD"
    )
    
    product_item = ProductItem.create!(
      product: product,
      name: "Album Preorder",
      description: "Make your release available for sale before the street date.",
      price: 25.00,
      currency: "USD",
      does_not_renew: 0
    )
    
    ProductItemRule.create!(
      product_item: product_item,
      rule_type: "price_only",
      rule: "price_for_each",
      inventory_type: "SalepointPreorderData",
      quantity: 1,
      :unlimited => 0,
      :true_false => 0,
      :minimum => 0,
      :maximum => 0,
      :price => 25.00,
      :currency => "CAD"
    )
    
  end
  
  def down
    products = Product.where(name: "Album Preorder")
    products += Product.where(name: "Preorder")
    products.each do |product|
      product.product_item_rules.first.destroy
      product.product_items.first.destroy
      product.destroy
    end
  end
end
