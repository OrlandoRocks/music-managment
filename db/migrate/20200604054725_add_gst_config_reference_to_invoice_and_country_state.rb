class AddGstConfigReferenceToInvoiceAndCountryState < ActiveRecord::Migration[6.0]
  def change
    add_belongs_to :country_states, :gst_config
    add_belongs_to :invoices, :gst_config
  end
end
