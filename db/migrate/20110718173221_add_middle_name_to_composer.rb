class AddMiddleNameToComposer < ActiveRecord::Migration[4.2]
  def self.up
    change_table :composers do |t|
      t.string :middle_name, :limit => 100
    end
  end

  def self.down
    change_table :composers do |t|
      t.remove :middle_name
    end
  end
end
