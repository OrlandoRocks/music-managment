class CreateLatestInEachAdyenPaymentMethods < ActiveRecord::Migration[6.0]
  def change
    create_view :latest_in_each_adyen_payment_methods
  end
end
