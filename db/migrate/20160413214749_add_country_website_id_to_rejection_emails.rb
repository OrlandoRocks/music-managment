class AddCountryWebsiteIdToRejectionEmails < ActiveRecord::Migration[4.2]
  def change
    change_table :rejection_emails do |t|
      t.integer :country_website_id
    end
    RejectionEmail.update_all ["country_website_id = ?", 1]
  end
end
