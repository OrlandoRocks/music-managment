class ArchiveSongwriters < ActiveRecord::Migration[6.0]
  def self.up
    if table_exists? :songwriters
      safety_assured do
        rename_table :songwriters, :archive_songwriters
      end
    end
  end

  def self.down
    if table_exists? :archive_songwriters
      safety_assured do
        rename_table :archive_songwriters, :songwriters
      end
    end
  end
end
