class AddCurrencyAndCountryWebsiteToBraintreeTransactions < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE braintree_transactions ADD currency CHAR(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency for invoice item' AFTER amount,
    ADD COLUMN `country_website_id` int(11) unsigned DEFAULT 1 COMMENT 'Foreign Key to country_websites' AFTER id,
    ADD COLUMN `processor_id` varchar(12) DEFAULT NULL COMMENT 'Processor used for transaction (there are multiple due to internationalization)' AFTER currency,
    ADD KEY `country_websites_currency` (`country_website_id`)")
  end

  def self.down
    remove_column :braintree_transactions, :currency
    remove_column :braintree_transactions, :processor_id
    remove_column :braintree_transactions, :country_website_id
  end
end
