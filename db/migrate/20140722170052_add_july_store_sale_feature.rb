class AddJulyStoreSaleFeature < ActiveRecord::Migration[4.2]
  def up
    Feature.find_or_create_by(:name=>'july_store_sale', :restrict_by_user => 1)
  end

  def down
    feature = Feature.find_by(name: 'july_store_sale')
    feature.destroy if !feature.nil?
  end
end
