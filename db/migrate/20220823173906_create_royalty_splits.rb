class CreateRoyaltySplits < ActiveRecord::Migration[6.0]
  def change
    create_table :royalty_splits do |t|
      t.string :title, null: false, comment: "Title of split"
      t.references :owner, comment: "Main Account Holder person_id for split, the only admin",
                           foreign_key: { to_table: :people },
                           type: :integer,
                           unsigned: true,
                           null: false

      t.index [:title, :owner_id], unique: true

      t.timestamps
    end
  end
end
