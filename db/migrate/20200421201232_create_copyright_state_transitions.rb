class CreateCopyrightStateTransitions < ActiveRecord::Migration[6.0]
  def change
    create_table :copyright_state_transitions do |t|
      t.integer :person_id
      t.integer :copyright_status_id
      t.string :from
      t.string :to

      t.timestamps
    end

    add_index :copyright_state_transitions, :person_id
    add_index :copyright_state_transitions, :copyright_status_id
  end
end
