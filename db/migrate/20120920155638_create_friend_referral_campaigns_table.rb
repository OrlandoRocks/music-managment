class CreateFriendReferralCampaignsTable < ActiveRecord::Migration[4.2]
  def self.up
    create_table :friend_referral_campaigns do |t|
      t.integer :person_id
      t.integer :campaign_id
      t.string :campaign_url
      t.boolean :active, :default => 1
      t.timestamps
    end
  end

  def self.down
    drop_table :friend_referral_campaigns
  end
end
