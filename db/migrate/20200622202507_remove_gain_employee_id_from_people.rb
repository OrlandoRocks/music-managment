class RemoveGainEmployeeIdFromPeople < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      remove_column :people, :gain_employee_id
    end
  end

  def down
    add_column :people, :gain_employee_id, :string
  end
end
