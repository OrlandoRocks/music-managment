class CreateCountryWebsitesStores < ActiveRecord::Migration[4.2]
  def self.up
    create_table :country_websites_stores, :id => false do |t|
      t.integer :country_website_id
      t.integer :store_id
    end
    add_index :country_websites_stores, :country_website_id
    add_index :country_websites_stores, :store_id
  end

  def self.down
    drop_table :country_websites_stores
  end
end
