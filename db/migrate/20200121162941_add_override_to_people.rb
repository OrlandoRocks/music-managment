class AddOverrideToPeople < ActiveRecord::Migration[4.2]
  def change
    safety_assured { add_column :people, :override_advance_ineligibility, :boolean, default: false }
  end
end
