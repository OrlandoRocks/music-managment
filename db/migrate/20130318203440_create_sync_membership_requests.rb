class CreateSyncMembershipRequests < ActiveRecord::Migration[4.2]
  def self.up
    create_table :sync_membership_requests do |t|
      t.string :name
      t.string :company
      t.string :email
      t.string :status
      t.string :website
      t.string :request_code
      t.integer :person_id

      t.timestamps
    end
    add_index :sync_membership_requests, :status
    add_index :sync_membership_requests, :person_id
  end

  def self.down
    drop_table :sync_membership_requests
  end
end
