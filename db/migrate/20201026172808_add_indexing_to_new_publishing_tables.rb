class AddIndexingToNewPublishingTables < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      add_index :publishing_composers, :legacy_composer_id
      add_index :publishing_composers, :legacy_cowriter_id
      add_index :publishing_composition_splits, :legacy_publishing_split_id, name: "index_publishing_composition_splits_on_legacy_pub_split_id"
      add_index :publishing_compositions, :legacy_composition_id
    end
  end

  def down
    safety_assured do
      remove_index :publishing_composers, :legacy_composer_id
      remove_index :publishing_composers, :legacy_cowriter_id
      remove_index :publishing_composition_splits, :legacy_publishing_split_id
      remove_index :publishing_compositions, :legacy_composition_id
    end
  end
end
