class AddGstColumnsToStoredCreditCard < ActiveRecord::Migration[6.0]
  def change
    add_belongs_to :stored_credit_cards, :state
    add_belongs_to :stored_credit_cards, :state_city
  end
end
