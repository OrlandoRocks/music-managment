class CreatePlansProductsJoinTable < ActiveRecord::Migration[6.0]
  def change
    create_join_table :plans, :products do |t|
      t.index :plan_id
      t.index :product_id
    end
  end
end
