class RenameDropkloudPurchases < ActiveRecord::Migration[6.0]
  def self.up
    if table_exists? :dropkloud_purchases
      safety_assured do
        rename_table :dropkloud_purchases, :archive_dropkloud_purchases
      end
    end
  end

  def self.down
    if table_exists? :archive_dropkloud_products
      safety_assured do
        rename_table :archive_dropkloud_purchases, :dropkloud_purchases
      end
    end
  end
end
