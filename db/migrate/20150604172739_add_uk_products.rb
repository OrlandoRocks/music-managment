class AddUkProducts < ActiveRecord::Migration[4.2]
  def up
    product_ids = [1, 2, 3, 9, 18, 19, 20, 21, 22, 23, 24, 25, 33, 36, 37, 38, 40, 41, 42, 43,
                   65, 91, 101, 103, 105, 107, 109, 111, 112, 113, 114, 115, 121, 123, 125, 127,
                   133, 134, 135, 139, 140, 143, 145, 147, 149, 151]

    Product.find(product_ids).sort.each do |p|
      uk_product = p.dup
      uk_product.update(country_website_id: 3, currency: "GBP")

      p.product_items.each do |pi|
        uk_product_item = pi.dup
        uk_product_item.update(product_id: uk_product.id, currency: "GBP")

        pi.product_item_rules.each do |pir|
          uk_product_item_rule = pir.dup
          uk_product_item_rule.update(product_item_id: uk_product_item.id, currency: "GBP")
        end
      end
    end
  end

  def down
    ProductItemRule.where(currency: "GBP").delete_all
    ProductItem.where(currency: "GBP").delete_all
    Product.where(currency: "GBP").delete_all
  end
end
