class AddWhyTunecoreConfigs < ActiveRecord::Migration[4.2]
  def self.up
    create_table :why_tunecore_configs do |t|
      t.text    :json
      t.boolean :currently_published, :default=>0
      t.boolean :draft,               :default=>0
    end
    
    json = %Q{
      \{
        "revision_number": 1,
        "filters": [
          \{
            "type": "tag",
            "name": "Quote"
          \}, 
          \{
            "type": "tag",
            "name": "Hero"
          \},
          \{
            "type": "tag",
            "name": "Text"
          \},
          \{
            "type": "tag",
            "name": "New Artists"
          \},
          \{
            "type": "tag",
            "name": "Stars"
          \},
          \{
            "type": "tag",
            "name": "Labels"
          \},
          \{
            "type": "genre",
            "name": "Rock"
          \},
          \{
            "type": "genre",
            "name": "Pop"
          \},
          \{
            "type": "genre",
            "name": "Hip Hop"
          \}, 
          \{
            "type": "genre",
            "name": "Spiritual"
          \}
        ],
        "modified": "Thu Mar 29 2012 19:00:00 GMT-0500 (CDT)",
        "created": "Thu Mar 29 2012 19:00:00 GMT-0500 (CDT)",
        "cards": [
          \{
            "uuid": "f81d4fae-7dec-11d0-a765-00a0c91e6bf1",
            "title": "Weja Priest",
            "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            "img_url": "/images/why_tunecore/WeJaPriest.jpg",
            "genres": ["Hip Hop"],
            "tags": ["Stars", "Hero"]
          \},
          \{
            "uuid": "f81d4fae-7dec-11d0-a765-00a0c91e6bf0",
            "description": "<p>The best Artist Support Team in the music industry</p>",
            "genres": [],
            "tags": ["Text"]
          \},
          \{
            "uuid": "f81d4fae-7dec-11d0-a765-00a0c91e6bf0",
            "description": "<p>The best Quote in the music industry</p>",
            "genres": [],
            "tags": ["Quote"]
          \},
          \{
            "uuid": "f81d4fae-7dec-11d0-a765-00a0c91e6bf0",
            "description": "<p>The longest text that could possibly be existing in the TuneCore textblock in the music industry</p><h4>We are the new music industry</h4>",
            "genres": [],
            "tags": ["Text"]
          \},
          \{
            "uuid": "f81d4fae-7dec-11d0-a765-00a0c91e6bf2",
            "title": "Suburbians",
            "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            "genres": ["Rock"],
            "tags": [],
            "img_url": "/images/why_tunecore/Suburbians.jpg"
          \},
          \{
            "uuid": "f81d4fae-7dec-11d0-a765-00a0c91e6bf3",
            "title": "Real T-Mack",
            "description": "Lorem ipsum dolor sit amet, adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            "genres": ["Hip Hop"],
            "tags": [],
            "img_url": "/images/why_tunecore/Real_TMack.jpg"
          \},   
          \{
            "uuid": "f81d4fae-7dec-11d0-a765-00a0c91e6bf4",
            "title": "Ken",
            "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            "genres": ["Spiritual","Rock"],
            "tags": ["Stars", "Hero"],
            "img_url": "/version/tunecore_neo/images/backgrounds/test_album_cover.jpg"
          \},
          \{
            "uuid": "f81d4fae-7dec-11d0-a765-00a0c91e6bf5",
            "title": "Hoodie Allen",
            "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            "genres": ["Rock"],
            "tags": ["New Artists"],
            "img_url": "/images/why_tunecore/Hoodie_allen.jpg"
          \},
          \{
            "uuid": "f81d4fae-7dec-11d0-a765-00a0c91e6bf6",
            "title": "Chris O&apos;Donnel",
            "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            "genres": ["Rock"],
            "tags": ["Labels"],
            "img_url": "/version/tunecore_neo/images/backgrounds/odonnel.png"
          \}
        ]
      \}
    }
    
    decoded_json = ActiveSupport::JSON.decode(json)
    encoded_json = ActiveSupport::JSON.encode(decoded_json)
    
    sql = "insert into why_tunecore_configs (json,currently_published,draft) values (?,?,?);"
    sql = ActiveRecord::Base.send("sanitize_sql_array", [sql, encoded_json, true, false])
    execute(sql)
  end

  def self.down
    drop_table :why_tunecore_configs
  end
end
