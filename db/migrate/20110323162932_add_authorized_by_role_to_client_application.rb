class AddAuthorizedByRoleToClientApplication < ActiveRecord::Migration[4.2]
  def self.up
    add_column :client_applications, :authorized_by_role, :boolean, :default => false
  end

  def self.down
    remove_column :client_applications, :authorized_by_role
  end
end
