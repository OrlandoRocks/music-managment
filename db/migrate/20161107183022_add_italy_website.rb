class AddItalyWebsite < ActiveRecord::Migration[4.2]
  def up
    CountryWebsite.create(:name=>'TuneCore Italy', :currency=>'EUR', :country => 'IT')
  end

  def down
    CountryWebsite.find_by(country: 'IT').destroy
  end
end
