class AddCodeToPayoutTransfers < ActiveRecord::Migration[4.2]
  def change
    add_column :payout_transfers, :code, :integer
  end
end
