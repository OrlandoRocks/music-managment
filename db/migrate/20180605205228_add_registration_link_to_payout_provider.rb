class AddRegistrationLinkToPayoutProvider < ActiveRecord::Migration[4.2]
  def change
    add_column :payout_providers, :provider_link, :string
  end
end
