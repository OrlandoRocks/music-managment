class AddPlanCreditToCreditUsages < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      add_column :credit_usages, :plan_credit, :boolean, default: false
      add_index :credit_usages, :plan_credit
    end
  end
end
