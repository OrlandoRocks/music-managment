class ChangeNokiaToMixradio < ActiveRecord::Migration[4.2]
  def up
    store = Store.find_by(short_name: "Nokia")
    store.update_attribute(:name, "MixRadio")
  end

  def down
    store = Store.find_by(short_name: "MixRadio")
    store.update_attribute(:name, "Nokia")
  end
end
