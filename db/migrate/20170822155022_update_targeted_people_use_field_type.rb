class UpdateTargetedPeopleUseFieldType < ActiveRecord::Migration[4.2]
  def up
    # at a later date  => remove_column :targeted_people, :used
    add_column :targeted_people, :usage_count, :integer, default: 0
  end

  def down
    remove_column :targeted_people, :usage_count
  end
end
