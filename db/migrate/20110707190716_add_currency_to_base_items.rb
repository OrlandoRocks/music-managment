class AddCurrencyToBaseItems < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE base_items ADD currency CHAR(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency for base item' AFTER price")
  end

  def self.down
    remove_column :base_items, :currency
  end
end
