class CreateCipherData < ActiveRecord::Migration[4.2]
  def change
    create_table :cipher_data do |t|
      t.binary :key, limit: 256
      t.binary :iv, limit: 256
      t.binary :tag, limit: 16

      t.references :cipherable, polymorphic: true

      t.timestamps null: false
    end
  end
end
