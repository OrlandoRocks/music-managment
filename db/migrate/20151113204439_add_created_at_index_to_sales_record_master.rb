class AddCreatedAtIndexToSalesRecordMaster < ActiveRecord::Migration[4.2]
  def change
    execute "create index idx_srm_created_at on sales_record_masters(created_at)"
  end
end
