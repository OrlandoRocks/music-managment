class CreateItunesLanguageTable < ActiveRecord::Migration[4.2]
  def self.up
    create_table :itunes_languages do |t|
      t.string :description
      t.string :code
    end
  end

  def self.down
    drop_table :itunes_languages
  end
end
