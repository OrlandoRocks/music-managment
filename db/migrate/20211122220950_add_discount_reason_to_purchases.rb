class AddDiscountReasonToPurchases < ActiveRecord::Migration[6.0]
  def up
    add_column :purchases, :discount_reason, :string, null: true
    add_index :purchases, :discount_reason
  end

  def down
    remove_column :purchases, :discount_reason
  end
end
