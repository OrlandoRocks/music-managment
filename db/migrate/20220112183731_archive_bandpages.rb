class ArchiveBandpages < ActiveRecord::Migration[6.0]
  def self.up
    if table_exists? :bandpages
      safety_assured do
        rename_table :bandpages, :archive_bandpages
      end
    end
  end

  def self.down
    if table_exists? :archive_bandpages
      safety_assured do
        rename_table :archive_bandpages, :bandpages
      end
    end
  end
end
