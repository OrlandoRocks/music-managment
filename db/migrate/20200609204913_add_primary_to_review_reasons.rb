class AddPrimaryToReviewReasons < ActiveRecord::Migration[6.0]
  def up
    add_column :review_reasons, :primary, :boolean
    change_column_default :review_reasons, :primary, false
  end

  def down
    remove_column :review_reasons, :primary
  end
end
