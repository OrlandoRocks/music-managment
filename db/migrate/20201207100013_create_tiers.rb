class CreateTiers < ActiveRecord::Migration[6.0]
  def change
    create_table :tiers do |t|
      t.string :name, null: false
      t.text :description
      t.string :badge_url
      t.float :hierarchy, null: false, unique: true
      t.boolean :is_active, null: false, default: true
      t.boolean :is_sub_tier, null: false, default: false
      t.integer :parent_id

      t.timestamps
    end
  end
end
