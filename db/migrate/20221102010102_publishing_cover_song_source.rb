class PublishingCoverSongSource < ActiveRecord::Migration[6.0]
  def change
    create_table :publishing_cover_song_source do |t|
      t.string :source_name, limit: 24, null: false
      t.timestamps
    end
  end
end
