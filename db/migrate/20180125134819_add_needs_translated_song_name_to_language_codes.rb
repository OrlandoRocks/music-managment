class AddNeedsTranslatedSongNameToLanguageCodes < ActiveRecord::Migration[4.2]
  def up
    add_column :language_codes, :needs_translated_song_name, :boolean, default: false
  end

  def down
    remove_column :language_codes, :needs_translated_song_name
  end
end
