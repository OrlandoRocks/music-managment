class CreateTcReporterAuditTable < ActiveRecord::Migration[4.2]
  def up
    create_table :tc_reporter_audits do |t|
      t.string    :report,     null: false
      t.integer   :person_id,  null: false
      t.text      :api_params,     null: false
      t.text      :api_response,   null: false
      t.timestamps
    end
  end

  def down
    drop_table :tc_reporter_audits
  end
end
