class AddPausedToStoreDeliveryConfigs < ActiveRecord::Migration[4.2]
  def change
    add_column :store_delivery_configs, :paused, :boolean, default: false
  end
end
