class CreateCmsSets < ActiveRecord::Migration[4.2]
  def change
    create_table :cms_sets do |t|
      t.string :callout_type, null: false, limit: 25
      t.string :callout_name, null: false
      t.datetime :start_tmsp
      t.timestamps
    end
    
    add_index :cms_sets, [:callout_type, :start_tmsp], unique: true
  end
end
