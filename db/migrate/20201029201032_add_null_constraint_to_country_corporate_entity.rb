class AddNullConstraintToCountryCorporateEntity < ActiveRecord::Migration[6.0]
  def up
    data_file_key = ENV.fetch('COUNTRY_ENTITY_MAPPING', 'country_entity_mapping.csv')
    # You must have corporate entities, or
    # THIS WILL BREAK
    # this is a brittle migration
    CorporateEntityMappingService.new(data_file_key).map_to_countries

    safety_assured { change_column :countries, :corporate_entity_id, :bigint, null: false }
  end

  def down
    safety_assured { change_column :countries, :corporate_entity_id, :bigint, null: true }
  end
end
