class CreateGstConfigs < ActiveRecord::Migration[6.0]
  def change
    create_table :gst_configs do |t|
      t.float :cgst
      t.float :sgst
      t.float :igst
      t.datetime :effective_from
      t.timestamps
    end
  end
end
