class AdjustBraintreeTransactionsWithLongerProcessorId < ActiveRecord::Migration[4.2]
    def self.up
      execute("ALTER TABLE braintree_transactions MODIFY COLUMN processor_id varchar(20) DEFAULT NULL COMMENT 'Processor used for transaction (there are multiple due to internationalization)';")
      execute("UPDATE braintree_transactions
               SET processor_id = 'us2creditcard'
               WHERE processor_id = 'us2creditcar';")
    end

    def self.down
      execute("UPDATE braintree_transactions
               SET processor_id = 'us2creditcar'
               WHERE processor_id = 'us2creditcard';")
      execute("ALTER TABLE braintree_transactions MODIFY COLUMN processor_id varchar(12) DEFAULT NULL COMMENT 'Processor used for transaction (there are multiple due to internationalization)';")
    end
  end
