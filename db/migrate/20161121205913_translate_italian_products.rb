#!/bin/env ruby
# encoding: utf-8
class TranslateItalianProducts < ActiveRecord::Migration[4.2]
  def up
    translations = [
      {375 => {name: "Album 1 anno", description: "Il tuo album sarà disponibile per un anno."}},
      {383 => {name: "Suoneria 1 anno", description: "La tua suoneria sarà disponibile per un anno."}},
      {378 => {name: "Singolo 1 anno", description: "Il tuo singolo sarà disponibile per un anno."}},
      {376 => {name: "Album 2 anni", description: "Il tuo album sarà disponibile per due anni."}},
      {392 => {name: "Rinnovo album per 2 anni", description: "Quota di rinnovo per 2 anni per il tuo album."}},
      {379 => {name: "Singolo 2 anni", description: "Il tuo singolo sarà disponibile per due anni."}},
      {393 => {name: "Rinnovo singolo per 2 anni", description: "Quota di rinnovo per 2 anni per il tuo singolo."}},
      {377 => {name: "Album 5 anni", description: "Il tuo album sarà disponibile per cinque anni."}},
      {394 => {name: "Rinnovo album per 5 anni", description: "Quota di rinnovo per 5 anni per il tuo album."}},
      {380 => {name: "Singolo 5 anni", description: "Il tuo singolo sarà disponibile per cinque anni."}},
      {395 => {name: "Rinnovo singolo per 5 anni", description: "Quota di rinnovo per 5 anni per il tuo album."}},
      {366 => {name: "Store aggiunto", description: "Quota per aggiungere uno store a una pubblicazione già in distribuzione."}},
      {367 => {name: "Crediti di distribuzione album", description: "Crediti per la distribuzione di 1 album non appena la tua musica sarà pronta. Non possono essere utilizzati per i rinnovi."}},
      {369 => {name: "Crediti di distribuzione album - Pacchetto da 10", description: "Crediti per la distribuzione di 10 album non appena la tua musica sarà pronta. Non possono essere utilizzati per i rinnovi."}},
      {370 => {name: "Crediti di distribuzione album - Pacchetto da 20", description: "Crediti per la distribuzione di 20 album non appena la tua musica sarà pronta. Non possono essere utilizzati per i rinnovi."}},
      {368 => {name: "Crediti di distribuzione album - Pacchetto da 5", description: "Crediti per la distribuzione di 5 album non appena la tua musica sarà pronta. Non possono essere utilizzati per i rinnovi."}},
      {386 => {name: "Booklet", description: "Distribuisci booklet"}},
      {399 => {name: "Raccogli i tuoi proventi con il servizio YouTube Sound Recording", description: "TuneCore invierà le tue tracce a YouTube, raccoglierà i proventi maturati dalle tue registrazioni audio e depositerà il denaro sul tuo account TuneCore."}},
      {385 => {name: "Utilizzo credito", description: "I crediti di distribuzione non possono essere usati per i rinnovi."}},
      {401 => {name: "LANDR Instant Mastering", description: "Mastering istantaneo di una traccia. Nota: devi completare l'acquisto entro sei ore. In caso contrario il tuo ordine 401 verrà eliminato."}},
      {400 => {name: "Pre-ordine", description: "Rendi disponibile la tua pubblicazione prima della data di uscita nei punti vendita."}},
      {387 => {name: "Crediti di distribuzione suonerie", description: "La tua suoneria nello store iPhone. Distribuisci quando vuoi. Non possono essere utilizzati per i rinnovi."}},
      {390 => {name: "Crediti di distribuzione suonerie - Pacchetto da 10", description: "La tua suoneria nello store iPhone. Distribuisci quando vuoi. Non possono essere utilizzati per i rinnovi."}},
      {391 => {name: "Crediti di distribuzione suonerie - Pacchetto da 20", description: "La tua suoneria nello store iPhone. Distribuisci quando vuoi. Non possono essere utilizzati per i rinnovi."}},
      {388 => {name: "Crediti di distribuzione suonerie - Pacchetto da 3", description: "La tua suoneria nello store iPhone. Distribuisci quando vuoi. Non possono essere utilizzati per i rinnovi.  "}},
      {389 => {name: "Crediti di distribuzione suonerie - Pacchetto da 5", description: "La tua suoneria nello store iPhone. Distribuisci quando vuoi. Non possono essere utilizzati per i rinnovi."}},
      {382 => {name: "Rinnovo suonerie", description: "Quota annuale di rinnovo per la tua suoneria."}},
      {371 => {name: "Crediti di distribuzione singoli", description: "Crediti per la distribuzione di 1 singolo non appena la tua musica sarà pronta. Non possono essere utilizzati per i rinnovi."}},
      {373 => {name: "Crediti di distribuzione singoli - Pacchetto da 10", description: "Crediti per la distribuzione di 10 singoli non appena la tua musica sarà pronta. Non può essere utilizzato per i rinnovi."}},
      {374 => {name: "Crediti di distribuzione singoli - Pacchetto da 20", description: "Crediti per la distribuzione di 20 singoli non appena la tua musica sarà pronta. Non possono essere utilizzati per i rinnovi."}},
      {372 => {name: "Crediti di distribuzione singoli - Pacchetto da 5", description: "Crediti per la distribuzione di 5 singoli non appena la tua musica sarà pronta. Non possono essere utilizzati per i rinnovi."}},
      {365 => {name: "Rinnovo singolo", description:  "Quota annuale di rinnovo per il tuo singolo."}},
      {381 => {name: "Servizio per gli autori", description: "Raccogli, ottieni le licenze e controlla i tuoi copyright come autore"}},
      {384 => {name: "Store Automator", description: "La pubblicazione sarà distribuita in automatico presso i nuovi store"}},
      {397 => {name: "TuneCore Fan Reviews Enhanced", description: "Ottieni recensioni dai fan e utilissime analisi dei dati per il tuo brano."}},
      {398 => {name: "TuneCore Fan Reviews Premium", description: "Ottieni recensioni dai fan e utilissime analisi dei dati per il tuo brano."}},
      {396 => {name: "TuneCore Fan Reviews Starter", description: "Ottieni recensioni dai fan e utilissime analisi dei dati per il tuo brano."}},
      {364 => {name: "Rinnovo album annuale", description: "Quota annuale di rinnovo per il tuo album."}}
    ]

    translations.each do |record|
      record.each { |product_id, data| Product.find(product_id).update!(data)}
    end
  end

  def down
  end
end
