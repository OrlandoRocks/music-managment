class UpdateCharSetOfTraderNameForVatAudits < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      execute %(
        ALTER TABLE vat_information_audits
          MODIFY COLUMN trader_name VARCHAR(255)
             CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
          MODIFY COLUMN company_name VARCHAR(255)
             CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;
      )
    end
  end

  def down
    safety_assured do
      execute %(
        ALTER TABLE vat_information_audits
          MODIFY COLUMN trader_name VARCHAR(255)
            CHARACTER SET utf8,
          MODIFY COLUMN company_name VARCHAR(255)
            CHARACTER SET utf8;
      )
    end
  end
end
