class AddRoyaltyStoreIdToSipStores < ActiveRecord::Migration[6.0]
  def change
    add_column :sip_stores, :royalty_store_id, :integer, comment: "linking to the new stores structure."
  end
end
