class AddManifestFlagToStoreDeliveryConfigs < ActiveRecord::Migration[4.2]
  def change
    add_column :store_delivery_configs, :use_manifest, :boolean, default: false
  end
end
