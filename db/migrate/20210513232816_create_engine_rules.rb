class CreateEngineRules < ActiveRecord::Migration[6.0]
  def change
    create_table :engine_rules do |t|
      t.string :category
      t.string :name
      t.longtext :config
      t.boolean :enabled, default: false

      t.timestamps
    end
    add_index :engine_rules, [:category, :name], unique: true
  end
end
