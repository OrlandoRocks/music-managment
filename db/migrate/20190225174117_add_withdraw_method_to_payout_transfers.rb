class AddWithdrawMethodToPayoutTransfers < ActiveRecord::Migration[4.2]
  def change
    add_column :payout_transfers, :withdraw_method, :string
  end

end
