class AddPaypalToPersonPreferences < ActiveRecord::Migration[4.2]
  def self.up
    execute %Q(ALTER TABLE person_preferences
              ADD COLUMN preferred_paypal_account_id int(11) UNSIGNED DEFAULT NULL AFTER `preferred_credit_card_id`,   
              ADD COLUMN preferred_payment_type enum('PayPal', 'CreditCard') NOT NULL DEFAULT 'CreditCard' COMMENT 'which payment type to use' AFTER `preferred_paypal_account_id`,
              ADD COLUMN do_not_autorenew TINYINT(3) DEFAULT 0 NOT NULL COMMENT 'Flag to allow customer to opt out of auto-renewals' AFTER `preferred_payment_type`,
              ADD CONSTRAINT `FK_p_pref_paypal_account_id` FOREIGN KEY (`preferred_paypal_account_id`) REFERENCES `stored_paypal_accounts`(`id`))
  end

  def self.down
    execute("ALTER TABLE person_preferences DROP FOREIGN KEY `FK_p_pref_paypal_account_id`")
    remove_column :person_preferences, :preferred_paypal_account_id
    remove_column :person_preferences, :preferred_payment_type
    remove_column :person_preferences, :do_not_autorenew
  end
end
