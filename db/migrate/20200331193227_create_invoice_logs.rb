class CreateInvoiceLogs < ActiveRecord::Migration[6.0]
  def change
    create_table :invoice_logs do |t|
      t.integer  :person_id
      t.integer  :batch_transaction_id
      t.integer  :payment_batch_id
      t.integer  :braintree_transaction_id
      t.integer  :paypal_transaction_id
      t.integer  :stored_paypal_account_id
      t.integer  :renewal_id
      t.string   :current_method_name
      t.string   :caller_method_path
      t.string   :message
      t.integer  :invoice_id
      t.integer  :purchase_id
      t.datetime :invoice_created_at
      t.integer  :invoice_final_settlement_amount_cents, default: nil
      t.column   :invoice_currency, 'char(3)'
      t.column   :invoice_batch_status, "enum('visible_to_customer','waiting_for_batch','processing_in_batch')"
      t.datetime :invoice_settled_at
      t.integer  :invoice_vat_amount_cents
      t.integer  :invoice_foreign_exchange_pegged_rate_id
      t.datetime :purchase_created_at
      t.integer  :purchase_product_id
      t.integer  :purchase_cost_cents
      t.integer  :purchase_discount_cents
      t.column   :purchase_currency, 'char(3)'
      t.datetime :purchase_paid_at
      t.integer  :purchase_salepoint_id
      t.integer  :purchase_no_purchase_album_id
      t.integer  :purchase_related_id
      t.string   :purchase_related_type
      t.integer  :purchase_targeted_product_id
      t.integer  :purchase_price_adjustment_histories_count
      t.integer  :purchase_vat_amount_cents
      t.string   :purchase_status

      t.timestamps
    end

    add_index :invoice_logs, :person_id
    add_index :invoice_logs, :invoice_id
    add_index :invoice_logs, :purchase_id
    add_index :invoice_logs, [:purchase_related_id, :purchase_related_type], name: 'purchase_related_on_related_type'
    add_index :invoice_logs, :current_method_name
    add_index :invoice_logs, :caller_method_path
  end
end
