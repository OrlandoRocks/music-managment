class ChangeMusicKeyToArtTracks < ActiveRecord::Migration[4.2]
  def up
    store = Store.find_by(short_name: "YTMusic")
    store.update(name: "YouTube Art Tracks")
  end

  def down
    store = Store.find_by(short_name: "YTMusic")
    store.update(name: "YouTube Music Key")
  end
end
