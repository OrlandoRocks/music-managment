class CreatePayuInvoiceUploadResponses < ActiveRecord::Migration[6.0]
  def change
    create_table :payu_invoice_upload_responses do |t|
      t.references :payments_os_transaction,
                   null: false,
                   index: { name: "index_upload_responses_on_payments_os_transaction_id" }
      t.string :api_response_code, null: false
      t.string :api_message, null: false

      t.timestamps
    end
    add_index :payu_invoice_upload_responses, :api_response_code
  end
end
