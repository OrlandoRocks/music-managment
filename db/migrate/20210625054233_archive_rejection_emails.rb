class ArchiveRejectionEmails < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      rename_table :rejection_emails, :archives_rejection_emails
    end
  end
end
