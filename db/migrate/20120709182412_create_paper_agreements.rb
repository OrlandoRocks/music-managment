class CreatePaperAgreements < ActiveRecord::Migration[4.2]
  def self.up
    create_table :paper_agreements do |t|
      t.string :related_type
      t.integer :related_id
      t.timestamps
    end
  end

  def self.down
    drop_table :paper_agreements
  end
end
