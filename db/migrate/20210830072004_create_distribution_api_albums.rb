class CreateDistributionApiAlbums < ActiveRecord::Migration[6.0]
  def up
    create_table :distribution_api_albums do |t|
      t.references :distribution_api_service, foreign_key: true, null: false, type: :bigint
      t.references :album, index: { unique: true }, foreign_key: true, null: false, type: :integer
      t.string :source_album_id, null: false
      t.string :source_user_id, null: false
      t.text :source_contentreview_note
      t.timestamps
    end

    add_index :distribution_api_albums, [:distribution_api_service_id, :source_album_id], unique: true, name: 'index_distribution_api_albums_on_source_album_id_and_service_id'
  end

  def down
    drop_table :distribution_api_albums
  end
end
