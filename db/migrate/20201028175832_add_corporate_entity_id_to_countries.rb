class AddCorporateEntityIdToCountries < ActiveRecord::Migration[6.0]
  def change
    safety_assured { add_reference :countries, :corporate_entity, index: true }
  end
end
