class AddUpdatedByToSalepointSongs < ActiveRecord::Migration[4.2]
  def change
  	add_column :salepoint_songs, :updated_by, :integer
  end
end
