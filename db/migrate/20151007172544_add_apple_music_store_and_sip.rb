class AddAppleMusicStoreAndSip < ActiveRecord::Migration[4.2]
  def up
    store = Store.create(
      name:                     "Apple Music",
      abbrev:                   "ap",
      short_name:               "AppleMusic",
      position:                 1100,
      needs_rights_assignment:  false,
      is_active:                false,
      base_price_policy_id:     "0",
      is_free:                  false,
      in_use_flag:              false
    )
    if store && store.errors.empty?
      Rails.logger.info("Apple Music Store Created Successfully")
      create_sip_mappings(store.id) ? Rails.logger.info("Apple Music Store Sip Mappings Created Successfully") : Rails.logger.info("Apple Music Store Sip Mappings Failed")
    else
      Rails.logger.error("FAILED to create Apple Music Store")
    end
  end

  def down
    execute "DELETE FROM sip_stores WHERE NAME LIKE 'Apple Music%'"
    display_group = SipStoresDisplayGroup.find_by(name: "Apple Music")
    Rails.logger.error("Unable to destroy Apple Music sip store") unless display_group.destroy
    apple_music_store = Store.find_by(name: "Apple Music")
    Rails.logger.error("Unable to delete apple music store. Check DB") unless apple_music_store.destroy
  end

  def create_sip_mappings(store_id)
    sip_display_group = SipStoresDisplayGroup.create({
      name: "Apple Music",
      sort_order: 140,
      group_on_status: true})
    values = ["276,'Apple Music AU'","277,'Apple Music CA'", "278,'Apple Music CH'", "279,'Apple Music DK'","280,'Apple Music EU'","281,'Apple Music GB'","282,'Apple Music HK'","283,'Apple Music ID'","284,'Apple Music IN'","285,'Apple Music JP'","286,'Apple Music MX'","287,'Apple Music NO'","288,'Apple Music NZ'","289,'Apple Music RU'","290,'Apple Music SE'","291,'Apple Music SG'","292,'Apple Music US'","293,'Apple Music WW'","294,'Apple Music ZA'"]

    insert_sql = "INSERT INTO sip_stores (id, name, store_id, store_id_old, display_on_status_page, display_group_id, display_group_id_old) VALUES "

    values.each_with_index do |v, i|
      insert_sql += "(#{v.split(',')[0]},#{v.split(',')[1]},#{store_id},#{store_id},1,#{sip_display_group.id},#{sip_display_group.id})"
      values.size - 1 == i ?  insert_sql += ";" : insert_sql += ","
    end
    execute (insert_sql)
  end
end
