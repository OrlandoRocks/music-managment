class CreateNonTunecoreAccounts < ActiveRecord::Migration[4.2]
  def change
    create_table :non_tunecore_accounts do |t|
      t.string :name, null: false
      t.string :email, limit: 128
      t.string :status
      t.string :account_type
      t.string :provider_account_id
      t.string :provider_user_id
      t.timestamps
    end
  end
end
