class CreateTwitchUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :twitch_users do |t|
      t.references :people
      t.string :twitch_id
      t.string :twitch_email, index: true
      t.string :token
      t.timestamps
    end
  end
end
