class AddAppleIdToPerson < ActiveRecord::Migration[4.2]
  def change
    add_column :people, :apple_id, :string
  end
end
