class RemoveCwrNameFromPublishingCompositions < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      remove_column :publishing_compositions, :cwr_name
    end
  end

  def down
    safety_assured do
      add_column :publishing_compositions, :cwr_name, :string
    end
  end
end
