class IncreasePasswordColumnLengths < ActiveRecord::Migration[4.2]
  def change
    change_table :people do |t|
      t.change :salt, :string
      t.change :password, :string
    end
  end
end
