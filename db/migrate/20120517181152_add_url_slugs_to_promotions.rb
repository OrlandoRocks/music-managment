class AddUrlSlugsToPromotions < ActiveRecord::Migration[4.2]
  def self.up
    add_column :promotions, :url_slug, :string
  end

  def self.down
    remove_column :promotions, :url_slug
  end
end
