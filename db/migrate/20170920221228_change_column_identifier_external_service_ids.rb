class ChangeColumnIdentifierExternalServiceIds < ActiveRecord::Migration[4.2]
  def up
    change_column :external_service_ids, :identifier, :string, null: true
  end

  def down
    change_column :external_service_ids, :identifier, :string
  end
end
