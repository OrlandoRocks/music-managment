class ReorderStores < ActiveRecord::Migration[4.2]
  def self.up
    # 1.    iTunes
    # 2.    Spotify
    # 3.    AmazonMP3
    # 4.    Google Play
    # 5.    Rdio
    # 6.    Deezer
    # 7.    X-Box Live (formerly Zune) --> need to rename
    # 8.    Rhapsody
    # 9.    eMusic
    # 10.    Simfy
    # 11.    Muve Music
    # 12.    Myspace Music
    # 13.    iHeart Radio
    # 14.    Nokia
    # 15.    MediaNet
    # 16.    VerveLife
    # 17.    AOD

    Store.transaction do

      #Ordered stored ids
      store_ids = [36,26,13,28,35,32,24,7,10,31,33,23,25,21,8,12]

      store_ids.each_with_index do |id,i|
        Store.find(id).update({:position=>(i+1)*10})
      end

      #Reorder old regional salepoints
      store_ids = [1,2,3,4,5,6,22,29,34]
      store_ids.each_with_index do |id, i|
        store = Store.find(id)
        store.position = 10+i+1
        store.save(false)
      end

      #Ensure AOD is at the end of the list
      aod = Store.find(19)
      aod.position = 1000
      aod.save(false)
    end


  rescue StandardError => e
  end

  def self.down
  end
end
