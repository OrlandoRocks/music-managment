class AddRingtoneJapanSalepoint < ActiveRecord::Migration[4.2]
  def self.up
    begin
      Store.find(5)
      SalepointableStore.create(:salepointable_type => 'Ringtone', :store_id => 5)
    rescue ActiveRecord::RecordNotFound
    end
  end

  def self.down
    ss = SalepointableStore.find_by(salepointable_type: 'Ringtone', store_id: 5)
    if ss != nil
      ss.destroy
    end
  end
end
