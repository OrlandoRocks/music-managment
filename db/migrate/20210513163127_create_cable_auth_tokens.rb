class CreateCableAuthTokens < ActiveRecord::Migration[6.0]
  def change
    create_table :cable_auth_tokens do |t|
      t.belongs_to :person
      t.datetime   :expires_at
      t.string     :token     
      t.timestamps
    end
  end
end
