class AddIndexOnKeyToApiKeys < ActiveRecord::Migration[4.2]
  def change
    add_index :api_keys, :key
  end
end
