class AddWriterIdAndWriterTypeToPublishingSplits < ActiveRecord::Migration[4.2]
  def change
    add_column :publishing_splits, :writer_id, :integer
    add_column :publishing_splits, :writer_type, :string

    add_index :publishing_splits, [:writer_id, :writer_type]
  end
end
