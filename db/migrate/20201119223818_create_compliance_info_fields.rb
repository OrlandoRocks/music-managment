class CreateComplianceInfoFields < ActiveRecord::Migration[6.0]
  def change
    create_table :compliance_info_fields do |t|
      t.integer :person_id
      t.string :field_name
      t.text :field_value

      t.timestamps
    end

    add_index :compliance_info_fields, [:person_id, :field_name], unique: true
  end
end
