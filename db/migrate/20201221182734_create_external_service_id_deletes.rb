class CreateExternalServiceIdDeletes < ActiveRecord::Migration[6.0]
  def change
    create_table :external_service_id_deletes do |t|
      t.string :identifier
      t.string :service_name
      t.integer :person_id
      t.string :artist_name

      t.timestamps
    end
  end
end
