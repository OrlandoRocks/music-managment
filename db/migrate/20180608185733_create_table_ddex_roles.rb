class CreateTableDdexRoles < ActiveRecord::Migration[4.2]
  def change
    create_table :ddex_roles do |t|
      t.string :role_type
      t.boolean :resource_contributor_role
      t.boolean :indirect_contributor_role
    end
  end
end
