class AddShowDiscountPriceToTargetedProduct < ActiveRecord::Migration[4.2]
  def up
    add_column :targeted_products, :show_discount_price, :boolean, default: true
  end

  def down
    remove_column :targeted_products, :show_discount_price
  end
end
