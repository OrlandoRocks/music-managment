class AddIsMatchedToScrapiJobs < ActiveRecord::Migration[6.0]
  def change
    add_column :scrapi_jobs, :is_matched, :boolean
  end
end
