class CaseSensitiveLabelNames < ActiveRecord::Migration[4.2]
  def up
    execute "ALTER TABLE labels MODIFY `name` varchar(120) CHARACTER SET utf8 COLLATE utf8_bin"
  end

  def down
    execute "ALTER TABLE labels MODIFY `name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci"
  end
end
