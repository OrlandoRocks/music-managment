class AdjustYearlyRenewalProduct < ActiveRecord::Migration[4.2]
  def self.up
    #editing the CA renewal products to apply to Album/Single/Ringtone, since they incorrectly apply to 'None'
    album_renewal = Product.where(:display_name=>'Album Renewal', :country_website_id=>2).first
    album_renewal.update(:applies_to_product => 'Album', :name=>"Yearly Album Renewal", :display_name => "Yearly Album Renewal") if !album_renewal.nil?
    # ringtone renewal
    ringtone_renewal = Product.where(:display_name=>'Ringtone Renewal', :country_website_id=>2).first
    ringtone_renewal.update(:applies_to_product => 'Ringtone') if !ringtone_renewal.nil?
    # single renewal
    single_renewal = Product.where(:display_name=>'Single Renewal', :country_website_id=>2).first
    single_renewal.update(:applies_to_product => 'Single') if !single_renewal.nil?
  end

  def self.down
    #rollback to incorrect state of 'None'
    album_renewal = Product.where(:display_name=>'Yearly Album Renewal', :country_website_id=>2).first
    album_renewal.update(:applies_to_product => 'None', :display_name => "Album Renewal") if !album_renewal.nil?
    # ringtone renewal
    ringtone_renewal = Product.where(:display_name=>'Ringtone Renewal', :country_website_id=>2).first
    ringtone_renewal.update(:applies_to_product => 'None') if !ringtone_renewal.nil?
    # single renewal
    single_renewal = Product.where(:display_name=>'Single Renewal', :country_website_id=>2).first
    single_renewal.update(:applies_to_product => 'None') if !single_renewal.nil?
  end
end
