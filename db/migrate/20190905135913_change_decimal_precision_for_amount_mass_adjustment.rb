class ChangeDecimalPrecisionForAmountMassAdjustment < ActiveRecord::Migration[4.2]
  def change
    change_column :mass_adjustment_entries, :amount, :decimal, precision: 23, scale: 14
  end
end
