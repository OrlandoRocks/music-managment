class AddTicketboothUrlToSyncLicenseRequests < ActiveRecord::Migration[4.2]
  def self.up
    add_column :sync_license_requests, :ticketbooth_url, :string
  end

  def self.down
    remove_column :sync_license_requests, :ticketbooth_url
  end
end
