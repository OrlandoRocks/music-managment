class AddItalianProducts < ActiveRecord::Migration[4.2]
  def up
    product_ids = {
      "1"   => 364, # Yearly Album Renewal
      "3"   => 365, # Single Renewal
      "9"   => 366, # Added Store
      "18"  => 367, # Album Distribution Credit
      "19"  => 368, # Album Distribution Credit - 5 Pack
      "20"  => 369, # Album Distribution Credit - 10 Pack
      "21"  => 370, # Album Distribution Credit - 20 Pack
      "22"  => 371, # Single Distribution Credit
      "23"  => 372, # Single Distribution Credit - 5 Pack
      "24"  => 373, # Single Distribution Credit - 10 Pack
      "25"  => 374, # Single Distribution Credit - 20 Pack
      "36"  => 375, # 1 Year Album
      "37"  => 376, # 2 Year Album
      "38"  => 377, # 5 Year Album
      "41"  => 378, # 1 Year Single
      "42"  => 379, # 2 Year Single
      "43"  => 380, # 5 Year Single
      "65"  => 381, # Songwriter Service
      "101" => 382, # Ringtone Renewal
      "103" => 383, # 1 Year Ringtone
      "105" => 384, # Store Automator
      "107" => 385, # Credit Usage
      "109" => 386, # Booklet
      "111" => 387, # Ringtone Distribution
      "112" => 388, # Ringtone Distribution Credit - 3 Pack
      "113" => 389, # Ringtone Distribution Credit - 5 Pack
      "114" => 390, # Ringtone Distribution Credit - 10 Pack
      "115" => 391, # Ringtone Distribution Credit - 20 Pack
      "121" => 392, # 2 Year Album Renewal
      "123" => 393, # 2 Year Single Renewal
      "125" => 394, # 5 Year Album Renewal
      "127" => 395, # 5 Year Single Renewal
      "133" => 396, # TuneCore Fan Reviews Starter
      "134" => 397, # TuneCore Fan Reviews Enhanced
      "135" => 398, # TuneCore Fan Reviews Premium
      "145" => 399, # Collect Your YouTube Sound Recording Revenue
      "147" => 400, # Preorder
      "149" => 401  # LANDR Instant Mastering
    }

    # use ids from above based on name of product
    renewal_product_ids = {
      "336" => 364, # 1 Year Album
      "337" => 364, # 2 Year Album
      "338" => 364, # 5 Year Album
      "328" => 364, # Album Distribution Credit
      "329" => 364, # Album Distribution Credit - 5 Pack
      "330" => 364, # Album Distribution Credit - 10 Pack
      "331" => 364, # Album Distribution Credit - 20 Pack
      "332" => 365, # Single Distribution Credit
      "333" => 365, # Single Distribution Credit - 5 Pack
      "334" => 365, # Single Distribution Credit - 10 Pack
      "335" => 365, # Single Distribution Credit - 20 Pack
      "339" => 365, # 1 Year Single
      "340" => 365, # 2 Year Single
      "341" => 365, # 5 Year Single
      "344" => 382, # 1 Year Ringtone
      "348" => 382, # Ringtone Distribution
      "349" => 382, # Ringtone Distribution Credit - 3 Pack"9"
      "350" => 382, # Ringtone Distribution Credit - 5 Pack
      "351" => 382, # Ringtone Distribution Credit - 10 Pack
      "352" => 382, # Ringtone Distribution Credit - 20 Pack
    }

    product_ids.each do |us_id, italian_id|
      p = Product.find(us_id)
      italian_product = p.dup
      italian_product.id = italian_id
      italian_product.country_website_id = 7
      italian_product.currency = "EUR"
      italian_product.created_by = Person.first
      italian_product.save!

      p.product_items.each do |pi|
        italian_product_item = pi.dup
        italian_product_item.update!(
          product_id: italian_product.id,
          currency: "EUR",
          renewal_product_id: renewal_product_ids[italian_product.id.to_s]
        )

        pi.product_item_rules.each do |pir|
          italian_product_item_rule = pir.dup
          italian_product_item_rule.update(
            product_item_id: italian_product_item.id,
            currency: "EUR"
          )
        end
      end
    end

    SoundoutProduct.create(report_type: "starter", display_name: "TrackSmarts Starter Report", number_of_reviews: 40, available_in_days: 5, product: Product.find(396))
    SoundoutProduct.create(report_type: "enhanced", display_name: "TrackSmarts Enhanced Report", number_of_reviews: 100, available_in_days: 5, product: Product.find(397))
    SoundoutProduct.create(report_type: "premium", display_name: "TrackSmarts Premium Report", number_of_reviews: 225, available_in_days: 5, product: Product.find(398))
  end

  def down
    product_ids = [364, 365, 366, 367, 368, 369,
      370, 371, 372, 373, 374, 375, 376, 377, 378,
      379, 380, 381, 382, 383, 384, 385, 386, 387,
      388, 389, 390, 391, 392, 393, 394, 395, 396,
      397, 398, 399, 400, 401]

    product_ids.each do |p_id|
      Product.find(p_id).destroy
    end
  end
end
