class UpdateSalepointsWithoutType < ActiveRecord::Migration[4.2]
  def up
    spat = Salepoint.arel_table
    salepoints = Salepoint.where(spat[:store_id].in([18,75]))
      .where(spat[:finalized_at].gt(1.year.ago))
      .where(spat[:salepointable_type].eq(nil))

    salepoints.update_all(salepointable_type: "Album")
  end

  def down
  end
end
