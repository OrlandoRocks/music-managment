class AddCustomerTypeAndExchangeRateToVatTaxAdjustments < ActiveRecord::Migration[6.0]
  def up
    add_column :vat_tax_adjustments, :customer_type, :string
    add_column :vat_tax_adjustments, :vat_amount_in_eur, :decimal, precision: 23, scale: 14
    if table_exists?(:foreign_exchange_balance_rate)
      add_reference :vat_tax_adjustments, :foreign_exchange_balance_rate, foreign_key: true
    end
  end

  def down
    remove_column :vat_tax_adjustments, :customer_type
    remove_column :vat_tax_adjustments, :vat_amount_in_eur
    if table_exists?(:foreign_exchange_balance_rate)
      remove_reference :vat_tax_adjustments, :foreign_exchange_balance_rate
    end
  end
end
