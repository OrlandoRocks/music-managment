class UpdateAppliesToProductOnSalepointSubscriptionProducts < ActiveRecord::Migration[4.2]
  def self.up
    execute <<-SQL
      ALTER TABLE products
        MODIFY COLUMN applies_to_product enum('Album','Single','Ringtone','Video','None','Widget','CreditUsage','Booklet','SalepointSubscription') NOT NULL DEFAULT 'None'
    SQL

    if !(Rails.env.cucumber? || Rails.env.test?)
      products = Product.where("name = ?", "Store Automator")

      products.each do |product|
        product.applies_to_product = 'SalepointSubscription'
        product.save!
      end
    end
  end

  def self.down

    if !(Rails.env.cucumber? || Rails.env.test?)
      products = Product.where("name = ?", "Store Automator")

      products.each do |product|
        product.applies_to_product = 'None'
        product.save!
      end
    end
  end
end
