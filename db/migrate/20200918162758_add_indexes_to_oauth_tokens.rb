class AddIndexesToOauthTokens < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
       add_index :oauth_tokens, [:user_id, :client_application_id]
    end
  end

  def down
    safety_assured do
       remove_index :oauth_tokens, [:user_id, :client_application_id]
    end
  end
end
