class IndexEligibilityStatusForTrackMonetization < ActiveRecord::Migration[4.2]
  def change
    add_index :track_monetizations, :eligibility_status, using: :btree, length: 1
  end
end
