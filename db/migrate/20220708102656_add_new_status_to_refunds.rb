class AddNewStatusToRefunds < ActiveRecord::Migration[6.0]
  def change
    reversible do |dir|
      dir.up do
        safety_assured do
          execute <<-SQL
            SET @old_alter_algorithm = @@alter_algorithm;
          SQL

          execute <<-SQL
            SET SESSION alter_algorithm='INSTANT';
          SQL

          execute <<-SQL
            ALTER TABLE `refunds`
            MODIFY COLUMN status ENUM('success', 'error', 'pending') NOT NULL DEFAULT 'success'
            COMMENT 'status of transaction';
          SQL

          execute <<-SQL
            SET SESSION alter_algorithm = @old_alter_algorithm;
          SQL
        end
      end

      dir.down do
        execute <<-SQL
          ALTER TABLE `refunds`
          MODIFY COLUMN status ENUM('success', 'error') NOT NULL DEFAULT 'success'
          COMMENT 'status of transaction';
        SQL
      end
    end
  end
end
