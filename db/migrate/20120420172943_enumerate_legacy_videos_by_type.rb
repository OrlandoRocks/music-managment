class EnumerateLegacyVideosByType < ActiveRecord::Migration[4.2]
  def self.up
    Video.all.each do |vid|
      if vid.definition != nil
        vid.update_attribute(:video_type,"FeatureFilm")
      else
        vid.update_attribute(:video_type,"MusicVideo")
      end
    end
  end

  def self.down
    Video.all.each do |vid|
        vid.update_attribute(:video_type,"MusicVideo")
    end
  end
end
