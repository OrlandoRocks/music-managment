class AddIsWithdrawalBlockedToCopyrightClaims < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      add_column :copyright_claims, :is_withdrawal_blocked, :boolean, default: false
      change_column :copyright_claims, :internal_claim_id, :string
    end
  end
end
