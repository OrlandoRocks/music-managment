class CreateTempPersonRoyalty < ActiveRecord::Migration[6.0]
  def change
    create_table :temp_person_royalties, comment: "Stores royalty information in a form suitable for faster processing. Data will be deleted once the processing is done" do |t|
      t.integer :person_id, null: false, index: true
      t.string  :user_currency, limit: 3, null: false, index: true
      t.integer :corporate_entity_id, null: false, index: true, comment: "Used for finance team to process amounts separately"
      t.text    :sales_record_master_ids
      t.decimal :total_amount_in_usd, precision: 23, scale: 6, comment: "Amount earned from non-youtube stores"
      t.decimal :encumbrance_amount_in_usd, precision: 23, scale: 6, comment: "Amount paid back for encumbrances. Always a negative value"
      t.decimal :split_amount_in_usd, precision: 23, scale: 6, comment: "Amount debited/credited as part of split earnings/deductions"
      t.decimal :you_tube_amount_in_usd, precision: 23, scale: 6, comment: "Amount earned from youtube stores as part of the postings"
      t.decimal :encumbrance_percentage, precision: 23, scale: 14, comment: "% of amount earned that was used for repaying encumbrances"
      t.decimal :fx_rate, precision: 23, scale: 14
      t.decimal :coverage_rate, precision: 23, scale: 14
      t.decimal :total_amount_in_user_currency, precision: 23, scale: 6
      t.decimal :encumbrance_amount_in_user_currency, precision: 23, scale: 6
      t.decimal :split_amount_in_user_currency, precision: 23, scale: 6
      t.decimal :you_tube_amount_in_user_currency, precision: 23, scale: 6
      t.integer :person_intake_id, comment: "Used for idempotency"
      t.integer :you_tube_royalty_intake_id, comment: "Used for idempotency"
      t.boolean :summarized, default: 0, comment: "Used for idempotency"
      t.boolean :sales_records_copied, default: 0, comment: "Used for idempotency"
      t.boolean :youtube_records_copied, default: 0, comment: "Used for idempotency"
      t.timestamps
    end
  end
end
