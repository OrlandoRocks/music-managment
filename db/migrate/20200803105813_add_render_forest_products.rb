class AddRenderForestProducts < ActiveRecord::Migration[6.0]
  @@country_website_ids = [1, 2, 3, 4]
  @@created_by_id = 1541256
  @@rf_product = 'rf_video_download'
  @@rf_price = [
    {price: 14.99, currency: 'USD'},
    {price: 20.99, currency: 'CAD'},
    {price: 14.99, currency: 'GBP'},
    {price: 22.99, currency: 'AUD'},
  ]

  def up
    country_websites = CountryWebsite.where(id: @@country_website_ids)
    country_websites.each do |country|
      Product.create(created_by_id: @@created_by_id,
                     country_website_id: country.id,
                     display_name: @@rf_product,
                     name: 'RenderForest Video Purchase',
                     description: 'RenderForest Video Purchase',
                     status: 'Active', product_type: 'Ad Hoc',
                     is_default: 1, applies_to_product: 'None',
                     sort_order: 1, renewal_level: 'None',
                     price: @@rf_price.select {|c| c[:currency] == country.currency}[0][:price],
                     currency: country.currency)
    end
  end

  def down
    Product.where(display_name: @@rf_product).destroy_all
  end
end
