class CreateRecordings < ActiveRecord::Migration[4.2]
  def change
    create_table :recordings do |t|
      t.references :composition, index: true, foreign_key: true
      t.references :recordable, polymorphic: true, index: true
      t.string :recording_code

      t.timestamps null: false
    end
  end
end
