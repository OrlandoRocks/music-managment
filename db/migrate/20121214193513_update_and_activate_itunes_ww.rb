class UpdateAndActivateItunesWw < ActiveRecord::Migration[4.2]
  def self.up
    Store.transaction do
      itunes_store_group = StoreGroup.find(1)
      itunes_ww          = Store.find(36)
      itunes_us          = Store.find(1)

      #Remove all stores execpt itunes ww from the store group
      itunes_store_group.store_group_stores.each do |sgs|
        if sgs.store_id != itunes_ww.id
          puts("Removed #{sgs.store.name} from itunes store group")
          sgs.destroy
        end
      end

      #Delete erroneous link to variable price that does not exist
      ActiveRecord::Base.connection.execute("delete from variable_prices_stores where variable_price_id = 36");

      #Add itunes variable prices for itunes ww if not already present
      itunes_us.variable_price_stores.each do |vps|
        #Create the variable price store link if its active and doesn't already exist
        if vps.is_active && itunes_ww.variable_price_stores.count(:conditions=>["variable_price_id = ?",vps.variable_price_id]) == 0
          puts("Creating variable price for itunes ww: variable_price_id #{vps.variable_price_id}")
          itunes_ww.variable_price_stores.create!(:variable_price_id=>vps.variable_price_id,:position=>vps.position, :is_active=>vps.is_active)
        end
      end

      #Add default variable price for itunes ww
      puts("Setting default variable price for itunes ww")
      itunes_ww.default_variable_price = itunes_us.default_variable_price

      #Activate itunes ww
      puts("activating and setting itunes ww as free")
      itunes_ww.is_active = true

      #Set as a free_store
      itunes_ww.is_free = true

      itunes_ww.save!
    end

    #add defaults to aod and amp3
    amp3 = Store.find(13)
    aod  = Store.find(19)

    amp3.default_variable_price = amp3.variable_prices.find(1)
    aod.default_variable_price  = aod.variable_prices.find(19)

  rescue StandardError => e

  end

  def self.down
  end
end
