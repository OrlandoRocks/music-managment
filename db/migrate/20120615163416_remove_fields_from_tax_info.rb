class RemoveFieldsFromTaxInfo < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :tax_info, :email
    remove_column :tax_info, :alternate_email
    remove_column :tax_info, :phone
    remove_column :tax_info, :alternate_phone
  end

  def self.down
    add_column :tax_info, :email, :string, :limit => 100
    add_column :tax_info, :alternate_email, :string, :limit => 100
    add_column :tax_info, :phone, :string, :limit => 20
    add_column :tax_info, :alternate_phone, :string, :limit => 20
  end
end
