class CreatePersonGstins < ActiveRecord::Migration[6.0]
  def change
    create_table :person_gstins do |t|
      t.belongs_to :person
      t.column :gstin, 'char(15)'
      t.timestamps
    end
  end
end
