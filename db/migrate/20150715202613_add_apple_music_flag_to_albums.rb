class AddAppleMusicFlagToAlbums < ActiveRecord::Migration[4.2]
  def up
    p "This migration may take a while because it adds a column to the albums table and then updates that column based on rows in person_service_opt_ins."
    add_column :albums, :apple_music, :boolean, default: false, null: false

    p "The albums table is no longer locked. The updating of individual rows starts now."
    exclude_albums = ENV["APPLE_MUSIC_EXCLUDES"].nil? ? [-1] : CSV.read(ENV["APPLE_MUSIC_EXCLUDES"]).flatten

    PersonServiceOptIn.all.each do |opt_in|
      Album.where("person_id = ? and id not in (?)", opt_in.person_id, exclude_albums).update_all("apple_music = 1")
    end
  end

  def down
    remove_column :albums, :apple_music
  end
end
