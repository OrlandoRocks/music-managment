class RemoveHashCodeFromPeopleTable < ActiveRecord::Migration[6.0]
  def self.up
    safety_assured { remove_column :people, :hash_code }
  end

  def self.down
    safety_assured { add_column :people, :hash_code, :string }
  end
end
