class AddLodIdToComposer < ActiveRecord::Migration[4.2]
  def self.up
    add_column :composers, :lod_id, :integer
  end

  def self.down
    drop_column :composers, :lod_id
  end
end
