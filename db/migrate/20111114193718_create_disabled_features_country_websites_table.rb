class CreateDisabledFeaturesCountryWebsitesTable < ActiveRecord::Migration[4.2]
  def self.up
    create_table :disabled_features_country_websites do |t|
      t.integer :feature_id
      t.integer :country_website_id
      t.timestamps
    end
  end

  def self.down
    drop_table :disabled_features_country_websites
  end
end
