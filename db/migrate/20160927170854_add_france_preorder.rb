class AddFrancePreorder < ActiveRecord::Migration[4.2]

  VARIABLE_PRICE_EURO_PRICE_MAP = {
    30 =>	1.62, # EP *
    31 => 0.71, # Single Tier 2 *
    32 =>	4.35, # Mid *
    33 => 0.90, # Single Tier 1 *
    34 => 5.45, # Front One *
    35 => 2.70, # Budget One
    37 => 3.80, # Back
    38 =>	1.08, # Mini EP
    39 => 4.90, # Mid/Front *
    40 =>	0.81, # Digital 45 - $1.99
    41 => 2.18, # Mini Album
    42 => 6.50, # Front Plus
    43 => 0.50, # Single Tier 2
    44 => 3.25, # Budget Two
    45 => 7.05, # Deluxe One
    46 => 7.60, # Deluxe Two
    47 => 8.15, # Deluxe Three
    48 =>	8.70, # Deluxe Four
  }

  FRANCE_WEBSITE = CountryWebsite.find_by(country: "FR")

  def up
    CountryVariablePrice.create_prices_from_country_price_map(FRANCE_WEBSITE, VARIABLE_PRICE_EURO_PRICE_MAP)
  end

  def down
    cvps = CountryVariablePrice.where(country_website_id: FRANCE_WEBSITE)
    cvps.each { |cvp| cvp.delete }
  end

end
