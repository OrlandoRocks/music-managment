class CreatePlansStores < ActiveRecord::Migration[6.0]
  def change
    create_table :plans_stores do |t|
      t.references :plan, null: false
      t.references :store, null: false, type: 'smallint unsigned'
      t.timestamps
    end
  end
end
