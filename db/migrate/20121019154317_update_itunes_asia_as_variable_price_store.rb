class UpdateItunesAsiaAsVariablePriceStore < ActiveRecord::Migration[4.2]
  def self.up
    itunes_asia    = Store.find_by(name: "iTunes Asia")
    variable_price = VariablePrice.where(:id=>39).first

    if itunes_asia and variable_price

      #Create itunes variable_price stores for variable_prices 30-44
      (30..44).each do |vp_id|
        next if itunes_asia.variable_price_stores.where(:variable_price_id=>vp_id).count > 0
        itunes_asia.variable_price_stores.create!(:variable_price_id=>vp_id, :position=>vp_id, :is_active=>true)
      end

      #Create the default variable_price store for itunes asia
      itunes_asia.default_variable_price=variable_price

      connection.execute("UPDATE salepoints SET variable_price_id = #{variable_price.id}, track_variable_price_id = NULL where store_id=#{itunes_asia.id}")
    end
  end

  def self.down
    itunes_asia = Store.find_by(name: "iTunes Asia")

    if itunes_asia
      itunes_asia.default_variable_price=nil
      connection.execute("DELETE from variable_prices_stores where store_id = #{itunes_asia.id}")
      connection.execute("UPDATE salepoints SET variable_price_id = NULL, track_variable_price_id = NULL where store_id=#{itunes_asia.id}")
    end
  end
end
