class CreateSalesRecordsV2 < ActiveRecord::Migration[4.2]
  def up
    sql = %Q{
      CREATE TABLE IF NOT EXISTS sales_records_v2 LIKE sales_records;
    }

    puts "Running: #{sql}"
    ActiveRecord::Base.connection.execute(sql)
  end

  def down
    sql = %Q{
      DROP TABLE IF EXISTS sales_records_v2;
    }

    puts "Running: #{sql}"
    ActiveRecord::Base.connection.execute(sql)
  end
end
