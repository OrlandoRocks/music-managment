class AddLanguageCodeIdToSongs < ActiveRecord::Migration[4.2]
  def change
    add_column :songs, :language_code_id, :integer
  end
end
