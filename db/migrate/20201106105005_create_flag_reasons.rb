class CreateFlagReasons < ActiveRecord::Migration[6.0]
  def change
    create_table :flag_reasons do |t|
      t.references :flag, foreign_key: true
      t.string :reason, null: false

      t.timestamps
    end
  end
end
