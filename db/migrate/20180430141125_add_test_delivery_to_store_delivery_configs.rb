class AddTestDeliveryToStoreDeliveryConfigs < ActiveRecord::Migration[4.2]
  def change
    add_column :store_delivery_configs, :test_delivery, :boolean, default: false
  end
end
