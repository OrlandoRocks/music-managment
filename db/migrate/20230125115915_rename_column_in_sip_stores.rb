class RenameColumnInSipStores < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      change_table :sip_stores do |t|
        t.rename :royalty_store_id, :royalty_sub_store_id
      end
    end
  end
end
