class CreateDowngradeCategoryReasons < ActiveRecord::Migration[6.0]
  def change
    create_table :downgrade_category_reasons do |t|
      t.references :downgrade_category
      t.string :name

      t.timestamps
    end
  end
end
