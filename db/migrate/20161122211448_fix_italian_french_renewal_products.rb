class FixItalianFrenchRenewalProducts < ActiveRecord::Migration[4.2]
  def up
    renewal_product_mapping = {
      "375" => 364, # 1 Year Album
      "376" => 364, # 2 Year Album
      "377" => 364, # 5 Year Album
      "367" => 364, # Album Distribution Credit
      "368" => 364, # Album Distribution Credit - 5 Pack
      "369" => 364, # Album Distribution Credit - 10 Pack
      "370" => 364, # Album Distribution Credit - 20 Pack
      "371" => 365, # Single Distribution Credit
      "372" => 365, # Single Distribution Credit - 5 Pack
      "373" => 365, # Single Distribution Credit - 10 Pack
      "374" => 365, # Single Distribution Credit - 20 Pack
      "378" => 365, # 1 Year Single
      "379" => 365, # 2 Year Single
      "380" => 365, # 5 Year Single
      "383" => 382, # 1 Year Ringtone
      "387" => 382, # Ringtone Distribution
      "388" => 382, # Ringtone Distribution Credit - 3 Pack"9"
      "389" => 382, # Ringtone Distribution Credit - 5 Pack
      "390" => 382, # Ringtone Distribution Credit - 10 Pack
      "391" => 382, # Ringtone Distribution Credit - 20 Pack
      "336" => 363, # 1 Year Album
      "337" => 363, # 2 Year Album
      "338" => 363, # 5 Year Album
      "328" => 363, # Album Distribution Credit
      "329" => 363, # Album Distribution Credit - 5 Pack
      "330" => 363, # Album Distribution Credit - 10 Pack
      "331" => 363, # Album Distribution Credit - 20 Pack
      "332" => 326, # Single Distribution Credit
      "333" => 326, # Single Distribution Credit - 5 Pack
      "334" => 326, # Single Distribution Credit - 10 Pack
      "335" => 326, # Single Distribution Credit - 20 Pack
      "339" => 326, # 1 Year Single
      "340" => 326, # 2 Year Single
      "341" => 326, # 5 Year Single
      "344" => 343, # 1 Year Ringtone
      "348" => 343, # Ringtone Distribution
      "349" => 343, # Ringtone Distribution Credit - 3 Pack"9"
      "350" => 343, # Ringtone Distribution Credit - 5 Pack
      "351" => 343, # Ringtone Distribution Credit - 10 Pack
      "352" => 343 # Ringtone Distribution Credit - 20 Pack
    }

    renewal_product_mapping.each do |product_id, renewal_product_id|
      product = Product.find(product_id)
      product.product_items.first.update!(renewal_product_id: renewal_product_id)
    end
  end

  def down
  end
end
