class AddRelatedProductsForDistroCredits < ActiveRecord::Migration[4.2]
  def self.up

    if !Rails.env.test? and !Rails.env.cucumber?
      begin
        if Rails.env.development?
          user = Person.first
        else
          user = Person.where(:email=>"ewald@tunecore.com").first
        end
        index = 0
        [1,2,3,9,36,37,38,41,42,43,46,47,48,65,101,103].each do |product_id|
          [13,18,22].each do |related_product_id|
            index += 1
            RelatedProduct.create!(:product_id=>product_id, :related_product_id=>related_product_id, :created_by_id=>user.id, :sort_order=>index, :start_date=>Date.today - 1.day, :expiration_date=>Date.today - 1.day + 3.years )
          end
        end

        [66,67,68,69,70,71,85,88,95,96,97,98,99,100,102,104].each do |product_id|
          [72,77,81].each do |related_product_id|
            index += 1
            RelatedProduct.create!(:product_id=>product_id, :related_product_id=>related_product_id, :created_by_id=>user.id, :sort_order=>index, :start_date=>Date.today - 1.day, :expiration_date=>Date.today - 1.day + 3.years )
          end
        end
      rescue StandardError => e
        puts(e)
      end
    end
  end

  def self.down
  end
end
