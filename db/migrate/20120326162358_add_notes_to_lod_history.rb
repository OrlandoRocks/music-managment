class AddNotesToLodHistory < ActiveRecord::Migration[4.2]
  def self.up
    add_column :lod_history, :notes, :text
  end

  def self.down
    remove_column :lod_history, :notes
  end
end
