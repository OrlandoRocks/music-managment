class AddProductFamilyToProduct < ActiveRecord::Migration[4.2]
  def up
    add_column :products, :product_family, :string

    Product.where("id in (6, 7, 8, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 28, 29, 31, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 87, 95, 96, 97, 98, 99, 100, 103, 104, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120)").update_all("product_family = 'Distribution'")
    Product.where("(product_type = 'Renewal' or product_type like '%Extension%') and id not in (4,5)").update_all("product_family = 'Renewal'")
    Product.where("id in (65, 88)").update_all("product_family = 'Publishing'")
    Product.where("id in (149, 150)").update_all("product_family = 'Artist Services (non-distribution)'")
    Product.where("id in (133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146)").update_all("product_family = 'Artist Services (post-distribution)'")
    Product.where("id in (9, 85, 105, 106, 109, 110, 129, 130, 131, 132, 147, 148)").update_all("product_family = 'Distribution Add-Ons'")
    Product.where("id in (107, 108)").update_all("product_family = 'Credits'")
    Product.where("product_family is null").update_all("product_family = 'Miscellaneous'")
  end

  def down
    remove_column :products, :product_family
  end
end
