class AddCompanyInfoToInvoiceStaticCustomerInfo < ActiveRecord::Migration[6.0]
  def change
    add_column :invoice_static_customer_infos, :enterprise_number, :string
    add_column :invoice_static_customer_infos, :company_registry_data, :string
    add_column :invoice_static_customer_infos, :place_of_legal_seat, :string
    add_column :invoice_static_customer_infos, :registered_share_capital, :string
  end
end
