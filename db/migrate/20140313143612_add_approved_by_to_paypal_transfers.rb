class AddApprovedByToPaypalTransfers < ActiveRecord::Migration[4.2]
  def change
    add_column :paypal_transfers, :approved_by_id, :integer
  end
end
