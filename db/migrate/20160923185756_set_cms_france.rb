class SetCmsFrance < ActiveRecord::Migration[4.2]
  def up
    french_site = CountryWebsite.find(6)
    french_site.cms_sets.create!(callout_type: "interstitial", callout_name: "None", start_tmsp: Time.now + 1.second)
    french_site.cms_sets.create!(callout_type: "login", callout_name: "None", start_tmsp: Time.now + 1.second)
    french_site.cms_sets.create!(callout_type: "dashboard", callout_name: "None", start_tmsp: Time.now + 1.second)
  end

  def down
    CountryWebsite.find(6).cms_sets.each { |cs| cs.destroy }
  end
end
