class ChangeColumnsInOutboundRefunds < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      change_column :outbound_refunds, :invoice_sequence, :integer
      change_column_null :outbound_refunds, :user_invoice_prefix, false
      change_column_null :outbound_refunds, :refund_id, false
      change_column_null :outbound_refunds, :outbound_invoice_id, false
      change_column_null :outbound_refunds, :refund_settlement_id, false
    end
  end

  def down
    safety_assured do
      change_column :outbound_refunds, :invoice_sequence, :string
      change_column_null :outbound_refunds, :user_invoice_prefix, true
      change_column_null :outbound_refunds, :refund_id, true
      change_column_null :outbound_refunds, :outbound_invoice_id, true
      change_column_null :outbound_refunds, :refund_settlement_id, true
    end
  end
end
