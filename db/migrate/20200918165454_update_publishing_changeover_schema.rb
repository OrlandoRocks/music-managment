class UpdatePublishingChangeoverSchema < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      add_column :publishing_composers, :is_primary_composer, :boolean, default: false
      add_column :publishing_composers, :is_unknown, :boolean, default: false
      add_column :publishing_composers, :legacy_composer_id, :bigint
      add_column :publishing_composers, :legacy_cowriter_id, :bigint
      add_column :publishing_compositions, :legacy_composition_id, :bigint
      add_column :publishing_composition_splits, :legacy_publishing_split_id, :bigint
    end
  end

  def down
    safety_assured do
      remove_column :publishing_composers, :is_primary_composer
      remove_column :publishing_composers, :is_unknown
      remove_column :publishing_composers, :legacy_composer_id
      remove_column :publishing_composers, :legacy_cowriter_id
      remove_column :publishing_compositions, :legacy_composition_id
      remove_column :publishing_composition_splits, :legacy_publishing_split_id
    end
  end
end
