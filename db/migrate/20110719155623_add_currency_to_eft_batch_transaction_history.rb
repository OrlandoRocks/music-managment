class AddCurrencyToEftBatchTransactionHistory < ActiveRecord::Migration[4.2]
  def self.up
      execute("ALTER TABLE eft_batch_transaction_history
              ADD COLUMN currency CHAR(3) DEFAULT NULL COMMENT 'Currency' AFTER amount")
  end

  def self.down
    remove_column :eft_batch_transaction_history, :currency
  end
end
