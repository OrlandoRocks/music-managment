class CreatePersonCountries < ActiveRecord::Migration[4.2]
  def change
    create_table :person_countries do |t|
      t.integer :person_id, null: false
      t.integer :country_id, null: false
      t.string :source, null: false

      t.timestamps
    end
  end
end
