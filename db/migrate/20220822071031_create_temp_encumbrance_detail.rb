class CreateTempEncumbranceDetail < ActiveRecord::Migration[6.0]
  def change
    create_table :temp_encumbrance_details, comment: "Stores encumbrance deductions for the posting. Data will be deleted once the processing is done" do |t|
      t.integer :encumbrance_summary_id, null: false, index: true
      t.integer :person_id, null: false, index: true
      t.decimal :transaction_amount, precision: 12, scale: 2
      t.timestamps
    end
  end
end
