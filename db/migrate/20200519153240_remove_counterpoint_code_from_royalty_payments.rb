class RemoveCounterpointCodeFromRoyaltyPayments < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      remove_column :royalty_payments, :counterpoint_code, :integer
    end
  end
end
