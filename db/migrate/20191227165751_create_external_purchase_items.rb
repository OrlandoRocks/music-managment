class CreateExternalPurchaseItems < ActiveRecord::Migration[4.2][4.2]
  def change
    create_table :external_purchase_items do |t|
      t.string  :related_item_type
      t.integer :related_item_id
      t.references :external_purchase

      t.timestamps
    end

    add_index :external_purchase_items, [:related_item_type, :related_item_id], name: "uniq_index_related_items", unique: true
    add_index :external_purchase_items, :external_purchase_id
  end
end
