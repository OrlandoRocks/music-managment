class RemoveUnnecessaryComposerColumns < ActiveRecord::Migration[6.0]
  def change
    safety_assured {
      remove_column :publishing_composers, :pro_songwriter_affiliation
      remove_column :publishing_composers, :pro_songwriter_affiliation_answer
      remove_column :publishing_composers, :pro_publisher_affiliation_answer
      remove_column :publishing_composers, :pub_admin_status

      remove_column :composers, :pro_songwriter_affiliation
      remove_column :composers, :pro_songwriter_affiliation_answer
      remove_column :composers, :pro_publisher_affiliation_answer
      remove_column :composers, :pub_admin_status
    }
  end
end
