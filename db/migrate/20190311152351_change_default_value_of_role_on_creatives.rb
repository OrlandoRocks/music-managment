class ChangeDefaultValueOfRoleOnCreatives < ActiveRecord::Migration[4.2]
  def up
    change_column_default :creatives, :role, "primary_artist"
  end

  def down
    change_column_default :creatives, :role, "performer"
  end
end
