class AddCountryWebsiteToTargetedOffers < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE targeted_offers ADD country_website_id CHAR(3) NOT NULL DEFAULT 1 COMMENT 'Country that this targeted offer is offered to' AFTER id");
  end

  def self.down
    remove_column :targeted_offers, :country_website_id
  end
end
