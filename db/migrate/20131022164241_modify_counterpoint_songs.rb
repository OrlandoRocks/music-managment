class ModifyCounterpointSongs < ActiveRecord::Migration[4.2]
  def self.up
    rename_table :counterpoint_songs, :muma_song_sync
    execute("ALTER TABLE muma_song_sync
    CHANGE tunecore_song_id isrc varchar(12),
    MODIFY code INT UNIQUE")
  end

  def self.down
    execute("ALTER TABLE muma_song_sync
      CHANGE isrc tunecore_song_id varchar(255),
      DROP INDEX code")
    rename_table :muma_song_sync, :counterpoint_songs
  end
end
