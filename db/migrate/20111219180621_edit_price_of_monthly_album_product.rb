class EditPriceOfMonthlyAlbumProduct < ActiveRecord::Migration[4.2]
  def self.up
    #editing the monthly initial_purchase price to $4.99
    monthly_album_renewal_rule = ProductItemRule.find_by(id: 106, :inventory_type=>'Album', :product_item_id =>29)
    monthly_album_renewal_rule.update(:price => 4.99) if !monthly_album_renewal_rule.nil?
  end

  def self.down
    #editing the monthly initial_purchase price to $8.00
    monthly_album_renewal_rule = ProductItemRule.find_by(id: 106, :inventory_type=>'Album', :product_item_id =>29)
    monthly_album_renewal_rule.update(:price => 8.00) if !monthly_album_renewal_rule.nil?
  end
end
