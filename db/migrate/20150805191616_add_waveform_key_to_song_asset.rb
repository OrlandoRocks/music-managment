class AddWaveformKeyToSongAsset < ActiveRecord::Migration[4.2]
  def change
    add_column :song_assets, :waveform_key, :string
  end
end
