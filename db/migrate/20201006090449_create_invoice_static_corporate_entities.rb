class CreateInvoiceStaticCorporateEntities < ActiveRecord::Migration[6.0]
  def change
    create_table :invoice_static_corporate_entities do |t|
      t.integer :related_id, null: false
      t.string :related_type, null: false
      t.string :name
      t.string :currency
      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.string :postal_code
      t.string :country
      t.string :vat_registration_number
      t.string :crn
      t.string :paying_bank
      t.string :rib
      t.string :bic
      t.string :iban

      t.timestamps
    end
  end
end
