class AddTranslatedNameToCompositions < ActiveRecord::Migration[4.2]
  def change
    add_column :compositions, :translated_name, :string
  end
end
