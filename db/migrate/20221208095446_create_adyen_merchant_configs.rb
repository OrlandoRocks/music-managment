class CreateAdyenMerchantConfigs < ActiveRecord::Migration[6.0]
  def change
    create_table :adyen_merchant_configs do |t|
      t.string :merchant_account
      t.string :api_key
      t.string :client_key
      t.references :corporate_entity
      t.timestamps
    end
  end
end
