class AddDomainToCloudSearchBatches < ActiveRecord::Migration[4.2]
  def self.up
    add_column :cloud_search_batches, :domain, :string
  end

  def self.down
    remove_column :cloud_search_batches, :domain
  end
end
