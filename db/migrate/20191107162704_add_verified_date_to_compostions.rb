class AddVerifiedDateToCompostions < ActiveRecord::Migration[4.2]
  def change
    add_column :compositions, :verified_date, :datetime
  end
end
