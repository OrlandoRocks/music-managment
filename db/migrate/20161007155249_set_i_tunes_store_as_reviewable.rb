class SetITunesStoreAsReviewable < ActiveRecord::Migration[4.2]
  def up
    iTunes = Store.find(36)
    iTunes.reviewable = 1
    iTunes.save!
  end

  def down
    iTunes = Store.find(36)
    iTunes.reviewable = 0
    iTunes.save!
  end
end
