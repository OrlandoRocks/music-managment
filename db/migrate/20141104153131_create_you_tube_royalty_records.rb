class CreateYouTubeRoyaltyRecords < ActiveRecord::Migration[4.2]

  def up
    sql = %Q{
      CREATE TABLE `you_tube_royalty_records` (
        `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `person_id` int(10) unsigned NOT NULL,
        `person_intake_id` int(10) unsigned DEFAULT NULL,
        `sip_you_tube_royalty_record_id` int(10) unsigned DEFAULT NULL,
        `song_id` int(11) NOT NULL,
        `total_views` int(10) unsigned NOT NULL DEFAULT 0,
        `you_tube_policy_type` enum('monetize','track') NOT NULL DEFAULT 'track',
        `gross_revenue` decimal(15,6) NOT NULL COMMENT 'Gross revenue before TC tariff in USD',
        `exchange_rate` decimal(15,6) DEFAULT NULL COMMENT 'Exchange rate from Revenue currency',
        `tunecore_commision` decimal(15,6) DEFAULT 0,
        `net_revenue` decimal(15,6) NOT NULL COMMENT 'Revenue due to customer after TC tariff',
        `net_revenue_currency` char(3) NOT NULL DEFAULT 'USD' COMMENT 'Net revenue due to customer currency',
        `sales_period_start` date NOT NULL COMMENT 'first day of the sales period month.',
        `you_tube_video_id` varchar(255),
        `song_name` varchar(255) NOT NULL DEFAULT '',
        `album_name` varchar(255) NOT NULL DEFAULT '',
        `label_name` varchar(255) NOT NULL DEFAULT '',
        `artist_name` varchar(255) NOT NULL DEFAULT '',
        `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
        `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',
        PRIMARY KEY (`id`),
        KEY (`person_id`, `sales_period_start`, `song_id`),
        KEY (`person_id`, `person_intake_id` ),
        KEY (`created_at`),
        CONSTRAINT FOREIGN KEY (`person_intake_id`) REFERENCES `person_intakes` (`id`),
        CONSTRAINT FOREIGN KEY (`person_id`) REFERENCES `people` (`id`),
        CONSTRAINT FOREIGN KEY (`song_id`) REFERENCES `songs` (`id`)
      ) ENGINE=InnoDB CHARSET=utf8;
    }
    ActiveRecord::Base.connection.execute(sql)
  end

  def down
    ActiveRecord::Base.connection.execute("drop table you_tube_royalty_records")
  end
end
