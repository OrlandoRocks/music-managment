class AddMetadataLanguageCodeIdToAlbums < ActiveRecord::Migration[6.0]
  def change
    add_column :albums, :metadata_language_code_id, :integer
  end
end
