class CreateArchiveGain < ActiveRecord::Migration[6.0]
  def change
    create_table :archive_gain, comment: "Archive of data from people.gain_employee_id" do |t|
      t.string :gain_employee_id, comment: "Data from people.gain_employee_id"
      t.integer :person_id
      
      t.timestamps
    end
  end
end
