class BackfillSuspiciousFlags < ActiveRecord::Migration[4.2]
  def up
    PaypalTransfer.where(transfer_status: 'pending').each do |transfer|
      p "backfilling susp flag for record #{transfer.id}"
      Rails.logger.info("backfilling susp flag for record #{transfer.id}")
      transfer.set_suspicious_flags
      Rails.logger.info("Unable to update susp flag for record #{transfer.id}") unless transfer.save
    end
    CheckTransfer.where(transfer_status: 'pending').each do |transfer|
      p "backfilling susp flag for record #{transfer.id}"
      Rails.logger.info("backfilling susp flag for record #{transfer.id}")
      transfer.set_suspicious_flags
      Rails.logger.info("Unable to update susp flag for record #{transfer.id}") unless transfer.save
    end
    EftBatchTransaction.where(status: 'pending_approval').each do |transaction|
      p "backfilling susp flag for record #{transaction.id}"
      Rails.logger.info("backfilling susp flag for record #{transaction.id}")
      transaction.set_suspicious_flags
      Rails.logger.info("Unable to update susp flag for record #{transaction.id}") unless transaction.save
    end
  end

  def down
  end
end
