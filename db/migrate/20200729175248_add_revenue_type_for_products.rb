class AddRevenueTypeForProducts < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      add_column :products, :revenue_type, "ENUM('Artist Services', 'Distribution', 'Publishing', 'Video')", default: nil;
    end
  end

  def down
    remove_column :products, :revenue_type
  end
end