class AddItunesPanasiaToStores < ActiveRecord::Migration[4.2]
  def self.up
    if Store.find_by(name: "iTunes Asia") == nil
      Store.create(:name=>'iTunes Asia',:abbrev=>'pa', :short_name=>'iTunesPA', :position=>300, :needs_rights_assignment=>false, :is_active=>false,:base_price_policy_id=>"3" )
    else
      raise "iTunes Asia Store exists"
    end
  end

  def self.down
    execute("DELETE FROM stores where name = 'iTunes Asia'")
  end
end
