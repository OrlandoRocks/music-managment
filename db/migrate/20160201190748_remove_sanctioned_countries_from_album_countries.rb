class RemoveSanctionedCountriesFromAlbumCountries < ActiveRecord::Migration[4.2]
  def up
    countries       = Country::SANCTIONED_COUNTRIES
    album_countries = AlbumCountry.select(AlbumCountry.arel_table[Arel.star]).joins(:country)
                                  .where(Country.arel_table[:iso_code].in(countries))

    album_countries.destroy_all
  end

  def down
  end
end
