class AddNotificationsRole < ActiveRecord::Migration[4.2]
  def up
    role_name = "Notifications"
    role_count = Role.where("name=?",role_name).count 

    if role_count == 0
      role = Role.new(:name=>role_name, :long_name=>"Notification Admin", :description=>"Gives ability to administer Notifications", :is_administrative=>1)
      role.save!
    end
  end

  def down
    roles = Role.where("name=?","Notifications").all
    unless roles.blank?
      roles.each { |role| role.destroy }
    end
  end
end
