class AddInitiatedAtToDisputes < ActiveRecord::Migration[6.0]
  def up
    add_column :disputes, :initiated_at, :datetime
    change_column_default :disputes, :initiated_at, -> { "CURRENT_TIMESTAMP" }
  end

  def down
    remove_column :disputes, :initiated_at
  end
end
