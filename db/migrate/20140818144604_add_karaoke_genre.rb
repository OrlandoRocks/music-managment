class AddKaraokeGenre < ActiveRecord::Migration[4.2]
  def up
    sql = %Q{insert into genres (id, name, parent_id, is_active ) values ( 39, 'Karaoke', 0, 1 );}
    ActiveRecord::Base.connection.execute(sql)
  end

  def down
    sql = %Q{delete from genres where id = 39 and name = 'Karaoke'}
    ActiveRecord::Base.connection.execute(sql)
  end
end
