class UpdateRenewalProductIdAndParentIdForUkProducts < ActiveRecord::Migration[4.2]
  def up
    us_to_uk_renewal_product_map = {1 => 153, 3 => 155, 121 => 184, 33 => 165, 123 => 185, 101 => 174}
    product_items = ProductItem.find_by_sql("select product_items.* from product_items
      inner join products on product_items.product_id = products.id
      where products.country_website_id = 3 and product_items.renewal_product_id is not null")

    product_items.each do |pi|
      pi.update(renewal_product_id: us_to_uk_renewal_product_map[pi.renewal_product_id])
    end

    product_item_rules = ProductItemRule.find_by_sql("select product_item_rules.* from product_item_rules
      inner join product_items on product_item_rules.product_item_id = product_items.id
      inner join products on product_items.product_id = products.id
      where products.country_website_id = 3")

    product_item_rules.group_by{|pir| pir.product_item }.each do |k, v|
      base_parent = nil
      v.sort.each do |pir|
        next if pir.parent_id == nil
        if pir.rule == "price_for_each_above_included"
          pir.update(parent_id: v.find{|p| p.inventory_type == pir.inventory_type && p.id != pir.id}.id)
        else
          base_parent ||= v.find{|p| p.parent_id == nil }
          pir.update(parent_id: base_parent.id)
        end
      end
    end
  end

  def down
  end
end
