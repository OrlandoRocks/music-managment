class CreateDisputeAudits < ActiveRecord::Migration[6.0]
  def change
    create_table :dispute_audits do |t|
      t.references :dispute, index: true, null: false
      t.text :note
      t.timestamps
    end
  end
end
