class CreateGenrePermissions < ActiveRecord::Migration[6.0]
  def change
    create_table :genre_permissions do |t|
      t.integer :store_id, foreign_key: true, index: true, null: false, limit: 2
      t.integer :country_id, foreign_key: true, index: true, null: false, limit: 2
      t.references :album_genre_whitelist, foreign_key: true, index: true, null: false

      t.timestamps
    end
  end
end
