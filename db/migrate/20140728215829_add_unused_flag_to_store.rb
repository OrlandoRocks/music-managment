class AddUnusedFlagToStore < ActiveRecord::Migration[4.2]
  def change
    add_column :stores, :in_use_flag, :boolean, default: true

    Store.where("id in (6, 9, 11, 14, 15, 16, 17, 20, 27, 30, 37, 38, 49)").update_all("in_use_flag = false")
  end
end
