class CreateCountryWebsites < ActiveRecord::Migration[4.2]
  def self.up
    up_sql = %Q(CREATE TABLE `country_websites` (
      `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
      `name` char(20) DEFAULT NULL COMMENT 'base url',
      `url` char(20) DEFAULT NULL COMMENT 'base url',
      `currency` char(3) DEFAULT NULL COMMENT '3 letter ISO currency',
      `country` char(20) DEFAULT NULL COMMENT '2 letter ISO for country',
      `updated_at` TIMESTAMP NULL COMMENT 'Datetime of latest update',
      `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',
      PRIMARY KEY (`id`),
      KEY `country_websites_currency` (`currency`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;)
    execute up_sql
    # Populate Default data for now
    CountryWebsite.create(:name=>'TuneCore',:url=>'www.tunecore.com', :currency=>'USD', :country => 'US')
    CountryWebsite.create(:name=>'TuneCore Canada',:url=>'ca.tunecore.com', :currency=>'CAD', :country => 'CA')
  end

  def self.down
    drop_table :country_websites 
  end
end
