class RenameSourceContentreviewNoteInDistributionApiAlbum < ActiveRecord::Migration[6.0]
  def self.up
    add_column :distribution_api_albums, :source_contentreview_notes, :text
    safety_assured {
      remove_column :distribution_api_albums, :source_contentreview_note, :text
    }
  end

  def self.down
    add_column :distribution_api_albums, :source_contentreview_note, :text
    safety_assured {
      remove_column :distribution_api_albums, :source_contentreview_notes, :text
    }
  end
end
