class ChangeTransactionIdTypeOnBraintreeTransactions < ActiveRecord::Migration[4.2]
  def up
    change_column :braintree_transactions, :transaction_id, :string
    change_column :braintree_vault_transactions, :response_code, :string, limit: 12
  end

  def down
    change_column :braintree_transactions, :transaction_id, :int, limit: 20
    change_column :braintree_vault_transactions, :response_code, "char(4)"
  end
end
