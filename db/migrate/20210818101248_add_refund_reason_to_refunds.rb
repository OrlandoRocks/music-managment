class AddRefundReasonToRefunds < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      add_reference :refunds, :refund_reason, index: true
    end
  end
end
