class RemoveFormTypeFromTaxForms < ActiveRecord::Migration[6.0]
  def change
    safety_assured { remove_column :tax_forms, :form_type, :string }
    change_column_null :tax_forms, :tax_form_type_id, false
  end
end
