class CreateVatTaxAdjustments < ActiveRecord::Migration[6.0]
  def up
    create_table :vat_tax_adjustments do |t|
      t.references :person, null: false
      t.integer :related_id, null: false
      t.string  :related_type, null: false
      t.decimal :amount, precision: 23, scale: 14, null: false
      t.decimal :tax_rate, precision: 5, scale: 2, null: false
      t.string  :vat_registration_number
      t.string  :tax_type
      t.string  :trader_name
      t.string  :place_of_supply
      t.text    :error_message
      t.timestamps
    end

    add_index :vat_tax_adjustments, [:related_id, :related_type]
  end

  def down
    remove_index :vat_tax_adjustments, name: "index_vat_tax_adjustments_on_related_id_and_related_type"
    drop_table   :vat_tax_adjustments
  end
end
