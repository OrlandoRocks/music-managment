class AddAttemptToOptOutToHighEarningComposers < ActiveRecord::Migration[4.2]
  def change
    add_column :high_earning_composers, :attempt_to_opt_out, :boolean
  end
end
