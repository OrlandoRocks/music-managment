class AddColumnsToMasteredTracks < ActiveRecord::Migration[4.2]
  def change
    add_column :mastered_tracks, :landr_purchase_date, :datetime
    add_column :mastered_tracks, :job_id, :string
  end
end
