class AddCountryWebsiteLanguageIdToCmsSets < ActiveRecord::Migration[6.0]
  def change
      add_column :cms_sets, :country_website_language_id, :integer
  end
end
