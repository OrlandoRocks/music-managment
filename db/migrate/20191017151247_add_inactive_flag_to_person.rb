class AddInactiveFlagToPerson < ActiveRecord::Migration[4.2]
  def change
    add_column :people, :inactive, :boolean, default: false
  end
end
