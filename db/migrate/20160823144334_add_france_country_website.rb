class AddFranceCountryWebsite < ActiveRecord::Migration[4.2]
  def up
    CountryWebsite.create(:name=>'TuneCore France', :currency=>'EUR', :country => 'FR')
  end

  def down
    CountryWebsite.find_by(country: 'FR').destroy
  end
end
