class AddDelayedJobTables < ActiveRecord::Migration[4.2]
  def up
    sql = []
    sql <<  %Q(CREATE TABLE `1_delayed_jobs` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `priority` int(11) DEFAULT '0',
      `attempts` int(11) DEFAULT '0',
      `handler` mediumtext,
      `last_error` text,
      `run_at` datetime DEFAULT NULL,
      `locked_at` datetime DEFAULT NULL,
      `failed_at` datetime DEFAULT NULL,
      `locked_by` varchar(255) DEFAULT NULL,
      `created_at` datetime DEFAULT NULL,
      `updated_at` datetime DEFAULT NULL,
      `queue` varchar(255) DEFAULT NULL,
      PRIMARY KEY (`id`),
      KEY `delayed_jobs_priority` (`priority`,`run_at`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;)

    sql <<  %Q(CREATE TABLE `2_delayed_jobs` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `priority` int(11) DEFAULT '0',
      `attempts` int(11) DEFAULT '0',
      `handler` mediumtext,
      `last_error` text,
      `run_at` datetime DEFAULT NULL,
      `locked_at` datetime DEFAULT NULL,
      `failed_at` datetime DEFAULT NULL,
      `locked_by` varchar(255) DEFAULT NULL,
      `created_at` datetime DEFAULT NULL,
      `updated_at` datetime DEFAULT NULL,
      `queue` varchar(255) DEFAULT NULL,
      PRIMARY KEY (`id`),
      KEY `delayed_jobs_priority` (`priority`,`run_at`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;)

    sql <<  %Q(CREATE TABLE `3_delayed_jobs` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `priority` int(11) DEFAULT '0',
      `attempts` int(11) DEFAULT '0',
      `handler` mediumtext,
      `last_error` text,
      `run_at` datetime DEFAULT NULL,
      `locked_at` datetime DEFAULT NULL,
      `failed_at` datetime DEFAULT NULL,
      `locked_by` varchar(255) DEFAULT NULL,
      `created_at` datetime DEFAULT NULL,
      `updated_at` datetime DEFAULT NULL,
      `queue` varchar(255) DEFAULT NULL,
      PRIMARY KEY (`id`),
      KEY `delayed_jobs_priority` (`priority`,`run_at`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;)

    sql <<  %Q(CREATE TABLE `4_delayed_jobs` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `priority` int(11) DEFAULT '0',
      `attempts` int(11) DEFAULT '0',
      `handler` mediumtext,
      `last_error` text,
      `run_at` datetime DEFAULT NULL,
      `locked_at` datetime DEFAULT NULL,
      `failed_at` datetime DEFAULT NULL,
      `locked_by` varchar(255) DEFAULT NULL,
      `created_at` datetime DEFAULT NULL,
      `updated_at` datetime DEFAULT NULL,
      `queue` varchar(255) DEFAULT NULL,
      PRIMARY KEY (`id`),
      KEY `delayed_jobs_priority` (`priority`,`run_at`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;)

    sql <<  %Q(CREATE TABLE `5_delayed_jobs` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `priority` int(11) DEFAULT '0',
      `attempts` int(11) DEFAULT '0',
      `handler` mediumtext,
      `last_error` text,
      `run_at` datetime DEFAULT NULL,
      `locked_at` datetime DEFAULT NULL,
      `failed_at` datetime DEFAULT NULL,
      `locked_by` varchar(255) DEFAULT NULL,
      `created_at` datetime DEFAULT NULL,
      `updated_at` datetime DEFAULT NULL,
      `queue` varchar(255) DEFAULT NULL,
      PRIMARY KEY (`id`),
      KEY `delayed_jobs_priority` (`priority`,`run_at`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;)

    sql.each { |stmt| execute stmt }
  end

  def down
    execute "drop table 1_delayed_jobs;"
    execute "drop table 2_delayed_jobs;"
    execute "drop table 3_delayed_jobs;"
    execute "drop table 4_delayed_jobs;"
    execute "drop table 5_delayed_jobs;"
  end
end
