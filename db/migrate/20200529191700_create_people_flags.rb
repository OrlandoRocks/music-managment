class CreatePeopleFlags < ActiveRecord::Migration[6.0]
  def change
    create_table :people_flags do |t|
      t.integer :person_id
      t.integer :flag_id

      t.timestamps
    end

    add_index :people_flags, [:person_id, :flag_id]
  end
end
