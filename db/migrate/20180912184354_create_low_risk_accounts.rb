class CreateLowRiskAccounts < ActiveRecord::Migration[4.2]
  def change
    create_table :low_risk_accounts do |t|
      t.integer :person_id
      t.integer :added_by_admin_id

      t.timestamps
    end

    add_index :low_risk_accounts, :person_id, unique: true
    add_index :low_risk_accounts, :added_by_admin_id
  end
end
