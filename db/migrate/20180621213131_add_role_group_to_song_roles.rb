class AddRoleGroupToSongRoles < ActiveRecord::Migration[4.2]
  def change
    add_column :song_roles, :role_group, :string
  end
end
