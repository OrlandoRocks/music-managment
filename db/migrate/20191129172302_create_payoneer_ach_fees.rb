class CreatePayoneerAchFees < ActiveRecord::Migration[4.2]
  def change
    create_table :payoneer_ach_fees do |t|
      t.string :target_country_code
      t.decimal :transfer_fees, precision: 7, scale: 2
      t.decimal :minimum_threshold, precision: 7, scale: 2
    end
  end
end
