class AddSalepointPreorderDataToProductAppliesToProduct < ActiveRecord::Migration[4.2]
  def up
    execute <<-SQL
      ALTER TABLE products
        MODIFY COLUMN applies_to_product enum('Album','Single','Ringtone','Video','None','Widget','CreditUsage','Booklet','SalepointSubscription', 'SalepointPreorderData') NOT NULL DEFAULT 'None'
    SQL
  end
  
  def down
    execute <<-SQL
      ALTER TABLE products
        MODIFY COLUMN applies_to_product enum('Album','Single','Ringtone','Video','None','Widget','CreditUsage','Booklet','SalepointSubscription', 'SalepointPreorderData') NOT NULL DEFAULT 'None'
    SQL
  end
end
