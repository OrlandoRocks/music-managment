class AddIndexesToInvoiceStaticCorporateEntity < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      add_index :invoice_static_corporate_entities,
                [:related_id, :related_type],
                unique: true,
                name: "index_corporate_entities_on_related_id_and_related_type"
    end
  end
end
