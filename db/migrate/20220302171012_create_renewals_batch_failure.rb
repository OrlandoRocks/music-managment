class CreateRenewalsBatchFailure < ActiveRecord::Migration[6.0]
  def change
    create_table :renewals_batch_failures do |t|
      t.date :date, null: false, index: true, unique: true
      t.boolean :resolved, default: false, index: true
      t.timestamps
    end
  end
end
