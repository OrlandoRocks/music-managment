class UpdateMuveStore < ActiveRecord::Migration[4.2]
  def self.up
    store = Store.find_by(name: "Muve Music")
    if store != nil
      store.short_name = "Muve"
      store.save
      puts "muve store name updated"
    end
  end

  def self.down
    # no down migration, not needed
  end
end
