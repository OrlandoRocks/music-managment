class AddFileEvidenceToDisputeEvidence < ActiveRecord::Migration[6.0]
  def change
    add_attachment :dispute_evidences, :file_evidence
  end
end
