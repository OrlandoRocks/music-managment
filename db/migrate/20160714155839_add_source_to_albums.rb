class AddSourceToAlbums < ActiveRecord::Migration[4.2]
  def up
    add_column :albums, :source, :string
  end

  def down
    remove_column :albums, :source
  end
end
