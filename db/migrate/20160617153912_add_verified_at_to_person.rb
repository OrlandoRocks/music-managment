class AddVerifiedAtToPerson < ActiveRecord::Migration[4.2]
  def change
    add_column :people, :verified_at, :datetime
  end
end
