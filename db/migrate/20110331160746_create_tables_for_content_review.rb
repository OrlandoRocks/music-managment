class CreateTablesForContentReview < ActiveRecord::Migration[4.2]
  def self.up
    sqls = []
    create_review_reasons_table_sql=
              %Q(CREATE TABLE `review_reasons` (
              `id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
              `reason_type` ENUM('FLAGGED','REJECTED') NULL DEFAULT NULL COMMENT 'Indicates the type of reason',
              `reason` VARCHAR(255) DEFAULT NULL COMMENT 'reason why the content review decision has been made',
              `review_type` ENUM('LEGAL','STYLE') NOT NULL DEFAULT 'LEGAL' COMMENT 'Indicates content review type',
              `email_template_path` VARCHAR(255) NULL COMMENT 'email template path to notify users of their rejected release',
              `created_at` TIMESTAMP NULL DEFAULT NULL COMMENT 'Date record was created.',
              `updated_at` TIMESTAMP NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date last updated.',
              PRIMARY KEY (`id`)
            ) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='content review reasons';)
    sqls << create_review_reasons_table_sql

    create_review_audits_table_sql =
              %Q(CREATE TABLE `review_audits` (
              `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
              `album_id` INT(11) DEFAULT NULL COMMENT 'Foreign key to albums table.',
              `person_id` INT(10) UNSIGNED DEFAULT NULL COMMENT 'Foreign key to people table.',
              `event` ENUM('STARTED REVIEW','APPROVED', 'FLAGGED', 'REJECTED', 'SKIPPED', 'NOT SURE') NOT NULL DEFAULT 'STARTED REVIEW' COMMENT 'Indicates content review event',
              `note` VARCHAR(255) DEFAULT NULL COMMENT 'any additional note for the decision',
              `created_at` TIMESTAMP NULL DEFAULT NULL COMMENT 'Date record was created.',
              `updated_at` TIMESTAMP NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date last updated.',
              KEY `key_review_audits_album_id` (`album_id`),
              KEY `key_review_audits_person_id` (`person_id`),
              CONSTRAINT `FK_review_audits_album_id` FOREIGN KEY (`album_id`) REFERENCES `albums` (`id`),
              CONSTRAINT `FK_review_audits_person_id` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`),
              PRIMARY KEY (`id`)
            ) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='content review audits';)
    sqls << create_review_audits_table_sql

    create_review_reasons_roles_table_sql=
              %Q(CREATE TABLE `review_reasons_roles` (
              `review_reason_id` SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Foreign key to review_reasons table.',
              `role_id` INT(11) NOT NULL COMMENT 'Foreign key to roles table.',
              KEY `cr_reason_roles_review_reason_id` (`review_reason_id`),
              KEY `cr_reasons_roles_role_id` (`role_id`),
              CONSTRAINT `FK_review_reasons_roles_reason` FOREIGN KEY (`review_reason_id`) REFERENCES `review_reasons` (`id`),
              CONSTRAINT `FK_review_reasons_roles_role` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
            ) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='Links a review_reason to role';)
    sqls << create_review_reasons_roles_table_sql

    create_review_audits_review_reasons_table_sql=
              %Q(CREATE TABLE `review_audits_review_reasons` (
              `review_audit_id` INT(11) UNSIGNED NOT NULL COMMENT 'Foreign key to review_audits table.',
              `review_reason_id` SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Foreign key to review_reasons table.',
              KEY `review_audits_review_reasons_review_audit_id` (`review_audit_id`),
              KEY `review_audits_review_reasons_review_reason_id` (`review_reason_id`),
              CONSTRAINT `FK_review_audits_review_reasons_review_audit` FOREIGN KEY (`review_audit_id`) REFERENCES `review_audits` (`id`),
              CONSTRAINT `FK_review_audits_review_reasons_review_reason` FOREIGN KEY (`review_reason_id`) REFERENCES `review_reasons` (`id`)
            ) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='Links a review_reason to review_audit';)
    sqls << create_review_audits_review_reasons_table_sql


    for sql in sqls
      execute sql
    end
  end

  def self.down
    drop_table :review_reasons_roles
    drop_table :review_audits_review_reasons
    drop_table :review_reasons
    drop_table :review_audits
  end
end
