class AddSongLibraryUploadToSoundoutReport < ActiveRecord::Migration[4.2]
  def change
    add_column :soundout_reports, :song_library_upload_id, :integer, :null => true
    change_column :soundout_reports, :song_id, :integer, :null => true
  end
end
