class AddAutoIncrementToCountries < ActiveRecord::Migration[4.2]
  def change
    execute "ALTER TABLE countries MODIFY COLUMN id SMALLINT AUTO_INCREMENT;"
  end
end
