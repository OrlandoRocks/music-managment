class ChangeDecimalPrecisionForAmountBalanceAdjustment < ActiveRecord::Migration[4.2]
  def change
    change_column :balance_adjustments, :debit_amount, :decimal, precision: 23, scale: 14
    change_column :balance_adjustments, :credit_amount, :decimal, precision: 23, scale: 14
  end
end
