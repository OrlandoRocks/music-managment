class AddMuveToSalepointableStores < ActiveRecord::Migration[4.2]
  def self.up
    storeid = Store.find_by(name: "Muve Music").id
    if storeid !=nil
    execute ("insert into salepointable_stores (salepointable_type,store_id) values ('Album','#{storeid}')")
    execute ("insert into salepointable_stores (salepointable_type,store_id) values ('Single','#{storeid}')")
    else
      raise "No muve music in store db"
    end
  end

  def self.down
    storeid = Store.find_by(name: "Muve Music").id
        if storeid !=nil
          execute("delete from salepointable_stores where store_id = #{storeid}")
        else
          raise "no muve music in store DB"
        end
  end
end
