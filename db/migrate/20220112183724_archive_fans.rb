class ArchiveFans < ActiveRecord::Migration[6.0]
  def self.up
    if table_exists? :fans
      safety_assured do
        rename_table :fans, :archive_fans
      end
    end
  end

  def self.down
    if table_exists? :archive_fans
      safety_assured do
        rename_table :archive_fans, :fans
      end
    end
  end
end
