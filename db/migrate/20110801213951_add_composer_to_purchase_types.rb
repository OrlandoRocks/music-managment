class AddComposerToPurchaseTypes < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE purchases CHANGE COLUMN `related_type` `related_type` ENUM('Album','Single','Ringtone','Entitlement','Product','Renewal','ItunesUserReport','Video','Salepoint','Song','Widget','Composer') NULL DEFAULT NULL  ;")
  end

  def self.down
    execute("ALTER TABLE purchases CHANGE COLUMN `related_type` `related_type` ENUM('Album','Single','Ringtone','Entitlement','Product','Renewal','ItunesUserReport','Video','Salepoint','Song','Widget') NULL DEFAULT NULL  ;")
  end
end
