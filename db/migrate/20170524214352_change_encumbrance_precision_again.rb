class ChangeEncumbrancePrecisionAgain < ActiveRecord::Migration[4.2]
  def up
    change_column :encumbrance_details, :transaction_amount, :decimal, precision: 12, scale: 2
    change_column :encumbrance_summaries, :outstanding_amount, :decimal, precision: 12, scale: 2

    StoredProcedureLoader.load
  end

  def down
    change_column :encumbrance_details, :transaction_amount, :decimal, precision: 23, scale: 14
    change_column :encumbrance_summaries, :outstanding_amount, :decimal, precision: 23, scale: 14
  end
end
