class AddProductInformationForSalepointSubscriptions < ActiveRecord::Migration[4.2]
  def self.up
    if !(Rails.env.test?)
      prod_attrs = {
                   :created_by_id => 1,
                   :name => "Store Automator",
                   :display_name => "Store Automator",
                   :description => "Release automatically distributed to future stores",
                   :product_type => "Ad Hoc",
                   :status => "Active",
                   :is_default => true,
                   :price => 10.00,
                   :currency => "USD",
                   :country_website_id => 1
                   }
      us_prod = Product.create!(prod_attrs)
      prod_attrs[:currency] = "CAD"
      prod_attrs[:country_website_id] = 2
      cad_prod = Product.create!(prod_attrs)

      base_item_attrs = {
                        :name => "Store Automator",
                        :description => "Release automatically distributed to future stores",
                        :currency => "USD",
                        :price => 10.00
                        }
      us_base_item = BaseItem.create!(base_item_attrs)
      base_item_attrs[:currency] = "CAD"
      cad_base_item = BaseItem.create!(base_item_attrs)

      prod_item_attrs = {
                        :product => us_prod,
                        :base_item => us_base_item,
                        :name => "Store Automator",
                        :description => "Release automatically distributed to future stores",
                        :price => 10.00,
                        :currency => "USD"
                        }
      us_prod_item = ProductItem.create!(prod_item_attrs)
      prod_item_attrs[:currency] = "CAD"
      prod_item_attrs[:base_item] = cad_base_item
      prod_item_attrs[:product] = cad_prod
      cad_prod_item = ProductItem.create!(prod_item_attrs)

      base_item_options_attrs = {
                               :base_item => us_base_item,
                               :sort_order => 1,
                               :parent_id => 0,
                               :name => "Store Automator",
                               :product_type => "Ad Hoc",
                               :option_type => "required",
                               :rule_type => "price_only",
                               :rule => "price_for_each",
                               :quantity => 1,
                               :unlimited => 0,
                               :true_false => 0,
                               :minimum => 0,
                               :maximum => 0,
                               :price => 10.00,
                               :currency => "USD",
                               :inventory_type => "SalepointSubscription"
                               }
      us_base_item_option = BaseItemOption.create!(base_item_options_attrs)
      base_item_options_attrs[:currency] = "CAD"
      base_item_options_attrs[:base_item] = cad_base_item
      cad_base_item_option = BaseItemOption.create!(base_item_options_attrs)

      prod_item_rules_attrs = {
                              :product_item => us_prod_item,
                              :base_item_option => us_base_item_option,
                              :rule_type => "price_only",
                              :rule => "price_for_each",
                              :quantity => 1,
                              :unlimited => 0,
                              :true_false => 0,
                              :minimum => 0,
                              :maximum => 0,
                              :entitlement_rights_group_id => 0,
                              :price => 10.00,
                              :currency => "USD",
                              :inventory_type => "SalepointSubscription"
                              }
      us_prod_item_rules = ProductItemRule.create!(prod_item_rules_attrs)
      prod_item_rules_attrs[:currency] = "CAD"
      prod_item_rules_attrs[:base_item_option] = cad_base_item_option
      prod_item_rules_attrs[:product_item] = cad_prod_item
      cad_prod_item_rules = ProductItemRule.create!(prod_item_rules_attrs)
    end
  end

  def self.down
  end
end
