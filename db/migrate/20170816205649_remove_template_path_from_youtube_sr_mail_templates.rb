class RemoveTemplatePathFromYoutubeSrMailTemplates < ActiveRecord::Migration[4.2]
  def up
    remove_column :youtube_sr_mail_templates, :template_path
  end

  def down
    add_column :youtube_sr_mail_templates, :template_path, :string
  end
end
