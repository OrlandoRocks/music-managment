class AddValidationsToSourceCurrency < ActiveRecord::Migration[6.0]
  def change
    safety_assured { change_column :payoneer_ach_fees, :source_currency, 'char(3)', default: nil, null: false}
  end
end
