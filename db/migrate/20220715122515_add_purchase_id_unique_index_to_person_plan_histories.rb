class AddPurchaseIdUniqueIndexToPersonPlanHistories < ActiveRecord::Migration[6.0]
  def up
    Plans::DuplicatePersonPlanHistoriesService.call
    remove_index :person_plan_histories, :purchase_id
    add_index :person_plan_histories, :purchase_id, unique: true
  end

  def down
    remove_index :person_plan_histories, :purchase_id
    add_index :person_plan_histories, :purchase_id
  end
end
