class AddCreatedWithSongwriterToAlbum < ActiveRecord::Migration[4.2]
  def change
    add_column :albums, :created_with_songwriter, :boolean, default: false
  end
end
