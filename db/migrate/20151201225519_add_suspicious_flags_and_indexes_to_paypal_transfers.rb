class AddSuspiciousFlagsAndIndexesToPaypalTransfers < ActiveRecord::Migration[4.2]
  def change
    add_column :paypal_transfers, :suspicious_flags, :text
    add_index :paypal_transfers, :paypal_address
  end
end
