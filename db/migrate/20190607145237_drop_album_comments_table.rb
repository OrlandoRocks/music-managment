class DropAlbumCommentsTable < ActiveRecord::Migration[4.2]
  def up
    drop_table :album_comments
  end

  def down
    create_table :album_comments do |t|
      t.column :album_id, :integer
      t.column :person_id, :integer
      t.column :created_at, :datetime
      t.column :comment, :text
    end
  end
end
