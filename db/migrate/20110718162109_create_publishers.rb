class CreatePublishers < ActiveRecord::Migration[4.2]
  def self.up
    create_table :publishers do |t|
      t.string :name, :limit => 100
      t.string :cae, :limit => 11
      t.string :pro_affiliation, :limit => 20
      t.timestamps
    end
  end

  def self.down
    drop_table :publishers
  end
end
