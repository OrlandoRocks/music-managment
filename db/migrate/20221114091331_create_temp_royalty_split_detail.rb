class CreateTempRoyaltySplitDetail < ActiveRecord::Migration[6.0]
  def change
    create_table :temp_royalty_split_details do |t|
      t.belongs_to :person, null: false
      t.belongs_to :royalty_split, null: false
      t.belongs_to :song, null: false
      t.belongs_to :owner, comment: "Main Account Holder person_id for split, the only admin",
                           foreign_key: { to_table: :people },
                           type: :integer,
                           unsigned: true,
                           null: false
      t.integer :corporate_entity_id, null: false, index: true
      t.decimal :transaction_amount_in_usd, precision: 23, scale: 14
      t.string  :currency, limit: 3, null: false, index: true
      t.text :royalty_split_config
      t.boolean :processed, :default=>false
      t.timestamps
    end
  end
end
