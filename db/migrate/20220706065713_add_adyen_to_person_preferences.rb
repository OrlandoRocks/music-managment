class AddAdyenToPersonPreferences < ActiveRecord::Migration[6.0]
  def change
    add_column :person_preferences, :preferred_adyen_payment_method_id, :bigint
    add_foreign_key :person_preferences,
                    :adyen_stored_payment_methods,
                    column: :preferred_adyen_payment_method_id

    reversible do |dir|
      dir.up do
        safety_assured do
          execute <<-SQL
            SET @old_alter_algorithm = @@alter_algorithm;
          SQL

          execute <<-SQL
            SET SESSION alter_algorithm='INSTANT';
          SQL

          execute <<-SQL
            ALTER TABLE `person_preferences`
            MODIFY COLUMN preferred_payment_type ENUM('PayPal', 'CreditCard', 'Adyen') NOT NULL DEFAULT 'CreditCard'
            COMMENT 'which payment type to use';
          SQL

          execute <<-SQL
            SET SESSION alter_algorithm = @old_alter_algorithm;
          SQL
        end
      end

      dir.down do
        execute <<-SQL
          ALTER TABLE `person_preferences`
          MODIFY COLUMN preferred_payment_type ENUM('PayPal', 'CreditCard') NOT NULL DEFAULT 'CreditCard'
          COMMENT 'which payment type to use'
        SQL
      end
    end
  end
end
