class CreatePersonPlanHistoriesTable < ActiveRecord::Migration[6.0]
  def change
    create_table :person_plan_histories_tables do |t|
      t.references :person, null: false
      t.references :plan, null: false
      t.timestamps
    end
  end
end
