class DropDropkloudTables < ActiveRecord::Migration[4.2]
  ##reference: see 20170203205717_destroy_dropkloud_tables

  def up
    execute 'drop table if exists dropkloud_purchases'
    execute 'drop table if exists dropkloud_products'
    execute 'drop table if exists dropkloud_subscriptions'
  end

  def down
  end
end
