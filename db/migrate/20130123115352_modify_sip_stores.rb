class ModifySipStores < ActiveRecord::Migration[4.2]
  def self.up
    begin
      execute "ALTER TABLE sip_stores ADD COLUMN `store_id_old` smallint(5) unsigned DEFAULT NULL AFTER store_id, ADD COLUMN `display_group_id_old` smallint(5) unsigned DEFAULT NULL AFTER display_group_id"
      sip_store_array = SipStore.all
      sip_store_array.each do |ss|
        ss.store_id_old = ss.store_id
        ss.display_group_id_old = ss.display_group_id
        ss.save
      end
      itunes_sip_store_array = SipStore.where("left(name, 6) = 'iTunes' OR left(name, 6) = 'iCloud'")
      itunes_sip_stores_display_group = SipStoresDisplayGroup.find_or_create_by(name: "iTunes", sort_order: 0)
      itunes_store = Store.where("short_name = 'iTunesWW'").first
      itunes_sip_store_array.each do |itss|
        itss.display_group_id = itunes_sip_stores_display_group.id
        itss.store_id = itunes_store.id
        itss.save
      end
      itunes_store.name = "iTunes"
      itunes_store.save
      SipStore.where("name = 'LimeWire Store'").first.update_attribute(:display_on_status_page, 0)
      SipStore.where("name = 'Shockhound'").first.update_attribute(:display_on_status_page, 0)
    rescue StandardError => e
    end
  end

  def self.down
    begin
      SipStore.where("name = 'LimeWire Store'").first.update_attribute(:display_on_status_page, 1)
      SipStore.where("name = 'Shockhound'").first.update_attribute(:display_on_status_page, 1)
      itunes_store = Store.where("short_name = 'iTunesWW'").first
      itunes_store.name = "iTunes Worldwide"
      sip_store_array = SipStore.all
      sip_store_array.each do |ss|
        ss.store_id = ss.store_id_old
        ss.display_group_id = ss.display_group_id_old
        ss.save
      end
      change_table(:sip_stores) do |t|
        t.remove :store_id_old
        t.remove :display_group_id_old
      end
      itunes_sip_stores_display_group = SipStoresDisplayGroup.where("name = 'iTunes' and sort_order = 0").first
      itunes_sip_stores_display_group.destroy
      rescue StandardError => e
    end
  end
end
