class AddUkRelatedProducts < ActiveRecord::Migration[4.2]
  def up
    begin
      DropkloudProduct.create(product_name: "Dropkloud", product_type: "unlimited_monthly", product: Product.where("name = 'Dropkloud' and country_website_id = 3").first, term_length: 1)
    rescue
      p "DropkloudProduct does not exist"
    end

    FacebookRecognitionServiceProduct.create(product_name: "Facebook audio recognition", min_releases: 0, max_releases: 1, product: Product.where("name = 'Facebook audio recognition' and country_website_id = 3 and price = 2.25").first)
    FacebookRecognitionServiceProduct.create(product_name: "Facebook audio recognition", min_releases: 2, max_releases: 10, product: Product.where("name = 'Facebook audio recognition' and country_website_id = 3 and price = 5.00").first)
    FacebookRecognitionServiceProduct.create(product_name: "Facebook audio recognition", min_releases: 11, max_releases: 999999999, product: Product.where("name = 'Facebook audio recognition' and country_website_id = 3 and price = 10.00").first)

    SoundoutProduct.create(report_type: "starter", display_name: "TrackSmarts Starter Report", number_of_reviews: 40, available_in_days: 5, product: Product.where("name = 'TrackSmarts Starter' and country_website_id = 3").first)
    SoundoutProduct.create(report_type: "enhanced", display_name: "TrackSmarts Enhanced Report", number_of_reviews: 100, available_in_days: 5, product: Product.where("name = 'TrackSmarts Enhanced' and country_website_id = 3").first)
    SoundoutProduct.create(report_type: "premium", display_name: "TrackSmarts Premium Report", number_of_reviews: 225, available_in_days: 5, product: Product.where("name = 'TrackSmarts Premium' and country_website_id = 3").first)
  end

  def down
  end
end
