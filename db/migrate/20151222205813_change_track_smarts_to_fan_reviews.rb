class ChangeTrackSmartsToFanReviews < ActiveRecord::Migration[4.2]
  def up
    products = Product.where(name: ["TrackSmarts Starter", "TrackSmarts Enhanced", "TrackSmarts Premium"])
    products.each do |product|
      product.name.gsub!("TrackSmarts", "TuneCore Fan Reviews")
      product.save
    end

    SoundoutProduct.all.each do |soundout_product|
      soundout_product.display_name.gsub!("TrackSmarts", "TuneCore Fan Reviews")
      soundout_product.save
    end
  end

  def down
    products = Product.where(name: ["TuneCore Fan Reviews Starter", "TuneCore Fan Reviews Enhanced", "TuneCore Fan Reviews Premium"])
    products.each do |product|
      product.name.gsub!("TuneCore Fan Reviews", "TrackSmarts")
      product.save
    end

    SoundoutProduct.all.each do |soundout_product|
      soundout_product.display_name.gsub!("TuneCore Fan Reviews", "TrackSmarts")
      soundout_product.save
    end
  end
end
