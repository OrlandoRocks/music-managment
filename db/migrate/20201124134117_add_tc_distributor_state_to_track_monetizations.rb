class AddTcDistributorStateToTrackMonetizations < ActiveRecord::Migration[6.0]
  def change
    add_column :track_monetizations, :tc_distributor_state, :string
  end
end
