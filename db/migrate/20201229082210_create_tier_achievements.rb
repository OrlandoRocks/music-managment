class CreateTierAchievements < ActiveRecord::Migration[6.0]
  def change
    create_table :tier_achievements do |t|
      t.references :tier, foreign_key: true
      t.references :achievement, foreign_key: true

      t.timestamps
    end
  end
end
