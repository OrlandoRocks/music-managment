class AddStateColumnsToCompositions < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE `compositions` ADD `state` varchar(25),
      ADD `prev_state` varchar(25),
      ADD `state_updated_on` date;"
    )
  end

  def self.down
    remove_column :compositions, :state
    remove_column :compositions, :prev_state
    remove_column :compositions, :state_updated_on
  end
end
  
