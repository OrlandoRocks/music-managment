class AddIndexToMumaSongRegistrationHistory < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE muma_song_registration_history
      ADD INDEX idx_song_code_society_code_entry_created_at(song_code,society_code,entry_created_at)")
  end

  def self.down
    execute("ALTER TABLE muma_song_registration_history
          DROP INDEX idx_song_code_society_code_entry_created_at")
  end
  
end
