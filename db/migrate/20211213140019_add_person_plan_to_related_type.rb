class AddPersonPlanToRelatedType < ActiveRecord::Migration[6.0]

  def up
    safety_assured do
      execute "ALTER TABLE renewal_items 
      MODIFY COLUMN related_type enum(
        'Album',
        'Single',
        'Ringtone',
        'Entitlement',
        'Product',
        'Video',
        'Widget',
        'PersonPlan'
      ) NOT NULL"
    end
  end

  def down
    safety_assured do
      execute "ALTER TABLE renewal_items
      MODIFY COLUMN related_type enum(
        'Album',
        'Single',
        'Ringtone',
        'Entitlement',
        'Product',
        'Video',
        'Widget'
      ) NOT NULL"
    end
  end
end
