class AddingPaypalColumnsToPaymentBatches < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE `payment_batches` 
          ADD COLUMN `paypal_amount_total` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'Total Amount for Paypal in Batch' AFTER cannot_process_amount,
          ADD COLUMN `paypal_amount_successful` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'Total Successfully Paid by paypal in Batch' AFTER paypal_amount_total")
  end

  def self.down
    remove_column :payment_batches, :paypal_amount_total
    remove_column :payment_batches, :paypal_amount_successful
  end
end
