class AddCreatedWithColumnToAlbums < ActiveRecord::Migration[4.2]
  def change
    add_column :albums, :created_with, :string
  end
end
