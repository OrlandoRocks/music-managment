class DropPetriOrphans < ActiveRecord::Migration[6.0]
  def self.up
    drop_table :archive_petri_orphans
  end

  def self.down
    create_table :archive_petri_orphans do |t|
      t.column :created_at, :datetime, :null => false
      t.column :sqs_message_id, :string, :null => false
      t.column :sqs_message_body, :text, :null => false
    end
  end
end
