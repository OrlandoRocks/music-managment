class AddBusinessToPeople < ActiveRecord::Migration[4.2]
  def self.up
    add_column :people, :is_business, :boolean, :default => false
    add_column :people, :business_name, :string
    add_column :people, :mobile_number, :string
  end

  def self.down
    remove_column :people, :is_business
    remove_column :people, :business_name
    remove_column :people, :mobile_number
  end
end
