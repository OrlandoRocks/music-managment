class AddHashCodeToPeople < ActiveRecord::Migration[4.2]
  def up
    execute 'ALTER TABLE people ADD COLUMN hash_code varchar(255), ALGORITHM=INPLACE, LOCK=NONE;'
  end

  def down
    execute 'ALTER TABLE people DROP COLUMN hash_code;'
  end
end
