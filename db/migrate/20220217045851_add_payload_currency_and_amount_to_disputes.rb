class AddPayloadCurrencyAndAmountToDisputes < ActiveRecord::Migration[6.0]
  def change
    add_column :disputes, :payload_currency, :string
    add_column :disputes, :payload_amount, :integer
  end
end
