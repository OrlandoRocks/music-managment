class CreateTaxTokensCohorts < ActiveRecord::Migration[4.2]
  def change
    create_table :tax_tokens_cohorts do |t|
      t.datetime :start_date
      t.datetime :end_date
      t.timestamps
    end
  end
end
