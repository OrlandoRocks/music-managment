class CreateSubscriptionTablesAndCopyDataFromDropkloud < ActiveRecord::Migration[4.2]
  def up
    create_table :subscription_products do |t|
      t.string      :product_name, :product_type
      t.integer     :product_id, :term_length
      t.timestamps
    end

    create_table :subscription_purchases do |t|
      t.integer       :person_id, :subscription_product_id
      t.datetime      :effective_date, :termination_date
      t.timestamps
    end

    create_table :subscription_events do |t|
      t.integer :subscription_purchase_id, :person_subscription_status_id
      t.string  :event_type, :subscription_type
      t.timestamps
    end

    copy_dropkloud_products_data
    copy_dropkloud_purchases_data
    copy_dropkloud_subscription_events_data
  end

  def down
    drop_table :subscription_products
    drop_table :subscription_purchases
    drop_table :subscription_events
  end

  def copy_dropkloud_products_data
    execute "insert into subscription_products (product_name, product_type, product_id, term_length, created_at, updated_at) select product_name, product_type, product_id, term_length, created_at, updated_at from dropkloud_products"
  end

  def copy_dropkloud_purchases_data
    execute "insert into subscription_purchases (person_id, subscription_product_id, effective_date, termination_date, created_at, updated_at) select person_id, dropkloud_product_id, effective_date, termination_date, created_at, updated_at from dropkloud_purchases"
  end

  def copy_dropkloud_subscription_events_data
    execute "insert into subscription_events (subscription_purchase_id, person_subscription_status_id, event_type, subscription_type, created_at, updated_at) select dropkloud_purchase_id, person_subscription_status_id, event_type, 'Dropkloud', created_at, updated_at from dropkloud_subscription_events"
  end
end
