class AddRelatedToPayuInvoiceUploadResponses < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      add_column :payu_invoice_upload_responses, :related_id, :bigint
      add_column :payu_invoice_upload_responses, :related_type, :string
      add_index :payu_invoice_upload_responses, [:related_id, :related_type], name: "index_payu_responses_related"
    end
  end

  def down
    remove_column :payu_invoice_upload_responses, :related_id
    remove_column :payu_invoice_upload_responses, :related_type
  end
end
