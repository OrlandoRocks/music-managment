class AddUnallocatedSumColToCompositions < ActiveRecord::Migration[4.2]
  def self.up
    add_column :compositions, :is_unallocated_sum, :boolean, :default => 0
  end

  def self.down
    remove_column :compositions, :is_unallocated_sum
  end
end
