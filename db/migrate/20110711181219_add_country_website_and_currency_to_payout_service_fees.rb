class AddCountryWebsiteAndCurrencyToPayoutServiceFees < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE payout_service_fees ADD currency CHAR(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency for payout service fee' AFTER amount");
    execute("ALTER TABLE payout_service_fees ADD country_website_id int(11) unsigned DEFAULT 1 COMMENT 'Foreign key to country_websites' AFTER id");
  end

  def self.down
    remove_column :payout_service_fees, :currency
    remove_column :payout_service_fees, :country_website_id
  end
end
