class AddColumnSiftOrderIdToPersonSiftScores < ActiveRecord::Migration[6.0]
  def change
    add_column :person_sift_scores, :sift_order_id, :string
    add_index :person_sift_scores, :sift_order_id, unique: true
  end
end
