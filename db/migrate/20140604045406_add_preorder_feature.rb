class AddPreorderFeature < ActiveRecord::Migration[4.2]
  def up
    Feature.find_or_create_by(name: 'preorder', restrict_by_user: 1)
  end

  def down
    feature = Feature.find_by name: 'preorder'
    feature.destroy if feature
  end
end
