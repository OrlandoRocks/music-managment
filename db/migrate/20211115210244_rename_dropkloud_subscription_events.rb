class RenameDropkloudSubscriptionEvents < ActiveRecord::Migration[6.0]
  def self.up
    if table_exists? :dropkloud_subscription_events
      safety_assured do
        remove_index :dropkloud_subscription_events, :dropkloud_purchase_id
        rename_column :dropkloud_subscription_events, :dropkloud_purchase_id, :archive_dropkloud_puchase_id
        rename_table :dropkloud_subscription_events, :archive_dropkloud_subscription_events
        add_index :archive_dropkloud_subscription_events, :archive_dropkloud_puchase_id,
                  name: "index_arc_dropkloud_sub_events_on_arc_dropkloud_purchase_ids"
      end
    end
  end

  def self.down
    if table_exists? :archive_dropkloud_subscription_events
      safety_assured do
        remove_index :archive_dropkloud_subscription_events, :archive_dropkloud_purchase_id
        rename_column :archive_dropkloud_subscription_events, :archive_dropkloud_purchase_id, :dropkloud_puchase_id
        rename_table :archive_dropkloud_subscription_events, :dropkloud_subscription_events
        add_index :dropkloud_subscription_events, :dropkloud_puchase_id,
                  name: "index_dropkloud_sub_events_on_dropkloud_purchase_ids"
      end
    end
  end
end
