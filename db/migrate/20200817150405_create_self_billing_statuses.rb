class CreateSelfBillingStatuses < ActiveRecord::Migration[6.0]
  def change
    create_table :self_billing_statuses do |t|
      t.column :status, "enum('accepted','declined') NOT NULL"
      t.references :person
      t.timestamps
    end
  end
end
