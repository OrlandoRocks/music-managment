class AlterItunesComprensives < ActiveRecord::Migration[4.2]
  def self.up
    add_column :itunes_comprehensives, :raw_status, :string, :limit => 50
    add_column :itunes_comprehensives, :upload_created, :datetime
    add_column :itunes_comprehensives, :upload_state, :string, :limit => 50
    add_column :itunes_comprehensives, :upload_state_id, :integer
    add_column :itunes_comprehensives, :message, :text
  end

  def self.down
    remove_column :itunes_comprehensives, :raw_status
    remove_column :itunes_comprehensives, :upload_created
    remove_column :itunes_comprehensives, :upload_state
    remove_column :itunes_comprehensives, :upload_state_id
    remove_column :itunes_comprehensives, :message
  end
end
