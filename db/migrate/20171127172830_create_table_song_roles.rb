class CreateTableSongRoles < ActiveRecord::Migration[4.2]
  def change
    create_table :song_roles do |t|
      t.string :role_type
    end
  end
end
