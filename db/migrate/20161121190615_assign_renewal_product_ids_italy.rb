class AssignRenewalProductIdsItaly < ActiveRecord::Migration[4.2]
  def up
    ProductItem.where(name: "Album Distribution").each do |pi|
      pi.update_attribute :renewal_product_id, 364
    end
    ProductItem.where(name: "Single Distribution").each do |pi|
      pi.update_attribute :renewal_product_id, 365
    end
    ProductItem.where(name: "Ringtone Distribution").each do |pi|
      pi.update_attribute :renewal_product_id, 382
    end
  end

  def down
    ProductItem.where(name: "Album Distribution").each do |pi|
      pi.update_attribute :renewal_product_id, nil
    end
    ProductItem.where(name: "Single Distribution").each do |pi|
      pi.update_attribute :renewal_product_id, nil
    end
    ProductItem.where(name: "Ringtone Distribution").each do |pi|
      pi.update_attribute :renewal_product_id, nil
    end
  end
end
