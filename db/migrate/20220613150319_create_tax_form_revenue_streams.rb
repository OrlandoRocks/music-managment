class CreateTaxFormRevenueStreams < ActiveRecord::Migration[6.0]
  def change
    create_table :tax_form_revenue_streams do |t|
      t.references :tax_form,  null: false
      t.references :revenue_stream, null: false

      t.timestamps
    end
  end
end
