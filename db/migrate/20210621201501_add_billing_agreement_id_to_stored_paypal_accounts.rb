class AddBillingAgreementIdToStoredPaypalAccounts < ActiveRecord::Migration[6.0]
  def change
    add_column :stored_paypal_accounts, :billing_agreement_id, "char(19)"
    add_column :stored_paypal_accounts, :email, "varchar(50)"

    add_index :stored_paypal_accounts, :email
  end
end
