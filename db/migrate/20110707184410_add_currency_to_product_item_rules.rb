class AddCurrencyToProductItemRules < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE product_item_rules ADD currency CHAR(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency for product item rule' AFTER price")
  end

  def self.down
    remove_column :product_item_rules, :currency
  end
end
