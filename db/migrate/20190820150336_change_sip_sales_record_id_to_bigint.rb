class ChangeSipSalesRecordIdToBigint < ActiveRecord::Migration[4.2]
  def change
    change_column :sales_records_new, :sip_sales_record_id, :bigint
    change_column :sales_records, :sip_sales_record_id, :bigint
  end
end
