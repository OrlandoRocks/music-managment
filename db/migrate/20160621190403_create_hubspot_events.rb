class CreateHubspotEvents < ActiveRecord::Migration[4.2]
  def up
    create_table :hubspot_events do |t|
      t.string :event_id
      t.timestamps
    end

    create_table :marketing_event_triggers do |t|
      t.integer :related_id
      t.string :related_type
      t.string :event_trigger
      t.string :provider
      t.integer :provider_id
      t.timestamps
    end
  end

  def down
    drop_table :hubspot_events
    drop_table :marketing_event_triggers
  end


end
