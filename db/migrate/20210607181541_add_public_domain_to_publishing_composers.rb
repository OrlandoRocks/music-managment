class AddPublicDomainToPublishingComposers < ActiveRecord::Migration[6.0]
  def up
    add_column :publishing_compositions, :public_domain, :boolean
    change_column_default :publishing_compositions, :public_domain, false
  end

  def down
    remove_column :publishing_compositions, :public_domain
  end
end
