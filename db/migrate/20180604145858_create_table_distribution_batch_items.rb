class CreateTableDistributionBatchItems < ActiveRecord::Migration[4.2]
  def change
    create_table :distribution_batch_items do |t|
      t.integer :distribution_id
      t.integer :distribution_batch_id
      t.string :status, default: "present"
    end

    add_index :distribution_batch_items, :distribution_id
    add_index :distribution_batch_items, :distribution_batch_id
  end
end
