class AddColumnsToPersonPlanHistory < ActiveRecord::Migration[6.0]
  def change
    add_column :person_plan_histories, :change_type, :string, null: false
    add_column :person_plan_histories, :discount_reason, :string
    add_column :person_plan_histories, :plan_start_date, :datetime, null: false
    add_column :person_plan_histories, :plan_end_date, :datetime, null: false
  end
end
