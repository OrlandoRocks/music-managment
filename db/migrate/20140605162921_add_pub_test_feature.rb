class AddPubTestFeature < ActiveRecord::Migration[4.2]
  def self.up
    Feature.find_or_create_by(name: 'pubtest', restrict_by_user: 0)
  end

  def self.down
    feature = Feature.find_by(name: 'kickbacks')
    feature.destroy if feature
  end
end
