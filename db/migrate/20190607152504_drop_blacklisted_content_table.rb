class DropBlacklistedContentTable < ActiveRecord::Migration[4.2]
  def up
    drop_table :blacklisted_contents
  end

  def down
    execute(
      "CREATE TABLE `blacklisted_contents` (
      `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
      `title` varchar(127) NOT NULL,
      `artist` tinytext NOT NULL,
      `label` varchar(127) NOT NULL,
      `issue` tinytext NOT NULL,
      `sndx` varchar(64) DEFAULT NULL,
      `rsndx` varchar(64) DEFAULT NULL,
      `left_sndx` char(6) DEFAULT NULL,
      PRIMARY KEY (`id`),
      FULLTEXT KEY `sndx` (`sndx`),
      FULLTEXT KEY `rsndx` (`rsndx`),
      FULLTEXT KEY `left_sndx` (`left_sndx`),
      FULLTEXT KEY `title` (`title`),
      FULLTEXT KEY `title_2` (`title`,`artist`),
      FULLTEXT KEY `artist` (`artist`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;")
  end
end
