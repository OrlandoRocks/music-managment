class RenamePetriOrphans < ActiveRecord::Migration[6.0]
  def self.up
    safety_assured do
      rename_table :petri_orphans, :archive_petri_orphans
    end
  end

  def self.down
    safety_assured do
      rename_table :archive_petri_orphans, :petri_orphans
    end
  end
end
