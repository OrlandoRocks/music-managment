class ChangeBraintreeCustomerIdToString < ActiveRecord::Migration[4.2]
  def up
    change_column :people, :braintree_customer_id, :string
  end

  def down
    change_column :people, :braintree_customer_id, :integer
  end
end
