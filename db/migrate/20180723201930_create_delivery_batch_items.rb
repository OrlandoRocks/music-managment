class CreateDeliveryBatchItems < ActiveRecord::Migration[4.2]
  def change
    create_table :delivery_batch_items do |t|
      t.integer :deliverable_id
      t.string :deliverable_type
      t.integer :delivery_batch_id
      t.string :status, default: "present"
    end

    add_index :delivery_batch_items, :deliverable_id
    add_index :delivery_batch_items, :deliverable_type
    add_index :delivery_batch_items, :delivery_batch_id
  end
end
