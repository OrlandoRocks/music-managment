class AddIngroovesFontanaToSoundoutReports < ActiveRecord::Migration[4.2]
  def change
    add_column :soundout_reports, :ingrooves_fontana_submitted_at, :datetime, :default => nil
  end
end
