class AddPersonIdToCreatives < ActiveRecord::Migration[4.2]
  def up
    add_column :creatives, :person_id, :integer
    add_index :creatives, :person_id
  end

  def down
    remove_column :creatives, :person_id
  end
end
