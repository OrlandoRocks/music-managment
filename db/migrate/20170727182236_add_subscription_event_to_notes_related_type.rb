class AddSubscriptionEventToNotesRelatedType < ActiveRecord::Migration[4.2]
  def up
    # Add CreditUsage to the related_type column
    execute "ALTER TABLE notes MODIFY COLUMN related_type enum('Person','Album','Song','EftBatchTransaction', 'SubscriptionEvent') DEFAULT NULL COMMENT 'Type of entity.'"
  end

  def down
    execute "ALTER TABLE notes MODIFY COLUMN related_type enum('Person','Album','Song','EftBatchTransaction') DEFAULT NULL COMMENT 'Type of entity.'"
  end
end
