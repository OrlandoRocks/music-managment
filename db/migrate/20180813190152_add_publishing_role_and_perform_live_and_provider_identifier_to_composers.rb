class AddPublishingRoleAndPerformLiveAndProviderIdentifierToComposers < ActiveRecord::Migration[4.2]
  def change
    add_column :composers, :provider_identifier, :string
    add_column :composers, :perform_live, :boolean, default: false
    add_column :composers, :publishing_role_id, :integer
    add_column :composers, :performing_rights_organization_id, :integer

    add_index :composers, :provider_identifier
    add_index :composers, :publishing_role_id
    add_index :composers, :performing_rights_organization_id
  end
end
