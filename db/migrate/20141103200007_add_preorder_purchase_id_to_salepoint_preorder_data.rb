class AddPreorderPurchaseIdToSalepointPreorderData < ActiveRecord::Migration[4.2]
  def change
    add_column :salepoint_preorder_data, :preorder_purchase_id, :integer
  end
end
