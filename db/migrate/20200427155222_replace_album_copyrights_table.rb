class ReplaceAlbumCopyrightsTable < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      drop_table :album_copyrights

      create_table :copyrights do |t|
        t.references :copyrightable, polymorphic: true, index: true
        t.string     :ownership
        t.integer    :year
        t.string     :holder_name

        t.timestamps
      end
    end
  end
end
