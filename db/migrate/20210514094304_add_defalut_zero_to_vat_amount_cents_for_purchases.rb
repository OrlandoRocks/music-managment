class AddDefalutZeroToVatAmountCentsForPurchases < ActiveRecord::Migration[6.0]
  def change
    change_column_default :purchases, :vat_amount_cents, from: nil, to: 0
    change_column_default :purchases, :vat_amount_cents_in_eur, from: nil, to: 0
  end
end
