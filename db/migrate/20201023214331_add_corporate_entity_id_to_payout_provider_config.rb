class AddCorporateEntityIdToPayoutProviderConfig < ActiveRecord::Migration[6.0]
  def change
    safety_assured { add_reference :payout_provider_configs, :corporate_entity, index: true }
  end
end
