class RemovePaymentsOsTransactionIdFromPayuInvoiceUploadResponses < ActiveRecord::Migration[6.0]
  def change
    safety_assured { remove_column :payu_invoice_upload_responses, :payments_os_transaction_id }
  end
end
