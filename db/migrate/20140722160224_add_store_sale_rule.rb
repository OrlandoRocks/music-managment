class AddStoreSaleRule < ActiveRecord::Migration[4.2]
  def up
    #Create new rules that conditionally subtract 1.48 based on return of salepoint.store.on_sale?
    if !Rails.env.test?
      ProductItemRule.create!(
        :product_item_id=>4,
        :parent_id=>nil,
        :rule_type=>"price_only",
        :rule=>"true_false",
        :inventory_type=>"",
        :model_to_check=>"Salepoint",
        :method_to_check=>"store.on_sale?",
        :true_false=>1,
        :price=>-1.48,
        :currency=>"USD"
      )

      ProductItemRule.create!(
        :product_item_id=>58,
        :parent_id=>nil,
        :rule_type=>"price_only",
        :rule=>"true_false",
        :inventory_type=>"",
        :model_to_check=>"Salepoint",
        :method_to_check=>"store.on_sale?",
        :true_false=>1,
        :price=>-1.48,
        :currency=>"CAD"
      )
    end
  end

  def down
    product_item_rules = ProductItemRule.where("method_to_check = ?", "store.on_sale?")
    product_item_rules.each { |pi| pi.destroy }
  end
end
