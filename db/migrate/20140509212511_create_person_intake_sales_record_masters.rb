class CreatePersonIntakeSalesRecordMasters < ActiveRecord::Migration[4.2]
  def up
    sql = %Q{
    CREATE TABLE `person_intake_sales_record_masters` (
      `person_intake_id` int(11) unsigned NOT NULL,
      `sales_record_master_id` int(11) unsigned NOT NULL,
      PRIMARY KEY (`person_intake_id`,`sales_record_master_id`),
      KEY `sales_record_master_id` (`sales_record_master_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;}
    ActiveRecord::Base.connection.execute(sql)

    sql = %Q{
      CREATE PROCEDURE `populate_person_intake_sales_record_masters`()
      BEGIN
        DECLARE done BOOLEAN DEFAULT 0;
        DECLARE var_srm_id int;

        DECLARE cur1 cursor for
        SELECT srm.id from sales_record_masters srm where srm.id not in ( select distinct sales_record_master_id from person_intake_sales_record_masters );

        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

        OPEN cur1;
        fetch cur1 into var_srm_id;

        WHILE( !done ) DO

          select NOW();
          select var_srm_id;

          START TRANSACTION;
           insert into person_intake_sales_record_masters
           select person_intake_id, sales_record_master_id from sales_records where sales_record_master_id = var_srm_id group by person_intake_id, sales_record_master_id;
          COMMIT;

          fetch cur1 into var_srm_id;

        END WHILE;
        CLOSE cur1;
      END;;
    }
    ActiveRecord::Base.connection.execute(sql)
  end

  def down
    ActiveRecord::Base.connection.execute("drop table person_intake_sales_record_masters")
    ActiveRecord::Base.connection.execute("drop procedure populate_person_intake_sales_record_masters")
  end
end
