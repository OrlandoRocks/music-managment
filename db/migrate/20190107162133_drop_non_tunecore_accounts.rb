class DropNonTunecoreAccounts < ActiveRecord::Migration[4.2]
  def change
    drop_table :non_tunecore_accounts
  end
end
