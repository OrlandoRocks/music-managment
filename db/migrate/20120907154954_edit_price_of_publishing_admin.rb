class EditPriceOfPublishingAdmin < ActiveRecord::Migration[4.2]
  def self.up
    #editing publishing admin price to $75
    pub_admin_products = Product.where(name: SONGWRITER_PRODUCT_NAME)
    pub_admin_products.each do |p|
      Product.transaction do
        p.update_attribute(:price, 75)
        product_item = p.product_items.first rescue nil
        product_item_rule = p.product_items.first.product_item_rules.first rescue nil

        if product_item
          #Changing BaseItem and BaseItemOption too
          product_item.base_item.base_item_options.first(:price, 75)
        end

        if product_item_rule
          product_item_rule.update_attribute(:price, 75)
          product_item_rule.base_item_option.update_attribute(:price, 75)
        end
      end
    end
  end

  def self.down
    pub_admin_products = Product.where(name: SONGWRITER_PRODUCT_NAME)
    pub_admin_products.each do |p|
      Product.transaction do
        p.update_attribute(:price, 49.99)
        product_item = p.product_items.first rescue nil
        product_item_rule = p.product_items.first.product_item_rules.first rescue nil

        if product_item
          #Changing BaseItem and BaseItemOption too
          product_item.base_item.base_item_options.first(:price, 49.99)
        end

        if product_item_rule
          product_item_rule.update_attribute(:price, 49.99)
          product_item_rule.base_item_option.update_attribute(:price, 49.99)
        end
      end
    end
  end
end
