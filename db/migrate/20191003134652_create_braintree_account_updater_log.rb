class CreateBraintreeAccountUpdaterLog < ActiveRecord::Migration[4.2]
  def change
    create_table :braintree_account_updater_logs do |t|
      t.string :token
      t.string :update_type
      t.column :cc_last_four_digits, 'char(4)'
      t.string :expiration_date
      t.string :status
      t.string :message

      t.timestamps
    end
  end
end
