class ChangeLockReasonToVarchar < ActiveRecord::Migration[4.2]
  def up
    execute(
      "ALTER TABLE people MODIFY lock_reason varchar(255);"
    )
  end

  def down
  end
end
