class AddItunesPanasiaToStoreGroup < ActiveRecord::Migration[4.2]
    def self.up
      storeid = Store.find_by(name: "iTunes Asia").id
      if storeid != nil
        execute ("insert into store_group_stores (store_group_id,store_id) values ('1','#{storeid}')")
      else
        raise "No store found for iTunes Asia"
      end
    end

    def self.down
      storeid = Store.find_by(name: "iTunes Asia").id
      if storeid != nil
        execute("delete from store_group_stores where store_id = #{storeid}")
      end
    end
  end
