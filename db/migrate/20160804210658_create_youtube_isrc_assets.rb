class CreateYoutubeIsrcAssets < ActiveRecord::Migration[4.2]
  def up
    create_table :youtube_isrc_assets do |t|
      t.string    :tunecore_isrc,    :null => false
      t.string    :youtube_asset_id,    :null => false
      t.text      :api_response, null: false
      t.timestamps
    end
  end

  def down
    drop_table :youtube_isrc_assets
  end
end
