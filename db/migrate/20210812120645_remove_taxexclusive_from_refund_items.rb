class RemoveTaxexclusiveFromRefundItems < ActiveRecord::Migration[6.0]
  def change
    safety_assured { remove_column :refund_items, :tax_inclusive, :boolean, default: false }
  end
end
