class AddCloneTargetedOfferRight < ActiveRecord::Migration[4.2]
  CONTROLLER_NAME       = "admin/targeted_offers"
  CLONE_ACTION          = "clone"
  CLONE_NAME            = "targeted_offers/clone"

  ROLE_NAME             = "Targeted Offers"

  def self.up
    rights = []

    rights << Right.create!( :controller=>CONTROLLER_NAME, :action=>CLONE_ACTION, :name=>CLONE_NAME )

    to_role = Role.find_or_create_by(name: ROLE_NAME)

    rights.each do |right|

      to_role.rights << right

    end

    to_role.save!
  end

  def self.down
    clone_right   = Right.where("controller=:controller and action=:action and name=:name", {:controller=>CONTROLLER_NAME, :action=>CLONE_ACTION,    :name=>CLONE_NAME }).first

    #HABTM relationship cleans up the join table on destroy
    clone_right.destroy
  end

end
