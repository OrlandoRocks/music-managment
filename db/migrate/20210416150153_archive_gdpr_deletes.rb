class ArchiveGdprDeletes < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      return unless ActiveRecord::Base.connection.table_exists?(:gdpr_deletes)

      rename_table :gdpr_deletes, :archive_gdpr_deletes
    end
  end
end
