class DropPersonSiftScoreLogsTable < ActiveRecord::Migration[6.0]
  def up
    drop_table :person_sift_score_logs
  end

  def down
    create_table :person_sift_score_logs do |t|
      t.references :person
      t.references :person_sift_score

      t.integer :score, null: false, :limit => 1
      t.integer :related_id
      t.string :related_type
      t.text :raw_response

      t.datetime :created_at, null: false
    end
  end
end
