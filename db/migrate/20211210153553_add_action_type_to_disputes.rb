class AddActionTypeToDisputes < ActiveRecord::Migration[6.0]
  def change
    safety_assured {
      execute "ALTER TABLE `disputes` MODIFY `action_type` ENUM('accepted','dispute_initiated','dispute_completed')"
    }
  end
end
