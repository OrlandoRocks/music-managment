class CreateAppleArtistIds < ActiveRecord::Migration[4.2]
  def change
    create_table :apple_artist_ids do |t|
      t.integer :person_id, null: false
      t.integer :apple_artist_id, null: false
      t.timestamps
    end

    add_index :apple_artist_ids, [:person_id, :apple_artist_id], unique: true
  end
end
