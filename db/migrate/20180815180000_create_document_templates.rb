class CreateDocumentTemplates < ActiveRecord::Migration[4.2]
  def change
    create_table :document_templates do |t|
      t.string   :template_id, unique: true, null: false
      t.string   :document_title, null: false
      t.string   :document_type, null: false
      t.string   :description
      t.text     :raw_response

      t.datetime :last_revision_at
      t.timestamps
    end

    add_index :document_templates, :document_type, using: 'btree'
    add_index :document_templates, [
      :document_title, :document_type
    ], name: 'index_type_with_title_by_revision', unique: true, using: 'btree'

  end
end
