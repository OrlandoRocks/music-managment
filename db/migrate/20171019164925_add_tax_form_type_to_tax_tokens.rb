class AddTaxFormTypeToTaxTokens < ActiveRecord::Migration[4.2]
  def change
    add_column :tax_tokens, :tax_form_type, :string
  end
end
