class NewBelieveLiveStore < ActiveRecord::Migration[4.2]
  def up
    old_store = Store.find(73)
    new_store = old_store.dup # believe export

    new_store.id = 75
    new_store.name = "Believe Live"
    new_store.abbrev = "bl"
    new_store.short_name = "BelieveLiv"
    new_store.position = 691

    old_store.abbrev = "be"
    old_store.short_name = "BelieveExp"
    old_store.save!
    new_store.save!
  end

  def down
    Store.delete(75)
    old_store = Store.find(73)
    old_store.abbrev = "believ"
    old_store.short_name = "Believe"
    old_store.save!
  end
end
