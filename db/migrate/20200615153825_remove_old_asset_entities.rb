class RemoveOldAssetEntities < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      unless Rails.env.test? || Rails.env.development?
        rename_table :s3_assets_rackspace,   :archive_s3_assets_rackspace
        remove_column :songs,                :mp3_ident
      end

      rename_table :song_assets,             :archive_song_assets
      remove_column :songs,                  :s3_96kb_asset_id
      remove_column :songs,                  :upload_id
    end
  end
end
