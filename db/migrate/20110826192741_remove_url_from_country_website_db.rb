class RemoveUrlFromCountryWebsiteDb < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :country_websites, :url
  end

  def self.down
    execute("ALTER TABLE country_websites ADD COLUMN url char(20) DEFAULT NULL COMMENT 'base url' AFTER name")
    CountryWebsite.find(1).update(:url => 'www.tunecore.com')
    CountryWebsite.find(2).update(:url => 'ca.tunecore.com')
  end
end
