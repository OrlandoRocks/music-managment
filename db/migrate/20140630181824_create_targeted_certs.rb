class CreateTargetedCerts < ActiveRecord::Migration[4.2]
  def up
    create_table :targeted_certs do |t|
      t.string    :code, :limit => 16
      t.integer   :targeted_offer_id
      t.integer   :redeemed_by
      t.datetime  :redeemed_at
      t.timestamps
    end
  end

  def down
    drop_table :targeted_certs
  end
  
end
