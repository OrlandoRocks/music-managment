class AddSalepointPreorderDataToPurchaseTable < ActiveRecord::Migration[4.2]
  def self.up
    execute <<-SQL
      ALTER TABLE purchases
        MODIFY COLUMN related_type enum('Album','Single','Ringtone','Entitlement','Product','Renewal','ItunesUserReport','MusicVideo','FeatureFilm','Salepoint','Song','Widget','Composer','Video','SalepointSubscription','CreditUsage', 'Booklet', 'SalepointPreorderData') DEFAULT NULL    
    SQL
  end

  def self.down
    execute <<-SQL
      ALTER TABLE purchases
        MODIFY COLUMN related_type enum('Album','Single','Ringtone','Entitlement','Product','Renewal','ItunesUserReport','MusicVideo','FeatureFilm','Salepoint','Song','Widget','Composer','Video','SalepointSubscription','CreditUsage', 'Booklet') DEFAULT NULL
    SQL
  end
end
