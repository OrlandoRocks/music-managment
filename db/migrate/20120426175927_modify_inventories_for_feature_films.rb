class ModifyInventoriesForFeatureFilms < ActiveRecord::Migration[4.2]
  def self.up
    execute("alter table inventories change inventory_type inventory_type 
              enum('Album','Single','Ringtone','MusicVideo','FeatureFilm','ItunesUserReport','Booklet','Song','Salepoint','Widget','Video')")
    execute("alter table purchases change related_type related_type enum('Album','Single','Ringtone','Entitlement','Product','Renewal',
    'ItunesUserReport','MusicVideo','FeatureFilm','Salepoint','Song','Widget','Composer', 'Video')")
  end

  def self.down
    execute("alter table purchases change related_type related_type enum('Album',
    'Single','Ringtone','Entitlement','Product','Renewal','ItunesUserReport',
    'Salepoint','Song','Widget','Composer', 'Video')")
    execute("alter table inventories change inventory_type inventory_type 
              enum('Album','Single','Ringtone','ItunesUserReport','Booklet','Song','Salepoint','Widget','Video')")

  end
end
