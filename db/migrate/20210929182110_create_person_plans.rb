class CreatePersonPlans < ActiveRecord::Migration[6.0]
  def change
    create_table :person_plans do |t|
      t.references :person, null: false, index: { unique: true }
      t.references :plan, null: false
      t.datetime :expires_at, null: false
      t.timestamps
    end
  end
end
