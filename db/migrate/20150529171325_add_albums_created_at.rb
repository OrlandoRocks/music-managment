class AddAlbumsCreatedAt < ActiveRecord::Migration[4.2]
  def up
    add_column :albums, :created_at, :datetime
    execute("UPDATE albums set created_at = created_on")
  end

  def down
    remove_column :albums, :created_at
  end
end
