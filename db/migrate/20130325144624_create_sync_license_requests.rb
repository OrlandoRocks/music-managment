class CreateSyncLicenseRequests < ActiveRecord::Migration[4.2]
  def self.up
    create_table :sync_license_requests do |t|
      t.string            :status 
      t.text              :scene_description
      t.string            :duration
      t.string            :type_of_use
      t.column            :master_use, :boolean, :default => true
      
      t.integer           :song_id
      t.integer           :sync_license_production_id
      t.has_attached_file :request_document

      t.timestamps
    end
  end

  def self.down
    drop_table :sync_license_requests
  end
end
