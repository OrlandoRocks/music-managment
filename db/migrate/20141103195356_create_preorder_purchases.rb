class CreatePreorderPurchases < ActiveRecord::Migration[4.2]
  def change
    create_table :preorder_purchases do |t|
      t.integer :album_id, null: false
      t.boolean :itunes_enabled, null: false, default: false
      t.boolean :google_enabled, null: false, default: false
      t.datetime :paid_at
      t.timestamps
    end
    add_index :preorder_purchases, :album_id, unique: true
  end
end
