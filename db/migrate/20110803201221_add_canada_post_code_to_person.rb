class AddCanadaPostCodeToPerson < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE people
     ADD COLUMN canada_postal_code_id int(11) NULL")
  end

  def self.down
    remove_column :people, :canada_postal_code_id
  end
end
