class AddExtraColsToPersonEarningsTaxMetaDatum < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      add_column :person_earnings_tax_metadata, :tax_blocked_at, :datetime, null: true
      add_column :person_earnings_tax_metadata, :total_taxable_distribution_earnings, :decimal, default: 0.0, null: false, scale: 14, precision: 23
      add_column :person_earnings_tax_metadata, :total_taxable_publishing_earnings, :decimal, default: 0.0, null: false, scale: 14, precision: 23
      add_column :person_earnings_tax_metadata, :rollover_earnings, :decimal, default: 0.0, null: false, scale: 14, precision: 23
    end
  end
end
