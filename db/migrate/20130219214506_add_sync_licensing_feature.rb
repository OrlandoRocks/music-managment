class AddSyncLicensingFeature < ActiveRecord::Migration[4.2]
  def self.up
    Feature.find_or_create_by(:name=>'sync_licensing', :restrict_by_user => 1)
  end

  def self.down
    feature = Feature.find_by(name: 'sync_licensing')
    feature.destroy if feature
  end
end
