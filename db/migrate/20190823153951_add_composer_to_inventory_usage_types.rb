class AddComposerToInventoryUsageTypes < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE inventory_usages CHANGE COLUMN `related_type` `related_type` ENUM('Album','Single','Ringtone','Song','Salepoint','Video','Booklet','ItunesUserReport','ShippingLabel','Widget','Composer') NULL DEFAULT NULL  COMMENT 'the model name of the related inventory used';")
  end

  def self.down
    execute("ALTER TABLE inventory_usages CHANGE COLUMN `related_type` `related_type` ENUM('Album','Single','Ringtone','Song','Salepoint','Video','Booklet','ItunesUserReport','ShippingLabel','Widget') NULL DEFAULT NULL  COMMENT 'the model name of the related inventory used';")
  end
end
