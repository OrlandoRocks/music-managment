class AddUpdatedAtToArtwork < ActiveRecord::Migration[4.2]
  def self.up
    add_column :artworks, :artwork_updated_at, :datetime
    execute("update artworks set artwork_updated_at = NOW()");
  end

  def self.down
    remove_column :artworks, :artwork_updated_at
  end
end
