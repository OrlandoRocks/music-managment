class SetAlliTuneStoresAsReviewable < ActiveRecord::Migration[4.2]
  def up
    itune_store_ids = [1,2,3,4,5,6]
    itune_store_ids.each do |id|
      iTunes = Store.find(id)
      iTunes.reviewable = 1
      iTunes.save!
    end
  end

  def down
    itune_store_ids = [1,2,3,4,5,6]
    itune_store_ids.each do |id|
      iTunes = Store.find(id)
      iTunes.reviewable = 1
      iTunes.save!
    end
  end
end
