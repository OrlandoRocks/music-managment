class AddPriceToVariablePrices < ActiveRecord::Migration[4.2]
  def change
    add_column :variable_prices, :price, :decimal, precision: 6, scale: 2
  
    VariablePrice.all.each do |vp|
      m = /\d+\.\d{2}/.match(vp.price_code_display) || /\.\d{2}/.match(vp.price_code_display)
      price = m[0] if m
      vp.price = price
      vp.save!
    end
  end
end
