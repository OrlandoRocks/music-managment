class CreateCountryPhoneCodes < ActiveRecord::Migration[4.2]
  def up
    create_table :country_phone_codes do |t|
      t.references  :country
      t.text        :code
    end

    add_index :country_phone_codes, :country_id
  end

  def down
    drop_table :country_phone_codes
  end
end
