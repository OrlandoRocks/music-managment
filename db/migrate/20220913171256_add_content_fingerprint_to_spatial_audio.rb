class AddContentFingerprintToSpatialAudio < ActiveRecord::Migration[6.0]
  def change
    add_column :spatial_audios, :content_fingerprint, :string
  end
end
