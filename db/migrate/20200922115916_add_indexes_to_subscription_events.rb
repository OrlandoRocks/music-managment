class AddIndexesToSubscriptionEvents < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
       add_index :subscription_events, :subscription_type
    end
  end

  def down
    safety_assured do
       remove_index :subscription_events, :subscription_type
    end
  end
end
