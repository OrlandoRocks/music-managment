class AddNewTargetedOfferRights < ActiveRecord::Migration[4.2]

  CONTROLLER_NAME       = "admin/targeted_offers"
  ADD_PERSON_ACTION     = "add_person"
  ADD_PEOPLE_ACTION     = "add_people"
  REMOVE_PERSON_ACTION  = "remove_person"
  REMOVE_PEOPLE_ACTION  = "remove_people"
  ADD_PERSON_NAME       = "targeted_offers/add_person"
  ADD_PEOPLE_NAME       = "targeted_offers/add_people"
  REMOVE_PERSON_NAME    = "targeted_offers/remove_person"
  REMOVE_PEOPLE_NAME    = "targeted_offers/remove_people"

  ROLE_NAME             = "Targeted Offers"

  def self.up
    rights = []

    rights << Right.create!( :controller=>CONTROLLER_NAME, :action=>ADD_PERSON_ACTION,    :name=>ADD_PERSON_NAME    )
    rights << Right.create!( :controller=>CONTROLLER_NAME, :action=>REMOVE_PERSON_ACTION, :name=>REMOVE_PERSON_NAME )
    rights << Right.create!( :controller=>CONTROLLER_NAME, :action=>ADD_PEOPLE_ACTION,    :name=>ADD_PEOPLE_NAME )
    rights << Right.create!( :controller=>CONTROLLER_NAME, :action=>REMOVE_PEOPLE_ACTION, :name=>REMOVE_PEOPLE_NAME  )

    to_role = Role.find_or_create_by(name: ROLE_NAME)

    rights.each do |right|

      to_role.rights << right

    end

    to_role.save!
  end

  def self.down
    add_rights    = Right.where("controller=:controller and action=:action and name=:name", {:controller=>CONTROLLER_NAME, :name=>ADD_PERSON_NAME,    :action=>ADD_PERSON_ACTION }).first
    remove_rights = Right.where("controller=:controller and action=:action and name=:name", {:controller=>CONTROLLER_NAME, :name=>REMOVE_PERSON_NAME, :action=>REMOVE_PERSON_ACTION }).first
    add_people    = Right.where("controller=:controller and action=:action and name=:name", {:controller=>CONTROLLER_NAME, :name=>ADD_PEOPLE_NAME,    :action=>ADD_PEOPLE_ACTION }).first
    remove_people = Right.where("controller=:controller and action=:action and name=:name", {:controller=>CONTROLLER_NAME, :name=>REMOVE_PEOPLE_NAME, :action=>REMOVE_PEOPLE_ACTION }).first

    #HABTM relationship cleans up the join table on destroy
    add_rights.destroy
    remove_rights.destroy
    add_people.destroy
    remove_people.destroy
  end
end
