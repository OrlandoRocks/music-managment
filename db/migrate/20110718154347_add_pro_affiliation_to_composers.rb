# 2011-07-18 AK enum column for survey questions
# For pro_songwriter_affiliation_answer and pro_publisher_affiliation_answer, values are:
# 0 = I don't know
# 1 = Yes
# 2 = No
class AddProAffiliationToComposers < ActiveRecord::Migration[4.2]
  def self.up
    execute "ALTER TABLE composers ADD COLUMN pro_songwriter_affiliation_answer ENUM(\"I don't know\", 'Yes', 'No')"
    execute "ALTER TABLE composers ADD COLUMN pro_publisher_affiliation_answer ENUM(\"I don't know\", 'Yes', 'No')"
    change_table :composers do |t|
      t.string :pro_songwriter_affiliation, :limit => 20
    end
  end

  def self.down
    change_table :composers do |t|
      t.remove :pro_songwriter_affiliation_answer
      t.remove :pro_publisher_affiliation_answer
      t.remove :pro_songwriter_affiliation
    end
  end
end
