class AddExemptPayeeToTaxInfo < ActiveRecord::Migration[4.2]
  def self.up
    add_column :tax_info, :exempt_payee, :boolean, :default => 0
  end

  def self.down
    remove_column :tax_info, :exempt_payee
  end
end
