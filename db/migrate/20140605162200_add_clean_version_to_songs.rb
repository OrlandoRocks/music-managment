class AddCleanVersionToSongs < ActiveRecord::Migration[4.2]
  def change
    add_column :songs, :clean_version, :boolean, null: false, default: false
  end
end
