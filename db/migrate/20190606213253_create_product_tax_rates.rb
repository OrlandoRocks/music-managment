class CreateProductTaxRates < ActiveRecord::Migration[4.2]
  def change
    create_table :product_tax_rates do |t|
      t.decimal :tax_rate, precision: 10, scale: 2
      t.boolean :active_tax_rate
      t.string :description_label
      t.references :locale, polymorphic: true
      t.timestamps
    end

    add_index :product_tax_rates, [:locale_id, :locale_type]
  end
end
