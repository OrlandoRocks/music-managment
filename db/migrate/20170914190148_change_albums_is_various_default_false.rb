class ChangeAlbumsIsVariousDefaultFalse < ActiveRecord::Migration[4.2]
  def up
    change_column :albums, :is_various, :boolean, null: false
    change_column :albums, :is_various, :boolean, default: false
  end
end
