class AddCreditNoteInboundPrefixToInvoiceStaticCorporateEntity < ActiveRecord::Migration[6.0]
  def change
    add_column :invoice_static_corporate_entities, :credit_note_inbound_prefix, :string
  end
end
