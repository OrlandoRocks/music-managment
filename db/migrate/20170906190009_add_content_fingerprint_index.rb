class AddContentFingerprintIndex < ActiveRecord::Migration[4.2]
  def up
    add_index(:songs, [:content_fingerprint], name: 'by_content_fingerprint', length: {content_fingerprint: 32})
  end

  def down
    remove_index(:songs, name: 'by_content_fingerprint')
  end
end
