class CreateBlacklistedContents < ActiveRecord::Migration[4.2]
  def self.up
    upstring = %Q|
CREATE TABLE `blacklisted_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `artist` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `issue` varchar(64) DEFAULT NULL,
  `group_name` varchar(255) DEFAULT NULL,
  `sndx` varchar(32) DEFAULT NULL,
  `rsndx` varchar(32) DEFAULT NULL,
  `left_sndx` char(6) DEFAULT NULL,
  `siehe` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `sndx` (`sndx`),
  FULLTEXT KEY `rsndx` (`rsndx`),
  FULLTEXT KEY `left_sndx` (`left_sndx`),
  FULLTEXT KEY `title` (`title`),
  FULLTEXT KEY `title_2` (`title`,`artist`),
  FULLTEXT KEY `artist` (`artist`)
) ENGINE=MyISAM AUTO_INCREMENT=1417 DEFAULT CHARSET=utf8;|
    execute upstring

  end

  def self.down
    drop_table :blacklisted_contents
  end
end
