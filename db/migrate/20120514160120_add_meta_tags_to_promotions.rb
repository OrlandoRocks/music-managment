class AddMetaTagsToPromotions < ActiveRecord::Migration[4.2]
  def self.up
    add_column :promotions, :meta_keywords, :text
    add_column :promotions, :meta_description, :text
  end

  def self.down
    remove_column :promotions, :meta_keywords
    remove_column :promotions, :meta_description
  end

end
