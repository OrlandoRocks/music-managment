class AddTaxEntityNameOnTaxForms < ActiveRecord::Migration[6.0]
  def change
    add_column :tax_forms, :tax_entity_name, :string
  end
end
