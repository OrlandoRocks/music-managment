class AddPaymentsOsCountToPaymentBatches < ActiveRecord::Migration[6.0]
  def change
    add_column :payment_batches, :payments_os_amount_total, 'decimal(10,2)', null: false
    add_column :payment_batches, :payments_os_amount_successful, 'decimal(10,2)', null: false
  end
end
