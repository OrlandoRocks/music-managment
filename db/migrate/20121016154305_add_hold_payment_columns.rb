class AddHoldPaymentColumns < ActiveRecord::Migration[4.2]
  def self.up
    add_column :royalty_payments, :hold_payment, :boolean, :default => 0
    add_column :royalty_payments, :hold_type, :string
    add_column :batch_balance_adjustment_details, :hold_type, :string
  end

  def self.down
    remove_column :royalty_payments, :hold_payment
    remove_column :royalty_payments, :hold_type
    remove_column :batch_balance_adjustment_details, :hold_type
  end
end
