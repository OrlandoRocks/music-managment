class UpdatePaidAtDefaultValueOnPurchases < ActiveRecord::Migration[4.2]
  def up
    change_column_default :purchases, :paid_at, nil
  end

  def down
    change_column_default :purchases, :paid_at, '0000-00-00 00:00:00'
  end
end
