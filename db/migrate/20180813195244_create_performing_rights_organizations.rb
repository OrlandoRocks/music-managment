class CreatePerformingRightsOrganizations < ActiveRecord::Migration[4.2]
  def change
    create_table :performing_rights_organizations do |t|
      t.string :name
      t.string :cicaccode
      t.string :provider_identifier
      t.boolean :is_deleted, default: false

      t.timestamps
    end

    add_index :performing_rights_organizations, :name
    add_index :performing_rights_organizations, :cicaccode
    add_index :performing_rights_organizations, :provider_identifier
    add_index :performing_rights_organizations, :is_deleted
  end
end
