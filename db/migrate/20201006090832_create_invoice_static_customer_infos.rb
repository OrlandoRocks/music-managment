class CreateInvoiceStaticCustomerInfos < ActiveRecord::Migration[6.0]
  def change
    create_table :invoice_static_customer_infos do |t|
      t.integer :related_id, null: false
      t.string :related_type, null: false
      t.string :name
      t.string :person_currency
      t.string :customer_type
      t.string :vat_registration_number

      t.timestamps
    end
  end
end
