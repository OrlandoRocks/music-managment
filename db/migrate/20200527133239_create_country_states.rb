class CreateCountryStates < ActiveRecord::Migration[6.0]
  def change
    create_table :country_states, options: 'ENGINE=InnoDB DEFAULT CHARSET=utf8' do |t|
      t.string :name
      t.column :iso_code, 'char(5)'
      t.string :entity_type
      t.belongs_to :country
      t.timestamps
    end
  end
end
