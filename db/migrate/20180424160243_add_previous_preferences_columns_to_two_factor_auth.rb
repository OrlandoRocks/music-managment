class AddPreviousPreferencesColumnsToTwoFactorAuth < ActiveRecord::Migration[4.2]  
  def change
    add_column :two_factor_auths, :previous_phone_number, :string
    add_column :two_factor_auths, :previous_notification_method, :string
    add_column :two_factor_auths, :previous_country_code, :string
  end
end
