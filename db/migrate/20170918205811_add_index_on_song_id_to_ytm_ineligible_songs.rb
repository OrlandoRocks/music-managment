class AddIndexOnSongIdToYtmIneligibleSongs < ActiveRecord::Migration[4.2]
  def change
    add_index :ytm_ineligible_songs, :song_id
  end
end
