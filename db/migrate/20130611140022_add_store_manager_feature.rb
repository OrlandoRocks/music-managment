class AddStoreManagerFeature < ActiveRecord::Migration[4.2]
  def self.up
    Feature.find_or_create_by(:name=>'store_manager', :restrict_by_user => 1)
  end

  def self.down
    feature = Feature.find_by(name: 'store_manager')
    feature.destroy if feature
  end
end
