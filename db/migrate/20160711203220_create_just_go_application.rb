class CreateJustGoApplication < ActiveRecord::Migration[4.2]
  def up
    client_app = ClientApplication.create(name: "tc_social", url: "#{TC_SOCIAL_CONFIG['HOST']}", callback_url: "#{TC_SOCIAL_CONFIG['HOST']}/users/suth/tunecore/callback")
    client_app.update_column(:key, "#{TC_SOCIAL_CONFIG['tunecore_key']}")
    client_app.update_column(:secret, "#{TC_SOCIAL_CONFIG['tunecore_secret']}")
  end

  def down
  end
end
