class AddCompanyUpdatedAtToMumaSongs < ActiveRecord::Migration[4.2]
  def self.up
    add_column :muma_songs, :company_updated_at, :datetime
  end

  def self.down
    remove_column :muma_songs, :company_updated_at
  end
end
