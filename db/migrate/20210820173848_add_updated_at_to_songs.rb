class AddUpdatedAtToSongs < ActiveRecord::Migration[6.0]
  def change
    safety_assured {
      execute 'ALTER TABLE songs ADD COLUMN updated_at DATETIME, ALGORITHM=INPLACE, LOCK=NONE;'
    }
  end

  def down
    safety_assured {
      execute 'ALTER TABLE songs DROP COLUMN updated_at, ALGORITHM=INPLACE, LOCK=NONE;'
    }
  end
end
