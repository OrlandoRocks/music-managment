class AddS3AssetIdToLod < ActiveRecord::Migration[6.0]
  def change
    add_column :lods, :s3_asset_id, :integer
  end
end
