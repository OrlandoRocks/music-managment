class ModifyStateInTaxInfo < ActiveRecord::Migration[4.2]
  def self.up
    change_column :tax_info, :state, :string, :limit => 50
    change_column :tax_info, :mailing_state, :string,  :limit => 50
  end

  def self.down
    change_column :tax_info, :state, :string, :limit => 2
    change_column :tax_info, :mailing_state, :string, :limit => 2
  end
end
