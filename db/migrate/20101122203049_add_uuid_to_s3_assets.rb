class AddUuidToS3Assets < ActiveRecord::Migration[4.2]
  def self.up
    up_sql=%Q(ALTER TABLE `s3_assets`
              ADD COLUMN `uuid` VARCHAR(50) NULL;
            )
    execute up_sql
  end

  def self.down
    remove_column :s3_assets, :uuid
  end
end
