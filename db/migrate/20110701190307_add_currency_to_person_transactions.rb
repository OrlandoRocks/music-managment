class AddCurrencyToPersonTransactions < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE person_transactions ADD currency CHAR(3) NOT NULL DEFAULT 'USD'  COMMENT 'Currency for amounts' AFTER previous_balance")
  end

  def self.down
    remove_column :person_transactions, :currency
  end
end
