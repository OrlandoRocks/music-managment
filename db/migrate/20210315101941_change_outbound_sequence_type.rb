class ChangeOutboundSequenceType < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      change_column :outbound_invoices, :invoice_sequence, :integer, null: true
    end
  end
end
