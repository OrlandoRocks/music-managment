class CreateAccounts < ActiveRecord::Migration[4.2]
  def change
    drop_table :publishing_administrators

    create_table :accounts do |t|
      t.belongs_to :person
      t.string :account_type
      t.string :provider_account_id
      t.string :provider_user_id

      t.timestamps
    end
    add_index :accounts, :person_id
  end
end
