class ChangeLegalReviewStatusTypeToStringOnAlbums < ActiveRecord::Migration[4.2]
  def up
    execute("ALTER TABLE albums CHANGE legal_review_state legal_review_state VARCHAR(255) NOT NULL DEFAULT 'DO NOT REVIEW' COMMENT 'indicates content review states';")
    execute("ALTER TABLE albums CHANGE style_review_state style_review_state VARCHAR(255) NOT NULL DEFAULT 'DO NOT REVIEW' COMMENT 'indicates content review states';")
  end

  def down

  end
end
