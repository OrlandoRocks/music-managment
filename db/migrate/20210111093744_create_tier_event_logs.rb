class CreateTierEventLogs < ActiveRecord::Migration[6.0]
  def change
    create_table :tier_event_logs do |t|
      t.string :event, null: false
      t.integer :person_id, null: false

      t.timestamps
    end
  end
end
