class CreateWithholdingExemptions < ActiveRecord::Migration[6.0]
  def change
    create_table :withholding_exemptions do |t|
      t.integer :person_id, index: true, foreign_key: true
      t.references :withholding_exemption_reasons, index: true, foreign_key: true
      t.date :started_at, null: false
      t.date :ended_at

      t.timestamps
    end
  end
end
