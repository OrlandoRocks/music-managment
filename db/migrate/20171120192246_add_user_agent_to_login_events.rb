class AddUserAgentToLoginEvents < ActiveRecord::Migration[4.2]
  def change
    add_column :login_events, :user_agent, :string
  end
end
