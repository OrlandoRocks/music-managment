class AddProviderAccountIdToComposers < ActiveRecord::Migration[4.2]
  def change
    add_column :composers, :provider_account_id, :string

    add_index :composers, :provider_account_id 
  end
end
