class AddIndexToBraintreeTransactions < ActiveRecord::Migration[4.2]
  def self.up
	execute("ALTER TABLE braintree_transactions ADD INDEX(transaction_id); ")
  end

  def self.down
	execute("ALTER TABLE braintree_transactions DROP INDEX(transaction_id); ")
  end
end
