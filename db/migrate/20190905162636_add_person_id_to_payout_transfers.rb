class AddPersonIdToPayoutTransfers < ActiveRecord::Migration[4.2]
  def change
    add_reference :payout_transfers, :person, index: true
  end
end
