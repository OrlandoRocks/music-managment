class CreateKnowledgebaseLinks < ActiveRecord::Migration[4.2]
  def change
    create_table :knowledgebase_links do |t|
      t.string :article_id
      t.string :article_slug
      t.timestamps
    end

    add_index :knowledgebase_links, :article_slug, unique: true
  end
end
