class CreateCorporateEntities < ActiveRecord::Migration[6.0]
  def change
    create_table :corporate_entities do |t|
      t.string :name
    end

    add_index :corporate_entities, :name, unique: true
    add_reference :people, :corporate_entity, foreign_key: true
  end
end
