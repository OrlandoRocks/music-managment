class CreateAffiliateReports < ActiveRecord::Migration[4.2]
  def change
    create_table :affiliate_reports do |t|
      t.belongs_to :person
      t.string :person_email
      t.belongs_to :api_key
      t.string :product_name
      t.integer :units_sold
      t.integer :sale_amount
      t.string :sale_currency
      t.date :date_paid
      t.text :data

      t.timestamps
    end
    add_index :affiliate_reports, :person_id
    add_index :affiliate_reports, :api_key_id
    add_index :affiliate_reports, :person_email
  end
end
