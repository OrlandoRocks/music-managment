class AddIndiaProducts < ActiveRecord::Migration[4.2]
  def up
    product_ids_with_us_map = {
      "1"   => 454, # Yearly Album Renewal
      "3"   => 417, # Single Renewal
      "9"   => 418, # Added Store
      "18"  => 419, # Album Distribution Credit
      "19"  => 420, # Album Distribution Credit - 5 Pack
      "20"  => 421, # Album Distribution Credit - 10 Pack
      "21"  => 422, # Album Distribution Credit - 20 Pack
      "22"  => 423, # Single Distribution Credit
      "23"  => 424, # Single Distribution Credit - 5 Pack
      "24"  => 425, # Single Distribution Credit - 10 Pack
      "25"  => 426, # Single Distribution Credit - 20 Pack
      "36"  => 427, # 1 Year Album
      "37"  => 428, # 2 Year Album
      "38"  => 429, # 5 Year Album
      "41"  => 430, # 1 Year Single
      "42"  => 431, # 2 Year Single
      "43"  => 432, # 5 Year Single
      "65"  => 433, # Songwriter Service
      "101" => 434, # Ringtone Renewal
      "103" => 435, # 1 Year Ringtone
      "105" => 436, # Store Automator
      "107" => 437, # Credit Usage
      "109" => 438, # Booklet
      "111" => 439, # Ringtone Distribution Credit
      "112" => 440, # Ringtone Distribution Credit - 3 Pack
      "113" => 441, # Ringtone Distribution Credit - 5 Pack
      "114" => 442, # Ringtone Distribution Credit - 10 Pack
      "115" => 443, # Ringtone Distribution Credit - 20 Pack
      "121" => 444, # 2 Year Album Renewal
      "123" => 445, # 2 Year Single Renewal
      "125" => 446, # 5 Year Album Renewal
      "127" => 447, # 5 Year Single Renewal
      "133" => 448, # TuneCore Fan Reviews Starter
      "134" => 449, # TuneCore Fan Reviews Enhanced
      "135" => 450, # TuneCore Fan Reviews Premium
      "145" => 451, # Collect Your YouTube Sound Recording Revenue
      "147" => 452, # Preorder
      "149" => 453, # LANDR Instant Mastering
      "410" => 455  # Facebook Track Monetization
    }

    # use ids from above based on name of product
    renewal_product_ids = {
      "427" => 454, # 1 Year Album
      "428" => 454, # 2 Year Album
      "429" => 454, # 5 Year Album
      "419" => 454, # Album Distribution Credit
      "420" => 454, # Album Distribution Credit - 5 Pack
      "421" => 454, # Album Distribution Credit - 10 Pack
      "422" => 454, # Album Distribution Credit - 20 Pack
      "423" => 417, # Single Distribution Credit
      "424" => 417, # Single Distribution Credit - 5 Pack
      "425" => 417, # Single Distribution Credit - 10 Pack
      "426" => 417, # Single Distribution Credit - 20 Pack
      "430" => 417, # 1 Year Single
      "431" => 417, # 2 Year Single
      "432" => 417, # 5 Year Single
      "435" => 434, # 1 Year Ringtone
      "439" => 434, # Ringtone Distribution Credit
      "440" => 434, # Ringtone Distribution Credit - 3 Pack
      "441" => 434, # Ringtone Distribution Credit - 5 Pack
      "442" => 434, # Ringtone Distribution Credit - 10 Pack
      "443" => 434  # Ringtone Distribution Credit - 20 Pack
    }

    soundout_products = [
      {
        report_type: "starter",
        display_name: "TrackSmarts Starter Report",
        number_of_reviews: 40,
        available_in_days: 5,
        product_id: 448
      },
      {
        report_type: "enhanced",
        display_name: "TrackSmarts Enhanced Report",
        number_of_reviews: 100,
        available_in_days: 5,
        product_id: 449
      },
      {
        report_type: "premium",
        display_name: "TrackSmarts Premium Report",
        number_of_reviews: 225,
        available_in_days: 5,
        product_id: 450
      }
    ]

    inactive_products = [433, 438, 448, 449, 450, 451, 453, 455]
    zero_priced_products = [418, 427, 428, 429, 430, 431, 432, 437, 452, 455]
    created_by = Person.first

    product_ids_with_us_map.each do |us_id, india_id|
      us_product = Product.find(us_id)

      india_product = us_product.dup
      product_price = zero_priced_products.include?(india_id) ? 0.0 : 1.0
      product_status = inactive_products.include?(india_id) ? 'Inactive' : 'Active'
      applies_to_product = india_product.applies_to_product.blank? ? 'None' : india_product.applies_to_product

      india_product.update!({
        id: india_id,
        country_website_id: 8,
        currency: "INR",
        price: product_price,
        created_by: created_by,
        status: product_status,
        applies_to_product: applies_to_product
      })

      us_product.product_items.each do |pi|
        india_product_item = pi.dup

        india_product_item.update!(
          product_id: india_product.id,
          currency: "INR",
          renewal_product_id: renewal_product_ids[india_product.id.to_s]
        )

        pi.product_item_rules.each do |pir|
          india_product_item_rule = pir.dup
          india_product_item_rule.update(
            product_item_id: india_product_item.id,
            currency: "INR"
          )
        end
      end
    end

    soundout_products.each do |soundout_product|
      SoundoutProduct.create!(soundout_product)
    end
  end

  def down
    products = Product.where(country_website_id: 8)
    soundout_products = SoundoutProduct.where(product: products)
    product_items = ProductItem.where(product: products)

    ProductItemRule.where(product_item: product_items).destroy_all
    product_items.destroy_all
    soundout_products.destroy_all
    products.destroy_all

  end

end
