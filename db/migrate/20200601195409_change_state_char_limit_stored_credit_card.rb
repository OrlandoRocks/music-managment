class ChangeStateCharLimitStoredCreditCard < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      change_column :stored_credit_cards, :state, 'char(5)'
    end
  end
end
