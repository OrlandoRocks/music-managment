class AddRedemptionBrandFlagToPerson < ActiveRecord::Migration[4.2]
  def self.up
    add_column :people, :redemption_brand, :string
  end

  def self.down
    remove_column :people, :redemption_brand
  end
end
