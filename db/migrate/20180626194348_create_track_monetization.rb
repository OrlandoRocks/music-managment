class CreateTrackMonetization < ActiveRecord::Migration[4.2]
  def change
    create_table :track_monetizations do |t|
      t.references :song
      t.references :person
      t.references :store
      t.string :state
      t.string :job_id
      t.string :delivery_type
      t.string :eligibility_status
      t.integer :ineligibility_code
      t.datetime :takedown_at
      t.string :provider_status

      t.timestamps
    end

    add_index :track_monetizations, :store_id
    add_index :track_monetizations, :person_id
    add_index :track_monetizations, :song_id
    add_index :track_monetizations, :state
  end
end
