class CreatePaypalTransactions < ActiveRecord::Migration[4.2]
  def self.up
     up_sql = %Q(CREATE TABLE `paypal_transactions` (
       `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
       `person_id` INT(10) UNSIGNED DEFAULT NULL COMMENT 'person ID',
       `invoice_id` int(11) DEFAULT NULL COMMENT 'foreign key',
       `email` VARCHAR(50) NULL COMMENT 'email used in transaction',
       `amount` DECIMAL(8,2) NULL COMMENT 'Payment Amount',
       `action` ENUM('Sale','Authorization','Refund') DEFAULT NULL COMMENT 'type of transaction',
       `status` VARCHAR(50) DEFAULT NULL COMMENT 'status of paypal_transaction',
       `ack` VARCHAR(50) NULL COMMENT 'ack (success, failure, etc.) field returned by paypal, keeping for documentation purposes',
       `billing_agreement_id` char(19) NULL COMMENT 'Billing Agreement ID returned by paypal, used for reference transactions. Will always be 17 or 19 characters',
       `transaction_id` char(19) NULL COMMENT 'Transaction ID returned by paypal, used for reference transactions. Will always be 17 or 19 characters',
       `transaction_type` varchar(20) NULL COMMENT 'type of transaction from paypal',
       `referenced_paypal_account_id` int(11) UNSIGNED DEFAULT NULL COMMENT 'foreign key for reference transactions if a stored_paypal_account is used',
       `token` varchar(50) NULL COMMENT 'token used by paypal',
       `currency` CHAR(3) NULL COMMENT '3 letter currency iso code',
       `error_code` VARCHAR(50) NULL COMMENT 'Error code field returned by paypal, keeping for documentation purposes',
       `pending_reason` VARCHAR(50) NULL COMMENT 'Pending Reason field returned by paypal, keeping for documentation purposes',
       `fee` DECIMAL(6,2) NULL COMMENT 'Paypal processing Fee that we were charged',
       `raw_response` text NULL,
       `updated_at` TIMESTAMP NULL COMMENT 'Datetime of latest update',
       `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',
       PRIMARY KEY (`id`),
       KEY `paypal_pt_person_id` (`person_id`),
       KEY `paypal_pt_invoice_id` (`invoice_id`),
       KEY `paypal_pt_stored_paypal_account_id` (`referenced_paypal_account_id`),
       KEY `paypal_pt_transaction_id` (`transaction_id`),
       CONSTRAINT `FK_paypal_pt_person_id` FOREIGN KEY (`person_id`) REFERENCES `people`(`id`),
       CONSTRAINT `FK_paypal_pt_invoice_id` FOREIGN KEY (`invoice_id`) REFERENCES `invoices`(`id`)
     ) ENGINE=InnoDB DEFAULT CHARSET=utf8;)
     execute up_sql
   end

   def self.down
     drop_table :paypal_transactions
   end
end
