class AddForeignExchangeRateToInvoices < ActiveRecord::Migration[6.0]
  def change
    add_reference :invoices, :foreign_exchange_rate, foreign_key: true
  end
end
