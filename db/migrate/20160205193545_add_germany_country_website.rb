class AddGermanyCountryWebsite < ActiveRecord::Migration[4.2]
  def up
    CountryWebsite.create(:name=>'TuneCore Germany', :currency=>'EUR', :country => 'DE')
  end

  def down
    CountryWebsite.find_by(country: 'DE').destroy
  end
end
