class CreateSalepointSubscriptions < ActiveRecord::Migration[4.2]
  def self.up
    create_table :salepoint_subscriptions do |t|
      t.integer     :album_id, :null => false
      t.boolean     :is_active, :default => true
      t.datetime    :effective
      t.timestamps
    end

    add_index :salepoint_subscriptions, :album_id
  end

  def self.down
    remove_index :salepoint_subscriptions, :album_id
    drop_table :salepoint_subscriptions 
  end
end
