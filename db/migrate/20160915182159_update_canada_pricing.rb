class UpdateCanadaPricing < ActiveRecord::Migration[4.2]
  PRODUCT_ITEMS_RULES_MAP = {
    151 => { "new" =>  '11.99', "old" => '9.99'},
    147 => { "new" =>  '35.99', "old" => '29.99'},
    240 => { "new" =>  '22.78', "old" => '25.98'},
    244 => { "new" =>  '53.95', "old" => '44.95'},
    232 => { "new" =>  '91.98', "old" => '75.98'},
    236 => { "new" =>  '248.95', "old" => '206.95'},
    326 => { "new" =>  '17.99', "old" => '15.00'},
    327 => { "new" =>  '29.99', "old" => '25.00'},
    261 => { "new" =>  '2.49', "old" => '1.98'},
    257 => { "new" =>  '24.99', "old" => '19.99'}
  }

  PRODUCT_ITEMS_MAP = {
    73 => { "new" =>  '12.00', "old" => '10.00'},
    99 => { "new" =>  '12.00', "old" => '10.00'}
  }

  PRODUCTS_MAP = {
    66 => { "new" =>  '59.99', "old" => '49.99'},
    104 => { "new" =>  '24.99', "old" => '19.99'},
    68 => { "new" =>  '11.99', "old" => '9.99 '},
    124 => { "new" =>  '22.78', "old" => '18.98'},
    128 => { "new" =>  '53.95', "old" => '44.95'},
    122 => { "new" =>  '113.98', "old" => '94.98'},
    126 => { "new" =>  '269.95', "old" => '224.96'},
    104 => { "new" =>  '24.99', "old" => '19.99'},
    77 => { "new" =>  '35.99', "old" => '29.99'},
    78 => { "new" =>  '161.95', "old" => '134.95'},
    79 => { "new" =>  '316.95', "old" => '260.91'},
    80 => { "new" =>  '618.99', "old" => '497.83'},
    81 => { "new" =>  '11.99', "old" => '9.99'},
    82 => { "new" =>  '53.95', "old" => '44.95'},
    83 => { "new" =>  '105.95', "old" => '86.91'},
    84 => { "new" =>  '206.99', "old" => '165.83'},
    116 => { "new" =>  '24.99', "old" => '19.99'},
    117 => { "new" =>  '70.99', "old" => '56.99'},
    118 => { "new" =>  '112.95', "old" => '89.99'},
    119 => { "new" =>  '212.95', "old" => '169.99'},
    120 => { "new" =>  '399.99', "old" => '319.99'},
    106 => { "new" =>  '12.00', "old" => '10.00'},
    146 => { "new" =>  '12.00', "old" => '10.00'},
    88 => { "new" =>  '99.00', "old" => '75.00'},
    136 => { "new" =>  '18.00', "old" => '15.00'},
    137 => { "new" =>  '48.00', "old" => '40.00'},
    138 => { "new" =>  '138.00', "old" => '115.00'}
  }

  def up
    return true if Rails.env == "test"
    PRODUCT_ITEMS_RULES_MAP.each do |id,price|
      p "UPDATING product_item_rule #{id}"
      pir = ProductItemRule.find(id)
      pir.price = price["new"]
      pir.save!
    end

    PRODUCT_ITEMS_MAP.each do |id,price|
      p "UPDATING product_item #{id}"
      pi = ProductItem.find(id)
      pi.price = price["new"]
      pi.save!
    end

    PRODUCTS_MAP.each do |id,price|
      p "UPDATING product #{id}"
      p = Product.find(id)
      p.price = price["new"]
      p.created_by_id = 1
      p.save!
    end
  end

  def down
    return true if Rails.env == "test"
    PRODUCT_ITEMS_RULES_MAP.each do |id,price|
      p "REVERTING product_item_rule #{id}"
      pir = ProductItemRule.find(id)
      pir.price = price["old"]
      pir.save!
    end

    PRODUCT_ITEMS_MAP.each do |id,price|
      p "REVERTING product_item #{id}"
      pi = ProductItem.find(id)
      pi.price = price["old"]
      pi.save!
    end

    PRODUCTS_MAP.each do |id,price|
      p "REVERTING product #{id}"
      p = Product.find(id)
      p.price = price["old"]
      p.created_by_id = p.created_by_id || 1
      p.save!
    end
  end
end
