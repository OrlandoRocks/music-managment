class AddProviderIdentifierAndEmailRemoveProFromCowriters < ActiveRecord::Migration[4.2]
  def change
    remove_column :cowriters, :performing_rights_organization_id
    
    add_column :cowriters, :provider_identifier, :string
    add_column :cowriters, :email, :string 

    add_index :cowriters, :provider_identifier
    add_index :cowriters, :email
  end
end
