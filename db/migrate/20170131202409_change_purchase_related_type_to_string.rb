class ChangePurchaseRelatedTypeToString < ActiveRecord::Migration[4.2]
  def change
    change_column :purchases, :related_type, :string
  end
end
