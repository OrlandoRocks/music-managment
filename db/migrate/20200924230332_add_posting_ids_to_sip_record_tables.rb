class AddPostingIdsToSipRecordTables < ActiveRecord::Migration[6.0]
  def change
    #As posting_id is not present in test environment
    unless ActiveRecord::Base.connection.column_exists?(:sales_record_masters, :posting_id)
      add_column :sales_record_masters, :posting_id, :integer
    end
    unless ActiveRecord::Base.connection.column_exists?(:you_tube_royalty_records, :posting_id)
      add_column :you_tube_royalty_records, :posting_id, :integer
    end
  end
end
