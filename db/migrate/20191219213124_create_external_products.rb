class CreateExternalProducts < ActiveRecord::Migration[4.2][4.2]
  def self.up
    create_table :external_products do |t|
      t.references :person, null: false, comment: "references people.id of admin who created the product"
      t.references :external_service, null: false, comment: "references external_services.id of the related service"

      t.string  :name, null: false, comment: "name of product"
      t.string  :description, comment: "optional long description of product"
      t.integer :status, null: false, default: 0, comment: "determines if the product is sold in the system"
      t.decimal :price, default: 0
      t.date    :expires_at, comment: "date product can no longer be sold"

      t.timestamps
    end

    add_index :external_products, :external_service_id
    add_index :external_products, :status
  end

  def self.down
    drop_table :external_products
  end
end
