class RenameColumnsOnSongLibraryUploads < ActiveRecord::Migration[4.2]
  def up
    rename_column :song_library_uploads, :song_title, :name
  end

  def down
    rename_column :song_library_uploads, :name, :song_title
  end
end
