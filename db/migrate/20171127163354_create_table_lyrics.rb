class CreateTableLyrics < ActiveRecord::Migration[4.2]
  def change
    create_table :lyrics do |t|
      t.text :content
      t.integer :song_id
    end
  end
end
