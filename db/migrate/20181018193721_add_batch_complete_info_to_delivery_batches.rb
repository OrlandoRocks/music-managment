class AddBatchCompleteInfoToDeliveryBatches < ActiveRecord::Migration[4.2]
  def change
    add_column :delivery_batches, :processed_count, :integer, default: 0
    add_column :delivery_batches, :batch_complete_sent_at, :datetime
  end
end
