class AddCountryStateAndCountryStateCityIdsToPeople < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      add_column :people, :country_state_id, :integer
      add_column :people, :country_state_city_id, :integer
      add_index :people, :country_state_city_id
    end
  end
end
