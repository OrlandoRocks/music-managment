class CreatePublishingCompositions < ActiveRecord::Migration[6.0]
  def up
    create_table :publishing_compositions do |t|
      t.references :account

      t.string :name
      t.string :iswc

      t.boolean :is_unallocated_sum, default: 0

      t.string :cwr_name
      t.string :state, limit: 25
      t.string :prev_state, limit: 25

      t.date :state_updated_on

      t.string :provider_identifier
      t.string :provider_recording_id
      t.string :translated_name

      t.datetime :verified_date

      t.timestamps
    end

    add_index :publishing_compositions, :provider_identifier
    add_index :publishing_compositions, :provider_recording_id
  end

  def down
    drop_table :publishing_compositions
  end
end
