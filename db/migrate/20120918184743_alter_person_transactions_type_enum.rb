class AlterPersonTransactionsTypeEnum < ActiveRecord::Migration[4.2]
  def self.up
    connection.execute "ALTER TABLE person_transactions 
      MODIFY COLUMN `target_type` ENUM('BalanceAdjustment','BatchTransaction','CheckTransfer','EftBatchTransaction','Invoice','PaypalIpn','PaypalTransfer','Person','PersonIntake','FriendReferral') 
      DEFAULT NULL"
  end

  def self.down
    connection.execute "ALTER TABLE person_transactions 
      MODIFY COLUMN `target_type` ENUM('BalanceAdjustment','BatchTransaction','CheckTransfer','EftBatchTransaction','Invoice','PaypalIpn','PaypalTransfer','Person','PersonIntake') 
      DEFAULT NULL"
  end
end
