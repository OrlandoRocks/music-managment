class AddAssetToExternalServiceIdArtworks < ActiveRecord::Migration[4.2]
  def change
    add_attachment :external_service_id_artworks, :asset
    add_column :external_service_id_artworks, :asset_fingerprint, :string
  end
end
