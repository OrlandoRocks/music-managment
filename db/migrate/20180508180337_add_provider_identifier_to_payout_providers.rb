class AddProviderIdentifierToPayoutProviders < ActiveRecord::Migration[4.2]
  def change
    add_column :payout_providers, :provider_identifier, :string
    add_index :payout_providers, :provider_identifier
  end
end
