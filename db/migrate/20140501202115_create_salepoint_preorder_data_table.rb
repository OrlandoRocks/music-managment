class CreateSalepointPreorderDataTable < ActiveRecord::Migration[4.2]
  def up
    create_table :salepoint_preorder_data do |t|
      t.integer :salepoint_id
      t.date :start_date
      t.boolean :preview_songs
      t.integer :variable_price_id
      t.boolean :enabled
      t.datetime :paid_at
      t.timestamps
    end
  end

  def down
    drop_table :salepoint_preorder_data
  end
end
