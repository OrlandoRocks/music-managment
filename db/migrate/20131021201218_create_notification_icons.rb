class CreateNotificationIcons < ActiveRecord::Migration[4.2]
  def change
    create_table :notification_icons do |t|
      t.has_attached_file :file

      t.timestamps
    end
  end
end
