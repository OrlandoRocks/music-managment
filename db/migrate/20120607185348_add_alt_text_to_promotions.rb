class AddAltTextToPromotions < ActiveRecord::Migration[4.2]
  def self.up
    add_column :promotions, :alt_text, :string
  end

  def self.down
    remove_column :promotions, :alt_text
  end
end
