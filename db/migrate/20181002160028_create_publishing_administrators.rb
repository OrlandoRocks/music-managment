class CreatePublishingAdministrators < ActiveRecord::Migration[4.2]
  def change
    create_table :publishing_administrators do |t|
      t.belongs_to :person
      t.string :provider_identifier

      t.timestamps
    end
    add_index :publishing_administrators, :person_id
  end
end
