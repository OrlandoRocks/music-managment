class AddMediaListFlagToPerson < ActiveRecord::Migration[4.2]
  def self.up
    add_column :people, :enable_bigbox_list, :boolean
  end

  def self.down
    remove_column :people, :enable_bigbox_list
  end
end
