class ChangeNoteSubjectLength < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      change_column :notes, :subject, :string, :limit => 255, :null => false, comment: 'Subject'
    end
  end
end
