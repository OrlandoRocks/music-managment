class CreateTargetedOfferExclusions < ActiveRecord::Migration[4.2]
  def self.up

    up_sql = %Q(CREATE TABLE `targeted_offer_exclusions` (
      `id` int(10)                unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
      `targeted_offer_id` int(10) unsigned NOT NULL COMMENT 'Foreign Key to Targeted Offer Table',
      `exclude_targeted_offer_id` int(10) unsigned NOT NULL COMMENT 'Foreign Key to Targeted Offer Table',
      `updated_at`                timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
      `created_at`                timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',
      PRIMARY KEY (`id`),
      KEY `targeted_offer_id` (`targeted_offer_id`),
      KEY `exclude_targeted_offer_id` (`exclude_targeted_offer_id`),
      CONSTRAINT `FK_targetoffer_exclusion_targetoffers` FOREIGN KEY (`targeted_offer_id`) REFERENCES `targeted_offers` (`id`),
      CONSTRAINT `FK_targetoffer_exclusion_exclude_targetoffers` FOREIGN KEY (`exclude_targeted_offer_id`) REFERENCES `targeted_offers` (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;)

      execute(up_sql)
  end

  def self.down
    drop_table :targeted_offer_exclusions
  end
end
