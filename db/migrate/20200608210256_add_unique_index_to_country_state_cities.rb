class AddUniqueIndexToCountryStateCities < ActiveRecord::Migration[6.0]
  def change
    add_index :country_state_cities, [:name, :state_id], unique: true
  end
end
