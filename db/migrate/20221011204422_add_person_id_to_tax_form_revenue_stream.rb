class AddPersonIdToTaxFormRevenueStream < ActiveRecord::Migration[6.0]
  def change
    add_column :tax_form_revenue_streams, :person_id, :integer, comment: "Persons revenue stream in the context of a taxform mapping"
    add_index :tax_form_revenue_streams,  [:person_id, :revenue_stream_id], unique: true, name: :pid_rsid_tfrs, comment: "Person should only have one revenue stream mapped to one taxform at a time"
  end
end