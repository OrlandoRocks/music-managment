class AddPricingOverridesData < ActiveRecord::Migration[4.2]
  def self.up
    execute "UPDATE variable_prices SET user_editable = true where id in (1,2,3,4);"

    prices = [
      { code: "PROMOTIONAL", amount: "0.00", types: [ "Album", "Single", "Song" ], store_id: 13, position: 5 },
      { code: "ID_A1", amount: "1.40", types: [ "Album", "Single" ], store_id: 13, position: 6 },
      { code: "ID_A2", amount: "2.80", types: [ "Album", "Single" ], store_id: 13, position: 7 },
      { code: "ID_A3", amount: "3.50", types: [ "Album", "Single" ], store_id: 13, position: 8 },
      { code: "ID_A4", amount: "4.20", types: [ "Album", "Single" ], store_id: 13, position: 9 },
      { code: "ID_A5", amount: "4.90", types: [ "Album", "Single" ], store_id: 13, position: 10 },
      { code: "ID_A6", amount: "5.60", types: [ "Album", "Single" ], store_id: 13, position: 11 },
      { code: "ID_A7", amount: "6.30", types: [ "Album", "Single" ], store_id: 13, position: 12 },
      { code: "ID_A8", amount: "7.00", types: [ "Album", "Single" ], store_id: 13, position: 13 },
      { code: "ID_A9", amount: "8.40", types: [ "Album", "Single" ], store_id: 13, position: 14 },
      { code: "ID_A10", amount: "10.50", types: [ "Album", "Single" ], store_id: 13, position: 15 },
      { code: "ID_A11", amount: "11.90", types: [ "Album", "Single" ], store_id: 13, position: 16 },
      { code: "ID_A12", amount: "14.00", types: [ "Album", "Single" ], store_id: 13, position: 17 },
      { code: "ID_T1", amount: "0.49", types: [ "Song" ], store_id: 13, position: 18 },
      { code: "ID_T2", amount: "0.63", types: [ "Song" ], store_id: 13, position: 19 },
      { code: "ID_T3", amount: "0.70", types: [ "Song" ], store_id: 13, position: 20 },
      { code: "ID_T4", amount: "0.91", types: [ "Song" ], store_id: 13, position: 21 },
      { code: "free", amount: "0.00", types: [ "Album", "Single", "Song" ], store_id: 28, position: 1 },
      { code: "1", amount: "0.49", types: [ "Album", "Single", "Song" ], store_id: 28, position: 2 },
      { code: "2", amount: "0.70", types: [ "Album", "Single", "Song" ], store_id: 28, position: 3 },
      { code: "3", amount: "0.91", types: [ "Album", "Single", "Song" ], store_id: 28, position: 4 },
      { code: "4", amount: "1.40", types: [ "Album", "Single", "Song" ], store_id: 28, position: 5 },
      { code: "5", amount: "1.75", types: [ "Album", "Single", "Song" ], store_id: 28, position: 6 },
      { code: "6", amount: "2.10", types: [ "Album", "Single", "Song" ], store_id: 28, position: 7 },
      { code: "7", amount: "2.80", types: [ "Album", "Single", "Song" ], store_id: 28, position: 8 },
      { code: "8", amount: "3.50", types: [ "Album", "Single", "Song" ], store_id: 28, position: 9 },
      { code: "9", amount: "4.20", types: [ "Album", "Single", "Song" ], store_id: 28, position: 10 },
      { code: "10", amount: "4.90", types: [ "Album", "Single", "Song" ], store_id: 28, position: 11 },
      { code: "11", amount: "5.60", types: [ "Album", "Single", "Song" ], store_id: 28, position: 12 },
      { code: "12", amount: "6.30", types: [ "Album", "Single", "Song" ], store_id: 28, position: 13 },
      { code: "13", amount: "7.00", types: [ "Album", "Single", "Song" ], store_id: 28, position: 14 },
      { code: "14", amount: "7.70", types: [ "Album", "Single", "Song" ], store_id: 28, position: 15 },
      { code: "15", amount: "8.40", types: [ "Album", "Single", "Song" ], store_id: 28, position: 16 },
      { code: "16", amount: "9.10", types: [ "Album", "Single", "Song" ], store_id: 28, position: 17 },
      { code: "17", amount: "10.50", types: [ "Album", "Single", "Song" ], store_id: 28, position: 18 },
      { code: "18", amount: "11.90", types: [ "Album", "Single" ], store_id: 28, position: 19 },
      { code: "D2", amount: "14.00", types: [ "Album", "Single" ], store_id: 28, position: 20 },
      { code: "D3", amount: "21.00", types: [ "Album", "Single" ], store_id: 28, position: 21 },
      { code: "D4", amount: "28.00", types: [ "Album", "Single" ], store_id: 28, position: 22 },
      { code: "D5", amount: "35.00", types: [ "Album", "Single" ], store_id: 28, position: 23 },
      { code: "D6", amount: "42.00", types: [ "Album", "Single" ], store_id: 28, position: 24 },
      { code: "D7", amount: "49.00", types: [ "Album", "Single" ], store_id: 28, position: 25 },
      { code: "D8", amount: "56.00", types: [ "Album", "Single" ], store_id: 28, position: 26 },
      { code: "D9", amount: "63.00", types: [ "Album", "Single" ], store_id: 28, position: 27 },
      { code: "D10", amount: "70.00", types: [ "Album", "Single" ], store_id: 28, position: 28 },
      { code: "D11", amount: "77.00", types: [ "Album", "Single" ], store_id: 28, position: 29 },
      { code: "D12", amount: "84.00", types: [ "Album", "Single" ], store_id: 28, position: 30 },
      { code: "D13", amount: "91.00", types: [ "Album", "Single" ], store_id: 28, position: 31 },
      { code: "D14", amount: "98.00", types: [ "Album", "Single" ], store_id: 28, position: 32 },
      { code: "D15", amount: "105.00", types: [ "Album", "Single" ], store_id: 28, position: 33 },
      { code: "D16", amount: "112.00", types: [ "Album", "Single" ], store_id: 28, position: 34 },
      { code: "D17", amount: "119.00", types: [ "Album", "Single" ], store_id: 28, position: 35 },
      { code: "D18", amount: "126.00", types: [ "Album", "Single" ], store_id: 28, position: 36 },
      { code: "D19", amount: "133.00", types: [ "Album", "Single" ], store_id: 28, position: 37 },
      { code: "D20", amount: "140.00", types: [ "Album", "Single" ], store_id: 28, position: 38 }
    ]
    prices.each do |price|
      vp = VariablePrice.create({ price_code: price[:code] , price_code_display: "#{price[:code]} - $#{price[:amount]}", user_editable: false, active: true, position: price[:position] })
      price[:types].each do |type|
        VariablePriceSalepointableType.create({ salepointable_type: type, variable_price_id: vp.id})
      end

      vp_store = VariablePriceStore.create({ store_id: price[:store_id], variable_price_id: vp.id, is_active: true, position: price[:position] })
    end
  end

  def self.down
  end
end
