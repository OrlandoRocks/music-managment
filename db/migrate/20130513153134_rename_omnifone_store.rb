class RenameOmnifoneStore < ActiveRecord::Migration[4.2]
  def self.up
    store = Store.find_by(short_name: 'Omnifone')
    if store
      store.update(:name => 'Sony Music Unlimited', :abbrev => 'su', :short_name => 'SonyMU')
    end
  end

  def self.down
    store = Store.find_by(short_name: 'SonyMU')
    if store
      store.update(:name => 'Omnifone', :abbrev => 'om', :short_name => 'Omnifone')
    end
  end
end
