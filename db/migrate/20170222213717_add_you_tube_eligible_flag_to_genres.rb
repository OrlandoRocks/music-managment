class AddYouTubeEligibleFlagToGenres < ActiveRecord::Migration[4.2]
  def up
    add_column :genres, :you_tube_eligible, :boolean, default: true

    Genre.where( name: ['Audiobooks', 'Comedy', 'Spoken Word', 'Fitness & Workout', 'Karaoke'] ).update_all( you_tube_eligible: false )
  end

  def down
    remove_column :genres, :you_tube_eligible
  end
end

