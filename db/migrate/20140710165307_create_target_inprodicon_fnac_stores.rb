class CreateTargetInprodiconFnacStores < ActiveRecord::Migration[4.2]
  @@stores = [
    { name: "Yandex",       abbrev: "yandex", short_name: "Yandex",     position: 600 },
    { name: "InProdicon",   abbrev: "inprcn", short_name: "InProdicon", position: 610 },
    { name: "Target Music", abbrev: "target", short_name: "Target",     position: 620 },
    { name: "FNAC Jukebox", abbrev: "fnacjk", short_name: "FNAC",       position: 630 }
  ]

  def up
    @@stores.each do |store_info|
      store = Store.create(
        name:                     store_info[:name],
        abbrev:                   store_info[:abbrev],
        short_name:               store_info[:short_name],
        position:                 store_info[:position],
        needs_rights_assignment:  false,
        is_active:                false,
        base_price_policy_id:     "3",
        is_free:                  false
      )
      if store && store.errors.empty?
        store.salepointable_stores.create!(:salepointable_type => 'Album')
        store.salepointable_stores.create!(:salepointable_type => 'Single')
      else
        puts "Store creation for #{store_info[:name]} failed!"
      end
    end
  end

  def down
    @@stores.each do |store_info|
      if store = Store.find_by(name: store_info[:name])
        store.salepointable_stores.collect(&:destroy)
        store.destroy
      else
        puts "Couldn't find store by the short name of #{store_info[:name]}"
      end
    end
  end
end
