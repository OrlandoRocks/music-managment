# encoding: utf-8

class CleanupCountryFieldInPeople < ActiveRecord::Migration[4.2]
  def up
    country_map = {
      "" => "Unidentified",
      nil => "Unidentified",
      "-----------" => "Unidentified",
      "233" => "Unidentified",
      "Bosnia and Herzegowina" => "Bosnia and Herzegovina",
      "Burma" => "Myanmar",
      "Congo, the Democratic Republic of the" => "Congo, Democratic Republic of The",
      "East Timor" => "Timor-Leste",
      "England" => "United Kingdom",
      "Espana" => "Spain",
      "Falkland Islands" => "Falkland Islands (Malvinas)",
      "Great Britain" => "United Kingdom",
      "Heard and McDonald Islands" => "Heard Island and McDonald Islands",
      "Holy See (Vatican City State)" => "Vatican City",
      "Iran, Islamic Republic of" => "Iran",
      "Korea (South)" => "South Korea",
      "Korea, Democratic People's Republic of" => "North Korea",
      "Korea, Republic of" => "South Korea",
      "Lao People's Democratic Republic" => "Laos",
      "Libyan Arab Jamahiriya" => "Libya",
      "Macau" => "Macao",
      "Macedonia, The Former Yugoslav Republic Of" => "Macedonia",
      "Micronesia, Federated States of" => "Micronesia",
      "Moldova, Republic of" => "Moldova",
      "Northern Ireland" => "United Kingdom",
      "Palestinian Territory, Occupied" => "Palestine",
      "Russian Federation" => "Russia",
      "Saint Helena" => "Saint Helena, Ascension and Tristan da Cunha",
      "Samoa (Independent)" => "Samoa",
      "Scotland" => "United Kingdom",
      "Serbia and Montenegro" => "Serbia",
      "St. Helena" => "Saint Helena, Ascension and Tristan da Cunha",
      "Taiwan, Province of China" => "Taiwan",
      "Tanzania, United Republic of" => "Tanzania",
      "Trinidad" => "Trinidad and Tobago",
      "Vatican City State (Holy See)" => "Vatican City",
      "Viet Nam" => "Vietnam",
      "Virgin Islands (British)" => "Virgin Islands, British",
      "Virgin Islands, United State" => "Virgin Islands, United States",
      "Virgin Islands (U.S.)" => "Virgin Islands, United States",
      "Virgin Islands, U.S." => "Virgin Islands, United States",
      "Wales" => "United Kingdom",
      "Wallis and Futuna Islands" => "Wallis and Futuna",
      "Netherlands Antilles" => "Curaçao",
      "Российская Федерация" => "Russia",
      "Россия" => "Russia"
    }

    country_map.each { |k, v| Person.where( country: k ).update_all( country: v ) }

    # One offs for these 2 guys
    # 416077  Netherlands Antilles  Sint Maarten
    # 740543  Netherlands Antilles  Sint Maarten

    [416077, 740543].each do |id|
      p = Person.where(id: id).first
      p.update(country: "Sint Maarten") if p
    end

    # fix country table as well

    Country.where( name: "Virgin Islands, United State").update_all( name: "Virgin Islands, United States")
  end

  def down
  end
end
