class AddUkCountryWebsite < ActiveRecord::Migration[4.2]
  def up
    CountryWebsite.create(:name=>'TuneCore UK', :currency=>'GBP', :country => 'UK')
  end

  def down
    CountryWebsite.find_by(country: 'UK').destroy
  end
end
