class RemoveOldHeaderFeature < ActiveRecord::Migration[4.2]
  def self.up
    feature = Feature.find_by(name: "old_header")

    if feature
      execute("delete from features_people where feature_id = #{feature.id}")
      feature.destroy
    end
  end

  def self.down
    Feature.find_or_create_by(:name=>'old_header', :restrict_by_user => 1)
  end
end
