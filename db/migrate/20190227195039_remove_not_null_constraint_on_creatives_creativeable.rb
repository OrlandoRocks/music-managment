class RemoveNotNullConstraintOnCreativesCreativeable < ActiveRecord::Migration[4.2]
  def up
    change_column_null :creatives, :creativeable_id, true
    change_column_null :creatives, :creativeable_type, true
  end

  def down
    change_column_null :creatives, :creativeable_id, false
    change_column_null :creatives, :creativeable_type, false
  end
end
