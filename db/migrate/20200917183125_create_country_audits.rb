class CreateCountryAudits < ActiveRecord::Migration[6.0]
  def change
    create_table :country_audits do |t|
      t.integer :person_id
      t.integer :country_id
      t.integer :country_audit_source_id
      t.timestamps
    end
    add_index :country_audits, :person_id
    add_index :country_audits, :country_id
    add_index :country_audits, [:country_audit_source_id, :person_id]
  end
end
