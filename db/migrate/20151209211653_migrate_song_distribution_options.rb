class MigrateSongDistributionOptions < ActiveRecord::Migration[4.2]
  def up
    insert_free_song_sql = 'insert into song_distribution_options (song_id, option_name, option_value) select id, "free_song", free_song from songs where free_song is not null;'
    insert_album_only_sql = 'insert into song_distribution_options (song_id, option_name, option_value) select id, "album_only", album_only from songs where album_only is not null;'
    
    execute (insert_free_song_sql)
    execute (insert_album_only_sql)
  end

  def down
    SongDistributionOption.destroy_all
  end
end
