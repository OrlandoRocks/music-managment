class AddLanguageCodeAndRoleToDocumentTemplates < ActiveRecord::Migration[4.2]
  def change
    add_column :document_templates, :language_code, :string
    add_column :document_templates, :role, :string
    add_index :document_templates, [:language_code, :role]
  end
end
