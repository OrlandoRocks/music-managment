class AddLocationDataToLoginEvents < ActiveRecord::Migration[4.2]
  def change
    add_column :login_events, :subdivision, :string
    add_column :login_events, :city, :string
    add_column :login_events, :country, :string
    add_column :login_events, :latitude, :float
    add_column :login_events, :longitude, :float
  end
end
