class CreateDropkloudSubscriptionEvents < ActiveRecord::Migration[4.2]
  def up
    create_table :dropkloud_subscription_events do |t|
      t.integer :dropkloud_purchase_id
      t.string :event_type, null: false
      t.integer :person_subscription_status_id, null: false
      t.timestamps
    end

    add_index :dropkloud_subscription_events, :dropkloud_purchase_id
    add_index :dropkloud_subscription_events, :person_subscription_status_id, name: "index_dropkloud_sub_events_onperson_sub_status_id"
  end

  def down
    drop_table :dropkloud_subscription_events
  end
end
