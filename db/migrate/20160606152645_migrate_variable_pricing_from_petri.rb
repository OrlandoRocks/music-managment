class MigrateVariablePricingFromPetri < ActiveRecord::Migration[4.2]
  NEW_VARIABLE_PRICE_ARRAY = [
    { stores: [55, 47, 52, 51, 44], code: "A1", type: "Album" },
    { stores: [55, 47, 52, 51, 44], code: "T1", type: "Single" },
    { stores: [55, 47, 52, 51, 44], code: "T1", type: "Song" },
    { stores: [58, 60, 63, 56, 67, 68, 69, 70, 76], code: "AMid", type: "Album" },
    { stores: [58, 60, 63, 56, 67, 68, 69, 70, 76], code: "TMid", type: "Single" },
    { stores: [58, 60, 63, 56, 67, 68, 69, 70, 76], code: "TMid", type: "Song" },
    { stores: [53], code: "UNPRICED", type: "Album" },
    { stores: [53], code: "UNPRICED", type: "Single" },
    { stores: [53], code: "UNPRICED", type: "Song" },
    { stores: [71, 50], code: "album_1", type: "Album" },
    { stores: [71, 50], code: "Single_1", type: "Single" },
    { stores: [71, 50], code: "Single_1", type: "Song" },
    { stores: [43], code: "unpriced album", type: "Album" },
    { stores: [43], code: "unpriced single", type: "Single" },
    { stores: [43], code: "unpriced single", type: "Song" },
    { stores: [12], code: "TunecoreAlbum", type: "Album" },
    { stores: [12], code: "TunecoreTrack", type: "Single" },
    { stores: [12], code: "TunecoreTrack", type: "Song" },
    { stores: [8, 74], code: "Album", type: "Album" },
    { stores: [8, 74], code: "Single", type: "Single" },
    { stores: [8, 74], code: "Track", type: "Song" },
    { stores: [24], code: "MIDLINE", type: "Album" },
    { stores: [42], code: "album", type: "Album" },
    { stores: [42], code: "track", type: "Song" }
  ].freeze
  def up
    position = 50
    NEW_VARIABLE_PRICE_ARRAY.each do |options|
      vp = VariablePrice.create(price_code: options[:code], price_code_display: options[:code], position: position, active: true)
      raise "Unable to create code: #{options[:code]}" unless vp.present?
      vpst = VariablePriceSalepointableType.create(variable_price: vp, salepointable_type: options[:type])
      if vpst
        p "succesfully created variable price #{options[:code]} with salepointable type #{options[:type]}"
        create_stores(options[:stores], vp, position)
        position = position + 1
      else
        p "rolling back variable price #{options[:code]} with salepointable type #{options[:type]}"
        vp.destroy
      end
    end
  end

  def down
    new_codes = NEW_VARIABLE_PRICE_ARRAY.map { |h| h[:code] }
    new_codes.each do |code|
      vp = VariablePrice.find_by(price_code: code)
      vp.destroy if vp.present?
    end
  end

  def create_stores(stores, vp, position)
    stores.each do |store_id|
      store_obj = Store.find(store_id)
      VariablePriceStore.create!(store: store_obj, variable_price: vp, is_active: true, position: position)
    end
  end
end

# Spinlet = 55
# Slacker = 47
# Anghami = 52
# Akazoo = 51
# Yandex = 58
# Target = 60
# Clro Music = 63
# Neurotic = 56
# Zvooq = 67
# Saavn = 68
# 8Tracks = 69
# NMusic = 70
# MusicLoad = 76
# KKBox = 53
# Qsic = 71
# Guvera = 50
# seven digital = 43
# juke = 44
# thumbplay = 25
# vervelife = 12
# medianet = 8
# zune = 24
# cur = 74
# shazam = 42
