class CreateCountryWebsiteLanguage < ActiveRecord::Migration[6.0]
  def up
    create_table :country_website_languages do |t|
      t.references :country_website, null: false, foreign_key: true, type: 'int(11) unsigned'
      t.string     :yml_languages, null: true
      t.string     :selector_language, null: false
      t.string     :selector_country, null: false
      t.integer    :selector_order, null: false
      t.references :redirect_country_website,  null: true, type: 'int(11) unsigned', index: {name: "cwl_redirect_id"}, foreign_key: {to_table: :country_websites}

      t.timestamps
    end

    Seed::seed('db/seed/csv/country_website_languages.csv')

    safety_assured { rename_column :people, :language_code_id, :country_website_language_id }
  end

  def down
    drop_table :country_website_languages

    safety_assured { rename_column :people, :country_website_language_id, :language_code_id }
  end
end
