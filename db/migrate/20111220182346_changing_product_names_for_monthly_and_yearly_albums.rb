class ChangingProductNamesForMonthlyAndYearlyAlbums < ActiveRecord::Migration[4.2]
  def self.up
    #Change display_name of US yearly Product
    yearly_album = Product.where(:name=>'1 Year Album', :country_website_id=>1).first
    yearly_album.update(:display_name=>"Album - Yearly Plan") if !yearly_album.nil?
    #Change display_name of CA yearly Product
    ca_yearly_album = Product.where(:name=>'1 Year Album', :country_website_id=>2).first
    ca_yearly_album.update(:display_name=>"Album - Yearly Plan") if !ca_yearly_album.nil?
    #Change display_name of US yearly Product
    monthly_album = Product.where(:name=>'Monthly Album', :country_website_id=>1).first
    monthly_album.update(:display_name=>"Album - Monthly Plan") if !monthly_album.nil?
    #The monthly CA album is added in a rake task with the correct name
  end

  def self.down
    #Change display_name of US yearly Product
    yearly_album = Product.where(:name=>'1 Year Album', :country_website_id=>1).first
    yearly_album.update(:display_name=>"Album Distribution") if !yearly_album.nil?
    #Change display_name of CA yearly Product
    ca_yearly_album = Product.where(:name=>'1 Year Album', :country_website_id=>2).first
    ca_yearly_album.update(:display_name=>"Album Distribution") if !ca_yearly_album.nil?
    #Change display_name of US yearly Product
    monthly_album = Product.where(:name=>'Monthly Album', :country_website_id=>1).first
    monthly_album.update(:display_name=>"Monthly Plan") if !monthly_album.nil?
    #The monthly CA album is added in a rake task with the correct name

  end
end
