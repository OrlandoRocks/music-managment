class AddLocalizedNameToSongs < ActiveRecord::Migration[4.2]
  def change
    add_column :songs, :localized_name, :string
    add_column :songs, :song_version, :string
    add_column :songs, :previously_released, :boolean
    add_column :songs, :made_popular_by, :string
    add_column :songs, :cover_song, :boolean
  end
end
