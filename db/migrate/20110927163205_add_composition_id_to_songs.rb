class AddCompositionIdToSongs < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE songs ADD COLUMN composition_id INT(11);")
  end

  def self.down
  end
end
