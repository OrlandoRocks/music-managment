class CreateBelieveErrorsTable < ActiveRecord::Migration[4.2]
  def change
    create_table :believe_errors do |t|
      t.integer :album_id
      t.text :request_args
      t.string :endpoint
      t.text :response_errors
    end

    add_index :believe_errors, :album_id
  end
end
