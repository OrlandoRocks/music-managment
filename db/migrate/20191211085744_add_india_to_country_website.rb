class AddIndiaToCountryWebsite < ActiveRecord::Migration[4.2]
  def up
    country_id = Country.find_by(iso_code: 'IN')&.id
    CountryWebsite.create(
      name: 'TuneCore India',
      currency: 'INR',
      country: 'IN',
      country_id: country_id
    )
  end

  def down
    CountryWebsite.find_by(country: 'IN')&.destroy
  end
end
