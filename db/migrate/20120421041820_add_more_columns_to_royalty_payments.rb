class AddMoreColumnsToRoyaltyPayments < ActiveRecord::Migration[4.2]
  def self.up
    add_column :royalty_payments, :person_id, :integer
    add_column :royalty_payments, :counterpoint_code, :integer
  end

  def self.down
    remove_column :royalty_payments, :person_id
    remove_column :royalty_payments, :counterpoint_code
  end
end
