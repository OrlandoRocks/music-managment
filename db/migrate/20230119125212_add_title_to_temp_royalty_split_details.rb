class AddTitleToTempRoyaltySplitDetails < ActiveRecord::Migration[6.0]
  def change
    add_column :temp_royalty_split_details, :royalty_split_title, :string
  end
end
