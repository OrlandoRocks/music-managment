class AddInvalidRedemptionAttemptsToPerson < ActiveRecord::Migration[4.2]
  def self.up
    add_column :people, :redemption_attempts, :integer, :default => 0
    add_column :people, :redemption_locked_until, :datetime
  end

  def self.down
    remove_column :people, :redemption_attempts
    remove_column :people, :redemption_locked_until
  end
end
