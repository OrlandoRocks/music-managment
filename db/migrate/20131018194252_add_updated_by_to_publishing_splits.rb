class AddUpdatedByToPublishingSplits < ActiveRecord::Migration[4.2]
  def self.up
    add_column :publishing_splits, :updated_by, :string, :limit => 50
  end

  def self.down
    remove_column :publishing_splits, :updated_by
  end
end
