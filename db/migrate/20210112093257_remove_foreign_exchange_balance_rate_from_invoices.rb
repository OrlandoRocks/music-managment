class RemoveForeignExchangeBalanceRateFromInvoices < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      if table_exists?(:foreign_exchange_balance_rate)
        remove_reference :invoices,
                         :foreign_exchange_balance_rate,
                         foreign_key: true
      end
    end
  end
end
