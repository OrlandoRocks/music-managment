class RenameForeignExchangeRatesReferenceInDisputes < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      remove_reference :disputes, :foreign_exchange_rates, foreign_key: true
    end
    add_reference :disputes, :foreign_exchange_rate, foreign_key: true
  end
end
