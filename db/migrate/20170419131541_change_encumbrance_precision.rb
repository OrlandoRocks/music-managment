class ChangeEncumbrancePrecision < ActiveRecord::Migration[4.2]
  def up
    change_column :encumbrance_details, :transaction_amount, :decimal, precision: 23, scale: 14
    change_column :encumbrance_summaries, :outstanding_amount, :decimal, precision: 23, scale: 14

    db = ActiveRecord::Base.connection_config
    connect_string = "mysql #{db[:database]} -h #{db[:host]} --user=#{db[:username]} "
    connect_string += "--password=#{db[:password]}" if db[:password]

    `#{connect_string} < #{File.join(Rails.root, "db", "stored_procedures", "withhold_encumbrances.sql")}`
    `#{connect_string} < #{File.join(Rails.root, "db", "stored_procedures", "create_person_intake.sql")}`
  end

  def down
    change_column :encumbrance_details, :transaction_amount, :decimal, precision: 12, scale: 2
    change_column :encumbrance_summaries, :outstanding_amount, :decimal, precision: 12, scale: 2
  end
end
