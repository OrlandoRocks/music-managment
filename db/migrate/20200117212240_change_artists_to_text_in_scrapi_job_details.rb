class ChangeArtistsToTextInScrapiJobDetails < ActiveRecord::Migration[4.2]
  def change
    safety_assured {
      change_column :scrapi_job_details, :artists, :text
    }
  end
end
