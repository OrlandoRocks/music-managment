class CreateApiKeys < ActiveRecord::Migration[4.2]
  def up
    create_table :api_keys do |t|
      t.string :key
      t.string :partner_name
    end
  end

  def down
    drop_table :api_keys
  end
end
