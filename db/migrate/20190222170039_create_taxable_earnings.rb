class CreateTaxableEarnings < ActiveRecord::Migration[4.2]
  def change
    create_table :taxable_earnings do |t|
      t.string :revenue_source, null: false
      t.decimal :credit, precision: 23, scale: 14, null: false
      t.string :currency, limit: 3, null: false, default: "USD"
      t.references :person, null: false
      t.datetime :created_at, null: false
    end

    add_index :taxable_earnings, :person_id
  end
end
