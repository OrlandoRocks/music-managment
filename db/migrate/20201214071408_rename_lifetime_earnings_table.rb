class RenameLifetimeEarningsTable < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      rename_table :lifetime_earnings_tables, :lifetime_earnings
    end
  end
end
