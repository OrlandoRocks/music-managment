class ActivateServicesForIndia < ActiveRecord::Migration[6.0]
  @@product_ids = [
    Product::IN_BOOKLET,
    Product::IN_SOUNDOUT_PRODUCT_IDS,
    Product::IN_YTM,
    Product::IN_FBM
  ].flatten

  def up
    Product.where(id: @@product_ids).update_all(status: Product::ACTIVE)
  end

  def down
    Product.where(id: @@product_ids).update_all(status: Product::INACTIVE)
  end
end
