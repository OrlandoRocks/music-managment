class AddCountryIdToCountryWebsite < ActiveRecord::Migration[4.2]
  def change

    change_table :country_websites do |t|
      t.integer :country_id
    end

    CountryWebsite.all.each do |country_website|
      country_website.country_id = Country.find_by(iso_code: country_website.id == 3 ? "GB" : country_website.country).id
      country_website.save
    end
  end
end
