class ModifyColumnsInRoyaltyPayments < ActiveRecord::Migration[4.2]
  def self.up
    add_column :royalty_payments, :posting_type, :string
    add_column :royalty_payments, :posting_id, :integer
    change_column :royalty_payments, :amount, :decimal, :precision => 23, :scale => 14
  end

  def self.down
    remove_column :royalty_payments, :posting_type
    remove_column :royalty_payments, :posting_id
    change_column :royalty_payments, :amount, :decimal
  end
end
