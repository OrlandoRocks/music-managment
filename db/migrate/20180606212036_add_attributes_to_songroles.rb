class AddAttributesToSongroles < ActiveRecord::Migration[4.2]
  def change
    add_column :song_roles, :resource_contributor_role, :boolean
    add_column :song_roles, :indirect_contributor_role, :boolean
  end
end
