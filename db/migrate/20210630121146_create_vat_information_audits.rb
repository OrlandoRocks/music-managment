class CreateVatInformationAudits < ActiveRecord::Migration[6.0]
  def change
    create_table :vat_information_audits do |t|
      t.references :person, null: false
      t.string :company_name
      t.string :vat_registration_number
      t.string :vat_registration_status
      t.string :trader_name
      t.datetime :begin_date
      t.datetime :end_date

      t.timestamps
    end
  end
end


