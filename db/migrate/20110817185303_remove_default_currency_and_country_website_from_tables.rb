class RemoveDefaultCurrencyAndCountryWebsiteFromTables < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE people ALTER COLUMN country_website_id DROP DEFAULT")
    execute("ALTER TABLE person_balances ALTER COLUMN currency DROP DEFAULT")
    execute("ALTER TABLE person_transactions ALTER COLUMN currency DROP DEFAULT")
    execute(" ALTER TABLE balance_adjustments ALTER COLUMN currency DROP DEFAULT;")
    execute(" ALTER TABLE purchases ALTER COLUMN currency DROP DEFAULT;")
    execute(" ALTER TABLE invoices ALTER COLUMN currency DROP DEFAULT;")
    execute(" ALTER TABLE invoice_settlements ALTER COLUMN currency DROP DEFAULT;")
    execute(" ALTER TABLE manual_settlements ALTER COLUMN currency DROP DEFAULT;")

    execute(" ALTER TABLE products ALTER COLUMN country_website_id DROP DEFAULT;")
    execute(" ALTER TABLE products ALTER COLUMN currency DROP DEFAULT;")
    execute(" ALTER TABLE product_items ALTER COLUMN currency DROP DEFAULT;")
    execute(" ALTER TABLE product_item_rules ALTER COLUMN currency DROP DEFAULT;")
    execute(" ALTER TABLE targeted_products ALTER COLUMN currency DROP DEFAULT;")
    execute(" ALTER TABLE price_policies ALTER COLUMN currency DROP DEFAULT;")
    execute(" ALTER TABLE base_items ALTER COLUMN currency DROP DEFAULT;")
    execute(" ALTER TABLE base_item_options ALTER COLUMN currency DROP DEFAULT;")

    execute(" ALTER TABLE paypal_transfers ALTER COLUMN country_website_id DROP DEFAULT;")
    execute(" ALTER TABLE paypal_transfers ALTER COLUMN currency DROP DEFAULT;")
    execute(" ALTER TABLE check_transfers ALTER COLUMN currency DROP DEFAULT;")
    execute(" ALTER TABLE payout_service_fees ALTER COLUMN country_website_id DROP DEFAULT;")
    execute(" ALTER TABLE payout_service_fees ALTER COLUMN currency DROP DEFAULT;")

    execute(" ALTER TABLE paypal_transactions ALTER COLUMN country_website_id DROP DEFAULT;")
    execute(" ALTER TABLE paypal_transactions ALTER COLUMN currency DROP DEFAULT;")
    execute(" ALTER TABLE stored_paypal_accounts ALTER COLUMN country_website_id DROP DEFAULT;")
    execute(" ALTER TABLE braintree_transactions ALTER COLUMN country_website_id DROP DEFAULT;")
    execute(" ALTER TABLE braintree_transactions ALTER COLUMN currency DROP DEFAULT;")
    execute(" ALTER TABLE batch_transactions ALTER COLUMN currency DROP DEFAULT;")
    execute(" ALTER TABLE eft_batch_transactions ALTER COLUMN country_website_id DROP DEFAULT;")
    execute(" ALTER TABLE eft_batch_transactions ALTER COLUMN currency DROP DEFAULT;")
  end

  def self.down
    execute("ALTER TABLE people ALTER COLUMN country_website_id SET DEFAULT '1';")
    execute("ALTER TABLE person_balances ALTER COLUMN currency SET DEFAULT 'USD';")
    execute("ALTER TABLE person_transactions ALTER COLUMN currency SET DEFAULT 'USD';")
    execute("ALTER TABLE balance_adjustments ALTER COLUMN currency SET DEFAULT 'USD';")
    execute("ALTER TABLE purchases ALTER COLUMN currency SET DEFAULT 'USD';")
    execute("ALTER TABLE invoices ALTER COLUMN currency SET DEFAULT 'USD';")
    execute("ALTER TABLE invoice_settlements ALTER COLUMN currency SET DEFAULT 'USD';")
    execute("ALTER TABLE manual_settlements ALTER COLUMN currency SET DEFAULT 'USD';")

    execute("ALTER TABLE products ALTER COLUMN country_website_id SET DEFAULT '1';")
    execute("ALTER TABLE products ALTER COLUMN currency SET DEFAULT 'USD';")
    execute("ALTER TABLE product_items ALTER COLUMN currency SET DEFAULT 'USD';")
    execute("ALTER TABLE product_item_rules ALTER COLUMN currency SET DEFAULT 'USD';")
    execute("ALTER TABLE targeted_products ALTER COLUMN currency SET DEFAULT 'USD';")
    execute("ALTER TABLE price_policies ALTER COLUMN currency SET DEFAULT 'USD';")
    execute("ALTER TABLE base_items ALTER COLUMN currency SET DEFAULT 'USD';")
    execute("ALTER TABLE base_item_options ALTER COLUMN currency SET DEFAULT 'USD';")
    
    execute("ALTER TABLE paypal_transfers ALTER COLUMN country_website_id SET DEFAULT '1';")
    execute("ALTER TABLE paypal_transfers ALTER COLUMN currency SET DEFAULT 'USD';")
    execute("ALTER TABLE check_transfers ALTER COLUMN currency SET DEFAULT 'USD';")
    execute("ALTER TABLE payout_service_fees ALTER COLUMN country_website_id SET DEFAULT '1';")
    execute("ALTER TABLE payout_service_fees ALTER COLUMN currency SET DEFAULT 'USD';")

    execute("ALTER TABLE paypal_transactions ALTER COLUMN country_website_id SET DEFAULT '1';")
    execute("ALTER TABLE paypal_transactions ALTER COLUMN currency SET DEFAULT 'USD';")
    execute("ALTER TABLE stored_paypal_accounts ALTER COLUMN country_website_id SET DEFAULT '1';")
    execute("ALTER TABLE braintree_transactions ALTER COLUMN country_website_id SET DEFAULT '1';")
    execute("ALTER TABLE braintree_transactions ALTER COLUMN currency SET DEFAULT 'USD';")
    execute("ALTER TABLE batch_transactions ALTER COLUMN currency SET DEFAULT 'USD';")
    execute("ALTER TABLE eft_batch_transactions ALTER COLUMN country_website_id SET DEFAULT '1';")
    execute("ALTER TABLE eft_batch_transactions ALTER COLUMN currency SET DEFAULT 'USD';")
  end
end

