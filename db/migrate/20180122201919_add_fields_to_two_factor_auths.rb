class AddFieldsToTwoFactorAuths < ActiveRecord::Migration[4.2]
  def change
    remove_column :two_factor_auths, :dismissals

    add_column :two_factor_auths, :authy_id, :string
    add_column :two_factor_auths, :country_code, :string
    add_column :two_factor_auths, :phone_number, :string
    add_column :two_factor_auths, :notification_method, :string
    add_column :two_factor_auths, :activated_at, :datetime
    add_column :two_factor_auths, :disabled_at, :datetime
    add_column :two_factor_auths, :last_verified_at, :datetime
    add_column :two_factor_auths, :last_failed_at, :datetime

    add_index :two_factor_auths, :authy_id
  end
end
