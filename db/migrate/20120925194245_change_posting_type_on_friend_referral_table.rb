class ChangePostingTypeOnFriendReferralTable < ActiveRecord::Migration[4.2]
  def self.up
    change_column :friend_referrals, :posting_type, :string
  end

  def self.down
    change_column :friend_referrals, :posting_type, :integer
  end
end
