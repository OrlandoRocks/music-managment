class CreateIndexesForTargetedCerts < ActiveRecord::Migration[4.2]
  def up
    add_index :targeted_certs, :code, :unique
  end

  def down
    remove_index :targeted_certs, :code
  end
end
