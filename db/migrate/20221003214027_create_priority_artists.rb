class CreatePriorityArtists < ActiveRecord::Migration[6.0]
  def change
    create_table :priority_artists do |t|
      t.references :person, null: false
      t.references :artist, null: false
      t.index [:person_id, :artist_id], unique: true

      t.timestamps
    end
  end
end
