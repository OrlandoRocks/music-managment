class AddUniqIndexToInvoiceSequence < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      add_index :invoices, %i[corporate_entity_id invoice_sequence], unique: true
    end
  end
end
