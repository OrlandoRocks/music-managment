class CreateTableRedistributionPackages < ActiveRecord::Migration[4.2]
  def up
    create_table :redistribution_packages do |t|
      t.integer :person_id
      t.integer :artist_id
      t.string :state, default: 'pending'
      t.string :converter_class
    end
    add_index :redistribution_packages, [:person_id, :artist_id, :converter_class], name: "person_artist_store"
  end

  def down
    drop_table :redistribution_packages
  end
end
