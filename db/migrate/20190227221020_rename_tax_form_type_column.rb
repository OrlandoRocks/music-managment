class RenameTaxFormTypeColumn < ActiveRecord::Migration[4.2]
  def up
    rename_column :tax_forms, :type, :form_type
  end

  def down
    rename_column :tax_forms, :form_type, :type
  end
end
