class AddAmountCurrencyToSalesRecordMasters < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE sales_record_masters
     ADD COLUMN amount_currency CHAR(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency' AFTER tariff_name")
  end

  def self.down
    remove_column :sales_record_masters, :amount_currency
  end
end
