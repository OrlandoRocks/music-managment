class UpdateStoreCopyrightClaims < ActiveRecord::Migration[6.0]
  def change
  	safety_assured {
      change_column :copyright_claims, :store_id, :integer, null: true
    }
  end
end
