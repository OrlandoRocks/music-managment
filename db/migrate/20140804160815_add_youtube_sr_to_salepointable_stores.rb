class AddYoutubeSrToSalepointableStores < ActiveRecord::Migration[4.2]
  @@short_name = 'YoutubeSR'

  def up
    store = Store.where(short_name: @@short_name).first_or_create!(
      name: 'Youtube Sound Recording',
      abbrev: 'ytsr',
      position: 470,
      needs_rights_assignment: false,
      is_active: false,
      base_price_policy_id: 3,
      is_free: false
    )

    store.salepointable_stores.create!(:salepointable_type => 'Album')
    store.salepointable_stores.create!(:salepointable_type => 'Single')
  end

  def down
    if store = Store.find_by(short_name: @@short_name)
      store.salepointable_stores.collect(&:destroy)
    else
      puts "Couldn't find store by the short name of #{@@short_name}"
    end
  end
end
