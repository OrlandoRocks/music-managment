# 2012-1-17 AK relationship between albums and countries, for limiting distribution to certain countries (for rights assignment)
class CreateAlbumCountry < ActiveRecord::Migration[4.2]
  def self.up
    create_table :album_countries do |t|
      t.integer :album_id
      t.integer :country_id
      t.string :relation_type 
    end
  end

  def self.down
    drop_table :album_countries
  end
end
