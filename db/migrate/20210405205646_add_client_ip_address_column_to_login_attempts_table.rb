class AddClientIpAddressColumnToLoginAttemptsTable < ActiveRecord::Migration[6.0]
  def self.up
    safety_assured do
      add_column :login_attempts, :client_ip_address, :string
      add_index :login_attempts, :client_ip_address
    end
  end

  def self.down
    safety_assured do
      remove_index :login_attempts, name: "index_login_attempts_on_client_ip_address"
      remove_column :login_attempts, :client_ip_address
    end
  end
end
