class AddFieldsToComposers < ActiveRecord::Migration[4.2]
  def self.up
    change_table :composers do |t|
      t.string :name_prefix, :limit => 4
      t.string :name_suffix, :limit => 4
      t.string :alternate_email, :limit => 100
      t.string :country, :limit => 2
      t.string :alternate_phone, :limit => 20
      t.integer :publisher_id
      t.remove :pro_affiliation
    end
  end

  def self.down
    change_table :composers do |t|
      t.remove :name_prefix
      t.remove :name_suffix
      t.remove :alternate_email
      t.remove :country
      t.remove :alternate_phone
      t.remove :publisher_id
      t.string :pro_affilation, :limit => 20
    end
  end
end
