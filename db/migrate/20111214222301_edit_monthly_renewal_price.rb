class EditMonthlyRenewalPrice < ActiveRecord::Migration[4.2]
  def self.up
    #editing the monthly renewal price to $4.99
    monthly_album_renewal = Product.find_by(id: 33, :name=>'Monthly Album Renewal')
    monthly_album_renewal.update(:price => 4.99) if !monthly_album_renewal.nil?
  end

  def self.down
    #editing the monthly renewal price to $8.00
    monthly_album_renewal = Product.find_by(id: 33, :name=>'Monthly Album Renewal')
    monthly_album_renewal.update(:price => 8.00) if !monthly_album_renewal.nil?
  end
end
