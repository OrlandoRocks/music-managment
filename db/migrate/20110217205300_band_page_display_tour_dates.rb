class BandPageDisplayTourDates < ActiveRecord::Migration[4.2]
  def self.up
    add_column :bandpages, :display_tour_dates, :boolean, :default => true
  end

  def self.down
    remove_column :bandpages, :display_tour_dates
  end
end
