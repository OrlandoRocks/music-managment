class AddDropkloudProducts < ActiveRecord::Migration[4.2]
  def up
    execute("ALTER TABLE purchases CHANGE related_type related_type enum('Album','Single','Ringtone','Entitlement','Product','Renewal','ItunesUserReport','MusicVideo','FeatureFilm','Salepoint','Song','Widget','Composer','Video','SalepointSubscription','CreditUsage','Booklet','SalepointPreorderData','SoundoutReport','FacebookRecognitionServicePurchase','YoutubeMonetization','PreorderPurchase', 'MasteredTrack', 'DropkloudPurchase');")

    create_table  :dropkloud_products do |t|
      t.string    :product_name
      t.string    :product_type
      t.integer   :product_id
      t.integer   :term_length
      t.timestamps
    end

    create_table  :dropkloud_purchases do |t|
      t.integer  :person_id, null: false
      t.integer  :dropkloud_product_id, null: false
      t.datetime :effective_date
      t.datetime :termination_date
      t.timestamps
    end

    product_us = Product.create({
      created_by_id:      1,
      country_website_id: 1,
      name:               "Dropkloud",
      display_name:       "Dropkloud",
      description:        "Dropkloud",
      status:             "Active",
      product_type:       "Ad Hoc",
      is_default:         1,
      applies_to_product: "None",
      sort_order:         1,
      renewal_level:      "None",
      price:              8.99,
      currency:           "USD"
    })

    product_item_us = ProductItem.create({
      product_id:   product_us.id,
      name:         "Dropkloud",
      description:  "Dropkloud",
      price:        8.99,
      currency:     "USD"
    })

    product_item_rule_us = ProductItemRule.create({
      product_item_id:      product_item_us.id,
      rule_type:            "inventory",
      rule:                 "price_for_each",
      inventory_type:       "Dropkloud",
      true_false:           false,
      quantity:             0,
      unlimited:            1,
      minimum:              0,
      maximum:              0,
      price:                0,
      currency:             "USD"
    })

    DropkloudProduct.create({
      product_name: "Dropkloud",
      product_type: "unlimited_monthly",
      term_length: 1,
      product_id: product_us.id
    })

  end

  def down
    execute("ALTER TABLE purchases CHANGE related_type related_type enum('Album','Single','Ringtone','Entitlement','Product','Renewal','ItunesUserReport','MusicVideo','FeatureFilm','Salepoint','Song','Widget','Composer','Video','SalepointSubscription','CreditUsage','Booklet','SalepointPreorderData','SoundoutReport','FacebookRecognitionServicePurchase','YoutubeMonetization','PreorderPurchase', 'MasteredTrack');")

    drop_table :dropkloud_products
    drop_table :dropkloud_purchases

    execute("DELETE product_item_rules FROM product_item_rules inner join product_items on product_item_rules.product_item_id = product_items.id where product_items.name = 'Dropkloud';")
    execute("DELETE FROM product_items WHERE name = 'Dropkloud';")
    execute("DELETE FROM products WHERE name = 'Dropkloud';")


  end
end
