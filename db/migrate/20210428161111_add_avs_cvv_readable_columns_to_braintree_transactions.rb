class AddAvsCvvReadableColumnsToBraintreeTransactions < ActiveRecord::Migration[6.0]
  def change
    add_column :braintree_transactions, :avs_response_code, :string, null: true
    add_column :braintree_transactions, :avs_message, :string, null: true
    add_column :braintree_transactions, :avs_postal_response, :string, null: true
    add_column :braintree_transactions, :avs_postal_message, :string, null: true
    add_column :braintree_transactions, :avs_street_address_response, :string, null: true
    add_column :braintree_transactions, :avs_street_address_message, :string, null: true
    add_column :braintree_transactions, :cvv_response_code, :string, null: true
    add_column :braintree_transactions, :cvv_message, :string, null: true
  end
end
