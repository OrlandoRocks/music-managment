class AddIsSanctionedToCountries < ActiveRecord::Migration[4.2]
  def change
    change_column :countries, :id, :smallint
    add_column :countries, :is_sanctioned, :boolean, default: false
  end
end
