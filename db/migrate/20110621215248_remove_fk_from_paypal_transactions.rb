class RemoveFkFromPaypalTransactions < ActiveRecord::Migration[4.2]
  def self.up
    # this was already run in production, just including it here, and will uncomment it after deploy 
    # so that dev matches production
    execute("ALTER TABLE paypal_transactions DROP FOREIGN KEY FK_paypal_pt_invoice_id;")
  end

  def self.down
    execute("ALTER TABLE `paypal_transactions` ADD CONSTRAINT `FK_paypal_pt_invoice_id` FOREIGN KEY (`invoice_id`) REFERENCES `invoices`(`id`);")
  end
end
