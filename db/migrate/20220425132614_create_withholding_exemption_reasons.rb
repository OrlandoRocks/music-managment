class CreateWithholdingExemptionReasons < ActiveRecord::Migration[6.0]
  def change
    create_table :withholding_exemption_reasons do |t|
      t.string :name, index: { unique: true }, null: false
      t.string :description
      t.timestamps
    end
  end
end
