class CreatePayuTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :payu_transactions do |t|
      t.references :person, null: false
      t.references :invoice, null: false
      t.boolean :status
      t.string :payu_id
      t.integer :amount, null: false
      t.timestamps
    end
    add_index :payu_transactions, :payu_id
  end
end
