class ChangeWimpToTidal < ActiveRecord::Migration[4.2]
  def up
    store = Store.find_by(short_name: "Wimp")
    if store
      store.update(name: "Tidal")
    end
  end

  def down
    store = Store.find_by(short_name: "Tidal")
    if store
      store.update(name: "Wimp")
    end
  end
end
