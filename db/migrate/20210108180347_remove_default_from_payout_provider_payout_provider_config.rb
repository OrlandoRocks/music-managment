class RemoveDefaultFromPayoutProviderPayoutProviderConfig < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      change_column :payout_providers,
                    :payout_provider_config_id,
                    :bigint,
                    default: nil
    end
  end

  def down
    safety_assured do
      change_column :payout_providers,
                    :payout_provider_config_id,
                    :bigint,
                    default: PayoutProviderConfig.by_env.find_by_program_id(PayoutProviderConfig::TC_US_PROGRAM_ID).id
    end
  end
end
