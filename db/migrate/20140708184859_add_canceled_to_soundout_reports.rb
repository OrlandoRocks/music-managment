class AddCanceledToSoundoutReports < ActiveRecord::Migration[4.2]
  def change
    add_column :soundout_reports, :canceled_at, :datetime, :default=>nil
  end
end
