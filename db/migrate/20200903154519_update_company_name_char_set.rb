class UpdateCompanyNameCharSet < ActiveRecord::Migration[6.0]
  def self.up
    safety_assured do
      execute %(
        ALTER TABLE vat_informations
          MODIFY COLUMN company_name VARCHAR(255)
          CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL
      )
    end
  end

  def self.down
    safety_assured do
      execute %(
        ALTER TABLE vat_informations
          MODIFY COLUMN company_name VARCHAR(255)
          CHARACTER SET utf8
      )
    end
  end
end
