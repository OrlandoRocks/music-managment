class AddDefaultToStoreDeliveryConfigsUseResourceDir < ActiveRecord::Migration[4.2]
  def change
    change_column_default :store_delivery_configs, :use_resource_dir, true
  end
end
