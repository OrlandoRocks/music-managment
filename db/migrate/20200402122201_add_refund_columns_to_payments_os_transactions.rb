class AddRefundColumnsToPaymentsOsTransactions < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      change_table :payments_os_transactions do |t|
        t.bigint :original_transaction_id
        t.string :refund_reason
        t.string :refund_category
        t.string :refund_id
        t.text   :refund_raw_response
        t.column :refund_status, "enum('Pending','Succeed','Failed')"
      end
    end
  end
end
