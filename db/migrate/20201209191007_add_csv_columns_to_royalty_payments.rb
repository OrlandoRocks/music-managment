class AddCsvColumnsToRoyaltyPayments < ActiveRecord::Migration[6.0]
  def change
    safety_assured {
      add_column :royalty_payments, :csv_summary_file_name, :string
      add_column :royalty_payments, :csv_summary_file_size, :integer
      add_column :royalty_payments, :csv_summary_file_updated_at, :integer
    }
  end
end
