class AddMoreIndexesToMumaTables < ActiveRecord::Migration[4.2]
  def self.up
    add_index :muma_songs, :parent_code
    add_index :muma_song_societies, :song_code
  end

  def self.down
    remove_index :muma_songs, :parent_code
    remove_index :muma_song_societies, :song_code
  end
end
