class AddUserMappedAtToTaxFormRevenueStream < ActiveRecord::Migration[6.0]
  def change
    add_column :tax_form_revenue_streams, :user_mapped_at, :datetime
  end
end
