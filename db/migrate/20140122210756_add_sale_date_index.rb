class AddSaleDateIndex < ActiveRecord::Migration[4.2]
  def up
    sql = "create index index_albums_on_sale_date on albums (sale_date);"
    ActiveRecord::Base.connection.execute(sql)
  end

  def down
    sql = "drop index index_albums_on_sale_date on albums;"
    ActiveRecord::Base.connection.execute(sql)
  end
  
end
