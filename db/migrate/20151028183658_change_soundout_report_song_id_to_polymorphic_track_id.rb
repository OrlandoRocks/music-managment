class ChangeSoundoutReportSongIdToPolymorphicTrackId < ActiveRecord::Migration[4.2]
  def up
    remove_column :soundout_reports, :song_library_upload_id
    change_column :soundout_reports, :song_id, :integer, null: false
    rename_column :soundout_reports, :song_id, :track_id
    ActiveRecord::Base.connection.execute("ALTER TABLE soundout_reports ADD COLUMN track_type enum('Song', 'SongLibraryUpload') NOT NULL DEFAULT 'Song'")
  end

  def down
    add_column :soundout_reports, :song_library_upload_id, :integer, null: true
    remove_column :soundout_reports, :track_type
    rename_column :soundout_reports, :track_id, :song_id
    change_column :soundout_reports, :song_id, null: true
  end
end
