class EditMonthlyRenewalProducts < ActiveRecord::Migration[4.2]
  def self.up
    #editing the renewal products to apply to Album/Single/Ringtone, since they incorrectly apply to 'None'
    album_renewal = Product.find_by(id: 1, :name=>'Album Renewal')
    album_renewal.update(:applies_to_product => 'Album', :name=>"Yearly Album Renewal", :display_name => "Yearly Album Renewal") if !album_renewal.nil?
    # ringtone renewal
    ringtone_renewal = Product.find_by(id: 2, :name=>'Ringtone Renewal')
    ringtone_renewal.update(:applies_to_product => 'Ringtone') if !ringtone_renewal.nil?
    # single renewal
    single_renewal = Product.find_by(id: 3, :name=>'Single Renewal')
    single_renewal.update(:applies_to_product => 'Single') if !single_renewal.nil?
  end

  def self.down
    #rollback to incorrect state of 'None'
    album_renewal = Product.find_by(id: 1, :name=>'Yearly Album Renewal')
    album_renewal.update(:applies_to_product => 'None') if !album_renewal.nil?
    # ringtone renewal
    ringtone_renewal = Product.find_by(id: 2, :name=>'Ringtone Renewal')
    ringtone_renewal.update(:applies_to_product => 'None') if !ringtone_renewal.nil?
    # single renewal
    single_renewal = Product.find_by(id: 3, :name=>'Single Renewal')
    single_renewal.update(:applies_to_product => 'None') if !single_renewal.nil?
  end
end
