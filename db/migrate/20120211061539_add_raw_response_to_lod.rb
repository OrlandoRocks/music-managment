class AddRawResponseToLod < ActiveRecord::Migration[4.2]
  def self.up
    add_column :lods, :raw_response, :text
  end

  def self.down
    remove_column :lods, :raw_response
  end
end
