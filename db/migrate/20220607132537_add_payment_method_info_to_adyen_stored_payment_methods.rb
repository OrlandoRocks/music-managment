class AddPaymentMethodInfoToAdyenStoredPaymentMethods < ActiveRecord::Migration[6.0]
  def up
    add_reference :adyen_stored_payment_methods, :adyen_payment_method_info, index: { name: :adyen_payment_method_info }
  end

  def down
    remove_reference :adyen_stored_payment_methods, :adyen_payment_method_info
  end
end
