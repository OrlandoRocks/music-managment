class ChangeProductDisplayNamesToKeys < ActiveRecord::Migration[4.2]
  UPDATE = {"1_yr"=>"1 Year", "10_album_creds"=>"10 Album Distribution Credits", "10_ringtone_creds"=>"10 Ringtone Distribution Credits", "10_single_creds"=>"10 Single Distribution Credits", "2_more_yrs"=>"2 More Years", "2_yr_album_renewal"=>"2 Year Album Renewal", "2_yr_single_renewal"=>"2 Year Single Renewal", "2_yrs"=>"2 Years", "20_album_creds"=>"20 Album Distribution Credits", "20_ringtone_creds"=>"20 Ringtone Distribution Credits", "20_single_creds"=>"20 Single Distribution Credits", "3_ringtone_creds"=>"3 Ringtone Distribution Credits", "4_more_yrs"=>"4 More Years", "5_album_creds"=>"5 Album Distribution Credits", "5_ringtone_creds"=>"5 Ringtone Distribution Credits", "5_single_creds"=>"5 Single Distribution Credits", "5_yr_album_renewal"=>"5 Year Album Renewal", "5_yr_single_renewal"=>"5 Year Single Renewal", "5_yrs"=>"5 Years", "store"=>"Added Store", "album"=>"Album Distribution", "credit"=>"Album Distribution Credit", "renewal"=>"Album Renewal", "ytsr"=>"Collect Your YouTube Sound Recording Revenue", "booklet"=>"Distribute Booklet", "credit_usage"=>"Distribute With Credit", "dropkloud"=>"Dropkloud", "facebook"=>"Facebook audio recognition", "film"=>"Feature Film Distribution", "free_store"=>"Free Store", "landr"=>"LANDR Instant Mastering", "lifetime"=>"Lifetime", "monthly_album"=>"Monthly Album Renewal", "monthly"=>"Monthly Plan", "monthly_renewal"=>"Monthly Renewal", "monthly_trends"=>"Monthly Trend Report Subscription", "video"=>"Music Video Distribution", "video_renewal"=>"Music Video Distribution Renewal", "preorder"=>"Preorder", "pub_admin"=>"Publishing Administration Service", "ringtone"=>"Ringtone Distribution", "ringtone_credit"=>"Ringtone Distribution Credit", "ringtone_renewal"=>"Ringtone Renewal", "single"=>"Single Distribution", "single_credit"=>"Single Distribution Credit", "single_renewal"=>"Single Renewal", "automator"=>"Store Automator", "trend"=>"Trend Report", "trend_subscription"=>"Trend Report Subscription", "fan_reviews_2"=>"TuneCore Fan Reviews Enhanced", "fan_reviews_3"=>"TuneCore Fan Reviews Premium", "fan_reviews_1"=>"TuneCore Fan Reviews Starter", "widget"=>"Widget Subscription", "yearly_renewal"=>"Yearly Album Renewal"}

  def up
    UPDATE.each do |key, display_name|
      Product.where(display_name: display_name).each { |prod| prod.update(display_name: key) }
    end
  end

  def down
    UPDATE.each do |key, display_name|
      Product.where(display_name: key).each { |prod| prod.update(display_name: display_name) }
    end
  end
end
