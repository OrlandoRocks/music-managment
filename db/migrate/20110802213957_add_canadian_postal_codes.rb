class AddCanadianPostalCodes < ActiveRecord::Migration[4.2]
  def self.up
    create_table :canada_postal_codes do |t|
      t.integer :id, :null => false
      t.string :code, :null => false, :limit => 7
      t.string :city, :null => false, :limit => 40
      t.string :province, :limit => 24
      t.string :province_code, :limit => 2
      t.string :city_type, :limit => 5
      t.string :latitude, :limit => 32
      t.string :longitude, :limit => 32
    end

  end

  def self.down
    drop_table :canada_postal_codes
  end
end
