class CreateTableBlacklistedArtists < ActiveRecord::Migration[4.2]
  def change
    create_table :blacklisted_artists do | t |
      t.boolean :active, default: true
      t.references :artist, null: false
      t.timestamps
    end

    add_index :blacklisted_artists, :artist_id
  end
end
