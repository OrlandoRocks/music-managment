class AddDiscoveryPlatformToStores < ActiveRecord::Migration[6.0]
  def change
    safety_assured { add_column :stores, :discovery_platform, :boolean, default: false }
  end
end
