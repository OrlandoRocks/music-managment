class UpdateIRSTransactionOnIRSTaxWithholding < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      change_column :irs_tax_withholdings, :irs_transaction_id, :bigint, null: true
    end
  end
end
