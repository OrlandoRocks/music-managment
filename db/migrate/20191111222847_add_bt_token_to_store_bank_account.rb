class AddBtTokenToStoreBankAccount < ActiveRecord::Migration[4.2]
  def change
    add_column :stored_bank_accounts, :bt_token, :string
  end
end
