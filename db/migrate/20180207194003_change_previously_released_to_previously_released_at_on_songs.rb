class ChangePreviouslyReleasedToPreviouslyReleasedAtOnSongs < ActiveRecord::Migration[4.2]
  def up
    rename_column :songs, :previously_released, :previously_released_at
    change_column :songs, :previously_released_at, :date
  end

  def down
    rename_column :songs, :previously_released_at, :previously_released
    change_column :songs, :previously_released, :boolean
  end
end
