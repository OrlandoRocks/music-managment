class AddColumnPaymentChannelToSubscriptionPurchase < ActiveRecord::Migration[4.2]
  def change
    add_column :subscription_purchases, :payment_channel, :string
  end
end
