class MakeActiveProviderRequiredToPayoutProviders < ActiveRecord::Migration[4.2]
  def change
    change_column_null(:payout_providers, :active_provider, false )
  end
end
