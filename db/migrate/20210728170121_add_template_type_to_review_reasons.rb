class AddTemplateTypeToReviewReasons < ActiveRecord::Migration[6.0]
  def change
    add_column :review_reasons, :template_type, :string
  end
end
