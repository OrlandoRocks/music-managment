class ChangeItunesComprehensivesToArchivesItunesComprehensives < ActiveRecord::Migration[6.0]
  def change
  	safety_assured do
  	  rename_table :itunes_comprehensives, :archives_itunes_comprehensives
  	end
  end
end
