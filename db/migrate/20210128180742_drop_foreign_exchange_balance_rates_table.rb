# This table has been succeeded by foreign_exchange_rates table
class DropForeignExchangeBalanceRatesTable < ActiveRecord::Migration[6.0]
  def change
    drop_table :foreign_exchange_balance_rates
  end
end
