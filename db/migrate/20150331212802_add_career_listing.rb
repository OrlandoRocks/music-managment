class AddCareerListing < ActiveRecord::Migration[4.2]
  def self.up
    create_table  :career_listings do |t|
      t.string    :listing_title, null: false
      t.string    :listing_location, null: false
      t.text      :listing_description, null: false
      t.integer   :display_order
      t.boolean   :is_active, null: false, default: false
      t.timestamps
    end

    create_table  :career_listing_items do |t|
      t.integer   :career_listing_id, null: false
      t.string    :listing_item_type, null: false
      t.string    :listing_item_content, null: false
      t.integer   :display_order
      t.boolean   :is_active, null: false, default: false
      t.timestamps
    end

    create_table  :career_listing_boilerplates do |t|
      t.text      :boilerplate_text, null: false
      t.integer   :display_order
      t.boolean   :is_active, null: false, default: true
      t.timestamps
    end

    execute "ALTER TABLE career_listing_items ADD CONSTRAINT fk_career_listing FOREIGN KEY (career_listing_id) references career_listings(id)"

    CareerListingBoilerplate.create({
      display_order:    10,
      is_active:        true,
      boilerplate_text: "TuneCore is the premier digital music distributor with one of the largest music catalogs in the world. Since its launch in 2006, over 8 billion TuneCore Artists' music units have been downloaded or streamed, generating over $430 million in revenue earned by TuneCore Artists, ranging from indie artists to high-profile performers."
    })

    CareerListingBoilerplate.create({
      display_order:    20,
      is_active:        true,
      boilerplate_text: "For an annual flat fee, TuneCore Distribution provides an easy-to-use, affordable digital solution for artists to get their music distributed worldwide to iTunes, Amazon MP3, Spotify, Rdio, Google Play, and other major download and streaming sites. Artists keep 100% of their sales revenue and all their rights."
    })

    CareerListingBoilerplate.create({
      display_order:    30,
      is_active:        true,
      boilerplate_text: "TuneCore Music Publishing Administration gives songwriters worldwide the ability for TuneCore to license and register their compositions globally, collect revenues from over 60 countries and deposit the royalty revenue directly into their TuneCore account. Driven by in-house Creative, TuneCore actively markets compositions for sync licensing agreements for music placements in TV, film, commercials, video games, and more. The exclusive Sync & Master Licensing Database provides Music Supervisors access to TuneCore Artists' music for commercial use."
    })

  end

  def self.down
    drop_table :career_listing_items
    drop_table :career_listings
    drop_table :career_listing_boilerplates
  end
end
