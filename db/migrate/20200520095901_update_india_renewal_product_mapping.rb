class UpdateIndiaRenewalProductMapping < ActiveRecord::Migration[6.0]
  # Ref 20161122211448_fix_italian_french_renewal_products
  def up
    renewal_product_mapping = {
      Product::IN_TWO_YEAR_ALBUM_PRODUCT_ID => 444, # 2 Year Album renewal
      Product::IN_FIVE_YEAR_ALBUM_PRODUCT_ID => 446, # 5 Year Album renewal
      Product::IN_TWO_YEAR_SINGLE_PRODUCT_ID => 445, # 2 Year Single renewal
      Product::IN_FIVE_YEAR_SINGLE_PRODUCT_ID => 447 # 5 Year Single renewal
    }

    renewal_product_mapping.each do |product_id, renewal_product_id|
      product = Product.find(product_id)
      product.product_items.first.update!(renewal_product_id: renewal_product_id)
    end
  end

  def down
    old_mapping = {
      Product::IN_TWO_YEAR_ALBUM_PRODUCT_ID => 454, # 2 Year Album renewal
      Product::IN_FIVE_YEAR_ALBUM_PRODUCT_ID => 454, # 5 Year Album renewal
      Product::IN_TWO_YEAR_SINGLE_PRODUCT_ID => 417, # 2 Year Single renewal
      Product::IN_FIVE_YEAR_SINGLE_PRODUCT_ID => 417 # 5 Year Single renewal
    }

    old_mapping.each do |product_id, renewal_product_id|
      product = Product.find(product_id)
      product.product_items.first.update!(renewal_product_id: renewal_product_id)
    end
  end
end
