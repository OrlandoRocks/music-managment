class AddPageSourceColumnToReferralDatum < ActiveRecord::Migration[6.0]
  def change
    safety_assured { add_column :referral_data, :page_source, :string, default: nil }
  end
end
