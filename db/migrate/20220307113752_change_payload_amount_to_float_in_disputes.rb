class ChangePayloadAmountToFloatInDisputes < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      change_column :disputes, :payload_amount, :float
    end
  end
end
