class AddCertFilesToPayoutProviderConfig < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      add_column :payout_provider_configs, :cert_file, :string
      add_column :payout_provider_configs, :key_file, :string
    end
  end
end
