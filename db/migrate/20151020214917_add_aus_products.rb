class AddAusProducts < ActiveRecord::Migration[4.2]
  def up
    price_map = get_pricing_map

    product_ids = [1, 3, 9, 18, 19, 20, 21, 22, 23, 24, 25, 36, 37, 38, 41, 42, 43, 65, 101, 103, 105, 107, 109, 111, 112, 113, 114, 115, 121, 123, 125, 127, 133, 134, 135, 139, 140, 143, 145, 147, 149]

    Product.find(product_ids).sort.each do |p|
      override_price = if p.price.to_f == 9.99 && p.name == "LANDR Instant Mastering"
        13.99
      elsif p.price.to_f == 44.95 && p.name == "Single Distribution Credit - 5 Pack"
        67.78
      elsif p.price.to_f == 15.0 && p.name == "TrackSmarts Starter"
        19.99
      end

      au_product = p.dup
      au_product.update(country_website_id: 4, currency: "AUD", price: override_price || price_map[p.price.to_f] || 0)

      p.product_items.each do |pi|
        au_product_item = pi.dup
        au_product_item.update(product_id: au_product.id, currency: "AUD", price: price_map[pi.price.to_f] || 0)

        pi.product_item_rules.each do |pir|
          au_product_item_rule = pir.dup
          au_product_item_rule.update(product_item_id: au_product_item.id, currency: "AUD", price: price_map[pir.price.to_f] || 0)
        end
      end
    end

    FacebookRecognitionServiceProduct.create(product_name: "Facebook audio recognition", min_releases: 0, max_releases: 1, product: Product.where("name = 'Facebook audio recognition' and country_website_id = 4 and price = 3.19").first)
    FacebookRecognitionServiceProduct.create(product_name: "Facebook audio recognition", min_releases: 2, max_releases: 10, product: Product.where("name = 'Facebook audio recognition' and country_website_id = 4 and price = 6.99").first)
    FacebookRecognitionServiceProduct.create(product_name: "Facebook audio recognition", min_releases: 11, max_releases: 999999999, product: Product.where("name = 'Facebook audio recognition' and country_website_id = 4 and price = 13.99").first)

    SoundoutProduct.create(report_type: "starter", display_name: "TrackSmarts Starter Report", number_of_reviews: 40, available_in_days: 5, product: Product.where("name = 'TrackSmarts Starter' and country_website_id = 4").first)
    SoundoutProduct.create(report_type: "enhanced", display_name: "TrackSmarts Enhanced Report", number_of_reviews: 100, available_in_days: 5, product: Product.where("name = 'TrackSmarts Enhanced' and country_website_id = 4").first)
    SoundoutProduct.create(report_type: "premium", display_name: "TrackSmarts Premium Report", number_of_reviews: 225, available_in_days: 5, product: Product.where("name = 'TrackSmarts Premium' and country_website_id = 4").first)
  end

  def down
    ProductItemRule.where(currency: "AUD").delete_all
    ProductItem.where(currency: "AUD").delete_all
    Product.where(currency: "AUD").delete_all
  end

  def get_pricing_map
    price_map = {}
    price_map[9.99] = 13.99
    price_map[29.99] = 41.99
    price_map[19.99] = 27.99
    price_map[18.98] = 26.69
    price_map[44.95] = 62.99
    price_map[75.98] = 106.39
    price_map[206.95] = 289.99
    price_map[49.99] = 69.99
    price_map[94.98] = 132.99
    price_map[224.96] = 315.99
    price_map[1.98] = 2.79
    price_map[134.95] = 188.99
    price_map[263.91] = 367.99
    price_map[515.82] = 720.99
    price_map[87.91] = 122.99
    price_map[171.82] = 241.99
    price_map[56.99] = 79.99
    price_map[89.99] = 125.99
    price_map[169.99] = 237.99
    price_map[319.99] = 447.99
    price_map[20.0] = 27.99
    price_map[10.0] = 13.99
    price_map[15.0] = 20.99
    price_map[25.0] = 34.99
    price_map[75.0] = 104.99
    price_map[2.25] = 3.19
    price_map[5.0] = 6.99
    price_map[40.0] = 55.99
    price_map[115.0] = 160.99
    price_map[0.0] = 0.0

    price_map
  end
end
