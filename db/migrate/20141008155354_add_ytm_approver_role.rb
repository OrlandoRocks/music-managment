class AddYtmApproverRole < ActiveRecord::Migration[4.2]
  def up
    role = Role.create({
      name:               "YTM Approver",
      long_name:          "YouTube Money Approver",
      description:        "Allows access to approve and reject songs for YouTube monetization",
      is_administrative:  false
    })

    Right.create({
      controller: "ytm/salepoint_songs",
      action:     "index",
      name:       "salepoint_songs/index",
      roles: [role]
    })

    Right.create({
      controller: "ytm/salepoint_songs",
      action:     "update",
      name:       "salepoint_songs/update",
      roles: [role]
    })

    Right.create({
      controller: "ytm/salepoint_songs",
      action:     "play_song",
      name:       "salepoint_songs/play_song",
      roles: [role]
    })
  end

  def down
    Right.where("controller = 'ytm/salepoint_songs' and action in ('index','update','play_song')").destroy_all
    Role.find_by(name: "YTM Approver").destroy
  end
end
