class AddDefaultsToTrackMonetizations < ActiveRecord::Migration[4.2]
  def change
    change_column_default :track_monetizations, :eligibility_status, "pending"
    change_column_default :track_monetizations, :delivery_type, "full_delivery"
    change_column_default :track_monetizations, :state, "new"
  end
end
