class AddWithdrawMethodChangedToPayoutTransfers < ActiveRecord::Migration[4.2]
  def change
    add_column :payout_transfers, :withdraw_method_changed, :bool
    remove_column :payout_transfers, :payout_withdrawal_type_id
  end
end
