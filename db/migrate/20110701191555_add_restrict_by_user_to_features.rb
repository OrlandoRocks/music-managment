class AddRestrictByUserToFeatures < ActiveRecord::Migration[4.2]
  def self.up
    add_column :features, :restrict_by_user, :boolean, :default => false
  end

  def self.down
    remove_column :features, :restrict_by_user
  end
end
