class AddCurrencyToInvoiceItems < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE invoice_items ADD currency CHAR(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency for invoice item' AFTER cost_at_settlement_cents")
  end

  def self.down
    remove_column :invoice_items, :currency
  end
end
