class AddRefundedAtToMasteredTracks < ActiveRecord::Migration[4.2]
  def change
    add_column :mastered_tracks, :refunded_at, :datetime
  end
end
