require "#{Rails.root}/lib/utilities/databases"

class AddIndexesForSongwriterTables < ActiveRecord::Migration[4.2]
  def self.up
    begin
      if Utilities::Databases.index_exist?('composers', 'person_id')
        puts "Index composers.person_id exist"
      else
        execute "LOCK TABLES composers WRITE"
        execute "ALTER TABLE composers ADD INDEX person_id(person_id)"
      end
    
      if Utilities::Databases.index_exist?('publishing_splits', 'composer_id')
        puts "Index publishing_splits.composer_id exist"
      else
        execute "LOCK TABLES publishing_splits WRITE"
        execute "ALTER TABLE publishing_splits ADD INDEX composer_id(composer_id, composition_id)"
      end
    
      if Utilities::Databases.index_exist?('publishing_splits', 'composition_id')
        puts "Index publishing_splits.composition_id exist"
      else
        execute "LOCK TABLES publishing_splits WRITE"
        execute "ALTER TABLE publishing_splits ADD INDEX composition_id(composition_id)"
      end
    ensure
      execute "UNLOCK TABLES"
    end
  end

  def self.down
    execute "ALTER TABLE composers DROP INDEX person_id"
    execute "ALTER TABLE publishing_splits DROP INDEX composer_id"
    execute "ALTER TABLE publishing_splits DROP INDEX composition_id"
  end
end
