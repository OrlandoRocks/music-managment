class UpdateMinReleasesForFacebookProducts < ActiveRecord::Migration[4.2]
  def up
    fb_products = FacebookRecognitionServiceProduct.where("max_releases = 1")
    fb_products.each { |fb_product| fb_product.update_attribute(:min_releases, 0) }
  end

  def down
    fb_products = FacebookRecognitionServiceProduct.where("max_releases = 1")
    fb_products.each { |fb_product| fb_product.update_attribute(:min_releases, 1) }
  end
end
