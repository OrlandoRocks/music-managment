class CreateReportingDashboardTables < ActiveRecord::Migration[4.2]
  def up
    create_table  :reporting_dashboard_rollups do |t|
      t.integer   :reporting_date_id
      t.datetime  :report_date
      t.integer   :country_website_id
      t.decimal   :total_sales, :precision => 12, :scale => 2
      t.decimal   :average_sales, :precision => 12, :scale => 2
      t.integer   :new_dist_first_purchase
      t.integer   :new_dist_not_first_purchase
      t.integer   :returning_dist
      t.integer   :renewals
      t.integer   :pub_first_purchase
      t.integer   :pub_not_first_purchase
      t.timestamps
    end

    create_table  :reporting_dashboard_products do |t|
      t.integer   :reporting_date_id
      t.datetime  :report_date
      t.integer   :country_website_id
      t.integer   :product_id
      t.string    :product_name
      t.string    :product_family
      t.integer   :product_count
      t.decimal   :product_amount, :precision => 12, :scale => 2
      t.timestamps
    end

    create_table  :reporting_dates do |t|
      t.datetime  :reporting_date
      t.integer   :day_of_month
      t.integer   :day_of_year
      t.string    :day_display
      t.integer   :week_of_year
      t.integer   :week_year
      t.string    :week_display
      t.integer   :month_of_year
      t.integer   :month_year
      t.string    :month_display
      t.integer   :quarter
      t.integer   :quarter_year
      t.string    :quarter_display
      t.integer   :year
      t.timestamps
    end

    execute "ALTER TABLE reporting_dashboard_rollups ADD CONSTRAINT fk_rollup_report_date FOREIGN KEY (reporting_date_id) references reporting_dates(id)"

    execute "ALTER TABLE reporting_dashboard_products ADD CONSTRAINT fk_product_report_date FOREIGN KEY (reporting_date_id) references reporting_dates(id)"

    start_date = Date.parse("2007-01-01", "%Y-%m-%d")
    end_date = Date.parse("2020-12-31", "%Y-%m-%d")
    quarters = { "01" => 1, "02" => 1, "03" => 1, "04" => 2, "05" => 2, "06" => 2, "07" => 3, "08" => 3, "09" => 3, "10" => 4, "11" => 4, "12" => 4 }
    (start_date..end_date).each do |d|
      ReportingDate.create({
        reporting_date:   d,
        day_of_month:     d.strftime("%d").to_i,
        day_of_year:      d.strftime("%j").to_i,
        day_display:      d.strftime("%b %d, %Y"),
        week_of_year:     d.strftime("%U").to_i,
        week_year:        d.strftime("%Y%U").to_i,
        week_display:     "#{d.beginning_of_week(:sunday).strftime("%m/%d")} - #{d.end_of_week(:sunday).strftime("%m/%d")}",
        month_of_year:    d.strftime("%m").to_i,
        month_year:       d.strftime("%Y%m").to_i,
        month_display:    d.beginning_of_month.strftime("%b %Y"),
        quarter:          quarters[d.strftime("%m")],
        quarter_year:     "#{d.strftime("%Y")}#{quarters[d.strftime("%m")]}".to_i,
        quarter_display:  "Q#{quarters[d.strftime("%m")]} '#{d.strftime("%y")}",
        year:             d.strftime("%Y").to_i
      })
    end
  end

  def down
    drop_table :reporting_dashboard_rollups
    drop_table :reporting_dashboard_products
    drop_table :reporting_dates
  end
end
