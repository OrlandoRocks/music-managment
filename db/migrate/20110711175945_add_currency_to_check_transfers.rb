class AddCurrencyToCheckTransfers < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE check_transfers ADD currency CHAR(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency for check' AFTER admin_charge_cents")
  end

  def self.down
    remove_column :check_transfers, :currency
  end
end
