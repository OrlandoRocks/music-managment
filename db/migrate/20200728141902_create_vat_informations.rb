class CreateVatInformations < ActiveRecord::Migration[6.0]
  def change
    create_table :vat_informations do |t|
      t.integer :person_id
      t.string :company_name
      t.string :vat_registration_number
      t.string :vat_registration_status
      t.string :trader_name
      t.timestamps
    end
  end
end
