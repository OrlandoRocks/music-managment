class AddStatusToPurchases < ActiveRecord::Migration[4.2]
  def change
    add_column :purchases, :status, :string, default: 'new'
  end
end
