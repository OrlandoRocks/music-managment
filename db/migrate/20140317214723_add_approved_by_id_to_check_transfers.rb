class AddApprovedByIdToCheckTransfers < ActiveRecord::Migration[4.2]
  def change
    add_column :check_transfers, :approved_by_id, :integer
  end
end
