class CreateTierCertifications < ActiveRecord::Migration[6.0]
  def change
    create_table :tier_certifications do |t|
      t.references :tier, foreign_key: true
      t.references :certification, foreign_key: true

      t.timestamps
    end
  end
end
