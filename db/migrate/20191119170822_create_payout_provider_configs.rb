class CreatePayoutProviderConfigs < ActiveRecord::Migration[4.2]
  def change
    create_table :payout_provider_configs do |t|
      t.string :name
      t.column :currency, 'char(3)', null: false

      t.string :program_id
      t.string :username
      t.string :password

      t.boolean :sandbox

      t.timestamps null: false
    end
  end
end
