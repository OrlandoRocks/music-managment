class AddSecretsToCipherData < ActiveRecord::Migration[6.0]
  def change
    add_column :cipher_data, :secrets, :string
  end
end
