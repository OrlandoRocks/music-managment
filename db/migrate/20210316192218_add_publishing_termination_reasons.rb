class AddPublishingTerminationReasons < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      create_table :publishing_termination_reasons do |t|
        t.string :reason, null: false
      end

      add_column :terminated_composers, :publishing_termination_reason_id, :integer
      rename_column :terminated_composers, :reason, :admin_note

      PublishingTerminationReason.create([
        { :reason => 'Found a new publisher / administrator' },
        { :reason => 'Not happy with customer service' },
        { :reason => 'No longer needed' },
        { :reason => 'Royalties were less than expected' },
        ])
    end
  end
end
