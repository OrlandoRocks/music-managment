class MigrateToForeignExchangeRates < ActiveRecord::Migration[6.0]
  def change
    create_table :foreign_exchange_rates do |t|
      t.float :exchange_rate, null: false
      t.column  :source_currency, 'char(3)', null: false
      t.column  :target_currency, 'char(3)', null: false
      t.datetime :valid_from
      t.datetime :valid_till

      t.timestamps null: false
    end 
  end
end
