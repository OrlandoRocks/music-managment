class ChangeReceiptDataToText < ActiveRecord::Migration[4.2]
  def change
    change_column :payment_channel_receipts, :receipt_data, :text
  end
end
