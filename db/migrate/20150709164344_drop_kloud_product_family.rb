class DropKloudProductFamily < ActiveRecord::Migration[4.2]
  def up
    Product.where("name = 'Dropkloud'").update_all("product_family = 'Artist Services (non-distribution)'")
  end

  def down
  end
end
