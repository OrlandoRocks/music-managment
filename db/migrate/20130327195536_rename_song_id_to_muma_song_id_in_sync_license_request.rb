class RenameSongIdToMumaSongIdInSyncLicenseRequest < ActiveRecord::Migration[4.2]
  def self.up
    rename_column :sync_license_requests, :song_id, :muma_song_id
  end

  def self.down
    rename_column :sync_license_requests, :muma_song_id, :song_id
  end
end
