class CreatePostingMapping < ActiveRecord::Migration[6.0]
  def change
    create_table :posting_mappings, comment: "Stores the mapping between sip/symphony posting id to tc-www's posting_id" do |t|
      t.string :posting_id, null: false, index: true
      t.bigint :external_posting_id, null: false, index: true
      t.column :source, "enum('sip','symphony')", default: 'sip'

      t.index [:posting_id, :external_posting_id], unique: true

      t.timestamps
    end
  end
end
