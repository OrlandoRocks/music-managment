class PopulateYoutubeSrMailTemplates < ActiveRecord::Migration[4.2]
  def up
    template_params =
    [
      { template_name: 'removed_monetization', select_label: 'Removed from Monetization' , template_path: 'youtube_sr_mail_templates/removed_monetization', custom_fields: ['third_party'] },
      { template_name: 'instrumental_rights', select_label: 'Rights to Instrumental' , template_path: 'youtube_sr_mail_templates/instrumental_rights', custom_fields: ['third_party'] },
      { template_name: 'disputing_a_claim', select_label: 'Disputing a Claim on Your YouTube Channel' , template_path: 'youtube_sr_mail_templates/disputing_a_claim', custom_fields: ['link_to_video'] },
      { template_name: 'ownership_conflict', select_label: 'YouTube Ownership Conflict' , template_path: 'youtube_sr_mail_templates/ownership_conflict', custom_fields: ['third_party', 'territories'] },
      { template_name: 'authorization', select_label: 'Authorization for Use in Video' , template_path: 'youtube_sr_mail_templates/authorization', custom_fields: ['video_uploader', 'link_to_video'] },
      { template_name: 'disputed_claim', select_label: 'Disputed Claim' , template_path: 'youtube_sr_mail_templates/disputed_claim', custom_fields: ['plaintiff','plaintiff_message', 'link_to_video']  },
      { template_name: 'creative_commons_content', select_label: 'Creative Commons', template_path: 'youtube_sr_mail_templates/creative_commons_content', custom_fields: ['third_party'] }
    ]

    template_params.each { |params| YoutubeSrMailTemplate.create!(params) }
  end

  def down
    YoutubeSrMailTemplate.all.each { |t| t.destroy }
  end
end
