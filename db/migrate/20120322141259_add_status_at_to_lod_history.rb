class AddStatusAtToLodHistory < ActiveRecord::Migration[4.2]
  def self.up
    add_column :lod_history, :status_at, :datetime
  end

  def self.down
    remove_column :lod_history, :status_at
  end
end
