class AddPaymentsOsTransactionsTable < ActiveRecord::Migration[6.0]
  def change
    create_table :payments_os_transactions do |t|
      t.references :person
      t.references :stored_credit_card
      t.references :invoice
      t.string :reconciliation_id
      t.decimal :amount
      t.column :currency, "char(3)"
      t.column :action, "enum('sale','refund') NOT NULL"
      t.string :payment_id
      t.column :payment_status, "enum('Initialized','Credited','Pending','Authorized','Captured','Refunded','Voided','Failed') NOT NULL"
      t.text :payment_raw_response
      t.string :charge_id
      t.column :charge_status, "enum('Pending','Succeed','Failed') NOT NULL"
      t.text :charge_raw_response
      t.string :ip_address
      t.timestamps
    end
  end
end
