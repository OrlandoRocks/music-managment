class AddCustomerTypeToPurchaseTaxInfo < ActiveRecord::Migration[6.0]
  def change
    add_column :purchase_tax_information, :customer_type, "ENUM('Individual', 'Business')", default: nil
  end
end
