class ChangeDMusicStoreToKuackStore < ActiveRecord::Migration[4.2]
  def up
    store = Store.find_by(name: "D'Music")
    if store != nil
      store.name = "Kuack"
      store.save
      puts "Kuack store name updated"
    end
  end

  def down
    store = Store.find_by(name: "Kuack")
    if store != nil
      store.name = "D'Music"
      store.save
      puts "D'Music store name restored"
    end
  end
end
