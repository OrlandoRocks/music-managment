class CreateSongDistributionOptions < ActiveRecord::Migration[4.2]
  def change
    create_table :song_distribution_options do |t|
      t.integer :song_id, null: false
      t.string :option_name
      t.boolean :option_value
    end

    add_index :song_distribution_options, [:song_id, :option_name], :unique => true
  end
end
