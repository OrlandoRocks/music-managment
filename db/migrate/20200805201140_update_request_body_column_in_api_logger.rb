class UpdateRequestBodyColumnInApiLogger < ActiveRecord::Migration[6.0]
  def self.up
    safety_assured {
      execute %Q(
        ALTER TABLE api_loggers
          MODIFY COLUMN request_body text
          CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL
      )
    }
  end

  def self.down
    safety_assured {
      execute %Q(
        ALTER TABLE api_loggers
          MODIFY COLUMN request_body text
          CHARACTER SET utf8
      )
    }
  end
end
