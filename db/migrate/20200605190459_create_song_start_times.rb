class CreateSongStartTimes < ActiveRecord::Migration[6.0]
  def change
    create_table :song_start_times do |t|
      t.belongs_to :song, null: false
      t.belongs_to :store, null: false
      t.integer :start_time, default: 0, null: false

      t.timestamps
    end

    add_index :song_start_times, [:song_id, :store_id]
  end
end
