class CreateKickbackEventsTable < ActiveRecord::Migration[4.2]
  def self.up
    create_table :kickback_events do |t|
      t.integer :person_id
      t.string :event_type
      t.text :raw_request
      t.text :raw_response
      t.timestamps
    end
  end

  def self.down
    drop_table :kickback_events
  end
end
