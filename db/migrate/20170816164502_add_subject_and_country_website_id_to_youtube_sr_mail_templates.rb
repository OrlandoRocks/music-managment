class AddSubjectAndCountryWebsiteIdToYoutubeSrMailTemplates < ActiveRecord::Migration[4.2]
  def change
    add_column :youtube_sr_mail_templates, :subject, :string
    add_column :youtube_sr_mail_templates, :header, :text
    add_column :youtube_sr_mail_templates, :body, :text
    add_column :youtube_sr_mail_templates, :footer, :text
    add_column :youtube_sr_mail_templates, :country_website_id, :integer
    add_index :youtube_sr_mail_templates, :country_website_id
  end
end
