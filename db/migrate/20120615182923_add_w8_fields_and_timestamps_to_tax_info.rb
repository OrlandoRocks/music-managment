class AddW8FieldsAndTimestampsToTaxInfo < ActiveRecord::Migration[4.2]
  def self.up
    add_column :tax_info, :country_of_corp, :string, :limit => 80 
    add_column :tax_info, :owner_type, :string, :limit => 30
    add_column :tax_info, :mailing_address_1, :string, :limit => 45
    add_column :tax_info, :mailing_address_2, :string, :limit => 45
    add_column :tax_info, :mailing_city, :string, :limit => 45
    add_column :tax_info, :mailing_state, :string, :limit => 2
    add_column :tax_info, :mailing_zip, :string, :limit => 10
    add_column :tax_info, :mailing_country, :string, :limit => 2
    add_column :tax_info, :agreed_to_w8ben_at, :datetime
    add_column :tax_info, :created_at, :datetime
    add_column :tax_info, :updated_at, :datetime
  end

  def self.down
    remove_column :tax_info, :country_of_corp
    remove_column :tax_info, :owner_type
    remove_column :tax_info, :mailing_address_1
    remove_column :tax_info, :mailing_address_2
    remove_column :tax_info, :mailing_city
    remove_column :tax_info, :mailing_state
    remove_column :tax_info, :mailing_zip
    remove_column :tax_info, :mailing_country
    remove_column :tax_info, :agreed_to_w8ben_at
    remove_column :tax_info, :created_at
    remove_column :tax_info, :updated_at
  end
end
