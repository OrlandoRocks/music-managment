class AddUnknownFlagToCowriters < ActiveRecord::Migration[4.2]
  def change
    add_column :cowriters, :is_unknown, :boolean, default: false
  end
end
