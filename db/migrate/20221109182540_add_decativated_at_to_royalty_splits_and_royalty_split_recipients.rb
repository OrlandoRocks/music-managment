class AddDecativatedAtToRoyaltySplitsAndRoyaltySplitRecipients < ActiveRecord::Migration[6.0]
  def change
    add_column :royalty_splits, :deactivated_at, :timestamp
    add_column :royalty_split_recipients, :deactivated_at, :timestamp
  end
end
