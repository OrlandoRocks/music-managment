class ChangeExternalServiceIdsStoreNameToServiceName < ActiveRecord::Migration[4.2]
  def up
    rename_column :external_service_ids, :store_name, :service_name
  end

  def down
    rename_column :external_service_ids, :service_name, :store_name
  end
end
