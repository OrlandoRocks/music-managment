class ArchiveWidgets < ActiveRecord::Migration[6.0]
  def self.up
    if table_exists? :widgets
      safety_assured do
        rename_table :widgets, :archive_widgets
      end
    end
  end

  def self.down
    if table_exists? :archive_widgets
      safety_assured do
        rename_table :archive_widgets, :widgets
      end
    end
  end
end
