class ArchiveFacebookRecognitionServiceTables < ActiveRecord::Migration[6.0]
  def up
    rename_index :facebook_recognition_service_products, 'index_facebook_recognition_service_products_on_person_id', 'index_frs_products_on_person_id'
    rename_index :facebook_recognition_service_purchases, 'index_facebook_recognition_service_purchases_on_person_id', 'index_frs_purchases_on_person_id'

    safety_assured do
      rename_table :facebook_recognition_service_products, :archive_facebook_recognition_service_products
      rename_table :facebook_recognition_service_purchases, :archive_facebook_recognition_service_purchases
    end
  end

  def down
    safety_assured do
      rename_table :archive_facebook_recognition_service_products, :facebook_recognition_service_products
      rename_table :archive_facebook_recognition_service_purchases, :facebook_recognition_service_purchases
    end
  end
end
