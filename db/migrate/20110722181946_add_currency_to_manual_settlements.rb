class AddCurrencyToManualSettlements < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE manual_settlements
     ADD COLUMN currency CHAR(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency' AFTER settlement_amount_cents")
  end

  def self.down
    remove_column :manual_settlements, :currency
  end
end
