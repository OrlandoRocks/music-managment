class AddToCategoryEnumForBalanceAdjustments < ActiveRecord::Migration[4.2]
  def self.up
    execute(
    "ALTER TABLE `balance_adjustments` CHANGE `category` `category`
    ENUM('Refund - Renewal','Refund - Other','Service Adjustment','Songwriter Royalty','Other') NOT NULL DEFAULT 'Other' COMMENT 'Type of adjustment';")
  end

  def self.down
    execute(
    "ALTER TABLE `balance_adjustments` CHANGE `category` `category`
    ENUM('Refund - Renewal','Refund - Other','Service Adjustment','Other') NOT NULL DEFAULT 'Other' COMMENT 'Type of adjustment';")
  end
end
