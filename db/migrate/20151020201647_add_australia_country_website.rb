class AddAustraliaCountryWebsite < ActiveRecord::Migration[4.2]
  def up
    CountryWebsite.create(:name=>'TuneCore Australia', :currency=>'AUD', :country => 'AU')
  end

  def down
    CountryWebsite.find_by(country: 'AU').destroy
  end
end
