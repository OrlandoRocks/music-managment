class CreateMumaImportsTable < ActiveRecord::Migration[4.2]
  def self.up
    create_table :muma_imports do |t|
      t.string   :type
      t.datetime :requested_source_updated_at
      t.datetime :created_at
    end
  end

  def self.down
    drop_table :muma_imports
  end
end
