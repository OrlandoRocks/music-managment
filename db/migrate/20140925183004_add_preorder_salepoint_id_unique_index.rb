class AddPreorderSalepointIdUniqueIndex < ActiveRecord::Migration[4.2]
  def up
    #remove duplicates before adding the unique index
    preorders = SalepointPreorderData.find_by_sql("select max(spd.id) as id from salepoint_preorder_data spd
                                                      group by spd.`salepoint_id`
                                                      having count(spd.id) > 1")
    preorder_ids = preorders.map {|pre| pre.id }

    SalepointPreorderData.where("id IN (?)", preorder_ids).delete_all

    add_index :salepoint_preorder_data, :salepoint_id, unique: true
  end

  def down
    remove_index :salepoint_preorder_data, :salepoint_id
  end
end
