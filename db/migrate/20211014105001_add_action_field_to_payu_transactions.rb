class AddActionFieldToPayuTransactions < ActiveRecord::Migration[6.0]
  def up
    add_column :payu_transactions, :action, "enum('sale','refund')", null: false
    change_column_default :payu_transactions, :action, "sale"
  end

  def down
    remove_column :payu_transactions, :action
  end
end
