class DropTargetedOfferColumn < ActiveRecord::Migration[4.2]
  def up
    remove_column :targeted_offers, :delayed_job_status
  end

  def down
    add_column :targeted_offers, :delayed_job_status, :string
  end
end
