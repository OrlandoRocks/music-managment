class AddAssetToLegalDocument < ActiveRecord::Migration[4.2]
  def change
    add_attachment :legal_documents, :asset
  end
end
