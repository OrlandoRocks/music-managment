class CreateSongwriters < ActiveRecord::Migration[4.2]
  def change
    create_table :songwriters do |t|
      t.references :person, null: false
      t.string :first_name, null: false
      t.string :middle_name
      t.string :last_name, null: false
      t.timestamps
    end

    add_index :songwriters, :person_id
  end
end
