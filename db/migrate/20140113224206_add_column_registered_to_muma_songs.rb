class AddColumnRegisteredToMumaSongs < ActiveRecord::Migration[4.2]
  def change
     add_column :muma_songs, :registered, :boolean, :default => 0
  end
end
