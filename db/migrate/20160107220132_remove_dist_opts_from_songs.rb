class RemoveDistOptsFromSongs < ActiveRecord::Migration[4.2]
  def change
    remove_column :songs, :free_song
    remove_column :songs, :album_only
  end
end
