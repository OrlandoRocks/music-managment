class UpdateAcceptedFormat < ActiveRecord::Migration[4.2]
  def self.up
    execute ("update videos set accepted_format = 1 where accepted_format = 0")
    # this migration is to enable legacy videos to be added to cart through the new simplified order system
  end

  def self.down
    # there is no downward migration
  end
end
