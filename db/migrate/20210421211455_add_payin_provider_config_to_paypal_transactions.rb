class AddPayinProviderConfigToPaypalTransactions < ActiveRecord::Migration[6.0]
  def change
    add_reference :paypal_transactions, :payin_provider_config, index: true
  end
end
