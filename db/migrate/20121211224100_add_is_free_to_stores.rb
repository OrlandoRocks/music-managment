class AddIsFreeToStores < ActiveRecord::Migration[4.2]
  def self.up
    add_column :stores, :is_free, :boolean, :default=>false
  end

  def self.down
    remove_column :stores, :is_free
  end
end
