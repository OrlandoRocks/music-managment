class AddVerifiedAtToExternalServiceIds < ActiveRecord::Migration[6.0]
  def up
    safety_assured { add_column :external_service_ids, :verified_at, :datetime }
  end

  def down
    safety_assured { remove_column :external_service_ids, :verified_at }
  end
end
