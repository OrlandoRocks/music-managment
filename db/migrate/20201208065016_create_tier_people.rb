class CreateTierPeople < ActiveRecord::Migration[6.0]
  def change
    create_table :tier_people do |t|
      t.bigint :tier_id, foreign_key: true
      t.integer :person_id, foreign_key: true
      t.string :status, null: false, default: 'active'

      t.timestamps
    end
  end
end
