class AddRevokedAtToWithholdingExemptions < ActiveRecord::Migration[6.0]
  def change
    add_column :withholding_exemptions, :revoked_at, :datetime, comment: "Acts as a flag for the record. When populated, the associated person will no longer be exempt from payout tax withholding."
  end
end
