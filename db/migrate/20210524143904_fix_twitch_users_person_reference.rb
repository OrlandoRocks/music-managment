class FixTwitchUsersPersonReference < ActiveRecord::Migration[6.0]
  def change
    add_column :twitch_users, :person_id, :integer
    add_index :twitch_users, :person_id, unique: true
    safety_assured { remove_column :twitch_users, :people_id }
  end
end
