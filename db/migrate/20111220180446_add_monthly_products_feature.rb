class AddMonthlyProductsFeature < ActiveRecord::Migration[4.2]
  def self.up
    Feature.find_or_create_by(:name=>'monthly_products', :restrict_by_user => 1)
  end

  def self.down
    feature = Feature.find_by(name: 'monthly_products')
    feature.destroy if !feature.nil?
  end
end
