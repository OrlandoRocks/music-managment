class AddFrenchProducts < ActiveRecord::Migration[4.2]
  def up
    product_ids = {
      "1"   => 363, # Yearly Album Renewal
      "3"   => 326, # Single Renewal
      "9"   => 327, # Added Store
      "18"  => 328, # Album Distribution Credit
      "19"  => 329, # Album Distribution Credit - 5 Pack
      "20"  => 330, # Album Distribution Credit - 10 Pack
      "21"  => 331, # Album Distribution Credit - 20 Pack
      "22"  => 332, # Single Distribution Credit
      "23"  => 333, # Single Distribution Credit - 5 Pack
      "24"  => 334, # Single Distribution Credit - 10 Pack
      "25"  => 335, # Single Distribution Credit - 20 Pack
      "36"  => 336, # 1 Year Album
      "37"  => 337, # 2 Year Album
      "38"  => 338, # 5 Year Album
      "41"  => 339, # 1 Year Single
      "42"  => 340, # 2 Year Single
      "43"  => 341, # 5 Year Single
      "65"  => 342, # Songwriter Service
      "101" => 343, # Ringtone Renewal
      "103" => 344, # 1 Year Ringtone
      "105" => 345, # Store Automator
      "107" => 346, # Credit Usage
      "109" => 347, # Booklet
      "111" => 348, # Ringtone Distribution
      "112" => 349, # Ringtone Distribution Credit - 3 Pack
      "113" => 350, # Ringtone Distribution Credit - 5 Pack
      "114" => 351, # Ringtone Distribution Credit - 10 Pack
      "115" => 352, # Ringtone Distribution Credit - 20 Pack
      "121" => 353, # 2 Year Album Renewal
      "123" => 354, # 2 Year Single Renewal
      "125" => 355, # 5 Year Album Renewal
      "127" => 356, # 5 Year Single Renewal
      "133" => 357, # TuneCore Fan Reviews Starter
      "134" => 358, # TuneCore Fan Reviews Enhanced
      "135" => 359, # TuneCore Fan Reviews Premium
      "145" => 360, # Collect Your YouTube Sound Recording Revenue
      "147" => 361, # Preorder
      "149" => 362  # LANDR Instant Mastering
    }

    # use ids from above based on name of product
    renewal_product_ids = {
      "336" => 363, # 1 Year Album
      "337" => 363, # 2 Year Album
      "338" => 363, # 5 Year Album
      "328" => 363, # Album Distribution Credit
      "329" => 363, # Album Distribution Credit - 5 Pack
      "330" => 363, # Album Distribution Credit - 10 Pack
      "331" => 363, # Album Distribution Credit - 20 Pack
      "332" => 326, # Single Distribution Credit
      "333" => 326, # Single Distribution Credit - 5 Pack
      "334" => 326, # Single Distribution Credit - 10 Pack
      "335" => 326, # Single Distribution Credit - 20 Pack
      "339" => 326, # 1 Year Single
      "340" => 326, # 2 Year Single
      "341" => 326, # 5 Year Single
      "344" => 343, # 1 Year Ringtone
      "348" => 343, # Ringtone Distribution
      "349" => 343, # Ringtone Distribution Credit - 3 Pack"9"
      "350" => 343, # Ringtone Distribution Credit - 5 Pack
      "351" => 343, # Ringtone Distribution Credit - 10 Pack
      "352" => 343, # Ringtone Distribution Credit - 20 Pack
    }

    product_ids.each do |us_id, french_id|
      p = Product.find(us_id)
      french_product = p.dup
      french_product.id = french_id
      french_product.country_website_id = 6
      french_product.currency = "EUR"
      french_product.created_by = Person.first
      french_product.save!

      p.product_items.each do |pi|
        french_product_item = pi.dup
        french_product_item.update!(
          product_id: french_product.id,
          currency: "EUR",
          renewal_product_id: renewal_product_ids[french_product.id.to_s]
        )

        pi.product_item_rules.each do |pir|
          french_product_item_rule = pir.dup
          french_product_item_rule.update(
            product_item_id: french_product_item.id,
            currency: "EUR"
          )
        end
      end
    end

    SoundoutProduct.create(report_type: "starter", display_name: "TrackSmarts Starter Report", number_of_reviews: 40, available_in_days: 5, product: Product.find(357))
    SoundoutProduct.create(report_type: "enhanced", display_name: "TrackSmarts Enhanced Report", number_of_reviews: 100, available_in_days: 5, product: Product.find(358))
    SoundoutProduct.create(report_type: "premium", display_name: "TrackSmarts Premium Report", number_of_reviews: 225, available_in_days: 5, product: Product.find(359))
  end

  def down
    [363,326,327,328,329,330,331,332,333,334,335,336,
    337,338,339,340,341,342,343,344,345,346,347,348,349,
    350,351,352,353,354,355,356,357,358,359, 360, 361,362].each do |p_id|
      Product.find(p_id).destroy
    end
  end
end
