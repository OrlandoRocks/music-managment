class CreateTaxFormTypes < ActiveRecord::Migration[6.0]
  def change
    create_table :tax_form_types do |t|
      t.string :kind, null: false
      t.datetime :created_at, null: false
    end
  end
end
