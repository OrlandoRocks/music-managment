class CreateBatchBalanceAdjustmentDetails < ActiveRecord::Migration[4.2]
  def self.up
    create_table :batch_balance_adjustment_details do |t|
      t.integer :person_id
      t.string :code
      t.string :name
      t.decimal :minimum_payment, :precision => 23, :scale => 14
      t.string :hold_payment
      t.decimal :current_balance, :precision => 23, :scale => 14
      t.string :status
      t.integer :batch_balance_adjustment_id
      t.timestamp
    end
  end

  def self.down
    drop_table :batch_balance_adjustment_details
  end
end
