class RenamePersonGstinToGstInfo < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      rename_table :person_gstins, :gst_infos
    end
  end
end
