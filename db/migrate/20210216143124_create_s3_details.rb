class CreateS3Details < ActiveRecord::Migration[6.0]
  def change
    create_table :s3_details do |t|
      t.bigint :file_size
      t.string :file_type
      t.text :metadata_json
      t.integer :s3_asset_id, foreign_key: {to_table: :s3_assets}, null: false
      t.timestamps
    end
  end
end
