class RenameSalesRecordsV2 < ActiveRecord::Migration[4.2]
  def up
    sql = %Q{
      RENAME TABLE sales_records_v2 TO sales_record_archives;
    }

    puts "Running: #{sql}"
    ActiveRecord::Base.connection.execute(sql)
  end

  def down
    sql = %Q{
      RENAME TABLE sales_record_archives TO sales_records_v2;
    }

    puts "Running: #{sql}"
    ActiveRecord::Base.connection.execute(sql)
  end
end
