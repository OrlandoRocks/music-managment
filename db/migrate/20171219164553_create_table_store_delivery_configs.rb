class CreateTableStoreDeliveryConfigs < ActiveRecord::Migration[4.2]
  def up
    create_table :store_delivery_configs do |t|
      t.string  :party_id
      t.string  :party_full_name
      t.integer :image_size
      t.string  :file_type
      t.string  :username
      t.string  :password
      t.string  :hostname
      t.string  :keyfile
      t.boolean :skip_batching
      t.boolean :use_proprietary_id
      t.string  :track_price_code
      t.string  :album_price_code
      t.integer :port
      t.string  :ddex_version
      t.boolean :skip_media
      t.boolean :include_behalf
      t.boolean :remote_dir_timestamped
      t.string  :remote_dir
      t.boolean :display_track_id
      t.boolean :display_track_language
      t.boolean :use_resource_dir
      t.boolean :channels
      t.boolean :include_assets
      t.integer :store_id, index: true, unique: true
      t.string  :transfer_type
      t.string  :provider
      t.integer :bit_depth
      t.boolean :normalize_bit_depth
      t.string  :delivery_queue
    end

    Seed.seed("#{Rails.root}/db/seed/csv/store_delivery_configs.csv")
  end

  def down
    drop_table :store_delivery_configs
  end
end
