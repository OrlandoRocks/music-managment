class CreateVideoTypeColumn < ActiveRecord::Migration[4.2]
  def self.up
    add_column :videos, :video_type, "ENUM('MusicVideo', 'FeatureFilm')"      
  end

  def self.down
      remove_column :videos, :video_type
  end
end
