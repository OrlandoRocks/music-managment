class AddRenewalOptionsToTargetedProducts < ActiveRecord::Migration[4.2]
  def self.up
    add_column :targeted_products, :renewals_past_due, :boolean, :default=>true
    add_column :targeted_products, :renewals_due, :boolean, :default=>true
    add_column :targeted_products, :renewals_upcomming, :boolean, :default=>true
  end

  def self.down
    remove_column :targeted_products, :renewals_past_due
    remove_column :targeted_products, :renewals_due
    remove_column :targeted_products, :renewals_upcomming
  end

end
