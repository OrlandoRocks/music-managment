class ArchiveManualSettlements < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      rename_table :manual_settlements, :archive_manual_settlements
    end
  end
end
