class RestructureInvoiceNumberFields < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      remove_column :invoices, :invoice_prefix, :string
      add_reference :invoices, :corporate_entity, foreign_key: true
      rename_column :invoices, :invoice_number, :invoice_sequence
      add_index :invoices, :invoice_sequence
      
      add_column :corporate_entities, :inbound_invoice_prefix, "ENUM('BI-INV', 'TC-INV')", default: nil
      add_column :invoice_static_corporate_entities, :inbound_invoice_prefix, :string
      
      rename_column :outbound_invoices, :invoice_prefix, :user_invoice_prefix
      rename_column :outbound_invoices, :invoice_number, :invoice_sequence
      add_index :outbound_invoices, :invoice_sequence
    end
  end
end
