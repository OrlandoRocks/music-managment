class CreatePayuWebhookLogs < ActiveRecord::Migration[6.0]
  def change
    create_table :payu_webhook_logs do |t|
      t.boolean :success, null: false
      t.references :payu_transaction, null: false
      t.datetime :payu_transaction_created_at, null: false

      t.timestamps
    end
  end
end
