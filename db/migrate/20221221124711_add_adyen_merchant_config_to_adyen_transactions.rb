class AddAdyenMerchantConfigToAdyenTransactions < ActiveRecord::Migration[6.0]
  def change
    add_reference :adyen_transactions, :adyen_merchant_config, index: true
  end
end
