class ChangeSalepointAuditsToArchivesSalepointAudits < ActiveRecord::Migration[6.0]
  def change
  	safety_assured do
  	  rename_table :salepoint_audits, :archives_salepoint_audits
  	end
  end
end
