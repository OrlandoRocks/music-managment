class AddStatsColToMumaImportsTable < ActiveRecord::Migration[4.2]
  def self.up
    add_column :muma_imports, :stats, :text
  end

  def self.down
    remove_column :muma_imports, :stats
  end
end
