class AddForeignExchangeRateToDisputes < ActiveRecord::Migration[6.0]
  def change
    add_reference :disputes, :foreign_exchange_rates, foreign_key: true
  end
end
