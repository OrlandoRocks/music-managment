class AddNameToCohort < ActiveRecord::Migration[4.2]
  def change
    add_column :tax_tokens_cohorts, :name, :string, after: :id
  end
end
