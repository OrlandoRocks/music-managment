class CreatePlanAddons < ActiveRecord::Migration[6.0]
  def change
    create_table :plan_addons do |t|
      t.references :person, null: false
      t.string :addon_type, null: false, limit: 50
      t.datetime :paid_at
      t.timestamps
    end
  end
end
