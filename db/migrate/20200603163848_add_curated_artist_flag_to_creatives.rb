class AddCuratedArtistFlagToCreatives < ActiveRecord::Migration[6.0]
  def change
    add_column :creatives, :curated_artist_flag, :boolean
  end
end
