class RenameArtistRevenueReportsToArchiveArtistRevenueReports < ActiveRecord::Migration[6.0]
  def change
    safety_assured { rename_table :artist_revenue_reports, :archive_artist_revenue_reports }
  end
end
