class FixIndiaProductItemRules < ActiveRecord::Migration[6.0]
  def change
    # We had to empty this migration as the IDs referred here before were different in each envs
    # and since it was identified, emptying this to be a non-blocking but a valid migration.
    # The intended change is moved over to the below mentioned rake task.

    puts "Empty migration. Refer lib/tasks/transient/fix_india_product_item_rules.rake"
  end
end
