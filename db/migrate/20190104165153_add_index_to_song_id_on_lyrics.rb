class AddIndexToSongIdOnLyrics < ActiveRecord::Migration[4.2]
  def change
    add_index :lyrics, :song_id
  end
end
