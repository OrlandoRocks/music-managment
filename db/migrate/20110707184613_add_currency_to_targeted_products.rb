class AddCurrencyToTargetedProducts < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE targeted_products ADD currency CHAR(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency for targeted product' AFTER price_adjustment")
  end

  def self.down
    remove_column :targeted_products, :currency
  end
end
