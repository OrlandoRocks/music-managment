class CreateDirectAdvanceEarningsPeriodRecords < ActiveRecord::Migration[4.2]

  def up
    create_table :direct_advance_earnings_period_records do |t|
      t.integer     :person_id
      t.decimal     :amount, :precision => 23, :scale => 2
      t.text        :period
      t.date        :report_run_on
      t.timestamps
    end

    add_index :direct_advance_earnings_period_records, :person_id
    add_index :direct_advance_earnings_period_records, :report_run_on
  end

  def down
    drop_table :direct_advance_earnings_period_records
  end

end
