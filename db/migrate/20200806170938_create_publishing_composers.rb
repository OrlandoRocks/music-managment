class CreatePublishingComposers < ActiveRecord::Migration[6.0]
  def up
    create_table :publishing_composers do |t|
      t.references :person
      t.references :account
      t.references :lod
      t.references :publisher
      t.references :country
      t.references :publishing_role
      t.references :performing_rights_organization

      t.string :first_name, limit: 45, null: false
      t.string :last_name, limit: 45, null: false
      t.string :address_1, limit: 45
      t.string :address_2, limit: 45
      t.string :city, limit: 45
      t.string :state, limit: 2
      t.string :zip, limit: 10
      t.string :cae, limit: 11
      t.string :email, limit: 100
      t.string :phone, limit: 20

      t.datetime :agreed_to_terms_at

      t.string :name_prefix, limit: 6
      t.string :name_suffix, limit: 6
      t.string :alternate_email, limit: 100
      t.string :alternate_phone, limit: 20

      t.column :pro_songwriter_affiliation_answer, "enum('I don''t know','Yes','No')"
      t.column :pro_publisher_affiliation_answer, "enum('I don''t know','Yes','No')"

      t.string :pro_songwriter_affiliation, limit: 20
      t.string :middle_name, limit: 100

      t.date :dob

      t.boolean :nosocial, default: 0

      t.string :pub_admin_status

      t.datetime :terminated_at

      t.string :provider_identifier

      t.boolean :perform_live, default: 0
      t.boolean :sync_opted_in, default: nil

      t.datetime :sync_opted_updated_at

      t.timestamps
    end

    add_index :publishing_composers, :provider_identifier
  end

  def down
    drop_table :publishing_composers
  end
end
