class AddDisplayOnPageToWidgets < ActiveRecord::Migration[4.2]
  def self.up
    add_column :widgets, :display_on_page, :boolean, :default => false
  end

  def self.down
    remove_column :widgets, :display_on_page
  end
end
