class RemoveBetaFromYouTubeMusicKeyStore < ActiveRecord::Migration[4.2]
  def up
    store = Store.find_by(short_name: "YTMusic")
    if store
      store.update(name: "YouTube Music Key")
    end
  end

  def down
    store = Store.find_by(short_name: "YTMusic")
    if store
      store.update(name: "YouTube Music Key Beta")
    end
  end
end
