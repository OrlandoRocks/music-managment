class AddCountryWebsiteIdToCmsSets < ActiveRecord::Migration[4.2]
  def change
    add_column :cms_sets, :country_website_id, :integer, null: false
    remove_index :cms_sets, name: :index_cms_sets_on_callout_type_and_start_tmsp
    add_index :cms_sets, :country_website_id
    add_index :cms_sets, [:callout_type, :start_tmsp, :country_website_id], unique: true, :name => "by_type_tmsp_country"

    # make existing US
    CmsSet.all.each { |c| c.update(country_website_id: 1) }

    # create new cms sets for other country websites
    [2, 3, 4, 5].each do |country_website_id|
      ["login", "interstitial", "dashboard"].each do |type|
        set = CmsSet.new(country_website_id: country_website_id, callout_type: type, callout_name: "None", start_tmsp: Time.now + 2) # the +2 is to get around the validation
        set.save!
      end
    end
  end
end
