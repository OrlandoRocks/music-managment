class CreateCaptchaWhitelists < ActiveRecord::Migration[4.2]
  def change
    create_table :captcha_whitelists do |t|
      t.string :ip, null: false
      t.string :description, null: false

      t.timestamps
    end
  end
end
