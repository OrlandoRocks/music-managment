class UpdateUkPricing < ActiveRecord::Migration[4.2]
  PRODUCT_ITEMS_RULES_MAP = {
    370 => { "new" =>  '23.99', "old" => '19.99'}, # 1 year album
    373 => { "new" =>  '60.78', "old" => '51.49'}, # 2 Year Album
    376 => { "new" =>  '165.56', "old" => '140.49'}, # 5 Year Album
    382 => { "new" =>  '7.49', "old" => '5.99'}, # 1 year single
    385 => { "new" =>  '14.23', "old" => '11.39'}, # 2 year single
    388 => { "new" =>  '33.71', "old" => '26.99'}, # 5 year single
    392 => { "new" =>  '15.99', "old" => '13.99'}, # 1 Year Ringtone
    423 => { "new" =>  '11.99', "old" => '9.99'}, # Preorder (One)
    424 => { "new" =>  '19.99', "old" => '16.99'} # Preorder (Both)
  }

  PRODUCT_ITEMS_MAP = {
    124 => { "new" =>  '7.99', "old" => '6.99'}, # Store Automator
    135 => { "new" =>  '7.99', "old" => '6.99'} # Collect Your YouTube Sound Recording Revenue
  }

  PRODUCTS_MAP = {
    153 => { "new" =>  '39.99', "old" => '33.99'}, # Yearly Album Renewal
    155 => { "new" =>  '7.49', "old" => '5.99 '}, # Single Renewal
    157 => { "new" =>  '23.99', "old" => '19.99'}, # Album Distribution Credit
    158 => { "new" =>  '107.96', "old" => '89.99'}, # Album Distribution Credit - 5 Pack
    159 => { "new" =>  '211.11', "old" => '175.99'}, # Album Distribution Credit - 10 Pack
    160 => { "new" =>  '412.63', "old" => '342.99'}, # Album Distribution Credit - 20 Pack
    161 => { "new" =>  '7.49', "old" => '5.99'}, # Single Distribution Credit
    162 => { "new" =>  '33.71', "old" => '26.99'}, # Single Distribution Credit - 5 Pack
    163 => { "new" =>  '65.91', "old" => '52.99'}, # Single Distribution Credit - 10 Pack
    164 => { "new" =>  '128.83', "old" => '102.99'}, # Single Distribution Credit - 20 Pack
    173 => { "new" =>  '55.99', "old" => '49.99'},  # Songwriter Service
    175 => { "new" =>  '15.99', "old" => '13.99'}, # 1 Year Ringtone
    176 => { "new" =>  '7.99', "old" => '6.99'}, # Store Automator
    179 => { "new" =>  '15.99', "old" => '13.99'}, # Ringtone Distribution Credit
    180 => { "new" =>  '45.99', "old" => '39.99'}, # Ringtone Distribution Credit - 3 Pack
    181 => { "new" =>  '71.96', "old" => '62.99'}, # Ringtone Distribution Credit - 5 Pack
    182 => { "new" =>  '135.92', "old" => '118.99'}, # Ringtone Distribution Credit - 10 Pack
    183 => { "new" =>  '255.84', "old" => '223.99'}, # Ringtone Distribution Credit - 20 Pack
    184 => { "new" =>  '75.98', "old" => '64.89'}, # 2 Year Album Renewal
    185 => { "new" =>  '14.23', "old" => '11.39'}, # 2 Year Single Renewal
    186 => { "new" =>  '179.96', "old" => '153.79'}, # 5 Year Album Renewal
    187 => { "new" =>  '33.71', "old" => '26.99'}, # 5 Year Single Renewal
    194 => { "new" =>  '7.99', "old" => '6.99'} # Collect Your YouTube Sound Recording Revenue
  }

  def up
    p "Updating Product Item Rules"
    update_prices(PRODUCT_ITEMS_RULES_MAP, ProductItemRule)
    p "Updating Product Items"
    update_prices(PRODUCT_ITEMS_MAP, ProductItem)
    p "Updating Products"
    update_prices(PRODUCTS_MAP, Product)
  end

  def down
    p "Updating Product Item Rules"
    update_prices(PRODUCT_ITEMS_RULES_MAP, ProductItemRule, true)
    p "Updating Product Items"
    update_prices(PRODUCT_ITEMS_MAP, ProductItem, true)
    p "Updating Products"
    update_prices(PRODUCTS_MAP, Product, true)
  end

  def update_prices(price_map, product_model, revert=false)
    price_map.each do |id, price|
      p "#{revert ? 'Reverting' : 'Updating'} #{product_model.name} #{id}"
      product_record = product_model.find(id)
      raise "#{id} IS NOT A UK PRODUCT!" if product_record.currency != 'GBP'
      product_record.price = price[revert ? "old" : "new"]
      product_record.created_by_id = 1 if product_model == Product
      product_record.save!
    end
  end
end
