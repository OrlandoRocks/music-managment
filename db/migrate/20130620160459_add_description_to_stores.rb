class AddDescriptionToStores < ActiveRecord::Migration[4.2]
  def self.up
    change_table :stores do |t|
      t.text :description
    end

    stores = [ { :sname => "RhapsodyRH", :desc => "A key player in the U.S., Rhapsody bought Napster and is expanding the Napster brand across Europe.<br />
<strong>Strength:</strong> Music focus. Rhapsody and Napster are pure music brands and are not just in the game to sell devices." },
               { :sname => "Medianet", :desc => "One distribution point that gets you in multiple stores. MediaNet powers services like MOG (soon to be Beats Music). <br />
<strong>Strength:</strong> Connections. Easy access to multiple retailers, all through one click." },
               { :sname => "eMusic", :desc => "Be part of a highly-curated download service with discounted offerings for subscribers. <br />
<strong>Strength:</strong> Dedicated user base. eMusic subscribers are considered frequent music consumers who value the store's opinion." },
               { :sname => "GroupieTun", :desc => "A middle man who connects you with virtual reality service IMVU, the new HMV Digital Music store and many more.<br />
<strong>Strength:</strong> Connections. Vervelife builds music experiences for brands and as their network grows, so does yours." },
               { :sname => "Amazon", :desc => "The biggest retailer on planet Earth also sells downloads in all major music markets.<br />
<strong>Strength:</strong> Ubiquity. You can reach over 80 million people." },
               { :sname => "Nokia", :desc => "Nokia bundle a high-quality MP3 digital download store with a wide variety of their phones and other handheld devices.<br />
<strong>Strength:</strong> Reach.  Nokia sells millions of devices worldwide - with access to music built in." },
               { :sname => "Myspace", :desc => "With a recent high-profile relaunch, MySpace Music gives MySpace users easy access to your music.<br />
<strong>Strength:</strong> In-platform marketing.  Promote your music to your MySpace community." },
               { :sname => "Zune", :desc => "Xbox-ers want to hear your music. Tap into the nearly 35 million Xbox Live members by getting your music on one of the premier gaming consoles.<br />
<strong>Strength:</strong> Platform. Xbox is huge and more and more gamers are trying out Xbox Music." },
               { :sname => "Thumbplay", :desc => "The digital platform for Clear Channel, iHeartRadio gives users access to more than 750 live streams of U.S. radio stations.  It's all about discovery here.<br />
<strong>Strength:</strong> Discovery through radio.  Turn listeners on to your sound." },
               { :sname => "Spotify", :desc => "The on-demand streaming service everyone's talking about. Spotify is available in over 20 countries with big plans to expand. <br />
<strong>Strength:</strong> Reach. Your tracks join a catalog of millions of songs, available for a rapidly growing user base." },
               { :sname => "Google", :desc => "Google's entry into digital music covers downloads and streaming subscriptions.<br />
<strong>Strength:</strong> Platform. Three words - Google, YouTube, Android. 'Nuff said." },
               { :sname => "Simfy", :desc => "Music fans get access to over 16 million songs in the cloud through this music streaming provider in Germany.<br />
<strong>Strength:</strong> Local focus. Germany is one of the 5 biggest music markets in the world." },
               { :sname => "Deezer", :desc => "Available in more countries than any other streaming music service, Deezer gets your music in front of 20 million users.<br />
<strong>Strength:</strong> Reach. Go global with your music." },
               { :sname => "Muve", :desc => "As a Muve Music user, your phone and your music are one. Your mobile plan includes a music subscription.<br />
<strong>Strength:</strong> Bundling. People don't have to think about which service to pick, no apps to install. All the music is already there." },
               { :sname => "Rdio", :desc => "Rdio works on your desktop, Android, BlackBerry, WIndows Phone, iPhone, iPad, Sonos and Roku systems. <br />
<strong>Strength:</strong> Access. Let fans discover your music on their preferred devices." },
               { :sname => "iTunesWW", :desc => "The largest music store on planet Earth. Available in over 100 countries to anyone with a computer, iPad or iPhone.<br />
<strong>Strength:</strong> Ubiquity. You can reach over a hundred million people." },
               { :sname => "Wimp", :desc => "A highly curated by local editorial teams (Scandinavia) with one consumer goal in mind: music discovery.<br />
<strong>Strength:</strong> Editorially curated with a local edge." },
               { :sname => "SonyMU", :desc => "Sony, a household name around the world, gets your music to be played on Sony handhelds, tablets and some third party devices. <br />
<strong>Strength:</strong> Brand. Sony is trusted among music consumers." },
               { :sname => "Gracenote", :desc => "Top-notch music recognition technology that lets music fans identify and discover your music. Gracenote will play a key role in how iTunes Radio works.<br />
<strong>Strength:</strong> Discovery. Hundreds of millions of music fans can find out who you are." } ]

    stores.each do |store_hash|
      store = Store.where("short_name = ?", store_hash[:sname]).first
      if store
        store.description = store_hash[:desc]
        store.save!
      end
    end
  end

  def self.down
    remove_column :stores, :description
  end
end
