class AddE164ToTwoFactorAuths < ActiveRecord::Migration[6.0]
  def change
    add_column :two_factor_auths, :e164, :string
  end
end
