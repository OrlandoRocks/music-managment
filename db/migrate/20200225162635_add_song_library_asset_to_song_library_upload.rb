class AddSongLibraryAssetToSongLibraryUpload < ActiveRecord::Migration[6.0]
  def change
    add_column :song_library_uploads, :song_library_asset_data, :text
  end
end
