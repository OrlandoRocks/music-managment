class AddRouteSegmentToKnowledgebaseLinks < ActiveRecord::Migration[4.2]
  def change
    add_column :knowledgebase_links, :route_segment, :string
  end
end
