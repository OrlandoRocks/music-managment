class RenameKickbacksFeature < ActiveRecord::Migration[4.2]
  def self.up
    feature = Feature.find_by(name: 'kickbacks')
    feature.update_attribute(:name, 'friend_referral') if feature
  end

  def self.down
    feature = Feature.find_by(name: 'friend_referral')
    feature.update_attribute(:name, 'kickbacks') if feature
  end
end
