class AddLastDeliveryTypeToDistribution < ActiveRecord::Migration[4.2]
  def self.up
    add_column    :distributions, :last_delivery_type, :string
  end

  def self.down
    remove_column :distributions, :last_delivery_type, :string
  end
end
