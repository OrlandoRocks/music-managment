class AddPrevStatusToItunesStatuses < ActiveRecord::Migration[4.2]
  def self.up
    add_column :itunes_statuses, :prev_status, :string, :limit => 50
    add_column :itunes_statuses, :prev_status_updated_at, :datetime
  end

  def self.down
    remove_column :itunes_statuses, :prev_status
    remove_column :itunes_statuses, :prev_status_updated_at
  end
end
