class AddPublishingComposerToNotesRelatedType < ActiveRecord::Migration[6.0]
  def up
    safety_assured {
      execute "ALTER TABLE notes MODIFY COLUMN related_type enum('Person','Album','Song','EftBatchTransaction','SubscriptionEvent','Composer', 'PublishingComposer') DEFAULT NULL COMMENT 'Type of entity.'"
    }
  end

  def down
    safety_assured {
      execute "ALTER TABLE notes MODIFY COLUMN related_type enum('Person','Album','Song','EftBatchTransaction','SubscriptionEvent','Composer') DEFAULT NULL COMMENT 'Type of entity.'"
    }
  end
end
