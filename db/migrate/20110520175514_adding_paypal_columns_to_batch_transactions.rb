class AddingPaypalColumnsToBatchTransactions < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE `batch_transactions` 
            ADD COLUMN `stored_paypal_account_id` int(11) unsigned DEFAULT NULL  COMMENT 'Foreign Key to stored paypal_account table' AFTER stored_credit_card_id,
            ADD CONSTRAINT `FK_bt_stored_paypal_account_id` FOREIGN KEY (`stored_paypal_account_id`) REFERENCES `stored_paypal_accounts`(`id`);")
  end

  def self.down
    execute("ALTER TABLE batch_transactions DROP FOREIGN KEY `FK_bt_stored_paypal_account_id`")
    remove_column :batch_transactions, :stored_paypal_account_id
  end
end
