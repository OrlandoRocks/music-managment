class AddExchangeRateColumnsToInvoices < ActiveRecord::Migration[6.0]
  def change
    add_column :invoices, :vat_amount_cents_in_eur, :int
    if table_exists?(:foreign_exchange_balance_rate)
      add_reference :invoices, :foreign_exchange_balance_rate, foreign_key: true
    end
  end
end
