class AddTypeToUpcs < ActiveRecord::Migration[4.2]
  def up
    add_column :upcs, :upc_type, :string
  end

  def down
    remove_column :upcs, :upc_type
  end
end
