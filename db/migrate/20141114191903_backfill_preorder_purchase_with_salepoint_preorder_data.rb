class BackfillPreorderPurchaseWithSalepointPreorderData < ActiveRecord::Migration[4.2]
  def up
    sql1 = "insert into preorder_purchases (album_id, itunes_enabled, paid_at, created_at, updated_at)
            select salepoints.salepointable_id, salepoint_preorder_data.enabled, salepoint_preorder_data.paid_at, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
            from salepoint_preorder_data
            inner join salepoints on salepoint_preorder_data.salepoint_id = salepoints.id and salepoints.salepointable_type = 'Album'
            left outer join preorder_purchases on salepoint_preorder_data.preorder_purchase_id = preorder_purchases.id
            where preorder_purchases.id is null"

    sql2 = "update salepoint_preorder_data
            inner join salepoints on salepoint_preorder_data.salepoint_id = salepoints.id
            inner join albums on salepoints.salepointable_id = albums.id and salepoints.salepointable_type = 'Album'
            inner join preorder_purchases on albums.id = preorder_purchases.album_id
            set preorder_purchase_id = preorder_purchases.id"

    ActiveRecord::Base.transaction { execute(sql1) }
    ActiveRecord::Base.transaction { execute(sql2) }
  end

  def down
    PreorderPurchase.delete_all
  end
end
