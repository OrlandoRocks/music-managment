class ChangeSongParentalCleanToNullable < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      execute("
        ALTER TABLE songs
        MODIFY COLUMN parental_advisory TINYINT(1) NULL DEFAULT NULL, ALGORITHM=INPLACE, LOCK=NONE,
        MODIFY COLUMN clean_version TINYINT(1) NULL DEFAULT NULL, ALGORITHM=INPLACE, LOCK=NONE,
        MODIFY COLUMN instrumental TINYINT(1) NULL DEFAULT NULL, ALGORITHM=INPLACE, LOCK=NONE;
      ")
    end
  end
end
