class DropDelayedJobTables < ActiveRecord::Migration[4.2]
  def change
    if delayed_tables.empty?
      puts "No delayed job tables in the database."
    else
      delayed_tables.each do |table|
        drop_table table.to_sym
        puts "Dropped #{table} from database."
      end
    end
  end

  def delayed_tables
    ActiveRecord::Base.connection.tables.select { |t| t.include?("delayed") }
  end
end
