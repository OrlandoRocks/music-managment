class AddKickbacksFeature < ActiveRecord::Migration[4.2]
  def self.up
    Feature.find_or_create_by(:name=>'kickbacks', :restrict_by_user => 1)
  end

  def self.down
    feature = Feature.find_by(name: 'kickbacks')
    feature.destroy if feature
  end
end
