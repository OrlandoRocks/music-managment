class AddLastAddDateAndLastDeleteDateToCloudSearchBatches < ActiveRecord::Migration[4.2]
  def self.up
    add_column :cloud_search_batches, :last_add_date, :datetime
    add_column :cloud_search_batches, :last_delete_date, :datetime
  end

  def self.down
    remove_column :cloud_search_batches, :last_add_date
    remove_column :cloud_search_batches, :last_delete_date
  end
end
