class RemoveIneligibilityCodeFromTrackMonetizations < ActiveRecord::Migration[4.2]
  def up
    remove_column :track_monetizations, :ineligibility_code
  end

  def down
    add_column :track_monetizations, :ineligibility_code, :string
  end
end
