class AddProviderToLegalDocuments < ActiveRecord::Migration[4.2]
  def change
    add_column :legal_documents, :provider, :string
  end
end
