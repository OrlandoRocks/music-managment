class AddPerfColumnsToMumaSongIps < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE muma_song_ips ADD COLUMN perf_owned_share decimal(5,2) DEFAULT NULL, 
      ADD COLUMN perf_collect_share decimal(5,2) DEFAULT NULL")
  end

  def self.down
    execute("ALTER TABLE muma_song_ips DROP COLUMN perf_owned_share, DROP COLUMN perf_collect_share")
  end
end
