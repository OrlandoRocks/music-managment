class RenameSpatialAudiosToImmersiveAudios < ActiveRecord::Migration[6.0]
  def self.up
    if table_exists? :spatial_audios
      safety_assured do
        rename_table :spatial_audios, :immersive_audios
        add_column :immersive_audios, :audio_type, "enum('dolby_atmos')", null: false
      end
    end
  end

  def self.down
    if table_exists? :immersive_audios
      safety_assured do
        rename_table :immersive_audios, :spatial_audios
        remove_column :spatial_audios, :audio_type
      end
    end
  end
end
