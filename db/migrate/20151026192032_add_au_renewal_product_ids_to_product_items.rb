class AddAuRenewalProductIdsToProductItems < ActiveRecord::Migration[4.2]
  def up
    process_renewal_products(get_pricing_map)
  end

  def down
    process_renewal_products(get_pricing_map.invert)
  end

  def get_pricing_map
    price_map = {}
    price_map[1] = 198
    price_map[3] = 199
    price_map[101] = 216
    price_map[121] = 226
    price_map[123] = 227
    price_map
  end

  def process_renewal_products(price_map)
    renewal_products = Product.where("country_website_id = 4 AND renewal_level = 'Item' AND name <> 'Added Store'")
    renewal_products.each do |rp|
      rp.product_items.each do |pi|
        if pi.update(renewal_product_id: price_map[pi.renewal_product_id])
          p "Successfully update renewal for AU product #{rp.id}"
        else
          p "Unable to update renewal for AU product #{rp.id}"
        end
      end
    end
  end
end

