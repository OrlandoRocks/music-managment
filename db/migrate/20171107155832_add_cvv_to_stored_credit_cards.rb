class AddCvvToStoredCreditCards < ActiveRecord::Migration[4.2]
  def change
    add_column :stored_credit_cards, :cvv, :boolean
  end
end
