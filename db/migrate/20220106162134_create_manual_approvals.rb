class CreateManualApprovals < ActiveRecord::Migration[6.0]
  def change
    create_table :manual_approvals do |t|
      t.references :approveable, polymorphic: true
      t.integer :created_by_id, null: false

      t.timestamps
    end
  end
end
