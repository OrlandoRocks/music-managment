class ChangePayoutNamespacing < ActiveRecord::Migration[4.2]
  def change
    rename_table :payout_transactions, :payout_transfers

    rename_index :payout_transfers, 'index_payout_transactions_on_payout_provider_id', 'index_payout_transfers_on_payout_provider_id'
    rename_index :payout_transfers, 'index_payout_transactions_on_payout_withdrawal_type_id', 'index_payout_transfers_on_payout_withdrawal_type_id'
    rename_index :payout_transfers, 'index_payout_transactions_on_tunecore_processor_id', 'index_payout_transfers_on_tunecore_processor_id'
    rename_index :payout_transfers, 'index_payout_transactions_on_client_reference_id', 'index_payout_transfers_on_client_reference_id'
  end
end
