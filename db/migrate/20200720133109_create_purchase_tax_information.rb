class CreatePurchaseTaxInformation < ActiveRecord::Migration[6.0]
  def change
    create_table :purchase_tax_information do |t|
      t.integer :purchase_id, null: false
      t.string :customer_location
      t.string :vat_registration_number
      t.string :tax_type
      t.float :tax_rate
      t.string :trader_name
      t.string :place_of_supply
      t.string :error_code

      t.timestamps
    end

    add_index :purchase_tax_information, [:purchase_id]
  end
end
