class AddPreorderPurchaseToPurchaseRelatedEnum < ActiveRecord::Migration[4.2]
  def up
    execute("ALTER TABLE purchases CHANGE related_type related_type enum('Album','Single','Ringtone','Entitlement','Product','Renewal','ItunesUserReport','MusicVideo','FeatureFilm','Salepoint','Song','Widget','Composer','Video','SalepointSubscription','CreditUsage','Booklet','SalepointPreorderData','SoundoutReport','FacebookRecognitionServicePurchase','YoutubeMonetization','PreorderPurchase');")
  end
  
  def down
    execute("ALTER TABLE purchases CHANGE related_type related_type enum('Album','Single','Ringtone','Entitlement','Product','Renewal','ItunesUserReport','MusicVideo','FeatureFilm','Salepoint','Song','Widget','Composer','Video','SalepointSubscription','CreditUsage','Booklet','SalepointPreorderData','SoundoutReport','FacebookRecognitionServicePurchase','YoutubeMonetization');")
  end
end
