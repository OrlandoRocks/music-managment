class UpdateStateEnumForPostingRoyalties < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      execute <<-SQL
        ALTER TABLE posting_royalties MODIFY COLUMN state ENUM('pending', 'approved', 'submitted') DEFAULT 'pending'
      SQL
    end
  end

  def down
    safety_assured do
      execute <<-SQL
        ALTER TABLE posting_royalties MODIFY COLUMN state ENUM('pending', 'approved') DEFAULT 'pending'
      SQL
    end
  end
end
