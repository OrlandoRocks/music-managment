class CreateDeletedAccounts < ActiveRecord::Migration[4.2]
  def change
    create_table :deleted_accounts do |t|
      t.integer :person_id
      t.string :delete_type

      t.timestamps
    end

    add_index :deleted_accounts, :person_id, unique: true
  end
end
