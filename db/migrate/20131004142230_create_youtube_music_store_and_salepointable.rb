class CreateYoutubeMusicStoreAndSalepointable < ActiveRecord::Migration[4.2]
  def self.up
    store = Store.create(:name=>'YouTube Music', :abbrev=>'ytm', :short_name=>'YTMusic', :position=>460, :needs_rights_assignment=>false, :is_active=>false, :base_price_policy_id=>"3", :is_free => false)

    if store
      execute ("insert into salepointable_stores (salepointable_type,store_id) values ('Album','#{store.id}')")
      execute ("insert into salepointable_stores (salepointable_type,store_id) values ('Single','#{store.id}')")
    else
      puts "Store creation for YouTube Music failed"
    end
  end

  def self.down
    if store = Store.find_by(short_name: "YTMusic")
      execute("delete from salepointable_stores where store_id = #{store.id}")
      execute("DELETE FROM stores where id = #{store.id}")
    else
      puts "Couldn't find store by the short name of YTMusic"
    end
  end

end
