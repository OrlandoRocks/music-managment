class AddShouldUnfinalizeToReviewReasons < ActiveRecord::Migration[4.2]
  def up
    add_column :review_reasons, :should_unfinalize, :boolean, null: false, default: false

    unfinalizable_reason_ids = [5, 9, 13, 16, 17] + (21..27).to_a + [30] + (34..38).to_a + (40..46).to_a
    unfinalizable_reason_ids.each { |id| ReviewReason.find(id).update(should_unfinalize: true) }
  end

  def down
    remove_column :review_reasons, :should_unfinalize
  end
end
