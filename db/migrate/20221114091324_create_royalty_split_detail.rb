class CreateRoyaltySplitDetail < ActiveRecord::Migration[6.0]
  def change
    create_table :royalty_split_details do |t|
      t.belongs_to :person, null: false
      t.belongs_to :royalty_split_intake, null: false
      t.belongs_to :royalty_split, null: false
      t.belongs_to :song, null: false
      t.belongs_to :person_intake
      t.references :owner, comment: "Main Account Holder person_id for split, the only admin",
                           foreign_key: { to_table: :people },
                           type: :integer,
                           unsigned: true,
                           null: false
      t.decimal :amount, precision: 23, scale: 14
      t.string  :currency, limit: 3, null: false, index: true
      t.text :royalty_split_config
      t.timestamps
    end
  end
end
