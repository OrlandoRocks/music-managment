class CreateIneligibilityRules < ActiveRecord::Migration[4.2]
  def change
    create_table :ineligibility_rules do |t|
      t.string :property
      t.string :operator
      t.string :value
      t.belongs_to :store

      t.timestamps
    end
    add_index :ineligibility_rules, :store_id
  end
end
