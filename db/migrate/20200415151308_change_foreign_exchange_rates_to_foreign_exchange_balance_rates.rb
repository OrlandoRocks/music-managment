class ChangeForeignExchangeRatesToForeignExchangeBalanceRates < ActiveRecord::Migration[6.0]
  def change
    safety_assured { rename_table :foreign_exchange_rates, :foreign_exchange_balance_rates }
  end
end
