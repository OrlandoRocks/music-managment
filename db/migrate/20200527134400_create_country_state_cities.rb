class CreateCountryStateCities < ActiveRecord::Migration[6.0]
  def change
    create_table :country_state_cities, options: 'ENGINE=InnoDB DEFAULT CHARSET=utf8' do |t|
      t.string :name
      t.text :alt_name
      t.belongs_to :state
      t.timestamps
    end
  end
end
