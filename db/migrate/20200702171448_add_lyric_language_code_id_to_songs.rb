class AddLyricLanguageCodeIdToSongs < ActiveRecord::Migration[6.0]
  def change
    add_column :songs, :lyric_language_code_id, :integer
  end
end
