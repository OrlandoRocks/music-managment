class AddDeliveryServiceToStores < ActiveRecord::Migration[4.2]
  def change
    add_column :stores, :delivery_service, :string

    Store.update_all ["delivery_service = ?", "petri"]
  end
end
