class CreateArtistStoreWhitelists < ActiveRecord::Migration[6.0]
  def change
    create_table :artist_store_whitelists do |t|
      t.integer :artist_id, null: false
      t.integer :store_id, null: false

      t.timestamps
    end

    add_index :artist_store_whitelists, [:artist_id, :store_id], unique: true
  end
end
