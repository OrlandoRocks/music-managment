class CreateSevenDigitalStore < ActiveRecord::Migration[4.2]
  @store_name = '7Digital'

  def self.up
    store = Store.create(:name => @store_name, :abbrev => 'sd', :short_name => @store_name, :position => 452, :needs_rights_assignment => true, :is_active => false, :base_price_policy_id => "3", :is_free => false)

    if store
      execute ("insert into salepointable_stores (salepointable_type,store_id) values ('Album','#{store.id}')")
      execute ("insert into salepointable_stores (salepointable_type,store_id) values ('Single','#{store.id}')")
    else
      puts "Store creation for #{@store_name} failed"
    end
  end

  def self.down
    if store = Store.find_by(name: @store_name)
      execute("delete from salepointable_stores where store_id = #{store.id}")
      Store.destroy(store.id)
    else
      puts "Couldn't find store by the short name of #{@store_name}"
    end
  end
end
