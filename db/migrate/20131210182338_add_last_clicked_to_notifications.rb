class AddLastClickedToNotifications < ActiveRecord::Migration[4.2]
  def change
    add_column :notifications, :last_clicked, :datetime
  end
end
