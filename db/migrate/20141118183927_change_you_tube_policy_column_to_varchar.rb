class ChangeYouTubePolicyColumnToVarchar < ActiveRecord::Migration[4.2]
  def up
    sql = %Q{
    ALTER TABLE you_tube_royalty_records
    MODIFY COLUMN you_tube_policy_type ENUM('monetize','track', 'block', 'takedown') not null default 'track';
    }
    ActiveRecord::Base.connection.execute(sql)
  end

  def down
  end
end
