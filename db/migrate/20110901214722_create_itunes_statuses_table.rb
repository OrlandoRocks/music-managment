class CreateItunesStatusesTable < ActiveRecord::Migration[4.2]
  def self.up
    create_table :itunes_statuses do |t|      
      t.string :upc
      t.string :status
      t.datetime :upload_created
      t.string :upload_state
      t.integer :upload_state_id
      t.text :message
      t.timestamps
    end
  end

  def self.down
    drop_table :itunes_statuses
  end
end
