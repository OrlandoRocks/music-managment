class CreateReferralData < ActiveRecord::Migration[4.2]
  def change
    create_table :referral_data do |t|
      t.references :person
      t.string :key
      t.string :value
      t.string :page
      t.string :source
      t.datetime :timestamp

      t.timestamps
    end
    add_index :referral_data, :person_id
    add_index :referral_data, :key
    add_index :referral_data, :value
    add_index :referral_data, :page
    add_index :referral_data, :source
  end
end
