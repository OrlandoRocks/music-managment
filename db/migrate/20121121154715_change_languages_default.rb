class ChangeLanguagesDefault < ActiveRecord::Migration[4.2]
  def self.up
    change_column :albums, :language_code, :string, :default => "en"
  end

  def self.down
    change_column :albums, :language_code, :string, :default => "en-US"
  end
end
