class CreateTableCreativeSongRoles < ActiveRecord::Migration[4.2]
  def change
    create_table :creative_song_roles do |t|
      t.integer :song_role_id
      t.integer :creative_id
    end
  end
end
