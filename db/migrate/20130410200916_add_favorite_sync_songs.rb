class AddFavoriteSyncSongs < ActiveRecord::Migration[4.2]

  def self.up
    create_table :sync_favorite_songs do |t|
      t.integer :song_id
      t.integer :person_id
    end

    add_index :sync_favorite_songs, :song_id
    add_index :sync_favorite_songs, :person_id
  end

  def self.down
    drop_table :sync_favorite_songs
  end

end
