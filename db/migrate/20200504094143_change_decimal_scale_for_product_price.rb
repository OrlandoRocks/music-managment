class ChangeDecimalScaleForProductPrice < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      change_column :products, :price, :decimal, precision: 10, scale: 4, default: 0.0000
      change_column :product_items, :price, :decimal, precision: 10, scale: 4, default: 0.0000
      change_column :product_item_rules, :price, :decimal, precision: 10, scale: 4, default: 0.0000
    end
  end
end
