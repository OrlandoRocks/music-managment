class AddPayinProviderConfigToBraintreeTransactions < ActiveRecord::Migration[6.0]
  def change
    add_reference :braintree_transactions, :payin_provider_config, index: true
  end
end
