class CreatePublishingCompositionSplits < ActiveRecord::Migration[6.0]
  def up
    create_table :publishing_composition_splits do |t|
      t.references :publishing_composer
      t.references :publishing_composition
      t.boolean :right_to_collect,  default: 1
      t.decimal :percent, precision: 5, scale: 2
      t.string :updated_by, limit: 50

      t.timestamps
    end
  end

  def down
    drop_table :publishing_composition_splits
  end
end
