class ModifyFriendReferralTable < ActiveRecord::Migration[4.2]
  def self.up
    add_column :friend_referrals, :first_referral, :boolean, :default => 0
    change_column :friend_referrals, :commission, :decimal, :precision => 10, :scale => 2, :default => 0
    change_column :friend_referrals, :balance, :decimal, :precision => 10, :scale => 2, :default => 0
  end

  def self.down
    remove_column :friend_referrals, :first_referral
    change_column :friend_referrals, :commission, :decimal, :default => nil
    change_column :friend_referrals, :balance, :decimal, :default => nil
  end
end
