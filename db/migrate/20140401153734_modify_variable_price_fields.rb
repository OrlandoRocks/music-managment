class ModifyVariablePriceFields < ActiveRecord::Migration[4.2]
  def change
    add_column :variable_prices, :user_editable, :boolean, :default => false
  end
end
