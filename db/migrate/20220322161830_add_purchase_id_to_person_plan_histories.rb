class AddPurchaseIdToPersonPlanHistories < ActiveRecord::Migration[6.0]
  def change
    safety_assured { add_reference :person_plan_histories, :purchase, index: true }
  end
end
