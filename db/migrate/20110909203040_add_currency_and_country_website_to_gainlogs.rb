class AddCurrencyAndCountryWebsiteToGainlogs < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE gainlogs ADD currency CHAR(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency for invoice item' AFTER last_discount_amount_cents");
    execute("ALTER TABLE gainlogs ADD country_website_id int(11) unsigned DEFAULT 1 COMMENT 'Foreign key to country_websites' AFTER id");
  end

  def self.down
    remove_column :gainlogs, :currency
    remove_column :gainlogs, :country_website_id
  end
end
