class CreateTransferMetadata < ActiveRecord::Migration[6.0]
  def change
    create_table :transfer_metadata do |t|
      t.references :trackable, polymorphic: true, null: false
      t.boolean :auto_approved, null: false, default: false
      t.datetime :auto_approved_at
      t.references :login_event, null: false

      t.timestamps
    end
  end
end
