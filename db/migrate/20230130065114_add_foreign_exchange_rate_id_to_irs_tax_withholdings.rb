class AddForeignExchangeRateIdToIRSTaxWithholdings < ActiveRecord::Migration[6.0]
  def change
    add_column :irs_tax_withholdings, :foreign_exchange_rate_id, :bigint,
               default: nil
    add_foreign_key :irs_tax_withholdings,
                    :foreign_exchange_rates,
                    column: :foreign_exchange_rate_id
  end
end
