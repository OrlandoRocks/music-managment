class AddUpdatedAtCreatedAttoPaymentChannelReceipts < ActiveRecord::Migration[4.2]
  def change
    change_table :payment_channel_receipts do |t|
      t.timestamps
    end
  end
end
