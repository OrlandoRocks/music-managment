class AddPublisherManagerRole < ActiveRecord::Migration[4.2]
  def self.up
    Role.create(:name=>"Publishing Manager", :long_name => "Publishing Manager",:is_administrative => false, :description => "Allows publishing staff to view/modify publishng records")
  end

  def self.down
    Role.find_by(name: "Publishing Manager").delete
  end
end
