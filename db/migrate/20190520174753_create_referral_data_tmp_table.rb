class CreateReferralDataTmpTable < ActiveRecord::Migration[4.2]
  def change
    create_table :referral_data_tmp do |t|
      t.references :person, null: false
      t.string :referral
      t.string :referral_type, limit: 10
      t.string :referral_campaign
      t.timestamps
    end

    add_index :referral_data_tmp, :person_id, unique: true
  end
end
