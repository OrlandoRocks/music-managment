class AddProgramIdToTaxForms < ActiveRecord::Migration[6.0]
  def change
    add_column :tax_forms, :program_id, :string
  end
end
