class CreatePlansPlanFeatures < ActiveRecord::Migration[6.0]
  def change
    create_table :plans_plan_features do |t|
      t.references :plan, null: false
      t.references :plan_feature, null: false
      t.index [:plan_id, :plan_feature_id], unique: true

      t.timestamps
    end
  end
end
