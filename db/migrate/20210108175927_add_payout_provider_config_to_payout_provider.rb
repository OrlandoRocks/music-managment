class AddPayoutProviderConfigToPayoutProvider < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      add_reference :payout_providers,
                    :payout_provider_config,
                    index: true,
                    default: PayoutProviderConfig.by_env.find_by_program_id(PayoutProviderConfig::TC_US_PROGRAM_ID).id
    end
  end
end
