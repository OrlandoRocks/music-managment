class AddNoteToPurchaseTaxInformation < ActiveRecord::Migration[6.0]
  def change
    add_column :purchase_tax_information, :note, :string
  end
end
