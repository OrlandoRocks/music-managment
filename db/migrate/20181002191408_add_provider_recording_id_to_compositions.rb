class AddProviderRecordingIdToCompositions < ActiveRecord::Migration[4.2]
  def change
    add_column :compositions, :provider_recording_id, :string
    
    add_index :compositions, :provider_recording_id
  end
end
