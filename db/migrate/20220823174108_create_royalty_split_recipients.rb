class CreateRoyaltySplitRecipients < ActiveRecord::Migration[6.0]
  def change
    create_table :royalty_split_recipients do |t|
      t.references :royalty_split, foreign_key: true, null: false, comment: "Royalty split config"
      t.references :person,
                   foreign_key: { to_table: :people },
                   type: :integer,
                   unsigned: true,
                   default: nil,
                   comment: "Person that gets a split of royalties"

      t.string :email, default: nil,
                       comment: "Email addresses are only for split recipients not registered at tunecore yet, used instead of person_id"
      t.decimal :percent, scale: 2, precision: 6, null: false,
                          comment: "Percent of royalties recieved by recipient"

      t.index [:person_id, :royalty_split_id], unique: true
      t.index [:email, :royalty_split_id], unique: true

      t.timestamps
    end
  end
end
