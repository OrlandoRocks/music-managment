class CreateYouTubeRoyaltyIntakes < ActiveRecord::Migration[4.2]
  def up
    sql = %Q{
      CREATE TABLE `you_tube_royalty_intakes` (
        `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `person_id` int(10) unsigned NOT NULL,
        `amount` decimal(15,6) NOT NULL DEFAULT '0.000000',
        `currency` char(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency',
        `created_at` timestamp NULL DEFAULT NULL,
        PRIMARY KEY (`id`),
        CONSTRAINT FOREIGN KEY (`person_id`) REFERENCES `people` (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    }
    ActiveRecord::Base.connection.execute(sql)

    sql = %Q{
      ALTER TABLE you_tube_royalty_records
      DROP FOREIGN KEY you_tube_royalty_records_ibfk_1,
      DROP KEY person_id_2,
      DROP COLUMN person_intake_id,
      ADD COLUMN you_tube_royalty_intake_id int(10) unsigned DEFAULT NULL,
      ADD CONSTRAINT FOREIGN KEY (you_tube_royalty_intake_id) references you_tube_royalty_intakes (id),
      ADD KEY (person_id, you_tube_royalty_intake_id)
    }
    ActiveRecord::Base.connection.execute(sql)

    sql = %Q{
      ALTER TABLE person_transactions
      MODIFY COLUMN target_type enum('BalanceAdjustment','BatchTransaction','CheckTransfer','EftBatchTransaction','Invoice','PaypalIpn','PaypalTransfer','Person','PersonIntake','FriendReferral', 'YouTubeRoyaltyIntake') DEFAULT NULL;
    }
    ActiveRecord::Base.connection.execute(sql)
  end

  def down
    ActiveRecord::Base.connection.execute("drop table you_tube_royalty_intakes")
  end

end
