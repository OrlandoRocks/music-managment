  class CreateProductsChargebeeItems < ActiveRecord::Migration[6.0]
  def change
    create_table :products_chargebee_items do |t|
      t.references :product, null: false, index: true
      t.string :chargebee_item_id, null: false, index: true
      t.integer :quantity, null: false, default: 1
      t.string :interval_type
      t.integer :interval

      t.timestamps
    end
  end
end
