class CreatePersonPointsTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :person_points_transactions do |t|
      t.references :person, foreign_key: true, null: false, type: 'int(11) unsigned'
      t.integer :credit_points, null: false, default: 0
      t.integer :debit_points, null: false, default: 0
      t.references :target, polymorphic: true, index: true, null: false

      t.timestamps
    end
  end
end
