class CreateTunecoreTracking < ActiveRecord::Migration[4.2]
  def up
    create_table :tunecore_tracking do |t|
      t.integer :person_id, null: false
      t.string  :model, null: false
      t.integer :model_id, null: false
      t.integer :service, null: false
      t.boolean :active, default: true, null: false
      t.timestamps
    end
  end

  def down
    drop_table :tunecore_tracking
  end
end
