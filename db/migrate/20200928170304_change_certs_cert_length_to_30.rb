class ChangeCertsCertLengthTo30 < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      change_column :certs, :cert, :string, :limit => 30
    end
  end
  
  def down
    safety_assured do
      change_column :certs, :cert, :string, :limit => 16
    end
  end
end
