class AddAdyenStoredPaymentMethodToBatchTransactions < ActiveRecord::Migration[6.0]
  def up
    add_reference :batch_transactions, :adyen_stored_payment_method, after: "stored_paypal_account_id", comment: "Foreign Key to adyen_stored_payment_methods table"
  end

  def down
    remove_reference :batch_transactions, :adyen_stored_payment_method
  end
end
