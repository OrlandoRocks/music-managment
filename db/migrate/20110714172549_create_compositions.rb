class CreateCompositions < ActiveRecord::Migration[4.2]
  def self.up
    create_table :compositions do |t|
      t.string :name
      t.string :iswc

      t.timestamps
    end
  end

  def self.down
    drop_table :compositions
  end
end
