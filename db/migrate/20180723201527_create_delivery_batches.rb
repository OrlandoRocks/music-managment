class CreateDeliveryBatches < ActiveRecord::Migration[4.2]
  def change
    create_table :delivery_batches do |t|
      t.belongs_to :store
      t.string :batch_id
      t.integer :count, default: 0
      t.boolean :active
      t.integer :max_batch_size, default: 100
    end

    add_index :delivery_batches, :store_id
  end
end
