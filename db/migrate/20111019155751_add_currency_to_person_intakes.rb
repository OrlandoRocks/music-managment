class AddCurrencyToPersonIntakes < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE person_intakes
     ADD COLUMN currency CHAR(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency' AFTER amount")
  end

  def self.down
    remove_column :person_intakes, :currency
  end
end
