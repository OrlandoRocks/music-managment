class AddFacebookToBalanceAdjustmentCategories < ActiveRecord::Migration[4.2]
  def up
    execute "ALTER TABLE balance_adjustments MODIFY COLUMN category enum('Refund - Renewal','Refund - Other','Service Adjustment','Songwriter Royalty','Other','YouTube MCN Royalty', 'Facebook') DEFAULT NULL COMMENT 'Category of adjustment'"
  end

  def down
    execute "ALTER TABLE balance_adjustments MODIFY COLUMN category enum('Refund - Renewal','Refund - Other','Service Adjustment','Songwriter Royalty','Other','YouTube MCN Royalty') DEFAULT NULL COMMENT 'Category of adjustment'"
  end
end
