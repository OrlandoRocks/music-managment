class CreateExternalServiceIdArtworks < ActiveRecord::Migration[4.2]
  def change
    create_table :external_service_id_artworks do |t|
      t.belongs_to :external_service_id

      t.timestamps
    end
    add_index :external_service_id_artworks, :external_service_id_id
  end
end
