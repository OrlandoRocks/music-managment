class CreateSimfyAfricaStore < ActiveRecord::Migration[4.2]
  @@store_name = 'SimfyAfrica'

  def up
    store = Store.create(name: @@store_name, abbrev: 'sma', short_name: 'SimfyAF', position: 0, needs_rights_assignment: false,
                         is_active: false, base_price_policy_id: 3, is_free: false, in_use_flag: false)

   if store.errors.empty?
     store.salepointable_stores.create!(salepointable_type: 'Album')
     store.salepointable_stores.create!(salepointable_type: 'Single')
   else
     puts "Store creation for #{@@store_name} failed"
   end
  end

  def down
    if store = Store.find_by(name: @@store_name)
      store.salepointable_stores.collect(&:destroy)
      store.destroy
    else
      puts "Couldn't find store by the short name of #{@@store_name}"
    end
  end
end
