class AddReminderAndExtentionCountToLod < ActiveRecord::Migration[4.2]
  def self.up
    add_column :lods, :reminder_sent_count, :integer, :default => 0
    add_column :lods, :extension_count, :integer, :default => 0
  end

  def self.down
    remove_column :lods, :reminder_sent_count
    remove_column :lods, :extension_count
  end
end
