class CreateIterableCampaigns < ActiveRecord::Migration[6.0]
  def change
    create_table(:iterable_campaigns, id: false) do |t|
      t.bigint :id, null: false, primary_key: true, unsigned: true
      t.integer :campaign_id, null: false, unique: true, unsigned: true
      t.string :name, null: false
      t.string :environment, null: false

      t.timestamps

      t.index [:name, :environment], unique: true
    end
  end
end
