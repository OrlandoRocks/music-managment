class CreatePublishingCompositionMetadata < ActiveRecord::Migration[6.0]
  def change
    create_table :publishing_composition_metadata, primary_key: [:publishing_composition_id, :publishing_cover_song_source_id] do |t|
      t.bigint :publishing_composition_id, foreign_key: true, null: true, index: { name: 'index_on_publishing_compositions_id' }
      t.boolean :cover_song, default: false, null: false
      t.boolean :public_domain, null: false, default: false
      t.bigint :publishing_cover_song_source_id, foreign_key: true, null: false, index: { name: 'index_on_publishing_cover_song_source_id' }
      t.timestamps
    end
  end
end