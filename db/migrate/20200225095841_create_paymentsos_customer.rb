class CreatePaymentsosCustomer < ActiveRecord::Migration[4.2]
  def change
    create_table :payments_os_customers do |t|
      t.string :customer_reference
      t.string :customer_id
      t.belongs_to :person
    end
    add_index :payments_os_customers, [:customer_reference, :customer_id], name: :payos_customer_on_customer_ref_customer_id, unique: true
  end
end
