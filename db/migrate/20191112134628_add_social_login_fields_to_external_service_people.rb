class AddSocialLoginFieldsToExternalServicePeople < ActiveRecord::Migration[4.2]
  def change
  	add_column :external_services_people, :access_token, :string
  	add_column :external_services_people, :access_token_expiry, :datetime
  end
end
