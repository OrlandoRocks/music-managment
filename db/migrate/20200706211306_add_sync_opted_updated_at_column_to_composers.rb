class AddSyncOptedUpdatedAtColumnToComposers < ActiveRecord::Migration[6.0]
  def change
    safety_assured { add_column :composers, :sync_opted_updated_at, :datetime }
  end
end
