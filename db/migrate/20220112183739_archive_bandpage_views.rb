class ArchiveBandpageViews < ActiveRecord::Migration[6.0]
  def self.up
    if table_exists? :bandpage_views
      safety_assured do
        rename_table :bandpage_views, :archive_bandpage_views
      end
    end
  end

  def self.down
    if table_exists? :archive_bandpage_views
      safety_assured do
        rename_table :archive_bandpage_views, :bandpage_views
      end
    end
  end
end
