class UpdateYoutubeSipStoreName < ActiveRecord::Migration[4.2]
  def up
    s = SipStore.find_by(name: "Youtube Subcription Store")
    s.update(name: "YouTube Music Key")
  end

  def down
    s = SipStore.find_by(name: "YouTube Music Key")
    s.update(name: "Youtube Subcription Store")
  end
end
