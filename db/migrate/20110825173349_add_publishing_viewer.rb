#AK
class AddPublishingViewer < ActiveRecord::Migration[4.2]
  def self.up
    if ! Role.find_by(name: "Publishing Viewer")
      Role.create(:name => "Publishing Viewer",
                  :long_name => "Publishing Viewer",
                  :description => "Similar to Publishing Manager, but cannot view sensitive information such as SS#.",
                  :is_administrative => false)
    end
  end

  def self.down
  end
end
