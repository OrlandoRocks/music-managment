class RenameDropkloudProducts < ActiveRecord::Migration[6.0]
  def self.up
    if table_exists? :dropkloud_products
      safety_assured do
        rename_table :dropkloud_products, :archive_dropkloud_products
      end
    end
  end

  def self.down
    if table_exists? :archive_dropkloud_products
      safety_assured do
        rename_table :archive_dropkloud_products, :dropkloud_products
      end
    end
  end
end
