class AddBatchSizeToStoreDeliveryConfigs < ActiveRecord::Migration[4.2]
  def change
    add_column :store_delivery_configs, :batch_size, :integer, default: 100
  end
end
