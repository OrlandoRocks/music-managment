class AddCurrencyToBaseItemOptions < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE base_item_options ADD currency CHAR(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency for base item option' AFTER price")
  end

  def self.down
    remove_column :base_item_options, :currency
  end
end
