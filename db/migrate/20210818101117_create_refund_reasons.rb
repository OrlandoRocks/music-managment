class CreateRefundReasons < ActiveRecord::Migration[6.0]
  def change
    create_table :refund_reasons do |t|
      t.string :reason, null: false
      t.boolean :visible, default: true
      t.timestamps
    end
  end
end
