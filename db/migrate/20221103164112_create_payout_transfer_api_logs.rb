class CreatePayoutTransferApiLogs < ActiveRecord::Migration[6.0]
  def change
    create_table :payout_transfer_api_logs do |t|
      t.belongs_to :payout_transfer
      t.text :body
      t.string :kind
      t.string :url
      t.timestamps
    end
  end
end
