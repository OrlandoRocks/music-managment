class CreateBatchBalanceAdjustmentErrors < ActiveRecord::Migration[4.2]
  def self.up
    create_table :batch_balance_adjustment_errors do |t|
      t.integer :batch_balance_adjustment_id
      t.integer :batch_balance_adjustment_detail_id
      t.text :raw_transaction
      t.text :notes
    end
  end

  def self.down
    drop_table :batch_balance_adjustment_errors
  end
end
