class UpdateCompositionStatuses < ActiveRecord::Migration[4.2]
  def self.up
    Composition.send(:batch_update_submitted)
  end

  def self.down
  end
end
