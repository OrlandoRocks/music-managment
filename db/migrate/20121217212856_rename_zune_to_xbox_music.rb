class RenameZuneToXboxMusic < ActiveRecord::Migration[4.2]
  def self.up
    Store.transaction do
      zune=Store.find(24)
      zune.name = "Xbox Music"
      zune.save!
    end
  rescue StandardError => e
  end

  def self.down
    Store.transaction do
      zune=Store.find(24)
      zune.name = "Zune"
      zune.save!
    end
  rescue StandardError => e
  end
end
