class CreateRenewalProductChanges < ActiveRecord::Migration[4.2]
  def self.up
     up_sql = %Q(CREATE TABLE `renewal_product_changes` (
       `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
       `renewal_id` int(10) unsigned NOT NULL,
       `old_item_to_renew_id` int(10) unsigned NOT NULL,
       `old_item_to_renew_type` enum('Product','ProductItem') NOT NULL,
       `old_duration` tinyint(4) DEFAULT NULL COMMENT 'deteremines (with renewal_interval) how often an entitlement will renew',
       `old_interval` enum('month','day','week','year') DEFAULT NULL COMMENT 'old renewal interval',
       `new_item_to_renew_id` int(10) unsigned NOT NULL,
       `new_item_to_renew_type` enum('Product','ProductItem') NOT NULL,
       `new_duration` tinyint(4) DEFAULT NULL COMMENT 'deteremines (with renewal_interval) how often an entitlement will renew',
       `new_interval` enum('month','day','week','year') DEFAULT NULL COMMENT 'old renewal interval',
       `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime of creation.',
       PRIMARY KEY (`id`),
       KEY `renewal_product_changes_renewal_id` (`renewal_id`),
       KEY `renewal_product_changes_old_item_to_renew` (`old_item_to_renew_id`,`old_item_to_renew_type`),
       KEY `renewal_product_changes_new_item_to_renew` (`new_item_to_renew_id`,`new_item_to_renew_type`),
       CONSTRAINT `FK_rpc_renewal_id` FOREIGN KEY (`renewal_id`) REFERENCES `renewals`(`id`)
     ) ENGINE=InnoDB DEFAULT CHARSET=utf8;)
     execute up_sql
   end

   def self.down
     drop_table :renewal_product_changes
   end
end
