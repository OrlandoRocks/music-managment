class AddOptionNumberToSyncLicenseOption < ActiveRecord::Migration[4.2]
  def self.up
    add_column :sync_license_options, :number, :integer
  end

  def self.down
    remove_column :sync_license_options, :number
  end
end
