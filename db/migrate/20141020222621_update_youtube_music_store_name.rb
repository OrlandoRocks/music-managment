class UpdateYoutubeMusicStoreName < ActiveRecord::Migration[4.2]
  def up
    store = Store.find_by(abbrev: "ytm")
    store.update_attribute(:name, "YouTube Art Tracks")
  end

  def down
    store = Store.find_by(abbrev: "ytm")
    store.update_attribute(:name, "YouTube Music")
  end
end
