class CreateMassIngestionStatuses < ActiveRecord::Migration[4.2]
  def change
    create_table :mass_ingestion_statuses do |t|
      t.string :identifier
      t.string :identifier_type
      t.string :status
      t.text :info

      t.timestamps
    end

    add_index :mass_ingestion_statuses, :status
  end
end
