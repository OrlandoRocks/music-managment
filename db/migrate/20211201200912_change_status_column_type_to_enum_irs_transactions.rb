class ChangeStatusColumnTypeToEnumIRSTransactions < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      execute <<-SQL
          ALTER TABLE irs_transactions MODIFY status enum('pending', 'reported', 'completed') default 'pending';
      SQL
    end
  end

  def down
    safety_assured do
      change_column :irs_transactions, :status, :string # Previous type
    end
  end
end
