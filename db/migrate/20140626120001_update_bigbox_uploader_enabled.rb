class UpdateBigboxUploaderEnabled < ActiveRecord::Migration[4.2]
  def up
    rename_column :people, :bigbox_uploader_enabled, :migrated_to_bigbox
    
    sql = <<-SQL
    UPDATE people
    SET migrated_to_bigbox = 1
    SQL
    ActiveRecord::Base.connection.execute(sql)

    sql = <<-SQL
      UPDATE people
      INNER JOIN albums a on a.person_id = people.id
      INNER JOIN songs s on s.album_id = a.id
      SET migrated_to_bigbox = 0
      WHERE s.s3_asset_id IS NULL and s.s3_orig_asset_id IS NOT NULL
    SQL
    ActiveRecord::Base.connection.execute(sql)
  end

  def down
    rename column :people, :migrated_to_bigbox, :bigbox_uploader_enabled
  end
end
