class AddBalanceAdjustmentIdToBatchBalanceAdjustmentDetails < ActiveRecord::Migration[4.2]
  def self.up
    add_column :batch_balance_adjustment_details, :balance_adjustment_id, :integer
  end

  def self.down
    remove_column :batch_balance_adjustment_details, :balance_adjustment_id
  end
end
