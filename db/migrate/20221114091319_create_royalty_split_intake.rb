class CreateRoyaltySplitIntake < ActiveRecord::Migration[6.0]
  def change
    create_table :royalty_split_intakes do |t|
      t.belongs_to :person, null: false
      t.decimal :amount, precision: 23, scale: 14
      t.string  :currency, limit: 3, null: false, index: true
      t.timestamps
    end
  end
end
