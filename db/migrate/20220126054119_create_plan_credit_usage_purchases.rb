class CreatePlanCreditUsagePurchases < ActiveRecord::Migration[6.0]
  def change
    create_table :plan_credit_usage_purchases do |t|
      t.references :plan, null: false
      t.references :purchase, null: false
      t.references :credit_usage, null: false

      t.timestamps
    end
  end
end
