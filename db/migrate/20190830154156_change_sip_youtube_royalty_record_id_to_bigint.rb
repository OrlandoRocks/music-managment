class ChangeSipYoutubeRoyaltyRecordIdToBigint < ActiveRecord::Migration[4.2]
  def change
    change_column :you_tube_royalty_records, :sip_you_tube_royalty_record_id, :bigint
  end
end
