class RenameGainlogs < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      rename_table :gainlogs, :archive_gainlogs
    end
  end
end
