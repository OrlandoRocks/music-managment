class AddAutomatorDelayedJobsTable < ActiveRecord::Migration[4.2]
  def up
    execute "CREATE TABLE `automator_delayed_jobs` ( `id` int(11) NOT NULL AUTO_INCREMENT, `priority` int(11) DEFAULT '0', `attempts` int(11) DEFAULT '0', `handler` mediumtext, `last_error` text, `run_at` datetime DEFAULT NULL, `locked_at` datetime DEFAULT NULL, `failed_at` datetime DEFAULT NULL, `locked_by` varchar(255) DEFAULT NULL, `created_at` datetime DEFAULT NULL, `updated_at` datetime DEFAULT NULL, `queue` varchar(255) DEFAULT NULL, PRIMARY KEY (`id`), KEY `automator_delayed_jobs_priority` (`priority`,`run_at`) );"
  end

  def down
    execute "DROP TABLE `automator_delayed_jobs`;"
  end
end
