class AddCurrencyToBalanceAdjustments < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE balance_adjustments ADD currency CHAR(3) NOT NULL DEFAULT 'USD'  COMMENT 'Currency for debit or credit amount' AFTER credit_amount")
  end

  def self.down
    remove_column :balance_adjustments, :currency
  end
end
