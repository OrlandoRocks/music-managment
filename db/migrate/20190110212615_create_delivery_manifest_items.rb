class CreateDeliveryManifestItems < ActiveRecord::Migration[4.2]
  def change
    create_table :delivery_manifest_items do |t|
      t.belongs_to :delivery_batch_item
      t.string :xml_remote_path
      t.string :uuid
      t.string :upc
      t.string :xml_hashsum
      t.boolean :takedown
    end
  end
end
