class CreateComposers < ActiveRecord::Migration[4.2]
  def self.up
    create_table :composers do |t|
      t.integer :person_id, :null => false
      t.string :first_name, :null => false, :limit => 45
      t.string :last_name, :null => false, :limit => 45
      t.string :address_1, :limit => 45
      t.string :address_2, :limit => 45
      t.string :city, :limit => 45
      t.string :state, :limit => 2
      t.string :zip, :limit => 10
      t.string :tax_id, :limit => 9
      t.string :pro_affiliation, :limit => 20
      t.string :cae, :limit => 11
      t.string :email, :limit => 100
      t.string :phone, :limit => 20
      t.datetime :agreed_to_terms_at, :default => nil

      t.timestamps
    end
  end

  def self.down
    drop_table :composers
  end
end
