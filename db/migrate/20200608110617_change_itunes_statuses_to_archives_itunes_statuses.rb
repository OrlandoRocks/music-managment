class ChangeItunesStatusesToArchivesItunesStatuses < ActiveRecord::Migration[6.0]
  def change
  	safety_assured do
  	  rename_table :itunes_statuses, :archives_itunes_statuses
  	end
  end
end
