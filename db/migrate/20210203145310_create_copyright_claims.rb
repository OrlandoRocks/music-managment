class CreateCopyrightClaims < ActiveRecord::Migration[6.0]
  def change
    create_table :copyright_claims do |t|
      t.references  :asset, polymorphic: true, index: true, null: false
      t.references  :person, null: false, index: true, type: :integer
      t.integer :internal_claim_id, index: true
      t.references  :store, null: false, type: :integer
      t.integer :admin_id, null: false, index: true
      t.string  :fraud_type
      t.float :hold_amount, null: false
      t.boolean :is_active, default: true
      t.timestamps
    end
  end
end
