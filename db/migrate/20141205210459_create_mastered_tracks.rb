class CreateMasteredTracks < ActiveRecord::Migration[4.2]
  def up
    create_table :mastered_tracks do |t|
      t.integer :person_id
      t.string :name, null: :false
      t.string :download_link
      t.string :claim_id
      t.string :landr_track_id
      t.datetime :paid_at
      t.timestamps
    end

    add_index :mastered_tracks, :person_id
  end

  def down
    drop_table :mastered_tracks
  end
end
