class ExpandLengthOfPublishingComposerNames < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      change_column :publishing_composers, :first_name, :string, null: false, limit: 255
      change_column :publishing_composers, :last_name, :string, null: false, limit: 255
    end
  end

  def down
    safety_assured do
      change_column :publishing_composers, :first_name, :string, null: false, limit: 45
      change_column :publishing_composers, :last_name, :string, null: false, limit: 45
    end
  end
end
