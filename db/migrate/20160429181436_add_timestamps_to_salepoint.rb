class AddTimestampsToSalepoint < ActiveRecord::Migration[4.2]
  def change
    add_column(:salepoints, :created_at, :datetime)
    add_column(:salepoints, :updated_at, :datetime)
  end
end
