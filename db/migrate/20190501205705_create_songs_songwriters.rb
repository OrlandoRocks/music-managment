class CreateSongsSongwriters < ActiveRecord::Migration[4.2]
  def change
    create_table :songs_songwriters do |t|
      t.references :song, null: false
      t.references :songwriter, null: false
      t.timestamps
    end

    add_index :songs_songwriters, :song_id
    add_index :songs_songwriters, :songwriter_id
  end
end
