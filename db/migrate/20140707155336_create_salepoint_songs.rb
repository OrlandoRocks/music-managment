class CreateSalepointSongs < ActiveRecord::Migration[4.2]
  def change
    create_table :salepoint_songs do |t|
      t.datetime :takedown_at
      t.string :state, null: false, default: "new"
      t.integer :salepoint_id, null: false
      t.integer :song_id, null: false

      t.timestamps
    end
    
    add_index :salepoint_songs, :salepoint_id
    add_index :salepoint_songs, :song_id
  end
end
