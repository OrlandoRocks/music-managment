class UpdateSalepointSubscriptionProducts < ActiveRecord::Migration[4.2]
  def self.up
    products = Product.where("name = ?", "Store Automator")
    products.each do |prod|
      prod.renewal_level = "None"
      prod.save!
    end
  end

  def self.down
    products = Product.where("name = ?", "Store Automator")
    products.each do |prod|
      prod.renewal_level = "Item"
      prod.save!
    end
  end
end
