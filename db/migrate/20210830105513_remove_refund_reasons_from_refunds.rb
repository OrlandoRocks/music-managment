class RemoveRefundReasonsFromRefunds < ActiveRecord::Migration[6.0]
  def change
    safety_assured { remove_column :refunds, :refund_reason }
  end
end
