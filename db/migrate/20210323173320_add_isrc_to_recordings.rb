class AddIsrcToRecordings < ActiveRecord::Migration[6.0]
  def change
    add_column :recordings, :isrc, :string, null: true
  end
end
