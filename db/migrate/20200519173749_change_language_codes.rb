class ChangeLanguageCodes < ActiveRecord::Migration[6.0]
  def change
    safety_assured { add_column :language_codes, :ISO_639_1_code, :string }
    safety_assured { add_column :language_codes, :ISO_639_2_code, :string }
    safety_assured { add_column :language_codes, :ISO_639_3_code, :string }
  end
end
