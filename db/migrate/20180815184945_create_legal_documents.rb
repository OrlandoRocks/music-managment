class CreateLegalDocuments < ActiveRecord::Migration[4.2]
  def change
    create_table :legal_documents do |t|
      t.string     :name
      t.string     :external_uuid, unique: true, null: false
      t.references :document_template
      t.references :signer
      t.references :subject, polymorphic: true
      t.text       :raw_response
      t.datetime   :signed_at
      t.timestamps
    end

    add_index :legal_documents, :external_uuid, using: 'btree', unique: true
    add_index :legal_documents, :signer_id, using: 'btree'
    add_index :legal_documents, [:subject_id, :subject_type], using: 'btree'
  end
end
