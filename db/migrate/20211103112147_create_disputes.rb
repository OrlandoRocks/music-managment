class CreateDisputes < ActiveRecord::Migration[6.0]
  def change
    create_table :disputes do |t|
      t.string :source_type
      t.integer :source_id
      t.string :transaction_identifier
      t.string :dispute_identifier
      t.column :action_type, "enum('accepted','disputed')"
      t.string :reason
      t.integer :amount_cents
      t.string :currency
      t.references :processed_by, index: true
      t.references :dispute_status, null: false
      t.references :refund, null: false, index: true
      t.references :person, null: false, index: true
      t.references :invoice, null: false, index: true
      t.timestamps
    end
  end
end
