class RemoveProgramIdFromTaxForms < ActiveRecord::Migration[6.0]
  def change
    safety_assured { remove_column :tax_forms, :program_id, :string }
  end
end
