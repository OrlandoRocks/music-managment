class ChangeCounterpointCodeToCodeInRoyaltyPayments < ActiveRecord::Migration[6.0]
  def change
    add_column :royalty_payments, :code, :string
  end
end
