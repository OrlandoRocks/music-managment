class RenameRingtoneRelatedSongs < ActiveRecord::Migration[6.0]
  def self.up
    safety_assured do
      rename_table :ringtone_related_songs, :archive_ringtone_related_songs
    end
  end

  def self.down
    safety_assured do
      rename_table :achive_ringtone_related_songs, :ringtone_related_songs
    end
  end
end
