class UpdateYoutubeMoneyStoreName < ActiveRecord::Migration[4.2]
  def up
    store = Store.find_by(abbrev: "ytsr")
    store.update_attribute(:name, "YouTube Money")
  end

  def down
    store = Store.find_by(abbrev: "ytsr")
    store.update_attribute(:name, "Youtube Sound Recording")
  end
end
