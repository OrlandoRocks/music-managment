class AddDeluxeItunesPriceTiers < ActiveRecord::Migration[4.2]
  def self.up
    #Create deluxe one through four
    vp1 = VariablePrice.create(:store_id=>nil, :price_code=>50, :price_code_display=>"Deluxe One - 12.99", :active=>nil, :position=>12 )
    vp2 = VariablePrice.create(:store_id=>nil, :price_code=>51, :price_code_display=>"Deluxe Two - 13.99", :active=>nil, :position=>13 )
    vp3 = VariablePrice.create(:store_id=>nil, :price_code=>52, :price_code_display=>"Deluxe Three - 14.99", :active=>nil, :position=>14 )
    vp4 = VariablePrice.create(:store_id=>nil, :price_code=>53, :price_code_display=>"Deluxe Four - 15.99", :active=>nil, :position=>15 )

    stores = Store.where("name like ?", "%iTunes%")

    #Loop through all itunes stores and add the variable price
    stores.each do |store|
      [vp1,vp2,vp3,vp4].each do |vp|
        VariablePriceStore.create(:store=>store, :variable_price=>vp, :position=>vp.id, :is_active=>true)
      end
    end

    #These salepoints apply to albums and singles
    [vp1,vp2,vp3,vp4].each do |vp|
      VariablePriceSalepointableType.create(:salepointable_type=>"Album", :variable_price=>vp)
      VariablePriceSalepointableType.create(:salepointable_type=>"Single", :variable_price=>vp)
    end
  end

  def self.down
    VariablePrice.where(:store_id=>nil, :price_code=>50, :price_code_display=>"Deluxe One - 12.99", :active=>nil, :position=>12).first.destroy
    VariablePrice.where(:store_id=>nil, :price_code=>51, :price_code_display=>"Deluxe Two - 13.99", :active=>nil, :position=>13).first.destroy
    VariablePrice.where(:store_id=>nil, :price_code=>52, :price_code_display=>"Deluxe Three - 14.99", :active=>nil, :position=>14).first.destroy
    VariablePrice.where(:store_id=>nil, :price_code=>53, :price_code_display=>"Deluxe Four - 15.99", :active=>nil, :position=>15).first.destroy
  end
end
