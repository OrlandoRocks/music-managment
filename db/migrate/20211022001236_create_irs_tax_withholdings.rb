class CreateIRSTaxWithholdings < ActiveRecord::Migration[6.0]
  def change
    create_table :irs_tax_withholdings do |t|
      t.integer :withheld_from_person_transaction_id,
        null: false,
        unique: true,
        index: { name: :irs_tax_withholdings_on_withheld_txn }

      t.belongs_to :irs_transaction
      t.decimal    :withheld_amount, precision: 23, scale: 14, null: false
      t.timestamps
    end
  end
end
