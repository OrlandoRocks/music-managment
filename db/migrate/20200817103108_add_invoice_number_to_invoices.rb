class AddInvoiceNumberToInvoices < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      add_column :invoices, :invoice_number, :integer, blank: true
      add_column :invoices, :invoice_prefix, "ENUM('BI-INV', 'TC-INV')", default: nil
      add_column :invoices, :invoice_sent_at, :datetime, blank: true
    end

    safety_assured { add_index(:invoices, [:invoice_prefix]) }
  end
end

