class IncreateDjHandlerSizeLimit < ActiveRecord::Migration[4.2]
  def up
  	change_column :delayed_jobs, :handler, :text, :limit => 16777215
  end

  def down
  	change_column :delayed_jobs, :handler, :text
  end
end
