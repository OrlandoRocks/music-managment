class AddEntityColumnsToComposers < ActiveRecord::Migration[4.2]
  def self.up
    add_column :composers, :is_entity, :boolean, :default => 0
    add_column :composers, :entity_name, :string
  end

  def self.down
    remove_column :composers, :is_entity
    remove_column :composers, :entity_name
  end
end
