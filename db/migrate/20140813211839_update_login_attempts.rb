class UpdateLoginAttempts < ActiveRecord::Migration[4.2]
  def up 
    change_table :login_attempts do |t|
      t.rename :count, :all_time_count
      t.integer :since_last_login_count, :default=>0
    end
  end

  def down
    change_table :login_attempts do |t|
      t.rename :all_time_count, :count
      t.remove :since_last_login_count
    end
  end
end
