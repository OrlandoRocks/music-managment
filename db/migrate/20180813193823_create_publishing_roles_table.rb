class CreatePublishingRolesTable < ActiveRecord::Migration[4.2]
  def change
    create_table :publishing_roles do |t|
      t.string :title

      t.timestamps
    end

    add_index :publishing_roles, :title
  end
end
