class AddPayinProviderConfigToStoredCreditCards < ActiveRecord::Migration[6.0]
  def change
    add_reference :stored_credit_cards, :payin_provider_config, index: true
  end
end
