class AddTaxFormTypeRefToTaxForms < ActiveRecord::Migration[6.0]
  def change
    add_reference :tax_forms, :tax_form_type, foreign_key: true
  end
end
