class CreatePayoutPrograms < ActiveRecord::Migration[4.2]
  def change
    create_table  :payout_programs do |t|
      t.string  :payout_type
      t.integer :client_program_id
      t.string  :currency
      t.string  :region
      t.boolean :inside_region
      t.boolean :enabled
      t.string  :fee_type
      t.integer :amount_cents
      t.float   :percentage
      t.integer :max_fee

      t.timestamps
    end

    add_index :payout_programs, :payout_type, using: 'btree'
    add_index :payout_programs, :client_program_id, unique: true, using: 'btree'
    add_index :payout_programs, :currency, using: 'btree'
    add_index :payout_programs, :enabled, using: 'btree'

    add_column :payout_transactions, :payout_program_id, :integer, index: true
    add_column :payout_withdrawal_types, :payout_program_id, :integer, index: true

  end
end
