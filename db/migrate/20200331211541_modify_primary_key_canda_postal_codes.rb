class ModifyPrimaryKeyCandaPostalCodes < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      execute "ALTER TABLE canada_postal_codes DROP PRIMARY KEY, ADD PRIMARY KEY (id)"
      add_index :canada_postal_codes, [:code, :city], unique: true, name: "index_postal_codes_on_code_and_city"
    end
  end

  def down
    safety_assured do
      remove_index :canada_postal_codes, name: "index_postal_codes_on_code_and_city"
      execute "ALTER TABLE canada_postal_codes DROP PRIMARY KEY, ADD PRIMARY KEY (code, city)"
    end
  end
end
