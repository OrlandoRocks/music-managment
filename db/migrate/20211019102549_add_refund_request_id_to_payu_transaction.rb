class AddRefundRequestIdToPayuTransaction < ActiveRecord::Migration[6.0]
  def change
    add_column :payu_transactions, :refund_request_id, :string
  end
end
