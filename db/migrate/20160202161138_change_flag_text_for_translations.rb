class ChangeFlagTextForTranslations < ActiveRecord::Migration[4.2]
  def change
    Product.where("flag_text like 'Save%'").each do |product|
      product.update(flag_text: product.flag_text.downcase.gsub("%", ""))
    end

    Product.where("flag_text = 'Best deal'").each do |product|
      product.update(flag_text: product.flag_text.gsub!(" ", "_").downcase)
    end
  end
end
