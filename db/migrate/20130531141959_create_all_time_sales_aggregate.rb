class CreateAllTimeSalesAggregate < ActiveRecord::Migration[4.2]
  def self.up
    create_table :sales_by_year do |t|
      t.integer :year
      t.integer :store_id
      t.string  :related_type
      t.integer :related_id

      t.integer :qty
      t.float   :amount
    end

    add_index :sales_by_year, [:year, :store_id]
    add_index :sales_by_year, [:related_type, :related_id]
  end

  def self.down
    drop_table :sales_by_year
  end
  
end
