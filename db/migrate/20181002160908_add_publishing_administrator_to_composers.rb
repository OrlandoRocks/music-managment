class AddPublishingAdministratorToComposers < ActiveRecord::Migration[4.2]
  def change
    add_column :composers, :publishing_administrator_id, :integer
    add_index :composers, :publishing_administrator_id
  end
end
