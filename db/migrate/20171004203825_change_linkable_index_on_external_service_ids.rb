class ChangeLinkableIndexOnExternalServiceIds < ActiveRecord::Migration[4.2]
  def up
    remove_index :external_service_ids, name: :index_external_service_ids_on_linkable_id_and_store_name
    add_index :external_service_ids, [:linkable_id, :linkable_type, :service_name], name: :index_external_service_ids_on_linkable_and_service_name, unique: true
  end

  def down
    remove_index :external_service_ids, name: :index_external_service_ids_on_linkable_and_service_name
    add_index :external_service_ids, [:linkable_id, :service_name], name: :index_external_service_ids_on_linkable_id_and_store_name, unique: true
  end
end
