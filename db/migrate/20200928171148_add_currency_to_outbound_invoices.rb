class AddCurrencyToOutboundInvoices < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      execute("ALTER TABLE outbound_invoices ADD currency CHAR(3) NOT NULL DEFAULT 'USD'  COMMENT 'Currency for outbound invoice'")
    end
  end

  def down
    remove_column :outbound_invoices, :currency
  end
end
