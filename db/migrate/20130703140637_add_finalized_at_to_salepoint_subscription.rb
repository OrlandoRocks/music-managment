class AddFinalizedAtToSalepointSubscription < ActiveRecord::Migration[4.2]
  def self.up
    add_column :salepoint_subscriptions, :finalized_at, :datetime
  end

  def self.down
    remove_column :salepoint_subscriptions, :finalized_at
  end
end
