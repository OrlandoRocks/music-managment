class AddIpAddressToLoginEvents < ActiveRecord::Migration[4.2]
  def change
    add_column :login_events, :ip_address, :string, null: true
  end
end
