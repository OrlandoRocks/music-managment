class AddDestroyTargetedOfferRight < ActiveRecord::Migration[4.2]
  CONTROLLER_NAME       = "admin/targeted_offers"
  DESTROY_ACTION          = "destroy"
  DESTROY_NAME            = "targeted_offers/destroy"

  ROLE_NAME             = "Targeted Offers"

  def self.up
    rights = []

    rights << Right.create!( :controller=>CONTROLLER_NAME, :action=>DESTROY_ACTION, :name=>DESTROY_NAME )

    to_role = Role.find_or_create_by(name: ROLE_NAME)

    rights.each do |right|

      to_role.rights << right

    end

    to_role.save!
  end

  def self.down
    destroy_right   = Right.where("controller=:controller and action=:action and name=:name", {:controller=>CONTROLLER_NAME, :action=>DESTROY_ACTION,    :name=>DESTROY_NAME }).first

    #HABTM relationship cleans up the join table on destroy
    destroy_right.destroy
  end


end
