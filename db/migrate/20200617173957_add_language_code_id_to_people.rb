class AddLanguageCodeIdToPeople < ActiveRecord::Migration[6.0]
  def change
    safety_assured { add_column :people, :language_code_id, :integer }
  end
end
