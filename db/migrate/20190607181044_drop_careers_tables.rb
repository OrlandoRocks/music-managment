class DropCareersTables < ActiveRecord::Migration[4.2]
  def up
    drop_table :career_listing_items
    drop_table :career_listings
    drop_table :career_listing_boilerplates
  end

  def down
    create_table  :career_listings do |t|
      t.string    :listing_title, null: false
      t.string    :listing_location, null: false
      t.text      :listing_description, null: false
      t.integer   :display_order
      t.boolean   :is_active, null: false, default: false
      t.timestamps
    end

    create_table  :career_listing_items do |t|
      t.integer   :career_listing_id, null: false
      t.string    :listing_item_type, null: false
      t.string    :listing_item_content, null: false
      t.integer   :display_order
      t.boolean   :is_active, null: false, default: false
      t.timestamps
    end

    create_table  :career_listing_boilerplates do |t|
      t.text      :boilerplate_text, null: false
      t.integer   :display_order
      t.boolean   :is_active, null: false, default: true
      t.timestamps
    end
  end
end
