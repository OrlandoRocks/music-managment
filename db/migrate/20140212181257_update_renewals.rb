class UpdateRenewals < ActiveRecord::Migration[4.2]
  def up

    p = Product.create(
      :created_by_id=>1,
      :country_website_id=>1,
      :name=>"2 Year Album Renewal",
      :display_name=>"2 Year Album Renewal",
      :description=>"2 year renewal fee for your album.",
      :status=>"Active",
      :product_type=>"Renewal",
      :is_default=>1,
      :applies_to_product=>"Album",
      :sort_order=>1,
      :renewal_level=>"None",
      :currency=>"USD",
      :price=>0
    )

    pi = ProductItem.album_renewal_for_type_and_duration(CountryWebsite::UNITED_STATES, "Album", "year", 2 ).readonly(false).first
    pi.renewal_product = p
    pi.renewal_duration = 2
    pi.save!

    p = Product.create(
      :created_by_id=>1,
      :country_website_id=>2,
      :name=>"2 Year Album Renewal",
      :display_name=>"2 Year Album Renewal",
      :description=>"2 year renewal fee for your album.",
      :status=>"Active",
      :product_type=>"Renewal",
      :is_default=>1,
      :applies_to_product=>"Album",
      :sort_order=>1,
      :renewal_level=>"None",
      :currency=>"CAD",
      :price=>0
    )

    pi = ProductItem.album_renewal_for_type_and_duration(CountryWebsite::CANADA, "Album", "year", 2 ).readonly(false).first
    pi.renewal_product = p
    pi.renewal_duration = 2
    pi.save!

    p = Product.create(
      :created_by_id=>1,
      :country_website_id=>1,
      :name=>"2 Year Single Renewal",
      :display_name=>"2 Year Single Renewal",
      :description=>"2 year renewal fee for your single.",
      :status=>"Active",
      :product_type=>"Renewal",
      :is_default=>1,
      :applies_to_product=>"Single",
      :sort_order=>1,
      :renewal_level=>"None",
      :currency=>"USD",
      :price=>0
    )

    pi = ProductItem.album_renewal_for_type_and_duration(CountryWebsite::UNITED_STATES, "Single", "year", 2 ).readonly(false).first
    pi.renewal_product = p
    pi.renewal_duration = 2
    pi.save!

    p = Product.create(
      :created_by_id=>1,
      :country_website_id=>2,
      :name=>"2 Year Single Renewal",
      :display_name=>"2 Year Single Renewal",
      :description=>"2 year renewal fee for your single.",
      :status=>"Active",
      :product_type=>"Renewal",
      :is_default=>1,
      :applies_to_product=>"Single",
      :sort_order=>1,
      :renewal_level=>"None",
      :currency=>"CAD",
      :price=>0
    )

    pi = ProductItem.album_renewal_for_type_and_duration(CountryWebsite::CANADA, "Single", "year", 2 ).readonly(false).first
    pi.renewal_product = p
    pi.renewal_duration = 2
    pi.save!

  end

  def down
    us_album_product  = 1
    ca_album_product  = 66
    us_single_product = 3
    ca_single_product = 68

    pi = ProductItem.album_renewal_for_type_and_duration(CountryWebsite::UNITED_STATES, "Album", "year", 2 ).readonly(false).first
    pi.renewal_product.destroy
    pi.renewal_product_id = us_album_product
    pi.renewal_duration = 1
    pi.save!
    

    pi = ProductItem.album_renewal_for_type_and_duration(CountryWebsite::CANADA, "Album", "year", 2 ).readonly(false).first
    pi.renewal_product.destroy
    pi.renewal_product_id = ca_album_product
    pi.renewal_duration = 1
    pi.save!
    
    pi = ProductItem.album_renewal_for_type_and_duration(CountryWebsite::UNITED_STATES, "Single", "year", 2 ).readonly(false).first
    pi.renewal_product.destroy
    pi.renewal_product_id = us_single_product
    pi.renewal_duration = 1
    pi.save!

    pi = ProductItem.album_renewal_for_type_and_duration(CountryWebsite::CANADA, "Single", "year", 2 ).readonly(false).first
    pi.renewal_product.destroy
    pi.renewal_product_id = ca_single_product
    pi.renewal_duration = 1
    pi.save!
  end

end
