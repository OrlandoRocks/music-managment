class AddIndexesToSyncLicensing < ActiveRecord::Migration[4.2]
  def self.up
    add_index :sync_license_requests, :sync_license_production_id
    add_index :sync_license_requests, :song_code

    add_index :sync_license_options, :sync_license_request_id
  end

  def self.down
    remove_index :sync_license_requests, :sync_license_production_id
    remove_index :sync_license_requests, :song_code

    remove_index :sync_license_options, :sync_license_request_id
  end
end
