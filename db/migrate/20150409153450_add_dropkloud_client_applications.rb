class AddDropkloudClientApplications < ActiveRecord::Migration[4.2]
  def up
    if Rails.env.production?
      execute("INSERT INTO client_applications (authorized_by_role, callback_url, created_at, `key`, name, secret, support_url, updated_at, url, user_id) VALUES (0, 'https://dropk-production.herokuapp.com/users/auth/tunecore/callback', NOW(), 'wXV0vksv3dT0rEPYxfuX', 'dropkloud', 'LncTTKrVhmGASSSxS3xcKoaaburkbS7NWYswJGYU', NULL, NOW(), 'https://dropk-production.herokuapp.com', NULL)")
    elsif Rails.env.staging?
      execute("INSERT INTO client_applications (authorized_by_role, callback_url, created_at, `key`, name, secret, support_url, updated_at, url, user_id) VALUES (0, 'https://dropk.herokuapp.com/users/auth/tunecore/callback', NOW(), 'quLiYE4LTNUcUltcvFw', 'dropkloud', 'RL9dDqkFmlhWSpHKUDbGdWL6nY7nvTzEFHlAKvVn', NULL, NOW(), 'https://dropk.herokuapp.com', NULL)")
    else
      execute("INSERT INTO client_applications (authorized_by_role, callback_url, created_at, `key`, name, secret, support_url, updated_at, url, user_id) VALUES (0, 'http://localhost:5000/users/auth/tunecore/callback', NOW(), 'yskF8RvJvdthksZo1UZF', 'dropkloud', 'AgZpWjVWdXeZGiRCBwdWtN6ClM5z0qDKltbYBGIs', NULL, NOW(), 'http://localhost:5000', NULL)")
    end
  end

  def down
    execute("DELETE FROM client_applications WHERE name = 'dropkloud'")
  end
end
