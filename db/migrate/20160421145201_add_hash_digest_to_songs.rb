class AddHashDigestToSongs < ActiveRecord::Migration[4.2]
  def change
    add_column :songs, :content_fingerprint, :text
  end
end
