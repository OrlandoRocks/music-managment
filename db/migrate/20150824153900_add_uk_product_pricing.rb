class AddUkProductPricing < ActiveRecord::Migration[4.2]
  def up
    update_pricing(get_pricing_map)
  end

  def down
    update_pricing(get_pricing_map.invert)
  end

  # an exception at any level will rollback all three
  def update_pricing(price_map)
    uk_website_id = CountryWebsite.find_by(name: 'TuneCore UK').id
    uk_products = Product.where("country_website_id = ? AND currency = 'GBP'", uk_website_id)
    uk_products.each do |p|
      begin
        Product.transaction do
          p.update!(price: price_map[p.price.to_f])
          p.product_items.each do |pi|
            ProductItem.transaction(:requires_new => true) do
              pi.update!(price: price_map[pi.price.to_f])
              pi.product_item_rules.each do |pir|
                ProductItemRule.transaction(:requires_new => true) do
                  pir.update!(price: price_map[pir.price.to_f])
                end
              end
            end
          end
        end
        # double logging on purpose so viewable from console at run-time and viewable in logs
        p "Successfully updated UK Product Pricing for ID:#{p.id} Product Name:#{p.name} Product Description:#{p.description}"
        Rails.logger.info("Successfully updated UK Product Pricing for ID:#{p.id} Product Name:#{p.name} Product Description:#{p.description}")
      rescue StandardError => e
        p "ERROR: Unable to update UK Product Pricing for ID:#{p.id} Product Name:#{p.name}Product Description:#{p.description} Error:#{e.message}"
        Rails.logger.error("Unable to update UK Product Pricing for ID:#{p.id} Product Name:#{p.name}Product Description:#{p.description} Error:#{e.message}")
      end
    end
  end

  # k = usd, v = gbp
  def get_pricing_map
    price_map = {}
    price_map[-1.48] = -0.99
    price_map[0.0] = 0.0
    price_map[0.99] = 0.69
    price_map[1.98] = 1.49
    price_map[4.99] = 2.99
    # Dropkloud
    price_map[8.99] = 7.99
    price_map[9.99] = 6.99
    price_map[10.0] = 6.99
    price_map[15.0] = 9.99
    price_map[18.98] = 11.39
    price_map[19.98] = 13.99
    price_map[19.99] = 13.99
    price_map[20.0] = 13.99
    price_map[25.0] = 16.99
    price_map[29.99] = 19.99
    # Trackmarts Enhanced
    price_map[40.0] = 26.99
    # Single Distribution Credit - 5 Pack
    price_map[44.95] = 26.99
    price_map[49.99] = 33.99
    price_map[56.99] = 39.99
    price_map[75.98] = 51.49
    price_map[87.91] = 52.99
    price_map[89.99] = 62.99
    price_map[94.98] = 64.89
    price_map[115.0] = 76.99
    price_map[134.95] = 89.99
    price_map[169.99] = 118.99
    price_map[171.82] = 102.99
    price_map[206.95] = 140.49
    price_map[224.96] = 153.79
    price_map[263.91] = 175.99
    price_map[319.99] = 223.99
    price_map[515.82] = 342.99

    price_map
  end
end
