class BackfillPaypalTransferSuspiciousFlags < ActiveRecord::Migration[4.2]
  def up
    PaypalTransfer.where(transfer_status: 'pending').each do |pt|
      p "backfilling susp flag for record #{pt.id}"
      Rails.logger.info("backfilling susp flag for record #{pt.id}")
      pt.set_suspicious_flags
      Rails.logger.info("Unable to update susp flag for record #{pt.id}") unless pt.save
    end
  end

  def down
  end
end
