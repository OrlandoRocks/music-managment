class AddItunesWorldwideToSalepointableStores < ActiveRecord::Migration[4.2]
    def self.up
      storeid = Store.find_by(name: "iTunes Worldwide").id
      execute ("insert into salepointable_stores (salepointable_type,store_id) values ('Album','#{storeid}')")
      execute ("insert into salepointable_stores (salepointable_type,store_id) values ('Single','#{storeid}')")
      execute ("insert into salepointable_stores (salepointable_type,store_id) values ('Ringtone','#{storeid}')")
    end

    def self.down
      storeid = Store.find_by(name: "iTunes Worldwide").id
      execute("delete from salepointable_stores where store_id = #{storeid}")
    end
  end
