class CreateRecoverAssets < ActiveRecord::Migration[6.0]
  def change
    create_table :studio_recover_assets do |t|
      t.integer  :person_id
      t.integer  :admin_id
      t.string   :state
      t.text     :custom_fields
      t.column   :s3_asset_id, :integer

      t.timestamps
    end

    add_index :studio_recover_assets, :person_id
  end
end

