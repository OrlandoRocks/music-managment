class AddIndicesToCreativeSongRoles < ActiveRecord::Migration[4.2]
  def change
    add_index :creative_song_roles, :song_id
    add_index :creative_song_roles, :song_role_id
    add_index :creative_song_roles, :creative_id
  end
end
