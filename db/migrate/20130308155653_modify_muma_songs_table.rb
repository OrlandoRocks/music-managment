class ModifyMumaSongsTable < ActiveRecord::Migration[4.2]
  def self.up
    add_column :muma_songs, :parent_code, :integer 
  end

  def self.down
    remove_column :muma_songs, :parent_code
  end
end
