class CreateItunesWorldwide < ActiveRecord::Migration[4.2]
  def self.up
    if Store.find_by(name: "iTunes Worldwide") == nil
      Store.create(:name=>'iTunes Worldwide',:abbrev=>'ww', :short_name=>'iTunesWW', :position=>300, :needs_rights_assignment=>false, :is_active=>false,:base_price_policy_id=>"3" )
    else
      raise "iTunes Worldwide Store exists"
    end
  end

  def self.down
    execute("DELETE FROM stores where name = 'iTunes Asia'")
  end
end
