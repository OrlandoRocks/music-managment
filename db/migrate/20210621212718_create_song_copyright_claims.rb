class CreateSongCopyrightClaims < ActiveRecord::Migration[6.0]
  def change
    create_table :song_copyright_claims do |t|
      t.integer :song_id, null: false
      t.integer :person_id, null: true
      t.boolean :claimant_has_songwriter_agreement, default: false
      t.boolean :claimant_has_pro_or_cmo, default: false
      t.boolean :claimant_has_tc_publishing, default: false
    end
  end
end
