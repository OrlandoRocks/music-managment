class UpdatePublishingRelatedModels < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      add_column :non_tunecore_albums, :account_id, :integer
      add_column :non_tunecore_albums, :publishing_composer_id, :integer
      add_column :non_tunecore_songs, :publishing_composition_id, :integer
      add_column :songs, :publishing_composition_id, :integer
      add_column :muma_songs, :publishing_composition_id, :integer
      add_column :recordings, :publishing_composition_id, :integer
      add_column :terminated_composers, :publishing_composer_id, :integer
      add_column :tax_info, :publishing_composer_id, :integer
      add_column :royalty_payments, :publishing_composer_id, :integer

      add_index :non_tunecore_albums, :account_id
      add_index :non_tunecore_albums, :publishing_composer_id
      add_index :non_tunecore_songs, :publishing_composition_id
      add_index :songs, :publishing_composition_id
      add_index :muma_songs, :publishing_composition_id
      add_index :recordings, :publishing_composition_id
      add_index :terminated_composers, :publishing_composer_id
    end
  end

  def down
    safety_assured do
      remove_index :non_tunecore_albums, :account_id
      remove_index :non_tunecore_albums, :publishing_composer_id
      remove_index :non_tunecore_songs, :publishing_composition_id
      remove_index :songs, :publishing_composition_id
      remove_index :muma_songs, :publishing_composition_id
      remove_index :recordings, :publishing_composition_id
      remove_index :terminated_composers, :publishing_composer_id

      remove_column :non_tunecore_albums, :account_id
      remove_column :non_tunecore_albums, :publishing_composer_id
      remove_column :non_tunecore_songs, :publishing_composition_id
      remove_column :songs, :publishing_composition_id
      remove_column :muma_songs, :publishing_composition_id
      remove_column :recordings, :publishing_composition_id
      remove_column :terminated_composers, :publishing_composer_id
      remove_column :tax_info, :publishing_composer_id
      remove_column :royalty_payments, :publishing_composer_id
    end
  end
end
