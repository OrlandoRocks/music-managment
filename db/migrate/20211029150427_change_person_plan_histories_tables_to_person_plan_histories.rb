class ChangePersonPlanHistoriesTablesToPersonPlanHistories < ActiveRecord::Migration[6.0]
  def change
    safety_assured { rename_table :person_plan_histories_tables, :person_plan_histories }
  end
end
