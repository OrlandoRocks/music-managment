class AlterOfferTypeInTargetedOffer < ActiveRecord::Migration[6.0]
  def self.up
    safety_assured do
      execute("ALTER TABLE targeted_offers CHANGE COLUMN `offer_type` `offer_type` ENUM('new', 'existing', 'country_targeted') NULL DEFAULT NULL  ;")
    end
  end

  def self.down
    safety_assured do
      execute("ALTER TABLE targeted_offers CHANGE COLUMN `offer_type` `offer_type` ENUM('new', 'existing') NULL DEFAULT NULL  ;")
    end
  end
end
