class AddCurrencyToInvoiceSettlements < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE invoice_settlements ADD currency CHAR(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency for invoice settlement' AFTER settlement_amount_cents")
  end

  def self.down
    remove_column :invoice_settlements, :currency
  end
end
