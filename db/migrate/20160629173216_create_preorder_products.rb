class CreatePreorderProducts < ActiveRecord::Migration[4.2]
  def up
    create_table :preorder_products do |t|
      t.string :product_name
      t.timestamps
    end

    add_column :preorder_purchases, :preorder_product_id, :integer
  end

  def down
  end
end
