class AddIndexToDistributionsSalepoints < ActiveRecord::Migration[4.2]
  def up
    safety_assured {
      execute "CREATE UNIQUE INDEX `index_distributions_salepoints_distribution_id_salepoint_id`  ON `distributions_salepoints` (`distribution_id`, `salepoint_id`) ALGORITHM=INPLACE LOCK=NONE;"
    }
  end

  def down
    safety_assured {
      execute "DROP INDEX `index_distributions_salepoints_distribution_id_salepoint_id` ON `distributions_salepoints`;"
    }
  end
end
