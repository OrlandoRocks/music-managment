class AddProductTaxRateToProduct < ActiveRecord::Migration[4.2]
  def change
    add_column :products, :product_tax_rate_id, :int
  end
end
