class AddCurrencyToPaypalTransfers < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE paypal_transfers ADD currency CHAR(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency for payments' AFTER admin_charge_cents")
  end

  def self.down
    remove_column :paypal_transfers, :currency
  end
end
