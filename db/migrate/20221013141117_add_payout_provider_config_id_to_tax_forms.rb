class AddPayoutProviderConfigIdToTaxForms < ActiveRecord::Migration[6.0]
  def change
    add_reference :tax_forms, :payout_provider_config,
                  after: "tax_form_type_id",
                  comment: "references payout_provider_configs table."
  end
end
