class CreateExperiment4MarketingExperiment < ActiveRecord::Migration[4.2]
  MarketingExperiment.create(marketing_service: 'Hubspot', experiment_type: 'UNCONVERTED', experiment_name: 'experiment4', number_of_variations: 40, active_flag: 1)
end
