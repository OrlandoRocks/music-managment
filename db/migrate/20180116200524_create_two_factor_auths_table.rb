class CreateTwoFactorAuthsTable < ActiveRecord::Migration[4.2]
  def up
    create_table :two_factor_auths do |t|
      t.integer     :dismissals, default: 0
      t.references  :person, index: true, foreign_key: true

      t.timestamps
    end

    add_index :two_factor_auths, :person_id
  end

  def down
    drop_table :two_factor_auths
  end
end
