class AddTimestampsToWhyTunecoreConfigs < ActiveRecord::Migration[4.2]
  def self.up
    add_column :why_tunecore_configs, :updated_at, :datetime
    add_column :why_tunecore_configs, :created_at, :datetime
  end

  def self.down
    remove_column :why_tunecore_configs, :updated_at
    remove_column :why_tunecore_configs, :created_at
  end
end
