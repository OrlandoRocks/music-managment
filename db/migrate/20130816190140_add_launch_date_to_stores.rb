class AddLaunchDateToStores < ActiveRecord::Migration[4.2]
  def self.up
    # Add new column to keep track of when the store was made available
    add_column :stores, :launched_at, :datetime

    stores = Store.all
    stores.each do |s|
      iu = InventoryUsage.joins("INNER JOIN salepoints sp on inventory_usages.related_type = 'Salepoint' and inventory_usages.related_id = sp.id").where("sp.store_id = ?", s.id).order("inventory_usages.id ASC").first
      if iu
        s.launched_at = iu.created_at
        s.save!
      end
    end
  end

  def self.down
    remove_column :stores, :launched_at
  end
end
