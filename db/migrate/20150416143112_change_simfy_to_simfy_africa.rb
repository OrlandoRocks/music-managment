class ChangeSimfyToSimfyAfrica < ActiveRecord::Migration[4.2]
  def up
    store = Store.find_by(short_name: "Simfy")
    if store
      store.update(name: "Simfy Africa")
    end
  end

  def down
    store = Store.find_by(short_name: "Simfy Africa")
    if store
      store.update(name: "Simfy")
    end
  end
end
