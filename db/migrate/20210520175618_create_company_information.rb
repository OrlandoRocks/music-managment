class CreateCompanyInformation < ActiveRecord::Migration[6.0]
  def change
    create_table :company_informations do |t|
      t.references :person, null: false, index: { unique: true }
      t.string :enterprise_number
      t.string :company_registry_data
      t.string :place_of_legal_seat
      t.string :registered_share_capital

      t.timestamps
    end
  end
end
