class AddAltTextToPromotionalProduct < ActiveRecord::Migration[4.2]
  def self.up
    add_column :promotional_products, :alt_text, :string
  end

  def self.down
    remove_column :promotional_products, :alt_text
  end
end
