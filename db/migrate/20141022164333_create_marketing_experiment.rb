class CreateMarketingExperiment < ActiveRecord::Migration[4.2]
  def self.up
    create_table :marketing_experiments do |t|
      t.string  :marketing_service
      t.string  :experiment_type
      t.string  :experiment_name
      t.integer :number_of_variations
      t.boolean :active_flag

      t.timestamps
    end

    MarketingExperiment.create({
      marketing_service:    "Hubspot",
      experiment_type:      "UNCONVERTED",
      experiment_name:      "experiment2",
      number_of_variations: 2,
      active_flag:          true
    })
  end

  def self.down
    drop_table :marketing_experiments
  end
end
