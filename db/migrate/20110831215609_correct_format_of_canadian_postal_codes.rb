class CorrectFormatOfCanadianPostalCodes < ActiveRecord::Migration[4.2]
  def self.up
    execute("DROP TABLE if exists canada_postal_codes")
    execute("CREATE TABLE `canada_postal_codes` (
      `id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
      `code` char(6) NOT NULL,
      `city` varchar(30) NOT NULL,
      `province` enum('Alberta','British Columbia','Manitoba','New Brunswick','Newfoundland','Northwest Territories','Nova Scotia','Nunavut','Ontario','Prince Edward Island','Quebec','Saskatchewan','Yukon Territory') NOT NULL,
      `province_code` enum('AB','BC','MB','NB','NL','NS','NT','NU','ON','PE','QC','SK','YT') NOT NULL,
      `city_type` enum('A','D') NOT NULL,
      `latitude` decimal(17,15) NOT NULL,
      `longitude` decimal(18,15) NOT NULL,
      PRIMARY KEY (`code`,`city`),
      KEY `id` (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8")
  end

  def self.down
    #doing the reverse of the above migration by putting the previous schema back in place
    execute("DROP TABLE if exists canada_postal_codes")
    create_table :canada_postal_codes do |t|
      t.integer :id, :null => false
      t.string :code, :null => false, :limit => 7
      t.string :city, :null => false, :limit => 40
      t.string :province, :limit => 24
      t.string :province_code, :limit => 2
      t.string :city_type, :limit => 5
      t.string :latitude, :limit => 32
      t.string :longitude, :limit => 32
    end
  end
end
