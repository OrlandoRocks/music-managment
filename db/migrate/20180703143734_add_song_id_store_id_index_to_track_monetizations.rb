class AddSongIdStoreIdIndexToTrackMonetizations < ActiveRecord::Migration[4.2]
  def change
    add_index :track_monetizations, [:song_id, :store_id], unique: true
  end
end
