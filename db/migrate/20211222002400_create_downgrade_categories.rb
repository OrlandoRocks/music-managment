class CreateDowngradeCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :downgrade_categories do |t|
      t.string :name

      t.timestamps
    end
  end
end
