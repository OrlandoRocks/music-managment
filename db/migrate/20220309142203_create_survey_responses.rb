class CreateSurveyResponses < ActiveRecord::Migration[6.0]
  def change
    create_table :survey_responses do |t|
      t.references :person, null: false
      t.references :survey, null: false
      t.json       :json, null: false
      t.index [:person_id, :survey_id]

      t.timestamps
    end
  end
end
