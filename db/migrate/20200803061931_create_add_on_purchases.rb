class CreateAddOnPurchases < ActiveRecord::Migration[6.0]
  def change
    create_table :add_on_purchases do |t|
      t.references :person, null: false, foreign_key: true, type: 'int(11) unsigned'
      t.references :product, null: false, foreign_key: true, type: 'int(11) unsigned'
      t.string :payment_channel
      t.text :receipt_data
      t.datetime :payment_date
      t.timestamps
    end
  end
end
