class AddS3FileNameToMassAdjustmentBatch < ActiveRecord::Migration[4.2]
  def change
    add_column :mass_adjustment_batches, :s3_file_name, :string
  end
end
