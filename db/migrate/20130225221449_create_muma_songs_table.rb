class CreateMumaSongsTable < ActiveRecord::Migration[4.2]
  def self.up
    create_table :muma_songs do |t|
      t.integer   :code
      t.string    :title
      t.string    :label_copy
      t.integer   :composition_id
      t.decimal   :mech_collect_share, :precision => 5, :scale => 2
      t.string    :performing_artist
      t.string    :album_label
      t.string    :album_title
      t.string    :album_upc
      t.string    :isrc
      t.datetime  :ip_updated_at
      t.datetime  :source_created_at
      t.datetime  :source_updated_at
      t.datetime  :source_deleted_at
      t.timestamps
    end
  end

  def self.down
    drop_table :muma_songs
  end
end
