class CreateCopyrightClaimants < ActiveRecord::Migration[6.0]
  def change
    create_table :copyright_claimants do |t|
      t.string :email, index: true, null: false
      t.references :copyright_claim, null: false
      t.timestamps
    end
  end
end
