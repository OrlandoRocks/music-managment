class AddIpAddressAndVersionToSelfBilingStatus < ActiveRecord::Migration[6.0]
  def self.up
    add_column :self_billing_statuses, :version, :string
    add_column :self_billing_statuses, :ip_address, :string
  end

  def self.down
    remove_column :self_billing_statuses, :version
    remove_column :self_billing_statuses, :ip_address
  end
end
