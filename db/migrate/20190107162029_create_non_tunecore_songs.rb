class CreateNonTunecoreSongs < ActiveRecord::Migration[4.2]
  def change
    create_table :non_tunecore_songs do |t|
      t.string  :name, null: false
      t.integer :artist_id
      t.string  :isrc, limit: 12
      t.date    :release_date
      t.integer :song_id
      t.integer :non_tunecore_album_id, null: :false
      t.integer :composition_id
      t.timestamps
    end

    add_index :non_tunecore_songs, :song_id, where: "song_id IS NOT NULL"
    add_index :non_tunecore_songs, :artist_id, where: "artist_id IS NOT NULL"
    add_index :non_tunecore_songs, :non_tunecore_album_id
    add_index :non_tunecore_songs, :composition_id
  end
end
