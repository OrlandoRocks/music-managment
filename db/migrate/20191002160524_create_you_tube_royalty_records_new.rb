class CreateYouTubeRoyaltyRecordsNew < ActiveRecord::Migration[4.2]
  def up
    sql = "CREATE TABLE IF NOT EXISTS `you_tube_royalty_records_new` LIKE `you_tube_royalty_records`"
    ActiveRecord::Base.connection.execute(sql)

    sql = "ALTER TABLE `you_tube_royalty_records_new` DROP INDEX `person_id`, DROP INDEX `song_id`, DROP INDEX `you_tube_royalty_intake_id`, DROP INDEX `person_id_2`;"
    ActiveRecord::Base.connection.execute(sql)
  end

  def down
    ActiveRecord::Base.connection.execute("DROP TABLE `you_tube_royalty_records_new`")
  end
end
