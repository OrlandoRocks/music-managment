class AutoIncrementFlagsTable < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      execute "ALTER TABLE flags AUTO_INCREMENT=2;"
    end
  end

  def down
    safety_assured do
      execute "ALTER TABLE flags AUTO_INCREMENT=1161;"
    end
  end
end
