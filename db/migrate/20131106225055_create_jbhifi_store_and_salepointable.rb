class CreateJbhifiStoreAndSalepointable < ActiveRecord::Migration[4.2]
  def self.up
    store = Store.create(:name=>'Jbhifi', :abbrev=>'jb', :short_name=>'Jbhifi', :position=>461, :needs_rights_assignment=>false, :is_active=>false, :base_price_policy_id=>"3", :is_free => false)

    if store
      execute ("insert into salepointable_stores (salepointable_type,store_id) values ('Album','#{store.id}')")
      execute ("insert into salepointable_stores (salepointable_type,store_id) values ('Single','#{store.id}')")
    else
      puts "Store creation for Jbhifi failed"
    end
  end

  def self.down
    if store = Store.find_by(short_name: "Jbhifi")
      execute("delete from salepointable_stores where store_id = #{store.id}")
      execute("DELETE FROM stores where id = #{store.id}")
    else
      puts "Couldn't find store by the short name of Jbhifi"
    end
  end

end

