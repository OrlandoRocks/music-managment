class ChangeTargetedOfferSingleUseToUsageLimit < ActiveRecord::Migration[4.2]
  def up
    # at a later date => remove_column :targeted_offers, :single_use
    add_column :targeted_offers, :usage_limit, :integer, default: 0
  end

  def down
    remove_column :targeted_offers, :usage_limit
  end
end
