class AddCurrencyToPayoutProvider < ActiveRecord::Migration[4.2]
  def change
    add_column :payout_providers, :currency, 'char(3)'
  end
end
