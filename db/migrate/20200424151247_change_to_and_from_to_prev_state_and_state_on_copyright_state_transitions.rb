class ChangeToAndFromToPrevStateAndStateOnCopyrightStateTransitions < ActiveRecord::Migration[6.0]
  def change
    safety_assured { rename_column :copyright_state_transitions, :from, :prev_state }
    safety_assured { rename_column :copyright_state_transitions, :to, :state }
  end
end
