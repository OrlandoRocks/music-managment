class AddCurrencyToInvoices < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE invoices ADD currency CHAR(3) NOT NULL DEFAULT 'USD'  COMMENT 'Currency for invoice' AFTER final_settlement_amount_cents")
  end

  def self.down
    remove_column :invoices, :currency
  end
end
