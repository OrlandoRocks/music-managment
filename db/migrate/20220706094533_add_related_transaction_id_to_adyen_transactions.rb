class AddRelatedTransactionIdToAdyenTransactions < ActiveRecord::Migration[6.0]
  def change
    add_reference :adyen_transactions, :related_transaction, index: true
  end
end
