class RemoveProAffiliationFromPublishers < ActiveRecord::Migration[6.0]
  def change
    safety_assured { remove_column :publishers, :pro_affiliation }
  end
end
