class MarkBookletProductItemRulesInactive < ActiveRecord::Migration[4.2]
  def self.up
    change_table :product_item_rules do |t|
      t.boolean :is_active, :default => true
    end

    execute <<-SQL
      UPDATE product_item_rules SET `is_active`=1
    SQL

    rules = ProductItemRule.where("method_to_check = ?", "has_booklet?")
    rules.each do |rule|
      rule.update(:is_active => false)
    end
  end

  def self.down
    remove_column :product_item_rules, :is_active
  end
end
