class CreateStudioWatchlists < ActiveRecord::Migration[6.0]
  def change
    create_table :studio_watchlists, options: 'ENGINE=InnoDB DEFAULT CHARSET=utf8'  do |t|
      t.string :text_to_compare
      t.boolean :is_active
      t.integer :parent_id
      t.integer :group_id

      t.timestamps
    end
    add_index :studio_watchlists, :text_to_compare

  end
end
