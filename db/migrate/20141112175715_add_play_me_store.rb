class AddPlayMeStore < ActiveRecord::Migration[4.2]
  @@stores = [
    { name: "Play.me", abbrev: "playme", short_name: "PlayMe", position: 650 }
  ]

  def up
    @@stores.each do |store_info|
      store = Store.create(
        name:                     store_info[:name],
        abbrev:                   store_info[:abbrev],
        short_name:               store_info[:short_name],
        position:                 store_info[:position],
        needs_rights_assignment:  false,
        is_active:                false,
        base_price_policy_id:     "3",
        is_free:                  false,
        in_use_flag:              false
      )
      if store && store.errors.empty?
        store.salepointable_stores.create!(:salepointable_type => 'Album')
        store.salepointable_stores.create!(:salepointable_type => 'Single')
      else
        puts "Store creation for #{store_info[:name]} failed!"
      end
    end
  end

  def down
    @@stores.each do |store_info|
      if store = Store.find_by(name: store_info[:name])
        store.salepointable_stores.collect(&:destroy)
        store.destroy
      else
        puts "Couldn't find store by the short name of #{store_info[:name]}"
      end
    end
  end
end
