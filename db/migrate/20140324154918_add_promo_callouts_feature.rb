class AddPromoCalloutsFeature < ActiveRecord::Migration[4.2]
  def up
    Feature.find_or_create_by(:name=>'promo_callouts', :restrict_by_user => 1)
  end

  def down
    feature = Feature.find_by(name: 'promo_callouts')
    feature.destroy if !feature.nil?
  end
end
