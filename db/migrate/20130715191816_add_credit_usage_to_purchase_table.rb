class AddCreditUsageToPurchaseTable < ActiveRecord::Migration[4.2]
  def self.up
    # Add CreditUsage to the related_type column
    execute <<-SQL
      ALTER TABLE purchases
        MODIFY COLUMN related_type enum('Album','Single','Ringtone','Entitlement','Product','Renewal','ItunesUserReport','MusicVideo','FeatureFilm','Salepoint','Song','Widget','Composer','Video','SalepointSubscription','CreditUsage') DEFAULT NULL
    SQL
  end

  def self.down
  end
end
