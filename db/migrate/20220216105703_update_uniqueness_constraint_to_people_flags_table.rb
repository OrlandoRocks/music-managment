class UpdateUniquenessConstraintToPeopleFlagsTable < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      remove_index :people_flags, column: [:person_id, :flag_id], unique: true
      add_index :people_flags, [:person_id, :flag_id, :flag_reason_id], unique: true
    end
  end
end
