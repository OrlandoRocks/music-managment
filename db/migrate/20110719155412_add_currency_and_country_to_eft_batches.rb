class AddCurrencyAndCountryToEftBatches < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE eft_batches
     ADD COLUMN `country_website_id` int(11) unsigned DEFAULT 1 COMMENT 'Foreign Key to country_websites' AFTER id,
     ADD COLUMN currency CHAR(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency' AFTER country_website_id,
     ADD KEY `eb_country_website_id` (`country_website_id`)")
  end

  def self.down
    remove_column :eft_batches, :currency
    remove_column :eft_batches, :country_website_id
  end
end
