class AddIndexesToInvoiceStaticCustomerAddress < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      add_index :invoice_static_customer_addresses,
                [:related_id, :related_type],
                unique: true,
                name: "index_customer_addresses_on_related_id_and_related_type"
    end
  end
end
