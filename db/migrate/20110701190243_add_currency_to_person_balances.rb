class AddCurrencyToPersonBalances < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE person_balances ADD currency CHAR(3) NOT NULL DEFAULT 'USD'  COMMENT 'Currency for balance' AFTER balance;")
  end

  def self.down
    remove_column :person_balances, :currency
  end
end
