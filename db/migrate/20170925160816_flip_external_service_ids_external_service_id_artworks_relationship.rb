class FlipExternalServiceIdsExternalServiceIdArtworksRelationship < ActiveRecord::Migration[4.2]
  def up
    remove_column :external_service_id_artworks, :external_service_id_id
    add_column :external_service_ids, :external_service_id_artwork_id, :integer
    add_index :external_service_ids, :external_service_id_artwork_id
  end

  def down
    remove_column :external_service_ids, :external_service_id_artwork_id
    add_column :external_service_id_artworks, :external_service_id_id, :integer
    add_index :external_service_id_artworks, :external_service_id_id
  end
end
