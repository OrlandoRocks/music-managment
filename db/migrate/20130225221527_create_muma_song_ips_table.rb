class CreateMumaSongIpsTable < ActiveRecord::Migration[4.2]
  def self.up
    create_table :muma_song_ips do |t|
      t.integer   :song_code
      t.string    :ip_code
      t.integer   :ip_chain
      t.string    :ip_c_or_p
      t.boolean   :controlled
      t.decimal   :mech_owned_share, :scale => 2, :precision => 5
      t.decimal   :mech_collect_share, :scale => 2, :precision => 5
      t.string    :capacity_code, :limit => 2
      t.string    :link_parent_ip_code
      t.string    :publisher_name
      t.string    :publisher_code, :limit => 6
      t.string    :composer_first_name
      t.string    :composer_middle_name
      t.string    :composer_last_name
      t.string    :composer_code, :limit => 6
      t.datetime  :source_created_at
      t.datetime  :source_updated_at
      t.timestamps
    end
  end

  def self.down
    drop_table :muma_song_ips
  end
end
