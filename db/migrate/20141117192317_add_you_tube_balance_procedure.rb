class AddYouTubeBalanceProcedure < ActiveRecord::Migration[4.2]
  def up
    config   = Rails.configuration.database_configuration
    puts `mysql --host=#{config[Rails.env]["host"]} --user=#{config[Rails.env]["username"]} --password="#{config[Rails.env]["password"]}" --database=#{config[Rails.env]["database"]} < #{Rails.root}/db/stored_procedures/update_balances_from_you_tube_records.sql`
  end

  def down
  end
end
