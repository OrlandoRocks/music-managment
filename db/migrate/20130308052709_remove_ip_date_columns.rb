class RemoveIpDateColumns < ActiveRecord::Migration[4.2]
  def self.up
    remove_column :muma_song_ips, :source_created_at
    remove_column :muma_song_ips, :source_updated_at
  end

  def self.down
    add_column :muma_song_ips, :source_created_at, :datetime
    add_column :muma_song_ips, :source_updated_at, :datetime
  end
end
