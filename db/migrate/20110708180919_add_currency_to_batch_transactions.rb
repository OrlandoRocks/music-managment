class AddCurrencyToBatchTransactions < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE batch_transactions
    ADD COLUMN currency CHAR(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency' AFTER amount")
  end

  def self.down
    remove_column :batch_transactions, :currency
  end
end
