class RemoveUsedColumnFromTargetedPeople < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      remove_column :targeted_people, :used, :integer
    end
  end
end
