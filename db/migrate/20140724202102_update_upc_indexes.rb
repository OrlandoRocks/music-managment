class UpdateUpcIndexes < ActiveRecord::Migration[4.2]
  def up
    sql = %Q{
      ALTER TABLE upcs
      DROP INDEX upc_upcable_id,
      ADD INDEX (upcable_id, upcable_type, tunecore_upc,inactive,number)
    }
    ActiveRecord::Base.connection.execute(sql)
  end

  def down
    sql = %Q{
      ALTER TABLE upcs
      DROP INDEX upcable_id,
      ADD INDEX upc_upcable_id (upcable_id)
    }
    ActiveRecord::Base.connection.execute(sql)
  end
end
