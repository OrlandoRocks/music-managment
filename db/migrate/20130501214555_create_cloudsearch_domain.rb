class CreateCloudsearchDomain < ActiveRecord::Migration[4.2]

  def self.up
    create_table :cloud_search_domain do |t|
      t.string :search_endpoint
      t.string :document_endpoint
    end

    sql = "insert into cloud_search_domain (search_endpoint, document_endpoint) values (?,?)"

    if Rails.env.production?
      sql = ActiveRecord::Base.send(:sanitize_sql_array, [sql, "search-sync-qw6pmy7v76nuktgpjl73shpuwy.us-east-1.cloudsearch.amazonaws.com", "doc-sync-qw6pmy7v76nuktgpjl73shpuwy.us-east-1.cloudsearch.amazonaws.com/2011-02-01/documents/batch"])
    elsif Rails.env.staging?
      sql = ActiveRecord::Base.send(:sanitize_sql_array, [sql, "search-sync-qw6pmy7v76nuktgpjl73shpuwy.us-east-1.cloudsearch.amazonaws.com", "doc-sync-qw6pmy7v76nuktgpjl73shpuwy.us-east-1.cloudsearch.amazonaws.com/2011-02-01/documents/batch"])
    else
      sql = ActiveRecord::Base.send(:sanitize_sql_array, [sql, "search-sync-development-tenfovcvddxcawgwsrjrgm65ja.us-east-1.cloudsearch.amazonaws.com", "doc-sync-development-tenfovcvddxcawgwsrjrgm65ja.us-east-1.cloudsearch.amazonaws.com/2011-02-01/documents/batch"])
    end

    ActiveRecord::Base.connection.execute(sql)

  end

  def self.down
    drop_table :cloud_search_domain
  end

end
