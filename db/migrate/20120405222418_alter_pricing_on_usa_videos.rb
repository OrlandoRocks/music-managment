class AlterPricingOnUsaVideos < ActiveRecord::Migration[4.2]
  def self.up
    execute("update  base_item_options set price = '649.00' where currency='USD' and name = 'Cost for 21 - 30 minutes' limit 1" )
    execute("update  base_item_options set price = '499.00' where currency='USD' and name = 'Cost for 11 - 20 minutes' limit 1" )
    execute("update  base_item_options set price = '399.00' where currency='USD' and name = 'Cost for 6 - 10 minutes' limit 1" )
    execute("update  base_item_options set price = '299.00' where currency='USD' and name = 'Cost for 0 - 5 minutes' limit 1" )
  end

  def self.down
    execute("update  base_item_options set price = '300.00' where currency='USD' and name = 'Cost for 21 - 30 minutes' limit 1" )
    execute("update  base_item_options set price = '220.00' where currency='USD' and name = 'Cost for 11 - 20 minutes' limit 1" )
    execute("update  base_item_options set price = '150.00' where currency='USD' and name = 'Cost for 6 - 10 minutes' limit 1" )
    execute("update  base_item_options set price = '85.00' where currency='USD' and name = 'Cost for 0 - 5 minutes' limit 1" )
  end
end
