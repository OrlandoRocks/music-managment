class AddTimedReleaseTimingScenarioToAlbums < ActiveRecord::Migration[4.2]
  def change
    add_column :albums, :timed_release_timing_scenario, :string
  end
end

