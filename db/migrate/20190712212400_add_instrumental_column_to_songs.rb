class AddInstrumentalColumnToSongs < ActiveRecord::Migration[4.2]
  def change
    add_column :songs, :instrumental, :boolean, default: false
  end
end
