class AddSlugToTier < ActiveRecord::Migration[6.0]
  def change
    add_column :tiers, :slug, :string
    add_index :tiers, :slug, unique: true
  end
end
