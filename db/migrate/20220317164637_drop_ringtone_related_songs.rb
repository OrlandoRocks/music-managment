class DropRingtoneRelatedSongs < ActiveRecord::Migration[6.0]
  def self.up
    drop_table :archive_ringtone_related_songs
  end

  def self.down
    create_table  :archive_ringtone_related_songs do |t|
      t.integer   :song_id
      t.integer   :album_id
    end
  end
end
