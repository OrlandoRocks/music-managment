class AddIndicesToExternalServiceIds < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      execute <<~SQL
        ALTER TABLE external_service_ids
        ADD INDEX external_service_ids_identifier_index(identifier), ALGORITHM=INPLACE, LOCK=NONE,
        ADD INDEX external_service_ids_service_name_index(service_name), ALGORITHM=INPLACE, LOCK=NONE;
      SQL
    end
  end
end
