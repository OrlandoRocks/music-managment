class AddOldHeaderFeature < ActiveRecord::Migration[4.2]
  def self.up
    Feature.find_or_create_by(:name=>'old_header', :restrict_by_user => 1)
  end

  def self.down
    feature = Feature.find_by(name: 'old_header')
    feature.destroy if !feature.nil?
  end
end
