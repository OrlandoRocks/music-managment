class AddColumnGracePeriodLengthToPersonSubscriptionStatus < ActiveRecord::Migration[4.2]
  def change
    add_column :person_subscription_statuses, :grace_period_length, :integer
  end
end
