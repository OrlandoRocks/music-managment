class RenameTaxInfosToTaxInfo < ActiveRecord::Migration[4.2]
  def self.up
    rename_table :tax_infos, :tax_info
  end

  def self.down
    rename_table :tax_info, :tax_infos
  end
end
