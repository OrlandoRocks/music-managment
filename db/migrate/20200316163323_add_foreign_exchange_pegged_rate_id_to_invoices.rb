class AddForeignExchangePeggedRateIdToInvoices < ActiveRecord::Migration[6.0]
  def change
    safety_assured { add_column :invoices, :foreign_exchange_pegged_rate_id, :integer }
  end
end
