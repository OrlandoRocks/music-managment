class AddPersonIntakeIdToSalesRecordSummaries < ActiveRecord::Migration[4.2]
  def up
    execute("alter table sales_record_summaries add person_intake_id int(10);")
  end

  def down
    execute("alter table sales_record_summaries drop person_intake_id;")
  end
end
