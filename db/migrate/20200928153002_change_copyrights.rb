class ChangeCopyrights < ActiveRecord::Migration[6.0]
  def change
    add_column :copyrights, :composition, :boolean
    add_column :copyrights, :recording, :boolean
  end
end
