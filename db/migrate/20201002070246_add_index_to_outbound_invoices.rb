class AddIndexToOutboundInvoices < ActiveRecord::Migration[6.0]
  def up
    add_index :outbound_invoices, [:related_id, :related_type]
  end

  def down
    remove_index :outbound_invoices, [:related_id, :related_type]
  end
end
