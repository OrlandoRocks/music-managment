class RenameXboxMusicToGroove < ActiveRecord::Migration[4.2]
  def up
    store = Store.find_by(short_name: 'Zune')
    if store
      store.update(:name => 'Groove')
    end
  end

  def down
    store = Store.find_by(short_name: 'Zune')
    if store
      store.update(:name => 'Xbox Music')
    end
  end
end
