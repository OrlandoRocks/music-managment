class AddSalepointableTypeIndexToSalepoints < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      execute <<~SQL
        ALTER TABLE salepoints
        ADD INDEX salepointable_type_index(salepointable_type), ALGORITHM=INPLACE, LOCK=NONE
      SQL
    end
  end
end
