class CreateRightsAppErrors < ActiveRecord::Migration[4.2]
  def change
    create_table :rights_app_errors do |t|
      t.string :api_endpoint, null: :false
      t.string :http_method, null: :false
      t.integer :http_status_code
      t.integer :requestable_id, null: :false
      t.string :requestable_type, null: :false
      t.text :json_request
      t.text :json_response
      t.timestamps
    end

    add_index :rights_app_errors, :http_method
    add_index :rights_app_errors, [:requestable_id, :requestable_type]
  end
end
