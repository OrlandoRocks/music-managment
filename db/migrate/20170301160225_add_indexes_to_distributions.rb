class AddIndexesToDistributions < ActiveRecord::Migration[4.2]
  def change
    p Time.now
    add_index :distributions, :state
    add_index :distributions, :converter_class
    p Time.now
  end
end
