class AddW8benPart2FieldToTaxInfo < ActiveRecord::Migration[4.2]
  def self.up
    add_column :tax_info, :claim_of_treaty_benefits, :text
  end

  def self.down
    remove_column :tax_info, :claim_of_treaty_benefits
  end
end
