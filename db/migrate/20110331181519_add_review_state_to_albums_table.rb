class AddReviewStateToAlbumsTable < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE albums
       ADD COLUMN `legal_review_state` enum('FLAGGED', 'REJECTED', 'APPROVED', 'NEEDS REVIEW', 'DO NOT REVIEW', 'NOT SURE') NOT NULL DEFAULT 'DO NOT REVIEW' COMMENT 'indicates content review states',
       ADD COLUMN `style_review_state` enum('FLAGGED', 'REJECTED', 'APPROVED', 'NEEDS REVIEW', 'DO NOT REVIEW', 'NOT SURE') NOT NULL DEFAULT 'DO NOT REVIEW' COMMENT 'indicates content review states'
      ")
    Album.reset_column_information
  end

  def self.down
    remove_column :albums, :legal_review_state
    remove_column :albums, :style_review_state
  end
end
