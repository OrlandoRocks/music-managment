class AddDisplayGroupIdToSipStores < ActiveRecord::Migration[4.2]
  def self.up
    up_sql=%Q(ALTER TABLE `sip_stores`
              ADD COLUMN `display_group_id` SMALLINT(5) UNSIGNED NULL)
    execute up_sql
  end

  def self.down
    remove_column :sip_stores, :display_group_id
  end
end
