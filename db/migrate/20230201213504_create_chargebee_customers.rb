class CreateChargebeeCustomers < ActiveRecord::Migration[6.0]
  def change
    create_table :chargebee_customers do |t|
      t.references :person, null: false, index: true
      t.string :chargebee_id, null: false, index: true

      t.timestamps
    end
  end
end
