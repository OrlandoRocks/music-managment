class CreateExternalPurchases < ActiveRecord::Migration[4.2][4.2]
  def change
    create_table :external_purchases do |t|
      t.references :person, null: false
      t.references :external_product

      t.integer :final_cents
      t.string :currency, limit: 3, null: false
      t.datetime :paid_at
      t.string :invoice_url
      t.integer :order_id

      t.timestamps
    end

    add_index :external_purchases, :person_id
    add_index :external_purchases, :external_product_id
    add_index :external_purchases, :paid_at
  end
end
