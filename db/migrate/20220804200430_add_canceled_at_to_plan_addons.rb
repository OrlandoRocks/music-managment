class AddCanceledAtToPlanAddons < ActiveRecord::Migration[6.0]
  def change
    add_column :plan_addons, :canceled_at, :datetime
  end
end
