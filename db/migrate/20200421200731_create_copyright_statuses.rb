class CreateCopyrightStatuses < ActiveRecord::Migration[6.0]
  def change
    create_table :copyright_statuses do |t|
      t.references :relatable, polymorphic: true
      t.string :status

      t.timestamps
    end

    add_index :copyright_statuses, [:relatable_id, :relatable_type]
  end
end
