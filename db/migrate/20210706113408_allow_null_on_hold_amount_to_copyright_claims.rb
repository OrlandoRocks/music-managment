class AllowNullOnHoldAmountToCopyrightClaims < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      change_column :copyright_claims, :hold_amount, :float, :null => true
    end
  end
end
