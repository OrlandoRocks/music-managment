class AddOriginalTransactionIdToPayuTransaction < ActiveRecord::Migration[6.0]
  def change
    add_column :payu_transactions, :original_transaction_id, :bigint
  end
end
