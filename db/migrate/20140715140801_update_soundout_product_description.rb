class UpdateSoundoutProductDescription < ActiveRecord::Migration[4.2]
  def up
    tracksmart_products = Product.where("name like 'TrackSmarts%'")
    tracksmart_products.each do |product|
      product.update(description: "Get music fan reviews and helpful analytics for your song.")
    end
  end

  def down
  end
end
