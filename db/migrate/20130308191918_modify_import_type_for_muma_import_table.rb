class ModifyImportTypeForMumaImportTable < ActiveRecord::Migration[4.2]
  def self.up
    execute 'ALTER TABLE muma_imports CHANGE type import_type varchar(10);'
  end

  def self.down
    execute 'ALTER TABLE muma_imports CHANGE import_type type varchar(255);'
  end
  
end
