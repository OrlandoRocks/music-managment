class AddThreeDSecureInfoToBraintreeTransactions < ActiveRecord::Migration[6.0]
  def up
    add_column :braintree_transactions, :liability_shifted, :boolean
    add_column :braintree_transactions, :liability_shifted_at, :datetime
    add_column :braintree_transactions, :ds_transaction_id, :string
  end

  def down
    remove_column :braintree_transactions, :liability_shifted
    remove_column :braintree_transactions, :liability_shifted_at
    remove_column :braintree_transactions, :ds_transaction_id
  end
end
