class AddCoverSongToNonTunecoreSongs < ActiveRecord::Migration[6.0]
  def change
    add_column :non_tunecore_songs, :cover_song, :boolean
  end
end
