class CreateInvoiceStaticCustomerAddresses < ActiveRecord::Migration[6.0]
  def change
    create_table :invoice_static_customer_addresses do |t|
      t.integer :related_id, null: false
      t.string :related_type, null: false
      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.string :country
      t.string :zip

      t.timestamps
    end
  end
end
