class CreateExternalServicesPeople < ActiveRecord::Migration[4.2]
  def self.up
    create_table :external_services_people do |t|
      t.integer   :person_id
      t.integer   :external_service_id
      t.string    :account_id
    end

    add_index :external_services_people, :person_id
    add_index :external_services_people, :external_service_id
  end

  def self.down
    remove_index :external_services_people, :person_id
    remove_index :external_services_people, :external_service_id

    drop_table :external_services_people
  end
end
