class SeedPublishingTable < ActiveRecord::Migration[4.2]
  def self.up
    Publisher.create(:name => "TUNECORE PUBLISHING", :pro_affiliation => "ASCAP", :cae => '641638348')
    Publisher.create(:name => "TUNECORE DIGITAL MUSIC", :pro_affiliation => "BMI", :cae => '642005099')
    Publisher.create(:name => "TUNECORE SONGS", :pro_affiliation => "SESAC", :cae => '641080872')
  end

  def self.down
    Publisher.where(:name => "TUNECORE PUBLISHING", :pro_affiliation => "ASCAP", :cae => '641638348').first.delete
    Publisher.where(:name => "TUNECORE DIGITAL MUSIC", :pro_affiliation => "BMI", :cae => '642005099').first.delete
    Publisher.where(:name => "TUNECORE SONGS", :pro_affiliation => "SESAC", :cae => '641080872').first.delete
  end
end
