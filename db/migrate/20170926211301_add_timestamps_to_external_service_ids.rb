class AddTimestampsToExternalServiceIds < ActiveRecord::Migration[4.2]
  def change
    add_column :external_service_ids, :created_at, :timestamp
    add_column :external_service_ids, :updated_at, :timestamp
  end
end
