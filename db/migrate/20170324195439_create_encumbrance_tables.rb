class CreateEncumbranceTables < ActiveRecord::Migration[4.2]
  def up
    create_table  :encumbrance_summaries do |t|
      t.integer   :person_id, null: false
      t.string    :reference_id
      t.string    :reference_source, null: false
      t.decimal   :total_amount, precision: 12, scale: 2, null: false
      t.decimal   :fee_amount, precision: 12, scale: 2, null: false
      t.decimal   :outstanding_amount, precision: 12, scale: 2, null: false
      t.timestamps
    end

    add_index :encumbrance_summaries, [ :reference_id, :reference_source ]
    add_index :encumbrance_summaries, :person_id

    create_table  :encumbrance_details do |t|
      t.integer   :encumbrance_summary_id, null: false
      t.date      :transaction_date, null: false
      t.decimal   :transaction_amount, precision: 12, scale: 2, null: false
      t.timestamps
    end

    add_index :encumbrance_details, :encumbrance_summary_id

    create_table  :encumbrance_options do |t|
      t.string    :reference_source, null: false
      t.string    :option_name, null: false
      t.string    :option_value, null: false
      t.timestamps
    end

    add_index :encumbrance_options, [ :reference_source, :option_name ]

    EncumbranceOption.create!(reference_source: "LYRIC", option_name: "WITHHOLDING_MAX_PERCENTAGE", option_value: 100)

    db = ActiveRecord::Base.connection_config
    `mysql #{db[:database]} -h #{db[:host]} --user=#{db[:username]} --password=#{db[:password]} < #{File.join(Rails.root, "db", "stored_procedures", "withhold_encumbrances.sql")}`
    `mysql #{db[:database]} -h #{db[:host]} --user=#{db[:username]} --password=#{db[:password]} < #{File.join(Rails.root, "db", "stored_procedures", "create_person_intake.sql")}`

    ActiveRecord::Base.connection.execute("ALTER TABLE person_transactions MODIFY target_type VARCHAR(255)")
  end

  def down
    drop_table  :encumbrance_options
    drop_table  :encumbrance_details
    drop_table  :encumbrance_summaries
  end
end
