class CreateTargetedProductStore < ActiveRecord::Migration[6.0]
  def change
    create_table :targeted_product_stores do |t|
      t.integer :targeted_product_id
      t.integer :store_id

      t.timestamps
    end

    add_index :targeted_product_stores, [:targeted_product_id, :store_id], name: "targeted_product_store"
    add_index :targeted_product_stores, :store_id
  end
end
