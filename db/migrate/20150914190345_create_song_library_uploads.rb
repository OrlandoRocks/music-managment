class CreateSongLibraryUploads < ActiveRecord::Migration[4.2]
  def up
    create_table  :song_library_uploads do |t|
      t.integer   :person_id, null: false
      t.string    :song_title, null: false
      t.string    :artist_name, null: false
      t.integer   :genre_id, null: false
      t.string    :bucket_name, null: false
      t.string    :asset_key, null: false
      t.timestamps
    end 
  
    # must alter data type to match person_id data type
    execute "ALTER TABLE song_library_uploads CHANGE person_id person_id INT(10) UNSIGNED NOT NULL"
    execute "ALTER TABLE song_library_uploads ADD CONSTRAINT fk_song_library_uploads_person_id FOREIGN KEY (person_id) references people(id)"
    execute "ALTER TABLE song_library_uploads ADD CONSTRAINT fk_song_library_uploads_genre_id FOREIGN KEY (genre_id) references genres(id)"
  end
  
  def down
    drop_table :song_library_uploads
  end
end
