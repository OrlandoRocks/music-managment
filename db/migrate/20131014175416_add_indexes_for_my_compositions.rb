class AddIndexesForMyCompositions < ActiveRecord::Migration[4.2]
  def self.up
    execute("alter table muma_songs
    add index index_muma_songs_on_isrc(isrc(12))")
    
    execute("alter table counterpoint_songs
    add index index_cs_on_code(code),
    add index index_cs_on_status(status)")
  end

  def self.down
    execute("alter table muma_songs
    drop index index_muma_songs_on_isrc")
    
    execute("alter table counterpoint_songs
    drop index index_cs_on_code,
    drop index index_cs_on_status")
  end
end
