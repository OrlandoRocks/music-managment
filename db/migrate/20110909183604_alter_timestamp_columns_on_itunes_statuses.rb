class AlterTimestampColumnsOnItunesStatuses < ActiveRecord::Migration[4.2]
  def self.up
    rename_column :itunes_statuses, :updated_at, :updated_on
    rename_column :itunes_statuses, :created_at, :created_on
    rename_column :itunes_statuses, :prev_status_updated_at, :prev_status_updated_on
    change_column :itunes_statuses, :updated_on, :date
    change_column :itunes_statuses, :created_on, :date
    change_column :itunes_statuses, :prev_status_updated_on, :date
  end

  def self.down
    change_column :itunes_statuses, :updated_on, :datetime
    change_column :itunes_statuses, :created_on, :datetime
    change_column :itunes_statuses, :prev_status_updated_on, :datetime
    rename_column :itunes_statuses, :updated_on, :updated_at
    rename_column :itunes_statuses, :created_on, :created_at
    rename_column :itunes_statuses, :prev_status_updated_on, :prev_status_updated_at
  end
end
