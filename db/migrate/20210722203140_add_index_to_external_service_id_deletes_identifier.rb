class AddIndexToExternalServiceIdDeletesIdentifier < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      execute <<~SQL
        ALTER TABLE external_service_id_deletes
        ADD INDEX external_service_id_deletes_identifier_index(identifier), ALGORITHM=INPLACE, LOCK=NONE;
      SQL
    end
  end
end
