class CreateCopyrightDocuments < ActiveRecord::Migration[6.0]
  def change
    create_table :copyright_documents do |t|
      t.references :relatable, polymorphic: true
      t.column :s3_asset_id, :integer

      t.timestamps
    end

    add_index :copyright_documents, [:relatable_id, :relatable_type]
  end
end
