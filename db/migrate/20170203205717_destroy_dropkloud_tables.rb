class DestroyDropkloudTables < ActiveRecord::Migration[4.2]
  def up
    dropkoud_purchases            = execute("select count(*) from dropkloud_purchases").first.first
    dropkloud_products            = execute("select count(*) from dropkloud_products").first.first
    dropkloud_subscription_events = execute("select count(*) from dropkloud_subscription_events").first.first

    if SubscriptionPurchase.all.count == dropkoud_purchases && SubscriptionProduct.all.count == dropkloud_products && SubscriptionEvent.all.count == dropkloud_subscription_events
      drop_table :dropkloud_purchases
      drop_table :dropkloud_products
      drop_table :dropkloud_subscription_events
    end
  end

  def down
  end
end
