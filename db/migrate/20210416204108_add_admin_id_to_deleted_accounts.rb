class AddAdminIdToDeletedAccounts < ActiveRecord::Migration[6.0]
  def change
    safety_assured do
      add_reference :deleted_accounts, :admin, foreign_key: { to_table: :people }, type: 'int(11) unsigned'
    end
  end
end
