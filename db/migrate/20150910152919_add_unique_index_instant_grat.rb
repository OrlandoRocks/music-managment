class AddUniqueIndexInstantGrat < ActiveRecord::Migration[4.2]
  def change
    PreorderInstantGratSong.joins("inner join (select pigs.*, count(*) from preorder_instant_grat_songs pigs
                                   group by song_id, salepoint_preorder_data_id
                                   having count(*) > 1) dup_pigs on dup_pigs.song_id = preorder_instant_grat_songs.song_id and
                                   dup_pigs.salepoint_preorder_data_id = preorder_instant_grat_songs.salepoint_preorder_data_id")
                           .where("preorder_instant_grat_songs.id != dup_pigs.id").destroy_all
    add_index :preorder_instant_grat_songs, [ :song_id, :salepoint_preorder_data_id ], unique: true, name: 'index_pigs_on_spd_id_and_song_id'
  end
end
