class CreateTableBlacklistedPaypalPayees < ActiveRecord::Migration[4.2]
  def up
    create_table :blacklisted_paypal_payees do |t|
      t.string :payee
    end
  end

  def down
    drop_table :blacklisted_paypal_payees
  end
end
