class AddJobIdToApiLoggers < ActiveRecord::Migration[6.0]
  def change
    add_column :api_loggers, :job_id, :string
  end
end
