class CreateCountryVariablePrices < ActiveRecord::Migration[4.2]
  def change
    create_table :country_variable_prices do |t|
      t.integer :variable_price_id
      t.integer :country_website_id
      t.decimal :wholesale_price, precision: 6, scale: 2
      t.string :currency
      t.timestamps
    end

    add_index :country_variable_prices, :variable_price_id
    add_index :country_variable_prices, :country_website_id

    vps = VariablePrice.find_by_sql("select vp.* from variable_prices vp
                                     inner join variable_prices_stores vps on vp.id = vps.variable_price_id
                                     where vps.store_id = 36")

    countries = CountryWebsite.all

    wholesale_price_hash = wholesale_price_mapping
    countries.each do |country_website|
      price_hash = wholesale_price_hash[country_website.country]
      vps.each do |vp|
        CountryVariablePrice.create(variable_price: vp, country_website: country_website, wholesale_price: price_hash[vp.price.to_f], currency: country_website.currency)
      end
    end
  end

  def wholesale_price_mapping
    uk_price_hash = {
      0.69 => 0.40,
      0.99 => 0.57,
      1.29 => 0.72,
      1.99 => 0.80,
      2.99 => 1.07,
      3.99 => 1.35,
      4.99 => 1.62,
      5.99 => 2.15,
      6.99 => 2.70,
      7.99 => 3.25,
      8.99 => 3.75,
      9.99 => 4.30,
      10.99 => 4.85,
      11.99 => 5.95,
      12.99 => 6.45,
      13.99 => 7.00,
      14.99 => 7.55,
      15.99 => 8.10
    }

    au_price_hash = {
      0.69 => 0.69,
      0.99 => 0.99,
      1.29 => 1.29,
      1.99 => 1.98,
      2.99 => 2.99,
      3.99 => 3.99,
      4.99 => 4.99,
      5.99 => 5.99,
      6.99 => 6.99,
      7.99 => 7.99,
      8.99 => 8.99,
      9.99 => 9.99,
      10.99 => 10.99,
      11.99 => 11.99,
      12.99 => 12.99,
      13.99 => 13.99,
      14.99 => 14.99,
      15.99 => 15.99
    }

    us_price_hash = {}
    ca_price_hash = {}

    { "UK" => uk_price_hash, "AU" => au_price_hash, "US" => us_price_hash, "CA" => ca_price_hash }
  end
end
