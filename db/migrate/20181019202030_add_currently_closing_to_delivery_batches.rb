class AddCurrentlyClosingToDeliveryBatches < ActiveRecord::Migration[4.2]
  def change
    add_column :delivery_batches, :currently_closing, :boolean, default: false
  end
end
