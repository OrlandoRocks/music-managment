class AddDistributableClassToStoreDeliveryConfigs < ActiveRecord::Migration[4.2]
  def change
    add_column :store_delivery_configs, :distributable_class, :string, default: "Distribution"
  end
end
