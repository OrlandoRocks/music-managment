class CreateEmailChangeRequests < ActiveRecord::Migration[6.0]
  def change
    create_table :email_change_requests do |t|
      t.references :person, null: false
      t.string :token, null: false
      t.string :old_email, null: false
      t.string :new_email, null: false
      t.boolean :finalized, null: false, default: false
      t.timestamps
    end
  end
end
