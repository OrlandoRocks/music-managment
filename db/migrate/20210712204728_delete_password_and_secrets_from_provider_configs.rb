class DeletePasswordAndSecretsFromProviderConfigs < ActiveRecord::Migration[6.0]
  def change
    safety_assured {
      remove_column :payin_provider_configs, :secrets
      remove_column :payout_provider_configs, :password
    }
  end
end
