class CreateCmsSetAttributes < ActiveRecord::Migration[4.2]
  def change
    create_table :cms_set_attributes do |t|
      t.integer :cms_set_id, null: false
      t.string :attr_name, null: false
      t.string :attr_value
      t.text :attr_text
      t.timestamps
    end
    
    add_index :cms_set_attributes, :cms_set_id
  end
end
