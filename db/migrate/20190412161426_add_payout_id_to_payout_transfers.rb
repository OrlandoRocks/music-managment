class AddPayoutIdToPayoutTransfers < ActiveRecord::Migration[4.2]
  def change
    add_column :payout_transfers, :payout_id, :string
  end
end
