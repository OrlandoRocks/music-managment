class AddVoidTypeToActionColumnOnPaypalTransactions < ActiveRecord::Migration[4.2]
  def self.up
    execute %Q(
      ALTER TABLE paypal_transactions
      CHANGE action action enum('Sale', 'Authorization', 'Refund', 'Void') DEFAULT NULL COMMENT 'type of transaction'
    )
  end

  def self.down
    execute %Q(
      ALTER TABLE paypal_transactions
      CHANGE action action enum('Sale', 'Authorization', 'Refund') DEFAULT NULL COMMENT 'type of transaction'
    )
  end
end
