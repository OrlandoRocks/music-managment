class CreateTargetedNotifications < ActiveRecord::Migration[4.2]
  def change
    create_table :targeted_notifications do |t|
      t.integer :person_id
      t.integer :notification_icon_id
      t.text :text
      t.text :title
      t.string :url
      t.string :link_text

      t.boolean :global

      t.timestamps
    end
    add_index :targeted_notifications, :person_id
    add_index :targeted_notifications, :notification_icon_id
  end
end
