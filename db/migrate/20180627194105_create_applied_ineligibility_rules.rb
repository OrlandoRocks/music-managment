class CreateAppliedIneligibilityRules < ActiveRecord::Migration[4.2]
  def change
    create_table :applied_ineligibility_rules do |t|
      t.belongs_to :ineligibility_rule
      t.references :vetted_item, polymorphic: true

      t.timestamps
    end
    add_index :applied_ineligibility_rules, :ineligibility_rule_id
    add_index :applied_ineligibility_rules, [:vetted_item_id, :vetted_item_type], name: :index_applied_ineligibility_rules_on_item_id_and_item_type
  end
end

