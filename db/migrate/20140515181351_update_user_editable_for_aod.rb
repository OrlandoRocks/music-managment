class UpdateUserEditableForAod < ActiveRecord::Migration[4.2]
  def up
    amazon_od_store = Store.find_by(abbrev: "aod_us")
    if amazon_od_store
      variable_prices = VariablePrice.where(store_id: amazon_od_store.id)
      variable_prices.each do |vp|
        vp.user_editable = true
        vp.save!
      end
    end
  end

  def down
  end
end
