class AddAppleRoleToPerson < ActiveRecord::Migration[4.2]
  def change
    add_column :people, :apple_role, :string
  end
end
