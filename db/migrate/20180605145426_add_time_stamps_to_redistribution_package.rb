class AddTimeStampsToRedistributionPackage < ActiveRecord::Migration[4.2]
  def change
    add_column :redistribution_packages, :created_at, :datetime, null: false
    add_column :redistribution_packages, :updated_at, :datetime, null: false
  end
end
