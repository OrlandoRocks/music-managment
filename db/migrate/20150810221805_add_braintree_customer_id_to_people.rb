class AddBraintreeCustomerIdToPeople < ActiveRecord::Migration[4.2]
  def change
    add_column :people, :braintree_customer_id, :integer
  end
end
