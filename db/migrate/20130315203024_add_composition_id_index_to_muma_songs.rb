class AddCompositionIdIndexToMumaSongs < ActiveRecord::Migration[4.2]
  def self.up
    add_index :muma_songs, :composition_id
  end

  def self.down
    remove_index :muma_songs, :composition_id
  end
end
