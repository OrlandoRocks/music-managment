class AddKkboxPageFeature < ActiveRecord::Migration[4.2]
  def up
    Feature.find_or_create_by(:name=>'kkbox_page', :restrict_by_user => 1)
  end

  def down
    feature = Feature.find_by(name: 'kkbox_page')
    feature.destroy if !feature.nil?
  end
end
