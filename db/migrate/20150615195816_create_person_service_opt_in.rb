class CreatePersonServiceOptIn < ActiveRecord::Migration[4.2]
  def up
    create_table  :person_service_opt_ins do |t|
      t.integer   :person_id, null: false
      t.string    :service_identifier
      t.datetime  :opt_in_at
      t.timestamps
    end
  end

  def down
  end
end
