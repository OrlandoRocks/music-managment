class ChangePeopleInactiveToDormant < ActiveRecord::Migration[4.2]
  def change
    safety_assured { rename_column :people, :inactive, :dormant }
  end
end
