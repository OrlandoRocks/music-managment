class AddIndexOnUpcTypeToUpcs < ActiveRecord::Migration[4.2]
  def change
    add_index :upcs, :upc_type
  end
end
