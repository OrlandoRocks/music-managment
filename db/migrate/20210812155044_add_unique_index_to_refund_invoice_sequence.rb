class AddUniqueIndexToRefundInvoiceSequence < ActiveRecord::Migration[6.0]
  def change
    add_index :refunds, %i[corporate_entity_id invoice_sequence], unique: true
  end
end
