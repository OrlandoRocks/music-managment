class AddCurrencyAndCountryWebsiteToProducts < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE products ADD currency CHAR(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency for invoice item' AFTER price");
    execute("ALTER TABLE products ADD country_website_id int(11) unsigned DEFAULT 1 COMMENT 'Foreign key to country_websites' AFTER created_by_id");
  end

  def self.down
    remove_column :products, :currency
    remove_column :products, :country_website_id
  end
end
