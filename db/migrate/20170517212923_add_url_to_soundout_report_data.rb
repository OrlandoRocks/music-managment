class AddUrlToSoundoutReportData < ActiveRecord::Migration[4.2]
  def change
    add_column :soundout_report_data, :url, :text
  end
end
