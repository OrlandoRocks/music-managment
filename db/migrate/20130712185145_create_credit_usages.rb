class CreateCreditUsages < ActiveRecord::Migration[4.2]
  def self.up
    create_table :credit_usages do |t|
      t.integer       :person_id,       :null => false
      t.integer       :related_id,      :null => false
      t.string        :related_type,    :null => false
      t.string        :applies_to_type, :null => false
      t.datetime      :finalized_at
      t.timestamps
    end

    add_index :credit_usages, :person_id
    add_index :credit_usages, [:related_type, :related_id]
  end

  def self.down
    drop_table :credit_usages
  end
end
