class AddWimpToSalepointableStores < ActiveRecord::Migration[4.2]
  def self.up
    if store = Store.find_by(short_name: "Wimp")
      execute ("insert into salepointable_stores (salepointable_type,store_id) values ('Album','#{store.id}')")
      execute ("insert into salepointable_stores (salepointable_type,store_id) values ('Single','#{store.id}')")
    else
      puts "Couldn't find store by the short name of Wimp"
    end
  end

  def self.down
    if store = Store.find_by(short_name: "Wimp")
      execute("delete from salepointable_stores where store_id = #{store.id}")
    else
      puts "Couldn't find store by the short name of Wimp"
    end
  end

end

