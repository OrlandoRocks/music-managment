class PopulateRejectionEmailsWithData < ActiveRecord::Migration[4.2]
  def up
    reason_subject_mapping = {
        album_not_sure_maybe_block_from_sales: "Rights Verification",
        album_blocked_from_sales_cover_song_formatting: "Cover Song Formatting - Action Required",
        denied_ringtone_albums_if_itunes_is_selected: "Unable to distribute Ringtone Album to iTunes",
        denied_audiobooks_if_itunes_is_selected: "Unable to distribute Audiobook Album to iTunes",
        denied_album_high_profile_features: "Rights verification",
        denied_album_unauthorized_artwork: "Replacement artwork required",
        denied_ringtone_submitted_textone: "Unable to distribute text tones",
        denied_karaoke_ringtone: "release changes required",
        denied_album_unauthorized_remix: "Content removed",
        denied_album_possibly_using_masters: "Content removed",
        denied_album_unauthorized_samples_replace_not_OK: "Content removed",
        denied_album_unauthorized_samples_replace_OK: "Content removed",
        denied_album_covers_before_original_release_date: "Covers/Karaoke Versions Before Original Release Date",
        denied_album_artist_name_additional_artist_info: "release changes required",
        denied_album_titles_producer_credit: "update required",
        denied_album_artwork_cover_metadata_mismatch: "release changes required",
        denied_album_artwork_extra_info: "artwork update required",
        denied_album_artwork_contact_info: "release changes required",
        denied_album_artwork_store_logos: "release changes required",
        denied_album_artwork_reference_physical_packaging: "release changes required",
        denied_album_artist_name_too_generic: "update required",
        denied_album_dashes_spaces_underscores: "release changes required",
        denied_album_artwork_quality: "Artwork update required",
        denied_album_unsupported_language: "important information regarding your release",
        denied_album_classical_album: "important information regarding your release",
        denied_album_remove_iTunes_iPhone_references: "release changes required",
        denied_album_extra_info_associated_bands_instruments_members: "release changes required",
        denied_album_aka_or_alternate_artist_name: "release changes required",
        denied_album_reference_in_title: "release changes required",
        denied_album_trademark_in_artist_field: "release changes required",
        denied_album_subliminal_binaural: "release changes required",
        denied_album_artist_name_misleading_lyrics_original: "release changes required",
        denied_album_artist_name_incorrect_features: "release changes required",
        denied_streaming_fraud: "Important Information",
        denied_unnecessary_title: "release changes required",
        denied_update_artist_name: "release changes required",
        denied_live_formatting: "release changes required",
        denied_soundtrack_formatting: "release changes required",
        denied_duplicate_titles: "update required",
        denied_translations: "update required",
        denied_incorrect_parental_logo: "Artwork issue"
    }

    ['album_blocked_from_sales_cover_song_formatting', 'album_blocked_from_sales', 'album_not_sure_maybe_block_from_sales', 'album_will_not_be_distributed', 'denied_album_aka_or_alternate_artist_name', 'denied_album_artist_name_additional_artist_info', 'denied_album_artist_name_incorrect_features', 'denied_album_artist_name_misleading_lyrics_original', 'denied_album_artist_name_too_generic', 'denied_album_artwork_extra_info', 'denied_album_artwork_contact_info', 'denied_album_artwork_cover_metadata_mismatch', 'denied_album_artwork_quality', 'denied_album_artwork_reference_physical_packaging', 'denied_album_artwork_store_logos', 'denied_album_classical_album', 'denied_album_covers_before_original_release_date', 'denied_album_dashes_spaces_underscores', 'denied_album_extra_info_associated_bands_instruments_members', 'denied_album_high_profile_features', 'denied_album_possibly_using_masters', 'denied_album_remove_iTunes_iPhone_references', 'denied_album_titles_producer_credit', 'denied_album_unauthorized_artwork', 'denied_album_unauthorized_remix', 'denied_album_unauthorized_samples_replace_not_OK', 'denied_album_unauthorized_samples_replace_OK', 'denied_album_unsupported_language', 'denied_audiobooks_if_itunes_is_selected', 'denied_ringtone_albums_if_itunes_is_selected', 'denied_ringtone_submitted_textone', 'denied_album_reference_in_title', 'denied_album_trademark_in_artist_field', 'denied_album_subliminal_binaural', 'denied_streaming_fraud', 'denied_unnecessary_title', 'denied_update_artist_name', 'denied_live_formatting', 'denied_soundtrack_formatting', 'denied_duplicate_titles', 'denied_translations', 'denied_incorrect_parental_logo'].each do |email|
      file = File.open(Rails.root.to_s + "/app/views/person_notifier/#{email}.text.erb", "r")
      RejectionEmail.create(name: email, subject: reason_subject_mapping.fetch(email.to_sym, "Content Removed"), body: file.read)
      file.close
    end

    ReviewReason.all.each do |reason|
      reason.update(rejection_email_id: RejectionEmail.find_by(name: reason.email_template_path).id)
    end
  end

  def down
    RejectionEmail.all.each do |email|
      email.destroy
    end

    ReviewReason.all.each do |reason|
      reason.update(rejection_email_id: nil)
    end
  end
end
