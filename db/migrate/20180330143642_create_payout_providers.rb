class CreatePayoutProviders < ActiveRecord::Migration[4.2]
  def change
    create_table :payout_providers do |t|
      t.belongs_to :person
      t.string :provider_status
      t.string :tunecore_status
      t.string :client_payee_id
      t.string :registered_email
      t.datetime :provider_processed_at
      t.string :name

      t.timestamps
    end
    add_index :payout_providers, :person_id
    add_index :payout_providers, :client_payee_id
  end
end
