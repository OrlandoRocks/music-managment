class CreateSalesRecordSummaries < ActiveRecord::Migration[4.2]
  def up
    sql = %Q{
      CREATE TABLE `sales_record_summaries` (
        `person_id` int(10) unsigned NOT NULL,
        `sales_record_master_id` int(10) unsigned NOT NULL,
        `related_type` enum('Album','Song','Video') NOT NULL,
        `related_id` int(10) NOT NULL,
        `release_type` enum('Album','Single','Ringtone','Video') DEFAULT NULL,
        `release_id` int(10) DEFAULT NULL,
        `downloads_sold` int(10) DEFAULT '0',
        `streams_sold` int(10) DEFAULT '0',
        `download_amount` decimal(15,6) DEFAULT '0.000000',
        `stream_amount` decimal(15,6) DEFAULT '0.000000',
        PRIMARY KEY (`person_id`,`sales_record_master_id`,`related_type`,`related_id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    }
    puts "Running: #{sql}"
    ActiveRecord::Base.connection.execute(sql)

    sql = %Q{
      alter table sales_record_masters  add column summarized tinyint(1) unsigned not null default 0;
    }
    puts "Running: #{sql}"
    ActiveRecord::Base.connection.execute(sql)

    sql = %Q{
    CREATE PROCEDURE `populate_sales_record_summaries`()
    BEGIN

      DECLARE done BOOLEAN DEFAULT 0;
      DECLARE var_srm_id int;

      DECLARE cur1 cursor for
      SELECT srm.id from sales_record_masters srm where summarized = 0;

      DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

      OPEN cur1;
      FETCH cur1 into var_srm_id;
      WHILE( !done ) DO

        select NOW();	
        select var_srm_id;

        START TRANSACTION;
          INSERT into sales_record_summaries
          SELECT person_id, sales_record_master_id, related_type, related_id,
            IF( related_type = 'Video', 'Video', IF( related_type = 'Song', (select album_type from songs s inner join albums a on a.id = s.album_id where s.id = related_id), (select album_type from albums where albums.id = related_id))),
            IF( related_type = 'Video', related_id, if( related_type = 'Album', related_id, (select album_id from songs where songs.id = related_id))),
            SUM(IF(distribution_type='Download',quantity,0)), SUM(IF(distribution_type='Streaming',quantity,0)) AS streams_sold,
            SUM(IF(distribution_type='Download',amount,0)),
            SUM(IF(distribution_type='Streaming',amount,0))
          FROM sales_records
          WHERE sales_records.sales_record_master_id = var_srm_id
          GROUP BY person_id, related_type, related_id, sales_record_master_id;

          IF( select ROW_COUNT() > 0 ) 	THEN
            UPDATE sales_record_masters srm set srm.summarized = 1 where srm.id = var_srm_id;
          END IF;

        COMMIT;

        FETCH cur1 into var_srm_id;

      END WHILE;
      CLOSE cur1;

      END;;
    }
    puts "Creating stored procedure to populate table: #{sql}"
    ActiveRecord::Base.connection.execute(sql)
  end

  def down
    ActiveRecord::Base.connection.execute("drop table if exists sales_record_summaries")
    ActiveRecord::Base.connection.execute("drop procedure if exists populate_sales_record_summaries")
    ActiveRecord::Base.connection.execute("alter table sales_record_masters drop column summarized")
  end
end
