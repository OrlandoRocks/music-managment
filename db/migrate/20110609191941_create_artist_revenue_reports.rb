class CreateArtistRevenueReports < ActiveRecord::Migration[4.2]
  def self.up
    execute %Q(CREATE TABLE artist_revenue_reports (
      id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    	artist_name varchar(120),
    	artist_id int(10),
    	revenue decimal(23,14),
    	albums_sold int(10),
    	songs_sold int(10),
    	streams_sold int(10),
    	number_of_releases int(10),
    	srm_month date,
    	PRIMARY KEY (id)
    )  ENGINE=InnoDB DEFAULT CHARSET=utf8;)
  end
    
  def self.down
    drop_table :artist_revenue_reports
  end
end
