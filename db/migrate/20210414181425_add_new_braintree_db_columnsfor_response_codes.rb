class AddNewBraintreeDbColumnsforResponseCodes < ActiveRecord::Migration[6.0]
  def change
    add_column :braintree_transactions, :response_message, :string, null: true
    add_column :braintree_transactions, :transaction_status, :string, null: true
    add_column :braintree_transactions, :processor_response_text, :string, null: true
    add_column :braintree_transactions, :processor_response_type, :string, null: true
    add_column :braintree_transactions, :additional_processor_response, :string, null: true
    add_column :braintree_transactions, :network_response_text, :string, null: true
  end
end
