class CreateCloudSearchBatchDate < ActiveRecord::Migration[4.2]
  def self.up
    create_table :cloud_search_batch_date do |t|
      t.datetime :last_add_date
      t.datetime :last_delete_date
    end

    ActiveRecord::Base.connection.execute("insert into cloud_search_batch_date(last_add_date, last_delete_date) values (NULL, NULL)")
  end

  def self.down
    drop_table :cloud_search_batch_date
  end
end
