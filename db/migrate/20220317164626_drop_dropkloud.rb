class DropDropkloud < ActiveRecord::Migration[6.0]
  def self.up
    drop_table :archive_dropkloud_products
    drop_table :archive_dropkloud_purchases
    drop_table :archive_dropkloud_subscription_events
  end

  def self.down
    create_table  :archive_dropkloud_products do |t|
      t.string    :product_name
      t.string    :product_type
      t.integer   :product_id
      t.integer   :term_length
      t.timestamps
    end

    create_table :archive_dropkloud_purchases do |t|
      t.integer  :person_id, null: false
      t.integer  :archive_dropkloud_product_id, null: false
      t.datetime :effective_date
      t.datetime :termination_date
      t.timestamps
    end

    create_table :archive_dropkloud_subscription_events do |t|
      t.integer :archive_dropkloud_purchase_id
      t.string :event_type, null: false
      t.integer :person_subscription_status_id, null: false
      t.timestamps
    end

    add_index :archive_dropkloud_subscription_events, :archive_dropkloud_purchase_id,
              name: "index_arc_dropkloud_sub_events_on_arc_dropkloud_purchase_ids"
    add_index :archive_dropkloud_subscription_events, :person_subscription_status_id,
              name: "index_dropkloud_sub_events_onperson_sub_status_id"
  end
end
