class UpdateDefaultValueForPurchasesDiscountReason < ActiveRecord::Migration[6.0]
  def up
    safety_assured do
      change_column :purchases, :discount_reason, :string, null: true, default: "no discount"
    end
  end

  def down
    safety_assured do
      change_column :purchases, :discount_reason, :string, null: true
    end
  end
end
