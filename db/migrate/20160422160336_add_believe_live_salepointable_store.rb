class AddBelieveLiveSalepointableStore < ActiveRecord::Migration[4.2]
  def up
    SalepointableStore.create(salepointable_type: "Album", store_id: 75)
    SalepointableStore.create(salepointable_type: "Single", store_id: 75)
    SalepointableStore.create(salepointable_type: "Ringtone", store_id: 75)
  end

  def down
    SalepointableStore.where(salepointable_type: "Album", store_id: 75).destroy
    SalepointableStore.where(salepointable_type: "Single", store_id: 75).destroy
    SalepointableStore.where(salepointable_type: "Ringtone", store_id: 75).destroy
  end
end
