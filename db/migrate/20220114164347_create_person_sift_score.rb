class CreatePersonSiftScore < ActiveRecord::Migration[6.0]
  def change
    create_table :person_sift_score do |t|
      t.references :person

      t.integer :score, null: false, :limit => 1
      t.timestamps
    end
  end
end
