class CreatePersonSubscriptionStatuses < ActiveRecord::Migration[4.2]
  def change
    create_table :person_subscription_statuses do |t|
      t.integer :person_id, null: false
      t.datetime :effective_date
      t.datetime :termination_date
      t.datetime :canceled_at
      t.string :subscription_type, null: false
      t.timestamps
    end

    add_index :person_subscription_statuses, :person_id
  end
end
