class AddBalanceAdjustmentCategoryRefToMassAdjustmentBatch < ActiveRecord::Migration[6.0]
  def change
    add_reference :mass_adjustment_batches, :balance_adjustment_category, foreign_key: true
  end
end
