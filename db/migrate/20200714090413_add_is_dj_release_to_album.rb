class AddIsDjReleaseToAlbum < ActiveRecord::Migration[6.0]
  def up
    add_column :albums, :is_dj_release, :boolean
    change_column_default :albums, :is_dj_release, false
  end

  def down
    remove_column :albums, :is_dj_release
  end
end
