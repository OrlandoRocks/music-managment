class CreateDistributionApiSongsTable < ActiveRecord::Migration[6.0]
  def up
    create_table :distribution_api_songs do |t|
      t.references :distribution_api_album, foreign_key: true, null: false, type: :bigint, index: true
      t.references :song, foreign_key: true, null: false, type: :integer, index: { unique: true }
      t.string :source_song_id, null: false, index: true
      t.timestamps
    end
  end

  def down
    drop_table :distribution_api_songs
  end
end
