class RemoveNotNullConstraintFromAdyenTransactions < ActiveRecord::Migration[6.0]
  def up
    change_column_null :adyen_transactions, :local_currency, true
  end

  def down
    change_column_null :adyen_transactions, :local_currency, false
  end
end
