class CreateSocialProducts < ActiveRecord::Migration[4.2]
  def up
    person = Person.last
    products =
    [
      {id: 402, created_by: person, country_website_id: 1, name: "TuneCore Social Pro Monthly Subscription", display_name: "tc_social_monthly", description: "TuneCore Social Pro Monthly Subscription", price: 7.99, currency: "USD",  product_type: "Ad Hoc", product_family: "Artist Services (non-distribution)" },
      {id: 403, created_by: person, country_website_id: 1, name: "TuneCore Social Pro Annual Subscription", display_name: "tc_social_annually", description: "TuneCore Social Pro Annual Subscription", price: 83.88, currency: "USD",  product_type: "Ad Hoc", product_family: "Artist Services (non-distribution)" },
      {id: 404, created_by: person, country_website_id: 2, name: "TuneCore Social Pro Monthly Subscription", display_name: "tc_social_monthly", description: "TuneCore Social Pro Monthly Subscription", price: 10.99, currency: "CAD",  product_type: "Ad Hoc", product_family: "Artist Services (non-distribution)" },
      {id: 405, created_by: person, country_website_id: 2, name: "TuneCore Social Pro Annual Subscription", display_name: "tc_social_annually", description: "TuneCore Social Pro Annual Subscription", price: 119.88, currency: "CAD",  product_type: "Ad Hoc", product_family: "Artist Services (non-distribution)" },
      {id: 406, created_by: person, country_website_id: 3, name: "TuneCore Social Pro Monthly Subscription", display_name: "tc_social_monthly", description: "TuneCore Social Pro Monthly Subscription", price: 6.99, currency: "GBP",  product_type: "Ad Hoc", product_family: "Artist Services (non-distribution)" },
      {id: 407, created_by: person, country_website_id: 3, name: "TuneCore Social Pro Annual Subscription", display_name: "tc_social_annually", description: "TuneCore Social Pro Annual Subscription", price: 71.88, currency: "GBP",  product_type: "Ad Hoc", product_family: "Artist Services (non-distribution)" },
      {id: 408, created_by: person, country_website_id: 4, name: "TuneCore Social Pro Monthly Subscription", display_name: "tc_social_monthly", description: "TuneCore Social Pro Monthly Subscription", price: 9.99, currency: "AUD",  product_type: "Ad Hoc", product_family: "Artist Services (non-distribution)" },
      {id: 409, created_by: person, country_website_id: 4, name: "TuneCore Social Pro Annual Subscription", display_name: "tc_social_annually", description: "TuneCore Social Pro Annual Subscription", price: 107.88, currency: "AUD",  product_type: "Ad Hoc", product_family: "Artist Services (non-distribution)" },
    ]

    subscription_products =
    [
      {id: 4, product_name: "Social", product_type: "monthly", product_id: 402, term_length: 1},
      {id: 5, product_name: "Social", product_type: "annually", product_id: 403, term_length: 12},
      {id: 6, product_name: "Social", product_type: "monthly", product_id: 404, term_length: 1},
      {id: 7, product_name: "Social", product_type: "annually", product_id: 405, term_length: 12},
      {id: 8, product_name: "Social", product_type: "monthly", product_id: 406, term_length: 1},
      {id: 9, product_name: "Social", product_type: "annually", product_id: 407, term_length: 12},
      {id: 10, product_name: "Social", product_type: "monthly", product_id: 408, term_length: 1},
      {id: 11, product_name: "Social", product_type: "annually", product_id: 409, term_length: 12}
    ]

    products.each { |product| Product.create(product)}
    subscription_products.each { |sp| SubscriptionProduct.create(sp) }
  end

  def down
    Product.where(display_name: ["tc_social_annually", "tc_social_monthly"]).each{|product| product.destroy}
    SubscriptionProduct.where(product_name: "Social").each{|product| product.destroy}
  end
end
