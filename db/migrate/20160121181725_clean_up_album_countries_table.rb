class CleanUpAlbumCountriesTable < ActiveRecord::Migration[4.2]
  def up
    AlbumCountry.where(album_id: nil).delete_all
  end

  def down
  end
end
