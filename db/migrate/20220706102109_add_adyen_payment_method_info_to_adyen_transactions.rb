class AddAdyenPaymentMethodInfoToAdyenTransactions < ActiveRecord::Migration[6.0]
  def change
    add_reference :adyen_transactions, :payment_method_info, foreign_key: { to_table: :adyen_payment_method_infos }
  end
end
