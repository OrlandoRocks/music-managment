class DropStoreIngestionErrors < ActiveRecord::Migration[6.0]
  def self.up
    drop_table :archive_store_ingestions
    drop_table :archive_store_ingestion_errors
  end

  def self.down
    create_table :archive_store_ingestions do |t|
      t.string :job_id
      t.string :isrc
      t.string :upc
      t.integer :song_id
      t.integer :store_id
      t.string :status
      t.datetime :ingested_at
      t.datetime :updated_at
      t.datetime :created_at
      t.string :store_uuid
    end

    add_index :archive_store_ingestions, :status
    add_index :archive_store_ingestions, :isrc
    add_index :archive_store_ingestions, :ingested_at

    create_table :archive_store_ingestion_errors do |t|
      t.integer :archive_store_ingestion_id
      t.text :message
      t.timestamps
    end

    add_index :archive_store_ingestion_errors, :archive_store_ingestion_id,
              name: "index_arc_store_ing_errors_on_arc_store_ing_id"
  end
end
