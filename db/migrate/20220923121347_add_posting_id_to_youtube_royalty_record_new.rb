class AddPostingIdToYoutubeRoyaltyRecordNew < ActiveRecord::Migration[6.0]
  def up
    # This column already exists in production, therefore in all QA environments
    # but it seems to have been added directly instead of a migration.
    unless column_exists? :you_tube_royalty_records_new, :posting_id
      add_column :you_tube_royalty_records_new, :posting_id, :integer
    end
  end

  def down
    # This is an exception case of a migration. The column was already present while this
    # this migration is added. Due to this we do want this to be rollbacked on prod
    # at the time of writing this migration. Please remove the column directly from
    # your env if needed to reverse it.
    puts "Column removal not performed. Please remove manually. See migration file for details"
  end
end
