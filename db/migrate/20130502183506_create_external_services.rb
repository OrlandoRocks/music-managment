class CreateExternalServices < ActiveRecord::Migration[4.2]
  def self.up
    create_table :external_services do |t|
      t.string    :name
    end 

    ExternalService.create(:name => "facebook")
  end

  def self.down
    drop_table :external_services
  end
end
