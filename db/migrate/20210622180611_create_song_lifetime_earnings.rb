class CreateSongLifetimeEarnings < ActiveRecord::Migration[6.0]
  def change
    create_table :song_lifetime_earnings, id: false do |t|
      t.primary_key :song_id
      t.decimal :song_earnings

      t.timestamps
    end
  end
end
