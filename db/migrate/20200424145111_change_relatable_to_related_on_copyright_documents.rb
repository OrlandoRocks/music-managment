class ChangeRelatableToRelatedOnCopyrightDocuments < ActiveRecord::Migration[6.0]
  def change
    safety_assured { rename_column :copyright_documents, :relatable_id, :related_id  }
    safety_assured { rename_column :copyright_documents, :relatable_type, :related_type }

    safety_assured { rename_index :copyright_documents, [:relatable_id, :relatable_type], [:related_id, :related_type] }
  end
end
