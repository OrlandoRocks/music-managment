class CreateAlbumCopyright < ActiveRecord::Migration[6.0]
  def change
    create_table :album_copyrights do |t|
      t.references :album
      t.integer    :type
      t.integer    :year
      t.string     :holder_name

      t.timestamps
    end
  end
end
