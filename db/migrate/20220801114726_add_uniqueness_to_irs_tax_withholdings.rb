class AddUniquenessToIRSTaxWithholdings < ActiveRecord::Migration[6.0]
  def change
    remove_index :irs_tax_withholdings, name: :irs_tax_withholdings_on_withheld_txn    
    add_index :irs_tax_withholdings, :withheld_from_person_transaction_id, unique: true, name: :idx_irs_tax_withholdings_on_wft_id
  end
end
