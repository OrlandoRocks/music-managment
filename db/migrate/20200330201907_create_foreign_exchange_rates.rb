class CreateForeignExchangeRates < ActiveRecord::Migration[6.0]
  def change
    create_table :foreign_exchange_rates do |t|
      t.float   :exchange_rate, null: false
      t.integer :country_id, null: false
      t.column  :currency, 'char(3)', null: false

      t.timestamps null: false
    end

    add_index :foreign_exchange_rates, :country_id
    add_index :foreign_exchange_rates, :currency
  end
end
