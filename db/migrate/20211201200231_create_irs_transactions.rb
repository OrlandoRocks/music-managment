class CreateIRSTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :irs_transactions do |t|
      t.string :status, null: false
      t.string :transaction_code
      t.datetime :transacted_at

      t.timestamps
    end
  end
end
