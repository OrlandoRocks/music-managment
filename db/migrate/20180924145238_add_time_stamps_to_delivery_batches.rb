class AddTimeStampsToDeliveryBatches < ActiveRecord::Migration[4.2]
  def change
    add_column :delivery_batches, :updated_at, :datetime, null: false
    add_column :delivery_batches, :created_at, :datetime, null: false
    add_column :delivery_batch_items, :updated_at, :datetime, null: false
    add_column :delivery_batch_items, :created_at, :datetime, null: false
  end
end
