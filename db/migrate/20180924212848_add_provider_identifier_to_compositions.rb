class AddProviderIdentifierToCompositions < ActiveRecord::Migration[4.2]
  def change
    add_column :compositions, :provider_identifier, :string
    
    add_index :compositions, :provider_identifier
  end
end
