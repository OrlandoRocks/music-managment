class AddIswcColToMumaSongs < ActiveRecord::Migration[4.2]
  def self.up
    add_column :muma_songs, :iswc, :string, :limit => 11
    add_column :muma_songs, :copyrighted_at, :datetime
    add_column :muma_songs, :copyright_num, :string, :limit => 12
    add_column :muma_songs, :expired_at, :datetime 
  end

  def self.down
    remove_column :muma_songs, :iswc
    remove_column :muma_songs, :copyrighted_at
    remove_column :muma_songs, :copyright_num
    remove_column :muma_songs, :expired_at
  end
end
