class AddFeaturesPeopleTable < ActiveRecord::Migration[4.2]
  def self.up
    create_table :features_people do |t|
      t.integer :feature_id, :null => false
      t.integer :person_id, :null => false
      t.timestamps
    end

    # Create a unique index for the features - don't want to allow a feature to be added
    # several times to a partner.
    add_index :features_people, [:feature_id,:person_id],
      :unique => true, :name => "index_unique_features_people_ids"

  end

  def self.down
      execute "DROP TABLE IF EXISTS `features_people`;"
  end
end
