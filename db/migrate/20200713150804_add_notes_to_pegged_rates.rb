class AddNotesToPeggedRates < ActiveRecord::Migration[6.0]
  def change
    add_column :foreign_exchange_pegged_rates, :note, :text
    add_column :foreign_exchange_pegged_rates, :updated_by, :bigint
  end
end
