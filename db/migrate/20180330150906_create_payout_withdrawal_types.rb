class CreatePayoutWithdrawalTypes < ActiveRecord::Migration[4.2]
  def change
    create_table :payout_withdrawal_types do |t|
      t.belongs_to :payout_provider
      t.boolean :preferred
      t.string :withdrawal_type
      t.datetime :disabled_at

      t.timestamps
    end
    add_index :payout_withdrawal_types, :payout_provider_id
  end
end
