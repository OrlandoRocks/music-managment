class AddCountryWebsiteToSalesRecordMasters < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE sales_record_masters
     ADD COLUMN country_website_id int(11) unsigned DEFAULT 1 COMMENT 'Foreign key to country_websites' AFTER id")
  end

  def self.down
    remove_column :sales_record_masters, :country_website_id
  end
end
