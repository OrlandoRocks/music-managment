class AddCountryWebsiteToStoredPaypalAccounts < ActiveRecord::Migration[4.2]
  def self.up
    execute("ALTER TABLE stored_paypal_accounts ADD country_website_id CHAR(3) NOT NULL DEFAULT 1 COMMENT 'Foreign Key to country websties' AFTER id,
            ADD KEY `spa_transfers_country` (`country_website_id`);");
  end

  def self.down
    remove_column :stored_paypal_accounts, :country_website_id
  end
end
