class CreateTaxForms < ActiveRecord::Migration[4.2]
  def change
    create_table :tax_forms do |t|
      t.string :type, null: false
      t.string :provider
      t.datetime :submitted_at, null: false
      t.datetime :expires_at, null: false
      t.references :person, null: false
      t.text :payload

      t.timestamps
    end

    add_index :tax_forms, :person_id
  end
end
