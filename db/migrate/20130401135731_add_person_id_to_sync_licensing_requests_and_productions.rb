class AddPersonIdToSyncLicensingRequestsAndProductions < ActiveRecord::Migration[4.2]
  def self.up
    add_column :sync_license_requests, :person_id, :integer
    add_column :sync_license_productions, :person_id, :integer

    add_index :sync_license_requests, :person_id
    add_index :sync_license_productions, :person_id
  end

  def self.down
    remove_index :sync_license_requests, :person_id
    remove_index :sync_license_productions, :person_id

    remove_column :sync_license_requests, :person_id
    remove_column :sync_license_productions, :person_id
  end
end
