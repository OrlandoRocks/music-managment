class RemoveNullFalseForRefundInDisputes < ActiveRecord::Migration[6.0]
  def change
    change_column_null :disputes, :refund_id, true
  end
end
