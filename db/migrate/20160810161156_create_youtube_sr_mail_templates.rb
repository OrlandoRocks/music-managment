class CreateYoutubeSrMailTemplates < ActiveRecord::Migration[4.2]
  def up
    create_table :youtube_sr_mail_templates do |t|
      t.string        :template_name
      t.string        :select_label
      t.string        :template_path
      t.text          :custom_fields
    end
  end

  def down
  end
end
