class AddFlashSaleRule < ActiveRecord::Migration[4.2]
  def up
    if !Rails.env.test?
      ProductItemRule.create!(
        :product_item_id=>4,
        :parent_id=>nil,
        :rule_type=>"price_only",
        :rule=>"true_false",
        :inventory_type=>"",
        :model_to_check=>"Salepoint",
        :method_to_check=>"store.on_flash_sale?",
        :true_false=>1,
        :price=>-0.99,
        :currency=>"USD"
      )

      ProductItemRule.create!(
        :product_item_id=>58,
        :parent_id=>nil,
        :rule_type=>"price_only",
        :rule=>"true_false",
        :inventory_type=>"",
        :model_to_check=>"Salepoint",
        :method_to_check=>"store.on_flash_sale?",
        :true_false=>1,
        :price=>-0.49,
        :currency=>"CAD"
      )
    end
  end

  def down
    product_item_rules = ProductItemRule.where("method_to_check = ?", "store.on_flash_sale?")
    product_item_rules.each { |pi| pi.destroy }
  end
end
