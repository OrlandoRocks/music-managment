class AddS3FileNameToCovers < ActiveRecord::Migration[4.2]
  def change
    add_column :covers, :s3_file_name, :string
  end
end
