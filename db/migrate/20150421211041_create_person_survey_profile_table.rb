class CreatePersonSurveyProfileTable < ActiveRecord::Migration[4.2]
  def up
    create_table  :person_profile_survey_infos do |t|
      t.integer   :person_id, null: false
      t.string    :survey_token
      t.datetime  :last_visited_at
      t.string    :segment_question_response
      t.timestamps
    end

    execute "ALTER TABLE person_profile_survey_infos CHANGE person_id person_id INT(10) UNSIGNED NOT NULL"
    execute "ALTER TABLE person_profile_survey_infos ADD CONSTRAINT fk_person_id FOREIGN KEY (person_id) references people(id)"
  end

  def down
    drop_table :person_profile_survey_infos
  end
end
