class CreateSipStoresDisplayGroups < ActiveRecord::Migration[4.2]
  def self.up
    # DCD edited 10-4-11 for prosperity, removed sip_store_id_temp since it was removed in a data_migration
    upstring = %Q|
      CREATE TABLE `sip_stores_display_groups` (
        `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
        `name` varchar(100) DEFAULT NULL,
        `sort_order` smallint(5) unsigned DEFAULT NULL,
        `group_on_status` tinyint(3) unsigned NOT NULL DEFAULT '1',
        #`sip_store_id_temp` smallint(5) unsigned DEFAULT NULL,
         PRIMARY KEY (`id`)
      ) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;|
    execute upstring
    
    # DCD edited 10-4-11 for prosperity, remove 2 columns from the sip_stores table to match what is currently in production
    remove_column :sip_stores, :sort_order
    remove_column :sip_stores, :display_group
    
    
  end

  def self.down
    drop_table :sip_stores_display_groups
    
    # DCD edited 10-4-11 for prosperity, rollback of column removal
    up_sql=%Q(ALTER TABLE `sip_stores`
              ADD COLUMN `sort_order` INT(2) NULL,
              ADD COLUMN `display_group` INT(2) NULL
            )
    execute up_sql
  end
end
