class AddCurrencyColumnToPayoneerAchFees < ActiveRecord::Migration[4.2]
  def change
    add_column :payoneer_ach_fees, :target_currency, :string
  end
end
