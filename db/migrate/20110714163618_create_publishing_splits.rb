class CreatePublishingSplits < ActiveRecord::Migration[4.2]
  def self.up
    create_table :publishing_splits do |t|
      t.integer :composer_id
      t.integer :composition_id
      t.decimal :percent

      t.timestamps
    end
  end

  def self.down
    drop_table :publishing_splits
  end
end
