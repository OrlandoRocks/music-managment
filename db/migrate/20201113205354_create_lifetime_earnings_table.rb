class CreateLifetimeEarningsTable < ActiveRecord::Migration[6.0]
  def up
    create_table :lifetime_earnings_tables do |t|
      t.integer :person_id
      t.decimal :person_intake, precision: 23, scale: 14
      t.decimal :youtube_intake, precision: 23, scale: 14
      t.timestamps
    end
  end

  def down
    safety_assured do
      drop_table :lifetime_earnings_tables
    end
  end
end
