class ChangeDurationToSecondsInSyncLicenseRequest < ActiveRecord::Migration[4.2]
  def self.up
    change_column :sync_license_requests, :duration, :integer 
  end

  def self.down
    change_column :sync_license_requets, :duration, :string
  end
end
