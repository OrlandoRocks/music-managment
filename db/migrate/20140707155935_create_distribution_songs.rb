class CreateDistributionSongs < ActiveRecord::Migration[4.2]
  def change
    create_table :distribution_songs do |t|
      t.string :state, null: false, limit: 25
      t.integer :distribution_id, null: false
      t.integer :salepoint_song_id, null: false
      t.string :sqs_message_id, limit: 100
      t.integer :retry_count
      t.string :delivery_type, limit: 25

      t.timestamps
    end
    
    add_index :distribution_songs, :distribution_id
    add_index :distribution_songs, :salepoint_song_id
    add_index :distribution_songs, :sqs_message_id
  end
end
