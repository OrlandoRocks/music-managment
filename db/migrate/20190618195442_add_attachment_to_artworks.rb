class AddAttachmentToArtworks < ActiveRecord::Migration[4.2]
  def up
    add_column :artworks, :artwork_file_name, :string
    add_column :artworks, :artwork_content_type, :string
    add_column :artworks, :artwork_file_size, :int
  end

  def down
    remove_attachment :artworks, :artwork
  end
end
