class Seed
  FIELD_OPTIONS = %Q{
    FIELDS TERMINATED BY ',' ENCLOSED BY '"'
    LINES TERMINATED BY '\\n'
  }

  def self.seed_all
    raise "Do Not Run This in Production" if Rails.env.production?

    ActiveRecord::Base.transaction do
      # Turn off foreign key checks so we don't have to worry about order of inserts/deletes
      ActiveRecord::Base.connection.execute("SET foreign_key_checks = 0;")

      seed_files.each do |seed_file|
        seed(seed_file)
      end

      ActiveRecord::Base.connection.execute("SET foreign_key_checks = 1;")
    end
  end

  def self.dump_all
    seed_files.each do |seed_file|
      dump(seed_file)
    end
  end

  def self.seed(seed_file)
    tablename = tablename(seed_file)
    puts "Seeding #{tablename} with seed file #{seed_file}."
    CsvSeeder.seed_with_csv_file(tablename, seed_file)
  end

  def self.dump(seed_file)
    tablename = tablename(seed_file)
    puts "Dumping #{tablename} into seed file #{seed_file}."
    CsvDumper.dump_into_csv_file(tablename, seed_file)
  end

  def self.seed_files
    Dir["db/seed/csv/*.csv"]
  end

  def self.tablename(seed_file)
    File.basename(seed_file, ".csv")
  end

  class CsvDumper
    def self.dump_into_csv_file(tablename, filename)
      time_string = Time.now.to_f.to_s
      tmp_file = Tempfile.new(time_string)

      # Dump into temp file
      ActiveRecord::Base.connection.execute("
        SELECT *
        FROM #{tablename}
        INTO OUTFILE '#{tmp_file.path}'
        #{FIELD_OPTIONS};
      ")

      # Copy the tempfile into the seeds folder
      FileUtils.cp(tmp_file, filename) && FileUtils.rm(tmp_file)
    end
  end

  class CsvSeeder
    def self.seed_with_csv_file(tablename, filename)
      model = tablename.singularize.classify.constantize
      attribute_names = model.attribute_names
      rows = clean_data(model, attribute_names, CSV.read(filename, liberal_parsing: true))

      model.delete_all

      model.import(attribute_names, rows, validate: false)
    end

    def self.clean_data(model, attribute_names, parsed_file)
      parsed_file.map do |row|
        row.each_with_index.map do |data, i|
          data
            .yield_self { |value| transform_null(value) }
            .yield_self { |value| deserialize(model, attribute_names[i], value) }
        end
      end
    end

    def self.deserialize(model, attribute_name, value)
      model.type_for_attribute(attribute_name).deserialize(value)
    end

    def self.transform_null(value)
      if %w[NULL \\N].include?(value)
        nil
      else
        value
      end
    end
  end
end
