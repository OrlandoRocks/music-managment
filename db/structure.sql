
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `account_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider_account_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `blocked` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `index_accounts_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `achievements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `achievements` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `points` int(11) NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `add_on_purchases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `add_on_purchases` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `payment_channel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `receipt_data` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_add_on_purchases_on_person_id` (`person_id`),
  KEY `index_add_on_purchases_on_product_id` (`product_id`),
  CONSTRAINT `fk_rails_1a72f74dc7` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`),
  CONSTRAINT `fk_rails_ce5ffbdbb0` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `advertisements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advertisements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `country_website_id` char(3) NOT NULL COMMENT 'Country that this advertisement will be displayed in',
  `name` varchar(75) NOT NULL COMMENT 'name of ad',
  `category` enum('Dashboard','Album','Single','Ringtone') DEFAULT 'Album' COMMENT 'determines which ad to select',
  `created_by_id` int(10) unsigned NOT NULL COMMENT 'id of administrator who created the offer',
  `thumbnail_title` varchar(75) DEFAULT NULL COMMENT 'ad title for thumbnail ad',
  `sidebar_title` varchar(75) NOT NULL COMMENT 'ad title for sidebar ad',
  `wide_title` varchar(75) NOT NULL COMMENT 'ad title for wide ad',
  `thumbnail_copy` varchar(255) DEFAULT NULL COMMENT 'ad copy for thumbnail ad',
  `sidebar_copy` varchar(255) NOT NULL COMMENT 'ad copy for sidebar ad',
  `wide_copy` varchar(255) DEFAULT NULL COMMENT 'ad copy for wide ad',
  `display_price` decimal(8,2) DEFAULT NULL COMMENT 'display price for product being advertised',
  `display_price_modifier` varchar(10) DEFAULT NULL COMMENT 'textual modifier for the display_price as in - only 47.99',
  `display_price_notes` varchar(100) DEFAULT NULL COMMENT 'parenthetical text that further describes advertised product',
  `thumbnail_image` varchar(100) DEFAULT NULL COMMENT 'thumbnail image url',
  `sidebar_image` varchar(100) DEFAULT NULL COMMENT 'sidebar image url',
  `wide_image` varchar(100) DEFAULT NULL COMMENT 'wide image url',
  `image_caption` varchar(100) DEFAULT NULL COMMENT 'caption to display under selected image',
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active' COMMENT 'should this advertisement be displayed',
  `action_link` varchar(100) NOT NULL COMMENT 'url a customer follows when clicking on ad',
  `thumbnail_link_text` varchar(30) NOT NULL COMMENT 'text for the thumbnail link',
  `sidebar_link_text` varchar(30) NOT NULL COMMENT 'text for the sidebar link',
  `wide_link_text` varchar(20) NOT NULL COMMENT 'text to appear in button or link for action - wide',
  `sort_order` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'ad preference',
  `is_default` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'show as default ad',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  PRIMARY KEY (`id`),
  KEY `category` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `adyen_merchant_configs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = @saved_cs_client */;
CREATE TABLE `adyen_merchant_configs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `merchant_account` varchar(255) DEFAULT NULL,
  `api_key` varchar(255) DEFAULT NULL,
  `client_key` varchar(255) DEFAULT NULL,
  `corporate_entity_id` bigint(20) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_adyen_merchant_configs_on_corporate_entity_id` (`corporate_entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `adyen_payment_method_infos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adyen_payment_method_infos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `payment_method_name` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `adyen_stored_payment_methods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adyen_stored_payment_methods` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) NOT NULL,
  `country_id` bigint(20) NOT NULL,
  `corporate_entity_id` bigint(20) NOT NULL,
  `adyen_payment_method_info_id` bigint(20) NOT NULL,
  `recurring_reference` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_adyen_stored_payment_methods_on_person_id` (`person_id`),
  KEY `index_adyen_stored_payment_methods_on_country_id` (`country_id`),
  KEY `index_adyen_stored_payment_methods_on_corporate_entity_id` (`corporate_entity_id`),
  KEY `adyen_payment_method_info` (`adyen_payment_method_info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `adyen_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adyen_transactions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `adyen_stored_payment_method_id` bigint(20) DEFAULT NULL,
  `foreign_exchange_rate_id` bigint(20) DEFAULT NULL,
  `invoice_id` bigint(20) NOT NULL,
  `person_id` bigint(20) NOT NULL,
  `country_id` bigint(20) NOT NULL,
  `amount` float NOT NULL,
  `amount_in_local_currency` float NOT NULL,
  `auth_code` varchar(255) DEFAULT NULL,
  `currency` varchar(255) NOT NULL,
  `local_currency` varchar(255) DEFAULT NULL,
  `error_message` varchar(255) DEFAULT NULL,
  `merchant_reference` varchar(255) DEFAULT NULL,
  `psp_reference` varchar(255) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `result_code` enum('AuthenticationFinished','AuthenticationNotRequired','Authorised','Cancelled','ChallengeShopper','Error','IdentifyShopper','Pending','PresentToShopper','Received','RedirectShopper','Refused') DEFAULT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  `status` enum('success','failure') DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `adyen_payment_method_info_id` bigint(20) DEFAULT NULL,
  `related_transaction_id` bigint(20) DEFAULT NULL,
  `adyen_merchant_config_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_adyen_transactions_on_adyen_stored_payment_method_id` (`adyen_stored_payment_method_id`),
  KEY `index_adyen_transactions_on_foreign_exchange_rate_id` (`foreign_exchange_rate_id`),
  KEY `index_adyen_transactions_on_invoice_id` (`invoice_id`),
  KEY `index_adyen_transactions_on_person_id` (`person_id`),
  KEY `index_adyen_transactions_on_country_id` (`country_id`),
  KEY `index_adyen_transactions_on_adyen_payment_method_info_id` (`adyen_payment_method_info_id`),
  KEY `index_adyen_transactions_on_related_transaction_id` (`related_transaction_id`),
  KEY `index_adyen_transactions_on_adyen_merchant_config_id` (`adyen_merchant_config_id`),
  CONSTRAINT `fk_rails_279366c2b5` FOREIGN KEY (`adyen_payment_method_info_id`) REFERENCES `adyen_payment_method_infos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `affiliate_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliate_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `person_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `api_key_id` int(11) DEFAULT NULL,
  `product_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `units_sold` int(11) DEFAULT NULL,
  `sale_amount` int(11) DEFAULT NULL,
  `sale_currency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_paid` date DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_affiliate_reports_on_person_id` (`person_id`),
  KEY `index_affiliate_reports_on_api_key_id` (`api_key_id`),
  KEY `index_affiliate_reports_on_person_email` (`person_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `album_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `album_countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `relation_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_album_countries_on_album_id` (`album_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `album_flagged_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `album_flagged_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` int(11) NOT NULL,
  `flagged_content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_album_flagged_content_on_album_id` (`album_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `album_genre_whitelists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `album_genre_whitelists` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `genre_id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_album_genre_whitelists_on_genre_id` (`genre_id`),
  KEY `index_album_genre_whitelists_on_album_id` (`album_id`),
  CONSTRAINT `fk_rails_36cd5d5caf` FOREIGN KEY (`album_id`) REFERENCES `albums` (`id`),
  CONSTRAINT `fk_rails_6dbc7b0966` FOREIGN KEY (`genre_id`) REFERENCES `genres` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `album_intakes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `album_intakes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` int(11) NOT NULL DEFAULT 0,
  `person_id` int(11) NOT NULL DEFAULT 0,
  `reporting_month_id` int(11) NOT NULL DEFAULT 0,
  `person_intake_id` int(11) NOT NULL DEFAULT 0,
  `songs_sold` int(11) NOT NULL DEFAULT 0,
  `songs_streamed` int(11) NOT NULL DEFAULT 0,
  `albums_sold` int(11) NOT NULL DEFAULT 0,
  `usd_total_cents` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `album_intakes_reporting_month_id_index` (`reporting_month_id`),
  KEY `album_intakes_person_intake_id_index` (`person_intake_id`),
  KEY `album_intages_album_id_index` (`album_id`),
  KEY `album_intakes_person_id_index` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `album_itunes_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `album_itunes_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` int(11) NOT NULL,
  `content_review_status` varchar(255) DEFAULT NULL,
  `itunes_connect_status` varchar(255) DEFAULT NULL,
  `stores_list` varchar(1000) DEFAULT NULL,
  `itunes_created_at` datetime DEFAULT NULL,
  `content_state_status` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_album_itunes_statuses_on_album_id` (`album_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `albums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `albums` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '',
  `title_version` varchar(128) DEFAULT NULL,
  `p_copyright_year` date DEFAULT NULL,
  `c_copyright_year` date DEFAULT NULL,
  `p_copyright` varchar(128) DEFAULT NULL,
  `c_copyright` varchar(128) DEFAULT NULL,
  `price_id` varchar(3) NOT NULL DEFAULT '',
  `sale_date` date NOT NULL,
  `orig_release_year` date NOT NULL,
  `recording_location` varchar(128) DEFAULT NULL,
  `liner_notes` text DEFAULT NULL,
  `created_on` date NOT NULL DEFAULT '0000-00-00',
  `album_state` varchar(50) DEFAULT 'entered',
  `is_various` tinyint(1) DEFAULT 0,
  `label_id` int(10) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `approval_date` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `deleted_date` datetime DEFAULT NULL,
  `golive_date` datetime DEFAULT NULL,
  `is_not_uploading` tinyint(1) DEFAULT 0,
  `paid_idea` tinyint(1) DEFAULT 0,
  `updated_at` datetime DEFAULT NULL,
  `legacy_annual_fee` tinyint(1) NOT NULL DEFAULT 0,
  `price_policy_id` int(10) unsigned DEFAULT 0,
  `payment_applied` tinyint(1) NOT NULL DEFAULT 0,
  `finalized_at` datetime DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `locked_by_id` int(11) DEFAULT NULL,
  `locked_by_type` varchar(255) DEFAULT NULL,
  `allow_different_format` tinyint(1) DEFAULT 0,
  `purchase_id` int(11) DEFAULT NULL,
  `display_on_page` tinyint(1) DEFAULT 0,
  `primary_genre_id` int(11) DEFAULT NULL,
  `secondary_genre_id` int(11) DEFAULT NULL,
  `takedown_at` datetime DEFAULT NULL,
  `known_live_on` date DEFAULT NULL,
  `apple_identifier` varchar(30) DEFAULT NULL,
  `price_override` float DEFAULT NULL,
  `renewal_due_on` date DEFAULT NULL,
  `steps_required` int(11) DEFAULT NULL,
  `partner_id` int(11) DEFAULT NULL,
  `album_type` enum('Album','Single','Ringtone') DEFAULT 'Album',
  `album_type_OLD` varchar(20) NOT NULL DEFAULT 'full',
  `song_sale_count` int(11) DEFAULT NULL,
  `album_sale_count` int(11) DEFAULT NULL,
  `paid_streams` int(11) DEFAULT NULL,
  `promo_streams` int(11) DEFAULT NULL,
  `promo_downloads` int(11) DEFAULT NULL,
  `total_revenue_cents` int(11) DEFAULT NULL,
  `can_stream_cache` tinyint(1) DEFAULT NULL,
  `full_stream_widget` tinyint(1) DEFAULT 0,
  `full_stream_iphone` tinyint(1) DEFAULT 0,
  `myspace_id` bigint(20) unsigned DEFAULT NULL,
  `legal_review_state` varchar(255) NOT NULL DEFAULT 'DO NOT REVIEW' COMMENT 'indicates content review states',
  `style_review_state` varchar(255) NOT NULL DEFAULT 'DO NOT REVIEW' COMMENT 'indicates content review states',
  `language_code` varchar(255) DEFAULT 'en',
  `created_at` datetime DEFAULT NULL,
  `apple_music` tinyint(1) NOT NULL DEFAULT 0,
  `source` varchar(255) DEFAULT NULL,
  `created_with_songwriter` tinyint(1) DEFAULT 0,
  `timed_release_timing_scenario` varchar(255) DEFAULT NULL,
  `created_with` varchar(255) DEFAULT NULL,
  `release_datetime_utc` datetime DEFAULT NULL,
  `is_dj_release` tinyint(1) DEFAULT 0,
  `metadata_language_code_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_albums_on_apple_identifier` (`apple_identifier`),
  KEY `albums_person_id_index` (`person_id`),
  KEY `albums_label_id_index` (`id`),
  KEY `albums_chargeable_id_index` (`price_policy_id`),
  KEY `albums_invoice_id_index` (`invoice_id`),
  KEY `index_albums_on_finalized_at` (`finalized_at`),
  KEY `index_albums_on_purchase_id` (`purchase_id`),
  KEY `index_albums_on_primary_genre_id` (`primary_genre_id`),
  KEY `index_albums_on_secondary_genre_id` (`secondary_genre_id`),
  KEY `index_albums_on_renewal_due_on` (`renewal_due_on`),
  KEY `takedown_at_idx` (`takedown_at`),
  KEY `can_stream_cache` (`can_stream_cache`),
  KEY `known_live_on` (`known_live_on`),
  KEY `index_albums_on_legal_review_state` (`legal_review_state`),
  KEY `index_albums_on_language_code` (`language_code`),
  KEY `index_albums_on_sale_date` (`sale_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `annual_renewals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annual_renewals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` int(11) NOT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `price_policy_id` int(10) unsigned DEFAULT 0,
  `created_on` date DEFAULT NULL,
  `number_of_months` int(11) DEFAULT 12,
  `promotion_identifier` varchar(30) DEFAULT NULL,
  `finalized_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `album_id_idx` (`album_id`),
  KEY `purchase_id` (`purchase_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `api_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `api_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `partner_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_api_keys_on_key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `api_loggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `api_loggers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `endpoint` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `external_service_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `requestable_id` int(11) DEFAULT NULL,
  `requestable_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `http_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `headers` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `request_body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `response_body` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code_location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `job_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_api_loggers_on_requestable_id_and_requestable_type` (`requestable_id`,`requestable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `apple_artist_ids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apple_artist_ids` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `apple_artist_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_apple_artist_ids_on_person_id_and_apple_artist_id` (`person_id`,`apple_artist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `apple_identifiers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apple_identifiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `duplicated_isrc_id` int(11) NOT NULL DEFAULT 0,
  `apple_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_apple_identifiers_on_duplicated_isrc_id` (`duplicated_isrc_id`),
  KEY `index_apple_identifiers_on_apple_id` (`apple_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `applied_ineligibility_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applied_ineligibility_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ineligibility_rule_id` int(11) DEFAULT NULL,
  `vetted_item_id` int(11) DEFAULT NULL,
  `vetted_item_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_applied_ineligibility_rules_on_ineligibility_rule_id` (`ineligibility_rule_id`),
  KEY `index_applied_ineligibility_rules_on_item_id_and_item_type` (`vetted_item_id`,`vetted_item_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ar_internal_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ar_internal_metadata` (
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archive_artist_revenue_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_artist_revenue_reports` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `artist_name` varchar(120) DEFAULT NULL,
  `artist_id` int(10) DEFAULT NULL,
  `revenue` decimal(23,14) DEFAULT NULL,
  `albums_sold` int(10) DEFAULT NULL,
  `songs_sold` int(10) DEFAULT NULL,
  `streams_sold` int(10) DEFAULT NULL,
  `number_of_releases` int(10) DEFAULT NULL,
  `srm_month` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archive_bandpage_views`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_bandpage_views` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bandpage_id` int(11) NOT NULL,
  `person_id` int(11) DEFAULT NULL,
  `viewed_at` datetime NOT NULL,
  `http_referer` varchar(255) DEFAULT NULL,
  `http_user_agent` varchar(255) DEFAULT NULL,
  `http_remote_addr` varchar(128) DEFAULT NULL,
  `http_cookie` varchar(128) DEFAULT NULL,
  `http_accept_language` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archive_bandpage_views_on_bandpage_id` (`bandpage_id`),
  KEY `index_archive_bandpage_views_on_person_id` (`person_id`),
  KEY `index_archive_bandpage_views_on_viewed_at` (`viewed_at`),
  KEY `index_archive_bandpage_views_on_http_referer` (`http_referer`),
  KEY `index_archive_bandpage_views_on_http_remote_addr` (`http_remote_addr`),
  KEY `index_archive_bandpage_views_on_http_cookie` (`http_cookie`),
  KEY `index_archive_bandpage_views_on_http_accept_language` (`http_accept_language`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archive_bandpages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_bandpages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lookup_key` varchar(36) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `about` text DEFAULT NULL,
  `artwork_id` int(11) DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL,
  `active_by_user` tinyint(1) DEFAULT 1,
  `active_by_tc` tinyint(1) DEFAULT 1,
  `link_to_url` varchar(255) DEFAULT NULL,
  `display` tinyint(1) DEFAULT NULL,
  `display_tour_dates` tinyint(1) DEFAULT 1,
  `display_media_player` tinyint(1) DEFAULT 1,
  `displayed_widget` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_archive_bandpages_on_lookup_key` (`lookup_key`),
  KEY `person_id_idx` (`person_id`),
  KEY `index_archive_bandpages_on_display` (`display`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archive_dropkloud_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_dropkloud_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) DEFAULT NULL,
  `product_type` varchar(255) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `term_length` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archive_dropkloud_purchases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_dropkloud_purchases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `dropkloud_product_id` int(11) NOT NULL,
  `effective_date` datetime DEFAULT NULL,
  `termination_date` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archive_dropkloud_subscription_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_dropkloud_subscription_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `archive_dropkloud_puchase_id` int(11) DEFAULT NULL,
  `event_type` varchar(255) NOT NULL,
  `person_subscription_status_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_dropkloud_sub_events_onperson_sub_status_id` (`person_subscription_status_id`),
  KEY `index_arc_dropkloud_sub_events_on_arc_dropkloud_purchase_ids` (`archive_dropkloud_puchase_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archive_facebook_recognition_service_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_facebook_recognition_service_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) DEFAULT NULL,
  `min_releases` int(11) DEFAULT NULL,
  `max_releases` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archive_fans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_fans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `widget_id` int(11) DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `fan_type` enum('Fan','FreeSongFan') DEFAULT 'Fan',
  `unsubscribed_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `widget_id` (`widget_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archive_gain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_gain` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gain_employee_id` varchar(255) DEFAULT NULL COMMENT 'Data from people.gain_employee_id',
  `person_id` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Archive of data from people.gain_employee_id';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archive_gainlogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_gainlogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_website_id` int(11) unsigned DEFAULT 1 COMMENT 'Foreign key to country_websites',
  `date_used` datetime DEFAULT NULL,
  `album_total_after_discount` int(11) NOT NULL DEFAULT 0,
  `person_id` int(11) DEFAULT NULL,
  `gain_employee_id` int(11) DEFAULT NULL,
  `album_id` int(11) DEFAULT NULL,
  `percent_off` int(11) NOT NULL DEFAULT 0,
  `last_discount_amount_cents` int(11) DEFAULT NULL,
  `currency` char(3) DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archive_gainlogs_on_purchase_id` (`purchase_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archive_gdpr_deletes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_gdpr_deletes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archive_manual_settlements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_manual_settlements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `settlement_amount_cents` int(11) NOT NULL,
  `currency` char(3) NOT NULL COMMENT 'Currency',
  `comment` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archive_petri_orphans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_petri_orphans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `sqs_message_id` varchar(255) NOT NULL,
  `sqs_message_body` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_archive_petri_orphans_on_sqs_message_id` (`sqs_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archive_ringtone_related_songs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_ringtone_related_songs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `song_id` int(11) unsigned NOT NULL,
  `album_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archive_song_assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_song_assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `song_id` int(11) DEFAULT NULL,
  `bucket_name` varchar(255) DEFAULT NULL,
  `original_filename` varchar(255) DEFAULT NULL,
  `asset_key` varchar(255) DEFAULT NULL,
  `transcoder_job_id` varchar(255) DEFAULT NULL,
  `processing_status` varchar(255) DEFAULT NULL,
  `error_messages` text DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `song_metadata` text DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `waveform_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_song_id` (`song_id`),
  CONSTRAINT `fk_song_id` FOREIGN KEY (`song_id`) REFERENCES `songs` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archive_songs_songwriters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_songs_songwriters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `song_id` int(11) NOT NULL,
  `songwriter_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archive_songs_songwriters_on_song_id` (`song_id`),
  KEY `index_archive_songs_songwriters_on_songwriter_id` (`songwriter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archive_songwriters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_songwriters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archive_songwriters_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archive_store_ingestion_errors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_store_ingestion_errors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `archive_store_ingestion_id` int(11) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_arc_store_ing_errors_on_arc_store_ing_id` (`archive_store_ingestion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archive_store_ingestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_store_ingestions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` varchar(255) DEFAULT NULL,
  `isrc` varchar(12) DEFAULT NULL,
  `upc` varchar(13) DEFAULT NULL,
  `song_id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `ingested_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `store_uid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_isrc` (`isrc`),
  KEY `index_ingested_at` (`ingested_at`),
  KEY `index_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archive_widgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `config_data` blob DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deactivated_at` timestamp NULL DEFAULT NULL,
  `created_as_free` enum('Y','N') DEFAULT 'Y',
  `free_song_enable` enum('Y','N') DEFAULT 'Y',
  `free_song_id` int(10) unsigned DEFAULT NULL,
  `free_song_text` varchar(255) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `display_on_page` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archives_itunes_comprehensives`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archives_itunes_comprehensives` (
  `apple_id` varchar(19) DEFAULT NULL,
  `upc` varchar(18) DEFAULT NULL,
  `grid` varchar(2) DEFAULT NULL,
  `vendor_id` varchar(19) DEFAULT NULL,
  `artist` varchar(25) DEFAULT NULL,
  `album` varchar(25) DEFAULT NULL,
  `genre` varchar(25) DEFAULT NULL,
  `total_tracks` int(11) DEFAULT NULL,
  `total_discs` int(11) DEFAULT NULL,
  `is_complete` varchar(7) DEFAULT NULL,
  `itunes_plus_ready` varchar(7) DEFAULT NULL,
  `provider` varchar(15) DEFAULT NULL,
  `label_name` varchar(25) DEFAULT NULL,
  `status` varchar(12) DEFAULT NULL,
  `ww` varchar(1) DEFAULT NULL,
  `au` varchar(1) DEFAULT NULL,
  `at` varchar(1) DEFAULT NULL,
  `be` varchar(1) DEFAULT NULL,
  `ca` varchar(1) DEFAULT NULL,
  `ch` varchar(1) DEFAULT NULL,
  `de` varchar(1) DEFAULT NULL,
  `dk` varchar(1) DEFAULT NULL,
  `es` varchar(1) DEFAULT NULL,
  `fi` varchar(1) DEFAULT NULL,
  `fr` varchar(1) DEFAULT NULL,
  `gb` varchar(1) DEFAULT NULL,
  `gr` varchar(1) DEFAULT NULL,
  `ie` varchar(1) DEFAULT NULL,
  `it` varchar(1) DEFAULT NULL,
  `jp` varchar(1) DEFAULT NULL,
  `lu` varchar(1) DEFAULT NULL,
  `nl` varchar(1) DEFAULT NULL,
  `no` varchar(1) DEFAULT NULL,
  `nz` varchar(1) DEFAULT NULL,
  `pt` varchar(1) DEFAULT NULL,
  `se` varchar(1) DEFAULT NULL,
  `us` varchar(1) DEFAULT NULL,
  `raw_status` varchar(50) DEFAULT NULL,
  `upload_created` datetime DEFAULT NULL,
  `upload_state` varchar(50) DEFAULT NULL,
  `upload_state_id` int(11) DEFAULT NULL,
  `message` text DEFAULT NULL,
  KEY `index_archives_itunes_comprehensives_on_upc` (`upc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archives_itunes_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archives_itunes_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upc` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `upload_created` datetime DEFAULT NULL,
  `upload_state` varchar(255) DEFAULT NULL,
  `upload_state_id` int(11) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `updated_on` date DEFAULT NULL,
  `prev_status` varchar(50) DEFAULT NULL,
  `prev_status_updated_on` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archives_rejection_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archives_rejection_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `country_website_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archives_salepoint_audits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archives_salepoint_audits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` int(11) DEFAULT NULL,
  `salepoint_id` int(11) DEFAULT NULL,
  `status` varchar(20) NOT NULL DEFAULT '',
  `reported_status` varchar(20) DEFAULT NULL,
  `report_type` varchar(20) DEFAULT NULL,
  `created_on` date NOT NULL DEFAULT '0000-00-00',
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `salepoint_audits_album_id_index` (`album_id`),
  KEY `salepoint_audits_salepoint_id_index` (`salepoint_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `artist_store_whitelists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artist_store_whitelists` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `artist_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_artist_store_whitelists_on_artist_id_and_store_id` (`artist_id`,`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `artists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `scrubbed_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_artists_on_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `artworks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artworks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uploaded` tinyint(1) NOT NULL DEFAULT 0,
  `album_id` int(11) NOT NULL DEFAULT 0,
  `s3_asset_id` int(11) DEFAULT NULL,
  `last_errors` varchar(255) DEFAULT NULL,
  `auto_generated` tinyint(1) DEFAULT 0,
  `external_asset_id` int(11) DEFAULT NULL,
  `s3_thumb_100x100_asset_id` int(11) DEFAULT NULL,
  `s3_thumb_400x400_asset_id` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `artwork_file_name` varchar(255) DEFAULT NULL,
  `artwork_content_type` varchar(255) DEFAULT NULL,
  `artwork_file_size` int(11) DEFAULT NULL,
  `artwork_updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_artworks_on_album_id` (`album_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `audit_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `actor_type` varchar(255) DEFAULT NULL,
  `actor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `background_effects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `background_effects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `background_genres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `background_genres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `background_id` int(11) DEFAULT NULL,
  `genre_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `backgrounds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `backgrounds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `balance_adjustment_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `balance_adjustment_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `kind` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_balance_adjustment_categories_on_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `balance_adjustments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `balance_adjustments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `related_id` int(10) unsigned DEFAULT NULL COMMENT 'Self foreign key',
  `rollback` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT 'Indicated if record is rollback',
  `person_id` int(10) unsigned NOT NULL COMMENT 'Foreign Key to people table',
  `posted_by_id` int(10) unsigned NOT NULL COMMENT 'Foreign Key to people table',
  `posted_by_name` varchar(255) DEFAULT NULL COMMENT 'Name of admin for auditing purposes',
  `debit_amount` decimal(23,14) NOT NULL DEFAULT 0.00000000000000,
  `credit_amount` decimal(23,14) NOT NULL DEFAULT 0.00000000000000,
  `currency` char(3) NOT NULL COMMENT 'Currency for debit or credit amount',
  `category` enum('Refund - Renewal','Refund - Other','Service Adjustment','Songwriter Royalty','Other','YouTube MCN Royalty','Facebook','Tidal DAP') DEFAULT NULL COMMENT 'Category of adjustment',
  `customer_note` varchar(255) DEFAULT NULL COMMENT 'Adjustment note for Customer',
  `admin_note` varchar(255) DEFAULT NULL COMMENT 'Adjustment note for Admin only',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  PRIMARY KEY (`id`),
  KEY `related_id` (`related_id`),
  KEY `person_id` (`person_id`),
  KEY `posted_by_id` (`posted_by_id`),
  CONSTRAINT `FK_people` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`),
  CONSTRAINT `FK_people_by` FOREIGN KEY (`posted_by_id`) REFERENCES `people` (`id`),
  CONSTRAINT `FK_self` FOREIGN KEY (`related_id`) REFERENCES `balance_adjustments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bandpage_views`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bandpage_views` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bandpage_id` int(11) NOT NULL,
  `person_id` int(11) DEFAULT NULL,
  `viewed_at` datetime NOT NULL,
  `http_referer` varchar(255) DEFAULT NULL,
  `http_user_agent` varchar(255) DEFAULT NULL,
  `http_remote_addr` varchar(128) DEFAULT NULL,
  `http_cookie` varchar(128) DEFAULT NULL,
  `http_accept_language` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_bandpage_views_on_bandpage_id` (`bandpage_id`),
  KEY `index_bandpage_views_on_person_id` (`person_id`),
  KEY `index_bandpage_views_on_viewed_at` (`viewed_at`),
  KEY `index_bandpage_views_on_http_referer` (`http_referer`),
  KEY `index_bandpage_views_on_http_remote_addr` (`http_remote_addr`),
  KEY `index_bandpage_views_on_http_cookie` (`http_cookie`),
  KEY `index_bandpage_views_on_http_accept_language` (`http_accept_language`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bandpages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bandpages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lookup_key` varchar(36) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `about` text DEFAULT NULL,
  `artwork_id` int(11) DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL,
  `active_by_user` tinyint(1) DEFAULT 1,
  `active_by_tc` tinyint(1) DEFAULT 1,
  `link_to_url` varchar(255) DEFAULT NULL,
  `display` tinyint(1) DEFAULT NULL,
  `display_tour_dates` tinyint(1) DEFAULT 1,
  `display_media_player` tinyint(1) DEFAULT 1,
  `displayed_widget` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_bandpages_on_lookup_key` (`lookup_key`),
  KEY `index_bandpages_on_display` (`display`),
  KEY `person_id_idx` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `base_item_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_item_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `base_item_id` int(10) unsigned NOT NULL COMMENT 'references product.id',
  `sort_order` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'cardinal order for oprtion',
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT 'reference id for parent rule, required for multiple rules that govern prices',
  `name` varchar(150) NOT NULL COMMENT 'name of option',
  `product_type` enum('Ad Hoc','Package','All') DEFAULT 'Ad Hoc' COMMENT 'determines what type of product, the option applies to',
  `option_type` enum('required','select_one','optional') DEFAULT 'required' COMMENT 'how should this pricing rule option be selected on the UI',
  `rule_type` enum('inventory','entitlement','price_only') DEFAULT NULL COMMENT 'grants a user inventory or an entitlement',
  `entitlement_rights_group_id` int(10) unsigned DEFAULT NULL,
  `rule` enum('price_for_each','total_included','price_above_included','price_for_each_above_included','range','true_false') NOT NULL COMMENT 'processing rule for determining price',
  `inventory_type` varchar(50) NOT NULL COMMENT 'inventory_type of product item being sold',
  `model_to_check` varchar(50) DEFAULT NULL COMMENT 'required for rules other than price_for_each',
  `method_to_check` varchar(50) DEFAULT NULL COMMENT 'method to check on the provided model',
  `quantity` mediumint(9) DEFAULT NULL COMMENT 'how many are granted to the user',
  `unlimited` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'is the user granted unlimited inventory items',
  `true_false` tinyint(1) DEFAULT NULL COMMENT 'used for true_false rule',
  `minimum` mediumint(9) DEFAULT NULL COMMENT 'start of range/threshhold',
  `maximum` mediumint(9) DEFAULT NULL COMMENT 'end of range/threshold',
  `price` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT 'price when the rules are matched, can be 0.00',
  `currency` char(3) NOT NULL COMMENT 'Currency for base item option',
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `base_item_id` (`base_item_id`),
  CONSTRAINT `fk_base_item_id` FOREIGN KEY (`base_item_id`) REFERENCES `base_items` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `base_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL COMMENT 'name of product item',
  `description` varchar(255) DEFAULT NULL COMMENT 'optional short description of product item',
  `renewal_type` enum('entitlement','distribution','inventory') DEFAULT NULL COMMENT 'determines how the renewal is process on success or failure',
  `first_renewal_duration` tinyint(4) DEFAULT NULL COMMENT 'deteremines (with renewal_interval) how often long before the first interval/duration expires',
  `first_renewal_interval` enum('month','day','week','year') DEFAULT NULL COMMENT 'determines (with renewal_duration) how long before the first interval/duration expires',
  `renewal_duration` tinyint(4) DEFAULT NULL COMMENT 'deteremines (with renewal_interval) how often an entitlement will renew',
  `renewal_interval` enum('month','day','week','year') DEFAULT NULL COMMENT 'determines (with renewal_duration) how often an entitlement will renew',
  `price` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT 'overrides any rules set for this item',
  `currency` char(3) NOT NULL COMMENT 'Currency for base item',
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `batch_balance_adjustment_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch_balance_adjustment_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `minimum_payment` decimal(23,14) DEFAULT NULL,
  `hold_payment` varchar(255) DEFAULT NULL,
  `current_balance` decimal(23,14) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `batch_balance_adjustment_id` int(11) DEFAULT NULL,
  `balance_adjustment_id` int(11) DEFAULT NULL,
  `hold_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `batch_balance_adjustment_errors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch_balance_adjustment_errors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_balance_adjustment_id` int(11) DEFAULT NULL,
  `batch_balance_adjustment_detail_id` int(11) DEFAULT NULL,
  `raw_transaction` text DEFAULT NULL,
  `notes` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `batch_balance_adjustments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch_balance_adjustments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ingested_filename` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `batch_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch_transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `payment_batch_id` int(10) unsigned NOT NULL COMMENT 'Foreign Key to payment_batch table',
  `invoice_id` int(11) NOT NULL COMMENT 'Foreign Key to invoice table',
  `stored_credit_card_id` int(10) unsigned DEFAULT NULL COMMENT 'Foreign Key to stored credit card table (for braintree transactions)',
  `stored_paypal_account_id` int(11) unsigned DEFAULT NULL COMMENT 'Foreign Key to stored paypal_account table',
  `adyen_stored_payment_method_id` bigint(20) DEFAULT NULL COMMENT 'Foreign Key to adyen_stored_payment_methods table',
  `amount` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT 'Amount',
  `currency` char(3) NOT NULL COMMENT 'Currency',
  `processed` enum('pending','processed','cannot_process') NOT NULL DEFAULT 'pending' COMMENT 'Current State of process',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  `matching_id` int(10) unsigned DEFAULT NULL COMMENT 'Polymorphic foreign key',
  `matching_type` varchar(255) DEFAULT NULL COMMENT 'Polymorphic foreign key type',
  PRIMARY KEY (`id`),
  KEY `payment_batch_id` (`payment_batch_id`),
  KEY `stored_credit_card_id` (`stored_credit_card_id`),
  KEY `invoice_id` (`invoice_id`),
  KEY `FK_bt_stored_paypal_account_id` (`stored_paypal_account_id`),
  KEY `index_batch_transactions_on_adyen_stored_payment_method_id` (`adyen_stored_payment_method_id`),
  CONSTRAINT `FK_bt_stored_paypal_account_id` FOREIGN KEY (`stored_paypal_account_id`) REFERENCES `stored_paypal_accounts` (`id`),
  CONSTRAINT `FK_payment_batch` FOREIGN KEY (`payment_batch_id`) REFERENCES `payment_batches` (`id`),
  CONSTRAINT `FK_stored_credit_card` FOREIGN KEY (`stored_credit_card_id`) REFERENCES `stored_credit_cards` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `believe_errors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `believe_errors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` int(11) DEFAULT NULL,
  `request_args` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `endpoint` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `response_errors` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_believe_errors_on_album_id` (`album_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `blacklisted_artists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blacklisted_artists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) DEFAULT 1,
  `artist_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_blacklisted_artists_on_artist_id` (`artist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `blacklisted_paypal_payees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blacklisted_paypal_payees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payee` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `blacklists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blacklists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `booklets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booklets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` int(11) DEFAULT NULL,
  `s3_asset_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `paid_at` datetime DEFAULT NULL,
  `price_policy_id` int(10) unsigned DEFAULT 0,
  `purchase_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `album_id` (`album_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `braintree_account_updater_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `braintree_account_updater_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `update_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_last_four_digits` char(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiration_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `braintree_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `braintree_transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_website_id` int(11) unsigned NOT NULL,
  `person_id` int(10) unsigned NOT NULL COMMENT 'Reference people table',
  `stored_credit_card_id` int(10) unsigned DEFAULT NULL COMMENT 'Reference stored_credit_cards table',
  `invoice_id` int(10) unsigned DEFAULT NULL COMMENT 'References invoices table',
  `related_transaction_id` int(10) unsigned DEFAULT NULL COMMENT 'Self-referential for all refund transactions',
  `response_code` char(4) NOT NULL COMMENT 'Response code sent from Braintree',
  `avs_response` varchar(10) DEFAULT NULL COMMENT 'AVS Response code sent from Braintree',
  `cvv_response` varchar(10) DEFAULT NULL COMMENT 'Security Code (cvv) code sent from Braintree',
  `status` enum('success','fraud','declined','error','duplicate') NOT NULL DEFAULT 'success' COMMENT 'Status of the transaction',
  `action` enum('sale','refund') NOT NULL DEFAULT 'sale' COMMENT 'Type of transaction made',
  `transaction_id` varchar(255) DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL COMMENT 'Amount of transaction',
  `currency` char(3) NOT NULL COMMENT 'Currency for invoice item',
  `processor_id` varchar(20) DEFAULT NULL COMMENT 'Processor used for transaction (there are multiple due to internationalization)',
  `refunded_amount` decimal(10,2) DEFAULT NULL COMMENT 'Cached total of all refunds against this transaction',
  `refund_category` varchar(50) DEFAULT NULL COMMENT 'category of refund as defined by CS and Business',
  `refund_reason` varchar(255) DEFAULT NULL COMMENT 'Reason given for refunding the original transaction',
  `refunded_by_id` int(10) unsigned DEFAULT NULL COMMENT 'ID of Customer Service Rep who processed the refund',
  `ip_address` char(15) NOT NULL,
  `raw_response` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `response_message` varchar(255) DEFAULT NULL,
  `transaction_status` varchar(255) DEFAULT NULL,
  `processor_response_text` varchar(255) DEFAULT NULL,
  `processor_response_type` varchar(255) DEFAULT NULL,
  `additional_processor_response` varchar(255) DEFAULT NULL,
  `network_response_text` varchar(255) DEFAULT NULL,
  `payin_provider_config_id` bigint(20) DEFAULT NULL,
  `avs_response_code` varchar(255) DEFAULT NULL,
  `avs_message` varchar(255) DEFAULT NULL,
  `avs_postal_response` varchar(255) DEFAULT NULL,
  `avs_postal_message` varchar(255) DEFAULT NULL,
  `avs_street_address_response` varchar(255) DEFAULT NULL,
  `avs_street_address_message` varchar(255) DEFAULT NULL,
  `cvv_response_code` varchar(255) DEFAULT NULL,
  `cvv_message` varchar(255) DEFAULT NULL,
  `liability_shifted` tinyint(1) DEFAULT NULL,
  `liability_shifted_at` datetime DEFAULT NULL,
  `ds_transaction_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `stored_credit_card_id` (`stored_credit_card_id`),
  KEY `related_transaction_id` (`related_transaction_id`),
  KEY `refund_category` (`refund_category`),
  KEY `invoice_id` (`invoice_id`),
  KEY `country_websites_currency` (`country_website_id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `index_braintree_transactions_on_payin_provider_config_id` (`payin_provider_config_id`),
  CONSTRAINT `fk_related_transaction_id` FOREIGN KEY (`related_transaction_id`) REFERENCES `braintree_transactions` (`id`),
  CONSTRAINT `fk_stored_credit_card_id` FOREIGN KEY (`stored_credit_card_id`) REFERENCES `stored_credit_cards` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `braintree_vault_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `braintree_vault_transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL COMMENT 'References people table',
  `related_id` int(10) DEFAULT NULL COMMENT 'polymorphic related column',
  `related_type` varchar(30) DEFAULT NULL COMMENT 'polymorphic related type',
  `response_code` varchar(12) DEFAULT NULL,
  `ip_address` char(15) NOT NULL,
  `raw_response` text DEFAULT NULL COMMENT 'Original response from Braintree',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bundles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bundles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` int(11) DEFAULT NULL,
  `params` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `completed_at` datetime DEFAULT NULL,
  `canceled_at` datetime DEFAULT NULL,
  `in_progress_at` datetime DEFAULT NULL,
  `failed` tinyint(1) DEFAULT NULL,
  `comment` text DEFAULT NULL,
  `update_token` varchar(50) DEFAULT 'N/A',
  `sqs_message_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bundles_album_id_index` (`album_id`),
  KEY `index_bundles_on_album_id` (`album_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `cable_auth_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cable_auth_tokens` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_cable_auth_tokens_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `canada_postal_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `canada_postal_codes` (
  `id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `code` char(6) NOT NULL,
  `city` varchar(30) NOT NULL,
  `province` enum('Alberta','British Columbia','Manitoba','New Brunswick','Newfoundland','Northwest Territories','Nova Scotia','Nunavut','Ontario','Prince Edward Island','Quebec','Saskatchewan','Yukon Territory') NOT NULL,
  `province_code` enum('AB','BC','MB','NB','NL','NS','NT','NU','ON','PE','QC','SK','YT') NOT NULL,
  `city_type` enum('A','D') NOT NULL,
  `latitude` decimal(17,15) NOT NULL,
  `longitude` decimal(18,15) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_postal_codes_on_code_and_city` (`code`,`city`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `captcha_whitelists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `captcha_whitelists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `cert_batches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cert_batches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promotion` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `person_id` int(11) DEFAULT NULL,
  `sample_cert_yaml_REMOVE` text DEFAULT NULL,
  `finalized` tinyint(1) NOT NULL DEFAULT 0,
  `expiry_date` datetime DEFAULT NULL,
  `admin_only` tinyint(1) DEFAULT 0,
  `cert_engine` varchar(50) DEFAULT NULL,
  `engine_params` varchar(256) DEFAULT NULL,
  `spawning_code` varchar(30) DEFAULT NULL,
  `brand_code` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_cert_batches_on_promotion` (`promotion`),
  KEY `cert_batches_person_id_index` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `cert_batches_partners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cert_batches_partners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cert_batch_id` int(11) NOT NULL,
  `partner_id` smallint(5) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_cert_batches_partners_on_cert_batch_id_and_partner_id` (`cert_batch_id`,`partner_id`),
  KEY `partner_id` (`partner_id`),
  CONSTRAINT `cert_batches_partners_ibfk_1` FOREIGN KEY (`cert_batch_id`) REFERENCES `cert_batches` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cert_batches_partners_ibfk_2` FOREIGN KEY (`partner_id`) REFERENCES `partners` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `certifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `certifications` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `badge_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `points` int(11) NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `certs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `certs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cert` varchar(30) NOT NULL DEFAULT '',
  `date_used` datetime DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `percent_off` int(11) NOT NULL DEFAULT 0,
  `person_id` int(11) DEFAULT NULL,
  `album_id` int(11) DEFAULT NULL,
  `total_amount` int(10) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `cert_engine` varchar(30) DEFAULT NULL,
  `engine_params` varchar(30) DEFAULT NULL,
  `last_discount_amount_cents` int(11) DEFAULT NULL,
  `issuer` varchar(30) DEFAULT NULL,
  `cert_batch_id` int(11) DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `admin_only` tinyint(1) NOT NULL DEFAULT 0,
  `admin_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_certs_on_cert_batch_id` (`cert_batch_id`),
  KEY `certs_person_id_index` (`person_id`),
  KEY `certs_album_id_index` (`album_id`),
  KEY `index_certs_on_cert` (`cert`),
  KEY `index_certs_on_purchase_id` (`purchase_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
CREATE TABLE `chargebee_customers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) NOT NULL,
  `chargebee_id` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_chargebee_customers_on_person_id` (`person_id`),
  KEY `index_chargebee_customers_on_chargebee_id` (`chargebee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `check_transfers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `check_transfers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `transfer_status` varchar(20) NOT NULL DEFAULT '',
  `person_id` int(11) NOT NULL DEFAULT 0,
  `payment_cents` int(11) NOT NULL DEFAULT 0,
  `admin_charge_cents` int(11) NOT NULL DEFAULT 0,
  `currency` char(3) NOT NULL COMMENT 'Currency for check',
  `payee` text NOT NULL,
  `address1` text NOT NULL,
  `address2` text DEFAULT NULL,
  `city` text NOT NULL,
  `state` text NOT NULL,
  `postal_code` varchar(10) NOT NULL DEFAULT '',
  `country` varchar(56) NOT NULL DEFAULT '',
  `phone_number` varchar(255) DEFAULT NULL,
  `backend` varchar(255) DEFAULT 'ManualCheckTransfer',
  `debit_transaction_id` int(11) DEFAULT NULL,
  `refund_transaction_id` int(11) DEFAULT NULL,
  `export` tinyint(1) DEFAULT 0,
  `export_date` datetime DEFAULT NULL,
  `approved_by_id` int(11) DEFAULT NULL,
  `suspicious_flags` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `person_id_idx` (`person_id`),
  KEY `transfer_status` (`transfer_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `cipher_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cipher_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varbinary(256) DEFAULT NULL,
  `iv` varbinary(256) DEFAULT NULL,
  `tag` varbinary(16) DEFAULT NULL,
  `cipherable_id` int(11) DEFAULT NULL,
  `cipherable_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `secrets` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `client_applications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_applications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `support_url` varchar(255) DEFAULT NULL,
  `callback_url` varchar(255) DEFAULT NULL,
  `key` varchar(20) DEFAULT NULL,
  `secret` varchar(40) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `authorized_by_role` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_client_applications_on_key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `client_applications_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_applications_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_application_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `cloud_search_batches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cloud_search_batches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `songs_in_cloudsearch` int(11) DEFAULT NULL,
  `last_add_date` datetime DEFAULT NULL,
  `last_delete_date` datetime DEFAULT NULL,
  `domain` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `cloud_search_domain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cloud_search_domain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `search_endpoint` varchar(255) DEFAULT NULL,
  `document_endpoint` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `cms_assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_assets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `asset_name` varchar(100) NOT NULL COMMENT 'name of asset',
  `asset_category` enum('Ad','Dashboard','General') DEFAULT 'Dashboard',
  `file_name` varchar(100) DEFAULT NULL COMMENT 'file name',
  `file_path` varchar(200) DEFAULT NULL COMMENT 'full path and file name of asset',
  `file_type` varchar(20) NOT NULL COMMENT 'file type',
  `file_size` int(20) NOT NULL COMMENT 'file size',
  `created_by_id` int(10) NOT NULL COMMENT 'id of administrator who created the asset',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  PRIMARY KEY (`id`),
  KEY `ix_cms_asset_name` (`asset_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `cms_set_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_set_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cms_set_id` int(11) NOT NULL,
  `attr_name` varchar(255) NOT NULL,
  `attr_value` varchar(255) DEFAULT NULL,
  `attr_text` text DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_cms_set_attributes_on_cms_set_id` (`cms_set_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `cms_sets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_sets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `callout_type` varchar(25) NOT NULL,
  `callout_name` varchar(255) NOT NULL,
  `start_tmsp` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `country_website_id` int(11) NOT NULL,
  `country_website_language_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_type_tmsp_country` (`callout_type`,`start_tmsp`,`country_website_id`),
  KEY `index_cms_sets_on_country_website_id` (`country_website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `company_informations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_informations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) NOT NULL,
  `enterprise_number` varchar(255) DEFAULT NULL,
  `company_registry_data` varchar(255) DEFAULT NULL,
  `place_of_legal_seat` varchar(255) DEFAULT NULL,
  `registered_share_capital` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_company_informations_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `compliance_info_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compliance_info_fields` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `field_name` varchar(255) DEFAULT NULL,
  `field_value` text DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_compliance_info_fields_on_person_id_and_field_name` (`person_id`,`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `composers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `composers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `address_1` varchar(45) DEFAULT NULL,
  `address_2` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `cae` varchar(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `agreed_to_terms_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `name_prefix` varchar(4) DEFAULT NULL,
  `name_suffix` varchar(4) DEFAULT NULL,
  `alternate_email` varchar(100) DEFAULT NULL,
  `country` varchar(2) DEFAULT NULL,
  `alternate_phone` varchar(20) DEFAULT NULL,
  `publisher_id` int(11) DEFAULT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `nosocial` tinyint(1) DEFAULT 0,
  `lod_id` int(11) DEFAULT NULL,
  `terminated_at` datetime DEFAULT NULL,
  `provider_identifier` varchar(255) DEFAULT NULL,
  `perform_live` tinyint(1) DEFAULT 0,
  `publishing_role_id` int(11) DEFAULT NULL,
  `performing_rights_organization_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `sync_opted_in` tinyint(1) DEFAULT NULL,
  `sync_opted_updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `index_composers_on_provider_identifier` (`provider_identifier`),
  KEY `index_composers_on_publishing_role_id` (`publishing_role_id`),
  KEY `index_composers_on_performing_rights_organization_id` (`performing_rights_organization_id`),
  KEY `index_composers_on_account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `compositions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compositions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `iswc` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_unallocated_sum` tinyint(1) DEFAULT 0,
  `cwr_name` varchar(255) DEFAULT NULL,
  `state` varchar(25) DEFAULT NULL,
  `prev_state` varchar(25) DEFAULT NULL,
  `state_updated_on` date DEFAULT NULL,
  `provider_identifier` varchar(255) DEFAULT NULL,
  `provider_recording_id` varchar(255) DEFAULT NULL,
  `translated_name` varchar(255) DEFAULT NULL,
  `verified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_compositions_on_provider_identifier` (`provider_identifier`),
  KEY `index_compositions_on_provider_recording_id` (`provider_recording_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `copyright_claimants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `copyright_claimants` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `copyright_claim_id` bigint(20) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_copyright_claimants_on_email` (`email`),
  KEY `index_copyright_claimants_on_copyright_claim_id` (`copyright_claim_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `copyright_claims`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `copyright_claims` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `asset_type` varchar(255) NOT NULL,
  `asset_id` bigint(20) NOT NULL,
  `person_id` int(11) NOT NULL,
  `internal_claim_id` varchar(255) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `admin_id` int(11) NOT NULL,
  `fraud_type` varchar(255) DEFAULT NULL,
  `hold_amount` float DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT 1,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `is_withdrawal_blocked` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `index_copyright_claims_on_asset_type_and_asset_id` (`asset_type`,`asset_id`),
  KEY `index_copyright_claims_on_person_id` (`person_id`),
  KEY `index_copyright_claims_on_internal_claim_id` (`internal_claim_id`),
  KEY `index_copyright_claims_on_store_id` (`store_id`),
  KEY `index_copyright_claims_on_admin_id` (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `copyright_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `copyright_documents` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `related_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `related_id` bigint(20) DEFAULT NULL,
  `s3_asset_id` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_copyright_documents_on_related_type_and_related_id` (`related_type`,`related_id`),
  KEY `index_copyright_documents_on_related_id_and_related_type` (`related_id`,`related_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `copyright_state_transitions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `copyright_state_transitions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `copyright_status_id` int(11) DEFAULT NULL,
  `prev_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_copyright_state_transitions_on_person_id` (`person_id`),
  KEY `index_copyright_state_transitions_on_copyright_status_id` (`copyright_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `copyright_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `copyright_statuses` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `related_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `related_id` bigint(20) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_copyright_statuses_on_related_type_and_related_id` (`related_type`,`related_id`),
  KEY `index_copyright_statuses_on_related_id_and_related_type` (`related_id`,`related_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `copyrights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `copyrights` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `copyrightable_type` varchar(255) DEFAULT NULL,
  `copyrightable_id` bigint(20) DEFAULT NULL,
  `ownership` varchar(255) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `holder_name` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `composition` tinyint(1) DEFAULT NULL,
  `recording` tinyint(1) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `claimant_has_songwriter_agreement` tinyint(1) DEFAULT 0,
  `claimant_has_pro_or_cmo` tinyint(1) DEFAULT 0,
  `claimant_has_tc_publishing` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `index_copyrights_on_copyrightable_type_and_copyrightable_id` (`copyrightable_type`,`copyrightable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `corporate_entities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corporate_entities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `vat_registration_number` varchar(255) DEFAULT NULL,
  `crn` varchar(255) DEFAULT NULL,
  `paying_bank` varchar(255) DEFAULT NULL,
  `rib` varchar(255) DEFAULT NULL,
  `bic` varchar(255) DEFAULT NULL,
  `iban` varchar(255) DEFAULT NULL,
  `inbound_invoice_prefix` enum('BI-INV','TC-INV') DEFAULT NULL,
  `credit_note_inbound_prefix` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_corporate_entities_on_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `counterpoint_songs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `counterpoint_songs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `composer` varchar(255) DEFAULT NULL,
  `controlled_mech_share` varchar(255) DEFAULT NULL,
  `performing_artist_group` varchar(255) DEFAULT NULL,
  `first_album_label` varchar(255) DEFAULT NULL,
  `first_album_title` varchar(255) DEFAULT NULL,
  `created_date` varchar(255) DEFAULT NULL,
  `album_upc` varchar(255) DEFAULT NULL,
  `tunecore_song_id` varchar(255) DEFAULT NULL,
  `tunecore_composition_id` varchar(255) DEFAULT NULL,
  `grouping_code` varchar(255) DEFAULT NULL,
  `grouping_code_2` varchar(255) DEFAULT NULL,
  `credit_notes_label_copy` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `iso_code` char(2) NOT NULL,
  `region` varchar(255) NOT NULL,
  `sub_region` varchar(255) DEFAULT NULL,
  `tc_region` char(4) NOT NULL,
  `iso_code_3` char(3) NOT NULL,
  `is_sanctioned` tinyint(1) DEFAULT 0,
  `corporate_entity_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_countries_on_corporate_entity_id` (`corporate_entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `countries_payout_provider_configs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries_payout_provider_configs` (
  `country_id` bigint(20) NOT NULL,
  `payout_provider_config_id` bigint(20) NOT NULL,
  KEY `countries_on_payout_provider_configs` (`country_id`,`payout_provider_config_id`),
  KEY `payout_provider_configs_on_countries` (`payout_provider_config_id`,`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `country_audit_sources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country_audit_sources` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `country_audits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country_audits` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `country_audit_source_id` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_country_audits_on_person_id` (`person_id`),
  KEY `index_country_audits_on_country_id` (`country_id`),
  KEY `index_country_audits_on_country_audit_source_id_and_person_id` (`country_audit_source_id`,`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `country_help_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country_help_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meta_id` int(11) DEFAULT NULL,
  `us_id` int(11) DEFAULT NULL,
  `ca_id` int(11) DEFAULT NULL,
  `uk_id` int(11) DEFAULT NULL,
  `au_id` int(11) DEFAULT NULL,
  `de_id` int(11) DEFAULT NULL,
  `fr_id` int(11) DEFAULT NULL,
  `it_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `country_phone_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country_phone_codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `code` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_country_phone_codes_on_country_id` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `country_state_cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country_state_cities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `alt_name` text DEFAULT NULL,
  `state_id` bigint(20) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_country_state_cities_on_name_and_state_id` (`name`,`state_id`),
  KEY `index_country_state_cities_on_state_id` (`state_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `country_states`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country_states` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `iso_code` char(5) DEFAULT NULL,
  `entity_type` varchar(255) DEFAULT NULL,
  `country_id` bigint(20) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `gst_config_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_country_states_on_country_id` (`country_id`),
  KEY `index_country_states_on_gst_config_id` (`gst_config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `country_variable_prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country_variable_prices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `variable_price_id` int(11) DEFAULT NULL,
  `country_website_id` int(11) DEFAULT NULL,
  `wholesale_price` decimal(6,2) DEFAULT NULL,
  `currency` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_country_variable_prices_on_variable_price_id` (`variable_price_id`),
  KEY `index_country_variable_prices_on_country_website_id` (`country_website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `country_website_languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country_website_languages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `country_website_id` int(11) unsigned NOT NULL,
  `yml_languages` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `selector_language` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `selector_country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `selector_order` int(11) NOT NULL,
  `redirect_country_website_id` int(11) unsigned DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_country_website_languages_on_country_website_id` (`country_website_id`),
  KEY `cwl_redirect_id` (`redirect_country_website_id`),
  CONSTRAINT `fk_rails_8bfdfa28af` FOREIGN KEY (`redirect_country_website_id`) REFERENCES `country_websites` (`id`),
  CONSTRAINT `fk_rails_92be6d58a2` FOREIGN KEY (`country_website_id`) REFERENCES `country_websites` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `country_websites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country_websites` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(20) DEFAULT NULL COMMENT 'base url',
  `currency` char(3) DEFAULT NULL COMMENT '3 letter ISO currency',
  `country` char(20) DEFAULT NULL COMMENT '2 letter ISO for country',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `country_websites_currency` (`currency`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `country_websites_stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country_websites_stores` (
  `country_website_id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  KEY `index_country_websites_stores_on_country_website_id` (`country_website_id`),
  KEY `index_country_websites_stores_on_store_id` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `cover_song_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cover_song_metadata` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `song_id` int(11) NOT NULL,
  `cover_song` tinyint(1) NOT NULL DEFAULT 0,
  `licensed` tinyint(1) DEFAULT NULL,
  `will_get_license` tinyint(1) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_cover_song_metadata_on_song_id` (`song_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `covers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `covers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `background_id` int(11) DEFAULT NULL,
  `effect` varchar(255) DEFAULT NULL,
  `background_effect_id` int(11) DEFAULT NULL,
  `artist` varchar(255) DEFAULT NULL,
  `artist_typeface_id` int(11) DEFAULT NULL,
  `artist_typeface_pointsize` int(11) DEFAULT NULL,
  `artist_typeface_effect_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `title_typeface_id` int(11) DEFAULT NULL,
  `title_typeface_pointsize` int(11) DEFAULT NULL,
  `title_typeface_effect_id` int(11) DEFAULT NULL,
  `layout_class` varchar(255) DEFAULT NULL,
  `artist_height` int(11) DEFAULT NULL,
  `artist_width` int(11) DEFAULT NULL,
  `title_height` int(11) DEFAULT NULL,
  `title_width` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `album_id` int(11) DEFAULT NULL,
  `s3_file_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `album_id` (`album_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `cowriters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cowriters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `composer_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `provider_identifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_unknown` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `index_cowriters_on_first_name` (`first_name`),
  KEY `index_cowriters_on_last_name` (`last_name`),
  KEY `index_cowriters_on_composer_id` (`composer_id`),
  KEY `index_cowriters_on_provider_identifier` (`provider_identifier`),
  KEY `index_cowriters_on_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `creative_song_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creative_song_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `song_role_id` int(11) DEFAULT NULL,
  `creative_id` int(11) DEFAULT NULL,
  `song_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_creative_song_roles_on_song_id` (`song_id`),
  KEY `index_creative_song_roles_on_song_role_id` (`song_role_id`),
  KEY `index_creative_song_roles_on_creative_id` (`creative_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `creatives`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creatives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `creativeable_id` int(10) unsigned DEFAULT NULL,
  `creativeable_type` enum('Album','Song','Video') DEFAULT NULL,
  `artist_id` int(10) unsigned NOT NULL,
  `role` varchar(20) NOT NULL DEFAULT 'primary_artist',
  `person_id` int(11) DEFAULT NULL,
  `curated_artist_flag` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `creativeable_type` (`creativeable_type`,`creativeable_id`,`role`,`artist_id`),
  KEY `artist_id` (`artist_id`,`creativeable_type`,`creativeable_id`,`role`),
  KEY `creativeable_type_2` (`creativeable_type`,`creativeable_id`,`artist_id`),
  KEY `index_creatives_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `credit_usages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credit_usages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL,
  `related_type` varchar(255) DEFAULT NULL,
  `applies_to_type` varchar(255) DEFAULT NULL,
  `finalized_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `plan_credit` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `index_credit_usages_on_person_id` (`person_id`),
  KEY `index_credit_usages_on_related_type_and_related_id` (`related_type`,`related_id`),
  KEY `index_credit_usages_on_plan_credit` (`plan_credit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `current_isrc_duplicates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `current_isrc_duplicates` (
  `isrc` varchar(12) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  PRIMARY KEY (`isrc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `current_tax_forms`;
/*!50001 DROP VIEW IF EXISTS `current_tax_forms`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `current_tax_forms` (
  `tax_form_id` tinyint NOT NULL,
  `person_id` tinyint NOT NULL,
  `program_id` tinyint NOT NULL,
  `tax_form_type_id` tinyint NOT NULL,
  `submitted_at` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `data_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `data_report_id` int(11) NOT NULL,
  `resolution_unit` varchar(10) NOT NULL,
  `resolution_identifier` varchar(20) NOT NULL,
  `measure` float NOT NULL,
  `description` varchar(150) NOT NULL,
  `resolution_display` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_records_four_index` (`data_report_id`,`resolution_identifier`,`description`),
  KEY `index_data_records_on_data_report_id` (`data_report_id`),
  KEY `index_data_records_on_resolution_unit` (`resolution_unit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `data_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `resolution` varchar(255) DEFAULT NULL,
  `live_update_at` datetime DEFAULT NULL,
  `live_update_limit` int(11) DEFAULT 1440,
  `start_on` date NOT NULL,
  `last_calculated_on` date DEFAULT NULL,
  `updateable_past` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ddex_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ddex_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resource_contributor_role` tinyint(1) DEFAULT NULL,
  `indirect_contributor_role` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ddex_song_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ddex_song_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `song_role_id` int(11) DEFAULT NULL,
  `ddex_role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_ddex_song_roles_on_song_role_id` (`song_role_id`),
  KEY `index_ddex_song_roles_on_ddex_role_id` (`ddex_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `default_variable_prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `default_variable_prices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `variable_price_store_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_default_variable_prices_on_store_id` (`store_id`),
  UNIQUE KEY `index_default_variable_prices_on_variable_price_store_id` (`variable_price_store_id`),
  CONSTRAINT `default_variable_prices_ibfk_1` FOREIGN KEY (`variable_price_store_id`) REFERENCES `variable_prices_stores` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `deleted_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deleted_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `delete_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `admin_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_deleted_accounts_on_person_id` (`person_id`),
  KEY `index_deleted_accounts_on_admin_id` (`admin_id`),
  CONSTRAINT `fk_rails_0bfb875486` FOREIGN KEY (`admin_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `delivery_batch_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delivery_batch_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deliverable_id` int(11) DEFAULT NULL,
  `deliverable_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_batch_id` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'present',
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_delivery_batch_items_on_deliverable_id` (`deliverable_id`),
  KEY `index_delivery_batch_items_on_deliverable_type` (`deliverable_type`),
  KEY `index_delivery_batch_items_on_delivery_batch_id` (`delivery_batch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `delivery_batches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delivery_batches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) DEFAULT NULL,
  `batch_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `count` int(11) DEFAULT 0,
  `active` tinyint(1) DEFAULT NULL,
  `max_batch_size` int(11) DEFAULT 100,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `processed_count` int(11) DEFAULT 0,
  `batch_complete_sent_at` datetime DEFAULT NULL,
  `currently_closing` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `index_delivery_batches_on_store_id` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `delivery_manifest_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delivery_manifest_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `delivery_batch_item_id` int(11) DEFAULT NULL,
  `xml_remote_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uuid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `xml_hashsum` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `takedown` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `derived_media_assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `derived_media_assets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `width` smallint(6) unsigned DEFAULT NULL COMMENT 'Image width',
  `height` smallint(6) unsigned DEFAULT NULL COMMENT 'Image Height',
  `bit_depth` tinyint(4) unsigned DEFAULT NULL COMMENT 'Bit Depth',
  `size` int(10) unsigned NOT NULL COMMENT 'Size of file in bytes',
  `s3_asset_id` int(10) unsigned DEFAULT NULL COMMENT 'Foreign key to sc_assets table.',
  `media_asset_id` int(10) unsigned DEFAULT NULL COMMENT 'Foreign key to media_assets table.',
  `mime_type` enum('image/jpeg','image/png','image/tiff','image/bmp','image/gif') NOT NULL COMMENT 'Asset mime type.',
  `created_at` datetime NOT NULL COMMENT 'Date record Created.',
  `updated_at` datetime NOT NULL COMMENT 'Date record Updated.',
  PRIMARY KEY (`id`),
  KEY `media_asset_id` (`media_asset_id`),
  KEY `s3_asset_id` (`s3_asset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `direct_advance_earnings_period_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `direct_advance_earnings_period_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `amount` decimal(23,2) DEFAULT NULL,
  `period` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `report_run_on` date DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_direct_advance_earnings_period_records_on_person_id` (`person_id`),
  KEY `index_direct_advance_earnings_period_records_on_report_run_on` (`report_run_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `direct_advance_eligibility_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `direct_advance_eligibility_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `eligible` tinyint(1) DEFAULT NULL,
  `report_run_on` date DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_direct_advance_eligibility_records_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `disabled_features_country_websites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disabled_features_country_websites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feature_id` int(11) DEFAULT NULL,
  `country_website_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `disabled_features_partners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disabled_features_partners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feature_id` int(11) NOT NULL,
  `partner_id` smallint(5) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_unique_disabled_features_partners_ids` (`feature_id`,`partner_id`),
  KEY `partner_id` (`partner_id`),
  CONSTRAINT `disabled_features_partners_ibfk_1` FOREIGN KEY (`feature_id`) REFERENCES `features` (`id`) ON DELETE CASCADE,
  CONSTRAINT `disabled_features_partners_ibfk_2` FOREIGN KEY (`partner_id`) REFERENCES `partners` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dispute_audits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dispute_audits` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dispute_id` bigint(20) NOT NULL,
  `note` text DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_dispute_audits_on_dispute_id` (`dispute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dispute_evidences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dispute_evidences` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dispute_id` bigint(20) NOT NULL,
  `submitted_by_id` bigint(20) NOT NULL,
  `text_evidence` text DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `file_evidence_file_name` varchar(255) DEFAULT NULL,
  `file_evidence_content_type` varchar(255) DEFAULT NULL,
  `file_evidence_file_size` int(11) DEFAULT NULL,
  `file_evidence_updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_dispute_evidences_on_dispute_id` (`dispute_id`),
  KEY `index_dispute_evidences_on_submitted_by_id` (`submitted_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dispute_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dispute_statuses` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `source_type` enum('braintree','paypal','payu') DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `disputes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disputes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `source_type` varchar(255) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  `transaction_identifier` varchar(255) DEFAULT NULL,
  `dispute_identifier` varchar(255) DEFAULT NULL,
  `action_type` enum('accepted','dispute_initiated','dispute_completed') DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `amount_cents` int(11) DEFAULT 0,
  `currency` varchar(255) DEFAULT NULL,
  `processed_by_id` bigint(20) DEFAULT NULL,
  `dispute_status_id` bigint(20) NOT NULL,
  `refund_id` bigint(20) DEFAULT NULL,
  `person_id` bigint(20) NOT NULL,
  `invoice_id` bigint(20) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `initiated_at` datetime DEFAULT current_timestamp(),
  `payload_currency` varchar(255) DEFAULT NULL,
  `payload_amount` float DEFAULT NULL,
  `foreign_exchange_rate_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_disputes_on_processed_by_id` (`processed_by_id`),
  KEY `index_disputes_on_dispute_status_id` (`dispute_status_id`),
  KEY `index_disputes_on_person_id` (`person_id`),
  KEY `index_disputes_on_invoice_id` (`invoice_id`),
  KEY `index_disputes_on_foreign_exchange_rate_id` (`foreign_exchange_rate_id`),
  CONSTRAINT `fk_rails_7002896230` FOREIGN KEY (`foreign_exchange_rate_id`) REFERENCES `foreign_exchange_rates` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `distribution_api_albums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distribution_api_albums` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `distribution_api_service_id` bigint(20) NOT NULL,
  `album_id` int(11) NOT NULL,
  `source_album_id` varchar(255) NOT NULL,
  `source_user_id` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `source_contentreview_notes` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_distribution_api_albums_on_album_id` (`album_id`),
  UNIQUE KEY `index_distribution_api_albums_on_source_album_id_and_service_id` (`distribution_api_service_id`,`source_album_id`),
  KEY `index_distribution_api_albums_on_distribution_api_service_id` (`distribution_api_service_id`),
  CONSTRAINT `fk_rails_728a82339e` FOREIGN KEY (`distribution_api_service_id`) REFERENCES `distribution_api_services` (`id`),
  CONSTRAINT `fk_rails_bb3eff7e9e` FOREIGN KEY (`album_id`) REFERENCES `albums` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `distribution_api_external_service_id_api_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distribution_api_external_service_id_api_statuses` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `album_id` int(11) NOT NULL,
  `distribution_api_service_id` bigint(20) NOT NULL,
  `creation_date` date DEFAULT NULL,
  `response_status` text DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_distribution_api_esid_statuses_on_album_id` (`album_id`),
  KEY `index_distribution_api_esid_statuses_on_distribution_api_service` (`distribution_api_service_id`),
  CONSTRAINT `fk_rails_05e3dbc2f2` FOREIGN KEY (`distribution_api_service_id`) REFERENCES `distribution_api_services` (`id`),
  CONSTRAINT `fk_rails_3ff52dc479` FOREIGN KEY (`album_id`) REFERENCES `albums` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `distribution_api_ingestion_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distribution_api_ingestion_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `distribution_api_service_id` bigint(20) NOT NULL,
  `source_album_id` varchar(255) NOT NULL,
  `json_path` varchar(255) NOT NULL,
  `message_id` varchar(255) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `response_json` text DEFAULT NULL,
  `response_json_received` text DEFAULT NULL,
  `delivery_type` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `source_contentreview_notes` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_distribution_api_ingestion_logs_on_json_path` (`json_path`),
  KEY `index_distribution_api_ingestion_logs_on_service_id` (`distribution_api_service_id`),
  KEY `index_distribution_api_ingestion_logs_on_source_album_id` (`source_album_id`),
  CONSTRAINT `fk_rails_14863df77b` FOREIGN KEY (`distribution_api_service_id`) REFERENCES `distribution_api_services` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `distribution_api_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distribution_api_services` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_distribution_api_services_on_uuid` (`uuid`),
  UNIQUE KEY `index_distribution_api_services_on_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `distribution_api_songs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distribution_api_songs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `distribution_api_album_id` bigint(20) NOT NULL,
  `song_id` int(11) NOT NULL,
  `source_song_id` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_distribution_api_songs_on_song_id` (`song_id`),
  KEY `index_distribution_api_songs_on_distribution_api_album_id` (`distribution_api_album_id`),
  KEY `index_distribution_api_songs_on_source_song_id` (`source_song_id`),
  CONSTRAINT `fk_rails_9568720215` FOREIGN KEY (`distribution_api_album_id`) REFERENCES `distribution_api_albums` (`id`),
  CONSTRAINT `fk_rails_b44b0e0210` FOREIGN KEY (`song_id`) REFERENCES `songs` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `distribution_batch_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distribution_batch_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `distribution_id` int(11) DEFAULT NULL,
  `distribution_batch_id` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'present',
  PRIMARY KEY (`id`),
  KEY `index_distribution_batch_items_on_distribution_id` (`distribution_id`),
  KEY `index_distribution_batch_items_on_distribution_batch_id` (`distribution_batch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `distribution_batches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distribution_batches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) DEFAULT NULL,
  `batch_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `count` int(11) DEFAULT 0,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_distribution_batches_on_store_id` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `distribution_errors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distribution_errors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `distribution_songs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distribution_songs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(25) NOT NULL,
  `distribution_id` int(11) NOT NULL,
  `salepoint_song_id` int(11) NOT NULL,
  `sqs_message_id` varchar(100) DEFAULT NULL,
  `retry_count` int(11) DEFAULT NULL,
  `delivery_type` varchar(25) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_distribution_songs_on_distribution_id` (`distribution_id`),
  KEY `index_distribution_songs_on_salepoint_song_id` (`salepoint_song_id`),
  KEY `index_distribution_songs_on_sqs_message_id` (`sqs_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `distributions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distributions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(255) NOT NULL DEFAULT '',
  `converter_class` varchar(255) NOT NULL DEFAULT '',
  `petri_bundle_id` int(11) NOT NULL DEFAULT 0,
  `sqs_message_id` varchar(255) DEFAULT NULL,
  `retry_count` int(11) DEFAULT 0,
  `max_retries` int(11) DEFAULT 10,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `delivery_type` varchar(255) DEFAULT NULL,
  `last_delivery_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_distributions_on_sqs_message_id` (`sqs_message_id`),
  KEY `index_distributions_on_petri_bundle_id` (`petri_bundle_id`),
  KEY `index_distributions_on_state` (`state`),
  KEY `index_distributions_on_converter_class` (`converter_class`),
  KEY `index_distributions_on_updated_at` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `distributions_salepoints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distributions_salepoints` (
  `distribution_id` int(11) NOT NULL DEFAULT 0,
  `salepoint_id` int(11) NOT NULL DEFAULT 0,
  UNIQUE KEY `index_distributions_salepoints_distribution_id_salepoint_id` (`distribution_id`,`salepoint_id`),
  KEY `index_distributions_salepoints_on_distribution_id` (`distribution_id`),
  KEY `index_distributions_salepoints_on_salepoint_id` (`salepoint_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `document_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `document_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `document_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `raw_response` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_revision_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `language_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_document_templates_on_document_type` (`document_type`),
  KEY `index_document_templates_on_language_code_and_role` (`language_code`,`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `downgrade_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downgrade_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `downgrade_category_reasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downgrade_category_reasons` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `downgrade_category_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_downgrade_category_reasons_on_downgrade_category_id` (`downgrade_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `downgrade_other_reasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downgrade_other_reasons` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `plan_downgrade_request_id` bigint(20) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_downgrade_other_reasons_on_plan_downgrade_request_id` (`plan_downgrade_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `download_trackings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `download_trackings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `downloaded_at` datetime NOT NULL,
  `promotion` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `duplicated_isrcs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `duplicated_isrcs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `song_id` int(11) NOT NULL DEFAULT 0,
  `tunecore_isrc` varchar(12) DEFAULT NULL,
  `corrected_at` datetime DEFAULT NULL,
  `adjusted_isrc` varchar(12) DEFAULT NULL,
  `tunecore_upc` varchar(32) DEFAULT NULL,
  `optional_upc` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_duplicated_isrcs_on_song_id` (`song_id`),
  KEY `index_duplicated_isrcs_on_tunecore_isrc` (`tunecore_isrc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `eft_batch_transaction_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eft_batch_transaction_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `eft_batch_transaction_id` int(10) unsigned NOT NULL COMMENT 'Foreign Key to batch_transaction table',
  `amount` decimal(10,2) DEFAULT NULL COMMENT 'Amount',
  `currency` char(3) DEFAULT NULL COMMENT 'Currency',
  `status` enum('requested','debit_amount','debit_service_fee','canceled','rejected','rollback_debit','rollback_service_fee','approved','removed_approval','processing','error','sent_to_bank','success','failure','debit_failure_fee','email_sent') NOT NULL DEFAULT 'requested' COMMENT 'Current State of process',
  `person_transaction_id` int(11) DEFAULT NULL COMMENT 'Foreign Key to Person Tranasctions table',
  `posted_by_id` int(10) unsigned DEFAULT NULL COMMENT 'Foreign Key to People table',
  `posted_by_name` varchar(255) DEFAULT NULL COMMENT 'Admin name who approved/rejected/unapproved',
  `comment` varchar(255) DEFAULT NULL COMMENT 'Generic Text field used for responses, comments, etc.',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  PRIMARY KEY (`id`),
  KEY `eftbh_eft_batch_transaction_id` (`eft_batch_transaction_id`),
  KEY `eftbh_person_transaction_id` (`person_transaction_id`),
  KEY `eftbh_posted_by_id` (`posted_by_id`),
  CONSTRAINT `FK_eft_history_eft_batch_transaction` FOREIGN KEY (`eft_batch_transaction_id`) REFERENCES `eft_batch_transactions` (`id`),
  CONSTRAINT `FK_eft_history_person_transaction` FOREIGN KEY (`person_transaction_id`) REFERENCES `person_transactions` (`id`),
  CONSTRAINT `FK_eft_history_posted_by_id` FOREIGN KEY (`posted_by_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `eft_batch_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eft_batch_transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `country_website_id` int(11) unsigned DEFAULT NULL COMMENT 'Foreign Key to country_websites',
  `transaction_id` bigint(20) unsigned DEFAULT NULL COMMENT 'References transaction ID stored at Braintree',
  `order_id` varchar(35) DEFAULT NULL COMMENT 'Unique Order ID used by braintree (currently eft+id)',
  `eft_batch_id` int(10) unsigned DEFAULT NULL COMMENT 'Foreign Key to eft_batch table',
  `eft_query_id` int(10) unsigned DEFAULT NULL COMMENT 'Foreign Key to eft_query table',
  `stored_bank_account_id` int(10) unsigned DEFAULT NULL COMMENT 'Foreign Key to stored bank account table (for braintree transactions)',
  `payout_service_fee_id` int(10) unsigned DEFAULT NULL COMMENT 'Foreign Key to payout service fee tables',
  `failure_fee_id` int(10) unsigned DEFAULT NULL COMMENT 'Foreign Key to payout service fee tables',
  `amount` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT 'Amount',
  `currency` char(3) NOT NULL COMMENT 'Currency',
  `status` enum('pending_approval','waiting_for_batch','canceled','rejected','processing_in_batch','sent_to_bank','error','success','failure') NOT NULL DEFAULT 'pending_approval' COMMENT 'Current State of process',
  `response_code` char(4) DEFAULT NULL COMMENT 'Response Code returned by Braintree',
  `response_text` varchar(255) DEFAULT NULL COMMENT 'Response Text returned by Braintree',
  `email_message` text DEFAULT NULL COMMENT 'Email message added by admin',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  `suspicious_flags` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `eftb_eft_batch_id` (`eft_batch_id`),
  KEY `eftb_eft_query_id` (`eft_query_id`),
  KEY `eftb_stored_bank_account_id` (`stored_bank_account_id`),
  KEY `eftb_payout_service_fee_id` (`payout_service_fee_id`),
  KEY `eftb_failure_fee_id` (`payout_service_fee_id`),
  KEY `FK_eft_txn_failure_fee` (`failure_fee_id`),
  KEY `ebt_country_website_id` (`country_website_id`),
  CONSTRAINT `FK_eft_txn_eft_batch` FOREIGN KEY (`eft_batch_id`) REFERENCES `eft_batches` (`id`),
  CONSTRAINT `FK_eft_txn_eft_query` FOREIGN KEY (`eft_query_id`) REFERENCES `eft_queries` (`id`),
  CONSTRAINT `FK_eft_txn_failure_fee` FOREIGN KEY (`failure_fee_id`) REFERENCES `payout_service_fees` (`id`),
  CONSTRAINT `FK_eft_txn_payout_service_fee` FOREIGN KEY (`payout_service_fee_id`) REFERENCES `payout_service_fees` (`id`),
  CONSTRAINT `FK_eft_txn_stored_bank_account` FOREIGN KEY (`stored_bank_account_id`) REFERENCES `stored_bank_accounts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `eft_batches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eft_batches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary KEy',
  `country_website_id` int(11) unsigned NOT NULL,
  `currency` char(3) NOT NULL COMMENT 'Currency',
  `total_amount` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT 'Total Amount in Batch',
  `errored_amount` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT 'Total Amount for transactions that error out on the initial sent to braintree',
  `sent_to_bank_amount` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT 'Total Amount for BrainTree in Batch',
  `batch_file_name` varchar(200) DEFAULT NULL COMMENT 'Name of physical csv file saved on server, sent to braintree',
  `batch_sent_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime file sent to braintree',
  `response_processed_at` datetime DEFAULT NULL COMMENT 'Datetime file received back from braintree',
  `response_file_name` varchar(200) DEFAULT NULL COMMENT 'Name of physical csv file saved on server, received from Braintree',
  `status` enum('complete','uploaded','new','upload_confirmed') NOT NULL DEFAULT 'new' COMMENT 'Current State',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  PRIMARY KEY (`id`),
  KEY `eb_country_website_id` (`country_website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `eft_queries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eft_queries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `raw_query` text DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `email_change_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_change_requests` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) NOT NULL,
  `token` varchar(255) NOT NULL,
  `old_email` varchar(255) NOT NULL,
  `new_email` varchar(255) NOT NULL,
  `finalized` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_email_change_requests_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `encumbrance_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `encumbrance_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `encumbrance_summary_id` int(11) NOT NULL,
  `transaction_date` date NOT NULL,
  `transaction_amount` decimal(12,2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_encumbrance_details_on_encumbrance_summary_id` (`encumbrance_summary_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `encumbrance_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `encumbrance_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference_source` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `option_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `option_value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_encumbrance_options_on_reference_source_and_option_name` (`reference_source`,`option_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `encumbrance_summaries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `encumbrance_summaries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `reference_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reference_source` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_amount` decimal(12,2) NOT NULL,
  `fee_amount` decimal(12,2) NOT NULL,
  `outstanding_amount` decimal(12,2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_encumbrance_summaries_on_reference_id_and_reference_source` (`reference_id`,`reference_source`),
  KEY `index_encumbrance_summaries_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `engine_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `engine_rules` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `config` longtext DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT 0,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_engine_rules_on_category_and_name` (`category`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `entitlement_rights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entitlement_rights` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entitlement_rights_group_id` int(10) unsigned NOT NULL COMMENT 'link to parent group',
  `controller` varchar(40) NOT NULL COMMENT 'name of controller this rights entry has access to',
  `action` varchar(255) NOT NULL COMMENT 'name of action in controller this rights entry has permission to complete',
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `entitlement_rights_group_id` (`entitlement_rights_group_id`),
  CONSTRAINT `fk_entitlement_rights_group` FOREIGN KEY (`entitlement_rights_group_id`) REFERENCES `entitlement_rights_groups` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `entitlement_rights_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entitlement_rights_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL COMMENT 'name of grouped rights',
  `description` varchar(255) NOT NULL COMMENT 'description of access rights for offered product',
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `entitlements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entitlements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entitlement_rights_group_id` int(10) unsigned DEFAULT NULL COMMENT 'link to system rights provided to the inidividual',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `entitlement_rights_group` (`entitlement_rights_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `external_assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `external_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `external_service_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `price` decimal(10,0) DEFAULT 0,
  `expires_at` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_external_products_on_external_service_id` (`external_service_id`),
  KEY `index_external_products_on_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `external_purchase_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_purchase_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `related_item_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `related_item_id` int(11) DEFAULT NULL,
  `external_purchase_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_index_related_items` (`related_item_type`,`related_item_id`),
  KEY `index_external_purchase_items_on_external_purchase_id` (`external_purchase_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `external_purchases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_purchases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `external_product_id` int(11) DEFAULT NULL,
  `final_cents` int(11) DEFAULT NULL,
  `currency` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `paid_at` datetime DEFAULT NULL,
  `invoice_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_external_purchases_on_person_id` (`person_id`),
  KEY `index_external_purchases_on_external_product_id` (`external_product_id`),
  KEY `index_external_purchases_on_paid_at` (`paid_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `external_service_id_artworks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_service_id_artworks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `asset_file_name` varchar(255) DEFAULT NULL,
  `asset_content_type` varchar(255) DEFAULT NULL,
  `asset_file_size` int(11) DEFAULT NULL,
  `asset_updated_at` datetime DEFAULT NULL,
  `asset_fingerprint` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `external_service_id_deletes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_service_id_deletes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) DEFAULT NULL,
  `service_name` varchar(255) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `artist_name` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `external_service_id_deletes_identifier_index` (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `external_service_ids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_service_ids` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `linkable_id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `linkable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `external_service_id_artwork_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `verified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_external_service_ids_on_linkable_and_service_name` (`linkable_id`,`linkable_type`,`service_name`),
  KEY `index_external_service_ids_on_linkable_id_and_linkable_type` (`linkable_id`,`linkable_type`),
  KEY `index_external_service_ids_on_external_service_id_artwork_id` (`external_service_id_artwork_id`),
  KEY `external_service_ids_identifier_index` (`identifier`),
  KEY `external_service_ids_service_name_index` (`service_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `external_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `external_services_people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_services_people` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `external_service_id` int(11) DEFAULT NULL,
  `account_id` varchar(255) DEFAULT NULL,
  `access_token` varchar(255) DEFAULT NULL,
  `access_token_expiry` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_external_services_people_on_person_id` (`person_id`),
  KEY `index_external_services_people_on_external_service_id` (`external_service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `fans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `widget_id` int(11) DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `fan_type` enum('Fan','FreeSongFan') DEFAULT 'Fan',
  `unsubscribed_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `widget_id` (`widget_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `feature_blacklists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feature_blacklists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `feature` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_feature_blacklists_on_person_id_and_feature` (`person_id`,`feature`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `restrict_by_user` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `features_people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `features_people` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feature_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_unique_features_people_ids` (`feature_id`,`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `flag_reasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flag_reasons` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `flag_id` bigint(20) DEFAULT NULL,
  `reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_flag_reasons_on_flag_id` (`flag_id`),
  CONSTRAINT `fk_rails_09e8506a68` FOREIGN KEY (`flag_id`) REFERENCES `flags` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `flag_transitions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flag_transitions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `updated_by` int(11) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `flag_id` int(11) DEFAULT NULL,
  `add_remove` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `flag_reason_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_flag_transitions_on_updated_by_and_person_id_and_flag_id` (`updated_by`,`person_id`,`flag_id`),
  KEY `index_flag_transitions_on_flag_reason_id` (`flag_reason_id`),
  CONSTRAINT `fk_rails_9bd21e584d` FOREIGN KEY (`flag_reason_id`) REFERENCES `flag_reasons` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `flags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flags` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_flags_on_category_and_name` (`category`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `foreign_exchange_pegged_rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreign_exchange_pegged_rates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pegged_rate` float NOT NULL,
  `country_id` int(11) NOT NULL,
  `currency` char(3) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `country_website_id` int(11) NOT NULL,
  `note` text DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_foreign_exchange_pegged_rates_on_country_id` (`country_id`),
  KEY `index_foreign_exchange_pegged_rates_on_currency` (`currency`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `foreign_exchange_rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foreign_exchange_rates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `exchange_rate` float NOT NULL,
  `source_currency` char(3) NOT NULL,
  `target_currency` char(3) NOT NULL,
  `valid_from` datetime DEFAULT NULL,
  `valid_till` datetime DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `friend_referral_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `friend_referral_campaigns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `campaign_url` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `campaign_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `friend_referral_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `friend_referral_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `event_type` varchar(255) DEFAULT NULL,
  `raw_request` text DEFAULT NULL,
  `raw_response` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `friend_referrals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `friend_referrals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `referrer_id` int(11) DEFAULT NULL,
  `referee_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `commission` decimal(10,2) DEFAULT 0.00,
  `balance` decimal(10,2) DEFAULT 0.00,
  `status` varchar(255) DEFAULT NULL,
  `posting_id` int(11) DEFAULT NULL,
  `posting_type` varchar(255) DEFAULT NULL,
  `error_message` varchar(255) DEFAULT NULL,
  `raw_response` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `first_referral` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `genre_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genre_permissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `store_id` smallint(6) NOT NULL,
  `country_id` smallint(6) NOT NULL,
  `album_genre_whitelist_id` bigint(20) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_genre_permissions_on_store_id` (`store_id`),
  KEY `index_genre_permissions_on_country_id` (`country_id`),
  KEY `index_genre_permissions_on_album_genre_whitelist_id` (`album_genre_whitelist_id`),
  CONSTRAINT `fk_rails_9b1e212e55` FOREIGN KEY (`album_genre_whitelist_id`) REFERENCES `album_genre_whitelists` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `genres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `parent_id` int(3) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `you_tube_eligible` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `genres_videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genres_videos` (
  `genre_id` int(11) DEFAULT NULL,
  `video_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `gst_configs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gst_configs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cgst` float DEFAULT NULL,
  `sgst` float DEFAULT NULL,
  `igst` float DEFAULT NULL,
  `effective_from` datetime DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `gst_infos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gst_infos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) DEFAULT NULL,
  `gstin` char(15) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_gst_infos_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `high_earning_composers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `high_earning_composers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `attempt_to_opt_out` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_high_earning_composers_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `hubspot_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hubspot_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `immersive_audios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `immersive_audios` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `song_id` int(11) DEFAULT NULL,
  `s3_asset_id` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `content_fingerprint` varchar(255) DEFAULT NULL,
  `audio_type` enum('dolby_atmos') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_immersive_audios_on_song_id_and_s3_asset_id` (`song_id`,`s3_asset_id`),
  KEY `index_immersive_audios_on_song_id` (`song_id`),
  KEY `index_immersive_audios_on_s3_asset_id` (`s3_asset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ineligibility_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ineligibility_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `operator` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_ineligibility_rules_on_store_id` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `inventories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL COMMENT 'references people.id',
  `title` varchar(30) NOT NULL COMMENT 'the title to display',
  `product_item_id` int(10) unsigned NOT NULL COMMENT 'references product_items.id',
  `purchase_id` int(10) unsigned DEFAULT NULL COMMENT 'references purchases.id',
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT 'self-referential, ties certain groups of inventory records together for usage',
  `inventory_type` enum('Album','Single','Ringtone','MusicVideo','FeatureFilm','ItunesUserReport','Booklet','Song','Salepoint','Widget','Video','Composer') DEFAULT NULL,
  `quantity` mediumint(9) NOT NULL DEFAULT 0 COMMENT 'initial quantity purchased by a customer',
  `quantity_used` mediumint(9) NOT NULL DEFAULT 0 COMMENT 'cached quantity used by customer interaction with the system',
  `quantity_adjustment` int(10) NOT NULL DEFAULT 0 COMMENT 'quantity adjustment',
  `unlimited` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'flag for determining if an unlimited number of this item can be used',
  `expires_at` timestamp NULL DEFAULT NULL COMMENT 'date at which the inventory item is no longer available to the user',
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `purchase_id` (`purchase_id`),
  KEY `entitlement_type` (`inventory_type`),
  KEY `parent_id` (`parent_id`),
  KEY `product_item_inventories_id` (`product_item_id`),
  CONSTRAINT `fk_person_inventories_id` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`),
  CONSTRAINT `fk_product_inventories_id` FOREIGN KEY (`product_item_id`) REFERENCES `product_items` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `inventory_adjustments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_adjustments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `inventory_id` int(10) unsigned NOT NULL COMMENT 'Foreign Key to Inventory Table',
  `related_id` int(10) unsigned DEFAULT NULL COMMENT 'Self foreign key',
  `rollback` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT 'Indicated if record is rollback',
  `posted_by_id` int(10) unsigned NOT NULL COMMENT 'Foreign Key to people table',
  `posted_by_name` varchar(255) DEFAULT NULL COMMENT 'Name of admin for auditing purposes',
  `amount` int(10) NOT NULL DEFAULT 0 COMMENT 'Amount to credit account',
  `category` enum('Other','Refund','Error') NOT NULL DEFAULT 'Other' COMMENT 'Type of adjustment',
  `admin_note` varchar(255) DEFAULT NULL COMMENT 'Adjustment note for Admin only',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  PRIMARY KEY (`id`),
  KEY `inventory_id` (`inventory_id`),
  KEY `related_id` (`related_id`),
  KEY `posted_by_id` (`posted_by_id`),
  CONSTRAINT `FK_inv_adj_people_by` FOREIGN KEY (`posted_by_id`) REFERENCES `people` (`id`),
  CONSTRAINT `FK_inv_adj_self` FOREIGN KEY (`related_id`) REFERENCES `inventory_adjustments` (`id`),
  CONSTRAINT `FK_inventory` FOREIGN KEY (`inventory_id`) REFERENCES `inventories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `inventory_usages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_usages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `inventory_id` int(10) unsigned NOT NULL COMMENT 'references the inventory line-item used',
  `related_type` enum('Album','Single','Ringtone','Song','Salepoint','Video','Booklet','ItunesUserReport','ShippingLabel','Widget','Composer') DEFAULT NULL COMMENT 'the model name of the related inventory used',
  `related_id` int(10) unsigned NOT NULL COMMENT 'the id of the related model',
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `inventory_id` (`inventory_id`),
  KEY `related_type_related_id` (`related_type`,`related_id`),
  CONSTRAINT `fk_inventory_id` FOREIGN KEY (`inventory_id`) REFERENCES `inventories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `invoice_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL DEFAULT 0,
  `target_id` int(11) NOT NULL DEFAULT 0,
  `target_type` varchar(30) NOT NULL DEFAULT '',
  `cost_at_settlement_cents` int(11) DEFAULT NULL,
  `currency` char(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency for invoice item',
  PRIMARY KEY (`id`),
  KEY `invoice_item_details` (`invoice_id`,`target_id`,`target_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `invoice_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `batch_transaction_id` int(11) DEFAULT NULL,
  `payment_batch_id` int(11) DEFAULT NULL,
  `braintree_transaction_id` int(11) DEFAULT NULL,
  `paypal_transaction_id` int(11) DEFAULT NULL,
  `stored_paypal_account_id` int(11) DEFAULT NULL,
  `renewal_id` int(11) DEFAULT NULL,
  `current_method_name` varchar(255) DEFAULT NULL,
  `caller_method_path` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `invoice_created_at` datetime DEFAULT NULL,
  `invoice_final_settlement_amount_cents` int(11) DEFAULT NULL,
  `invoice_currency` char(3) DEFAULT NULL,
  `invoice_batch_status` enum('visible_to_customer','waiting_for_batch','processing_in_batch') DEFAULT NULL,
  `invoice_settled_at` datetime DEFAULT NULL,
  `invoice_vat_amount_cents` int(11) DEFAULT NULL,
  `invoice_foreign_exchange_pegged_rate_id` int(11) DEFAULT NULL,
  `purchase_created_at` datetime DEFAULT NULL,
  `purchase_product_id` int(11) DEFAULT NULL,
  `purchase_cost_cents` int(11) DEFAULT NULL,
  `purchase_discount_cents` int(11) DEFAULT NULL,
  `purchase_currency` char(3) DEFAULT NULL,
  `purchase_paid_at` datetime DEFAULT NULL,
  `purchase_salepoint_id` int(11) DEFAULT NULL,
  `purchase_no_purchase_album_id` int(11) DEFAULT NULL,
  `purchase_related_id` int(11) DEFAULT NULL,
  `purchase_related_type` varchar(255) DEFAULT NULL,
  `purchase_targeted_product_id` int(11) DEFAULT NULL,
  `purchase_price_adjustment_histories_count` int(11) DEFAULT NULL,
  `purchase_vat_amount_cents` int(11) DEFAULT NULL,
  `purchase_status` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `payments_os_transaction_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_invoice_logs_on_person_id` (`person_id`),
  KEY `index_invoice_logs_on_invoice_id` (`invoice_id`),
  KEY `index_invoice_logs_on_purchase_id` (`purchase_id`),
  KEY `purchase_related_on_related_type` (`purchase_related_id`,`purchase_related_type`),
  KEY `index_invoice_logs_on_current_method_name` (`current_method_name`),
  KEY `index_invoice_logs_on_caller_method_path` (`caller_method_path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `invoice_settlements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_settlements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `settlement_amount_cents` int(11) NOT NULL DEFAULT 0,
  `currency` char(3) NOT NULL COMMENT 'Currency for invoice settlement',
  `source_id` int(11) NOT NULL DEFAULT 0,
  `source_type` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `invoice_settlements_invoice_id_index` (`invoice_id`),
  KEY `source_id_idx` (`source_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `invoice_static_corporate_entities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_static_corporate_entities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `related_id` int(11) NOT NULL,
  `related_type` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `vat_registration_number` varchar(255) DEFAULT NULL,
  `crn` varchar(255) DEFAULT NULL,
  `paying_bank` varchar(255) DEFAULT NULL,
  `rib` varchar(255) DEFAULT NULL,
  `bic` varchar(255) DEFAULT NULL,
  `iban` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `inbound_invoice_prefix` varchar(255) DEFAULT NULL,
  `credit_note_inbound_prefix` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_corporate_entities_on_related_id_and_related_type` (`related_id`,`related_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `invoice_static_customer_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_static_customer_addresses` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `related_id` int(11) NOT NULL,
  `related_type` varchar(255) NOT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_customer_addresses_on_related_id_and_related_type` (`related_id`,`related_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `invoice_static_customer_infos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_static_customer_infos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `related_id` int(11) NOT NULL,
  `related_type` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `person_currency` varchar(255) DEFAULT NULL,
  `customer_type` varchar(255) DEFAULT NULL,
  `vat_registration_number` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `enterprise_number` varchar(255) DEFAULT NULL,
  `company_registry_data` varchar(255) DEFAULT NULL,
  `place_of_legal_seat` varchar(255) DEFAULT NULL,
  `registered_share_capital` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_customer_info_on_related_id_and_related_type` (`related_id`,`related_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `final_settlement_amount_cents` int(11) DEFAULT NULL,
  `currency` char(3) NOT NULL COMMENT 'Currency for invoice',
  `batch_status` enum('visible_to_customer','waiting_for_batch','processing_in_batch') DEFAULT 'visible_to_customer',
  `settled_at` datetime DEFAULT NULL,
  `vat_amount_cents` int(11) DEFAULT 0,
  `foreign_exchange_pegged_rate_id` int(11) DEFAULT NULL,
  `gst_config_id` bigint(20) DEFAULT NULL,
  `gst_info_id` bigint(20) DEFAULT NULL,
  `vat_amount_cents_in_eur` int(11) DEFAULT 0,
  `invoice_sequence` int(11) DEFAULT NULL,
  `invoice_sent_at` datetime DEFAULT NULL,
  `corporate_entity_id` bigint(20) DEFAULT NULL,
  `foreign_exchange_rate_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_invoices_on_corporate_entity_id_and_invoice_sequence` (`corporate_entity_id`,`invoice_sequence`),
  KEY `invoice_details` (`person_id`,`created_at`,`final_settlement_amount_cents`,`settled_at`),
  KEY `batch_status` (`batch_status`),
  KEY `index_invoices_on_gst_config_id` (`gst_config_id`),
  KEY `index_invoices_on_corporate_entity_id` (`corporate_entity_id`),
  KEY `index_invoices_on_invoice_sequence` (`invoice_sequence`),
  KEY `index_invoices_on_foreign_exchange_rate_id` (`foreign_exchange_rate_id`),
  CONSTRAINT `fk_rails_9682cd84c4` FOREIGN KEY (`foreign_exchange_rate_id`) REFERENCES `foreign_exchange_rates` (`id`),
  CONSTRAINT `fk_rails_ad7b46ad73` FOREIGN KEY (`corporate_entity_id`) REFERENCES `corporate_entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `irs_tax_withholdings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `irs_tax_withholdings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `withheld_from_person_transaction_id` int(11) NOT NULL,
  `irs_transaction_id` bigint(20) DEFAULT NULL,
  `withheld_amount` decimal(23,14) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `foreign_exchange_rate_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_irs_tax_withholdings_on_wft_id` (`withheld_from_person_transaction_id`),
  KEY `idx_irs_tax_withholdings_on_it_id` (`irs_transaction_id`),
  KEY `fk_rails_2ae1b140e7` (`foreign_exchange_rate_id`),
  CONSTRAINT `fk_rails_2ae1b140e7` FOREIGN KEY (`foreign_exchange_rate_id`) REFERENCES `foreign_exchange_rates` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `irs_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `irs_transactions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` enum('pending','reported','completed') DEFAULT 'pending',
  `transaction_code` varchar(255) DEFAULT NULL,
  `transacted_at` datetime DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_irs_transactions_on_transaction_code` (`transaction_code`),
  KEY `index_irs_transactions_on_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `iterable_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `iterable_campaigns` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `environment` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_iterable_campaigns_on_name_and_environment` (`name`,`environment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `itunes_comprehensives`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itunes_comprehensives` (
  `apple_id` varchar(19) DEFAULT NULL,
  `upc` varchar(18) DEFAULT NULL,
  `grid` varchar(2) DEFAULT NULL,
  `vendor_id` varchar(19) DEFAULT NULL,
  `artist` varchar(25) DEFAULT NULL,
  `album` varchar(25) DEFAULT NULL,
  `genre` varchar(25) DEFAULT NULL,
  `total_tracks` int(11) DEFAULT NULL,
  `total_discs` int(11) DEFAULT NULL,
  `is_complete` varchar(7) DEFAULT NULL,
  `itunes_plus_ready` varchar(7) DEFAULT NULL,
  `provider` varchar(15) DEFAULT NULL,
  `label_name` varchar(25) DEFAULT NULL,
  `status` varchar(12) DEFAULT NULL,
  `ww` varchar(1) DEFAULT NULL,
  `au` varchar(1) DEFAULT NULL,
  `at` varchar(1) DEFAULT NULL,
  `be` varchar(1) DEFAULT NULL,
  `ca` varchar(1) DEFAULT NULL,
  `ch` varchar(1) DEFAULT NULL,
  `de` varchar(1) DEFAULT NULL,
  `dk` varchar(1) DEFAULT NULL,
  `es` varchar(1) DEFAULT NULL,
  `fi` varchar(1) DEFAULT NULL,
  `fr` varchar(1) DEFAULT NULL,
  `gb` varchar(1) DEFAULT NULL,
  `gr` varchar(1) DEFAULT NULL,
  `ie` varchar(1) DEFAULT NULL,
  `it` varchar(1) DEFAULT NULL,
  `jp` varchar(1) DEFAULT NULL,
  `lu` varchar(1) DEFAULT NULL,
  `nl` varchar(1) DEFAULT NULL,
  `no` varchar(1) DEFAULT NULL,
  `nz` varchar(1) DEFAULT NULL,
  `pt` varchar(1) DEFAULT NULL,
  `se` varchar(1) DEFAULT NULL,
  `us` varchar(1) DEFAULT NULL,
  `raw_status` varchar(50) DEFAULT NULL,
  `upload_created` datetime DEFAULT NULL,
  `upload_state` varchar(50) DEFAULT NULL,
  `upload_state_id` int(11) DEFAULT NULL,
  `message` text DEFAULT NULL,
  KEY `index_itunes_comprehensives_on_upc` (`upc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `itunes_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itunes_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upc` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `upload_created` datetime DEFAULT NULL,
  `upload_state` varchar(255) DEFAULT NULL,
  `upload_state_id` int(11) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `updated_on` date DEFAULT NULL,
  `prev_status` varchar(50) DEFAULT NULL,
  `prev_status_updated_on` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `knowledgebase_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `knowledgebase_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `article_slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `route_segment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_knowledgebase_links_on_article_slug` (`article_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `labels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `labels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_labels_on_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `language_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language_codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `needs_translated_song_name` tinyint(1) DEFAULT 0,
  `ISO_639_1_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ISO_639_2_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ISO_639_3_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `latest_in_each_adyen_payment_methods`;
/*!50001 DROP VIEW IF EXISTS `latest_in_each_adyen_payment_methods`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `latest_in_each_adyen_payment_methods` (
  `id` tinyint NOT NULL,
  `adyen_payment_method_info_id` tinyint NOT NULL,
  `person_id` tinyint NOT NULL,
  `created_at` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `legal_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `legal_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `external_uuid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `document_template_id` int(11) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `subject_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `raw_response` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `signed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `provider_identifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `asset_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `asset_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `asset_file_size` int(11) DEFAULT NULL,
  `asset_updated_at` datetime DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_legal_documents_on_uuid_and_subject` (`external_uuid`,`subject_id`,`subject_type`),
  KEY `index_legal_documents_on_signer_id` (`person_id`),
  KEY `index_legal_documents_on_subject_id_and_subject_type` (`subject_id`,`subject_type`),
  KEY `index_legal_documents_on_provider_identifier` (`provider_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `lifetime_earnings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lifetime_earnings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `person_intake` decimal(23,14) DEFAULT NULL,
  `youtube_intake` decimal(23,14) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_lifetime_earnings_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `lod_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lod_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(255) DEFAULT NULL,
  `lod_id` int(11) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status_at` datetime DEFAULT NULL,
  `notes` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `lods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `esignature_url` varchar(255) DEFAULT NULL,
  `document_guid` varchar(255) DEFAULT NULL,
  `template_guid` varchar(255) DEFAULT NULL,
  `last_status` varchar(255) DEFAULT NULL,
  `last_status_at` datetime DEFAULT NULL,
  `last_status_by` datetime DEFAULT NULL,
  `raw_request` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `raw_response` text DEFAULT NULL,
  `reminder_sent_count` int(11) DEFAULT 0,
  `extension_count` int(11) DEFAULT 0,
  `s3_asset_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `user` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`,`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
 PARTITION BY RANGE (year(`timestamp`))
(PARTITION `p2012` VALUES LESS THAN (2013) ENGINE = InnoDB,
 PARTITION `p2013` VALUES LESS THAN (2014) ENGINE = InnoDB,
 PARTITION `p2014` VALUES LESS THAN (2015) ENGINE = InnoDB,
 PARTITION `p2015` VALUES LESS THAN (2016) ENGINE = InnoDB,
 PARTITION `p2016` VALUES LESS THAN (2017) ENGINE = InnoDB,
 PARTITION `prest` VALUES LESS THAN MAXVALUE ENGINE = InnoDB);
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `login_attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` int(10) unsigned DEFAULT NULL,
  `all_time_count` smallint(6) DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `since_last_login_count` int(11) DEFAULT 0,
  `client_ip_address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_login_attempts_on_ip_address` (`ip_address`),
  KEY `index_login_attempts_on_client_ip_address` (`client_ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `login_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `subdivision` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_login_events_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `login_tracks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_tracks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trackable_id` int(11) NOT NULL,
  `trackable_type` varchar(255) NOT NULL,
  `login_event_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_login_tracks_on_trackable_id_and_trackable_type` (`trackable_id`,`trackable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `low_risk_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `low_risk_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `added_by_admin_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_low_risk_accounts_on_person_id` (`person_id`),
  KEY `index_low_risk_accounts_on_added_by_admin_id` (`added_by_admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `lyrics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lyrics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `song_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_lyrics_on_song_id` (`song_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `manual_approvals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manual_approvals` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `approveable_type` varchar(255) DEFAULT NULL,
  `approveable_id` bigint(20) DEFAULT NULL,
  `created_by_id` int(11) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_manual_approvals_on_approveable_type_and_approveable_id` (`approveable_type`,`approveable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `manufacturing_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manufacturing_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `shipping_name` varchar(255) DEFAULT NULL,
  `shipping_address` varchar(255) DEFAULT NULL,
  `shipping_address2` varchar(255) DEFAULT NULL,
  `shipping_city` varchar(255) DEFAULT NULL,
  `shipping_state` varchar(255) DEFAULT NULL,
  `shipping_zip` varchar(255) DEFAULT NULL,
  `album` varchar(255) DEFAULT NULL,
  `artist` varchar(255) DEFAULT NULL,
  `upc` varchar(255) DEFAULT NULL,
  `run` varchar(255) DEFAULT NULL,
  `packaging` varchar(255) DEFAULT NULL,
  `jd_panels` varchar(255) DEFAULT NULL,
  `jd_insert` varchar(255) DEFAULT NULL,
  `jd_traycard` varchar(255) DEFAULT NULL,
  `cw_wallets` varchar(255) DEFAULT NULL,
  `d_type` varchar(255) DEFAULT NULL,
  `tray` varchar(255) DEFAULT NULL,
  `shrinkwrap` varchar(255) DEFAULT NULL,
  `shrinkwrap_quantity` int(11) DEFAULT NULL,
  `topspine` varchar(255) DEFAULT NULL,
  `topspine_quantity` int(11) DEFAULT NULL,
  `special_instructions` text DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `send_barcode` tinyint(1) DEFAULT NULL,
  `assign_upc` tinyint(1) DEFAULT NULL,
  `generate_barcode` tinyint(1) DEFAULT NULL,
  `address3` varchar(255) DEFAULT NULL,
  `shipping_address3` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `marketing_event_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketing_event_triggers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `related_id` int(11) DEFAULT NULL,
  `related_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_trigger` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `marketing_experiments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketing_experiments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marketing_service` varchar(255) DEFAULT NULL,
  `experiment_type` varchar(255) DEFAULT NULL,
  `experiment_name` varchar(255) DEFAULT NULL,
  `number_of_variations` int(11) DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `mass_adjustment_batches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mass_adjustment_batches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `csv_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `allow_negative_balance` tinyint(1) DEFAULT 0,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `s3_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `balance_adjustment_category_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_mass_adjustment_batches_on_balance_adjustment_category_id` (`balance_adjustment_category_id`),
  CONSTRAINT `fk_rails_fb17e5904e` FOREIGN KEY (`balance_adjustment_category_id`) REFERENCES `balance_adjustment_categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `mass_adjustment_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mass_adjustment_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `mass_adjustment_batch_id` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` decimal(23,14) DEFAULT NULL,
  `balance_turns_negative` tinyint(1) DEFAULT NULL,
  `status_info` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mass_adjustment_batch_id` (`mass_adjustment_batch_id`),
  CONSTRAINT `mass_adjustment_entries_ibfk_1` FOREIGN KEY (`mass_adjustment_batch_id`) REFERENCES `mass_adjustment_batches` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `mass_ingestion_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mass_ingestion_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `identifier_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_mass_ingestion_statuses_on_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `mastered_tracks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mastered_tracks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `download_link` varchar(255) DEFAULT NULL,
  `claim_id` varchar(255) DEFAULT NULL,
  `landr_track_id` varchar(255) DEFAULT NULL,
  `paid_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `landr_purchase_date` datetime DEFAULT NULL,
  `job_id` varchar(255) DEFAULT NULL,
  `refunded_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_mastered_tracks_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `media_assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_assets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `description` varchar(500) DEFAULT NULL COMMENT 'Description of asset',
  `width` smallint(6) unsigned DEFAULT NULL COMMENT 'Image width',
  `height` smallint(6) unsigned DEFAULT NULL COMMENT 'Image Height',
  `bit_depth` tinyint(4) unsigned DEFAULT NULL COMMENT 'Bit Depth',
  `size` int(10) unsigned NOT NULL COMMENT 'Size of file in bytes',
  `s3_asset_id` int(10) unsigned DEFAULT NULL COMMENT 'Foreign key to sc_assets table.',
  `person_id` int(10) unsigned NOT NULL COMMENT 'Foreigh key to people table.',
  `type` enum('BandPhoto') NOT NULL COMMENT 'Asset type.',
  `mime_type` enum('image/jpeg','image/png','image/tiff','image/bmp','image/gif','image/pjpeg','image/x-png') NOT NULL COMMENT 'Asset mime type.',
  `created_at` datetime NOT NULL COMMENT 'Date record Created.',
  `updated_at` datetime NOT NULL COMMENT 'Date record Updated.',
  PRIMARY KEY (`id`),
  KEY `s3_asset_id` (`s3_asset_id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `meta_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meta_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_website_id` int(11) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_meta_tags_on_page_name_and_name_and_country_website_id` (`page_name`,`name`,`country_website_id`),
  KEY `index_meta_tags_on_country_website_id` (`country_website_id`),
  KEY `index_meta_tags_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `muma_imports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `muma_imports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `import_type` varchar(10) DEFAULT NULL,
  `requested_source_updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `stats` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `muma_song_ips`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `muma_song_ips` (
  `song_code` int(11) DEFAULT NULL,
  `ip_code` varchar(255) DEFAULT NULL,
  `ip_chain` int(11) DEFAULT NULL,
  `ip_c_or_p` varchar(255) DEFAULT NULL,
  `controlled` tinyint(1) DEFAULT NULL,
  `mech_owned_share` decimal(5,2) DEFAULT NULL,
  `mech_collect_share` decimal(5,2) DEFAULT NULL,
  `capacity_code` varchar(2) DEFAULT NULL,
  `link_parent_ip_code` varchar(255) DEFAULT NULL,
  `publisher_name` varchar(255) DEFAULT NULL,
  `publisher_code` varchar(6) DEFAULT NULL,
  `composer_first_name` varchar(255) DEFAULT NULL,
  `composer_middle_name` varchar(255) DEFAULT NULL,
  `composer_last_name` varchar(255) DEFAULT NULL,
  `composer_code` varchar(6) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `territory_code` varchar(4) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `perf_owned_share` decimal(5,2) DEFAULT NULL,
  `perf_collect_share` decimal(5,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_muma_song_ips_on_song_code` (`song_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `muma_song_registration_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `muma_song_registration_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `song_code` int(11) DEFAULT NULL,
  `society_code` varchar(12) DEFAULT NULL,
  `entry_created_at` datetime DEFAULT NULL,
  `processed_at` datetime DEFAULT NULL,
  `status` varchar(2) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_song_code_society_code_entry_created_at` (`song_code`,`society_code`,`entry_created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `muma_song_societies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `muma_song_societies` (
  `song_code` int(11) DEFAULT NULL,
  `society_code` varchar(4) DEFAULT NULL,
  `work_num` varchar(255) DEFAULT NULL,
  `registered_at` datetime DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `index_muma_song_societies_on_song_code` (`song_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `muma_songs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `muma_songs` (
  `code` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `label_copy` varchar(255) DEFAULT NULL,
  `composition_id` int(11) DEFAULT NULL,
  `mech_collect_share` decimal(5,2) DEFAULT NULL,
  `performing_artist` varchar(255) DEFAULT NULL,
  `album_label` varchar(255) DEFAULT NULL,
  `album_title` varchar(255) DEFAULT NULL,
  `album_upc` varchar(255) DEFAULT NULL,
  `isrc` varchar(255) DEFAULT NULL,
  `ip_updated_at` datetime DEFAULT NULL,
  `source_created_at` datetime DEFAULT NULL,
  `source_updated_at` datetime DEFAULT NULL,
  `source_deleted_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `parent_code` int(11) DEFAULT NULL,
  `company_code` varchar(4) DEFAULT NULL,
  `company_updated_at` datetime DEFAULT NULL,
  `iswc` varchar(11) DEFAULT NULL,
  `copyrighted_at` datetime DEFAULT NULL,
  `copyright_num` varchar(12) DEFAULT NULL,
  `expired_at` datetime DEFAULT NULL,
  `registered` tinyint(1) DEFAULT 0,
  `publishing_composition_id` int(11) DEFAULT NULL,
  KEY `index_muma_songs_on_code` (`code`),
  KEY `index_muma_songs_on_composition_id` (`composition_id`),
  KEY `index_muma_songs_on_parent_code` (`parent_code`),
  KEY `index_muma_songs_on_isrc` (`isrc`(12)),
  KEY `index_muma_songs_on_publishing_composition_id` (`publishing_composition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `newsletter_subscribers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_subscribers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) DEFAULT NULL,
  `address` varchar(128) NOT NULL,
  `created_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_newsletter_subscribers_on_address` (`address`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `non_tunecore_albums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `non_tunecore_albums` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `composer_id` int(11) DEFAULT NULL,
  `album_id` int(11) DEFAULT NULL,
  `upc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orig_release_year` date DEFAULT NULL,
  `record_label` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `account_id` int(11) DEFAULT NULL,
  `publishing_composer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_non_tunecore_albums_on_composer_id` (`composer_id`),
  KEY `index_non_tunecore_albums_on_account_id` (`account_id`),
  KEY `index_non_tunecore_albums_on_publishing_composer_id` (`publishing_composer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `non_tunecore_songs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `non_tunecore_songs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `artist_id` int(11) DEFAULT NULL,
  `isrc` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `release_date` date DEFAULT NULL,
  `song_id` int(11) DEFAULT NULL,
  `non_tunecore_album_id` int(11) DEFAULT NULL,
  `composition_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `publishing_composition_id` int(11) DEFAULT NULL,
  `cover_song` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_non_tunecore_songs_on_song_id` (`song_id`),
  KEY `index_non_tunecore_songs_on_artist_id` (`artist_id`),
  KEY `index_non_tunecore_songs_on_non_tunecore_album_id` (`non_tunecore_album_id`),
  KEY `index_non_tunecore_songs_on_composition_id` (`composition_id`),
  KEY `index_non_tunecore_songs_on_publishing_composition_id` (`publishing_composition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `related_id` int(10) unsigned NOT NULL COMMENT 'Foreign Key of the entity the note is about',
  `related_type` enum('Person','Album','Song','EftBatchTransaction','SubscriptionEvent','Composer','PublishingComposer','FlagTransition','CopyrightClaim') DEFAULT NULL COMMENT 'Type of entity.',
  `note_created_by_id` int(10) unsigned NOT NULL COMMENT 'Foreign Key references people table for the user that created the note',
  `subject` varchar(255) NOT NULL COMMENT 'Subject',
  `note` text NOT NULL COMMENT 'Note',
  `created_at` datetime DEFAULT NULL COMMENT 'Date note was created',
  `updated_at` datetime DEFAULT NULL COMMENT 'Date note was updated',
  `ip_address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `person_id` (`related_id`),
  KEY `note_created_by_id` (`note_created_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `notification_icons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_icons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_file_name` varchar(255) DEFAULT NULL,
  `file_content_type` varchar(255) DEFAULT NULL,
  `file_file_size` int(11) DEFAULT NULL,
  `file_updated_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `targeted_notification_id` int(11) DEFAULT NULL,
  `notification_item_id` int(11) DEFAULT NULL,
  `notification_item_type` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT 'Notification',
  `text` mediumtext DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `link_text` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `first_seen` datetime DEFAULT NULL,
  `first_clicked` datetime DEFAULT NULL,
  `first_archived` datetime DEFAULT NULL,
  `click_count` int(11) DEFAULT NULL,
  `effective_created_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `last_clicked` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_notifications_on_person_id` (`person_id`),
  KEY `index_notifications_on_targeted_notification_id` (`targeted_notification_id`),
  KEY `index_notifications_on_person_id_and_targeted_notification_id` (`person_id`,`targeted_notification_id`),
  KEY `notification_item` (`notification_item_id`,`notification_item_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `oauth_nonces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_nonces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nonce` varchar(255) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_oauth_nonces_on_nonce_and_timestamp` (`nonce`,`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `oauth_providers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_providers` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `name` varchar(50) NOT NULL COMMENT 'Provider Name',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='OAuth Providers.';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `oauth_providers_people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_providers_people` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `person_id` int(10) unsigned NOT NULL COMMENT 'Foreign key to people table.',
  `oauth_provider_id` smallint(5) unsigned NOT NULL COMMENT 'Foreign key to oauth_providers table.',
  `access_token` varchar(30) NOT NULL COMMENT 'Access token.',
  `access_token_secret` varchar(50) NOT NULL COMMENT 'Access token secret.',
  `is_active` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT 'Indicates if tocken is active.',
  `date_added` timestamp NULL DEFAULT NULL COMMENT 'Date record was created.',
  `date_updated` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp() COMMENT 'Date last updated.',
  PRIMARY KEY (`id`),
  KEY `oauth_provider_id` (`oauth_provider_id`),
  KEY `person_id` (`person_id`),
  CONSTRAINT `FK_oauth_providers_people` FOREIGN KEY (`oauth_provider_id`) REFERENCES `oauth_providers` (`id`),
  CONSTRAINT `FK_oauth_providers_person` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Links a person account to oauth_provider';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `oauth_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `client_application_id` int(11) DEFAULT NULL,
  `token` varchar(20) DEFAULT NULL,
  `secret` varchar(40) DEFAULT NULL,
  `callback_url` varchar(255) DEFAULT NULL,
  `verifier` varchar(20) DEFAULT NULL,
  `authorized_at` datetime DEFAULT NULL,
  `invalidated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_oauth_tokens_on_token` (`token`),
  KEY `index_oauth_tokens_on_user_id_and_client_application_id` (`user_id`,`client_application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `setting_type` varchar(255) DEFAULT NULL,
  `typeface_effect_id` int(11) DEFAULT NULL,
  `default_value` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `outbound_invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outbound_invoices` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) NOT NULL,
  `invoice_sequence` int(11) DEFAULT NULL,
  `related_id` int(11) NOT NULL,
  `related_type` varchar(255) NOT NULL,
  `vat_tax_adjustment_id` int(11) DEFAULT NULL,
  `invoice_date` datetime NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `currency` char(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency for outbound invoice',
  `user_invoice_prefix` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_outbound_invoices_on_user_prefix_and_sequence` (`user_invoice_prefix`,`invoice_sequence`),
  KEY `index_outbound_invoices_on_person_id` (`person_id`),
  KEY `index_outbound_invoices_on_related_id_and_related_type` (`related_id`,`related_type`),
  KEY `index_outbound_invoices_on_invoice_sequence` (`invoice_sequence`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `outbound_refunds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outbound_refunds` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_invoice_prefix` varchar(255) NOT NULL,
  `invoice_sequence` int(11) DEFAULT NULL,
  `refund_id` bigint(20) NOT NULL,
  `outbound_invoice_id` bigint(20) NOT NULL,
  `vat_tax_adjustment_id` bigint(20) DEFAULT NULL,
  `refund_settlement_id` bigint(20) NOT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_outbound_refunds_on_refund_id` (`refund_id`),
  KEY `index_outbound_refunds_on_outbound_invoice_id` (`outbound_invoice_id`),
  KEY `index_outbound_refunds_on_vat_tax_adjustment_id` (`vat_tax_adjustment_id`),
  KEY `index_outbound_refunds_on_refund_settlement_id` (`refund_settlement_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `paper_agreements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paper_agreements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `related_type` varchar(255) DEFAULT NULL,
  `related_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `partners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partners` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `name` varchar(50) NOT NULL COMMENT 'Partner Name',
  `domain` varchar(255) NOT NULL COMMENT 'Domain for the partner',
  `skin` varchar(50) NOT NULL COMMENT 'Skin Identifier for the partner.',
  PRIMARY KEY (`id`),
  KEY `domain` (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `payin_provider_configs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payin_provider_configs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `currency` char(3) NOT NULL,
  `corporate_entity_id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `merchant_account_id` varchar(255) DEFAULT NULL,
  `merchant_id` varchar(255) DEFAULT NULL,
  `public_key` varchar(255) DEFAULT NULL,
  `sandbox` tinyint(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `payment_batches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_batches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary KEy',
  `country_website_id` int(11) unsigned NOT NULL,
  `currency` char(3) NOT NULL COMMENT 'Currency',
  `batch_date` datetime DEFAULT NULL COMMENT 'Datetime that batch was run',
  `total_amount` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT 'Total Amount in Batch',
  `balance_amount` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT 'Total Paid by Customer Balance in Batch',
  `cannot_process_amount` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT 'Total Amount for Cannot Process in Batch',
  `paypal_amount_total` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT 'Total Amount for Paypal in Batch',
  `paypal_amount_successful` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT 'Total Successfully Paid by paypal in Batch',
  `braintree_amount_total` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT 'Total Amount for BrainTree in Batch',
  `braintree_amount_successful` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT 'Total Successfully Paid by Braintree in Batch',
  `adyen_amount_total` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT 'Total Amount for Adyen in Batch',
  `adyen_amount_successful` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT 'Total Successfully Paid by Adyen in Batch',
  `batch_file_name` varchar(200) DEFAULT NULL COMMENT 'Name of physical csv file saved on server, sent to braintree',
  `batch_sent_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime file sent to braintree',
  `response_received` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT 'Indicate if the batch has been received',
  `response_processed_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime file received back from braintree',
  `response_file_name` varchar(200) DEFAULT NULL COMMENT 'Name of physical csv file saved on server, received from Braintree',
  `status` enum('complete','processing','new') NOT NULL DEFAULT 'new' COMMENT 'Current State',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  `payments_os_amount_total` decimal(10,2) NOT NULL,
  `payments_os_amount_successful` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `payment_batches_country_website_id` (`country_website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `payment_channel_receipts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_channel_receipts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subscription_purchase_id` int(11) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `receipt_data` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `receipt_person_or_purchase` (`person_id`,`subscription_purchase_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `payments_os_customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments_os_customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_reference` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `payos_customer_on_customer_ref_customer_id` (`customer_reference`,`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `payments_os_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments_os_transactions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) DEFAULT NULL,
  `stored_credit_card_id` bigint(20) DEFAULT NULL,
  `invoice_id` bigint(20) DEFAULT NULL,
  `reconciliation_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` decimal(10,0) DEFAULT NULL,
  `currency` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action` enum('sale','refund') COLLATE utf8_unicode_ci NOT NULL,
  `payment_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_status` enum('Initialized','Credited','Pending','Authorized','Captured','Refunded','Voided','Failed') COLLATE utf8_unicode_ci NOT NULL,
  `payment_raw_response` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `charge_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `charge_status` enum('Pending','Succeed','Failed') COLLATE utf8_unicode_ci NOT NULL,
  `charge_raw_response` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `original_transaction_id` bigint(20) DEFAULT NULL,
  `refund_reason` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refund_category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refund_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refund_raw_response` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `refund_status` enum('Pending','Succeed','Failed') COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_payments_os_transactions_on_person_id` (`person_id`),
  KEY `index_payments_os_transactions_on_stored_credit_card_id` (`stored_credit_card_id`),
  KEY `index_payments_os_transactions_on_invoice_id` (`invoice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `payoneer_ach_fees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payoneer_ach_fees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `target_country_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transfer_fees` decimal(7,2) DEFAULT NULL,
  `minimum_threshold` decimal(7,2) DEFAULT NULL,
  `target_currency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `source_currency` char(3) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `payout_programs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payout_programs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payout_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_program_id` int(11) DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inside_region` tinyint(1) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL,
  `fee_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount_cents` int(11) DEFAULT NULL,
  `percentage` float DEFAULT NULL,
  `max_fee` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_payout_programs_on_client_program_id` (`client_program_id`),
  KEY `index_payout_programs_on_payout_type` (`payout_type`),
  KEY `index_payout_programs_on_currency` (`currency`),
  KEY `index_payout_programs_on_enabled` (`enabled`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `payout_provider_configs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payout_provider_configs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `program_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sandbox` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `corporate_entity_id` bigint(20) NOT NULL,
  `cert_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `key_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_payout_provider_configs_on_corporate_entity_id` (`corporate_entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `payout_providers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payout_providers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `provider_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tunecore_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_payee_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `registered_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider_processed_at` datetime DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `provider_identifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active_provider` tinyint(1) NOT NULL,
  `currency` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payout_provider_config_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_payout_providers_on_person_id` (`person_id`),
  KEY `index_payout_providers_on_client_payee_id` (`client_payee_id`),
  KEY `index_payout_providers_on_provider_identifier` (`provider_identifier`),
  KEY `index_payout_providers_on_payout_provider_config_id` (`payout_provider_config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `payout_service_fees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payout_service_fees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `country_website_id` int(11) unsigned DEFAULT NULL COMMENT 'Foreign key to country_websites',
  `fee_type` enum('eft_service','eft_failure','check_service') DEFAULT NULL COMMENT 'type of service fee',
  `amount` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT 'Amount',
  `currency` char(3) NOT NULL COMMENT 'Currency for payout service fee',
  `posted_by_id` int(10) unsigned DEFAULT NULL COMMENT 'Foreign Key to People table',
  `posted_by_name` varchar(255) DEFAULT NULL COMMENT 'Admin name who changed service fee',
  `archived_at` datetime DEFAULT NULL COMMENT 'Date no longer in use',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  PRIMARY KEY (`id`),
  KEY `psf_posted_by_id` (`posted_by_id`),
  CONSTRAINT `FK_psf_posted_by_id` FOREIGN KEY (`posted_by_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `payout_transfer_api_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payout_transfer_api_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `payout_transfer_id` bigint(20) DEFAULT NULL,
  `body` text DEFAULT NULL,
  `kind` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_payout_transfer_api_logs_on_payout_transfer_id` (`payout_transfer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `payout_transfers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payout_transfers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payout_provider_id` int(11) DEFAULT NULL,
  `client_reference_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tunecore_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount_cents` int(11) DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fee_cents` int(11) DEFAULT NULL,
  `transaction_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `tunecore_processed_at` datetime DEFAULT NULL,
  `provider_processed_at` datetime DEFAULT NULL,
  `tunecore_processor_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `raw_response` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `payout_program_id` int(11) DEFAULT NULL,
  `withdraw_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payout_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `withdraw_method_changed` tinyint(1) DEFAULT NULL,
  `rolled_back` tinyint(1) DEFAULT 0,
  `person_id` int(11) DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_payout_transfers_on_payout_provider_id` (`payout_provider_id`),
  KEY `index_payout_transfers_on_tunecore_processor_id` (`tunecore_processor_id`),
  KEY `index_payout_transfers_on_client_reference_id` (`client_reference_id`),
  KEY `index_payout_transfers_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `payout_withdrawal_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payout_withdrawal_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payout_provider_id` int(11) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL,
  `withdrawal_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disabled_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `payout_program_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_payout_withdrawal_types_on_payout_provider_id` (`payout_provider_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `paypal_ipns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paypal_ipns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `person_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `payment_status` varchar(30) DEFAULT NULL,
  `paypal_txn_id` varchar(20) DEFAULT NULL,
  `paypal_item_number` varchar(100) DEFAULT NULL,
  `payment_amount_cents` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `paypal_ipns_person_index` (`person_id`),
  KEY `paypal_ipns_invoice_id_index` (`invoice_id`),
  KEY `paypal_txn_id_idx` (`paypal_txn_id`(8))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `paypal_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paypal_transactions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `country_website_id` char(3) NOT NULL COMMENT 'Foreign Key to country websties',
  `person_id` int(10) unsigned DEFAULT NULL COMMENT 'person ID',
  `invoice_id` int(11) DEFAULT NULL COMMENT 'foreign key',
  `email` varchar(50) DEFAULT NULL COMMENT 'email used in transaction',
  `amount` decimal(8,2) DEFAULT NULL COMMENT 'Payment Amount',
  `action` enum('Sale','Authorization','Refund','Void') DEFAULT NULL COMMENT 'type of transaction',
  `status` varchar(50) DEFAULT NULL COMMENT 'status of paypal_transaction',
  `ack` varchar(50) DEFAULT NULL COMMENT 'ack (success, failure, etc.) field returned by paypal, keeping for documentation purposes',
  `billing_agreement_id` char(19) DEFAULT NULL COMMENT 'Billing Agreement ID returned by paypal, used for reference transactions. Will always be 17 or 19 characters',
  `transaction_id` char(19) DEFAULT NULL COMMENT 'Transaction ID returned by paypal, used for reference transactions. Will always be 17 or 19 characters',
  `transaction_type` varchar(20) DEFAULT NULL COMMENT 'type of transaction from paypal',
  `referenced_paypal_account_id` int(11) unsigned DEFAULT NULL COMMENT 'foreign key for reference transactions if a stored_paypal_account is used',
  `token` varchar(50) DEFAULT NULL COMMENT 'token used by paypal',
  `currency` char(3) DEFAULT NULL COMMENT '3 letter currency iso code',
  `error_code` varchar(50) DEFAULT NULL COMMENT 'Error code field returned by paypal, keeping for documentation purposes',
  `pending_reason` varchar(50) DEFAULT NULL COMMENT 'Pending Reason field returned by paypal, keeping for documentation purposes',
  `fee` decimal(6,2) DEFAULT NULL COMMENT 'Paypal processing Fee that we were charged',
  `raw_response` text DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  `payin_provider_config_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `paypal_pt_person_id` (`person_id`),
  KEY `paypal_pt_invoice_id` (`invoice_id`),
  KEY `paypal_pt_stored_paypal_account_id` (`referenced_paypal_account_id`),
  KEY `paypal_pt_transaction_id` (`transaction_id`),
  KEY `spa_transfers_country` (`country_website_id`),
  KEY `index_paypal_transactions_on_payin_provider_config_id` (`payin_provider_config_id`),
  CONSTRAINT `FK_paypal_pt_person_id` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`),
  CONSTRAINT `FK_referenced_paypal_account_id` FOREIGN KEY (`referenced_paypal_account_id`) REFERENCES `stored_paypal_accounts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `paypal_transfers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paypal_transfers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_website_id` char(3) NOT NULL COMMENT 'Foreign Key to country websties',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `transfer_status` varchar(20) NOT NULL DEFAULT '',
  `person_id` int(11) NOT NULL DEFAULT 0,
  `payment_cents` int(11) NOT NULL DEFAULT 0,
  `admin_charge_cents` int(11) NOT NULL DEFAULT 0,
  `currency` char(3) NOT NULL,
  `paypal_address` varchar(80) NOT NULL DEFAULT '',
  `approved_by_id` int(11) DEFAULT NULL,
  `suspicious_flags` text DEFAULT NULL,
  `correlation_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `person_id_idx` (`person_id`),
  KEY `transfer_status` (`transfer_status`),
  KEY `paypal_transfers_country` (`country_website_id`),
  KEY `index_paypal_transfers_on_paypal_address` (`paypal_address`),
  KEY `index_paypal_transfers_on_correlation_id` (`correlation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `payu_invoice_upload_responses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payu_invoice_upload_responses` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `api_response_code` varchar(255) NOT NULL,
  `api_message` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `related_id` bigint(20) DEFAULT NULL,
  `related_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_payu_invoice_upload_responses_on_api_response_code` (`api_response_code`),
  KEY `index_payu_responses_related` (`related_id`,`related_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `payu_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payu_transactions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) NOT NULL,
  `invoice_id` bigint(20) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `payu_id` varchar(255) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `error_code` varchar(255) DEFAULT NULL,
  `action` enum('sale','refund') NOT NULL DEFAULT 'sale',
  `refund_request_id` varchar(255) DEFAULT NULL,
  `original_transaction_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_payu_transactions_on_person_id` (`person_id`),
  KEY `index_payu_transactions_on_invoice_id` (`invoice_id`),
  KEY `index_payu_transactions_on_payu_id` (`payu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `payu_webhook_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payu_webhook_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `success` tinyint(1) NOT NULL,
  `payu_transaction_id` bigint(20) NOT NULL,
  `payu_transaction_created_at` datetime NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_payu_webhook_logs_on_payu_transaction_id` (`payu_transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_website_id` int(11) unsigned NOT NULL,
  `name` varchar(80) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `artist_id` int(10) unsigned DEFAULT NULL,
  `salt` varchar(255) NOT NULL DEFAULT '',
  `is_verified` tinyint(3) unsigned DEFAULT 0,
  `invite_code` varchar(40) DEFAULT NULL,
  `invite_expiry` datetime DEFAULT NULL,
  `deleted` tinyint(3) unsigned DEFAULT 0,
  `delete_after` datetime DEFAULT NULL,
  `is_opted_in` tinyint(1) DEFAULT 0,
  `label_id` int(10) unsigned DEFAULT NULL,
  `temp_ownership_flag` tinyint(1) NOT NULL DEFAULT 0,
  `phone_number` varchar(255) DEFAULT NULL,
  `info_sharing` tinyint(1) DEFAULT NULL,
  `recent_login` datetime DEFAULT NULL,
  `recent_login_failure` datetime DEFAULT NULL,
  `info_sharing_date` datetime DEFAULT NULL,
  `referral` varchar(255) DEFAULT NULL,
  `created_on` datetime DEFAULT '2007-08-01 10:44:40',
  `referral_campaign` varchar(255) DEFAULT NULL,
  `referral_type` varchar(10) DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `login_attempts` int(11) DEFAULT 0,
  `account_locked_until` datetime DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `us_zip_code_id` mediumint(8) unsigned DEFAULT NULL,
  `interstitial_shown_on` date DEFAULT NULL,
  `partner_id` int(10) unsigned DEFAULT NULL,
  `use_experimental_ezpay_backend` tinyint(1) DEFAULT 0,
  `referral_token` varchar(50) DEFAULT NULL,
  `alternate_upload_REMOVE` tinyint(1) NOT NULL DEFAULT 0,
  `referral_promotion_applied` tinyint(1) DEFAULT 0,
  `vip` tinyint(1) DEFAULT 0,
  `sent_to_parature` tinyint(1) DEFAULT 0,
  `foreign_postal_code` varchar(32) DEFAULT NULL,
  `accepted_terms_and_conditions_on` datetime DEFAULT NULL,
  `status` enum('Active','Inactive','Locked','Suspicious') NOT NULL DEFAULT 'Active' COMMENT 'Status of user, inactive prevents the user from logging in',
  `status_updated_at` datetime DEFAULT NULL COMMENT 'Date and time user status was changed',
  `lock_reason` varchar(255) DEFAULT NULL,
  `is_business` tinyint(1) DEFAULT 0,
  `business_name` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(255) DEFAULT NULL,
  `migrated_to_bigbox` tinyint(1) DEFAULT 0,
  `enable_bigbox_list` tinyint(1) DEFAULT NULL,
  `redemption_attempts` int(11) DEFAULT 0,
  `redemption_locked_until` datetime DEFAULT NULL,
  `redemption_brand` varchar(255) DEFAULT NULL,
  `last_logged_in_ip` varchar(255) DEFAULT NULL,
  `apple_id` varchar(255) DEFAULT NULL,
  `apple_role` varchar(255) DEFAULT NULL,
  `braintree_customer_id` varchar(255) DEFAULT NULL,
  `password_reset_tmsp` datetime DEFAULT NULL,
  `verified_at` datetime DEFAULT NULL,
  `dormant` tinyint(1) DEFAULT 0,
  `override_advance_ineligibility` tinyint(1) DEFAULT 0,
  `country_website_language_id` int(11) DEFAULT NULL,
  `corporate_entity_id` bigint(20) DEFAULT NULL,
  `customer_type` enum('individual','business') DEFAULT 'individual',
  `country_state_id` int(11) DEFAULT NULL,
  `country_state_city_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_people_on_email` (`email`),
  KEY `lastlogin` (`recent_login`),
  KEY `people_artist_id` (`artist_id`),
  KEY `people_label_id_index` (`label_id`),
  KEY `referral` (`referral`),
  KEY `created_on` (`created_on`),
  KEY `country_websites_currency` (`country_website_id`),
  KEY `index_people_on_corporate_entity_id` (`corporate_entity_id`),
  KEY `index_people_on_country_state_city_id` (`country_state_city_id`),
  KEY `people_status_index` (`status`),
  CONSTRAINT `fk_rails_7627e214e8` FOREIGN KEY (`corporate_entity_id`) REFERENCES `corporate_entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `people_flags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people_flags` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `flag_id` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `flag_reason_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_people_flags_on_person_id_and_flag_id_and_flag_reason_id` (`person_id`,`flag_id`,`flag_reason_id`),
  KEY `index_people_flags_on_flag_reason_id` (`flag_reason_id`),
  CONSTRAINT `fk_rails_55ad44a81d` FOREIGN KEY (`flag_reason_id`) REFERENCES `flag_reasons` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `people_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people_roles` (
  `person_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  KEY `people_roles_person_id_index` (`person_id`),
  KEY `people_roles_role_id_index` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `performing_rights_organizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `performing_rights_organizations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cicaccode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider_identifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_performing_rights_organizations_on_name` (`name`),
  KEY `index_performing_rights_organizations_on_cicaccode` (`cicaccode`),
  KEY `index_performing_rights_organizations_on_provider_identifier` (`provider_identifier`),
  KEY `index_performing_rights_organizations_on_is_deleted` (`is_deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `person_analytics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_analytics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `metric_name` varchar(255) NOT NULL,
  `metric_value` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_person_analytics_on_person_id_and_metric_name` (`person_id`,`metric_name`),
  KEY `index_person_analytics_on_metric_name` (`metric_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `person_balances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_balances` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL,
  `balance` decimal(23,14) NOT NULL DEFAULT 0.00000000000000,
  `currency` char(3) NOT NULL COMMENT 'Currency for balance',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `people_person_id_index` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `person_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `source` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `person_earnings_tax_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_earnings_tax_metadata` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) NOT NULL,
  `pub_taxform_submitted_at` datetime DEFAULT NULL,
  `pub_taxform_id` bigint(20) DEFAULT NULL,
  `dist_taxform_submitted_at` datetime DEFAULT NULL,
  `dist_taxform_id` bigint(20) DEFAULT NULL,
  `total_publishing_earnings` decimal(23,14) NOT NULL DEFAULT 0.00000000000000,
  `total_distribution_earnings` decimal(23,14) NOT NULL DEFAULT 0.00000000000000,
  `tax_year` int(11) DEFAULT NULL,
  `tax_blocked` tinyint(1) DEFAULT 0,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `tax_blocked_at` datetime DEFAULT NULL,
  `total_taxable_distribution_earnings` decimal(23,14) NOT NULL DEFAULT 0.00000000000000,
  `total_taxable_publishing_earnings` decimal(23,14) NOT NULL DEFAULT 0.00000000000000,
  `rollover_earnings` decimal(23,14) NOT NULL DEFAULT 0.00000000000000,
  PRIMARY KEY (`id`),
  KEY `index_person_earnings_tax_metadata_on_person_id` (`person_id`),
  KEY `index_person_earnings_tax_metadata_on_tax_year` (`tax_year`),
  KEY `index_person_earnings_tax_metadata_on_pub_taxform_id` (`pub_taxform_id`),
  KEY `index_person_earnings_tax_metadata_on_dist_taxform_id` (`dist_taxform_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `person_gstins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_gstins` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) DEFAULT NULL,
  `gstin` char(15) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_person_gstins_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `person_intake_sales_record_masters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_intake_sales_record_masters` (
  `person_intake_id` int(11) unsigned NOT NULL,
  `sales_record_master_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`person_intake_id`,`sales_record_master_id`),
  KEY `sales_record_master_id` (`sales_record_master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `person_intakes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_intakes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned DEFAULT NULL,
  `amount` decimal(23,14) NOT NULL DEFAULT 0.00000000000000,
  `currency` char(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency',
  `created_at` timestamp NULL DEFAULT NULL,
  `reporting_month_id_temp` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `s_person_intakes_person_id_index` (`person_id`),
  KEY `s_ix_reporting_month_temp` (`reporting_month_id_temp`),
  KEY `s_person_intakes_combo_person_month` (`person_id`,`reporting_month_id_temp`),
  CONSTRAINT `FK_pi_person_id` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `person_plan_histories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_plan_histories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) NOT NULL,
  `plan_id` bigint(20) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `change_type` varchar(255) NOT NULL,
  `discount_reason` varchar(255) DEFAULT NULL,
  `plan_start_date` datetime NOT NULL,
  `plan_end_date` datetime NOT NULL,
  `purchase_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_person_plan_histories_on_purchase_id` (`purchase_id`),
  KEY `index_person_plan_histories_on_person_id` (`person_id`),
  KEY `index_person_plan_histories_on_plan_id` (`plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `person_plans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_plans` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) NOT NULL,
  `plan_id` bigint(20) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_person_plans_on_person_id` (`person_id`),
  KEY `index_person_plans_on_plan_id` (`plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `person_points_balances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_points_balances` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) unsigned NOT NULL,
  `balance` int(11) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_person_points_balances_on_person_id` (`person_id`),
  CONSTRAINT `fk_rails_f2b9d66b61` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `person_points_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_points_transactions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) unsigned NOT NULL,
  `credit_points` int(11) NOT NULL DEFAULT 0,
  `debit_points` int(11) NOT NULL DEFAULT 0,
  `target_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `target_id` bigint(20) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_person_points_transactions_on_person_id` (`person_id`),
  KEY `index_person_points_transactions_on_target_type_and_target_id` (`target_type`,`target_id`),
  CONSTRAINT `fk_rails_77fdc48c7a` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `person_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_preferences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `person_id` int(10) unsigned NOT NULL COMMENT 'Person foreign key',
  `pay_with_balance` tinyint(3) unsigned NOT NULL DEFAULT 1 COMMENT 'Pay renewals with balance',
  `preferred_credit_card_id` int(10) unsigned DEFAULT NULL COMMENT 'Foreign key to the stored_credit_card table',
  `preferred_paypal_account_id` int(11) unsigned DEFAULT NULL,
  `preferred_payment_type` enum('PayPal','CreditCard','Adyen') NOT NULL DEFAULT 'CreditCard' COMMENT 'which payment type to use',
  `do_not_autorenew` tinyint(3) NOT NULL DEFAULT 0 COMMENT 'Flag to allow customer to opt out of auto-renewals',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  `preferred_adyen_payment_method_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `preferred_credit_card_id` (`preferred_credit_card_id`),
  KEY `FK_p_pref_paypal_account_id` (`preferred_paypal_account_id`),
  KEY `fk_rails_d65b83531f` (`preferred_adyen_payment_method_id`),
  CONSTRAINT `FK_p_pref_paypal_account_id` FOREIGN KEY (`preferred_paypal_account_id`) REFERENCES `stored_paypal_accounts` (`id`),
  CONSTRAINT `FK_person` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`),
  CONSTRAINT `fk_rails_d65b83531f` FOREIGN KEY (`preferred_adyen_payment_method_id`) REFERENCES `adyen_stored_payment_methods` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `person_profile_survey_infos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_profile_survey_infos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL,
  `survey_token` varchar(255) DEFAULT NULL,
  `last_visited_at` datetime DEFAULT NULL,
  `segment_question_response` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_person_id` (`person_id`),
  CONSTRAINT `fk_person_id` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `person_service_opt_ins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_service_opt_ins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `service_identifier` varchar(255) DEFAULT NULL,
  `opt_in_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `person_sift_scores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_sift_scores` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) DEFAULT NULL,
  `score` decimal(17,17) NOT NULL DEFAULT 0.00000000000000000,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `sift_order_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_person_sift_scores_on_sift_order_id` (`sift_order_id`),
  KEY `index_person_sift_scores_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `person_subscription_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_subscription_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `effective_date` datetime DEFAULT NULL,
  `termination_date` datetime DEFAULT NULL,
  `canceled_at` datetime DEFAULT NULL,
  `subscription_type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `grace_period_length` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_person_subscription_statuses_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `person_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `person_id` int(10) unsigned NOT NULL,
  `debit` decimal(23,14) NOT NULL DEFAULT 0.00000000000000,
  `credit` decimal(23,14) NOT NULL DEFAULT 0.00000000000000,
  `previous_balance` decimal(23,14) NOT NULL DEFAULT 0.00000000000000,
  `currency` char(3) NOT NULL COMMENT 'Currency for amounts',
  `target_id` int(10) unsigned DEFAULT NULL,
  `target_type` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `person_transactions_target_index` (`target_type`,`target_id`),
  KEY `person_transactions_person_id_index` (`person_id`),
  KEY `person_transaction_date` (`created_at`),
  CONSTRAINT `FK_pt_people_id` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `petri_bundles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `petri_bundles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` int(11) NOT NULL DEFAULT 0,
  `state` varchar(255) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_petri_bundles_on_album_id` (`album_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `petri_orphans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `petri_orphans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `sqs_message_id` varchar(255) NOT NULL,
  `sqs_message_body` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_petri_orphans_on_sqs_message_id` (`sqs_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `plan_addons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plan_addons` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) NOT NULL,
  `addon_type` varchar(50) NOT NULL,
  `paid_at` datetime DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `canceled_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_plan_addons_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `plan_credit_usage_purchases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plan_credit_usage_purchases` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `plan_id` bigint(20) NOT NULL,
  `purchase_id` bigint(20) NOT NULL,
  `credit_usage_id` bigint(20) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_plan_credit_usage_purchases_on_plan_id` (`plan_id`),
  KEY `index_plan_credit_usage_purchases_on_purchase_id` (`purchase_id`),
  KEY `index_plan_credit_usage_purchases_on_credit_usage_id` (`credit_usage_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `plan_downgrade_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plan_downgrade_requests` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) DEFAULT NULL,
  `requested_plan_id` int(11) DEFAULT NULL,
  `reason_id` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_plan_downgrade_requests_on_person_id` (`person_id`),
  KEY `index_plan_downgrade_requests_on_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `plan_features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plan_features` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `feature` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_plan_features_on_feature` (`feature`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `plans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plans` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_plans_on_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `plans_plan_feature_histories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plans_plan_feature_histories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `plan_id` bigint(20) NOT NULL,
  `plan_feature_id` bigint(20) NOT NULL,
  `change_type` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_plans_plan_feature_histories_on_plan_id` (`plan_id`),
  KEY `index_plans_plan_feature_histories_on_plan_feature_id` (`plan_feature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `plans_plan_features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plans_plan_features` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `plan_id` bigint(20) NOT NULL,
  `plan_feature_id` bigint(20) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_plans_plan_features_on_plan_id_and_plan_feature_id` (`plan_id`,`plan_feature_id`),
  KEY `index_plans_plan_features_on_plan_id` (`plan_id`),
  KEY `index_plans_plan_features_on_plan_feature_id` (`plan_feature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `plans_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plans_products` (
  `plan_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  KEY `index_plans_products_on_plan_id` (`plan_id`),
  KEY `index_plans_products_on_product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `plans_stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plans_stores` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `plan_id` bigint(20) NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_plans_stores_on_plan_id` (`plan_id`),
  KEY `index_plans_stores_on_store_id` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `posting_mappings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posting_mappings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `posting_id` varchar(255) NOT NULL,
  `external_posting_id` bigint(20) NOT NULL,
  `source` enum('sip','symphony') DEFAULT 'sip',
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_posting_mappings_on_posting_id_and_external_posting_id` (`posting_id`,`external_posting_id`),
  KEY `index_posting_mappings_on_posting_id` (`posting_id`),
  KEY `index_posting_mappings_on_external_posting_id` (`external_posting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the mapping between sip/symphony posting id to tc-www''s posting_id';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `posting_royalties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posting_royalties` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `posting_id` varchar(255) NOT NULL,
  `corporate_entity_id` varchar(255) NOT NULL,
  `currency` varchar(3) NOT NULL,
  `total_amount_in_usd` decimal(23,6) DEFAULT NULL,
  `encumbrance_amount_in_usd` decimal(23,6) DEFAULT NULL,
  `split_adjustments_in_usd` decimal(23,6) DEFAULT NULL,
  `total_you_tube_amount_in_usd` decimal(23,6) DEFAULT NULL,
  `converted_amount` decimal(23,6) DEFAULT NULL,
  `base_fx_rate` decimal(23,14) DEFAULT NULL,
  `fx_coverage_rate` decimal(23,14) DEFAULT NULL,
  `fx_rate` decimal(23,14) DEFAULT NULL,
  `approved_by` varchar(255) DEFAULT NULL,
  `approved_on` datetime DEFAULT NULL,
  `state` enum('pending','approved','submitted') DEFAULT 'pending',
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `fx_rate_submitted_by` varchar(255) DEFAULT NULL,
  `fx_rate_submitted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_posting_royalties_on_posting_id` (`posting_id`),
  KEY `index_posting_royalties_on_corporate_entity_id` (`corporate_entity_id`),
  KEY `index_posting_royalties_on_currency` (`currency`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores posting related information required by finance team.';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `preorder_instant_grat_songs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `preorder_instant_grat_songs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `song_id` int(11) DEFAULT NULL,
  `salepoint_preorder_data_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pigs_on_spd_id_and_song_id` (`song_id`,`salepoint_preorder_data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `preorder_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `preorder_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `preorder_purchases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `preorder_purchases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` int(11) NOT NULL,
  `itunes_enabled` tinyint(1) NOT NULL DEFAULT 0,
  `google_enabled` tinyint(1) NOT NULL DEFAULT 0,
  `paid_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `preorder_product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_preorder_purchases_on_album_id` (`album_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `price_adjustment_histories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price_adjustment_histories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `purchase_id` int(10) unsigned NOT NULL COMMENT 'Foreign Key to Purchase Table',
  `admin_id` int(10) unsigned NOT NULL COMMENT 'Foreign Key to People Table',
  `adjustment_cents` int(11) DEFAULT NULL,
  `original_cost_cents` int(11) DEFAULT NULL,
  `original_discount_cents` int(11) DEFAULT NULL,
  `new_cost_cents` int(11) DEFAULT NULL,
  `note` text NOT NULL COMMENT 'Note',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  PRIMARY KEY (`id`),
  KEY `purchase_id` (`purchase_id`),
  KEY `admin_id` (`admin_id`),
  CONSTRAINT `FK_price_adj_people` FOREIGN KEY (`admin_id`) REFERENCES `people` (`id`),
  CONSTRAINT `FK_price_adj_purchases` FOREIGN KEY (`purchase_id`) REFERENCES `purchases` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `price_policies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price_policies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `short_code` varchar(20) DEFAULT NULL,
  `price_calculator` varchar(50) DEFAULT NULL,
  `base_price_cents` int(11) NOT NULL DEFAULT 0,
  `currency` char(3) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `priority_artists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `priority_artists` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) NOT NULL,
  `artist_id` bigint(20) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_priority_artists_on_person_id_and_artist_id` (`person_id`,`artist_id`),
  KEY `index_priority_artists_on_person_id` (`person_id`),
  KEY `index_priority_artists_on_artist_id` (`artist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `product_item_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_item_rules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_item_id` int(10) unsigned NOT NULL COMMENT 'references product.id',
  `base_item_option_id` int(11) DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT 'reference id for parent rule, required for multiple rules that govern prices',
  `rule_type` enum('inventory','entitlement','price_only') DEFAULT NULL COMMENT 'grants a user inventory or an entitlement',
  `rule` enum('price_for_each','total_included','price_above_included','price_for_each_above_included','range','true_false') NOT NULL COMMENT 'processing rule for determining price',
  `inventory_type` varchar(50) NOT NULL COMMENT 'inventory_type of product item being sold',
  `model_to_check` varchar(50) DEFAULT NULL COMMENT 'required for rules other than price_for_each',
  `method_to_check` varchar(50) DEFAULT NULL COMMENT 'method to check on the provided model',
  `quantity` mediumint(9) NOT NULL DEFAULT 1 COMMENT 'inventory granted',
  `unlimited` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'used when inventory granted is unlimited',
  `true_false` tinyint(1) DEFAULT NULL COMMENT 'used for true_false rule',
  `minimum` mediumint(9) DEFAULT NULL COMMENT 'start of range/threshhold',
  `maximum` mediumint(9) DEFAULT NULL COMMENT 'end of range/threshold',
  `entitlement_rights_group_id` int(10) unsigned DEFAULT NULL COMMENT 'references entitlement_rights_groups.id when rule_type is entitlement',
  `price` decimal(10,4) NOT NULL DEFAULT 0.0000 COMMENT 'price when the rules are matched, can be 0.00',
  `currency` char(3) NOT NULL COMMENT 'Currency for product item rule',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `product_item_id` (`product_item_id`),
  CONSTRAINT `fk_product_item_id` FOREIGN KEY (`product_item_id`) REFERENCES `product_items` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `product_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL COMMENT 'references product.id',
  `base_item_id` int(11) DEFAULT NULL,
  `name` varchar(150) NOT NULL COMMENT 'name of product item',
  `description` varchar(255) DEFAULT NULL COMMENT 'optional short description of product item',
  `price` decimal(10,4) NOT NULL DEFAULT 0.0000 COMMENT 'overrides any rules set for this item',
  `currency` char(3) NOT NULL COMMENT 'Currency for product item',
  `does_not_renew` tinyint(1) NOT NULL DEFAULT 0,
  `renewal_type` enum('entitlement','distribution','inventory') DEFAULT NULL COMMENT 'determines how the renewal is process on success or failure',
  `first_renewal_duration` tinyint(4) DEFAULT NULL COMMENT 'deteremines (with renewal_interval) how often long before the first interval/duration expires',
  `first_renewal_interval` enum('month','day','week','year') DEFAULT NULL COMMENT 'determines (with renewal_duration) how long before the first interval/duration expires',
  `renewal_duration` tinyint(4) DEFAULT NULL COMMENT 'deteremines (with renewal_interval) how often an entitlement will renew',
  `renewal_interval` enum('month','day','week','year') DEFAULT NULL COMMENT 'determines (with renewal_duration) how often an entitlement will renew',
  `renewal_product_id` int(10) unsigned DEFAULT NULL COMMENT 'references the product to be purchased when this item renews',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `fk_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `product_purchase_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_purchase_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `purchaseable_id` int(11) NOT NULL,
  `purchaseable_type` enum('Album','Single','Ringtone','Entitlement','Product','Renewal','ItunesUserReport','Video','Salepoint','Song') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ppi_purchase_id` (`purchase_id`),
  KEY `ppi_purchaseable` (`purchaseable_id`,`purchaseable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `product_tax_rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_tax_rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_rate` decimal(10,2) DEFAULT NULL,
  `active_tax_rate` tinyint(1) DEFAULT NULL,
  `description_label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale_id` int(11) DEFAULT NULL,
  `locale_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_product_tax_rates_on_locale_id_and_locale_type` (`locale_id`,`locale_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `original_product_id` int(10) unsigned DEFAULT NULL COMMENT 'references an original product that this was based off of - for comparison only',
  `created_by_id` int(10) unsigned NOT NULL COMMENT 'references people.id of admin who created the product',
  `country_website_id` int(11) unsigned NOT NULL,
  `name` varchar(150) NOT NULL COMMENT 'name of product',
  `flag_text` varchar(15) DEFAULT NULL,
  `display_name` varchar(50) DEFAULT NULL,
  `description` text DEFAULT NULL COMMENT 'optional long description of product',
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Inactive' COMMENT 'determines if the product is sold in the system',
  `product_type` enum('Ad Hoc','Package','Renewal','Renewal Extension','Distribution Extension') NOT NULL DEFAULT 'Package',
  `is_default` smallint(1) NOT NULL DEFAULT 1,
  `applies_to_product` enum('Album','Single','Ringtone','Video','None','Widget','CreditUsage','Booklet','SalepointSubscription','SalepointPreorderData','SpatialAudio') NOT NULL DEFAULT 'None',
  `sort_order` smallint(6) NOT NULL DEFAULT 1,
  `renewal_level` enum('Product','Item','None') NOT NULL DEFAULT 'Item' COMMENT 'tells the system if renewal rules are set at the product or product_item level',
  `first_renewal_duration` tinyint(4) DEFAULT NULL COMMENT 'deteremines (with renewal_interval) how often long before the first interval/duration expires',
  `first_renewal_interval` enum('month','day','week','year') DEFAULT NULL COMMENT 'determines (with renewal_duration) how long before the first interval/duration expires',
  `renewal_duration` tinyint(4) DEFAULT NULL COMMENT 'deteremines (with renewal_interval) how often an entitlement will renew',
  `renewal_interval` enum('month','day','week','year') DEFAULT NULL COMMENT 'determines (with renewal_duration) how often an entitlement will renew',
  `renewal_product_id` int(10) unsigned DEFAULT NULL COMMENT 'references the product to be purchased when this item renews',
  `price` decimal(10,4) DEFAULT 0.0000 COMMENT 'price for Package types, not used for Ad Hoc products',
  `currency` char(3) NOT NULL COMMENT 'Currency for invoice item',
  `expires_at` timestamp NULL DEFAULT NULL COMMENT 'date product can no longer be sold',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `product_family` varchar(255) DEFAULT NULL,
  `product_tax_rate_id` int(11) DEFAULT NULL,
  `revenue_type` enum('Artist Services','Distribution','Publishing','Video') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `product_type` (`product_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `products_chargebee_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products_chargebee_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) NOT NULL,
  `chargebee_item_id` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT 1,
  `interval_type` varchar(255) DEFAULT NULL,
  `interval` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_products_chargebee_items_on_product_id` (`product_id`),
  KEY `index_products_chargebee_items_on_chargebee_item_id` (`chargebee_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `promo_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promo_codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promo` varchar(12) NOT NULL,
  `code` varchar(18) NOT NULL,
  `used` tinyint(1) NOT NULL DEFAULT 0,
  `created_on` date DEFAULT NULL,
  `used_on` date DEFAULT NULL,
  `lock_version` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `index_promo_codes_on_used` (`used`),
  KEY `index_promo_codes_on_promo` (`promo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `promotional_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promotional_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `short_title` varchar(255) DEFAULT NULL,
  `body` text DEFAULT NULL,
  `posted` datetime DEFAULT NULL,
  `online` tinyint(1) DEFAULT 1,
  `position` int(11) DEFAULT NULL,
  `start_at` datetime DEFAULT NULL,
  `finish_at` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `meta_keywords` text DEFAULT NULL,
  `meta_description` text DEFAULT NULL,
  `url_slug` varchar(255) DEFAULT NULL,
  `alt_text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `promotions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promotions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `short_title` varchar(255) DEFAULT NULL,
  `body` text DEFAULT NULL,
  `online` tinyint(1) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `finish` datetime DEFAULT NULL,
  `on_homepage` tinyint(1) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `meta_keywords` text DEFAULT NULL,
  `meta_description` text DEFAULT NULL,
  `url_slug` varchar(255) DEFAULT NULL,
  `alt_text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `start` (`start`),
  KEY `finish` (`finish`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `publishers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publishers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `cae` varchar(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `performing_rights_organization_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_publishers_on_performing_rights_organization_id` (`performing_rights_organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `publishing_composers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publishing_composers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) DEFAULT NULL,
  `account_id` bigint(20) DEFAULT NULL,
  `lod_id` bigint(20) DEFAULT NULL,
  `publisher_id` bigint(20) DEFAULT NULL,
  `country_id` bigint(20) DEFAULT NULL,
  `publishing_role_id` bigint(20) DEFAULT NULL,
  `performing_rights_organization_id` bigint(20) DEFAULT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `address_1` varchar(45) DEFAULT NULL,
  `address_2` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `cae` varchar(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `agreed_to_terms_at` datetime DEFAULT NULL,
  `name_prefix` varchar(6) DEFAULT NULL,
  `name_suffix` varchar(6) DEFAULT NULL,
  `alternate_email` varchar(100) DEFAULT NULL,
  `alternate_phone` varchar(20) DEFAULT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `nosocial` tinyint(1) DEFAULT 0,
  `terminated_at` datetime DEFAULT NULL,
  `provider_identifier` varchar(255) DEFAULT NULL,
  `perform_live` tinyint(1) DEFAULT 0,
  `sync_opted_in` tinyint(1) DEFAULT NULL,
  `sync_opted_updated_at` datetime DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `is_primary_composer` tinyint(1) DEFAULT 0,
  `is_unknown` tinyint(1) DEFAULT 0,
  `legacy_composer_id` bigint(20) DEFAULT NULL,
  `legacy_cowriter_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_publishing_composers_on_person_id` (`person_id`),
  KEY `index_publishing_composers_on_account_id` (`account_id`),
  KEY `index_publishing_composers_on_lod_id` (`lod_id`),
  KEY `index_publishing_composers_on_publisher_id` (`publisher_id`),
  KEY `index_publishing_composers_on_country_id` (`country_id`),
  KEY `index_publishing_composers_on_publishing_role_id` (`publishing_role_id`),
  KEY `index_publishing_composers_on_performing_rights_organization_id` (`performing_rights_organization_id`),
  KEY `index_publishing_composers_on_provider_identifier` (`provider_identifier`),
  KEY `index_publishing_composers_on_legacy_composer_id` (`legacy_composer_id`),
  KEY `index_publishing_composers_on_legacy_cowriter_id` (`legacy_cowriter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `publishing_composition_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publishing_composition_metadata` (
  `publishing_composition_id` bigint(20) NOT NULL,
  `cover_song` tinyint(1) NOT NULL DEFAULT 0,
  `public_domain` tinyint(1) NOT NULL DEFAULT 0,
  `publishing_cover_song_source_id` bigint(20) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`publishing_composition_id`,`publishing_cover_song_source_id`),
  KEY `index_on_publishing_compositions_id` (`publishing_composition_id`),
  KEY `index_on_publishing_cover_song_source_id` (`publishing_cover_song_source_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `publishing_composition_splits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publishing_composition_splits` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `publishing_composer_id` bigint(20) DEFAULT NULL,
  `publishing_composition_id` bigint(20) DEFAULT NULL,
  `right_to_collect` tinyint(1) DEFAULT 1,
  `percent` decimal(5,2) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `legacy_publishing_split_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_publishing_composition_splits_on_publishing_composer_id` (`publishing_composer_id`),
  KEY `index_publishing_composition_splits_on_publishing_composition_id` (`publishing_composition_id`),
  KEY `index_publishing_composition_splits_on_legacy_pub_split_id` (`legacy_publishing_split_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `publishing_compositions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publishing_compositions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `iswc` varchar(255) DEFAULT NULL,
  `is_unallocated_sum` tinyint(1) DEFAULT 0,
  `state` varchar(25) DEFAULT NULL,
  `prev_state` varchar(25) DEFAULT NULL,
  `state_updated_on` date DEFAULT NULL,
  `provider_identifier` varchar(255) DEFAULT NULL,
  `translated_name` varchar(255) DEFAULT NULL,
  `verified_date` datetime DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `legacy_composition_id` bigint(20) DEFAULT NULL,
  `public_domain` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `index_publishing_compositions_on_account_id` (`account_id`),
  KEY `index_publishing_compositions_on_provider_identifier` (`provider_identifier`),
  KEY `index_publishing_compositions_on_legacy_composition_id` (`legacy_composition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `publishing_cover_song_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publishing_cover_song_source` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `source_name` varchar(24) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `publishing_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publishing_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_publishing_roles_on_title` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `publishing_splits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publishing_splits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `composer_id` int(11) DEFAULT NULL,
  `composition_id` int(11) DEFAULT NULL,
  `percent` decimal(5,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `writer_id` int(11) DEFAULT NULL,
  `writer_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `composer_id` (`composer_id`,`composition_id`),
  KEY `composition_id` (`composition_id`),
  KEY `index_publishing_splits_on_writer_id_and_writer_type` (`writer_id`,`writer_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `publishing_termination_reasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publishing_termination_reasons` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reason` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `purchase_tax_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchase_tax_information` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(11) NOT NULL,
  `customer_location` varchar(255) DEFAULT NULL,
  `vat_registration_number` varchar(255) DEFAULT NULL,
  `tax_type` varchar(255) DEFAULT NULL,
  `tax_rate` float DEFAULT NULL,
  `trader_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place_of_supply` varchar(255) DEFAULT NULL,
  `error_code` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `customer_type` enum('Individual','Business') DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_purchase_tax_information_on_purchase_id` (`purchase_id`),
  KEY `idx_purchase_tax_info_vat_reg_num_customer_location` (`vat_registration_number`,`customer_location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `purchases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchases` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `person_id` int(10) unsigned NOT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `cost_cents` int(11) DEFAULT NULL,
  `discount_cents` int(11) DEFAULT 0,
  `currency` char(3) NOT NULL COMMENT 'Currency for cost_cents',
  `paid_at` datetime DEFAULT NULL,
  `purchase_id_old` int(11) DEFAULT NULL,
  `salepoint_id` int(11) DEFAULT NULL,
  `no_purchase_album_id` int(11) DEFAULT NULL,
  `related_id` int(10) unsigned DEFAULT NULL,
  `related_type` varchar(255) DEFAULT NULL,
  `targeted_product_id` int(10) unsigned DEFAULT NULL,
  `price_adjustment_histories_count` int(11) NOT NULL DEFAULT 0 COMMENT 'Count of times the purchase has been price adjusted',
  `price_policy` text DEFAULT NULL,
  `vat_amount_cents` int(11) DEFAULT 0,
  `status` varchar(255) DEFAULT 'new',
  `vat_amount_cents_in_eur` int(11) DEFAULT 0,
  `discount_reason` varchar(255) DEFAULT 'no discount',
  PRIMARY KEY (`id`),
  KEY `index_purchases_on_person_id` (`person_id`),
  KEY `index_purchases_on_invoice_id` (`invoice_id`),
  KEY `index_purchases_on_paid_at` (`paid_at`),
  KEY `ix_purchases_on_product_id` (`product_id`),
  KEY `ix_purchases_on_related` (`related_id`,`related_type`),
  KEY `ix_purchases_targeted_product_id` (`targeted_product_id`),
  KEY `index_purchases_on_discount_reason` (`discount_reason`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `query_builders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `query_builders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_query_id` int(11) DEFAULT NULL,
  `col` varchar(255) DEFAULT NULL,
  `operator` varchar(255) DEFAULT NULL,
  `val` varchar(255) DEFAULT NULL,
  `tbl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `recordings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recordings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `composition_id` int(11) DEFAULT NULL,
  `recordable_id` int(11) DEFAULT NULL,
  `recordable_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recording_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `publishing_composition_id` int(11) DEFAULT NULL,
  `isrc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_recordings_on_composition_id` (`composition_id`),
  KEY `index_recordings_on_recordable_type_and_recordable_id` (`recordable_type`,`recordable_id`),
  KEY `index_recordings_on_publishing_composition_id` (`publishing_composition_id`),
  CONSTRAINT `fk_rails_bd012df556` FOREIGN KEY (`composition_id`) REFERENCES `compositions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `redistribution_packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `redistribution_packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `artist_id` int(11) DEFAULT NULL,
  `state` varchar(255) DEFAULT 'pending',
  `converter_class` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `person_artist_store` (`person_id`,`artist_id`,`converter_class`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `referral_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `referral_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `page` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `source` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `page_source` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_referral_data_on_person_id` (`person_id`),
  KEY `index_referral_data_on_key` (`key`),
  KEY `index_referral_data_on_value` (`value`),
  KEY `index_referral_data_on_page` (`page`),
  KEY `index_referral_data_on_source` (`source`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `referral_data_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `referral_data_tmp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `referral` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `referral_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `referral_campaign` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_referral_data_tmp_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `refund_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `refund_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `refund_id` bigint(20) NOT NULL,
  `purchase_id` bigint(20) NOT NULL,
  `base_amount_cents` int(11) DEFAULT 0,
  `tax_amount_cents` int(11) DEFAULT 0,
  `vat_cents_in_euro` int(11) DEFAULT 0,
  `currency` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_refund_items_on_refund_id` (`refund_id`),
  KEY `index_refund_items_on_purchase_id` (`purchase_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `refund_reasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `refund_reasons` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reason` varchar(255) NOT NULL,
  `visible` tinyint(1) DEFAULT 1,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `refund_settlements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `refund_settlements` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `refund_id` bigint(20) DEFAULT NULL,
  `settlement_amount_cents` int(11) DEFAULT 0,
  `source_id` int(11) DEFAULT NULL,
  `source_type` varchar(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_refund_settlements_on_refund_id` (`refund_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `refunds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `refunds` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoice_id` bigint(20) NOT NULL,
  `invoice_sequence` int(11) DEFAULT NULL,
  `corporate_entity_id` bigint(20) NOT NULL,
  `base_amount_cents` int(11) DEFAULT 0,
  `tax_amount_cents` int(11) DEFAULT 0,
  `total_amount_cents` int(11) DEFAULT 0,
  `vat_cents_in_euro` int(11) DEFAULT 0,
  `currency` varchar(255) DEFAULT NULL,
  `tax_inclusive` tinyint(1) DEFAULT 0,
  `admin_note` text DEFAULT NULL,
  `refunded_by_id` int(11) DEFAULT NULL,
  `status` enum('success','error','pending') NOT NULL DEFAULT 'success' COMMENT 'status of transaction',
  `failure_reason` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `refund_reason_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_refunds_on_corporate_entity_id_and_invoice_sequence` (`corporate_entity_id`,`invoice_sequence`),
  KEY `index_refunds_on_invoice_id` (`invoice_id`),
  KEY `index_refunds_on_corporate_entity_id` (`corporate_entity_id`),
  KEY `index_refunds_on_refunded_by_id` (`refunded_by_id`),
  KEY `index_refunds_on_refund_reason_id` (`refund_reason_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `related_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `related_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `related_product_id` int(10) NOT NULL COMMENT 'id of related product',
  `product_id` int(10) NOT NULL COMMENT 'product this upsell product is related to',
  `display_name` varchar(50) DEFAULT NULL COMMENT 'override display name for product table',
  `description` varchar(100) DEFAULT NULL COMMENT 'override description for product table',
  `sort_order` smallint(6) NOT NULL COMMENT 'sort order of upsell offers',
  `start_date` timestamp NULL DEFAULT NULL COMMENT 'upsell starts on',
  `expiration_date` timestamp NULL DEFAULT NULL COMMENT 'upsell ends on',
  `created_by_id` int(10) unsigned NOT NULL COMMENT 'id of administrator who created the offer',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `renewal_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `renewal_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `renewal_id` int(10) unsigned NOT NULL,
  `purchase_id` int(10) unsigned NOT NULL,
  `starts_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'start date of renewal history record',
  `expires_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'expiration date on which this particular renewal will expire',
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_id` (`purchase_id`),
  KEY `renewal` (`renewal_id`,`starts_at`,`expires_at`),
  CONSTRAINT `fk_renewal` FOREIGN KEY (`renewal_id`) REFERENCES `renewals` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `renewal_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `renewal_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `renewal_id` int(10) unsigned NOT NULL,
  `related_id` int(10) unsigned NOT NULL,
  `related_type` enum('Album','Single','Ringtone','Entitlement','Product','Video','Widget','PersonPlan') NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `related` (`related_id`,`related_type`),
  KEY `renewal` (`renewal_id`),
  CONSTRAINT `fk_renewal_items` FOREIGN KEY (`renewal_id`) REFERENCES `renewals` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `renewal_product_changes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `renewal_product_changes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `renewal_id` int(10) unsigned NOT NULL,
  `old_item_to_renew_id` int(10) unsigned NOT NULL,
  `old_item_to_renew_type` enum('Product','ProductItem') NOT NULL,
  `old_duration` tinyint(4) DEFAULT NULL COMMENT 'deteremines (with renewal_interval) how often an entitlement will renew',
  `old_interval` enum('month','day','week','year') DEFAULT NULL COMMENT 'old renewal interval',
  `new_item_to_renew_id` int(10) unsigned NOT NULL,
  `new_item_to_renew_type` enum('Product','ProductItem') NOT NULL,
  `new_duration` tinyint(4) DEFAULT NULL COMMENT 'deteremines (with renewal_interval) how often an entitlement will renew',
  `new_interval` enum('month','day','week','year') DEFAULT NULL COMMENT 'old renewal interval',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  PRIMARY KEY (`id`),
  KEY `renewal_product_changes_renewal_id` (`renewal_id`),
  KEY `renewal_product_changes_old_item_to_renew` (`old_item_to_renew_id`,`old_item_to_renew_type`),
  KEY `renewal_product_changes_new_item_to_renew` (`new_item_to_renew_id`,`new_item_to_renew_type`),
  CONSTRAINT `FK_rpc_renewal_id` FOREIGN KEY (`renewal_id`) REFERENCES `renewals` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `renewals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `renewals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL,
  `item_to_renew_id` int(10) unsigned NOT NULL,
  `item_to_renew_type` enum('Product','ProductItem') NOT NULL,
  `canceled_at` timestamp NULL DEFAULT NULL COMMENT 'when did the user cancel this renewal',
  `takedown_at` timestamp NULL DEFAULT NULL COMMENT 'set when the user wants to take down a distributed item',
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `item_to_renew` (`item_to_renew_id`,`item_to_renew_type`),
  CONSTRAINT `fk_renewals_person_id` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `renewals_batch_failures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `renewals_batch_failures` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `resolved` tinyint(1) DEFAULT 0,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_renewals_batch_failures_on_date` (`date`),
  KEY `index_renewals_batch_failures_on_resolved` (`resolved`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `renewals_legacy_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `renewals_legacy_temp` (
  `person_id` int(10) DEFAULT NULL,
  `album_id` int(10) DEFAULT NULL,
  `expires_at` date DEFAULT NULL,
  `original_cost` decimal(4,2) DEFAULT NULL,
  `mail_cohort` date DEFAULT NULL,
  KEY `album_id` (`album_id`),
  KEY `person_id_idx` (`person_id`),
  KEY `mail_cohort_idx` (`mail_cohort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `reporting_dashboard_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reporting_dashboard_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reporting_date_id` int(11) DEFAULT NULL,
  `report_date` datetime DEFAULT NULL,
  `country_website_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_family` varchar(255) DEFAULT NULL,
  `product_count` int(11) DEFAULT NULL,
  `product_amount` decimal(12,2) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_product_report_date` (`reporting_date_id`),
  CONSTRAINT `fk_product_report_date` FOREIGN KEY (`reporting_date_id`) REFERENCES `reporting_dates` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `reporting_dashboard_rollups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reporting_dashboard_rollups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reporting_date_id` int(11) DEFAULT NULL,
  `report_date` datetime DEFAULT NULL,
  `country_website_id` int(11) DEFAULT NULL,
  `total_sales` decimal(12,2) DEFAULT NULL,
  `average_sales` decimal(12,2) DEFAULT NULL,
  `new_dist_first_purchase` int(11) DEFAULT NULL,
  `new_dist_not_first_purchase` int(11) DEFAULT NULL,
  `returning_dist` int(11) DEFAULT NULL,
  `renewals` int(11) DEFAULT NULL,
  `pub_first_purchase` int(11) DEFAULT NULL,
  `pub_not_first_purchase` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rollup_report_date` (`reporting_date_id`),
  CONSTRAINT `fk_rollup_report_date` FOREIGN KEY (`reporting_date_id`) REFERENCES `reporting_dates` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `reporting_dates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reporting_dates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reporting_date` datetime DEFAULT NULL,
  `day_of_month` int(11) DEFAULT NULL,
  `day_of_year` int(11) DEFAULT NULL,
  `day_display` varchar(255) DEFAULT NULL,
  `week_of_year` int(11) DEFAULT NULL,
  `week_year` int(11) DEFAULT NULL,
  `week_display` varchar(255) DEFAULT NULL,
  `month_of_year` int(11) DEFAULT NULL,
  `month_year` int(11) DEFAULT NULL,
  `month_display` varchar(255) DEFAULT NULL,
  `quarter` int(11) DEFAULT NULL,
  `quarter_year` int(11) DEFAULT NULL,
  `quarter_display` varchar(255) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `reporting_months`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reporting_months` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_year` int(11) NOT NULL DEFAULT 0,
  `report_month` int(11) NOT NULL DEFAULT 0,
  `report_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`),
  KEY `reporting_month_date` (`report_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `revenue_streams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `revenue_streams` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `code_info` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `revenue_streams_transaction_mappings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `revenue_streams_transaction_mappings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `revenue_stream_id` bigint(20) NOT NULL,
  `person_transaction_type` varchar(255) NOT NULL,
  `balance_adjustment_category` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_revenue_streams_transaction_mappings_on_revenue_stream_id` (`revenue_stream_id`),
  CONSTRAINT `fk_rails_4e44caedf9` FOREIGN KEY (`revenue_stream_id`) REFERENCES `revenue_streams` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `review_audits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review_audits` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `album_id` int(11) DEFAULT NULL COMMENT 'Foreign key to albums table.',
  `person_id` int(10) unsigned DEFAULT NULL COMMENT 'Foreign key to people table.',
  `event` enum('STARTED REVIEW','APPROVED','FLAGGED','REJECTED','SKIPPED','NOT SURE') NOT NULL DEFAULT 'STARTED REVIEW' COMMENT 'Indicates content review event',
  `note` varchar(255) DEFAULT NULL COMMENT 'any additional note for the decision',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Date record was created.',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp() COMMENT 'Date last updated.',
  PRIMARY KEY (`id`),
  KEY `key_review_audits_album_id` (`album_id`),
  KEY `key_review_audits_person_id` (`person_id`),
  CONSTRAINT `FK_review_audits_album_id` FOREIGN KEY (`album_id`) REFERENCES `albums` (`id`),
  CONSTRAINT `FK_review_audits_person_id` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='content review audits';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `review_audits_review_reasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review_audits_review_reasons` (
  `review_audit_id` int(11) unsigned NOT NULL COMMENT 'Foreign key to review_audits table.',
  `review_reason_id` smallint(5) unsigned NOT NULL COMMENT 'Foreign key to review_reasons table.',
  KEY `review_audits_review_reasons_review_audit_id` (`review_audit_id`),
  KEY `review_audits_review_reasons_review_reason_id` (`review_reason_id`),
  CONSTRAINT `FK_review_audits_review_reasons_review_audit` FOREIGN KEY (`review_audit_id`) REFERENCES `review_audits` (`id`),
  CONSTRAINT `FK_review_audits_review_reasons_review_reason` FOREIGN KEY (`review_reason_id`) REFERENCES `review_reasons` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Links a review_reason to review_audit';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `review_reasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review_reasons` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `reason_type` enum('FLAGGED','REJECTED','NOT SURE') DEFAULT NULL COMMENT 'Indicates the type of reason',
  `reason` varchar(255) DEFAULT NULL COMMENT 'reason why the content review decision has been made',
  `review_type` enum('LEGAL','STYLE') NOT NULL DEFAULT 'LEGAL' COMMENT 'Indicates content review type',
  `email_template_path` varchar(255) DEFAULT NULL COMMENT 'email template path to notify users of their rejected release',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Date record was created.',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp() COMMENT 'Date last updated.',
  `rejection_email_id` int(11) DEFAULT NULL,
  `should_unfinalize` tinyint(1) NOT NULL DEFAULT 0,
  `primary` tinyint(1) DEFAULT 0,
  `template_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rejection_email_id_ix` (`rejection_email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='content review reasons';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `review_reasons_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review_reasons_roles` (
  `review_reason_id` smallint(5) unsigned NOT NULL COMMENT 'Foreign key to review_reasons table.',
  `role_id` int(11) NOT NULL COMMENT 'Foreign key to roles table.',
  KEY `cr_reason_roles_review_reason_id` (`review_reason_id`),
  KEY `cr_reasons_roles_role_id` (`role_id`),
  CONSTRAINT `FK_review_reasons_roles_reason` FOREIGN KEY (`review_reason_id`) REFERENCES `review_reasons` (`id`),
  CONSTRAINT `FK_review_reasons_roles_role` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Links a review_reason to role';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `rewards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rewards` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `points` int(11) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `rights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `controller` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `rights_app_errors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rights_app_errors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `api_endpoint` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `http_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `http_status_code` int(11) DEFAULT NULL,
  `requestable_id` int(11) DEFAULT NULL,
  `requestable_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `json_request` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `json_response` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_rights_app_errors_on_http_method` (`http_method`),
  KEY `index_rights_app_errors_on_requestable_id_and_requestable_type` (`requestable_id`,`requestable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `rights_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rights_roles` (
  `right_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ringtone_related_songs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ringtone_related_songs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `song_id` int(11) unsigned NOT NULL,
  `album_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `long_name` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_administrative` smallint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `royalty_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `royalty_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `composer_id` int(11) DEFAULT NULL,
  `amount` decimal(23,14) DEFAULT NULL,
  `period_year` int(11) DEFAULT NULL,
  `period_interval` int(11) DEFAULT NULL,
  `period_sort` date DEFAULT NULL,
  `pdf_summary_file_name` varchar(255) DEFAULT NULL,
  `pdf_summary_file_type` varchar(255) DEFAULT NULL,
  `pdf_summary_file_size` int(11) DEFAULT NULL,
  `pdf_summary_updated_at` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `period_type` enum('monthly','quarterly') CHARACTER SET binary DEFAULT 'quarterly',
  `posting_type` varchar(255) DEFAULT NULL,
  `posting_id` int(11) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `hold_payment` tinyint(1) DEFAULT 0,
  `hold_type` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `publishing_composer_id` int(11) DEFAULT NULL,
  `csv_summary_file_name` varchar(255) DEFAULT NULL,
  `csv_summary_file_size` int(11) DEFAULT NULL,
  `csv_summary_file_updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `royalty_split_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `royalty_split_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) NOT NULL,
  `royalty_split_intake_id` bigint(20) NOT NULL,
  `royalty_split_id` bigint(20) NOT NULL,
  `royalty_split_title` varchar(255) DEFAULT NULL,
  `song_id` bigint(20) NOT NULL,
  `person_intake_id` bigint(20) DEFAULT NULL,
  `owner_id` int(10) unsigned NOT NULL COMMENT 'Main Account Holder person_id for split, the only admin',
  `amount` decimal(23,14) DEFAULT NULL,
  `currency` varchar(3) NOT NULL,
  `royalty_split_config` text DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_royalty_split_details_on_person_id` (`person_id`),
  KEY `index_royalty_split_details_on_royalty_split_intake_id` (`royalty_split_intake_id`),
  KEY `index_royalty_split_details_on_royalty_split_id` (`royalty_split_id`),
  KEY `index_royalty_split_details_on_song_id` (`song_id`),
  KEY `index_royalty_split_details_on_person_intake_id` (`person_intake_id`),
  KEY `index_royalty_split_details_on_owner_id` (`owner_id`),
  KEY `index_royalty_split_details_on_currency` (`currency`),
  CONSTRAINT `fk_rails_a06a9afda3` FOREIGN KEY (`owner_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `royalty_split_intakes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `royalty_split_intakes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) NOT NULL,
  `amount` decimal(23,14) DEFAULT NULL,
  `currency` varchar(3) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_royalty_split_intakes_on_person_id` (`person_id`),
  KEY `index_royalty_split_intakes_on_currency` (`currency`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `royalty_split_recipients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `royalty_split_recipients` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `royalty_split_id` bigint(20) NOT NULL COMMENT 'Royalty split config',
  `person_id` int(10) unsigned DEFAULT NULL COMMENT 'Person that gets a split of royalties',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email addresses are only for split recipients not registered at tunecore yet, used instead of person_id',
  `percent` decimal(6,2) NOT NULL COMMENT 'Percent of royalties recieved by recipient',
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `deactivated_at` timestamp NULL DEFAULT NULL,
  `accepted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_royalty_split_recipients_on_person_id_and_royalty_split_id` (`person_id`,`royalty_split_id`),
  UNIQUE KEY `index_royalty_split_recipients_on_email_and_royalty_split_id` (`email`,`royalty_split_id`),
  KEY `index_royalty_split_recipients_on_royalty_split_id` (`royalty_split_id`),
  KEY `index_royalty_split_recipients_on_person_id` (`person_id`),
  CONSTRAINT `fk_rails_36b58f6e54` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`),
  CONSTRAINT `fk_rails_7849ead086` FOREIGN KEY (`royalty_split_id`) REFERENCES `royalty_splits` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `royalty_split_songs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `royalty_split_songs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `royalty_split_id` bigint(20) NOT NULL COMMENT 'Royalty split config id',
  `song_id` int(11) NOT NULL COMMENT 'Song associated with a split',
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_royalty_split_songs_on_song_id` (`song_id`),
  KEY `index_royalty_split_songs_on_royalty_split_id` (`royalty_split_id`),
  CONSTRAINT `fk_rails_0d72191fa7` FOREIGN KEY (`royalty_split_id`) REFERENCES `royalty_splits` (`id`),
  CONSTRAINT `fk_rails_1433ea6ca4` FOREIGN KEY (`song_id`) REFERENCES `songs` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `royalty_splits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `royalty_splits` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT 'Title of split',
  `owner_id` int(10) unsigned NOT NULL COMMENT 'Main Account Holder person_id for split, the only admin',
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `deactivated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_royalty_splits_on_title_and_owner_id` (`title`,`owner_id`),
  KEY `index_royalty_splits_on_owner_id` (`owner_id`),
  CONSTRAINT `fk_rails_c67e44642f` FOREIGN KEY (`owner_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `royalty_stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `royalty_stores` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_royalty_stores_on_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores information that will be used for royalty purposes. Eventually will be merged with sip_stores';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `royalty_sub_stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `royalty_sub_stores` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `royalty_store_id` bigint(20) NOT NULL,
  `royalty_rate` decimal(14,2) NOT NULL DEFAULT 1.00 COMMENT 'Value between 0 and 1',
  `tariff_rate` decimal(14,2) NOT NULL DEFAULT 0.00,
  `sales_period_type` enum('monthly','quarterly') NOT NULL DEFAULT 'monthly',
  `royalty_source` enum('symphony','sip') NOT NULL DEFAULT 'sip',
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_royalty_sub_stores_on_name` (`name`),
  KEY `index_royalty_sub_stores_on_royalty_store_id` (`royalty_store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Structure to add sub stores belonging to a royalty store';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `s3_assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s3_assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `bucket` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `uuid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `s3_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s3_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_size` bigint(20) DEFAULT NULL,
  `file_type` varchar(255) DEFAULT NULL,
  `metadata_json` text DEFAULT NULL,
  `s3_asset_id` int(11) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_s3_details_on_s3_asset_id` (`s3_asset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `salepoint_audits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salepoint_audits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` int(11) DEFAULT NULL,
  `salepoint_id` int(11) DEFAULT NULL,
  `status` varchar(20) NOT NULL DEFAULT '',
  `reported_status` varchar(20) DEFAULT NULL,
  `report_type` varchar(20) DEFAULT NULL,
  `created_on` date NOT NULL DEFAULT '0000-00-00',
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `salepoint_audits_album_id_index` (`album_id`),
  KEY `salepoint_audits_salepoint_id_index` (`salepoint_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `salepoint_preorder_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salepoint_preorder_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `salepoint_id` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `preview_songs` tinyint(1) DEFAULT NULL,
  `variable_price_id` int(11) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL,
  `paid_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `preorder_purchase_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_salepoint_preorder_data_on_salepoint_id` (`salepoint_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `salepoint_songs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salepoint_songs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `takedown_at` datetime DEFAULT NULL,
  `state` varchar(255) NOT NULL DEFAULT 'new',
  `salepoint_id` int(11) NOT NULL,
  `song_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `reason_for_rejection` varchar(255) DEFAULT NULL,
  `comments_for_rejection` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_unique_salepoint_song_ids` (`salepoint_id`,`song_id`),
  KEY `index_salepoint_songs_on_song_id` (`song_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `salepoint_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salepoint_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` int(11) NOT NULL,
  `is_active` tinyint(1) DEFAULT 1,
  `effective` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `finalized_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_salepoint_subscriptions_on_album_id` (`album_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `salepointable_stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salepointable_stores` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Priimary Key',
  `salepointable_type` enum('Album','Single','Ringtone') NOT NULL COMMENT 'Type of distribution.',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Foreign key to stores table.',
  PRIMARY KEY (`id`),
  KEY `store_id` (`store_id`),
  CONSTRAINT `FK_salepointable_stores` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `salepoints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salepoints` (
  `store_id` int(3) NOT NULL DEFAULT 0,
  `salepointable_id` int(80) DEFAULT NULL,
  `status` varchar(40) NOT NULL DEFAULT 'new',
  `current_audit_id` int(11) DEFAULT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `has_rights_assignment` tinyint(1) NOT NULL DEFAULT 0,
  `price_policy_id` int(10) unsigned DEFAULT 0,
  `payment_applied` tinyint(1) NOT NULL DEFAULT 0,
  `finalized_at` datetime DEFAULT NULL,
  `salepointable_type` varchar(255) DEFAULT NULL,
  `variable_price_id` int(11) DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `store_identifier` varchar(18) DEFAULT NULL,
  `takedown_at` datetime DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  `track_variable_price_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `store_id` (`store_id`),
  KEY `album_id` (`salepointable_id`),
  KEY `current_audit_id` (`current_audit_id`),
  KEY `salepoints_chargeable_id_index` (`price_policy_id`),
  KEY `index_salepoints_on_purchase_id` (`purchase_id`),
  KEY `salepointable_id_index` (`salepointable_id`),
  KEY `salepointable_type_index` (`salepointable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sales_by_year`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_by_year` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `related_type` varchar(255) DEFAULT NULL,
  `related_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_sales_by_year_on_year_and_store_id` (`year`,`store_id`),
  KEY `index_sales_by_year_on_related_type_and_related_id` (`related_type`,`related_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sales_record_archives`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_record_archives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL COMMENT 'customer identifier',
  `person_intake_id` int(10) unsigned DEFAULT NULL COMMENT 'related person_intake',
  `sip_sales_record_id` int(10) unsigned DEFAULT NULL COMMENT 'pointer back to sip',
  `sales_record_master_id` int(10) unsigned DEFAULT NULL,
  `related_id` int(10) NOT NULL COMMENT 'record identifier',
  `related_type` enum('Album','Song','Video') DEFAULT NULL COMMENT 'record type identifier',
  `distribution_type` enum('Download','Streaming') DEFAULT NULL COMMENT 'type of distribution - download or streaming',
  `quantity` int(10) NOT NULL COMMENT 'total sales for this record',
  `revenue_per_unit` decimal(23,14) NOT NULL COMMENT 'revenue per unit',
  `revenue_total` decimal(23,14) NOT NULL COMMENT 'total revenue',
  `amount` decimal(23,14) DEFAULT NULL COMMENT 'USD Amount Entered',
  PRIMARY KEY (`id`),
  KEY `s_ix_sales_record_master` (`sales_record_master_id`),
  KEY `s_ix_sip_sales_record_id` (`sip_sales_record_id`),
  KEY `s_ix_sales_records_person_intakes` (`person_intake_id`),
  KEY `s_ix_sales_records_related` (`related_id`,`related_type`),
  KEY `s_ix_sales_records_person_master` (`person_id`,`sales_record_master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sales_record_masters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_record_masters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_website_id` int(11) unsigned DEFAULT 1 COMMENT 'Foreign key to country_websites',
  `sip_store_id` smallint(5) unsigned DEFAULT NULL,
  `status` enum('new','processing','posted') NOT NULL DEFAULT 'new' COMMENT 'status of crediting money to the users account used by the posting stored procedure',
  `period_interval` tinyint(4) NOT NULL COMMENT 'number representing sales period month or quarter',
  `period_year` smallint(5) NOT NULL COMMENT 'year of sales period',
  `period_type` enum('monthly','quarterly') DEFAULT 'monthly' COMMENT 'is this a quarterly or monthly sales period',
  `period_sort` date DEFAULT NULL COMMENT 'first day of the sales period month. allows easier sorting',
  `revenue_currency` varchar(3) DEFAULT NULL COMMENT 'Currency the amount is denominated in',
  `exchange_rate` decimal(23,14) DEFAULT NULL COMMENT 'Exchange Rate',
  `tariff` decimal(23,14) DEFAULT NULL COMMENT 'tariff percentage',
  `tariff_name` varchar(50) DEFAULT NULL COMMENT 'tariff name used for display',
  `amount_currency` char(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency',
  `country` varchar(56) NOT NULL COMMENT 'Country of Sale in two character ISO code',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `summarized` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `posting_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `s_is_master_composite_temp` (`sip_store_id`,`country`,`revenue_currency`,`period_sort`,`period_interval`,`period_year`),
  KEY `idx_srm_created_at` (`created_at`),
  CONSTRAINT `FK_srm_sip_store_id` FOREIGN KEY (`sip_store_id`) REFERENCES `sip_stores` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sales_record_masters_new`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_record_masters_new` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_website_id` int(11) unsigned DEFAULT 1 COMMENT 'Foreign key to country_websites',
  `sip_store_id` smallint(5) unsigned DEFAULT NULL,
  `status` enum('new','processing','posted') NOT NULL DEFAULT 'new' COMMENT 'status of crediting money to the users account used by the posting stored procedure',
  `period_interval` tinyint(4) NOT NULL COMMENT 'number representing sales period month or quarter',
  `period_year` smallint(5) NOT NULL COMMENT 'year of sales period',
  `period_type` enum('monthly','quarterly') DEFAULT 'monthly' COMMENT 'is this a quarterly or monthly sales period',
  `period_sort` date DEFAULT NULL COMMENT 'first day of the sales period month. allows easier sorting',
  `revenue_currency` varchar(3) DEFAULT NULL COMMENT 'Currency the amount is denominated in',
  `exchange_rate` decimal(23,14) DEFAULT NULL COMMENT 'Exchange Rate',
  `tariff` decimal(23,14) DEFAULT NULL COMMENT 'tariff percentage',
  `tariff_name` varchar(50) DEFAULT NULL COMMENT 'tariff name used for display',
  `amount_currency` char(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency',
  `country` varchar(56) NOT NULL COMMENT 'Country of Sale in two character ISO code',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `summarized` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `posting_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `s_is_master_composite_temp` (`sip_store_id`,`country`,`revenue_currency`,`period_sort`,`period_interval`,`period_year`),
  KEY `idx_srm_created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sales_record_summaries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_record_summaries` (
  `person_id` int(10) unsigned NOT NULL,
  `sales_record_master_id` int(10) unsigned NOT NULL,
  `related_type` enum('Album','Song','Video') NOT NULL,
  `related_id` int(10) NOT NULL,
  `release_type` enum('Album','Single','Ringtone','Video') DEFAULT NULL,
  `release_id` int(10) DEFAULT NULL,
  `downloads_sold` int(10) DEFAULT 0,
  `streams_sold` int(10) DEFAULT 0,
  `download_amount` decimal(15,6) DEFAULT 0.000000,
  `stream_amount` decimal(15,6) DEFAULT 0.000000,
  `person_intake_id` int(10) DEFAULT NULL,
  KEY `idx_srs_person_srm_related` (`person_id`,`sales_record_master_id`,`related_type`,`related_id`),
  KEY `idx_srs_srm_id` (`sales_record_master_id`),
  KEY `idx_srs_pi_id` (`person_intake_id`),
  KEY `idx_srs_p_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sales_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_records` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL COMMENT 'customer identifier',
  `person_intake_id` int(10) unsigned DEFAULT NULL COMMENT 'related person_intake',
  `sip_sales_record_id` bigint(20) DEFAULT NULL,
  `sales_record_master_id` int(10) unsigned DEFAULT NULL,
  `related_id` int(10) NOT NULL COMMENT 'record identifier',
  `related_type` enum('Album','Song','Video') DEFAULT NULL COMMENT 'record type identifier',
  `distribution_type` enum('Download','Streaming') DEFAULT NULL COMMENT 'type of distribution - download or streaming',
  `quantity` int(10) NOT NULL COMMENT 'total sales for this record',
  `revenue_per_unit` decimal(23,14) NOT NULL COMMENT 'revenue per unit',
  `revenue_total` decimal(23,14) NOT NULL COMMENT 'total revenue',
  `amount` decimal(23,14) DEFAULT NULL COMMENT 'USD Amount Entered',
  `posting_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `s_ix_sales_record_master` (`sales_record_master_id`),
  KEY `s_ix_sip_sales_record_id` (`sip_sales_record_id`),
  KEY `s_ix_sales_records_person_intakes` (`person_intake_id`),
  KEY `s_ix_sales_records_related` (`related_id`,`related_type`),
  KEY `s_ix_sales_records_person_master` (`person_id`,`sales_record_master_id`),
  CONSTRAINT `sales_records_ibfk_1` FOREIGN KEY (`person_intake_id`) REFERENCES `person_intakes` (`id`),
  CONSTRAINT `sales_records_ibfk_2` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`),
  CONSTRAINT `sales_records_ibfk_3` FOREIGN KEY (`sales_record_master_id`) REFERENCES `sales_record_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sales_records_new`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_records_new` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL COMMENT 'customer identifier',
  `person_intake_id` int(10) unsigned DEFAULT NULL COMMENT 'related person_intake',
  `sip_sales_record_id` bigint(20) DEFAULT NULL,
  `sales_record_master_id` int(10) unsigned DEFAULT NULL,
  `related_id` int(10) NOT NULL COMMENT 'record identifier',
  `related_type` enum('Album','Song','Video') DEFAULT NULL COMMENT 'record type identifier',
  `distribution_type` enum('Download','Streaming') DEFAULT NULL COMMENT 'type of distribution - download or streaming',
  `quantity` int(10) NOT NULL COMMENT 'total sales for this record',
  `revenue_per_unit` decimal(23,14) NOT NULL COMMENT 'revenue per unit',
  `revenue_total` decimal(23,14) NOT NULL COMMENT 'total revenue',
  `amount` decimal(23,14) DEFAULT NULL COMMENT 'USD Amount Entered',
  `reporting_month_id_temp` int(10) unsigned NOT NULL DEFAULT 0,
  `posting_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sr_reporting_month_temp` (`reporting_month_id_temp`),
  KEY `ix_sales_record_master` (`sales_record_master_id`),
  KEY `ix_sales_records_person` (`person_id`),
  KEY `ix_sales_records_person_intakes` (`person_intake_id`),
  KEY `ix_sales_records_related` (`related_id`,`related_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sales_records_new_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_records_new_index` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL COMMENT 'customer identifier',
  `person_intake_id` int(10) unsigned DEFAULT NULL COMMENT 'related person_intake',
  `sip_sales_record_id` int(10) unsigned DEFAULT NULL COMMENT 'pointer back to sip',
  `sales_record_master_id` int(10) unsigned DEFAULT NULL,
  `related_id` int(10) NOT NULL COMMENT 'record identifier',
  `related_type` enum('Album','Song','Video') DEFAULT NULL COMMENT 'record type identifier',
  `distribution_type` enum('Download','Streaming') DEFAULT NULL COMMENT 'type of distribution - download or streaming',
  `quantity` int(10) NOT NULL COMMENT 'total sales for this record',
  `revenue_per_unit` decimal(23,14) NOT NULL COMMENT 'revenue per unit',
  `revenue_total` decimal(23,14) NOT NULL COMMENT 'total revenue',
  `amount` decimal(23,14) DEFAULT NULL COMMENT 'USD Amount Entered',
  PRIMARY KEY (`id`),
  KEY `s_ix_sales_record_master` (`sales_record_master_id`),
  KEY `s_ix_sip_sales_record_id` (`sip_sales_record_id`),
  KEY `s_ix_sales_records_person_intakes` (`person_intake_id`),
  KEY `s_ix_sales_records_related` (`related_id`,`related_type`),
  KEY `s_ix_sales_records_person_master` (`person_id`,`sales_record_master_id`),
  CONSTRAINT `sales_records_new_index_ibfk_1` FOREIGN KEY (`person_intake_id`) REFERENCES `person_intakes` (`id`),
  CONSTRAINT `sales_records_new_index_ibfk_2` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`),
  CONSTRAINT `sales_records_new_index_ibfk_3` FOREIGN KEY (`sales_record_master_id`) REFERENCES `sales_record_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `schema_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_info` (
  `version` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `scrapi_job_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scrapi_job_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `track_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isrc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `album_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `album_upc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `artists` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `genres` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `scrapi_job_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_scrapi_job_details_on_scrapi_job_id` (`scrapi_job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `scrapi_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scrapi_jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `song_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_matched` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_scrapi_jobs_on_song_id` (`song_id`),
  KEY `index_scrapi_jobs_on_job_id` (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `self_billing_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `self_billing_statuses` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` enum('accepted','declined') NOT NULL,
  `person_id` bigint(20) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `version` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_self_billing_statuses_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `serials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `serials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(128) NOT NULL DEFAULT '',
  `taken` tinyint(1) NOT NULL DEFAULT 0,
  `person_id` int(11) DEFAULT NULL,
  `claimed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `os` varchar(15) DEFAULT '',
  `program` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_serials_on_number` (`number`),
  UNIQUE KEY `uniq_person_prog_serial` (`person_id`,`program`),
  KEY `index_serials_on_taken_and_person_id` (`taken`,`person_id`),
  KEY `serial_os_type` (`os`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `settable_type` varchar(255) DEFAULT NULL,
  `settable_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `shipping_labels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipping_labels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_id` int(11) DEFAULT NULL,
  `price_policy_id` int(10) unsigned DEFAULT 0,
  `weight` float DEFAULT NULL,
  `service_type` varchar(255) DEFAULT NULL,
  `label` blob DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `tracking_number` varchar(255) DEFAULT NULL,
  `fedex_cost_cents` int(11) DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `printed` tinyint(1) DEFAULT 0,
  `shipping_service_type` varchar(30) DEFAULT NULL,
  `global_flag` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `video_id` (`video_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sip_stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sip_stores` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `store_id_old` smallint(5) unsigned DEFAULT NULL,
  `display_on_status_page` tinyint(3) unsigned NOT NULL DEFAULT 1,
  `display_group_id` smallint(5) unsigned DEFAULT NULL,
  `display_group_id_old` smallint(5) unsigned DEFAULT NULL,
  `royalty_sub_store_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sip_stores_store_id` (`store_id`),
  CONSTRAINT `FK_sip_stores_store_id` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sip_stores_display_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sip_stores_display_groups` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `sort_order` smallint(5) unsigned DEFAULT NULL,
  `group_on_status` tinyint(3) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sip_stores_order2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sip_stores_order2` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `sort_order` smallint(5) unsigned DEFAULT NULL,
  `store_id` smallint(5) unsigned DEFAULT NULL,
  `display_group` smallint(5) unsigned DEFAULT NULL,
  `display_on_status_page` tinyint(3) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `song_copyright_claims`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `song_copyright_claims` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `song_id` int(11) NOT NULL,
  `person_id` int(11) DEFAULT NULL,
  `claimant_has_songwriter_agreement` tinyint(1) DEFAULT 0,
  `claimant_has_pro_or_cmo` tinyint(1) DEFAULT 0,
  `claimant_has_tc_publishing` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `index_song_copyright_claims_on_song_id` (`song_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `song_distribution_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `song_distribution_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `song_id` int(11) NOT NULL,
  `option_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `option_value` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_song_distribution_options_on_song_id_and_option_name` (`song_id`,`option_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `song_library_uploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `song_library_uploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `artist_name` varchar(255) NOT NULL,
  `genre_id` int(11) NOT NULL,
  `bucket_name` varchar(255) DEFAULT NULL,
  `asset_key` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `song_library_asset_data` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_song_library_uploads_person_id` (`person_id`),
  KEY `fk_song_library_uploads_genre_id` (`genre_id`),
  CONSTRAINT `fk_song_library_uploads_genre_id` FOREIGN KEY (`genre_id`) REFERENCES `genres` (`id`),
  CONSTRAINT `fk_song_library_uploads_person_id` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `song_lifetime_earnings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `song_lifetime_earnings` (
  `song_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `song_earnings` decimal(10,0) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`song_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `song_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `song_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_type` varchar(255) DEFAULT NULL,
  `resource_contributor_role` tinyint(1) DEFAULT NULL,
  `indirect_contributor_role` tinyint(1) DEFAULT NULL,
  `user_defined` tinyint(1) DEFAULT NULL,
  `role_group` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `song_start_times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `song_start_times` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `song_id` bigint(20) DEFAULT NULL,
  `store_id` bigint(20) DEFAULT NULL,
  `start_time` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_song_start_times_on_song_id` (`song_id`),
  KEY `index_song_start_times_on_store_id` (`store_id`),
  KEY `index_song_start_times_on_song_id_and_store_id` (`song_id`,`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `songs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `songs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '',
  `album_id` int(6) DEFAULT 0,
  `created_on` date NOT NULL DEFAULT '0000-00-00',
  `songstatus` varchar(32) NOT NULL DEFAULT 'entered',
  `track_num` smallint(5) unsigned DEFAULT NULL,
  `tunecore_isrc` varchar(12) DEFAULT NULL,
  `optional_isrc` varchar(12) DEFAULT NULL,
  `s3_flac_asset_id` int(11) DEFAULT NULL,
  `s3_orig_asset_id` int(11) DEFAULT NULL,
  `last_errors` text DEFAULT NULL,
  `parental_advisory` tinyint(1) DEFAULT NULL,
  `price_policy_id` int(10) unsigned DEFAULT 0,
  `upload_error_log` varchar(255) DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `price_override` float DEFAULT NULL,
  `external_asset_id` int(11) DEFAULT NULL,
  `s3_asset_id` int(10) unsigned DEFAULT NULL,
  `composition_id` int(11) DEFAULT NULL,
  `clean_version` tinyint(1) DEFAULT NULL,
  `content_fingerprint` text DEFAULT NULL,
  `language_code_id` int(11) DEFAULT NULL,
  `translated_name` varchar(255) DEFAULT NULL,
  `song_version` varchar(255) DEFAULT NULL,
  `previously_released_at` date DEFAULT NULL,
  `made_popular_by` varchar(255) DEFAULT NULL,
  `cover_song` tinyint(1) DEFAULT NULL,
  `instrumental` tinyint(1) DEFAULT NULL,
  `duration_in_seconds` int(11) DEFAULT NULL,
  `lyric_language_code_id` int(11) DEFAULT NULL,
  `metadata_language_code_id` int(11) DEFAULT NULL,
  `publishing_composition_id` int(11) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `s3_streaming_asset_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `songs_chargeable_id_index` (`price_policy_id`),
  KEY `index_songs_on_purchase_id` (`purchase_id`),
  KEY `index_songs_on_tunecore_isrc` (`tunecore_isrc`),
  KEY `index_songs_on_optional_isrc` (`optional_isrc`),
  KEY `songs_album_id_index` (`album_id`,`track_num`),
  KEY `composition_id` (`composition_id`),
  KEY `by_content_fingerprint` (`content_fingerprint`(32)),
  KEY `index_songs_on_duration_in_seconds` (`duration_in_seconds`),
  KEY `index_songs_on_publishing_composition_id` (`publishing_composition_id`),
  CONSTRAINT `songs_ibfk_1` FOREIGN KEY (`composition_id`) REFERENCES `compositions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `songs_songwriters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `songs_songwriters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `song_id` int(11) NOT NULL,
  `songwriter_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_songs_songwriters_on_song_id` (`song_id`),
  KEY `index_songs_songwriters_on_songwriter_id` (`songwriter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `songwriters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `songwriters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_songwriters_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `soundout_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `soundout_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_type` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `number_of_reviews` int(11) DEFAULT NULL,
  `available_in_days` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `soundout_report_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `soundout_report_data` (
  `soundout_report_id` int(11) NOT NULL,
  `market_potential` int(11) DEFAULT NULL,
  `market_potential_genre` int(11) DEFAULT NULL,
  `track_rating` float DEFAULT NULL,
  `passion_rating` float DEFAULT NULL,
  `in_genre_class` varchar(255) DEFAULT NULL,
  `report_data` mediumtext DEFAULT NULL,
  `url` text DEFAULT NULL,
  PRIMARY KEY (`soundout_report_id`),
  KEY `report_id_market_potential` (`soundout_report_id`,`market_potential`),
  KEY `index_soundout_report_data_on_market_potential` (`market_potential`),
  CONSTRAINT `soundout_report_data_ibfk_1` FOREIGN KEY (`soundout_report_id`) REFERENCES `soundout_reports` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `soundout_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `soundout_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `soundout_product_id` int(11) DEFAULT NULL,
  `person_id` int(11) NOT NULL,
  `track_id` int(11) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `soundout_id` varchar(255) DEFAULT NULL,
  `report_requested_tmsp` datetime DEFAULT NULL,
  `retrieval_attempts` int(11) DEFAULT NULL,
  `report_received_tmsp` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `canceled_at` datetime DEFAULT NULL,
  `ingrooves_fontana_submitted_at` datetime DEFAULT NULL,
  `track_type` enum('Song','SongLibraryUpload') NOT NULL DEFAULT 'Song',
  PRIMARY KEY (`id`),
  KEY `index_soundout_reports_on_person_id_and_song_id` (`person_id`,`track_id`),
  KEY `index_soundout_reports_on_soundout_id` (`soundout_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `store_delivery_configs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store_delivery_configs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `party_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `party_full_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_size` int(11) DEFAULT NULL,
  `file_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hostname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyfile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `skip_batching` tinyint(1) DEFAULT NULL,
  `use_proprietary_id` tinyint(1) DEFAULT NULL,
  `track_price_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `album_price_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `ddex_version` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `skip_media` tinyint(1) DEFAULT NULL,
  `include_behalf` tinyint(1) DEFAULT NULL,
  `remote_dir_timestamped` tinyint(1) DEFAULT NULL,
  `remote_dir` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `display_track_id` tinyint(1) DEFAULT NULL,
  `display_track_language` tinyint(1) DEFAULT NULL,
  `use_resource_dir` tinyint(1) DEFAULT 1,
  `channels` tinyint(1) DEFAULT NULL,
  `include_assets` tinyint(1) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `transfer_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bit_depth` int(11) DEFAULT NULL,
  `normalize_bit_depth` tinyint(1) DEFAULT NULL,
  `delivery_queue` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deliver_booklet` tinyint(1) DEFAULT 0,
  `test_delivery` tinyint(1) DEFAULT 0,
  `batch_size` int(11) DEFAULT 100,
  `paused` tinyint(1) DEFAULT 0,
  `use_manifest` tinyint(1) DEFAULT 0,
  `distributable_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Distribution',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `store_group_stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store_group_stores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_group_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_store_group_stores_on_store_group_id_and_store_id` (`store_group_id`,`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `store_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_store_groups_on_key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `store_ingestion_errors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store_ingestion_errors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_ingestion_id` int(11) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_store_ingestion_id` (`store_ingestion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `store_ingestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store_ingestions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` varchar(255) DEFAULT NULL,
  `isrc` varchar(12) DEFAULT NULL,
  `upc` varchar(13) DEFAULT NULL,
  `song_id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `ingested_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `store_uid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_isrc` (`isrc`),
  KEY `index_ingested_at` (`ingested_at`),
  KEY `index_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `store_intakes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store_intakes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ruby_type` varchar(20) DEFAULT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `reporting_month_id` int(11) NOT NULL DEFAULT 0,
  `local_currency` char(3) DEFAULT NULL,
  `local_total_cents` int(11) NOT NULL DEFAULT 0,
  `exchange_symbol` varchar(6) DEFAULT NULL,
  `exchange_rate_fixed` bigint(19) DEFAULT NULL,
  `exchange_rate_scale` int(11) DEFAULT NULL,
  `usd_actual_cents` int(11) NOT NULL DEFAULT 0,
  `usd_total_cents` int(11) NOT NULL DEFAULT 0,
  `usd_payout_cents` int(11) NOT NULL DEFAULT 0,
  `comment` text DEFAULT NULL,
  `unique_id` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_store_intakes_on_unique_id` (`unique_id`),
  KEY `store_intakes_store_id_index` (`store_id`),
  KEY `store_intakes_reporting_month_id_index` (`reporting_month_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `store_status_schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store_status_schema_migrations` (
  `filename` varchar(255) NOT NULL,
  PRIMARY KEY (`filename`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `stored_bank_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stored_bank_accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL COMMENT 'References people table',
  `customer_vault_id` char(32) NOT NULL COMMENT 'References external storage id at Braintree',
  `bank_name` varchar(255) DEFAULT NULL COMMENT 'Bank Name',
  `last_four_routing` char(4) DEFAULT NULL COMMENT 'Last four digits of routing #',
  `last_four_account` char(4) DEFAULT NULL COMMENT 'Last four digits of account #',
  `sec_code` char(3) DEFAULT NULL COMMENT 'Sec code used for processing, all EFT''s should be CCD''',
  `account_type` enum('checking','savings') DEFAULT NULL COMMENT 'type of account (used by braintree)',
  `account_holder_type` enum('personal','business') DEFAULT NULL COMMENT 'type of account holder (used by braintree)',
  `name` varchar(120) DEFAULT NULL COMMENT 'Full name on account',
  `company` varchar(100) DEFAULT NULL COMMENT 'Company name on bank account if applicable',
  `address1` varchar(255) DEFAULT NULL COMMENT 'Bank Account Address line 1',
  `address2` varchar(255) DEFAULT NULL COMMENT 'Bank Account Address line 2',
  `city` varchar(100) DEFAULT NULL COMMENT 'Bank Account Address city',
  `state` char(2) DEFAULT NULL COMMENT 'Bank Account Address State - ANSI CODE',
  `country` char(2) DEFAULT NULL COMMENT 'Bank Account Address country - ANSI CODE',
  `zip` varchar(10) DEFAULT NULL COMMENT 'Bank Account Address zip',
  `phone` varchar(50) DEFAULT NULL COMMENT 'contact phone number',
  `status` enum('valid','processing_error','retrieval_error','destroyed','destruction_error') DEFAULT NULL COMMENT 'defines the state of the sba record. current means it is up to date',
  `deleted_at` datetime DEFAULT NULL COMMENT 'date customer deleted bank acount',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `bt_token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sba_person_id` (`person_id`),
  KEY `sba_customer_vault_id` (`customer_vault_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `stored_credit_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stored_credit_cards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL COMMENT 'References people table',
  `customer_vault_id` char(32) NOT NULL COMMENT 'References external storage id at Braintree',
  `cc_type` enum('Visa','Mastercard','Discover','American Express','Diner''s Club') DEFAULT NULL,
  `last_four` char(4) DEFAULT NULL COMMENT 'Last four digits of stored credit card',
  `expiration_month` tinyint(3) unsigned DEFAULT NULL COMMENT 'expiration month of the stored credit card',
  `expiration_year` smallint(5) unsigned DEFAULT NULL COMMENT 'expiration year of credit card',
  `first_name` varchar(100) DEFAULT NULL COMMENT 'First name on card',
  `last_name` varchar(100) DEFAULT NULL COMMENT 'Last name on card',
  `company` varchar(100) DEFAULT NULL COMMENT 'Company name on card if applicable',
  `address1` varchar(255) DEFAULT NULL COMMENT 'Billing Address line 1',
  `address2` varchar(255) DEFAULT NULL COMMENT 'Billing Address line 2',
  `city` varchar(100) DEFAULT NULL COMMENT 'Billing Address city',
  `state` char(5) DEFAULT NULL COMMENT 'Billing Address State - ANSI CODE',
  `country` char(2) DEFAULT NULL COMMENT 'Billing Address country - ANSI CODE',
  `zip` varchar(10) DEFAULT NULL COMMENT 'Billing Address zip',
  `status` enum('new','current','processing_error','retrieval_error','destruction_error') NOT NULL DEFAULT 'new' COMMENT 'defines the state of the scc record. current means it is up to date',
  `deleted_at` datetime DEFAULT NULL COMMENT 'date customer deleted credit card',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `bt_token` varchar(255) DEFAULT NULL,
  `cvv` tinyint(1) DEFAULT NULL,
  `payments_os_token` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `state_id` bigint(20) DEFAULT NULL,
  `state_city_id` bigint(20) DEFAULT NULL,
  `payin_provider_config_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `customer_vault_id` (`customer_vault_id`),
  KEY `index_stored_credit_cards_on_state_id` (`state_id`),
  KEY `index_stored_credit_cards_on_state_city_id` (`state_city_id`),
  KEY `index_stored_credit_cards_on_payin_provider_config_id` (`payin_provider_config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `stored_paypal_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stored_paypal_accounts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `country_website_id` char(3) NOT NULL COMMENT 'Foreign Key to country websties',
  `person_id` int(10) unsigned DEFAULT NULL COMMENT 'person ID',
  `archived_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  `payin_provider_config_id` bigint(20) DEFAULT NULL,
  `billing_agreement_id` char(19) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `stored_paypal_pt_person_id` (`person_id`),
  KEY `spa_transfers_country` (`country_website_id`),
  KEY `index_stored_paypal_accounts_on_payin_provider_config_id` (`payin_provider_config_id`),
  KEY `index_stored_paypal_accounts_on_email` (`email`),
  CONSTRAINT `FK_stored_paypal_pt_person_id` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stores` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL DEFAULT '',
  `abbrev` varchar(6) DEFAULT '',
  `short_name` varchar(10) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `needs_rights_assignment` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) DEFAULT 1,
  `base_price_policy_id` int(11) DEFAULT 3,
  `is_free` tinyint(1) DEFAULT 0,
  `description` text DEFAULT NULL,
  `launched_at` datetime DEFAULT NULL,
  `on_sale` tinyint(1) DEFAULT 0,
  `in_use_flag` tinyint(1) DEFAULT 1,
  `on_flash_sale` tinyint(1) DEFAULT 0,
  `delivery_service` varchar(255) DEFAULT NULL,
  `reviewable` tinyint(1) DEFAULT 0,
  `discovery_platform` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `abbrev` (`abbrev`),
  KEY `position` (`position`),
  KEY `index_stores_on_in_use_flag` (`in_use_flag`),
  KEY `index_stores_on_is_active` (`is_active`),
  KEY `index_stores_on_abbrev` (`abbrev`),
  KEY `index_stores_on_short_name` (`short_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `streaming_authorizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `streaming_authorizations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `streaming_type` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `redeemed_at` datetime DEFAULT NULL,
  `redeemed_by_id` int(11) DEFAULT NULL,
  `expires_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `studio_recover_assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studio_recover_assets` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_fields` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `s3_asset_id` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_studio_recover_assets_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `studio_watchlists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studio_watchlists` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `text_to_compare` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_studio_watchlists_on_text_to_compare` (`text_to_compare`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `subscription_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subscription_purchase_id` int(11) DEFAULT NULL,
  `person_subscription_status_id` int(11) DEFAULT NULL,
  `event_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subscription_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_subscription_events_on_subscription_type` (`subscription_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `subscription_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `term_length` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `subscription_purchases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription_purchases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `subscription_product_id` int(11) DEFAULT NULL,
  `effective_date` datetime DEFAULT NULL,
  `termination_date` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `payment_channel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_subscription_purchases_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `survey_responses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `survey_responses` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) NOT NULL,
  `survey_id` bigint(20) NOT NULL,
  `json` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_survey_responses_on_person_id` (`person_id`),
  KEY `index_survey_responses_on_survey_id` (`survey_id`),
  KEY `index_survey_responses_on_person_id_and_survey_id` (`person_id`,`survey_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `surveys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveys` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `multiple_response` tinyint(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mandatory_json_keys` text DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_surveys_on_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sync_favorite_songs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sync_favorite_songs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `song_id` int(11) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_sync_favorite_songs_on_song_id` (`song_id`),
  KEY `index_sync_favorite_songs_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sync_license_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sync_license_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media` varchar(255) DEFAULT NULL,
  `territory` varchar(255) DEFAULT NULL,
  `term` varchar(255) DEFAULT NULL,
  `offer` varchar(255) DEFAULT NULL,
  `comments` text DEFAULT NULL,
  `sync_license_request_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_sync_license_options_on_sync_license_request_id` (`sync_license_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sync_license_productions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sync_license_productions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `studio` varchar(255) DEFAULT NULL,
  `budget` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_sync_license_productions_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sync_license_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sync_license_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(255) DEFAULT NULL,
  `scene_description` text DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `type_of_use` varchar(255) DEFAULT NULL,
  `master_use` tinyint(1) DEFAULT 1,
  `song_id` int(11) DEFAULT NULL,
  `sync_license_production_id` int(11) DEFAULT NULL,
  `request_document_updated_at` datetime DEFAULT NULL,
  `request_document_file_name` varchar(255) DEFAULT NULL,
  `request_document_content_type` varchar(255) DEFAULT NULL,
  `request_document_file_size` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `ticketbooth_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_sync_license_requests_on_sync_license_production_id` (`sync_license_production_id`),
  KEY `index_sync_license_requests_on_person_id` (`person_id`),
  KEY `index_sync_license_requests_on_song_id` (`song_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sync_membership_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sync_membership_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `request_code` varchar(255) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_sync_membership_requests_on_status` (`status`),
  KEY `index_sync_membership_requests_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `system_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `targeted_advertisements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `targeted_advertisements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `targeted_offer_id` int(10) unsigned NOT NULL COMMENT 'id of the offer this person should see',
  `advertisement_id` int(10) unsigned NOT NULL COMMENT 'id of advertisment to display',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  PRIMARY KEY (`id`),
  KEY `targeted_offer_id` (`targeted_offer_id`),
  KEY `advertisement_id` (`advertisement_id`),
  CONSTRAINT `fk_targeted_ad_ad_id` FOREIGN KEY (`advertisement_id`) REFERENCES `advertisements` (`id`),
  CONSTRAINT `fk_targeted_ad_offer_id` FOREIGN KEY (`targeted_offer_id`) REFERENCES `targeted_offers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `targeted_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `targeted_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `notification_icon_id` int(11) DEFAULT NULL,
  `text` mediumtext DEFAULT NULL,
  `title` mediumtext DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `link_text` varchar(255) DEFAULT NULL,
  `global` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_targeted_notifications_on_person_id` (`person_id`),
  KEY `index_targeted_notifications_on_notification_icon_id` (`notification_icon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `targeted_offer_exclusions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `targeted_offer_exclusions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `targeted_offer_id` int(10) unsigned NOT NULL COMMENT 'Foreign Key to Targeted Offer Table',
  `exclude_targeted_offer_id` int(10) unsigned NOT NULL COMMENT 'Foreign Key to Targeted Offer Table',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  PRIMARY KEY (`id`),
  KEY `targeted_offer_id` (`targeted_offer_id`),
  KEY `exclude_targeted_offer_id` (`exclude_targeted_offer_id`),
  CONSTRAINT `FK_targetoffer_exclusion_exclude_targetoffers` FOREIGN KEY (`exclude_targeted_offer_id`) REFERENCES `targeted_offers` (`id`),
  CONSTRAINT `FK_targetoffer_exclusion_targetoffers` FOREIGN KEY (`targeted_offer_id`) REFERENCES `targeted_offers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `targeted_offers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `targeted_offers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `country_website_id` char(3) NOT NULL DEFAULT '1' COMMENT 'Country that this targeted offer is offered to',
  `name` varchar(75) NOT NULL COMMENT 'name of targeted offer',
  `created_by_id` int(10) unsigned NOT NULL COMMENT 'id of administrator who created the offer',
  `status` enum('New','Active','Inactive') DEFAULT 'New',
  `offer_type` enum('new','existing','country_targeted') DEFAULT NULL,
  `single_use` tinyint(1) DEFAULT 0,
  `start_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'start date for the targeted offer',
  `expiration_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'expiration date of targeted offer',
  `date_constraint` enum('expiration','join plus','permanent') NOT NULL DEFAULT 'expiration' COMMENT 'when does this offer expire for members in the targeted population',
  `join_plus_duration` smallint(6) DEFAULT NULL COMMENT 'number of intervals to use when calculating the expiration of an offer for targeted customers (e.g. 10 days)',
  `join_token` varchar(50) DEFAULT NULL COMMENT 'url token to match a new customer to a targeted offer',
  `include_sidebar_cms` tinyint(1) DEFAULT 0,
  `country` varchar(20) DEFAULT NULL COMMENT 'population criteria field for selecting an existing population',
  `zip` varchar(5) DEFAULT NULL COMMENT 'population criteria field for selecting an existing population',
  `created_on_start` date DEFAULT NULL COMMENT 'population criteria field for selecting an existing population',
  `created_on_end` date DEFAULT NULL COMMENT 'population criteria field for selecting an existing population',
  `referral` varchar(20) DEFAULT NULL COMMENT 'population criteria field for selecting an existing population',
  `partner_id` smallint(6) DEFAULT NULL COMMENT 'population criteria field for selecting an existing population',
  `has_never_made_a_purchase` smallint(1) DEFAULT NULL COMMENT 'population criteria field for selecting an existing population',
  `purchase_criteria_type` enum('never_made','has_made','date_range','n/a') DEFAULT NULL,
  `last_purchase_made_start` date DEFAULT NULL COMMENT 'population criteria field for selecting an existing population',
  `last_purchase_made_end` date DEFAULT NULL COMMENT 'population criteria field for selecting an existing population',
  `population_criteria_count` int(10) unsigned DEFAULT NULL COMMENT 'total count of customers found using population_criteria',
  `targeted_population_count` int(10) unsigned DEFAULT NULL COMMENT 'cached total count of customers targeted with this offer',
  `population_cap` int(10) unsigned DEFAULT NULL COMMENT 'total population for a targeted_offer with an offer_type of `new`',
  `product_show_type` enum('price_override','show_only_selected') DEFAULT 'price_override' COMMENT 'how does this targeted offer display products',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  `usage_limit` int(11) DEFAULT 0,
  `job_status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `join_token` (`join_token`),
  KEY `date_constraint` (`date_constraint`),
  KEY `expiration_date` (`expiration_date`),
  KEY `start_date` (`start_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `targeted_people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `targeted_people` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `targeted_offer_id` int(10) unsigned NOT NULL COMMENT 'id of the offer this person should see',
  `person_id` int(10) unsigned NOT NULL COMMENT 'id of the targeted customer',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  `usage_count` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `targeted_offer_id` (`targeted_offer_id`),
  KEY `person_id` (`person_id`),
  CONSTRAINT `fk_targeted_people_offer_id` FOREIGN KEY (`targeted_offer_id`) REFERENCES `targeted_offers` (`id`),
  CONSTRAINT `fk_targeted_people_people_id` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `targeted_product_stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `targeted_product_stores` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `targeted_product_id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `targeted_product_store` (`targeted_product_id`,`store_id`),
  KEY `index_targeted_product_stores_on_store_id` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `targeted_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `targeted_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `targeted_offer_id` int(10) unsigned NOT NULL COMMENT 'id of the offer this person should see',
  `product_id` int(10) unsigned NOT NULL COMMENT 'id of the product to adjust the price of',
  `display_name` varchar(50) DEFAULT NULL COMMENT 'overriding display_name for targeted product',
  `price_adjustment_type` enum('percentage_off','override','dollars_off','none') NOT NULL DEFAULT 'percentage_off' COMMENT 'determines which action should be used when adjusting a products price',
  `price_adjustment` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT 'decimal value to use in adjusting the price of the selected product',
  `currency` char(3) NOT NULL COMMENT 'Currency for targeted product',
  `price_adjustment_description` varchar(75) DEFAULT NULL COMMENT 'admin specified description for the price adjustment...shows on price',
  `flag_text` varchar(15) DEFAULT NULL COMMENT 'override text to display in product lists',
  `show_expiration_date` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'specifies whether or not the expiration date of the offer is shown along with the product',
  `sort_order` smallint(6) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  `renewals_past_due` tinyint(1) DEFAULT 1,
  `renewals_due` tinyint(1) DEFAULT 1,
  `renewals_upcomming` tinyint(1) DEFAULT 1,
  `show_discount_price` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `targeted_offer_id` (`targeted_offer_id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `fk_targeted_offer_offer_id` FOREIGN KEY (`targeted_offer_id`) REFERENCES `targeted_offers` (`id`),
  CONSTRAINT `fk_targeted_product_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `tax_form_revenue_streams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_form_revenue_streams` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tax_form_id` bigint(20) NOT NULL,
  `revenue_stream_id` bigint(20) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `user_mapped_at` datetime DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL COMMENT 'Persons revenue stream in the context of a taxform mapping',
  PRIMARY KEY (`id`),
  UNIQUE KEY `pid_rsid_tfrs` (`person_id`,`revenue_stream_id`) COMMENT 'Person should only have one revenue stream mapped to one taxform at a time',
  KEY `index_tax_form_revenue_streams_on_tax_form_id` (`tax_form_id`),
  KEY `index_tax_form_revenue_streams_on_revenue_stream_id` (`revenue_stream_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `tax_form_revenue_types`;
/*!50001 DROP VIEW IF EXISTS `tax_form_revenue_types`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `tax_form_revenue_types` (
  `tax_form_id` tinyint NOT NULL,
  `revenue_stream_code` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `tax_form_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_form_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `kind` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `tax_forms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submitted_at` datetime NOT NULL,
  `expires_at` datetime NOT NULL,
  `person_id` int(11) NOT NULL,
  `payload` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `tax_form_type_id` bigint(20) NOT NULL,
  `payout_provider_config_id` bigint(20) DEFAULT NULL COMMENT 'references payout_provider_configs table.',
  `tax_entity_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_tax_forms_on_person_id` (`person_id`),
  KEY `index_tax_forms_on_tax_form_type_id` (`tax_form_type_id`),
  KEY `index_tax_forms_on_payout_provider_config_id` (`payout_provider_config_id`),
  CONSTRAINT `fk_rails_08086e1c21` FOREIGN KEY (`tax_form_type_id`) REFERENCES `tax_form_types` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `tax_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `composer_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address_1` varchar(45) DEFAULT NULL,
  `address_2` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `country` varchar(2) DEFAULT NULL,
  `nosocial` tinyint(1) DEFAULT 0,
  `agreed_to_w9_at` datetime DEFAULT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `encrypted_tax_id` varchar(40) DEFAULT NULL,
  `is_entity` tinyint(1) DEFAULT 0,
  `entity_name` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `classification` varchar(30) DEFAULT NULL,
  `llc_classification` varchar(30) DEFAULT NULL,
  `country_of_corp` varchar(80) DEFAULT NULL,
  `owner_type` varchar(30) DEFAULT NULL,
  `mailing_address_1` varchar(45) DEFAULT NULL,
  `mailing_address_2` varchar(45) DEFAULT NULL,
  `mailing_city` varchar(45) DEFAULT NULL,
  `mailing_state` varchar(50) DEFAULT NULL,
  `mailing_zip` varchar(10) DEFAULT NULL,
  `mailing_country` varchar(2) DEFAULT NULL,
  `agreed_to_w8ben_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `exempt_payee` tinyint(1) DEFAULT 0,
  `claim_of_treaty_benefits` text DEFAULT NULL,
  `publishing_composer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `tax_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_visible` tinyint(1) DEFAULT 0,
  `person_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `tax_form_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax_tokens_cohort_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `tax_tokens_cohorts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_tokens_cohorts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `taxable_earnings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taxable_earnings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `revenue_source` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `credit` decimal(23,14) NOT NULL,
  `currency` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'USD',
  `person_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_taxable_earnings_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `tc_reporter_audits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tc_reporter_audits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `person_id` int(11) NOT NULL,
  `api_params` text COLLATE utf8_unicode_ci NOT NULL,
  `api_response` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `temp_encumbrance_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp_encumbrance_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `encumbrance_summary_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `transaction_amount` decimal(12,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_temp_encumbrance_details_on_encumbrance_summary_id` (`encumbrance_summary_id`),
  KEY `index_temp_encumbrance_details_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores encumbrance deductions for the posting. Data will be deleted once the processing is done';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `temp_person_royalties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp_person_royalties` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `user_currency` varchar(3) NOT NULL,
  `corporate_entity_id` int(11) NOT NULL COMMENT 'Used for finance team to process amounts separately',
  `sales_record_master_ids` text DEFAULT NULL,
  `total_amount_in_usd` decimal(23,6) DEFAULT NULL COMMENT 'Amount earned from non-youtube stores',
  `encumbrance_amount_in_usd` decimal(23,6) DEFAULT NULL COMMENT 'Amount paid back for encumbrances. Always a negative value',
  `you_tube_amount_in_usd` decimal(23,6) DEFAULT NULL COMMENT 'Amount earned from youtube stores as part of the postings',
  `encumbrance_percentage` decimal(23,14) DEFAULT NULL COMMENT '% of amount earned that was used for repaying encumbrances',
  `fx_rate` decimal(23,14) DEFAULT NULL,
  `coverage_rate` decimal(23,14) DEFAULT NULL,
  `total_amount_in_user_currency` decimal(23,6) DEFAULT NULL,
  `encumbrance_amount_in_user_currency` decimal(23,6) DEFAULT NULL,
  `you_tube_amount_in_user_currency` decimal(23,6) DEFAULT NULL,
  `person_intake_id` int(11) DEFAULT NULL COMMENT 'Used for idempotency',
  `you_tube_royalty_intake_id` int(11) DEFAULT NULL COMMENT 'Used for idempotency',
  `summarized` tinyint(1) DEFAULT 0 COMMENT 'Used for idempotency',
  `sales_records_copied` tinyint(1) DEFAULT 0 COMMENT 'Used for idempotency',
  `youtube_records_copied` tinyint(1) DEFAULT 0 COMMENT 'Used for idempotency',
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `split_computed` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `index_temp_person_royalties_on_person_id` (`person_id`),
  KEY `index_temp_person_royalties_on_user_currency` (`user_currency`),
  KEY `index_temp_person_royalties_on_corporate_entity_id` (`corporate_entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores royalty information in a form suitable for faster processing. Data will be deleted once the processing is done';
/*!40101 SET character_set_client = @saved_cs_client */;
CREATE TABLE `temp_royalty_split_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) NOT NULL,
  `royalty_split_id` bigint(20) NOT NULL,
  `song_id` bigint(20) NOT NULL,
  `owner_id` int(10) unsigned NOT NULL COMMENT 'Main Account Holder person_id for split, the only admin',
  `corporate_entity_id` int(11) NOT NULL,
  `transaction_amount_in_usd` decimal(23,14) DEFAULT NULL,
  `currency` varchar(3) NOT NULL,
  `royalty_split_config` text DEFAULT NULL,
  `processed` tinyint(1) DEFAULT 0,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `royalty_split_title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_temp_royalty_split_details_on_person_id` (`person_id`),
  KEY `index_temp_royalty_split_details_on_royalty_split_id` (`royalty_split_id`),
  KEY `index_temp_royalty_split_details_on_song_id` (`song_id`),
  KEY `index_temp_royalty_split_details_on_owner_id` (`owner_id`),
  KEY `index_temp_royalty_split_details_on_corporate_entity_id` (`corporate_entity_id`),
  KEY `index_temp_royalty_split_details_on_currency` (`currency`),
  CONSTRAINT `fk_rails_c09e159224` FOREIGN KEY (`owner_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `terminated_composers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terminated_composers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `composer_id` int(11) DEFAULT NULL,
  `termination_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alt_provider_acct_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `effective_date` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `publishing_composer_id` int(11) DEFAULT NULL,
  `publishing_termination_reason_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_terminated_composers_on_composer_id` (`composer_id`),
  KEY `index_terminated_composers_on_publishing_composer_id` (`publishing_composer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `tier_achievements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tier_achievements` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tier_id` bigint(20) DEFAULT NULL,
  `achievement_id` bigint(20) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_tier_achievements_on_tier_id` (`tier_id`),
  KEY `index_tier_achievements_on_achievement_id` (`achievement_id`),
  CONSTRAINT `fk_rails_3236ac9b3f` FOREIGN KEY (`tier_id`) REFERENCES `tiers` (`id`),
  CONSTRAINT `fk_rails_330b8ecd6d` FOREIGN KEY (`achievement_id`) REFERENCES `achievements` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `tier_certifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tier_certifications` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tier_id` bigint(20) DEFAULT NULL,
  `certification_id` bigint(20) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_tier_certifications_on_tier_id` (`tier_id`),
  KEY `index_tier_certifications_on_certification_id` (`certification_id`),
  CONSTRAINT `fk_rails_6e7cdfc66a` FOREIGN KEY (`certification_id`) REFERENCES `certifications` (`id`),
  CONSTRAINT `fk_rails_fedff13817` FOREIGN KEY (`tier_id`) REFERENCES `tiers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `tier_event_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tier_event_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `event` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `person_id` int(11) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `tier_people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tier_people` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tier_id` bigint(20) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_tier_people_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `tier_rewards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tier_rewards` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tier_id` bigint(20) NOT NULL,
  `reward_id` bigint(20) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_tier_rewards_on_tier_id` (`tier_id`),
  KEY `index_tier_rewards_on_reward_id` (`reward_id`),
  CONSTRAINT `fk_rails_1b246d50a5` FOREIGN KEY (`reward_id`) REFERENCES `rewards` (`id`),
  CONSTRAINT `fk_rails_de8cef7ece` FOREIGN KEY (`tier_id`) REFERENCES `tiers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `tier_thresholds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tier_thresholds` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tier_id` bigint(20) DEFAULT NULL,
  `lte_min_amount` int(11) DEFAULT NULL,
  `min_release` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `tiers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `badge_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hierarchy` decimal(5,2) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_sub_tier` tinyint(1) NOT NULL DEFAULT 0,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_tiers_on_slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `total_purchases_by_date`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `total_purchases_by_date` (
  `_date_` date DEFAULT NULL,
  `_purchases_` bigint(21) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `tour_dates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tour_dates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `person_id` int(10) unsigned NOT NULL COMMENT 'Foreign key to people table.',
  `artist_id` int(10) unsigned NOT NULL COMMENT 'Foreign key to artists table.',
  `country_id` smallint(5) unsigned NOT NULL COMMENT 'Foreign key to countries table',
  `us_state_id` tinyint(3) unsigned NOT NULL COMMENT 'Foreign key to us_states table.',
  `city` varchar(50) DEFAULT NULL COMMENT 'City name.',
  `venue` varchar(50) DEFAULT NULL COMMENT 'Venue Name.',
  `information` text DEFAULT NULL COMMENT 'tourdate information',
  `created_at` datetime DEFAULT NULL COMMENT 'Date record created.',
  `updated_at` datetime DEFAULT NULL COMMENT 'Date record last updated.',
  `event_time_at` time DEFAULT NULL,
  `event_date_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `artist_id` (`artist_id`),
  KEY `country_id` (`country_id`),
  KEY `us_state_id` (`us_state_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `track_monetization_blockers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `track_monetization_blockers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `song_id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_track_monetization_blockers_on_song_id_and_store_id` (`song_id`,`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `track_monetizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `track_monetizations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `song_id` int(11) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'new',
  `job_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'full_delivery',
  `eligibility_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'pending',
  `takedown_at` datetime DEFAULT NULL,
  `provider_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `retry_count` int(11) DEFAULT 0,
  `tc_distributor_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_track_monetizations_on_song_id_and_store_id` (`song_id`,`store_id`),
  KEY `index_track_monetizations_on_store_id` (`store_id`),
  KEY `index_track_monetizations_on_person_id` (`person_id`),
  KEY `index_track_monetizations_on_song_id` (`song_id`),
  KEY `index_track_monetizations_on_state` (`state`),
  KEY `index_track_monetizations_on_eligibility_status` (`eligibility_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `transaction_error_adjustments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_error_adjustments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) NOT NULL,
  `adjusted_debit` decimal(23,14) NOT NULL DEFAULT 0.00000000000000,
  `adjusted_credit` decimal(23,14) NOT NULL DEFAULT 0.00000000000000,
  `note` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_transaction_error_adjustments_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `transfer_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transfer_metadata` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `trackable_type` varchar(255) NOT NULL,
  `trackable_id` bigint(20) NOT NULL,
  `auto_approved` tinyint(1) NOT NULL DEFAULT 0,
  `auto_approved_at` datetime DEFAULT NULL,
  `login_event_id` bigint(20) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_transfer_metadata_on_trackable_type_and_trackable_id` (`trackable_type`,`trackable_id`),
  KEY `index_transfer_metadata_on_login_event_id` (`login_event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `transitions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state_machine_id` int(11) NOT NULL DEFAULT 0,
  `state_machine_type` varchar(255) NOT NULL DEFAULT '',
  `actor` varchar(255) NOT NULL DEFAULT '',
  `old_state` varchar(255) NOT NULL DEFAULT '',
  `new_state` varchar(255) NOT NULL DEFAULT '',
  `message` text DEFAULT NULL,
  `backtrace` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_transitions_on_state_machine_id` (`state_machine_id`),
  KEY `state_machine_type_idx` (`state_machine_type`),
  KEY `created_at_idx` (`created_at`),
  KEY `state_machine_type_created_at_idx` (`state_machine_type`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `trend_city_totals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trend_city_totals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trend_report_id` int(11) NOT NULL,
  `city` varchar(40) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `total_units_sold` int(11) NOT NULL DEFAULT 0,
  `total_usd_cents` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `trend_city_totals` (`trend_report_id`,`city`,`state`,`total_units_sold`,`total_usd_cents`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `trend_report_sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trend_report_sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trend_report_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `target_type` varchar(10) NOT NULL,
  `units_sold` int(11) NOT NULL,
  `usd_cents` int(11) NOT NULL DEFAULT 0,
  `is_promo` tinyint(1) NOT NULL DEFAULT 0,
  `us_zip_code_id` int(11) DEFAULT NULL,
  `apple_customer_id` int(11) DEFAULT NULL,
  `apple_identifier` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `trend_sales_identifiers` (`trend_report_id`,`target_id`,`target_type`,`us_zip_code_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `trend_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trend_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sold_on` date NOT NULL,
  `imported_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_trend_reports_on_date` (`sold_on`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `tunecore_tracking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tunecore_tracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model_id` int(11) NOT NULL,
  `service` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `event_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `twitch_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitch_users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `twitch_id` varchar(255) DEFAULT NULL,
  `twitch_email` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `person_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_twitch_users_on_person_id` (`person_id`),
  KEY `index_twitch_users_on_twitch_email` (`twitch_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `two_factor_auth_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `two_factor_auth_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `page` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `successful` tinyint(1) DEFAULT NULL,
  `two_factor_auth_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_two_factor_auth_events_on_type` (`type`),
  KEY `index_two_factor_auth_events_on_page` (`page`),
  KEY `index_two_factor_auth_events_on_action` (`action`),
  KEY `index_two_factor_auth_events_on_two_factor_auth_id` (`two_factor_auth_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `two_factor_auth_prompts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `two_factor_auth_prompts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `dismissed_count` int(11) DEFAULT 0,
  `prompt_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_two_factor_auth_prompts_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `two_factor_auths`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `two_factor_auths` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `authy_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notification_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated_at` datetime DEFAULT NULL,
  `disabled_at` datetime DEFAULT NULL,
  `last_verified_at` datetime DEFAULT NULL,
  `last_failed_at` datetime DEFAULT NULL,
  `previous_phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `previous_notification_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `previous_country_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `e164` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_two_factor_auths_on_person_id` (`person_id`),
  KEY `index_two_factor_auths_on_authy_id` (`authy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `typeface_effects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typeface_effects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `use_for_suggested_covers` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `typeface_genres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typeface_genres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typeface_id` int(11) DEFAULT NULL,
  `genre_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `typefaces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typefaces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `upc_seeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `upc_seeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prefix` int(11) DEFAULT NULL,
  `seed` int(11) DEFAULT NULL,
  `block_full` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `upcs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `upcs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(255) DEFAULT NULL,
  `upcable_id` int(11) DEFAULT NULL,
  `upcable_type` varchar(255) DEFAULT NULL,
  `tunecore_upc` tinyint(1) DEFAULT 0,
  `inactive` tinyint(1) DEFAULT 0,
  `upc_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_upcs_on_number` (`number`),
  KEY `unique_upc_number` (`number`),
  KEY `upcable_id` (`upcable_id`,`upcable_type`,`tunecore_upc`,`inactive`,`number`),
  KEY `index_upcs_on_upc_type` (`upc_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `upcs_optimized`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `upcs_optimized` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` char(13) DEFAULT NULL,
  `upcable_id` int(11) DEFAULT NULL,
  `upcable_type` char(6) DEFAULT NULL,
  `tunecore_upc` tinyint(1) DEFAULT 0,
  `inactive` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_upcs_on_number` (`number`),
  KEY `unique_upc_number` (`number`),
  KEY `upcable_id` (`upcable_id`,`upcable_type`,`tunecore_upc`,`inactive`,`number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `upload_servers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `upload_servers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `failed_attempts` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `uploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `song_id` int(11) DEFAULT NULL,
  `original_filename` varchar(255) DEFAULT NULL,
  `uploaded_filename` varchar(255) DEFAULT NULL,
  `converted_filename` varchar(255) DEFAULT NULL,
  `filetype` varchar(32) DEFAULT NULL,
  `bitrate` int(4) DEFAULT NULL,
  `is_probably_lossless` tinyint(1) DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uploads_song_id_index` (`song_id`),
  KEY `index_uploads_on_uploaded_filename` (`uploaded_filename`),
  KEY `index_uploads_on_original_filename` (`original_filename`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `urls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `urls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) DEFAULT NULL,
  `album_id` int(11) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `urls_album_id_idx` (`album_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `us_states`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `us_states` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `abbreviation` char(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `us_zip_code_prefixes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `us_zip_code_prefixes` (
  `prefix` char(3) NOT NULL,
  `city` varchar(40) NOT NULL,
  `state` char(2) NOT NULL,
  KEY `index_us_zip_code_prefixes_on_prefix_and_city_and_state` (`prefix`,`city`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `us_zip_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `us_zip_codes` (
  `code` char(5) NOT NULL,
  `city` char(40) NOT NULL,
  `state` char(2) NOT NULL,
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `index_us_zip_codes_on_code_and_city_and_state` (`code`,`city`,`state`),
  KEY `us_zip_codes_city_idx` (`city`),
  KEY `us_zip_codes_state_idx` (`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_store_royalty_rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_store_royalty_rates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `royalty_sub_store_id` bigint(20) NOT NULL,
  `person_id` bigint(20) NOT NULL,
  `royalty_rate` decimal(14,2) NOT NULL DEFAULT 0.80 COMMENT 'value between 0 and 1',
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_user_store_royalty_rates_on_royalty_sub_store_id` (`royalty_sub_store_id`),
  KEY `index_user_store_royalty_rates_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Royalty rate config for VIP users at sub store level';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `variable_prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `variable_prices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) DEFAULT NULL,
  `price_code` varchar(255) DEFAULT NULL,
  `price_code_display` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `user_editable` tinyint(1) DEFAULT 0,
  `price` decimal(6,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `store_id` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `variable_prices_salepointable_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `variable_prices_salepointable_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `salepointable_type` varchar(255) DEFAULT NULL,
  `variable_price_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_variable_price_id_by_type` (`variable_price_id`,`salepointable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `variable_prices_stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `variable_prices_stores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `variable_price_id` int(11) NOT NULL,
  `position` int(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_variable_prices_stores_on_store_id_and_variable_price_id` (`store_id`,`variable_price_id`),
  KEY `store_id` (`store_id`,`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `vat_information_audits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vat_information_audits` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) NOT NULL,
  `company_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat_registration_number` varchar(255) DEFAULT NULL,
  `vat_registration_status` varchar(255) DEFAULT NULL,
  `trader_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `begin_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_vat_information_audits_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `vat_informations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vat_informations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `company_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat_registration_number` varchar(255) DEFAULT NULL,
  `vat_registration_status` varchar(255) DEFAULT NULL,
  `trader_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `vat_tax_adjustments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vat_tax_adjustments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` bigint(20) NOT NULL,
  `related_id` int(11) NOT NULL,
  `related_type` varchar(255) NOT NULL,
  `amount` decimal(23,14) NOT NULL,
  `tax_rate` decimal(5,2) NOT NULL,
  `vat_registration_number` varchar(255) DEFAULT NULL,
  `tax_type` varchar(255) DEFAULT NULL,
  `trader_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place_of_supply` varchar(255) DEFAULT NULL,
  `error_message` text DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `customer_type` varchar(255) DEFAULT NULL,
  `vat_amount_in_eur` decimal(23,14) DEFAULT NULL,
  `foreign_exchange_rate_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_vat_tax_adjustments_on_person_id` (`person_id`),
  KEY `index_vat_tax_adjustments_on_related_id_and_related_type` (`related_id`,`related_type`),
  KEY `index_vat_tax_adjustments_on_foreign_exchange_rate_id` (`foreign_exchange_rate_id`),
  CONSTRAINT `fk_rails_2cfd74ff77` FOREIGN KEY (`foreign_exchange_rate_id`) REFERENCES `foreign_exchange_rates` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_type` varchar(255) NOT NULL,
  `item_id` int(11) NOT NULL,
  `event` varchar(255) NOT NULL,
  `whodunnit` varchar(255) DEFAULT NULL,
  `object` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_versions_on_item_type_and_item_id` (`item_type`,`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `video_purchase_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_purchase_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `purchaseable_id` int(11) NOT NULL,
  `purchaseable_type` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_video_purchase_items_on_purchase_id` (`purchase_id`),
  KEY `index_video_purchase_items_on_purchaseable_id` (`purchaseable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `label_id` int(11) DEFAULT NULL,
  `album_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  `closed_captioned` tinyint(1) DEFAULT NULL,
  `director` varchar(255) DEFAULT NULL,
  `dp` varchar(255) DEFAULT NULL,
  `editor` varchar(255) DEFAULT NULL,
  `producer` varchar(255) DEFAULT NULL,
  `executive_producer` varchar(255) DEFAULT NULL,
  `production_company` varchar(255) DEFAULT NULL,
  `sale_date` date DEFAULT NULL,
  `tunecore_isrc` varchar(255) DEFAULT NULL,
  `optional_isrc` varchar(255) DEFAULT NULL,
  `length` int(11) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `finalized_at` datetime DEFAULT NULL,
  `orig_release_year` date DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `accepted_format` tinyint(1) DEFAULT 0,
  `tracking_number` varchar(255) DEFAULT NULL,
  `locked_by_id` int(11) DEFAULT NULL,
  `locked_by_type` varchar(30) DEFAULT NULL,
  `price_policy_id` int(10) unsigned DEFAULT 0,
  `parental_advisory` tinyint(1) DEFAULT 0,
  `allow_different_format` tinyint(1) DEFAULT 0,
  `purchase_id` int(11) DEFAULT NULL,
  `approval_date` date DEFAULT NULL,
  `metadata_picked_up` tinyint(1) DEFAULT 0,
  `created_on` date DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `c_copyright_date` date DEFAULT NULL,
  `primary_genre_id` int(11) DEFAULT NULL,
  `secondary_genre_id` int(11) DEFAULT NULL,
  `copyright` varchar(255) DEFAULT NULL,
  `definition` varchar(2) DEFAULT NULL,
  `video_type` enum('MusicVideo','FeatureFilm') DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_tc_isrc` (`tunecore_isrc`),
  UNIQUE KEY `unique_optional_isrc` (`optional_isrc`),
  KEY `index_videos_on_person_id` (`person_id`),
  KEY `index_videos_on_label_id` (`label_id`),
  KEY `index_videos_on_album_id` (`album_id`),
  KEY `index_videos_on_purchase_id` (`purchase_id`),
  KEY `index_videos_on_primary_genre_id` (`primary_genre_id`),
  KEY `index_videos_on_secondary_genre_id` (`secondary_genre_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `why_tunecore_configs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `why_tunecore_configs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `json` text DEFAULT NULL,
  `currently_published` tinyint(1) DEFAULT 0,
  `draft` tinyint(1) DEFAULT 0,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `widgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `config_data` blob DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deactivated_at` timestamp NULL DEFAULT NULL,
  `created_as_free` enum('Y','N') DEFAULT 'Y',
  `free_song_enable` enum('Y','N') DEFAULT 'Y',
  `free_song_id` int(10) unsigned DEFAULT NULL,
  `free_song_text` varchar(255) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `display_on_page` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `withholding_exemption_reasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `withholding_exemption_reasons` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_withholding_exemption_reasons_on_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `withholding_exemptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `withholding_exemptions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `started_at` date NOT NULL,
  `ended_at` date DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `withholding_exemption_reason_id` bigint(20) DEFAULT NULL,
  `revoked_at` datetime DEFAULT NULL COMMENT 'Acts as a flag for the record. When populated, the associated person will no longer be exempt from payout tax withholding.',
  PRIMARY KEY (`id`),
  KEY `index_withholding_exemptions_on_person_id` (`person_id`),
  KEY `withholding_exp_on_withholding_exp_reason` (`withholding_exemption_reason_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `you_tube_ineligible_keywords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `you_tube_ineligible_keywords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `you_tube_royalty_intakes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `you_tube_royalty_intakes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL,
  `amount` decimal(15,6) NOT NULL DEFAULT 0.000000,
  `currency` char(3) NOT NULL DEFAULT 'USD' COMMENT 'Currency',
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  CONSTRAINT `you_tube_royalty_intakes_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `you_tube_royalty_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `you_tube_royalty_records` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL,
  `sip_you_tube_royalty_record_id` bigint(20) DEFAULT NULL,
  `song_id` int(11) NOT NULL,
  `total_views` int(10) unsigned NOT NULL DEFAULT 0,
  `you_tube_policy_type` enum('monetize','track','block','takedown') NOT NULL DEFAULT 'track',
  `gross_revenue` decimal(15,6) NOT NULL COMMENT 'Gross revenue before TC tariff in USD',
  `exchange_rate` decimal(15,6) DEFAULT NULL COMMENT 'Exchange rate from Revenue currency',
  `tunecore_commision` decimal(15,6) DEFAULT 0.000000,
  `net_revenue` decimal(15,6) NOT NULL COMMENT 'Revenue due to customer after TC tariff',
  `net_revenue_currency` char(3) NOT NULL DEFAULT 'USD' COMMENT 'Net revenue due to customer currency',
  `sales_period_start` date NOT NULL COMMENT 'first day of the sales period month.',
  `you_tube_video_id` varchar(255) DEFAULT NULL,
  `song_name` varchar(255) NOT NULL DEFAULT '',
  `album_name` varchar(255) NOT NULL DEFAULT '',
  `label_name` varchar(255) NOT NULL DEFAULT '',
  `artist_name` varchar(255) NOT NULL DEFAULT '',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  `you_tube_royalty_intake_id` int(10) unsigned DEFAULT NULL,
  `posting_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`,`sales_period_start`,`song_id`),
  KEY `created_at` (`created_at`),
  KEY `song_id` (`song_id`),
  KEY `you_tube_royalty_intake_id` (`you_tube_royalty_intake_id`),
  KEY `person_id_2` (`person_id`,`you_tube_royalty_intake_id`),
  CONSTRAINT `you_tube_royalty_records_ibfk_2` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`),
  CONSTRAINT `you_tube_royalty_records_ibfk_3` FOREIGN KEY (`song_id`) REFERENCES `songs` (`id`),
  CONSTRAINT `you_tube_royalty_records_ibfk_4` FOREIGN KEY (`you_tube_royalty_intake_id`) REFERENCES `you_tube_royalty_intakes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `you_tube_royalty_records_new`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `you_tube_royalty_records_new` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL,
  `sip_you_tube_royalty_record_id` bigint(20) DEFAULT NULL,
  `song_id` int(11) NOT NULL,
  `total_views` int(10) unsigned NOT NULL DEFAULT 0,
  `you_tube_policy_type` enum('monetize','track','block','takedown') NOT NULL DEFAULT 'track',
  `gross_revenue` decimal(15,6) NOT NULL COMMENT 'Gross revenue before TC tariff in USD',
  `exchange_rate` decimal(15,6) DEFAULT NULL COMMENT 'Exchange rate from Revenue currency',
  `tunecore_commision` decimal(15,6) DEFAULT 0.000000,
  `net_revenue` decimal(15,6) NOT NULL COMMENT 'Revenue due to customer after TC tariff',
  `net_revenue_currency` char(3) NOT NULL DEFAULT 'USD' COMMENT 'Net revenue due to customer currency',
  `sales_period_start` date NOT NULL COMMENT 'first day of the sales period month.',
  `you_tube_video_id` varchar(255) DEFAULT NULL,
  `song_name` varchar(255) NOT NULL DEFAULT '',
  `album_name` varchar(255) NOT NULL DEFAULT '',
  `label_name` varchar(255) NOT NULL DEFAULT '',
  `artist_name` varchar(255) NOT NULL DEFAULT '',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Datetime of latest update',
  `created_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Datetime of creation.',
  `you_tube_royalty_intake_id` int(10) unsigned DEFAULT NULL,
  `posting_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `youtube_isrc_assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `youtube_isrc_assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tunecore_isrc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube_asset_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `api_response` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `youtube_monetizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `youtube_monetizations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `pub_opted_in` tinyint(1) DEFAULT 0,
  `effective_date` datetime DEFAULT NULL,
  `termination_date` datetime DEFAULT NULL,
  `agreed_to_terms_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `block_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_youtube_monetizations_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `youtube_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `youtube_preferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `channel_id` varchar(255) NOT NULL,
  `whitelist` tinyint(1) NOT NULL DEFAULT 1,
  `mcn` tinyint(1) NOT NULL DEFAULT 0,
  `mcn_agreement` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_youtube_preferences_on_person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `youtube_preferences_exports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `youtube_preferences_exports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from` datetime DEFAULT NULL,
  `to` datetime DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `youtube_sr_mail_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `youtube_sr_mail_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `select_label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_fields` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `header` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_website_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_youtube_sr_mail_templates_on_country_website_id` (`country_website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ytm_blocked_songs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ytm_blocked_songs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `song_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_ytm_blocked_songs_on_song_id` (`song_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ytm_ineligible_songs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ytm_ineligible_songs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `song_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_ytm_ineligible_songs_on_song_id` (`song_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 DROP PROCEDURE IF EXISTS `create_person_intake` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `create_person_intake`()
BEGIN
  DECLARE done, changed_status BOOLEAN DEFAULT 0;
  DECLARE var_person_id, var_balance_id, var_last_id, var_updated_ids int;
  DECLARE var_total, var_old_balance, var_streaming_total DECIMAL(23,14);
  DECLARE var_sales_record_masters_ids text;
  DECLARE var_amount_currency VARCHAR(3);
  DECLARE var_withheld_amount DECIMAL(12,2);


  DECLARE cur1 CURSOR FOR
  SELECT sr.person_id, srm.amount_currency, SUM(sr.amount) AS total, SUM(CASE WHEN sr.distribution_type = 'Streaming' THEN sr.amount ELSE 0 END) as streaming_total, GROUP_CONCAT(DISTINCT srm.id SEPARATOR '),( pi_id, ') AS sales_record_masters_ids
  FROM sales_record_masters srm
  STRAIGHT_JOIN sales_records sr ON sr.sales_record_master_id = srm.id
  LEFT OUTER JOIN (
    SELECT person_id
    FROM sales_record_masters srm
    STRAIGHT_JOIN person_intake_sales_record_masters pisrm ON srm.id = pisrm.sales_record_master_id
    INNER JOIN person_intakes pi ON pi.id = pisrm.person_intake_id
    WHERE srm.status IN ('new', 'processing')
    GROUP BY person_id ) processed_people ON processed_people.person_id = sr.person_id
  WHERE srm.status IN ('new','processing')
  AND processed_people.person_id is null
  GROUP BY person_id, srm.amount_currency;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;


  SET SESSION group_concat_max_len = 1000000;

  SELECT count(*) INTO var_updated_ids FROM sales_record_masters WHERE STATUS IN ('new', 'processing');

  if(var_updated_ids > 0) then

    OPEN cur1;
    FETCH cur1 INTO var_person_id, var_amount_currency, var_total, var_streaming_total, var_sales_record_masters_ids;

    WHILE( !done ) DO
      IF NOT changed_status THEN
        UPDATE sales_record_masters SET sales_record_masters.status = 'processing' WHERE sales_record_masters.status = 'new';
        SET changed_status = 1;
      END IF;

      START TRANSACTION;
        SET var_last_id = 0;


        INSERT INTO person_intakes (person_id, amount, created_at, reporting_month_id_temp, currency) VALUES (var_person_id, var_total, now(), 0, var_amount_currency);
        SELECT LAST_INSERT_ID() INTO var_last_id;


        UPDATE sales_record_masters
        STRAIGHT_JOIN sales_records ON sales_records.sales_record_master_id = sales_record_masters.id
        SET person_intake_id = var_last_id
        WHERE person_id = var_person_id
        AND sales_record_masters.status = 'processing';


        SET @person_intake_sql = CONCAT('INSERT INTO person_intake_sales_record_masters VALUES ( pi_id,', var_sales_record_masters_ids, ');');
        SET @person_intake_sql = REPLACE(@person_intake_sql, 'pi_id', var_last_id);
        PREPARE stmt FROM @person_intake_sql;
        EXECUTE stmt;


        SELECT id, balance INTO var_balance_id, var_old_balance FROM person_balances WHERE person_id = var_person_id FOR UPDATE;


        INSERT INTO person_transactions (created_at, person_id, debit, credit, previous_balance, target_id, target_type, currency) VALUES (NOW(), var_person_id, IF(var_total < 0, abs(var_total), 0), IF(var_total >= 0, var_total, 0), var_old_balance, var_last_id, 'PersonIntake', var_amount_currency);



        INSERT INTO taxable_earnings (revenue_source, credit, currency, person_id, created_at) VALUES ('streaming_royalties', var_streaming_total, var_amount_currency, var_person_id, NOW());

        IF var_total > 0 THEN
          ENCUMBRANCE_BLOCK: BEGIN
            CALL withhold_encumbrances(var_person_id, var_old_balance, var_total, var_withheld_amount);
          END ENCUMBRANCE_BLOCK;
        ELSE
          SET var_withheld_amount = 0;
        END IF;

        UPDATE person_balances SET balance = balance + var_total - var_withheld_amount WHERE person_id = var_person_id;
      COMMIT;

      FETCH cur1 INTO var_person_id, var_amount_currency, var_total, var_streaming_total, var_sales_record_masters_ids;

    END WHILE;
    CLOSE cur1;

    UPDATE sales_record_masters SET sales_record_masters.status = 'posted' WHERE sales_record_masters.status = 'processing';

  END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `populate_sales_record_summaries` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `populate_sales_record_summaries`()
BEGIN

  DECLARE done BOOLEAN DEFAULT 0;
  DECLARE var_srm_id int;

  DECLARE cur1 cursor for
  SELECT srm.id from sales_record_masters srm where summarized = 0;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

  OPEN cur1;
  FETCH cur1 into var_srm_id;
  WHILE( !done ) DO

    START TRANSACTION;
      INSERT into sales_record_summaries
      SELECT person_id, sales_record_master_id, related_type, related_id,
        IF( related_type = 'Video', 'Video', IF( related_type = 'Song', (select album_type from songs s inner join albums a on a.id = s.album_id where s.id = related_id), (select album_type from albums where albums.id = related_id))),
        IF( related_type = 'Video', related_id, if( related_type = 'Album', related_id, (select album_id from songs where songs.id = related_id))),
        SUM(IF(distribution_type='Download',quantity,0)), SUM(IF(distribution_type='Streaming',quantity,0)) AS streams_sold,
        SUM(IF(distribution_type='Download',amount,0)),
        SUM(IF(distribution_type='Streaming',amount,0)),
        person_intake_id
      FROM sales_records
      WHERE sales_records.sales_record_master_id = var_srm_id
      GROUP BY person_id, related_type, related_id, sales_record_master_id, person_intake_id;

      IF( select ROW_COUNT() > 0 ) THEN
        UPDATE sales_record_masters srm set srm.summarized = 1 where srm.id = var_srm_id;
      END IF;

    COMMIT;

    FETCH cur1 into var_srm_id;

  END WHILE;
  CLOSE cur1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `withhold_encumbrances` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `withhold_encumbrances`(
  IN in_person_id INTEGER,
  IN in_old_balance DECIMAL(23,14),
  IN in_posting_amount DECIMAL(23,14),
  OUT out_withheld_amount DECIMAL(12,2)
)
BEGIN
  DECLARE complete BOOLEAN DEFAULT 0;
  DECLARE var_encumbrance_id, var_last_detail_id INTEGER;
  DECLARE var_reference_source, var_currency VARCHAR(255);
  DECLARE var_outstanding_amount, var_witholding_percentage, var_max_drawdown, var_remaining_drawdown, var_person_balance DECIMAL(23,14);

  DECLARE cur_encumbrances CURSOR FOR
  SELECT id, outstanding_amount, reference_source
  FROM encumbrance_summaries
  WHERE person_id = in_person_id
  AND outstanding_amount > 0
  ORDER BY created_at ASC;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET complete = 1;

  SET out_withheld_amount = 0;
  SET var_person_balance = in_posting_amount + in_old_balance;

  SELECT currency
  INTO var_currency
  FROM person_balances
  WHERE person_id = in_person_id;

  OPEN cur_encumbrances;
  FETCH cur_encumbrances INTO var_encumbrance_id, var_outstanding_amount, var_reference_source;

  WHILE (!complete) DO

    SELECT  CASE WHEN option_value IS NULL
            THEN 0
            ELSE CONVERT(option_value, SIGNED INTEGER) / 100
            END
    INTO    var_witholding_percentage
    FROM    encumbrance_options
    WHERE   option_name = 'WITHHOLDING_MAX_PERCENTAGE'
    AND     reference_source = var_reference_source;

    SET var_max_drawdown = TRUNCATE(in_posting_amount * var_witholding_percentage, 2);

    IF var_remaining_drawdown IS NULL THEN
      SET var_remaining_drawdown = var_max_drawdown;
    END IF;

    IF var_outstanding_amount >= var_remaining_drawdown THEN
      INSERT INTO encumbrance_details (
        encumbrance_summary_id,
        transaction_date,
        transaction_amount,
        created_at,
        updated_at
      ) VALUES (
        var_encumbrance_id,
        CURDATE(),
        -1 * var_remaining_drawdown,
        CURRENT_TIMESTAMP,
        CURRENT_TIMESTAMP
      );

      SELECT LAST_INSERT_ID() INTO var_last_detail_id;

      UPDATE encumbrance_summaries
      SET outstanding_amount = outstanding_amount - var_remaining_drawdown,
          updated_at = CURRENT_TIMESTAMP
      WHERE id = var_encumbrance_id;

      INSERT INTO person_transactions (
        created_at,
        person_id,
        debit,
        credit,
        currency,
        previous_balance,
        target_id,
        target_type,
        comment
      ) VALUES (
        CURRENT_TIMESTAMP,
        in_person_id,
        var_remaining_drawdown,
        0,
        var_currency,
        var_person_balance,
        var_last_detail_id,
        'EncumbranceDetail',
        'TuneCore Direct Advance Recoupment'
      );

      SET var_person_balance = var_person_balance - var_remaining_drawdown;
      SET out_withheld_amount = out_withheld_amount + var_remaining_drawdown;
      SET var_remaining_drawdown = 0;
      SET complete = 1;
    ELSE
      INSERT INTO encumbrance_details (
        encumbrance_summary_id,
        transaction_date,
        transaction_amount,
        created_at,
        updated_at
      ) VALUES (
        var_encumbrance_id,
        CURDATE(),
        -1 * var_outstanding_amount,
        CURRENT_TIMESTAMP,
        CURRENT_TIMESTAMP
      );

      SELECT LAST_INSERT_ID() INTO var_last_detail_id;

      UPDATE encumbrance_summaries
      SET outstanding_amount = 0,
          updated_at = CURRENT_TIMESTAMP
      WHERE id = var_encumbrance_id;

      INSERT INTO person_transactions (
        created_at,
        person_id,
        debit,
        credit,
        currency,
        previous_balance,
        target_id,
        target_type,
        comment
      ) VALUES (
        CURRENT_TIMESTAMP,
        in_person_id,
        var_outstanding_amount,
        0,
        var_currency,
        var_person_balance,
        var_last_detail_id,
        'EncumbranceDetail',
        'TuneCore Direct Advance Recoupment'
      );

      SET var_person_balance = var_person_balance - var_outstanding_amount;
      SET out_withheld_amount = out_withheld_amount + var_outstanding_amount;
      SET var_remaining_drawdown = var_remaining_drawdown - var_outstanding_amount;
    END IF;

    FETCH cur_encumbrances INTO var_encumbrance_id, var_outstanding_amount, var_reference_source;
  END WHILE;

  CLOSE cur_encumbrances;

  SELECT out_withheld_amount;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50001 DROP TABLE IF EXISTS `current_tax_forms`*/;
/*!50001 DROP VIEW IF EXISTS `current_tax_forms`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `current_tax_forms` AS with ranked_tax_forms as (select `tunecore_development`.`tax_forms`.`id` AS `id`,`tunecore_development`.`tax_forms`.`program_id` AS `program_id`,`tunecore_development`.`tax_forms`.`submitted_at` AS `submitted_at`,`tunecore_development`.`tax_forms`.`created_at` AS `created_at`,`tunecore_development`.`tax_forms`.`person_id` AS `person_id`,`tunecore_development`.`tax_forms`.`tax_form_type_id` AS `tax_form_type_id`,row_number() over ( partition by `tunecore_development`.`tax_forms`.`person_id`,`tunecore_development`.`tax_forms`.`program_id` order by `tunecore_development`.`tax_forms`.`submitted_at`,`tunecore_development`.`tax_forms`.`created_at` desc) AS `ranked_form` from `tunecore_development`.`tax_forms`)select `ranked_tax_forms`.`id` AS `tax_form_id`,`ranked_tax_forms`.`person_id` AS `person_id`,`ranked_tax_forms`.`program_id` AS `program_id`,`ranked_tax_forms`.`tax_form_type_id` AS `tax_form_type_id`,`ranked_tax_forms`.`submitted_at` AS `submitted_at` from `ranked_tax_forms` where `ranked_tax_forms`.`ranked_form` = 1 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `latest_in_each_adyen_payment_methods`*/;
/*!50001 DROP VIEW IF EXISTS `latest_in_each_adyen_payment_methods`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `latest_in_each_adyen_payment_methods` AS select `t1`.`id` AS `id`,`t1`.`adyen_payment_method_info_id` AS `adyen_payment_method_info_id`,`t1`.`person_id` AS `person_id`,`t1`.`created_at` AS `created_at` from (`tunecore_test`.`adyen_stored_payment_methods` `t1` join (select `tunecore_test`.`adyen_stored_payment_methods`.`adyen_payment_method_info_id` AS `adyen_payment_method_info_id`,max(`tunecore_test`.`adyen_stored_payment_methods`.`created_at`) AS `created_at` from `tunecore_test`.`adyen_stored_payment_methods` group by `tunecore_test`.`adyen_stored_payment_methods`.`person_id`,`tunecore_test`.`adyen_stored_payment_methods`.`adyen_payment_method_info_id`) `t2` on(`t1`.`adyen_payment_method_info_id` = `t2`.`adyen_payment_method_info_id` and `t1`.`created_at` = `t2`.`created_at`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `tax_form_revenue_types`*/;
/*!50001 DROP VIEW IF EXISTS `tax_form_revenue_types`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `tax_form_revenue_types` AS select `tax_forms`.`id` AS `tax_form_id`,`revenue_streams`.`code` AS `revenue_stream_code` from ((`tax_forms` join `tax_form_revenue_streams` on(`tax_forms`.`id` = `tax_form_revenue_streams`.`tax_form_id`)) join `revenue_streams` on(`tax_form_revenue_streams`.`revenue_stream_id` = `revenue_streams`.`id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

INSERT INTO `schema_migrations` (version) VALUES
('1'),
('10'),
('100'),
('101'),
('102'),
('103'),
('104'),
('105'),
('106'),
('107'),
('108'),
('109'),
('11'),
('110'),
('111'),
('112'),
('113'),
('114'),
('115'),
('116'),
('117'),
('118'),
('119'),
('12'),
('120'),
('121'),
('122'),
('123'),
('124'),
('125'),
('126'),
('127'),
('128'),
('129'),
('13'),
('130'),
('131'),
('132'),
('133'),
('134'),
('135'),
('136'),
('137'),
('138'),
('139'),
('14'),
('140'),
('141'),
('142'),
('143'),
('144'),
('145'),
('146'),
('147'),
('148'),
('149'),
('15'),
('150'),
('151'),
('152'),
('153'),
('154'),
('155'),
('156'),
('157'),
('158'),
('159'),
('16'),
('160'),
('161'),
('162'),
('163'),
('164'),
('165'),
('166'),
('167'),
('168'),
('169'),
('17'),
('170'),
('171'),
('172'),
('173'),
('174'),
('175'),
('176'),
('177'),
('178'),
('179'),
('18'),
('180'),
('181'),
('182'),
('183'),
('184'),
('185'),
('186'),
('187'),
('188'),
('189'),
('19'),
('190'),
('191'),
('192'),
('193'),
('194'),
('195'),
('196'),
('197'),
('198'),
('199'),
('2'),
('20'),
('200'),
('20090513172828'),
('20090601184356'),
('20090805180803'),
('20090806151953'),
('20090806161856'),
('20090807174858'),
('20090807182009'),
('20090811205130'),
('20090814164103'),
('20090814165344'),
('20090814195006'),
('20090814210031'),
('20090818212500'),
('20090818212600'),
('20090825183937'),
('20090825185220'),
('20090826150843'),
('20090831193841'),
('20090831194829'),
('20090831194916'),
('20090901175833'),
('20090901175910'),
('20090903150010'),
('20090908162852'),
('20090914191625'),
('20090928145714'),
('20090928145750'),
('20091005162506'),
('20091005205303'),
('20091005205607'),
('20091005205726'),
('20091005215636'),
('20091014172055'),
('20091014180953'),
('20091015152143'),
('20091015201242'),
('20091029205553'),
('20091106220427'),
('20091111182712'),
('20091111190420'),
('20091111195323'),
('20091111210217'),
('20091111210241'),
('20091119171534'),
('20091201190118'),
('20091204172257'),
('20091215221102'),
('20091217151535'),
('20091217222709'),
('20091217222809'),
('20091217222947'),
('20091217223552'),
('20091217223610'),
('20091217230441'),
('20091217235704'),
('20091230213145'),
('201'),
('20100104162232'),
('20100106192652'),
('20100106195345'),
('20100106221250'),
('20100107221705'),
('20100108181532'),
('20100108201443'),
('20100111213714'),
('20100114153531'),
('20100120192306'),
('20100121174559'),
('20100219183322'),
('20100222175613'),
('20100222192508'),
('20100301190524'),
('20100303175924'),
('20100303190353'),
('20100303192146'),
('20100304174413'),
('20100304174540'),
('20100304174708'),
('20100304202512'),
('20100309151013'),
('20100309154512'),
('20100310192344'),
('20100319171040'),
('20100325203123'),
('20100326213706'),
('20100329193433'),
('20100331145900'),
('20100422204953'),
('20100422205217'),
('20100422205236'),
('20100422205314'),
('20100422205316'),
('20100423193703'),
('20100504190507'),
('20100504192234'),
('20100504193915'),
('20100504194556'),
('20100504204707'),
('20100504210457'),
('20100510140933'),
('20100520212317'),
('20100618163911'),
('20100628184939'),
('20100706203417'),
('20100709201552'),
('20100709202347'),
('20100804170114'),
('20100825132725'),
('20100827181311'),
('20100827184131'),
('20100831145019'),
('20100831160716'),
('20100908141347'),
('20100908182242'),
('20100921201909'),
('20100923182442'),
('20100927151511'),
('20101004204920'),
('20101005144009'),
('20101122201543'),
('20101122203049'),
('20101215160115'),
('20101229195558'),
('20110105202151'),
('20110208203715'),
('20110217205300'),
('20110223182558'),
('20110323162932'),
('20110323182128'),
('20110328160758'),
('20110328185359'),
('20110331160746'),
('20110331181519'),
('20110401174325'),
('20110401204413'),
('20110401212456'),
('20110426210426'),
('20110427224815'),
('20110503201623'),
('20110505155230'),
('20110506145545'),
('20110506145627'),
('20110506185856'),
('20110506205943'),
('20110520174922'),
('20110520175514'),
('20110609191941'),
('20110615183531'),
('20110621215248'),
('20110701170346'),
('20110701172848'),
('20110701190243'),
('20110701190307'),
('20110701190323'),
('20110701191555'),
('20110701191956'),
('20110705145642'),
('20110705154436'),
('20110705161232'),
('20110705164904'),
('20110707170524'),
('20110707174100'),
('20110707183219'),
('20110707184410'),
('20110707184613'),
('20110707185134'),
('20110707190716'),
('20110707193222'),
('20110708180919'),
('20110708180947'),
('20110711174249'),
('20110711175945'),
('20110711181219'),
('20110714161732'),
('20110714163618'),
('20110714172549'),
('20110718153418'),
('20110718154347'),
('20110718162109'),
('20110718173221'),
('20110718174017'),
('20110718180951'),
('20110719155352'),
('20110719155412'),
('20110719155623'),
('20110719180846'),
('20110721143336'),
('20110722181946'),
('20110725194151'),
('20110727211626'),
('20110728161855'),
('20110731211626'),
('20110731211627'),
('20110801213951'),
('20110802213957'),
('20110803192531'),
('20110803201221'),
('20110805153741'),
('20110805163303'),
('20110812191802'),
('20110817185303'),
('20110825173349'),
('20110826192741'),
('20110831215609'),
('20110901150013'),
('20110901214722'),
('20110902175329'),
('20110906152351'),
('20110908215914'),
('20110909154552'),
('20110909183604'),
('20110909203040'),
('20110920220036'),
('20110921172408'),
('20110921191224'),
('20110927163205'),
('20110927163710'),
('20110928192436'),
('20111006204127'),
('20111006210506'),
('20111006211702'),
('20111017210852'),
('20111019155751'),
('20111019155832'),
('20111027200258'),
('20111114193718'),
('20111115152527'),
('20111128203404'),
('20111129201605'),
('20111129220135'),
('20111207232414'),
('20111212153428'),
('20111212154004'),
('20111212154024'),
('20111212163545'),
('20111212231444'),
('20111214222301'),
('20111215184436'),
('20111219180621'),
('20111220052521'),
('20111220180446'),
('20111220182346'),
('20120105203441'),
('20120106172517'),
('20120117212300'),
('20120129062939'),
('20120203171948'),
('20120207212009'),
('20120209145947'),
('20120209202751'),
('20120209211106'),
('20120211061539'),
('20120213170426'),
('20120214192743'),
('20120216215004'),
('20120217223039'),
('20120229171648'),
('20120308215825'),
('20120309170141'),
('20120309220736'),
('20120309220809'),
('20120309220847'),
('20120309220901'),
('20120309220922'),
('20120309220939'),
('20120322141259'),
('20120323214956'),
('20120325205146'),
('20120326162358'),
('20120327153528'),
('20120403175932'),
('20120403190921'),
('20120403190922'),
('20120404160228'),
('20120404172327'),
('20120404190111'),
('20120404220033'),
('20120404222431'),
('20120405180301'),
('20120405204810'),
('20120405222418'),
('20120409180143'),
('20120410160945'),
('20120417192516'),
('20120419215055'),
('20120420155553'),
('20120420170227'),
('20120420172943'),
('20120421041820'),
('20120426175927'),
('20120426191522'),
('20120502204440'),
('20120504160840'),
('20120510212941'),
('20120514160120'),
('20120517181056'),
('20120517181152'),
('20120524140652'),
('20120604202323'),
('20120607185231'),
('20120607185348'),
('20120607195738'),
('20120612154053'),
('20120612154133'),
('20120612154255'),
('20120614161228'),
('20120615143836'),
('20120615163416'),
('20120615182923'),
('20120619154045'),
('20120619214842'),
('20120620163834'),
('20120620211142'),
('20120622184017'),
('20120629195340'),
('20120709182412'),
('20120709184333'),
('20120709184453'),
('20120709184534'),
('20120709184830'),
('20120726163940'),
('20120801142937'),
('20120802173310'),
('20120802173936'),
('20120802174019'),
('20120809214821'),
('20120815214941'),
('20120828203411'),
('20120907154954'),
('20120907160105'),
('20120912215740'),
('20120918143143'),
('20120918184743'),
('20120918194253'),
('20120920155638'),
('20120925194245'),
('20120926212948'),
('20121001171634'),
('20121002143838'),
('20121003153149'),
('20121005155415'),
('20121005160901'),
('20121005163438'),
('20121011192737'),
('20121016154305'),
('20121019154317'),
('20121031181608'),
('20121113195113'),
('20121121154715'),
('20121126152945'),
('20121211224100'),
('20121214193513'),
('20121214205955'),
('20121217212856'),
('20121219141326'),
('20130123115352'),
('20130211222122'),
('20130215154114'),
('20130219214506'),
('20130225221449'),
('20130225221527'),
('20130307195558'),
('20130308052709'),
('20130308155653'),
('20130308191918'),
('20130311202841'),
('20130313202156'),
('20130314162206'),
('20130315163922'),
('20130315203024'),
('20130318203440'),
('20130320215447'),
('20130325144624'),
('20130325174150'),
('20130325175053'),
('20130325191219'),
('20130325222508'),
('20130327152852'),
('20130327195536'),
('20130327211531'),
('20130328141936'),
('20130328195918'),
('20130329152507'),
('20130401135731'),
('20130401181909'),
('20130401193822'),
('20130403161251'),
('20130410143249'),
('20130410200916'),
('20130425191347'),
('20130425202943'),
('20130426164424'),
('20130501214555'),
('20130502183353'),
('20130502183506'),
('20130513153134'),
('20130520205854'),
('20130528190027'),
('20130530165607'),
('20130531141959'),
('20130604144418'),
('20130604210536'),
('20130605143625'),
('20130611140022'),
('20130619161928'),
('20130620160459'),
('20130625141757'),
('20130625171948'),
('20130625183615'),
('20130703140637'),
('20130703142238'),
('20130712185145'),
('20130712210958'),
('20130712210959'),
('20130715191816'),
('20130719175723'),
('20130719200000'),
('20130725173205'),
('20130731200536'),
('20130801181406'),
('20130808162202'),
('20130815181208'),
('20130815215942'),
('20130816190140'),
('20130904213439'),
('20130905192251'),
('20130910215828'),
('20130916221112'),
('20130926222247'),
('20130927192612'),
('20131004142230'),
('20131014175416'),
('20131017195339'),
('20131018194252'),
('20131021201218'),
('20131022164241'),
('20131029211739'),
('20131105195840'),
('20131105221249'),
('20131106225055'),
('20131113164633'),
('20131126215202'),
('20131206203206'),
('20131210182338'),
('20131210194948'),
('20140113224206'),
('20140120153356'),
('20140122210756'),
('20140124182042'),
('20140212181257'),
('20140227045800'),
('20140313063600'),
('20140313143612'),
('20140317214723'),
('20140324154918'),
('20140401153734'),
('20140402212036'),
('20140402215532'),
('20140414195705'),
('20140415202311'),
('20140416205009'),
('20140423143032'),
('20140425203553'),
('20140428211222'),
('20140501202115'),
('20140506215145'),
('20140509212511'),
('20140515181351'),
('20140520183725'),
('20140520185658'),
('20140522161837'),
('20140522213037'),
('20140523184800'),
('20140528221514'),
('20140529200908'),
('20140530214332'),
('20140604045406'),
('20140605162200'),
('20140605162921'),
('20140619164402'),
('20140624212255'),
('20140626120000'),
('20140626120001'),
('20140626120002'),
('20140630181824'),
('20140701154440'),
('20140707155336'),
('20140707155935'),
('20140708184741'),
('20140708184859'),
('20140710165307'),
('20140715140801'),
('20140718204112'),
('20140722160208'),
('20140722160224'),
('20140722170052'),
('20140724202102'),
('20140724202235'),
('20140728215829'),
('20140804160815'),
('20140813211839'),
('20140818144604'),
('20140819184906'),
('20140825175315'),
('20140827194434'),
('20140829155526'),
('20140902192822'),
('20140903201348'),
('20140904213158'),
('20140908182216'),
('20140910182239'),
('20140916194755'),
('20140916211506'),
('20140922205736'),
('20140925183004'),
('20140925224016'),
('20140929174416'),
('20141001174213'),
('20141008155354'),
('20141009192811'),
('20141015163526'),
('20141020194100'),
('20141020222621'),
('20141021202912'),
('20141022164333'),
('20141031154748'),
('20141103195356'),
('20141103200007'),
('20141104153131'),
('20141110202453'),
('20141112175715'),
('20141114191903'),
('20141117192317'),
('20141118183927'),
('20141119164603'),
('20141124221054'),
('20141201163549'),
('20141205185504'),
('20141205210459'),
('20141217212935'),
('20141222193208'),
('20150128142504'),
('20150128142529'),
('20150220155639'),
('20150310155153'),
('20150317012751'),
('20150327160029'),
('20150331212802'),
('20150403205230'),
('20150407201136'),
('20150409153450'),
('20150413153056'),
('20150416143112'),
('20150421170013'),
('20150421211041'),
('20150430143042'),
('20150501170018'),
('20150520190903'),
('20150527150831'),
('20150527152710'),
('20150528185751'),
('20150529171325'),
('20150529193945'),
('20150602172255'),
('20150604172739'),
('20150611180730'),
('20150615195816'),
('20150629143825'),
('20150630213135'),
('20150701190050'),
('20150702151146'),
('20150708171124'),
('20150709164344'),
('20150713190234'),
('20150714192705'),
('20150715140801'),
('20150715202613'),
('20150723225129'),
('20150805191616'),
('20150810212629'),
('20150810221805'),
('20150811022019'),
('20150821181914'),
('20150824153900'),
('20150824185252'),
('20150902145410'),
('20150902155835'),
('20150909184818'),
('20150910152919'),
('20150914190345'),
('20150930211059'),
('20150930211146'),
('20151005183327'),
('20151007172544'),
('20151020171021'),
('20151020201647'),
('20151020214917'),
('20151026192032'),
('20151028183658'),
('20151029213045'),
('20151103225220'),
('20151113204439'),
('20151117210244'),
('20151119155233'),
('20151130163852'),
('20151130184234'),
('20151201160530'),
('20151201165250'),
('20151201225519'),
('20151202193829'),
('20151202213508'),
('20151204160039'),
('20151204224159'),
('20151209211653'),
('20151214181003'),
('20151216162814'),
('20151222205813'),
('20160105224440'),
('20160105232710'),
('20160107220132'),
('20160108160815'),
('20160111212409'),
('20160112162627'),
('20160112193024'),
('20160121181725'),
('20160121185635'),
('20160127213657'),
('20160201190748'),
('20160202161138'),
('20160203195753'),
('20160205193545'),
('20160208220308'),
('20160216192002'),
('20160303000904'),
('20160308175814'),
('20160314151124'),
('20160321153536'),
('20160322182130'),
('20160328152030'),
('20160404133608'),
('20160406153612'),
('20160411163046'),
('20160412140013'),
('20160413214749'),
('20160415165654'),
('20160419194857'),
('20160421145201'),
('20160421150044'),
('20160422160336'),
('20160422184728'),
('20160429181436'),
('20160511195457'),
('20160523145453'),
('20160606152645'),
('20160610164626'),
('20160615142916'),
('20160617153912'),
('20160621181912'),
('20160621190403'),
('20160621201755'),
('20160629173216'),
('20160629201246'),
('20160711203220'),
('20160714155839'),
('20160718174049'),
('20160728215310'),
('20160804210658'),
('20160810161156'),
('20160810174428'),
('20160810200645'),
('20160823144334'),
('20160915182159'),
('20160921153834'),
('20160922174349'),
('20160923185756'),
('20160927170854'),
('20160927184436'),
('20161007154937'),
('20161007155249'),
('20161013211506'),
('20161014020549'),
('20161021015402'),
('20161104162018'),
('20161107183022'),
('20161107183055'),
('20161108171224'),
('20161121172909'),
('20161121190615'),
('20161121205913'),
('20161122211448'),
('20161122233848'),
('20170104152603'),
('20170131160737'),
('20170131202409'),
('20170203205048'),
('20170203205558'),
('20170203205717'),
('20170209155328'),
('20170222213717'),
('20170222230946'),
('20170301160225'),
('20170320153235'),
('20170322193627'),
('20170324195439'),
('20170330203535'),
('20170406195554'),
('20170407202308'),
('20170411195125'),
('20170419131541'),
('20170424152714'),
('20170510170631'),
('20170512142044'),
('20170517212923'),
('20170524214352'),
('20170609170455'),
('20170614192542'),
('20170705174911'),
('20170727182236'),
('20170728185024'),
('20170801205737'),
('20170802154108'),
('20170802154159'),
('20170803160525'),
('20170816164502'),
('20170816205649'),
('20170822134918'),
('20170822155022'),
('20170822215808'),
('20170828143013'),
('20170828150151'),
('20170828174219'),
('20170906190009'),
('20170908145400'),
('20170914190148'),
('20170915151704'),
('20170918205811'),
('20170919144041'),
('20170920152924'),
('20170920221228'),
('20170921150044'),
('20170922174126'),
('20170925160816'),
('20170926211301'),
('20170929163659'),
('20171002160306'),
('20171004203825'),
('20171016174311'),
('20171019164925'),
('20171107155832'),
('20171110202007'),
('20171116164804'),
('20171120155555'),
('20171120155936'),
('20171120161137'),
('20171120192246'),
('20171127154052'),
('20171127160856'),
('20171127163354'),
('20171127172830'),
('20171127173310'),
('20171201203639'),
('20171206191048'),
('20171206191754'),
('20171208152402'),
('20171215220056'),
('20171219164553'),
('20180116200524'),
('20180122201919'),
('20180124202313'),
('20180125134819'),
('20180207194003'),
('20180208172247'),
('20180216163982'),
('20180228161130'),
('20180313191117'),
('20180316134020'),
('20180330143642'),
('20180330150906'),
('20180330153337'),
('20180417142125'),
('20180424160243'),
('20180427200431'),
('20180430141125'),
('20180508180337'),
('20180516183350'),
('20180517173014'),
('20180604145857'),
('20180604145858'),
('20180605145426'),
('20180605205228'),
('20180606212036'),
('20180608185733'),
('20180608185835'),
('20180609210232'),
('20180621213131'),
('20180626142140'),
('20180626194348'),
('20180626204815'),
('20180627171733'),
('20180627194105'),
('20180627205810'),
('20180628185655'),
('20180629141738'),
('20180629193548'),
('20180703143734'),
('20180706161445'),
('20180723201527'),
('20180723201930'),
('20180730205416'),
('20180807213712'),
('20180809174729'),
('20180810144827'),
('20180813190152'),
('20180813192445'),
('20180813193823'),
('20180813194938'),
('20180813195244'),
('20180815180000'),
('20180815184945'),
('20180824195817'),
('20180829145606'),
('20180831151618'),
('20180912184354'),
('20180924145238'),
('20180924150609'),
('20180924150610'),
('20180924212848'),
('20180926161444'),
('20180927161951'),
('20181001192128'),
('20181002160028'),
('20181002160908'),
('20181002191408'),
('20181010175201'),
('20181010180604'),
('20181018193721'),
('20181018211415'),
('20181019202030'),
('20181024144516'),
('20181108171854'),
('20181128162234'),
('20181129201257'),
('20181204210807'),
('20181220200017'),
('20190102195733'),
('20190104165153'),
('20190107161606'),
('20190107162029'),
('20190107162133'),
('20190108204642'),
('20190109193034'),
('20190110212615'),
('20190110223256'),
('20190122230013'),
('20190130205601'),
('20190204221447'),
('20190211162524'),
('20190222170039'),
('20190222170828'),
('20190225174117'),
('20190227195039'),
('20190227221020'),
('20190228153650'),
('20190307200612'),
('20190307220956'),
('20190311152351'),
('20190404161335'),
('20190410145711'),
('20190410155123'),
('20190412161426'),
('20190501200850'),
('20190501205705'),
('20190514180104'),
('20190515161344'),
('20190520174753'),
('20190523164107'),
('20190603151723'),
('20190606213253'),
('20190606215533'),
('20190606220117'),
('20190606220243'),
('20190607145237'),
('20190607152504'),
('20190607154952'),
('20190607181044'),
('20190607193922'),
('20190613193445'),
('20190614201148'),
('20190618195442'),
('20190709134014'),
('20190712212400'),
('20190716113339'),
('20190723212554'),
('20190726144422'),
('20190729174836'),
('20190729175023'),
('20190809202019'),
('20190815205229'),
('20190820150336'),
('20190823153951'),
('20190826204938'),
('20190830154156'),
('20190903202844'),
('20190905135913'),
('20190905135938'),
('20190905162636'),
('20191002160524'),
('20191003134652'),
('20191004145145'),
('20191008203312'),
('20191014203645'),
('20191017151247'),
('20191018153949'),
('20191023172701'),
('20191107162704'),
('20191111222847'),
('20191112134628'),
('20191119170822'),
('20191121034844'),
('20191125203738'),
('20191129172302'),
('20191211085744'),
('20191218175647'),
('20191219213124'),
('20191219213256'),
('20191226203302'),
('20191227165751'),
('20191227194122'),
('202'),
('20200103051656'),
('20200116164646'),
('20200117212240'),
('20200121162941'),
('20200213063824'),
('20200213082118'),
('20200213170818'),
('20200225095841'),
('20200225100007'),
('20200225162635'),
('20200225212723'),
('20200311184751'),
('20200313181334'),
('20200316163323'),
('20200317072806'),
('20200319193607'),
('20200324173402'),
('20200327043619'),
('20200330201907'),
('20200331075829'),
('20200331193227'),
('20200331211541'),
('20200402122201'),
('20200402152447'),
('20200413083931'),
('20200413191043'),
('20200415151308'),
('20200421194610'),
('20200421200731'),
('20200421200926'),
('20200421201232'),
('20200422071158'),
('20200424144828'),
('20200424145111'),
('20200424151247'),
('20200427155222'),
('20200427184522'),
('20200427184653'),
('20200427185019'),
('20200427185053'),
('20200428164006'),
('20200430005541'),
('20200504094143'),
('20200507190111'),
('20200508035130'),
('20200518202127'),
('20200519153240'),
('20200519173749'),
('20200520095901'),
('20200527133239'),
('20200527134400'),
('20200527182236'),
('20200527182456'),
('20200528135839'),
('20200529190423'),
('20200529191700'),
('20200529193238'),
('20200601195409'),
('20200603163848'),
('20200604042310'),
('20200604054725'),
('20200604073904'),
('20200605150531'),
('20200605190459'),
('20200608110617'),
('20200608110649'),
('20200608210256'),
('20200609202803'),
('20200609204913'),
('20200610101231'),
('20200615153825'),
('20200616141623'),
('20200617173957'),
('20200617194305'),
('20200618195606'),
('20200618200429'),
('20200622202507'),
('20200624150841'),
('20200625160153'),
('20200701153920'),
('20200702171448'),
('20200706130303'),
('20200706211306'),
('20200707214549'),
('20200713150804'),
('20200714090413'),
('20200716101351'),
('20200716151610'),
('20200717155739'),
('20200720133109'),
('20200720154056'),
('20200723100657'),
('20200728141902'),
('20200729175248'),
('20200803061931'),
('20200803105813'),
('20200804125421'),
('20200805201140'),
('20200806170938'),
('20200806171103'),
('20200806171210'),
('20200806183304'),
('20200811182422'),
('20200817103108'),
('20200817150405'),
('20200817193452'),
('20200825174439'),
('20200826131057'),
('20200902092646'),
('20200903135444'),
('20200903154519'),
('20200903154551'),
('20200908173721'),
('20200911133233'),
('20200914161357'),
('20200916154118'),
('20200917105210'),
('20200917183125'),
('20200917184925'),
('20200918095425'),
('20200918162758'),
('20200918165454'),
('20200921133216'),
('20200921134120'),
('20200922115908'),
('20200922115916'),
('20200923170356'),
('20200924103009'),
('20200924230332'),
('20200925183905'),
('20200928153002'),
('20200928170304'),
('20200928171148'),
('20200929154434'),
('20200929172223'),
('20201001112749'),
('20201002070246'),
('20201006090213'),
('20201006090449'),
('20201006090832'),
('20201014110721'),
('20201016114724'),
('20201020101228'),
('20201020171837'),
('20201023214331'),
('20201026172808'),
('20201028175832'),
('20201029200932'),
('20201029201032'),
('20201029201132'),
('20201104124105'),
('20201104213641'),
('20201105130344'),
('20201106105005'),
('20201106105153'),
('20201106112813'),
('20201113205354'),
('20201119223818'),
('20201124134117'),
('20201127135629'),
('20201207100013'),
('20201207101859'),
('20201207162622'),
('20201208065016'),
('20201208073011'),
('20201208192942'),
('20201209191007'),
('20201211195204'),
('20201214071408'),
('20201214084915'),
('20201221182734'),
('20201229064908'),
('20201229080523'),
('20201229082122'),
('20201229082210'),
('20201229093537'),
('20201229133602'),
('20201231090920'),
('20210106083321'),
('20210106094939'),
('20210106101258'),
('20210108175927'),
('20210108180347'),
('20210111093744'),
('20210112093257'),
('20210113090814'),
('20210113094400'),
('20210113094504'),
('20210114051433'),
('20210128180742'),
('20210201062149'),
('20210203145310'),
('20210216143124'),
('20210218194144'),
('20210218223433'),
('20210223062736'),
('20210223153056'),
('20210302205229'),
('20210302205902'),
('20210310181714'),
('20210315101941'),
('20210316192218'),
('20210319210342'),
('20210323143554'),
('20210323173320'),
('20210330154840'),
('20210402144133'),
('20210405205646'),
('20210414181425'),
('20210416150153'),
('20210416204108'),
('20210419203653'),
('20210421211455'),
('20210421211507'),
('20210428161111'),
('20210429192234'),
('20210429192246'),
('20210504095833'),
('20210506165950'),
('20210513163127'),
('20210513232816'),
('20210514094222'),
('20210514094304'),
('20210514154525'),
('20210519164053'),
('20210519201941'),
('20210520175618'),
('20210520202257'),
('20210524143904'),
('20210526213420'),
('20210603210724'),
('20210607181541'),
('20210621201501'),
('20210621212718'),
('20210622180611'),
('20210625054233'),
('20210629210421'),
('20210630121146'),
('20210630182825'),
('20210630184703'),
('20210706113408'),
('20210708165102'),
('20210712204728'),
('20210713161241'),
('20210713170421'),
('20210713170604'),
('20210713171218'),
('20210716183626'),
('20210721145632'),
('20210722203140'),
('20210728170121'),
('20210730112357'),
('20210810010954'),
('20210812120645'),
('20210812125905'),
('20210812155044'),
('20210812184337'),
('20210813112129'),
('20210813150635'),
('20210818101117'),
('20210818101248'),
('20210819122659'),
('20210819150923'),
('20210819150943'),
('20210820173848'),
('20210830071948'),
('20210830072004'),
('20210830072018'),
('20210830105513'),
('20210903181405'),
('20210907123113'),
('20210907124704'),
('20210908152034'),
('20210921070837'),
('20210921070843'),
('20210921181839'),
('20210929182101'),
('20210929182110'),
('20210929182140'),
('20210930143814'),
('20210930173757'),
('20211004093722'),
('20211013171913'),
('20211014105001'),
('20211019102549'),
('20211020190148'),
('20211021121145'),
('20211022001236'),
('20211029150427'),
('20211103112147'),
('20211104152836'),
('20211115144427'),
('20211115145242'),
('20211115162211'),
('20211115165014'),
('20211115210123'),
('20211115210130'),
('20211115210244'),
('20211116165906'),
('20211116165940'),
('20211116211352'),
('20211116221247'),
('20211117204928'),
('20211118113603'),
('20211122220950'),
('20211123222037'),
('20211130172549'),
('20211201200231'),
('20211201200722'),
('20211201200912'),
('20211206145854'),
('20211209201631'),
('20211210153553'),
('20211213064313'),
('20211213140019'),
('20211216055738'),
('20211216093451'),
('20211220174622'),
('20211221230228'),
('20211222002248'),
('20211222002347'),
('20211222002400'),
('20220103132415'),
('20220103222012'),
('20220106162134'),
('20220112183714'),
('20220112183724'),
('20220112183731'),
('20220112183739'),
('20220114164347'),
('20220114164350'),
('20220120122159'),
('20220120193910'),
('20220126054119'),
('20220203230735'),
('20220203230940'),
('20220203232117'),
('20220204203121'),
('20220204203618'),
('20220209074605'),
('20220216105703'),
('20220217045851'),
('20220223124840'),
('20220225105959'),
('20220302171012'),
('20220307111147'),
('20220307113752'),
('20220309140203'),
('20220309142152'),
('20220309142203'),
('20220317164553'),
('20220317164626'),
('20220317164637'),
('20220317164650'),
('20220317195938'),
('20220317202712'),
('20220317202723'),
('20220322161830'),
('20220331193156'),
('20220331193159'),
('20220405150927'),
('20220408183002'),
('20220412152820'),
('20220412180040'),
('20220425132614'),
('20220425132624'),
('20220523091246'),
('20220523092550'),
('20220607114613'),
('20220607132537'),
('20220613145637'),
('20220613150319'),
('20220613150632'),
('20220704145624'),
('20220706065713'),
('20220706094533'),
('20220706102109'),
('20220708102656'),
('20220715122515'),
('20220719152158'),
('20220721173851'),
('20220721203656'),
('20220726134004'),
('20220801114726'),
('20220802170625'),
('20220804200430'),
('20220809125705'),
('20220816145832'),
('20220817190205'),
('20220819123230'),
('20220822071031'),
('20220823173906'),
('20220823174108'),
('20220824174940'),
('20220830164050'),
('20220901045635'),
('20220905092112'),
('20220906171252'),
('20220912135131'),
('20220912135308'),
('20220913171256'),
('20220923115807'),
('20220923121347'),
('20220928172721'),
('20221003202344'),
('20221003214027'),
('20221006112559'),
('20221011204422'),
('20221012143826'),
('20221012181744'),
('20221012203158'),
('20221013073715'),
('20221013083929'),
('20221013091632'),
('20221013141117'),
('20221013164352'),
('20221102010101'),
('20221102010102'),
('20221025142906'),
('20221028152408'),
('20221025200904'),
('20221021160215'),
('20221103164112'),
('20221109182540'),
('20221114091319'),
('20221114091324'),
('20221114091331'),
('20221122022954'),
('20221122065946'),
('20221128135231'),
('20221128135553'),
('20221213081509'),
('20221201150327'),
('20221212143007'),
('20221208095446'),
('20221221124711'),
('20230125115915'),
('20230119000547'),
('20230119125212'),
('20230119125222'),
('20230127143318'),
('20230201213504'),
('20230201213610'),
('20230130065114'),
('20230206191426'),
('20230206195404'),
('20230214151457'),
('203'),
('204'),
('205'),
('206'),
('207'),
('208'),
('209'),
('21'),
('210'),
('211'),
('212'),
('213'),
('214'),
('215'),
('216'),
('217'),
('218'),
('219'),
('22'),
('220'),
('221'),
('222'),
('223'),
('224'),
('225'),
('226'),
('227'),
('228'),
('229'),
('23'),
('230'),
('231'),
('232'),
('233'),
('234'),
('235'),
('236'),
('237'),
('238'),
('239'),
('24'),
('240'),
('241'),
('242'),
('243'),
('244'),
('245'),
('246'),
('247'),
('248'),
('249'),
('25'),
('250'),
('251'),
('252'),
('253'),
('254'),
('255'),
('256'),
('257'),
('258'),
('259'),
('26'),
('260'),
('261'),
('262'),
('263'),
('264'),
('265'),
('266'),
('267'),
('268'),
('269'),
('27'),
('270'),
('271'),
('272'),
('273'),
('274'),
('275'),
('276'),
('277'),
('278'),
('279'),
('28'),
('280'),
('281'),
('282'),
('283'),
('284'),
('285'),
('286'),
('287'),
('288'),
('289'),
('29'),
('290'),
('291'),
('292'),
('293'),
('294'),
('295'),
('296'),
('297'),
('298'),
('299'),
('3'),
('30'),
('300'),
('301'),
('302'),
('303'),
('304'),
('305'),
('306'),
('307'),
('308'),
('309'),
('31'),
('310'),
('311'),
('312'),
('313'),
('314'),
('315'),
('316'),
('317'),
('318'),
('319'),
('32'),
('320'),
('321'),
('322'),
('323'),
('324'),
('325'),
('326'),
('327'),
('328'),
('329'),
('33'),
('330'),
('331'),
('332'),
('333'),
('334'),
('335'),
('336'),
('337'),
('338'),
('339'),
('34'),
('340'),
('341'),
('342'),
('343'),
('344'),
('345'),
('346'),
('347'),
('348'),
('349'),
('35'),
('350'),
('351'),
('352'),
('353'),
('354'),
('355'),
('356'),
('357'),
('358'),
('359'),
('36'),
('360'),
('361'),
('362'),
('363'),
('364'),
('365'),
('366'),
('367'),
('368'),
('369'),
('37'),
('370'),
('371'),
('372'),
('373'),
('374'),
('375'),
('376'),
('377'),
('378'),
('379'),
('38'),
('380'),
('381'),
('382'),
('383'),
('384'),
('385'),
('386'),
('387'),
('388'),
('389'),
('39'),
('390'),
('391'),
('392'),
('393'),
('394'),
('395'),
('396'),
('397'),
('398'),
('399'),
('4'),
('40'),
('400'),
('401'),
('402'),
('403'),
('404'),
('405'),
('406'),
('407'),
('408'),
('409'),
('41'),
('410'),
('411'),
('412'),
('413'),
('414'),
('415'),
('416'),
('417'),
('418'),
('419'),
('42'),
('420'),
('421'),
('422'),
('423'),
('424'),
('425'),
('426'),
('427'),
('428'),
('429'),
('43'),
('430'),
('44'),
('45'),
('46'),
('47'),
('48'),
('49'),
('5'),
('50'),
('51'),
('52'),
('53'),
('54'),
('55'),
('56'),
('57'),
('58'),
('59'),
('6'),
('60'),
('61'),
('62'),
('63'),
('64'),
('65'),
('66'),
('67'),
('68'),
('69'),
('7'),
('70'),
('71'),
('72'),
('73'),
('74'),
('75'),
('76'),
('77'),
('78'),
('79'),
('8'),
('80'),
('81'),
('82'),
('83'),
('84'),
('85'),
('86'),
('87'),
('88'),
('89'),
('9'),
('90'),
('91'),
('92'),
('93'),
('94'),
('95'),
('96'),
('97'),
('98'),
('99');
