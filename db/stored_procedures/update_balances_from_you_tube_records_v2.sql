DROP PROCEDURE IF EXISTS update_balances_from_you_tube_records_v2;;
DELIMITER ;;
CREATE PROCEDURE update_balances_from_you_tube_records_v2(
  IN in_posting_ids TEXT
)
BEGIN
  DECLARE done BOOLEAN DEFAULT 0;
  declare var_person_id, var_balance_id, var_last_id int;
  declare var_net_revenue, var_old_balance DECIMAL(23,14);
  DECLARE var_net_revenue_currency CHAR(3);

  /* Get people that have money in any you tube royalty records without a you_tube_royalty_intake associated yet */
  declare cur1 cursor for
  SELECT person_id, net_revenue_currency, sum(net_revenue) as net_revenue
  FROM you_tube_royalty_records
  WHERE posting_id IN (in_posting_ids) and you_tube_royalty_intake_id is null and net_revenue > 0
  group by person_id, net_revenue_currency;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

  DECLARE exit handler for sqlexception
    BEGIN
    ROLLBACK;
  END;

  OPEN cur1;
  fetch cur1 into var_person_id, var_net_revenue_currency, var_net_revenue;

  WHILE( !done ) DO

    START TRANSACTION;
      set var_last_id=0;

      /* Create a person intake for the total amount gained/lost */
      INSERT INTO you_tube_royalty_intakes (person_id, amount, created_at, currency) VALUES (var_person_id, var_net_revenue, now(), var_net_revenue_currency);
      select last_insert_id() into var_last_id;

      /* Update you tube records with you_tube_royalty_intake_id */
      UPDATE you_tube_royalty_records
      SET you_tube_royalty_intake_id=var_last_id
      WHERE person_id=var_person_id and
      you_tube_royalty_intake_id is null and
      net_revenue > 0 and
      net_revenue_currency = var_net_revenue_currency COLLATE utf8_general_ci;

      /* Select old balance */
      SELECT id, balance into var_balance_id, var_old_balance FROM person_balances WHERE person_id=var_person_id for update;

      /* Create person transactions and update the persons balance */
      INSERT INTO person_transactions (created_at, person_id, debit, credit, previous_balance, target_id, target_type, currency) VALUES (NOW(), var_person_id, IF(var_net_revenue<0, abs(var_net_revenue), 0), IF(var_net_revenue>=0, var_net_revenue, 0), var_old_balance, var_last_id, 'YouTubeRoyaltyIntake', var_net_revenue_currency);

      /* Create taxable_earnings for youtube_streaming */
      INSERT INTO taxable_earnings (revenue_source, credit, currency, person_id, created_at) VALUES ('youtube', var_net_revenue, var_net_revenue_currency, var_person_id, NOW());
      UPDATE person_balances SET balance=balance+var_net_revenue WHERE person_id=var_person_id;
    COMMIT;

    fetch cur1 into var_person_id, var_net_revenue_currency, var_net_revenue;

  END WHILE;
  CLOSE cur1;

END;;
DELIMITER ;
