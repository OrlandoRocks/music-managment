DROP PROCEDURE IF EXISTS withhold_encumbrances;
DELIMITER ;;
CREATE PROCEDURE withhold_encumbrances(
  IN in_person_id INTEGER,
  IN in_old_balance DECIMAL(23,14),
  IN in_posting_amount DECIMAL(23,14),
  OUT out_withheld_amount DECIMAL(12,2)
)
BEGIN
  DECLARE complete BOOLEAN DEFAULT 0;
  DECLARE var_encumbrance_id, var_last_detail_id INTEGER;
  DECLARE var_reference_source, var_currency VARCHAR(255);
  DECLARE var_outstanding_amount, var_witholding_percentage, var_max_drawdown, var_remaining_drawdown, var_person_balance DECIMAL(23,14);

  DECLARE cur_encumbrances CURSOR FOR
  SELECT id, outstanding_amount, reference_source
  FROM encumbrance_summaries
  WHERE person_id = in_person_id
  AND outstanding_amount > 0
  ORDER BY created_at ASC;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET complete = 1;

  SET out_withheld_amount = 0;
  SET var_person_balance = in_posting_amount + in_old_balance;

  SELECT currency
  INTO var_currency
  FROM person_balances
  WHERE person_id = in_person_id;

  OPEN cur_encumbrances;
  FETCH cur_encumbrances INTO var_encumbrance_id, var_outstanding_amount, var_reference_source;

  WHILE (!complete) DO

    SELECT  CASE WHEN option_value IS NULL
            THEN 0
            ELSE CONVERT(option_value, SIGNED INTEGER) / 100
            END
    INTO    var_witholding_percentage
    FROM    encumbrance_options
    WHERE   option_name = 'WITHHOLDING_MAX_PERCENTAGE'
    AND     reference_source = var_reference_source;

    SET var_max_drawdown = TRUNCATE(in_posting_amount * var_witholding_percentage, 2);

    IF var_remaining_drawdown IS NULL THEN
      SET var_remaining_drawdown = var_max_drawdown;
    END IF;

    IF var_outstanding_amount >= var_remaining_drawdown THEN
      INSERT INTO encumbrance_details (
        encumbrance_summary_id,
        transaction_date,
        transaction_amount,
        created_at,
        updated_at
      ) VALUES (
        var_encumbrance_id,
        CURDATE(),
        -1 * var_remaining_drawdown,
        CURRENT_TIMESTAMP,
        CURRENT_TIMESTAMP
      );

      SELECT LAST_INSERT_ID() INTO var_last_detail_id;

      UPDATE encumbrance_summaries
      SET outstanding_amount = outstanding_amount - var_remaining_drawdown,
          updated_at = CURRENT_TIMESTAMP
      WHERE id = var_encumbrance_id;

      INSERT INTO person_transactions (
        created_at,
        person_id,
        debit,
        credit,
        currency,
        previous_balance,
        target_id,
        target_type,
        comment
      ) VALUES (
        CURRENT_TIMESTAMP,
        in_person_id,
        var_remaining_drawdown,
        0,
        var_currency,
        var_person_balance,
        var_last_detail_id,
        'EncumbranceDetail',
        'TuneCore Direct Advance Recoupment'
      );

      SET var_person_balance = var_person_balance - var_remaining_drawdown;
      SET out_withheld_amount = out_withheld_amount + var_remaining_drawdown;
      SET var_remaining_drawdown = 0;
      SET complete = 1;
    ELSE
      INSERT INTO encumbrance_details (
        encumbrance_summary_id,
        transaction_date,
        transaction_amount,
        created_at,
        updated_at
      ) VALUES (
        var_encumbrance_id,
        CURDATE(),
        -1 * var_outstanding_amount,
        CURRENT_TIMESTAMP,
        CURRENT_TIMESTAMP
      );

      SELECT LAST_INSERT_ID() INTO var_last_detail_id;

      UPDATE encumbrance_summaries
      SET outstanding_amount = 0,
          updated_at = CURRENT_TIMESTAMP
      WHERE id = var_encumbrance_id;

      INSERT INTO person_transactions (
        created_at,
        person_id,
        debit,
        credit,
        currency,
        previous_balance,
        target_id,
        target_type,
        comment
      ) VALUES (
        CURRENT_TIMESTAMP,
        in_person_id,
        var_outstanding_amount,
        0,
        var_currency,
        var_person_balance,
        var_last_detail_id,
        'EncumbranceDetail',
        'TuneCore Direct Advance Recoupment'
      );

      SET var_person_balance = var_person_balance - var_outstanding_amount;
      SET out_withheld_amount = out_withheld_amount + var_outstanding_amount;
      SET var_remaining_drawdown = var_remaining_drawdown - var_outstanding_amount;
    END IF;

    FETCH cur_encumbrances INTO var_encumbrance_id, var_outstanding_amount, var_reference_source;
  END WHILE;

  CLOSE cur_encumbrances;

  SELECT out_withheld_amount;
END;;
DELIMITER ;
