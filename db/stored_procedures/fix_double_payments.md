# State of Double Payments
The `create_person_intake` stored procedure was run twice. This procedure is idempotent, so how did pay some customers twice?

* The second run was initiated while the first one was in-progress, and the stored procedure does not lock any of the tables it interacts with. The stored procedure identifies who to pay and how much by querying the `sales_record_masters` table for records that are _not_ in the `"posted"` state. The sales record masters marked as `"posted"` at the end of the stored procedure. The stored procedure happens in a transaction so the second run of the procedure identified sales recrod masters that had actually been paid, as unpaid, because their state was not yet updated to "posted" because the first procedure was still running.

Breakdown of double payments:

```
CORRECT Sip posting total: 2,988,534
Amount in total we paid our customers: 3,836,178
Amount by which we overpaid our customers: 847644.941272
Number of peopele we paid twice: 60,359
```

Queries we ran to get this info:

```
# Amount by which we overpaid
sum(amount) FROM person_intakes WHERE person_id IN (SELECT person_id FROM person_intakes WHERE created_at > '2018-05-18' GROUP BY 1 HAVING count(1) = 2) AND created_at > '2018-05-18';

# Number of people who were overpaid
select person_id, count(1) as count from person_intakes where created_at > '2018-05-18' group by 1 having count = 2;

# total amount posting SHOULD have been (query in Sip DB)
SELECT sum(amount) FROM postings WHERE postings.created_at > '2018-05-18' AND TYPE = 'StandardPosting'

# Posting IDs that this effects:
1420, 1421, 1422, 1423, 1424, 1425, 1426
```

## Impact of the Stored Procedure

#### Tl;dr

The stored procdure `create_person_intake.sql` ran and kicked off another stored procedure `, withhold_encumbrances.sql`. Overall it:

* Selects sales record masters that are in the `new` or `processing` state.
* Uses those records to create a person_intake record for each person who needs to get paid from this posting, with the amount from that person's `sales_record_master` record.
* Creates a `person_intake_sales_record_master` record.
* Creates a `person_transaction` represending the debit/credit to the person with the amount from `sales_record_masters`.
* Calculates encumbrances.
  * If the given person has an `encumbrances_summary` record with an `outstanding_amount` that is greater than 0, we will calculate the percentage of your posting that we need to without to pay off that amount.
  * If the amount to withhold is greater than the outstanding amount, we will take a percentage out of the posting amount we are paying you. Then we will set the encumbrances summary outstanding ammount to `outstanding_amount - what_we_witheld`.
  * Use that amount to debit your account by creating a person transaction record for that debit.
  * If the amount to withold is less than the outstanding amount, we will withold all of our oustanding amount you owe from this posting and set the encumbrance summary outstanding amount to 0. We will create a person transaction record to represent this debit.
  * This procedure returns the total amount we witheld/debited.
* Update the person_balace with the posting amount from sales record masters minus the amount witheld.
* Set sales record masters state to "posted".

### What We Need to Do:

* Identify the people who were paid twice.
* Identify their person_intake records and identify which of the two intakes from 5/18 were duplicative.
* Identify that amount of money they _should_ have been paid (i.e. the amount of _one_ of the intakes)
* Identify the encumbrance, if any, that was applied to their person_balance by identifing the person transaction that represendted that encumbrance.
* Create a person_transaction to debit the customers account for the duplicative sip payment (i.e. the amount from their person_intake) _and_ create a person transaction to debit the account for the encumbrance witholding, if any.
* Debit the person balance the amount of the sip payment debit + amount of encumbrance debit.
* Gotcha: We have to account for any person intakes with negative amounts/person transactions from this posting that represent debits, make sure we _credit_ these accounts to undo the "second payement", i.e. impact of the second run of the stored procedure.

#### Details of stored procedure

Here is the stored procedure that ran:

```
DROP PROCEDURE IF EXISTS create_person_intake;;
DELIMITER ;;
CREATE PROCEDURE create_person_intake()
BEGIN
  DECLARE done, changed_status BOOLEAN DEFAULT 0;
  DECLARE var_person_id, var_balance_id, var_last_id, var_updated_ids int;
  DECLARE var_total, var_old_balance DECIMAL(23,14);
  DECLARE var_sales_record_masters_ids text;
  DECLARE var_amount_currency VARCHAR(3);
  DECLARE var_withheld_amount DECIMAL(12,2);

  /* Get people that have money in any new or processing sales_record_masters that haven't had a person_intake created for any of those new or processing sales_record_masters */
  DECLARE cur1 CURSOR FOR
  SELECT sr.person_id, srm.amount_currency, SUM(sr.amount) AS total, GROUP_CONCAT(DISTINCT srm.id SEPARATOR '),( pi_id, ') AS sales_record_masters_ids
  FROM sales_record_masters srm
  STRAIGHT_JOIN sales_records sr ON sr.sales_record_master_id = srm.id
  LEFT OUTER JOIN (
    SELECT person_id
    FROM sales_record_masters srm
    STRAIGHT_JOIN person_intake_sales_record_masters pisrm ON srm.id = pisrm.sales_record_master_id
    INNER JOIN person_intakes pi ON pi.id = pisrm.person_intake_id
    WHERE srm.status IN ('new', 'processing')
    GROUP BY person_id ) processed_people ON processed_people.person_id = sr.person_id
  WHERE srm.status IN ('new','processing')
  AND processed_people.person_id is null
  GROUP BY person_id, srm.amount_currency;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

  /* Since we add text to the group concat up the limit */
  SET SESSION group_concat_max_len = 1000000;

  SELECT count(*) INTO var_updated_ids FROM sales_record_masters WHERE STATUS IN ('new', 'processing');

  if(var_updated_ids > 0) then

    OPEN cur1;
    FETCH cur1 INTO var_person_id, var_amount_currency, var_total, var_sales_record_masters_ids;

    WHILE( !done ) DO
      IF NOT changed_status THEN
        UPDATE sales_record_masters SET sales_record_masters.status = 'processing' WHERE sales_record_masters.status = 'new';
        SET changed_status = 1;
      END IF;

      START TRANSACTION;
        SET var_last_id = 0;

        /* Create a person intake for the total amount gained/lost */
        INSERT INTO person_intakes (person_id, amount, created_at, reporting_month_id_temp, currency) VALUES (var_person_id, var_total, now(), 0, var_amount_currency);
        SELECT LAST_INSERT_ID() INTO var_last_id;

        /* Update sales records with the person_intake_id */
        UPDATE sales_record_masters
        STRAIGHT_JOIN sales_records ON sales_records.sales_record_master_id = sales_record_masters.id
        SET person_intake_id = var_last_id
        WHERE person_id = var_person_id
        AND sales_record_masters.status = 'processing';

        /* Create a statment to insert into person_intake_sales_record_masters with the unique sales_record_masters, replace pi_id with the new person_intake_id */
        SET @person_intake_sql = CONCAT('INSERT INTO person_intake_sales_record_masters VALUES ( pi_id,', var_sales_record_masters_ids, ');');
        SET @person_intake_sql = REPLACE(@person_intake_sql, 'pi_id', var_last_id);
        PREPARE stmt FROM @person_intake_sql;
        EXECUTE stmt;

        /* Select old balance */
        SELECT id, balance INTO var_balance_id, var_old_balance FROM person_balances WHERE person_id = var_person_id FOR UPDATE;

        /* Create person transactions and update the persons balance */
        INSERT INTO person_transactions (created_at, person_id, debit, credit, previous_balance, target_id, target_type, currency) VALUES (NOW(), var_person_id, IF(var_total < 0, abs(var_total), 0), IF(var_total >= 0, var_total, 0), var_old_balance, var_last_id, 'PersonIntake', var_amount_currency);

        IF var_total > 0 THEN
          ENCUMBRANCE_BLOCK: BEGIN
            CALL withhold_encumbrances(var_person_id, var_old_balance, var_total, var_withheld_amount);
          END ENCUMBRANCE_BLOCK;
        ELSE
          SET var_withheld_amount = 0;
        END IF;

        UPDATE person_balances SET balance = balance + var_total - var_withheld_amount WHERE person_id = var_person_id;
      COMMIT;

      FETCH cur1 INTO var_person_id, var_amount_currency, var_total, var_sales_record_masters_ids;

    END WHILE;
    CLOSE cur1;

    UPDATE sales_record_masters SET sales_record_masters.status = 'posted' WHERE sales_record_masters.status = 'processing';

  END IF;
END;;
DELIMITER ;

```
Here is a breakdown of the records created and/or updated by the stored procedure:

* We create a `person_intake` record using data from `sales_record_masters`. We pull the person ID, amount, created_at, etc from the sales_record_masters that are in a state of "new" or "processing" and create a person_intake for each such sales_record_master.

```
 INSERT INTO person_intakes (person_id, amount, created_at, reporting_month_id_temp, currency) VALUES (var_person_id, var_total, now(), 0, var_amount_currency);
        SELECT LAST_INSERT_ID() INTO var_last_id;
```

* Then, we update the sales_record_masters records to have a person intake ID of the newly created intake:

```
UPDATE sales_record_masters
        STRAIGHT_JOIN sales_records ON sales_records.sales_record_master_id = sales_record_masters.id
        SET person_intake_id = var_last_id
        WHERE person_id = var_person_id
        AND sales_record_masters.status = 'processing';
```

* Then, we create a person_intake_sales_records record in this join table:

```
SET @person_intake_sql = CONCAT('INSERT INTO person_intake_sales_record_masters VALUES ( pi_id,', var_sales_record_masters_ids, ');');
        SET @person_intake_sql = REPLACE(@person_intake_sql, 'pi_id', var_last_id);
        PREPARE stmt FROM @person_intake_sql;
        EXECUTE stmt;
```

* Create a person transaction. This might be a debit or a credit depending on whether or not the amount pulled from the sales_record_master record is negative or postive:

```
INSERT INTO person_transactions (created_at, person_id, debit, credit, previous_balance, target_id, target_type, currency) VALUES (NOW(), var_person_id, IF(var_total < 0, abs(var_total), 0), IF(var_total >= 0, var_total, 0), var_old_balance, var_last_id, 'PersonIntake', var_amount_currency);
```

* If the amount is NOT negative, then calculate encumbrances:

```
IF var_total > 0 THEN
          ENCUMBRANCE_BLOCK: BEGIN
            CALL withhold_encumbrances(var_person_id, var_old_balance, var_total, var_withheld_amount);
          END ENCUMBRANCE_BLOCK;
        ELSE
          SET var_withheld_amount = 0;
        END IF;
```

* This kicks off another stored procedure:

```
# withold_encumbrances.sql

DROP PROCEDURE IF EXISTS withhold_encumbrances;
DELIMITER ;;
CREATE PROCEDURE withhold_encumbrances(
  IN in_person_id INTEGER,
  IN in_old_balance DECIMAL(23,14),
  IN in_posting_amount DECIMAL(23,14),
  OUT out_withheld_amount DECIMAL(12,2)
)
BEGIN
  DECLARE complete BOOLEAN DEFAULT 0;
  DECLARE var_encumbrance_id, var_last_detail_id INTEGER;
  DECLARE var_reference_source, var_currency VARCHAR(255);
  DECLARE var_outstanding_amount, var_witholding_percentage, var_max_drawdown, var_remaining_drawdown DECIMAL(23,14);

  DECLARE cur_encumbrances CURSOR FOR
  SELECT id, outstanding_amount, reference_source
  FROM encumbrance_summaries
  WHERE person_id = in_person_id
  AND outstanding_amount > 0
  ORDER BY created_at ASC;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET complete = 1;

  SET out_withheld_amount = 0;

  SELECT currency
  INTO var_currency
  FROM person_balances
  WHERE person_id = in_person_id;

  OPEN cur_encumbrances;
  FETCH cur_encumbrances INTO var_encumbrance_id, var_outstanding_amount, var_reference_source;

  WHILE (!complete) DO

    SELECT  CASE WHEN option_value IS NULL
            THEN 0
            ELSE CONVERT(option_value, SIGNED INTEGER) / 100
            END
    INTO    var_witholding_percentage
    FROM    encumbrance_options
    WHERE   option_name = 'WITHHOLDING_MAX_PERCENTAGE'
    AND     reference_source = var_reference_source;

    SET var_max_drawdown = TRUNCATE(in_posting_amount * var_witholding_percentage, 2);

    IF var_remaining_drawdown IS NULL THEN
      SET var_remaining_drawdown = var_max_drawdown;
    END IF;

    IF var_outstanding_amount >= var_remaining_drawdown THEN
      INSERT INTO encumbrance_details (
        encumbrance_summary_id,
        transaction_date,
        transaction_amount,
        created_at,
        updated_at
      ) VALUES (
        var_encumbrance_id,
        CURDATE(),
        -1 * var_remaining_drawdown,
        CURRENT_TIMESTAMP,
        CURRENT_TIMESTAMP
      );

      SELECT LAST_INSERT_ID() INTO var_last_detail_id;

      UPDATE encumbrance_summaries
      SET outstanding_amount = outstanding_amount - var_remaining_drawdown,
          updated_at = CURRENT_TIMESTAMP
      WHERE id = var_encumbrance_id;

      INSERT INTO person_transactions (
        created_at,
        person_id,
        debit,
        credit,
        currency,
        previous_balance,
        target_id,
        target_type,
        comment
      ) VALUES (
        CURRENT_TIMESTAMP,
        in_person_id,
        var_remaining_drawdown,
        0,
        var_currency,
        in_posting_amount + in_old_balance,
        var_last_detail_id,
        'EncumbranceDetail',
        'TuneCore Direct Advance Recoupment'
      );

      SET out_withheld_amount = out_withheld_amount + var_remaining_drawdown;
      SET var_remaining_drawdown = 0;
      SET complete = 1;
    ELSE
      INSERT INTO encumbrance_details (
        encumbrance_summary_id,
        transaction_date,
        transaction_amount,
        created_at,
        updated_at
      ) VALUES (
        var_encumbrance_id,
        CURDATE(),
        -1 * var_outstanding_amount,
        CURRENT_TIMESTAMP,
        CURRENT_TIMESTAMP
      );

      SELECT LAST_INSERT_ID() INTO var_last_detail_id;

      UPDATE encumbrance_summaries
      SET outstanding_amount = 0,
          updated_at = CURRENT_TIMESTAMP
      WHERE id = var_encumbrance_id;

      INSERT INTO person_transactions (
        created_at,
        person_id,
        debit,
        credit,
        currency,
        previous_balance,
        target_id,
        target_type,
        comment
      ) VALUES (
        CURRENT_TIMESTAMP,
        in_person_id,
        var_outstanding_amount,
        0,
        var_currency,
        in_posting_amount + in_old_balance,
        var_last_detail_id,
        'EncumbranceDetail',
        'TuneCore Direct Advance Recoupment'
      );

      SET out_withheld_amount = out_withheld_amount + var_outstanding_amount;
      SET var_remaining_drawdown = var_remaining_drawdown - var_outstanding_amount;
    END IF;

    FETCH cur_encumbrances INTO var_encumbrance_id, var_outstanding_amount, var_reference_source;
  END WHILE;

  CLOSE cur_encumbrances;

  SELECT out_withheld_amount;
END;;
DELIMITER ;

```

* This stored procedure takes in the person_id, the person's current balance (_before_ the posting amount has been paid to them), and the amount of money that will be paid to them from the posting.

* It selects encumbrance options for a given person where the encumbrances option's source is person's reference source from encumbrance summaries.
* Calculate the withholding percentage

```
(option_value, SIGNED INTEGER) / 100
```

* Calculate max drawdown:

```
SET var_max_drawdown = TRUNCATE(in_posting_amount * var_witholding_percentage, 2);
```

* If outstanding amount from person's encumbrance summary record is greater than remaning drawdow (set initially to `var_max_drawdown`, then we will calculate a percentage of the posting amount to withold from you.

* Insert into encumbrance_details:

```
IF var_outstanding_amount >= var_remaining_drawdown THEN
  INSERT INTO encumbrance_details (
    encumbrance_summary_id,
    transaction_date,
    transaction_amount,
    created_at,
    updated_at
  ) VALUES (
    var_encumbrance_id,
    CURDATE(),
    -1 * var_remaining_drawdown,
    CURRENT_TIMESTAMP,
    CURRENT_TIMESTAMP
  );
```

* Then, update the assocaited ecumbrance summary record's `outstanding_amount` minus the amount that was witheld, i.e. `-1 * var_remaining_drawdown` where `var_remaining_drawdown` was calculated:

```
SET var_max_drawdown = TRUNCATE(in_posting_amount * var_witholding_percentage, 2);
```

```
UPDATE encumbrance_summaries
  SET outstanding_amount = outstanding_amount - var_remaining_drawdown,
      updated_at = CURRENT_TIMESTAMP
  WHERE id = var_encumbrance_id;
```

* Then, create a person transaction record representing this encumbrance witholding:

```
INSERT INTO person_transactions (
        created_at,
        person_id,
        debit,
        credit,
        currency,
        previous_balance,
        target_id,
        target_type,
        comment
      ) VALUES (
        CURRENT_TIMESTAMP,
        in_person_id,
        var_remaining_drawdown,
        0,
        var_currency,
        in_posting_amount + in_old_balance,
        var_last_detail_id,
        'EncumbranceDetail',
        'TuneCore Direct Advance Recoupment'
      );

```

* If it is NOT true that `var_outstanding_amount >= var_remaining_drawdown`, then will will pay off your total outstanding amount out of this posting amount. Insert into encumbrance details and update the associated encumbrance summary to have an outstanding amount of `0`

```
NSERT INTO encumbrance_details (
        encumbrance_summary_id,
        transaction_date,
        transaction_amount,
        created_at,
        updated_at
      ) VALUES (
        var_encumbrance_id,
        CURDATE(),
        -1 * var_outstanding_amount,
        CURRENT_TIMESTAMP,
        CURRENT_TIMESTAMP
      );

      SELECT LAST_INSERT_ID() INTO var_last_detail_id;

      UPDATE encumbrance_summaries
      SET outstanding_amount = 0,
          updated_at = CURRENT_TIMESTAMP
      WHERE id = var_encumbrance_id;
```

* Then, create a person transaction with a credit of that full outstandinga mount:

```
INSERT INTO person_transactions (
        created_at,
        person_id,
        debit,
        credit,
        currency,
        previous_balance,
        target_id,
        target_type,
        comment
      ) VALUES (
        CURRENT_TIMESTAMP,
        in_person_id,
        var_outstanding_amount,
        0,
        var_currency,
        in_posting_amount + in_old_balance,
        var_last_detail_id,
        'EncumbranceDetail',
        'TuneCore Direct Advance Recoupment'
      );

```

* Then, FINALLY, back in `create_person_intake.sql` procdure, update the person's balance with their posting amount (from sales_record_masters) _minus_ the witheld amount that was return from the `withold_encumbrances` procedure.




