DROP PROCEDURE IF EXISTS create_person_intake;;
DELIMITER ;;
CREATE PROCEDURE create_person_intake()
BEGIN
  DECLARE done, changed_status BOOLEAN DEFAULT 0;
  DECLARE var_person_id, var_balance_id, var_last_id, var_updated_ids int;
  DECLARE var_total, var_old_balance, var_streaming_total DECIMAL(23,14);
  DECLARE var_sales_record_masters_ids text;
  DECLARE var_amount_currency VARCHAR(3);
  DECLARE var_withheld_amount DECIMAL(12,2);

  /* Get people that have money in any new or processing sales_record_masters that haven't had a person_intake created for any of those new or processing sales_record_masters */
  DECLARE cur1 CURSOR FOR
  SELECT sr.person_id, srm.amount_currency, SUM(sr.amount) AS total, SUM(CASE WHEN sr.distribution_type = 'Streaming' THEN sr.amount ELSE 0 END) as streaming_total, GROUP_CONCAT(DISTINCT srm.id SEPARATOR '),( pi_id, ') AS sales_record_masters_ids
  FROM sales_record_masters srm
  STRAIGHT_JOIN sales_records sr ON sr.sales_record_master_id = srm.id
  LEFT OUTER JOIN (
    SELECT person_id
    FROM sales_record_masters srm
    STRAIGHT_JOIN person_intake_sales_record_masters pisrm ON srm.id = pisrm.sales_record_master_id
    INNER JOIN person_intakes pi ON pi.id = pisrm.person_intake_id
    WHERE srm.status IN ('new', 'processing')
    GROUP BY person_id ) processed_people ON processed_people.person_id = sr.person_id
  WHERE srm.status IN ('new','processing')
  AND processed_people.person_id is null
  GROUP BY person_id, srm.amount_currency;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

  /* Since we add text to the group concat up the limit */
  SET SESSION group_concat_max_len = 1000000;

  SELECT count(*) INTO var_updated_ids FROM sales_record_masters WHERE STATUS IN ('new', 'processing');

  if(var_updated_ids > 0) then

    OPEN cur1;
    FETCH cur1 INTO var_person_id, var_amount_currency, var_total, var_streaming_total, var_sales_record_masters_ids;

    WHILE( !done ) DO
      IF NOT changed_status THEN
        UPDATE sales_record_masters SET sales_record_masters.status = 'processing' WHERE sales_record_masters.status = 'new';
        SET changed_status = 1;
      END IF;

      START TRANSACTION;
        SET var_last_id = 0;

        /* Create a person intake for the total amount gained/lost */
        INSERT INTO person_intakes (person_id, amount, created_at, reporting_month_id_temp, currency) VALUES (var_person_id, var_total, now(), 0, var_amount_currency);
        SELECT LAST_INSERT_ID() INTO var_last_id;

        /* Update sales records with the person_intake_id */
        UPDATE sales_record_masters
        STRAIGHT_JOIN sales_records ON sales_records.sales_record_master_id = sales_record_masters.id
        SET person_intake_id = var_last_id
        WHERE person_id = var_person_id
        AND sales_record_masters.status = 'processing';

        /* Create a statment to insert into person_intake_sales_record_masters with the unique sales_record_masters, replace pi_id with the new person_intake_id */
        SET @person_intake_sql = CONCAT('INSERT INTO person_intake_sales_record_masters VALUES ( pi_id,', var_sales_record_masters_ids, ');');
        SET @person_intake_sql = REPLACE(@person_intake_sql, 'pi_id', var_last_id);
        PREPARE stmt FROM @person_intake_sql;
        EXECUTE stmt;

        /* Select old balance */
        SELECT id, balance INTO var_balance_id, var_old_balance FROM person_balances WHERE person_id = var_person_id FOR UPDATE;

        /* Create person transactions and update the persons balance */
        INSERT INTO person_transactions (created_at, person_id, debit, credit, previous_balance, target_id, target_type, currency) VALUES (NOW(), var_person_id, IF(var_total < 0, abs(var_total), 0), IF(var_total >= 0, var_total, 0), var_old_balance, var_last_id, 'PersonIntake', var_amount_currency);


      /* Create taxable_earnings for streaming other than youtube */
        INSERT INTO taxable_earnings (revenue_source, credit, currency, person_id, created_at) VALUES ('streaming_royalties', var_streaming_total, var_amount_currency, var_person_id, NOW());

        IF var_total > 0 THEN
          ENCUMBRANCE_BLOCK: BEGIN
            CALL withhold_encumbrances(var_person_id, var_old_balance, var_total, var_withheld_amount);
          END ENCUMBRANCE_BLOCK;
        ELSE
          SET var_withheld_amount = 0;
        END IF;

        UPDATE person_balances SET balance = balance + var_total - var_withheld_amount WHERE person_id = var_person_id;
      COMMIT;

      FETCH cur1 INTO var_person_id, var_amount_currency, var_total, var_streaming_total, var_sales_record_masters_ids;

    END WHILE;
    CLOSE cur1;

    UPDATE sales_record_masters SET sales_record_masters.status = 'posted' WHERE sales_record_masters.status = 'processing';

  END IF;
END;;
DELIMITER ;
