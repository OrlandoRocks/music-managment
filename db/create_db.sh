#!/bin/sh

function create {
  dropdb   --username=tunecore tunecore_$1
  createdb --username=tunecore --encoding UNICODE tunecore_$1
}

if [[ "$1" == "dev"  || "$1" == "test" || "$1" == "prod" ]]; then
  create $1 $2
  exit
fi

if [ "$1" == "all" ]; then
  create dev  $2
  create test $2
  create prod $2
  exit
fi

echo "usage: $0 dev|test|prod|all"
