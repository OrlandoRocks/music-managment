-- MySQL dump 10.14  Distrib 5.5.33a-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: trends_production
-- ------------------------------------------------------
-- Server version	5.5.33a-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) DEFAULT NULL,
  `code` varchar(5) DEFAULT NULL,
  `spotify_active` tinyint(1) DEFAULT NULL,
  `amazon_active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `geo`
--

DROP TABLE IF EXISTS `geo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `geo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trend_data_summary_id` int(11) NOT NULL,
  `country_code` varchar(2) NOT NULL,
  `cbsa` char(5) NOT NULL DEFAULT '0',
  `qty` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_date_summary_id_country_cbsa` (`date`,`trend_data_summary_id`,`country_code`,`cbsa`),
  KEY `index_geo_on_trend_data_summary_id` (`trend_data_summary_id`),
  KEY `index_geo_on_country_code` (`country_code`),
  KEY `index_geo_on_cbsa` (`cbsa`),
  KEY `index_geo_on_created_at` (`created_at`),
  KEY `index_geo_on_updated_at` (`updated_at`),
  KEY `index_geo_on_date` (`date`)
) ENGINE=InnoDB AUTO_INCREMENT=237585410 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `geo_two`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `geo_two` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `trend_data_summary_id` int(11) NOT NULL,
  `country_code` varchar(2) NOT NULL,
  `cbsa` char(5) NOT NULL DEFAULT '0',
  `qty` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_date_summary_id_country_cbsa` (`date`,`trend_data_summary_id`,`country_code`,`cbsa`),
  KEY `index_geo_on_trend_data_summary_id` (`trend_data_summary_id`),
  KEY `index_geo_on_country_code` (`country_code`),
  KEY `index_geo_on_cbsa` (`cbsa`),
  KEY `index_geo_on_created_at` (`created_at`),
  KEY `index_geo_on_updated_at` (`updated_at`),
  KEY `index_geo_on_date` (`date`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `import_log`
--

DROP TABLE IF EXISTS `import_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `import_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `started_at` datetime NOT NULL,
  `ended_at` datetime DEFAULT NULL,
  `file_name` varchar(255) NOT NULL,
  `period_ending` date DEFAULT NULL,
  `provider_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_filename_provider_id` (`file_name`,`provider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8075 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `last_import`
--

DROP TABLE IF EXISTS `last_import`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `last_import` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_id` int(11) DEFAULT NULL,
  `country_code` varchar(5) DEFAULT NULL,
  `trend_date` date DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_provider_id_country_code` (`provider_id`,`country_code`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `providers`
--

DROP TABLE IF EXISTS `providers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `providers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trans_types`
--

DROP TABLE IF EXISTS `trans_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trans_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_trans_types_on_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trend_data_detail`
--

DROP TABLE IF EXISTS `trend_data_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trend_data_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL DEFAULT '0',
  `sale_date` date NOT NULL DEFAULT '0000-00-00',
  `country_code` varchar(2) NOT NULL,
  `zip_code` varchar(12) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `artist_name` varchar(120) DEFAULT NULL,
  `artist_id` int(11) DEFAULT NULL,
  `album_name` varchar(255) DEFAULT NULL,
  `album_id` int(11) NOT NULL DEFAULT '0',
  `album_type` varchar(20) DEFAULT NULL,
  `song_name` varchar(255) DEFAULT NULL,
  `song_id` int(11) NOT NULL DEFAULT '0',
  `tunecore_isrc` varchar(12) DEFAULT NULL,
  `optional_isrc` varchar(12) DEFAULT NULL,
  `upc` varchar(14) DEFAULT NULL,
  `trans_type_id` int(11) NOT NULL,
  `provider_id` int(11) DEFAULT NULL,
  `royalty_currency_id` varchar(3) DEFAULT NULL,
  `royalty_price` decimal(14,4) DEFAULT NULL,
  `customer_price` decimal(14,4) DEFAULT NULL,
  `is_promo` tinyint(1) DEFAULT NULL,
  `import_log_id` int(11) DEFAULT NULL,
  `trend_data_summary_id` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_person_date_album_song_ttype_provider` (`person_id`,`sale_date`,`country_code`,`zip_code`,`album_id`,`song_id`,`trans_type_id`,`provider_id`),
  KEY `index_trend_data_detail_on_person_id` (`person_id`),
  KEY `index_trend_data_detail_on_sale_date` (`sale_date`),
  KEY `index_trend_data_detail_on_country_code` (`country_code`),
  KEY `index_trend_data_detail_on_artist_name` (`artist_name`),
  KEY `index_trend_data_detail_on_artist_id` (`artist_id`),
  KEY `index_trend_data_detail_on_trans_type_id` (`trans_type_id`),
  KEY `index_trend_data_detail_on_trend_data_summary_id` (`trend_data_summary_id`),
  KEY `index_trend_data_detail_on_zip_code` (`zip_code`)
) ENGINE=InnoDB AUTO_INCREMENT=251642696 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `trend_data_detail_two`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trend_data_detail_two` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL DEFAULT '0',
  `sale_date` date NOT NULL DEFAULT '0000-00-00',
  `country_code` varchar(2) NOT NULL,
  `zip_code` varchar(12) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `artist_name` varchar(120) DEFAULT NULL,
  `artist_id` int(11) DEFAULT NULL,
  `album_name` varchar(255) DEFAULT NULL,
  `album_id` int(11) NOT NULL DEFAULT '0',
  `album_type` varchar(20) DEFAULT NULL,
  `song_name` varchar(255) DEFAULT NULL,
  `song_id` int(11) NOT NULL DEFAULT '0',
  `tunecore_isrc` varchar(12) DEFAULT NULL,
  `optional_isrc` varchar(12) DEFAULT NULL,
  `upc` varchar(14) DEFAULT NULL,
  `trans_type_id` int(11) NOT NULL,
  `provider_id` int(11) DEFAULT NULL,
  `royalty_currency_id` varchar(3) DEFAULT NULL,
  `royalty_price` decimal(14,4) DEFAULT NULL,
  `customer_price` decimal(14,4) DEFAULT NULL,
  `is_promo` tinyint(1) DEFAULT NULL,
  `import_log_id` int(11) DEFAULT NULL,
  `trend_data_summary_id` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_person_date_album_song_ttype_provider` (`person_id`,`sale_date`,`country_code`,`zip_code`,`album_id`,`song_id`,`trans_type_id`,`provider_id`),
  KEY `index_trend_data_detail_on_person_id` (`person_id`),
  KEY `index_trend_data_detail_on_sale_date` (`sale_date`),
  KEY `index_trend_data_detail_on_country_code` (`country_code`),
  KEY `index_trend_data_detail_on_artist_name` (`artist_name`),
  KEY `index_trend_data_detail_on_artist_id` (`artist_id`),
  KEY `index_trend_data_detail_on_trans_type_id` (`trans_type_id`),
  KEY `index_trend_data_detail_on_trend_data_summary_id` (`trend_data_summary_id`),
  KEY `index_trend_data_detail_on_zip_code` (`zip_code`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trend_data_detail_error`
--

DROP TABLE IF EXISTS `trend_data_detail_error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trend_data_detail_error` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `sale_date` date DEFAULT NULL,
  `country_code` varchar(2) DEFAULT NULL,
  `zip_code` varchar(12) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `artist_name` varchar(120) DEFAULT NULL,
  `artist_id` int(11) DEFAULT NULL,
  `album_name` varchar(255) DEFAULT NULL,
  `album_id` int(11) DEFAULT NULL,
  `album_type` varchar(20) DEFAULT NULL,
  `song_name` varchar(255) DEFAULT NULL,
  `song_id` int(11) DEFAULT NULL,
  `tunecore_isrc` varchar(12) DEFAULT NULL,
  `optional_isrc` varchar(12) DEFAULT NULL,
  `upc` varchar(14) DEFAULT NULL,
  `trans_type_id` int(11) DEFAULT NULL,
  `provider_id` int(11) DEFAULT NULL,
  `provider_customer_id` int(11) DEFAULT NULL,
  `store_identifier` varchar(20) DEFAULT NULL,
  `royalty_currency_id` varchar(3) DEFAULT NULL,
  `royalty_price` decimal(14,4) DEFAULT NULL,
  `customer_price` decimal(14,4) DEFAULT NULL,
  `is_promo` tinyint(1) DEFAULT NULL,
  `import_log_id` int(11) DEFAULT NULL,
  `trend_data_summary_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4970876 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trend_data_summary`
--

DROP TABLE IF EXISTS `trend_data_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trend_data_summary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL DEFAULT '0000-00-00',
  `person_id` int(11) DEFAULT NULL,
  `album_id` int(11) NOT NULL DEFAULT '0',
  `song_id` int(11) NOT NULL DEFAULT '0',
  `trans_type_id` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `provider_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_person_date_album_song_ttype_provider` (`person_id`,`date`,`album_id`,`song_id`,`trans_type_id`,`provider_id`),
  KEY `index_trend_data_summary_on_date` (`date`),
  KEY `index_trend_data_summary_on_person_id` (`person_id`),
  KEY `index_trend_data_summary_on_album_id` (`album_id`),
  KEY `index_trend_data_summary_on_song_id` (`song_id`),
  KEY `index_trend_data_summary_on_trans_type_id` (`trans_type_id`),
  KEY `index_trend_data_summary_on_created_at` (`created_at`),
  KEY `index_trend_data_summary_on_updated_at` (`updated_at`),
  KEY `person_id` (`person_id`,`provider_id`,`date`)
) ENGINE=InnoDB AUTO_INCREMENT=87946491 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zipcodes`
--

DROP TABLE IF EXISTS `zipcodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zipcodes` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `zipcode` char(5) NOT NULL COMMENT 'Five digit zip code of the area.',
  `primary_record` char(1) DEFAULT NULL COMMENT 'Indicates if this is a primary zipcode or not.',
  `population` int(10) unsigned DEFAULT NULL COMMENT 'Population of the zip.',
  `households` int(10) unsigned DEFAULT NULL COMMENT 'Number of households of the zipcode',
  `white_population` int(10) unsigned DEFAULT NULL COMMENT 'Estimated population.',
  `black_population` int(10) unsigned DEFAULT NULL COMMENT 'Estimated population.',
  `hispanic_population` int(10) unsigned DEFAULT NULL COMMENT 'Estimated population.',
  `asian_population` int(10) unsigned DEFAULT NULL COMMENT 'Estimated population.',
  `hawaiian_population` int(10) unsigned DEFAULT NULL COMMENT 'Estimated population.',
  `indian_population` int(10) unsigned DEFAULT NULL COMMENT 'Estimated population.',
  `other_population` int(10) unsigned DEFAULT NULL COMMENT 'Estimated population.',
  `male_population` int(10) unsigned DEFAULT NULL COMMENT 'Estimated Male population.',
  `female_population` int(10) unsigned DEFAULT NULL COMMENT 'Estimated Female population.',
  `persons_per_household` decimal(4,2) DEFAULT NULL COMMENT 'Estimated average number of persons per household.',
  `average_house_value` int(10) unsigned DEFAULT NULL COMMENT 'Average house value in County.',
  `income_per_household` int(10) unsigned DEFAULT NULL COMMENT 'Average household income.',
  `median_age` decimal(3,1) DEFAULT NULL COMMENT 'Median age for all people.',
  `median_age_male` decimal(3,1) DEFAULT NULL COMMENT 'Median age for male.',
  `median_age_female` decimal(3,1) DEFAULT NULL COMMENT 'Median age for female.',
  `latitude` decimal(12,6) DEFAULT NULL COMMENT 'Geo coordinate measured in degrees north or south of equator.',
  `longitude` decimal(12,6) DEFAULT NULL COMMENT 'Geo coordinate measured in degrees east or west of the greenwitch meridian',
  `elevation` int(11) DEFAULT NULL COMMENT 'The average elevation of the County.',
  `state` char(2) DEFAULT NULL COMMENT '2 letter state name abbrivation.',
  `state_name` varchar(35) DEFAULT NULL COMMENT 'The full US state name.',
  `city_type` char(1) DEFAULT NULL COMMENT 'Indicates type of locale such as Post Office, Stations or Branch.',
  `city_alias_abbrivation` varchar(13) DEFAULT NULL COMMENT '12 Character abbrevation for the city alias name.',
  `area_code` varchar(55) DEFAULT NULL COMMENT 'Telephone area codes available in this zip code.',
  `city` varchar(35) DEFAULT NULL COMMENT 'Name of the city as designated by the USPS.',
  `city_alias_name` varchar(35) DEFAULT NULL COMMENT 'Alias name of the city if it exist.',
  `county` varchar(45) DEFAULT NULL COMMENT 'Name of County or Parish this zip code resides in.',
  `county_fips` char(5) DEFAULT NULL COMMENT 'FIPS code for the County this zip code resides in.',
  `state_fips` char(2) DEFAULT NULL COMMENT 'FIPS code for the State this zip code resides in.',
  `time_zone` char(2) DEFAULT NULL COMMENT 'Hours past Greenwich Time Zone this ZIP code belongs to.',
  `daylight_savings` char(1) DEFAULT NULL COMMENT 'Flag indicating whether this ZIP Code observes daylight savings.',
  `msa` varchar(35) DEFAULT NULL COMMENT 'Metropolitan Statistical Area number assigned by Census 2000.',
  `pmsa` char(4) DEFAULT NULL COMMENT 'Primary Metropolitan Statistical Area Number.',
  `csa` char(3) DEFAULT NULL COMMENT 'Core Statistical Area. This area is a group of MSA''s combined into a population core area.',
  `cbsa` char(5) DEFAULT NULL COMMENT 'Core Based Statistical Area Number.',
  `cbsa_div` char(5) DEFAULT NULL COMMENT 'Core Based Statistical Area Division.',
  `cbsa_type` char(5) DEFAULT NULL COMMENT 'Core Based Statistical Area Type (Metro or Micro).',
  `cbsa_name` varchar(150) DEFAULT NULL COMMENT 'Core Based Statistical Area Name or Title.',
  `msa_name` varchar(150) DEFAULT NULL COMMENT 'Primary Metropolitan Statistical Area name or Title.',
  `pmsa_name` varchar(150) DEFAULT NULL COMMENT 'Primary Metropolitan Statistical Area name or Title.',
  `region` varchar(10) DEFAULT NULL COMMENT 'A geographic area consisting of several States defined by the U.S. Department of Commerce, Bureau of the Census. The States are grouped into four regions and nine divisions.',
  `division` varchar(20) DEFAULT NULL COMMENT 'A geographic area consisting of several States defined by the U.S. Department of Commerce, Bureau of the Census. The States are grouped into four regions and nine divisions.',
  `mailing_name` char(1) DEFAULT NULL COMMENT 'Yes or No (Y/N) flag indicating whether or not the USPS accepts this City Alias Name for mailing purposes.',
  `number_of_businesses` int(11) DEFAULT NULL COMMENT 'The total number of Business Establishments for this ZIP Code. Data taken from Census for 2006 Business Patterns.',
  `number_of_employees` int(11) DEFAULT NULL COMMENT 'The total number of employees for this ZIP Code. Data taken from Census for 2006 Business Patterns.',
  `business_first_quarter_payroll` int(11) DEFAULT NULL COMMENT 'The total payroll for the frist quarter this ZIP Code in $1000. Data taken from Census for 2006 Business Patterns.',
  `business_annual_payroll` int(11) DEFAULT NULL COMMENT 'The total annual payroll for this ZIP Code in $1000. Data taken from Census for 2006 Business Patterns.',
  `business_employment_flag` char(1) DEFAULT NULL COMMENT 'Due to confidentiality, some areas do not have actual figures for employment information. Employment and payroll data are replaced by zeroes with the Employment Flag denoting employment size class.',
  `growth_rank` int(11) DEFAULT NULL COMMENT 'The rank in which this county is growing according to the US Census.',
  `growth_housing_units_2003` int(11) DEFAULT NULL COMMENT 'The estimated number of housing units in this county as of July 1, 2003.',
  `growth_housing_units_2004` int(11) DEFAULT NULL COMMENT 'The estimated number of housing units in this county as of July 1, 2004. Source: Housing Unit Estimates for the 100 Fastest Growing U.S. Counties between July 1, 2003 and July 1, 2004. Population Division, U.S. Census Bureau.',
  `growth_increase_number` int(11) DEFAULT NULL COMMENT 'The change in housing units from 2003 to 2004, expressed as a number.',
  `growth_increase_percentage` decimal(3,1) DEFAULT NULL COMMENT 'The change in housing units from 2003 to 2004, expressed as a percentage.',
  `cbsa_pop_2003` int(11) DEFAULT NULL COMMENT 'The estimated population for the selected CBSA in 2003.',
  `cbsa_div_pop_2003` int(11) DEFAULT NULL COMMENT 'The estimated population for the selected CBSA Division in 2003.',
  `congressional_district` varchar(150) DEFAULT NULL COMMENT 'This field shows which Congressional Districts the ZIP Code belongs to. Currently set for 111th Congress.',
  `congressional_land_area` varchar(150) DEFAULT NULL COMMENT 'This field provides the approximate land area covered by the Congressional District for which the ZIP Code belongs to.',
  `delivery_residential` int(11) DEFAULT NULL COMMENT 'The number of active residential delivery mailboxes and centralized units for this ZIP Code. This excludes PO Boxes and all other contract box types.',
  `delivery_business` int(11) DEFAULT NULL COMMENT 'The number of active business delivery mailboxes and centralized units for this ZIP Code. This excludes PO Boxes and all other contract box types.',
  `delivery_total` int(11) DEFAULT NULL COMMENT 'The total number of delivery receptacles for this ZIP Code. This includes curb side mailboxes, centralized units, PO Boxes, NDCBU, and contract boxes.',
  `preferred_last_line_key` varchar(10) DEFAULT NULL COMMENT 'Links this record with other products ZIP-Codes.com offers.',
  `classification_code` char(1) DEFAULT NULL COMMENT 'The classification type of this ZIP Code.',
  `multi_county` char(1) DEFAULT NULL COMMENT 'Flag indicating whether this ZIP Code crosses county lines.',
  `csa_name` varchar(255) DEFAULT NULL COMMENT 'The name of the CSA this ZIP Code is in.',
  `csa_div_name` varchar(255) DEFAULT NULL COMMENT 'The name of the CBSA Division this ZIP Code is in.',
  `city_state_key` char(6) DEFAULT NULL COMMENT 'Links this record with other products ZIP-Codes.com offers such as the ZIP+4.',
  `population_estimate` int(11) DEFAULT NULL COMMENT 'An up to the month population estimate for this ZIP Code.',
  `land_area` decimal(12,6) DEFAULT NULL COMMENT 'The land area of this ZIP Code in Square Miles.',
  `water_area` decimal(12,6) DEFAULT NULL COMMENT 'The water area of this ZIP Code in Square Miles.',
  `city_alias_code` varchar(5) DEFAULT NULL COMMENT 'Code indication the type of the city alias name for this record. Record can be Abbreviations, Universities, Government, and more.',
  `city_mixed_case` varchar(35) DEFAULT NULL COMMENT 'The city name in mixed case (i.e. Not in all uppercase letters).',
  `city_alias_mixed_case` varchar(35) DEFAULT NULL COMMENT 'The city alias name in mixed case (i.e. Not in all uppercase letters).',
  `box_count` int(11) DEFAULT NULL COMMENT 'Total count of PO Box deliveries in this ZIP Code ',
  `sfdu` int(11) DEFAULT NULL COMMENT 'Total count of single family deliveries in this ZIP Code; generally analogous to homes',
  `mfdu` int(11) DEFAULT NULL COMMENT 'Total count of multifamily deliveries in this ZIP Code; generally analogous to apartments',
  `cbsa_short_name` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `zipcode` (`zipcode`,`primary_record`),
  KEY `state_city` (`state`,`city`),
  KEY `index_zipcodes_on_cbsa` (`cbsa`)
) ENGINE=InnoDB AUTO_INCREMENT=80247 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-10-02 13:12:06
