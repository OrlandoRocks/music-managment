truncate table 1_delayed_jobs;
truncate table 2_delayed_jobs;
truncate table 3_delayed_jobs;
truncate table 4_delayed_jobs;
truncate table 5_delayed_jobs;
truncate table artist_revenue_reports;
truncate table sales_records_new;
truncate table sales_records_new_index;
truncate table sessions;
truncate table versions;

drop table aa;
drop table delayed_jobs;
drop table my_lazy_jobs;
drop table my_lazy_jobs_1;
drop table my_lazy_jobs_2;
drop table my_lazy_jobs_3;
drop table stored_credit_cards_20150923;
drop table youtube_compositions_tmp;

update braintree_transactions set raw_response = '{}';
update braintree_vault_transactions set raw_response = '{}';
update composers set address_1 = '1313 Webfoot Walk', address_2 = null, phone = '7078675309', email = concat('composer_', id,'@tunecore.com');
update external_assets set url = 'http://youcannotseeme.com';
update fans set name = 'A Fan', email = concat('fan_', id, '@tunecore.com'), gender = mod(id, 2), birthday = '1890-09-15';
update friend_referral_events set raw_request = '{}', raw_response = '{}';
update friend_referrals set raw_response = '{}';
update lods set raw_request = '{}', raw_response = '{}';

update manufacturing_orders set customer_name = 'Irwin Fletcher', address = '1313 Webfoot Walk', address2 = null, phone = '7078675309', email = concat('mnfctr_', id, '@tunecore.com'), shipping_name = 'Irwin Fletcher', shipping_address = '1313 Webfoot Walk', shipping_address2 = null;
update newsletter_subscribers set address = concat('newsletter_',id,'@tunecore.com');
update oauth_providers_people set access_token = 'token', access_token_secret = 'secret';
update oauth_tokens set token = concat('token_', id), secret = 'secret';
update paypal_transactions set email = concat('paypal_txn_',id,'@tunecore.com');
update paypal_transfers set paypal_address = concat('paypal_transfer_',id,'@tunecore.com');
update people set email = concat('tc_', id, '@tunecore.com'), password = '9db0add18faff06e364e52050f3a70c04fed5068bafaf3a30adceb6b42b54ac2', salt = '502b8b73e274930ac884dd140928e059f454cd2895b33ee363faf951d8e60b91', invite_code = '12345', phone_number = '7078675309', last_logged_in_ip = '0.0.0.0', password_reset_tmsp = '2015-12-21 13:52:10' where email not in ('agulati@tunecore.com', 'troy@tunecore.com', 'zach@tunecore.com', 'civ2boss@gmail.com', 'robin@tunecore.com');

update shipping_labels set address1 = '1313 Webfoot Walk', address2 = null, tracking_number = id;
update stored_bank_accounts set customer_vault_id = 123, bank_name = 'Iron Bank of Braavos', last_four_routing = '1334', last_four_account = '0002', name = 'Irwin Fletcher', address1 = '1313 Webfoot Walk', address2 = null, phone = '7078675309';
update sync_membership_requests set email = concat('sync_',id,'@tunecore.com');
update tax_info set name = 'Irwin Fletcher', address_1 = '1313 Webfoot Walk', address_2 = null, salt = 'UMv+zVt52d7m6WoyewwKwQ==', encrypted_tax_id = 'Vub+cAK8PoEbYxQOfws3ug==', dob = '1890-09-15';
update client_applications set url = 'http://portal-int.com', callback_url = 'http://portal-int.com/users/auth/tunecore/callback', `key` = 'akey', secret = 'asecret' where name = 'content_review';
