# TuneCore Platform

[![CircleCI master](https://img.shields.io/circleci/build/gh/tunecore/tc-www/master.svg?label=master&token=177db532d200e5517224c6b9880c4d4be827d06f)](https://circleci.com/gh/tunecore/tc-www)
[![CircleCI integration](https://img.shields.io/circleci/build/gh/tunecore/tc-www/integration.svg?label=integration&token=177db532d200e5517224c6b9880c4d4be827d06f)](https://circleci.com/gh/tunecore/tc-www/tree/integration)

Setup instructions for this app are located [here].

[here]: https://tunecore.atlassian.net/l/cp/xe7J1uyT
