# script/runner -e production RAILS_ROOT/script/composition_switcher.rb SONG_COMP_UPDATES=path/to/file
# SONG_COMP_UPDATES should be a csv file with no header line and format [song id with composition id to be changed], [current composition id in song record], [new composition id for song record] e.g. 312, 38, 22
puts "Starting script"
f = open(ENV['SONG_COMP_UPDATES'])
a = f.readlines
a.each do |line|
  begin
    line_array = line.split(",")
    song_id = line_array[0].to_i
    old_composition_id = line_array[1].to_i
    new_composition_id = line_array[2].to_i
    song = Song.find(song_id)
    case song.composition_id
      when old_composition_id
        song.composition_id = new_composition_id
        song.save
        puts "Song #{song_id} changed from composition_id #{old_composition_id} to #{new_composition_id}"
      when new_composition_id
        puts "Song #{song_id} already has composition_id #{old_composition_id}"
      else
        puts "Song #{song_id} has composition_id #{song.composition_id}, not #{old_composition_id} or #{new_composition_id}"
    end
  rescue
    puts "Couldn't find song id #{song_id}"
  end
end
puts "Script complete"
