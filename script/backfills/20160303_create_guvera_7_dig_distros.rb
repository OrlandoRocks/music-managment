# this code is to fix an issue uncovered where
# GuveraSevenDigital releases were getting associated
# as salepoints under Guvera rather than their own
# distribution when the add_new_salepoint rake task is run for store 50/57.

salepoints = Salepoint.joins(:distributions).
               where(
                 Distribution.arel_table[:converter_class].eq("MusicStores::Omnifone::Converter").
                 and(Salepoint.arel_table[:store_id].eq(57))
               )

salepoints.each do |salepoint|
  last_distro  = salepoint.distributions.order("updated_at desc").limit(1).last
  new_distro = last_distro.dup
  new_distro.update(
    converter_class: "MusicStores::GuveraSevenDigital::Converter",
    delivery_type: last_distro.last_delivery_type || "full_delivery",
    retry_count: 0,
    state: 'new',
  )
  new_distro.salepoints << salepoint
  last_distro.salepoints.delete(salepoint)
end
