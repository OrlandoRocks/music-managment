#!/bin/bash

CERT_DIR=$1
ENV=$2

if [ "$ENV" == "development" ] || [ "$ENV" == "test" ]; then
  exit 0
fi

mkdir -p "$CERT_DIR"

echo "Pulling v2 jwt signing keys"
aws s3 cp \
  s3://secrets.tunecore.com/"$ENV"/tc-www/certs "$CERT_DIR" \
  --recursive --exclude "*" --include "v2_jwt*"
