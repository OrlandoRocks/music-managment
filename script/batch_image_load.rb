def start
	# Load your yml config from rails
	db_config = YAML::load(File.open("../config/database.yml"))
	ActiveRecord::Base.establish_connection(db_config[ENV['rails_env'] || 'development'])
  d = Dir.new("../public/images/artwork/templates/tiffs")
  d.each  { |file|
	       if file != "." && file != ".." && file != ".svn" && file != "newalbum.gif"
						 artwork = Artwork.new
						 artwork.uploaded = 0
						 artwork.file = file
						 artwork.save
				end
	}
end

start

