#!/bin/bash


HOSTNAME=`hostname -s`

cp /var/log/tunecore_access.log /srv/logs/$HOSTNAME/apache/
cp /dev/null /var/log/tunecore_access.log

cp /var/log/tunecore_error.log /srv/logs/$HOSTNAME/apache/
cp /dev/null /var/log/tunecore_error.log

cp /home/tunecore/current/log/production.log /srv/logs/$HOSTNAME/mongrel/
cp /dev/null /home/tunecore/current/log/production.log
