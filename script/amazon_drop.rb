require 'rubygems'
require 'net/sftp'
require 'net/sftp/operations/errors'
require 'net/ssh/version'
require 'fileutils'
require 'builder'


class Connection
  def initialize(options = {})
    @host     = options[:host]      or raise "No hostname"
    @username = options[:username]  or raise "No username."
    @keyfile  = options[:keyfile]   or raise "No keyfile."
    @port     = options[:port]      || 22
    @paranoid = options[:paranoid]  || false
    @password = options[:password]
  end

  def connect(&block)
    #forking here, because net-ssh.put_file deadlocks on rehdat.... not necessary OSX, BSD, ubuntu.
    pid = Process.fork do
      Net::SFTP.start(@host, @username, @password, :keys => [@keyfile], :port => @port, :paranoid => @paranoid) do |sftp|
        block.call(sftp)
      end
    end

    pid, status = Process.waitpid2(pid)
    raise Error.new("ssh process failed.") unless status.exitstatus == 0
  end
end

def raise_on_fail(message)
  begin
    yield
  rescue StandardError => e
    raise Error.new("#{message}: #{e.message}")
  end
end

def mkdir_p(dirname,sftp)
  raise_on_fail("Could not create directory #{dirname}") do
    path = dirname.split("/")
    path.each_with_index do |element, idx|
      next if element == ''
      path_to_this_element = path.slice(0..idx).join("/")
      if !fexist?(path_to_this_element,sftp)
        sftp.mkdir(path_to_this_element, {})
      end
    end
  end
end

def fexist?(filename,sftp)
  begin
    sftp.stat(filename)
  rescue Net::SFTP::Operations::StatusException => e
    if e.code == Net::SFTP::Operations::Constants::FX_NO_SUCH_FILE
      return false
    else
      raise e
    end
  end
end

def find_completed_packages(source,logger,packages)
  begin
    @amazon_server1 = Connection.new(:host=>'data.amazon-digital-ftp.com', :username=>'tunecore', :password=>'e/3jN0yDkq', :keyfile => File.expand_path("~/.ssh/id_dsa"));
    @amazon_server1.connect do |sftp1|
      bdir=sftp1.opendir(source)
      bundles = sftp1.readdir(bdir)
      bundles.each do |bundle|
        if not (bundle.filename.eql?(".") || bundle.filename.eql?(".."))
          upc = bundle.filename
          begin
            meta = sftp1.open("#{source}/#{upc}/#{upc}.xml")
            sftp1.close_handle(meta)
            logger.debug "package - #{upc} is completed."
            packages << "#{upc}\n"
          rescue
            logger.debug "package - #{upc} NOT completed."
          end
        end
      end
    end
  rescue Exception => e
    logger.debug "errored while reading packages - #{e}."
  end
end


def batch_packages(feed_source,source,package,logger)
#  @amazon_server3 = Connection.new(:host=>'69.28.168.154', :username=>'tunecore', :password=>'8kwFdeB3p', :keyfile => File.expand_path("~/.ssh/id_dsa"));
@amazon_server2 = Connection.new(:host=>'data.amazon-digital-ftp.com', :username=>'tunecore', :password=>'e/3jN0yDkq', :keyfile => File.expand_path("~/.ssh/id_dsa"));
  @amazon_server2.connect do |sftp2|
    logger.info "moving batching #{source}/#{package} to #{feed_source}."
    mkdir_p(feed_source,sftp2)
    sftp2.rename("#{source}/#{package}", "#{feed_source}/#{package}")
    logger.info "complted batching #{source}/#{package} to #{feed_source}."
  end
end

def create_feed_manifest(feed_source,feed_name,packages,logger)
  logger.debug "starting to build the manifest for #{feed_name}."
  manifest_file=File.join("/tmp/", "#{feed_name}_manifest.xml")
  File.open(manifest_file, "w") do |f|
    xml = ::Builder::XmlMarkup.new(:target => f, :indent => 2 )
    xml.Feed  do |f|
      f.FeedId feed_name
      f.BundleCount packages.size
      packages.each do |upc|
        f.AlbumBundle do |album|
          album.DirectoryName upc
        end
      end
    end
  end
  logger.debug "created the manifest file."
  logger.debug "uploading the manifest file."
  #@amazon_server4 = Connection.new(:host=>'69.28.168.154', :username=>'tunecore', :password=>'8kwFdeB3p', :keyfile => File.expand_path("~/.ssh/id_dsa"));
  @amazon_server3 = Connection.new(:host=>'data.amazon-digital-ftp.com', :username=>'tunecore', :password=>'e/3jN0yDkq', :keyfile => File.expand_path("~/.ssh/id_dsa"));
  @amazon_server3.connect do |sftp3|
    remote_manifest_file = File.join("#{feed_source}/", "manifest.xml")
    sftp3.put_file(manifest_file, remote_manifest_file)
  end
end

def mv_feed(feed_source,destination,feed_name,logger)
  #@amazon_server4 = Connection.new(:host=>'69.28.168.154', :username=>'tunecore', :password=>'8kwFdeB3p', :keyfile => File.expand_path("~/.ssh/id_dsa"));
  @amazon_server4 = Connection.new(:host=>'data.amazon-digital-ftp.com', :username=>'tunecore', :password=>'e/3jN0yDkq', :keyfile => File.expand_path("~/.ssh/id_dsa"));
  @amazon_server4.connect do |sftp4|
    logger.info "moving feed #{feed_source} to #{destination}"
    sftp4.rename("#{feed_source}", "#{destination}/#{feed_name}")
    logger.info "complted moving feed #{feed_source} to #{destination}"
  end
end

def init()
  source = "/music4/tunecore/drop/"
  destination = "/music4/tunecore/"
  shipped = "/home/petri/shipped/amazon/"+Time.new.strftime('%Y_%m_%d')
  batch_size=50
  FileUtils.mkdir_p(shipped)
  log_file = "/home/petri/plan_b/logs/amazon_drop.log"
  logger = Logger.new(log_file,shift_age = 7)
  logger.level = Logger::DEBUG
  #get completed packages
  packages = File.open("/tmp/amazon_drop_packages", "a")
  begin
  find_completed_packages(source,logger,packages)
  file = File.new("/tmp/amazon_drop_packages", "r")
  upcs = Array.new
  while (line = file.gets)
    upcs << line.gsub(/\n/,"")
  end
  completed_packages = upcs.uniq
  logger.info("total completed packages on the server: "+completed_packages.size.to_s)
  count = 0
  logger.info "Total packages to be batch - #{completed_packages.size.to_s} - #{batch_size.to_s} packages will be batch into a feed in this round."
  batched_packages=Array.new
  if completed_packages.size >= batch_size
    logger.info "Total packages to be batch - #{completed_packages.size.to_s} > #{batch_size.to_s} so starting batching."
    feed_name = "TUCI_"+Time.new.strftime('%Y.%m.%d.%H.%M')
    feed_source = source+feed_name
    completed_packages.each do |package|
      if count < batch_size
        batched_packages << package
        icount = count+1
        logger.info "Batching #{package} - #{icount} of  #{batch_size.to_s}."
        batch_packages(feed_source,source,package,logger)
      end
      count = count+1
    end
    create_feed_manifest(feed_source,feed_name,batched_packages,logger)
    mv_feed(feed_source,destination,feed_name,logger)
    FileUtils.mv("/tmp/#{feed_name}_manifest.xml","#{shipped}/")
  else
    logger.info "Total packages to be batch - #{completed_packages.size.to_s} < #{batch_size.to_s} therefore exiting without batching."
  end
  rescue StandardError => e
     logger.fatal "errored aborting the session. #{e}"
  end
  FileUtils.mv("/tmp/amazon_drop_packages","/tmp/amazon_drop_packages.last")
end

init()
