#!/bin/bash

SIDEKIQ_CONFIG=$1

ps -ef | grep sidekiq | grep -v grep |grep -v $0
ecode=$?

if [[ $ecode != 0 ]]; then
  echo -n `date`
  echo " restarting"
  cd /var/www/tunecore/current
  RAILS_ENV=production bundle exec sidekiq -d -C $SIDEKIQ_CONFIG
  echo "restarted at `date`" | mail -s "ALERT: restarted sidekiq on `hostname`" techteam@tunecore.com
else
  echo -n `date`
  echo " not restarting"
fi
