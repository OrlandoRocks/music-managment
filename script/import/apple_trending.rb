#!/usr/bin/env ruby

RAILS_ENV = ENV["RAILS_ENV"] || "production"

unless ARGV[0] and File.exists? ARGV[0]
  puts "missing filename"
  exit
end

#unless ARGV[1]
#  puts "missing date"
#  exit
#end

require 'rubygems'
require File.dirname(__FILE__) + '/../../config/environment'

include Trending::Processor
filename = File.expand_path(ARGV[0])
# strip the date out of the filename
str = Date.parse(filename[-12,8]).strftime("%Y-%m-%d")
date = Date.parse(str)
# date = Date.parse(ARGV[1])
ingest_trend_report!(ARGV[0], date)


#          query = ["SELECT person_id, created_at AS 'stop_on',  SUM(units) as 'number_of_records'
#              FROM itunes_user_trends AS iut WHERE iut.created_at = '#{date}' GROUP BY person_id;"]

#      trends = ItunesUserTrends.find_by_sql(query)

#      trends.each do |t|
#        trend_report = ItunesUserReport.new(:stop_on => t.stop_on,
#                                          :start_on => t.stop_on.to_date - 6,
#                                          :person_id => t.person_id,
#                                          :number_of_records => t.number_of_records,
#                                          :created_at => Time.now)
#        trend_report.save!
#      end


#  ItunesUserTrends.connection.execute(%Q! UPDATE itunes_user_trends, us_zip_codes SET itunes_user_trends.city = us_zip_codes.city, itunes_user_trends.state = us_zip_codes.state WHERE us_zip_codes.code = itunes_user_trends.zipcode AND itunes_user_trends.created_at = '#{date}'; !)
