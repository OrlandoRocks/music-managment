#!/usr/bin/env ruby

RAILS_ENV = ENV["RAILS_ENV"] || "production"

unless ARGV[0] and File.exists? ARGV[0]
  puts "missing filename"
  exit
end

require 'rubygems'
require File.dirname(__FILE__) + '/../../config/environment'

filename = File.expand_path(ARGV[0])
#date = Date.parse(ARGV[1])
# strip the date out of the filename
datestr = Date.parse(filename[-12,8]).strftime("%Y-%m-%d")
date = Date.parse(datestr)

start_time = Time.now


puts "loading #{filename} for #{date} into db"
puts

puts "Checking if file has already been imported.."
puts

# def check_last_import_date so we don't blow up the report objects and import the same file twice!!!!
date_query = "SELECT created_at FROM itunes_user_trends WHERE created_at = '#{date}' LIMIT 1;"
check_date = ItunesUserTrends.find_by_sql(date_query).map {|d| d.created_at.strftime('%Y-%m-%d')}.to_s

  if check_date == datestr
    puts "WARNING! - An import date for #{datestr} has been found, it appears you already imported #{filename}"
    puts "Make sure that you are not importing the same report twice! (Doh!)"
    puts
  else

  puts "We are OK, continuing the import and build of report objects.."


  ItunesUserTrends.connection.execute(%Q! LOAD DATA LOCAL INFILE '#{filename}' INTO TABLE itunes_user_trends FIELDS TERMINATED BY '\t' IGNORE 1 LINES
                                        (@dummy, @dummy, @dummy, upc, isrc, artist, title, label_name, @dummy,
                                        units, @dummy, @dummy, @dummy, zipcode, @dummy, @report_date, @dummy,
                                        @dummy, country_code, @dummy, @dummy, @dummy, @dummy, @dummy, apple_id, @created_at)
                                        SET report_date = STR_TO_DATE(@report_date,'%m/%d/%Y'), created_at = '#{date}'; !)


  import_elapsed_time = sprintf("%.2f",(Time.now - start_time) / 60)

  #format_trends = ItunesUserTrends.find(:all)

  # ft = ItunesUserTrends.find_by_sql("SELECT id, isrc, title, upc, album, song FROM itunes_user_trends AS iut WHERE iut.created_at = '2008-05-11' LIMIT 20")
  #
  #  ft.each do |f|
  #    if f.isrc.nil? || f.isrc.empty?
  #      f.update(:album => f.title)
  #    else
  #      f.update(:song => f.title)
  #    end
  #
  #  end

  puts "REPORT IMPORT: finished at #{Time.now}. Elapsed Time #{import_elapsed_time} minutes"
  puts
  puts "Populating Report Objects.."

    query = ["SELECT person_id, created_at AS 'stop_on',  SUM(units) as 'number_of_records'
              FROM itunes_user_trends AS iut, albums AS a
              WHERE iut.apple_id = a.apple_identifier AND iut.created_at = '#{date}' GROUP BY person_id;"]

    trends = ItunesUserTrends.find_by_sql(query)

    trends.each do |t|
      trend_report = ItunesUserReport.new(:stop_on => t.stop_on,
                                          :start_on => t.stop_on.to_date - 6,
                                          :person_id => t.person_id,
                                          :number_of_records => t.number_of_records,
                                          :created_at => Time.now)
      trend_report.save!
    end

    puts "Updating City and States.."

     ItunesUserTrends.connection.execute(%Q! UPDATE itunes_user_trends, us_zip_codes SET itunes_user_trends.city = us_zip_codes.city, itunes_user_trends.state = us_zip_codes.state
                                             WHERE us_zip_codes.code = itunes_user_trends.zipcode AND itunes_user_trends.created_at = '#{date}'; !)

    puts "Update City/State Complete!"
    puts
    puts "Add Person Id's To itunes_user_trends (NOTE: This will handle only the album trends not songs!)"
    puts
     ItunesUserTrends.connection.execute(%Q! UPDATE itunes_user_trends, albums SET itunes_user_trends.person = albums.person_id
                                             WHERE itunes_user_trends.apple_id = albums.apple_identifier AND itunes_user_trends.created_at = '#{date}'; !)
     puts "UPDATE BY TUNECORE ISRC - (WORKING ON THIS PART::DEACTIVATED)"

     # ItunesUserTrends.connection.execute(%Q/ UPDATE itunes_user_trends i, songs s , albums a SET i.person = a.person_id WHERE i.isrc IS NOT NULL AND i.isrc != '' AND a.id = s.album_id AND i.isrc = s.tunecore_isrc AND i.title = s.name AND i.created_at = '#{date}'; /)
         puts "UPDATE BY OPTIONAL IRSC  - (WORKING ON THIS PART::DEACTIVATED)"
     #
     #      ItunesUserTrends.connection.execute(%Q/ UPDATE itunes_user_trends i, songs s , albums a SET i.person = a.person_id WHERE i.isrc IS NOT NULL AND i.isrc != '' AND a.id = s.album_id AND i.isrc = s.optional_isrc AND i.title = s.name AND i.created_at = '#{date}'; /)
     #    puts "Update Person Id's Complete!"
    puts

    total_elapsed_time = sprintf("%.2f",(Time.now - start_time) / 60)
    puts "POPULATE REPORT OBJECTS: finished at #{Time.now}. Elapsed Time #{total_elapsed_time} minutes"

end

# if isrc check againsts dup isrc table and if "live" check against the apple id table and will know that it is limed up with what is on record.
#   if it is a match put the apple id.
#     migration to add apple_id to songs table, if can populate against trends reports, I will.
#


    # dup_isrcs = DuplicatedIsrc.find_by_sql("select tunecore_isrc from duplicated_isrcs;"
    #
    # trend_isrcs = ItunesUserTrends.find_by_sql("select isrc from itunes_user_trends as i WHERE i.isrc IS NOT NULL AND i.isrc != '';")
    #
