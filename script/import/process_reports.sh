#!/bin/bash
# Just a fast script to run all daily reports. The "apple_treding.rb" file does the heavy lifting..
# This script just checks the files in the directory where you upload the reports to and runs them
# 
# TODO: save date ranges of reports run to a text file
# Author: Mike Steinfeld QUestions: mike@tunecore.com

for file in *.txt
do
       echo "Importing: '$file'"
       echo ""
       ruby /home/tunecore/current/script/import/apple_trending.rb $file
       sleep 0.1
done