#!/usr/bin/env ruby

require 'rubygems'
require 'csv'

require File.dirname(__FILE__) + '/../../config/environment'

unless ARGV[0] and File.exists? ARGV[0]
  puts "missing filename"
  exit
end

raise "No emusic store" unless store = Store.find(17)

start_time = Time.now
puts "BEGIN #{start_time}"

date = start_time.strftime('%Y-%m-%d')
filename =  '/tmp/' + 'AMIEST_' + date + '.TSV'
error_log = File.new("/tmp/AMIEST_AUDIT_ERRORS_#{date}.TXT", "w+")

if File.exists? filename
  puts 'file has already been preprocessed, starting import'
else
  puts 'copying file'
  `cp #{ARGV[0]} #{filename}`
  puts
  puts "Cleaning up file format"
  `sed -i -e 's/[\"]/_/g' #{filename}`
  puts "File Cleaned"
  puts
end

count_live = 0
count_fail = 0
count_no_sp = 0
was_live = 0
count = 0

CSV.foreach(filename, { :force_quotes => false, :col_sep => "\t"}) do |arr|

  count += 1

  upc = arr[0].to_i
  url = arr[3]

    if upc == 0
      STDERR.puts "skipping row #{arr.to_s}"
      count_fail += 1
    else
      #puts "#{count}:: #{arr.to_s}"
      if album = Album.find_upc(upc)
      status = 'live'

        salepoint = Salepoint.find_by(store_id: store.id, salepointable_id: album.id, salepointable_type: "Album")

        if salepoint
          has_link =  Url.find_by(store_id: 17, album_id: album.id, link: url)
          if has_link
            was_live += 1
          else
            count_live += 1
            Url.create(:store_id => 17, :album_id => album.id, :link => url)
            mesg = "#{count}: album #{upc} is now #{status} in Amie Street"
            puts mesg
          end

        else
          count_no_sp += 1
          puts "#{count}: #{upc} was found in albums, but does not appear to have a salepoint!"
          #audit.update_attribute :created_on, @audit_date
          #salepoint.update_attribute(:status, audit.id)
        end
      else
        count_fail += 1
          #mesg = "#{count_fail}: album #{upc} is ALREADY #{status} in Amazon MP3: WE DO NOTHING"
        error_log.puts  "#{count_fail}: #{upc}"
        #STDERR.puts "Did not find album #{upc}"
      end

    end
  end

  count = count -1 # subtract header row
  count_fail = count_fail -1

  stop_time =  Time.now - start_time
  total_rows = count_live + count_fail

  elapsed_time = sprintf("%.2f",(Time.now - start_time) / 60)
  puts "finished at #{Time.now}. Elapsed Time #{elapsed_time} minutes"

  puts "#{count} total albums. #{count - count_no_sp - count_fail} albums went live"
  puts "#{was_live} albums already live : #{count_no_sp} did not have a salepoint "
  puts "#{count_fail - count_no_sp} were not found anywhere."
  puts
  puts "Check /tmp/AMIEST_AUDIT_ERRORS.txt for albums not found during import."

