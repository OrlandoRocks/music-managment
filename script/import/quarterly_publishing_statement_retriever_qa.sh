#clear out old Statements if they exist
rm -rf /tmp/publishing/Statements.zip
rm -rf /tmp/publishing/Statements/
rm -rf /tmp/publishing/pdfs/
rm -rf tmp/royalty_payments/

# set a variable of the time we want to grab pdfs from
quarter=$1
year=$2
timestamp=$1Q$2

# create publishing directory and grab pdfs
mkdir /tmp/publishing
mkdir /tmp/publishing/pdfs
mkdir /tmp/publishing/csvs

# grab and unzip the files
aws s3 cp --recursive s3://ftp.tunecore.com/tcxfr/$timestamp\ Royalties/ /tmp/publishing
unzip /tmp/publishing/Statements.zip -d /tmp/publishing

# move files from directories into pdf folder
find /tmp/publishing/Statements/ -name '*.pdf' -exec mv {} /tmp/publishing/pdfs \;
find /tmp/publishing/Statements/ -name '*.csv' -exec mv {} /tmp/publishing/csvs \;

mkdir tmp/royalty_payments
mkdir tmp/royalty_payments/$year
mkdir tmp/royalty_payments/$year/Q1
mkdir tmp/royalty_payments/$year/Q2
mkdir tmp/royalty_payments/$year/Q3
mkdir tmp/royalty_payments/$year/Q4
mkdir tmp/royalty_payments/$year/Q$quarter/pdfs
mkdir tmp/royalty_payments/$year/Q$quarter/csvs

find /tmp/publishing/pdfs/ -name '*.*' -exec mv {} tmp/royalty_payments/$year/Q$quarter/pdfs \;
find /tmp/publishing/csvs/ -name '*.*' -exec mv {} tmp/royalty_payments/$year/Q$quarter/csvs \;
