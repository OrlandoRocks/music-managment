#!/bin/bash

##############################################################################################
# TITLE: ITUNES REPORT DOWNLOAD AUTOMATOR 																									 #
# AUTHOR: MICHAEL STEINFELD																																	 #
#	DATE: 2008-09-03 																																					 #
# This script is used to download "yesterday's" Sales/Trend Report data.										 #
# This script requires a version of 'curl' that support HTTPS.															 #
##############################################################################################

# Set some account globals
VENDORID="80036242"

# Set these to your iTunes Connect username and password
username='trends@tunecore.com'
password='n00m4rk'

# If you want anything other than yesterday's data then do what you need to do here to get the right date.
# At the moment the date must be in the form mm/dd/yyyy.
# YEST is the format sent to Apple's server.
# DATE is the format used for the data's filename when saved locally.
# Get yesterday's date. Depending on the time of day this script is run and where you live it may be off a bit
YEST=`TZ=PST+24 date "+%m/%d/%Y"`
DATE=`TZ=PST+24 date "+%Y-%m-%d"`
echo "Getting sales and trend data for $YEST"

rm -f cookies

# Go to the login page
curl -s --cookie-jar cookies https://phobos.apple.com/WebObjects/MZLabel.woa/wa/default -o loginscreen.html

# Parse the page looking for the URL used to submit the login form
FORM=`grep '<form' loginscreen.html`
PARTS=`echo $FORM`
ACTION=`for i in $PARTS; do echo $i | grep 'action=';done`
ACTION=${ACTION#action=\"}
ACTION=${ACTION%\">}
#echo $ACTION

rm -f loginscreen.html

# Submit the login form and get the home page
curl -s --cookie-jar cookies --cookie cookies -e ";auto" "https://phobos.apple.com${ACTION}?theAccountName=${username}&theAccountPW=${password}&theAuxValue=" -o homepage.html

# Parse the home page looking for the Sales/Trend Reports link
# - If we want to automate the Catalog Reports/Comprehensives we would begin changes here!
URL=`grep -B1 "<b>Sales/Trend Reports</b>" homepage.html | grep "href"`
PARTS=`echo $URL`
ACTION=`for i in $PARTS; do echo $i | grep 'href=';done`
ACTION=${ACTION#href=\"}
ACTION=${ACTION%\">}
#echo $ACTION

rm -f homepage.html

# Request the Sales/Trend Reports URL. The result of this page is a short reply with a redirect to the itts server.
curl -s --cookie-jar cookies --cookie cookies --location-trusted -e ";auto" "https://phobos.apple.com${ACTION}" -o redirect.html

rm -f redirect.html

# Send a request to the redirect URL. This is hardcoded and will break if the above redirect changes in the future.
curl -s --cookie-jar cookies --cookie cookies --location-trusted -e ";auto" "https://itts.apple.com/cgi-bin/WebObjects/Piano.woa" -o salestrend.html

#Request the correct vendor from the drop down list. (multipart-form)
FORM=`grep 'superPage' salestrend.html`
PARTS=`echo $FORM`
ACTION=`for i in $PARTS; do echo $i | grep 'action=';done`
ACTION=${ACTION#action=\"}
ACTION=${ACTION%\">}
#echo $ACTION

rm -f salestrend.html

# We select our vendorId from the form, which triggers a POST and then populates the READ-ONLY text box
curl -s --cookie-jar cookies --cookie cookies --location-trusted -e ";auto" -d "9%2e6%2e0=${VENDORID}" "https://itts.apple.com${ACTION}" -o selectvendor.html

FORM=`grep 'superPage' selectvendor.html`
PARTS=`echo $FORM`
ACTION=`for i in $PARTS; do echo $i | grep 'action=';done`
ACTION=${ACTION#action=\"}
ACTION=${ACTION%\">}
#echo $ACTION

rm -f selectvendor.html

# Request the selected vendor fields
curl -s  --cookie-jar cookies --cookie cookies --location-trusted -e ";auto" "https://itts.apple.com${ACTION}?9.6.0=${VENDORID}&vndrid=${VENDORID}&Select1=999998.0&9.18=&SubmitBtn=Submit" -o reportoptions.html

URL=`grep "Transaction" reportoptions.html | grep "href" `
PARTS=`echo $URL`
ACTION=`for i in $PARTS; do echo $i | grep 'href=';done`
ACTION=${ACTION#href=\"}  
ACTION=${ACTION%\">}
ACTION=$(echo $ACTION | cut -d "\"" -f1)
#echo $ACTION

rm -f reportoptions.html

# Follow link for transactions (Trend Reports)
# - If we want to automate the financials we would begin changes here!

curl -s --cookie-jar cookies --cookie cookies --location-trusted -e ";auto" "https://itts.apple.com${ACTION}" -o transactions.html

FORM=`grep 'frmVendorPage' transactions.html`
PARTS=`echo $FORM`
ACTION=`for i in $PARTS; do echo $i | grep 'action=';done`
ACTION=${ACTION#action=\"}
ACTION=${ACTION%\">}
#echo $ACTION

rm -f transactions.html

curl -s --cookie-jar cookies --cookie cookies --location-trusted -e ";auto"  "https://itts.apple.com${ACTION}?9.5=Detailed&9.7=Daily&hiddenDayOrWeekSelection=09%2F02%2F2008&hiddenSubmitTypeName=ClearSummary" -o download.html


FORM=`grep 'frmVendorPage' download.html`
PARTS=`echo $FORM`
ACTION=`for i in $PARTS; do echo $i | grep 'action=';done`
ACTION=${ACTION#action=\"}
ACTION=${ACTION%\">}
#echo $ACTION

rm -f download.html

# Keep the filename the same as Apple so we don't break the importer script
FDATE=`TZ=PST+24 date "+%Y%m%d"`  # - YESTERDAY PACIFIC STANDARD TIME
OUTFILE="D_D_${VENDORID}_999998_${FDATE}.txt.gz"

echo "Downloading ${OUTFILE}.."

# URL FORMAT  ?9.5=Detailed&9.7=Daily&9.9.1=09%2F01%2F2008&download=Download&hiddenDayOrWeekSelection=09%2F01%2F2008&hiddenSubmitTypeName=Download
curl -s --cookie-jar cookies --cookie cookies -e ";auto" --location-trusted -d "9%2e5=Detailed" -d "9%2e7=Daily" -d"9%2e9%2e1=${YEST}" -d "download=Download" -d "hiddenDayOrWeekSelection=${YEST}" -d "hiddenSubmitTypeName=Download"  "https://itts.apple.com${ACTION}" -o ${OUTFILE}

echo "Download of ${OUTFILE} complete!"

rm -f cookies

