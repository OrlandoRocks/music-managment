#!/bin/bash

ROOT_DIR=$1
LOG_FILE=$2
ENV=$3
sed -e "s;%FILEPATH%;${ROOT_DIR}/${LOG_FILE};g" ${ROOT_DIR}/config/awslogs.conf.template | sed "s;%ENV%;${ENV};g" > /var/awslogs/etc/awslogs.conf
service awslogs restart
