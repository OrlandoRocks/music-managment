#!/bin/bash

CERT_DIR=$1
ENV=$2

if [ $ENV == "development" ] || [ $ENV == "test" ]; then
  BUCKET=secrets-dev
else
  BUCKET=secrets
fi

mkdir -p $CERT_DIR

echo "Pulling Apple MusicKit private key"
aws s3 cp \
  s3://$BUCKET.tunecore.com/$ENV/tc-www/certs $CERT_DIR \
  --recursive --exclude "*" --include "AppleMusicKit*"

chmod 400 ${CERT_DIR}AppleMusicKit*
