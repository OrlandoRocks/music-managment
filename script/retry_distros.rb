# 2012-03-01 AK
# A script that takes a file containing a list of UPCs (1 per line), and retries them to the specified stores

ENV["RAILS_ENV"] ||= "production"
require File.dirname(__FILE__) + '/../config/environment'
require 'optparse'
require 'ostruct'

STDOUT.sync = true # no buffer

options = OpenStruct.new
options.file = ''
options.stores = []
options.comment = ''

OptionParser.new do |opts|
  opts.banner = "Usage: retry_distros.rb [options]"
  
  opts.on('-f','--file UPC_FILE','File containing UPCs to retry, one per line.') do |upc_file|
    options.file = upc_file
  end
  
  opts.on('-s','--stores STORES',Array,"Comma seperated list of stores to deliver to") do |stores|
    options.stores = stores
  end
  
  opts.on('-c','--comment COMMENT',"Comment that will be added to the retried distros") do |comment|
    options.comment = comment
  end
  
  opts.on('-t','--delivery_type DELIVERY_TYPE',"The delivery type. Default is full_delivery. Options are full_delivery | metadata_only") do |delivery_type|
    options.delivery_type = delivery_type
  end
end.parse!

# Check that store names are valid
options.stores.each do |store|
  begin
    eval "MusicStores::#{store}::Converter"
  rescue
    puts ("Store name: #{store} is invalid, it should match the Converter name (ex: Itunes,Amazon)")
  end
end

stime = Time.now

puts "Reading #{options.file}"
f = open(options.file)
upcs = f.readlines().collect{|l| l.chomp()}
f.close
puts "Found #{upcs.size} UPCs"
puts ""
n = 0
f = 0
upcs.each do |upc|
  begin
    a = Album.find_upc(upc)
    options.stores.each do |store|
      print "Retrying #{upc} to #{store} ... "
      distro = a.distributions.detect{|d| d.converter_class == "MusicStores::#{store}::Converter"}
      if ! distro
        print "No #{store} distro, skipping\n"
        next
      end
      distro.delivery_type = options.delivery_type.blank? ? 'full_delivery' : options.delivery_type
      distro.retry(:actor => "Alex's retry script", :message => options.comment)
      print "OK\n"
      n+=1
    end
  rescue => e
    print "Failed: #{e.message})\n"
    f+=1
  end
end

puts ""
puts "Retried #{n} distributions. #{f} failed.  Run time: #{(Time.now - stime).to_i} sec"