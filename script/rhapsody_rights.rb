# Adds usage rights data to Rhapsody metdata XML file
require "rexml/document"
require "logger"
require "find"
include REXML

class RhapsodyRights

  def initialize
    # these are the attributes for the <right> tag ... ex: <right type="labelStream">Y</right>  
    @rights_types = ["labelStream", "labelBurn", "labelDownload", "publisherBurn", "publisherDownload"]
    @log = Logger.new(STDOUT)
    @log.info "Initializing with types: " + @rights_types.collect.join(",")
  end
  
  #get this party started!
  def self.start
    if ARGV[0] == nil
      puts "usage: rhapsody_rights.rb {file or directory to process}"
      return
    end
    rr = self.new 
    if File.directory?( ARGV[0] )
      rr.process_dirs( ARGV[0] )
    else
      rr.process_file( ARGV[0] )
    end
  end
  
  #processes sub-directories of a given path, process all xml files that we find
  def process_dirs( base_dir )
    Find.find(base_dir) do |path|
        process_file( path ) if File.extname(path) == ".xml"
    end 
  end

  def process_file( filename )
    @filename = filename
    file = File.open( filename )
    @log.info "Opening #{filename}"
    doc = REXML::Document.new file
    file.close
    new_doc = add_rights_to_album( doc )
    save_file( doc )
    return true
  end
  
  def save_file( doc )
    @log.info "Saving to #{@filename}"
    file = File.open(@filename, "w")
    doc.write file
    file.close
    @log.info "Finished with #{@filename}"
  end

  def add_rights_to_album( doc )
    #find all the track elements
    tracks = []
    doc.elements.each("//albums/album/track") {|a| tracks<< a}
    #process each trach
    tracks.each do |track|
      @log.info "Adding rights to track"
      add_rights_to_track( track )
    end
    return doc
  end

  # takes the track element and applies proper rights
  def add_rights_to_track( track )   
    # figure out which rights are missing from the track so we can add them
    existing_rights = []
    track.elements.each("right") {|e| existing_rights << e.attributes["type"] }
    rights_to_add = @rights_types - existing_rights
    @log.info "Adding rights: " + rights_to_add.collect.join(",")
    # we have to insert the rights elements after the artist element
    artist_element = track.elements["artist"]
    # create the elements that need to be added to the file and insert them appropriately
    rights_to_add.each do |right_type|
      right_element = Element.new "right"
      right_element.text = "Y"
      right_element.attributes["type"] = right_type
      track.insert_after artist_element, right_element
    end 
  end
end

RhapsodyRights.start