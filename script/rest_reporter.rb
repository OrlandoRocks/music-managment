require "net/http"
require "uri"
module RestReporter

 # The timestamp is used to find the corresponding distribution for this upc, since
 # we might have multiple distributions with this upc.
 def report_delivered(upc, timestamp)
  uri = URI.parse("http://www.tunecore.com/distribution/update")
  res = Net::HTTP.post_form( uri, {"upc" => upc, "timestamp" => timestamp, "actor" => "upload script", "message" => "packaged" } )
 end

end
