#!/usr/bin/env ruby -w

#===========================================================#
# Quick script to copy images to new dir stucture
# You must run this from inside the dir where the images are
#===========================================================#
require "fileutils"

arr = []

def pushpath(i)
  file = i + ".jpg"
  dir = i.scan(/..?/).join("/") + "/"
  FileUtils.mkdir_p(dir)

  if File.exists?(dir)
    FileUtils.cp(file, dir)
    puts "copying.. #{file} to #{dir}"
  else
    resuce e
    puts "The Directory does not exists, can't copy file!"
  end
end

Dir['*.jpg'].each { |d| arr << d[0..-5] if !File.directory?(d) }

arr.each { |p| pushpath(p)  }

