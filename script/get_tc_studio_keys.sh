#!/bin/bash

CERT_DIR=$1
ENV=$2

if [ $ENV == "development" ] || [ $ENV == "test" ]; then
  BUCKET=secrets-dev
else
  BUCKET=secrets
fi

mkdir -p $CERT_DIR

echo "Pulling tc-studio jwt signing keys"
aws s3 cp \
  s3://$BUCKET.tunecore.com/$ENV/tc-www/certs $CERT_DIR \
  --recursive --exclude "*" --include "tc_studio_jwt*"
