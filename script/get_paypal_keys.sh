#!/bin/bash

CERT_DIR=$1

if [ "$2" == "development" ] || [ "$2" == "test" ]; then
    BUCKET=secrets-dev
    ENV=development
    FILE_ENV=development
fi

if [ "$2" == "qa"  ] || [ "$2" == "staging" ]; then
    BUCKET=secrets
    ENV=staging
    FILE_ENV=development
fi

if [ "$2" == "production" ]; then
    BUCKET=secrets
    ENV=production
    FILE_ENV=production
fi

echo "Pulling private paypal keys/certs"
mkdir -p $CERT_DIR

for country in ca us uk
do
  for file_ext in cert key
  do
    aws s3 cp s3://$BUCKET.tunecore.com/$ENV/tc-www/certs/paypal_${country}_$ENV.${file_ext} $CERT_DIR/paypal_${country}_$FILE_ENV.${file_ext}
    chmod 400 $CERT_DIR/paypal_${country}_$ENV.${file_ext}
  done
done


for program in bi tc
do
  for program_type in cad default
  do
    for file_ext in cert key
    do
      aws s3 cp s3://$BUCKET.tunecore.com/$ENV/tc-www/certs/paypal_${program}_${program_type}_$ENV.${file_ext} $CERT_DIR/paypal_${program}_${program_type}_$ENV.${file_ext}
      chmod 400 $CERT_DIR/paypal_${program}_${program_type}_$ENV.${file_ext}
    done
  done
done
