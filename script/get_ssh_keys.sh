#!/bin/bash

echo "Pulling private keys"
list_of_keys="simfyaf_rsa
akazoo_rsa
amazon_rsa
artwork_id_rsa
docusign_rsa
google_rsa
gracenote_tcus_rsa
joox_rsa
netease_rsa
neurotic_media_rsa
pandora
rhapsody_dsa
saavn_rsa
seven_digital_rsa
shazam_rsa
soundcloud_rsa
spotify_rsa
target_music_rsa
tidal_rsa
tunecore
uma_rsa
youtube_music_rsa
youtube_sr_rsa
zedplus_rsa
facebook2_rsa
quboz_rsa
peloton_rsa
"

if [ "$RAILS_ENV" == "development" ]; then
  aws s3 cp s3://secrets-dev.tunecore.com/development/tc-www/ssh_keys/docusign_rsa ~/.ssh/docusign_rsa
  chmod 400 ~/.ssh/docusign_rsa
elif  [ "$RAILS_ENV" == "staging" ]; then
  aws s3 cp s3://secrets.tunecore.com/staging/tc-www/ssh_keys/tc-distributor-ftp-rsa ~/.ssh/tc-distributor-ftp
  chmod 400 ~/.ssh/tc-distributor-ftp
else
  for key in $list_of_keys
  do
    aws s3 cp s3://secrets.tunecore.com/${RAILS_ENV}/tc-www/ssh_keys/${key} ~/.ssh/${key}
    chmod 400 ~/.ssh/${key}
  done
fi
