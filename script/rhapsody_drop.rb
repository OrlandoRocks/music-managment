require 'rubygems'
require 'net/sftp'
require 'net/sftp/operations/errors'
require 'net/ssh/version'
require 'fileutils'
require 'logger'

class Connection
  def initialize(options = {})
    @host     = options[:host]      or raise "No hostname"
    @username = options[:username]  or raise "No username."
    @keyfile  = options[:keyfile]   or raise "No keyfile."
    @port     = options[:port]      || 22
    @paranoid = options[:paranoid]  || false
    @password = options[:password]
  end

  def connect(&block)
    #forking here, because net-ssh.put_file deadlocks on rehdat.... not necessary OSX, BSD, ubuntu.
    pid = Process.fork do
      Net::SFTP.start(@host, @username, @password, :keys => [@keyfile], :port => @port, :paranoid => @paranoid) do |sftp|
        block.call(sftp)
      end
    end

    pid, status = Process.waitpid2(pid)
    raise Error.new("ssh process failed.") unless status.exitstatus == 0
  end
end

def get_packages(destination,logger,packages)
  begin
    @rhapsody_server = Connection.new(:host=>'madmax.listen.com', :username=>'tunecore_108642',:keyfile=>'/home/petri/.ssh/rhapsody_dsa', :port=>'5000');
    @rhapsody_server.connect do |sftp3|
      bdir=sftp3.opendir(destination)
      dirs = sftp3.readdir(bdir)
      packages << "#{dirs.size}\n"
    end
  rescue => err
    logger.error "Errror #{source} - #{err}"
  end
end
def find_completed_packages(source,packages,logger)
  begin
    Dir.foreach(source) do |dir|
      if not (dir.eql?(".") || dir.eql?(".."))
        Dir.foreach(source+"/#{dir}") do |xdir|
          if  File.extname(xdir).eql?(".complete")
            packages << "#{dir}"
          end
        end
      end
    end
  rescue => err
    logger.error "Errror #{source} - #{err}"
  end
end

def upload(source,destination,package_name,logger,shipped)
  begin
    @rhapsody_server = Connection.new(:host=>'madmax.listen.com', :username=>'tunecore_108642',:keyfile=>'/home/petri/.ssh/rhapsody_dsa', :port=>'5000');
    @rhapsody_server.connect do |sftp|
      logger.info "starting #{package_name}"
      r_dir = destination+package_name
      begin
        sftp.stat(r_dir)
      rescue Net::SFTP::Operations::StatusException => e
        if e.code == Net::SFTP::Operations::Constants::FX_NO_SUCH_FILE
          sftp.mkdir(r_dir,{})
        else
          logger.fatal "sftp.stat #{e}"
        end
      end
      l_dir = source+package_name
      Dir.foreach(l_dir) do |dir|
        if File.extname(dir).eql?(".jpg")
          logger.info "uploading cover art #{dir}."
          l_cover_art =File.join(l_dir, dir)
          r_cover_art = File.join(r_dir,dir)
          sftp.put_file(l_cover_art,r_cover_art)
        elsif  File.extname(dir).eql?(".aac")
          logger.info "uploading track #{dir}."
          l_aac =File.join(l_dir, dir)
          r_aac = File.join(r_dir,dir)
          sftp.put_file(l_aac,r_aac)
        elsif  File.extname(dir).eql?(".xml")
          logger.info "uploading metadata #{dir}."
          l_xml =File.join(l_dir, dir)
          r_xml =File.join(r_dir,dir)
          sftp.put_file(l_xml,r_xml)
        end
      end
      #move the package to the archive
      FileUtils.mv("#{source}#{package_name}",shipped)
      logger.info "moved #{package_name} to the ~/shipped dir."
    end
  rescue => err
    logger.fatal "Errror while uploading - #{err}"
  end
end

def delivery_complete(destination,logger)
  begin
    @rhapsody_server = Connection.new(:host=>'madmax.listen.com', :username=>'tunecore_108642',:keyfile=>'/home/petri/.ssh/rhapsody_dsa', :port=>'5000');
    @rhapsody_server.connect do |sftp2|
      delivery_complete=File.join("/tmp/", "delivery.complete")
      File.open(delivery_complete, "w")
      remote_delivery_complete = File.join(destination, "delivery.complete")
      logger.info "uploading #{delivery_complete} file."
      sftp2.put_file(delivery_complete, remote_delivery_complete)
      logger.info "completed uploading delivery_complete file."
    end
  rescue StandardError => err
    logger.fatal "Errror while uploading - #{err}"
  end
end

def init()
  source = "/home/petri/drops/rhapsody/"
  destination = "/incoming/"
  shipped = "/home/petri/shipped/rhapsody/"+Time.new.strftime('%Y_%m_%d')
  FileUtils.mkdir_p(shipped)
  log_file = "/home/petri/plan_b/logs/rhapsody_drop.log"
  logger = Logger.new(log_file,shift_age = 'daily')
  logger.level = Logger::DEBUG
  packages = File.open("/tmp/rhapsody_pkgs", "a")
  get_packages(destination,logger,packages)
  file2 = File.new("/tmp/rhapsody_pkgs", "r")
  while (pkgs = file2.gets)
    pkgs_size = pkgs.gsub(/\n/,"")
  end
  FileUtils.rm("/tmp/rhapsody_pkgs")
  upload_size=400
  begin
    #is remote dir empty?
    #if pkgs_size.to_i == 2
      #get completed packages
      packages = Array.new
      find_completed_packages(source,packages,logger)
      if packages.size > 0
        completed_packages = packages.uniq
        icount = 0
        logger.info "Total packages to be shipped - #{packages.size.to_s} - max of #{upload_size} packages will be delivered in this round."
        completed_packages.each do |package|
          #upload the packages
          if icount < upload_size
            count = icount+1
            logger.info "Uploading - #{count} of #{upload_size}."
            upload(source,destination,package,logger,shipped)
          end
          icount = icount+1
        end
        delivery_complete(destination,logger)
      end
    #else
    #  logger.info "terminating upload - remote #{destination} has #{pkgs_size} packages. It has to be empty to upload new packages."
    #end
  rescue StandardError => e
    logger.fatal "#{e}"
  end
end

#run
init()
