#!/usr/bin/env ruby
#
#This script fixes artwork missing on S3 but stored on the production server.

ENV['RAILS_ENV'] ||= 'production'
require File.dirname(__FILE__) + '/../config/environment'

def upload_art(art)
  art.s3_asset = S3Asset.new(:bucket => TIFF_BUCKET_NAME, :key => "#{art.s3_key_prefix}.tiff", :filename => @full_image_file)
  art.s3_asset.put!   # actually upload the file...
  art.s3_asset.save!
  art.save!

end

@image_dir = 'tmp'
albums = ARGV or raise "need album id(s) to fix"
albums.each do |album_id|

  image_file = "#{album_id}.jpg"
  @full_image_file = File.join(@image_dir, image_file)
  command = "wget -P #{@image_dir} http://www.tunecore.com/images/artwork/complete/tiffs/#{image_file}"

  if system(command) && File.exists?(@full_image_file)
    art = Album.find(album_id).artwork
    if art
      if art.s3_asset
        puts "#{album_id}- S3 Asset already exists: #{art.s3_asset.inspect}"
        exit 0
      end

      upload_art(art)

      if art.is_tiff_actually_on_s3?
        puts "#{album_id}- Artwork added successfully"
      else
        puts "#{album_id}- Artwork not added"
      end
      FileUtils.rm_f(@full_image_file, :verbose=>true) if File.exists?(@full_image_file)
    end
  else
    puts "#{album_id}- Download failed\n"
    #exit 0
  end

end
