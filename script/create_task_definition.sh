#!/bin/bash

RAILS_ENV=$1    # production/staging
DEPLOY_ENV=$2   # api/studio/qa#/staging
CPU_UNITS=$3
MEMORY_UNITS=$4
SERVICE=$5      # nginx_puma/sidekiq_default/sidekiq_delivery
IMAGE_PREFIX=$6 # example: staging-qa2

if [ "$SERVICE" == "sidekiq_default" ]; then
    SERVICE="sidekiq"
    SIDEKIQ_ENV="default"
elif [ "$RAILS_ENV" == "sidekiq_delivery" ]; then
    SERVICE="sidekiq"
    SIDEKIQ_ENV="delivery"
fi

sed -e "s/%RAILS_ENV%/${RAILS_ENV}/g; s/%DEPLOY_ENV%/${DEPLOY_ENV}/g; s/%NGINX_CPU_UNITS%/${CPU_UNITS}/g; s/%NGINX_MEMORY_UNITS%/${CPU_UNITS}/g; s/%PUMA_CPU_UNITS%/${CPU_UNITS}/g; s/%PUMA_MEMORY_UNITS%/${MEMORY_UNITS}/g; s/%SIDEKIQ_CPU_UNITS%/${CPU_UNITS}/g; s/%SIDEKIQ_MEMORY_UNITS%/${MEMORY_UNITS}/g; s/%SIDEKIQ_ENV%/${SIDEKIQ_ENV}/g; s/%IMAGE_PREFIX%/${IMAGE_PREFIX}/g;" infra/ecs/${SERVICE}-task-definition.json > tmp/${SERVICE}-task-definition.${DEPLOY_ENV}.json
aws ecs register-task-definition --family tc-www-${RAILS_ENV}-${DEPLOY_ENV}-${SERVICE} --cli-input-json file://tmp/${SERVICE}-task-definition.${DEPLOY_ENV}.json

rm -f tmp/${SERVICE}-task-definition.${DEPLOY_ENV}.json