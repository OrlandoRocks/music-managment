var Notifications = function(options) {

  //
  // Options
  //
  var menu = options.menu;
  var link = options.link;
  var count = options.count;
  var dropdown = options.dropdown;
  var archive_link = options.archive;
  var notifications_item = options.notifications_item;
  var page_link = options.page_link;
  var notifications_archive = options.notifications_archive;
  var page_select = options.page_select;
  var notification_action = options.notification_action;

  // Use jQuery
  var $ = jQuery;

  var self = {

    //
    //  Construct and initialize
    //
    init: function() {

      // Setup events
      $(link).on('click', this.show);
      $(document).on('click', self.hide);

      //Setup dropdown link
      $(document).on("ajax:success", link, function(event, data, status, xhr) {
        $(menu).addClass("seen");
        $(count).text("0");
        $(dropdown).find('ul').empty().append(data);
      })
      .on("ajax:beforeSend", link, function(xhr, settings) {
        var state = $(this).data('state');
        state = !state;
        $(this).data('state', state);

        if (state) {
          $(dropdown).show();
          $(dropdown).find('ul').empty().append('<li style="text-align: center"><img src="/images/spinner02.gif" alt=""></li>');
          return true;
        } else {
          self.hide();
          return false
        }
      })

      //Setup archive link
      $(document).on("ajax:success", archive_link, function(event, data, status, xhr) {
        $(this).closest(notifications_item).remove();
      });

      //Setup archive page links
      $(document).on("ajax:success", page_link, function(event, data, status, xhr) {
        $(notifications_archive).replaceWith(data);
      });

      //Setup archive page select box
      $(document).on("ajax:success", page_select, function(event, data, status, xhr) {
        $(notifications_archive).replaceWith(data);
      });

      $(document).on("change", page_select + " select", function() {
        $(this).submit();
      });

      $(document).on("click", notification_action, function() {
       $.ajax({
         async: false,
         url: $(this).data("action-url"),
         type: 'post'
       });
      })
    },

    hide: function() {
      $(dropdown).hide();
      $(link).data('state', false);
    }
  };
  
  self.init();
  return self;
};

var init_notifications = function(){
  new Notifications({
    'menu': '.notifications',
    'link': '.notifications_link',
    'count': '.notifications_count',
    'dropdown': '.notifications_dropdown',
    'archive': '.hide_notification',
    'notifications_item': '.notifications_item',
    'page_link': '#notifications_archive .page_link',
    'notifications_archive': '#notifications_archive',
    'page_select': '.page_select_form',
    'notification_action': '.notification_action'
  });
};
jQuery(document).ready(init_notifications);

// $("#close_notifications_link").live("click", function(){
//   $("#notification_dropdown").hide();
// });
// 
// jQuery(function($){
// 
//   /* Bind javascript callbacks for the notfications link */
//   // $("#notification_link").bind("ajax:success", function(evt, data, status, xhr){
//   //   $("#notification_dropdown").html(data);
//   //   $("#notification_dropdown").show();
//   //   $("#notification_link").text("0");
//   // })
//   // 
//   // $("#close_notifications_link").live("click", function(){
//   //   $("#notification_dropdown").hide();
//   // })
// 
// });
