// mini-carousel is a manually operated simple carousel script.
// Load it to the page required and wrap your whole carousel in `mini-carousel` class.
// Wrap individual slide in carousel with `.slide` class
// Mark links that should go to next slide with `.next-slide` class
// Mark links that should go to previous slide with `.previous-slide` class

jQuery(document).ready(function($) {
  $('.mini-carousel .next-slide').on('click', function(e) {
    var thisSlide = $(this).closest('.slide');

    // Only if next slide exists
    if (thisSlide.next('.slide').length > 0) {
      thisSlide.hide();
      thisSlide.next('.slide').show();
    }
  });

  $('.mini-carousel .previous-slide').on('click', function(e) {
    var thisSlide = $(this).closest('.slide');

    // Only if previous slide exists
    if (thisSlide.prev('.slide').length > 0) {
      thisSlide.hide();
      thisSlide.prev('.slide').show();
    }
  });
});
