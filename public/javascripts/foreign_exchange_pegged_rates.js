function refresh_pegged_prices() {
  var urlParams = new URLSearchParams(window.location.search);
  var updatedRate = $('#foreign_exchange_pegged_rate_pegged_rate').val();
  var currency = urlParams.get('currency');
  $.get("/admin/foreign_exchange_pegged_rates/preview_price?currency="+currency+"&updated_pegged_rate="+updatedRate);
}
