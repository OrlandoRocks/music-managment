$(function(){
  var TC = window.TC || {};
  TC.FormUtils = {};

  /* 
    Select Field With Other Option
    Binds to a select field and shows/hides a text input used to collect additional data if "other" is selected within the select field.
  */

  TC.FormUtils.selectFieldWithOtherOption = function(config){
    return this.configure(config)
               .bind()
               .change(!!1),
           this;
  };

  TC.FormUtils.selectFieldWithOtherOption.prototype = {

    configure: function(config){
      return this.initConfig(config)
                 .configSelectFieldTriggerValue()
                 .configInputField(),
             this;
    },

    initConfig: function(config){
      return $.extend(this, {
                   selectField: null, // required as jquery element
                   selectFieldTriggerValue: 'other', // str or array
                   inputFieldContainer: null, // required as jquery element unless input field is passed
                   inputField: null // required as jquery element unless input field container is passed
                 }, config),
             this;
    },

    configSelectFieldTriggerValue: function(){
      if(typeof this.selectFieldTriggerValue == 'string') this.selectFieldTriggerValue = [this.selectFieldTriggerValue];
      return this;
    },

    configInputField: function(){
      if(!this.inputField) this.inputField = this.inputFieldContainer.find('input[type="text"]:first');
      return this;
    },

    bind: function(){
      return this.bindChange(),
             this;
    },

    bindChange: function(){
      var t = this;
      t.selectField.on('change', function(){
        t.change();
      });
      return this;
    },

    change: function(init){
      var val = this.selectField.val();
      if(this.selectFieldTriggerValue.indexOf(val) > -1){
        this.clearInputField().showInputField();
        if(!init) this.focusInputField();
      }
      else this.hideInputField().setInputField(val);
      return this;
    },

    clearInputField: function(){
      return this.inputField.val(''),
             this;
    },

    setInputField: function(val){
      return this.inputField.val(val),
             this;
    },

    showInputField: function(){
      (!!this.inputFieldContainer) ? this.inputFieldContainer.show() : this.inputField.show();
      return this;
    },

    hideInputField: function(){
      (!!this.inputFieldContainer) ? this.inputFieldContainer.hide() : this.inputField.hide();
      return this;
    },

    focusInputField: function(){
      return this.inputField.focus(),
             this;
    }

  };

  window.TC = TC;

});
