// Description
// The TC Style Guide plugin checks inputs against a list of default, or passed-in
// rules and provides a non-blocking warning for the user, informing them of the recommended usage.
// The message will stay as long as the input matches one of the provided rules.
//
// Usage
//   $(':input').styleGuide({
//       rules: {'+': /\+/},
//       messages: {'+': 'You can't have a plug sign!'}
//    });
//
// Change Log
// [2009-12-21 -- MT] First stab at jQuery plugin architecture.
// [2010-1-22  -- MT] First stab at jQuery plugin architecture.
//
// TODO: Add 'Ignore' link, set up event delegation/.live() for multiple creatives


(function($){
  $.fn.styleGuide = function(options){

    // merge in additional rules and messages as necessary, store that as opts
    var opts = $.extend({}, $.fn.styleGuide.defaults, options);

    // plugin defaults: unused at the moment
    $.fn.styleGuide.defaults = {
      rules: {},
      messages: {}
    };
    // iterate over each element in the jQuery object, then return the object
    return this.each(function(){
      // cache this to prevent endless querying of the dom
      var $this = $(this);

      // create suggestion container
      $this.after('<p class=\'style-suggestion hide\'></p>');

      // on keyup
      $this.bind('keyup', function(){
          // wait a bit before testing against the rules
        setTimeout(function(){

          var current_val = $this.val();
            // loop through rules object
          for (var rule in opts.rules){
              // test the current value
            result = ($.isFunction(opts.rules[rule])) ? opts.rules[rule].call(null, current_val) : opts.rules[rule].test(current_val);
            if (result){
                // fake the cursor position
              var pos = ($this.caret() * 7) + 'px';
                // if there's a match, populate the message that corresponds
                // to the rule in our message container
              $this.next()
                  .text(opts.messages[rule])
                  .removeClass('hide')
                  .css('left', pos);
                  //wait a bit, then hide the message based on user action
                  //NOTE: the message will re-appear if the input still violates a rule
              setTimeout(function(){
                $this.bind('keyup blur', function(){
                  $this.next().addClass('hide');
                });
              }, 100);
            }
          }
        }, 100);
      }
      );
    });
  };
})(jQuery);

// A small plugin to get the carets position in a textfield
(function($) {
  $.fn.caret = function(pos) {
    var target = this[0];
    if (arguments.length === 0) { //get
      if (target.selectionStart) { //DOM
        pos = target.selectionStart;
        return pos > 0 ? pos : 0;
      }
      else if (target.createTextRange) { //IE
        target.focus();
        var range = document.selection.createRange();
        if (range === null)
          return '0';
        var re = target.createTextRange();
        var rc = re.duplicate();
        re.moveToBookmark(range.getBookmark());
        rc.setEndPoint('EndToStart', re);
        return rc.text.length;
      }
      else return 0;
    } //set
    if (target.setSelectionRange) //DOM
      target.setSelectionRange(pos, pos);
    else if (target.createTextRange) { //IE
      range = target.createTextRange();
      range.collapse(true);
      range.moveEnd('character', pos);
      range.moveStart('character', pos);
      range.select();
    }
  };
})(jQuery);
