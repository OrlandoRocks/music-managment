;(function( $ ){

  $.fn.syncTable = function( options ) {
    var settings = $.extend({
      spinner: '/images/spinner02.gif',
      pagination_link: '.requests-pagination a',
      more_results_link: '#per_page',
      columns: '5',
      url: ''
    }, options);
    var $container = this;
    
    return this.each(function() {
      var self = {
        init: function() {
          $(document).on('click', settings.pagination_link, self.get_page);
          $(document).on('change', settings.more_results_link, self.get_more_results);
        },
        
        get_page: function(e) {
          e.preventDefault();
          self.results($(this).attr("href"), "per_page=" + $(settings.more_results_link).val());
        },
        
        get_more_results: function(e) {
          e.preventDefault();
          self.results(settings.url, "per_page=" + $(settings.more_results_link).val());
        },
        
        results: function(url, data) {
          $container.find('tbody').html('<tr><td colspan="' + settings.columns + '" style="text-align: center;"><img src="' + settings.spinner + '" alt="" /></td></tr>');
          $.ajax({
            type: "GET",
            url: url,
            data: data,
            success: function(html) {
              $container.html(html);
            }
          });
        }
      };
      return self.init();
    });
  };

})( jQuery );