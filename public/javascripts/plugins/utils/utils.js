(function($) {
    // todo: fix bug where slow animation speed causes glitches
    // todo: should we allow multiple drops per handle? if so how do we store access to both api's
    var Drop = function(handle, display, config) {
        var t = this;
        t.handle = $(handle);
        t.handle.addClass('drop_handle');
        t.display = $(display);
        t.display.addClass('drop_display').addClass('drop_display_hidden');
        t.config = {
            animation_speed: 500,
            show_delay: 500,
            hide_delay: 1000,
            listener: 'hover',
            // hover, focus, click, what else?
            callbacks: {
                beforeShow: function(api) {},
                onShow: function(api) {},
                beforeHide: function(api) {},
                onHide: function(api) {}
            }
        }
        $.extend(true, t.config, config);
        t.id = Math.floor(Math.random() * 100000000000);
        // create generator
        t.block = false;
        t.hide_display = function() {
            if (t.display.hasClass('drop_display_visible')) {
                t.config.callbacks.beforeHide(t);
                if (t.block == false) {
                    t.display.removeClass('drop_display_visible').addClass('drop_display_hidden').show().fadeOut(t.config.animation_speed);
                    t.config.callbacks.onHide(t);
                }
                t.block = false;
            }
        }
        t.show_display = function() {
            if (t.display.hasClass('drop_display_hidden')) {
                t.config.callbacks.beforeShow(t);
                if (t.block == false) {
                    t.display.removeClass('drop_display_hidden').addClass('drop_display_visible').hide().fadeIn(t.config.animation_speed);
                    t.config.callbacks.onShow(t);
                }
                t.block = false;
            }
        }
        var start_hide_timer = function(api) {
            api.handle.data('hide_timer_' + api.id, setTimeout(function(api) {
                return function() {
                    api.hide_display();
                }
            } (api), api.config.hide_delay));
        }
        var start_show_timer = function(api) {
            api.handle.data('show_timer_' + api.id, setTimeout(function(api) {
                return function() {
                    api.show_display();
                }
            } (api), api.config.show_delay));
        }
        var clear_hide_timer = function(api) {
            clearTimeout(api.handle.data('hide_timer_' + api.id));
        }
        var clear_show_timer = function(api) {
            clearTimeout(api.handle.data('show_timer_' + api.id));
        }
        var show_switch = function() {
            if (! (t.handle.hasClass('drop_handle_activated_' + t.id))) {
                t.handle.addClass('drop_handle_activated_' + t.id);
                start_show_timer(t);
                clear_hide_timer(t);
            }
        }
        var hide_switch = function() {
            if (t.handle.hasClass('drop_handle_activated_' + t.id)) {
                t.handle.removeClass('drop_handle_activated_' + t.id);
                start_hide_timer(t);
                clear_show_timer(t);
            }
            return false;
        }
        switch (t.config.listener) {
        case 'hover':
            t.handle.bind({
                mouseenter:
                function() {
                    show_switch();
                },
                mouseleave: function() {
                    hide_switch();
                }
            });
            t.display.bind({
                mouseenter: function() {
                    show_switch();
                },
                mouseleave: function() {
                    hide_switch();
                }
            });
            break;
        case 'focus':
            t.handle.bind({
                focusin:
                function() {
                    show_switch();
                },
                focusout: function() {
                    hide_switch();
                }
            });
            break;
        case 'click':
            t.handle.bind({
                click:
                function() {
                    if (t.display.hasClass('drop_display_hidden')) {
                        show_switch();
                    }
                    else {
                        hide_switch();
                    }
                    return false;
                }
            });
            $(window).bind({
                click:
                function() {
                    hide_switch();
                }
            });
            break;
        }
        t.handle.data('drop', t);
        // make api accessible through html element
        return t;
    };

    $.Drop = function(handle, display, config) {
        var config = config || {};
        new Drop(handle, display, config);
    };

    var Tip = function(handle, config) {
        var t = this;
        var body = $('body');
        t.handle = $(handle);
        t.config = {
            animation_speed: 500,
            pointer: 'tl',
            pointer_color: '#CCCCCC',
            pointer_size: 10,
            margin: 0,
            listener: 'hover',
            callbacks: {
                content: function(api) {
                    return api.handle.attr('tip');
                }
            },
            show_delay: 500,
            hide_delay: 1000,
            refresh: false,
            disabled: false,
            // not implemented yet
            solo: false,
            add_class: ''
        };
        $.extend(true, t.config, config);
        var pointers = {
            tl: 'M 0 10 L 0 0 L 10 10 Z',
            tm: 'M 0 10 L 5 0 L 10 10 Z',
            tr: 'M 0 10 L 10 0 L 10 10 Z',
            rt: 'M 0 0 L 10 0 L 0 10 Z',
            rm: 'M 0 0 L 10 5 L 0 10 Z',
            rb: 'M 0 0 L 10 10 L 0 10 Z',
            br: 'M 0 0 L 10 0 L 10 10 Z',
            bm: 'M 0 0 L 5 10 L 10 0 Z',
            bl: 'M 0 0 L 0 10 L 10 0 Z',
            lb: 'M 10 10 L 0 10 L 10 0 Z',
            lm: 'M 10 0 L 10 10 L 0 5 Z',
            lt: 'M 0 0 L 10 0 L 10 10 Z'
        };
        t.id = Math.floor(Math.random() * 100000000000);
        body.append('<div class="tip" id="tip_:id:"><div class="tip_relative"><div class="tip_pointer" id="pointer_:id:"></div><div class="tip_content">:content:</div></div></div>'.replace(/:id:/g, t.id).replace(/:content:/, t.config.callbacks.content(t)));
        t.tip = $('#tip_' + t.id);
        if (t.config.add_class || false) {
            t.tip.addClass(t.config.add_class);
        }
        if (t.config.listener == 'show') {
            t.tip.addClass('show_listener');
        }
        var paper = Raphael('pointer_' + t.id, t.config.pointer_size, t.config.pointer_size);
        paper.path(pointers[t.config.pointer]).attr('stroke', t.config.pointer_color).attr('stroke-width', 0).attr('fill', t.config.pointer_color);
        paper.renderfix();
        t.pointer = $('#pointer_' + t.id);
        t.position = function(api) {
            var hOffset = api.handle.offset();
            var hWidth = api.handle.outerWidth();
            var hHeight = api.handle.outerHeight();
            var tWidth = api.tip.outerWidth();
            var tHeight = api.tip.outerHeight();
            var pWidth = api.config.pointer_size;
            var pHeight = api.config.pointer_size;
            var margin = api.config.margin;
            var tip_top = 0;
            var tip_left = 0;
            var pointer_top = 0;
            var pointer_left = 0;
            switch (api.config.pointer) {
            default:
                alert('Tip position does not exist!');
                break;
            case 'tl':
                tip_top = hOffset.top + hHeight + pHeight + margin;
                tip_left = hOffset.left + hWidth + margin;
                pointer_top = pHeight * -1;
                pointer_left = 0;
                break;
            case 'tm':
                tip_top = hOffset.top + hHeight + pHeight + margin;
                tip_left = hOffset.left + hWidth / 2 - tWidth / 2;
                pointer_top = pHeight * -1;
                pointer_left = tWidth / 2 - pWidth / 2;
                break;
            case 'tr':
                tip_top = hOffset.top + hHeight + pHeight + margin;
                tip_left = hOffset.left - tWidth - margin;
                pointer_top = pHeight * -1;
                pointer_left = tWidth - pWidth;
                break;
            case 'rt':
                tip_top = hOffset.top + hHeight + margin;
                tip_left = hOffset.left - tWidth - pWidth - margin;
                pointer_top = 0;
                pointer_left = tWidth;
                break;
            case 'rm':
                tip_top = hOffset.top + hHeight / 2 - tHeight / 2;
                tip_left = hOffset.left - tWidth - pWidth - margin;
                pointer_top = tHeight / 2 - pHeight / 2;
                pointer_left = tWidth;
                break;
            case 'rb':
                tip_top = hOffset.top - tHeight - margin;
                tip_left = hOffset.left - tWidth - pWidth - margin;
                pointer_top = tHeight - pHeight;
                pointer_left = tWidth;
                break;
            case 'br':
                tip_top = hOffset.top - tHeight - pHeight - margin;
                tip_left = hOffset.left - tWidth - margin;
                pointer_top = tHeight;
                pointer_left = tWidth - pWidth;
                break;
            case 'bm':
                tip_top = hOffset.top - tHeight - pHeight - margin;
                tip_left = hOffset.left + hWidth / 2 - tWidth / 2;
                pointer_top = tHeight;
                pointer_left = tWidth / 2 - pWidth / 2;
                break;
            case 'bl':
                tip_top = hOffset.top - tHeight - pHeight - margin;
                tip_left = hOffset.left + hWidth + margin;
                pointer_top = tHeight;
                pointer_left = 0;
                break;
            case 'lb':
                tip_top = hOffset.top - tHeight - margin;
                tip_left = hOffset.left + hWidth + pWidth + margin;
                pointer_top = tHeight - pHeight;
                pointer_left = pWidth * -1;
                break;
            case 'lm':
                tip_top = hOffset.top + hHeight / 2 - tHeight / 2;
                tip_left = hOffset.left + hWidth + pWidth + margin;
                pointer_top = tHeight / 2 - pHeight / 2;
                pointer_left = pWidth * -1;
                break;
            case 'lt':
                tip_top = hOffset.top + hHeight + margin;
                tip_left = hOffset.left + hWidth + pWidth + margin;
                pointer_top = 0;
                pointer_left = pWidth * -1;
                break;
            }
            api.tip.css('top', tip_top).css('left', tip_left);
            api.pointer.css('top', pointer_top).css('left', pointer_left);
        };
        if (t.config.listener != 'show') {
            $.Drop(t.handle, t.tip, {
                animation_speed: t.config.animation_speed,
                show_delay: t.config.show_delay,
                hide_delay: t.config.hide_delay,
                listener: t.config.listener,
                callbacks: {
                    beforeShow: function(drop_api) {
                        if (t.config.refresh) {
                            t.tip.find('.tip_content').html(t.config.callbacks.content(t));
                        }
                    },
                    onShow: function(drop_api) {
                        t.position(t);
                        if (t.config.solo) {
                            t.visible_tips = $('.tip.show_listener:visible');
                            t.visible_tips.hide();
                        }
                    },
                    beforeHide: function(drop_api) {},
                    onHide: function(drop_api) {
                        if (t.config.solo) {
                            t.visible_tips.show();
                        }
                    }
                }
            });
        }
        else {
            t.position(t);
        }
        t.handle.data('tip', t);
        return t;
    };

    $.fn.Tip = function(config) {
        var config = config || {};
        return this.each(function() {
            new Tip(this, config);
        });
    };

    var Utils = {
        object: {
            size: function(obj) {
                var count = 0,
                key;
                for (key in obj) {
                    if (obj.hasOwnProperty(key)) {
                        count++;
                    }
                }
                return count;
            }
        },
        validation: {
            test: {
                required: function(field, validation) {
                    if ($.trim(field.val()) || false) {
                        /// RENAME THESE TO field, validation
                        if (field.type == 'checkbox' && $.trim(field.val()) == 0) {
                            return false;
                        }
                        return true;
                    }
                    return false;
                },
                alpha: function(field, validation) {
                    if (field.val() || false) {
                        var pattern = /[a-z- ]+$/i;
                        return pattern.test(field.val());
                    }
                    return true;
                },
                numeric: function(field, validation) {
                    if (field.val() || false) {
                        var pattern = /[0-9]+$/;
                        return pattern.test(field.val());
                    }
                    return true;
                },
                alphanumeric: function(field, validation) {
                    if (field.val() || false) {
                        var pattern = /[a-z0-9 ]+$/i;
                        return pattern.test(field.val());
                    }
                    return true;
                },
                minlen: function(field, validation) {
                    if (field.val() || false) {
                        if (field.val().length >= parseInt(validation.minlen)) {
                            return true;
                        }
                        return false;
                    }
                    return true;
                },
                len: function(field, validation) {
                    if (field.val() || false) {
                        if (field.val().length == parseInt(validation.len)) {
                            return true;
                        }
                        return false;
                    }
                    return true;
                },
                maxlen: function(field, validation) {
                    if (field.val() || false) {
                        if (field.val().length <= parseInt(validation.maxlen)) {
                            return true;
                        }
                        return false;
                    }
                    return true;
                },
                match: function(field, validation) {
                    // test this
                    if (field.val() || false) {
                        if (field.val() == field.form.fields[validation.match].val()) {
                            return true;
                        }
                        return false;
                    }
                    return true;
                },
                email: function(field, validation) {
                    if (field.val() || false) {
                        var pattern = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
                        return pattern.test(field.val());
                    }
                    return true;
                },
                phone: function(field, validation) {
                    // do this later
                    },
                url: function(field, validation) {
                    // do this later
                    },
                ip: function(field, validation) {
                    // do this later
                    },
                postal_code: function(field, validation) {
                    // do this later
                    },
                ca_postal_code: function(field, validation) {
                    // do this later (canada postal code)
                    }
            },
            bind: {
                alpha: function(handle) {
                    handle.bind({
                        keydown: function(e) {
                            var charv = String.fromCharCode(e.which);
                            var pattern = /[a-z]$/ig;
                            if ((e.shiftKey == true && e.which in {
                                48: '',
                                49: '',
                                50: '',
                                51: '',
                                52: '',
                                53: '',
                                54: '',
                                55: '',
                                56: '',
                                57: ''
                            }) || e.which in {
                                107: '',
                                109: ''
                            } || (pattern.test(charv) == false && !(e.which in {
                                8: '',
                                9: '',
                                13: '',
                                32: '',
                                35: '',
                                36: '',
                                37: '',
                                38: '',
                                39: '',
                                40: '',
                                46: ''
                            }))) {
                                return false;
                            }
                        }
                    });
                },
                numeric: function(handle) {
                    handle.bind({
                        keydown: function(e) {
                            var charv = String.fromCharCode(e.which);
                            var pattern = /[0-9]$/g;
                            if ((e.shiftKey == true && e.which in {
                                48: '',
                                49: '',
                                50: '',
                                51: '',
                                52: '',
                                53: '',
                                54: '',
                                55: '',
                                56: '',
                                57: ''
                            }) || e.which in {
                                107: '',
                                109: ''
                            } || (pattern.test(charv) == false && !(e.which in {
                                8: '',
                                9: '',
                                13: '',
                                32: '',
                                35: '',
                                36: '',
                                37: '',
                                38: '',
                                39: '',
                                40: '',
                                46: ''
                            }))) {
                                return false;
                            }
                        }
                    });
                },
                alphanumeric: function(handle) {
                    handle.bind({
                        keydown: function(e) {
                            var charv = String.fromCharCode(e.which);
                            var pattern = /[a-z0-9]$/ig;
                            if ((e.shiftKey == true && e.which in {
                                48: '',
                                49: '',
                                50: '',
                                51: '',
                                52: '',
                                53: '',
                                54: '',
                                55: '',
                                56: '',
                                57: ''
                            }) || e.which in {
                                107: '',
                                109: ''
                            } || (pattern.test(charv) == false && !(e.which in {
                                8: '',
                                9: '',
                                13: '',
                                32: '',
                                35: '',
                                36: '',
                                37: '',
                                38: '',
                                39: '',
                                40: '',
                                46: ''
                            }))) {
                                return false;
                            }
                        }
                    });
                },
                maxlen: function(field, maxlen) {
                    field.handle.bind({
                        keydown: function(e) {
                            if (field.val().length + 1 > maxlen && !(e.which in {
                                8: '',
                                9: '',
                                13: '',
                                35: '',
                                36: '',
                                37: '',
                                38: '',
                                39: '',
                                40: '',
                                46: ''
                            })) {
                                return false;
                            }
                        }
                    });
                }
            }
        }
    };

    // TO DO's
    // default messages if not supplied? and allow to pass validation as just a string
    // callbacks on fields for events like focus, keydown, etc..
    // auto increment (add another artist)
    // submit and reset buttons?
    // password meter
    // captcha
    // Masking
    // Multi Select
    // Multi Checkbox
    var Form = function(handle, fields, default_values, config) {
        var t = this;
        t.handle = $(handle);
        t.handle.addClass('form_handle');
        t.default_values = default_values;
        t.config = {
            action: '',
            method: 'post',
            ajax: false,
            clear: false,
			model: '',
            callbacks: {
				onInit: function(api){},
                onSubmit: function(api) {
                    // overwrite this if you'd like to do something extremely custom
                    if ((api.config.action || false) && api.valid) {
                        // see if we have an action defined to do auto submit
                        if (api.config.ajax) {
							if(api.config.model || false){
								var data = {};
								data[api.config.model] = api.data;
							}
							else{
								var data = api.data;
							}
                            if (api.config.method == 'post') {
                                $.post(api.config.action, data,
                                function(r) {
                                    api.config.callbacks.onResponse(api, r);
                                });
                            }
                            else if (api.config.method == 'get') {
                                $.get(api.config.action, data,
                                function(r) {
                                    api.config.callbacks.onResponse(api, r);
                                });
                            }
                            else if (api.config.method == 'getJSON') {
                                $.getJSON(api.config.action, data,
                                function(r) {
                                    api.config.callbacks.onResponse(api, r);
                                });
                            }
                        }
                        else {
                            api.handle.find('form').submit();
                        }
                    }
                },
                onResponse: function(t, response) {},
                // this callback is only useful if you use auto submit with an ajax request and want to do something with the response data
                onValidation: function(field_api, form_api) {}
            },
            tip_config: {
                // standard tip config goes here from above plugin if you wish to override
                pointer: 'lm',
                pointer_color: '#CCCCCC',
                listener: 'focus',
                show_delay: 0,
                hide_delay: 0,
                margin: 5,
                add_class: 'form_tip'
            },
            error_tip_config: {
                pointer: 'lm',
                refresh: true,
                listener: 'hover',
                margin: 0,
                pointer_color: '#990000',
                add_class: 'form_tip error_tip'
            }
        };
        $.extend(true, t.config, config);
        t.id = Math.floor(Math.random() * 100000000000);
        t.valid = false;
        t.fields = {};
        t.data = {};
		// build html
		t.html = '';
        $.each(fields,
        function() {
			if('html' in this){
				t.html += this.html;
			}
			else{
           		var field = this;
	            field.form = t;
                if(this.type == 'group'){
                    var f = new Group(field);
                }
                else{
                    var f = new Field(field);
                }
	            t.fields[f.name] = f;
				t.html += f.html;
			}
        });
		t.handle.html(t.html);
		// init fields
		$.each(t.fields, 
		function() {
			this.init();
		});
        // wrap form (if applicable)
        if (! (t.config.ajax)) {
            t.handle.wrapInner('<form method="' + t.config.method + '" action="' + t.config.action + '" />');
        }
        t.validate = function() {
            var valid = true;
            $.each(t.fields,
            function() {
                if (! (this.validate())) {
                    valid = false;
                }
            });
            t.valid = valid;
            return valid;
        };
        t.getData = function() {
            t.validate();
            var data = {};
            $.each(t.fields,
            function() {
                data[this.name] = this.val();
            });
            t.data = data;
            return t.data;
        };
        t.submit = function() {
            t.validate();
            var data = {};
            $.each(t.fields,
            function() {
                data[this.name] = this.val();
            });
            t.data = data;
            t.config.callbacks.onSubmit(t);
            return t.data;
        };
        t.handle.data('form', t);
		t.config.callbacks.onInit(t);
        return t;
    };

    var Group = function(group) {
        var t = this;
        t.visible = true;
        t.label = null;
        t.default_values = null;
        t.add_class = null;
        $.extend(true, t, group);
        t.valid = false;
        t.errors = [];
        t.fields_config = t.fields;
        t.fields = [];
        t.id = t.name + '_' + t.form.id;
        t.count = 1;
        if(t.default_values || false){
            t.count = t.default_values.length;
        }
        var html = '';
        for(var i = 0; i < t.count; i++){
            var fields = {};
            html += '<div class="group_section">';
            $.each(t.fields_config, function(){
                var field = this;
	            field.form = t.form;
                field.group = t;
                field.group_index = t.fields.length;
                // default_value
                if((t.default_values || false) && (t.default_values.length >= i)){
                    if(field.name in t.default_values[i]){
                        field.default_value = t.default_values[i][field.name];
                    }
                }
                var f = new Field(field);
	            fields[f.name] = f;
				html += f.html;
            }); 
            if(i > 0){
                html += '<a href="#" class="group_remove" index="'+i+'">&nbsp;</a></div>';
            }
            else{
                html += '</div>';
            }
            t.fields.push(fields);
        }
        t.html = '<div class="form_section group :add_class:" id=":id:">:label:<div class="form_section group_fields">:fields:</div><div class="form_section"><a href="#" class="add_set">+ Add</a></div></div>'
        .replace(/:add_class:/g, function(){
            if(t.add_class || false){
                return t.add_class;
            }
            return '';
        })
        .replace(/:id:/g, t.id)
        .replace(/:label:/g, function(){
            if(t.label || false){
                return '<div class="form_label unselectable"><label for=":id:_field">' + t.label + '</label></div>';
            }
            return '';
        })
        .replace(/:fields:/g, html);
        t.init = function(){
            t.handle = $('#'+t.id);
            //bind to add button
            t.handle.find('.add_set').bind({
                click: function(){
                    t.add();
                    return false;
                }   
            });
            t.handle.find('.group_remove').bind({
                click: function(){
                    var th = $(this);
                    t.remove(th.attr('index'));
                    th.parents('.group_section').remove();
                    return false;
                }
            });
            $.each(t.fields, function(){
                var fields = this;
                $.each(fields, function(){
                    this.init();
                });
            });
	        t.hide = function() {
            t.visible = false;
            t.handle.hide();
            return t.handle;
	        };
	        t.show = function() {
          t.visible = true;
	            t.handle.show();
              return t.handle;
	        };
          if(!(t.visible || false)){
            t.hide();
          }
        }
        t.validate = function(){
            var valid = true;
            $.each(t.fields, function(){
                var fields = this;
                $.each(fields, function(){
                    if(!(this.validate())){
                        valid = false;
                    }
                });
            });
            t.valid = valid;
            return valid;
        }
        t.val = function(){
            var data = [];
            $.each(t.fields, function(){
                var fields = this;
                var fd = {};
                $.each(fields, function(){
                    fd[this.name] = this.val();
                });
                data.push(fd);
            });
            return data;
        }
        t.add = function(){
            var fields = {};
            var html = '<div class="group_section">';
            var group_index = t.count;
            t.count++;
            $.each(t.fields_config, function(){
                var field = this;
	            field.form = t.form;
                field.group = t;
                field.group_index = group_index;
                field.default_value = null; // how do we fix this for defaut values when we want them
                var f = new Field(field);
	            fields[f.name] = f;
				html += f.html;
            }); 
            html += '<a href="#" class="group_remove" index="'+group_index+'">&nbsp;</a></div>';
            t.fields.push(fields);
            t.handle.find('.group_fields').append(html);
            $.each(fields, function(){
                this.init();
            });
            t.handle.find('.group_remove:last').bind({
                click: function(){
                    var th = $(this);
                    t.remove(th.attr('index'));
                    th.parents('.group_section').remove();
                    return false;
                }
            });
            if('callbacks' in t){
                if('onAdd' in t.callbacks){
                    t.callbacks.onAdd();
                }
            }

        }
        t.remove = function(index){
            var i = 0;
            var todel = null;
            $.each(t.fields, function(){
                var fields = this;
                $.each(fields, function(){
                    if(this.group_index == parseInt(index)){
                        todel = i;
                    }
                });
                i++;
            });
            t.fields.splice(todel, 1);
        }
    };

    var Field = function(field) {
        var t = this;
		t.visible = true;
        t.label = null;
        t.default_value = null;
        t.add_class = null;
        t.tab_index = 0;
        t.group = null;
        t.options = [];
		t.validation = [];
        // used for radio and selects
        t.callbacks = {
            // figure these out
            onHover: function() {

            }
        };
        $.extend(true, t, field);
        if (! (t.default_value || false) && t.name in t.form.default_values) {
            t.default_value = t.form.default_values[t.name];
        }
        t.valid = false;
        t.errors = [];
        if (! (t.clear || false)) {
            t.clear = t.form.config.clear;
        }
		if (! (t.model || false)) {
            t.model = t.form.config.model;
        }
        if(t.group || false){
            t.id = t.name + '_' + t.group_index + '_' +t.form.id;
        }
        else{
            t.id = t.name + '_' + t.form.id;
        }
        var templates = {
            section: '<div class="form_section :type: :add_class:" id=":id:">:label:<div class="form_field">:field:</div><span class="form_error_tip">&nbsp;</span><span class="form_success_check">&nbsp;</span>:capslock:</div>',
            label: '<div class="form_label unselectable"><label for=":id:_field">:label:</label></div>',
            input: '<input type=":type:" name=":field_name:" id=":id:_field" tabindex=":tab_index:" value=":default_value:" />',
            // text, password, also used for date
            textarea: '<textarea name=":field_name:" id=":id:_field" tabindex=":tab_index:">:default_value:</textarea>',
            // also considered an input
            select: '<input name=":field_name:" type="hidden" id=":id:_field" value=":default_value:" class="form_hidden" /><div class="form_select unselectable" tabindex=":tab_index:"><div class="select_text">select</div><div class="select_arrow">&nbsp;</div><div class="select_options">:options:</div></div>',
            radio: '<input name=":field_name:" type="hidden" id=":id:_field" value=":default_value:" class="form_hidden" /><div class="form_radio unselectable">:options:</div>',
            option: '<div tabindex=":tab_index:" class="option_handle" value=":value:">:option:</div>',
            checkbox: '<input name=":field_name:" type="hidden" id=":id:_field" value=":default_value:" class="form_hidden" /><div tabindex=":tab_index:" class="checkbox_handle unselectable">:label:</div>'
        };
        var options = function() {
            var html = '';
            $.each(t.options,
            function() {
                if (t.type in {
                    radio: '',
                    select: ''
                }) {
                    html += templates.option.replace(/:value:/g, this[0]).replace(/:option:/g, this[1]);
                }
            });
            return html;
        };
        t.html = templates.section
        .replace(/:add_class:/g,
        function() {
            if (t.add_class || false) {
                return t.add_class;
            }
            return '';
        })
        .replace(/:label:/g,
        function() {
            if (t.type != 'checkbox' && (t.label || false)) {
                return templates.label;
            }
            return '';
        })
        .replace(/:field:/g,
        function() {
            switch (t.type) {
            default:
                return templates[t.type];
            case 'text':
            case 'password':
                return templates.input;
                break;
            }
        })
        .replace(/:options:/, options())
        .replace(/:type:/g, t.type)
        .replace(/:label:/g, t.label)
        .replace(/:id:/g, t.id)
		.replace(/:field_name:/g, function(){
            var name = '';
            if((t.model || false) && (t.group || false)){
                return t.model + '[' + t.group.name + ']' + '[' + t.group_index + ']' + '[' + t.name + ']';
            }
            else if(t.group || false){
                return t.group.name + '[' + t.group_index + ']' + '[' + t.name + ']';
            }
            else if(t.model || false){
				return t.model + '[' + t.name + ']';
			}
			return t.name;
		})
		.replace(/:capslock:/g, function(){
			if(t.type == 'password'){
				return '<div class="note capslock">Your Capslock is <strong>On</strong></div>';
			}
			return '';
		})
        .replace(/:tab_index:/g, t.tab_index)
        .replace(/:default_value:/g,
        function() {
            if (t.default_value || false) {
                return t.default_value;
            }
            return '';
        });
		t.init = function(){
        	t.handle = $('#' + t.id);
	        t.hide = function() {
				t.visible = false;
	            t.handle.hide();
                return t.handle;
	        };
	        t.show = function() {
				t.visible = true;
	            t.handle.show();
                return t.handle;
	        };
	        t.focus = function() {
	            t.handle.addClass('focus');
	            t.handle.find('[tabindex]:first').focus();
                return t.handle;
	        };
	        t.blur = function() {
	            t.handle.removeClass('focus');
                return t.handle;
	            // can we blur out here or what would we do anything?
	        };
	        t.val = function(v) {
	            if (v != undefined) {
	                switch (t.type) {
	                default:
	                    t.handle.find('input').val(v);
	                    break;
	                case 'checkbox':
	                    if (v in {
	                        1: '',
	                        '1': '',
	                        'true': '',
	                        'checked': ''
	                    } || v == true) {
	                        t.handle.addClass('checked').find('input').val('1');
	                    }
	                    else {
	                        t.handle.removeClass('checked').find('input').val('0');
	                    }
	                    break;
	                case 'radio':
	                    var checked = t.handle.find('.option_handle[value="' + v + '"]');
	                    if (checked || false) {
	                        t.handle.find('.option_handle').removeClass('checked');
	                        checked.addClass('checked');
	                        t.handle.addClass('checked').find('input').val(v);
	                    }
	                    else {
	                        t.handle.removeClass('checked');
	                        t.handle.find('.option_handle').removeClass('checked');
	                        t.handle.find('input').val('');
	                    }
	                    break;
	                case 'select':
	                    var selected = t.handle.find('.option_handle[value="' + v + '"]');
	                    if (selected || false) {
	                        t.handle.find('.option_handle').removeClass('selected');
	                        selected.addClass('selected');
	                        t.handle.find('input').val(v);
	                        t.handle.find('.select_text').html(selected.html());
	                        t.handle.addClass('selected');
	                    }
	                    else {
	                        t.handle.removeClass('selected');
	                        t.handle.find('.option_handle').removeClass('selected');
	                        t.handle.find('input').val('');
	                    }
	                    break;
	                }
	                return t;
	            }
	            else {
	                var v = t.handle.find('input, textarea').val();
	                if (t.clear && t.default_value == v) {
	                    return '';
	                }
	                return v;
	            }
	        };
	        // apply tip, pre-validation and default bindings
	        // these first bindings apply to everything
	        t.handle.find('input, textarea, .checkbox_handle, .form_radio, .form_select').bind({
	            mouseenter: function() {
	                t.handle.addClass('hover');
	            },
	            mouseleave: function() {
	                t.handle.removeClass('hover');
	            }
	        });
	        switch (t.type) {
	        case 'text':
	        case 'password':
	        case 'textarea':
	            if (t.tip || false) {
	                t.handle.find('input, textarea').attr('tip', t.tip).Tip(t.form.config.tip_config);
	            }
	            $.each(t.validation,
	            function() {
	                if (typeof this == 'object') {
	                    if (this.type in Utils.validation.bind) {
	                        switch (this.type) {
	                        default:
	                            Utils.validation.bind[this.type](t.handle);
	                            break;
	                        case 'maxlen':
	                            Utils.validation.bind[this.type](t, this[this.type]);
	                            break;
	                        }
	                    }
	                }
	            });
	            t.handle.find('input, textarea').bind({
	                focusin: function() {
	                    var o = $(this);
	                    t.handle.addClass('focus').removeClass('success').removeClass('error');
	                    if (t.clear) {
	                        if (o.val() == t.default_value) {
	                            o.val('');
	                        }
	                    }
	                },
	                focusout: function() {
	                    var o = $(this);
	                    t.handle.removeClass('focus');
	                    if (t.default_value) {
	                        if (o.val() == t.default_value || o.val() == '') {
	                            t.handle.addClass('has_default');
	                        }
	                        else {
	                            t.handle.removeClass('has_default');
	                        }
	                    }
	                    if (t.clear) {
	                        if (o.val() == t.default_value || o.val() == '') {
	                            o.val(t.default_value);
	                        }
	                    }
						if(t.type == 'password'){
							t.handle.removeClass('capslock');
						}
	                    t.validate();
	                },
	                keydown: function(e) {
	                    if (e.which == 13) {
	                        t.form.submit();
	                    }
						// capslock check, this has to stay in keydown not keypress..
						if (e.which == 20){
							if (t.handle.hasClass('capslock')){
								t.handle.removeClass('capslock');
							}
							else{
								t.handle.addClass('capslock');
							}
						}
	                },
					keypress: function(e){
						// capslock
						if (t.type == 'password'){
							var s = String.fromCharCode( e.which );
						    if ( s.toUpperCase() === s && s.toLowerCase() !== s && !e.shiftKey ) {
						        t.handle.addClass('capslock');
						    }
						}
					}
	            });
	            break;
	        case 'checkbox':
	            if (t.tip || false) {
	                t.handle.find('.checkbox_handle').attr('tip', t.tip).Tip($.extend(true, {},
	                t.form.config.tip_config, {
	                    listener: 'hover'
	                }));
	            }
	            t.handle.find('.checkbox_handle').bind({
	                click: function() {
	                    if (t.handle.hasClass('checked')) {
	                        t.val('0');
	                    }
	                    else {
	                        t.val('1');
	                    }
	                    t.validate();
	                    return false;
	                },
	                keydown: function(e) {
	                    if (e.which == 32) {
	                        $(this).click();
	                    }
	                    else if (e.which == 13) {
	                        t.form.submit();
	                    }
	                },
	                focusin: function() {
	                    t.handle.addClass('focus');
	                },
	                focusout: function() {
	                    t.handle.removeClass('focus');
	                }
	            });
	            break;
	        case 'radio':
	            if (t.tip || false) {
	                t.handle.find('.form_radio').attr('tip', t.tip).Tip($.extend(true, {},
	                t.form.config.tip_config, {
	                    listener: 'hover'
	                }));
	            }
	            t.handle.find('.option_handle').bind({
	                click: function() {
	                    var o = $(this);
	                    t.handle.find('.option_handle.checked').removeClass('checked');
	                    o.addClass('checked');
	                    t.handle.addClass('checked');
	                    t.handle.find('input').val(o.attr('value'));
	                    t.validate();
	                    return false;
	                },
	                keydown: function(e) {
	                    if (e.which == 32) {
	                        $(this).click();
	                    }
	                    else if (e.which == 13) {
	                        t.form.submit();
	                    }
	                    else if (e.which == 37) {
	                        var prev_item = $(this).prev();
	                        if (prev_item.length > 0) {
	                            $(this).removeClass('focus');
	                            prev_item.focus().addClass('focus');
	                            t.handle.addClass('focus');
	                        }
	                    }
	                    else if (e.which == 39) {
	                        var next_item = $(this).next();
	                        if (next_item.length > 0) {
	                            $(this).removeClass('focus');
	                            next_item.focus().addClass('focus');
	                            t.handle.addClass('focus');
	                        }
	                    }
	                },
	                mouseenter: function() {
	                    var o = $(this);
	                    o.addClass('hover');
	                },
	                mouseleave: function() {
	                    var o = $(this);
	                    o.removeClass('hover');
	                },
	                focusin: function() {
	                    var o = $(this);
	                    o.addClass('focus');
	                    t.handle.addClass('focus');
	                },
	                focusout: function() {
	                    var o = $(this);
	                    o.removeClass('focus');
	                    t.handle.removeClass('focus');
	                }
	            });
	            break;
	        case 'select':
	            if (t.tip || false) {
	                t.handle.find('.form_select').attr('tip', t.tip).Tip($.extend(true, {},
	                t.form.config.tip_config, {
	                    listener: 'focus'
	                }));
	            }
	            $.Drop(t.handle, t.handle.find('.select_options'), {
	                animation_speed: 0,
	                show_delay: 0,
	                hide_delay: 0,
	                listener: 'click',
	                callbacks: {
	                    beforeShow: function(api) {
	                        t.handle.addClass('click');
	                    },
	                    onShow: function(api) {
	                        t.handle.find('.option_handle:first').focus();
	                    },
	                    onHide: function(api) {
	                        t.handle.removeClass('click');
	                    }
	                }
	            });
	            t.handle.find('.form_select').bind({
	                focusin: function() {
	                    t.handle.addClass('focus');
	                },
	                focusout: function() {
	                    t.handle.removeClass('focus');
	                },
	                keydown: function(e) {
	                    if (e.which == 32) {
	                        $(this).click();
	                    }
	                    else if (e.which == 40) {
	                        if (! (t.handle.hasClass('click'))) {
	                            $(this).click();
	                        }
	                    }
	                    else if (e.which == 13) {
	                        t.form.submit();
	                    }
	                }
	            });
	            t.handle.find('.option_handle').bind({
	                click: function() {
	                    var o = $(this);
	                    t.handle.find('input').val(o.attr('value'));
	                    t.handle.find('.select_text').html(o.html());
	                    t.handle.find('.form_select').focus();
	                    t.handle.addClass('focus');
	                    t.validate();
	                    // don't return false here like usual
	                },
	                keydown: function(e) {
	                    if (e.which == 32) {
	                        $(this).removeClass('focus').click();
	                    }
	                    else if (e.which == 13) {
	                        t.form.submit();
	                    }
	                    else if (e.which == 38) {
	                        var prev_item = $(this).prev();
	                        if (prev_item.length > 0) {
	                            $(this).removeClass('focus');
	                            prev_item.focus().addClass('focus');
	                        }
	                    }
	                    else if (e.which == 40) {
	                        var next_item = $(this).next();
	                        if (next_item.length > 0) {
	                            $(this).removeClass('focus');
	                            next_item.focus().addClass('focus');
	                        }
	                    }
	                },
	                mouseenter: function() {
	                    var o = $(this);
	                    o.addClass('hover');
	                },
	                mouseleave: function() {
	                    var o = $(this);
	                    o.removeClass('hover');
	                },
	                focusin: function() {
	                    var o = $(this);
	                    o.addClass('focus');
	                    t.handle.addClass('focus');
	                },
	                focusout: function() {
	                    var o = $(this);
	                    o.removeClass('focus');
	                    t.handle.removeClass('focus');
	                }
	            });
	            break;
	        }
	        // default value
	        if (t.default_value != undefined) {
	            t.handle.addClass('has_default');
	            t.val(t.default_value);
	        }
	        // apply tip to error
	        // put check here later if we want to disable tips in general through config
	        t.handle.find('.form_error_tip').Tip($.extend(true, {},
	        t.form.config.error_tip_config, {
	            callbacks: {
	                content: function(api) {
	                    return t.errors.join('<br />');
	                }
	            }
	        }));
	        t.validate = function() {
	            var val = t.val();
	            var valid = true;
	            t.errors = [];
	            $.each(t.validation,
	            function() {
	                var test = null;
	                switch (typeof this) {
	                case 'function':
	                    test = this(val);
	                    if (test != true) {
	                        this.message = test;
	                    }
	                    break;
	                case 'object':
	                    if (this.type in Utils.validation.test) {
	                        test = Utils.validation.test[this.type](t, this);
	                    }
	                    else {
	                        // do we throw error here? ehhhhh
	                        }
	                    break;
	                }
	                if (test != true) {
	                    t.errors.push(this.message);
	                    valid = false;
	                }
	            });
	            t.valid = valid;
	            t.form.config.callbacks.onValidation(t, t.form);
	            if (t.valid) {
	                t.handle.addClass('success').removeClass('error');
	                return t.valid;
	            }
	            else {
	                t.handle.addClass('error').removeClass('success');
	                return t.valid;
	            }
	        }
			// show or hide default
			if(!(t.visible || false)){
				t.hide();
			}
	        t.handle.data('field', t);
		}
        return t;
    };

    $.fn.Form = function(action, data) {
        var fields = data.fields || {};
        var default_values = data.default_values || {};
        var config = data.config || {};
        return this.each(function() {
            switch (action) {
            case 'init':
                new Form(this, fields, default_values, config);
                break;
            }
        });
    };
})(jQuery);
