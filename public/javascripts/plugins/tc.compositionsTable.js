;(function( $ ){

  $.fn.compositionsTable = function( options ) {
    var settings = $.extend({
      spinner: '/images/spinner02.gif',
      pagination_link: '.pagination a',
      select_page_link: '.select_page',
      more_results_link: '.per_page',
      sort_link: '.sort',
      page: '.page',
      columns: '4',
      url: '/paginate'
    }, options);
    var $container = this;
    
    return this.each(function() {
      var self = {
        init: function() {
          $(document).on('click', settings.pagination_link, self.get_page);
          $(document).on('change', settings.select_page_link, self.select_page);
          $(document).on('change', settings.more_results_link, self.get_more_results);
          $(document).on('click', settings.sort_link, self.get_page);
        },
        
        get_page: function(e) {
          e.preventDefault();
          var temp_url = $(settings.page).val(),
          params = $.param.querystring(temp_url),
          per_page = $(settings.more_results_link).val();
          params = "per_page=" + per_page;
          self.results($(this).attr("href"), params);
        },
        
        select_page: function(e) {
          e.preventDefault();
          var temp_url = $(settings.page).val(),
          params = $.param.querystring(temp_url),
          page = $(settings.select_page_link).val();
          params = params + "&page=" + page;
          self.results(settings.url, params);
        },
        
        get_more_results: function(e) {
          e.preventDefault();
          var temp_url = $(settings.page).val(),
          params = $.param.querystring(temp_url),
          page = "1",
          per_page = $(settings.more_results_link).val();
          params = params + "&page=" + page + "&per_page=" + per_page;
          self.results(settings.url, params);
        },
        
        results: function(url, data) {
          $container.find('tbody').html('<tr><td colspan="' + settings.columns + '" style="text-align: center;"><img src="' + settings.spinner + '" alt="" /></td></tr>');
          $.ajax({
            type: "GET",
            url: url,
            data: data,
            success: function(html) {
              $container.html(html);
            }
          });
        }
      };
      return self.init();
    });
  };

})( jQuery );
