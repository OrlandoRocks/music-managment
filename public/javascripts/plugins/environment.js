
/* 
 * Refresh the current window using javascript 
 */
function upload_refresh(){
  window.location.reload();
}

/*
 *  Setup jQuery to not conflict w/ Prototype's $ syntax
 */
jQuery.noConflict();

/*
 *  Setup jQuery Ajax to send text/javascript header 
 */
jQuery.ajaxSetup({ 'beforeSend': function(xhr) {xhr.setRequestHeader('Accept', 'text/javascript')} });

