// ----------------------------------------------------------------------------
// markItUp!
// ----------------------------------------------------------------------------
// Copyright (C) 2011 Jay Salvat
// http://markitup.jaysalvat.com/
// ----------------------------------------------------------------------------
// Html tags
// http://en.wikipedia.org/wiki/html
// ----------------------------------------------------------------------------
// Basic set. Feel free to add more tags
// ----------------------------------------------------------------------------
var mySettings = {
	onShiftEnter:  	{keepDefault:false, replaceWith:'<br />\n'},
	onCtrlEnter:  	{keepDefault:false, openWith:'\n<p>', closeWith:'</p>'},
	onTab:    		{keepDefault:false, replaceWith:'    '},
	markupSet:  [
		{name:'Artist Name', replaceWith:"<%=@artist_name%>"},
		{name:'Album Name', replaceWith:"<%=@album_name%>"},
		{name:'Person Name', replaceWith:"<%=@person_name%>"},
    {name:'Email Note', replaceWith:"<%=@email_note%>"},
    {name:'UPC', replaceWith:"<%=@upc%>"}
	]
}
