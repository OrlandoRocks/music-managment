/*
 * jQuery filter :icontains. Same as :contains but case insensitive.
 */
$.expr[':'].icontains = function(obj, index, meta, stack){
    return (obj.textContent || obj.innerText || jQuery(obj).text() || '')
     .toLowerCase()
     .indexOf(meta[3].toLowerCase()) >= 0;
};

/*
 * Tablesorter widget to filter table rows by its content.
 */
$.tablesorter.addWidget({ 
    /*
     * Property: id
     * The widget id.
     */
    id: 'filter',
    
    defaults: {
        title: 'Search...',
        container: false
    },
    
    /*
     * Method: init
     * Constructor.
     *
     * Parameters:
     *  table - {HTML Object} The table.
     */
    init: function(table) {
        var _this = this; 
        
        // Merge config.
        table.config.filter = $.extend({}, this.defaults, table.config.filter);

        var title = table.config.filter.title;
        var filterName = $(table).attr('id');
        var filterId = 'tsFilter_' +filterName;

        // Make filter input.
        var f = [];
        f.push('<input class="tablesorter tsFilter');
        if(title)
            f.push(' empty');
        f.push('"');
        f.push(' id="' +filterId +'"');
        f.push(' type="text"');
        f.push(' name="' +filterName +'"');
        if(title){
            f.push(' value="' +title +'"');
            f.push(' title="' +title +'"');
        }
        f.push(' />');
            
        var filterInput = $('input#' +filterId);
        
        // Insert filter input if not already added.
        if($(filterInput).length == 0){
            if(table.config.filter.container)
                $(table.config.filter.container).html(f.join(''));
            else
                $(table).before(f.join(''));
        }
        
        var filterInput = $('input#' +filterId);
             
        // Enable input value on focusin/ out
        if(title){
            $(filterInput)
             .unbind('focusout.tablesorter.filter')
             .unbind('focusin.tablesorter.filter')
             .bind('focusout.tablesorter.filter', function(e){
                if($(this).val() == ''){
                    $(this).addClass('empty');
                    $(this).val(title);
                }
            }).bind('focusin.tablesorter.filter', function(e){
                if($(this).hasClass('empty')){
                    $(this).removeClass('empty');
                    $(this).val('');
                }
            });
        }
                
        // Filter table on every keyup event.
        $(filterInput)
         .unbind('keyup.tablesorter.filter')
         .bind('keyup.tablesorter.filter', function(e){
            if(!$(this).hasClass('empty'))
                _this.filter(table, $(this).val());
        });
        
        // Apply filter on every table update.
        $(table)
         .unbind('update.tablesorter.filter')
         .bind('update.tablesorter.filter', function(e){
            if(!$(filterInput).hasClass('empty'))
                _this.filter(table, $(filterInput).val());
        });
        
    },
    
    /*
     * Method: filter
     * Filter the table contents by string.
     * 
     * Parameters:
     *     table - {HTML Object} The table.
     *     str - {String} The string to filter on.
    */ 
    filter: function(table, str){
        // Convert input string into array in case of multiple words
        var array = str.split(' ');
        // Reset table to show all entries
        $('tbody tr:hidden', table).show();
        /* 
         * For each word of input string, hide unrelated table rows that's still showing
         * this will allow out of order filtering of multiple words
         * ex:
         *     We are filtering for this string "tunecore nav".
         *     We have this list of rows:
         *     |------------------------------|
         *     |  homepage_copy_tunecore_neo  |
         *     |  homepage_nav_tunecore_neo   |
         *     |  preview_faq_tunecore_neo    |
         *     |------------------------------|
         *
         *     The filtering will show the second item only, even with the words out of
         *     order on the filtering string.
         */
        $.each(array, function(index, value) {
          $('tbody tr:not(:icontains("' + value + '"))', table).hide();
        });
    },
    
    /*
     * Method: format
     * Will be called on init.
     */
    format: function(table){
        this.init(table);
    }
    
});
