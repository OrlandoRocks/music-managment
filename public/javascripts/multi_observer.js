Abstract.TimedMultiObserver = function() {}
Abstract.TimedMultiObserver.prototype = {
  initialize: function(elements, frequency, callback) {
    this.frequency = frequency;
    this.elements = new Array();
    this.values = new Array();
    this.callback  = callback;
for(i=0;i<elements.length;i++) {
        var e = elements[i];
    this.elements[i] = e;
    this.values[i] = this.getValue(e);
    }
    this.registerCallback();
  },

  registerCallback: function() {
    setInterval(this.onTimerEvent.bind(this), this.frequency * 1000);
  },

  onTimerEvent: function() {
    var changed = 0;
    for(i=0;i<this.elements.length;i++) {
    var value = this.getValue(this.elements[i]);
    if (this.values[i] != value) {
      changed = 1;
      this.values[i] = value;
    }
    }
    if (changed) {
    this.callback(this.elements, this.values);

    }
  }
}

Form.Element.MultiObserver = Class.create();
Form.Element.MultiObserver.prototype = Object.extend(new Abstract.TimedMultiObserver(), {
  getValue: function(element) {
    return Form.Element.getValue(element);
  }
});