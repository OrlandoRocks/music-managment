/* Sound Manager Module */

jQuery(document).ready(function($){

  window.TC = window.TC || {};

  TC.SM = function(config){
    var t = this;
    this.initConfig(config)
        .initSM();
    this.sounds = [];
    this.player = $(this.cssPlayerContainerSelector);
    this.needFlash = $('#sm_player-need_flash');
    this.needFlashCloseButton = $('.sm_player-need_flash_close');
    this.scrubber = $('.sm_player-scrubber');
    this.loadProgress = $('.sm_player-scrubber-load-progress');
    this.playProgress = $('.sm_player-scrubber-play-progress');
    this.playTime = $('.sm_player-play-time');
    this.duration = $('.sm_player-duration');
    soundManager.onready(function(){
      t.initSounds();
    });
    this.bind();
    return this;
  };

  TC.SM.prototype = {

    initConfig: function(config){
      return $.extend(this, {
               cssPlayerContainerSelector: '.sm_player',
               cssContainerSelector: 'body',
               cssSoundSelector: '.sm_sound',
               metadata: ['sm_player-song_id',
                          'sm_player-is_favorite',
                          'sm_player-artwork',
                          'sm_player-title',
                          'sm_player-artist_name',
                          'sm_player-album',
                          'sm_player-composers',
                          'sm_player-composer_percentage',
                          'sm_player-label_copy']
             }, config||{}),
             this;
    },

    initSM: function(){
      var t = this;
      soundManager.setup({
        url: '/flash/',
        flashVersion: 9,
        preferFlash: true,
        defaultOptions: {
          whileplaying: function(){
            t.updateLoadProgress()
             .updatePlayProgress();
          }
        }
      });
      return this;
    },

    initSounds: function(){
      var t = this;
      $(this.cssContainerSelector).find(this.cssSoundSelector).each(function(){
        var se = $(this);
        if(!t.soundExists(se.attr('sound'))) t.initSound(this);
      });
      return this;
    },

    initSound: function(soundElement){
      var data = this.pluckMetaDataFromSoundElementAttributes(soundElement);
      data.element = soundElement;
      this.addSound(data);
      return this;
    },

    addSound: function(metadata){
      var sound = metadata.sound,
          element = metadata.element;
      delete metadata.sound;
      delete metadata.element;
      var data = {metadata: metadata, element: element};
      data.sound = soundManager.createSound({
        id: sound,
        serverURL: this.serverURL,
        url: sound
      });
      this.sounds.push(data);
      return this;
    },

    updateCurrentSoundMetaData: function(){
      var metadata = this.pluckMetaDataFromSoundElementAttributes(this.currentSound.element);
      delete metadata.sound;
      this.currentSound.metadata = metadata;
      return this;
    },

    findSound: function(sound){
      var result = null;
      $(this.sounds).each(function(i,v){
        if(v.sound.id == sound){
          result = v;
          return !1;
        }
      });
      return result;
    },

    soundExists: function(sound){
      return !!this.findSound(sound);
    },

    pluckMetaDataFromSoundElementAttributes: function(soundElement){
      var se = $(soundElement),
          data = {};
      data.sound = se.attr('sound');
      $(this.metadata).each(function(i,v){
        data[v] = se.attr(v);
      });
      return data;
    },

    bind: function(){
      return this.bindNeedFlashDialog()
                 .bindNeedFlashCloseButton()
                 .bindToContainerForNewSoundElementInserts()
                 .bindToSoundElementsOnClick()
                 .bindToPlayControl()
                 .bindToPauseControl()
                 .bindToStopControl()
                 .bindToScrubberControl(),
             this;
    },

    bindNeedFlashDialog: function(){
      this.needFlash.dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        draggable: false,
        width: 600,
        maxWidth: 600,
        height: 300,
        maxHeight: 300
      });
      return this;
    },

    bindNeedFlashCloseButton: function(){
      var t = this;
      this.needFlashCloseButton.on('click', function(e){
        t.closeNeedFlashDialog();
        e.preventDefault();
      });
      return this;
    },

    bindToContainerForNewSoundElementInserts: function(){
      var t = this;
      $(this.cssContainerSelector).on('DOMNodeInserted', function(){
        t.initSounds();
      });
      return this;
    },

    bindToSoundElementsOnClick: function(){
      var t = this;
      $(this.cssContainerSelector).on('click', this.cssSoundSelector, function(){
        if(swfobject.hasFlashPlayerVersion('9.0.0')) t.onSoundElementClick(this);
        else t.openNeedFlashDialog();
      });
      return this;
    },

    bindToPlayControl: function(){
      var t = this;
      this.player.on('click', '.sm_player-play-control', function(e){
        $(this).hide();
        t.player.find('.sm_player-pause-control').show();
        t.resumeCurrentSound();
        e.preventDefault();
      });
      return this;
    },

    bindToPauseControl: function(){
      var t = this;
      this.player.on('click', '.sm_player-pause-control', function(e){
        $(this).hide();
        t.player.find('.sm_player-play-control').show();
        t.pauseCurrentSound();
        e.preventDefault();
      });
      return this;
    },

    bindToStopControl: function(){
      var t = this;
      this.player.on('click', '.sm_player-stop-control', function(e){
        t.removeActiveClassFromAllSound();
        t.stopCurrentSound();
        e.preventDefault();
      });
      return this;
    },

    bindToScrubberControl: function(){
      var t = this;
      this.scrubber.on('click', function(e){
        var pxOffset = e.pageX - t.scrubber.offset().left,
            percentage = pxOffset / t.scrubber.width();
        t.seekCurrentSound(percentage * t.currentSound.sound.durationEstimate);
      });
      return this;
    },

    openNeedFlashDialog: function(){
      this.needFlash.dialog('open');
      return this;
    },

    closeNeedFlashDialog: function(){
      this.needFlash.dialog('close');
      return this;
    },

    onSoundElementClick: function(soundElement){
      var se = $(soundElement),
          metadata = this.pluckMetaDataFromSoundElementAttributes(soundElement);
      if(!this.currentSound){
        se.addClass('active');
        this.setCurrentSound(metadata.sound);
        this.playCurrentSound();
      }
      else if(this.sameSoundAsCurrentSound(metadata)){
        if(this.currentSoundIsPlaying()){
          this.stopCurrentSound();
          se.removeClass('active');
        }
        else{
          this.playCurrentSound();
          se.addClass('active');
        }
      }
      else{
        this.removeActiveClassFromAllSound();
        se.addClass('active');
        this.switchSound(metadata.sound);
      }
      return this;
    },

    removeActiveClassFromAllSound: function(){
      $(this.cssContainerSelector).find(this.cssSoundSelector+'.active').removeClass('active');
      return this;
    },

    sameSoundAsCurrentSound: function(metadata){
      return this.currentSound.sound.id == metadata.sound;
    },

    currentSoundIsPlaying: function(){
      return this.currentSound.sound.playState == 1;
    },

    stopCurrentSound: function(){
      var t = this;
      this.stopUpdateTime();
      this.currentSound.sound.stop();
      this.player.fadeOut(function(){
        t.resetPlayerUI();
      });
      return this;
    },

    pauseCurrentSound: function(){
      this.stopUpdateTime();
      this.currentSound.sound.pause();
      return this;
    },

    resumeCurrentSound: function(){
      this.startUpdateTime();
      this.currentSound.sound.resume();
      return this;
    },

    playCurrentSound: function(){
      var t = this;
      this.renderMetaData();
      this.renderDownloadLink();
      this.player.fadeIn(function(){
        t.currentSound.sound.play();
        t.startUpdateTime();
      });
      return this;
    },

    seekCurrentSound: function(position){
      this.currentSound.sound.setPosition(position);
      return this;
    },

    renderMetaData: function(){
      var t = this;
      this.updateCurrentSoundMetaData();
      $.each(this.currentSound.metadata, function(k,v){
        if(k == 'sm_player-artwork') t.player.find('.'+k).attr('src', v);
        else if(k == 'sm_player-song_id') t.player.find('.favorite_link').attr('href', '/sync/favorites/toggle_favorite?song_id='+v);
        else if(k == 'sm_player-is_favorite' && v == '1') t.player.find('.favorite_link').addClass('favorite').find('i:first').removeClass('fa-star-o').addClass('fa-star');
        else if(k == 'sm_player-composer_percentage'){
          if(v) t.player.find('.sm_player-composer_percentage').html('<span class="control control-full">100</span> <span class="control control-partial">'+v+'</span>').parent().show();
          else t.player.find('.sm_player-composer_percentage').parent().hide();
        }
        else t.player.find('.'+k).html(v);
      });
      var isFav = this.currentSound.metadata['sm_player-is_favorite']||null;
      ((isFav != '0' && isFav != '1')) ? this.player.find('.favorite_link').hide() : this.player.find('.favorite_link').show();
      return this;
    },

    renderDownloadLink: function(){
      this.player.find('.sm_player-download').attr('href', this.currentSound.sound.url);
      return this;
    },

    setCurrentSound: function(sound){
      this.currentSound = this.findSound(sound);
      return this;
    },

    switchSound: function(sound){
      return this.stopCurrentSound()
                 .setCurrentSound(sound)
                 .playCurrentSound(),
             this;
    },

    msToMinAndSec: function(ms){
      var sec = Math.ceil(ms/1000),
          m = (''+(sec/60)).replace(/\..*/, '')||'0',
          s = ''+(sec % 60);
          if(s.length <= 1) s = '0'+s;
          return m + ':' + s;
    },

    resetPlayerUI: function(){
      this.player.find('.sm_player-pause-control').show();
      this.player.find('.sm_player-play-control').hide();
      this.loadProgress.css('width', 0);
      this.playProgress.css('width', 0);
      this.playTime.html('0:00');
      this.duration.html('0:00');
      return this;
    },

    updateLoadProgress: function(){
      var s = this.currentSound.sound;
      this.loadProgress.css('width',  (((s.bytesLoaded/s.bytesTotal)*100)+'%'));
      return this;
    },

    updatePlayProgress: function(){
      var s = this.currentSound.sound;
      this.playProgress.css('width',  (((s.position/s.durationEstimate)*100)+'%'));
      return this;
    },

    startUpdateTime: function(){
      var t = this;
      this.updatePlayTimeAndDuration();
      this.playTimer = setInterval(function(){
        t.updatePlayTimeAndDuration();
      }, 1000);
      return this;
    },

    stopUpdateTime: function(){
      clearInterval(this.playTimer);
      return this;
    },

    updatePlayTimeAndDuration: function(){
      var s = this.currentSound.sound;
      this.playTime.html(this.msToMinAndSec(s.position));
      this.duration.html(this.msToMinAndSec(s.durationEstimate));
      return this;
    },

  };

});

