// Shoo, a script to tell IE6 users to upgrade.
//
// Based largely on SevenUp 0.3
// GNU LGPL License v3
// SevenUp 0.3 is released into the wild under a GNU LGPL v3
// http://code.google.com/p/sevenup///

// This script is only served to IE6 via conditional comments, and only
// sniffs for the presence of Chrome Frame

var shoo = function() {
  // Define 'private vars' here.
  var options = {
    enableClosing: true,
    enableQuitBuggingMe: false, // NEVER
    overlayColor: "#000000",  
    lightboxColor: "#ffffff",
    borderColor: "#66ccff",
    overrideLightbox: false,
    lightboxHTML: null,
    showToAllBrowsers: true,
    usePlugin: false
  };
  
  function isChromeFrame(){
    // test to see if the user has Chrome Frame installed. Currently, ie6 w/ chrome frame 
    // reports its UA as: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) chromeframe/4.0
    var cf = /chrome/i;
    return cf.test(navigator.userAgent);
  }
  
  // Return object literal and public methods here.
  return {
    // Hate to define CSS this way, but trying to keep to one file.
    // I'll keep it as pretty as possible.
    overlayCSS: function() {
      return "display: block; position: absolute; top: 0%; left: 0%;" +
      "width: 100%; height: 100%; background-color: " + options.overlayColor + "; " +
      "filter: alpha(opacity: 80); z-index:1001;";
    },
    lightboxCSS: function() {
      return "display: block; position: absolute; top: 25%; left: 25%; width: 50%; " +
      "padding: 16px; border: 8px solid;" + options.borderColor + "; " +
      "background-color:" + options.lightboxColor + "; " +
      "z-index:1002; overflow: hidden;";
    },
    lightboxContents: function() {
      var html = options.lightboxHTML;
      if (!html) {
        html =
        "<div style='width: 100%; height: 95%; filter:alpha(opacity:100)'>" +
          "<h2>TuneCore no longer supports Internet Explorer 6</h2>" +
          "<div class='upgrade_msg'>" +
            "<p>It's nearly 10 years old! Before you'll be able to continue, you'll need to install one of the following modern browsers, <strong>all of which are free to download and install</strong>:</p>" +
            "<ul>"+
              "<li><img src='/images/browsers/msie.sm.png'><a href='http://www.microsoft.com/windows/Internet-explorer/default.aspx'>Internet Explorer</a></li>"+
              "<li><img src='/images/browsers/gecko.sm.png'><a href='http://www.getfirefox.com/'>FireFox</a></li>"+
              "<li><img src='/images/browsers/webkit.sm.png'><a href='http://www.apple.com/safari/download/'>Safari</a></li>"+
              "<li><img src='/images/browsers/presto.sm.png'><a href='http://www.opera.com/download/'>Opera</a></li>"+
              "<li><img src='/images/browsers/chrome.sm.png'><a href='http://www.google.com/chrome/'>Google Chrome</a></li>"+
            "</ul>"+
            "<p>Or, if you're forced to use IE6, you'll have to install the <a href='http://code.google.com/chrome/chromeframe/'>Google&nbsp;Chrome&nbsp;Frame</a> browser plugin.</p>"+
          "</div>" +
        "</div>";
        if (options.enableClosing) {
          html += "<div style='font-size: 11px; text-align: right;'>";
          html += options.enableQuitBuggingMe ?
          ("<a href='#' onclick='shoo.quitBuggingMe();' " +
              "style='color: #0000EE'>" +
              "Quit bugging me" +
          "</a>") :
          ("<a href='#' onclick='shoo.close();' " +
              "style='color: #0000EE'>" +
              "close" +
            "</a>");
          html += "</div>";
        }
      }
      return html;
    },
    test: function(newOptions, callback) {
  	  if (!isChromeFrame()) {
  	    // Write layer into the document.
  	    var layerHTML = "<div id='sevenUpCallbackSignal'></div>";
        if (options.overrideLightbox) {
          layerHTML += options.lightboxHTML;
        } else {
          layerHTML += "<div id='sevenUpOverlay' style='" + this.overlayCSS() + "'>" +
  	        "</div>" +
            "<div id='sevenUpLightbox' style='" + this.lightboxCSS() + "'>" +
              this.lightboxContents() +
            "</div>";
        }
        if (options.showToAllBrowsers !== true) {
          layerHTML = "<!--[if lt IE 7]>" + layerHTML + "<![endif]-->";
        }
        var layer = document.createElement('div');
        layer.innerHTML = layerHTML;
  	    document.body.appendChild(layer);
        // Fire callback.
        // I don't like this hack but IE6 seems to restrict dynamically created <script> tags to <head> only, 
        // and I don't see a way to add conditional comments around the script tag or its contents.
        // So for now, we write a 'signal' div inside the CC. If anyone has a better way please let me know.
        if (callback && document.getElementById('sevenUpCallbackSignal')) {
          callback(options);
        }
  	  }  
  	},
    quitBuggingMe: function() {
      var exp = new Date();
      exp.setTime(exp.getTime()+(7*24*3600000));
      document.cookie = "sevenup=dontbugme; expires="+exp.toUTCString();
      this.close();
    },
    close: function() {
      var overlay = document.getElementById('sevenUpOverlay');
      var lightbox = document.getElementById('sevenUpLightbox');
      if (overlay) { overlay.style.display = 'none'; }
      if (lightbox) { lightbox.style.display = 'none'; }
    },
    plugin: {}
  };
}();

