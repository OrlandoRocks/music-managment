(function($){	
	var d;
	var num = 0;
	var defaults = {
		beaconUrl: 'http://beacon.tunecore.com/cleardot.json',
		app: 'default',
		always: {},
		history: {},
		callbacks:{
			error: function(error){}
		}
	};
	var methods = {
		init: function(config){
			if(config != ''){
				d = $.extend(true, {}, defaults, config);
			}
			else{
				d = defaults;
			}
			methods.call({event: 'beacon_initialized', version: '1.0'});
			$(window).unload(function(){
				var exitParams = $.extend(true, {event: 'page_exit'}, d.history);
				methods.call(exitParams);
			});	
		},
		call: function(params){
			if('event' in params){
				if(params.event in d.history){
					d.history[params.event] = parseInt(d.history[params.event]) + 1;
				}
				else{
					d.history[params.event] = 1;
				}
			}
			num++;
			params.app = d.app;
			params.sequence = num;
			params.no_cache = Math.ceil(Math.random()*100000000000);
			var p = $.extend(true, {}, params, d.always);
			$.getJSON(d.beaconUrl, p);
			/*
			$.jsonp({
  				url: d.beaconUrl,
				data: p
			});
			*/
		},
		error: function(params){
			d.callbacks.error(params.error);
		}
	}	
    $.fn.Beacon = function(method, params){
        return this.each(function(){
			if(params == undefined){
				params = '';
			}
            if(method != undefined){
				switch(method){
					default:
						methods.error({error: 'Invalid Method'});
					break;
					case 'init':
						methods.init(params);
					break;
					case 'call':
						methods.call(params);
					break;
				}
			}
			else{
				methods.error({error: 'Invalid Method'});
			}
        });
    };
})(jQuery);