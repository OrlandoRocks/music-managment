var $ = jQuery;

$('.mass_takedown_songs_submit').on('click', function (e) {
  $('.admin_mass_retry_songs_form_song_ids_and_isrcs').prop('disabled', true);
  e.preventDefault();

  var inputValue= $("#admin_mass_retry_songs_form_song_ids_and_isrcs").val();
  $.ajax({
    context: this,
    url: '/admin/mass_retry_songs/encumbrance_info',
    type: 'POST',
    data: {admin_mass_retry_songs_form: {song_ids_and_isrcs: inputValue}},
    dataType: 'json'
  }).done(function (data) {
    if(data.song_ids_and_isrcs.length > 0) {
      encumbranceText = "The following IDs/UPCs/ISRCs " + data.song_ids_and_isrcs.join(', ') + " belong to customers who have encumbrances. Do you want to review the list, or continue with the takedown action?"
      $("#encumbrance_dailog").dialog({
        resizable: false,
        draggable: false,
        height: 100,
        width: 450,
        modal: true,
        buttons: {
          "CONTINUE": function() {
            $("#new_admin_mass_retry_songs_form").submit();
          },
          "BACK TO REVIEW": function() {
            $('.admin_mass_retry_songs_form_song_ids_and_isrcs').prop('disabled', false);
            $(this).dialog("destroy");
          }
        }
      }).find(".show-encumbrance-info").html(encumbranceText);
    }
    else {
      $('#new_admin_mass_retry_songs_form').submit()
    }
  }).fail(function (data) {
    return true
  });
});
