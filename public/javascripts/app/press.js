var Press = function(){
  var $ = jQuery,
  $year_switch = $(".year_switch"),
  $items = $(".pod-rows");
  
  var self = {
    init: function() {
      $year_switch.on('click', 'a', self.display_year);
      $(document).on('click', '.read_more a', self.show_more);
      self.show_active_year();
    },
    
    show_active_year: function() {
      $year_switch.find(".active").click();
    },
    
    // Display items from a certain year
    display_year: function(e) {
      e.preventDefault();
      var $this = $(this),
      year_clicked = new String( $this.data("year") ).split(','),
      category = $this.closest(".year_switch").data("category"),
      items = $items.filter("." + category);
      
      // Remove all active class
      $this.closest(".year_switch").find("a").removeClass();
      // Remove 'Read More' links
      $("." + category + ".read_more").remove();
      // Add it to currently selected year
      $this.addClass("active");
      // Hide all other items from category
      items.hide();
      // Filter and show only first 5 items with correct year
      var show_items = items.filter(function(i) {
        var $item = $(this),
        match_year = false;
        $.each(year_clicked, function(i, val) {
          if ( $item.data("year") == val ) {
            match_year = true;
          }
        });
        if ( match_year ) {
          return this;
        }
      });
      $.each(show_items, function(i, item) {
        if ( i < 5 ) {
          $(item).show();
        } else if ( i == 5 ) {
          $this.closest(".year_switch").next(".pod-body").append(
            '<div class="' + category + ' read_more"><a href="#">Read More</a></div>'
          );
        }
      });
    },
    
    // Show more items if some are hidden
    show_more: function(e) {
      e.preventDefault();
      var $this = $(this),
      $this_link = $this.parent(),
      year_clicked = new String( $this.closest(".pod-body").prev(".year_switch").find(".active").data("year") ).split(','),
      category = $this.closest(".pod-body").prev(".year_switch").data("category"),
      hidden_items = $items.filter("." + category).filter(":hidden"),
      count = 0;
      
      // Filter and show only the next 5 items with correct year
      var show_items = hidden_items.filter(function(i) {
        var $item = $(this),
        match_year = false;
        $.each(year_clicked, function(i, val) {
          if ( $item.data("year") == val ) {
            match_year = true;
          }
        });
        if ( match_year ) {
          return this;
        }
      });
      $.each(show_items, function(i, item) {
        if ( i < 5 ) {
          $(item).show();
        }
      });
      if ( !show_items.filter(":hidden").length ) {
        $this_link.remove();
      }
    }
  };
  
  self.init();
  return self;
};

jQuery(document).ready( function(){
  var press = new Press();
});