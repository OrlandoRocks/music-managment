var Compositions = function() {
  var filter_form = ".filter_form"
  var $links = $('.collapsible').find('.toggle-button'); // collect the toggle links
  // $options = $links.parent().next().find('li');
  var $section = $('.toggle-select'),
  querystring = $.deparam.querystring( true ),
  status = querystring.status,
  split = querystring.split,
  release = querystring.release,
  timer,
  select_all = '.select_all',
  deselect_all = '.deselect_all',
  table = '.compositions-table',
  select_all_splits = '.select_all_splits',
  deselect_all_splits = '.deselect_all_splits',
  bulk_select_section = '.bulk_select_splits',
  bulk_select = '.bulk_splits',
  bulk_custom_split = '.bulk_custom_split',
  individual_select = '.select_splits',
  apply_bulk_splits = '.apply_bulk_splits',
  submit_splits_btn = '.submit_splits',
  error = '#errorExplanation';
  
  var self = {
    init: function() {
      $(error).dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        draggable: false,
        width: 250,
        maxWidth: 375,
        height: 100,
        maxHeight: 250
      });
      
      if ( $(error) ) {
        $(error).dialog('open');
      }
      
      // add toggle indicator to all toggle links
      // toggle open on click
      // hide section initially
      $links.addClass('toggle_plus').on('click', self.toggleFilterOpen).parent().next().hide();
      
      // open any default toggles
      $.each($links, function() {
        var $this = $(this);
        if ( $this.data("toggle") == "open" ) {
          $(this).removeClass('toggle_plus').addClass('toggle_minus').data("toggle", "open");
          $(this).parent().next().stop().slideDown();
        }
      });
      
      // hide filter options greater than 10 shown
      $.each($section, function() {
        var $this = $(this),
        $options = $this.next().find('li');
        if ( $options.length > 10 ) {
          $options.filter(function(i) {
            if ( i > 9 ) {
              $(this).hide();
            }
          });
          $this.next().append('<li><a href="#" class="show_more">Show More&hellip;</a></li>');
        }
      });
      
      // show more filters 10 at a time if show more is clicked
      $(document).on('click', '.show_more', function(e) {
        e.preventDefault();
        var $link = $(this).parent(),
        $container = $link.parent('ul'),
        $hidden_links = $container.find('li:hidden');
    
        // If more than 10 hidden links, keep $link and show 10 more links
        if ( $hidden_links.length > 10 ) {
          $hidden_links.filter(function(i) {
            if ( i < 10 ) {
              $(this).show();
            }
          });
        // Else less than 10 hidden links, remove $link and show remaining links
        } else {
          $link.remove();
          $hidden_links.show();
        }
      });
      
      $(filter_form).on('reset', function() {
        window.location.href = window.location.pathname;
      });
      
      // Open filter section if something is checked
      self.select_from_querystring(status, ".status");
      self.select_from_querystring(split, ".split");
      self.select_from_querystring(release, ".release");
      
      // Disable & hide search button when JS is on
      $("#search_btn").prop('disabled', true).hide();
      $(".search_form").submit(function() {return false;});
      // Perform live search after user stops typing for half a second
      $(document).on('keyup', '#search', function(e) {
        e.preventDefault();
        if (e.which == 13) {
          return false;
        }
        clearTimeout(timer);
        var search_field = this;
    
        timer = setTimeout(function() {
          self.liveSearch(search_field);
        }, 500);
      });
      $(document).on('click', '.clear_btn', function(e) {
        e.preventDefault();
        var search_field = '#search';
        $(search_field).val("");
        self.liveSearch(search_field);
      });
      
      // Select/Deselect all checkboxes
      $(document).on('click', select_all, function(e) {self.toggleFilterCheckbox(e, $(this), true)});
      $(document).on('click', deselect_all, function(e) {self.toggleFilterCheckbox(e, $(this), false)});
      
      // When "other" is selected on splits dropdown show input box
      $(document).on('change', bulk_select, self.showOtherInput);
      $(document).on('change', individual_select, self.showOtherInput);
      
      // Select/Deselect all splits checkboxes
      $(document).on('click', select_all_splits, function(e) {self.toggleSplitCheckbox(e, $(this), true)});
      $(document).on('click', deselect_all_splits, function(e) {self.toggleSplitCheckbox(e, $(this), false)});
      
      // Mirror top and bottom bulk select dropdowns
      $(table).on('change', bulk_select, function() {
        var value = $(this).val();
        $(bulk_select).val(value);
        if (value == "other") {
          $(bulk_select).next('.other_percentage').show();
        } else {
          $(bulk_select).next('.other_percentage').hide();
        }
      });
      
      $(table).on('change', bulk_custom_split, function() {
        var value = $(this).val();
        $(bulk_custom_split).val(value);
      });
      
      // Apply bulk split percentage to all checked rows
      $(table).on('click', apply_bulk_splits, self.applyBulkSplit);
      
      // Submit splits of rows that are checked
      $(document).on('click', submit_splits_btn, function(e) {
        e.preventDefault();
    
        $('#splits_form').submit();
      });
    },
    
    toggleFilterCheckbox: function(e, $this, toggle) {
      e.preventDefault();
      $this.closest('.toggle-select').next('ul').find(':checkbox').prop('checked', toggle);
    },
    
    toggleSplitCheckbox: function(e, $this, toggle) {
      e.preventDefault(); e.stopImmediatePropagation();
      $this.closest('.compositions').find(':checkbox').prop('checked', toggle);
    },
    
    toggleFilterOpen: function(e) {
      e.preventDefault();
      if ( $(this).data("toggle") != "open" ) {
        $(this).removeClass('toggle_plus').addClass('toggle_minus').data("toggle", "open");
        $(this).parent().next().stop().slideDown();
      } else {
        $(this).removeClass('toggle_minus').addClass('toggle_plus').data("toggle", "close");
        $(this).parent().next().stop().slideUp();
      }
    },
    
    select_from_querystring: function(type, class_name) {
      if ( type ) {
        var $section = $(class_name),
        $list = $section.find("ul");
        if ( $section.find(".toggle-button").data("toggle") != "open" ) {
          $section.find(".toggle-button").click();
        }
      }
    },
    
    liveSearch: function(search_field) {
      var $search_field = $(search_field),
      search_term = $search_field.val(),
      $table = $(".table-container");
      $('.loading').remove();
      $search_field.after('<img src="/images/icons/spinner-16x16.gif" alt="Loading..." class="loading" style="padding-left: 10px; vertical-align: text-bottom;">');
      
      $.ajax({
        type: "GET",
        url: $('.search_form').attr('action'),
        data: $('.search_form').serialize(),
        success: function(html){
          $('.loading').remove();
          $table.html(html);
        },
        dataType: 'html'
      });
    },
    
    showOtherInput: function() {
      var $this = $(this);
      if ( $this.find("option:selected").val() == "other" ) {
        $this.next('.other_percentage').show().prop('disabled', false);
      } else {
        $this.next('.other_percentage').hide().prop('disabled', true);
      }
    },
    
    applyBulkSplit: function(e) {
      e.preventDefault();
      var $selected_splits = $(table).find('tbody tr').filter(function() {
        return $(this).find(':checkbox').prop('checked') == true;
      }),
      value = $(this).closest('.controls').find(bulk_select).val(),
      other_value = $(this).closest('.controls').find(bulk_select).next('.other_percentage').find('.custom_split').val();
  
      if ( value == "other" ) {
        $.each($selected_splits, function(i, row) {
          $(row).find('.select_splits').val(value).next('.other_percentage').show().find('.custom_split').val(other_value);
        });
      } else {
        $.each($selected_splits, function(i, row) {
          $(row).find('.select_splits').val(value).next('.other_percentage').hide();
        });
      }
    }
  };
  
  self.init();
  return self;
};

jQuery(function( $ ) {
  var compositions = new Compositions();
});
