jQuery(function($) {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
      || location.hostname == this.hostname) {
      var target = $(this.hash),
          nav = parseInt($('.masthead:first').css('height'),10),
          secondary_nav = $('.masthead-secondary').length > 0 ? parseInt($('.masthead-secondary').css('height'),10) : 0,
          nav_offset = nav + secondary_nav;
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').scrollTop( target.offset().top - nav_offset );
        return false;
      }
    }
  });
  
  setTimeout(function() {
    if (location.hash) {
      var target = $(location.hash),
          nav = parseInt($('.masthead:first').css('height'),10),
          secondary_nav = $('.masthead-secondary').length > 0 ? parseInt($('.masthead-secondary').css('height'),10) : 0,
          nav_offset = nav + secondary_nav;
      target = target.length ? target : $('[name=' + location.hash.slice(1) +']');
      if (target.length) {
        $('html,body').scrollTop( target.offset().top - nav_offset );
        return false;
      }
    }
  }, 1);
});