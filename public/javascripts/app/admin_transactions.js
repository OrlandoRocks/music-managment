/*
  Transaction form for administrative search.
  Also used on fraud/index to initiate date fields.
  CH - 2009-9-30
*/
var TransactionForm = function(){
  var date_class = ".date-box";
  var transaction_type_select = "#transaction_type";
  var $ = jQuery;

  var self = {
    init: function(){
      $(transaction_type_select).change(this.show_or_hide_options);
      $('#d_country_website_id').on('change', this.load_vendor_options);
      // need to show or hide when the form first loads
      this.show_or_hide_options();
      this.load_vendor_options();
      self.init_date_pickers();
    },
    load_vendor_options: function(){
      if($('#d_country_website_id').val() != "8"){
        $('#braintree_status').show();
        $('#d_status').prop('disabled', '');
        $('#d_payment_status').prop('disabled', 'disabled');
        $('#payments_os_status').hide();
      } else {
        $('#d_status').prop('disabled', 'disabled');
        $('#braintree_status').hide();
        $('#payments_os_status').show();
        $('#d_payments_os_status').prop('disabled', '');
      }
    },
    show_or_hide_options: function(){
      if(["Credit Card", "PayU Transaction"].includes($(transaction_type_select).val())){
        if($("#credit_card_options").is(":hidden")) {
          $("#credit_card_options").show();
          $("#d_action").prop("disabled","");
          $("#d_status").prop("disabled","");
          $("#paypal_options").hide();
          $("#d_payment_status").prop("disabled","disabled");
          $("#d_paypal_action").prop("disabled","disabled");
          if($(transaction_type_select).val() == "PayU Transaction") {
            $('#d_country_website_id').val("8").trigger("change");
          }
        }
      }else{
        if($("#paypal_options").is(":hidden")) {
          $("#credit_card_options").hide();
          $("#d_action").prop("disabled","disabled");
          $("#d_status").prop("disabled","disabled");
          $("#paypal_options").show();
          $("#d_paypal_action").prop("disabled","")
          $("#d_payment_status").prop("disabled","");
        }
      }
    },

    init_date_pickers: function(){
      new DatePicker(date_class,{});
    }
  };

  return self.init();
};


var init_transaction_form = function(){
  new TransactionForm();
};
jQuery(document).ready(init_transaction_form);
