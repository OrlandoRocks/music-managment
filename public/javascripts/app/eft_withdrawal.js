var EftWithdrawal = function(balance, service_fee, max_amount){
  var $ = jQuery;
  var form = '#new_eft_batch_transaction';
	var form_input = ".context-wrapper input";
	
  var self = {
    init: function(){
	    self.init_helper_text();
      self.init_validation();
    },
    init_validation: function(){
      $(form).validate({
        rules: {
          'eft_batch_transaction[amount_from_form]': {
            required: true,
            validAmount: true,
            cashAmountLessThanEqual: (balance > max_amount) ? max_amount :  balance,
            cashAmountGreaterThan: service_fee            
          }
        },
        messages: {
          'eft_batch_transaction[amount_from_form]': {
            required: "Please enter your withdrawal amount",            
            cashAmountLessThanEqual: (balance > max_amount) ? "Please enter an amount less than or equal to max withdrawal amount of $" + max_amount
                : "Please enter an amount less than or equal to your balance of $" + balance,
            cashAmountGreaterThan: "Please enter an amount greater than the service fee of $" + service_fee
  
          }
        }  
      });
    },
  	init_helper_text: function(){
      $(form_input).focus(this.show_helper_text);
      $(form_input).blur(this.hide_helper_text);
    },
		show_helper_text: function(){
      var help = $(this).next('.contextual-help');

      if ((help.find('p').text().length !== 0) && (help.prev().attr('class') !== 'error')){
        help.removeClass('hidden');
      };
    },
		disable_submit_btn: function(){
      $(this).prop('disabled', 'disabled');
    },

    hide_helper_text: function(){
      $(this).next('.contextual-help').addClass('hidden');
    }	
  };

  self.init();
  return self;
};

jQuery.validator.addMethod("validAmount", function(value, element) {
  return !(/[^0-9\$\,\.\s]+/.test(value))
}, "Please enter a valid amount (only numbers)");

jQuery.validator.addMethod("cashAmountLessThanEqual", function(value, element, params) {
 value_without_dollar = value.replace(/\$/, "");
 cleaned_number =  parseFloat(value_without_dollar.replace(/\,/, ""));
 return (cleaned_number <= parseFloat(params));
}, "Please enter a smaller amount");

jQuery.validator.addMethod("cashAmountGreaterThan", function(value, element, params) {
 value_without_dollar = value.replace(/\$/, "");
 cleaned_number =  parseFloat(value_without_dollar.replace(/\,/, ""));
 return (cleaned_number > parseFloat(params));
}, "Please enter a greater amount");