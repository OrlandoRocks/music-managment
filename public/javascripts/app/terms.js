jQuery(function($){
  $(window).bind('hashchange', function(){
    // find nav
    var $nav = $(".terms-nav");
    // find sections
    var $sections = $(".terms");
    // find url hash (if exists)
    var hash = window.location.hash || '#music_distribution_terms';
    // clear active class and remove defaults
    $nav.find("a").removeClass("active").click(function(e){e.stopPropagation()});
    $sections.removeClass("active");
    // go to tab with hash (else go to the first tab)
    $nav.find('a[href=' + hash + ']').addClass("active");
    $sections.filter(hash).addClass("active");
  });
  
  $(window).trigger("hashchange");
});