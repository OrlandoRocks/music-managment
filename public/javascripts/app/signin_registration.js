// Lazy loading
$(window).on('load', function(){
  if (!navigator.cookieEnabled) {
    $("#main_content").prepend("<div class=\"message row\"><div id=\"errorExplanation\" class=\"seven columns centered\"><span>Please enable cookies in your browser and reload this page. Without cookies enabled, we cannot log you in. Need help? <a href=\"https://support.google.com/accounts/bin/answer.py?hl=en&answer=61416\">Learn more</a></span></div></div>")
    $("#login-button").prop("disabled", true)
    $("#login-button").css({"background-color": "#999", "color": "#666"})
  }
});
