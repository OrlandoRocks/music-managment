/* TC.JSONP.js
  = Description
  The tcjsonp function allows us to make JSONP requests between the rails
  app and the django reporting server.
  
  = Change log
  [2010-3-31 -- MAT] Initial Creation
  [2010-4-02 -- MAT] Added TC.portalConnector()
  [2010-4-12 -- MAT] Change TC.portalConnector() to work with "display_code"
  
  = Dependencies
  Depends on Julian Auborg's jquery.jsonp-1.1.3.js, which adds
  timeout and error callback support for JSONP requests made
  with jQuery.
  
  = Usage
  TC.jsonp('http://xxx.tunecore.com/some/service', successCallback, errorCallback);
  
*/

// Set up the TC global namespace to prevent any collision with
// libraries or code out of our control. If TC does not exist,
// create a new Object namespace.

var TC = TC || {};

// Global timeout before the error callback is called.

jQuery.jsonp.setup({timeout: 20000});

// The tcjsonp function takes 3 arguments: the URL of the request,
// a success function, and an error function. The error function will
// fire if the server returns an error, of if the timeout is exceeded
// Takes two arguments, the url of the request and the callback function
// that the response will be returned in.
//
// If the JSON response contains a "display" key, use TC.portalConnector()
// instead.

TC.jsonp = function(url, successCallback, errorCallback) {
  jQuery.jsonp({
    url: url + "?callback=?",
    success: function(json) {
      successCallback.call(this, json);
    },
    error: function() {
      if (errorCallback){
        errorCallback.call(this); 
      };
    }
  });
};

// TC.portalConnector() is designed to work with the portal reporting
// server which always returns a "display_code" key in its JSON response:
// e.g., {"display_code": 1} or {"display_code": 0}. If the returned key is not 0, 
// we call the successCallback function; if the key is any other value we call the 
// errorCallback function.

TC.portalConnector = function(url, successCallback, errorCallback) {
  jQuery.jsonp({
    url: url + "?callback=?",
    success: function(json) {
      if (json.display_code === 0) {
        successCallback.call(this, json); 
      } else {
        errorCallback.call(this);
      }
    },
    error: function() {
      if (errorCallback){
        errorCallback.call(this); 
      };
    }
  });
};




TC.uploadConnector = function(url, successCallback, errorCallback) {

  jQuery.jsonp.setup({timeout: 3600000});
  jQuery.jsonp({
    url: url + "&callback=?",
    callback: "jsonp_callback",
    success: function(json) {
      if (json.status_code === 0) {
          successCallback.call(this);
        } else {
          errorCallback.call(this);
        }
    },
    error: function() {
      if (errorCallback){
        errorCallback.call(this); 
      };
    }
  });
};

