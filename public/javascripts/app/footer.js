jQuery(document).ready(function($){
  $.getJSON('http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=10&callback=?&q=http://www.tunecore.com/blog/feed', function(r){
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    html = '';
    $.each(r.responseData.feed.entries, function(i,v){
      if(i < 4){
        var d = new Date(this.publishedDate);
        html += '<div><span>'+months[d.getMonth()]+' '+d.getDate()+':</span><a href="'+this.link+'" target="_blank">'+this.title+'</a></div>';
      }
    });
    $('#footer .blog').append(html);
  });
});
