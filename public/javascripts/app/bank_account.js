//
//  Create bank Account form:
//

var BankAccount = function(){
  var $ = jQuery;
  var form = '#new_stored_bank_account';
  var form_input = ".context-wrapper input";
	var account_holder_type = $("#stored_bank_account_account_holder_type")
	var company = $("#stored_bank_account_company_list_item")
	
  var self = {
    init: function(){
			self.show_company();
			self.mask_phone();
      self.init_events();
      self.init_helper_text();
      self.init_validation();
    },
    
    init_helper_text: function(){
      $(form_input).focus(this.show_helper_text);
      $(form_input).blur(this.hide_helper_text);
    },
    
    init_validation: function(){
	    //stored_bank_accounts/new
	    $(form).validate({
        submitHandler: function(form) {
          braintree.client.create({
            authorization: gon.braintreeToken
          }).then(function (clientInstance) {
            braintree.usBankAccount.create({
              client: clientInstance
            }).then(function (bankAccountInstance) {
              var bankDetails = {
                accountNumber: $('#stored_bank_account_last_four_account').val(),
                routingNumber: $('#stored_bank_account_last_four_routing').val(),
                accountType: $('#stored_bank_account_account_type').val(),
                ownershipType: $('#stored_bank_account_account_holder_type').val(),
                billingAddress: {
                  streetAddress: $('#stored_bank_account_address1').val(),
                  extendedAddress: $('#stored_bank_account_address2').val(),
                  locality: $('#stored_bank_account_city').val(),
                  region: $('#stored_bank_account_state').val(),
                  postalCode: $('#stored_bank_account_zip').val(),
                }
              }
              if (bankDetails.ownershipType === 'personal') {
                bankDetails.firstName = $('#stored_bank_account_name').val().split(' ')[0]
                bankDetails.lastName = $('#stored_bank_account_name').val().split(' ')[1]
              } else {
                bankDetails.businessName = $('#stored_bank_account_name').val()
              }

              bankAccountInstance.tokenize({
                bankDetails: bankDetails,
                mandateText: $('#mandate_text').text(),
              }).then(function(tokenizedPayload) {
                $('#braintree_payment_nonce').val(tokenizedPayload.nonce)
                form.submit()
              })
            })
          }).catch(function(err) {
            console.log("Error: ", err)
          })
        },
	      rules: {
					merchant_defined_field_9: {
	          required: true,
	          minlength: 2
	        },
	        checkaba: {
	          required: true,
	          minlength: 9,
						maxlength:9,
	          digits: true,
						routingCheckSum: true
	        },
					checkaccount: {
	          required: true,
	          minlength: 3,
	          digits: true
	        },
					checkaccount_confirmation: {
	          required: true,
	          minlength: 3,
	          digits: true,
						equalTo: '#stored_bank_account_last_four_account'
	        },
	        account_type: {
            required: true
          },
					account_holder_type: {
            required: true
          },
          company: {
            required: function(element){
              return $("#stored_bank_account_account_holder_type :selected").val() == "business";
            },
            noSlashes: true
          },
					checkname: {
	          required: true,
	          minlength: 2,
	          noSlashes:true
	        },
					address1: {
	          required: true,
	          minlength: 2
	        },
					city: {
	          required: true,
	          minlength: 2,
	          noCommas: true
	        },
          state: {
            required: true
          },
          zip: {
            required: true,
            minlength: 5
          },
          phone: {
            required: true,
            phoneUS: true
          }
				},
	      messages: {
					merchant_defined_field_9: "Please enter your bank name.",
	        checkaba: {
	          required: "Please enter your routing number.",
	          minlength: "A valid routing number is 9 digits.",
						maxlength: "A valid routing number is 9 digits.",
						digits: "A valid routing number contains only digits (no letters or other characters)"
	        },
					checkaccount: {
	          required: "Please enter your account number.",
						digits: "Your account number must be all digits",
	          minlength: "A valid account number must be more than 3 digits."
	        },
					checkaccount_confirmation: {
	          required: "Please re-enter your account number.",
						digits: "Your account number must be all digits",
	          minlength: "A valid account number must be more than 3 digits.",
						equalTo: "Account Numbers do not match, please re-enter both."
	        },
					account_type: "Please enter your account type.",
          account_holder_type: "Please enter account holder type.",
					checkname: {
	          required: "Please enter your full name.",
	          minlength: "Please enter your full name.",
						noSlashes: "Your name cannot contain slashes: \\ or /. Make sure you did not include your company name in this field."
	        },
					company: {
					  required: "Company Name is required for Business Accounts",
					  noSlashes: "A valid Company Name cannot contain slashes: \\ or /."
					},
					address1: "Please enter your address.",
          city: "Please enter your city.",
          city: {
					  required: "Please enter your city.",
					  noCommas: "A valid city name cannot include a comma. Please make sure you did not include the state."
					},
          zip: "Please enter your zip code.",
          state: "Please enter your state or U.S. territory.",
          country: "Please enter your country.",
          phone: "Please enter a valid phone number starting with your area code."
	      }
      });
  	},
    
    disable_submit_btn: function(){
      $(this).prop('disabled', 'disabled');
    },
    
    show_helper_text: function(){
      var help = $(this).next('.contextual-help');
      
      if ((help.find('p').text().length !== 0) && (help.prev().attr('class') !== 'error')){
        help.removeClass('hidden');
      };
    },
    
    hide_helper_text: function(){
      var t = $(this);
      setTimeout(function(){
        t.next('.contextual-help').addClass('hidden');
      }, 100);
    },
		init_events: function(){
	    account_holder_type.change(self.show_company);
	  },
		show_company: function(){
	    type_selected = $("#stored_bank_account_account_holder_type :selected").val();
	    type_selected=="business" ? company.slideDown(300) : company.slideUp(300);
	  },
	  mask_phone: function(){
      var phone = '[id$=stored_bank_account_phone]';
      $(phone).mask('(999) 999-9999');
    },
  };

  self.init();
  return self;
};

jQuery.validator.addMethod("phoneUS", function(phone_number, element) {
    phone_number = phone_number.replace(/\s+/g, ""); 
	return this.optional(element) || phone_number.length > 9 &&
		phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
}, "Please specify a valid phone number");

jQuery.validator.addMethod("noSlashes", function(value, element) {
	if (!value.match(/(\\|\/)/)) 
		return true;
}, "This field cannot contain slashes: \\ or /.");

jQuery.validator.addMethod("noCommas", function(value, element) {
	if (!value.match(/(\,)/)) 
		return true;
}, "This field cannot contain commas.");

jQuery.validator.addMethod("routingCheckSum", function(value, element) {
  
	if (/[^0-9]{9}/.test(value)) 
		return false;

	total = 3*(parseInt(value[0]) + parseInt(value[3]) + parseInt(value[6])) +
					7*(parseInt(value[1]) + parseInt(value[4]) + parseInt(value[7])) +
					   parseInt(value[2]) + parseInt(value[5]) + parseInt(value[8]);
	
	return (total % 10 == 0);

}, "This is not a valid routing number, please make sure you typed it correctly.");

jQuery(document).ready(function(){
  new BankAccount();
});
