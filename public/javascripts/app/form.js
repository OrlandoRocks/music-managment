
/*
 *  Signup:
 *
 *  A simple object to encapsulate signup javascript logic
 *
 */
var Signup = function(){

  //
  //  Page style definitions
  //
  var $ = jQuery,
  $form_input             = $("input"),
  email_field             = "#person_email",
  email_confirm_field     = "#person_email_confirmation",
  password_field          = "#person_password",
  password_confirm_field  = "#person_password_confirmation",
  $country_field          = $("#person_country"),
  $zip_code_field         = $("#person_postal_code"),
  $password               = $(".password"),
  $confirm_password       = $(".password-confirmation"),
  $error_msg              = $("#errorExplanation");

  var self = {

    //
    //  Setup Form Elements
    //
    init: function() {
      $(email_confirm_field).parents("fieldset").hide();
      $form_input.each(this.hide_helper_text);
      $form_input.focus(this.show_helper_text);
      $form_input.blur(this.hide_helper_text);
      self.setup_monitor(email_field, email_confirm_field);
      self.setup_monitor(password_field, password_confirm_field);
      $('select.auto-complete').selectToAutocomplete();
    },

    setup_monitor: function(field, confirm_field) {
      if ( $(field).length && $(confirm_field).length ) {
        setInterval(function() {
          if ( $(field).val().length ){
            $(confirm_field).parents("fieldset").show();
          } else if ( $(confirm_field).parents(".fieldWithErrors").length ) {
            $(confirm_field).parents("fieldset").show();
          }
          return true;
        }, 100);
      }
    },

    show_helper_text: function() {
      var help = $(this).parents("fieldset").children(".contextual-help");
      if ( help.find('p').text().length !== 0 ) {
        help.fadeIn("slow").css("overflow", "visible");
      }
    },

    hide_helper_text: function() {
      var help = $(this).parents("fieldset").children(".contextual-help");
      // Delay hiding one second so people can click on links in help.
      setTimeout(function() {
        if ( help.find('p').text().length !== 0 ) {
          help.slideUp().css("overflow", "visible");
        }
      }, 500);
    }
  };

  return self.init();
};

// Initialize on document ready
jQuery(document).ready( function() {
  new Signup();
});
