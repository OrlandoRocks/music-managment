$(function() {

  var pageLaunchedFromSocialCheckout = window.location.href.includes('tcs_return_msg=true');

  if (pageLaunchedFromSocialCheckout) {
    $('.tabHeader').hide();
    $('.note_alert').hide();
    $('.payment-settings').hide();
    $('.pay-with-balance').hide();
  }
});
