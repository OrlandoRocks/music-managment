/* Processing on submit
* 
* [Nov 05 2010 DCD] Hiding the submit and cancel buttons on form submit
* Used on the withdrawal pages to prevent users from accidentally submitting withdraw requests with a quick double click
*
* - Usage
* called with no variables, on submit of a form on a page, it would hide that form's submit button and the cancel button and show 
* the div with id 'processing-label'
*  new ProcessingOnSubmit();
*
* You can pass in the form id if you have multiple forms on the page, and the processing label if it's named something different from
* 'processing-label'
* new ProcessingOnSubmit("#new_form_to_use","#label_to_show");
*/

var ProcessingOnSubmit = function(form_id, processing_label){
  var $ = jQuery;
  var form = (typeof form_id == 'undefined') ? $('form') : $(form_id);
  var processing_field =  (typeof processing_label == 'undefined') ? $('#processing-label') : $(processing_label);
  // undisables the submit button in case people use the broswer back button to navigate to this page
  $(form).find('input[type=submit]').prop('disabled', false);
  
  return form.submit(function(){
    $(this).find('input[type=submit]').prop('disabled', true);
    $(this).find('input[type=submit]').fadeOut();
    processing_field.show();
    $('.cancel').hide();
  });
};
