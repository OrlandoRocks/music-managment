/*
This method can be used to add a select all checkboxes checkbox (and have multiple 'select all on the page)

USAGE: new SelectAllButton(selector_of_the_select_all_input_checkbox, selector_for_the_section_where_the_check_boxes_to_be_selected_are);
EXAMPLE:
jQuery(document).ready(function(){
  new SelectAllButton('#select_all', '#eft-transaction-section');
  new SelectAllButton('#select_all2', '#eft-transaction-section2'); // selector names must be unique on the page
});

using the window[] javascript function to create variables unique to the selector. If one uses 

DCD created
TODO - abstract this so you can call it once on the page and it will create the select alls for all sections marked by a class 'select-all-section' and a select within that section with a class 'select-all'
*/
var SelectAllButton = function(selector,within_selector){
  var $ = jQuery;
  window['select_all_checkbox'+selector] = $(selector);
  window['checkboxes' + selector] = $('input:checkbox', within_selector);
  var section = '.select-all-section';
  
  var self = {
    init: function(){
      // self.update_checkboxes();
      self.init_events();
    },
    init_events: function(){
      window['select_all_checkbox'+selector].click(self.update_checkboxes);
    },
    update_checkboxes: function(){
      window['select_all'+selector] = window['select_all_checkbox'+selector].is(':checked');
      window['select_all'+selector] ? window['checkboxes' + selector].each(function(){this.checked = 'checked';}) : window['checkboxes' + selector].each(function(){this.checked = false;});
    }
  };
  return self.init();
};
/* Select_All.js
  = Description
  This method can be used to add a select all checkboxes checkbox
  (and have multiple 'select all on the page).
  
  = Change log
  [2010-06-11 -- DD] Initial Creation
  [2011-01-31 -- RT] Recreated script for version 2
  [2011-02-23 -- RT] Added type checkbox detecting (With help from LA)
  
  = Dependencies
  Depends on no plugin.
  
  = Usage
  Add the class 'select-all-section' for all sections you want to have a select all,
  Add a checkbox in the section with the class 'select-all', which will be the select all.
  NOTE: Current revision heavily designed for SIP customer view pages, might not work on another page.
        Use the old version above instead.
  
  = Example
  <div class="select-all-section">
    <label><input type="checkbox" class="select-all" /> Select All</label>
    <label><input type="checkbox" /> Option 1</label>
    <label><input type="checkbox" /> Option 2</label>
    <label><input type="checkbox" /> Option 3</label>
  </div>
  
*/
var TC = TC || {};
var SelectAllButton2 = function () {
  var $ = jQuery;
  var section = '.select-all-section',
  $album = $("#release_type_album"),
  $single = $("#release_type_single"),
  $ringtone = $("#release_type_ringtone");
  $video = $("#release_type_video");
  
  var self = {
    init: function () {
      self.select_all();
      self.master_select_all();
      self.type_select();
    },
    
    // Finds select all in section
    // 
    // 1. Find each select all sections
    // 
    // -- In each of these sections…
    // 2. Find all checkboxes except for the select all checkbox
    // 3. Find the select all checkbox
    // 4. Each time select all is used, check if it's checked or unchecked, then
    //    traverse through all the checkboxes to check or uncheck them all
    //
    select_all: function () {
      $(section).each(function () { // 1.
        var $inputs = $(this).find('input:checkbox'), // 2.
        $selectAll = $(this).find('a.select-all'), // 3.
        $clearAll = $(this).find('a.select-none'),
        $this = $(this); // This saves a selection of the current section
        
        $selectAll.click(function () { // 4.
          $inputs.each(function () { $(this).prop('checked', true); });
          return false;
        });
        
        $clearAll.click(function () {
          $inputs.each(function () { $(this).prop('checked', false); });
          return false;
        });
      });
    },
    
    // Checks and unchecks all releases of a certain type
    // 
    // 1. Find each type of releases
    // 2. Each time a type checkbox is used, check if it's checked or unchecked, then
    //    traverse through all the checkboxes to check or uncheck them all
    //
    type_select: function () {
      var $albumInputs = $("#albums").find('input:checkbox'), // 1.
      $singleInputs = $("#singles").find('input:checkbox'), // 1.
      $ringtoneInputs = $("#ringtones").find('input:checkbox'), // 1.
      $videoInputs = $("#videos").find('input:checkbox'), // 1.
      $albumSection = $("#albums"),
      $singleSection = $("#singles"),
      $ringtoneSection = $("#ringtones"),
      $videoSection = $("#videos"),
      $typeSelectAll = $("#select-all-type");
      
      self.input_change($album, $albumInputs); // 2.
      self.input_change($single, $singleInputs); // 2.
      self.input_change($ringtone, $ringtoneInputs); // 2.
      self.input_change($video, $videoInputs); // 2.
    },
    
    input_change: function (type, input_type) {
      type.change(function () {
        $(this).is(':checked') ? input_type.each(function () { this.checked = 'checked'; }) : input_type.each(function () { this.checked = false; });
      });
    },

    master_select_all: function(){
      $('.select-all-box.master').each(function(){
        var t = $(this);
        // select all
        t.find('.select-all').click(function(){
          $(section).each(function(){
            $(this).find('input:checkbox').attr('checked', true);
          });
          $('.select-all-section').each(function(){
            var sas = $(this);
            if(!sas.is(':visible')) sas.parents('.container').find('.toggle-button').click();
          });
          var firstReleaseSection = $('#releases .select-all-section:first');
          if(!firstReleaseSection.is(':visible')) $('#releases .toggle-button').click();
          return false;
        });
        // clear all
        t.find('.select-none').click(function(){
          $(section).each(function(){
            $(this).find('input:checkbox').attr('checked', false);
          });
          $('.select-all-section').each(function(){
            var sas = $(this);
            if(sas.is(':visible')) sas.parents('.container').find('.toggle-button').click();
          });
          var firstReleaseSection = $('#releases .select-all-section:first');
          if(firstReleaseSection.is(':visible')) $('#releases .toggle-button').click();
          return false;
        });
      });
    }
  };
  return self.init();
};

jQuery(document).ready(function () {
  TC.selectAll = new SelectAllButton2();
});
