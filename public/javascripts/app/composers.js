jQuery(document).ready(function($){
  setupProcessingDialog();

  $('#why').magicTip({
    position: 'bottomMiddle',
    content: function(){
      return '<strong>Why do we need your Social Security Number?</strong><br />We have to report your royalties as earnings to the government each year. You will receive a 1099 each year to use when filing your tax returns etc. Without providing your Social Security number or Tax ID number, we cannot account accordingly to you.';
    }
  });
  $('#why').bind('click', function(){return false;});
  $('#entity_warning').magicTip({
    position: 'bottomMiddle',
    content: function(){
      return 'Do not enter a publishing company name in this<br /> field unless you have affiliated this exact name with your performance rights society <strong>in addition</strong> to your writer affiliation.';
    }
  });
  $('#entity_warning').bind('click', function(){return false;});
  $('.cl').bind('focus', function(){
    var defaultValue = $(this)[0].defaultValue;
    if($(this).val() == defaultValue){
        $(this).val('');
    }
  });
  $('.cl').bind('blur', function(){
    var defaultValue = $(this)[0].defaultValue;
    if($(this).val() == ''){
        $(this).val(defaultValue);
    }
  });

  var set_full_name = function(){
    var full_name = '';
    var first = $('#first').val();
    var last = $('#last').val();
    var suffix = $('#suffix').val();
    if(first || false){
      full_name += first+' ';
    }
    if(last || false){
      full_name += last;
    }
    if(suffix || false){
      full_name += ', '+suffix+'.';
    }
    if(full_name || false){
      full_name += ', ';
      $('.full_name').html(full_name);
    }
  };

  $('#first, #last, #suffix').bind('blur', function(){
    set_full_name();
  });

  $('#tos').scroll(function(){
    var t = $(this);
    var diff = t[0].scrollHeight - t.scrollTop();
    if(diff <= t.outerHeight()){
      set_full_name();
        $('#agree').show();
    }
  });

  $(document).on('click', 'input:submit', function(e){
    e.preventDefault();
    $.magnificPopup.open({
      items: { src: '#confirm', type: 'inline' }
    });
  });
  $(document).on("click", ".go_back", function (e) {
    e.preventDefault();
    $.magnificPopup.close();
  });
  $(document).on("click", ".continue", function (e) {
    $.magnificPopup.close();
    jQuery('#processingDialog').dialog('open');
    $(this).prop("disabled", true);
    $("#songwriter").submit();
  });
});

function setupProcessingDialog() {
  jQuery('#processingDialog').dialog({
    closeOnEscape: false,
    open: function(event, ui) {
      $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
    },
    position: { my: "center", at: "center top", of: window},
    autoOpen: false,
    resizable: false,
    modal: true,
    draggable: false,
    width: 600,
    maxWidth: 600,
    height: 300,
    maxHeight: 300
  });
}

