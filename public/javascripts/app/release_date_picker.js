var ReleaseDatePicker = (function() {
  var dateBox = '.date-box';
  var salesDateBox = '.sale-date';
  var originalDateBox = '.original-date-box';
  var $calendar = $('.calendar');
  var $calendarEventsList = $('.calendar-events');
  var calendarObject = '.calendar-object';
  var events = {};
  var eventListLeft = [];
  var eventListRight = [];
  var purchased = $calendar.hasClass('purchased');

  function init() {
    $(dateBox).each(_makeReadOnly);

    var options = $.extend(
      {},
      $.datepicker.regional[gon.datepicker_locale],
      {
        changeYear: true,
        yearRange: '1950:+2'
      }
    );
    $.datepicker.setDefaults(options);
    _setOriginalDatePicker();
    _createCalendar();
  }

  function _setOriginalDatePicker() {
    DatePicker(originalDateBox, {
      dateFormat: gon.long_date_picker_format,
      onClose: function(dateText, inst) {
        if(inst.selectedDay && !$.isNumeric(inst.selectedDay)){
          currentDay = inst.currentDay.match(/(\d+)/)[0]
        }
        else
        {
          currentDay = inst.currentDay
        }
        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, currentDay));
      }
    });
  }

  function _setSalesDatePicker() {
    DatePicker(calendarObject, {
      altField: salesDateBox,
      altFormat: gon.long_date_picker_format,
      beforeShowDay: function(date) {
        var event = events[date];
        if (event) {
          return [event.selectable, event.className, event.text];
        } else {
          return [true, '', ''];
        }
      },
      defaultDate: new Date(TC.selected_date),
      minDate: purchased ? null : '0d',
      numberOfMonths: 2,
      stepMonths: 2,
      showOtherMonths: false,
      onChangeMonthYear: _showVisibleMonthEvents,
      onSelect: function() {
        $(salesDateBox).stop().effect('highlight', {color:'#7ACA4A'}, 1000);
      }
    });
  }

  function _showVisibleMonthEvents(year, month) {
    eventListLeft = [];
    eventListRight = [];
    for (var date in events) {
      if (Object.hasOwnProperty.call(events, date)) {
        _showEvents(date, events, year, month);
      }
    }
    $('.event_list-left').html(eventListLeft.join(''));
    $('.event_list-right').html(eventListRight.join(''));
  }

  function _showEvents(date, events, year, month) {
    var event = events[date];
    if (event.showInList) {
      var eventDate = new Date(date);
      _renderEventsHTML(event, eventDate, year, month);
    }
  }

  function _renderEventsHTML(event ,eventDate, year, month) {
    var eventYear = eventDate.getFullYear();
    var eventMonth = eventDate.getMonth() + 1;
    var eventDay = eventDate.getDate();
    var realYear = year;
    var realMonth = month;
    var eventHTML = '<li class="' + event.className + '"><a class="ui-state-default">' + eventDay + '</a> <span>' + event.text + '</span></li>';

    if (realMonth === eventMonth && realYear === eventYear) {
      eventListLeft.push(eventHTML);
    } else if (realMonth + 1 === eventMonth && realYear === eventYear) {
      eventListRight.push(eventHTML);
    } else if (realMonth - 11 === eventMonth && realYear + 1 === eventYear) {
      eventListRight.push(eventHTML);
    }
  }

  function _findRealDate() {
    var realDate = new Date(TC.selected_date);
    var newDate = new Date();

    if (!purchased) {
      if (realDate.getFullYear() < newDate.getFullYear()){
        realDate = new Date();
      } else if (realDate.getFullYear() === newDate.getFullYear() && realDate.getMonth() < newDate.getMonth()) {
        realDate = new Date();
      } else if (realDate.getFullYear() === newDate.getFullYear() && realDate.getMonth() === newDate.getMonth() && realDate.getDate() < newDate.getDate()) {
        realDate = new Date();
      }
    }

    return realDate;
  }

  function _createCalendar() {
    $calendarEventsList.append('<ol class="event_list event_list-left"></ol><ol class="event_list event_list-right"></ol>');

    var date = new Date();
    var today = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
    var tempDate = new Date(TC.suggest_date);
    var suggestedDate = tempDate.getMonth() + 1 + '/' + tempDate.getDate() + '/' + tempDate.getFullYear();
    events[new Date(suggestedDate)] = new _Event(gon.suggested_date, 'suggestion', true, true);
    _addDeadlineHighlights();
    _addDelayNotices();
    _setSalesDatePicker();
    var realDate = _findRealDate();
    var realYear = realDate.getFullYear();
    var realMonth = realDate.getMonth() + 1;
    _showVisibleMonthEvents(realYear, realMonth);
  }

  function _addDeadlineHighlights() {
    ['one', 'two', 'three', 'four'].forEach(function(el) {
      _deadlineHighlight('.delay-' + el, gon['itunesHoliday' + el.toUpperCase() + 'Deadline']);
    });
  }

  function _calendarNotices(dates, message, cl) {
    dates.forEach(function(curr, i) {
      var showInList = i === 0;
      events[new Date(curr)] = new _Event(message, cl, true, showInList);
    });
  }

  function _addDelayNotices() {
    var delays = ["One", "Two", "Three", "Four"].map(function(el) {
      return [gon["itunesHoliday"+ el + "Delay"], el];
    });
    delays.forEach(_createDelayCopy);
  }

  function _createDelayCopy(curr, i) {
    var arr = curr[0];
    var name = curr[1];
    var textCopy = gon['itunesHoliday' + name + 'HelpText'];
    _calendarNotices(arr, textCopy, 'delay' + name);
  }

  function _deadlineHighlight(cl, deadlineDate) {
    var deadline = new Date(deadlineDate);
    var month = deadline.getMonth();
    var date = deadline.getDate();
    $calendar.on({
      mouseenter: function() {
        $calendar.find('[data-month=' + month + '] a').filter(function() {
          return $(this).text() === date;
        }).addClass('active');
      },
      mouseleave: function() {
        $calendar.find('[data-month=' + month + '] a').filter(function() {
          return $(this).text() === date;
        }).removeClass('active');
      }
    }, cl);
  }

  function _Event(text, className, selectable, showInList) {
    this.text = text;
    this.className = className;
    this.selectable = selectable;
    this.showInList = showInList;
  }

  function _makeReadOnly() {
    $(this).attr('readonly', 'true');
  }

  return {
    init: init
  };
}());

$(function() {
  ReleaseDatePicker.init();
});
