// [June 29 2010] -- MAT -- Removing from State 1, Unused right now.
//
//  New User Promotion Lightbox
//
//  TODO: place the 'hide-promo' flag in sessionStorage, *then* fall back to window.name
var PromotionLightbox = function(){
  var $ = jQuery;
  var buy = '#buy-now';
  var cart = $('#new-user-promo a.cart-link').attr('href');
  var wrap = $('#new-user-promo');
  var promo_code = window.location.search.match(/(?:key=)(.*)/);
  
  var self = {
    
    init: function(){
      self.build_lightbox();
      self.bind_events();
    },
    
    build_lightbox: function(){
      if (window.name != 'hide-promo'){
        $('#new-user-promo').modal({ 
          'escClose': true,
          'minWidth': 723,
          'minHeight': 355,
          'containerCss': {'padding': '5px 6px 6px 5px', 'border':'none', '-moz-border-radius': '16px', '-webkit-border-radius': '16px'}
        });
      
        self.hide_close_button();
      }
    },
    
    bind_events: function(){
      $(document).on('click', buy, self.insert_shoppingcart);
    },
    
    insert_shoppingcart: function(){
      var iframe = $('<iframe src=' + cart + ' width="724" height="356" style="position:relative;right:24px;bottom:24px">');
      wrap.html(iframe);
      self.show_close_button();
      return false;
    },
    
    show_confirmation: function(){
      //this will control showing the promo code on the page
    },
    
    hide_close_button: function(){
      $('#simplemodal-container').find('a.modalCloseImg').css('display', 'none');
    },
    
    show_close_button: function(){
      $('#simplemodal-container').find('a.modalCloseImg').css('display', 'inline');
      window.name = "hide-promo";
    }
  };

  return self.init();
};


jQuery(document).ready(function(){
  new PromotionLightbox();
});
