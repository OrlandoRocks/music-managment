jQuery(function($) {
  $.datepicker.setDefaults({dayNamesMin: $.datepicker._defaults.dayNamesShort});
  $("input[type='month']").datepicker({
    dateFormat: "yy-mm"
  });
});