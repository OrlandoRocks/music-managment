// graphs functions for Raphael


var Graph = function(prefs)  {
 
	var $ = jQuery;
	$("#follower").hide();
	
	
	var xPos = prefs.dimensions[0];
	var yPos = prefs.dimensions[1];
	var graphHeight = prefs.dimensions[3];
	var graphWidth = prefs.dimensions[2];
	
	var r = Raphael(prefs.id, graphWidth, graphHeight + 20);
	var spinner = r.image("/images/ajax-loader-sm.gif",108,68,24,24);

	function setColors() {
		
		r.g.txtattr.font = "10px 'Helvetica Neue', Helvetica, Arial, sans-serif";
		r.g.txtattr.fill = "White";
		r.g.colors = [];
		if (prefs.hues) { // first bar set is specified by caller
			r.g.colors.push("hsb(" + prefs.hues[0] + "%, " + prefs.hues[1] + "%, " + prefs.hues[2] + "%)");
    	} 
	};
	
	function setCaption(caption) {
		r.g.text(15,15, caption).attr({"text-anchor":"start","font-size":"12px"})
	}

	
	function setBackground() {
		var bg = r.rect(1, 25, graphWidth, graphHeight, 0).attr({fill: "#000", opacity: 0.8, stroke: "#666", "stroke-opacity": 0.5 }).toBack();
		//bg.animate({opacity:0.7},1000);	
	};
	
	function setRollover() {
		var c = r.rect(xPos,yPos,graphWidth,graphHeight,10).attr({fill: "#eee",opacity: 0.01}).toFront(); 
		
		follow = $("#help_placard");
		follow.hide();
		follow.css({"background-color":"#ffa"});
		c.click(function() { follow.hide(); window.location="/sales" });
		
		$("#"+prefs.id).mousemove(function(e){
			//c.animate({opacity:0.6},50);	
			follow.html(prefs.helptext)
			var offset = $("#states").offset();
	        follow.show();
			follow.css({
	            top: (e.pageY - offset.top + 10),
	            left: (e.pageX - offset.left + 10)
	        });
	    });
	
		$("#"+prefs.id).mouseout(function(e){
			//c.animate({opacity:0},100);	
			follow.hide();
		});	
		
	};
	
	var self = {
		init: function(dataset){
			setColors();
			setCaption(prefs.caption);
			setBackground();
			
			if(prefs.chartStyle == 'Line'){
				self.linechart(dataset);
			}else if(prefs.chartStyle == 'Bar'){
				self.barchart(dataset);
			}else if(prefs.chartStyle == 'HorizontalBar'){
				self.hbarchart(dataset);
			}
		},

		hbarchart: function(dataset) {

		
		    label = function() {  // for future use
			
				max_value = Math.max.apply(null, units);
				if (max_value/this.bar.value <= 10) {
					x_offset = this.bar.x-15;
				} else {
					x_offset = this.bar.x+15;
				}
			
				this.bar.insertBefore(r.g.text(x_offset, this.bar.y, this.bar.value).attr({"text-anchor":"right","font-weight": "bold"}));	
			};
			
			var songs=[],units=[];
			
			for (var i = 0, ii = dataset.data.length; i < ii; i++) {		
				units.push(dataset.data[i].total);
				songs.push(dataset.data[i].SongName);
		  	};
		
			var guttervals = ["0%","0%","300%","250%","200%","150%"];
			
			var bars = r.g.hbarchart(xPos,yPos,graphHeight,graphWidth,[units],{type: "round", gutter: guttervals[dataset.data.length]});
			
			for (var i = 1, ii = dataset.data.length; i <= ii; i++) {
				var label_offsets = [0,0,95,105,120,125];  // a variety of offsets to line up the bar graph labels
				iii = label_offsets[dataset.data.length]/ii;
				var song_title = songs[i-1].substr(0,40);
				if (songs[i-1].length > 40) { song_title = song_title+"..."; };
				r.g.text(15,(iii*i)+13,song_title).attr({"text-anchor":"start"});
			}
			
			setRollover();
			spinner.hide();
			
		},
				
		barchart: function(dataset) {
			var labels=[];
			var values=[];
			
			for (var i = 0, ii = dataset.length; i < ii; i++) {
				labels.push(dataset[i].label);
				values.push(dataset[i].value);
		  };
		  
			fin = function () {
				this.flag = r.g.flag(this.bar.x, this.bar.y, this.bar.value || "0", 270).insertBefore(this);
							},
		    fout = function () {
				this.flag.animate({opacity: 0}, 300, function () {this.hide();});
							},
		  	barlabel = function () {	
			
				num = this.bar.value+'';

				if (num.length >= 4) {
					for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++) {
						num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3));
					}
				}
				num = "$"+num;
			
				r.g.text(this.bar.x,this.bar.y-8,num);
		  };

		 var bars = r.g.barchart(xPos, yPos, graphWidth, graphHeight, [values], {stacked: true, type: "soft"});
		
		    bars.each(barlabel);
			var label_offsets = [0,0,40,30,20,13,10]; 
			for (var i = 1, ii = dataset.length; i <= ii; i++) {
				iii = 222/ii;
				r.g.text((iii*i)-label_offsets[dataset.length],795,labels[i-1]); 
				// not crazy about this hack to label the bars with months
			}
			bars.attr({opacity:1});
			bars.animate({opacity:1},500);
			setRollover();
			spinner.hide();
		},
		
		linechart: function(dataset) {
			var labels=[];
			var xCoords=[];
			var yCoords=[]
			var pxBetweenCoords=graphWidth/dataset.length;
			var	currentXCoord = 0;
			
			for (var i = 0, ii = dataset.length; i < ii; i++) {
				labels.push(dataset[i].label);
				xCoords.push(currentXCoord);
				yCoords.push((dataset[i].value/prefs.maxValue)*graphHeight);
				var line = r.path("M" + currentXCoord + " 0L" + currentXCoord + " " + graphHeight).attr({fill: "#000", stroke: "#fff", "stroke-width": 2});
				currentXCoord += pxBetweenCoords;
		  };

			//for (var i = 0, ii = xCoords.length; i < ii; i++){
			//	var line = r.line(xCoords[i], 23, pxBetweenCoords, graphHeight).attr({fill: "#000", stroke: "#fff", "stroke-width": 2});
			//}
			
//			for (var i = 0, ii = xCoords.length; i < ii; i++){
//				var line = r.path("M" + xCoords[i] + " 0L" + xCoords[i] + " " + graphHeight).attr({fill: "#000", stroke: "#fff", "stroke-width": 2});
//			}
	 		

			var line = r.g.linechart(xPos, yPos, graphWidth, graphHeight, xCoords, yCoords, {type: "soft"});
			

			
			spinner.hide();			
			
		}
		
	};

return self;
	
};