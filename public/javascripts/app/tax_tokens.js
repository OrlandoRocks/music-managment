$(document).ready(function(){
  editTaxForm()
})

function editTaxForm() {
  $("#edit-tax-form").on("click", function(e) {
    e.preventDefault();
    e.stopPropagation();
    $.ajax({
      type: 'PUT',
      dataType: 'json',
      url: this.href,
      success: function (response) {
        var token = response.token
        var win = window.open("https://taxforms.payoneer.com/?tk=" + token, '_blank');
         win.focus();
      }
    });
  })
}
