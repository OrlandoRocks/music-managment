/*
  money-graphs.js
  Draws a lot of pretty canvas things on the my account page
*/

var Dashboard = function(){
  var $ = jQuery;
  var revenueRequest = 'http://'+ TC.domain + '/tc_services/sales_6_mo_rev/';
  var unitsRequest = 'http://'+ TC.domain + '/tc_services/sales_6_mo_units/';
  var topTracksRequest = 'http://'+ TC.domain +'/tc_services/top_5_songs/';
  var ad1link = '/store/product/82';
  var ad2link = '/store/product/72';
  var ad3link = 'http://tunecore.discproductionservices.com/SelfServiceQuoter/PreSelectProject.aspx?configid=191376&itemid=CDONLINE-T-CONTENT&qty=100&proditemid=5DAY';
  
  var r = Raphael("states", 800, 170); // special Raph canvas just for the spinner Gifs
  var s1 = makeSpinners(108), s2 = makeSpinners(358), s3 = makeSpinners(588);

  //private functions
  function createCanvas(id, url) {
    var canvas = document.createElement('canvas');
    if ($.browser.msie) { // for ie/excanvas
      G_vmlCanvasManager.initElement(canvas); 
    };
    var $canvas = $(canvas);
    var link = $('<a href='+ url +'></a>');
    $canvas.attr({'id': id, 'height': 144, 'width': 236});
    link.html($canvas);
    return link;
  };

  function makeSpinners(x) {
    return r.image("/images/ajax-loader-sm.gif",x,78,24,24);
  };

  
  var self = { 
    
    init: function(){
      
      if (TC.displayGraphs) {
        self.getJson([s1,s2,s3]);
      } else {
        self.adFallback();
      };
      
    },

    getJson: function(s) {
      TC.portalConnector(revenueRequest, self.drawRevenueGraph, self.draw_ad_1);
      TC.portalConnector(unitsRequest, self.drawUnitsGraph, self.draw_ad_2);
      TC.portalConnector(topTracksRequest, self.drawTracksGraph, self.draw_ad_3);
    },

    drawRevenueGraph: function(json) {
      var f = new Graph({"id": "placard_1", "hues": [33,39,84], "type": "Revenue", "caption":"Earnings by Month",
      "helptext":"The amount of money you made from download sales and streams each month."}).barchart(json);
    s1.remove();
    },
    
    drawUnitsGraph: function(json) {
      var f = new Graph({"id": "placard_2","hues":  [9,38,100], "type": "UnitSales", "caption":"Unit Sales by Month",
       "helptext":"The number of album, songs and ringtone downloads your fans bought in digital stores. This does not include streams."}).barchart(json);
    s2.remove();
    },
    
    drawTracksGraph: function(json) {
      //console.log(3);
      var f = new Graph({"id": "placard_3", "hues": [50,39,67], "type": "TrendSongs", "caption":"Top Selling Songs in iTunes", "helptext" : "Your top song sales for the previous two weeks as reported in your iTunes Trend Reports."}).hbarchart(json);
    s3.remove();
    },
    
    adFallback: function() {
      self.draw_ad_1();
      self.draw_ad_2();
      self.draw_ad_3();
    },
    
    draw_ad_1: function(){
      var canvas = createCanvas('ad_1', ad1link);
      $('#placard_1').html(canvas);
      
      var elem = document.getElementById('ad_1');
      if (elem) { // test to see if canvas is on the page
        // Get the canvas 2d context.
        
        var context = elem.getContext('2d');

        //place bg image
        var img = new Image();
        img.src = '/version/shared/images/plaques/glossy_bg.png';
        $(img).load(function() {
          context.drawImage(this,0,0);
        });

        // write text
        context.font = 'bold 28px Hevetica, sans-serif';
        context.fillStyle = "#66CCFF";
        context.textAlign = 'center';
        if (context.fillText) {
          context.fillText('Single Packs', 118, 62);
          context.fillText('Save 10%', 118, 96);
        }
    s1.remove();
      };
    },
    
    draw_ad_2: function(){
      var canvas = createCanvas('ad_2', ad2link);
      $('#placard_2').html(canvas);
      
      var elem = document.getElementById('ad_2');
      if (elem) {
      // Get the canvas 2d context.
      var context = elem.getContext('2d');
      

      //place bg image
      var img = new Image();
      img.src = '/version/shared/images/plaques/glossy_bg.png';
      $(img).load(function() {
        context.drawImage(this,0,0);
      });
      
      // write text
      context.font = 'bold 30px Hevetica, sans-serif';
      context.fillStyle = "#66CCFF";
      context.textAlign = 'center';
      if (context.fillText) {
        context.fillText('Get', 118, 45);
        context.fillText('Guaranteed', 118, 80);
        context.fillText('Radio Plays', 118, 115);
        }
      }
    s2.remove();
    },
    
    draw_ad_3: function(){
      var canvas = createCanvas('ad_3', ad3link);
      $('#placard_3').html(canvas);
      
      var elem = document.getElementById('ad_3');
      if (elem) {
      // Get the canvas 2d context.
      var context = elem.getContext('2d');
      

      //place bg image
      var img = new Image();
      img.src = '/version/shared/images/plaques/glossy_bg.png';
      $(img).load(function() {
        context.drawImage(this,0,0);
      });
      
      // write text
      context.font = 'bold 30px Hevetica, sans-serif';
      context.fillStyle = "#66CCFF";
      context.textAlign = 'center';
      if (context.fillText) {
        context.fillText('Make 100', 118, 45);
        context.fillText('CDs for', 118, 80);
        context.fillText('$199', 118, 115);
        }
      }
      s3.remove();
    },
    
    draw_renewal_plaque: function(){
      var elem = document.getElementById('plaque-1');
      if (elem) { // test to see if canvas is on the page
        // Get the canvas 2d context.
        var context = elem.getContext('2d');

        //place bg image
        var img = new Image();
        img.src = '/version/shared/images/plaques/glossy_bg.png';
        $(img).load(function() {
          context.drawImage(this,0,0);
        });

        // write text
        context.font = 'bold 28px Hevetica, sans-serif';
        context.fillStyle = "#76C646";
        context.textAlign = 'center';
        if (context.fillText) {
          context.fillText('Your Annual', 118, 46);
          context.fillText('Renewal', 118, 76);
          context.fillText('is Due', 118, 106);
        }
      };
    }
  };
  
  return self.init();
};

jQuery(document).ready(function(){
  var f = new Dashboard();
});
