jQuery(document).ready(function($) {

  pushEventToDataLayer('view-facebook-pop-up',
  'view-pop-up-for-facebook-monetization');

  $('.fbtracks-sign-up-button').on('click', function() {
    pushEventToDataLayer('click-sign-up-on-facebook-pop-up', 
      'click-to-sign-up-for-facebook-monetization');
  });

  $('.fbtracks-modal-cancel, .close-modal').on('click', function() {
    pushEventToDataLayer('click-cancel-on-facebook-pop-up', 
      'click-to-close-facebook-monetization-pop-up');
  });

  function pushEventToDataLayer(eventName, eventAction) {
    dataLayer.push({event: eventName, 
                    category: "pop-up",
                    action: eventAction});
  }

});
