$(function() {

  $('#add-social-to-cart-btn').on('click', function() {
    pushEventToDataLayer('Cart_Page_TC_Pro_Add_to_Cart');
  });

  $('#add-ytsr-to-cart-btn').on('click', function() {
    pushEventToDataLayer('Cart_Page_YTSR_Add_to_Cart');
  });

  function pushEventToDataLayer(eventName) {
    dataLayer.push({event: eventName, label: 'Add to Cart'});
  }

});
