// M//
//  Store / Partner Manager Controller
//
//
jQuery(document).ready( function(){

  var manager = function(){

    var $ = jQuery;
    var checkbox = ":checkbox";
    var description = $('.edit_description');
    var modal = $('#modal');
    var form = '.desc_form';
    var msg_good = $('#goodErrorExplanation span');
    var msg_bad =  $('#errorExplanation span');

    modal.dialog({
      autoOpen: false,
      resizable: false,
      modal: true,
      draggable: false,
      width: 800,
      maxWidth: 800,
      close: function () {
        modal.html('');
      }
    });

    var checkbox_handler = function (link) {
      $.ajax({
        type: "POST",
        url: "manager/update",
        data: "value=" + link.val(),
        success: function (response) {
          if (response == "store") {
            link.parent().parent('tr').toggleClass('even');
          } else {
            link.parent('td').toggleClass('live');
          }
        }
      });
    };

    var description_handler = function (link) {
      $.ajax({
        url: link.attr('href'),
        success: function (response) {
          modal.html(response);
          modal.dialog('open');
        }
      });
    };

    var form_handler = function (link) {
      var f = $('.desc_form');
      $.ajax({
        type: 'POST',
        url: f.attr('action'),
        data: f.serialize(),
        success: function (response) {
          msg_good.html("Description saved!");
          msg_good.parent().show(0).fadeOut(5000);
        },
        error: function (response) {
          msg_bad.html("Description could not update.");
          msg_bad.parent().show(0).fadeOut(5000);
        },
        complete: function (response) {
          modal.dialog('close');
        }
      });
    };

    var self = {
      init: function() {
        $(checkbox).click(function() {
          checkbox_handler($(this));
        });

        description.click(function (e) {
          e.preventDefault();
          description_handler($(this));
        });

        $(document).on('submit', form, function (e) {
          e.preventDefault();
          form_handler($(this));
        });
      }
    };

    return self;
  }();

  manager.init();
});
