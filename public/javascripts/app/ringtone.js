/*
 *
 *  Ringtone Creation Form
 *  Based on album.js
 *
 */
var RingtoneForm = function(){
  // note: in sizzle, #id elm.class, or just elm.class is faster than .class
  // reusable style variables
  var $ = jQuery;
  var originalClass = '.original-date-box';
  var previouslyReleasedRadio = '.previously-released';
  var formInput = '.context-wrapper input, .context-wrapper select, .context-wrapper textarea';
  var prevReleasedRadios = '.prev-released';
  var artistNameField = '.artist-name';
  var various = '.various';
  var ringtoneTitle = $('#ringtone_album_name');
  var ringtoneArtist = $('#ringtone_creatives__name');
  var previouslySelectedId = '#previouslySelectedId';

  var self = {

    init: function(){
      $(prevReleasedRadios).change(this.setAsPreviouslyReleased);
      $(various).change(this.variousChangeEvent);
      self.setArtistName($('.various:checked'));
      self.initHelperText();
      self.initOriginalReleaseDate();
      self.checkRelatedSongPulldown();
      if (gon.locale === 'en') {
        self.initStyleguides();
      }
    },


    initStyleguides: function(){
      // single and album
      ringtoneTitle.styleGuide({
        rules: {
          'volume': /\sV(?!ol\.\s\d)|v[Oo][Ll](?:[Uu][Mm][Ee])?(\.)?\s(?:\d|[OoTtFfSsEeNn])/,
          'part': /\s(?:P[^t]|p[Tt]?|P)(?:[Aa][Rr][Tt])?(\S)?\s(?:\d|[oOtTfFsSeEnN])/,
          'featuring': /\s\W?f(?:ea)?t(?:(?:\w)+|(?:\s)|(?:\.)?)\s?/i,
          'produced' : /\s\W?[pP]rod(\W|uce|\s)/
        },
        messages: {
          'volume': 'Is this a multiple volume/disc release? If so, iTunes has specific formatting rules that must be followed. For example: "Album Title, Vol. 1"',
          'part': 'Is this a multiple volume/disc release? If so, iTunes has specific formatting rules that must be followed. For example: "Album Title, Pt. 1"',
          'featuring': 'Are you trying to add a featured artist to this song? If so, please enter them in the "Featured Artist" field below.',
          'produced' : 'Are you trying to enter a producer? Stores do not allow this information to be listed, please do not include or your release may be delayed.'
        }
      });

      ringtoneArtist.styleGuide({
        rules: {
          ',': /\,/,
          '/': /\//,
          '&': /\s\&\s/,
          '+': /\s\+\s/,
          '-': /\s\-\s/,
          'and': /\sand\s/i,
          'with': /\sw((ith)|\/)\s/i,
          'featuring': /\s\W?f(?:ea)?t(?:(?:\w)+|(?:\s)|(?:\.)?)\s?/i
        },
        messages: {
          ',': 'Are you trying to add another artist to this album? If so, please click "Add Another Main Artist" below.',
          '/': 'Are you trying to add another artist to this album? If so, please click "Add Another Main Artist" below.',
          '&': 'Are you trying to add another artist to this album? If so, please click "Add Another Main Artist" below.',
          '+': 'Are you trying to add another artist to this album? If so, please click "Add Another Main Artist" below.',
          '-': 'Are you trying to add another artist to this album? If so, please click "Add Another Main Artist" below.',
          'and': 'Are you trying to add another artist to this album? If so, please click "Add Another Main Artist" below.',
          'with': 'Are you trying to add another artist to this album? If so, please click "Add Another Main Artist" below.',
          'featuring': 'Are you trying to add a featured artist to this song? If so, please enter them in the "Featured Artist" field below.'
        }
      });

    },

    checkRelatedSongPulldown: function() {
      if ($(previouslySelectedId).val() > 0 ) {
        $('#ringtone_related_song_'+$(previouslySelectedId).val()).prop('checked','checked');
      }
    },

    initOriginalReleaseDate: function(){
      if( $(originalClass).attr('value') !== '' ){
        $(originalClass).parents('li').removeClass('hide');
        $(previouslyReleasedRadio).prop('checked','checked');
      }
    },

    variousChangeEvent: function(){
      self.setArtistName($(this));
    },

    setArtistName: function(el){
      if( $(el).val() === 'true' ){
        self.hideArtistName();
        self.clearArtistName();
      }
      else {
        self.showArtistName();
      }
    },

    clearArtistName: function(){
      $(artistNameField).val('');
    },

    hideArtistName: function(){
      $(artistNameField).parents('li').addClass('hidden');
    },

    showArtistName: function(){
      $(artistNameField).parents('li').removeClass('hidden');
    },

    initHelperText: function(){
      $(formInput).focus(this.showHelperText);
      $(formInput).blur(this.hideHelperText);
    },

    //
    // Determine if the album has already been released
    //
    setAsPreviouslyReleased: function(){
      if( $(this).hasClass('previously-released') ){
        $(originalClass).parents('li').removeClass('hide');
      }
      else {
        $(originalClass).parents('li').addClass('hide');
        $(originalClass).val('');
      }
    },

    //
    // Simple methods for hiding and showing contextual help for forms
    //
    showHelperText: function(){
      var help = $(this).nextAll('.contextual-help');
      if (help.find('p').text().length !== 0){
        help.fadeIn();
      }
    },
    hideHelperText: function(){
      var t = $(this);
      setTimeout(function(){
        t.nextAll('.contextual-help').fadeOut();
      }, 1000);
    }
  };

  return self.init();
};

var initRingtoneForm = function(){
  new RingtoneForm();
};
jQuery(document).ready(initRingtoneForm);
