jQuery(function ($) {
  $(document).ready(function(){
    var albumClipboard = new Clipboard(".url-copy-button");
    var trackClipboard = new Clipboard(".copy-track-uri");

    albumClipboard.on("success", function(e){
      toggleAlbumButtons($(e.trigger));
    });

    trackClipboard.on("success", function(e){
      toggleTrackButtons($(e.trigger));
    });

    $("#title_spotify_uri").append([
        "<i class='fa fa-info-circle uri-explanation' style='font-size: 10px; margin: 2px;' aria-hidden='true'></i>",
        "<div hidden>URIs can be embedded in a webpage to take listeners directly to your album on Spotify, without being routed through your web browser.</div>"
        ].join("")
    );

    $(".uri-explanation").on("mouseover", function(e){
      $(this).qtip({
        content: { text: $(this).next().text() },
        show: { event: e.type, ready: true },
        style: {
          classes: "album-uri-tooltip",
          tip: {
            corner: true,
            mimic: "center",
            width: 36,
            height: 12,
            offset: 10
          }
        },
        hide: { delay: 200, fixed: true },
        position: { my: "bottom right", at: "top right", target: $(this) },
      });
    });
  });

  var toggleAlbumButtons = function(elem){
    $(".copied").hide();
    $(".copied#" + elem.context.id).toggle();
  };

  var toggleTrackButtons = function(elem){
    elem.toggleClass("fa-chain fa-check");
    setTimeout(function(){
      elem.toggleClass("fa-chain fa-check");
    }, 1000);
  };
});
