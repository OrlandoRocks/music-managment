var $ = jQuery;

var pricing_json = { 
  "#one_year": {"album_price": "$29.99", "single_price": "$9.99", "ringtone_price": "$19.99" },
  "#two_year": {"album_price": "$37.99", "single_price": "$9.49", "ringtone_price": "$18.99" },
  "#five_year": {"album_price": "$41.39", "single_price": "$8.99", "ringtone_price": "$17.99" }
};
var PricingData = eval(pricing_json);

function updateContent(caller) {
  var cid = "#"+caller.attr("id");
  var item = $(caller).parent().attr("id")+"_price";
  
  $('#'+$(caller).parent().attr("id") +'_price').fadeOut(300, function() {
   	$(this).text(PricingData[cid][item]).fadeIn(300);
  });
};

function lightUp() {
  $(".price_choose").find(".one_year").addClass("lit");
};

$(document).ready(function() {
  setTimeout(lightUp, 500);
  
  $(".price_choose").find("li").hover(function() {
    $(this).siblings().removeClass("lit");
    $(this).addClass("lit");
  });
});