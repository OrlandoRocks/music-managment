
//
//  Simple Accordion
//

//
// Function
//
jQuery.fn.simple_accordion = function(options){

  var selector = this.selector;
  var $ = jQuery;
  var opts = $.extend(jQuery.fn.simple_accordion.defaults, options);

  //
  // Event Handlers
  //
  var toggle_clicked = function(){
    var el = $(this);
    opts.before_toggle(el); // callback
    toggle_body_visibility(el);
    opts.after_toggle(el); // callback
  }

  //
  // DOM Operations
  //
  var toggle_body_visibility = function(el){
    var container = el.closest(selector);
    var body = container.find(opts.body);
    body.toggleClass("hide");
  }

  return this.each(function(){
    $(this).find(opts.toggle).click(toggle_clicked);
  });
};

//
// Default Parameters
//
jQuery.fn.simple_accordion.defaults = {

  // Classes
  body: '.accordion-body', // the body
  header: '.accordion-header', // the header
  toggle: '.accordion-toggle', // the element to trigger the toggle


  // Callbacks
  before_toggle: function(){}, // callback, sends the element triggering the toggle
  after_toggle: function(){} // callback, sends the element triggering the toggle
};
