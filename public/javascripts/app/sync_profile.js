/* Profile.js
  = Description
  The following code allows the save, and edit links on the Album Page
  to make ajax requests for updating or adding profile content.
  
  = Change log
  [2011-06-29 -- RT] Initial Creation
  
  = Dependencies
  Depends on nothing.
  
  = Usage
  Not a function to be used outside this file.
  
*/
var TC = TC || {};

var Profile = function() {
  // Set up $ to be used locally for jQuery then
  // cache selectors so you don't have to traverse
  // the DOM each time.
  var $ = jQuery;
  var save_buttons = $(".save-button");
  
  var self = {
    init: function() {
      $('#goodErrorExplanation').fadeOut('slow');
      var goToTab = function(){
        var t = $(this);
        $('.active').removeClass('active');
        t.addClass('active');
        $('.tabSection').hide();
        $(t.attr('href')).show();
      };

      $('.tabHeader nav a').on('click', function(e){
        goToTab.call(this);
        e.preventDefault();
      });
      $(".profile_modal").dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        draggable: false,
        width: 800,
        maxWidth: 800
      });
      $('#account_settings').on('click', '.button-callout-alt', self.edit_changes);
      $(document).on("click", ".save-button", self.submit_changes);
    },
    
    //  Click Edit Button
    edit_changes: function(e) {
      e.preventDefault();
      var link = $(this).data('modal');
      $('#' + link).dialog('open');
    },
    
    //  Click Submit Button
    //  1. Ajax POST
    //  2. Append new profile info from response via load_post_results function
    submit_changes: function(e) {
      var $save_button = $(this);
      var $cancel_button = $save_button.parents("form").find(".cancel-button");
      var spinner = $save_button.parents("form").find(".spinner");
      var section = $save_button.parents(".section-body");
      var form = $save_button.parents("form");
      var href = form.attr("action");
      var data = form.serialize();
      var password_section = section.find("div.original-password");
      var original_password_is_blank = section.find("#person_original_password:blank");
      if (original_password_is_blank.length == 1){
        password_section.show();
        password_section.effect("highlight", {}, 1000);
      } else {
        $save_button.hide();
        $cancel_button.hide();
        spinner.show();
      
        $.post( // 1.
          href,
          data,
          function(response){
            if ( $(response).find(".fieldWithErrors").length){ // IF Error
              $('.ui-dialog-content:visible').html(response);
              section.find(".original-password").show();
              section.find("form").append('<li><a href="#" class="cancel-button">Cancel</a></li>');
              section.find("form").append('<li><img src="/images/icons/spinner-16x16.gif" class="spinner" alt="" style="display:none" /></li>');
            } else if ( section.find(".fieldWithErrors").length){ // ELSE IF Resubmit form
              $('.ui-dialog-content:visible').html(response);
              section.parent("li").effect("highlight", {}, 1000);
            } else { // ELSE Normal
              var dialog = $save_button.parents('.ui-dialog-content'),
                  contentName = dialog.attr('id');
              $('div.'+contentName).html(response); // 2.
              section.parent("li").effect("highlight", {}, 1000);
              $save_button.show();
              dialog.dialog("close");
              if(contentName == 'email_info') $('.emailtxt').html($('#person_email').val());
              var url = href.replace(/update/ig, 'edit');
              $.get(url, function(html){
                dialog.html(html);
              });
            }
          },
          "html"
        );
      }
      e.preventDefault();
    }
  };
  return self.init();
};

jQuery(document).ready(function($){
  TC.profile = new Profile();
});
