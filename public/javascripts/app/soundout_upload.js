$("#song_library_asset:file").change(e => {
  const $input = $(e.target);

  if (isExtensionValid($input.val())) {
    const $form = $input.closest("form"),
          $submitButton = $form.find("button#upload_song_btn"),
          $spinner = $form.find(".spinner");

    $form.submit();
    $submitButton.hide();
    $spinner.css('display', 'flex'); // Unhide the spinner
  } else {
    $('.soundout_error')
      .html('<strong>ERROR:</strong> The file must be in the MP3 format.')
      .show();
  }
});

$('.upload_tracks').on('click', '#upload_song_btn', function(e) {
  e.stopImmediatePropagation();
  e.preventDefault();

  if (isFormDataValid()) {
    $('.upload_file_field').click();
  } else {
    $('.soundout_error')
      .html('<strong>ERROR:</strong> All fields are required.')
      .show();
  }
});

function isExtensionValid(fileName) {
  const extension = fileName.split('.').pop();

  return extension == 'mp3';
}

function allNonEmptyValues(accum, el) {
  return accum && $(el).val().trim() !== '';
}

function isFormDataValid() {
  return $('.upload_input').toArray().reduce(allNonEmptyValues, true);
}
