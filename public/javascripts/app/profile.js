var $ = jQuery;
var TC = TC || {};
$(document).ready(function($){
  $('#goodErrorExplanation').fadeOut(5000, "swing");
  var formatDate = function(date){
    var date = new Date(date);
    return (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear();
  };
  var bindZClipOnce = !1,
    bindZclip = function(){
      if(!bindZClipOnce){
        bindZClipOnce = !!1;
        $('a#copy_link').zclip({
          path: '/flash/ZeroClipboard.swf',
          copy: function(){ return $('input#copy_input').val(); },
          afterCopy: function(){
            copyAlert();
          }
        });
      }
    };
  var copyAlert = function(){
    var btn = $('a#copy_link');
    btn.animate({
      backgroundColor: 'green'
    }, 500, function(){
      btn.html('Copied');
      setTimeout(function(){
        btn.animate({
          backgroundColor: '#44B1FA'
        }, 500, function(){
          btn.html('Copy');
        });
      }, 2000);
    });
  };
  var goToTab = function(){
    var t = $(this);
    $('.active').removeClass('active');
    t.addClass('active');
    $('.tabSection').hide();
    $(t.attr('href')).show();
    if(t.hasClass('referLink')) bindZclip();
    if(t.attr('href') == '#account_profile') {
      var ifr = $('#survey-iframe');
      var surveyToken = ifr.attr("class");
      ifr.attr('src','https://www.surveygizmo.com/s3/2218482/Customer-Profile-Survey-Revised-7-1?sguid=' + surveyToken);
    }
  };
  if($('.referLink').hasClass('active')) goToTab.call($('.referLink'));

  $('.tabHeader nav a').on('click', function(e){
    goToTab.call(this);
    e.preventDefault();
  });
  $('.goToHistory').on('click', function(e){
    $('.historyLink').click();
    e.preventDefault();
  });
  $("#my_referrals, #my_payouts").dialog({
    autoOpen: false,
    resizable: false,
    modal: true,
    draggable: false,
    width: 800,
    maxWidth: 800
  });
  $("#paypalModal").dialog({
    autoOpen: false,
    resizable: false,
    modal: true,
    draggable: false,
    width: 320,
    maxWidth: 400
  });
  $('.tabSection .add_pp_link').on('click', function(e){
    $('#paypalModal').dialog('open');
    e.preventDefault();
  });
  $('a.my_referrals').on('click', function(e){
    $('#my_referrals').dialog('open');
    var table = $('#my_referrals table tbody');
    $.getJSON('/friend_referral/referee_list.json', function(response){
      var html = '',
        referees = response.referees;
      if(referees.length > 0){
        $.each(referees, function(k,v){
          html += '<tr><td>'+v.name+'</td><td>'+formatDate(v.date)+'</td></tr>';
        });
      }
      else{
        html += '<tr><td colspan="2">You currently have no referrals</td></tr>';
      }
      table.html(html);
    });
    e.preventDefault();
  });
  $('a.my_payouts').on('click', function(e){
    $('#my_payouts').dialog('open');
    var table = $('#my_payouts table tbody');
    $.getJSON('/friend_referral/payouts.json', function(response){
      var html = '',
        payouts = response.payouts;
      if(payouts.length > 0){
        $.each(payouts, function(k,v){
          html += '<tr><td>'+v.amount+'</td><td>'+formatDate(v.date)+'</td></tr>';
        });
      }
      else{
        html += '<tr><td colspan="2">You currently have no payouts</td></tr>';
      }
      table.html(html);
    });
    e.preventDefault();
  });
  $('.ambassador_signup').on('click', function(e){
    $('#signupAmbassadorForm').submit();
    $('.ambassador_signup').fadeOut();
    e.preventDefault();
  });
  $('.share_url').on('click', function(){
    $(this).focus().select();
  });
  $('#refer').on('click', 'a.toggle', function(e){
    var t = $(this),
      parent = t.parents('.row:first');
    if(t.html() == '+'){
      t.html('-');
      parent.next().show();
    }
    else{
      t.html('+');
      parent.next().hide();
    }
    e.preventDefault();
  });
  $(".payoneer-tax-navigation-image").magnificPopup({type: "image"});
});
