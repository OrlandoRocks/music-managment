(function(){
  //create on click handlers
  var $ = jQuery;
  clickCheckbox();
  $('.mass-pay-checkbox').click(clickCheckbox);
  $('.mass-pay-select-all').click(() => setTimeout(clickCheckbox, 50));
  $('.submit-paypal-payouts').click(submitPaypalPayouts);
  function submitPaypalPayouts() {
    var confirmed = confirm('Click OK to actually process these payments through PayPal now. This cannot be undone!');
    if (!confirmed) {
      return false;
    };
    setTimeout(function() {
      $(".submit-paypal-payouts").prop('disabled', true);
    }, 50);
  };

  function clickCheckbox() {
    var hasCheckedItem = $('.mass-pay-checkbox:checked').length > 0;
    $(".submit-paypal-payouts").prop('disabled', !hasCheckedItem);
    $(".submit-check-payouts").prop('disabled', !hasCheckedItem);
  };
})();
