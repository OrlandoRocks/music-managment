(function(){
  var $ = jQuery

  $(document).ready(function(){

    var allAlbumIdsThisPage   = getAllAlbumIdsThisPage()
    var allAlbumIdsAllPages   = gon.failed_takedown_album_ids
    var selectedIds           = [];
    var $selectedIdsText      = $("#selected_album_ids")

    clearSelections();

    $(".album_id").change(function() {
      var albumId   = this.id
      var $checkbox = $(this)

      $checkbox.prop('checked') ? addId(albumId) : removeId(albumId)
      updateIdsDisplay();
    });

    $(".select_all_check_box").change(function() {
      var $checkbox = $(this)
      manageSelectAllChange($checkbox);
    });

    function addId(id) {
      if (!alreadySelected(id)) {
        selectedIds.push(id);
      }
    }

    function addListOfIds(idList) {
      var limitedList = idList.slice(0,2000)
      $.each(limitedList, function(index, val) {
        addId(val);
      })
    }

    function removeId(id) {
      if (alreadySelected(id)) {
        var idIndex = $.inArray(id, selectedIds)
        selectedIds.splice(idIndex, 1);
      }
    }

    function alreadySelected(id) {
      return selectedIds.includes(id);
    }

    function updateIdsDisplay() {
      if (selectedIds.length > 0) {
        $selectedIdsText.html(formatIds(selectedIds));
      } else {
        clearSelections();
      }
    }

    function formatIds(ids) {
      if (ids.length > 2000) {
        var html = "<p>There are too many failed takedowns to display - listing only the 2,000 most recent albums.</p>"
      }
      return (html || "").concat("<p>" + ids.slice(0,2000).join(", ") + "</p>");
    }

    function getAllAlbumIdsThisPage() {
      var ids = $(".album_id").map(function(index, el) {
        return el.id
      });

      return ids.get();
    }

    function checkAllAlbums(boolean) {
      $.each($(".album_id"), function(index, val) {
        $(val).prop('checked', boolean);
      });
    }

    function clearSelections() {
      selectedIds = []
      checkAllAlbums(false);
      $selectedIdsText.html("<p>No Albums Selected</p>");
    }

    function manageSelectAllChange($checkbox) {
      clearSelections()
      if ($checkbox.prop('checked')) {
        checkAllAlbums(true);
        uncheckOtherSelectAll($checkbox);
        updateSelectedIDsForThisSelectAll($checkbox)
      } else {
        clearSelections();
      }
    }

    function updateSelectedIDsForThisSelectAll($checkbox) {
      var selectAll = $checkbox.prop("id")

      if (selectAll == "this_page_only") {
        addListOfIds(allAlbumIdsThisPage)
        $selectedIdsText.html(formatIds(allAlbumIdsThisPage));
      } else if (selectAll == "all_pages") {
        addListOfIds(allAlbumIdsAllPages)
        $selectedIdsText.html(formatIds(allAlbumIdsAllPages));
      }
    }

    function uncheckOtherSelectAll($checkbox) {
      var selectAll = $checkbox.prop("id")

      if (selectAll == "this_page_only") {
        $("#all_pages").prop('checked', false)
      } else if (selectAll == "all_pages") {
        $("#this_page_only").prop('checked', false)
      }
    }

  })
})();
