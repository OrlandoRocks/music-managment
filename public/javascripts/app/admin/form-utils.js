jQuery(document).ready(function($){
  updateSelectedCount();

  $("input.check_boxes").click(function() {
    var checkedCheckboxes = $(".check_boxes:checked").length;
    var checkboxesLength  = $(".check_boxes").length;
    var checkAllCheckbox  = checkedCheckboxes === checkboxesLength;

    updateSelectedCount();

    $("#all_toggler").attr("checked", checkAllCheckbox);
  });

  $("#all_toggler").click(function() {
    var checkedAll = this.checked;

    $.each($(".check_boxes"), function(i, e) {
      $(e).attr("checked", checkedAll);
    });

    updateSelectedCount();
  });


  function updateSelectedCount() {
    var checkedCheckboxes = $(".check_boxes:checked").length;
    var checkboxesLength  = $(".check_boxes").length;

    $(".numSelected").html(checkedCheckboxes);
    $(".totalCheckBoxes").html(checkboxesLength);
  }
});
