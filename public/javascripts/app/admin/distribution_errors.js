jQuery(function(){
  selectAllCheckbox();
  individualCheckbox();
  retryAllFormSubmit();
  scrollToTable();
  scrollToTop();
});

var $ = jQuery;

function selectAllCheckbox() {
  $(".select-all").click(function() {
    var $this            = $(this);
    var tableIndex       = $this.data("table");
    var selectAllChecked = $this.is(":checked");
    var $tableRows       = $("#distribution_errors-table-rows-" + tableIndex + " input");

    $tableRows.each(function(i, row) { row.checked = selectAllChecked });
  });
};

function individualCheckbox() {
  $(".individual-checkbox").click(function() {
    var tableIndex        = $(this).data("table-index");
    var $tableRows        = $("#distribution_errors-table-rows-" + tableIndex + " :input");
    var $table            = $("#distribution_errors-table-" + tableIndex);
    var selectAllCheckbox = $(".select-all", $table)[0];
    var count             = 0;

    $tableRows.each(function(i, row) { if(row.checked) count++; });
    selectAllCheckbox.checked = (count == $tableRows.length);
  });
};

function retryAllFormSubmit() {
  $(".distribution_errors-retry-all-btn").click(function(e) {
    e.preventDefault();
    var distributionIds = $(".individual-checkbox::checkbox:checked").map(function() {
      return $(this).val();
    }).get();

    $.post("/admin/distribution/errors", { distribution_ids: distributionIds });
  });
};

function scrollToTop() {
  $(".scroll-to-top").click(function(e) {
    e.preventDefault();
    $("html").scrollTop(0);
  });
};

function scrollToTable() {
  $(".scroll-to-table").click(function(e) {
    var table = this.className.split(' ').first();
    document.getElementById(table).scrollIntoView(true);
    window.scrollBy(0, -20);
  });
};

function flashRetryMessage(distributionIds) {
  var message = "<strong style='color: red;'>Retrying Distribution.</strong>";

  distributionIds.forEach(function(id) {
    $("tr.distribution-id-" + id + " td:nth-child(3)").html(message);
  });
};

function clearDistributions(distributionIds) {
  setTimeout(function() {
    var removedRowsFromTable = {};

    distributionIds.forEach(function(id) {
      var $row        = $("tr.distribution-id-" + id);
      var $tableIndex = $(".distribution-id-" + id + " .individual-checkbox").data('table-index');

      removedRowsFromTable[$tableIndex] = (removedRowsFromTable[$tableIndex] || 0) + 1;
      $row.remove();
    });

    updateErrorsText(removedRowsFromTable);
    updateErrorsTotal(removedRowsFromTable);
  }, 2000);
};

function updateErrorsText(removedRowsFromTable) {
  Object.keys(removedRowsFromTable).forEach(function(tableIndex) {
    var $table             = $("#distribution_errors-table-" + tableIndex);
    var $errorsTextSummary = $(".distribution_errors-table-" + tableIndex);
    var tableRowsLength    = $("tbody tr", $table).length;

    if (tableRowsLength === 0) {
      $table.remove();
      $errorsTextSummary.remove();
    } else {
      var currentCount = $(".table-errors", $table).text().trim().split(" ").first();
      var newCount     = currentCount - removedRowsFromTable[tableIndex];
      var errorsText   = newCount < 2 ? " error" : " errors";

      $(".errors-summary", $errorsTextSummary).text(newCount + errorsText);
      $(".table-errors", $table).text(newCount + errorsText);
    }
  })
};

function updateErrorsTotal(removedRowsFromTable) {
  var removedErrors = 0;

  $.each(removedRowsFromTable, function(key, value) {
    removedErrors += value;
  });

  var currentTotal  = $(".distribution_errors-summary-total").text().trim().split(' ').first();
  var newTotal      = currentTotal - removedErrors
  var errorsSummary = newTotal < 2 ? " Error" : " Total Errors";

  $(".distribution_errors-summary-total").text(newTotal + errorsSummary);
};
