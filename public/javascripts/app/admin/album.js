/*
 *  Distribution Overview
 *  Simple bind function to a select change and updates distribution's variable price.
 */

var Distribution = function(){

  // reusable style variables
  var $ = jQuery;

  var change_subscription_status = function (link) {
    $.ajax({
      url: link.attr('href'),
      type: 'POST',
      dataType: 'json',
      beforeSend: function (xhr) {
        link.toggle();
      },
      success: function (response) {
        var status = response.active ? "Active" : "Inactive";
        $('#automator_status').html(status);
        $('#automator_status').effect('highlight', {}, 2000);
      },
      complete: function (response) {
        link.toggle();
      }
    });
  };

  var self = {
    init: function(){
      self.init_bind();
    },

    init_bind: function(){
      $('.salepoint_variable_price').bind("change", self.update_variable_price);
      $('.aod_template_id').bind("change", self.update_aod_template_id);
      $('#automator_toggle_link').click(function(e) {
        e.preventDefault();
        change_subscription_status($(this));
      });
    },

    update_variable_price :function(){
      var form = $(this).parents("form");
      var href = form.attr("action");
      var data = form.serialize();
      $.post(
          href,
          data,
          self.load_post_results,
          "json"
      );
      return false;
    },

    update_aod_template_id :function(){
      var form = $(this).parents("form");
      var href = form.attr("action");
      var data = form.serialize();
      $.post(
          href,
          data,
          self.load_post_aod_template_results,
          "json"
      );
      return false;
    },

    load_post_aod_template_results: function(response){
      var salepoint_id = $(response).attr("id")
      var response_message = $(response).attr("message")
      var update_status_id = "#update_aod_template_status_" + salepoint_id
      $(update_status_id).html(response_message)
      $(update_status_id).fadeIn(3000)
      $(update_status_id).fadeOut(6000);
    },

    load_post_results: function(response){
      var salepoint_id = $(response).attr("id")
      var response_message = $(response).attr("message")
      var update_status_id = "#update_status_" + salepoint_id
      $(update_status_id).html(response_message)
      $(update_status_id).fadeIn(3000)
      $(update_status_id).fadeOut(6000);
    }

  };

  return self.init();
};

/*
 *  Track Overview
 *  Simple bind function to a check box click and updates track as album only
 */

var Track = function(){
  var $ = jQuery;
  var self = {
    init: function(){
      self.init_bind();
    },
    init_bind: function(){
      $('.song_distribution_option_popup').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '.note_text'
      });
    },
  }
  return self.init();
};


jQuery(document).ready( function($){
  Distribution();
  Track();
  $(document).on("click", ".block_distro", function(e) {
    e.preventDefault();
    var $this = $(this),
    store = $this.data("store"),
    link = $this.attr("href");

    $("#block-confirm").dialog({
      resizable: false,
      draggable: false,
      height:140,
      modal: true,
      buttons: {
        "Block": function() {
          window.location = link;
        },
        Cancel: function() {
          $( this ).dialog( "destroy" );
        }
      }
    }).html("Are you sure you want to block <span class='store'>"+ store +"</span>?");
  });

  $(document).on("click", ".unblock_distro", function(e) {
    e.preventDefault();
    var $this = $(this),
    store = $this.data("store"),
    link = $this.attr("href");

    $("#unblock-confirm").dialog({
      resizable: false,
      draggable: false,
      height:140,
      modal: true,
      buttons: {
        "Un-Block": function() {
          window.location = link;
        },
        Cancel: function() {
          $(this).dialog("destroy");
        }
      }
    }).html("Are you sure you want to un-block <span class='store'>"+ store +"</span>?");

  });

  $(document).on('click', '.edit_golive_date', function(e) {
    $('#timed_release_fields').show();
  });

  $(document).on('click', '.timed_release_cancel', function(e) {
    $('#timed_release_fields').hide();
  });
});
