(function() {
  var $ = jQuery;
  $(document).on("ready", function() {
    $("table.admin-stores").dataTable({
      bPaginate: false,
      aaSorting: [[ 5, "desc" ]],
      scrollX: true
    });
  });
})();
