var EftRejection = function(){
  var $ = jQuery;
  var form = '.edit_eft_batch_transaction';
 	var self = {
   	init_validation: function(){
	    $(form).validate({
	      rules: { 'note[note]': {required: true}},
	      messages: {	'note[note]': "Required" } 
			});
  	}
	};
  self.init_validation();
  return self;
};
jQuery(document).ready(function(){
  new EftRejection();
});