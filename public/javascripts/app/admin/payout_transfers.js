jQuery(document).ready(function($){
  $(".admin-payout-transfers-table input[type='checkbox']").click(function() {
    var selectedCount = $(".check_boxes:checked").length;
    console.log("selected count", selectedCount, $("input.admin-payout-transfers-submit"));
    $("input.admin-payout-transfers-submit").attr("disabled", selectedCount === 0);
  });
});
