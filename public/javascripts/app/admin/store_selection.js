//
//
//  Drives the Admin salepoint price selection.
//
//
var SalepointVariablePriceAccordion = function(options){

  var $ = jQuery;
  var container = ".store-group";
  var link_state = "highlight";
  var toggle_links = ".toggle";

  //
  //  Actions
  //
  var hide_all_select = function(el){
    el.closest(container).find(".all-select").toggleClass("hide");
  }

  var switch_toggle_links = function(el){
    el.siblings(toggle_links).addClass(link_state);
    el.removeClass(link_state);
  }

  //
  //  Callbacks
  //
  var after_toggle = function(el){
    hide_all_select(el);
    switch_toggle_links(el);
  }

  var init = function(){

    // init accordion
    $(container).simple_accordion({
      toggle: toggle_links,
      header: ".all-stores",
      body: ".individual-stores",
      after_toggle: after_toggle
    });
  }();

}

var SalepointVariablePriceSelects = function(options){

  var $ = jQuery;
  var container = ".store-group";

  $('.select-all').on('change', function () {
    var checked = this.checked
    $.each($('.store-checkbox'), function (index, ele) {
      ele.checked = checked;
    })
  })

  $('.store-checkbox').on('change', function () {
    var checked = this.checked
    if ( !checked ) {
      $('.select-all')[0].checked = false
    }
  })

  $('select').on('change', function () {
    $(this).parent().parent().children('.grid_1').children('.store-checkbox')[0].checked = true
  })

  var select_changed = function(){

    // find the individual selects
    var select_class = $(this).attr("class");
    var individuals = ".individual-stores ." + select_class;
    var selects = $(this).closest(container).find(individuals);

    // reset value
    selects.attr("value", $(this).attr("value"));

  }

  var init = function(){
    $(".all-select select").change(select_changed);
    $("input[type='submit']").removeAttr("disabled");
  };

  // kick it off
  init();
}

jQuery(document).ready(function(){
  SalepointVariablePriceAccordion();
  SalepointVariablePriceSelects();
});
