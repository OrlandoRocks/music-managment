jQuery(document).ready(function($) {

  var service, albumId, $button;

  $('#getAppleInfo, #getSpotifyInfo').on('click', function(event) {
    setVars(event)
    getExternalInfo();
    $button.attr('disabled', true)
    return false;
  });

  function setVars(event) {
    $button = $(event.target)
    albumId = $button.attr('data-album-id')
    service = $button.attr('data-external-service')
  }

  function getExternalInfo() {
    $.post('/admin/external_service_ids', {
      external_service: service,
      linkable_type: 'Album',
      linkable_id: albumId
    }).done(function() {
      $button.val('Request sent');
    });
  }

});
