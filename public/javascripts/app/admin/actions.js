jQuery(document).ready(function($){
  $(document).on("click", ".block-ytm", function(e) {
    e.preventDefault();

    $("#block-confirm").dialog({
      resizable: false,
      draggable: false,
      height: 150,
      width: 450,
      modal: true,
      buttons: {
        "Block": function() {
          $(".block-ytm-form").submit();
        },
        "Cancel": function() {
          $(this).dialog("destroy");
        }
      }
    }).find(".data-ytsr").html("Note that this will remove all currently monetized tracks from the YouTube Sound Recording Service on this account and prevent the user from re-purchasing the service.");
  });

  $(document).on("click", ".unblock-ytm", function(e) {
    e.preventDefault();

    $("#unblock-confirm").dialog({
      resizable: false,
      draggable: false,
      height: 100,
      width: 450,
      modal: true,
      buttons: {
        "Unblock": function() {
          $(".unblock-ytm-form").submit();
        },
        "Cancel": function() {
          $(this).dialog("destroy");
        }
      }
    }).find(".data-ytsr").html("Note that this will allow the user to send their tracks to be monetized on YouTube again.");
  });


  $(document).on("click", ".block-withdrawals", function(e) {
    e.preventDefault();

    $("#block_withdrawals").dialog({
      resizable: false,
      draggable: false,
      height: 100,
      width: 450,
      modal: true,
      buttons: {
        "Block": function() {
          $(".block-withdrawals-form").submit();
        },
        "Cancel": function() {
          $(this).dialog("destroy");
        }
      }
    }).find(".data-block-withdrawals").html("Are you sure you want to block this account from requesting withdrawals?");
  });

  $(document).on("click", ".unblock-withdrawals", function(e) {
    e.preventDefault();

    $("#unblock_withdrawals").dialog({
      resizable: false,
      draggable: false,
      height: 100,
      width: 450,
      modal: true,
      buttons: {
        "Unblock": function() {
          $(".unblock-withdrawals-form").submit();
        },
        "Cancel": function() {
          $(this).dialog("destroy");
        }
      }
    }).find(".data-unblock-withdrawals").html("Are you sure you want to un-block this account from requesting withdrawals?");
  });
});
