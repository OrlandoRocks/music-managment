(function() {
  $(document).on("ready", function() {
    var $ = jQuery;
    var $adminCheckboxListItem = $("li.admin-role");
    var $adminCheckbox = $adminCheckboxListItem.children("input")[0];
    var $otherAdministrativeRolesListItem = $("li.other-administrative-roles");
    var $otherAdministrativeRolesCheckboxes = $("input.other-administrative-role-checkboxes");
    function showOrHideAdminRoles(checked){
      if(!checked){
        $otherAdministrativeRolesListItem.hide();
        $otherAdministrativeRolesCheckboxes.each( function (index, el) {
          el.checked = false;
        });
      }else{
        $otherAdministrativeRolesListItem.show();
      }
    }


    $adminCheckboxListItem.on("click", function() {
      showOrHideAdminRoles($adminCheckbox.checked);
    });
  });
})();


