jQuery(document).ready(function($){
  $('.open-finalize-popup').magnificPopup({
    type: 'inline',
    midClick: true
  });

  $('.open-unfinalize-popup').magnificPopup({
    type: 'inline',
    midClick: true
  });

  $(document).on('submit', '#mass_finalize_unfinalize_form', function(e){
    e.preventDefault();
    var selectedAction = $('input:radio[name="selected_action"]:checked').val();
    !$(this).hasClass('ready') ? $(`.open-${selectedAction}-popup`).click() : this.submit();
  });

  $('#finalize-popup').on('click', '.needs-review, .no-review-needed', function(){
    var reviewNeeded = $(this).hasClass('needs-review') ? 'true' : 'false';
    $('#review-field').val(reviewNeeded);
    $('#mass_finalize_unfinalize_form').addClass('ready');
    $('#mass_finalize_unfinalize_form').unbind('submit').submit();
  });

  $('#unfinalize-popup').on('click', '.unfinalize', function(){
    $('#mass_finalize_unfinalize_form').addClass('ready');
    $('#mass_finalize_unfinalize_form').unbind('submit').submit();
  });

  $(document).on('click', '.close-unfinalize-popup', function (e) {
    e.preventDefault();
    $.magnificPopup.close();
  });
});
