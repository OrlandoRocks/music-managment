jQuery(document).ready(function($) {
  $('#admin-wrapper').attr('id', '');
  var emailType = $('#email_type').val();

  $('body').on('change', '.email_type', function(){
    $('#isrc-search-form').submit();
  });

  $("#isrc-search-form").bind('ajax:complete', function(){
    jQuery('#status-update').html("");
    emailType = $('#email_type').val();
    updateActionButtonsDisplay();
  });

  $('body').on('blur', '.custom_fields', function(e){
    var currentRow = $(this).closest('tr');
    var person_id = $(currentRow).data('person-id');
    var currentCustomFields = $('.data-row-'+person_id).find('.custom_fields');
    var rowCheckbox = $('.data-row-'+person_id+' td.ready-checkbox .song-checkboxes');
    if (allInputsPopulated(currentCustomFields) == true)
    {
      $(rowCheckbox).attr('disabled', false);
      $(rowCheckbox).prop('checked', true);
    }
    else
    {
      $(rowCheckbox).attr('disabled', false);
      $(rowCheckbox).prop('checked', false);
      $(rowCheckbox).attr('disabled', true);
    }

    if ($('.song-checkboxes').is(':checked') == true)
    {
      $('#send-email-button').attr('disabled', false);
    }
      else
    {
      $('#send-email-button').attr('disabled', true);
    }

  });

  $('body').on('submit', '.email-form', function(){
    $('#send-email-button').attr('disabled', true);
  });

  $('body').on('click', '.add-row-button', function(){
    var currentRow = $(this).closest('tr');
    var person_id = $(currentRow).data('person-id');
    var custom_fields = $(currentRow).data('custom-fields');

    var rowCopy = currentRow.clone();
    cleanElementRow(rowCopy);
    rowCopy.insertAfter(currentRow);

    $('.data-row-'+person_id).each(function(index, row){
      var song_id = $(row).data('song-id');
      $(custom_fields).each(function(i, field_name){
        var prefix = "email_data["+person_id+"]["+song_id+"]["+(index+1)+"]["+field_name+"]"
        $(row).find('.'+field_name).attr('name', prefix);
      });
    });
  });

  $('body').on('click', '.remove-row-button', function(){
    $(this).closest('tr').remove();
  });

  var updateActionButtonsDisplay = function(){
    if (emailType == 'authorization')
      $('.add-row').show();
    else
      $('.add-row').hide();
  }

  var cleanElementRow = function(row){
    var cells = row.find('td');
    $.each(cells, function(i, item){
      var cell = $(item);
      if (cell.hasClass('input-cell') == true)
        cell.find('.custom_field').val('');
      else if (cell.hasClass('isrc-cell') == false && cell.hasClass('add-row') == false)
        cell.html('');
      else if (cell.hasClass('add-row') == true)
        cell.html('<a href="#!" class="remove-row-button">Remove Claim</button>');
    });
    updateActionButtonsDisplay();
  };

  var allInputsPopulated = function(input_fields) {
    var valid = true;
    input_fields.each(function () {
      if (!$.trim($(this).val()) == true) { valid = false; }
    });
    return valid;
  }
});
