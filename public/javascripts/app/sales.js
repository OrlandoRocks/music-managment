/* Sales.js
  = Description
  The following code allows the sales page to have select all buttons,
  table sorting, collapsing panels, and ajax requests.

  = Change log
  [2010-12-30 -- RT] Initial Creation
  [2011-01-03 -- RT] Added comments, and collapsible toggles on individual release pages
  [2011-01-07 -- RT] Replaced filter accordions with the same collapsible toggles
  [2011-01-10 -- RT] Refactored collapsible panels into reproducible code
  [2011-01-10 -- RT] Added table pager plugin
  [2011-01-10 -- RT] Added a class that allows opened by default panels
  [2011-01-11 -- RT] Added table sorter to individual releases
  [2011-01-14 -- RT] Added ajax function for songs tables
  [2011-01-20 -- RT] Added functions to produce show more panels and links
  [2011-01-31 -- RT] Moved hash loader into separate function
  [2011-02-04 -- RT] Added state saving to cookie method
  [2011-02-24 -- RT] Added negative date hider
  [2011-03-18 -- RT] Added a decimal shower and hider

  = Dependencies
  Depends on the jquery.tablesorter, jquery.tablesorter.pager, jquery.rule, and jquery.cookie plugin.

  = Usage
  Not a function to be used outside this file.

*/
var TC = TC || {};
var Sales = function () {

  // Set up $ to be used locally for jQuery then
  // cache selectors so you don't have to traverse
  // the DOM each time.
  var $ = jQuery;
  var cookie_filter = $.cookie('filter_state');
  var cookie_hidden = $.cookie('hidden_state');
  var filter_state = cookie_filter ? cookie_filter.split('|').getUnique() : [];
  var hidden_state = cookie_hidden ? cookie_hidden.split('|').getUnique() : [];
  var collapsible = 'div.collapsible';
  var song_links = '#song-sales-record .toggle-button';
  var container = '#sales-filters .container';
  var show_more = '.show_more';
  var $salesTable = $('#sales-table');
  var $date_start_selector = $('#date_start_date');
  var $date_end_selector = $('#date_end_date');
  var $sales_total = $('#sales-totals span');

  var self = {

    init: function () {
      self.currency_decimal_hider();
      self.custom_date_parser();
      self.sales_table_sorting(); // optimize me
      self.collapsible_panels( $(collapsible) );
      $(song_links).one('click', self.show_song_sales);
      self.hash_load( $(collapsible) );
      self.cookie_load(); // optimize me
      self.negative_dates_hider();
      self.filter_type_hider();
      self.filter_show_hide();
      self.release_table_expand_collapse();
      self.sales_table_search();
    },

    // This uses the dataTables plugin to create sorting and other
    // functions for the sales tables. The actual method is contained in the
    // dataTable_maker() function to make the code DRY.
    //
    sales_table_sorting: function () {
      if ($salesTable.find('tbody tr').length <= 10) {
        $('.pager').hide();
      }
      if ( $('#sales-table').hasClass('alternate') ) {
        $salesTable.tablesorter({
          headers: {
            0: { sorter: 'monthYear' },
            3: { sorter: 'commaNumber' },
            4: { sorter: 'currencyUnit' },
            5: { sorter: 'currencyUnit' },
            7: { sorter: 'commaNumber' }
          },
          widgets: ['zebra']
        });
      } else if ( $salesTable.hasClass('date-report') ) {
        $salesTable.tablesorter({
          headers: {
            0: { sorter: 'monthYear' },
            1: { sorter: 'commaNumber' },
            2: { sorter: 'commaNumber' },
            3: { sorter: 'commaNumber' },
            4: { sorter: 'commaNumber' }
          },
          widgets: ['zebra']
        });
      } else if ( $salesTable.hasClass('release-report') ) {
        $salesTable.tablesorter({
          headers: {
            2: { sorter: 'commaNumber' },
            3: { sorter: 'commaNumber' },
            4: { sorter: 'commaNumber' },
            5: { sorter: 'commaNumber' }
          },
          widgets: ['zebra']
        });
      } else if ( $salesTable.hasClass('song-report') ) {
        $salesTable.tablesorter({
          headers: {
            2: { sorter: 'commaNumber' },
            3: { sorter: 'commaNumber' },
            4: { sorter: 'commaNumber' }
          },
          widgets: ['zebra']
        });
      } else if ( $salesTable.hasClass('store-report') ) {
        $salesTable.tablesorter({
          headers: {
            1: { sorter: 'commaNumber' },
            2: { sorter: 'commaNumber' },
            3: { sorter: 'commaNumber' },
            4: { sorter: 'commaNumber' }
          },
          widgets: ['zebra']
        });
      } else if ( $salesTable.hasClass('country-report') ) {
        $salesTable.tablesorter({
          headers: {
            1: { sorter: 'commaNumber' },
            2: { sorter: 'commaNumber' },
            3: { sorter: 'commaNumber' },
            4: { sorter: 'commaNumber' }
          },
          widgets: ['zebra']
        });
      } else {
        $salesTable.tablesorter({
          widgets: ['zebra']
        });
      }
      $salesTable.tablesorterPager({
        container: $('#pager'),positionFixed: false
      });
    },

    // This also uses the dataTables plugin as well. This method is
    // for each individual song table on the individual release pages.
    //
    song_table_sorting: function (table, song_id) {
      if ($(table).find('tbody').find('tr').length <= 10) {
        $(table).siblings('.table_bottom').hide();
      }
      $(table).tablesorter({
        headers: {
          0: { sorter: 'monthYear' },
          3: { sorter: 'commaNumber' },
          4: { sorter: 'currencyUnit' },
          5: { sorter: 'currencyUnit' },
          7: { sorter: 'commaNumber' }
        },
        widgets: ['zebra']
      }).tablesorterPager({container: $('#pager-' + song_id),positionFixed: false});
    },

    // Custom sorting parsers
    //
    custom_date_parser: function(){
      var monthNames = {
        Jan: '01',
        Feb: '02',
        Mar: '03',
        Apr: '04',
        May: '05',
        Jun: '06',
        Jul: '07',
        Aug: '08',
        Sep: '09',
        Oct: '10',
        Nov: '11',
        Dec: '12'
      };
      $.tablesorter.addParser({
        id: 'monthYear',
        is: function(s) {
          return false;
        },
        format: function(s) {
          var date = s.match(/^(\w{3})[ ](\d{4})$/);
          if(!date){
            return s;
          }
          var m = monthNames[date[1]];
          var y = date[2];
          return '' + y + m;
        },
        type: 'numeric'
      });

      $.tablesorter.addParser({
        id: 'commaNumber',
        is: function(s) {
          return false;
        },
        format: function(s) {
          return $.tablesorter.formatFloat( s.replace(/([,\.\$\€])/g,'') );
        },
        type: 'numeric'
      });

      $.tablesorter.addParser({
        id: 'currencyUnit',
        is: function(s) {
          return false;
        },
        format: function(s) {
          return $.tablesorter.formatFloat( s.replace(/\([A-Z]{3}\) (-?)(\$)/g,'$1') );
        },
        type: 'numeric'
      });
    },

    sales_table_search: function(){
      $('#filter-box').keyup(function() {
        var filter = $(this).val();

        if (filter.length > 0) {
          if ($('.pagesize').val() !== '10000') {
            // 500 is not already the value
            $('.pagesize').append('<option value="10000">10000</option>');
            $('.pagesize').val('10000');
          } // else do nothing
        } else {
          $('.pagesize option[value=\'10000\']').remove();
          $('.pagesize').val('10');
        }
        $('.pagesize').change();
        $('#filter-box').quicksearch('#sales-table tbody tr', {
          'delay': 500,
          'noResults': '#noresults'
        });
      });
    },

    // Adds a collapsible link to each header anchor
    //
    // Must have the following layout:
    // <div class="collapsible"/>
    //   <someHeading /><a href="#optionalHash" class="toggle-button"/>
    //   <elementToBeHidden id="optionalHash"/>
    //
    // If there's a hash on the url corresponding to the optionalHash
    // it will be opened when loaded.
    // Add a class called dOpen to the anchor to have it be opened by default.
    //
    collapsible_panels: function (container) {
      var containerDiv = $(container);
      var $links = containerDiv.find('a.toggle-button'); // collect the song record links
      var locked = containerDiv.data('locked');
      var cssClassPlus = locked ? "toggle_plus locked" : "toggle_plus";
      var cssClassMinus = locked ? "toggle_minus locked" : "toggle_minus";

      $links.append(' <span class="' + cssClassPlus + '"></span>'); // add toggle indicator to all links
      $links.click(function () {
        $(this).parent().next().slideToggle();
        return false;
      }).toggle(function () {
        $(this).children('span.toggle_plus').remove();
        $(this).append(' <span class="' + cssClassMinus + '"></span>');
        if ($(this).parents('.collapsible').attr('id') !== 'song-sales-record'){
          var item = $(this).parent().next();
          self.update_cookie( item, 'show' );
        }
      }, function () {
        $(this).children('span.toggle_minus').remove();
        $(this).append(' <span class="' + cssClassPlus + '"></span>');
        if ($(this).parents('.collapsible').attr('id') !== 'song-sales-record'){
          var item = $(this).parent().next();
          self.update_cookie( item, 'hidden' );
        }
      }).parent().next().hide();
    },

    // Adds a loading function that checks the url for a hash to
    // a song. It will then initiate the ajax to load that song table.
    //
    hash_load: function (container) {
      var $links = $(container).find('a.toggle-button'); // collect the song record links

      // if there is a hash on the url, filter the links by the hash
      var toShow = window.location.hash ? window.location.hash : '';
      // if found we simulate a click on that link and the previous code will do the rest
      if (toShow !== ''){
        $links.filter(toShow).click();
      }
    },

    // Creates or updates a cookie to store the state of the filters.
    //
    update_cookie: function (item, state) {
      var cookie_type = item.get(0).tagName === 'DIV' ? 'filter' : 'hidden', // TODO: select all section can't be used!!!!!!
        cid = item.parent().attr('id'),
        tmp = cookie_type === 'filter' ? filter_state.getUnique() : hidden_state.getUnique();

      if (state === 'show') {
        tmp.push(cid);
      } else {
        tmp.splice( tmp.indexOf(cid), 1 );
      }

      cookie = cookie_type === 'filter' ? filter_state = tmp.getUnique() : hidden_state = tmp.getUnique();
      tmp_str = cookie_type === 'filter' ? 'filter_state' : 'hidden_state';
      $.cookie(tmp_str, cookie.join('|'), { path: '/', domain: 'tunecore.com' });
    },

    // Load any existing cookies and handle default opened panels if none found.
    //
    cookie_load: function () {
      $.map( filter_state, function (n){
        $( '#' + n ).find('.toggle-button').click();
      });
      $.map( hidden_state, function (n){
        $( '#' + n ).find('.show_more').click();
      });

      // if there is are any of the class dOpen, simulate a click on it
      // unless we find a cookie
      if ( filter_state.length === 0 ){
        $('a.dOpen').each(function() {$(this).click();});
      }
    },

    // Click show song sales link
    //
    // 1. Make sure table doesn't already exist
    // 2. Show spinner in placeholder
    // 3. Ajax GET song sales table
    // 4. Replace placeholder div with table
    // 5. Add song table sorting
    //
    show_song_sales: function () {
      if (!$(this).parent().next('div').children('table').length){ // 1.
        var $this = $(this);
        var song_id = $(this).attr('id');
        var href = $(this).attr('href');
        var $placeholder = $(this).parent().next('div');
        $placeholder.prepend('<img src="/images/icons/spinner-24x24.gif" alt="" class="add-spinner" />'); // 2.

        $.get( // 3.
          href,
          function (response) {
            $placeholder.prepend(response); // 4.
            $placeholder.find('td.sales-total-earned').each(function (index) {
              self.currency_decimal_separator($(this), 2);
            });
            $placeholder.find('td.currency span.reports-amount').each(function () {
              self.currency_decimal_separator($(this), 4);
            });
            self.song_table_sorting($placeholder.find('table'), song_id); // 5.
            $('.add-spinner').remove();
          },
          'html'
        );

        return false;
      }
    },

    // Show only end dates that comes after start date
    //
    // 1. Get all the date options in arrays
    // 2. If we use the start date selector
    // 3. Disable dates which come before it in the end date selector
    // 4. If an end date which is older is already selected move the date up
    //    to the current start date
    //
    negative_dates_hider: function () {
      var $start_dates = $('#date_start_date').find('option'), // 1.
        $end_dates = $('#date_end_date').find('option'); // 1.

      $date_start_selector.change(function () { // 2.
        var selected_date = $(this).find('option:selected').val();

        $end_dates.each(function () {
          if ( $(this).val() < selected_date ) { // 3.
            $(this).prop('disabled', true);
          } else {
            $(this).prop('disabled', false);
          }
        });

        if ( $date_end_selector.find('option:selected').val() < selected_date ) { // 4.
          $date_end_selector.val(selected_date);
        }
      });
    },

    // Separates decimals on currency from the thousandths place and on from the rest of the amount
    //
    // 1. First separate the amount using the cent separator
    // 2. Then we will have three different amounts:
    //  2a. The dollar amount
    //  2b. The cents amount
    //  2c. The 0.9 cents and so on (decimal cents) amount
    // 3. Then we replace the html so 2c. will be enclosed in a span
    //
    currency_decimal_separator: function (currency, precision) {
      if (currency.length) {
        var delimiter = gon.currency_delimiter;
        var total = $(currency).html().split(delimiter); // 1.
        var dollar_total = total[0]; // 2a.
        var decimals_shown = total[1].substring(0,precision); // 2b.
        var rest_of_decimals = total[1].substring(2); // 2c.

        $(currency).html(dollar_total + delimiter + decimals_shown + '<span class="decimals">' + rest_of_decimals + '</span>'); // 3.
      }
    },

    // Sets up showing and hiding the decimal cents amount
    //
    // 1. Triggers a separation of the decimal cents
    // 2. Appends the show decimal trigger link on element
    // 3. Attach click handler to the trigger
    //
    currency_decimal_hider: function () {
      self.currency_decimal_separator($sales_total, 2); // 1.
      $('td.sales-total-earned').each(function () {
        self.currency_decimal_separator($(this), 2); // 1.
      });
      $('td.currency span.reports-amount').each(function () {
        self.currency_decimal_separator($(this), 4); // 1.
      });
      if (!$('#release-sales-records').length) {
        $('th.sales-total-earned').append(' <br /><a href="#" class="toggle_decimals">Show decimals</a>');
        self.decimal_click_handler('th', 'tbody');
      }
      $('#sales-totals h5').append(' <a href="#" class="toggle_decimals">Show decimals</a>'); // 2.
      self.decimal_click_handler('h5', '#sales-totals'); // 3.
    },

    // Click handler for decimal cents
    //
    // 1. When the trigger is toggled:
    //  1a. Show the decimals by appending 'show' class to container element
    //  1b. Replace the trigger text with contrary message
    // 2. When the trigger is toggled again:
    //  2a. Hide the decimals by appending 'show' class to container element
    //  2b. Replace the trigger text with original message
    //
    decimal_click_handler: function (a, b) {
      $(a + ' a.toggle_decimals').toggle(function (e) {
        e.preventDefault();
        e.stopPropagation();

        $(b).addClass('show'); // 1a.
        $(a + ' a.toggle_decimals').html('Hide decimals'); // 1b.
      }, function (e) {
        e.preventDefault();
        e.stopPropagation();

        $(b).removeClass('show'); // 2a.
        $(a + ' a.toggle_decimals').html('Show decimals'); // 2b.
      });
    },

    // Empty filter hider
    //
    // 1. If any type has one or less items, remove that section
    // 2. If there are no release type shown as a result, remove that section
    // 3. If there are no filters shown as a result of the previous two rules, hide all the advanced options
    //
    filter_type_hider: function () {
      var $types = $('#advanced-options').find('.container'),
        $releases_group = $('#releases-filter'),
        $releases = $('#releases-filter').find('.container'),
        $advanced_options = $('#advanced-options');

      $types.each(function () {
        if ($(this).find('li').length <= 1) { // 1.
          $(this).parent('.container').remove();
        }
      });

      $releases.each(function () {
        if ($(this).find('li').length <= 1) { // 2.
          $(this).parent('.container').remove();
        }
      });

      if ($releases_group.find('.container').length < 1) {
        $releases_group.parents('#releases').remove();
      }

      if ($advanced_options.find('.container').length === 0) { // 3.
        $advanced_options.prev('h2').remove();
        $advanced_options.remove();
      }
    },

    // Show/Hide filters
    //
    // 1. When trigger is toggled:
    //  1a. Slide down hidden filters
    // 2. When trigger is again toggled:
    //  2a. Slide up visible filters
    //
    filter_show_hide: function () {
      var $container = $('#filter-container'),
        $filters = $('#sales-filters');

      $container.find('a.filter').toggle(function (e) {
        $filters.slideDown(500); // 1a.

        e.preventDefault();
      }, function (e) {
        $filters.slideUp(500); // 2a.

        e.preventDefault();
      });
    },

    // Expand and collapse the sales release table
    //
    // Expand button:
    // 1. After sliding up the filters:
    //  1a. We fade out the expand table button
    //  1b. Animate the expansion of the table
    // 2. After the table is done expanding
    //  NO LONGER USED -> 2a. We fade in the show filter button
    //  2b. We replace the expand table button by adding a collapse table button
    //  2c. Show the decimal cents by appending 'show' class to container element
    //
    // Collapse button:
    // NO LONGER USED -> 3. We fade out the show filter button
    // 4. We fade out the collapse table button
    // 5. Hide the decimal cents by appending 'show' class to container element
    // 6. Animate the collapse of the table
    // 7. After the table is collapsed
    //  7a. We replace the collapse table button by adding a expand table button
    //  7b. We slide down the filters
    //
    release_table_expand_collapse: function () {
      var $container = $('#sales-container'),
        $salesTable = $('#release-sales-records'),
        $sales_filter = $('#sales-filters'),
        $songs_tables = $('#song-sales-record');
      //$filter_button = $("#filter_btn");

      $(document).on('click', '#expand', function (e) { // We use a live click handler since we will remove the anchor later
        $sales_filter.slideUp(500, function () { // Finished hiding sales-filters
          $('#expand').fadeOut(250); // 1a.
          $container.animate({ // 1b.
            width: '940px'
          }, 500, function () { // Finished expanding sales-table
            //$filter_button.fadeIn(250); // 2a.
            $salesTable.prepend('<a id="collapse" class="filter start-arrow" href="#">Show filters</a>').removeClass().addClass('expanded'); // 2b.
            $songs_tables.addClass('expanded'); // 2b.
            // $.rule('td .decimals{ display: inline; }').appendTo('link:eq(0)');
            // $('td .decimals').css("display", "inline");
            $container.addClass('show'); // 2c.
          });
        });

        e.preventDefault();
      });

      $(document).on('click', '#collapse', function (e) {
        //$filter_button.fadeOut(250); // 3.
        $('#collapse').fadeOut(250); // 4.
        $container.removeClass('show'); // 5.
        $container.animate({ // 6.
          width: '700px'
        }, 500, function () { // Finished collapsing sales-table
          $salesTable.prepend('<a id="expand" class="filter end-arrow" href="#">Show decimals</a>').removeClass(); // 7a.
          $songs_tables.removeClass('expanded');
          $sales_filter.slideDown(500); // 7b.
        });

        e.preventDefault();
      });
    }
  };
  return self.init();
};

jQuery(document).ready(function () {
  TC.sales = new Sales();
});

// Return a unique array.
Array.prototype.getUnique = function() {
  var o = new Object();
  var i, e;
  for (i = 0; e = this[i]; i++) {o[e] = 1;}
  var a = new Array();
  for (e in o) {a.push (e);}
  return a;
};

// Fix indexOf in IE
if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function(obj, start) {
    for (var i = (start || 0), j = this.length; i < j; i++) {
      if (this[i] === obj) { return i; }
    }
    return -1;
  };
}
