/* Song_Single.js
  = Description
  The following code allows the save, edit, delete, and upload buttons on
  the Album Page to make ajax requests for updating or adding song content.
  
  = Change log
  [2010-10-21 -- GB] Initial Creation

  = Dependencies
  Depends on the jquery.ui for highlighting appendages.
  
  = Usage
  Not a function to be used outside this file.
  
*/
var TC = TC || {};

var SongSingle = function() {

  // Set up $ to be used locally for jQuery then
  // cache selectors so you don't have to traverse
  // the DOM each time.
  var $ = jQuery;
  var upload_button = ".upload-button";
     
  //  Make a call to the webserver to return the album checklist and update
  //  it on the page.
  
  var self = {
    
    init: function(){
      $(document).on("click", upload_button, self.upload_song);
      return self;
    },

    upload_song: function(){
        reorder_toggle.hide();
        var this_button = $(this);
        var href = this_button.attr("href");

        $(".upload-button").hide();// a.
        this_button.show(); // Only show current button so we can have a spinner
        this_button.html('<img src="/images/icons/spinner-16x16-grn.gif" alt="" style="margin-top:4px" />'); 

        $.get( // 1.
          href,
          function(response){
            var s = $(response).children().unwrap();
            selector.html(s); // 2.
          },
          "html"
        );
        // updateChecklist();
        return false;
     },
          
     updateChecklist: function(songid){
       updateChecklist(songid);
     },
    
    updateChecklist: function(){
        updateChecklist();
    }
    
  }
  return self.init();
};

jQuery(document).ready(function(){
    
  TC.song_single = new SongSingle();
  
});

//  After response of uploaded song from flash uploader
//  render the show song partial again
var show_song = function(song_id){
  var $ = jQuery;
  var song_path = $("#single-upload-path").html();

  $.get(
    song_path,
    function(response){
      s = $(response).children().unwrap();
      $('#' + song_id).html(s).effect("highlight", {}, 200);
    },
    "html"
  );

};
