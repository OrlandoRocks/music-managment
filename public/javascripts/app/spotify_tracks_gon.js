jQuery(function ($) {
  var spotifyTracks = function(data){
    gon.retry_count --;

    if (gon.retry_count == 0){
      gon.unwatch("album_spotify_tracks", spotifyTracks)
      return false;
    }

    if ( data == null ) { return }

    if ( data.length !== 0 ){
      gon.unwatch("album_spotify_tracks", spotifyTracks);
      gon.magnific = true;
      enableTrackButton();
      completeTracksModal(data);
    }

  };

  var enableTrackButton = function(){
    $(".get-spotify-tracks-button").text("Get My Spotify Track URIs");
  };

  var completeTracksModal = function(data){
    if ( $(".spotify-track").length !== 0){
      gon.unwatch("album_spotify_tracks");
      return false;
    }

    $(data).each(function(i, track){
      var divOpen = "<div class='spotify-track'>";
      var label = "<span class='track-title'>" + track.name + "</span>";
      var input = "<input readonly class='hidden-uri' id='" + i + "' value='spotify:track:" + track.identifier + "'></input>";
      var button = "<button class='fa fa-chain copy-track-uri' data-clipboard-target='#hidden-" + i + "''></button>";
      var divClose = "</div>";
      $("#spotify-tracks-display").append(divOpen + label + input + button + divClose);
    });

    gon.unwatch("album_spotify_tracks");
    return false;
  };


  $(document).ready(function(){
    gon.retry_count = 2
    if (gon.album_spotify) {
      gon.watch("album_spotify_tracks", {interval: 3000}, spotifyTracks);
    }
  });
});
