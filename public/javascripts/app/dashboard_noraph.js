//	Dashboard.js
//	Draws a lot of pretty canvas things on the dashboard. This
//	relies on a fork of Google's excanvas to "port" canvas to IE
//	and support the fillText() method.


var Dashboard = function(){
  var $ = jQuery;

	var self = {
		init: function(){
		  self.draw_plaque_0();
		  self.draw_plaque_1();
		  self.draw_plaque_2();
		  self.draw_plaque_3();
		},
		draw_plaque_0: function(){
      var elem = document.getElementById('plaque-0');
      if (elem) { // test to see if canvas is on the page
        // Get the canvas 2d context.
        var context = elem.getContext('2d');

        //place bg image
        var img = new Image();
        img.src = '/version/shared/images/plaques/glossy_bg.png';
        $(img).load(function() {
          context.drawImage(this,0,0);
        });

        // write text
        context.font = 'bold 28px Hevetica, sans-serif';
        context.fillStyle = "#66CCFF";
        context.textAlign = 'center';
        if (context.fillText) {
          context.fillText('Guaranteed', 118, 46);
          context.fillText('Online Radio', 118, 80);
          context.fillText('Airplay', 118, 115);
        }
      };
    },
		
		draw_plaque_1: function(){
      var elem = document.getElementById('plaque-1');
      if (elem) { // test to see if canvas is on the page
        // Get the canvas 2d context.
        var context = elem.getContext('2d');

        //place bg image
        var img = new Image();
        img.src = '/version/shared/images/plaques/glossy_bg.png';
        $(img).load(function() {
          context.drawImage(this,0,0);
        });

        // write text
        context.font = 'bold 28px Hevetica, sans-serif';
        context.fillStyle = "#76C646";
        context.textAlign = 'center';
        if (context.fillText) {
          context.fillText('Your Annual', 118, 46);
          context.fillText('Renewal', 118, 76);
          context.fillText('is Due', 118, 106);
        }
      };
    },
    
    draw_plaque_2: function(){
      var elem = document.getElementById('plaque-2');
      if (elem) {
      // Get the canvas 2d context.
      var context = elem.getContext('2d');
      

      //place bg image
      var img = new Image();
      img.src = '/version/shared/images/plaques/glossy_bg.png';
      $(img).load(function() {
        context.drawImage(this,0,0);
      });
      
      // write text
      context.font = 'bold 30px Hevetica, sans-serif';
      context.fillStyle = "#66CCFF";
      context.textAlign = 'center';
      if (context.fillText) {
        context.fillText('iPhone', 118, 45);
        context.fillText('Ringtone', 118, 80);
        context.fillText('Distribution', 118, 115);
        }
      }
    },
    
    draw_plaque_3: function(){
      var elem = document.getElementById('plaque-3');
      if (elem) {
      // Get the canvas 2d context.
      var context = elem.getContext('2d');
      

      //place bg image
      var img = new Image();
      img.src = '/version/shared/images/plaques/glossy_bg.png';
      $(img).load(function() {
        context.drawImage(this,0,0);
      });
      
      // write text
      context.font = 'bold 30px Hevetica, sans-serif';
      context.fillStyle = "#66CCFF";
      context.textAlign = 'center';
      if (context.fillText) {
        context.fillText('Music Industry', 118, 45);
        context.fillText('Survival', 118, 80);
        context.fillText('Manuals', 118, 115);
        }
      }
    }
  };
	
	return self.init();
};


var init_dashboard = function(){
  var d = new Dashboard();
  var a = new Slider('albums');
  var r = new Slider('ringtones');
  var s = new Slider('singles');
};

jQuery(document).ready(init_dashboard);


