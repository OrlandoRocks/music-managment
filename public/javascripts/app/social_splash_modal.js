$(function() {

  var showSocialModal = gon.show_social_modal;
  var socialUrl       = gon.social_url;

  if (showSocialModal) {

    $.magnificPopup.open({
      items: {
        src: $('<div class="social-splash-modal"><img class="social-purchased" src="/images/invoices/social_purchased_pop_up.png" /></div>')
      },
      closeOnContentClick: true,
      closeOnBgClick: true,
      showCloseBtn: false,
      enableEscapeKey: false
    });


    $('.social-purchased').on('click', function() {
      window.open(socialUrl);
      $('body').off('click');
    });

  }

});
