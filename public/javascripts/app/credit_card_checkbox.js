/*
* Check out with new Credit Card Page
* [Apr 27 2010 DCD] Show/hide function for mark as prefered checkbox on CC checkout page
*
*/
var MarkAsPreferred = function(){
  var $ = jQuery;
	var stored_field = $("input[name='customer_vault']");
  var preferred = $("#mark_as_preferred");
  
  var self = {
    init: function(){
      self.show_preferred();
	    stored_field.click(self.show_preferred);
	   },
    show_preferred: function(){
      stored = $("input[name='customer_vault']:checked").val();
			stored == "add_customer" ? preferred.slideDown(300) : preferred.slideUp(300);
    }
  };
  return self.init();
};

jQuery(document).ready(function(){
  var prefs = new MarkAsPreferred();
});