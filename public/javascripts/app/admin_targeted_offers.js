/*
    CH - 2010-06-16
*/
var TargetedOfferForm = function(){
    var date_class = ".date-box";
    var date_constraint_select = "#targeted_offer_date_constraint";
    var offer_type_select = "#targeted_offer_offer_type";
    var form_input = ".context-wrapper input";
    var $ = jQuery;

    var self = {
        init: function(){
            $(date_constraint_select).change(this.show_or_hide_options);
            $(offer_type_select).change(this.show_or_hide_options);
            $(form_input).focus(this.show_helper_text);
            $(form_input).blur(this.hide_helper_text);
            self.init_date_pickers();
        },

        show_or_hide_options: function(){
            if ($(date_constraint_select).val()=="join plus") {
              $("#targeted_offer_join_plus_duration_list_item").removeClass('hidden');
            } else {
              $("#targeted_offer_join_plus_duration_list_item").addClass('hidden');
            }

            if ($(offer_type_select).val()=='new') {
              $("#targeted_offer_join_token_list_item").removeClass('hidden');
              $("#targeted_offer_population_cap_list_item").removeClass('hidden');
            } else if ($(offer_type_select).val()=='existing'){
              $("#targeted_offer_join_token_list_item").addClass('hidden');
              $("#targeted_offer_population_cap_list_item").addClass('hidden');
            }
         },

    show_helper_text: function(){
      var help = $(this).parent().find('.contextual-help');
        if (help.find('p').text().length){
          help.removeClass('hidden');
       };
     },

     hide_helper_text: function(){
       $(".contextual-help:not(.forced)").addClass("hidden");
     },

        init_date_pickers: function(){
        new DatePicker(date_class,{});
        }
    };

    return self.init();
};


var init_targeted_offer_form = function(){
  new TargetedOfferForm();
};
jQuery(document).ready(init_targeted_offer_form);
