jQuery(function($){
  var $ = jQuery;
  $('#loadingDialog').dialog({
    autoOpen: false,
    resizable: false,
    modal: true,
    draggable: false,
    width: 600,
    maxWidth: 600,
    height: 300,
    maxHeight: 300
  });
  $('.loadingDialog').click(function(){
    $('#loadingDialog').dialog('open');
  });
  $('#loadingDialog .cancelLoad').click(function(){
    window.stop();
    $('#loadingDialog').dialog('close');
  });
});
