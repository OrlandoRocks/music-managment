jQuery(document).ready(function() {
  setupConfirmModal();
  setupPaginateModal();
  setupProcessingDialog();
  enableButton();
});

function setupProcessingDialog() {
  jQuery('#processingDialog').dialog({
    closeOnEscape: false,
    open: function(event, ui) {
      $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
    },
    autoOpen: false,
    resizable: false,
    modal: true,
    draggable: false,
    width: 600,
    maxWidth: 600,
    height: 300,
    maxHeight: 300
  });
  jQuery('.continue').click(function(){
    jQuery.magnificPopup.close();
    jQuery('#processingDialog').dialog('open');
  });
}

function enableButton(){
  jQuery(".takedown-release-btn").removeAttr("disabled");
}

function setupConfirmModal(){
  jQuery(":submit").magnificPopup({
    type: "inline",
    items: {src: "#takedown-confirm-modal"},
    callbacks: {
      beforeOpen: function () {
        setCounts();
      }
    },
    midClick: true
  });
  jQuery( "#modal-submit" ).click(function(e){
    jQuery("#new_bulk_release_subscription_form").submit();
    e.preventDefault();
  });
  $(".go_back").on( "click", function() {
    jQuery.magnificPopup.close();
  });
}

function setupPaginateModal(){
  jQuery(".pagination a").each(function() {
    jQuery(this).magnificPopup({
      type: "inline",
      items: {src: "#paginate-confirm-modal"},
      callbacks: {
        beforeOpen: function () {
          var newLink = this.st.el.attr("href");
          jQuery("#page-submit").attr("href", newLink);
        }
      },
      midClick: true
    });
  });

  $(".go_back").on( "click", function() {
    jQuery.magnificPopup.close();
  });
}

function getCount(action){
  var count = 0;
  jQuery(':radio[name^="bulk_release_subscription"]:checked').each(function(){
    if (this.value === action) { count ++; }
  });
  return count;
}

function setCounts() {
  var takedownCount = getCount("takedown");
  var cancelCount = getCount("cancel");
  if (takedownCount === 0 && cancelCount === 0) {
    jQuery("#modal-content").hide();
    jQuery("#none-selected").show();
  } else {
    jQuery("#takedown-count").html(takedownCount);
    jQuery("#cancel-count").html(cancelCount);
    jQuery("#none-selected").hide();
    jQuery("#modal-content").show();
  }
}

function clearRadio(object) {
  var container = object.closest(".radio-container");
  jQuery(container).children().children().each(function() {
    jQuery(this).removeAttr("checked");
  });
}
