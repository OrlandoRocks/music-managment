jQuery(function(){
  var $ = jQuery,
  $mobile_menu = $("#mobile-navigation");
  
  $mobile_list = $mobile_menu.find('ul')
  
  var $trigger = $("#open-mobile-menu");
  $trigger.addClass("open_btn");
  $trigger.toggle(function () {
    
    $mobile_list.animate({ top: 00 }, 500, function () {
	  $trigger.addClass("close_btn");
      $trigger.removeClass("open_btn");
    });
    return false;
  }, function () {
    $mobile_list.animate({ top: -1190 }, 500, function () {
	  $trigger.addClass("open_btn");
	  $trigger.removeClass("close_btn");
    });
    
    return false;
  });
});