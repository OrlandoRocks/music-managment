/* Modal_Popup.js
  = Description
  This method can be used to add a warning modal box when distributing albums,
  singles and ringtones.

  = Change Log
  [2011-05-16 -- RT] Initial Creation

  = Dependencies
  Depends on SimpleModal.

  = Usage
  Current set up to be used on distribution buttons on albums, singles, and ringtones.

  = Example
  N/A

*/
var TC = TC || {};
var modalWarning = function () {
  var $ = jQuery,
      $warningMsg = $("#warning"),
      $convertMsg = $("#warning_conversion"),
      $failedTracks = $("#failed_tracks"),
      properText = $("#failed_tracks").clone().html(),
      albumId = window.location.pathname.match(/(?:albums|singles|ringtones)\/(\d+)/)[1],
      request = "/albums/" + albumId + "/album_meta_data.json";

  var self = {
    init: function () {
      self.click_handler();
    },

    // TODO - replace the remaining, old SimpleModals with the newer MagnificPopups
    // there are both in this file which is confusing, but SimpleModals are noted in comments


    // Adds a click handler to activate the jQuery UI Dialog
    //
    click_handler: function () {
      $(document).on("click", "#distributeMyAlbum", function (e) {
        e.preventDefault();
        $.getJSON(request)
        .done(function(TC){
          // If there are any failed tracks in the album
          if ( TC.failedTracks.length != 0 ) {
            $.each( TC.failedTracks, function( index, value ) {
              $("p", $failedTracks).append( value );
              if ( index != (TC.failedTracks.length - 1)) {
                $("p", $failedTracks).append(", ");
              };
            });
            $("p", $failedTracks).append(".<br>If you still can't distribute after re-uploading your tracks, contact customer care.")
            $.magnificPopup.open({
              items: {
                src: $failedTracks,
                type: 'inline'
              },
              callbacks: {
                close: function () {
                  $failedTracks.html(properText);
                }
              }
            });
          // If there's only one track in the album
          } else if ( TC.albumSize == 1 && TC.albumType == "Album" ) {
            // And if the single track is 10 minutes or longer show regular warning
            if ( TC.trackLength >= 600 || TC.trackLength == 0 ) {
              // SimpleModal
              $warningMsg.modal({
                minWidth: 500,
                maxWidth: 500
              });
            } else { // Else show the converting to single warning
              // SimpleModal
              $convertMsg.modal({
                minWidth: 500,
                maxWidth: 500
              });
            }
          // If there are no errors in the album, standard warning about final edits
          } else {
            // SimpleModal
            $warningMsg.modal({
              minWidth: 500,
              maxWidth: 500
            });
          }
          $('#simplemodal-container').css('height', 'auto');

        }).fail(function(data) {
          $.magnificPopup.open({
            items: {
              src: $("#failed_call"),
              type: 'inline'
            }
          })
        });
      });
      $(document).on("click", ".cancel", function (e) {
        e.preventDefault();
        $.modal.close();
      });
      $(document).on("click", ".distribute", function (e) {
        e.preventDefault();
        $("#doDistribute").submit();
      });
    }
  };
  return self.init();
};

jQuery(document).ready(function($){
  TC.modalWarning = new modalWarning();
});
