
/*
 *  Header:
 *
 *  A simple object to encapsulate header javascript logic
 *
 */
var Header = function(){
  const NO_ACCORDION_CLASS = 'no-accordion'

  //
  //  Page style definitions
  //
  var $ = jQuery,
    main_menu = '.main_menu',
    mobile_nav = '.mobile-nav',
    fx_balance_icon = 'i.foreign-exchange-balance-display',
    $fx_balance_icon_el = $(fx_balance_icon),
    $head_nav = $('#header').find('nav'),
    $summary_menu = $('#summary').find('.menu').find('a[href="#"]'),
    $head_new = $('.header'),
    timer;

  var self = {

    //
    //  Setup Form Elements
    //
    init: function() {
      $(document).on('click', main_menu, self.toggle_menu);
      $(document).on('click', fx_balance_icon, self.toggle_fx_balance_tooltip);
      if ($(main_menu).find(mobile_nav)) {
        self.toggle_menu_accordion();
      }

      $head_new.find('a[href="#"]').click(function(e) {
        e.preventDefault();
      });
      var $nav_elements = $head_nav.children('a[href="#"]');
      $nav_elements.click(function(e) {
        e.preventDefault();
      });

      $nav_elements.focus(function() {
        // clear animations if submenu is opened
        clearTimeout(timer);
        var lists = $nav_elements.siblings('ul');

        // if submenu is already opened, just hide current all submenus and show new submenu
        // else animate submenu opening
        if ( lists.filter(function() { return $(this).css('height') != '0px'; }).length == 1 ) {
          lists.css({'display': 'none', 'margin-bottom': '0', 'height': '0'}).stop();
          $(this).next('ul').css({'display': 'block', 'margin-bottom': '20px', 'height': '79px'});
        } else {
          lists.css({'display': 'none', 'margin-bottom': '0', 'height': '0'}).stop();
          $(this).next('ul').css({'display': 'block', 'margin-bottom': '20px'}).animate({
            height: '79px'
          }, 500);
        }
      });

      $nav_elements.blur(function() {
        var $submenu = $(this);
        timer = setTimeout(function() {
          $submenu.next('ul').animate({
            height: '0'
          }, 500, function() {
            $(this).css({'display': 'none', 'margin-bottom': '0'});
          });
        }, 500);
      });

      $nav_elements.siblings('ul').find('a').click(function() {
        clearTimeout(timer);
        $(this).parent('ul').stop().css({'display': 'block', 'margin-bottom': '20px', 'height': '79px'});
      });

      $summary_menu.click(function(e) {
        e.preventDefault();
      });
    },

    toggle_menu: function(e) {
      e.preventDefault();
      var $$ = $(this),
        $menu = $$.closest('nav').next('.mobile-nav'),
        $caret = $$.find('i:last-child');

      if ( $menu.length == 0 ) {
        $menu = $$.find('.mobile-nav');
      }

      if ( $$.hasClass('opened') ) {
        $$.removeClass('opened');
        $caret.removeClass().addClass('fa fa-caret-down');
        $menu.removeClass('opened');
      } else {
        $$.addClass('opened');
        $caret.removeClass().addClass('fa fa-caret-up');
        $menu.addClass('opened');
      }
    },

    toggle_menu_accordion : function() {
      var $$ = $(mobile_nav),
        accordion = $$.find('> ul > li > a');
      accordion.on('click', function(e) {
        var $$ = $(this);
        e.stopPropagation();

        if (!$$.hasClass(NO_ACCORDION_CLASS)) {
          e.preventDefault();
          $$.toggleClass('opened').next('ul').toggle();
        }

        $$.next('ul').find('a').on('click', function(e) {e.stopPropagation();});
      });
    },

    toggle_fx_balance_tooltip: function(e) {
      var tooltip = $fx_balance_icon_el.data('tooltip');

      $fx_balance_icon_el.qtip({
        overwrite: false,
        content: tooltip,
        show: { event: e.type, ready: true },
        hide: { delay: 200, fixed: true },
        position: { my: 'top right', at: 'bottom left', target: $fx_balance_icon_el }
      }, e);
    }
  };

  self.init();
  return self;
};

// Initialize on document ready
jQuery(document).ready( function() {
  new Header();
});
