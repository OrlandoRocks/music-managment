/*

  Placeholder emulation for form fields
  
  Usage: see Homepage.js for an example

*/

var PasswordFields = function(options){

  // options
  var field = options.field,
  $ = jQuery;

  var self = {

    init: function(options){
      // self.setup_focus();
      // self.setup_blur();
      $(field).prev().css("opacity", 1);
      self.setup_keypress();
      self.hide_label();
      self.setup_monitor();
    },

    setup_focus: function(){
      $(field).focus(this.focus);
    },

    setup_blur: function(){
      $(field).blur(this.blur);
    },

    setup_keypress: function(){
      $(field).keyup(this.keypress);
    },

    setup_monitor: function(){
      setTimeout(function(){
        if ($(field).val().length){
          $(field).prev().stop().css("opacity", 0).addClass('hidden');
        }
        return true;
      }, 100);
    },

    hide_label: function() {
      // if we ever upgrade to 1.4.x, change to $.browser.webkit
      ($.browser.safari) ? 
      setTimeout(self.hide_label_on_load, 75) : self.hide_label_on_load();
    },

    hide_label_on_load: function(){
      if (!self.is_field_empty()){
        $(field).prev().addClass('hidden');
      }
    },

    focus: function(){
      if (self.is_field_empty()){
        $(field).prev().addClass('hidden');
      }
    },

    blur: function(){
      if (self.is_field_empty()){
        $(field).prev().removeClass('hidden');
      }
    },

    keypress: function(){
      if (!self.is_field_empty()){
        $(field).prev().stop().css("opacity", 0).addClass('hidden');
      } else {
        $(field).prev().removeClass('hidden').animate({opacity: 1}, 250);
      }
    },

    is_field_empty: function(){
      return $(field).val() === '';
    }
  };

  return self.init();
};

