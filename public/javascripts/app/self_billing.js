var $ = jQuery;

$(function () {
  $(".self-billing-component input[type='radio']").on("change", function () {
    $(".self-billing-component .selfBillingFormError").hide();
  });
});

function onAcceptRadioChecked(event) {
  const checkedRadioInput = $(
    ".self-billing-component input[type='radio']:checked"
  )[0];

  if (checkedRadioInput.value === "accepted") {
    $("#modal-bind")
      .magnificPopup({
        items: {
          src: "#self-billing-positive-modal",
        },
        closeOnContentClick: false,
        closeOnBgClick: false,
        showCloseBtn: true,
        enableEscapeKey: false,
        midClick: true,
      })
      .magnificPopup("open");
  }
}

function onDeclineRadioChecked(event) {
  const checkedRadioInput = $(
    ".self-billing-component input[type='radio']:checked"
  )[0];

  if (checkedRadioInput.value === "declined") {
    $("#modal-bind")
      .magnificPopup({
        items: {
          src: "#self-billing-negative-modal",
        },
        closeOnContentClick: false,
        closeOnBgClick: false,
        showCloseBtn: false,
        enableEscapeKey: false,
        midClick: true,
      })
      .magnificPopup("open");
  }
}

function saveSelfBillingInfo(event) {
  if (event) {
    event.preventDefault();
  }

  const checkedRadioInput = $(
    ".self-billing-component input[type='radio']:checked"
  )[0];

  if (!checkedRadioInput) {
    const formErrorElement = $(".self-billing-component .selfBillingFormError");

    formErrorElement.css({
      display: "inline-block",
    });

    if (formErrorElement.hasClass("hide")) {
      formErrorElement.removeClass("hide");
    }
  } else {
    submitSelfBillingForm();
  }
}

function closeModal(event) {
  if (event) {
    event.preventDefault();
  }
  $.magnificPopup.close();
}

function submitSelfBillingForm(event) {
  if (event) {
    event.preventDefault();
  }

  $(".self-billing-component form").trigger("submit");
  closeModal();
}

function declineModalConfirmed(event) {
  if (event) {
    event.preventDefault();
  }

  $("#modal-bind")
    .magnificPopup({
      items: {
        src: "#self-billing-negative-confirm-modal",
      },
      closeOnContentClick: false,
      closeOnBgClick: false,
      showCloseBtn: false,
      enableEscapeKey: false,
      midClick: true,
    })
    .magnificPopup("open");
}

function submitWithdrawForm(event) {
  $(".payout-transfer-form form").trigger("submit");
}
