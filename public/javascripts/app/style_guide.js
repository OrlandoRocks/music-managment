//NOTE: This script got ported into a jQuery plugin. Use tc.styleguide.js instead */

// Description
// The Style Guide object checks inputs against a list of iTunes Style guide
// recommendations and provides a non-blocking warning for the user, informing
// them of the recommended usage.
//
// Usage
// A great place to put some notes on how to use the model. Include, gotchas
// and whatnot as well.
//
//   var foo = new StyleGuide();
//
// Change Log
// [2009-12-18 -- MT]
// Adding intial rDoc documentation
//
// 
var StyleGuide = function(){
  // reused strings & cached references
  var $ = jQuery;
  
  //we're going to hit the following a lot, thus caching it now to prevent
  //needless DOM queries
  var $fields = $('#single_name, #single_creatives__name');
  var $artist = $('#single_creatives__name');
  var $title = $('#single_name');
  
  //the suggestion actually needs to be requeried
  var suggestion = '.style-suggestion';

  // essentially our style guide grammar
  // the keys in the rules and message objects need to match
  // otherwise, this thing will blow up.
  
  var rules = {
    'artist': {
      '/': /\//,
      '&': /\s\&\s/,
      '+': /\s\+\s/,
      '-': /\-/,
      'and': /\sand\s/i,
      'with': /\sw((ith)|\/)\s/i, // with, w/ 
      'featuring': /f(ea)?t((\s)|(\.)?)\s?/i // featuring, features, feat., ft. , ft
    },
    'title': {
      'volume': /\s((volume)|(vol\.?))\s(\d|[otfsen])/i, // volume , vol., vol 1, vol 2, (etc), vol one, vol two (etc), volume one (etc)
      'part': /\sp(ar)?t\.?\s(\d|[otfsen])/i // part , pt , pt.}
    }
  };
                
  var messages = { 
    'artist': {
      '/': 'Are you trying to add another artist to this album? If so, please click "Add Another Main Artist" below.',
      '&': 'Are you trying to add another artist to this album? If so, please click "Add Another Main Artist" below.',
      '+': 'Are you trying to add another artist to this album? If so, please click "Add Another Main Artist" below.',
      '-': 'Are you trying to add another artist to this album? If so, please click "Add Another Main Artist" below.',
      'and': 'Are you trying to add another artist to this album? If so, please click "Add Another Main Artist" below.',
      'with': 'Are you trying to add another artist to this album? If so, please click "Add Another Main Artist" below.',
      'featuring': 'Are you trying to add a featured artist to this song? If so, please enter them in the "Featured Artist" field below.'
    },
    'title': {
      'volume': 'Is this a multiple volume/disc release? If so, iTunes has specific formatting rules that must be followed. For example: "Album Title, Vol. 1"',
      'part': 'Is this a multiple volume/disc release? If so, iTunes has specific formatting rules that must be followed. For example: "Album Title, Pt. 1"'
    }
  }
  ;
  var self = {
    init: function(){
      self.make_containers();
      self.assign_rules();
      self.bind_events();
    },
    
    make_containers: function(){
      $fields.after("<p class='style-suggestion'></p>");
    },
    
    // assign the rule and message set key 
    
    assign_rules: function(){
      var title_rules = $title.data('rules', 'title');
      var artist_rules = $artist.data('rules', 'artist');
    },
    
    bind_events: function(){
      $(document).on('keyup', '#single_name', self.listen);
      $(document).on('keyup', '#single_creatives__name', self.listen);
    },
    
    listen: function(){
      var t = this;
      setTimeout(function(){
        self.test_input.call(t);
      }, 400);
    },
    
    test_input: function(){
      var $ruleset = $(this).data('rules');
      var currentVal = $(this).val();
      //iterate over rules object, testing each rule against current input
      for (var rule in rules[$ruleset]) {
        if (rules[$ruleset][rule].test(currentVal)){
          self.display_suggestion.apply(this, [this, rule]);
        }  
      }
    },
    
    display_suggestion: function(element,message){
      var $ruleset = $(this).data('rules');
      var pos = ($(element).caret() * 7) + 'px';  //a rough estimate of where the cursor will be
      $(element).next()
        .text(messages[$ruleset][message])
        .fadeIn(350)
        .css('left', pos);
        setTimeout(function(){
          self.hide_suggestion();
        }, 1000);
    },
    
    hide_suggestion: function(){
      $(this).prev().bind('keyup keydown blur', function(){
        $(this).next().css('display', 'none');}
      );
    }
  };
  return self.init();
  
};

var init_styleguide = function(){
  new StyleGuide();
};

jQuery(document).ready(init_styleguide);

// A small plugin to get the carets position in a textfield
(function($) {
  $.fn.caret = function(pos) {
    var target = this[0];
    if (arguments.length == 0) { //get
      if (target.selectionStart) { //DOM
        var pos = target.selectionStart;
        return pos > 0 ? pos : 0;
      }
      else if (target.createTextRange) { //IE
        target.focus();
        var range = document.selection.createRange();
        if (range == null)
          return '0';
        var re = target.createTextRange();
        var rc = re.duplicate();
        re.moveToBookmark(range.getBookmark());
        rc.setEndPoint('EndToStart', re);
        return rc.text.length;
      }
      else return 0;
    } //set
    if (target.setSelectionRange) //DOM
      target.setSelectionRange(pos, pos);
    else if (target.createTextRange) { //IE
      var range = target.createTextRange();
      range.collapse(true);
      range.moveEnd('character', pos);
      range.moveStart('character', pos);
      range.select();
    }
  };
})(jQuery);
