/*
  money-graphs.js
  Draws a lot of pretty canvas things on the my account page
*/

var SalesGraph = function(){
  var $ = jQuery;
  var revenueRequest = 'http://'+ TC.domain + '/tc_services/sales_6_mo_rev/';
  var unitsRequest = 'http://'+ TC.domain + '/tc_services/sales_6_mo_units/';
  var topTracksRequest = 'http://'+ TC.domain +'/tc_services/top_5_songs/';
  var ad1link = '/store/product/82';
  var ad2link = '/store/product/72';
  var ad3link = 'http://tunecore.discproductionservices.com/SelfServiceQuoter/PreSelectProject.aspx?configid=191376&itemid=CDONLINE-T-CONTENT&qty=100&proditemid=5DAY';
  var height = 175;
	var width = 939;

  //var r = Raphael("graph", 961, 170); // special Raph canvas just for the spinner Gifs
  //var s1 = makeSpinners(358);

  //private functions
  function createCanvas(id, url) {
    var canvas = document.createElement('canvas');
    if ($.browser.msie) { // for ie/excanvas
      G_vmlCanvasManager.initElement(canvas); 
    };
    var $canvas = $(canvas);
    var link = $('<a href='+ url +'></a>');
    $canvas.attr({'id': id, 'height': height, 'width': width});
    link.html($canvas);
    return link;
  };

  function makeSpinners(x) {
    return r.image("/images/ajax-loader-sm.gif",x,78,24,24);
  };

  
  var self = { 
    
    init: function(){
      
      if (TC.displayGraphs) {
       	self.drawGraph();
      };
      
    },

    drawGraph: function(json) {
      var f = new Graph({"id": "sales-graph", "hues": [33,39,84], "type": "Revenue", "caption":"Earnings by Month", "dimensions": [1,23,width,height],
      "helptext":"The amount of money you made from download sales and streams each month.", "chartStyle":graphSettings.chartStyle, 
			"maxValue":graphSettings.maxValue}).init(graphSettings.data);
    }       
  };
  
  return self.init();
};

jQuery(document).ready(function(){
  var f = new SalesGraph();
});
