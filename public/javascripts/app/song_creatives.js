var TC = TC || {};

var Creatives = function(options){
  var $ = jQuery;
  var add_main = '.add-main';
  var add_featured = '.add-featured';
  var remove_button = '.remove-creative';
  var creatives = '.creative';
  var preview = '#preview-title';
  var preview_text = '#preview-title-text';
  var preview_artist_text = '#preview-artist-text';

  var input_trigger_title_preview = 'input.trigger-title-preview';
  var input_trigger_artist_preview = 'input.trigger-artist-preview';
  var link_trigger_title_preview = 'a.trigger-title-preview';
  var link_trigger_artist_preview = 'a.trigger-artist-preview';

  var main_artists = '.primary_artist';
  var feat_artists = '.featuring';
  var creative_types = $(TC.roles);
  var artist_timer = 0;
  var title_timer  = 0;
  var firstMainArtist = $.trim($('#artist-name-text').html());

  var self = {
    init: function(){
      $(document).on('click', remove_button, self.remove_clicked);
      $(document).on('click', add_main, self.add_main_artist);
      $(document).on('click', add_featured, self.add_featured_artist);
      self.init_trigger_preview();
      self.set_creative_numbers();
      self.bind_artist_validator();
    },

    init_trigger_preview: function(){
      //Bind events to input fields
      $(document).on('click', input_trigger_title_preview, self.show_preview);
      $(document).on('click', input_trigger_artist_preview, self.show_preview);
      $(document).on('keyup change', input_trigger_title_preview, self.title_changed);
      $(document).on('keyup change', input_trigger_artist_preview, self.artist_changed);

      //Bind events to links that remove creatives
      $(document).on('click', link_trigger_title_preview, self.title_changed);
      $(document).on('click', link_trigger_artist_preview, self.artist_changed);
    },

    //
    // Event handlers
    //
    add_main_artist: function(e){
      e.preventDefault();
      var $this = $(this);
      var $container = $this.closest('fieldset').prev('.primary_artists');
      if ( $container.find('.creative').last().is(':hidden') ) {
        $container.find('.creative').first().find('.creative_number').show();
        $container.find('.creative').last().find('.form-text').removeClass('number-hidden');
        $container.find('.creative').last().find('.remove-creative').removeClass('hide');
        $container.find('.creative').last().find('.creative_number').show();
        $container.find('.creative').last().removeClass('hide');
        $('.primary_artists').children('label').find('span').text(gon.main_artists);
      } else if ( TC.is_various && self.total_main_artist_count() === 1 ) {
        $container.find('.creative').first().find('.creative_number').show();
        $container.find('.creative').first().find('.form-text').removeClass('number-hidden');
        $container.find('.creative').first().find('.remove-creative').removeClass('hide');
        $('.primary_artists').children('label').find('span').text(gon.main_artists);
        self.add($container.find('.creative').last(), creative_types[1], '.primary_artists');
      } else {
        self.add($container.find('.creative').last(), creative_types[1], '.primary_artists');
      }
      self.set_creative_numbers();
      self.one_artist();
      self.bind_main_artist_validator($container.find('.creative').last().find('.main_artist'));
    },

    add_featured_artist: function(e){
      e.preventDefault();
      var $this = $(this);
      var $container = $this.closest('fieldset').prev('.featuring_artists');
      if ( self.total_feat_artist_count() === 1 ) {
        $container.find('.creative').first().find('.form-text').removeClass('number-hidden');
        $container.find('.creative').first().find('.remove-creative').removeClass('hide');
        $container.find('.creative').first().find('.creative_number').show();
        $('.featuring_artists').children('label').text('Featured Artists');
      }
      self.add($container.find('.creative').last(), creative_types[0], '.featuring_artists');
      self.set_creative_numbers();
      self.one_artist();
      self.bind_featured_artist_validator($container.find('.creative').last().find('.featured_artist'));
    },

    remove_clicked: function(e){
      e.preventDefault();
      var creative = self.find_creative(this);
      var type = creative.data('type');
      // If Main artist and not a various artist album then
      // don't delete 2nd to last creative only hide it and clear it
      if (type === 'primary_artist' && self.total_main_artist_count() === 2 && !TC.is_various) {
        creative.addClass('hide');
        self.clear_fields(creative);
        $(main_artists).first().find('.creative_number').hide();
        $('.primary_artists').children('label').find('span').text(gon.main_artist);
      } else {
        self.remove(creative);
        self.set_creative_numbers();
        if (self.total_feat_artist_count() === 1) {
          $(feat_artists).first().find('.form-text').addClass('number-hidden');
          $(feat_artists).first().find('.remove-creative').addClass('hide');
          $(feat_artists).first().find('.creative_number').hide();
          $('.featuring_artists').children('label').text('Featured Artist');
        } if (self.total_main_artist_count() === 1 && TC.is_various) {
          $(main_artists).first().find('.form-text').addClass('number-hidden');
          $(main_artists).first().find('.remove-creative').addClass('hide');
          $(main_artists).first().find('.creative_number').hide();
          $('.primary_artists').children('label').find('span').text(gon.main_artist);
        }
      }
    },

    title_changed: function(e){
      /* Triggered by keyup on the input fields */
      if (e.type === 'keyup') {

        /* Clear a current running timer for the title change */
        if( title_timer !== 0 ){
          clearTimeout(title_timer);
        }

        /* Delay title update */
        title_timer = setTimeout(function(){
          self.show_update_preview('title');
          title_timer = 0;
        }, 750);

      /* Triggered by the remove creative links */
      } else if (e.type === 'click') {
        self.show_update_preview('title');
      /* Triggered by loss of focus on the input fields */
      } else if (e.type === 'change') {
        /* If we've actually trigged a key up that hasn't been updated do it immediately */
        if( title_timer !== 0 ) {
          clearTimeout(title_timer);
          self.show_update_preview('title');
          title_timer = 0;
        }
      }
      return true; // allow default action to take place
    },

    artist_changed: function(e){
      if (e.type === 'keyup') { // add delay for keyup only

        if( artist_timer !== 0 ){
          clearTimeout(artist_timer);
        }

        /* Delay artist update */
        artist_timer = setTimeout(function(){
          self.show_update_preview('artist');
          artist_timer = 0;
        }, 750);

      /* Triggered by the remove creative links */
      } else if (e.type === 'click') {
        self.show_update_preview('artist');
      /* Triggered by loss of focus with change on the input fields */
      } else if ( e.type === 'change' ) {
        //If we've actually trigged a key up that hasn't been updated do it immediately
        if( artist_timer !== 0 ) {
          clearTimeout(artist_timer);
          self.show_update_preview('artist');
          artist_timer = 0;
        }
      }
      return true; // allow default action to take place
    },

    show_update_preview: function(type){
      self.show_preview();
      self.update_preview(type);
    },

    //
    //  DOM Transformations
    //

    //
    //  Add a new creative after the current
    //  creative
    //
    add: function(el, type, container){
      var creative = self.new_creative(el);
      $(creative).insertAfter(el);
      $(container).find('.creative').last().addClass(type[1]);
      $(container).find('.creative_type').last().html(type[0]);
      $(container).find('.creatives_hidden').last().val(type[1]);
      if ( type[1] === 'primary_artist' ) {
        $(container).find('.creative_number').last().html(self.next_main_index());
      } else {
        $(container).find('.creative_number').last().html(self.next_feat_index());
      }
      self.init_trigger_preview();
    },

    //
    //  Remove the current creative
    //
    remove: function(el){
      if( self.is_last_creative() ){
        self.clear_fields($(creatives));
        self.hide();
        self.show_link();
      }
      else {
        $(el).remove();
      }
    },

    //
    //  An action to pull the preview for the title
    //  from the server and update the UI
    //
    update_preview: function(type){
      if( $(preview).is(':visible') ){
        var field, url_slug, set_action;

        if (type === 'title') {
          field = preview_text;
          url_slug = '_title';
          set_action = self.set_preview_text;
        } else {
          field = preview_artist_text;
          url_slug = '_artist';
          set_action = self.set_preview_artist_text;
        }
        $(field).trigger('preview'); //trigger custom event to fire input autogrow before
        form_submission = $('.preview-title-form');
        form_data = $(field).parents('form');
        action = form_submission.attr('action') + '/preview';
        data = form_data.serialize();
        if (data.length) {
          self.start_spinner(field);
          $.get(action + url_slug, data, set_action, 'text');
          setTimeout(function(){$(field).trigger('preview');}, 300); // and after
        }
      }
    },

    show_preview: function(){
      if( $(preview).is(':hidden') ){
        $(preview).show();
      }
    },

    //
    // Sets the given value to
    // the preview text box
    //
    set_preview_text: function(text){
      if (text !== '') {
        $(preview_text).html(text);
      } else {
        $(preview_text).html('Song Title Here');
      }
      self.stop_spinner(preview_text);
    },

    set_preview_artist_text: function(text){
      if (text !== '') {
        $(preview_artist_text).html('by ' + text);
      } else {
        $(preview_artist_text).html('by Artist');
      }
      self.stop_spinner(preview_artist_text);
    },

    set_creative_numbers: function(){
      $(main_artists).each(function(i) {
        $(this).find('.creative_number').html(i + 1);
      });
      $(feat_artists).each(function(i) {
        $(this).find('.creative_number').html(i + 1);
      });
    },

    //
    // Spinner Controls
    //
    start_spinner: function(preview){
      $(preview).addClass('spinner-bg');
    },

    stop_spinner: function(preview){
      $(preview).removeClass('spinner-bg');
    },

    //
    //  Remove all values from the
    //  given element
    //
    clear_fields: function(el){
      $(el).find('.form-text').prop('value','');
      return el;
    },

    //
    //  Data
    //
    is_last_creative: function(){
      return $(creatives).length <= 1;
    },

    //
    //  Find the creative container of the given
    //  element.
    //
    find_creative: function(el){
      return $(el).closest(creatives);
    },

    next_main_index: function(){
      return self.total_main_artist_count();
    },

    next_feat_index: function(){
      return self.total_feat_artist_count();
    },

    //
    //  Get the total amount of creative
    //  elements
    //
    total_main_artist_count: function(){
      return $(main_artists).size();
    },

    total_feat_artist_count: function(){
      return $(feat_artists).size();
    },

    //
    //  Put together a new creative element
    //  with empty fields
    //
    new_creative: function(seed){
      var creative = $(seed).clone(false);
      var new_creative = creative;
      creative = self.clear_fields(creative);
      return new_creative;
    },

    one_artist: function(){
      $('.one-artist').styleGuide({
        rules: {
          '.': /\./,
          ',': /\,/,
          '/': /\//,
          '&': /\s\&\s/,
          '+': /\s\+\s/,
          '-': /\s\-\s/,
          'and': /\sand\s/i,
          'with': /\sw((ith)|\/)\s/i,
          'meets': /\smeets\s/i,
          'versus': /\sv(s|ersus)\s/i
        },
        messages: {
          '.': 'Are you trying to add another artist to this song? If so, please click "Add Another Main Artist"',
          ',': 'Are you trying to add another artist to this song? If so, please click "Add Another Main Artist"',
          '/': 'Are you trying to add another artist to this song? If so, please click "Add Another Main Artist"',
          '&': 'Are you trying to add another artist to this song? If so, please click "Add Another Main Artist"',
          '+': 'Are you trying to add another artist to this song? If so, please click "Add Another Main Artist"',
          '-': 'Are you trying to add another artist to this song? If so, please click "Add Another Main Artist"',
          'and': 'Are you trying to add another artist to this song? If so, please click "Add Another Main Artist"',
          'with': 'Are you trying to add another artist to this song? If so, please click "Add Another Main Artist"',
          'meets': 'Are you trying to add another artist to this song? If so, please click "Add Another Main Artist"',
          'versus': 'Are you trying to add another artist to this song? If so, please click "Add Another Main Artist"'
        }
      });
    },

    collectMainArtists: function(){
      var artists = [firstMainArtist];
      $('.main_artist').each(function(){
        var t = $(this);
        artists.push(t.val());
      });
      return artists;
    },

    collectFeaturedArtists: function(){
      var artists = [];
      $('.featured_artist').each(function(){
        var t = $(this);
        artists.push(t.val());
      });
      return artists;
    },

    bind_main_artist_validator: function(field){
      field.styleGuide({
        rules: {
          duplicate_artist: function(v) {
            return self.duplicate_main_artist(v);
          }
        },
        messages: {
          duplicate_artist: 'Primary and Featuring artists should not be the same.'
        }
      });
    },

    bind_featured_artist_validator: function(field){
      field.styleGuide({
        rules: {
          duplicate_artist: function(v) {
            return self.duplicate_feat_artist(v);
          }
        },
        messages: {
          duplicate_artist: 'Primary and Featuring artists should not be the same.'
        }
      });
    },

    duplicate_main_artist: function(value) {
      return self.duplicate_artist(value, self.collectFeaturedArtists());
    },

    duplicate_feat_artist: function(value) {
      return self.duplicate_artist(value, self.collectMainArtists());
    },

    duplicate_artist: function(value, array) {
      value = value.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '');
      var artists = array;
      var reg = new RegExp(value, 'ig');
      var result = false;
      if (value.length > 1){
        $.each(artists, function(i, value){
          if (value.length > 1 && reg.test(value)) result = true;
        });
      }
      return result;
    },

    bind_artist_validator: function(){
      $('.main_artist').each(function(){
        self.bind_main_artist_validator($(this));
      });
      $('.featured_artist').each(function(){
        self.bind_featured_artist_validator($(this));
      });
    }

  };
  self.init();
  return self;
};

jQuery(document).ready(function(){
  TC.creatives = new Creatives();
});
