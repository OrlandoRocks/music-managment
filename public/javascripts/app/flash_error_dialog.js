jQuery(document).ready(function($) {
  var flashErrorDialog = $('#flashErrorDialog');
  if (flashErrorDialog.length > 0) {
    flashErrorDialog.dialog({
      autoOpen: true,
      resizable: false,
      modal: true,
      draggable: false,
      width: 400,
      maxWidth: 400,
      height: 200,
      maxHeight: 200
    });
    flashErrorDialog.removeClass('hide'); // for tc-foundation. gotta love it. it's actually mandatory 😡
  }
});
