$(function(){

  var TCSoundoutDashboard = function(){
    var reportsTmpl = _.template($('#reportsTmpl').html().replace(/(\r\n|\n|\r)/gm, '')),
        pagerTmpl = _.template($('#pagerTmpl').html().replace(/(\r\n|\n|\r)/gm, '')),
        lastSearch = null,
        page = 1,
        filterForm = $('.filter_form'),
        container = $('.reports'),
        searchInput = $('#search'),
        sortBySelect = $('#sort_by'),
        applyButton = $('.apply-button'),
        resetButton = $('.reset-button'),
        pager = $('.pager'),
        refreshModal = $('#refreshModal'),
        self = {
          init: function(){
            return self.bind(),
                   self;
          },
          changePage: function(pg){
            page = parseInt(pg);
            return self.fetch($.extend(true, {}, lastSearch, {page: page})),
                   self;
          },
          search: function(){
            page = 1;
            var search = searchInput.val(),
                params = {
                  order: sortBySelect.val(),
                  page: page
                };
            if(search || !1) params.search = search;
            return self.fetch(params),
                   self;
          },
          reset: function(){
            setTimeout(function(){self.search()}, 0);
            return self;
          },
          fetch: function(params){
            params.per_page = 30;
            lastSearch = params;
            container.html('<div class="text-center"><img src="/images/disco-loader.gif"></div>');
            return self.fetch_and_render(params),
                   self;
          },
          fetch_and_render: function(params){
            $.getJSON('/api/soundout_reports.json', params, function(d){
              if(d.soundout_reports.length) self.render(d);
              else self.showNoResults();
            }).error(function(){
              self.showRefreshDialog();
            });
            return self;
          },
          render: function(data){
            return container.html(reportsTmpl(data)),
                   pager.html(pagerTmpl(data)).show(),
                   self;
          },
          showRefreshDialog: function(){
            refreshModal.modal({
              maxWidth: 450,
              minWidth: 450,
              maxHeight: 100,
              minHeight: 100,
              persist: true
            });
            return self;
          },
          showNoResults: function(){
            return pager.html('').hide(),
                   container.html('<div class="text-left">No results found</div>'),
                   self;
          },
          bind: function(){
            return self.bindChangePageSelect()
                       .bindClickPageNext()
                       .bindClickPagePrev()
                       .bindClickApply()
                       .bindResetFilterForm(),
                   self;
          },
          bindChangePageSelect: function(){
            pager.on('change', '.select_page', function(){
              var t = $(this);
              self.changePage(t.val());
            });
            return self;
          },
          bindClickPageNext: function(){
            pager.on('click', '.pager-right.page_link', function(e){
              e.preventDefault();
              self.changePage(page+1);
            });
            return self;
          },
          bindClickPagePrev: function(){
            pager.on('click', '.pager-left.page_link', function(e){
              e.preventDefault();
              self.changePage(page-1);
            });
            return self;
          },
          bindClickApply: function(){
            applyButton.on('click', function(e){
              e.preventDefault();
              self.search();
            });
            return self;
          },
          bindResetFilterForm: function(){
            filterForm.on('reset', function(e){
              self.reset();
            });
            return self;
          }
        };

    return self.init(),
           self;
  };

  new TCSoundoutDashboard();

});
