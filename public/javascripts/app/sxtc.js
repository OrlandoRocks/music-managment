var TC = TC || {};
var sxtc = function () {
  var $ = jQuery;
      
  var self = {
    init: function () {
      self.load_artists();
    },
    
    // Adds a click handler to activate the jQuery UI Dialog
    //
    load_artists: function () {
      var easing_technique = ( $('html').hasClass('ie') || navigator.userAgent.indexOf("Trident/5") > -1) ? "linear" : "ease-in-out";
      
      $(".sxtc-acts").isotope({
        itemSelector: '.act',
        layoutMode: 'masonry',
        animationEngine : 'best-available',
        animationOptions:{
          duration: 500,
          easing: easing_technique,
          queue: !1
        }
      });
    }
  };
  return self.init();
};

jQuery(document).ready(function($){
  $(window).load(function(){ TC.sxtc = new sxtc(); });
});