(function() {
  var $ = jQuery;
  $(document).on("ready", function() {
    var $currencyInputMain = $(".currency-input-main");
    var $currencyInputFractional = $(".currency-input-fractional");

    $currencyInputMain.on("keydown", function(e) {
      if (!isNumber(e) && !isCommandKey(e)) {
        e.preventDefault();
      }
    });

    $currencyInputMain.on("keyup", function() {
      var value = $currencyInputMain.val();
      var regex = new RegExp(/[a-z]|[A-z]|\W/g);
      value = value.replace(regex, "");
      $currencyInputMain.val(addCommas(value));
    });

    $currencyInputFractional.on("keydown", function(e) {
      var hasNoSelection = window.getSelection().toString().length === 0;
      var lengthExceeded = $currencyInputFractional.val().length > 1;

      if (!isNumber(e) && !isCommandKey(e)) {
        e.preventDefault();
      } else if (lengthExceeded && hasNoSelection && !isCommandKey(e)) {
        e.preventDefault();
      }
    });
  });

  function isNumber(e) {
    return /\d/.test(e.key);
  }

  function isCommandKey(e) {
    return e.key.length > 1;
  }

  function addCommas(nStr) {
    // https://stackoverflow.com/a/2646441
    nStr = nStr.toString();
    var x = nStr.split(gon.currency_delimiter);
    var x1 = x[0];
    var x2 = x.length > 1 ? gon.currency_delimiter + x[1] : "";
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
      x1 = x1.replace(rgx, "$1" + gon.currency_separator + "$2");
    }
    return x1 + x2;
  }
})();
