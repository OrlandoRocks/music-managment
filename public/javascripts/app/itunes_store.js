jQuery(function($) {
  $(".pod-questions-container").on("click", ".question", function(e) {
    e.preventDefault();
    var $t = $(this),
    $parent = $t.parent(),
    $all_questions = $(".question");
    
    if ( $parent.hasClass("openned") ) {
      $parent.removeClass("openned");
      $t.find(".fa").removeClass("fa-rotate-90");
    } else {
      $all_questions.parent().removeClass("openned");
      $all_questions.find(".fa").removeClass("fa-rotate-90");
      $parent.addClass("openned");
      $t.find(".fa").addClass("fa-rotate-90");
    }
  });
});