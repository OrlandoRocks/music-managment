/* Sales.js
  = Description
  The following code allows the sales page to have select all buttons,
  table sorting, collapsing panels, and ajax requests.
  
  = Change log
  [2011-01-27 -- RT] Initial Creation
  
  = Dependencies
  Depends on the jquery.tablesorter, jquery.tablesorter.pager, and selectToUISlider.jQuery plugins.
  
  = Usage
  Not a function to be used outside this file.
  
*/
var TC = TC || {};
var Sales = function () {

  // Set up $ to be used locally for jQuery then
  // cache selectors so you don't have to traverse
  // the DOM each time.
  var $ = jQuery,
  table = "table.transaction-report",
  $date_start_selector = $("#date_start_date"),
  $date_end_selector = $("#date_end_date");
  
  var self = {
    
    init: function () {
      self.collapsible_rows( $(table) );
      self.negative_dates_hider();
    },

    // Adds a collapsible link to each header anchor
    // 
    // Must have the following layout:
    // <div class="collapsible"/>
    //   <someHeading /><a href="#optionalHash" class="toggle-button"/>
    //   <elementToBeHidden id="optionalHash"/>
    //
    // If there's a hash on the url corresponding to the optionalHash
    // it will be opened when loaded.
    // Add a class called dOpen to the anchor to have it be opened by default.
    //
    collapsible_rows: function (container) {
      var $links = $(container).find('a.column-expand'); // collect the links
      
      // add toggle indicator to all links
      $links.parents('td.transaction-desc').prev().prepend('<span class="toggle_plus"></span>');
      
      // add class to tbody
      $links.parents('tbody').addClass('collapsible');
      
      $links.parents('tr.transaction').click(function () {
        $(this).next('.export').toggleClass("hidden").nextAll('.intake-transaction').toggleClass("hidden");
        $(this).nextAll('.split-transaction').toggleClass("hidden");
        return false;
      }).toggle(function () {
          $(this).children('th.side').children("span.toggle_plus").remove();
          $(this).children('th.side').prepend('<span class="toggle_minus"></span>');
        }, function () {
          $(this).children('th.side').children("span.toggle_minus").remove();
          $(this).children('th.side').prepend('<span class="toggle_plus"></span>');
        });
    },
    
    // Adds a collapsible link to each header anchor
    // 
    // Must have the following layout:
    // <div class="collapsible"/>
    //   <someHeading /><a href="#optionalHash" class="toggle-button"/>
    //   <elementToBeHidden id="optionalHash"/>
    //
    // If there's a hash on the url corresponding to the optionalHash
    // it will be opened when loaded.
    // Add a class called dOpen to the anchor to have it be opened by default.
    //
    collapsible_panels: function (container) {
      var $links = $(container).find('a.toggle-button'); // collect the song record links

      $links.append(' <span class="toggle_plus"></span>'); // add toggle indicator to all links
      $links.click(function () {
        $(this).parent().next().slideToggle();
        return false;
      }).toggle(function () {
          $(this).children("span.toggle_plus").remove();
          $(this).append(' <span class="toggle_minus"></span>');
        }, function () {
          $(this).children("span.toggle_minus").remove();
          $(this).append(' <span class="toggle_plus"></span>');
        }).parent().next().hide();

      // if there is are any of the class dOpen, simulate a click on it
      $('a.dOpen').each(function() {$(this).click();});
    },
    
    // Show only end dates that comes after start date
    // 
    // 1. Get all the date options in arrays
    // 2. If we use the start date selector
    // 3. Disable dates which come before it in the end date selector
    // 4. If an end date which is older is already selected move the date up
    //    to the current start date
    // 
    negative_dates_hider: function () {
      var $start_dates = $("#date_start_date").find("option"), // 1.
      $end_dates = $("#date_end_date").find("option"); // 1.
      
      $date_start_selector.change(function () { // 2.
        var selected_date = $(this).find("option:selected").val();
        
        $end_dates.each(function () {
          if ( $(this).val() < selected_date ) { // 3.
            $(this).prop('disabled', true);
          } else {
            $(this).prop('disabled', false);
          }
        });
        
        if ( $date_end_selector.find("option:selected").val() < selected_date ) { // 4.
          $date_end_selector.val(selected_date);
        }
      });
    }
  };
  return self.init();
};

jQuery(document).ready(function ($) {
  TC.sales = new Sales();

  $("#sales-table th").on('click', 'i.qtip-it', show_tooltip)

  function show_tooltip(e) {
  var $element = $(e.target)
  var tooltip = $element.data('tooltip');
    $element.qtip({
      overwrite: false,
      content: tooltip,
      show: { event: e.type, ready: true },
      hide: { delay: 200, fixed: true }
    }, e);
  }
});
