/*
	Transaction form for administrative search.
	Also used on fraud/index to initiate date fields.
	CH - 2009-9-30
*/
var SearchForm = function(){
	var date_class = ".date-box";
	var resolution_select = "#resolution";
  var range_select = ".range-select:enabled";
	var $ = jQuery;
	
	var self = {
		init: function(){
			$(resolution_select).change(this.show_or_hide_options);
      $(range_select).change(this.show_or_hide_range_options);
			//self.init_date_pickers();
		},
		

    show_or_hide_range_options: function(){
      if($(range_select).val()=="custom"){
        self.show_hide_custom_date_selection(true);
      }
      else{
        self.show_hide_custom_date_selection(false);
      }
    },

		show_hide_custom_date_selection: function(show){
			if(show == true){
				if($("#custom-date-selection").is(":hidden")) { 
					$("#custom-date-selection").show(); 
					$("#custom-date-selection").prop("disabled","");
				}
			}else{
				$("#custom-date-selection").hide(); 
				$("#custom-date-selection").prop("disabled","disabled");
			}
		}, 

		show_or_hide_options: function(){
      self.show_hide_custom_date_selection(false);
			if($(resolution_select).val()=="day"){
		    self.show_hide_day(true);
				self.show_hide_month(false);
				self.show_hide_week(false);
		  }else if($(resolution_select).val()=="month"){
		    self.show_hide_day(false);
				self.show_hide_month(true);
				self.show_hide_week(false);
		  }else{
			  self.show_hide_day(false);
				self.show_hide_month(false);
				self.show_hide_week(true);
			}
		},

		show_hide_week: function(show){
			if(show == true){
				if($("#week_range").is(":hidden")) { 
					$("#week_range").show(); 
					$("#week_range").prop("disabled","");
				}
			}else{
				$("#week_range").hide(); 
				$("#week_range").prop("disabled","disabled");
			}
		},

		show_hide_month: function(show){
			if(show == true){
				if($("#month_range").is(":hidden")) { 
					$("#month_range").show(); 
					$("#month_range").prop("disabled","");
				}
			}else{
				$("#month_range").hide(); 
				$("#month_range").prop("disabled","disabled");		
			}
		},
		
		show_hide_day: function(show){
			if(show == true){
				if($("#day_range").is(":hidden")) { 
					$("#day_range").show(); 
					$("#day_range").prop("disabled","");
				}
			}else{
				$("#day_range").hide(); 
				$("#day_range").prop("disabled","disabled");
			}
		},
		
		init_date_pickers: function(){
  		new DatePicker(date_class,{});			
		}
	};
	
	return self.init();
};


var init_search_form = function(){
  new SearchForm();
};
jQuery(document).ready(init_search_form);



