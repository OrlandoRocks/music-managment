jQuery(function ($) {
  $(document).ready(function(){

    $('.popup-spotify-tracks-modal').magnificPopup({
      type: 'inline',
      preloader: false,
      modal: true,
      disableOn: function(){
        return gon.magnific;
      }
    });

    $(document).on('click', '.close-modal', function (e) {
      e.preventDefault();
      $.magnificPopup.close();
    });
  });
});
