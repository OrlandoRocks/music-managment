/* Song_Album.js
  = Description
  The following code allows the save, edit, delete, and upload buttons on
  the Album Page to make ajax requests for updating or adding song content.

  = Change log
  [2010-5-18 -- GB] Initial Creation
  [2010-5-19 -- RT] Changed ordering in load_post_results for the added
                    track to come from response, while the song form is
                    now an ajax request
  [2010-5-20 -- RT] Added edit song, and delete song functionality
  [2010-5-20 -- GB] Added reordering songs functionality
  [2010-5-25 -- GB] Added zebra striping of song list and improved styling
  [2010-6-02 -- MT] Adding a function to help position error messages for songs
  [2010-6-03 -- ML] Adding logic to allow multiple uploads to be toggled on and off
  [2010-6-08 -- RT] Disabled all edit and delete buttons after clicking Upload
  [2010-6-25 -- MT] Adding metadata suggester
  [2011-3-15 -- GB] Fixed possible clobbered merge

  = Dependencies
  Depends on the jquery.ui for highlighting appendages.

  = Usage
  Not a function to be used outside this file.

*/
var TC = TC || {};
var MU = 0;
var currentNum = 0;
var currentClip = '';
var uuidPlaying = 0;
var uploadedFileList = {};

var SongAlbum = function() {
  // Set up $ to be used locally for jQuery then
  // cache selectors so you don't have to traverse
  // the DOM each time.
  var $ = jQuery;
  var save_button = '.save-button';
  var upload_button = '.upload-button';
  var edit_button = '.edit-button';
  var delete_button = '.delete-button';
  var track_list = $('#track-list');
  var song_form = $('#song-form');
  var reorder_toggle = $('#reorder-toggle');
  var show_form_toggle = $('#show-form-toggle');
  var hidden_form_toggle = $('#show-form-toggle.hidden');

  function formatDuration(d){
    var duration = Number(d);
    duration = duration / 1000;
    var h = Math.floor(duration / 3600);
    var m = Math.floor(duration % 3600 / 60);
    var s = Math.floor(duration % 3600 % 60);
    return ((h > 0 ? h + ':' : '') + (m > 0 ? (h > 0 && m < 10 ? '0' : '') + m + ':' : '0:') + (s < 10 ? '0' : '') + s);
  }

  function formatSR(d){
    var duration = Number(d);
    duration = duration * 0.001;
    return duration.toString() + 'k';
  }

  function formatBR(d){
    var duration = Number(d);
    duration = Math.floor(duration/1000);
    return duration.toString()+' kbps';
  }

  function splitFileName(name){
    var a = name.split('-');
    a.shift();
    return a.join();
  }

  var restripe = function() {
    $('#track-list').children('li').removeClass('odd');
    $('#track-list').children('li:nth-child(odd)').addClass('odd');
    $('#track-list').children('li:nth-child(even)').addClass('even');
  };

  //  Make a call to the webserver to return the album checklist and update
  //  it on the page.
  var updateChecklist = function(){
    var request = '/albums/' + TC.albumId + '/distribution_panel';

    $.get(request,
          function(response){
            $('#distribution-panel').empty().append(response);

            //if the purchased credit prompt box is in the response
            //set up event handlers
            if ($(response).find('#upgrade-prompt').length) {
              var no = '#upgrade-no',
                yes = '#upgrade-yes',
                prompt = $('#upgrade-prompt'),
                opts = $('#upgrade-options'),
                use = $('#use-credit'),
                keep = $('#keep-credit'),
                goBack = 'a.upgrade-change';

              var hidePrompt = function(){
                prompt.addClass('hidden');
                opts.removeClass('hidden');
              };

              $(document).on('click', no, function(){
                hidePrompt();
                use.removeClass('hidden');
              });

              $(document).on('click', yes, function(){
                hidePrompt();
                keep.removeClass('hidden');
              });

              $(document).on('click', goBack, function(){
                prompt.removeClass('hidden');
                opts.addClass('hidden');
                keep.addClass('hidden');
                use.addClass('hidden');
              });
            }
          },
          'html');
  };

  var self = {

    init: function(){
      //  If the album has not been finalized, continue.
      if (!TC.finalized){
        // Set up metadata suggester
        if (gon.locale === 'en') {
          self.init_styleguides();
        }

        //  Prepare ajax links.
        $(document).on('click', save_button, self.submit_song);
        $(document).on('mouseover', upload_button, self.move_uploader);
        $(document).on('click', edit_button, self.edit_song);
        $(document).on('click', delete_button, self.delete_song);

        //  Add html for js-only links.
        if ( track_list.children('li').length > 1 ) { reorder_toggle.text('Reorder Songs'); }

        if ( track_list.children('li').length < 1 ) { track_list.hide(); }

        show_form_toggle.html('<a href="#" class="addsong button-callout-alt">' + gon.add_song + '</a>');
        self.add_done_link();

        //  Show form only if "Add Song" toggle is hidden.
        if (hidden_form_toggle.length > 0) {
          song_form.show();
        }

        //  Show Add Song Form Controls
        //  Also clear form after submit
        show_form_toggle.children('a').click(function(e){
          e.preventDefault();

          song_form.find(':input').each(function(){
            switch(this.type) {
            case 'text':
              if ($(this).disabled === false) {
                $(this).val('');
              }
              break;
            case 'checkbox':
              this.checked = false;
              break;
            }
          });
          $('#add-songs .number').html($('#track-list > li').length + 1);
          song_form.show();
          show_form_toggle.hide();
        });

        self.init_sortable();
        return self;
      } else {
        self.disable_all_features();
      }
    },

    init_styleguides: function(){
      if ($('#song_name')) {
        $('#song_name').styleGuide({
          rules: {
            'featuring': /\s\W?f(?:ea)?t(?:(?:\w)+|(?:\s)|(?:\.)?)\s?/i,
            'explicit': /\s([\[(]?([eE]xplicit|[dD]irty)[\])]?)/,
            'produced' : /\s\W?[pP]rod(\W|uce|\s)/
          },
          messages: {
            'featuring': 'Are you trying to add a featured artist to this song? If so, please enter them in the "Featured Artist" field below.',
            'explicit': 'Is this song explicit? If so, click the \'explicit\' checkbox below instead of putting this information in the title.',
            'produced' : 'Are you trying to enter a producer? Stores do not allow this information to be listed, please do not include or your release may be delayed.'
          }
        });
      }
    },

    updateChecklist: function(){
      updateChecklist();
    },

    //  Add Done Link
    add_done_link: function(){
      song_form.find('#song_Save_list_item').append('<a href="#" class="done-button cancel button-interface">I\'m done adding songs</a>');
      $('.done-button').click(function(e){
        e.preventDefault();

        song_form.hide();
        show_form_toggle.show();
      });
    },

    //  Hide Add Song Form
    hide_add_song_form: function(){
      song_form.hide();
      show_form_toggle.show();
    },

    // We need a little help to position the error messages
    // since each <li> in the form is a different length
    adjust_error_messages: function() {
      $('.formError').each(function(){
        var error = $(this);
        var label = error.parent().prev();
        var adjust = label.width() + 20;
        error.css('left', adjust);
      });
    },

    //  Event Handlers
    //
    //  Click Save Button
    //    -IF Add Song Form
    //      1. Ajax POST
    //      2. Append new song from response via load_post_results function
    //    -ELSE Edit Song Form
    //      1. Ajax POST
    //      2. Unwrap partial from li (because we are putting it back into an li)
    //      3. Replace edit song form with the song partial
    //      3a. Restripe the table and do a highlight effect
    submit_song: function(e){
      e.preventDefault();

      var selector = $(this);
      var uuid = $(this).parent('.context-wrapper').parent('#song_Save_list_item').parent('ul').parent('form').parent('fieldset').parent('li').attr('uuid');
      var form = selector.parents('form');
      var href = form.attr('action');
      var data = form.serialize();

      if (form.closest('#add-songs').length > 0){ // IF Add Song Form
        // Start spinner and hide submit/cancel buttons
        $('#song_Save_list_item').append('<img src="/images/icons/spinner-24x24.gif" alt="" id="add-spinner" />');
        selector.hide();
        song_form.find('input').prop('disabled',true);
        $('.done-button').hide();
        $.post( // 1.
          href,
          data,
          function(response){
            if ( $(response).find('.fieldWithErrors').length){ // IF Error
              song_form.empty().append(response);
              self.adjust_error_messages(response);
              self.add_done_link();
            } else if ( song_form.find('.fieldWithErrors').length){ // ELSE IF Resubmit form
              track_list.show();
              $(response).appendTo(track_list).effect('highlight', {}, 2000); // 2.
              restripe();
              $('.track-cancelreupload').hide();
              self.get_song_form();
              updateChecklist();
            } else { // ELSE Normal
              track_list.show();
              $(response).appendTo(track_list).effect('highlight', {}, 2000); // 2.
              $('.track-cancelreupload').hide();
              restripe();
              self.get_song_form();
              reorder_toggle.show();
              if ( track_list.children('li').length > 1) { reorder_toggle.text('Reorder Songs'); }
              $('#add-songs .number').html($('#track-list > li').length + 1);
              updateChecklist();
              $(document).trigger('song_create');
            }
            /*
            $('.song-name, .file-name, .artist-name').magicTip({
              refresh: true,
              position: 'bottomLeft'
            });
            $('.song-name, .file-name, .artist-name').each(function(){
              var t = $(this);
              if(t.attr('title') == t.html()){
                t.attr('disableTip', 'true');
              }
            });
            */
          },
          'html'
        );
        // hide the multiple artists stuff and recreate the link for add multiple artists HERE

      } else { // ELSE Edit Song Form
        $('.save-button[rel=\'edit-form\']').hide();
        $('#song_Save_list_item').append('<img src="/images/icons/spinner-24x24.gif" alt="" style="margin-top:4px" />');
        $('.edit_song').find('input').prop('disabled',true);
        $('.cancel-button').remove();
        reorder_toggle.hide();

        $.post( // 1.
          href,
          data,
          function(response){
            var song_item = selector.parents('li');
            var uuid = song_item.attr('uuid');
            var s = $(response).children().unwrap(); // 2.
            // IF Error
            if ( $(s).find('.fieldWithErrors, .formError').length ){
              s = $(response);
            }
            // ELSE Normal
            $(s).find('#song_Save_list_item').append('<a href="#" class="cancel cancel-button button-interface">Cancel</a>');
            selector.parents('li').effect('highlight', {}, 1000);
            selector.parents('li').html(s); // 3.
            self.adjust_error_messages(response);
            if ( !$(s).find('.fieldWithErrors').length ){
              restripe(); // 3a. (it's a hack don't judge me)
            }
            self.update_assets(uuid);
            reorder_toggle.show();
            $('.edit-button').show();
            $('.track-cancelreupload').hide();
            uuidPlaying = uuid;
            $(s).find('.'+uuidPlaying).find('.track-play').show();
            self.get_song_form();
            $('#add-songs').show();
            if ( $('#track-list > li').length + 1 >= 101 && !TC.hundred_tracks_allowed ) { // 4b.
              song_form.show();
            } else { // 4b.
              show_form_toggle.show();
            }
            restripe();
            // IF Error (ELSE do nothing after this happens)
            song_item.find('.save-button').attr('rel', 'edit-form');
            $('.cancel-button').click(function(e){
              e.preventDefault();

              reorder_toggle.show();
              $('.edit-button').show();
              $('#add-songs').show();
              $.get(
                $(s).last().attr('action'),
                function(response){
                  s = $(response).children().unwrap();
                  song_item.html(s).effect('highlight', {}, 1000); // 3.
                  restripe(); // 3a.
                },
                'html'
              );
              $('.save-button[rel=\'edit-form\']').hide();
              $('#song_Save_list_item').append('<img src="/images/icons/spinner-24x24.gif" alt="" style="margin-top:4px" />');
              $('.cancel-button').remove();
            });
            $(document).trigger('song_update');
          },
          'html'
        );
      }
    },

    //  Click Upload Button
    //  1. Ajax GET upload song form
    //    a. Hide all other upload buttons if multiple uploads not allowed
    //    b. Add count to allowed multiple uploads if it is allowed
    //    c. Hide add song form and add song toggle
    //  2. Replace song item with upload song form
    //
    //  Multiple uploads can be toggled on and off by setting the variable TC.allowMultipleUploads
    //  (currently set at the top of the album/show.erb file)
    move_uploader: function(e){
      e.preventDefault();

      //reorder_toggle.hide();
      var this_button = $(this);
      var parent = this_button.parent('.track-actions').parent('li');
      var listid = parent.attr('id');
      currentNum = listid;

      var add = 0;
      $('#track-list li').each(function(){
        var t = $(this);
        if( parseInt( t.find('.track-number').html(), 10 ) < parseInt( parent.find('.track-number').html(), 10 ) ){
          add = add + t.outerHeight();
        }
      });
      $('#uploader').css('right', 11).css('top', function(){
        var top = 21;
        //var add = (parseInt($('#'+listid).find('.track-number').html()) - 1) * (parseInt(parent.outerHeight()) +1);//60;
        return top + add;
      });

      //$(".edit-button").hide();
      //$(".delete-button").hide();
      //self.hide_add_song_form(); // c.
      //show_form_toggle.hide(); // c.
      //this_button.show(); // Only show current button so we can have a spinner
    },

    //  Click Edit Button
    //  1. Cache song item
    //  2. Ajax GET edit song form
    //  3. Replace song item with edit song form
    //  3a. Hide all other edit buttons and the add song button
    //  3b. Change attributes of the spinner and save button
    //  4. If cancel from form, replace with cached song item
    //  4a. Show hidden controls
    //  4b. Show add song form if limit allowed else show form toggle
    edit_song: function(e){
      e.preventDefault();

      $('#uploader').css('right', 1000);
      if ($f('flowplayer') !== null){
        $f('flowplayer').stop();
      }
      $('#flowplayer').css('right', 5000);
      $('#closeFlowPlayer').css('right', 5000);
      $('.'+uuidPlaying).find('.track-play').show();
      reorder_toggle.hide();
      var href = $(this).attr('href');
      var selector = $(this).parents('li');
      selector.removeClass('odd even');

      var songitem = selector.html(); // 1.
      $(this).html('<img src="/images/icons/spinner-16x16.gif" alt="" />');
      $(this).parents('.rollover-hide').css('display','inline');

      $.get( // 2.
        href,
        function(response){
          selector.empty().append(response); // 3.
          song_form.hide();
          $('.edit-button').hide(); // 3a.
          $('#add-songs').hide(); // 3a.
          if (gon.locale === 'en') {
            self.init_styleguides();
          }
          self.set_creative_numbers();
          song_form.html('');
          selector.find('#spinner').attr('id', 'edit-spinner'); // 3b.
          selector.find('.save-button').attr('rel', 'edit-form'); // 3b.
          selector.find('#song_Save_list_item').append('<a href="#" class="cancel-button cancel button-interface">Cancel</a>');
          $('.cancel-button').click(function(e){
            e.preventDefault();

            selector.html(songitem); // 4.
            reorder_toggle.show(); // 4a.
            $('.edit-button').show(); // 4a.
            self.get_song_form();
            $('#add-songs').show(); // 4a.
            if ( $('#track-list > li').index(selector) + 1 >= 101 && !TC.hundred_tracks_allowed ) { // 4b.
              song_form.show();
            } else { // 4b.
              show_form_toggle.show();
            }
            restripe();
          });
          selector.find('.number').html($('#track-list > li').index(selector) + 1);
        },
        'html'
      );
    },

    //  Click Delete Button
    //  1. -Hide list item- Not necessary if we're rebuilding track list
    //     Confirm delete with alert
    //  2. Ajax POST
    //  3. Update track_list with response
    delete_song: function(e){
      e.preventDefault();

      var href = $(this).attr('href');
      var song_name = $(this).parents('li').find('.song-name').html();
      if ( confirm(gon.confirm_deletion + ': \'' + song_name + '\'?') ){
        $('#uploader').css('right', 1000);
        if ($f('flowplayer') !== null){
          $f('flowplayer').stop();
        }
        $('#flowplayer').css('right', 5000);
        $('#closeFlowPlayer').css('right', 5000);
        $('.'+uuidPlaying).find('.track-play').show();
        $(this).parents('li').fadeOut(500, function(){ $(this).remove(); });
        if ( (track_list.children('li').length-1) > 1) {
          reorder_toggle.text('Reorder Songs');
        } else {
          reorder_toggle.text(' ');
        }
        $.post(
          href.replace('/delete', ''),
          { '_method': 'delete' },
          function() {
            var form = track_list.parents('form');
            var href = form.attr('action');
            var data = form.serialize();
            $.ajax({
              type: 'POST',
              url: href,
              data: data,
              success: self.reload_track_list,
              dataType: 'html'
            });
            $(document).trigger('song_destroy');
            if ( $('#track-list > li').length + 1 >= 101 && !TC.hundred_tracks_allowed ) {
              self.get_song_form();
            }
          },
          'html'
        );
      }
      updateChecklist();
    },

    //  After sending POST for delete a song
    //  or reordering tracks,
    //  we replace the track list with the response
    reload_track_list: function(response){
      track_list.html(response).effect('highlight', {}, 1000);
      if ( !track_list.children('li').length ) {
        track_list.hide();
      } else {
        $('.track-cancelreupload').hide();
        reorder_toggle.show();
      }
      var assets = $(window).data('assets');
      if(assets !== ''){
        var d = assets.response;
        $.each(d, function(i, u){
          var item = $('.'+u.uuid);
          if(u.streaming_url !== '' && u.streaming_url !== undefined){
            item.find('.track-play').show();
          }
          $.each(u, function(k, v){
            item.attr(k, v);
          });
        });
      } else {
        self.update_assets('');
      }
      /*
      $('.song-name, .file-name, .artist-name').magicTip({
        refresh: true,
        position: 'bottomLeft'
      });
      $('.song-name, .file-name, .artist-name').each(function(){
        var t = $(this);
        if(t.attr('title') == t.html()){
          t.attr('disableTip', 'true');
        }
      });
      */
    },

    //  Using an ajax request, grab the song form
    //  from the new song action.
    get_song_form: function(f){
      $.get( window.location.href.replace(window.location.search,'').replace(window.location.hash,'') + '/songs/new',
        function(response){
          song_form.empty().append(response);
          if (gon.locale === 'en') {
            self.init_styleguides();
          }
          self.add_done_link();
          $('#add-songs .number').html($('#track-list > li').length + 1);
          if (typeof f === 'function') f();
        },
        'html'
      );
    },

    // sortable list controls
    // upon init, binds the toggle control to the Reorder Tracks link, and makes the list sortable

    init_sortable: function(){
      self.toggle_reorder();
      track_list.sortable({
        update: self.finished_sort,
        handle: '.handle'
      });
    },

    // set creative numbers for additional artists
    set_creative_numbers: function(){
      $('.primary_artist').each(function(i) {
        $(this).find('.creative_number').html(i + 1);
      });
      $('.featuring').each(function(i) {
        $(this).find('.creative_number').html(i + 1);
      });
    },

    // callback function from sortable track list, serializes and submits AJAX data to reorder tracks

    finished_sort: function(){
      var form = track_list.parents('form');
      var href = form.attr('action');
      var data = form.serialize();
      reorder_toggle.hide();
      $('.handle').html('<img src="/images/icons/spinner-16x16.gif" alt="" />');
      reorder_toggle.hide();
      self.save_sort_submit(href,data);
    },

    // callback rebuilds the sorted list

    save_sort_submit: function(href,data){
      $.ajax({
        type: 'POST',
        url: href,
        data: data,
        success: self.after_save_sort,
        dataType: 'html'
      });
    },

    // housekeeping on the now-sorted song list

    after_save_sort: function(response){
      self.reload_track_list(response);
      $('.track-actions').hide();
      $('.song-data').animate({'left':'40px'},1);
      $('.handle').show();
      reorder_toggle.show();
      $(document).trigger('song_update');
      /*
      $('.song-name, .file-name, .artist-name').magicTip({
        refresh: true,
        position: 'bottomLeft'
      });
      $('.song-name, .file-name, .artist-name').each(function(){
        var t = $(this);
        if(t.attr('title') == t.html()){
          t.attr('disableTip', 'true');
        }
      });
      */
    },

    // toggling the Reorder Songs link moves the song listings over to bring in the handles
    // and changes the link text to Done
    // and hides/shows the entire add-songs form or button, as well as internal song controls to focus
    // the user on reordering tracks.
    // we COULD include delete on these. Maybe.
    //

    toggle_reorder: function() {
      reorder_toggle.toggle(function() {
        $('.song-data').each(function() {
          $(this).stop().animate({'left': '40px'}, 100, function() {
            $('.handle').show();
            $('.track-actions').fadeOut(100);
          });
        });
        $(this).text('Done Reordering');
        $('#add-songs').hide();
        if ($f('flowplayer') !== null){
          $f('flowplayer').stop();
        }
        $('#flowplayer').css('right', 5000);
        $('#closeFlowPlayer').css('right', 5000);
        $('.'+uuidPlaying).find('.track-play').show();
      }, function() {
        $('.handle').hide();
        $('.song-data').each(function() {
          $(this).stop().animate({'left': '15px'}, 100, function() {
            $('.track-actions').fadeIn(100);
          });
        });
        $(this).text('Reorder Songs');
        $('#add-songs').show();
      });
    },

    disable_all_features: function() {
      $('.upload-button').remove();
      $('.edit-button').remove();
      $('.delete-button').remove();
      $('#reorder-toggle').remove();
      $('#show-form-toggle').remove();
    },

    // Show all creatives - housekeeping when adding subsequent songs from one form
    show_creatives: function(){
      // TC.creatives.show.call();
      // var x = new SelectMenu("select.trigger-preview", {'width': 134});
    },

    update_assets: function(uuid){
      var callback = '?callback=?';
      var env = $('#env').attr('env');
      var tc_pid = $('#tc_pid').attr('tc_pid');
      var person_id = $('#person_id').attr('person_id');
      var uploadURL = $('#upload_url').attr('upload_url');
      var registerURL = $('#register_url').attr('register_url');
      var assetsURL = $('#assets_url').attr('assets_url') + callback;
      var elb_name = $('#elb_name').attr('elb_name');
      var currentSongID;
      var currentSongObj;
      var fileList;
      var uploadedList = [];
      $.getJSON('//'+elb_name+'/bigbox/list/?callback=?', {tc_pid: tc_pid}, function(response){
        if(response.status === 0){
          $(window).data('assets', response);
          var d = response.response;
          var html = '';
          $.each(d, function(i, u){
            html += '<div class="asset"><a href="#" class="play" clip="'+u.streaming_url+'">Play</a><a href="#" class="switch" orig_filename="'+u.orig_filename+'" duration="'+formatDuration(u.duration)+'" bit_rate="'+formatBR(u.bit_rate)+'" sample_rate="'+formatSR(u.sample_rate)+'" key="'+u.key+'" bucket="'+u.bucket+'" uuid="'+u.uuid+'" streaming_url="'+u.streaming_url+'">'+splitFileName(u.orig_filename)+'</a> <span>Duration: '+formatDuration(u.duration)+' Sample Rate: '+formatSR(u.sample_rate)+' Bit Rate: '+formatBR(u.bit_rate)+'</span></div><br />';
            if(uuid === u.uuid || uuid === ''){
              var item = $('.'+u.uuid);
              if(u.streaming_url !== '' && u.streaming_url != undefined){
                item.find('.track-play').show();
              }
              $.each(u, function(k, v){
                item.attr(k, v);
              });
            }
          });
          $('#assets').html(html);
        } else {
          // error
          //alert('Oops! There has been an error, please refresh your browser and try again.  If the problem persists please contact support.');
          $('#error').dialog('option', 'title', 'Upload Error').dialog('open');
        }
      });
    }
  };
  return self.init();
};

//  After response of uploaded song from flash uploader
//  render the show song partial again
var show_song = function(song_id){
  var $ = jQuery;
  var song_path = $('#' + song_id).find('#song-path').val();

  // Subtract count to allowed multiple uploads if it is allowed
  if (TC.allowMultipleUploads === true) {
    if (MU > 0) {
      MU -= 1;
    } else {
      MU = 0;
    }
  }

  $.get(
    song_path,
    function(response){
      s = $(response).children().unwrap();
      $('#' + song_id).html(s).effect('highlight', {}, 1000);
      // Restripe
      $('#track-list li').removeClass('odd');
      $('#track-list li:nth-child(odd)').addClass('odd');
      $('#track-list li:nth-child(even)').addClass('even');
      // Show hidden toggles and buttons
      $('#reorder-toggle').show();
      $('.upload-button').show();
      $('#show-form-toggle').show();
      $('.edit-button').show();
      $('.delete-button').show();
      /*
      $('.song-name, .file-name, .artist-name').magicTip({
        refresh: true,
        position: 'bottomLeft'
      });
      $('.song-name, .file-name, .artist-name').each(function(){
        var t = $(this);
        if(t.attr('title') == t.html()){
          t.attr('disableTip', 'true');
        }
      });
      */
    },
    'html'
  );
  TC.song_album.updateChecklist();
};

jQuery(document).ready(function($){
  /* Split Selector Start */
  $('#confirm_splits_dialog').dialog({autoOpen: false, draggable: false, modal: true, resizable: false, width: 400, height: 400, maxWidth: 400, maxHeight: 400});

  $('.split_selector').hover(
    function(){
      var parent = $(this).parents('li');
      parent.css('z-index', 1000);
      parent.find('.split_options_holder').show();
    },
    function(){
      var parent = $(this).parents('li');
      parent.css('z-index', 100);
      parent.find('.split_options_holder').hide();
    }
  );

  $('a.split').click(function(){return false;});

  $('.split_options a').click(
    function(){
      var parent = $(this).parents('li');
      var t = $(this);
      var percentage = t.attr('percentage');
      if(percentage !== 'other') {
        parent.find('.split_options_holder').hide();
        parent.find('.split_selector_value').val(percentage);
        parent.find('.split_percentage').html(percentage+'%');
        parent.find('.split').html(percentage+'%');
      }
      else {
        parent.find('.other_option').show();
        parent.find('.split_options').hide();
        parent.find('.split_selector_value').val('').focus();
      }
      return false;
    }
  );

  $('.split_selector_value').keydown(
    function(e){
      var t = $(this);
      var percentage = t.val();
      if(e.which in {8:'', 13:'', 37:'', 39:'', 46:'', 48:'', 49:'', 50:'', 51:'', 52:'', 53:'', 54:'', 55:'', 56:'', 57:'', 190:''}){
        if(percentage.length > 5){
          percentage = percentage.substr(0, 5);
          t.val(percentage);
        }
        if (e.which === 13 && percentage !== '') {
          var parent = $(this).parents('li');
          if (parseFloat(percentage) > 100) {
            parent.find('.other_option_error').show();
          } else {
            parent.find('.other_option_error').hide();
            parent.find('.other_option').hide();
            parent.find('.split_options').show();
            parent.find('.split_options_holder').hide();
            parent.find('.split').html(percentage+'%');
            parent.find('.split_percentage').html(percentage+'%');
          }
        }
      }
      else{
        return false;
      }
    }
  );

  $('#save_splits').click(function() {
    $('.split_percentage').show();
    $('.split_selector').hide();
    $('#confirm_splits_holder').show();
    $(this).hide();
    return false;
  });

  $('#change_splits').click(function() {
    $('.split_percentage').hide();
    $('.split_selector').show();
    $('#save_splits').show();
    $('#confirm_splits_holder').hide();
    return false;
  });

  $('#confirm_splits').click(function() {
    $('#confirm_splits_dialog').dialog('open');
    return false;
  });

  $('#cancel_confirm_splits').click(function() {
    $('#confirm_splits_dialog').dialog('close');
    return false;
  });

  $('#confirm_splits_again').click(function() {
    $('#confirm_splits_dialog').dialog('close');
    $('#confirm_splits_holder').hide();
    $('#confirm_splits_spinner').show();
    var data = [];
    $('#track-list li').each(function(){
      var t = $(this);
      data.push('{"song_id": "'+t.attr('id')+'", "split_pct": "'+t.find('.split_percentage').html().replace(/%/, '')+'"}');
    });
    $.post('/publishing_splits', {splits: '['+data.join(',')+']'}, function(r) {
      if(r.status > 0) {
        $('#error').dialog('option', 'title', 'Error').html('There has been an error, please refresh the page and try again!').dialog('open');
      }
      else {
        // do something?
        $('#songwriter_service_success').show();
        $('#confirm_splits_spinner').hide();
        window.location.hash='jumptosuccess';
        var redirect = function() {
          window.location.href = '/discography';
        };
        setTimeout(redirect(), 1000);
      }
    });
    return false;
  });

  /* Split Selector End */

  YAHOO.util.Event.onDOMReady(function () {
    YUI().use('uploader', function(Y) {
      Y.type = 'html5';
      Y.Uploader = Y.UploaderHTML5;
      var uploader = new Y.Uploader({appendNewFiles: false, simLimit: 5, multipleFiles:false, withCredentials: false, selectFilesButton: Y.Node.create('<button class=\'yui3-button\' style=\'opacity:0.0;\'>Upload</button>')}).render('#uploader');
      uploader.on('fileselect', onFileSelect);
      var callback = '?callback=?';
      var env = $('#env').attr('env');
      var tc_pid = $('#tc_pid').attr('tc_pid');
      var person_id = $('#person_id').attr('person_id');
      var tunecore_id = $('#tunecore_id').attr('tunecore_id');
      var uploadURL = $('#upload_url').attr('upload_url');
      var registerURL = $('#register_url').attr('register_url');
      var assetsURL = $('#assets_url').attr('assets_url') + callback;
      var elb_name = $('#elb_name').attr('elb_name');
      var currentSongID;
      var currentSongObj;
      var fileList;
      var uploadedList = [];

      init();

  //2012-12-13 AK Return true if the given filename is an mp3, otherwise false
      function is_mp3(filename){
        pattern = /mp3$/i;
        return pattern.test(filename);
      }

      function is_wav(filename){
        pattern = /wav$/i;
        return pattern.test(filename);
      }

      function matchAsset(dur, sr, bd, transactionID, filename) {
        var duration = parseInt(dur, 10);
        var sampleRate = parseInt(sr, 10);
        var bitDepth = parseInt(bd, 10);
        var response = 0;
        var beaconObj = {
          event: 'asset_match_results',
          sample_rate: 'pass',
          bit_depth: 'pass',
          transaction_id: transactionID
        };
        if ($('#ringtone_check').length) {
          beaconObj.duration = 'pass';
          if(duration < 5000 || duration > 30000){
            response = 1;
            beaconObj.duration = 'fail';
          }
        }
        if (sampleRate < 44100) {
          response = 1;
          beaconObj.sample_rate = 'fail';
        }
        if (isNaN(bd)) {
          bitDepth = null;
        }

        if ( !(bitDepth === 16 || is_mp3(filename)) ) {
          if ( !(bitDepth === 24 && is_wav(filename)) ) {
            response = 1;
            beaconObj.bit_depth = 'fail';
          }
        }


        return response;
      }

      function chop(str, num){
        if(str.length > num){
          return str.substr(0, num) + '...';
        }
        else{
          return str;
        }
      }

      function formatDuration(d){
        var duration = Number(d);
        duration = duration / 1000;
        var h = Math.floor(duration / 3600);
        var m = Math.floor(duration % 3600 / 60);
        var s = Math.floor(duration % 3600 % 60);
        return ((h > 0 ? h + ':' : '') + (m > 0 ? (h > 0 && m < 10 ? '0' : '') + m + ':' : '0:') + (s < 10 ? '0' : '') + s);
      }

      function formatSR(d){
        var duration = Number(d);
        duration = duration * 0.001;
        return duration.toString() + 'k';
      }

      function formatBR(d){
        var duration = Number(d);
        duration = Math.floor(duration/1000);
        return duration.toString()+' kbps';
      }

      function splitFileName(name){
        var a = name.split('-');
        a.shift();
        return a.join();
      }

      function formatBeaconStr(str){
        var string = $.trim(str);
        string = string.replace(/\s/g, '_').replace(/"/g, '').replace(/=/g, '_').replace(/[[]/g, '').replace(/#/g, '');
        string = string.substr(0, parseInt( string.length, 10 ) - 1);
        return string;
      }

      function init(){
        var playerVersion = swfobject.getFlashPlayerVersion();
        var output = playerVersion.major + '.' + playerVersion.minor + '.' + playerVersion.release;
        $('#action').dialog({autoOpen: false, draggable: false, modal: true, resizable: false, width: 800, height: 400, maxWidth: 800, maxHeight: 400, zIndex: 10000});
        $('#error').dialog({autoOpen: false, draggable: false, modal: true, resizable: false, width: 400, height: 200, maxWidth: 400, maxHeight: 200, zIndex: 10000});
        $('#assets').dialog({autoOpen: false, draggable: false, modal: true, resizable: false, width: 800, height: 400, maxWidth: 800, maxHeight: 400, zIndex: 10000});
        TC.song_album = new SongAlbum();
        getAssets();
        $('.track-cancelreupload').hide();
      }

      function getAssets(){
      /*
      var uuids = '';
      $('#track-list li').each(function(k,v){
        var t = $(this);
        var uuid = t.attr('uuid');
        if(uuid != undefined){
          uuids += '&uuid[]='+uuid;
        }
      });
      */

        $.getJSON('//'+elb_name+'/bigbox/list/?callback=?', {tc_pid: tc_pid}, function(response){
          if(response.status === 0){
            $(window).data('assets', response);
            var d = response.response;
            var html = '';
            $.each(d, function(i, u){
              html += '<div class="asset"><a href="#" class="play" clip="'+u.streaming_url+'">Play</a><a href="#" class="switch" orig_filename="'+u.orig_filename+'" duration="'+formatDuration(u.duration)+'" bit_rate="'+formatBR(u.bit_rate)+'" sample_rate="'+formatSR(u.sample_rate)+'" key="'+u.key+'" bucket="'+u.bucket+'" uuid="'+u.uuid+'" streaming_url="'+u.streaming_url+'">'+splitFileName(u.orig_filename)+'</a> <span>Duration: '+formatDuration(u.duration)+' Sample Rate: '+formatSR(u.sample_rate)+' Bit Rate: '+formatBR(u.bit_rate)+'</span></div><br />';
              var item = $('.'+u.uuid);
              if(u.streaming_url !== '' && u.streaming_url !== undefined){
                item.find('.track-play').show();
              }
              $.each(u, function(k, v){
                item.attr(k, v);
              });
            });
            $('#assets').html(html);
          } else {
          // error
            $('#error').dialog('option', 'title', 'Upload Error').dialog('open');
          //alert('Oops! There has been an error, please refresh your browser and try again.  If the problem persists please contact support.');
          }
        });
      }

      function onFileSelect(event){
        if('fileList' in event && event.fileList !== null){
          fileList = event.fileList;
          Y.each(fileList, function (fileInstance) {
            fileInstance.on('uploadprogress', onUploadProgress);
            fileInstance.on('uploadcomplete', onUploadResponse);
            fileInstance.on('uploaderror', onUploadError);
            var fileID = fileInstance.get('id');
            var name = fileInstance.get('name');
            var ext = name.slice(-3);
            var it = $('#'+currentNum);
            var perFileVars = {};
            if(ext !== 'wma' && ext !== 'WMA') {
              var inArray = $.inArray(fileID, uploadedList);
              if(inArray <= -1) {
                uploadedList.push(fileID);
                $('#uploader').css('right', 1000);
                it.addClass(fileID);
                it.data('transaction_id', Math.ceil(Math.random()*100000000000));
                it.find('.track-actions').fadeOut();
                it.find('.track-filename').html(name);
                it.find('.track-cancel').attr('file_id', fileID);
                it.find('.track-uploading').show();
                it.find('.track-cancelreupload').hide();
                var theUploadURL = '//'+elb_name+'/uploader/create/';
                uploadedFileList[fileID] = fileInstance;
                uploader.upload(fileInstance, theUploadURL, {tc_pid: tc_pid, packager_class: 'BasicPackager'});
              }
            } else {
              $('#uploader').css('right', 1000);
              //uploader.cancel(fileID);
              it.find('.track-uploading').hide();
              it.find('.track-saving').hide();
              it.find('.track-registering').hide();
              it.find('.track-error').html('<font style="color: red; font-size: 11px;">Sorry, we do not accept .wma files.</font> <a href="#" style="font-size: 11px;" class="track-retry" file_id="'+fileID+'">Please try uploading again.</a>').show();
            }
          });
        }
      }

      function onUploadProgress(event) {
        $('#reorder-toggle').hide();
        var prog = Math.round(100*(parseFloat(event.bytesLoaded)/parseFloat(event.bytesTotal)));
        var item = $('.'+event.currentTarget.get('id'));
        item.find('.track-bar').css('width', prog+'%');
        var one = item.data('one');
        var two = item.data('two');
        var three = item.data('three');
        if ((prog === 75 && three !== true) || (prog === 50 && two !== true) || (prog === 25 && one !== true)){
          if (prog === 75){
            item.data('three', true);
          }
          else if (prog === 50){
            item.data('two', true);
          }
        else if (prog === 25){
          item.data('one', true);
        }
        }
      }

      function onUploadError(event) {
        var item = $('.'+event.currentTarget.get('id'));
        item.find('.track-uploading').hide();
        item.find('.track-saving').hide();
        item.find('.track-registering').hide();
        item.find('.track-error').html('<font style="color: red; font-size: 11px;">'+gon.could_not_upload+'</font> <a href="#" style="font-size: 11px;" class="track-retry" file_id="'+event.currentTarget.get('id')+'">'+gon.try_again+'</a>').show();
      }

      function onUploadResponse(event) {
        var item = $('.'+event.currentTarget.get('id'));
        item.find('.track-saving').show();
        item.find('.track-uploading').hide();
        item.data('one', false);
        item.data('two', false);
        item.data('three', false);
        item.data('four', false);
        item.find('.track-saving').hide();
        item.find('.track-registering').show();
        var d = eval('(' + event.data + ')');
        if(d.status === 0){
          var ma = matchAsset(d.response.duration, d.response.sample_rate, d.response.bit_depth, item.data('transaction_id'),d.response.orig_filename);
          if(ma === 0){
            var uuid = d.response.uuid;
            var song_id = item.attr('id');
            var key = d.response.key;
            var bucket = d.response.bucket;
            $.post(registerURL, {format: 'json', song_id: song_id, 's3_asset[uuid]': uuid, 's3_asset[key]': key, 's3_asset[bucket]': bucket, 'upload[song_id]': song_id, 'upload[orig_filename]': d.response.orig_filename, 'upload[bit_rate]': d.response.bit_rate}, function(response){
            // register complete
              if(response.status === 0){
              //item.find('.tcPlay a').attr('clip', d.response.streaming_url).fadeIn();
                $.each(d.response, function(k, v){
                  item.attr(k, v);
                });
                item.addClass(uuid);
                item.find('.track-registering').hide();
                item.find('.track-actions').show();
                item.find('.track-reupload').show();
                item.find('.track-cancelreupload').hide();
                if(d.response.streaming_url !== '' && d.response.streaming_url !== undefined){
                  item.find('.track-play').show();
                }
                item.find('.upload-button').hide();
                item.find('.choose-button').hide();
                $('#reorder-toggle').show();
                item.find('.file-name').html(chop(splitFileName(d.response.orig_filename), 15)).attr('title', splitFileName(d.response.orig_filename)).show();
                TC.song_album.updateChecklist();
              /*
              $('.song-name, .file-name, .artist-name').magicTip({
                refresh: true,
                position: 'bottomLeft'
              });
              $('.song-name, .file-name, .artist-name').each(function(){
                var t = $(this);
                if(t.attr('title') == t.html()){
                  t.attr('disableTip', 'true');
                }
              });
              */
                $(document).trigger('song_update');
              } else {
              // error
                item.find('.track-registering').hide();
                item.find('.track-error').html('<font style="color: red; font-size: 11px;">'+gon.could_not_upload+'</font> <a href="#" style="font-size: 11px;" class="track-retry" file_id="'+event.currentTarget.get('id')+'">'+gon.try_again+'</a>').show();
              }
            });
          } else {
            $('#uploader').css('right', 2000);
            item.find('.track-registering').hide();
            item.find('.track-error').html('<font style="color: red; font-size: 11px;">Your audio file is not in the format we need.</font> &nbsp; <a href="#" style="font-size: 11px;" class="fixIt" track_number="'+$('.'+event.currentTarget.get('id')).find('.track-number').html()+'" song_name="'+$('.'+event.currentTarget.get('id')).find('.song-name').attr('orig_title')+'" orig_filename="'+d.response.orig_filename+'" duration="'+d.response.duration+'" sample_rate="'+d.response.sample_rate+'" bit_depth="'+d.response.bit_depth+'">See how to fix it.</a> &nbsp; &nbsp; <span style="color: #EEEEEE;">|</span> &nbsp; &nbsp; <a href="#" style="font-size: 11px;" class="track-retry" file_id="'+event.currentTarget.get('id')+'">close (x)</a>').show();
          }
        } else {
        // error
          item.find('.track-registering').hide();
          item.find('.track-error').html('<font style="color: red; font-size: 11px;">'+gon.could_not_upload+'</font> <a href="#" style="font-size: 11px;" class="track-retry" file_id="'+event.currentTarget.get('id')+'">'+gon.try_again+'</a>').show();
        }
      }

      $f('flowplayer', {src :'/javascripts/flowplayer/flowplayer.commercial-3.2.5.swf', wmode: 'transparent'}, {
        key: '#@1c75812e91764348dd1',
        onLoad: function(){
          this.setVolume(100);
        },
        plugins: {
          controls: {
            url: '/javascripts/flowplayer/flowplayer.controls-3.2.3.swf',
            fullscreen: false,
            height: 30,
            autoHide: false,
            play: true,
            volume: false,
            mute: false,
            time: false,
            stop: false,
            buttonColor: '#2bacd7',
            buttonOverColor: '#2bacd7',
            buttonBorderWidth: 0,
            progressColor: '#ace1f3',
            bufferColor: '#FFFFFF',
            sliderColor: '#91d0e5',
            sliderBorderWidth: 1,
            sliderBorderColor: '#dbe8ec',
            tooltipColor: '#2bacd7',
            tooltipTextColor: '#FFFFFF',
            progressGradient: 'none',
            backgroundGradient: 'none',
            backgroundColor: 'transparent'
          }
        },
        canvas: {
          backgroundColor: 'transparent',
          backgroundGradient: 'none'
        },
        clip: {
          autoPlay: false
        }
      });

      $(document).on('click', '.track-cancel', function(){
        var file_id = $(this).attr('file_id');
        var parent = $(this).parent('.track-uploading').parent('li').attr('id');
        var html = 'Are you sure you\'d like to cancel your upload?<br /><br /><a href="#" class="noCancel button" style="color: #FFFFFF">No</a><a href="#" class="yesCancel button" style="color: #FFFFFF" file_id="' + file_id + '" parent="' + parent + '">Yes</a>';
        $('#action').dialog('option', 'title', 'Upload Cancel Confirmation').html(html).dialog('open');
        return false;
      });

      $(document).on('click', '.yesCancel', function(){
        var file_id = $(this).attr('file_id');
        var parent = $('#'+$(this).attr('parent'));
        uploadedFileList[file_id].cancelUpload();
        $('#'+file_id).remove();
        isUploading = false;
        $('#action').dialog('close');
        parent.find('.track-uploading').hide();
        parent.find('.track-actions').show();
        parent.find('.track-play').hide();
        return false;
      });

      $(document).on('click', '.noCancel', function(){
        $('#action').dialog('close');
        return false;
      });

      $(document).on('click', '.track-retry', function(){
        var file_id = $(this).attr('file_id');
        uploadedFileList[file_id].cancelUpload();
        var parent = $(this).parent('.track-error').parent('li');
        parent.find('.track-error').hide();
        parent.find('.track-actions').show();
        parent.find('.track-play').hide();
        parent.find('.upload-button').show();
        parent.find('.choose-button').show();
        return false;
      });

      $(document).on('click', '.track-reupload', function(){
        var parent = $(this).parent('.rollover-hide').parent('.track-actions').parent('li');
        parent.find('.file-name').hide();
        parent.find('.upload-button').show();
        parent.find('.choose-button').show();
        parent.find('.track-reupload').hide();
        parent.find('.track-cancelreupload').show();
        parent.find('.track-play').hide();
        return false;
      });

      $(document).on('click', '.track-cancelreupload', function(){
        var parent = $(this).parent('.rollover-hide').parent('.track-actions').parent('li');
        parent.find('.upload-button').hide();
        parent.find('.choose-button').hide();
        parent.find('.track-reupload').show();
        parent.find('.track-cancelreupload').hide();
        var streaming_url = parent.attr('streaming_url');
        if(streaming_url !== '' && streaming_url !== undefined){
          parent.find('.track-play').show();
        }
        parent.find('.file-name').show();
        $('#uploader').css('right', 1000);
        return false;
      });

      $(document).on('click', '.track-play', function(){
        $('.'+uuidPlaying).find('.track-play').show();
        $('.'+uuidPlaying).find('.track-actions').show();
        var parent = $(this).parent('.track-actions').parent('li');
        parent.find('.track-actions').hide();
        var uuid = parent.attr('uuid');
        uuidPlaying = uuid;
        var clip = parent.attr('streaming_url');
        var listid = parent.attr('id');

        if ($('.flowplayer_holder').length === 0) {
          $('#flowplayer').css('right', 44).css('top', function(){
            var add = parseInt( parent.find('.track-number').html(), 10 ) * 63;
            return add - 45;
          });
          $('#closeFlowPlayer').css('right', 310).css('top', function(){
            var add = parseInt( parent.find('.track-number').html(), 10 ) * 63;
            return add - 45;
          });
        } else {
          $('.flowplayer_holder').css('left', 271).css('top', 18);
          $('#flowplayer').css('left', 0).css('top', 0);
          $('#closeFlowPlayer').css('right', 310).css('top', 18);
        }

        if(currentClip === clip){
          currentClip = '';
          $f('flowplayer').stop();
        }
        else{
          currentClip = clip;
          $f('flowplayer').play(clip);
          $(this).hide();
        }
        return false;
      });

      $(document).on('click', '.play', function(){
        var $t = $(this);
        var clip = $t.attr('clip');
        if(currentClip === clip){
          currentClip = '';
          $f('flowplayer').stop();
          $('.tcSongHolder .tcPlay a, .asset a.play').html('Play');
        }
        else{
          currentClip = clip;
          $f('flowplayer').play(clip);
          $('.tcSongHolder .tcPlay a, .asset a.play').html('Play');
          $t.html('Stop');
        }
        return false;
      });

      $(document).on('click', '#closeFlowPlayer', function(){

        if ($('.flowplayer_holder').length === 0) {
          $('#flowplayer').css('right', 5000);
        } else {
          $('.flowplayer_holder').css('left', 5000);
        }
        $('#closeFlowPlayer').css('right', 5000);
        $f('flowplayer').stop();
        var parent = $('.'+uuidPlaying);
        parent.find('.track-play').show();
        parent.find('.track-actions').show();
        return false;
      });

    /*
    $('.song-name, .file-name, .artist-name').magicTip({
      refresh: true,
      position: 'bottomLeft'
    });

    $('.song-name, .file-name, .artist-name').each(function(){
      var t = $(this);
      if(t.attr('title') == t.html()){
        t.attr('disableTip', 'true');
      }
    });
    */

      $(document).on('click', '.fixIt', function() {
        var t = $(this);
        var track_number = t.attr('track_number');
        var song_name = t.attr('song_name');
        var orig_filename = t.attr('orig_filename');
        var duration = t.attr('duration');
        var sample_rate = t.attr('sample_rate');
        var bit_depth = t.attr('bit_depth');
        if (duration === '' || duration === null || duration === 'null' || duration === undefined || duration === 'undefined') {
          duration = 0;
        }
        if (sample_rate === '' || sample_rate === null || sample_rate === 'null' || sample_rate === undefined || sample_rate === 'undefined') {
          sample_rate = 0;
        }
        if (bit_depth === '' || bit_depth === null || bit_depth === 'null' || bit_depth === undefined || bit_depth === 'undefined') {
          bit_depth = 0;
        }
        getExt = orig_filename.split('.');
        var ext = getExt.pop();
        var goGreen = '/images/green_check.png';
        var goRed = '/images/red_check.png';
        $('#action').dialog('option', 'title', 'Upload Error').html('<div class="fih">'+$('#fixIt').html()+'</div>').dialog('open');
        $('.fih .fih_track_number').html(track_number);
        $('.fih .fih_song_name').html(song_name);
        $('.fih .fih_orig_filename').html(splitFileName(orig_filename));
        $('.fih .fih_duration').html(Math.floor(parseInt( duration, 10 )/1000));
        $('.fih .fih_sample_rate').html(parseInt( sample_rate, 10 ) * 0.001);
        $('.fih .fih_bit_depth').html(bit_depth);
        $('.fih .fih_ext').html(ext);

        if (parseInt( sample_rate, 10 ) === 44100) {
          $('.fih .fih_sample_rate_img').attr('src', goGreen);
        }
        if (ext === 'wav') {
          $('.fih .fih_ext_img').attr('src', goGreen);
        }
        if (parseInt( bit_depth, 10 ) === 16) {
          $('.fih .fih_bit_depth_img').attr('src', goGreen);
        }

        if ($('#ringtone_check').length) {
          $('.fih .isRingtone').show();
          if(duration > 500 && duration < 3000){
            $('.fih .fih_duration_img').attr('src', goGreen);
          }
        }
        return false;
      });

      $(document).on('click', '.choose-button', function(){
        var parent = $(this).parent('.track-actions').parent('li');
        var listid = parent.attr('id');
        currentNum = listid;
        $('#assets').dialog('open');
        return false;
      });

      $(document).on('click', '.asset a.switch', function(){
        var t = $(this);
        var item = $('#'+currentNum);
        var uuid = t.attr('uuid');
        var bucket = t.attr('bucket');
        var key = t.attr('key');
        var filename = t.attr('orig_filename');
        var duration = t.attr('duration');
        var bit_rate = t.attr('bit_rate');
        var sample_rate = t.attr('sample_rate');
        var streaming_url = t.attr('streaming_url');
        var song_id = item.attr('id');
        $('#assets').dialog('close');
        $('#reorder-toggle').hide();
        item.find('.track-registering').show();
        item.find('.track-actions').hide();
        item.find('.track-filename').html(filename);
        item.find('.track-cancelreupload').hide();
        $.post(registerURL, {format: 'json', song_id: song_id, 's3_asset[uuid]': uuid, 's3_asset[key]': key, 's3_asset[bucket]': bucket, 'upload[song_id]': song_id, 'upload[orig_filename]': filename, 'upload[bit_rate]': bit_rate}, function(response){
        // register complete
          if(response.status === 0){
          // fix the variables now on the <li>
            item.addClass(uuid);
            item.attr('uuid', uuid);
            item.attr('orig_filename', filename);
            item.attr('streaming_url', streaming_url);
            item.attr('bit_rate', bit_rate);
            item.attr('sample_rate', sample_rate);
            item.attr('key', key);
            item.attr('duration', duration);
            item.attr('bucket', bucket);
            item.find('.track-registering').hide();
            item.find('.track-actions').show();
            item.find('.track-reupload').show();
            item.find('.track-cancelreupload').hide();
            if(streaming_url !== '' && streaming_url !== undefined){
              item.find('.track-play').show();
            }
            item.find('.upload-button').hide();
            item.find('.choose-button').hide();
            $('#reorder-toggle').show();
            item.find('.file-name').html(chop(splitFileName(filename), 15)).attr('title', splitFileName(filename)).show();
            TC.song_album.updateChecklist();
          } else {
          // error
            item.find('.track-registering').hide();
            item.find('.track-error').html('<font style="color: red; font-size: 11px;">'+gon.could_not_upload+'</font> <a href="#" style="font-size: 11px;" class="track-retry">'+gon.try_again+'</a>').show();
          }
        });
        return false;
      });
    });
  });
});
