jQuery(function($) {
  $(document).on("click", ".favorite_link", function(e) {
    e.preventDefault();
    
    var $$ = $(this);
    if ($$.hasClass("running")) { return; }
    
    var container = $$.parent("td"),
        link = $$.attr("href"),
        songID = link.match(/^.*song_id=(\d+).*$/)[1],
        sound = $('.sm_sound[sm_player-song_id="'+songID+'"]'),
        $$$ = $('.favorite_link[href="'+link+'"]');
    
    if($$.hasClass("favorite")){
      $$$.html('<i class="fa fa-star-o"></i>');
      sound.attr('sm_player-is_favorite', '0');
    } else {
      $$$.html('<i class="fa fa-star"></i>');
      sound.attr('sm_player-is_favorite', '1');
    };
    
    $.ajax({
      type: "POST",
      url: link,
      dataType: "json",
      success: function(json) {
         if(json.favorite == true) $$$.addClass("favorite");
         else $$$.removeClass("favorite");
      },
      error: function() {
        if ($$.hasClass("favorite")) {
          $$$.html('<i class="fa fa-star"></i>');
          sound.attr('sm_player-is_favorite', '1');
        } else {
          $$$.html('<i class="fa fa-star-o"></i>');
          sound.attr('sm_player-is_favorite', '0');
        };
      },
      complete: function() {
        $$.removeClass("running");
      }
    });
    
    $$.addClass("running");
  });
});
