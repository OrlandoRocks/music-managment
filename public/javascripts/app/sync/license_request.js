$(function(){

  $.fn.outerHTML = function(){
    return $(this).clone().wrap('<div></div>').parent().html();
  };

  var TC = window.TC || {};

  TC.SyncLicenseRequestExpandable = function(header, content){
    return this.header = header,
           this.content = content,
           this.bind(),
           this;
  };

  TC.SyncLicenseRequestExpandable.prototype = {

    bind: function(){
      return this.bindClickHeader()
                 .bindTerritorySelect(),
             this;
    },

    bindClickHeader: function(){
      var t = this;
      t.header.on('click', function(e){
        t.clickHeader();
        e.preventDefault();
      });
      return this;
    },

    bindTerritorySelect: function(){
      this.otherTerritory = new TC.FormUtils.selectFieldWithOtherOption({
        selectField: this.content.find('.territory_select'),
        selectFieldTriggerValue: 'Other',
        inputFieldContainer: this.content.find('.territory')
      });
      return this;
    },

    clickHeader: function(){
        (this.header.hasClass('expanded')) ? this.header.removeClass('expanded') : this.header.addClass('expanded');
        return this;
    },

    changeTitle: function(title){
      return this.header.find('a').html(title),
             this;
    }

  };

  TC.SyncLicenseRequest = {

    init: function(){
      return this.initProductionSelect()
                 .initExpandableHTML()
                 .initExpandables()
                 .bind(),
             this;
    },

    bind: function(){
      return this.bindFlowPlayer()
                 .bindSyncTipToggle()
                 .bindUploader()
                 .bindProductionSelect()
                 .bindOtherTypeOfUse()
                 .bindAddAnotherOption(),
             this;
    },

    bindFlowPlayer: function(){
      if($('#flowplayer').length > 0){
        $f("flowplayer", {src :"/javascripts/flowplayer/flowplayer.commercial-3.2.5.swf", wmode: 'transparent'}, {
          key: '#@1c75812e91764348dd1',
          onLoad: function(){
            this.setVolume(100);
          },
          plugins: {
            rtmp: {
              url: "/javascripts/flowplayer/flowplayer.rtmp-3.2.3.swf",
              netConnectionUrl: 'rtmp://cfstreaming.tunecore.com/cfx/st',
            },
            controls: {
              url: '/javascripts/flowplayer/flowplayer.controls-3.2.3.swf',
              fullscreen: false,
              height: 30,
              autoHide: false,
              play: true,
              volume: true,
              mute: false,
              time: true,
              stop: true,
              durationColor: '#F2F2F2',
              buttonColor: '#000000',
              buttonOverColor: '#000000',
              buttonBorder: 'none',
              progressColor: '#F2F2F2',
              bufferColor: '#FFFFFF',
              sliderColor: '#91d2fd',
              sliderBorder: 'none',
              volumeBorder: 'none',
              tooltipColor: '#000000',
              tooltipTextColor: '#FFFFFF',
              progressGradient: 'none',
              backgroundGradient: 'none',
              backgroundColor: 'transparent'
            }
          },
          canvas: {
            backgroundColor: 'transparent',
            backgroundGradient: 'none'
          },
          clip: {
            provider: 'rtmp',
            autoPlay: false
          }
        });
      }
      return this;
    },
    
    bindSyncTipToggle: function(){
      var t = this;
      $('.syncTipToggle').on('click', function(e){
        t.toggleSyncTip();
        e.preventDefault();
      });
      return this;
    },

    bindUploader: function(){
      var btn = $('.fileupload_button'),
          uploader = $('.sync_tip-uploader'),
          content = $('.sync_tip-content'),
          bar = $('.sync_progress-bar'),
          success = $('.upload_success');
      $('#fileupload').fileupload({
        url: $('#fileupload').data('action'),
        sequentialUploads: !1,
        dataType: 'json',
        add: function(e, data){
          $('.sync_tip-filename').html(data.files[0].name);
          btn.hide();
          content.hide();
          success.hide();
          uploader.show();
          bar.show();
          $('#new_sync_license_request').fadeOut(1000);

          data.submit();
        },
        progressall: function(e, data) {
          var progress = parseInt(data.loaded / data.total * 100, 10);
          bar.find('.sync_progress-progress').css('width', progress+'%');
        },
        done: function(e, data){
          if(data.result.status <= 0){
            success.show('slow');
            bar.hide();
            btn.find('strong').html('Re-upload File');
            btn.show();

            if (data.result.upload_url) {
              $('#fileupload').fileupload('option', {
                url: data.result.upload_url,
                dataType: 'json',
                type: 'PUT'
              });
            }
          }
          else{
            // shouldn't get here unless file too big?
            alert('There has been an error, please refresh your browser and try again!');
          }
        }
      });
      return this;
    },

    bindProductionSelect: function(){
      var t = this;
      this.productionSelect.on('change', function(){
        t.changeProductionSelect();
      });
      return this;
    },

    bindOtherTypeOfUse: function(){
      return this.otherTypeOfUse = new TC.FormUtils.selectFieldWithOtherOption({
                   selectField: $('#type_of_use_select'),
                   selectFieldTriggerValue: 'Other',
                   inputFieldContainer: $('#sync_license_request_type_of_use_list_item')
                 }),
             this;
    },

    bindAddAnotherOption: function(){
      var t = this;
      $('.sync_expandables > footer > a').on('click', function(e){
        t.initNewExpandable();
        e.preventDefault();
      });
      return this;
    },

    toggleSyncTip: function(){
      var tip = $('.sync_tip'),
          link = $('.syncTipToggle:first');
      tip.toggle();
      link.toggle();
      return this;
    },

    initExpandableHTML: function(){
      this.expandableHTML = 
      ($('.sync_expandables > header:first').outerHTML() +
      $('.sync_expandables > div:first').outerHTML())
      .replace(/\[0\]/g, '[{{number}}]')
      .replace(/_0_/g, '{{number}}')
      .replace(/value="0"/g, 'value="{{number}}"');
      return this;
    },

    initExpandables: function(){
      this.expandables = [];
      var t = this;
      $('.sync_expandables > header').each(function(){
        var header = $(this),
            content = header.next(),
            expandable = t.createExpandable(header, content);
        t.addExpandable(expandable);
      });
      return this;
    },

    initNewExpandable: function(){
        this.addExpandableToDOM();
        var header = $('.sync_expandables > header:last'),
            content = header.next(),
            expandable = this.createExpandable(header, content);
        this.addExpandable(expandable);
        expandable.changeTitle('Option #'+(this.expandables.length-1));
        return this;
    },

    createExpandable: function(header, content){
      return new TC.SyncLicenseRequestExpandable(header, content);
    },

    addExpandable: function(expandable){
      return this.expandables.push(expandable),
             this;
    },

    addExpandableToDOM: function(){
      var expandableHTML = this.generateExpandableHTML();
      $('.sync_expandables > footer').before(expandableHTML);
      return this;
    },

    generateExpandableHTML: function(){
      return this.expandableHTML.replace(/{{number}}/g, this.expandables.length);
    },

    changeProductionSelect: function(){
      var details = $('#production_details');
      (!this.productionSelect.val()) ? details.show() : details.hide(), this.clearProductionDetails();
      return this;
    },

    initProductionSelect: function(){
      this.productionSelect = $('#sync_license_request_sync_license_production_id');
      return this.changeProductionSelect(),
             this;
    },

    clearProductionDetails: function(){
      $('#production_details').find('input, textarea').val('');
      return this;
    },

    playAudio: function(clip){
      $f('flowplayer').play(clip);
      return this;
    },

    stopAudio: function(){
      $f('flowplayer').stop();
      return this;
    }

  }.init();

  window.TC = TC;

});
