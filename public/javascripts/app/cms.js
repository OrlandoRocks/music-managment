//
//  CMS Form Controller:
//
//  Control UI in the CMS form
//
//  note: select boxes are managed site wide.
//
var CmsForm = function(){
var $ = jQuery;

var self = {
	toggleSubSelect: function(){
		var partner = $('#partner_name').val();
		$('.partner_file').hide();
		$('#'+partner).show('slow');
    },

    init_bind: function(){
      $('#partner_name').bind("change", self.toggleSubSelect);
    },
    
    init: function(){
      self.toggleSubSelect(); //in case a user refreshes their screen
      self.init_bind();
    }
  };

  self.init();
  return self;
}

jQuery(document).ready( function(){
  //new CmsForm();
});
