function init_new_card_form(){
  let scriptTag = $("#payu-cc-js");
  POS.setPublicKey(scriptTag.data('payu-pubkey'));
  const style = {
    base:{
      color: 'black',
      lineHeight: '40px',
      height: 45,
      fontWeight: 800,
      fontSize: '13px'
    }
  };
  POS.setStyle(style);
  POS.setEnvironment(scriptTag.data('payu-env'));
  POS.initSecureFields('card-secure-fields');
}

$(document).ready(function(){
  init_new_card_form();

  $('.credit_card_form').on('submit', function(event){
    if($("#stored_credit_card_payments_os_token").val() == "0"){
      event.preventDefault();
      var additionalData = {
        holder_name: $('#stored_credit_card_first_name').val() + $('#stored_credit_card_last_name').val(),
        billing_address: {phone: $('#stored_credit_card_phone_number').val()}
      }
      POS.createToken(additionalData, result => {
        data = JSON.parse(result)
        $("#stored_credit_card_payments_os_token").val(data.token);
        $(this).submit();
      });
    }
  });
  
});
