// graphs functions for Raphael


var Graph = function(prefs)  {

	var $ = jQuery;
	$("#follower").hide();
	
	var r = Raphael(prefs.id, 240, 170);
	var spinner = r.image("/images/ajax-loader-sm.gif",108,68,24,24);

	function setColors() {
		
		r.g.txtattr.font = "10px 'Helvetica Neue', Helvetica, Arial, sans-serif";
		r.g.txtattr.fill = "White";
		r.g.colors = [];
		if (prefs.hues) { // first bar set is specified by caller
			r.g.colors.push("hsb(" + prefs.hues[0] + "%, " + prefs.hues[1] + "%, " + prefs.hues[2] + "%)");
    	} 
	};
	
	function setCaption(caption) {
		r.g.text(15,15, caption).attr({"text-anchor":"start","font-size":"12px"})
	}

	
	function setBackground() {
		var bg = r.rect(5,25,230,140,10).attr({fill: "90-#000-#fff", opacity: 0, stroke: "#666", "stroke-opacity": 0.5 }).toBack();
		bg.animate({opacity:0.3},1000);	
	};
	
	function setRollover() {
		var c = r.rect(5,25,230,140,10).attr({fill: "#eee",opacity: 0.01}).toFront(); 
		
		follow = $("#help_placard");
		follow.hide();
		follow.css({"background-color":"#ffa"});
		
		if (prefs.type == "TrendSongs") {

			c.click(function() { follow.hide(); window.location="/trend_reports"});
		} else {
			
			c.click(function() { follow.hide(); window.location="/my_account" });
		}
		
		$("#"+prefs.id).mousemove(function(e){
			//c.animate({opacity:0.6},50);	
			follow.html(prefs.helptext)
			var offset = $("#states").offset();
	        follow.show();
			follow.css({
	            top: (e.pageY - offset.top + 10),
	            left: (e.pageX - offset.left + 10)
	        });
	    });
	
		$("#"+prefs.id).mouseout(function(e){
			//c.animate({opacity:0},100);	
			follow.hide();
		});	
		
	};
	
	var self = {

		hbarchart: function(dataset) {
			
			
			setColors();
			setCaption(prefs.caption);
			setBackground();
			
		    label = function() {  // for future use
			
				max_value = Math.max.apply(null, units);
				if (max_value/this.bar.value <= 10) {
					x_offset = this.bar.x-15;
				} else {
					x_offset = this.bar.x+15;
				}
			
				this.bar.insertBefore(r.g.text(x_offset, this.bar.y, this.bar.value).attr({"text-anchor":"right","font-weight": "bold"}));	
			};
			
			var songs=[],units=[];
			
			for (var i = 0, ii = dataset.data.length; i < ii; i++) {		
				units.push(dataset.data[i].total);
				songs.push(dataset.data[i].SongName);
		  	};
		
			var guttervals = ["0%","0%","300%","250%","200%","150%"];
			
			var bars = r.g.hbarchart(15,28,210,140,[units],{type: "round", gutter: guttervals[dataset.data.length]});
			
			for (var i = 1, ii = dataset.data.length; i <= ii; i++) {
				var label_offsets = [0,0,95,105,120,125];  // a variety of offsets to line up the bar graph labels
				iii = label_offsets[dataset.data.length]/ii;
				var song_title = songs[i-1].substr(0,40);
				if (songs[i-1].length > 40) { song_title = song_title+"..."; };
				r.g.text(15,(iii*i)+13,song_title).attr({"text-anchor":"start"});
			}
			
			setRollover();
			spinner.hide();
			
		},
				
		barchart: function(dataset) {
			setColors();
			setBackground();
			setCaption(prefs.caption);
			
			var months=[];
			var years=[];
			var data=[];
			
			for (var i = 0, ii = dataset.data.length; i < ii; i++) {
				
				months.push(dataset.data[i].Month);
				data.push(dataset.data[i][prefs.type]);
				years.push(dataset.data[i].Year);
		  	};
		  
			fin = function () {
				this.flag = r.g.flag(this.bar.x, this.bar.y, this.bar.value || "0", 270).insertBefore(this);
							},
		    fout = function () {
				this.flag.animate({opacity: 0}, 300, function () {this.hide();});
							},
		  	barlabel = function () {	
			
				num = this.bar.value+'';

				if (prefs.type == "Revenue") {
	 
					if (num.length >= 4) {
						for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++) {
							num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3));
						}
					}
				num = "$"+num;
			}
			
			r.g.text(this.bar.x,this.bar.y-8,num);

		  };

		 var bars = r.g.barchart(5,23,230,140, 
				[data],
				{stacked: true, type: "soft"});
		
		    bars.each(barlabel);
			var label_offsets = [0,0,40,30,20,13,10]; 
			for (var i = 1, ii = dataset.data.length; i <= ii; i++) {
				iii = 222/ii;
				r.g.text((iii*i)-label_offsets[dataset.data.length],153,months[i-1]+"-"+years[i-1]); 
				// not crazy about this hack to label the bars with months
			}
			bars.attr({opacity:0});
			bars.animate({opacity:1},500);
			setRollover();
			spinner.hide();
		}
			
	};

return self;
	
};

/*
var init_graph = function(){

	// this is the test data structure as passed to us by the tc.jsonp stuff


				
var dataset1 = {"display_code": 0, "data": [{"total": 282, "SongName": "Passion Play"}, {"total": 112, "SongName": "This is an Incredibly long song title holy bejesus its long"}, {"total": 84, "SongName": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum"}, {"total": 70, "SongName": "My Life Changed balh balshlsa dsfjk sdkjf sdkfjlk"}, {"total": 67, "SongName": "When I Come Home"}]};
				
var dataset2 = {"display_code": 0, "data": [{"Year": "09", "Revenue": 9, "Month": "Nov"}, {"Year": "09", "Revenue": 0, "Month": "Dec"}, {"Year": "10", "Revenue": 7, "Month": "Jan"}, {"Year": "10", "Revenue": 9, "Month": "Feb"}, {"Year": "10", "Revenue": 3, "Month": "Mar"}, {"Year": "10", "Revenue": 7, "Month": "Apr"}]};
						
    var a = new Graph({"id": "placard_1", "hues": [55,60,20], "type": "TrendSongs", "caption":"Songs by Trends","helptext": "the help text is here"}).hbarchart(dataset1);

    var b = new Graph({"id": "placard_2", "hues": [55,60,100], "type": "Revenue", "caption":"Songs by Trends","helptext": "the help text is here for two"}).barchart(dataset2);


};

jQuery(document).ready(init_graph);
*/