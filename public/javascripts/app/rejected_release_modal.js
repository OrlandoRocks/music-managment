$(function () {
  $('.popup-rejection-reason-modal').magnificPopup({
    type: 'inline',
    preloader: false,
    modal: true
  });
  $(document).on('click', '.popup-rejection-reason-modal-dismiss', function (e) {
    e.preventDefault();
    $.magnificPopup.close();
  });
});
