//
//  Function to reload the page when upload is complete
//
var SongUpload = function(options){

  var $ = jQuery;

  var self = {
    init: function(){
      $(document).on("click", ".new-button", self.new_song);
      $(document).on("click", ".save-button", self.submit_song);
      $(document).on("click", ".edit-button", self.edit_song);
      $(document).on("click", ".upload-button", self.upload_song);

      self.init_sortable();
      return self;
    },

    init_sortable: function(){
      $("#track-list").sortable({
        update: self.finished_sort,
        handle: ".handle"
      });
    },

    init_lightbox: function(){
      new Lightbox();
    },

    //
    //  Spinner Controls
    //
    start_spinner: function(){
      $("#spinner").removeClass("hidden");
    },

    stop_spinner: function(){
      $("#spinner").addClass("hidden");
    },

    disable_submit_button: function(){
      $(".save-button").addClass("hidden");
    },

    //
    //  Event Handlers
    //
    new_song: function(){
      var href = $(this).attr("href");
      self.load_new_song_form(href);
      self.start_spinner();
      return false;
    },

    edit_song: function(){
      var href = $(this).attr("href");
      self.load_edit_song_form(href);
      self.back_to_top();
      self.start_spinner();
      return false;
    },

    submit_song: function(){
      var form = $(this).parents("form");
      var href = form.attr("action");
      var data = form.serialize();
      self.start_spinner();
      self.submit_song_form(href,data);
      self.disable_submit_button();
      return false;
    },

    finished_sort: function(){
      var form = $("#track-list").parents("form");
      var href = form.attr("action");
      var data = form.serialize();
      self.save_sort_submit(href,data);
      //return false; Don't return false
    },

    upload_song: function(){
      return false;
    },

    //
    //  Requests
    //

    // Get new song form
    // e.g /albums/song/1231/new
    load_new_song_form: function(href){
      $.get(
        href,
        null,
        self.update_page_with_form,
        "html"
      );
    },

    load_edit_song_form: function(href){
      $.get(
        href,
        null,
        self.update_page_with_form,
        "html"
      );
    },

    // submit song
    submit_song_form: function(href,data){
      $.post(
        href,
        data,
        self.load_post_results,
        "html"
      );
    },

    // get list of songs from the server
    // e.g.  /albums/1231/songs
    get_songs: function(href){
      $.get(
        href,
        null,
        self.update_songs,
        "html"
      );
    },

    save_sort_submit: function(href,data){
      $.post(
        href,
        data,
        self.after_save_sort,
        "html"
      );
    },

    //
    // Response Handlers
    //
    update_page_with_form: function(response){
      $("#song-form").html(response);
      self.init_lightbox();
    },

    load_post_results: function(response){
      self.update_page_with_form(response);
      self.get_songs(self.songs_path());
    },

    update_songs: function(response){
      $("#track-list").html(response);
      self.init_sortable();
      self.init_lightbox();
    },

    after_save_sort: function(response){
      self.update_songs(response);
    },


    //
    //  Helpers
    //
    new_song_form_path: function(){
      return $(".new-song-button").attr("href");
    },

    songs_path: function(){
      return window.location.href;
    },

    back_to_top: function(){
      $("html").animate({
        scrollTop: "0px"
      },1000);
    }

  }
  return self.init();
}

song_uploader = null;
jQuery(document).ready(function(){
  song_uploader = new SongUpload();
});
