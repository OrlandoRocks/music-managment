$(function() {
  $('#rejected-release-more-info').on('click', function() {
    pushEventToDataLayer('Open reason modal', 'More Info');
  });

  $('.rejected-release-corrected-issue').on('click', function() {
    pushEventToDataLayer('Open zendesk ticket', 'I Have Corrected This Issue');
  });

  $("document").on("click", "#rejected-release-corrected-issue", function() {
    pushEventToDataLayer('Open zendesk ticket', 'I Have Corrected This Issue');
  });

  $('#rejected-release-need-help').on('click', function() {
    pushEventToDataLayer('Go to support page', 'I Need More Help');
  });

  $('#rejected-release-dismiss-modal').on('click', function() {
    pushEventToDataLayer('Close popop', 'Got It');
  });

  function pushEventToDataLayer(eventName, eventLabel) {
    dataLayer.push({event: eventName, label: eventLabel, category: "CRT popop"});
  }

});

