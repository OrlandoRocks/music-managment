window.TC = window.TC || {};
TC.YouTubePreferences = function(){
  var clientId = window.gon.youtube_client_id,
      apiKey = window.gon.youtube_api_key,
      scopes = ['https://www.googleapis.com/auth/youtube.readonly'],
      submitBtn = $('#submit'),
      authBtn = $('#authorize-button'),
      logoutBtn = $('.google-logout'),
      authSteps = $('.step'),
      authStep = $('.step.auth'),
      authedStep = $('.step.authed'),
      channel = $('#channel_id'),
      agree = $('#youtube_whitelist_false'),
      disagree = $('#youtube_whitelist_true'),
      disagreeAlert = $('.whitelist-alert'),
      configureAlert = $('.configure-alert'),
      channelDialog = $('#channel-dialog'),
      monetizationDialog = $('#monetization-dialog'),
      exampleDialog = $('#example-dialog'),
      exampleLink = $('.example-dialog'),
      mcn = $('#youtube_mcn'),
      s1 = $('.s1'),
      s2 = $('.s2'),
      s3 = $('.s3'),
      requests = [],
      formatNum = function(n){
          var n = n.split('.'),
              n1 = n[0],
              n2 = (n.length > 1) ? '.' + n[1] : '',
              rgx = /(\d+)(\d{3})/;
          while(rgx.test(n1)){
              n1 = n1.replace(rgx, '$1' + ',' + '$2');
          }
          return n1 + n2;
      },
  self = {

    init: function(){
      return self.bind()
                 .setApiKey()
                 .initChannel(),
             self;
    },

    initChannel: function(){
      var channelID = channel.val();
      (channelID || !1) ? self.requestChannel(channelID).loadYouTubeAPI() :  self.checkAuth(!1);
      return self;
    },

    bind: function(){
      return self.bindClickAuth()
                 .bindClickLogout()
                 .bindClickSubmit()
                 .bindClickExampleLink()
                 .bindDialogs()
                 .bindRadioManager(),
             self;
    },

    bindClickAuth: function(){
      authBtn.on('click', function(e){
        self.checkAuth(!1);
        e.preventDefault();
      });
      return self;
    },

    bindClickLogout: function(){
      logoutBtn.on('click', function(e){
        channel.val('');
        var iframe = document.createElement('IFRAME');
        iframe.onload = function(){
          authedStep.hide();
          authStep.show();
          s1.removeClass('checked');
          iframe.remove();
        };
        iframe.setAttribute('src', 'https://accounts.google.com/logout');
        document.body.appendChild(iframe);
        e.preventDefault();
      });
      return self;
    },

    bindClickSubmit: function(){
      submitBtn.on('click', function(e){
        if(!(channel.val() || !1)){
          e.preventDefault();
          channelDialog.dialog('open');
        }
        else if(!agree.is(':checked') && !disagree.is(':checked')){
          e.preventDefault();
          s2.addClass('error');
          monetizationDialog.dialog('open');
        }
      });
      return self;
    },

    bindClickExampleLink: function(){
      exampleLink.on('click', function(e){
        exampleDialog.dialog('open');
        e.preventDefault();
      });
      return self;
    },

    bindDialogs: function(){
      channelDialog.dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        draggable: false,
        width: 400,
        maxWidth: 400,
        height: 150,
        maxHeight: 150
      });
      monetizationDialog.dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        draggable: false,
        width: 400,
        maxWidth: 400,
        height: 150,
        maxHeight: 150
      });
      exampleDialog.dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        draggable: false,
        width: 600,
        maxWidth: 600,
        height: 400,
        maxHeight: 400
      });
      return self;
    },

    bindRadioManager: function(){
      agree.on('click', function(){
        s2.addClass('checked');
        s2.removeClass('error');
      });
      disagree.on('click', function(){
        s2.addClass('checked');
        s2.removeClass('error');
      });
      mcn.on('click', function(){
        mcn.is(':checked') ? s3.addClass('checked') : s3.removeClass('checked');
      });
      return self;
    },

    setApiKey: function(){
      return gapi.client.setApiKey(apiKey),
             self;
    },

    checkAuth: function(immediate){
      return gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: immediate}, self.handleAuthResult),
             self;
    },

    handleAuthResult: function(result){
      authSteps.hide();
      if(result && !result.error){
        self.requestChannelWhileAuthed();
        if(!self.youTubeAPIExists()) self.loadYouTubeAPI();
        else self.runRequests();
      }
      else authStep.show();
      return self;
    },

    youTubeAPIExists: function(){
      return 'youtube' in gapi.client;
    },

    logApiError: function(response) {
      $.ajax({
        url: "/youtube_preferences/log_api_error",
        type: "get",
        data: {
          response: response
        }
      })
    },

    loadYouTubeAPI: function(){
      gapi.client.load('youtube', 'v3', function() {
        self.runRequests();
      });
      return self;
    },

    request: function(request){
      if(self.youTubeAPIExists()) self.runRequest(request);
      else{
        requests.push(request);
      }
      return self;
    },

    requestChannel: function(channelID){
      self.request(function(){
        var req = gapi.client.youtube.channels.list({id: channelID, part: 'id,snippet,statistics'});
        req.execute(function(r) {
          self.renderChannel(r);
        });
      });
      return self;
    },

    requestChannelWhileAuthed: function(){
      self.request(function(){
        var req = gapi.client.youtube.channels.list({part: 'id,snippet,statistics', mine: !!1});
        req.execute(function(r) {
          self.renderChannel(r);
        });
      });
      return self;
    },

    runRequests: function(){
      $.each(requests, function(){
        self.runRequest(this);
      });
      requests = [];
      return self;
    },

    runRequest: function(request){
      return request.call(),
             self;
    },
    getSubscriberCountFormat: function(stats) {
      if (stats.subscriberCount) {
        return formatNum(stats.subscriberCount)
      } else {
        return "N/A"
      }
    },
    renderChannel: function(response){
      if('items' in response && response.items.length > 0){
        var chan = response.items[0],
            channelID = chan.id,
            snippet = chan.snippet,
            stats = chan.statistics;
        channel.val(channelID);
        authSteps.hide();
        authedStep.find('.title span').html(snippet.title||'Untitled Channel');
        (snippet.description || !1) ? authedStep.find('.content p').html(snippet.description) : authedStep.find('.content p').hide();
        authedStep.find('.thumb').html('<img src="'+snippet.thumbnails.high.url+'">');
        authedStep.find('.stats-video').html(formatNum(stats.videoCount));
        authedStep.find('.stats-subscribers').html(self.getSubscriberCountFormat(stats));
        authedStep.find('.stats-views').html(formatNum(stats.viewCount));
        authedStep.show();
        s1.addClass('checked');
      } else {
        self.logApiError(response);
        authSteps.hide();
        s1.addClass('checked');
        configureAlert.show();
      }
      return self;
    }

  };
  return self;
}();

window.onLoadYouTube = function(){
  TC.YouTubePreferences.init();
};
