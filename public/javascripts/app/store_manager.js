$(function(){

  window.TC = window.TC || {};

  TC.StoreManager = {

    init: function(){
      this.delayHideAlert();
      var data = JSON.parse($('#data').html());
      this.container = $('.store_manager');
      this.uncheckAllByDefault = gon.uncheck_all
      this.uncheckStoresByDefault = gon.disable_expander
      this.allChecks = this.container.find(':checkbox').not('#selectAllFutureCheck');
      this.selectAllCheck = $('#selectAllCheck');
      this.selectStoreCheck = '.selectStoreCheck';
      this.selectReleaseCheck = '.selectReleaseCheck';
      this.selectReleaseForAllLink = '#selectReleaseForAllLink';
      this.selectReleaseLink = '.selectReleaseLink';
      this.releaseDialog = $('#releaseDialog');
      this.releaseDialogSelectAll = $('.release_dialog-select_all');
      this.releaseDialogUnselectAll = $('.release_dialog-unselect_all');
      this.closeReleaseDialogLink = $('.closeReleaseDialog');
      this.releaseTable = $('.table-releases');
      this.subtotalContainer = $('.store_manager-subtotal_price');
      this.totalContainer = $('.store_manager-total_price');
      this.hiddenInput = $('#checkoutData');
      this.form = $('#storeManagerForm');
      this.checkoutButton = $('#checkoutButton');
      this.toWhere = $('.to-where');
      this.everyStore = false;
      this.releaseIDs = [];
      this.releases = [];
      this.stores = data.stores;
      this.future = data.future||[];
      this.freeTranslated = data.free;
      this.release_meta_data = data.release_meta_data;
      this.currency = data.currency;
      
      return this.initData()
                 .bind()
                 .setupApple(),
             this;
    },

    initData: function(){
      var t = this;
      this.initAutomator()
      $.each(this.stores, function(){
        var store = this;
        store.total = store.missing_releases.length * store.upgrade_cost;
        store.enabledReleases = store.missing_releases.length;
        store.element = $('[store_id="'+store.id+'"]');
        t.setInitialCheck(store)
        $.each(store.missing_releases, function(i,v){
          var release = store.missing_releases[i] = {
            id: v,
            enabled: true,
            metadata: t.release_meta_data[v]
          };
          if(store.id != 'future' && $.inArray(release.id, t.releaseIDs) < 0){
            t.releaseIDs.push(release.id);
            t.releases.push($.extend(true, {}, release));
          }
        });
      });
      return this;
    },

    initAutomator: function() {
      if (gon.disable_automator) return

      this.stores.push({
        id: 'future',
        name: 'future stores',
        upgrade_cost: this.future.price||0,
        missing_releases: this.future.missing_releases||[],
        currency_symbol: this.future.currency_symbol
      });
    },

    setInitialCheck: function(store) {
      switch (true) {
        case this.uncheckAllByDefault:
          this.disableStore(store)
          break
        case this.uncheckStoresByDefault:
          if (store.id === 'future') {
            this.enableStore(store)
          } else {
            this.disableStore(store)
          }
          break
        default:
          this.enableStore(store)
          break
      }
    },

    selectEverything: function(enable) {
      var t = this;
      const stores = gon.disable_automator
        ? this.stores 
        : this.stores.slice(0,-1)
      $.each(stores, function(){
        var store = this;
        store.total = store.missing_releases.length * store.upgrade_cost;
        store.enabledReleases = store.missing_releases.length;
        enable ? t.enableStore(store) : t.disableStore(store);
        $.each(store.missing_releases, function(i,v){
          v.enabled = enable;
        });
      });
      $.each(this.releases, function(){
        this.enabled = enable;
      });
      return this;
    },

    selectAll: function(){
      this.selectEverything(true);
    },

    unselectAll: function(){
      this.selectEverything(false);
    },

    updateTotals: function(){
      var subtotal = 0,
        future;
      $.each(this.stores, function(){
        if(this.enabled && this.id != 'future') subtotal = subtotal + this.total;
        else if(this.id == 'future') future = this;
      });
      $.get('/price_formats/format_price', {price: subtotal * 100, currency: this.currency, div: '.store_manager-subtotal_price'});
      var total = subtotal;
      if(future && future.enabled) total += future.total;
      $.get('/price_formats/format_price', {price: total * 100, currency: this.currency, div: '.store_manager-total_price'});
      return this;
    },

    updateStore: function(store){
      store.total = store.enabledReleases * store.upgrade_cost;
      if(store.total <= 0 && store.upgrade_cost === 0) store.element.find('.store_manager-store_total_price').html(this.freeTranslated);
      else {
        var price, releaseCount, releaseText;
        if (store.enabled) {
          price = store.total * 100;
          releaseCount = store.enabledReleases;
          releaseText = (store.enabledReleases === 1) ? 'release' : 'releases';
        } else {
          price = 0;
          releaseCount = 0;
          releaseText = 'releases';
        }
        $.get('/price_formats/format_price', {price: price, currency: this.currency, div: '#' + store.element.find('.store_manager-store_total_price').attr('id')});
        store.element.find('.store_manager-store_release_count').html(releaseCount);
        store.element.find('.store_manager-release_txt').html(releaseText);
      }
      return this.updateTotals(),
             this;
    },

    updateAllStores: function(){
      var t = this;
      $.each(this.stores, function(){
        t.updateStore(this);
      });
      return this;
    },

    updateSelectAll: function(){
      var selectAll = true;
      $.each(this.stores, function(){
        if(this.id != 'future' && ((this.enabledReleases != this.missing_releases.length) || !this.enabled)) selectAll = false;
      });
      return this.selectAllCheck.prop('checked', selectAll),
             this;
    },

    addCommas: function(o){
      var o = o.split('.'),
        x1 = o[0],
        x2 = (o.length > 1) ? '.' + o[1] : '',
        rgx = /(\d+)(\d{3})/;
      while(rgx.test(x1)){
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
      }
      return x1 + x2;
    },

    preciseRound: function(num, decimals){
      return Math.round(num*Math.pow(10,decimals))/Math.pow(10,decimals);
    },

    formatNumber: function(num){
      return this.addCommas(this.preciseRound(num, 2).toFixed(2));
    },

    getStoreIDFromParent: function(element){
      return element.parents('[store_id]:first').attr('store_id');
    },

    onSelectAllCheck: function(){
      var checked = this.selectAllCheck.is(':checked');
      this.selectEverything(checked);
      this.updateTotals()
          .updateAllStores();
      this.allChecks.prop('checked', checked);

      return this;
    },

    onSelectStoreCheck: function(check){
      var storeID = this.getStoreIDFromParent(check),
        store = this.findStoreByID(storeID);
      (check.is(':checked')) ? this.enableStore(store) : this.disableStore(store);
      return this.updateStore(store)
                 .updateSelectAll(),
             this;
    },

    onSelectReleaseCheck: function(check){
      var t = this,
        releaseID = check.attr('release_id'),
        isChecked = check.is(':checked');
      if(this.everyStore){
        $.each(this.releases, function(){
          if(this.id == releaseID) this.enabled = isChecked;
        });
        $.each(this.stores, function(){
          if(this.id != 'future'){
            var store = this,
              release = t.findReleaseByIDFromStore(store, releaseID);
            if(!!release && release.enabled != isChecked){
              release.enabled = isChecked;
              store.enabledReleases += (isChecked) ? 1 : -1;
              t.updateStore(store);
            }
          }
        });
      }
      else{
        var release = this.findReleaseByIDFromCurrentStore(releaseID);
        release.enabled = isChecked;
        this.currentStore.enabledReleases += (isChecked) ? 1 : -1;
        this.updateStore(this.currentStore);
        var allOfThisTypeAreChecked = true;
        $.each(this.stores, function(){
          if(this.id != 'future'){
            var release = t.findReleaseByIDFromStore(this, releaseID);
            if(release && !release.enabled) allOfThisTypeAreChecked = false;
          }
        });
        if(allOfThisTypeAreChecked){
          $.each(this.releases, function(){
            if(this.id != 'future' && this.id == releaseID) this.enabled = true;
          });
        }
      }
      return this.updateSelectAll(),
             this;
    },

    findReleaseByIDFromStore: function(store, releaseID){
      var release;
      $.each(store.missing_releases, function(){
        if(this.id == releaseID) return release = this, false;
      });
      return release;
    },

    findReleaseByIDFromCurrentStore: function(id){
      return this.findReleaseByIDFromStore(this.currentStore, id);
    },

    onClickSelectReleaseLink: function(link){
      this.everyStore = false;
      var storeID = this.getStoreIDFromParent(link);
      this.currentStore = this.findStoreByID(storeID);
      this.toWhere.html(this.currentStore.name);
      return this.renderSelectReleaseTable(this.currentStore.missing_releases)
                 .openReleaseDialog(),
             this;
    },

    onClickSelectReleaseForAllLink: function(){
      if (gon.disable_expander) return

      this.everyStore = true;
      this.toWhere.html(gon.all_stores);
      return this.renderSelectReleaseTable(this.releases)
                 .openReleaseDialog(),
             this;
    },

    renderSelectReleaseTable: function(releases){
      var html = '';
      $.each(releases, function(){
        html += '<tr><td>'+this.metadata.name+'</td><td>'+this.metadata.release_type+'</td><td>'+this.metadata.upc+'</td><td><input class="selectReleaseCheck" type="checkbox" '+((this.enabled) ? 'checked="checked"' : '')+' release_id="'+this.id+'"></td></tr>';
      });
      this.releaseTable.find('tbody').html(html);
      return this;
    },

    enableStore: function(store){
      store.enabled = true;
      store.element.removeClass('disabled');
      this.updateMissingReleaseStatus(store);
      return this;
    },

    disableStore: function(store){
      store.enabled = false;
      store.element.addClass('disabled');
      this.updateMissingReleaseStatus(store);
      return this;
    },

    updateMissingReleaseStatus: function(store){
      $.each(store.missing_releases, function(){
        this.enabled = store.enabled;
      });
      return this;
    },

    findStoreByID: function(id){
      var store;
      $.each(this.stores, function(){
        if(this.id == id) return store = this, false;
      });
      return store;
    },

    openReleaseDialog: function(){
      this.releaseTable.trigger('update');
      $.magnificPopup.open({
        items: { src: this.releaseDialog, type: 'inline' }
      });
      return this;
    },

    closeReleaseDialog: function(){
      $.magnificPopup.close();
      return this;
    },

    selectAllInReleaseDialog: function(){
      return this.releaseDialog.find('input:checkbox:not(:checked)').click(),
             this;
    },

    unselectAllInReleaseDialog: function(){
      return this.releaseDialog.find('input:checkbox:checked').click(),
             this;
    },

    checkout: function(){
      var data = this.processData();
      this.hiddenInput.val(data);
      this.form.submit();
      return this;
    },

    processData: function(){
      var data = {
        stores: {},
        future: []
      };
      $.each(this.stores, function(){
        var store = this;
        if(store.enabled){
          data.stores[store.id] = [];
          $.each(store.missing_releases, function(){
            if(this.enabled) data.stores[store.id].push(this.id);
          });
          if(data.stores[store.id].length <= 0) delete data.stores[store.id];
        }
      });
      data.future = data.stores.future || [];
      delete data.stores.future;
      if(data.future.length <= 0) delete data.future;
      return JSON.stringify(data);
    },

    bind: function(){
      return this.bindSelectAllCheck()
                 .bindSelectStoreCheck()
                 .bindSelectReleaseCheck()
                 .bindClickSelectReleaseLink()
                 .bindClickSelectReleaseForAllLink()
                 .bindSelectReleaseDialog()
                 .bindReleaseDialogSelectAll()
                 .bindReleaseDialogUnselectAll()
                 .bindClickCloseReleaseDialogLink()
                 .bindClickCheckoutButton()
                 .bindTableSorter(),
             this;
    },

    bindSelectAllCheck: function(){
      var t = this;
      this.container.on('click', '#selectAllCheck', function(){
        t.onSelectAllCheck();
      });
      return this;
    },

    bindSelectStoreCheck: function(){
      var t = this;
      this.container.on('click', this.selectStoreCheck, function(){
        t.onSelectStoreCheck($(this));
      });
      return this;
    },

    bindSelectReleaseCheck: function(){
      var t = this;
      $('body').on('change', this.selectReleaseCheck, function(){
        t.onSelectReleaseCheck($(this));
      });
      return this;
    },

    bindClickSelectReleaseLink: function(){
      var t = this;
      this.container.on('click', this.selectReleaseLink, function(e){
        t.onClickSelectReleaseLink($(this));
        e.preventDefault();
      });
      return this;
    },

    bindClickSelectReleaseForAllLink: function(){
      var t = this;
      this.container.on('click', this.selectReleaseForAllLink, function(e){
        t.onClickSelectReleaseForAllLink();
        e.preventDefault();
      });
      return this;
    },

    bindSelectReleaseDialog: function(){
      this.releaseDialog.dialog({
        autoOpen: false,
        draggable: false,
        modal: true,
        resizable: false,
        width: 600,
        height: 400,
        closeOnEscape: false,
        open: function(event, ui){
          $('#ui-dialog-title-releaseDialog').next().hide();
        }
      });
      return this;
    },

    bindReleaseDialogSelectAll: function(){
      var t = this;
      this.releaseDialogSelectAll.on('click', function(e){
        t.selectAllInReleaseDialog();
        e.preventDefault();
      });
      return this;
    },

    bindReleaseDialogUnselectAll: function(){
      var t = this;
      this.releaseDialogUnselectAll.on('click', function(e){
        t.unselectAllInReleaseDialog();
        e.preventDefault();
      });
      return this;
    },

    bindClickCloseReleaseDialogLink: function(){
      var t = this;
      this.closeReleaseDialogLink.on('click', function(e){
        t.closeReleaseDialog();
        e.preventDefault();
      });
      return this;
    },

    bindClickCheckoutButton: function(){
      var t = this;
      this.checkoutButton.on('click', function(e){
        $(this).hide('slow');
        t.checkout();
        e.preventDefault();
      });
      return this;
    },

    bindTableSorter: function(){
      this.releaseTable.tablesorter({sortList: [[0,0]], widgets: ['filter']});
      return this;
    },

    delayHideAlert: function(){
      setTimeout(function() {
        $('.message').fadeOut(2000);
      }, 5000);
    },

    setupApple: function(){
      var appleAlbums = $('#apple-albums'),
        appleConfirm = $('#apple-confirm'),
        appleForm = $('#apple-form'),
        countSpan = $('.apple-count'),
        count = Number(countSpan.html()),
        releaseText = $('.apple-release-text');

      $('#apple-select').on('click', function(e) {
        e.preventDefault();
        $.magnificPopup.open({
          items: { src: appleAlbums, type: 'inline' }
        });
      });

      $('#apple-table').tablesorter({sortList: [[0,0]], widgets: ['filter']});

      $('#apple-select-all').on('click', function(e) {
        e.preventDefault();
        appleAlbums.find('input:checkbox:not(:checked)').click();
      });

      $('#apple-unselect-all').on('click', function(e) {
        e.preventDefault();
        appleAlbums.find('input:checkbox::checked').click();
      });

      $('.apple-box').on('change', function() {
        if(this.checked) {
          count = count + 1;
        } else {
          count = count - 1;
        }

        var word = 'releases';
        if(count === 1) { word = 'release'; }
        countSpan.html(count);
        releaseText.html(word);
      });

      $('#apple-submit').on('click', function(e) {
        var modal = appleConfirm;
        if (count === 0) {
          modal = $('#no-apple-albums');
        }
        e.preventDefault();
        $.magnificPopup.open({
          items: { src: modal, type: 'inline' }
        });
      });

      $('#apple-send').on('click', function(e) {
        e.preventDefault();
        appleForm.submit();
      });
    }

  }.init();

});