$(function(){

  var TCPreorder = function(){
    var data, tiers,
        albumId = $('#album_id').val(),
        form = $('#preorder-form'),
        submitButton = $('.preorder-submit'),
        savedMsg = $('.saved-msg'),
        preorderChecklistStatus = $('.status.preorder-checklist'),
        preorderRadioButtons = $('.preorder-radio'),
        preorderContent = $('.preorder-content-holder'),
        preorderSections = $('.preorder-option-content'),
        preorderRequiredSections = $('.preorder-option-content.required-content'),
        preorderHeaders = $('.preorder-option-header'),
        preorderHeaderLinks = $('.preorder-option-header .link'),
        preorderStartDate = $('#preorder_start_date'),
        previewCheckbox = $('#preview_checkbox'),
        startDateOption = $('#start_date_option'),
        preorderOption = $('#main_options'),
        iTunesOption = $('#itunes_only_options'),
        previewOption = $('#preview_option'),
        gratOption = $('#gratification_option'),
        pricingOption = $('#pricing_option'),
        gratHolder = $('.grat-songs'),
        preorderPriceSelect = $('#preorder-price-dropdown'),
        trackPriceSelect = $('#track-price-dropdown'),
        albumPriceDisplay = $('#album-price'),
        preorderAddOnBox = $('.preorder-addon'),
        pricingWarning = $('.pricing-warning'),
        pricingFields = $('.pricing-fields'),
        pricingFixed = $('.notice.preorder'),
        maxTrackPrice = 15.99,
        date_format = "",
        currency = "",
        currencySymbol = "",
        isShowingIsoCode = false,
        isSingle = ($("#song-form").length === 0 ? true : false),
        self = {
          init: function(){
            $.get( "/albums/" + albumId + "/get_preorder_data", function(d) {
              data = d;
              date_format = d.date_format;
              tiers = d.preorder_prices;
              currency = d.currency;
              currencySymbol = d.currency_symbol;
              isShowingIsoCode = d.show_iso_code;
              if(submitButton.length > 0){
                return self.checkboxCacheDestroyer()
                           .disabledChecker()
                           .toShowPricing()
                           .renderGratCheckboxes(data.songs, data.grat_songs) // doing this because of checkbox cache issues
                           .overridePrices(data.track_price, data.preorder_price)
                           .gratCheckboxChanged(true)
                           .bind(),
                       self;
              }
              else return self;
            });
          },
          checkboxCacheDestroyer: function(){
            if(data.itunes_enabled && data.google_enabled) {
              preorderRadioButtons.filter("[value='all']").prop('checked', true).parent("label").addClass("active");
            } else if (data.itunes_enabled) {
              preorderRadioButtons.filter("[value='itunes']").prop('checked', true).parent("label").addClass("active");
            } else if (data.google_enabled) {
              preorderRadioButtons.filter("[value='google']").prop('checked', true).parent("label").addClass("active");
            } else {
              preorderRadioButtons.filter("[value='none']").prop('checked', true).parent("label").addClass("active");
            }
            previewCheckbox.prop('checked', data.track_preview);
            return self;
          },
          disabledChecker: function(){
            preorderRadioButtons.removeClass("disabled").filter(":disabled").parent("label").addClass("disabled");
            return self;
          },
          toShowPricing: function(){
            if(isSingle){
              pricingWarning.hide();
              if($('.song_item_for_preorder').attr('duration') > 600000){
                pricingFields.hide();
                pricingFixed.show();
              } else {
                pricingFixed.hide();
                pricingFields.show();
              }
            } else if($('.song_item_for_preorder').length > 0){
              pricingWarning.hide();
              pricingFields.show();
            }
            return self;
          },
          renderGratCheckboxes: function(songs, gratTracks){
            // todo: Need to refactor/replace
            var text = {
              en: {
                no_tracks: "You currently have no tracks.",
                add_more_tracks: "Add more tracks to set up Instant Gratification."
              },
              de: {
                no_tracks: "Es sind noch keine Titel hinterlegt.",
                add_more_tracks: "Füge weitere Titel hinzu um die Instant Gratification einzurichten."
              },
              es: {
                no_tracks: "Actualmente no tienes canciones.",
                add_more_tracks: "Agrega más canciones para configurar una gratificación instantánea."
              },
              fr: {
                no_tracks: "Vous n’avez pas de titres actuellement.",
                add_more_tracks: "Ajoutez d’autres titres pour mettre en place l’Instant Gratification."
              },
              it: {
                no_tracks: "Non si dispone di titoli ora.",
                add_more_tracks: "Aggiungere altri titoli per implementare la gratificazione immediata."
              },
              pt: {
                no_tracks: "Você não possui faixas no momento.",
                add_more_tracks: "Adicione mais faixas para configurar a gratificação instantânea."
              }
            };

            no_tracks       = text[gon.locale].no_tracks || text["en"].no_tracks
            add_more_tracks = text[gon.locale].add_more_tracks || text["en"].add_more_tracks

            var html = '';
            if(songs.length <= 0) html += '<p style="margin-bottom: 15px;">' + no_tracks + '</p>';
            else if(songs.length <= 3) html += '<p style="margin-bottom: 15px;">' + add_more_tracks + '</p>';
            else{
              $.each(songs, function(i,s){
                var checked = ($.inArray(s.id, gratTracks) > -1) ? 'checked' : '';
                html += '<div class="grat-track"><input type="checkbox" class="grat_checkbox" autocomplete="off" value="'+s.id+'" '+checked+'><label><strong>'+(i+1)+'. '+s.name+'</strong> by '+s.artist+'</label></div>';
              });
            }
            gratHolder.html(html);
            return self;
          },
          refreshGratCheckboxes: function(songs){
            var songs      = songs || self.collectSongData();
            var gratTracks = self.collectGratTracks();
            return self.renderGratCheckboxes(songs, gratTracks)
                       .gratCheckboxChanged(),
                   self;
          },
          collectSongData: function(){
            var songs = [];
            $('.song_item_for_preorder').each(function(){
              var t = $(this);
              songs.push({id: t.attr('id'), name: t.find('.song-name').attr('title'), artist: t.find('.artist-name').attr('title')});
            });
            return songs;
          },
          overridePrices: function(tPrice, pPrice){
            var tPrice = ''+tPrice,
                pPrice = ''+pPrice;
            if(tPrice || false) trackPriceSelect.val(tPrice);
            self.updatePreorderDropdown();
            if(pPrice || false) preorderPriceSelect.val(pPrice);
            return self;
          },
          activatePreorder: function(radioChecked){
            preorderChecklistStatus.find('.fa-circle-o').removeClass('fa-circle-o').addClass('fa-check-circle-o');
            if (!radioChecked) {
              preorderRadioButtons.filter('[value="all"]').prop('checked', true);
            }

            if (preorderRadioButtons.filter('[value="google"]:checked').length) {
              preorderOption.addClass('active');
              iTunesOption.removeClass('active');
              self.uncheckHeader(iTunesOption);
            } else {
              preorderOption.addClass('active');
              iTunesOption.addClass('active');
              self.checkHeader(iTunesOption);
            }
            self.updateAddOnText();
            preorderAddOnBox.show();
            self.expandRequired()
                .checkHeader(startDateOption);
            return self;
          },
          disablePreorder: function(){
            preorderChecklistStatus.find('.fa-check-circle-o').removeClass('fa-check-circle-o').addClass('fa-circle-o');
            preorderRadioButtons.filter('[value="none"]').prop('checked', true);
            preorderOption.removeClass('active');
            iTunesOption.removeClass('active');
            self.uncheckHeader(iTunesOption);
            preorderAddOnBox.hide();
            self.collapseAll();
            return self;
          },
          updateAddOnText: function(){
            var selected = preorderRadioButtons.filter(":checked"),
                price = preorderRadioButtons.filter(":checked").data("price"),
                type = preorderRadioButtons.filter(":checked").val(),
                label, help;

            if (type === "all") {
              label = "iTunes &amp; Google Play Pre-order - " + price;
              help = "Set up a pre-order period on iTunes &amp; Google Play.";
            } else if (type === "itunes") {
              label = "iTunes Pre-order - " + price;
              help = "Set up a pre-order period on iTunes.";
            } else if (type === "google") {
              label = "Google Play Pre-order - " + price;
              help = "Set up a pre-order period on Google Play.";
            }

            preorderAddOnBox.find(".addon-label").html(label);
            preorderAddOnBox.find(".addon-helptext").html(help);
          },
          collapseAll: function(){
            preorderSections.hide();
            return self;
          },
          expandAll: function(){
            preorderSections.show();
            return self;
          },
          expandRequired: function(){
            preorderRequiredSections.show();
            return self;
          },
          toggleSection: function(sectionHeader){
            if($(sectionHeader).parents('.preorder-options').hasClass('active')){
              var section = sectionHeader.next(),
                  headerLink = sectionHeader.find('.link');
              if(!section.hasClass('required-content')){
                if(section.is(':visible')){
                  section.hide();
                  headerLink.html(headerLink.html().replace(/Hide/, 'Show'));
                }
                else{
                  section.show();
                  headerLink.html(headerLink.html().replace(/Show/, 'Hide'));
                }
              }
            }
            return self;
          },
          startDateChanged: function(){
            var date = preorderStartDate.val();
            self.checkHeader(startDateOption);
            if(!(date || false) || date == 'unknown') return preorderStartDate.val(data.min_date), self;
            return self.submit(true),
                   self;
          },
          preorderRadioButtonsChanged: function(){
            preorderRadioButtons.filter('[value="none"]:checked').length ? self.disablePreorder() : self.activatePreorder(true);
            preorderRadioButtons.parent("label").removeClass("active");
            preorderRadioButtons.filter(':checked').parent("label").addClass("active");
            return self.submit(true),
                   self;
          },
          previewCheckboxChanged: function(){
            previewCheckbox.is(':checked') ? self.checkHeader(previewOption) : self.uncheckHeader(previewOption);
            return self.submit(true),
                   self;
          },
          gratCheckboxChanged: function(skipSubmit){
            var gratTotal = self.gratCount(),
                trackCount = self.trackCount(),
                gratAllowance = (trackCount % 2) ? (trackCount-1)/2 : trackCount/2,
                maxGratAllowance = self.calculateMaxGratTracks();
                option69 = trackPriceSelect.find("option[value='0.69']"),
                option99 = trackPriceSelect.find("option[value='0.99']");

            (gratTotal >= Math.min(gratAllowance, maxGratAllowance) || (trackCount == 4 && gratTotal == 1 && trackPriceSelect.val() == '0.99')) ?
                $('.grat_checkbox').not(':checked').attr('disabled', true)
              : $('.grat_checkbox').attr('disabled', false);

            self.disableTrackPriceTier(trackCount, gratTotal);

            if(gratTotal > gratAllowance) $('.grat_checkbox:checked:last').prop('checked', false).attr('disabled', true);
            self.setTrackPrice(trackCount, gratTotal);

            gratTotal > 0 ? self.checkHeader(gratOption) : self.uncheckHeader(gratOption);
            if(!skipSubmit) self.submit(true);
            return self.updatePreorderDropdown()
                       .updateSalePrices(),
                   self;
          },
          preorderPriceChanged: function(){
            return self.checkHeader(pricingOption)
                       .updateSalePrices()
                       .submit(true),
                   self;
          },
          trackPriceChanged: function(){
            return self.checkHeader(pricingOption)
                       .gratCheckboxChanged()
                       .submit(true),
                   self;
          },
          songChanged: function(songs){
            return self.refreshGratCheckboxes(songs)
                       .toShowPricing(),
                   self;
          },
          updateSalePrices: function(){
            if (currency == "EUR") {
              var trackCount = self.trackCount(),
                preorderPrice = self.sp(preorderPriceSelect.val()),
                price = (trackCount > 10) ? ((preorderPrice > 4.90) ? preorderPrice : 4.90) : self.calculateMaxPrice();
                convertedPrice = self.findConvertedPrice(price.toFixed(2));
                if (parseFloat(convertedPrice) > 8.70) convertedPrice = '8.70';
                if (isNaN(convertedPrice)) {
                  convertedPrice = '4.90';
                }
              return albumPriceDisplay.html(self.formatEuroPriceString(convertedPrice) + ' <span class="price-unit">' + currency + '</span>'),
              self;
            } else {
              var trackCount = self.trackCount(),
                preorderPrice = self.sp(preorderPriceSelect.val()),
                price = (trackCount > 10) ? ((preorderPrice > 10) ? preorderPrice : 9.99) : self.calculateMaxPrice();
              return albumPriceDisplay.html(currencySymbol + self.findConvertedPrice(price.toFixed(2)) + ' <span class="price-unit">' + currency + '</span>'),
              self;
            }

          },
          updatePreorderDropdown: function(){
            var gratTotal = self.gratCount(),
                finalTiers = [],
                sumPrice = self.calculateSumPrice(),
                minPrice = self.calculateMinPrice(),
                maxPrice = self.calculateMaxPrice(),
                currencyIso = "";
            $.each(tiers, function(i,v){
              if(v[1] >= minPrice && v[1] <= maxPrice) finalTiers.push(v);
            });


            if (isShowingIsoCode) {
              currencyIso = " " + currency;
            }

            if(gratTotal <= 0 && sumPrice >= minPrice && sumPrice <= maxPrice) {
              if (currency == "EUR") {
                var convertedPrice = self.findConvertedPrice(sumPrice);
                if (parseFloat(convertedPrice) > 8.70) convertedPrice = '8.70';
                finalTiers.push([ self.formatEuroPriceString(convertedPrice) + currencyIso, sumPrice]);
              } else {
                finalTiers.push([currencySymbol + self.findConvertedPrice(sumPrice) + currencyIso, sumPrice]);
              }
            }
            finalTiers.sort(function(a, b){
              return a[1] - b[1];
            });
            var options = '',
                currentPrice = self.sp(preorderPriceSelect.val()) || finalTiers[finalTiers.length - 1][1];
            $.each(finalTiers, function(i,v){
              options += '<option value="'+v[1]+'" '+(v[1] == currentPrice ? 'selected' : '')+'>'+v[0]+'</option>';
            });
            preorderPriceSelect.empty().html(options);
            return self;
          },
          checkHeader: function(option){
            option.find('.fa-circle-o').first().removeClass('fa-circle-o').addClass('fa-check-circle-o');
            return self;
          },
          uncheckHeader: function(option){
            option.find('.fa-check-circle-o').first().removeClass('fa-check-circle-o').addClass('fa-circle-o');
            return self;
          },
          collectData: function(){
            return {
              all: {
                enabled: preorderRadioButtons.filter(":checked").val(),
                start_date: preorderStartDate.val(),
                preview_songs: previewCheckbox.is(':checked')
              },
              itunes: {
                album_price: preorderPriceSelect.val(),
                track_price: trackPriceSelect.val(),
                gratification_tracks: self.collectGratTracks()
              }
            };
          },
          submit: function(auto){
            var auto = auto || false;
            if(!auto){
              submitButton.blur().hide();
              savedMsg.show();
            }
            $.post(form.attr('action'), self.collectData())
              .success(function(data, status){
                var error = data.errored;
                if(!auto && !error){
                  setTimeout(function(){
                    savedMsg.hide();
                    submitButton.show();
                  }, 5000);
                } else if (error) {
                  showError();
                }
              })
              .error(function(){
                showError();
              });

            showError = function(){
              savedMsg.hide();
              $('#errorModal').modal({
                maxWidth: 450,
                minWidth: 450,
                maxHeight: 250,
                minHeight: 250,
                persist: true
              });
            };

            return self;
          },
          bind: function(){
            return self.bindClickPreorderChecklistStatus()
                       .bindChangePreorderRadioButtons()
                       .bindToggleSection()
                       .bindPreorderStartDatePicker()
                       .bindChangePreviewCheckbox()
                       .bindChangeGratCheckbox()
                       .bindChangePreorderPrice()
                       .bindChangeTrackPrice()
                       .bindChangeOfSong()
                       .bindModals()
                       .bindClickSubmit(),
                   self;
          },
          bindClickPreorderChecklistStatus: function(){
            preorderChecklistStatus.on('click', function(){
              self.activatePreorder();
            });
            return self;
          },
          bindChangePreorderRadioButtons: function(){
            preorderRadioButtons.on('change', function(){
              self.preorderRadioButtonsChanged();
            });
            return self;
          },
          bindToggleSection: function(){
            preorderHeaders.on('click', function(){
              self.toggleSection($(this));
            });
            preorderHeaderLinks.on('click', function(e){
              e.preventDefault();
            });
            return self;
          },
          bindPreorderStartDatePicker: function(){
            var options = $.extend(
                {},
                $.datepicker.regional[gon.datepicker_locale],
                {
                  dateFormat: $.datepicker.regional[gon.datepicker_locale].dateFormat,
                  maxDate: data.max_date,
                  minDate: data.min_date,
                  defaultDate: data.min_date
                }
            );
            $.datepicker.setDefaults(options);
            preorderStartDate.datepicker();
            preorderStartDate.on('change', function(){
              self.startDateChanged();
            });
            return self;
          },
          bindChangePreviewCheckbox: function(){
            previewCheckbox.on('change', function(){
              self.previewCheckboxChanged();
            });
            return self;
          },
          bindChangeGratCheckbox: function(){
            gratHolder.on('change', '.grat_checkbox', function(e){
              self.gratCheckboxChanged();
            });
            return self;
          },
          bindChangePreorderPrice: function(){
            preorderPriceSelect.change(function() {
              self.preorderPriceChanged();
            });
            return self;
          },
          bindChangeTrackPrice: function(){
            trackPriceSelect.change(function() {
              self.trackPriceChanged();
            });
            return self;
          },
          bindChangeOfSong: function(){
            $(document).on('song_create song_update song_destroy update_calendar', function(e){
              var songs = e.originalEvent ? e.originalEvent.detail.songs : null
              if (songs) {
                self.updateTrackCount(songs)
              }
              self.songChanged(songs);
            });
            return self;
          },
          bindModals: function(){
            $('#previewModalLink').on('click', function(e){
              e.preventDefault();
              $('#previewModal').modal({
                maxWidth: 650,
                minWidth: 650,
                maxHeight: 250,
                minHeight: 250,
                persist: true
              });
            });
            $('#gratModalLink').on('click', function(e){
              e.preventDefault();
              $('#gratModal').modal({
                maxWidth: 650,
                minWidth: 650,
                maxHeight: 300,
                minHeight: 300,
                persist: true
              });
            });
            $('#pricingModalLink').on('click', function(e){
              e.preventDefault();
              $('#pricingModal').modal({
                maxWidth: 850,
                minWidth: 850,
                maxHeight: 600,
                minHeight: 600,
                persist: true
              });
            });
            return self;
          },
          bindClickSubmit: function(){
            submitButton.on('click', function(e){
              e.preventDefault();
              self.submit();
            });
            return self;
          },
          gratCount: function(){
            return $('.grat_checkbox:checked').length;
          },
          updateTrackCount: function(songs) {
            self.trackLength = songs.length;
          },
          trackCount: function(){
            return self.trackLength || $('.song_item_for_preorder').length;
          },
          collectGratTracks: function(){
            var tracks = [];
            $('.grat_checkbox:checked').each(function(){
              var t = $(this);
              tracks.push(t.val());
            });
            return tracks;
          },
          sp: function(price){
            return parseFloat(price);
          },
          findClosestTier: function(price, direction){
            // when direction is 0, we're going down to find a maximum.
            // when direction is 1, we're going up to find a minimum.
            var price = self.sp(price);
            if((Math.round(price) - 0.01) < price) {
              return direction ? (Math.round(price) + 0.99) : (Math.round(price) - 0.01);
            } else if((Math.round(price) - 0.01) > price) {
              return direction ? (Math.round(price) - 0.01) : (Math.round(price) - 1.01);
            } else {
              return price;
            }
          },
          calculateMaxPrice: function(){
            var gratTotal = self.gratCount(),
                sumPrice = self.calculateSumPrice();
            if(gratTotal > 0) return self.findClosestTier(sumPrice, 0) > maxTrackPrice ? maxTrackPrice : self.findClosestTier(sumPrice, 0);
            else return sumPrice > maxTrackPrice ? maxTrackPrice : sumPrice;
          },
          calculateMinPrice: function(){
            var minPrice = null,
                gratTotal = self.gratCount(),
                sumPrice = self.calculateSumPrice();
                trackCount = self.trackCount(),
                trackPrice = self.sp(trackPriceSelect.val() || 0.99);
            if(gratTotal > 0){
              var gratMin = self.findClosestTier((gratTotal * trackPrice) + 1.98, 1);
              if(trackCount > 10) return (4.99 < gratMin) ? gratMin : 4.99;
              else if(trackCount > 7) return (3.99 < gratMin) ? gratMin : 3.99;
              else if(trackCount > 4) return (2.99 < gratMin) ? gratMin : 2.99;
              else if(trackCount > 1) return (1.99 < gratMin) ? gratMin : 1.99;
              else return sumPrice;
            }
            else{
              if(trackCount > 10) return 4.99;
              else if(trackCount > 7) return 3.99;
              else if(trackCount > 4) return 2.99;
              else if(trackCount > 2) return 1.99;
              // two-track albums with a track price of 1.29 can be
              // priced at 1.99, which is lower than sum of tracks
              else if(trackCount == 2 && trackPrice > 1) return 1.99;
              else if(trackCount == 2 && trackPrice < 1) return sumPrice;
              else return sumPrice;
            }
          },
          calculateSumPrice: function(){
            return self.sp((self.sp(trackPriceSelect.val() || 0.99) * self.trackCount()).toFixed(2));
          },

          calculateMaxGratTracks: function(skipSubmit){
            var selectedTrackPrice = $("#track-price-dropdown option:selected").val();
            selectedTrackPrice = parseFloat(selectedTrackPrice);
            return parseInt((maxTrackPrice - 0.99) / selectedTrackPrice) - 1;
          },
          disableTrackPriceTier: function(trackCount, gratTotal){
            var option69 = trackPriceSelect.find("option[value='0.69']"),
                option99 = trackPriceSelect.find("option[value='0.99']");

            (trackCount >= 4 && trackCount <= 7 && (gratTotal == Math.floor(trackCount / 2) || (trackCount == 4 && gratTotal == 1))) ?
                option69.attr('disabled', true)
              : option69.attr('disabled', false);

            (trackCount == 4 && gratTotal == 2) ? option99.attr('disabled', true) : option99.attr('disabled', false);
          },
          setTrackPrice: function(trackCount, gratTotal){
            if(trackCount >= 4 && trackCount <= 7 && (gratTotal == Math.floor(trackCount / 2) || (trackCount == 4 && gratTotal == 1)) && trackPriceSelect.val() == '0.69') trackPriceSelect.val('0.99');
            if(trackCount == 4 && gratTotal == 2 && ['0.69', '0.99'].includes(trackPriceSelect.val())) trackPriceSelect.val('1.29');
          },
          findConvertedPrice: function(price){
            var tier = tiers.find(function(e){
              return e[1] == price;
            });
            if (tier) {
              if (currency == "EUR") {
                convertedEuro = self.sanitizePriceString(tier[0]);
                return convertedEuro;
              } else {
                return parseFloat(tier[0].substr(1)).toFixed(2);
              }
            } else {
              var p = $("#track-price-dropdown option:selected").html();
              var sanitizedPrice = self.sanitizePriceString(p);
              return (parseFloat(sanitizedPrice) * self.trackCount()).toFixed(2);
            }
          },
          sanitizePriceString: function(price){
            // strip out all currency symbols!
            return price.replace(",",".").replace(/[$,€,£,₹]+/g, '').trim();
          },
          formatEuroPriceString: function(price){
            return price.replace(".", ",") + ' ' + currencySymbol;
          }
        };

        self.init();
        return self;
  };
  new TCPreorder();

});
