var $ = jQuery;

$(function () {
  const checkedInputValue = $(
    ".self-billing-component input[type='radio']:checked"
  ).val();

  if (checkedInputValue === "declined") {
    $(".self-billing-form-trigger-button").attr("disabled", true);
  }
});
