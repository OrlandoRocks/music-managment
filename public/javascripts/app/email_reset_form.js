var $ = jQuery;

function mfpCloseListener() {
    $.magnificPopup.close();
    $("#email-reset-success-modal").addClass("mfp-hide");
    $("#email-reset-failure-modal").addClass("mfp-hide");
}

var emailValidator = function(){
  $("#edit_person_email_reset_form").validate({
    rules: {
      'person[email]': {
        required: true,
        email: true
      },
      'person[email_confirmation]': {
        required: true,
        email: true,
        equalTo: '[name="person[email]"]'
      },
      'person[original_password]': {
        required: true,
        minlength: 8 
      }
    }
  });
};

jQuery(document).ready(function(){
  new emailValidator;
});
