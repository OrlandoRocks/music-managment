var $ = jQuery;

var switchCard = function(flag) {
  if ($(window).width() >= "480") {
    /*
      This slideshow code just applies and removes classes at intervals.  The animation
      is handled purely by CSS transitions.

    */
    var $active = $('#cards .flipped,#cards .active');
    $("#card_1").removeClass('active'); // kills the initial animation-free load class
    $(".card").removeClass('flipaway'); // the outgoing animation - this is for the cyclical transitions
    var $cycPrev = $active.prev().length ? $active.prev() : $('#cards LI:last');
    var $cycNext = $active.next().length ? $active.next() : $('#cards LI:first');

    var $old = (flag == "next") ? $cycPrev : $cycNext;
    var $next = (flag == "next") ? $cycNext : $cycPrev;

    if($active.attr("data-icon")) {
        $("#"+$active.attr("data-icon")).removeClass('flipped');
    }

    if($next.attr("data-icon")) {
      $("#"+$next.attr("data-icon")).addClass('flipped');
    }
    $active.addClass('flipaway');
    $active.removeClass('flipped');
    $next.addClass('flipped'); //the incoming animation
  } else {
    $(".card").removeClass('flipped').removeClass('flipaway');
    $("#card_1").removeClass('flipaway').addClass('flipped');
    $(".thirds").removeClass('flipped');
  }
};

var jumpToCard = function(item) {
  $("#card_1").removeClass('active'); // kills the initial animation-free load class

  if ($(window).width() >= "480") {
    if (!$("#"+item).hasClass('flipped')) {
      $(".card").removeClass('flipaway'); // housecleaning
      $('#cards .flipped').addClass('flipaway');  // get rid of whichever's already lit
      $('.card').removeClass('flipped');
      $('.thirds').removeClass('flipped');
      $("#"+item).addClass('flipped');
      $('*[data-icon="'+item+'"]').first().addClass('flipped');
    } else {
      return false;
    }
  } else {
    $(".card").removeClass('flipped').removeClass('flipaway');
    $("#card_1").removeClass('flipaway').addClass('flipped');
    $(".thirds").removeClass('flipped');
  }
};

var highlightNav = function() {
  var $active = $('#cards .flipped,#cards .active'),
      active_slide = $active.data("icon"),
      $dots_container = $("#slideshow").find(".dots"),
      $dots = $("#slideshow").find(".dot");

  $dots.each(function() {
    if ($(this).data("slide") == active_slide) {
      $(this).removeClass("fa-circle-o").addClass("fa-circle");
    } else {
      $(this).removeClass("fa-circle").addClass("fa-circle-o");
    }
  });
};

var clickableNav = function() {
  var $dots_container = $("#slideshow").find(".dots"),
      $dots = $("#slideshow").find(".dot");

  $dots.on('click', function(e) {
    e.preventDefault();
    var slide = $(this).data("slide");
    jumpToCard(slide);
    highlightNav();
  });
};

var parallaxView = function() {
  $(window).scroll(function(){
    var scrollPosition = $(window).scrollTop(),
    slideshow = $("#slideshow"),
    cards = $('#card_1,#card_3');

    if ( (scrollPosition + $(window).height() ) > (0) &&
        ( (slideshow.height()) > scrollPosition ) ) {
       var yPos = (scrollPosition / 2) + 'px';
       var yPcnt = (100 - (scrollPosition / 2)) + '%';
       cards.css({ 'background-position-y' : yPos });
       $('#card_2').css({'background-position-y' : yPcnt });
    }
  });
};



$(document).ready(function() {
  clickableNav();

  // Hides URL bar on mobile Safari only after 1 second timeout
  /mobi/i.test(navigator.userAgent) && !location.hash && setTimeout(function () {
    if (!pageYOffset) window.scrollTo(0, 1);
  }, 1000);


  $(window).resize(function() {
    if ($(window).width() <= "480") {
      $(".card").removeClass('flipped').removeClass('flipaway');
      $("#card_1").removeClass('flipaway').addClass('flipped');
      $(".thirds").removeClass('flipped');
    } else {
      var flipped = $(".card.flipped").data('icon');
      $('#'+flipped).addClass('flipped');
    }
  });


  // parallaxView(); removed because of excessive judderiness in chrome
  if ($(window).width() >= "640") {
    $(".masthead").stickyDiv(80);
  }

  // var slideshow = window.setInterval(function() { switchCard("next"); }, 15000); // every ten seconds

  $("#nextArrow").click(function(e) {
    e.preventDefault();
    // window.clearInterval(slideshow);
    switchCard("next");
    highlightNav();
    // slideshow = window.setInterval(function() { switchCard("next"); }, 20000);  // start up slower slideshow
  });

  $("#prevArrow").click(function(e) {
    e.preventDefault();
    // window.clearInterval(slideshow);
    switchCard("prev");
    highlightNav();
    // slideshow = window.setInterval(function() { switchCard("next"); }, 20000);
  });

  $(".thirds").click(function() {
    // window.clearInterval(slideshow);
    jumpToCard($(this).attr("id"));
    // slideshow = window.setInterval(function() { switchCard("next"); }, 20000);
  });

  $(".photo_credit").hover(function () {  // slide will not cycle away when checking photo credit
      // window.clearInterval(slideshow);
    }, function () {
      // slideshow = window.setInterval(function() { switchCard("next"); }, 25000);
  });

  // Load the YT JS
  var tag = document.createElement('script');
      tag.src = "//www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  // HP Img To Vid Swapping - this should probably be a seperate json obj.
  (function(t){
    t.vids = {
      jdash: '<a href="#" data-ytid="HEBN0Sg7R7g" id="jdash" style="background-image: url(/images/vid_jdash.jpg)"><h2>J. Dash Uses TuneCore</h2></a>',
      costello: '<a href="#" data-ytid="Q4iVhUqD_qQ" id="costello" style="background-image: url(/images/vid_costello.jpg)"><h2>Katie Costello says hello to TuneCore</h2></a>',
      coltford: '<a href="#" data-ytid="RkgeumJbthM" id="coltford" style="background-image: url(/images/vid_coltford.jpg)"><h2>Colt Ford uses TuneCore</h2></a>',
      benwells: '<a href="#" data-ytid="k-5WD9p3WaQ" id="benwells" style="background-image: url(/images/vid_benwells.jpg)"><h2>Ben Wells Thanks TuneCore</h2></a>',
      youngromans: '<a href="#" data-ytid="A8IdSWZ4_NY" id="youngromans" style="background-image: url(/images/vid_youngromans.jpg)"><h2>The Young Romans shout out for TuneCore</h2></a>',
      copyright101: '<a href="#" data-ytid="FMvhjwwzcQg" id="copyright101"><h2>TuneCore Copyright 101</h2></a>'
    };
    t.onYouTubePlayerReady = function(v){
      var id = v.target.a.id;
      t[id] = function(n){
        if(n.data === 0){
          $('.videos .showing').html(t.vids[id]);
        }
      };
      v.target.addEventListener('onStateChange', id);
    };
    $('.videos .showing').on('click', 'a', function(e){
      e.preventDefault();
      var p = $(this),
          id = p.attr('id'),
          ytid = p.data('ytid');
          t.player = new YT.Player(id, {
                playerVars: { 'autoplay': 1 },
                height: '100%',
                width: '100%',
                videoId: ytid,
                data: 'test',
                events: {
                  'onReady': onYouTubePlayerReady
                }
          });
    });
  })(window);

  // for switching active videos
  $('.videos .thumbs a').on('click', function(e){
    e.preventDefault();
    var t = $(this);
    $('.videos .thumbs a.active').removeClass('active');
    t.addClass('active');
    $('.videos .showing').html(vids[t.attr('data-aname')]);
  });

  // more store logos
  $('#store_logos .more').on('click', function(e){
    e.preventDefault();
    var p = $('.store_logos'),
        t = $(this);
    if(p.hasClass('all')){
      p.removeClass('all');
      t.html('More Stores');
    }
    else{
      p.addClass('all');
      t.html('Hide Stores');
    }
  });

  // keeping the aspect ratio
  var resizeVids = function(){
    $('.showing').each(function(){
      var t = $(this);
      t.height(function(){
        return 9 / 16 * t.width() + 'px';
      });
    });
  };
  resizeVids();
  $(window).on('resize', function(){
    resizeVids();
  });

  var $tellUsLink = $("#tellUs");
  $modal = $("#tellUsModal"),
  $iframe = $modal.find("iframe");

  // tell us dialog
  $modal.dialog({
    autoOpen: false,
    resizable: false,
    modal: true,
    draggable: false,
    width: 600,
    height: 500
  });

  $tellUsLink.on('click', function(e){
    e.preventDefault();

    $modal.html('<iframe src="http://tunecore.sites.hubspot.com/testimonial" scrolling="no" style="width: 100%; height: 1350px; border: 0px;"></iframe>').dialog('open');
  });

  $('.bxslider').bxSlider({
    minSlides: 1,
    maxSlides: 1,
    slideMargin: 0,
    slideWidth: 690,
    prevSelector: '.prev',
    prevText: '<i class="fa fa-chevron-left fa-2x"></i>',
    nextSelector: '.next',
    nextText: '<i class="fa fa-chevron-right fa-2x"></i>',
    pagerCustom: '.dots2'
  });
});


// Lazy loading
// $(window).on('load', function(){
//   // $('#card_2').addClass("loaded");
//   // $('#card_3').addClass("loaded");
//   // $('#jdash').css({
//   //   'background-image': 'url(/images/vid_jdash.jpg)'
//   // });
// });
