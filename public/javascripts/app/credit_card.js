//
//  Credit Cards:
//

var CreditCard = function(){
  var $ = jQuery;
  var radio = '#stored_card_id_new_card';
  var form = '#new_cc_form';
  var pay_buttons = '#pay_with_stored';
  var form_input = ".context-wrapper input";
  var stored_radio = "[id^=stored_credit_card] input";
  var add_cc_form = '#new_stored_credit_card';
  var edit_cc_form = '.edit_stored_credit_card';
  var add_payment_cc = '.edit_credit_card';
  var stored_cc_form = '#stored-credit-cards';

  var self = {
    init: function(){
      self.mask_expiry_date();
      self.init_helper_text();
      self.init_card_form();
      self.init_validation();
      self.check_new_card_radio();
      self.highlight_card_type();
    },

    init_card_form: function(){
      $(radio).click(self.show_card_form);
      $(':radio:not(#stored_card_id_new_card)').click(self.hide_card_form);
    },

    init_helper_text: function(){
      $(form_input).focus(this.show_helper_text);
      $(form_input).blur(this.hide_helper_text);
    },

    init_validation: function(){
      //for the stored cards
      $(stored_cc_form).validate({
        submitHandler: function(form){
          $('#pay_with_stored_card').prop('disabled', 'disabled');
          form.submit();
        }
      });

      //stored_credit_cards/new
      $(add_cc_form).validate({
        submitHandler: function(form){
          // Checking whether 3DSecure is enabled
          // This prevents the form from submitting and
          // triggering the old flow.
          if (is3DSecureEnabled()) {
            return;
          }

          $('.credit_card_form input[type=submit]').prop('disabled', true);
          client = new braintree.api.Client({ clientToken: gon.braintreeToken });

          client.tokenizeCard({
            number: $("#stored_credit_card_last_four").val(),
            expirationDate: $("#stored_credit_card_expiration_month").val(),
            cardholderName: $("#stored_credit_card_first_name").val() + " " + $("#stored_credit_card_last_name").val(),
            cvv: $("#stored_credit_card_cvv").val(),
            billingAddress: {
              postalCode: $("#stored_credit_card_zip").val()
            }
          }, function (err, nonce) {
            $("#braintree_payment_nonce").val(nonce);
            form.submit();
          });
        },
        rules: {
          ccnumber: {
            required: true,
            rangelength: [14,16],
            digits: true,
            ccCheckSum: {visa:true, mastercard:true, discover:true, amex: true}
          },
          ccexp: {
            required: true,
            ccExpDate: true
          },
          firstname: {
            required: true,
            minlength: 1
          },
          lastname: {
            required: true,
            minlength: 2
          },
          address1: {
            required: true,
            minlength: 2
          },
          city: {
            required: true,
            minlength: 2
          },
          zip: {
            required: true,
            minlength: 2
          },
          country: {
            required: true
          },
          cvv: {
            required: true
          },
          state: {
            required: function(element){
              return jQuery('#stored_credit_card_country').val() === 'US';
            }
          },
          company: {
            allowed_input: true
          }
        },
        messages: {
          ccnumber: {
            required: "Please enter a valid credit card number.",
            rangelength: jQuery.format("Enter between {0} and {1} digits.")
          },
          ccexp: {
            required: "Please enter an expiration date.",
            ccExpDate: "Please enter a valid expiration date in the format MM/YYYY (example: 05/2014)"
          },
          firstname: "Please enter your first name.",
          lastname: "Please enter your last name.",
          address1: "Please enter your address.",
          city: "Please enter your city.",
          zip: "Please enter your postal code.",
          state: "Please enter your state.",
          country: "Please enter your country."
        }
      });

      // stored_credit_cards/edit
      $(edit_cc_form).validate({
        submitHandler: function(form){
          $('.edit_stored_credit_card input[type=submit]').prop('disabled', true);
          client = new braintree.api.Client({ clientToken: gon.braintreeToken });

          client.tokenizeCard({
            number: $("#stored_credit_card_last_four").val(),
            expirationDate: $("#stored_credit_card_expiration_month").val(),
            cardholderName: $("#stored_credit_card_first_name").val() + " " + $("#stored_credit_card_last_name").val(),
            cvv: $("#stored_credit_card_cvv").val(),
            billingAddress: {
              postalCode: $("#stored_credit_card_zip").val()
            }
          }, function (err, nonce) {
            $("#braintree_payment_nonce").val(nonce);
            form.submit();
          });
        },
        rules: {
          ccexp: {
            required: true,
            ccExpDate: true
          },
          cvv: {
            required: true
          },
          firstname: {
            required: true,
            minlength: 1
          },
          lastname: {
            required: true,
            minlength: 2
          },
          address1: {
            required: true,
            minlength: 2
          },
          city: {
            required: true,
            minlength: 2
          },
          zip: {
            required: true,
            minlength: 2
          },
          country: {required: true},
          state: {
            required: function(element){
              return jQuery('#stored_credit_card_country').val() === 'US';
            }
          },
          company: {
            allowed_input: true
          }
        },
        messages: {
          ccexp: {
            required: "Please enter an expiration date.",
            ccExpDate: "Please enter a valid expiration date in the format MM/YYYY (example: 05/2014)"
          },
          firstname: "Please enter your first name.",
          lastname: "Please enter your last name.",
          address1: "Please enter your address.",
          city: "Please enter your city.",
          zip: "Please enter your postal code.",
          state: "Please enter your state.",
          country: "Please enter your country."
        }
      });

      $(add_payment_cc).validate({
        rules: {
          ccexp: {
            required: true,
            ccExpDate: true
          },
          ccnumber: {
            required: true,
            rangelength: [14,16],
            digits: true,
            ccCheckSum: {visa:true, mastercard:true, discover:true, amex: true}
          },
          firstname: {
            required: true,
            minlength: 1
          },
          lastname: {
            required: true,
            minlength: 2
          },
          address1: {
            required: true,
            minlength: 2
          },
          city: {
            required: true,
            minlength: 2
          },
          zip: {
            required: true,
            minlength: 2
          },
          country: {required: true},
          state: {
            required: function(element){
              console.log(jQuery('#country').val());
              return jQuery('#country').val() === 'US';
            }
          },
          cvv: {
            required: true,
            rangelength: [3,4]
          },
          company: {
            allowed_input: true
          }
        },
        messages: {
          ccnumber: {
            required: "Please enter a valid credit card number.",
            rangelength: jQuery.format("Enter between {0} and {1} digits.")
          },
          ccexp: {
            required: "Please enter an expiration date.",
            ccExpDate: "Please enter a valid expiration date in the format MM/YYYY (example: 05/2014)"
          },
          firstname: "Please enter your first name.",
          lastname: "Please enter your last name.",
          address1: "Please enter your address.",
          city: "Please enter your city.",
          zip: "Please enter your postal code.",
          state: "Please enter your state.",
          country: "Please enter your country.",
          cvv: {
            required: "Please enter a security code.",
            rangelength: jQuery.format("The security code is between {0} and {1} digits.")
          }
        },
        submitHandler: function(form){
          $('#pay_with_new_card').prop('disabled', 'disabled');
          $('#pay_with_stored_card').prop('disabled', 'disabled');
          client = new braintree.api.Client({ clientToken: gon.braintreeToken });

          client.tokenizeCard({
            number: $("#stored_credit_card_last_four").val(),
            expirationDate: $("#stored_credit_card_expiration_month").val(),
            cardholderName: $("#stored_credit_card_first_name").val() + " " + $("#stored_credit_card_last_name").val(),
            cvv: $("#stored_credit_card_cvv").val(),
            billingAddress: {
              postalCode: $("#stored_credit_card_zip").val()
            }
          }, function (err, nonce) {
            $("#braintree_payment_nonce").val(nonce);
            form.submit();
          });
        }
      });
    },

    highlight_card_type: function(){
      $('#stored_credit_card_last_four').on('keyup change', function(){
        var $first_num = $(this).val().charAt(0),
        $cc_icons = $(".card-icons").find("img");
        if ($first_num == 3) {
          $cc_icons.filter(':not(.amex)').css('opacity', '0.5');
        } else if ($first_num == 4) {
          $cc_icons.filter(':not(.visa)').css('opacity', '0.5');
        } else if ($first_num == 5) {
          $cc_icons.filter(':not(.mastercard)').css('opacity', '0.5');
        } else if ($first_num == 6) {
          $cc_icons.filter(':not(.discover)').css('opacity', '0.5');
        } else {
          $cc_icons.css('opacity', '1');
        }
      });
    },

    check_new_card_radio: function(){
      if ($(radio).is(':checked')){
        $(form).removeClass('hidden');
        $(pay_buttons).addClass('hidden');
      };
    },

    mask_expiry_date: function(){
      var expiry_date = '[id$=credit_card_expiration_month]';
      // $(expiry_date).mask('99/99');
      $(expiry_date).inputmask("m/y",{ "placeholder": "*" });
    },

    show_card_form: function(){
      if ($(form).is(':hidden')){
        $(form).slideDown(500);
        $(pay_buttons).slideUp(200);
      };
    },
    hide_card_form: function(){
      $(form).slideUp(500);
      $(pay_buttons).slideDown(200);
    },

    disable_submit_btn: function(){
      $(this).prop('disabled', 'disabled');
    },

    show_helper_text: function(){
      var help = $(this).next('.contextual-help');

      if ((help.find('p').text().length !== 0) && (help.prev().attr('class') !== 'error')){
        help.removeClass('hidden');
      };
    },

    hide_helper_text: function(){
      $(this).next('.contextual-help').addClass('hidden');
    }
  };

  // We have to add a setTimeout for this to work properly
  // This dynamically sets the minimum length of characters required for specified languages, default is 2
  setTimeout(function() {
    if (["sc", "tc"].includes(window.gon.locale)) {
      $('[name*="lastname"]').rules('add', { minlength: 1})
    }
  }, 0);

  self.init();
  return self;
};

//custom validation method
jQuery.validator.addMethod("ccExpDate",function(value, element) {
    var check = false,
        re = /^\d{1,2}\/\d{4}$/;

    if (re.test(value)){
        var adata = value.split('/'),
            mm = parseInt(adata[0],10),
            yyyy = parseInt(adata[1],10),
            userDate = new Date(yyyy,mm-1),
            nd = new Date(),
            currYear = nd.getFullYear(),
            currMonth = nd.getMonth();

            if (mm > 0) {
              if (!!(yyyy === currYear) && !!(mm < currMonth)) {
                  check = false;
            } else if ((userDate.getFullYear() >= currYear) && (userDate.getMonth() >= mm - 1)) {
                  check = true;
                } else {
                  check = false;
                };
            } else {
              check = false;
            }
            return this.optional(element) || check;
          }
    });

// NOTICE: Modified version of Castle.Components.Validator.CreditCardValidator
// Redistributed under the the Apache License 2.0 at http://www.apache.org/licenses/LICENSE-2.0
// Which has a hacked-in version of http://blog.planzero.org/2009/08/javascript-luhn-modulus-implementation/
// Luhn algorithm number checker - (c) 2005-2009 - planzero.org

jQuery.validator.addMethod("ccCheckSum", function(value, element, param) {

	if (/[^0-9-]+/.test(value))
		return false;

	// Strip any non-digits (useful for credit card numbers with spaces and hyphens)
	var value = value.replace(/\D/g, "");

	// Set the string length and parity
	var number_length = value.length;
	var parity = number_length % 2;

	var check = {luhn: function(){
	  var total = 0;
	  for (i=0; i < number_length; i++){
	    var digit = value.charAt(i);
	    //Multiply alternate digits by two
	    if (i % 2 == parity) {
	      digit = digit * 2;
	      //If the sum is two digits, add them together ( in effect )
	      if (digit > 9) {
	        digit = digit - 9;
	      }
	    }
	    // Total up the digits
	    total = total + parseInt(digit, 10);
	  }

	  //If the total mod 10 equals 0, the number is valid
	  if (total % 10 == 0) {
	    return true;
    } else {
      return false;
	  }
	}};
	var validTypes = 0x0000;

	if (param.mastercard)
		validTypes |= 0x0001;
	if (param.visa)
		validTypes |= 0x0002;
	if (param.amex)
		validTypes |= 0x0004;
	if (param.dinersclub)
		validTypes |= 0x0008;
	if (param.enroute)
		validTypes |= 0x0010;
	if (param.discover)
		validTypes |= 0x0020;
	if (param.jcb)
		validTypes |= 0x0040;
	if (param.all)
		validTypes = 0x0001 | 0x0002 | 0x0004 | 0x0008 | 0x0010 | 0x0020 | 0x0040;

	if (validTypes & 0x0001 && (/^(51|52|53|54|55)/).test(value)) { //mastercard
		return value.length == 16 && check.luhn();
	}
	if (validTypes & 0x0002 && (/^(4)/).test(value)) { //visa
		return value.length == 16 && check.luhn();
	}
	if (validTypes & 0x0004 && (/^(34|37)/).test(value)) { //amex
		return value.length == 15 && check.luhn();
	}
	if (validTypes & 0x0008 && (/^(300|301|302|303|304|305|36|38)/).test(value)) { //dinersclub
		return value.length == 14 && check.luhn();
	}
	if (validTypes & 0x0010 && (/^(2014|2149)/).test(value)) { //enroute
		return value.length == 15 && check.luhn();
	}
	if (validTypes & 0x0020 && (/^(6011)/).test(value)) { //discover
		return value.length == 16 && check.luhn();
	}
	if (validTypes & 0x0040 && (/^(3)/).test(value)) { //jcb
		return value.length == 16 && check.luhn();
	}
	if (validTypes & 0x0040 && (/^(2131|1800)/).test(value)) { //jcb
		return value.length == 15 && check.luhn();
	}
	return false;
}, "Please enter a valid credit card number.");

jQuery.validator.addMethod(
  "allowed_input", function (value) {
    return (/^[a-zA-Z0-9.#,'&\s]*$/).test(value);
  }, "Characters allowed are a-z A-Z 0-9 . # , ' &"
);

function cardAddressInfoValidations() {;
  var countrySelector = $('#stored_credit_card_country');
  
  var cardAddress1Input = $('#stored_credit_card_address1');
  var cardAddress2Input = $('#stored_credit_card_address2');
  var cityInput = $('#stored_credit_card_city');
  var stateInput = $('#stored_credit_card_state');
  var zipCodeInput = $('#stored_credit_card_postal_code');
  var addressInputs = [
    cardAddress1Input,
    cardAddress2Input,
    cityInput,
    stateInput,
    zipCodeInput
  ];

  applyRegexToInputs(addressInputs);
  toggleStateInput(countrySelector, stateInput);
}

function applyRegexToInputs(addressInputs) {
  addressInputs.forEach(function(input) {
    var regex = new RegExp(input.data('regex'), input.data('regexFlag'));
    input.inputFilter(function(value) {
      return regex.test(value);
    });
  });
}

function toggleRequiredFields(fields, type) {
  switch(type) {
    case "hide":
      fields.forEach(function(field) {
        field.find('.required').hide();
      });
      break;
    case "show":
      fields.forEach(function(field) {
        field.find('.required').show();
      });
      break;
    default:
      break;
  }
}

function toggleStateInput(countrySelector, stateInput) {
  var stateField = $('#stored_credit_card_state_list_item');
  var optionalAddressCountries = countrySelector.data('optionalAddressCountries');

  if (optionalAddressCountries.includes(countrySelector.val())) {
    stateField.show();
    toggleRequiredFields([stateInput], "show");
  } else {
    stateField.hide();
  }

    countrySelector.on('change', function() {
    if (optionalAddressCountries.includes(countrySelector.val())) {
      stateField.show();
      toggleRequiredFields([stateInput], "show");
    } else {
      stateField.hide();
    }
  });
}

jQuery(document).ready(function(){
  new CreditCard();
  cardAddressInfoValidations();
});


$.fn.inputFilter = function(inputFilter) {
  var input = $(this);
  return input.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
    var changeInput = $(this);
    if (inputFilter(changeInput.val())) {
      changeInput.attr('prevValue', changeInput.val());
      changeInput.attr('prevSelectionStart', this.selectionStart);
      changeInput.attr('prevSelectionEnd', this.selectionEnd);
    } else if (!!changeInput.attr('prevValue')) { // reverts back to last correct input when user inputs improperly
      changeInput.val(changeInput.attr('prevValue'));
      this.setSelectionRange(changeInput.attr('prevSelectionStart'), changeInput.attr('prevSelectionEnd'));
    } else {
      changeInput.val("");
    }
  });
};

function is3DSecureEnabled() {
  return typeof ThreeDSecure === "object" && ThreeDSecure.enabled
}
