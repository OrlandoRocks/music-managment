jQuery.fn.liveUpdate = function(list_div,songs){
	
	
	var $ = jQuery; 
	var dropdown_list = $(list_div); 

	this.keyup(filter).keyup();

	return this;
	
		
	function filter(){
		var term = $.trim( $(this).val().toLowerCase() ), scores = [],matched_songs = [];
		dropdown_list.empty(); 
		dropdown_list.hide();
		
		if (term) {
			$.each(songs, function(i) {
				var score = (this.lc_name + this.album).score(term);
				if (score > 0) { 
					scores.push(score);
					matched_songs.push([this.id,this.display_name,this.album,this.track_num]); 
				}
			});
		
			$.each(
				scores.sort(), 
				function(i) {
					dropdown_list.append("<li><input type='radio' id='ringtone_related_song_"+matched_songs[i][0]+"' name='ringtone[related_song_id]' value='"+ matched_songs[i][0] +"'>" + matched_songs[i][1] + "("+matched_songs[i][2]+":"+matched_songs[i][3]+")" + "</li>");  

					if (matched_songs.length == 1) {
						$("#ringtone_related_song_"+matched_songs[i][0]).prop('checked',"checked");
					}; 
				}
			);
			
			if (matched_songs.length > 0) {
				dropdown_list.show();
			};
		}
	
	}
};

