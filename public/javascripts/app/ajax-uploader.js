/*
 *  AjaxUploader:
 *    sets up ajax form submission, kicks off progress bar widget
 *
 *  ProgressBar:
 *    provides abstraction of progress bar widget
 * 
 */
var AjaxUploader = function(){
  var $ = jQuery,
      submit_button = ".ajax-uploader",
      spinner = $('#spinner');

  var self = {
    // Setup 
    init: function(){
      $(document).on("click", submit_button, self.send);
    },
  
    //
    // Event handlers
    //
    send: function(){
      self.start_spinner();
      self.disable_submit_button();
    },
  
  
    //
    //  Actions
    //
    start_spinner: function(){
      spinner.removeClass("hidden");
    },
  
    disable_submit_button: function(){
      submit_button.addClass("hidden");
    }
  };

    return self.init();
};
  
jQuery(document).ready(function(){
  new AjaxUploader();
});