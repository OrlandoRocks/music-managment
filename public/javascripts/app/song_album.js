/* Song_Album.js
  = Description
  The following code allows the save, edit, delete, and upload buttons on
  the Album Page to make ajax requests for updating or adding song content.

  = Change log
  [2010-05-18 -- GB] Initial Creation
  [2010-05-19 -- RT] Changed ordering in load_post_results for the added
                    track to come from response, while the song form is
                    now an ajax request
  [2010-05-20 -- RT] Added edit song, and delete song functionality
  [2010-05-20 -- GB] Added reordering songs functionality
  [2010-05-25 -- GB] Added zebra striping of song list and improved styling
  [2010-06-02 -- MT] Adding a function to help position error messages for songs
  [2010-06-03 -- ML] Adding logic to allow multiple uploads to be toggled on and off
  [2010-06-08 -- RT] Disabled all edit and delete buttons after clicking Upload
  [2010-06-25 -- MT] Adding metadata suggester

  = Dependencies
  Depends on the jquery.ui for highlighting appendages.

  = Usage
  Not a function to be used outside this file.

*/
var TC = TC || {};
var MU = 0;

var SongAlbum = function() {

  // Set up $ to be used locally for jQuery then
  // cache selectors so you don't have to traverse
  // the DOM each time.
  var $ = jQuery;
  var save_button = '.save-button';
  var upload_button = '.upload-button';
  var edit_button = '.edit-button';
  var delete_button = '.delete-button';
  var track_list = $('#track-list');
  var song_form = $('#song-form');
  var reorder_toggle = $('#reorder-toggle');
  var show_form_toggle = $('#show-form-toggle');
  var hidden_form_toggle = $('#show-form-toggle.hidden');

  var song_title = $('#song_name');
  var song_artist = $('#new_song input.trigger-preview');

  var restripe = function() {
    $('#track-list li').removeClass('odd');
    $('#track-list li:nth-child(odd)').addClass('odd');
    $('#track-list li:nth-child(even)').addClass('even');
  };

  //  Make a call to the webserver to return the album checklist and update
  //  it on the page.
  var updateChecklist = function(){
    var request = '/albums/' + TC.albumId + '/distribution_panel';

    $.get(request,
          function(response){
            $('#distribution-panel').html(response);

            //if the purchased credit prompt box is in the response
            //set up event handlers
            if ($(response).find('#upgrade-prompt').length) {
              var no = '#upgrade-no',
                yes = '#upgrade-yes',
                prompt = $('#upgrade-prompt'),
                opts = $('#upgrade-options'),
                use = $('#use-credit'),
                keep = $('#keep-credit'),
                goBack = 'a.upgrade-change';

              function hidePrompt(){
                prompt.addClass('hidden');
                opts.removeClass('hidden');
              }

              $(document).on('click', no, function(){
                hidePrompt();
                use.removeClass('hidden');
              });

              $(document).on('click', yes, function(){
                hidePrompt();
                keep.removeClass('hidden');
              });

              $(document).on('click', goBack, function(){
                prompt.removeClass('hidden');
                opts.addClass('hidden');
                keep.addClass('hidden');
                use.addClass('hidden');
              });
            }
          },
          'html');
  };

  var self = {

    init: function(){
      //  If the album has not been finalized, continue.
      if (!TC.finalized){
        // Set up metadata suggester
        if (gon.locale == 'en') {
          self.init_styleguides();
        }

        //  Prepare ajax links.
        $(document).on('click', save_button, self.submit_song);
        $(document).on('click', upload_button, self.upload_song);
        $(document).on('click', edit_button, self.edit_song);
        $(document).on('click', delete_button, self.delete_song);

        //  Add html for js-only links.
        if ( track_list.children('li').length > 1) { reorder_toggle.text('Reorder Songs'); }

        if ( track_list.children('li').length < 1 ) { track_list.hide(); }

        show_form_toggle.html('<a href="#" class="button">Add Song</a>');
        self.add_done_link();

        //  Show form only if "Add Song" toggle is hidden.
        if (hidden_form_toggle.length > 0) {
          song_form.show();
        }

        //  Show Add Song Form Controls
        //  Also clear form after submit
        show_form_toggle.children('a').click(function(){
          song_form.find(':input').each(function(){
            switch(this.type) {
            case 'text':
              if ($(this).disabled == false) {
                $(this).val('');
              }
              break;
            case 'checkbox':
              this.checked = false;
              break;
            }
          });
          song_form.show();
          show_form_toggle.hide();
          return false;
        });

        self.init_sortable();
        return self;
      } else {
        self.disable_all_features();
      }
    },

    init_styleguides: function(){
      // single and album
      if (song_title.length) {
        song_title.styleGuide({
          rules: {
            'volume': /\sV(?!ol\.\s\d)|v[Oo][Ll](?:[Uu][Mm][Ee])?(\.)?\s(?:\d|[OoTtFfSsEeNn])/,
            'part': /\s(?:P[^t]|p[Tt]?|P)(?:[Aa][Rr][Tt])?(\S)?\s(?:\d|[oOtTfFsSeEnN])/,
            'explicit': /\s([\[(]?([eE]xplicit|[dD]irty)[\])]?)/
          },
          messages: {
            'volume': 'Is this a multiple volume/disc release? If so, iTunes has specific formatting rules that must be followed. For example: "Album Title, Vol. 1"',
            'part': 'Is this a multiple volume/disc release? If so, iTunes has specific formatting rules that must be followed. For example: "Album Title, Pt. 1"',
            'explicit': 'Is this song explicit? If so, click the \'explicit\' checkbox below instead of putting this information in the title.'
          }
        });
      }

      if (song_artist.length) {
        song_artist.styleGuide({
          rules: {
            '/': /\//,
            '&': /\s\&\s/,
            '+': /\s\+\s/,
            '-': /\s\-\s/,
            'and': /\sand\s/i,
            'with': /\sw((ith)|\/)\s/i,
            'featuring': /\sf(ea)?t((\s)|(\.)?)\s?/i,
            'produced' : /\s\W?[pP]rod(\W|uce|\s)/
          },
          messages: {
            '/': 'Are you trying to add another artist to this album? If so, please click "Add Another Main Artist" below.',
            '&': 'Are you trying to add another artist to this album? If so, please click "Add Another Main Artist" below.',
            '+': 'Are you trying to add another artist to this album? If so, please click "Add Another Main Artist" below.',
            '-': 'Are you trying to add another artist to this album? If so, please click "Add Another Main Artist" below.',
            'and': 'Are you trying to add another artist to this album? If so, please click "Add Another Main Artist" below.',
            'with': 'Are you trying to add another artist to this album? If so, please click "Add Another Main Artist" below.',
            'featuring': 'Are you trying to add a featured artist to this song? If so, please enter them in the "Featured Artist" field below.',
            'produced' : 'Are you trying to enter a producer? Stores do not allow this information to be listed, please do not include or your release may be delayed.'
          }
        });
      }
    },

    updateChecklist: function(){
      updateChecklist();
    },

    //  Add Done Link
    add_done_link: function(){
      song_form.find('#song_Save_list_item').append('<a href="#" class="done-button button cancel">I\'m done adding songs</a>');
      $('.done-button').click(function(){
        song_form.hide();
        show_form_toggle.show();
        return false;
      });
    },

    //  Hide Add Song Form
    hide_add_song_form: function(){
      song_form.hide();
      show_form_toggle.show();
    },

    // We need a little help to position the error messages
    // since each <li> in the form is a different length
    adjust_error_messages: function() {
      $('.formError').each(function(){
        var error = $(this);
        var label = error.parent().prev();
        var adjust = label.width() + 20;
        error.css('left', adjust);
      });
    },

    //  Event Handlers
    //
    //  Click Save Button
    //    -IF Add Song Form
    //      1. Ajax POST
    //      2. Append new song from response via load_post_results function
    //    -ELSE Edit Song Form
    //      1. Ajax POST
    //      2. Unwrap partial from li (because we are putting it back into an li)
    //      3. Replace edit song form with the song partial
    //      3a. Restripe the table and do a highlight effect
    submit_song: function(){
      var selector = $(this);
      var form = selector.parents('form');
      var href = form.attr('action');
      var data = form.serialize();

      if (form.closest('#add-songs').length > 0){ // IF Add Song Form
        // Start spinner and hide submit/cancel buttons
        $('#song_Save_list_item').append('<img src="/images/icons/spinner-24x24.gif" alt="" id="add-spinner" />');
        selector.hide();
        $('.done-button').hide();
        $.post( // 1.
          href,
          data,
          function(response){
            if ( $(response).find('.fieldWithErrors').length){ // IF Error
              song_form.html(response);
              self.adjust_error_messages(response);
              self.add_done_link();
            } else if ( song_form.find('.fieldWithErrors').length){ // ELSE IF Resubmit form
              track_list.show();
              $(response).appendTo(track_list).effect('highlight', {}, 2000); // 2.
              restripe();
              self.hide_creatives();
              self.get_song_form();
            } else { // ELSE Normal
              track_list.show();
              self.hide_creatives();
              $(response).appendTo(track_list).effect('highlight', {}, 2000); // 2.
              song_form.find('#song_name').val('');
              $('#add-spinner').hide().remove();
              selector.show();
              $('.done-button').show();
              reorder_toggle.show();
              restripe();
              if ( track_list.children('li').length > 1) { reorder_toggle.text('Reorder Songs'); }
              updateChecklist();
              $(document).trigger('song_create');
            }
          },
          'html'
        );
        // hide the multiple artists stuff and recreate the link for add multiple artists HERE

      } else { // ELSE Edit Song Form
        $('.save-button[rel=\'edit-form\']').hide();
        $('#song_Save_list_item').append('<img src="/images/icons/spinner-24x24.gif" alt="" style="margin-top:4px" />');
        $('.cancel-button').remove();
        reorder_toggle.hide();

        $.post( // 1.
          href,
          data,
          function(response){
            var song_item = selector.parents('li');
            var s = $(response).children().unwrap(); // 2.
            // IF Error
            if ( $(s).find('.fieldWithErrors').length ){
              s = $(response);
            }
            // ELSE Normal
            $(s).find('.edit_song ul').append('<li><a href="#" class="cancel-button button cancel">Cancel</a></li>');
            selector.parents('li').effect('highlight', {}, 1000);
            selector.parents('li').html(s); // 3.
            self.adjust_error_messages(response);
            if ( !$(s).find('.fieldWithErrors').length ){
              restripe(); // 3a. (it's a hack don't judge me)
            }
            reorder_toggle.show();
            $('.edit-button').show();
            self.get_song_form();
            $('#add-songs').show();
            // IF Error (ELSE do nothing after this happens)
            song_item.find('.save-button').attr('rel', 'edit-form');
            $('.cancel-button').click(function(){
              reorder_toggle.show();
              $('.edit-button').show();
              self.get_song_form();
              $('#add-songs').show();
              $.get(
                $(s).find('.edit_song').attr('action'),
                function(response){
                  s = $(response).children().unwrap();
                  song_item.html(s).effect('highlight', {}, 1000); // 3.
                  restripe(); // 3a.
                },
                'html'
              );
              $('.save-button[rel=\'edit-form\']').hide();
              $('#song_Save_list_item').append('<img src="/images/icons/spinner-24x24.gif" alt="" style="margin-top:4px" />');
              $('.cancel-button').remove();
              return false;
            });
            $(document).trigger('song_update');
          },
          'html'
        );
      }
      return false;
    },

    //  Click Upload Button
    //  1. Ajax GET upload song form
    //    a. Hide all other upload buttons if multiple uploads not allowed
    //    b. Add count to allowed multiple uploads if it is allowed
    //    c. Hide add song form and add song toggle
    //  2. Replace song item with upload song form
    //
    //  Multiple uploads can be toggled on and off by setting the variable TC.allowMultipleUploads
    //  (currently set at the top of the album/show.erb file)
    upload_song: function(){
      reorder_toggle.hide();
      var this_button = $(this);
      var href = this_button.attr('href');
      var selector = this_button.parents('li');

      // Hide all upload buttons if TC.allowMultipleUploads is false
      // or if it is allowed, do not allow more than 20
      // TC.MultipleUploadsAllowed is the variable that can be set to
      //   change the maximum number of uploads allowed at a time.
      //   (currently set at the top of the album/show.erb file)
      if (TC.allowMultipleUploads === false) {
        $('.upload-button').hide();// a.
      } else {
        // b. Add count to allowed multiple uploads if it is allowed
        MU += 1;
        if (MU == TC.MultipleUploadsAllowed) {
          $('.upload-button').hide();
        }
      }

      $('.edit-button').hide();
      $('.delete-button').hide();
      self.hide_add_song_form(); // c.
      show_form_toggle.hide(); // c.
      this_button.show(); // Only show current button so we can have a spinner
      this_button.html('<img src="/images/icons/spinner-16x16-grn.gif" alt="" style="margin-top:4px" />');

      $.get( // 1.
        href,
        function(response){
          var s = $(response).children().unwrap();
          selector.html(s); // 2.
        },
        'html'
      );

      return false;
    },

    //  Click Edit Button
    //  1. Cache song item
    //  2. Ajax GET edit song form
    //  3. Replace song item with edit song form
    //  3a. Hide all other edit buttons and the add song button
    //  3b. Change attributes of the spinner and save button
    //  4. If cancel from form, replace with cached song item
    //  4a. Show hidden controls
    edit_song: function(){
      reorder_toggle.hide();
      var href = $(this).attr('href');
      var selector = $(this).parents('li');
      selector.removeClass('odd even');

      var songitem = selector.html(); // 1.
      $(this).html('<img src="/images/icons/spinner-16x16.gif" alt="" style="margin-top:6px" />');
      $(this).parents('.rollover-hide').css('display','inline');

      $.get( // 2.
        href,
        function(response){
          selector.html(response); // 3.
          song_form.hide();
          show_form_toggle.show();
          $('.edit-button').hide(); // 3a.
          $('#add-songs').hide(); // 3a.
          song_form.html('');
          selector.find('#spinner').attr('id', 'edit-spinner'); // 3b.
          selector.find('.save-button').attr('rel', 'edit-form'); // 3b.
          selector.find('#song_Save_list_item').after('<li><a href="#" class="cancel-button">Cancel</a></li>');
          $('.cancel-button').click(function(){
            selector.html(songitem); // 4.
            reorder_toggle.show(); // 4a.
            $('.edit-button').show(); // 4a.
            self.get_song_form();
            $('#add-songs').show(); // 4a.
            restripe();
            return false;
          });
        },
        'html'
      );
      return false;
    },

    //  Click Delete Button
    //  1. -Hide list item- Not necessary if we're rebuilding track list
    //     Confirm delete with alert
    //  2. Ajax POST
    //  3. Update track_list with response
    delete_song: function(){
      var href = $(this).attr('href');
      var song_name = $(this).parents('li').find('.song-name').html();
      if ( confirm(gon.confirm_deletion + ': \'' + song_name + '\'?') ){
        $(this).parents('li').fadeOut(500, function(){ $(this).remove(); });
        if ( (track_list.children('li').length-1) > 1) {
          reorder_toggle.text('Reorder Songs');
        } else {
          reorder_toggle.text(' ');
        }
        $.post(
          href.replace('/delete', ''),
          { '_method': 'delete' },
          self.reload_track_list,
          'html'
        );
      }
      updateChecklist();
      return false;
    },

    //  After sending POST for delete a song
    //  or reordering tracks,
    //  we replace the track list with the response
    reload_track_list: function(response){
      track_list.html(response).effect('highlight', {}, 1000);
      if ( !track_list.children('li').length ) {
        track_list.hide();
      } else {
        reorder_toggle.show();
      }
      $(document).trigger('song_destroy');
    },

    //  Using an ajax request, grab the song form
    //  from the new song action.
    get_song_form: function(f){
      $.get(
        window.location.href+'/songs/new',
        function(response){
          song_form.html(response);
          self.add_done_link();
          if (typeof f == 'function') f();
        },
        'html'
      );
    },

    // sortable list controls
    // upon init, binds the toggle control to the Reorder Tracks link, and makes the list sortable

    init_sortable: function(){
      self.toggle_reorder();
      track_list.sortable({
        update: self.finished_sort,
        handle: '.handle'
      });
    },

    // callback function from sortable track list, serializes and submits AJAX data to reorder tracks

    finished_sort: function(){
      var form = track_list.parents('form');
      var href = form.attr('action');
      var data = form.serialize();
      reorder_toggle.hide();
      $('.handle').html('<img src="/images/icons/spinner-16x16.gif" alt="" />');
      reorder_toggle.hide();
      self.save_sort_submit(href,data);
    },

    // callback rebuilds the sorted list

    save_sort_submit: function(href,data){
      $.post(
        href,
        data,
        self.after_save_sort,
        'html'
      );
    },

    // housekeeping on the now-sorted song list

    after_save_sort: function(response){
      self.reload_track_list(response);
      $('.track-actions').hide();
      $('.song-data').animate({'left':'40px'},1);
      $('.handle').show();
      reorder_toggle.show();
      $(document).trigger('song_update');
    },


    // toggling the Reorder Songs link moves the song listings over to bring in the handles
    // and changes the link text to Done
    // and hides/shows the entire add-songs form or button, as well as internal song controls to focus
    // the user on reordering tracks.
    // we COULD include delete on these. Maybe.
    //

    toggle_reorder: function() {
      reorder_toggle.toggle(function() {
        $('.song-data').animate({'left': '40px'}, 100, function() { $('.handle').show(); $('.track-actions').fadeOut(100); });
        $(this).text('Done Reordering');
        $('#add-songs').hide();
      }, function() {
        $('.handle').hide();
        $('.song-data').animate({'left': '15px'}, 100, function() { $('.track-actions').fadeIn(100); });
        $(this).text('Reorder Songs');
        $('#add-songs').show();
      });
    },

    disable_all_features: function() {
      $('.upload-button').remove();
      $('.edit-button').remove();
      $('.delete-button').remove();
      $('#reorder-toggle').remove();
      $('#show-form-toggle').remove();
    },

    // Show all creatives - housekeeping when adding subsequent songs from one form
    show_creatives: function(){
      TC.creatives.show.call();
    },

    // Hide all creatives - housekeeping when adding multiple songs from one form
    hide_creatives: function(){
      TC.creatives.hide.call();
      $('input#song_creatives__name').not('.artist-name').val('');
      $('#multiple-artists-link').removeClass('hide');
    }
  };
  return self.init();
};

jQuery(document).ready(function(){
  TC.song_album = new SongAlbum();
});

//  After response of uploaded song from flash uploader
//  render the show song partial again
var show_song = function(song_id){
  var $ = jQuery;
  var song_path = $('#' + song_id).find('#song-path').val();

  // Subtract count to allowed multiple uploads if it is allowed
  if (TC.allowMultipleUploads === true) {
    if (MU > 0) {
      MU -= 1;
    } else {
      MU = 0;
    }
  }

  $.get(
    song_path,
    function(response){
      s = $(response).children().unwrap();
      $('#' + song_id).html(s).effect('highlight', {}, 1000);
      // Restripe
      $('#track-list li').removeClass('odd');
      $('#track-list li:nth-child(odd)').addClass('odd');
      $('#track-list li:nth-child(even)').addClass('even');
      // Show hidden toggles and buttons
      $('#reorder-toggle').show();
      $('.upload-button').show();
      $('#show-form-toggle').show();
      $('.edit-button').show();
      $('.delete-button').show();
    },
    'html'
  );
  TC.song_album.updateChecklist();
};
