jQuery(function() {
  $('.bxslider').bxSlider({
    minSlides: 1,
    maxSlides: 1,
    slideMargin: 0,
    slideWidth: 917,
    prevSelector: '.prev',
    prevText: '<i class="fa fa-chevron-left fa-2x"></i>',
    nextSelector: '.next',
    nextText: '<i class="fa fa-chevron-right fa-2x"></i>',
    pagerCustom: '.dots'
  });
});
