
/*
 *  Bandpages:
 *
 *  A simple object to encapsulate bandpages creation javascript logic
 *
 */
var Bandpages = function(){

  //
  //  Page style definitions
  //
  var form_input = ".context-wrapper input";
  var $ = jQuery;

  var self = {

    //
    //  Setup Form Elements
    //
    init: function(){
      $(form_input).focus(this.show_helper_text);
      $(form_input).blur(this.hide_helper_text);
    },
    
    show_helper_text: function(){
      var help = $(this).next('.contextual-help');
      if (help.find('p').text().length !== 0){
        help.removeClass('hidden');
      };
    },

    hide_helper_text: function(){
      $(".contextual-help").addClass("hidden");
    }
  };

  return self.init();
};

// Initialize on document ready 
jQuery(document).ready( function(){
  new Bandpages();
});