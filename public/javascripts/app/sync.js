jQuery(document).ready(function(){

  window.TC = window.TC || {};

  var SyncSearch = function() {
    // Set up $ to be used locally for jQuery then
    // cache selectors so you don't have to traverse
    // the DOM each time.
    var $ = jQuery;
    var isAdmin = $('.isAdmin').length > 0;
    var $search_form = $("#search_form");
    var $search_submit = $("#submit_search_link");
    var $results = $("#results");
    var $results_found = $("#results_found");
    var $pagination = $("#searchResultPagination");
    var $facets = $('#facet-select');
    var catSelect = $('#q_field');
    var searchInput = $('#q');
    var term = sessionStorage.getItem('term') || '';
    var genres = function(){
      var g = [];
      $.each(JSON.parse($('#data').html()), function(i,v){
        if(v.genre.is_active) g.push((v.genre.name == 'Fitness &amp; Workout') ? 'Fitness & Workout' : v.genre.name);
      });
      return g;
    }();

    var self = {
      init: function() {
        $search_submit.on('click', function(e) {
          e.preventDefault();
          $(".sync_search-table").show();
          delete self.filter_facets;
          self.submit_request(1);
        });

        self.init_favorites_tip();
        self.init_search_results();
        self.init_sound_manager();
        self.bind_facet_links();
        self.bind_facet_removal_links();
        self.bind_category_select_change();
        self.bindSyncTipToggle();
        self.bindSetAndSearch();
        self.on_category_select();
      },

      bind_category_select_change: function(){
        catSelect.on('change', function(){
          self.on_category_select();
        });
      },

      on_category_select: function(){
        (catSelect.val() == 'genre') ? self.bind_genre_auto_complete() : self.destroy_genre_auto_complete();
      },

      bind_genre_auto_complete: function(){
        searchInput.autocomplete({
          source: genres
        });
      },

      destroy_genre_auto_complete: function(){
        searchInput.autocomplete('destroy');
      },

      init_favorites_tip: function(){
        var showFavoritesTip = !parseInt(localStorage.getItem('hide_favorite_tip')||!1);
        if(showFavoritesTip) $('.sync_search-favorites_help .sync_tip').fadeIn('slow');
      },

      init_search_results: function(){
        var data = sessionStorage.getItem('search_results');
        if(!!data){
          searchInput.val(term);
          $('.sync_search-table').show();
          self.show_results(JSON.parse(data));
        }
      },

      init_sound_manager: function(){
        self.sm = new TC.SM();
      },

      submit_request: function(page) {
        term = searchInput.val();
        sessionStorage.setItem('term', term);
        if (!self.is_blank(term)) {
          $facets.empty();
          form_data = $search_form.serializeArray();
          form_data.push({name: "pg", value: page});
          if (!$.isEmptyObject(self.filter_facets)) {
            form_data.push({name: "facets", value: JSON.stringify(self.filter_facets)});
            delete self.filter_facets;
          }

          self.add_spinner();
          $pagination.hide();
          $results_found.hide();

          $.getJSON($search_form.attr('action'), form_data, function(data, status) {
            sessionStorage.setItem('search_results', JSON.stringify(data));
            self.show_results(data);
          });
        }
      },

      show_results: function(data){
        $results_found.html("Results Found: " + data.query.hits.found).show();
        $results.find("tr:gt(0)").remove();
        if (data.songs.length) {
          var html = '';
          $.each(data.songs, function(key, val) {
            var song = val.song,
                master_share = song.album_takedown ? 0 : 100,
                link = $('#results').data('quote') + '?song_id=' + song.id,
                favorite_link = $('#results').data('favorite') + '?song_id=' + song.id,
                favorite_class = song.is_favorite == "1" ? 'favorite' : '',
                favorite_icon = song.is_favorite == "1" ? 'fa fa-star' : 'fa fa-star-o';
                sound = (song.stream_url || '').replace(/\.dev/, ''),
                soundHtml = '',
                genComposerLinks = function(composers){
                  var h = '';
                  $.each(composers, function(){
                    h += '<a href="#" class="setAndSearch" category="composers" search="'+this+'">'+this+'</a><br>';
                  });
                  return h;
                },
                parental = (song.parental_advisory && isAdmin) ? '<br><img src="/images/explicit.png">' : '',
                adminLink = (isAdmin) ? '<br><a href="/admin/people/'+song.person_id+'" target="_blank">(admin)</a>' : '';
                if(!!sound) soundHtml += '<span class="sm_sound" sound="'+sound+'" sm_player-song_id="'+song.id+'" sm_player-is_favorite="'+song.is_favorite+'" sm_player-artwork="'+song.artwork_url+'" sm_player-title="'+song.name+'" sm_player-artist_name="'+song.artist_name+'" sm_player-album="'+song.album_name+'" sm_player-composers="'+song.composer_names.join(", ")+'" sm_player-label_copy="'+song.label_copy+'" sm_player-composer_percentage="'+Math.round(song.mech_collect_share)+'"></span>';
                html += ('<tr> <td><a class="favorite_link ' + favorite_class + '" href="' + favorite_link  + '"><i class="' + favorite_icon +'"></i></a></td>' +
                              '<td style="padding:none;"><img src="'+song.artwork_url+'" width="80" height="80" style="padding:none;margin:none;"></td>' +
                              '<td ><table class="sync_search-ghost-table"><tr><td style="width: 25px;">' + soundHtml + '</td><td>' + song.name + song.formatted_duration + parental + '</td></tr></table></td>' +
                              '<td><a href="#" class="setAndSearch" category="artist" search="'+song.artist_name+'">' + song.artist_name        + '</a></td>' +
                              '<td>' + genComposerLinks(song.composer_names) + adminLink    + '</td>' +
                              '<td>' + song.primary_genre      + '</td>' +
                              '<td><span class="control control-full">' + master_share + '</span> <span class="control control-partial">' + Math.round(song.mech_collect_share) + '</span></td>'
                        ).replace(new RegExp('('+term+')(?=[^>]*<)', 'ig'), '<span class="sync_search-highlight">$1</span>') +
                        '<td><a href="' + link + '" class="button">Get&nbsp;Quote</a></td></tr>';
                });
            $results.find("tbody").html(html);
        } else {
          $results.find("tbody").html( '<tr><td colspan="8">No results found.</td></tr>');
        }
        $(".sync_search-favorites_help").show();
        self.parse_filter_facets(data.query["match-expr"]);
        self.parse_result_facets(data.query.facets);
        $pagination.pagination({
          items: data.query.hits.found,
          itemsOnPage: 10,
          currentPage: data.page,
          cssStyle: 'sync_search-pagination',
          onPageClick: function(page) {
            self.submit_request(page);
          }
        });

        // hide and show the pagination controls based on search results
        (data.query.hits.found < 10) ? $pagination.hide() : $pagination.show();
      },

      add_spinner: function() {
        $results.find('tbody').html('<tr><td colspan="8" style="text-align: center;"><img src="/images/spinner02.gif" alt="" /></td></tr>');
      },

      parse_filter_facets: function(query) {
        var skip = false;

        // if we have a search field selected, we need to skip the
        // first filter so we don't give the appearance the user has
        // already selected a facet
        if (catSelect.val() !== "") {
          skip = true;
        }
        if (query) {
          self['filter_facets'] = {};
          index = query.indexOf(":");

          while (index >= 0) {
            var process = query.slice(0, index).split(" ");
            var key = process[process.length - 1];

            var slice_index = query.indexOf("'", index + 2);
            var val = query.substr(index + 2, slice_index - (index + 2));

            if (!skip) {
              if (self.filter_facets.hasOwnProperty(key)) {
                self.filter_facets[key].push(val);
              } else {
                self.filter_facets[key] = [val];
              }
            }

            query = query.slice(slice_index);
            index = query.indexOf(":");
            skip = false;
          }
        }
      },

      parse_result_facets: function(facets) {
        if (facets) {
          var group_of_facets = [];
          $.each(facets, function(key, val) {
            if (val.constraints && key != 'percent') {
              var single_facet = [];
              var group_of_constraints = [];
              single_facet.push("<h3>" + key + "</h3>");
              single_facet.push("<ul>");

              $.each(val.constraints, function(val_k, val_v) {
                if (self.filter_facets.hasOwnProperty(key) && ($.inArray(val_v.value, self.filter_facets[key]) != -1)) {
                  group_of_constraints.push('<li><b><a class="remove" data-facet="' + val_v.value + '" data-cat="' + key + '" href="#">' + val_v.value + ' <i class="fa fa-minus-circle"></i></a></b></li>');
                } else {
                  group_of_constraints.push('<li><a class="filter" data-facet="'+ val_v.value +'" data-cat="' + key + '" href="">' + val_v.value + ' (' + val_v.count + ')</a></li>');
                }
              });

              single_facet.push(group_of_constraints.join(''));
              single_facet.push("</ul>");
              group_of_facets.push("<section class='sync_search-filter_type'>" + single_facet.join('') + "</section>");
            }
          });
          $facets.html(group_of_facets.join(''));
          self.truncate_list_of_facet_links();
        }
      },

      bind_facet_links: function() {
        $facets.on('click', 'a.filter', function(e) {
          e.preventDefault();

          var cat = $(this).attr("data-cat");
          var val = $(this).attr("data-facet");

          if (!self.hasOwnProperty('filter_facets')) {
            self['filter_facets'] = {};
          }

          if (self.filter_facets.hasOwnProperty(cat)) {
            if ($.inArray(val, self.filter_facets[cat]) == -1) {
              self.filter_facets[cat].push(val);
            }
          } else {
            self.filter_facets[cat] = [val];
          }

          self.submit_request(1);
        });
      },

      bind_facet_removal_links: function() {
        $facets.on('click', 'a.remove', function(e) {
          e.preventDefault();

          var cat = $(this).attr("data-cat");
          var val = $(this).attr("data-facet");
          var facets = self.filter_facets[cat];

          facets.splice(facets.indexOf(val), 1);
          self.submit_request(1);
        });
      },

      truncate_list_of_facet_links: function() {
        var filter_containers = $facets.find('.sync_search-filter_type');

        filter_containers.each(function() {
          var container = $(this),
              item_list = container.find('ul'),
              items = item_list.find('li'),
              truncated_items,
              ary = [];

          if (items.length > 5) {
            truncated_items = items.filter(function(index) {
              return index > 4;
            }).remove();
            item_list.append('<li class="more"><a href="#"><i class="fa fa-plus-circle"></i>More</a></li>');

            ary.push('<div class="sync_search-more_filters"><ul>');
            truncated_items.each(function() { ary.push( '<li>' + $(this).html() + '</li>' ); });
            ary.push('</ul></div>');
            container.append(ary.join(''));
          }
        });

        filter_containers.on('click', '.more', function(e) {
          e.preventDefault();
          var link = $(this),
              list = link.closest('.sync_search-filter_type').find('.sync_search-more_filters');

          if (link.hasClass('less')) {
            link.removeClass('less');
            link.find('a').html('<i class="fa fa-plus-circle"></i>More');
            list.hide();
          } else {
            link.addClass('less');
            link.find('a').html('<i class="fa fa-minus-circle"></i>Less');
            list.show();
          }
        });
      },

      is_blank: function(str) {
        return (!str || /^\s*$/.test(str));
      },

      bindSyncTipToggle: function(){
        var t = this;
        $('.syncTipToggle').on('click', function(e){
          var tog = $(this);
          if(tog.attr('rel') == 'favorite') localStorage.setItem('hide_favorite_tip', 1);
          e.preventDefault();
          t.toggleSyncTip();
        });
        return this;
      },

      toggleSyncTip: function(){
        var tip = $('.sync_tip'),
            link = $('.syncTipToggle:first');
        tip.toggle();
        link.toggle();
        return this;
      },

      setAndSearch: function(category, search){
        self['filter_facets'] = {};
        catSelect.val(category);
        searchInput.val(search);
        self.submit_request(1);
        return this;
      },

      bindSetAndSearch: function(){
        $(window).on('click', '.setAndSearch', function(e){
          var t = $(this);
          self.setAndSearch(t.attr('category'), t.attr('search'));
          e.preventDefault();
        });
      }

    };

    return self.init();
  };

  TC.sync_search = new SyncSearch();
});
