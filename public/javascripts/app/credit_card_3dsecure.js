$(() => {
  const clientToken = gon.braintreeToken;
  ThreeDSecure.initialize(clientToken);
  CreditCardForm.bindAddCardClickHandler();
});

/**
 * I18n deals with translations
 */
const I18n = {
  translations: (() => gon.translations || {})(),

  t(key) {
    const translations = I18n.translations || {};
    return translations[key] || "";
  },
};

/**
 * CreditCardForm deals with any interaction with the form or page elements.
 */
const CreditCardForm = {
  disableTriggerButton(bool = true) {
    $("#add-card-button, #update-card-button").prop("disabled", bool);
  },

  bindAddCardClickHandler() {
    $("#add-card-button, #update-card-button").on("click", async () => {
      const threeDSecureEnabled =
        ThreeDSecure.enabled &&
        ThreeDSecure.enabledForUser &&
        ThreeDSecure.enabledForSelectedCountry();

      if (threeDSecureEnabled) {
        CreditCardForm.threeDSecureVerificationFlow();
      } else {
        CreditCardForm.regularCardSubmissionFlow();
      }
    });
  },

  async threeDSecureVerificationFlow() {
    try {
      CreditCardForm.disableTriggerButton(true);
      ModalHelpers.open("#verification-modal");
      const result = await ThreeDSecure.verify();
      if (result) {
        ModalHelpers.closeAll();
        CreditCardForm.submit();
      }
    } catch (error) {
    } finally {
      CreditCardForm.disableTriggerButton(false);
    }
  },

  async regularCardSubmissionFlow() {
    try {
      CreditCardForm.disableTriggerButton(true);
      const tokenizedCard = await ThreeDSecure.tokenizeCard();
      CreditCardForm.addBraintreeNonceToForm(tokenizedCard.nonce);
      // 3DSecure parameters must be removed so that proper flow is
      // followed at API
      CreditCardForm.remove3DSecureDataFromForm();
      CreditCardForm.submit();
    } catch (error) {
      ModalHelpers.closeAll();
      ModalHelpers.setContentsAndShow(
        "#error-modal",
        `${I18n.t("tokenize_error")} ${I18n.t("try_again")}`
      );
      return false;
    } finally {
      CreditCardForm.disableTriggerButton(false);
    }
  },

  getCountryDataParams() {
    return $("#stored_credit_card_country").data() || {};
  },

  getFormData() {
    return {
      firstName: $("#stored_credit_card_first_name").val(),
      lastName: $("#stored_credit_card_last_name").val(),
      company: $("#stored_credit_card_company").val(),
      address1: $("#stored_credit_card_address1").val(),
      address2: $("#stored_credit_card_address2").val(),
      city: $("#stored_credit_card_city").val(),
      state: $("#stored_credit_card_state").val(),
      zipCode: $("#stored_credit_card_zip").val(),
      country: $("#stored_credit_card_country").val(),
    };
  },

  addBraintreeNonceToForm(nonce) {
    $("#braintree_payment_nonce").val(nonce);
  },

  add3DSecureDataToForm({ nonce, authenticationId }) {
    $("#three_d_secure_nonce").val(nonce);
    $("#braintree_3dsecure_auth_id").val(authenticationId);
  },

  remove3DSecureDataFromForm() {
    $("#three_d_secure_nonce").val(null);
    $("#braintree_3dsecure_auth_id").val(null);
  },

  submit() {
    $("form.credit_card_form")[0].submit();
  },
};

/**
 * ThreeDSecure deals with hosted fields, card verification and data processing for 3DS
 */
const ThreeDSecure = {
  initialized: false,
  hf: null,
  threeDS: null,
  enabled: (() => $("#three_d_secure_nonce").length > 0)(),
  enabledForUser: (() =>
    $("input[type='hidden'][name='affiliated_to_bi']").val() === "true")(),

  enabledForSelectedCountry: () => {
    const { country } = CreditCardForm.getFormData();
    const data = CreditCardForm.getCountryDataParams();
    const validCountries =
      data && data.threeDSecureCountries ? data.threeDSecureCountries : [];
    return (validCountries || []).includes(country);
  },

  initializeHostedFields(clientToken) {
    return braintree.hostedFields.create({
      authorization: clientToken,
      styles: {
        input: {
          "font-size": "12px",
          "font-family": "'Helvetica Neue', Helvetica, Arial, Sans-serif",
        },
      },
      fields: {
        number: { selector: "#hf-number", placeholder: "" },
        cvv: { selector: "#hf-cvv", placeholder: "" },
        expirationDate: { selector: "#hf-date", placeholder: "**/****" },
      },
    });
  },

  initializeThreeDSecure(clientToken) {
    return braintree.threeDSecure.create({
      authorization: clientToken,
      version: 2,
    });
  },

  async initialize(clientToken) {
    try {
      const [hostedFields, threeDSecure] = await Promise.all([
        ThreeDSecure.initializeHostedFields(clientToken),
        ThreeDSecure.initializeThreeDSecure(clientToken),
      ]);

      ThreeDSecure.hf = hostedFields;
      ThreeDSecure.threeDS = threeDSecure;
      ThreeDSecure.initialized = true;
    } catch (error) {
      ThreeDSecure.initialized = false;
      ModalHelpers.closeAll();
      ModalHelpers.setContentsAndShow("#error-modal", I18n.t("3ds_init_error"));
    }
  },

  getAdditionalData() {
    const formData = CreditCardForm.getFormData();
    return {
      givenName: formData.firstName || "",
      surname: formData.lastName || "",
      streetAddress: formData.address1 || "",
      extendedAddress: formData.address2 || "",
      locality: formData.city || "",
      // Region has to be the 2 letter code for US states or an ISO-3166-2 country subdivision code of up to three letters.
      // https://braintree.github.io/braintree-web/current/ThreeDSecure.html#~billingAddress
      // region: formData.state || "",
      postalCode: formData.zipCode || "",
      countryCodeAlpha2: formData.country || "",
    };
  },

  validateAdditionalData(data) {
    return data;
  },

  async tokenizeCard() {
    return ThreeDSecure.hf.tokenize();
  },

  async verifyCard(tokenizedCard, billingAddress) {
    return ThreeDSecure.threeDS.verifyCard({
      challengeRequested: true,
      amount: "0.00",
      nonce: tokenizedCard.nonce,
      bin: tokenizedCard.details.bin,
      billingAddress,
      onLookupComplete: function (data, next) {
        next();
      },
    });
  },

  onCardSuccessfullyVerified(payload) {
    if (!payload.liabilityShifted) {
      ModalHelpers.closeAll();
      ModalHelpers.setContentsAndShow(
        "#error-modal",
        `${I18n.t("verification_failed")} ${I18n.t("try_again")}`
      );
      return false;
    }

    CreditCardForm.add3DSecureDataToForm({
      nonce: payload && payload.nonce ? payload.nonce : undefined,
      authenticationId:
        payload &&
        payload.threeDSecureInfo &&
        payload.threeDSecureInfo.threeDSecureAuthenticationId
          ? payload.threeDSecureInfo.threeDSecureAuthenticationId
          : undefined,
    });
    return true;
  },

  getError(message) {
    const cleanedMessage = message.toLowerCase().trim();
    const errors = ThreeDSecure.errors;
    for (let i = 0; i < errors.length; ++i) {
      const text = errors[i].message;
      if (cleanedMessage === text.toLowerCase()) {
        return errors[i];
      }
    }

    return null;
  },

  parseErrors(errorObject) {
    try {
      const response =
        errorObject.details.originalError.details.originalError.error.message;
      const messages = response.split("\n");
      const errors = messages.map(ThreeDSecure.getError);
      const filtered = errors.filter(Boolean);
      return filtered || [];
    } catch (error) {
      return [];
    }
  },

  getErrorTranslations(errors) {
    const translations = errors.map(({ t_key }) => I18n.t(t_key));
    return translations;
  },

  createErrorContent(errorTranslations) {
    // generic error message
    if (!Array.isArray(errorTranslations) || errorTranslations.length === 0) {
      return `${I18n.t("verification_failed")} ${I18n.t("try_again")}`;
    }

    const list = errorTranslations.map((t) => `<li>${t}</li>`);

    return `
      <ul>
        ${list.join("")}
      </ul>
    `;
  },

  async verify() {
    try {
      if (!ThreeDSecure.initialized) {
        throw new Error("3DSecure was not initialized");
      }

      const additionalData = ThreeDSecure.getAdditionalData();
      const data = ThreeDSecure.validateAdditionalData(additionalData);
      const tokenizedCard = await ThreeDSecure.tokenizeCard();
      const payload = await ThreeDSecure.verifyCard(tokenizedCard, data);
      return ThreeDSecure.onCardSuccessfullyVerified(payload);
    } catch (error) {
      const errors = ThreeDSecure.parseErrors(error);
      const translations = ThreeDSecure.getErrorTranslations(errors);
      const errorContent = ThreeDSecure.createErrorContent(translations);

      ModalHelpers.closeAll();
      ModalHelpers.setContentsAndShow("#error-modal", errorContent);
      return false;
    }
  },

  // Braintree does not provide error code as response to JavaScript three-d-secure verifyCard
  // Only error message text is provided.
  // codes from https://developer.paypal.com/braintree/docs/reference/general/validation-errors/all
  errors: [
    {
      code: "94504",
      message: "Unsupported card type",
      t_key: "unsupported_card",
    },
    {
      code: "94524",
      message: "Billing phone number format is invalid.",
      t_key: "billing_phone_number_invalid",
    },
    {
      code: "94527",
      message: "Billing postal code format is invalid.",
      t_key: "billing_postal_code_invalid",
    },
    {
      code: "94529",
      message: "Billing given name format is invalid.",
      t_key: "billing_given_name_invalid",
    },
    {
      code: "94530",
      message: "Billing surname format is invalid.",
      t_key: "billing_surname_invalid",
    },
    {
      code: "94531",
      message: "Billing line1 format is invalid.",
      t_key: "billing_line1_invalid",
    },
    {
      code: "94532",
      message: "Billing line2 format is invalid.",
      t_key: "billing_line2_invalid",
    },
    {
      code: "94534",
      message: "Billing line3 format is invalid.",
      t_key: "billing_line3_invalid",
    },
    {
      code: "94535",
      message: "Billing city format is invalid.",
      t_key: "billing_city_invalid",
    },
    {
      code: "94536",
      message: "Billing state format is invalid.",
      t_key: "billing_state_invalid",
    },
    {
      code: "94538",
      message: "Billing country code format is invalid.",
      t_key: "billing_country_code_invalid",
    },
  ],
};
