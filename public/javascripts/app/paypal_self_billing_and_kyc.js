var $ = jQuery;

$(function () {
  if (typeof addressInfoValidations === "function") {
    addressInfoValidations();
  }
});

function onCheckboxClicked(checkbox) {
  $("#self-billing-required-error").hide();
  $("#self-billing-customer-care-error").hide();

  if (checkbox.checked) {
    $("#modal-bind")
      .magnificPopup({
        items: {
          src: "#self-billing-positive-modal",
        },
        closeOnContentClick: false,
        closeOnBgClick: false,
        showCloseBtn: true,
        enableEscapeKey: false,
        midClick: true,
        callbacks: {
          open: function () {
            $("#self-billing-positive-modal").on(
              "click",
              ".mfp-close",
              function () {
                $("#self-billing-required-error").hide();
                $("#self-billing-customer-care-error").show();
                $(checkbox).prop("checked", false);
              }
            );
          },
        },
      })
      .magnificPopup("open");
  }
}

function selfBillingKYCFormSubmitHandler(event, form) {
  const selfBillingCheckBox = $(form).find(
    ".self-billing-component input[type='checkbox']"
  )[0];

  if (selfBillingCheckBox && !selfBillingCheckBox.checked) {
    event.preventDefault();
    event.stopPropagation();

    const formErrorElement = $("#self-billing-required-error");

    formErrorElement.show();
    if (formErrorElement.hasClass("hide")) {
      formErrorElement.removeClass("hide");
    }
  }
}

function closeModal(event) {
  if (event) {
    event.preventDefault();
  }
  $.magnificPopup.close();
}
