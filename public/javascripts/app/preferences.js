/*
* Payment Preferences Page
*
*
* [Feb 02 2010 MAT] Converting existing logic into a single-page constructor function
*
*/

var PaymentPreferences = function(){
  var $ = jQuery;
  var pay_with_balance_cb = $("#pay_with_balance");

  pay_with_balance_cb.on("click", function (){
    $.ajax({
      url: $(".edit_person_preference").attr("action"),
      type: 'PATCH',
      dataType: 'json',
      data: $('.edit_person_preference').serialize()
    });
  });
};

jQuery(document).ready(function(){
  var prefs = new PaymentPreferences();
});
