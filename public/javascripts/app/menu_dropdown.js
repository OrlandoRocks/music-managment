/* Menu_DropDown.js
  = Description
  The following code allows the drop-down menus to have a delay for opening and closing it.
  
  = Change log
  [2011-01-28 -- RT] Initial Creation
  
  = Dependencies
  Depends on the jquery.hoverIntent plugins.
  
  = Usage
  Not a function to be used outside this file.
  
*/
var TC = TC || {};
var menu_dropdown = function () {
  var $ = jQuery;
  var toplevel = $("li.top-level");
  var balancelink = $("li.balance-link");
  var $messages = $("#goodErrorExplanation");
  
  var self = {
    init: function () {
      self.menu_delay( toplevel );
      self.menu_delay( balancelink );
      self.vanish_alerts();
    },
    
    vanish_alerts: function() {
      if ($messages.length) {
        $messages.fadeOut(3000).slideUp(1000, function() {
          $messages.parent(".row").slideUp();
        });
      }
    },
    
    menu_delay: function (menu) {
      $(menu).hoverIntent({
        interval: 150,
        over: drops_show,
        timeout: 500,
        out: drops_hide
      }).addClass('with-js');
      
      function drops_show () { $(this).addClass('show').removeClass('with-js'); }
      function drops_hide () { $(this).removeClass('show').addClass('with-js'); }
    }
  };
  return self.init();
};

jQuery(function(){
  TC.menu_dropdown = new menu_dropdown();
});
