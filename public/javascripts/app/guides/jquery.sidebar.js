var name                    = "#leftnav";
var sidebar_top_limit       = 0;
var sidebar_top_margin      = -120;
var sidebar_slide_duration  = 320;
var $ = jQuery;

$(window).scroll(function() {
 
  offset = $(document).scrollTop() + sidebar_top_margin;
   
  if(offset < sidebar_top_limit)
      offset = sidebar_top_limit;
   
  //$(name).animate({top:offset},{duration:sidebar_slide_duration,queue:false});
  $(name).css('top', offset); // seems to animate any how because browser fires a bunch of window scroll events.  The animate seemed to freeze stuff up because of duration lag.
  
});

jQuery(document).ready(function () {
  var top = $('.main').offset().top;
  
  var scroll_nav = function() {
    // what the y position of the scroll is
    var y = $(this).scrollTop();
    
    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('nav').addClass('fixed');
    } else {
      // otherwise remove it
      $('nav').removeClass('fixed');
    }
  };
  
  var check_nav = function() {
    var inview = '#' + $('.main > section:in-viewport:first > h1').parent().attr('id'),
      $link = $('.sidebar > nav a').filter(function() {
          return this.hash == inview;
      });
      
      console.log("inview", inview, "link", $link);
    if ($link.length && !$link.parent().is('.active')) {
      $('.sidebar > nav a').parent().removeClass('selected');
      $link.parent().addClass('selected');
    }
  };
  
  scroll_nav();
  check_nav();
  
  $(window).scroll(function (event) {
    // main section should have the same offset as the sidebar nav
    // we don't want to rely on the sidebar nav offset because we are moving it
    top = $('.main').offset().top;
    
    scroll_nav();
    check_nav();
  });
});
