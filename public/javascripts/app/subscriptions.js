/*
* Interactions for Subscriptions and Renewals page
*
* [Feb 2, 2010 MAT] Initial creation
*/
var Subscription = function(){
  var $ = jQuery;
  var tabs = $('#tabs');

  var self = {
    init: function(){
      self.init_tabs();
    },
    
    init_tabs: function(){
      tabs.tabs();
    }
  };
  
  return self.init();
};

jQuery(document).ready(function(){
  new Subscription;
});