jQuery(document).ready(function($){

  /*
   * Country Selector
  */
  var cs = {

    init: function(){
      cs.bind.all();
      cs.updateHiddenInput();
      if(ShowUpgradeDialog){
        $("#upgradeDialog").modal({
          maxWidth: 300,
          minWidth: 300,
          maxHeight: 180,
          minHeight: 180,
          persist: true
        });
      }
    },

    // Data Storage
    data: {
      worldwide: (function(){
        return CountrySelectorCountriesJSON.length == CountrySelectorCountryCodesSelectedJSON.length
      })(),
      countries: (function(){
        var countries = {};
        $.each(CountrySelectorCountriesJSON, function(){
          countries[this[1]] = this[0];
        });
        return countries;
      })(),
      countryCodes: (function(){
        var codes = [];
        $.each(CountrySelectorCountriesJSON, function(){
          codes.push(this[1]);
        });
        return codes;
      })(),
      countryCount: CountrySelectorCountriesJSON.length,
      selectedCodes: CountrySelectorCountryCodesSelectedJSON,
      state: (function(){
        // Assuming that if all country codes are selected, they have not made a change and are not in a state
        if(CountrySelectorCountriesJSON.length == CountrySelectorCountryCodesSelectedJSON.length) null;
        else return ((CountrySelectorCountriesJSON.length / 2) > CountrySelectorCountryCodesSelectedJSON.length) ? 'include' : 'exclude';
      })(),

      presets: {
        ':US': {
          name: 'Include United States (US) Only',
          mode: 'include',
          countryCodes: ['US']
        },
        ':NONUS': {
          name: 'Exclude United States (US) Only',
          mode: 'exclude',
          countryCodes: ['US']
        },
        ':EUUK': {
          name: 'Include Europe and UK (EU/UK) Only',
          mode: 'include',
          countryCodes: ['AL','AD','MC','RS','BE','HR','DK','DE','ES','FI','FR','GR','IE','IT','LU','NL','NO','AT','PT','CH','SE','GB','BG', 'CY', 'CZ', 'EE', 'HU', 'LV', 'LT', 'MT', 'PL', 'RO', 'SK', 'SI','IS','LI','MK','MD','ME','BY','VA','JE']
        },
        ':NONEUUK': {
          name: 'Exclude Europe and UK (EU/UK) Only',
          mode: 'exclude',
          countryCodes: ['AL','AD','MC','RS','BE','HR','DK','DE','ES','FI','FR','GR','IE','IT','LU','NL','NO','AT','PT','CH','SE','GB','BG', 'CY', 'CZ', 'EE', 'HU', 'LV', 'LT', 'MT', 'PL', 'RO', 'SK', 'SI','IS','LI','MK','MD','ME','BY','VA','JE']
        },
        ':UK': {
          name: 'Include UK Only',
          mode: 'include',
          countryCodes: ['GB']
        },
        ':NONUK': {
          name: 'Exclude UK Only',
          mode: 'exclude',
          countryCodes: ['GB']
        },

      }
    },

    mode: function(state){
      cs.data.state = state;
      $('#step2, .showCountries').removeClass('exclude include').addClass(state);
      $('.tag_holder').html('');
      var footerOrHeader;
      // check if there are selectedCodes and switch them
      if(cs.data.state == 'include'){
        // check if we have all country codes selected
        // if so we need to remove them all and only include what the user asks for
        if(cs.data.selectedCodes.length == cs.data.countryCount) cs.data.selectedCodes = [];
        $.each(cs.data.selectedCodes, function(){
          cs.renderTag(this);
        });
        footerOrHeader = 'footer';
      }
      else if(cs.data.state == 'exclude'){
        $.each(cs.data.countries, function(code, country){
          if($.inArray(code, cs.data.selectedCodes) < 0) cs.renderTag(code);
        });
        footerOrHeader = 'header';
      }
      $('.tags').each(function(){
        var t = $(this);
        t.find('header p, footer p').appendTo(t.find(footerOrHeader));
      });
      cs.renderCount();
    },

    preset: function(command){
      if(command in cs.data.presets){
        var mode = cs.data.presets[command].mode,
            countryCodes = cs.data.presets[command].countryCodes.slice(0);
        if(mode == 'exclude'){
          var newCountryCodes = [];
          $.each(cs.data.countries, function(code, country){
            if($.inArray(code, countryCodes) < 0) newCountryCodes.push(code);
          });
          countryCodes = newCountryCodes;
        }
        cs.data.selectedCodes = countryCodes;
        cs.updateHiddenInput();
        cs.mode(mode);
      }
    },

    add: function(countryOrCode){
      if(countryOrCode[0] == ':') cs.preset(countryOrCode);
      else{
        var code = cs.findCode(countryOrCode);
        if((code || !1) && $('.tag_holder .'+code).length <= 0){
          $('#country_selector').val('');
          cs.renderTag(code);
          if(cs.data.state == 'include') cs.includeCountryCode(code);
          else if(cs.data.state == 'exclude') cs.excludeCountryCode(code);
          cs.renderCount();
          cs.updateHiddenInput();
        }
      }
    },

    renderTag: function(code){
        $('.tag_holder').prepend('<span class="tag '+code+'">'+cs.data.countries[code]+' <span class="xclose" code="'+code+'">x</span></span>');
    },

    renderCount: function(){
      var count = 0;
      if(cs.data.state == 'include') count = cs.data.selectedCodes.length;
      else if(cs.data.state == 'exclude') count = cs.data.countryCount - cs.data.selectedCodes.length;
      if(count <= 1) $('.country_count').hide();
      else $('.'+cs.data.state+' .country_count').html(count).show();
    },

    includeCountryCode: function(code){
      if($.inArray(code, cs.data.selectedCodes) < 0) cs.data.selectedCodes.push(code);
    },

    findCode: function(countryOrCode){
      var c = null,
          r = new RegExp('^'+countryOrCode+'$', 'i');
      $.each(cs.data.countries, function(code, country){
        if(r.test(code) || r.test(country)) c = code;
      });
      return c;
    },

    remove: function(code){
      $('.tag_holder .'+code).remove();
      if(cs.data.state == 'include') cs.excludeCountryCode(code);
      else if(cs.data.state == 'exclude') cs.includeCountryCode(code);
      cs.renderCount();
      cs.updateHiddenInput();
    },

    excludeCountryCode: function(code){
      var index = $.inArray(code, cs.data.selectedCodes);
      if(index > -1) cs.data.selectedCodes.splice(index, 1);
    },

    reset: function(){
      $('.step').hide();
      $('#step1').show();
      $('.tag_holder').html('');
      cs.data.selectedCodes = cs.data.countryCodes.slice(0);
      cs.updateHiddenInput();
    },

    cancel: function(){
      cs.data.worldwide = !!1;
      $('.step').hide();
      $('.countryQuestion').show();
      $('.showCountries').hide();
      $('a.open').hide();
      $('#yes').prop('checked', true);
      $('.tag_holder').html('');
    },

    save: function() {
      if (!albumId) { return; }

      $.ajax({
        url: '/albums/' + albumId,
        type: 'PUT',
        data: JSON.stringify({ album: { iso_codes: $('#iso_codes').val() } }),
        contentType: "application/json"
      });
    },

    step1: function(){
      cs.data.worldwide = !1;
      $('.step').hide();
      $('#step1').show();
      cs.openDialog();
    },

    step2: function(state){
      $('#no').prop('checked', true);
      $('.step').hide();
      $('#step2').show();
      $('.countryQuestion').hide();
      $('.showCountries').show();
      $('a.open').show();
      cs.mode(state);
    },

    updateHiddenInput: function(){
      $('#iso_codes').val(cs.data.selectedCodes.join(','));
    },

    openDialog: function(){
        $("#countrySelectorDialog").modal({
          maxWidth: 800,
          minWidth: 400,
          maxHeight: 400,
          minHeight: 400,
          persist: true,
          containerId: 'country-selector-modal',
          onClose: function(){
            if(cs.data.countryCount == cs.data.selectedCodes.length) cs.cancel();
            cs.save();
            cs.closeDialog();
            $('body').css('overflow', 'auto');
          }
        });
        $('body').css('overflow', 'hidden');
    },

    closeDialog: function(){
        $.modal.close();
    },

    openWWDialog: function(){
        $("#wwDialog").modal({
          maxWidth: 500,
          minWidth: 500,
          maxHeight: 200,
          minHeight: 200,
          persist: true
        });
    },

    closeWWDialog: function(){
        $.modal.close();
    },

    // Bindings
    bind: {
      all: function(){
        cs.bind.onClickNo();
        cs.bind.startExclude();
        cs.bind.startInclude();
        cs.bind.autoSuggestAndPressEnter();
        cs.bind.onClickX();
        cs.bind.onClickReset();
        cs.bind.onClickOpen();
        cs.bind.onClickWWDialog();
        cs.bind.onClickCloseWW();
        cs.bind.onClickActivateWW();
      },
      onClickNo: function(){
        // maybe block this if no is already checked
        $('#no').click(function(){
          if(cs.data.worldwide) cs.step1();
        });
      },
      startExclude: function(){
        $('.startExclude').click(function(e){
          cs.step2('exclude');
          return false;
        });
      },
      startInclude: function(){
        $('.startInclude').click(function(e){
          cs.step2('include');
          return false;
        });
      },
      autoSuggestAndPressEnter: function(){
        $('#country_selector').autocomplete({
            source: (function(){
              var s = [];
              $.each(cs.data.countries, function(code, country){
                s.push({
                  label: (code + ' - ' + country),
                  value: code
                });
              });
              $.each(cs.data.presets, function(command, preset){
                s.push({
                  label: preset.name,
                  value: command
                });
              });
              return s;
            })(),
            minLength: 1,
            select: function(event, ui){
              cs.add(ui.item.value);
            },
            close: function(event, ui){
              $('#country_selector').val('');
            }
        }).keypress(function(e){
          if(e.which == 13) cs.add($('#country_selector').val()),
                            $('#country_selector').val(''),
                            $('.ui-autocomplete').hide();
        });
      },
      onClickX: function(){
        $(document).on("click", ".xclose", function(e){
          cs.remove($(this).attr('code'));
          return false;
        });
      },
      onClickReset: function(){
        $('a.reset').click(function(e){
          if(confirm('Are you sure you\'d like to start over?')){
            cs.reset();
            cs.save();
          }
          return false;
        });
      },
      onClickOpen: function(){
        $('a.open').click(function(e){
          cs.openDialog();
          return false;
        });
      },
      onClickWWDialog: function(){
        $('a.wwDialog').click(function(e){
          cs.openWWDialog();
          return false;
        });
      },
      onClickCloseWW: function(){
        $('a.closeWW').click(function(e){
          cs.closeWWDialog();
          return false;
        });
      },
      onClickActivateWW: function(){
        $('.activateWW').click(function(e){
          var data = {};
          $('#store-group-itunes .checkbox:first :checkbox').prop('checked', true);
          $('.wwUpgrade').hide();
          cs.closeWWDialog();
          $('form#salepoint-form').prepend('<input type="hidden" name="upgrade" value="1" style="display: none;">').submit();
          return false;
        });
      }
    }

  };
  cs.init();
});
