(function($) {
  $.fn.stickyDiv = function( size ) {
    return this.each( function() {
      var $this = $(this);
      
      $(window).scroll(function(){
        var scrollPosition = $(window).scrollTop();
        if (scrollPosition > size){
          $this.addClass('sticky');
        } else {
          $this.removeClass('sticky');
        }
      });
    });
  };
})(jQuery);