/*
 *
 *  Album Creation Form
 *
 */
var TC = TC || {};

var AlbumForm = function(){
  // note: in sizzle, #id elm.class, or just elm.class is faster than .class
  // reusable style variables
  var $ = jQuery;
  var original_class = '.original-date-box';
  var not_previously_released_radio = '.not-previously-released';
  var previously_released_radio = '.previously-released';
  var form_input = '.context-wrapper input, .context-wrapper select, .context-wrapper textarea';
  var form_text_area = '.context-wrapper textarea';
  var primary_genre = '#album_primary_genre_id';
  var single_primary_genre = '#single_primary_genre_id';
  var secondary_genre = '#album_secondary_genre_id';
  var single_secondary_genre = '#single_secondary_genre_id';
  var language = '#album_language_code_legacy_support';

  var prev_released_radios = '.prev-released';
  var artist_name_field = '#album_creatives__name';
  var various = '.various';

  var single_title = '#single_name';
  var album_title = '#album_name';
  var single_artist = '#single_creatives__name';
  var add_artist_button = '#add-main-artists';
  var additional_artists = '.additional-artists';

  var release_submission_buttons = '.release-submission-button';
  var submission_spinners = '.loading-release-submission-spinner';

  // Save artist_name
  var artist_name = $(artist_name_field).val();

  //caching the jquery object, so we don't need to requery the dom every time
  //someone clicks on the radio button
  var $warning_msg = $('#warning');
  var multi_lang = /(?=.*[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz])(?=.*[^\s'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0987654321\.,-\/#!$%\^&\*;:=\-_`~\?])/;

  var self = {

    init: function(){
      $(prev_released_radios).change(this.set_as_previously_released);
      $(various).change(this.various_change_event);
      self.set_artist_name($('.various:checked'));
      self.init_helper_text();
      self.init_original_release_date();
      if (gon.locale === 'en') {
        self.init_styleguides();
      }
      self.init_auto_correct();
      self.init_custom_validation();
      $('#explicit-selector a').click(this.setExplicitOption);
      $('#clean-selector a').click(this.setCleanOption);
      self.initExplicitValidation();
      $([primary_genre, single_primary_genre, secondary_genre, single_secondary_genre].join(', ')).change(this.handleIndiaGenre);
    },

    init_styleguides: function(){
      // single and album
      $(single_title + ',' + album_title).styleGuide({
        rules: {
          'volume': /\sV(?!ol\.\s\d)|v[Oo][Ll](?:[Uu][Mm][Ee])?(\.)?\s(?:\d|[OoTtFfSsEeNn])/,
          'part': /\s(?:P[^t]|p[Tt]?|P)(?:[Aa][Rr][Tt])?(\S)?\s(?:\d|[oOtTfFsSeEnN])/,
          'multi_lang': multi_lang
        },
        messages: {
          'volume': 'Is this a multiple volume/disc release? If so, iTunes has specific formatting rules that must be followed. For example: "Album Title, Vol. 1"',
          'part': 'Is this a multiple volume/disc release? If so, iTunes has specific formatting rules that must be followed. For example: "Album Title, Pt. 1"',
          'multi_lang': 'Please do not use other languages and English together. You may only use one language.'
        }
      });

      $(single_title).styleGuide({
        rules: {
          'featuring': /\s\W?f(?:ea)?t(?:(?:\w)+|(?:\s)|(?:\.)?)\s?/i,
          'explicit': /\s([\[(]?([eE]xplicit|[dD]irty)[\])]?)/,
          'produced' : /\s\W?[pP]rod(\W|uce|\s)/
        },
        messages: {
          'featuring': 'Are you trying to add a featured artist to this song? If so, please enter them in the "Featured Artist" field below.',
          'explicit': 'Is this song explicit? If so, click the \'explicit\' checkbox below instead of putting this information in the title.',
          'produced' : 'Are you trying to enter a producer? Stores do not allow this information to be listed, please do not include or your release may be delayed.'
        }
      });

      $('.one-artist').styleGuide({
        rules: {
          '.': /\./,
          ',': /\,/,
          '/': /\//,
          '&': /\s\&\s/,
          '+': /\s\+\s/,
          '-': /\s\-\s/,
          'and': /\sand\s/i,
          'with': /\sw((ith)|\/)\s/i,
          'meets': /\smeets\s/i,
          'versus': /\sv(s|ersus)\s/i
        },
        messages: {
          '.': 'Are you trying to add another artist to this song? If so, please click "Add Another Main Artist"',
          ',': 'Are you trying to add another artist to this song? If so, please click "Add Another Main Artist"',
          '/': 'Are you trying to add another artist to this song? If so, please click "Add Another Main Artist"',
          '&': 'Are you trying to add another artist to this song? If so, please click "Add Another Main Artist"',
          '+': 'Are you trying to add another artist to this song? If so, please click "Add Another Main Artist"',
          '-': 'Are you trying to add another artist to this song? If so, please click "Add Another Main Artist"',
          'and': 'Are you trying to add another artist to this song? If so, please click "Add Another Main Artist"',
          'with': 'Are you trying to add another artist to this song? If so, please click "Add Another Main Artist"',
          'meets': 'Are you trying to add another artist to this song? If so, please click "Add Another Main Artist"',
          'versus': 'Are you trying to add another artist to this song? If so, please click "Add Another Main Artist"'
        }
      });

      $(single_artist).styleGuide({
        rules: {
          'featuring': /\s\W?f(?:ea)?t(?:(?:\w)+|(?:\s)|(?:\.)?)\s?/i
        },
        messages: {
          'featuring': 'Are you trying to add a featured artist to this song? If so, please enter them in the "Featured Artist" field below.'
        }
      });

    },

    various_change_event: function(){
      var $this = $(this);
      $warning_msg.modal({
        minWidth: 400,
        minHeight: 200,
        maxWidth: 400,
        maxHeight: 200
      });
      $('#simplemodal-container').find('a.modalCloseImg').hide();

      $warning_msg.on('click', '.continue', function(e){
        e.preventDefault();
        $.modal.close();
        self.set_artist_name($this);
      });
      $warning_msg.on('click', '.cancel', function(e){
        e.preventDefault();
        if ($this.prop('checked')) {
          $this.prop('checked', false);
        } else {
          $this.prop('checked', true);
        }
        $.modal.close();
        self.set_artist_name($this);
      });
    },

    handleIndiaGenre: function(e){
      var sub_genre_fieldset = $(e.target).closest('fieldset').next('.sub_genre_field');

      if (e.target.value === '40') {
        sub_genre_fieldset.show();
      } else {
        sub_genre_fieldset.hide();
      }
    },

    set_artist_name: function(el){
      if( $(el).is(':checked') === true ){
        self.hide_artist_name();
        self.hide_add_artist_button();
        self.hide_additional_artists();
      } else {
        self.show_artist_name();
        self.show_add_artist_button();
        self.show_additional_artists();
      }
    },

    clear_artist_name: function(){
      $(artist_name_field).val('');
    },

    hide_artist_name: function(){
      $(artist_name_field).parents('fieldset').addClass('hidden');
    },

    hide_add_artist_button: function(){
      $(add_artist_button).addClass('hidden');
    },

    hide_additional_artists: function(){
      $(additional_artists).addClass('hidden');
    },

    save_artist_name: function(){
      if( artist_name === '' ){
        artist_name = $(artist_name_field).val();
      }
    },

    recall_artist_name: function(){
      $(artist_name_field).val(artist_name);
    },

    show_artist_name: function(){
      $(artist_name_field).parents('fieldset').removeClass('hidden');
    },

    show_add_artist_button: function(){
      $(add_artist_button).removeClass('hidden');
    },

    show_additional_artists: function(){
      $(additional_artists).removeClass('hidden');
    },

    init_helper_text: function(){
      $(form_input).focus(this.show_helper_text);
      $('#album_name').focus(this.offset_targeted_offer);
      $('#album_selected_artist_name').focus(this.offset_targeted_offer);
      $(form_input).blur(this.hide_helper_text);
      $('#album_name').blur(this.unoffset_targeted_offer);
      $('#album_selected_artist_name').blur(this.unoffset_targeted_offer);
      if ($('#primary-artist-errors').length) {
        this.offset_targeted_offer;
      }
    },

    //
    // Ensure that the text boxes are read only
    // performing this here for accessability reasons
    //
    make_read_only: function(){
      $(this).attr('readonly', 'true');
    },

    //
    // Determine if the album has already been released
    //
    set_as_previously_released: function(){
      var $originalClass = $(original_class)
      if( $(this).hasClass('previously-released') ){
        $originalClass.parents('fieldset').removeClass('hide');
      }
      else {
        $originalClass.parents('fieldset').addClass('hide');
        $originalClass.val('');
      }
    },

    //
    //  Check to see if the Original Date and Digital Date match
    //
    all_dates_are_equal: function(){
      var flag = true; // setup pre-condition

      var original_date = $(original_class).prop('value');
      var sale_date = TC.selected_date;

      if (sale_date !== original_date){
        flag = false;
      }
      return flag;
    },

    init_original_release_date: function(){
      var $originalClass = $(original_class)
      if( ($originalClass.prop('value') !== '') && !(self.all_dates_are_equal()) ){
        $originalClass.parents('fieldset').removeClass('hide');
        $(previously_released_radio).prop('checked','checked');
      }
    },

    //
    // Simple methods for hiding and showing contextual help for forms
    //
    show_helper_text: function(){
      var help = $(this).nextAll('.contextual-help');
      if (help.find('p').text().length !== 0){
        help.fadeIn();
      }
    },
    hide_helper_text: function(){
      var t = $(this);
      setTimeout(function(){
        t.nextAll('.contextual-help').fadeOut();
      }, 1000);
    },
    offset_targeted_offer: function(){
      if ( $('div.ad') ) {
        $('div.ad').stop().animate({top: '160px'});
      }
    },
    unoffset_targeted_offer: function(){
      if ( $('div.ad') ) {
        setTimeout(function(){
          $('div.ad').stop().animate({top: '20px'});
        }, 1500);
      }
    },

    has_cyrillic: function(str){
      return str.match(/[АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюяЙЦУКЕНГШЩЗХЇЄЖДЛОРПАВІФґЯЧСМИТЬБЮйцукенгшщзхїєждлорпавіфґячсмитьбю]/);
    },

    has_russian: function(str){
      return str.match(/[ЁЪЫЭёъыэ]/);
    },

    has_ukrainian: function(str){
      return str.match(/[ЇЄІґїєіґ]/);
    },

    has_multi_lang: function(str){
      return str.match(multi_lang);
    },

    not_english: function(str){
      return str.match(/[^\sABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0987654321\.,-\/#!$%\^&\*;:{}=\-_`~()\?]/);
    },

    sentence_case: function(str){
      return str.charAt(0).toUpperCase() + str.toLowerCase().substr(1);
    },

    sentence_case_title: function(){
      $('body').on('focusout', '#single_name, #album_name, #song_name', function(){
        var t = $(this),
          v = t.val();
        if(self.has_cyrillic(v)){
          v = self.sentence_case(v);
          v = v.replace(/\(.+\)/g, function(vv){
            return '('+self.sentence_case(vv.substr(1, vv.length - 2))+')';
          });
          t.val(v);
        }
      });
    },

    remove_special: function(){
      $('body').on('focusout', '#single_name, #album_name, #song_name', function(){
        var t = $(this),
          v = t.val();
        t.val(v.replace(/\s?(\(Кавер\)|\(Концерт\)|Кавер|Концерт)\s?/ig, ''));
      });
    },

    init_auto_correct: function(){
      self.init_forward_slash_auto_correct();
      self.init_apostrophe_auto_correct();
      self.init_cyrillic_auto_correct();
      self.init_cyrillic_language_auto_select();
    },

    init_forward_slash_auto_correct: function(){
      $('body').on('focusout', '#single_name, #album_name, #song_name', function(){
        var t = $(this),
          v = t.val();
        t.val(v.replace(/\s?\/\s?/g, ' / '));
      });
    },

    init_apostrophe_auto_correct: function(){
      $('body').on('focusout', '#single_name, #album_name, #song_name', function(){
        var t = $(this),
          v = t.val();
        t.val(
          v.replace(/\b([Ll])ivin\b'?/g, '$1ivin\'')
           .replace(/\b([Gg])ivin\b'?/g, '$1ivin\'')
           .replace(/\b([Gg])ettin\b'?/g, '$1ettin\'')
           .replace(/\b([Hh])avin\b'?/g, '$1avin\'')
           .replace(/\b([Pp])impin\b'?/g, '$1impin\'')
           .replace(/\b([Ll])eavin\b'?/g, '$1eavin\'')
           .replace(/\b([Cc])hillin\b'?/g, '$1hillin\'')
           .replace(/\b([Rr])ockin\b'?/g, '$1ockin\'')
           .replace(/\b([Kk])eepin\b'?/g, '$1eepin\'')
           .replace(/\b([Gg])amin\b'?/g, '$1amin\'')
           .replace(/\b([Pp])artyin\b'?/g, '$1artyin\'')
           .replace(/\b([Cc])allin\b'?/g, '$1allin\'')
           .replace(/\b([Kk])illin\b'?/g, '$1illin\'')
           .replace(/\b([Bb])ouncin\b'?/g, '$1ouncin\'')
           .replace(/\b([Hh])ittin\b'?/g, '$1ittin\'')
           .replace(/\b([Gg])rindin\b'?/g, '$1rindin\'')
           .replace(/\b([Dd])on'?t\b/g, '$1on\'t')
           .replace(/'?\b([Ee])m\b/g, '\'$1m')
           .replace(/\b([Aa])in'?t\b/g, '$1in\'t')
           .replace(/\b([Ww])on'?t\b/g, '$1on\'t')
           .replace(/\b([Cc])an'?t\b/g, '$1an\'t')
           .replace(/\b([Yy])ou'?re\b/g, '$1ou\'re')
        );
      });
    },

    init_cyrillic_auto_correct: function(){
      self.remove_special();
      self.sentence_case_title();
    },

    init_cyrillic_language_auto_select: function(){
      $(single_title + ',' + album_title).on('focusout', function(){
        var t = $(this),
          v = t.val(),
          lang = $(language);
        if(self.has_russian(v)) lang.val('ru');
        else if(self.has_ukrainian(v)) lang.val('uk');
        else if(self.has_cyrillic(v)) lang.val('ru');
      });
    },

    init_custom_validation: function(){
      self.validate_language();
      self.validate_song_titles();
      self.init_genre_warning();
      self.validate_name();
    },

    validate_language: function(){
      $(language).on('change', function(){
        var t = $(this),
          v = t.val(),
          title = $(single_title + ',' + album_title).val(),
          hasRussian = self.has_russian(title),
          hasUkrainian = self.has_ukrainian(title),
          hasCyrillic = self.has_cyrillic(title),
          notEnglish = self.not_english(title);
        if(hasRussian && v !== 'ru'){
          self.show_language_error('ru');
        }
        else if(hasUkrainian && v !== 'uk'){
          self.show_language_error('uk');
        }
        else if(hasCyrillic && v !== 'ru' && v !== 'uk'){
          self.show_language_error();
        }
        else if(notEnglish && v === 'en'){
          self.show_language_error();
        }
        else{
          self.hide_language_error();
        }
      });
    },

    show_language_error: function(type){
      var msg = '',
        langError = $('#language-error');
      if(type && type === 'ru') msg = 'Please select Russian as your release language.';
      else if(type && type === 'uk') msg = 'Please select Ukrainian as your release language.';
      else msg = 'Are you sure you have selected the correct language?';
      sendEvent('showLanguageError');
      langError.html(msg).show();
    },

    hide_language_error: function(){
      var langError = $('#language-error');
      langError.hide();
    },

    validate_song_titles: function(){
      var str = $('.albuminfo dl').html();
      if((str || !1) && str.match(/Russian/g)){
        $('#song_name').styleGuide({
          rules: {
            'no_english_with_russian': /(?=.*[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz])/
          },
          messages: {
            'no_english_with_russian': 'Please do not use English in song names if your release is in Russian.'
          }
        });
      }
    },

    validate_name: function(){
      $('#single_name, #album_name, #song_name').on('focusout', function(){
        var primary = $.trim($('#artist-name-text').text());
        if(!primary) primary = $('[name="single[creatives][][name]"]').eq(0).val();
        var featuring = $('[name="song[creatives][][name]"]').eq(1).val();
        if(!featuring) featuring = $('[name="single[creatives][][name]"]').eq(1).val();
        if(primary && featuring && primary === featuring) return 'Primary and Featuring artists should not be the same.';
      });
    },

    init_genre_warning: function(){
      $('#album_primary_genre_id, #single_primary_genre_id').on('change', function(){
        var t = $(this);
        if(t.val() === 38) $('#no-classical').show();
        else $('#no-classical').hide();
      });
    },

    send_event: function(str) {
      window.optimizely = window.optimizely || [];
      window.optimizely.push(['trackEvent', str]);
      var ga = ga || [];
      if ( TC && TC.albumId ) {
        ga('send', 'event', 'LanguageError', str, 'For a_id: ' + TC.albumId, {'nonInteraction': 1});
      } else if ( _paq && _paq[0] && _paq[0][1] && _paq[0][1].id ) {
        ga('send', 'event', 'LanguageError', str, 'For p_id: ' + _paq[0][1].id, {'nonInteraction': 1});
      } else {
        ga('send', 'event', 'LanguageError', str, '/new', {'nonInteraction': 1});
      }
    },

    setExplicitOption: function(e){
      e.preventDefault();

      $('#explicit-selector-errors').hide();

      var selectedValue = $(this).data('value');

      $('#single_parental_advisory').val(selectedValue);

      $('#explicit-selector .button-interface').removeClass('option-selected');
      $(this).addClass('option-selected');
      $('.inform').attr("disabled", false);

      if(selectedValue == 1) {
        $('#single_clean_version').attr("disabled", true);
        $('#clean-selector-container').addClass('hidden')
      } else {
        $('#single_clean_version').attr("disabled", false);
        $('#clean-selector-container').removeClass('hidden')
      }
    },

    setCleanOption: function(e){
      e.preventDefault();

      $('#clean-selector-errors').hide();

      var selectedValue = $(this).data('value');

      $('#single_clean_version').val(selectedValue);

      $('#clean-selector .button-interface').removeClass('option-selected');
      $(this).addClass('option-selected');
      $('.inform').attr("disabled", false);
    },

    initExplicitValidation: function() {
      $('#new_single').on('submit', function(e) {
        var explicitOptionSelected = $('#explicit-selector .option-selected').length;
        var explicitOptionValue = $('#single_parental_advisory').val();
        var cleanOptionSelected = $('#clean-selector .option-selected').length;
        var singleCleanVersionSelectorEnabled = $('#single_clean_version_selector_enabled').data('value')
        var errorsPresent = false

        if (
          singleCleanVersionSelectorEnabled === true &&
          explicitOptionSelected >= 1 &&
          cleanOptionSelected < 1 &&
          explicitOptionValue != "1"
        ) {
          e.preventDefault();
          errorsPresent = true
          $('#clean-selector-errors').show();
        }

        if (errorsPresent) {
          $(submission_spinners).hide();
          $(release_submission_buttons).show();
          $(window).scrollTop(0);
        }
      })
    }

  };

  return self.init();
};

var init_album_form = function(){
  AlbumForm();
};
jQuery(document).ready(init_album_form);
