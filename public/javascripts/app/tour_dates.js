//
//  Tour Dates Form Controller:
//
//  Control UI in the Tour Dates form
//
//  note: select boxes are managed site wide.
//
var TourDatesForm = function(){
  var $ = jQuery;

  var self = {
    toggleUsStateSelect: function(){
      var country = $('#tour_date_country_id').val();
      //233 is USA
      if (country == '233'){
        $('#tour_date_us_state_id_list_item').show('drop',{},500);
      }
      else{
        if ($('#tour_date_us_state_id_list_item').css('display') != 'none'){
          $('#tour_date_us_state_id_list_item').hide('drop',{},500);
        }
      }
    },

    init_bind: function(){
      $('#tour_date_country_id').bind("change", self.toggleUsStateSelect);
    },

    init: function(){
      // new DatePicker("input.date-box", {'minDate': new Date()});
      var options = $.extend({}, $.datepicker.regional[gon.datepicker_locale])
      $.datepicker.setDefaults(options);
      $("input.date-box").datepicker({'minDate': new Date()})

      self.toggleUsStateSelect(); //in case a user refreshes their screen
      self.init_bind();
    }
  };

  self.init();
  return self;
}


jQuery(document).ready( function(){
  new TourDatesForm();

});
