
/**
 * Requires d3.js
*/

jQuery(function(){
  var $ = jQuery;
  var data = JSON.parse($('#data').html()) || {};

  // set locale for momentjs
  moment.locale(data.locale);

  var formatNum = function(n){
          var n = n.split('.'),
              n1 = n[0],
              n2 = (n.length > 1) ? '.' + n[1] : '',
              rgx = /(\d+)(\d{3})/;
          while(rgx.test(n1)){
              n1 = n1.replace(rgx, '$1' + ',' + '$2');
          }
          return n1 + n2;
      },
      trunc = function(str, count){
        return (str.length > count) ? str.slice(0, count) + '...' : str;
      },
      syncTime = function(d){
        var d = new Date(moment(d)._d);
        return new Date(d.getTime() + (d.getTimezoneOffset()*60*1000));
      },
      startDate = moment(data.sd)._d || null,
      endDate = moment(data.ed)._d || null,
      chartDataRange = function(){
        if('sd' in data && 'ed' in data){
          var sd = moment(data.sd),
              ed = moment(data.ed),
              add = [];
          while(!sd.isAfter(ed, 'day')){
            add.push({time: sd._d, downloads: 0});
            sd = moment(sd).add('d', 1);
          }
          return add;
        }
      }(),
      buildChartData = function(d){
        var add = [],
            merged = [];
        $.each(d, function(){
          add.push({time: this.sale_date, downloads: parseInt(this.quantity||0)});
        });
        // Merge a quantity for all days in the chart date range
        if(add.length > 0){
          var a = add.shift();
          $.each(chartDataRange, function(){
            if(a != undefined && moment(this.time).isSame(a.time, 'day')){
              merged.push(a);
              a = add.shift();
            }
            else merged.push(this);
          });
        }
        else merged = chartDataRange;
        // Code to fake downloads
        /*
        $.each(merged, function(){
          this.downloads = Math.floor((Math.random()*1000)+1);
        });
        */
        return merged;
      },
      albumDownloadsData = buildChartData(data.album_downloads || []),
      songDownloadsData = buildChartData(data.song_downloads || []),
      ringtoneDownloadsData = buildChartData(data.ringtone_downloads || []),
      streamData = buildChartData(data.streams || []),
      albumChart = $('#album_downloads'),
      songChart = $('#song_downloads'),
      ringtoneChart = $('#ringtone_downloads'),
      streamChart = $('#streams_chart'),
      d3Chart = function(id, data){
        var container = $('#'+id),
            containerWidth = container.width(),
            small = containerWidth <= 550,
            margin = {top: 20, right: 20, bottom: function(){return (small) ? 50 : 30;}(), left: 40},
            width = containerWidth - margin.left - margin.right,
            height = 200 - margin.top - margin.bottom,
            xAxisPadding = function(){return (width / (data.length + 1) * 0.6) }(),
            xAxisWidth = width - xAxisPadding * 2,
            ticks = {x: 7, y: 5},
            x = d3.time.scale()
                  .range([0, xAxisWidth]);
            y = d3.scale.linear()
                  .range([height, 0]);

            x.domain([startDate, endDate]);
            y.domain([0, d3.max(data, function(d) { return d.downloads; })]);

        var xAxis = d3.svg.axis()
                      .scale(x)
                      .orient('bottom')
                      .ticks(d3.time.days, 1)
                      .tickFormat(d3.time.format('%b %d'));

        if(data.length < ticks.x) xAxis.ticks(d3.time.days, 1);
        else xAxis.ticks(ticks.x);

        var yAxis = d3.svg.axis()
                      .scale(y)
                      .orient('left')
                      .ticks(ticks.y)
                      .tickFormat(function(d){
                        if((d / 1000) >= 1) d = d / 1000 + "K";
                        else if(d != 0 && d < 1) d = null;
                        return d;
                      }),

            // svg
            svg = d3.select('#'+id).append('svg')
                    .attr('width', width + margin.left + margin.right)
                    .attr('height', height + margin.top + margin.bottom)
                    .attr('xmlns', 'http://www.w3.org/2000/svg')
                    .attr('version', '1.1')
                    .append('g')
                    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

            // x axis
            svg.append('g')
               .attr('class', 'x axis')
               .attr('transform', 'translate('+ xAxisPadding +',' + height + ')')
               .call(xAxis);

            // tilt the labels
            if(small){
              svg.select('.x.axis').selectAll('text')
                 .style('text-anchor', 'end')
                 .attr('dx', '-.8em')
                 .attr('dy', '.25em')
                 .attr('transform', function(d) {
                   return 'rotate(-50)'
                 });
            }

            // y axis
            svg.append('g')
               .attr('class', 'y axis')
               .call(yAxis);

            // bars
            svg.selectAll('.bar')
               .data(data)
               .enter().append('rect')
               .attr('class', 'bar')
               .attr('x', function(d) {
                 return x(moment(d.time));
               })
               .attr('width', function(d){
                 var w = width / (data.length + 1) * .75;
                 return w >= 0 ? w : 0;
               })
               .attr('y', function(d){
                 return y(d.downloads);
               })
               .attr('height', function(d){
                 return height - y(d.downloads);
               })
               .append('svg:title')
               .text(function(d,i) {
                  var type;
                  if(id == 'album_downloads') type = 'releases';
                  else if(id == 'song_downloads') type = 'songs';
                  else if(id == 'ringtone_downloads') type = 'ringtones';
                  else type = 'streams';
                  return moment(d.time).format('dddd') + ', ' + moment(d.time).format('L') + ' - ' + d.downloads + ' '+type+(type == 'streams' ? '' : ' sold');
               });

            // xaxis line
            svg.append('line')
               .attr('class', 'ln')
               .attr('x1', 0)
               .attr('x2', xAxisPadding)
               .attr('y1', height)
               .attr('y2', height);

            svg.append('line')
                .attr('class', 'ln')
                .attr('x1', width - xAxisPadding)
                .attr('x2', width)
                .attr('y1', height)
                .attr('y2', height);

      };

  // Tablesorter
  jQuery.tablesorter.addParser({
    id: 'fancyNumber',
    is: function(s) {
      return /^[0-9]?[0-9,\.]*$/.test(s);
    },
    format: function(s) {
      return jQuery.tablesorter.formatFloat( s.replace(/,/g,'') );
    },
    type: 'numeric'
  });

  var bindTableSorter = function(table){
    table.tablesorter().tablesorterPager({
      container: table.next(),
      positionFixed: false
    });
  };

  // Init tablesorter on tables rendered in view
  if('releases_downloaded' in data && data.releases_downloaded.length > 0 ) bindTableSorter($('.downloads .releases table'));
  if('releases_streamed' in data && data.releases_streamed.length > 0 ) bindTableSorter($('.streams .releases table'));
  if('songs_downloaded' in data && data.songs_downloaded.length > 0 ) bindTableSorter($('.downloads .songs table'));
  if('songs_streamed' in data && data.songs_streamed.length > 0 ) bindTableSorter($('.streams .songs table'));
  if('ringtones' in data && data.ringtones.length > 0 ) bindTableSorter($('.downloads .ringtones table'));
  if('markets' in data && data.markets.length > 0 ) bindTableSorter($('.downloads .markets table'));
  if('countries_downloaded' in data && data.countries_downloaded.length > 0 ) bindTableSorter($('.downloads .countries table'));
  if('countries_streamed' in data && data.countries_streamed.length > 0 ) bindTableSorter($('.streams .countries table'));

  // Tabs
  var itunesEnabled = function(){
    return $('#iTunesFilter').is(':checked');
  };
  $('.downloads .tabs li').on('click', function(e){
    var t = $(this),
        section = t.attr('section');
    if(section != 'markets' || itunesEnabled()){
      $('.downloads .tabs li.active').removeClass('active');
      t.addClass('active');
      $('.downloads .tables .row').hide();
      $('.downloads .tables .row.'+section).show();
    }
    else $('#usmarketsmodal').dialog('open');
    e.preventDefault();
  });

  $('.streams .tabs li').on('click', function(e){
    var t = $(this),
        section = t.attr('section');
    $('.streams .tabs li.active').removeClass('active');
    t.addClass('active');
    $('.streams .tables .row').hide();
    $('.streams .tables .row.'+section).show();
    e.preventDefault();
  });
  //
  // Datepicker
  var setStartDate = function(){
        var ed = moment($('#ed').val(), 'MMMM DD, YYYY'),
            resolution = $('#resolution').val(),
            sd = ed.subtract(resolution[1], resolution[0]);
            if(resolution[1] == 'w' || resolution[1] == 'd') sd.add('d', 1);
        $('#sd').val(moment(syncTime(sd._d)).format('MMMM DD, YYYY'));
      },
  lastDate = moment(data.last_trend_date);


  var options = $.extend(
      {},
      $.datepicker.regional[gon.datepicker_locale]
  );
  $.datepicker.setDefaults(options);

  $('#ed').datepicker({
    maxDate: lastDate._d,
    dateFormat: "MM d, yy",
    numberOfMonths: 1,
    onClose: function( selectedDate ) {
      setStartDate();
    }
  });

  // hacking this class away for some reason it's breaking datepicker
  $('.ui-helper-hidden-accessible').removeClass('ui-helper-hidden-accessible');

  // Resolution
  $('#resolution').on('change', function(){
    setStartDate();
  });

  // Chart
  var redrawAll = function(){
    if('album_downloads' in data && albumChart.length > 0 && albumChart.is(':visible')) albumChart.html(''), d3Chart('album_downloads', albumDownloadsData);
    if('song_downloads' in data && songChart.length > 0 && songChart.is(':visible')) songChart.html(''), d3Chart('song_downloads', songDownloadsData);
    if('ringtone_downloads' in data && ringtoneChart.length > 0 && ringtoneChart.is(':visible')) ringtoneChart.html(''), d3Chart('ringtone_downloads', ringtoneDownloadsData);
    if('streams' in data && streamChart.length > 0 && streamChart.is(':visible')) streamChart.html(''), d3Chart('streams_chart', streamData);
  };

  var w = $(window);
  w.on('resize', function(){
    redrawAll();
  });

  $('.chart_controls input').click(function(){
    $('.downloads .chart').hide();
    $('#'+$('.chart_controls input:checked').val()+'_downloads').show();
    redrawAll();
  });

  $('#sdcontrol').prop('checked', 'checked').click();
  if($('#trends').attr('action') == 'ringtone_view') $('#rdcontrol').prop('checked', 'checked').click();

  redrawAll();

  // Modal
  $('#trendmodal, #trendcsvmodal, #usmarketsmodal').dialog({
    autoOpen: false,
    resizable: false,
    modal: true,
    draggable: false,
    width: 700,
    maxWidth: 700,
    height: 400,
    maxHeight: 400
  });

  $('.trendmodal').on('click', function(e){
    $('#trendmodal').dialog('open');
    e.preventDefault();
  });

   $('.trendcsvmodal').on('click', function(e){
    $('#trendcsvmodal').dialog('open');
    e.preventDefault();
  });

  // Spinner When Refreshing
  $('#range .blue-btn').on('click', function(e){
    $('#loadingDialog').dialog('open');
  });

});

// Provider ID Filter
$('.downloads .trends_filter input').on('click', function(e){
  var ids = [];
  $('.downloads .trends_filter input:checked').each(function(){
    ids.push($(this).attr('provider_id'));
  });
  $('#download_provider_ids').val(ids.join(','));
  $('.trends_range form:first').attr('action', '').submit();
});
