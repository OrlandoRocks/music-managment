/* Sales_Report.js
  = Description
  The following code allows the sales page to have select all buttons,
  table sorting, collapsing panels, and ajax requests.

  = Dependencies
  Depends on the jquery.tablesorter, jquery.tablesorter.pager, jquery.rule, and jquery.cookie plugin.

  = Usage
  Not a function to be used outside this file.

*/
var TC = TC || {};
var Sales = function () {

  // Set up $ to be used locally for jQuery then
  // cache selectors so you don't have to traverse
  // the DOM each time.
  var $ = jQuery;
  var cookie_filter = $.cookie("filter_state");
  var cookie_hidden = $.cookie("hidden_state");
  var filter_state = cookie_filter ? cookie_filter.split("|").getUnique() : [];
  var hidden_state = cookie_hidden ? cookie_hidden.split("|").getUnique() : [];
  var collapsible = "div.collapsible";
  var song_links = ".song-sales-record";
  var container = ".sales-filters .container";
  var show_more = ".show_more";
  var $salesTable = $(".sales-table");
  var $date_start_selector = $("#date_start_date");
  var $date_end_selector = $("#date_end_date");
  var $sales_total = $(".sales-totals span");

  var self = {

    init: function () {
      self.currency_decimal_hider();
      self.custom_date_parser();
      self.sales_table_sorting(); // optimize me
      self.collapsible_panels( $(collapsible) );
      $(song_links).on("click", ".toggle-button", self.show_song_sales);
      self.hash_load( $(collapsible) );
      self.negative_dates_hider();
      self.filter_type_hider();
      self.filter_show_hide();
      self.release_table_expand_collapse();
      self.sales_table_search();
    },

    // This uses the dataTables plugin to create sorting and other
    // functions for the sales tables. The actual method is contained in the
    // dataTable_maker() function to make the code DRY.
    //
    sales_table_sorting: function () {
      if ($salesTable.find("tbody tr").length <= 10) {
        $(".pager").hide();
      }
      if ( $(".sales-table.alternate").find("th:first-child").html() === "Sales Period" ) {
        $salesTable.tablesorter({
          headers: {
            0: { sorter: 'monthYear' },
            1: { sorter: 'commaNumber' },
            2: { sorter: 'commaNumber' },
            3: { sorter: 'commaNumber' },
            4: { sorter: 'commaNumber' }
          },
          widgets: ['zebra']
        });
      } else if ( $salesTable.hasClass('date-report') ) {
        $salesTable.tablesorter({
          headers: {
            0: { sorter: 'monthYear' },
            3: { sorter: 'commaNumber' }
          },
          widgets: ['zebra']
        });
      } else if ( $salesTable.hasClass('release-report') ) {
        $salesTable.tablesorter({
          headers: {
            2: { sorter: 'commaNumber' },
            3: { sorter: 'commaNumber' }
          },
          widgets: ['zebra']
        });
      } else if ( $salesTable.hasClass('song-report') ) {
        $salesTable.tablesorter({
          headers: {
            2: { sorter: 'commaNumber' },
            3: { sorter: 'commaNumber' }
          },
          widgets: ['zebra']
        });
      } else if ( $salesTable.hasClass('artist-report') ) {
        $salesTable.tablesorter({
          headers: {
            1: { sorter: 'commaNumber' },
            2: { sorter: 'commaNumber' }
          },
          widgets: ['zebra']
        });
      } else {
        $salesTable.tablesorter({
          widgets: ['zebra']
        });
      }
      $salesTable.tablesorterPager({
        container: $(".pager"),positionFixed: false
      });
    },

    // This also uses the dataTables plugin as well. This method is
    // for each individual song table on the individual release pages.
    //
    song_table_sorting: function (table, song_id) {
      if ($(table).find('tbody').find('tr').length <= 10) {
        $(table).siblings(".table_bottom").hide();
      }
      $(table).tablesorter({
        headers: {
          0: { sorter:'monthYear' },
          1: { sorter: 'commaNumber' },
          2: { sorter: 'commaNumber' },
          3: { sorter: 'commaNumber' },
          4: { sorter: 'commaNumber' }
        },
        widgets: ['zebra']
      }).tablesorterPager({container: $(".pager-" + song_id),positionFixed: false});
    },

    // Custom sorting parsers
    //
    custom_date_parser: function(){
      var monthNames = {
        Jan: "01",
        Feb: "02",
        Mar: "03",
        Apr: "04",
        May: "05",
        Jun: "06",
        Jul: "07",
        Aug: "08",
        Sep: "09",
        Oct: "10",
        Nov: "11",
        Dec: "12"
      };
      $.tablesorter.addParser({
        id: 'monthYear',
        is: function(s) {
          return false;
        },
        format: function(s) {
          var date = s.match(/^(\w{3})[ ](\d{4})$/);
          var m = monthNames[date[1]];
          var y = date[2];
          return '' + y + m;
        },
        type: 'numeric'
      });

      $.tablesorter.addParser({
        id: "commaNumber",
        is: function(s) {
          return false;
        },
        format: function(s) {
          return $.tablesorter.formatFloat( s.replace(/([,\.\$])/g,'') );
        },
        type: "numeric"
      });

      $.tablesorter.addParser({
        id: "currencyUnit",
        is: function(s) {
          return false;
        },
        format: function(s) {
          console.log(s, s.replace(/\([A-Z]{3}\) [-]?(\$)/g,''));
          return $.tablesorter.formatFloat( s.replace(/\([A-Z]{3}\) (-?)(\$)/g,'$1') );
        },
        type: "numeric"
      });
    },

    sales_table_search: function(){
      $("#filter-box").keyup(function() {
        var filter = $(this).val();

        if (filter.length > 0) {
          if ($(".pagesize").val() != "10000") {
            // 500 is not already the value
            $(".pagesize").append('<option value="10000">10000</option>');
            $(".pagesize").val("10000");
          } // else do nothing
        } else {
          $(".pagesize option[value='10000']").remove();
          $(".pagesize").val("10");
        }
        $(".pagesize").change();
        $("#filter-box").quicksearch(".sales-table tbody tr", {
          'delay': 500,
          'noResults': '#noresults'
        });
      });
    },

    // Adds a collapsible link to each header anchor
    //
    // Must have the following layout:
    // <div class="collapsible"/>
    //   <someHeading /><a href="#optionalHash" class="toggle-button"/>
    //   <elementToBeHidden id="optionalHash"/>
    //
    // If there's a hash on the url corresponding to the optionalHash
    // it will be opened when loaded.
    // Add a class called dOpen to the anchor to have it be opened by default.
    //
    collapsible_panels: function (container) {
      // add toggle indicator to all links
      $(container).find('a.toggle-button').append(' <span class="fa fa-plus-circle"></span>').parent().next().hide();

      $(container).on('click', '.toggle-button', function(e) {
        e.preventDefault();
        var $this = $(this),
        item;
        $this.parent().next().slideToggle();

        if ($this.find('.fa-plus-circle').length) {
          $this.find('.fa-plus-circle').remove();
          $this.append(' <span class="fa fa-minus-circle"></span>');
        } else {
          $this.find(".fa-minus-circle").remove();
          $this.append(' <span class="fa fa-plus-circle"></span>');
        }
      });
    },

    // Adds a loading function that checks the url for a hash to
    // a song. It will then initiate the ajax to load that song table.
    //
    hash_load: function (container) {
      var $links = $(container).find('.toggle-button'); // collect the song record links

      // if there is a hash on the url, filter the links by the hash
      var toShow = window.location.hash ? window.location.hash : '';
      // if found we simulate a click on that link and the previous code will do the rest
      if (toShow !== ''){
        $links.filter(toShow).click();
      }
    },

    // Click show song sales link
    //
    // 1. Make sure table doesn't already exist
    // 2. Show spinner in placeholder
    // 3. Ajax GET song sales table
    // 4. Replace placeholder div with table
    // 5. Add song table sorting
    //
    show_song_sales: function (e) {
      if (!$(this).parent().next('div').children('table').length){ // 1.
        var $this = $(this);
        var song_id = $(this).attr("id");
        var href = $(this).attr("href");
        var $placeholder = $(this).parent().next('div');
        $placeholder.prepend('<img src="/images/icons/spinner-24x24.gif" alt="" class="add-spinner" />'); // 2.

        $.ajax({ // 3.
          url: href,
          dataType: "html"
        }).done(
          function (response) {
            $placeholder.prepend(response); // 4.
            $placeholder.find("td.sales-total-earned").each(function (index) {
              self.currency_decimal_separator($(this), 2);
            });
            $placeholder.find("td.currency span.reports-amount").each(function () {
              self.currency_decimal_separator($(this), 4);
            });
            self.song_table_sorting($placeholder.find('table'), song_id); // 5.
            $(".add-spinner").remove();
          }
        );

        return false;
      }
    },

    // Show only end dates that comes after start date
    //
    // 1. Get all the date options in arrays
    // 2. If we use the start date selector
    // 3. Disable dates which come before it in the end date selector
    // 4. If an end date which is older is already selected move the date up
    //    to the current start date
    //
    negative_dates_hider: function () {
      var $start_dates = $("#date_start_date").find("option"), // 1.
      $end_dates = $("#date_end_date").find("option"); // 1.

      $date_start_selector.change(function () { // 2.
        var selected_date = $(this).find("option:selected").val();

        $end_dates.each(function () {
          if ( $(this).val() < selected_date ) { // 3.
            $(this).prop('disabled', true);
          } else {
            $(this).prop('disabled', false);
          }
        });

        if ( $date_end_selector.find("option:selected").val() < selected_date ) { // 4.
          $date_end_selector.val(selected_date);
        }
      });
    },

    // Separates decimals on currency from the thousandths place and on from the rest of the amount
    //
    // 1. First separate the amount using the cent separator
    // 2. Then we will have three different amounts:
    //  2a. The dollar amount
    //  2b. The cents amount
    //  2c. The 0.9 cents and so on (decimal cents) amount
    // 3. Then we replace the html so 2c. will be enclosed in a span
    //
    currency_decimal_separator: function (currency, precision) {
      var delimiter = gon.currency_delimiter;
      var total = $(currency).html().split(delimiter); // 1.
      var dollar_total = total[0]; // 2a.
      var decimals_shown = total[1].substring(0,precision); // 2b.
      var rest_of_decimals = total[1].substring(2); // 2c.

      $(currency).html(dollar_total + delimiter + decimals_shown + '<span class="decimals">' + rest_of_decimals + '</span>'); // 3.
    },

    // Sets up showing and hiding the decimal cents amount
    //
    // 1. Triggers a separation of the decimal cents
    // 2. Appends the show decimal trigger link on element
    // 3. Attach click handler to the trigger
    //
    currency_decimal_hider: function () {
      self.currency_decimal_separator($sales_total, 2); // 1.
      $("td.sales-total-earned").each(function () {
        self.currency_decimal_separator($(this), 2); // 1.
      });
      $("td.currency span.reports-amount").each(function () {
        self.currency_decimal_separator($(this), 4); // 1.
      });
      if (!$(".release-sales-records").length) {
        $("th.sales-total-earned").append(' <br /><a href="#" class="toggle_decimals">Show decimals</a>');
        self.decimal_click_handler("th", "tbody");
      }
      $(".sales-totals h5").append(' <a href="#" class="toggle_decimals">Show decimals</a>'); // 2.
      self.decimal_click_handler("h5", ".sales-totals"); // 3.
    },

    // Click handler for decimal cents
    //
    // 1. When the trigger is toggled:
    //  1a. Show the decimals by appending 'show' class to container element
    //  1b. Replace the trigger text with contrary message
    // 2. When the trigger is toggled again:
    //  2a. Hide the decimals by appending 'show' class to container element
    //  2b. Replace the trigger text with original message
    //
    decimal_click_handler: function (a, b) {
      $(a + " a.toggle_decimals").toggle(function (e) {
        e.preventDefault();
        e.stopPropagation();

        $(b).addClass('show'); // 1a.
        $(a + " a.toggle_decimals").html("Hide decimals"); // 1b.
      }, function (e) {
        e.preventDefault();
        e.stopPropagation();

        $(b).removeClass('show'); // 2a.
        $(a + " a.toggle_decimals").html("Show decimals"); // 2b.
      });
    },

    // Empty filter hider
    //
    // 1. If any type has one or less items, remove that section
    // 2. If there are no release type shown as a result, remove that section
    // 3. If there are no filters shown as a result of the previous two rules, hide all the advanced options
    //
    filter_type_hider: function () {
      var $types = $("#advanced-options").find('.container'),
      $releases_group = $("#releases-filter"),
      $releases = $("#releases-filter").find('.container'),
      $advanced_options = $("#advanced-options");

      $types.each(function () {
        if ($(this).find('li').length <= 1) { // 1.
          $(this).parent('.container').remove();
        }
      });

      $releases.each(function () {
        if ($(this).find('li').length <= 1) { // 2.
          $(this).parent('.container').remove();
        }
      });

      if ($releases_group.find('.container').length < 1) {
        $releases_group.parents('#releases').remove();
      }

      if ($advanced_options.find('.container').length === 0) { // 3.
        $advanced_options.prev('h2').remove();
        $advanced_options.remove();
      }
    },

    // Show/Hide filters
    //
    // 1. When trigger is toggled:
    //  1a. Slide down hidden filters
    // 2. When trigger is again toggled:
    //  2a. Slide up visible filters
    //
    filter_show_hide: function () {
      var $container = $(".filter-container"),
      $filters = $(".sales-filters");

      $container.find("a.filter").toggle(function (e) {
        $filters.slideDown(500); // 1a.

        e.preventDefault();
      }, function (e) {
        $filters.slideUp(500); // 2a.

        e.preventDefault();
      });
    },

    // Expand and collapse the sales release table
    release_table_expand_collapse: function () {
      var $container = $("#sales-container"),
      $sales_filter = $(".sales-filters"),
      $songs_tables = $(".song-sales-record");

      $(document).on('click', "#expand", function (e) {
        e.preventDefault();
        $(".filter").attr("id", "collapse").html("Hide decimals");
        $songs_tables.addClass('expanded');
        $container.addClass("show");
      });

      $(document).on('click', "#collapse", function (e) {
        e.preventDefault();
        $container.removeClass("show");
        $(".filter").attr("id", "expand").html("Show decimals");
        $songs_tables.removeClass('expanded');
      });
    }
  };
  return self.init();
};

jQuery(document).ready(function () {
  TC.sales = new Sales();
});

// Return a unique array.
Array.prototype.getUnique = function() {
  var o = {};
  var i, e;
  for (i = 0; e == this[i]; i++) {o[e] = 1;}
  var a = [];
  for (e in o) {a.push (e);}
  return a;
};

// Fix indexOf in IE
if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function(obj, start) {
    for (var i = (start || 0), j = this.length; i < j; i++) {
      if (this[i] == obj) { return i; }
    }
    return -1;
  };
}
