/* French initialisation for the jQuery UI date picker plugin. */
/* Written by Milian Wolff (mail@milianw.de). */
( function( factory ) {
  if ( typeof define === "function" && define.amd ) {

    // AMD. Register as an anonymous module.
    define( [ "../widgets/datepicker" ], factory );
  } else {

    // Browser globals
    factory( jQuery.datepicker );
  }
}( function( datepicker ) {

datepicker.regional.fr = {
  closeText: "fermeture",
  prevText: "&#x3C;Dos",
  nextText: "Avant&#x3E;",
  currentText: "aujourd'hui",
  monthNames: [ "Janvier","Février","Mars", "Avril","Mai","Juin", "Juillet","Août","Septembre","Octobre","Novembre","Décembre" ],
  monthNamesShort: [ "Janv","Févr","Mars","Avril","Mai","Juin","Juil","Août","Sept","Oct","Nov","Déc" ],
  dayNames: [ "Dimanche", "Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi" ],
  dayNamesShort: [ "Dim", "Lun","Mar","Mer","Jeu","Ven","Sam" ],
  dayNamesMin: [ "Dim", "Lun","Mar","Mer","Jeu","Ven","Sam" ],
  weekHeader: "KW",
  dateFormat: "dd.mm.yy",
  firstDay: 1,
  isRTL: false,
  showMonthAfterYear: false,
  yearSuffix: "" };
datepicker.setDefaults( datepicker.regional.fr );

return datepicker.regional.fr;

} ) );
