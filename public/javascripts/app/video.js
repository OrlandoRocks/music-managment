/*
 *
 *  Video Creation Form
 *
 */
var VideoForm = function(){
  // note: in sizzle, #id elm.class, or just elm.class is faster than .class
  // reusable style variables
  var $ = jQuery;
  var form_input = $("div.context-wrapper input, div.context-wrapper textarea");

  var self = {

    init: function(){

  	  self.init_links();
	  self.init_locks();
	  self.check_preload();
      self.init_helper_text();
      self.number_only_field();
    },

	check_preload: function(){
		if ($('#preload').html() == 'feature') {
			$('#feature_form').removeClass("hidden");
			$('#featurebtn').addClass("lit");
		};
		if ($('#preload').html() == 'video') {
			$('#video_form').removeClass("hidden");
			$('#videobtn').addClass("lit");
		};
	},
	
	init_locks: function(){
		if ($('#prelock').html() == 'video') {

			$("#show_video").unbind('click');
			$("#show_video").addClass("greyed");
			$("#videobtn").addClass("greyed");
		} 
		
		if ($('#prelock').html() == 'feature') {
			$("#show_feature").unbind('click');
			$("#show_feature").addClass("greyed");
			$("#featurebtn").addClass("greyed");
		}
	},
	
	init_links: function(){ 

		$("#show_video").click(function() {
			$("#feature_form").fadeOut('medium', function() {$("#video_form").fadeIn('medium')} );
			$('#videobtn').addClass("lit");
			$('#featurebtn').removeClass("lit");
			$('#errorExplanation').fadeOut('medium');
			});
	
		$("#show_feature").click(function() {
			$("#video_form").fadeOut('medium',function() {$("#feature_form").fadeIn('medium')});
			$('#featurebtn').addClass("lit");
			$('#videobtn').removeClass("lit");
			$('#errorExplanation').fadeOut('medium');
	 		});
	

	},
	
    init_helper_text: function(){
      form_input.focus(self.show_helper_text);
      form_input.blur(self.hide_helper_text);
    },

    //
    // Simple methods for hiding and showing contextual help for forms
    //
    show_helper_text: function(){
      var help = $(this).next('.contextual-help');
      if (help.find('p').text().length !== 0){
        help.removeClass('hidden');
      };
    },
    hide_helper_text: function(){
      $(this).next('.contextual-help').addClass('hidden');
    },
    
    number_only_field: function(){
      $("input.numberonly").keydown(function (event) {
        if ((!event.shiftKey && !event.ctrlKey && !event.altKey) && ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105))) { // 0-9 or numpad 0-9, disallow shift, ctrl, and alt
          // check textbox value now and tab over if necessary
        } else if (event.keyCode != 8 && event.keyCode != 46 && event.keyCode != 37 && event.keyCode != 39) { // not esc, del, left or right
          event.preventDefault();
        }
        // else the key should be handled normally
      });
    }
  };

  return self.init();
};

var init_video_form = function(){
  new VideoForm();
};

jQuery(document).ready(init_video_form);


