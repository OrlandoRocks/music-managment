var Monetizations = function() {
  var filter_form = ".filter_form"
  var $links = $('.collapsible').find('.toggle-button'); // collect the toggle links
  // $options = $links.parent().next().find('li');
  var $section = $('.toggle-select'),
  querystring = $.deparam.querystring( true ),
  status = querystring.status,
  artist = querystring.artist,
  release = querystring.release,
  timer,
  select_all = '.select_all',
  deselect_all = '.deselect_all',
  table = '.table-default',
  select_all_rows = '.select_all_songs',
  deselect_all_rows = '.deselect_all_songs',
  submit_splits_btn = '.submit_splits',
  error = '#errorExplanation';
  
  var self = {
    init: function() {
      // add toggle indicator to all toggle links
      // toggle open on click
      // hide section initially
      $links.addClass('toggle_plus').on('click', self.toggleFilterOpen).parent().next().hide();
      
      // open any default toggles
      $.each($links, function() {
        var $this = $(this);
        if ( $this.data("toggle") == "open" ) {
          $(this).removeClass('toggle_plus').addClass('toggle_minus').data("toggle", "open");
          $(this).parent().next().stop().slideDown();
        }
      });
      
      // hide filter options greater than 10 shown
      $.each($section, function() {
        var $this = $(this),
        $options = $this.next().find('li');
        if ( $options.length > 10 ) {
          $options.filter(function(i) {
            if ( i > 9 ) {
              $(this).hide();
            }
          });
          $this.next().append('<li><a href="#" class="show_more">Show More&hellip;</a></li>');
        }
      });
      
      $('.monetizations').on('change', '.monetizations_checkbox', function() {
        var $$ = $(this);
        if ( $$.is(':checked') ) {
          $$.parents('tr').find('input[type="hidden"]').prop('disabled', false);
        } else {
          $$.parents('tr').find('input[type="hidden"]').prop('disabled', true);
        }
      });
      
      $(document).on('click', '.mark-ineligible', function(e) {
        e.preventDefault();
        $('#monetizations_form').attr('action', '/youtube_monetizations/mark_as_ineligible').submit();
      });
      
      $(document).on('click', '.send-to-youtube', function(e) {
        e.preventDefault();
        var action = $('#monetizations_form').attr('action');
        $('#monetizations_form').attr('action', action + '/confirm').submit();
      });
      
      $(document).on('click', '.confirm-submit', function(e) {
        e.preventDefault();
        var action = $('#monetizations_form').attr('action');
        $('#monetizations_form').submit();
      });
      
      // show more filters 10 at a time if show more is clicked
      $(document).on('click', '.show_more', function(e) {
        e.preventDefault();
        var $link = $(this).parent(),
        $container = $link.parent('ul'),
        $hidden_links = $container.find('li:hidden');
    
        // If more than 10 hidden links, keep $link and show 10 more links
        if ( $hidden_links.length > 10 ) {
          $hidden_links.filter(function(i) {
            if ( i < 10 ) {
              $(this).show();
            }
          });
        // Else less than 10 hidden links, remove $link and show remaining links
        } else {
          $link.remove();
          $hidden_links.show();
        }
      });
      
      $(filter_form).on('reset', function() {
        window.location.href = window.location.pathname;
      });
      
      // Open filter section if something is checked
      self.select_from_querystring(artist, ".artist");
      self.select_from_querystring(release, ".release");
      
      // Disable & hide search button when JS is on
      $("#search_btn").prop('disabled', true).hide();
      $(".search_form").submit(function() {return false;});
      // Perform live search after user stops typing for half a second
      $(document).on('keyup', '#search', function(e) {
        e.preventDefault();
        if (e.which == 13) {
          return false;
        }
        clearTimeout(timer);
        var search_field = this;
    
        timer = setTimeout(function() {
          self.liveSearch(search_field);
        }, 500);
      });
      $(document).on('click', '.clear_btn', function(e) {
        e.preventDefault();
        var search_field = '#search';
        $(search_field).val("");
        self.liveSearch(search_field);
      });
      
      // Select/Deselect all filter checkboxes
      $(document).on('click', select_all, function(e) {self.toggleFilterCheckbox(e, $(this), true)});
      $(document).on('click', deselect_all, function(e) {self.toggleFilterCheckbox(e, $(this), false)});
      
      // Select/Deselect all table row checkboxes
      $(document).on('click', select_all_rows, function(e) {self.toggleRowCheckbox(e, $(this), true)});
      $(document).on('click', deselect_all_rows, function(e) {self.toggleRowCheckbox(e, $(this), false)});
      
      // Submit splits of rows that are checked
      $(document).on('click', submit_splits_btn, function(e) {
        e.preventDefault();
    
        $('#splits_form').submit();
      });
    },
    
    toggleFilterCheckbox: function(e, $this, toggle) {
      e.preventDefault();
      $this.closest('.toggle-select').next('ul').find(':checkbox').prop('checked', toggle).change();
    },
    
    toggleRowCheckbox: function(e, $this, toggle) {
      e.preventDefault(); e.stopImmediatePropagation();
      $this.closest('section').find(':checkbox').prop('checked', toggle).change();
    },
    
    toggleFilterOpen: function(e) {
      e.preventDefault();
      if ( $(this).data("toggle") != "open" ) {
        $(this).removeClass('toggle_plus').addClass('toggle_minus').data("toggle", "open");
        $(this).parent().next().stop().slideDown();
      } else {
        $(this).removeClass('toggle_minus').addClass('toggle_plus').data("toggle", "close");
        $(this).parent().next().stop().slideUp();
      }
    },
    
    select_from_querystring: function(type, class_name) {
      if ( type ) {
        var $section = $(class_name),
        $list = $section.find("ul");
        if ( $section.find(".toggle-button").data("toggle") != "open" ) {
          $section.find(".toggle-button").click();
        }
      }
    },
    
    liveSearch: function(search_field) {
      var $search_field = $(search_field),
      search_term = $search_field.val(),
      $table = $(".table-container");
      $('.loading').remove();
      $search_field.after('<img src="/images/icons/spinner-16x16.gif" alt="Loading..." class="loading" style="padding-left: 10px; vertical-align: text-bottom;">');
      
      $.ajax({
        type: "GET",
        url: $('.search_form').attr('action'),
        data: $('.search_form').serialize(),
        success: function(html){
          $('.loading').remove();
          $table.html(html);
        }
      });
    }
  };
  
  self.init();
  return self;
};

jQuery(function( $ ) {
  var monetizations = new Monetizations();
});
