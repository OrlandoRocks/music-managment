//
//  Order History
//
//  Control UI in the Order History Page
//
var OrderHistory = function(){
  var $ = jQuery;
  var input = $('#search-history input[type=search]');
  var label = $('#history-search');
  

  var self = {
      init: function() {
        self.placeholder();
      },
      
      placeholder: function() {
        self.bind_events();
      },
      
      bind_events: function() {
        input.focus(self.hide_label);
        input.blur(self.show_label);
      },
      
      hide_label:function() {
       $(this).prev().addClass('hide');
      },
      
      show_label:function() {
        //requery the dom here to get current val
        if (!$(input).val()){
          $(this).prev().removeClass('hide'); 
        }
      }
    };
    
    return self.init();
};

jQuery(document).ready(function(){
  new OrderHistory();
});
