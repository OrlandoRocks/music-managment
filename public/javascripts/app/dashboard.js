/*
  Dashboard.js
  Draws a lot of pretty canvas things on the dashboard
*/

var Dashboard = function(){
  var $ = jQuery;
  var ad1link = '/videos/new';
  var ad2link = '/store/product/72';
  var ad3link = 'http://tunecore.discproductionservices.com/Quoter/Products.aspx';

  //var r = Raphael("states", 800, 170); // special Raph canvas just for the spinner Gifs
  //var s1 = makeSpinners(108), s2 = makeSpinners(358), s3 = makeSpinners(588);

  //private functions
  function createCanvas(id, url, height, width) {
    var defaultOptions = {
      height: 144,
      width: 263
    };
    var options = $.extend(defaultOptions, {'id': id, 'url': url, 'height': height, 'width': width});
    var canvas = document.createElement('canvas');
    if ($.browser.msie) { // for ie/excanvas
      G_vmlCanvasManager.initElement(canvas);
    }
    var $canvas = $(canvas);
    var link = $('<a href='+ url +'></a>');
    $canvas.attr(options);
    link.html($canvas);
    return link;
  }

  function makeSpinners(x) {
    return r.image('/images/ajax-loader-sm.gif',x,78,24,24);
  }


  var self = {

    init: function(){
      self.drawAds();
    },

    drawAds: function() {
      self.draw_ad_1();
      self.draw_ad_2();
      self.draw_ad_3();
    },

    // draw_ad_1: function(){
    //
    //   var placard ='';
    //   var publish = false;
    //   if (document.getElementById('placard_1_publishing')) {
    //     ad1link = '/songwriters';
    //     placard = $('#placard_1_publishing');
    //     publish = true;
    //   } else {
    //     placard = $('#placard_1');
    //   };
    //
    //   var canvas = createCanvas('ad_1', ad1link);
    //   placard.html(canvas);
    //
    //   var elem = document.getElementById('ad_1');
    //
    //   if (elem) { // test to see if canvas is on the page
    //     // Get the canvas 2d context.
    //     var context = elem.getContext('2d');
    //
    //
    //     // write text
    //     if (publish) {
    //       var img = new Image();
    //       img.src = '/version/shared/images/plaques/worldwide_pub_admin.jpg';
    //       $(img).load(function() {
    //         context.drawImage(this,0,0);
    //       });
    //     } else {
    //
    //       //place bg image
    //       var img = new Image();
    //       img.src = '/version/shared/images/plaques/glossy_bg.png';
    //       $(img).load(function() {
    //         context.drawImage(this,0,0);
    //       });
    //
    //     context.font = 'bold 28px Hevetica, sans-serif';
    //     context.fillStyle = "#66CCFF";
    //     context.textAlign = 'center';
    //     if (context.fillText) {
    //         context.fillText('Single Packs', 118, 62);
    //         context.fillText('Save 10%', 118, 96);
    //
    //       }
    //     }
    //        s1.remove();
    //   } else {
    //     alert("elem not found for some reason");
    //   }
    // },

    draw_ad_1: function(){
      var canvas = createCanvas('ad_1', ad1link);
      $('#placard_1').html(canvas);

      var elem = document.getElementById('ad_1');
      if (elem) {
      // Get the canvas 2d context.
        var context = elem.getContext('2d');


      //place bg image
        var img = new Image();
        img.src = '/version/shared/images/plaques/tc_musicvideo.jpg';
        $(img).load(function() {
          context.drawImage(this,0,0);
        });

      // write text
        context.font = 'bold 30px Hevetica, sans-serif';
        context.fillStyle = '#66CCFF';
        context.textAlign = 'center';
        if (context.fillText) {
        //context.fillText('Get', 118, 45);
        //context.fillText('Guaranteed', 118, 80);
        //context.fillText('Radio Plays', 118, 115);
        }
      }
      //s2.remove();
    },

    draw_ad_2: function(){
      var canvas = createCanvas('ad_2', ad2link);
      $('#placard_2').html(canvas);

      var elem = document.getElementById('ad_2');
      if (elem) {
      // Get the canvas 2d context.
        var context = elem.getContext('2d');


      //place bg image
        var img = new Image();
        img.src = '/version/shared/images/plaques/jango-dashboard.png';
        $(img).load(function() {
          context.drawImage(this,0,0);
        });

      // write text
        context.font = 'bold 30px Hevetica, sans-serif';
        context.fillStyle = '#66CCFF';
        context.textAlign = 'center';
        if (context.fillText) {
        //context.fillText('Get', 118, 45);
        //context.fillText('Guaranteed', 118, 80);
        //context.fillText('Radio Plays', 118, 115);
        }
      }
      //s1.remove();
    },

    draw_ad_3: function(){
      var canvas = createCanvas('ad_3', ad3link);
      $('#placard_3').html(canvas);

      var elem = document.getElementById('ad_3');
      if (elem) {
      // Get the canvas 2d context.
        var context = elem.getContext('2d');


      //place bg image
        var img = new Image();
        img.src = '/version/shared/images/plaques/cd-dashboard.png';
        $(img).load(function() {
          context.drawImage(this,0,0);
        });

      // write text
        context.font = 'bold 30px Hevetica, sans-serif';
        context.fillStyle = '#66CCFF';
        context.textAlign = 'center';
        if (context.fillText) {
        //context.fillText('Make 100', 118, 45);
        //context.fillText('CDs for', 118, 80);
        //context.fillText('$199', 118, 115);
        }
      }
      //s3.remove();
    },

		                                        draw_renewal_plaque: function(){
  var elem = document.getElementById('plaque-1');
  if (elem) { // test to see if canvas is on the page
        // Get the canvas 2d context.
    var context = elem.getContext('2d');

        //place bg image
    var img = new Image();
    img.src = '/version/shared/images/plaques/glossy_bg.png';
    $(img).load(function() {
      context.drawImage(this,0,0);
    });

        // write text
    context.font = 'bold 28px Hevetica, sans-serif';
    context.fillStyle = '#76C646';
    context.textAlign = 'center';
    if (context.fillText) {
      context.fillText('Your Annual', 118, 46);
      context.fillText('Renewal', 118, 76);
      context.fillText('is Due', 118, 106);
    }
  }
}
  };

  return self.init();
};

jQuery(document).ready(function(){
  var f = new Dashboard();
});
