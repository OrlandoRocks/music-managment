var TC = TC || {};

var Widget = function() {
  var $ = jQuery;
  var save_button = $(".widget_submit");
  
  var self = {
    
    init: function(){
      $(".widget_submit").click(self.submit_name);
      alert("live");
      return self;
    },
    
    submit_name: function(){
      alert("Submit clicked");
      var form = $(this).parents("form");
      var href = form.attr("action");
      var data = form.serialize();
      self.submit_name_form(href,data);
      self.disable_submit_button();
      return false;
    },
       
    submit_name_form: function(href,data){
      $.post(
        href,
        data,
        self.load_post_results,
        "html"
      );
    }
    
  }
  return self.init();
}

jQuery(document).ready(function(){
  widget = new Widget();
});