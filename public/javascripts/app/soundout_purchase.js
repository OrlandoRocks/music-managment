$(function(){

  var TCSoundoutPurchase = function() {
    var data = JSON.parse($('#data').html()),
        containerTmpl = _.template($('#containerTmpl').html().replace(/(\r\n|\n|\r)/gm, '')),
        albumNavTmpl = _.template($('#albumNavTmpl').html().replace(/(\r\n|\n|\r)/gm, '')),
        lastSearch = {},
        selected = {},
        assetType = {},
        page = 1,
        tsPage = 1,
        lastTsPage = parseInt($(".ts_pages").html()),
        form = $('#hidden_form'),
        filterForm = $('.filter_form'),
        hiddenInput = $('#hidden_input'),
        hiddenInputAsset = $('#hidden_input_asset'),
        removeAllButton = $('.remove-all'),
        addSelectedButton = $('.add-selected'),
        container = $('.album_container'),
        tsContainer = $('.soundout_songs_container'),
        tsNav = $('.soundout_nav'),
        searchInput = $('#search'),
        releaseTypeSelect = $('#release_type'),
        listBySelect = $('#list_by'),
        sortBySelect = $('#sort_by'),
        applyButton = $('.apply-button'),
        resetButton = $('.reset-button'),
        subtotal = $('.subtotal .price'),
        albumNav = $('.album_nav'),
        errorModal = $('#errorModal'),
        refreshModal = $('#refreshModal'),
        notAvailableModal = $('#notAvailableModal'),
        currentPlayer = '';

        self = {
          init: function(){
            return self.bind(),
                   self.pushSearchBox(),
                   self;
          },
          removeAll: function(){
            selected = {};
            var selects = $('.track_actions select, .album_actions select'),
                checkboxes = $('.checkbox :checkbox');
            checkboxes.prop('checked', !1);
            var firstVal = selects.first().find('option:first').val();
            selects.val(firstVal);
            return self.updateSubtotal(),
                   self;
          },
          submit: function(){
            hiddenInput.val(JSON.stringify(selected));
            hiddenInputAsset.val(JSON.stringify(assetType));
            if($.isEmptyObject(selected)){
              $.magnificPopup.open({
                items: { src: errorModal, type: 'inline' }
              });
            }
            else{
              form.submit();
            }
            return self;
          },
          changeReport: function(songID){
            var t = $('#song-'+songID),
                select = t.find('select'),
                checkbox = t.find(':checkbox');
            checkbox.is(':checked') ? selected[songID] = select.val() : delete selected[songID];
            checkbox.is(':checked') ? assetType[songID] = "song" : delete assetType[songID];
            return self.updateSubtotal(),
                   self;
          },
          changeSongLibraryReport: function(songID){
            var t = $('#song-library-'+songID),
                select = t.find('select'),
                checkbox = t.find(':checkbox');
            checkbox.is(':checked') ? selected[songID] = select.val() : delete selected[songID];
            checkbox.is(':checked') ? assetType[songID] = "song_library" : delete assetType[songID];
            return self.updateSubtotal(),
                   self;
          },
          updateSubtotal: function(){
            var total = 0;
            $.each(selected, function(k,v){
              total += parseFloat(data.report_type_prices[v]);
            });
            $.get("/price_formats/format_price", {price: total * 100, currency: data.currency, div: ".subtotal .price"})
            return self;
          },
          changePage: function(pg){
            page = parseInt(pg);
            return self.fetch($.extend(true, {}, lastSearch, {page: page})),
                   self;
          },
          search: function(){
            page = 1;
            var releaseType = releaseTypeSelect.val(),
                search = searchInput.val(),
                params = {
                  order: sortBySelect.val(),
                  page: page
                };
            if(search || !1) params.search = search;
            if(releaseType || !1) params.release_type = releaseType;
            return self.fetch(params),
                   self;
          },
          reset: function(){
            setTimeout(function(){self.search()}, 0);
            return self;
          },
          fetch: function(params){
            var listBy = listBySelect.val();
                params.per_page = (listBy == 'Album' ? 5 : 25);
                lastSearch = params;
                container.html('<div class="text-center"><img src="/images/disco-loader.gif"></div>');
            (listBy == 'Album') ? self.fetch_and_render_by_album(params) : self.fetch_and_render_by_track(params);
            return self;
          },
          fetch_and_render_by_album: function(params){
            $.getJSON('/soundout_albums.json', params, function(d){
              var isSearch = params.search || !1,
                  lookUpCount = 0;
              if(d.albums.length){
                $.each(d.albums, function(){
                  var songParams = {},
                      t = this;
                  if(isSearch && t.song_matched) songParams.search = params.search;
                  $.getJSON('/soundout_albums/'+t.id+'/soundout_songs.json', songParams, function(dd){
                    lookUpCount++;
                    t.songs = dd.songs;
                    if(lookUpCount == d.albums.length){
                      self.render(self.prep_data(d));
                    }
                  });
                });
              }
              else self.showNoResults();
            }).error(function(){
              self.showRefreshDialog();
            });
            return self;
          },
          fetch_and_render_by_track: function(params){
            $.getJSON('/soundout_songs.json', params, function(d){
              if(d.songs.length) self.render(self.prep_data(d));
              else self.showNoResults();
            }).error(function(){
              self.showRefreshDialog();
            });
            return self;
          },
          prep_data: function(d){
            var listBy = listBySelect.val();
            d.list_by = listBy;
            d.selected = selected;
            d.report_type_options = data.report_type_options;
            return d;
          },
          render: function(data){
            return container.html(containerTmpl({data: data})),
                   albumNav.html(albumNavTmpl(data)),
                   self;
          },
          showRefreshDialog: function(){
            $.magnificPopup.open({
              items: { src: refreshModal, type: 'inline' }
            });
            return self;
          },
          showNotAvailableDialog: function(){
            $.magnificPopup.open({
              items: { src: notAvailableModal, type: 'inline' }
            });
            return self;
          },

          showNoResults: function(){
            return container.html(''),
                   albumNav.html('<div class="text-left">No results found</div>'),
                   self;
          },
          showPlayer: function(tsId){
            currentPlayer = tsId;
            $('#play-button-'+tsId).hide();
            $('#ts-'+tsId).hide();
            $('#audio-container-'+tsId).show();
            return self;
          },
          closePlayer: function(tsId){
            $('#player-'+tsId).trigger('pause');
            $('#audio-container-'+tsId).hide();
            $('#play-button-'+tsId).show();
            $('#ts-'+tsId).show();
            return self;
          },
          closeCurrentPlayer: function(){
            self.closePlayer(currentPlayer);
            return self;
          },
          bind: function(){
            return self.bindClickRemoveAll()
                       .bindClickAddSelected()
                       .bindChangeReportSelect()
                       .bindChangeReportCheckbox()
                       .bindChangePageSelect()
                       .bindClickPageNext()
                       .bindClickPagePrev()
                       .bindClickApply()
                       .bindResetFilterForm()
                       .bindClickNotAvailableLink()
                       .bindPlayButton()
                       .bindClosePlayerButton()
                       .bindChangeTsPageSelect()
                       .bindClickTsPageNext()
                       .bindClickTsPagePrev(),
                   self;
          },
          bindClickRemoveAll: function(){
            removeAllButton.on('click', function(e){
              e.preventDefault();
              self.removeAll();
            });
            return self;
          },
          bindClickAddSelected: function(){
            addSelectedButton.on('click', function(e){
              e.preventDefault();
              self.submit();
            });
            return self;
          },
          bindChangeReportSelect: function(){
            container.on('change', 'select', function(){
              var t = $(this);
              self.changeReport(t.attr('song_id'));
            });
            tsContainer.on('change', 'select', function(){
              var t = $(this);
              self.changeSongLibraryReport(t.attr('song_id'));
            });
            return self;
          },
          bindChangeReportCheckbox: function(){
            container.on('change', ':checkbox', function(){
              var t = $(this);
              self.changeReport(t.attr('song_id'));
            });
            tsContainer.on('change', ':checkbox', function(){
              var t = $(this);
              self.changeSongLibraryReport(t.attr('song_id'));
            });
            return self;
          },
          bindChangePageSelect: function(){
            albumNav.on('change', '.select_page', function(){
              var t = $(this);
              self.changePage(t.val());
            });
            return self;
          },
          bindClickPageNext: function(){
            albumNav.on('click', '.next_page:not(.disabled)', function(e){
              e.preventDefault();
              self.changePage(page+1);
            });
            return self;
          },
          bindClickPagePrev: function(){
            albumNav.on('click', '.prev_page:not(.disabled)', function(e){
              e.preventDefault();
              self.changePage(page-1);
            });
            return self;
          },
          bindClickApply: function(){
            applyButton.on('click', function(e){
              e.preventDefault();
              self.search();
            });
            return self;
          },
          bindResetFilterForm: function(){
            filterForm.on('reset', function(e){
              self.reset();
            });
            return self;
          },
          bindClickNotAvailableLink: function(){
            container.on('click', '.not_available_link', function(e){
              e.preventDefault();
              self.showNotAvailableDialog();
            });
            tsContainer.on('click', '.not_available_link', function(e){
              e.preventDefault();
              self.showNotAvailableDialog();
            });
            return self;
          },
          bindPlayButton: function(){
            tsContainer.on('click', '.track-play', function(e){
              e.preventDefault();
              self.closeCurrentPlayer();
              self.showPlayer($(this).attr('tracksmart_id'));
            });
            return self;
          },
          bindClosePlayerButton: function(){
            tsContainer.on('click', '.audio-close', function(e){
              e.preventDefault();
              self.closePlayer($(this).attr('tracksmart_id'));
            });
            return self;
          },
          bindChangeTsPageSelect: function(){
            tsNav.on('change', '.select_page', function(){
              var t = $(this);
              self.changeTsPage(t.val());
            });
            return self;
          },
          bindClickTsPageNext: function(){
            tsNav.on('click', '.next_page:not(.disabled)', function(e){
              e.preventDefault();
              self.changeTsPage(tsPage+1);
            });
            return self;
          },
          bindClickTsPagePrev: function(){
            tsNav.on('click', '.prev_page:not(.disabled)', function(e){
              e.preventDefault();
              self.changeTsPage(tsPage-1);
            });
            return self;
          },
          updateTsNav: function(){
            tsNav.find("select").val(tsPage);
            if (tsPage == lastTsPage) {
              tsNav.find(".ts_next").html('<span class="next_page disabled"><i class="fa fa-chevron-circle-right fa-lg"></i></span>');
            } else {
              tsNav.find(".ts_next").html('<a href="#" class="next_page"><i class="fa fa-chevron-circle-right fa-lg"></i></a>');
            };

            if (tsPage == 1) {
              tsNav.find(".ts_prev").html('<span class="prev_page disabled"><i class="fa fa-chevron-circle-left fa-lg"></i></span>');
            } else {
              tsNav.find(".ts_prev").html('<a href="#" class="prev_page"><i class="fa fa-chevron-circle-left fa-lg"></i></a>');
            };
            return self;
          },
          changeTsPage: function(pg){
            tsPage = parseInt(pg);
            tsContainer.html('<div class="text-center"><img src="/images/disco-loader.gif"></div>');
            $.get("/api/song_library_uploads/", { page: tsPage }, function(data) {
              tsContainer.html(data);
              self.updateTsNav();
            }).error(function(){
              self.showRefreshDialog();
            });
            return self;
          },
          pushSearchBox: function() {
            var searchBox = $("aside");
            if (searchBox.length == 1) {
              searchBox.css("margin-top", $(".album_container").position().top + 10);
            }
          }
        };

    return self.init(),
           self;
  };

  new TCSoundoutPurchase();

});
