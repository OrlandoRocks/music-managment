/*
 *  Homepage:
 *
 *  A simple object to encapsulate homepage javascript logic
 *
 */
var Homepage = function(){

  //
  //  Page style definitions
  //
  var $ = jQuery; // switch to use jQuery

  var self = {

    //
    //  Setup Form Elements
    // 
    init: function(){
      new PasswordFields({
        'field': '#email'
      });
      new PasswordFields({
        'field': '#pw'
      });
    }
  };

  return self.init();
};

// Initialize on document ready 
jQuery(document).ready( function(){
  new Homepage();
  
  // Remove chrome's autocomplete colors
  if (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) {
    jQuery(window).load(function(){
      jQuery('input:-webkit-autofill').each(function(){
        var text = jQuery(this).val();
        var name = jQuery(this).attr('name');
        jQuery(this).after(this.outerHTML).remove();
        jQuery('input[name=' + name + ']').val(text);
      });
    });
  }
});


