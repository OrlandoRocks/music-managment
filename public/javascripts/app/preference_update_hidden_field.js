//	Since an unchecked checkbox does not submit a value to an html form, and we are posting directly to braintree (i.e. we can't edit this in the controller),
// 	We need to use a checkbox and a hidden form element with a value of "0" or "1" that is update via javascript when the checkbox is checked
var MarkAsPreferred = function(){
  var $ = jQuery;
  var mark_as_preferred_checkbox = $("input[name='mark_as_preferred_checkbox']");
  var hidden_field = $("input[name='merchant_defined_field_5']");

  var self = {
    init: function(){
      mark_as_preferred_checkbox.click(self.update_hidden_field);
    },
    update_hidden_field: function(){
			checked = $("input[name='mark_as_preferred_checkbox']:checked").val();
      (checked=="1") ? hidden_field.val('1') : hidden_field.val('0');
    }
  };
  return self.init();
};
jQuery(document).ready(function(){
  var mark = new MarkAsPreferred();
});