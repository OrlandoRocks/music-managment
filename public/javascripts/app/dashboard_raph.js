/*
	Dashboard.js
	Draws a lot of pretty canvas things on the dashboard
*/
var Dashboard = function(){

	var r = Raphael("plaque-2", 712, 190);
	
	var self = { init: function(){ 
		self.set_attrs();
		//self.put_up_loader();
		self.draw_background(); 
		self.draw_titles();
		setTimeout(self.draw_bars,1500);
		setTimeout(self.draw_lines,2000);
		setTimeout(self.draw_pie,2500);
		//self.kill_loader();
		},
		
	
	set_attrs: function() {
		r.g.txtattr.font = "12px 'Helvetica Neue', Helvetica, Arial, sans-serif";
		r.g.txtattr.fill = "white";
		r.g.colors = [];
		var hues = [[33,45,82],[33,100,66],[55,36,62],[10,12,92],[21,69,84],[55,55,96],[0,0,77],];


		for (var i = 0; i < 10; i++) {
		    if (i < hues.length) {
				 r.g.colors.push("hsb(" + hues[i][0] + "%, " + hues[i][1] + "%, " + hues[i][2] + "%)");
		    } else {
		    	 r.g.colors.push("hsb(" + hues[i - hues.length] + "%, 100%, 50%)");
		    }
		}
	},
	
    draw_background: function() {

		// backgrounds with a draw in 
		var bgs = r.set();
		bgs.push(
			r.image("/version/shared/images/plaques/glossy_bg.png", 10, 30, 234, 143),
			r.image("/version/shared/images/plaques/glossy_bg.png", 244, 30, 234, 143),
			r.image("/version/shared/images/plaques/glossy_bg.png", 478, 30, 234, 143)
			);
		bgs.attr({opacity:0});	
		bgs.animate({opacity:1},2000);		
    },

	draw_titles: function() {
		
		textheads = r.set();
		textheads.push(
			r.g.text(105, 0, "Sales and Streams by Month"),
        	r.g.text(335, 0, "Sales by Album March 2010"),
        	r.g.text(560, 0, "Widget Streams by Month")
			);
        textheads.attr({opacity:0,"font-weight":800});
		textheads.translate(0,-50);
		for (var i = 0, ii = textheads.length; i < ii; i++) {
			textheads[i].animate({opacity:1,y:40},(i+1)*600,">");
		}
	},
	
	draw_bars: function() {
		
		fadein = function () { 
			var y = [], res = [], type = "downloads";
			res.push();
		    for (var i = this.bars.length; i--;) {
				if (i%2) { type=" Streams"} else { type=" Downloads"}
				y.push(this.bars[i].y);
				res.push(this.bars[i].value + type || "0");
		    	}

			this.flag = r.g.popup(	this.bars[0].x, 
									Math.min.apply(Math, y), 
								  	res.join("\n")).insertBefore(this);
			};

		fadeout = function () {
				this.flag.animate({opacity: 0, cx: 20}, 300, function () {this.remove();});
			};
			
		var bars = r.g.barchart(10, 40, 234, 130, 
			[[55, 20, 0, 32, 5, 1], [10, 0, 17, 19, 32, 13]], 
			{stacked: true, type: "soft"}).hoverColumn(fadein, fadeout);
		bars.attr({opacity:0});
		bars.animate({opacity:1},500);
	},
	
	draw_lines: function() {
		
		var lines = r.g.linechart(500, 50, 200, 100, 
			[[1,2,3,4,5,6,7]], 
			[[900,850,700,740,705,620,500]], 
			{nostroke: false, axis: "0 0 1 1", symbol: "o", colors: r.g.colors})

	    lines.attr({opacity:0});
	
		lines.hoverColumn(function () {
			this.tags = r.set();
			for (var i = 0, ii = this.y.length; i < ii; i++) {
				this.tags.push(r.g.tag(
					this.x, 
					this.y[i], 
					this.values[i], 210, 10).insertBefore(this).attr(
						[{fill: "#000"}, 
						{fill: this.symbols[i].attr("fill")}]
						)
					);
				}
			}, function () {
				this.tags && this.tags.animate({opacity: 0}, 200, function() {this.hide();this.remove();});
			});
		lines.animate({opacity:1},500);
	},
	
	draw_pie: function() {
		
		var pies = r.g.piechart(310, 110, 45, [110, 30, 130, 90], {legend: ["Big Album", "Another Al..", "My Happy..", "Crazy Album"], legendcolor: "white", legendpos: "east", href: ["/my_accounts"]});

		pies.hover(function () {
			this.sector.stop();
			this.sector.scale(1.2, 1.2, this.cx, this.cy);
			if (this.label) {
				this.label[0].stop();
				this.label[0].scale(1.5);
				this.label[0].attr({stroke: "#fff"});
				this.label[1].attr({"font-weight": "bold"});
				this.label[1].scale(1.5);
			}
			}, function () {
				this.sector.animate({scale: [1, 1, this.cx, this.cy]}, 500, "bounce");
				if (this.label) {
					this.label[0].animate({scale: 1}, 500, "linear");
 					this.label[1].attr({"font-weight": 400});
					this.label[1].scale(1);
					this.label[0].attr({stroke: "#000"});
				}
			});
		
		pies.attr({opacity:0});
		pies.animate({opacity:1},500);
		
	}
	
	
	};
	return setTimeout(self.init,2000);
};



//lines.lines[0].animate({"stroke-width": 2}, 1000);
	// lines.symbols[0].attr({stroke: "#fff"});
//  lines.symbols[0][1].animate({fill: "#f00"}, 1000);

var init_dashboard = function(){
  var d = new Dashboard();
};

jQuery(document).ready(init_dashboard);
