$(function(){

  addthis.toolbox('.addthis_toolbox');

  var filterForm = $('#filter'),
      pageSelect = $('.page'),
      perPageSelect = $('.per_page');

  pageSelect.on('change', function(){
    var t = $(this);
    pageSelect.val(t.val());
    filterForm.submit();
  });

  perPageSelect.on('change', function(){
    var t = $(this);
    pageSelect.val(1);
    perPageSelect.val(t.val());
    filterForm.submit();
  });

});
