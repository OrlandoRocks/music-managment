/*

 Written to work with both old and new app layouts

 Dependencies:
  - addthis
  - zClip

*/

$(function(){

  var TC = window.TC = window.TC || {};

  TC.ShareItunesLinkModal = {

    init: function(){
      this.modal = $('#shareItunesLinkModal');
      this.shareLinks = '.shareItunesLink';
      this.copyTextArea = this.modal.find('textarea');
      this.addThis = this.modal.find('.addthis_toolbox');
      this.plusOneBadge = $('.addthis_button_google_plusone_badge');
      return this.bind(),
             this;
    },

    updateModal: function(url, title){
      return this.updateCopyTextArea(url)
                 .updateAddThis(url, title),
             this;
    },

    updateCopyTextArea: function(url){
      return this.copyTextArea.val(url),
             this;
    },

    updateAddThis: function(url, title){
      this.addThis.attr('addthis:url', url);
      this.addThis.attr('addthis:title', title);
      this.plusOneBadge.attr('g:plusone:href', url);
      addthis.toolbox('.addthis_toolbox');
      return this;
    },

    openModal: function(){
      return this.modal.dialog('open'),
             this;
    },

    closeModal: function(){
      return this.modal.dialog('close'),
             this;
    },

    bind: function(){
      return this.bindModal()
                 .bindLinks(),
             this;
    },

    bindModal: function(){
      this.modal.dialog({
        autoOpen: !1,
        resizable: !1,
        modal: !!1,
        draggable: !1,
        width: 400,
        maxWidth: 400
      });
      return this;
    },

    bindLinks: function(){
      var t = this;
      $(document).on('click', this.shareLinks, function(){
        var link = $(this),
            url = link.attr('url'),
            title = link.attr('title');
        t.updateModal(url, title)
         .openModal()
        return !1;
      });
      return this;
    }

  }.init();

});
