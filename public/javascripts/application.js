

/* The following function helps is used on the store selection page to toggle the visibility of 
rights assignments and variable price selection */


function additionalStoreInfoOn(storeId) {
  try{
    $('rights_assignment_' + storeId).removeClassName('hide');
  }
  catch(err){}
  try{    
    $('variable_pricing_assignment_' + storeId).removeClassName('hide');
  }
  catch(err){}
}

function additionalStoreInfoOff(storeId){
  try{
    $('rights_assignment_' + storeId).addClassName('hide');
  }
  catch(err){}
  try{
    $('variable_pricing_assignment_' + storeId).addClassName('hide');  
  }
  catch(err){}
}

Ajax.Responders.register({
  onCreate: function() {
    if($('busy_indicator') && Ajax.activeRequestCount>0)
      Effect.Appear('busy_indicator',{duration:0.5,queue:'end'});
  },
  onComplete: function() {
    if($('busy_indicator') && Ajax.activeRequestCount==0)
      Effect.Fade('busy_indicator',{duration:0.0,queue:'end'});
  }
});

function Popup(desired) {
  window.open('/'+desired+'.html',desired,
  'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=570,height=640,left=50,top=50'); 
}

function showHelp(dest, src) {
  if ($(dest) && $(src)) {
    $(dest).innerHTML = $(src).innerHTML;
  }
}

function showPricing(dest, src) {
    showHelp(dest, src);
    new Effect.Highlight('pricing_info', {startcolor:'#E2F5FF', endcolor:'#FFFFFF', restorecolor:'#FFFFFF'});
}


function displayNewUserQuery(index) {
  if (this.options[this.selectedIndex].value = "new_query") {
    console.log("displayNewUserQuery");
    $('new_user_query_div').setStyle( { display: 'block' } );

  }
}


function manArtworkChange() {

  sel = document.getElementById('cd_type');

  if ('two_panel' == sel.options[sel.selectedIndex].value){
    turnOffClass('four-panel');     
    turnOnClass('two-panel');     
  }else{
    turnOffClass('two-panel');     
    turnOnClass('four-panel');     
  }

}
/* aod template switching */
function updateTemplateSelection(id) {
    $('distribution_template_id').value = id;
    template = $$('#aod_templates img.reflected')[id - 1].alt.toLowerCase().gsub(' ', '');
    $('outside-booklet').setStyle({backgroundImage: 'url(/images/artwork_template/' + template + '_sprite.jpg)'});
    $('inside-booklet').setStyle({backgroundImage: 'url(/images/artwork_template/' + template + '_sprite.jpg)'});
    $('inside-traycard').setStyle({backgroundImage: 'url(/images/artwork_template/' + template + '_sprite.jpg)'});

    // uncomment the following line when we're offering artwork tool generated covers for AOD
    //$('suggested-artwork').writeAttribute('src', '/images/artwork_template/sistine-' + template + '.jpg');
}

/* oad template locking */
function freezeTemplateSelection(id) {
  $$('#aod_templates img').invoke('hide');
  $$('#aod_templates canvas').invoke('hide');
  $$('#aod_templates img')[id - 1].show();
}

function withdrawal_country_onchange() {
  select = $('withdrawal_state_select');
  text = $('withdrawal_state_text');
  country = $('withdrawal_country');
  if (country.value == 'US') {
    select.show();
    select.setAttribute('name', 'withdrawal[state]');
    text.hide();
    text.setAttribute('name', 'disabled_withdrawal_state')
  } else {
    select.hide();
    select.setAttribute('name', 'disabled_withdrawal_state');
    text.show();
    text.setAttribute('name', 'withdrawal[state]')
  }
}

function submitAOD(){
  document.getElementById("aodTemplate").submit();
}
