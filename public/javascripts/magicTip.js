/////////////////////////////////////////////////////////////////////////////////////
// MAGIC TIP ////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

(function($){
    $.fn.magicTip = function(config){
        
        var defaults = {
            position: 'topLeft',
            pointer:{
                size: 10      
            },
            delay: 1000,
            html:{
                template: '<div class="magicTipHolder">$MAGIC$TIP</div>',
                tip: '<div class="magicTip"><div class="magicTipPointer"><span>&nbsp;</span><div class="magicTipBorder"><div class="magicTipContent"></div></div></div></div>'
            },
            content: function(){
                return '';
            },
			refresh: false
        };
        var d = $.extend(true, defaults, config);
       
       // BUILD HTML ////////////////////////////////////////////////////
        
        var buildTip = function(t){ 
            $(t).addClass('magicTipTrigger');
            var magic = $('<div>').append( $(t).eq(0).clone() ).html();  
            var tip = d.html.tip;  
            var html = d.html.template;
                html = html.replace(/\$MAGIC/g, magic);
                html = html.replace(/\$TIP/g, tip);
            
            $(t).replaceWith(html);
        };

       var checkTip = function(t){
		if($(t).hasClass('magicTipTrigger')){
			return true;
		}
		else{
			return false;
		}
	   };

        var setEvents = function(){
            $('.magicTipTrigger').mouseenter(function(){ 
                if($(this).data('enter') != true){
                    $(this).data('enter', true); 
                    $(this).data('leaving', false);  
                    clearTimeout($(this).data('timeout'));
                    $('.magicTip').hide(); // hide all of them besides the one we want
                    var currentContent = $(this).siblings('.magicTip').children('.magicTipPointer').children('.magicTipBorder').children('.magicTipContent').html();
                    if(currentContent == '' || d.refresh == true){
                        $(this).siblings('.magicTip').children('.magicTipPointer').children('.magicTipBorder').children('.magicTipContent').html(getContent(this));
                    }
					var disableTip = $(this).attr('disableTip');
					if(disableTip != 'true'){
                    	$(this).siblings('.magicTip').show();
					} 
                    $(this).parents('.magicTipHolder').css('zIndex', 9000);
                    
                    // POSITION //////////////////////////////////////////////////
            
                    var thisWidth = parseInt($(this).outerWidth());
                    var thisHeight = parseInt($(this).outerHeight());
                    var tipWidth = parseInt($(this).siblings('.magicTip').outerWidth());
                    var tipHeight = parseInt($(this).siblings('.magicTip').outerHeight());  
                    var tipPosition = $(this).attr('tipPosition');
                    
                    if(tipPosition == undefined){
                        tipPosition = d.position;
                    }

                    switch(tipPosition){
                        case 'topLeft':
                            //alert('thisWidth: ' + thisWidth + ' thisHeight: ' + thisHeight);
                            $(this).siblings('.magicTip').css('left', function(){
                                return thisWidth;
                            }).css('top', function(){
                                return thisHeight;
                            });
                            $(this).siblings('.magicTip').children('.magicTipPointer').children('span').css('backgroundPosition', '0px 0px').css('left', '0').css('top', '0'); 
                            $(this).siblings('.magicTip').children('.magicTipPointer').css('paddingTop', d.pointer.size);  
                        break;
                        case 'topMiddle':
                            $(this).siblings('.magicTip').css('left', function(){
                                if(thisWidth > tipWidth){
                                    return (thisWidth - tipWidth) / 2;
                                }
                                else if(tipWidth > thisWidth){
                                    return (tipWidth - thisWidth) / 2 * -1;
                                }
                            }).css('top', function(){
                                return thisHeight;
                            });
                            $(this).siblings('.magicTip').children('.magicTipPointer').children('span').css('backgroundPosition', '0px -10px').css('left', function(){
                                return (tipWidth / 2) - (d.pointer.size / 2);
                            }).css('top', '0'); 
                            $(this).siblings('.magicTip').children('.magicTipPointer').css('paddingTop', d.pointer.size);  
                        break;
                        case 'topRight':
                            $(this).siblings('.magicTip').css('right', function(){
                                return thisWidth;
                            }).css('top', function(){
                                return thisHeight;
                            });
                            $(this).siblings('.magicTip').children('.magicTipPointer').children('span').css('backgroundPosition', '0px -20px').css('right', '0').css('top', '0'); 
                            $(this).siblings('.magicTip').children('.magicTipPointer').css('paddingTop', d.pointer.size);  
                        break;
                        case 'rightTop':
                            $(this).siblings('.magicTip').css('right', function(){
                                return thisWidth;
                            }).css('top', function(){
                                return thisHeight;
                            });
                            $(this).siblings('.magicTip').children('.magicTipPointer').children('span').css('backgroundPosition', '0px -30px').css('right', '0').css('top', '0'); 
                            $(this).siblings('.magicTip').children('.magicTipPointer').css('paddingRight', d.pointer.size);  
                        break;
                        case 'rightMiddle':
                            $(this).siblings('.magicTip').css('right', function(){
                                return thisWidth;
                            }).css('top', function(){
                                if(thisHeight > tipHeight){
                                    return (thisHeight - tipHeight) / 2;
                                }
                                else if(tipHeight > thisHeight){
                                    return (tipHeight - thisHeight) / 2 * -1;
                                }
                            });
                            $(this).siblings('.magicTip').children('.magicTipPointer').children('span').css('backgroundPosition', '0px -40px').css('right', '0').css('top', function(){
                                return (tipHeight / 2) - (d.pointer.size / 2);
                            }); 
                            $(this).siblings('.magicTip').children('.magicTipPointer').css('paddingRight', d.pointer.size);  
                        break;
                        case 'rightBottom':
                            $(this).siblings('.magicTip').css('right', function(){
                                return thisWidth;
                            }).css('bottom', function(){
                                return thisHeight;
                            });
                            $(this).siblings('.magicTip').children('.magicTipPointer').children('span').css('backgroundPosition', '0px -50px').css('right', '0').css('bottom', '0'); 
                            $(this).siblings('.magicTip').children('.magicTipPointer').css('paddingRight', d.pointer.size);  
                        break;
                        case 'bottomRight':
                            $(this).siblings('.magicTip').css('right', function(){
                                return thisWidth;
                            }).css('bottom', function(){
                                return thisHeight;
                            });
                            $(this).siblings('.magicTip').children('.magicTipPointer').children('span').css('backgroundPosition', '0px -60px').css('right', '0').css('bottom', '0'); 
                            $(this).siblings('.magicTip').children('.magicTipPointer').css('paddingBottom', d.pointer.size);  
                        break;
                        case 'bottomMiddle':
                            $(this).siblings('.magicTip').css('left', function(){
                                if(thisWidth > tipWidth){
                                    return (thisWidth - tipWidth) / 2;
                                }
                                else if(tipWidth > thisWidth){
                                    return (tipWidth - thisWidth) / 2 * -1;
                                }
                            }).css('bottom', function(){
                                return thisHeight;
                            });
                            $(this).siblings('.magicTip').children('.magicTipPointer').children('span').css('backgroundPosition', '0px -70px').css('left', function(){
                                return (tipWidth / 2) - (d.pointer.size / 2);
                            }).css('bottom', '0'); 
                            $(this).siblings('.magicTip').children('.magicTipPointer').css('paddingBottom', d.pointer.size);  
                        break;
                        case 'bottomLeft':
                            $(this).siblings('.magicTip').css('left', function(){
                                return thisWidth;
                            }).css('bottom', function(){
                                return thisHeight;
                            });
                            $(this).siblings('.magicTip').children('.magicTipPointer').children('span').css('backgroundPosition', '0px -80px').css('left', '0').css('bottom', '0'); 
                            $(this).siblings('.magicTip').children('.magicTipPointer').css('paddingBottom', d.pointer.size);  
                        break;
                        case 'leftBottom':
                            $(this).siblings('.magicTip').css('left', function(){
                                return thisWidth;
                            }).css('bottom', function(){
                                return thisHeight;
                            });
                            $(this).siblings('.magicTip').children('.magicTipPointer').children('span').css('backgroundPosition', '0px -90px').css('left', '0').css('bottom', '0'); 
                            $(this).siblings('.magicTip').children('.magicTipPointer').css('paddingLeft', d.pointer.size);  
                        break;
                        case 'leftMiddle':
                            $(this).siblings('.magicTip').css('left', function(){
                                return thisWidth;
                            }).css('top', function(){
                                if(thisHeight > tipHeight){
                                    return (thisHeight - tipHeight) / 2;
                                }
                                else if(tipHeight > thisHeight){
                                    return (tipHeight - thisHeight) / 2 * -1;
                                }
                            });
                            $(this).siblings('.magicTip').children('.magicTipPointer').children('span').css('backgroundPosition', '0px -100px').css('left', '0').css('top', function(){
                                return (tipHeight / 2) - (d.pointer.size / 2);
                            }); 
                            $(this).siblings('.magicTip').children('.magicTipPointer').css('paddingLeft', d.pointer.size);  
                        break;
                        case 'leftTop':
                            $(this).siblings('.magicTip').css('left', function(){
                                return thisWidth;
                            }).css('top', function(){
                                return thisHeight;
                            });
                            $(this).siblings('.magicTip').children('.magicTipPointer').children('span').css('backgroundPosition', '0px -110px').css('left', '0').css('top', '0'); 
                            $(this).siblings('.magicTip').children('.magicTipPointer').css('paddingLeft', d.pointer.size);  
                        break; 
                    }
                }   
            }); 
            
            $('.magicTipTrigger').mouseleave(function(){
                if($(this).data('leaving') != true){  
                    var timeout = setTimeout(function(t){return function(){mouseIsGone(t);}}($(this).siblings('.magicTip')), d.delay); 
                    $(this).data('timeout', timeout);
                    $(this).data('leaving', true);
                    $(this).data('enter', false);
                }
            }); 
            
            $('.magicTip').mouseenter(function(){
                if($(this).siblings('.magicTipTrigger').data('enter') != true){
                    $(this).siblings('.magicTipTrigger').data('enter', true); 
                    $(this).siblings('.magicTipTrigger').data('leaving', false);  
                    window.clearTimeout($(this).siblings('.magicTipTrigger').data('timeout'));
                }    
            });
            
            $('.magicTip').mouseleave(function(){      
                if($(this).siblings('.magicTipTrigger').data('leaving') != true){  
                    var timeout = setTimeout(function(t){return function(){mouseIsGone(t);}}($(this)), d.delay); 
                    $(this).siblings('.magicTipTrigger').data('timeout', timeout);
                    $(this).siblings('.magicTipTrigger').data('leaving', true);
                    $(this).siblings('.magicTipTrigger').data('enter', false);
                }
            });   
        }
        
        var mouseIsGone = function(t){         
            t.hide();
            t.parents('.magicTipHolder').css('zIndex', 0); 
        }
          
        var getContent = function(t){  
            var content = d.content(t);
            if(content != ''){
                var response = content;
            }
            else{
                var title = $(t).attr('title');
                if(title != ''){  
                    $(t).removeAttr('title');
                    $(t).attr('magicTitle', title); 
                    var response = title;   
                }
                else{
                    title = $(t).attr('magicTitle');
                    if(title != ''){
                        var response = title;
                    }
                    else{
                        var response = '';
                    } 
                }
            }
            
            return response;
        }
                
        // INIT /////////////////////////////////////////////////////////
        
        var init = function(t){ 
			if(checkTip(t) == false){
				buildTip(t);
            	setEvents();
			}
        };
        
        /////////////////////////////////////////////////////////////////
        
        return this.each(function(){
            init(this);
        });
    };
})(jQuery);

/////////////////////////////////////////////////////////////////////////////////////
// MAGIC TIP END ////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////