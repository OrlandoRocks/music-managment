if (!Cookies.get('cookieOk')) {
  var cookieWarning = jQuery(".cookie-warning"),
  eatCookie = cookieWarning.find(".eat-cookie");

  cookieWarning.removeClass("hide");

  eatCookie.on('click', function(e) {
    e.preventDefault();
    cookieWarning.addClass("hide");
    Cookies.set("cookieOk", "true", { expires: 1500 });
  });
}
