function handle_refunds_conversion() {
  var amount_input = document.querySelector('.refund-amount-input');
  var converted_amount_field = document.querySelector('#converted_amount_live');
  var pegged_rate = amount_input.getAttribute('data-pegged-rate');

  if(!!pegged_rate && pegged_rate > 0){
    var converted_value = Number(amount_input.value) * Number(pegged_rate);
    converted_amount_field.innerHTML = converted_value.toFixed(2);
  }
}

window.onload = function() {
  handle_refunds_conversion()
  document.querySelector('.refund-amount-input').addEventListener('keyup', function() {
    handle_refunds_conversion()
  });
}
