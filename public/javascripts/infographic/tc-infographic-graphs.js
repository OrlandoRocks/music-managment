tcig_graphs = {

    barSetsData: [],
    barWidthSel: '.bar-width',
    barSetSel: '.bar-set',
    barGroupSel: '.bar-group',

    init: function() {
        this.drawBarGraphs();
    },

    drawBarGraphs: function() {
        this.setBarSetEls();
        this.setBarSetsData();
        this.setBarSetsWidth();

    },

    setBarSetEls: function() {
        this.$barSets = $(this.barSetSel);
    },

    setBarSetsData: function() {
        var _this = this;
        this.$barSets.each( function(index, el) {
            _this.$barSetEl = $(el);
            _this.setBarSetData();
        });
    },

    setBarSetData: function(el) {
        var _this = this;
        this.barSetData = this.initBarSetData();
        this.setBarGroups();
        this.barSetsData.push(this.barSetData);
    },

    initBarSetData: function($barSetEl) {
        return {
            $el: this.$barSetEl,
            groups: [],
            max: 0
        };
    },

    setBarGroups: function() {
        var _this = this;
        this.$barGroups = this.$barSetEl.find(this.barGroupSel);
        this.$barGroups.each( function(index, el) {
            _this.$barGroupEl = $(el);
            _this.setBarGroupData();
        });
    },

    setBarGroupData: function() {
        var amount = this.getBarGroupAmount(),
        barSetGroupData = {
            $el: this.$barGroupEl,
            amount: this.getBarGroupAmount()
        }
        this.barSetData.groups.push(barSetGroupData);
        this.setMaxAmountOfSet(amount);
    },

    getBarGroupAmount() {
        var $el = this.$barGroupEl.find('.amount');
        var val =  $el.data('amount') || $el.text();
        return parseInt(val, 10);
    },

    setMaxAmountOfSet: function(amount) {
        this.barSetData.max = Math.max(this.barSetData.max, amount);
    },

    setBarSetsWidth: function() {
        for(setId in this.barSetsData) {
            this.barSet = this.barSetsData[setId];
            this.setBarSetWidths()
        }
    },

    setBarSetWidths: function() {
        for(groupId in this.barSet['groups']) {
            this.group = this.barSet['groups'][groupId];
            this.setBarGroupWidth();
        }
    },

    setBarGroupWidth: function() {
        var width = this.getBarGroupWidthValue();
        this.group.$el.find(this.barWidthSel).css('width', width)
    },

    getBarGroupWidthValue: function() {
        return (this.group.amount / this.barSet.max * 100) + '%';
    },



}

tcig_graphs.init();


