function inIframe () {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}

if(inIframe()) {
    $('section:not(#world-map)').remove();
    $('#world-map h3').remove();
    $('.infographic').addClass('embed');
}

$('.infographic').css('display','block');
