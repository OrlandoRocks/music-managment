var $ = jQuery;

$("#mass_retry-batch_retry").click(function() {
  if ($(this).prop("checked")) {
    $("#mass_retry-daily-retry-limit").removeClass("hidden");
  } else {
    $("#mass_retry-daily-retry-limit input").val("");
    $("#mass_retry-daily-retry-limit").addClass("hidden");
  }
});

$("#mass_retry-import-csv").click(function() {
  if ($(this).prop("checked")) {
    $("#mass_retry-uploader").removeClass("hidden");
  } else {
    $("#mass_retry-uploader input").val("");
    $("#mass_retry-uploader").addClass("hidden");
  }
});

$("#mass_takedown_form_album_level_stores_28").change(function () {
  let checked = $("#mass_takedown_form_album_level_stores_28").is(":checked")
  if (checked) {
    $(".ytsr-toggle-checkbox").prop("checked", checked)
    $("#mass_takedown_form_album_level_stores_105").prop("checked", checked)
  } 
})

$("#mass_takedown_form_album_level_stores_105").change(function () {
  let checked = $("#mass_takedown_form_album_level_stores_105").is(":checked")
  $(".ytsr-toggle-checkbox").prop("checked", checked)
  if (!checked) {
    $("#mass_takedown_form_album_level_stores_28").prop("checked", false)
  }
})

$(".ytsr-toggle-checkbox").change(function () {
  let checked = $(".ytsr-toggle-checkbox").is(":checked")
  $("#mass_takedown_form_album_level_stores_105").prop("checked", checked)
  if (!checked) {
    $("#mass_takedown_form_album_level_stores_28").prop("checked", false)
  }
})

$('.mass_takedown_submit').on('click', function (e) {
  $('.mass_takedown_submit').prop('disabled', true);
  e.preventDefault();

  var inputValue= $("#mass_takedown_form_album_ids").val();
  $.ajax({
    context: this,
    url: '/admin/petri/encumbrance_info',
    type: 'POST',
    data: {album_ids: inputValue},
    dataType: 'json'
  }).done(function (data) {
    if(data.album_ids.length > 0) {
      encumbranceText = "The following IDs/UPCs/ISRCs " + data.album_ids.join(', ') + " belong to customers who have encumbrances. Do you want to review the list, or continue with the takedown action?"
      $("#encumbrance_dailog").dialog({
        resizable: false,
        draggable: false,
        height: 100,
        width: 450,
        modal: true,
        buttons: {
          "CONTINUE": function() {
            $(".new_mass_takedown_form").submit();
          },
          "BACK TO REVIEW": function() {
            $('.mass_takedown_submit').prop('disabled', false);
            $(this).dialog("destroy");
          }
        }
      }).find(".show-encumbrance-info").html(encumbranceText);
    }
    else {
      $(".new_mass_takedown_form").submit();
    }
  }).fail(function (data) {
    return true
  });
});
