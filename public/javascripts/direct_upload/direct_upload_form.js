var DirectUpload = function(songId) {
  var uploaderForm = $("#s3-song-uploader-" + songId);
  var listItem = $('#'+ songId);
  var songId = songId;
  var fileName = "";

  // instantiate uploader
  uploaderForm.S3Uploader({additional_data:{song_id: songId}, before_add: validExtension});
  /*
  * BINDING FUNCTIONS
  **/
  uploaderForm.bind("s3_upload_complete", function(e, content) {
    listItem.find('.track-saving').show();
    pollStatus();
  });

  uploaderForm.bind("s3_upload_failed", function(e, content) {
    if (content.error_thrown == "abort"){
      listItem.find('.track-uploading').hide();
      var processing_status = listItem.attr('processing_status');
      if (processing_status == "complete") {
        listItem.find('.track-cancelreupload').show();
        listItem.find('.upload-button').show();
      } else if (processing_status == "") {
        listItem.find('.upload-button').show();
      }
      listItem.find('.track-actions').show();
      listItem.find('.track-play').hide();
    } else {
      attachErrorBox();
    }
  });

  uploaderForm.bind('fileuploadprogress', function (e, data) {
    var prog = Math.round(100*(data.loaded/data.total));
    listItem.find('.track-bar').css('width', prog+'%');
  });

  uploaderForm.bind('fileuploadadd', function (e, data) {
    jqXHR = data.submit();
  });

  listItem.find('.track-cancel').click(function (e) {
    if (jqXHR) {
      jqXHR.abort();
      jqXHR = null;
    }
    return false;
  });

  uploaderForm.bind("s3_uploads_start", function(e, content) {
    $('#reorder-toggle').hide();
    listItem.find('.track-actions').fadeOut();
    listItem.find('.upload-button').hide();
    listItem.find('.track-filename').html(fileName);
    listItem.find('.track-cancel').attr('file_id', songId);
    listItem.find('.track-uploading').show();
	  listItem.find('.track-cancelreupload').hide();
  });

  uploaderForm.bind("ajax:success", function(e, data) {
    if (data.error == "true") {
      attachErrorBox();
    } else {
      listItem.find('.upload-button').hide();
      listItem.find('.track-uploading').hide();
      listItem.find('.track-actions').show();
      listItem.find('.track-reupload').show();
      listItem.find('.track-cancelreupload').hide();
      $(".upload-form-"+songId).remove();
      $('#reorder-toggle').show();
    }
  });

  $('#'+songId).on("click", '.upload-button', function(e){
    e.stopImmediatePropagation();
    e.preventDefault();
    $('.upload-form-'+songId).find("#file").click();
  });

  /*
  * HELPER METHODS
  **/
  function validExtension(file) {
    fileName = file.name
    var extension = fileName.split('.').pop();
    if (extension != 'mp3' && extension != 'wav' && extension != 'flac') {
      attachErrorBox();
      return false;
    }
    return true;
  }

  function attachErrorBox() {
    listItem.find('.track-uploading').hide();
    listItem.find('.track-saving').hide();
    listItem.find('.track-error').html('<font style="color: red; font-size: 11px;">'+gon.could_not_upload+'</font> <a href="#" style="font-size: 11px;" class="track-retry" file_id="'+songId+'">'+gon.try_again+'</a>').show();
  }

  function pollStatus() {
    $.get("/song_assets/processing_status/"+songId, function(data) {
      if (data.processing_status !== "complete" &&  data.processing_status !== "error") {
        setTimeout(pollStatus,20000);
      } else if (data.processing_status == "complete") {
        listItem.find('.track-saving').hide();
        listItem.find('.track-error').hide();
        listItem.find('.track-actions').show();
        listItem.find('.track-play').show();
        listItem.find('.upload-button').hide();
        listItem.find('.file-name').html(fileName).show();
      } else {
        attachErrorBox();
      }
    });
  }

}
