jQuery( function () {
  var $ddGranularity = jQuery("#select-granularity")
  var $ddCountry     = jQuery("#select-country")

  var refreshReport = function () {
    window.location = "/new_reports?granularity=" + $ddGranularity.val() + "&country=" + $ddCountry.val()
  }

  $ddGranularity.on("change", refreshReport)
  $ddCountry.on("change", refreshReport)

  jQuery("h2").on("click", function (e) {
    var className = jQuery(e.target).attr("class")
    jQuery("table." + className + " tbody > tr.data").toggleClass("hide")
  })

  jQuery("table.add-totals").each( function (tableIndex, tbl) {
    rows = jQuery(tbl).children("tbody").children("tr")

    columnCount = jQuery("#column-count").val()
    totals = []
    for (i=0; i<parseInt(columnCount)+1; i++) {
      totals[i] = 0.0
    }

    rows.each( function (rowIndex, row) {
      cells = jQuery(row).children("td")
      cells.each( function(cellIndex, cell) {
        if (cellIndex > 0 && cellIndex < parseInt(columnCount)+2) {
          totals[cellIndex-1] += parseInt(jQuery(cell).html())
        }
      })
    })
    totalRow = jQuery(tbl).children("tbody").append("<tr/>")
    totalRow.append("<td class=\"head\">Totals</td>")
    sum = 0
    for(i=0; i<totals.length; i++) {
      totalRow.append("<td>" + totals[i] + "</td>")

      if (i < (totals.length-1)) {
        sum += totals[i]
      }
    }

    totalRow.append("<td>" + (sum/(totals.length - 1)).toFixed(2) + "</td>")
  })

  jQuery("table.product-sales tr.header").each( function (sectionIndex, headerRow) {
    dataRows = jQuery("tr.data." + jQuery(headerRow).attr("class").replace("header ", ""))

    columnCount = jQuery("#column-count").val()
    totals = []
    for (i=0; i<parseInt(columnCount)+1; i++) {
      totals[i] = 0.0
    }

    dataRows.each( function (rowIndex, row) {
      cells = jQuery(row).children("td")
      cells.each( function(cellIndex, cell) {
        if (cellIndex > 0 && cellIndex < parseInt(columnCount)+2) {
          totals[cellIndex-1] += parseInt(jQuery(cell).html())
        }
      })
    })

    totalRowHtml = "<tr><td class=\"head\">Totals</td>"
    sum = 0
    for(i=0; i<totals.length; i++) {
      totalRowHtml += "<td>" + totals[i] + "</td>"
      if (i < (totals.length-1)) {
        sum += totals[i]
      }
    }
    totalRowHtml += "<td>$ " + (sum/(totals.length - 1)).toFixed(2) + "</td></tr>"
    jQuery(headerRow).after(totalRowHtml)
  })
})
