(function($){	
	var instances = 0;
	var d;
	var defaults = {
		swfUrl: '/flash/SoundSpectrum.swf',
		callbacks: {
			load: function(element, mp3Url){
				
			},
			progress: function(element, mp3Url, bytesLoaded, bytesTotal){
				
			},
			complete: function(element, mp3Url, mp3Duration, bytesLoaded, bytesTotal){
				
			},
			analyzed: function(element, mp3Url){
				
			},
			play: function(element){
				
			},
			pause: function(element){
				
			},
			stop: function(element){
				
			},
			select: function(element, startTimeMs, endTimeMs, durationTimeMs){
				
			},
			cancel: function(element){
				
			},
			error: function(element, error){
				
			}
		},
		config: {
			mp3Url: '',
			seconds: 30,
			repeat: false,
			playOnMove: true,
			backgroundColor: 'FFFFFF',
			leftChannelColor: '3366FF',
			rightChannelColor: '3399FF',
			selectionBackgroundColor: 'FFCC00',
			selectionBorderColor: 'CC9900',
			selectionAlpha: '0.5',
			selectionFontColor: '000000',
			selectionFontSize: 10,
			selectionPlayColor: '000000',
			selectionPlayAlpha: '0.25',
			selectedBackgroundColor: '666666',
			selectedBorderColor: '000000',
			selectedAlpha: '0.5',
			circleAlpha: '0.5',
			circleSize: 10,
			circleBorderColor: 'CCCCCC',
			circleBackgroundColor: 'F2F2F2',
			circleFontSize: 10,
			circleFontColor: '333333'
		}
	};
	var utils = {
		getMovie: function(movie){
			if(navigator.appName.indexOf("Microsoft") != -1){
	             return window[movie];
	        } 
			else{
	             return document[movie];
	        }
		},
		getParams: function(id){
			var t = $('.'+id);
			var newParams = {
				mp3Url: t.attr('mp3Url'),
				seconds: t.attr('seconds'),
				repeat: t.attr('repeat'),
				playOnMove: t.attr('playOnMove'),
				backgroundColor: t.attr('backgroundColor'),
				leftChannelColor: t.attr('leftChannelColor'),
				rightChannelColor: t.attr('rightChannelColor'),
				selectionBackgroundColor: t.attr('selectionBackgroundColor'),
				selectionBorderColor: t.attr('selectionBorderColor'),
				selectionAlpha: t.attr('selectionAlpha'),
				selectionFontColor: t.attr('selectionFontColor'),
				selectionFontSize: t.attr('selectionFontSize'),
				selectionPlayColor: t.attr('selectionPlayColor'),
				selectionPlayAlpha: t.attr('selectionPlayAlpha'),
				selectedBackgroundColor: t.attr('selectedBackgroundColor'),
				selectedBorderColor: t.attr('selectedBorderColor'),
				selectedAlpha: t.attr('selectedAlpha'),
				circleAlpha: t.attr('circleAlpha'),
				circleSize: t.attr('circleSize'),
				circleBorderColor: t.attr('circleBorderColor'),
				circleBackgroundColor: t.attr('circleBackgroundColor'),
				circleFontSize: t.attr('circleFontSize'),
				circleFontColor: t.attr('circleFontColor')
			}
			return $.extend(true, {}, d.config, newParams);
		},
		doAction: function(movie, action){
			switch(action){
				case 'play':
					movie.play();
				break;
				case 'pause':
					movie.pause();
				break;
				case 'stop':
					movie.stop();
				break;
				case 'select':
					movie.select();
				break;
				case 'cancel':
					movie.cancel();
				break;
			}
		}
	}
	var methods = {
		init: function(t, config){
			var t = $(t);
			instances++;
			if(config != ''){
				d = $.extend(true, {}, defaults, config);
			}
			else{
				d = defaults;
			}
			t.html('<embed FlashVars="elementID=soundSpectrum'+instances+'" width="100%" height="100%" menu="false" wmode="transparent" allowscriptaccess="always" quality="high" bgcolor="#ffffff" name="soundSpectrum'+instances+'" id="soundSpectrum'+instances+'" style="" src="'+d.swfUrl+'" type="application/x-shockwave-flash">');
			t.addClass('soundSpectrum'+instances).attr('instance', 'soundSpectrum'+instances);
		},
		ready: function(params){
			var p = utils.getParams(params.elementID);
			if(p.mp3Url != ''){
				var r = utils.getMovie(params.elementID).init(p);
				if(r == '' || r.code > 0){
					// error: flash problem
					methods.error({element: params.elementID, error: 'Swf Init Error'});
				}
			}
			else{
				// error: mp3Url is null
				methods.error({element: params.elementID, error: 'Mp3 Url Required'});
			}
		},
		load: function(params){
			d.callbacks.load(params.elementID, params.mp3Url);
		},
		progress: function(params){
			d.callbacks.progress(params.elementID, params.mp3Url, params.bytesLoaded, params.bytesTotal);
		},
		complete: function(params){
			d.callbacks.complete(params.elementID, params.mp3Url, params.mp3Duration, params.bytesLoaded, params.bytesTotal);
		},
		analyzed: function(params){
			var t = $('.'+params.elementID);
			t.attr('analyzed', 'true');
			d.callbacks.analyzed(params.elementID, params.mp3Url);
		},
		play: function(params){
			d.callbacks.play(params.elementID);
		},
		pause: function(params){
			d.callbacks.pause(params.elementID);
		},
		stop: function(params){
			d.callbacks.stop(params.elementID);
		},
		select: function(params){
			d.callbacks.select(params.elementID, params.startTimeMs, params.endTimeMs, params.durationTimeMs);
		},
		cancel: function(params){
			d.callbacks.cancel(params.elementID);
		},
		error: function(params){
			d.callbacks.error(params.elementID, params.error);
		},
		control: function(t, params, action){
			var t = $(t);
			var instance = t.attr('instance');
			var analyzed = t.attr('analyzed');
			if(analyzed == 'true'){
				if(params != '' && params.elements != ''){
					$.each(params.elements, function(k, v){
						if(v == instance){
							var r = utils.doAction(utils.getMovie(instance), action);
						}
					});
				}
				else{
					var r = utils.doAction(utils.getMovie(instance), action);
				}
			}
			else{
				methods.error({element: instance, error: 'Mp3 Has Not Completed Analysis'});
			}
		}
	}	
    $.fn.SoundSpectrum = function(method, params){
        return this.each(function(){
			var t = this;
			if(params == undefined){
				params = '';
			}
            if(method != undefined){
				switch(method){
					default:
						methods.error({element: t, error: 'Invalid Method'});
					break;
					case 'init':
						methods.init(t, params);
					break;
					case 'play':
						methods.control(t, params, 'play');
					break;
					case 'pause':
						methods.control(t, params, 'pause');
					break;
					case 'stop':
						methods.control(t, params, 'stop');
					break;
					case 'select':
						methods.control(t, params, 'select');
					break;
					case 'cancel':
						methods.control(t, params, 'cancel');
					break;
					case 'swfReady':
						methods.ready(params);
					break;
					case 'swfLoad':
						methods.load(params);
					break;
					case 'swfProgress':
						methods.progress(params);
					break;
					case 'swfComplete':
						methods.complete(params);
					break;
					case 'swfAnalyzed':
						methods.analyzed(params);
					break;
					case 'swfPlay':
						methods.play(params);
					break;
					case 'swfPause':
						methods.pause(params);
					break;
					case 'swfStop':
						methods.stop(params);
					break;
					case 'swfSelect':
						methods.select(params);
					break;
					case 'swfCancel':
						methods.cancel(params);
					break;
					case 'swfError':
						methods.error(params);
					break;
				}
			}
			else{
				methods.error({element: t, error: 'Invalid Method'});
			}
        });
    };
})(jQuery);