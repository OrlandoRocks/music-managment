User-agent: *
Allow: /
Disallow: /widgets/
#Disallow: /blog/feed/							####REMOVED is there any specific reason to block this? This is a good hint for google to crawl new blog content ####
#Disallow: /blog/trackback/						####REMOVED redirects 302 /blog, no reason to block then, might even send a bad signal ######
Disallow: /blog/wp-admin/
Disallow: /blog/wp-includes/
#Disallow: /blog/xmlrpc.php						####REMOVED returns 403 anyway, no reason to block, no use in security by obscurity ####
#Disallow: /index/promotions					####REMOVED returns 403 anyway, no reason to block, no use in security by obscurity ####
#Disallow: /index/promotion						####REMOVED returns 403 anyway, no reason to block, no use in security by obscurity ####
Disallow: /store/								####ADDED Will block all pages under /store/

Sitemap: http://www.tunecore.com/sitemap.xml	####FIXED sitemap loc